angular.module('app')
    .controller('PartsSalesOrderController', function($scope, ASPricingEngine, bsAlert, $q, $http, $filter, PartsGlobal, CurrentUser, PartsSalesOrder, PartsSalesBilling, constantSO, ngDialog, CustData, $timeout, PartsCurrentUser, bsNotify, LocalService, CMaster, uiGridConstants, PrintRpt) {
        var vVATPercent = 0
        //Options Switch
        $scope.items = ['PO Bengkel Lain', 'Customer Langsung'];
        $scope.selection = $scope.items[0];
        $scope.user = CurrentUser.user();
        console.log("$scope.user", $scope.user)
            //$scope.disableSelect = false;
        $scope.actionVisible = false;
        $scope.mySelections = [];
        $scope.addcustomer = false;
        $scope.ubah_customer = false;
        $scope.kirimapprovalbilling = false;
        $scope.modalMode = "new";
        $scope.viewBengkel = false;
        $scope.viewBengkelLain = false;
        $scope.minDate = new Date();
        $scope.custAddress = {};
        $scope.dataBilling = {};
        $scope.warehousedata = {};
        $scope.mForgot = { show: false, flag: "", name: "", valueHp: "", valueEmail: "" };
        $scope.buttonSettingModal = { save: { text: 'Check Dan Kirim Toyota ID' } };
        $scope.onProcessSearch = false;
        $scope.tomboladdcustomer = {
            save: {
                text: "simpan"
            }
        }
        $scope.tombolkirimapprovalbilling = {
            save: {
                text: "Kirim Approval"
            }
        }
        $scope.isOverlayForm = false;
        $scope.captionLabel = {};
        $scope.captionLabel.new = "Buat";
        $scope.currentDate = new Date();
        $scope.CCategoryTemp = []; // kategori kastemer
        $scope.KodeJenisTransaksiTemp = [];
        $scope.DataProvinsiTemp = [];
        $scope.onProcessSave = false;
        $scope.materialtypes = 0;
        $scope.servicetypes = 0;
        $scope.isView = false;
        $scope.isEdit = false;
        $scope.UserId = 0;

        //tipe referensi
        $scope.dataTipeReferensi = [{
                id: 1,
                name: 'Bengkel Lain'
            },
            {
                id: 2,
                name: 'Penjualan Langsung'
            }
        ];

        $scope.getPaymentMethodeo  = function() {
            PartsSalesOrder.getPaymentMethode().then(function(res) {
                console.log("getPaymentMethode", res.data);
            });
        }

        $scope.optPaymentMethod = [
            // {
            //     id: 1,
            //     name: 'Cash'
            // },
            // {
            //     id: 2,
            //     name: 'Tagihan'
            // }
            {
                id: 201,
                name: 'Bank Transfer'
            },
            {
                id: 202,
                name: 'Cash'
            },
            {
                id: 203,
                name: 'Credit/Debit Card'
            }

        ];

        $scope.PaymentMethod = [
            //   {
            //   id: 1,
            //   name: 'Cash'
            // },
            // {
            //   id: 2,
            //   name: 'Tagihan'
            // }
            {
                id: 201,
                name: 'Bank Transfer'
            },
            {
                id: 202,
                name: 'Cash'
            },
            {
                id: 203,
                name: 'Credit/Debit Card'
            }
        ];

        $scope.SOStatus = [{
                id: 1,
                name: 'Request Approval'
            },
            {
                id: 2,
                name: 'Open'
            },
            {
                id: 3,
                name: 'Partial'
            },
            {
                id: 4,
                name: 'Completed'
            },
            {
                id: 5,
                name: 'Cancelled'
            }
        ];

        $scope.MaterialType = [{
                id: 0,
                name: ''
            },
            {
                id: 1,
                name: 'Parts'
            },
            {
                id: 2,
                name: 'Bahan'
            }
        ];

        $scope.vPaymentPeriod = 10;

        //model batal
        $scope.Alasan = {};
        $scope.BatalData = PartsSalesOrder.getSalesOrderHeader();

        $scope.onRegisterApi = function(gridApi) {
            $scope.myGridApi = gridApi;
        };

        $scope.afterRegisterGridApi = function(gridApi) {
            console.log(gridApi);
        }

        $scope.checkRights = function(bit) {
            console.log("Rights : ", $scope.myRights);
            var p = $scope.myRights & Math.pow(2, bit);
            var res = (p == Math.pow(2, bit));
            console.log("myRights => ", $scope.myRights);
            return res;
        }
        $scope.getRightX = function(a, b) {
            var i = 0;
            if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
            else if ($scope.checkRights(b)) i = 2;
            else if ($scope.checkRights(a)) i = 1;
            return i;
        }
        $scope.getRightMaterialType = function() {
            //var i = $scope.getRightX(8, 9);
            // if (($scope.checkRights(10) && $scope.checkRights(11))) i = 3;
            // else if ($scope.checkRights(11)) i= 2;
            // else if ($scope.checkRights(10)) i= 1;
            //return i;
            // PartsSalesOrder.getserviceandmaterial().then(function(res) {
            //     $scope.materialtypes = res.data[0].MaterialType;
            // });
            var materialTypeId = $scope.materialtypes;
            // switch($scope.user.RoleId)
            // {
            //     case 1135:
            //     case 1122:
            //     case 1123:
            //         materialTypeId = 1;
            //         break;
            //     case 1124:
            //     case 1125:
            //         materialTypeId = 2;
            //         break;
            // }
            return materialTypeId;
        }
        $scope.getRightServiceType = function() {
            // var i = $scope.getRightX(10, 11);
            // return i;
            // PartsSalesOrder.getserviceandmaterial().then(function(res) {
            //     $scope.servicetypes = res.data[0].Servicetype;
            // });
            var serviceTypeId =  $scope.servicetypes;
            // switch($scope.user.RoleId)
            // {
            //     case 1135:
            //     case 1125:
            //     case 1122:
            //         serviceTypeId = 1;
            //         break;
            //     case 1123:
            //     case 1124:
            //         serviceTypeId = 0;
            //         break;
            // }
            return serviceTypeId;
        }

        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            // $scope.getAddressList();
            //$scope.getPaymentMethodeo();
            // $scope.GetCustomer();

            var vUser = PartsCurrentUser.getCurrentUserId();
            var resData = 0;
            vUser.then(function (res) {
                console.log("res==>",res);
                resData = res.data;
                $scope.UserId = resData;
                console.log("resData==>",resData);
            });
            console.log("resData==>",resData);
            console.log("vUser==>",vUser);
            console.log("$scope.UserId==>",$scope.UserId);

            $scope.GetOutletLain();
            $scope.GetDataProvinsi();
            if ($scope.user.RoleId == 1128) {
                $scope.getDataApprove();
            }
            $scope.loading = false;

            PartsSalesOrder.getserviceandmaterial().then(function(res) {
                $scope.materialtypes = res.data[0].MaterialType;
                $scope.servicetypes = res.data[0].Servicetype;
                console.log("$scope.materialtypes=>",$scope.materialtypes);
                console.log("$scope.servicetypes=>",$scope.servicetypes);
    
                // $timeout(function() {
                    var parts = $scope.checkRights(2);
                    console.log("edit ?= ", parts);
                    console.log('user=', $scope.user);
                    console.log("Rights : ", $scope.myRights);
                    $scope.mData.OutletId = $scope.user.OrgId;
                    $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
                    $scope.mData.ServiceTypeId = $scope.getRightServiceType();
                // });
                // $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
                // $scope.mData.ServiceTypeId = $scope.getRightServiceType();
                console.log("Mat Type Id : ", $scope.mData.MaterialTypeId);
                console.log("Service Type Id : ", $scope.mData.ServiceTypeId);
                var wh = PartsGlobal.getMyWarehouse({
                    OutletId: $scope.user.OrgId,
                    MaterialTypeId: $scope.mData.MaterialTypeId,
                    ServiceTypeId: $scope.mData.ServiceTypeId
                });

                console.log("MyWareHouse : ", wh);
                if (wh == {}) {
                    console.log("wh : Not Defined")
                } else {
                    console.log("wh :", wh);
                }
                $scope.warehousedata = wh
                console.log("$scope.warehousedata", $scope.warehousedata)    
            });

            ASPricingEngine.getPPN(1, 4).then(function (res) {
                vVATPercent = res.data
            });
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.ApprovalProcessId = 8150;
        //console.log("user=>",$scope.user);
        //console.log("mode =>");
        $scope.onBulkApproveSO = function(mData) {
            console.log("mData : ", mData);
            // alert("Approve");
            // PartsGlobal.actApproval({
            //   ProcessId: 8150, DataId: mData[0].SalesOrderId, ApprovalStatus: 1
            // })
            $scope.ApproveSO(mData);
        }
        $scope.onBulkRejectSO = function(mData) {
            PartsGlobal.actApproval({
                ProcessId: 8150,
                DataId: mData[0].SalesOrderId,
                ApprovalStatus: 2
            });
            $scope.getDataApprove();
        }

        // approve
        $scope.ApproveSO = function(data) {
            console.log('ApproveDS data = ', data);
            $scope.ApproveData = data[0];
            console.log('$scope.ApproveData = ', $scope.ApproveData);
            PartsSalesOrder.setSalesOrderHeader($scope.ApproveData);
            ngDialog.openConfirm({
                template: '\
                   <div align="center" class="ngdialog-buttons">\
                   <p><b>Konfirmasi</b></p>\
                   <p>Approve dokumen Sales Order ini?</p>\
                   <div class="ngdialog-buttons" align="center">\
                     <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                     <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinApprove(ApproveData)">Ya</button>\
                   </div>\
                   </div>',
                plain: true,
                // controller: 'PartsSalesOrderController',
                scope : $scope
            });
            console.log('$scope.ApproveData bawah = ', $scope.ApproveData);
        };

        $scope.yakinApprove = function(data) {
            console.log('yakinApprove => ', data);
            $scope.mData = data;
            $scope.ngDialog.close();
            // $scope.mData.SalesOrderStatusId = 2; //status = Open
            console.log('yakinApprove mData = ', $scope.mData);
            // for approval
            PartsGlobal.actApproval({
                    ProcessId: $scope.ApprovalProcessId,
                    DataId: $scope.mData.SalesOrderId,
                    ApprovalStatus: 1
                }).then(function(res){
                    PartsSalesOrder.sendNotif(data).then(function(res){
                        console.log('notif send to login 4101')
                    });
                    bsNotify.show({
                        title: "Approval",
                        content: "Sales Order has been successfully approved.",
                        type: 'success'
                    });
                    $scope.getDataApprove();
                });
                // [START] PUT
                //30/08/2018 comment
            // PartsSalesOrder.cancel($scope.mData).then(function(res) {
            //         console.log('res cancel = ', res);
            //         bsNotify.show({
            //             title: "Approval",
            //             content: "Sales Order has been successfully approved.",
            //             type: 'success'
            //         });
            //         $scope.getDataApprove();
            //     },
            //     function(err) {
            //         console.log("err=>", err);
            //     }
            // );
            // PartsSalesOrder.sendNotif(data).then(function(res){
            //     console.log('notif send to login 4101')
            // });

            //PartsTransferOrder.formApi.setMode("grid");
        }

        //Complated
        $scope.yakinApproved = function(data) {
            console.log('yakinApprove => ', data);
            $scope.mData = data;
            $scope.ngDialog.close();
            // $scope.mData.SalesOrderStatusId = 2; //status = Open
            console.log('yakinApprove mData = ', $scope.mData);
            // for approval
            PartsGlobal.actApproval({
                    ProcessId: $scope.ApprovalProcessId,
                    DataId: $scope.mData.SalesOrderId,
                    ApprovalStatus: 1
                }).then(function(res){
                    PartsSalesOrder.sendNotif(data).then(function(res){
                        console.log('notif send to login 4101')
                    });
                    bsNotify.show({
                        title: "Approval",
                        content: "Sales Order has been completed.",
                        type: 'success'
                    });
                    $scope.getDataApprove();
                });

        }

        // $scope.searchCustomerPhone = function(item){
        //     PartsSalesOrder.SearchCustomerByCode(item).then(function(res) {
        //         console.log("Res Customer ====>", res);
        //         console.log("res.data.Result Customer ====>", res.data);
        //         // $scope.CustData = res.data;
        //         if(res.data.length > 0){
        //             //  Npwp: null
        //             // KTPKITAS
        //             $scope.mData.CustomerCode = res.data[0].CustomerCode;
        //             $scope.mData.CustomerName = res.data[0].CustomerName;
        //             $scope.mData.CustomerId = res.data[0].CustomerId;
        //             $scope.mData.Phone1 = res.data[0].Phone1;
        //             $scope.mData.Phone2 = res.data[0].Phone2;

        //             $scope.CustData = res.data;
        //         }else{
        //             bsNotify.show({
        //                 title: "Peringatan",
        //                 content: "Data customer tidak ditemukan",
        //                 type: 'danger'
        //             });
        //         }
        //     });
        // }
        $scope.disabledKategoriCustomer = false;
        $scope.loadingProcess = false;
        $scope.noResults = true;
        $scope.selectedPhone = {};
        $scope.onSelectPhone = function($item, $model, $label){
            console.log('selected Phone $item', $item);
            console.log('selected Phone $model', $model);
            console.log('selected Phone $label', $label);
            $scope.disabledKategoriCustomer = true;
            $scope.mData.CustomerCode = $item.CustomerCode;
            $scope.mData.CustomerName = $item.CustomerName ? $item.CustomerName : $item.Name ;
            $scope.mData.CustomerId = $item.CustomerId;
            $scope.mData.Handphone1 = $item.Handphone1;
            $scope.mData.Handphone2 = $item.Handphone2;
            $scope.mData.CustomerTypeId = $item.CustomerTypeId;
            $scope.mData.PICName = $item.PICName;
            $scope.mData.PICHp = $item.PICHp;
            $scope.mData.Npwp = $item.Npwp;
            $scope.mData.KTPKITAS = $item.KTPKITAS;
            $scope.ubahCust = $item;
            $scope.ubahCust.nama = $item.CustomerName ? $item.CustomerName : $item.Name ;
            $scope.ubahCust.phone1 = $item.Handphone1;
            $scope.ubahCust.phone2 = $item.Handphone2;
            $scope.ubahCust.Npwp = $item.Npwp;
            $scope.ubahCust.KTPKITAS = $item.KTPKITAS;
            $scope.ubahCust.alamat = $item.Address;
            $scope.ubahCust.rt = $item.rt;
            $scope.ubahCust.rw = $item.rw;
            $scope.ubahCust.ProvinceId = $item.ProvinceId;
            $scope.ubahCust.CityRegencyId = $item.CityRegencyId;
            $scope.ubahCust.DistrictId = $item.DistrictId;
            $scope.ubahCust.VillageId = $item.VillageId;
            $scope.ubahCust.PostalCode = $item.PostalCode;
            $scope.ubahCust.CustomerTypeId = $item.CustomerTypeId;
            $scope.ubahCust.PICName = $item.PICName;
            $scope.ubahCust.PICHp = $item.PICHp;
            console.log("Masuk Pak Eko =>",$item.CityRegencyId)
        }
        $scope.onNoResult = function(){
            $scope.mData.CustomerCode = null;
            $scope.mData.CustomerName = null;
            $scope.mData.CustomerId = null;
            $scope.mData.Handphone1 = null;
            $scope.mData.Handphone2 = null;
            $scope.mData.Npwp = null;
            $scope.mData.KTPKITAS = null;
            bsNotify.show({
                title: "Peringatan",
                content: "Data customer tidak ditemukan",
                type: 'danger'
            });
        }
        $scope.onGotResult = function(){

        }
        $scope.searchCustomerPhone = function(item){
            var response = PartsSalesOrder.SearchCustomerByCode(item).then(function(res) {
                _.map(res.data, function(val){
                    console.log('=====>', val.PICHp, val.PICName)
                    val.xphoneName = (val.Handphone1 !== null ? val.Handphone1 : val.PICHp) +' - '+ (val.CustomerName !== null ? val.CustomerName : val.PICName);
                    console.log('=====>', val.PICHp, val.PICName)

                });
                return res.data
            });
            return response
        }

        // Reject
        $scope.RejectSO = function(data) {
            console.log('Reject data = ', data);
            $scope.RejectData = data[0];
            console.log('$scope.RejectData = ', $scope.RejectData);
            PartsSalesOrder.setTransferOrderHeader($scope.RejectData);
            ngDialog.openConfirm({
                template: '\
                   <div align="center" class="ngdialog-buttons">\
                   <p><b>Konfirmasi</b></p>\
                   <p>Reject dokumen Sales Order ini?</p>\
                   <div class="ngdialog-buttons" align="center">\
                     <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                     <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinReject(RejectData)">Ya</button>\
                   </div>\
                   </div>',
                plain: true,
                controller: 'PartsSalesOrderController',
            });
            //console.log('$scope.RejectData bawah = ', $scope.RejectData);
        };

        $scope.yakinReject = function(data) {
            console.log('yakinReject => ', data);
            $scope.mData = data;
            $scope.ngDialog.close();
            $scope.mData.SalesOrderStatusId = 13; //status = Reject
            console.log('yakinReject mData = ', $scope.mData);
            // for approval
            PartsGlobal.actApproval({
                    ProcessId: $scope.ApprovalProcessId,
                    DataId: $scope.mData.SalesOrderId,
                    ApprovalStatus: 2
                })
                // [START] PUT
            PartsSalesOrder.cancel($scope.mData).then(function(res) {
                    console.log('res cancel = ', res);
                    bsNotify.show({
                        title: "Approval",
                        content: "Sales Order has been successfully rejected.",
                        type: 'succes'
                    });
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            //PartsTransferOrder.formApi.setMode("grid");
        }


        var newDataTest = {};
        $scope.mData = {}; //Model
        $scope.mData.GridDetail = {}; //Model
        $scope.xRole = {
            selected: []
        };

        $scope.ngDialog = ngDialog;
        $scope.formApi = {};
        $scope.mData.GridDetail.isLunas = false;
        $scope.mData.OutletId = {};

        //$scope.mData.TimeNeeded = $filter('date')(new Date(), 'yyyy-MM-ddTHH:mm:ssZ');
        $scope.mData.TimeNeeded = new Date();
        //$scope.mData.TimeNeeded = {};

        $scope.ApproveData = PartsSalesOrder.getSalesOrderHeader();
        $scope.ApproveDataDetail = PartsSalesOrder.getSalesOrderDetail();

        $scope.ubahCust = {}; // model untuk edit customer

        // $scope.waktuSekarang = function () {
        //   function checkTime(i) {
        //       return (i < 10) ? "0" + i : i;
        //   }
        //   var today = new Date(),
        //     h = checkTime(today.getHours()),
        //     m = checkTime(today.getMinutes()),
        //     s = checkTime(today.getSeconds());
        //     $scope.mData.TimeNeeded = h + ':' + m + ':' + s;
        //     console.log("$scope.mData.TimeNeeded =>", $scope.mData.TimeNeeded);
        // }

        $scope.localeDate = function(data) {
            var tmpDate = new Date(data);
            var resDate = new Date(tmpDate.toISOString().replace("Z","-0700")).toISOString(); 
            return resDate;
        }
        
        //button kembali pada overlay
        $scope.goBack = function() {
            $scope.isOverlayForm = false;
            $scope.formApi.setMode("grid");
            $scope.mData = {};
        }
        $scope.goBack2 = function() {
            $scope.isOverlayForm = false;
            $scope.formApi.setMode("grid");
        }

        //----------------------------------
        // Get Data
        //----------------------------------
        //$scope.gridData = [];
        var gridData = [];
        $scope.getData = function(mData) {
            console.log("getData mData = ", $scope.mData);
            console.log("getData SalesOrderStatusId = ", $scope.mData.SalesOrderStatusId);
            console.log("$scope.tmpFilter1==>", $scope.tmpFilter);
            $scope.tmpFilter = mData;
            console.log("$scope.tmpFilter2==>", $scope.tmpFilter);
            if (mData.SalesOrderStatusId > 0) {
                console.log("SalesOrderStatusId > 0 ", mData.SalesOrderStatusId > 0);
                if (mData.startDate == null || mData.startDate == 'undefined' || mData.startDate == "") {
                    bsNotify.show({
                        title: "Mandatory",
                        content: "Tanggal Sales Order belum diisi",
                        type: 'danger'
                    });
                } else if (mData.endDate == null || mData.endDate == 'undefined' || mData.endDate == "") {
                    bsNotify.show({
                        title: "Mandatory",
                        content: "Tanggal Sales Order (end date) belum diisi",
                        type: 'danger'
                    });
                } else {
                    PartsSalesOrder.getData(mData).then(function(res) {
                        var gridData = res.data.Result;
                        console.log("<controller getData> GridData => ", gridData);
                        console.log("getData mData 2 = ", $scope.mData);
                        var tmpRes = res.data.Result;
                        _.map(tmpRes, function(val) {
                            if (val.RefSOTypeId == "1") {
                                val.RefSOTypeIdDesc = "PO Bengkel Lain"
                            } else {
                                val.RefSOTypeIdDesc = "Customer Langsung"
                            }

                            if (val.SalesOrderStatusId == "1") {
                                val.SalesOrderStatusIdDesc = "Request Approval"
                            } else if (val.SalesOrderStatusId == "2") {
                                val.SalesOrderStatusIdDesc = "Open"
                            } else if (val.SalesOrderStatusId == "3") {
                                val.SalesOrderStatusIdDesc = "Partial"
                            } else if (val.SalesOrderStatusId == "4") {
                                val.SalesOrderStatusIdDesc = "Completed"
                            } else if (val.SalesOrderStatusId == "5") {
                                val.SalesOrderStatusIdDesc = "Canceled"
                            } else if (val.SalesOrderStatusId == "13") {
                                val.SalesOrderStatusIdDesc = "Rejected"
                            }

                            // var tglstring = val.DocDate.toISOString().split('T');
                            // val.DocDateDesc = tglstring[0];
                            var tglstring = angular.copy(val.DocDate);
                            var month = tglstring.getMonth() +1;
                            if (month < 10){
                                month = '0'+month;
                            }
                            val.DocDateDesc = tglstring.getFullYear() + '-' + month + '-' + tglstring.getDate();
                        })

                        for(var i in gridData) {
                            if (gridData[i].CreatedDate != null) {
                                gridData[i].CreatedDate = $scope.localeDate(gridData[i].CreatedDate);
                            }
                        }
       
                        $scope.grid.data = gridData;
                        $scope.loading = false;
                    },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
                } // end else
            } else {
                PartsSalesOrder.getData(mData).then(function(res) {
                    var gridData = res.data.Result;
                    console.log("<controller getData> GridData => ", gridData);
                    console.log("getData mData 3 = ", $scope.mData);
                    var tmpRes = res.data.Result;
                    _.map(tmpRes, function(val) {
                        if (val.RefSOTypeId == "1") {
                            val.RefSOTypeIdDesc = "PO Bengkel Lain"
                        } else {
                            val.RefSOTypeIdDesc = "Customer Langsung"
                        }

                        if (val.SalesOrderStatusId == "1") {
                            val.SalesOrderStatusIdDesc = "Request Approval"
                        } else if (val.SalesOrderStatusId == "2") {
                            val.SalesOrderStatusIdDesc = "Open"
                        } else if (val.SalesOrderStatusId == "3") {
                            val.SalesOrderStatusIdDesc = "Partial"
                        } else if (val.SalesOrderStatusId == "4") {
                            val.SalesOrderStatusIdDesc = "Completed"
                        } else if (val.SalesOrderStatusId == "5") {
                            val.SalesOrderStatusIdDesc = "Canceled"
                        } else if (val.SalesOrderStatusId == "13") {
                            val.SalesOrderStatusIdDesc = "Rejected"
                        }

                        // var tglstring = val.DocDate.toISOString().split('T');
                        // val.DocDateDesc = tglstring[0];
                        var tglstring = angular.copy(val.DocDate);
                        var month = tglstring.getMonth() +1;
                        if (month < 10){
                            month = '0'+month;
                        }
                        val.DocDateDesc = tglstring.getFullYear() + '-' + month + '-' + tglstring.getDate();
                    })

                    for(var i in gridData) {
                        if (gridData[0].CreatedDate != null) {
                            gridData[i].CreatedDate = $scope.localeDate(gridData[i].CreatedDate);
                        }
                    }

                    $scope.grid.data = gridData;
                    $scope.loading = false;
                },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            } // end else
        } // end getData

        $scope.getDataApprove = function() {
            console.log('mData', $scope.mData);
            $scope.mData.SalesOrderStatusId = 1;
            $scope.mData.OutletId = $scope.user.OrgId;
            PartsSalesOrder.getDataApprove($scope.mData).then(function(res) {
                var gridData = res.data.Result;
                console.log("<controller getData> GridData => ", gridData);
                var tmpRes = res.data.Result;
                _.map(tmpRes, function(val) {
                    if (val.RefSOTypeId == "1") {
                        val.RefSOTypeIdDesc = "PO Bengkel Lain"
                    } else {
                        val.RefSOTypeIdDesc = "Customer Langsung"
                    }

                    if (val.SalesOrderStatusId == "1") {
                        val.SalesOrderStatusIdDesc = "Request Approval"
                    } else if (val.SalesOrderStatusId == "2") {
                        val.SalesOrderStatusIdDesc = "Open"
                    } else if (val.SalesOrderStatusId == "3") {
                        val.SalesOrderStatusIdDesc = "Partial"
                    } else if (val.SalesOrderStatusId == "4") {
                        val.SalesOrderStatusIdDesc = "Completed"
                    } else if (val.SalesOrderStatusId == "5") {
                        val.SalesOrderStatusIdDesc = "Canceled"
                    } else if (val.SalesOrderStatusId == "13") {
                        val.SalesOrderStatusIdDesc = "Rejected"
                    }

                    //   var tglstring = val.DocDate.toISOString().split('T');
                    //   val.DocDateDesc = tglstring[0];
                    var tglstring = angular.copy(val.DocDate);
                    var month = tglstring.getMonth() +1;
                    if (month < 10){
                        month = '0'+month;
                    }
                    val.DocDateDesc = tglstring.getFullYear() + '-' + month + '-' + tglstring.getDate();

                })

                for(var i in gridData) {
                    gridData.CreatedDate = $scope.localeDate(gridData[i].CreatedDate);
                }

                $scope.grid.data = gridData;
                $scope.loading = false;
            },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        // Hapus Filter
        $scope.onDeleteFilter = function() {
            //Pengkondisian supaya seolaholah kosong
            $scope.mData.SalesOrderStatusId = null;
            $scope.mData.startDate = null;
            $scope.mData.endDate = null;
            // $scope.mData.SalesOrderNo = null;
            // $scope.mData.RefSONo = null;
            // $scope.mData.Bengkel = null;
            // $scope.mData.CustomerName = null;
            $scope.mData.vSalesOrderNo = null;
            $scope.mData.vRefSONo = null;
            $scope.mData.vBengkel = null;
            $scope.mData.vCustomerName = null;
        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
            $scope.testmodel = rows;
            console.log("Isi test component =>", $scope.testmodel.VehicleName);
        }

        $scope.alertAfterSave = function(item) {
            var noso = item.ResponseMessage;
            noso = noso.substring(noso.lastIndexOf('#') + 1);
            bsAlert.alert({
                    title: "Data tersimpan dengan Nomor Sales Order",
                    text: noso,
                    type: "success",
                    showCancelButton: false
                },
                function() {
                    // $scope.formApi.setMode('grid');
                    $scope.goBack();
                },
                function() {

                }
            )
        }

        $scope.alertAfterSave2 = function(item1, item2) {
            var noso = item1.ResponseMessage;
            var nosb = item2.ResponseMessage;
            console.log('noso', noso);
            console.log('nosb', nosb);
            noso = noso.substring(noso.lastIndexOf('#') + 1);
            nosb = nosb.substring(nosb.lastIndexOf('#') + 1);
            bsAlert.alert({
                    title: "Berhasil tersimpan",
                    text: "Nomor Sales Order: " + noso + " & " + " Nomor Sales Billing: " + nosb,
                    type: "warning",
                    showCancelButton: false
                },
                function() {
                    // $scope.formApi.setMode('grid');
                    $scope.goBack();
                },
                function() {

                }
            )
        }


        // ======== tombol  simpan =========
        $scope.simpan = function() {
            ngDialog.openConfirm({
                template: '<div ng-include=\"\'app/parts/directpartssales/salesorder/referensi/dialog_simpan.html\'\"></div>',
                plain: true,
                controller: 'PartsSalesOrderController',
            });
        };
        // ======== end tombol  simpan =========

        //POST - SIMPAN DATA
        //$scope.SimpanData = function(refDisposal, tipeMaterial, mData){
        $scope.SimpanDatanya = function(mData) {
            console.log("datanya", mData);
        };
        // $scope.SimpanDataBilling = function (refTypeSO, statusSO, mData){
        //   $scope.mData['RefSOTypeId'] = refTypeSO;
        //   $scope.mData['SalesOrderStatusId'] = statusSO;
        //   $scope.mData.OutletId = loginData[0].OutletId;
        //   $scope.mData.WarehouseId = loginData[0].WarehouseId;
        //   PartsSalesBilling.setSalesBillingHeader($scope.mData);
        //   PartsSalesBilling.setSalesBillingDetail($scope.mData.GridDetail);
        // };

        // GENERATE DOCUMENT NUMBER
        $scope.GenerateDocNum = function() {
            console.log("$scope.mData.ServiceTypeId = ", $scope.mData.ServiceTypeId);
            PartsCurrentUser.getFormatId($scope.mData.ServiceTypeId, 'SO').then(function(res) {
                    var result = res.data;
                    //console.log("FormatId result = ", result);
                    console.log("FormatId = ", result[0].Results);

                    PartsSalesOrder.getDocumentNumber(result[0].Results).then(function(res) {
                            var DocNo = res.data;
                            if (typeof DocNo === 'undefined' || DocNo == null) {
                                bsNotify.show({
                                    title: "Sales Order",
                                    content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                                    type: 'danger'
                                });
                            } else {
                                if(DocNo[1]){
                                    var date = new Date(DocNo[1]);
                                    var today = new Date();
                                    date.setHours(today.getHours(),today.getMinutes(),today.getSeconds());
                                }
                                $scope.mData.SalesOrderNo = DocNo[0];
                                $scope.mData.DocDate = date;
                                console.log("Generating DocNo & DocDate ->");
                                console.log($scope.mData.SalesOrderNo);
                                console.log($scope.mData.DocDate, date);
                            }
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            // [END] Get Current User Warehouse
        }

        $scope.SimpanData = function(refTypeSO, statusSO, mData) {
                $scope.mData = mData;
                var UserId = '';
                var DocNo = '';
                $scope.isApproved = true;
                console.log("$scope.mData=>",$scope.mData);

                $scope.mData['RefSOTypeId'] = refTypeSO;
                $scope.mData['SalesOrderStatusId'] = statusSO;
                //$scope.mData['MaterialTypeId'] = tipeMaterial;
                var OutletId
                    // [START] Get Current User Warehouse
                var param_warehouse = {}
                param_warehouse = {
                    OutletId: $scope.user.OrgId,
                    MaterialTypeId: $scope.mData.MaterialTypeId,
                    ServiceTypeId: $scope.mData.ServiceTypeId
                }
                PartsGlobal.getMyWarehouse(param_warehouse).then(function(res) {
                        var loginData = res.data;
                        console.log("loginData", loginData.Result[0]);
                        $scope.mData.OutletId = $scope.user.OrgId;
                        $scope.mData.WarehouseId = loginData.Result[0].WarehouseId;
                        $scope.mData.WarehouseName =
                            loginData.Result[0].WarehouseName.split(" ").pop();

                        // $scope.mData.WarehouseId = loginData[0].WarehouseId;
                        if (LocalService.get('BSLocalData')) {
                            LoginObject = angular.fromJson(LocalService.get('BSLocalData')).LoginData;
                        }

                        UserId = angular.fromJson(LoginObject).UserId;

                        PartsSalesOrder.getDocumentNumber(UserId).then(function(res) {

                                DocNo = res.data;
                                console.log('DocNo => ', DocNo);
                                $scope.mData.SalesOrderNo = DocNo[0];

                                $scope.mData['RefSOTypeId'] = refTypeSO;
                                $scope.mData['SalesOrderStatusId'] = statusSO;
                                $scope.mData['SOStatusId'] = statusSO; // ini hanya untuk validasi

                                console.log('RefSOTypeId before create => ', $scope.mData.RefSOTypeId);

                                // start validasi DP kecil

                                switch ($scope.mData.RefSOTypeId) {
                                    case 1:
                                        $scope.mData.GridDetail = $scope.gridOptions.data;
                                        break;
                                    case 2:
                                        $scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
                                        break;
                                }

                                var n = 0;
                                var x = 0;
                                for (var i = 0; i < $scope.mData.GridDetail.length; i++) {

                                    // if ($scope.mData.GridDetail[i].DPAmount < $scope.mData.GridDetail[i].RetailPriceDP) {
                                    //     n = n + 1;
                                    //     console.log('n in Loop = ', n);
                                    //     console.log('$scope.mData.GridDetail[i].RetailPriceDP = ', $scope.mData.GridDetail[i].RetailPriceDP);
                                    // };

                                    if (typeof($scope.mData.GridDetail[i].DPAmount) == 'undefined') {
                                        x = x + 1;
                                    };

                                    console.log('$scope.mData.GridDetail[i].DPAmount  = ', $scope.mData.GridDetail[i].DPAmount);
                                    console.log('x = ', x);
                                }

                                PartsSalesOrder.GetCheckDiskonAppoval($scope.mData.GridDetail[i].DiscountSpecialPercent, $scope.user.OrgId, $scope.mData.ServiceTypeId).then(function(res) {
                                    console.log("somewhere only we know",res)
                                    if (res.data!= null || res.data!= undefined)
                                    {
                                        n = 2;
                                        console.log("somewhere ly we know",res.data.result)
                                    }
                                    else{
                                        n = 0;
                                    }
                                });


                                console.log('n end = ', n);
                                //console.log('RetailPriceDP end = ', $scope.mData.GridDetail[i].RetailPriceDP);
                                //if($scope.mData.GridDetail[0].DPAmount == 0){

                                if (x > 0) {
                                    bsNotify.show({
                                        title: "Sales Order",
                                        content: "Data DP Amount belum diisi",
                                        type: 'danger'
                                    });
                                } else
                                if (n == 0) {

                                    for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                                        //console.log($scope.mData.GridDetail[i].PartsName);
                                        $scope.mData.GridDetail[i].SalesOrderId = $scope.mData.SalesOrderId;
                                        $scope.mData.GridDetail[i].SalesOrderStatusId = $scope.mData.SalesOrderStatusId;
                                        $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
                                        $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;

                                        $scope.mData.GridDetail[i].MaterialRequestStatusId = 1;
                                    }
                                    console.log('$scope.mData.GridDetail => ', $scope.mData.GridDetail);

                                    PartsSalesOrder.create($scope.mData, $scope.mData.GridDetail).then(function(res) {
                                            var insertResponse = res.data;
                                            console.log("what is like ", $scope.mData);
                                            console.log("insertResponse => ", res.data);
                                            bsNotify.show({
                                                title: "SalesOrder",
                                                content: "Data Header berhasil disimpan",
                                                type: 'succes'
                                            });
                                            $scope.SalesOrderIdnya = 0;
                                            var idSO = insertResponse.ResponseMessage.split('#');
                                            $scope.SalesOrderIdnya = idSO[1].substr(1, idSO[1].length-2)
                                            $scope.SalesOrderIdnya = parseInt($scope.SalesOrderIdnya )
                                            // $scope.SalesOrderIdnya = insertResponse.ResponseMessage.substring(18, 16);
                                            $scope.cetakPPL($scope.SalesOrderIdnya);
                                            $scope.alertAfterSave(insertResponse);


                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                        }
                                    ); // end PartsSalesOrder.create


                                } else {
                                    console.log('mData = ', mData);
                                    console.log('DocNo => ', DocNo);
                                    //$scope.mData.GridDetail = $scope.gridOptions.data;
                                    PartsSalesOrder.setSalesOrderHeader($scope.mData);

                                    switch ($scope.mData.RefSOTypeId) {
                                        case 1:
                                            $scope.mData.GridDetail = $scope.gridOptions.data;
                                            break;
                                        case 2:
                                            $scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
                                            break;
                                    }
                                    PartsSalesOrder.setSalesOrderDetail($scope.mData.GridDetail);
                                    //$scope.mData.Grid = $scope.gridOptions.data;

                                    ngDialog.openConfirm({
                                        template: '<div ng-include=\"\'app/parts/directpartssales/salesorder/referensi/dialog_approval.html\'\"></div>',
                                        plain: true,
                                        controller: 'PartsSalesOrderController',
                                    }).catch(function(value) {
                                        $scope.isApproved = false;
                                        console.log("masuk cancel=>",value);
                                    });
                                }
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        ); // end PartsSalesOrder.getDocumentNumber

                        console.log("Persiapan Create => ");
                        console.log($scope.mData);

                        //$scope.mData = {};
                        //$scope.mData.GridDetail = {};
                        // // [START] Get Search Appointment
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
                // [END] Get Current User Warehouse


            } // end POST




        $scope.SimpanDataBilling = function(refTypeSO, statusSO, mData) {
            $scope.mData = mData;
            var param_warehouse = {}
            var UserId = '';
            var DocNo = '';
            var Doctype = '';
            var DocName = '';
            // var wartype = '';
            $scope.isApproved = true;

            $scope.mData['RefSOTypeId'] = refTypeSO;
            $scope.mData['SalesOrderStatusId'] = statusSO;
            //$scope.mData['MaterialTypeId'] = tipeMaterial;
            param_warehouse = {
                OutletId: $scope.user.OrgId,
                MaterialTypeId: $scope.mData.MaterialTypeId,
                ServiceTypeId: $scope.mData.ServiceTypeId
            }
            PartsGlobal.getMyWarehouse(param_warehouse).then(function(res) {
                    var loginData = res.data;
                    console.log("loginData", loginData.Result[0]);
                    $scope.mData.OutletId = $scope.user.OrgId;
                    $scope.mData.WarehouseId = loginData.Result[0].WarehouseId;
                    $scope.mData.WarehouseName = loginData.Result[0].WarehouseName.split(" ").pop();

                    if ($scope.mData.WarehouseName == "GR") {
                        console.log("gr");
                        Doctype = 1
                        DocName = 'SB'
                    } else {
                        console.log("bp");
                        Doctype = 2
                        DocName = 'SB'
                    }
                    var loginData = res.data;
                    if (LocalService.get('BSLocalData')) {
                        LoginObject = angular.fromJson(LocalService.get('BSLocalData')).LoginData;
                    }

                    UserId = angular.fromJson(LoginObject).UserId;
                    console.log("Doctype", Doctype);
                    PartsCurrentUser.getFormatId(Doctype, DocName).then(function(res) {

                            DocNo = res;
                            console.log('DocNo => ', DocNo);
                            $scope.mData.SalesOrderNo = DocNo[0];

                            $scope.mData['RefSOTypeId'] = refTypeSO;
                            $scope.mData['SalesOrderStatusId'] = statusSO;

                            console.log('RefSOTypeId before create => ', $scope.mData.RefSOTypeId);

                            // start validasi DP kecil

                            switch ($scope.mData.RefSOTypeId) {
                                case 1:
                                    $scope.mData.GridDetail = $scope.gridOptions.data;
                                    break;
                                case 2:
                                    $scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
                                    break;
                            }

                            var n = 0;
                            var x = 0;
                            console.log('n var = ', n);
                            console.log('$scope.mData.GridDetail.length = ', $scope.mData.GridDetail.length);
                            console.log('$scope.mData.GridDetail[0].DPAmount = ', $scope.mData.GridDetail[0].DPAmount);
                            console.log('$scope.mData.GridDetail[0].RetailPriceDP = ', $scope.mData.GridDetail[0].RetailPriceDP);
                            for (var i = 0; i < $scope.mData.GridDetail.length; i++) {

                                if ($scope.mData.GridDetail[i].DPAmount < $scope.mData.GridDetail[i].RetailPriceDP) {
                                    n = n + 1;
                                    console.log('n in Loop = ', n);
                                    console.log('$scope.mData.GridDetail[i].RetailPriceDP = ', $scope.mData.GridDetail[i].RetailPriceDP);
                                };

                                if (typeof($scope.mData.GridDetail[i].DPAmount) == 'undefined') {
                                    x = x + 1;
                                };

                                console.log('$scope.mData.GridDetail[i].DPAmount  = ', $scope.mData.GridDetail[i].DPAmount);
                                console.log('x = ', x);
                            }
                            console.log('n end = ', n);
                            //console.log('RetailPriceDP end = ', $scope.mData.GridDetail[i].RetailPriceDP);
                            //if($scope.mData.GridDetail[0].DPAmount == 0){

                            if (x > 0) {
                                bsNotify.show({
                                    title: "Sales Order",
                                    content: "Data DP Amount belum diisi",
                                    type: 'danger'
                                });
                            } else if (n == 0) {

                                for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                                    //console.log($scope.mData.GridDetail[i].PartsName);
                                    $scope.mData.GridDetail[i].SalesOrderId = $scope.mData.SalesOrderId;
                                    $scope.mData.GridDetail[i].SalesOrderStatusId = $scope.mData.SalesOrderStatusId;
                                    $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
                                    $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;

                                    $scope.mData.GridDetail[i].MaterialRequestStatusId = 1;
                                }
                                console.log('$scope.mData.GridDetail => ', $scope.mData.GridDetail);
                                $scope.dataBilling = angular.copy($scope.mData);
                                newDataTest = angular.copy($scope.mData);
                                $scope.testingParam($scope.mData);
                                console.log("$scope.dataBilling", $scope.dataBilling);
                                PartsSalesOrder.create($scope.mData, $scope.mData.GridDetail).then(function(res) {
                                        console.log("NO DRAMA",$scope.mData);
                                        console.log("insertResponse => ", res.data);
                                        var temp = res.data.ResponseMessage;
                                        var insertResponse = temp.split("#").pop();
                                        console.log("insertResponse", insertResponse);
                                        $scope.mData.SalesOrderId = insertResponse;
                                        // $scope.dataBilling = {
                                        //   salesorderid : res.data
                                        // }


                                        // $scope.kirimapprovalbilling = true;
                                        // bsNotify.show(
                                        //   {
                                        //       title: "SalesOrder",
                                        //       content: "Data Header berhasil disimpan",
                                        //       type: 'success'
                                        //   }
                                        // );
                                        ngDialog.openConfirm({
                                            template: '<div ng-include=\"\'app/parts/directpartssales/salesorder/referensi/dialog_approvalBilling.html\'\"></div>',
                                            plain: true,
                                            controller: 'PartsSalesOrderController',
                                        }).then(function(value) {
                                            $scope.simpanapprovalBilling();
                                            // console.log("masuk yes");
                                        });
                                        //     PartsSalesBilling.getDocumentNumber(UserId).then(function(res1) {
                                        //       var DocNo = res.data;
                                        //       $scope.SalesBillingDocNo = "";
                                        //       $scope.SalesBillingDocNo = DocNo[0];
                                        //       PartsSalesBilling.create($scope.SalesBillingDocNo,insertResponse.SOId,$scope.mData, $scope.mData.GridDetail).then(function(res2){

                                        //       }),function(err2){

                                        //       }
                                        //     }),function(err1){

                                        // }
                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                ); // end PartsSalesOrder.create


                            } else {
                                console.log('mData = ', mData);
                                console.log('DocNo => ', DocNo);
                                //$scope.mData.GridDetail = $scope.gridOptions.data;
                                PartsSalesOrder.setSalesOrderHeader($scope.mData);

                                switch ($scope.mData.RefSOTypeId) {
                                    case 1:
                                        $scope.mData.GridDetail = $scope.gridOptions.data;
                                        break;
                                    case 2:
                                        $scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
                                        break;
                                }
                                PartsSalesOrder.setSalesOrderDetail($scope.mData.GridDetail);
                                //$scope.mData.Grid = $scope.gridOptions.data;

                                ngDialog.openConfirm({
                                    template: '<div ng-include=\"\'app/parts/directpartssales/salesorder/referensi/dialog_approval.html\'\"></div>',
                                    plain: true,
                                    controller: 'PartsSalesOrderController',
                                }).catch(function(value) {
                                    $scope.isApproved = false;
                                    console.log("masuk cancel=>",value);
                                });
                            }
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    ); // end PartsSalesOrder.getDocumentNumber

                    console.log("Persiapan Create => ");
                    console.log($scope.mData);

                    //$scope.mData = {};
                    //$scope.mData.GridDetail = {};
                    // // [START] Get Search Appointment
                    //console.log(loginData[0]);
                    // $scope.mData.OutletId = loginData[0].OutletId;
                    // $scope.mData.WarehouseId = loginData[0].WarehouseId;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            // [END] Get Current User Warehouse

        }; // end POST
        $scope.testingParam = function(data) {
            console.log('testingParam data', data);
            $scope.newTestingData = data;
        };

        $scope.simpanapprovalBilling = function(data) {
                console.log("$scope.dataBilling ", $scope.dataBilling);
                console.log("newDataTest ", newDataTest);

                // $scope.dataApprovebilling = PartsSalesOrder.getSalesOrderHeader();
                // $scope.dataApproveBillingDetail = PartsSalesOrder.getSalesOrderDetail();

                // $scope.mData = PartsSalesBilling.getSalesBillingHeader();
                // $scope.mData.GridDetail = PartsSalesBilling.getSalesBillingDetail();

                console.log("mData => ", $scope.mData);
                // console.log("mData Detail=> ", $scope.dataApproveBillingDetail);

                for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                    $scope.mData.GridDetail[i].SalesBillingStatusId = 2; //for approval
                    $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
                    //$scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;
                }

                PartsSalesBilling.createApprove($scope.mData, $scope.mData.GridDetail).then(function(res) {
                        var create = res.data;
                        console.log('res.data = ', res.data);
                        bsNotify.show({
                            title: "Sales Billing",
                            content: "Data berhasil disimpan",
                            type: 'success'
                        });

                        $scope.goBack();
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

                $scope.ngDialog.close();
            }
            // $scope.kirimApproval = function(){
            //   alert('kirimApprovalkirimApprovalkirimApproval');
            // };
            //POST - simple - SIMPAN DATA
            //$scope.SimpanData = function(refDisposal, tipeMaterial, mData){
        $scope.SimpanApproval = function() {
            // console.log("Data untuk Approval ", data);

            // $scope.mData = PartsSalesBilling.getSalesBillingHeader();
            // $scope.mData.GridDetail = PartsSalesBilling.getSalesBillingDetail();

            console.log("mData => ", $scope.mData);
            console.log("mData Detail=> ", $scope.mData.GridDetail);

            for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                $scope.mData.GridDetail[i].SalesBillingStatusId = 2; //for approval
                $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
                //$scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;
            }

            PartsSalesBilling.createApprove($scope.mData, $scope.mData.GridDetail).then(function(res) {
                    var create = res.data;
                    console.log('res.data = ', res.data);
                    bsNotify.show({
                        title: "Sales Billing dan Sales Order",
                        content: "Data berhasil disimpan",
                        type: 'success'
                    });

                    $scope.goBack();
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

            $scope.ngDialog.close();

        }


        // ======== tombol simpan sales order =========
        $scope.SimpanSO = function(refTypeSO, statusSO, mData) {
            $scope.onProcessSave = true;
            $scope.isApproved = true;
            if ($scope.mData.TimeNeeded == null || $scope.mData.TimeNeeded === undefined) {
                $scope.mData.TimeNeeded = $filter('date')(new Date(), 'HH:mm:ss');
            }
            //console.log("Time needed ", $scope.mData.TimeNeeded);
            console.log("mData = ", mData);
            $scope.mData.DN = $filter('date')($scope.mData.DateNeeded, 'yyyy-MM-dd');
            $scope.mData.TN = $filter('date')($scope.mData.TimeNeeded, 'HH:mm:ss');
            $scope.mData.VDate = $scope.mData.DN + 'T' + $scope.mData.TN + 'Z';
            $scope.mData.DateNeeded = $filter('date')($scope.mData.VDate, 'yyyy-MM-ddTHH:mm:ssZ');

            if ($scope.mData.DateNeeded == null || $scope.mData.DateNeeded === 'undefined' || $scope.mData.DateNeeded == '') {
                bsNotify.show({
                    title: "Mandatory",
                    content: "Tanggal Dibutuhkan harus diisi",
                    type: 'danger'
                });
                $scope.onProcessSave = false;
            } else if ($scope.mData.RefSONo == null || $scope.mData.RefSONo === 'undefined' || $scope.mData.RefSONo == '') {
                bsNotify.show({
                    title: "Mandatory",
                    content: "No. PO harus diisi",
                    type: 'danger'
                });
                $scope.onProcessSave = false;
            } else if ($scope.mData.PaymentMethodId == null || $scope.mData.PaymentMethodId === 'undefined' || $scope.mData.PaymentMethodId == '') {
                bsNotify.show({
                    title: "Mandatory",
                    content: "Metode Pembayaran harus diisi",
                    type: 'danger'
                });
                $scope.onProcessSave = false;
            } else if($scope.mData.PaymentPeriod == null && $scope.mData.PaymentMethodId == 203 || $scope.mData.PaymentPeriod == undefined && $scope.mData.PaymentMethodId == 203 ){
                bsNotify.show({
                    title: "Mandatory",
                    content: "Jangka Waktu Bayar harus diisi",
                    type: 'danger'
                });
                $scope.onProcessSave = false;
            } else {

                // validasi approval = jika diskon spesial > 0
                var vAppr = 0;
                var vDP = 0;
                for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                    // if ($scope.mData.GridDetail[i].DiscountSpecialAmount == 0) {
                    //     vAppr = vAppr + 0;
                    //     //console.log("vAppr = vAppr + 0 ", vAppr);
                    // }
                    // else if ($scope.mData.GridDetail[i].DiscountSpecialAmount > 0) {
                    //     vAppr = vAppr + 1;
                    // }

                    PartsSalesOrder.GetCheckDiskonAppoval($scope.mData.GridDetail[i].DiscountSpecialPercent, $scope.user.OrgId, $scope.mData.ServiceTypeId).then(function(res) {
                        console.log("somewhere only we know",res)
                        if (res.data != null || res.data != undefined)
                        {
                            vAppr = vAppr + 1;
                            console.log("somewhere only know",res.data.result)
                        }
                        else{
                            vAppr = 0;
                        }
                    });

                    if (
                        $scope.mData.GridDetail[i].QtySO == null ||
                        $scope.mData.GridDetail[i].QtySO === undefined ||
                        $scope.mData.GridDetail[i].QtySO === "" ||
                        $scope.mData.GridDetail[i].QtySO == 0 // tambahan yap
                    ) {
                        //yap
                        // qty so tidak boleh kosong lagi saat di simpan
                        // $scope.mData.GridDetail[i].QtySO = 0;
                        // if(value.QtySO < 1){
                        bsNotify.show({
                            title: "Mandatory",
                            content: "Semua Qty SO harus lebih dari 0",
                            type: 'danger'
                        });
                        $scope.onProcessSave = false;
                        return;
                        // }
                        console.log("GridDetail[i].QtySO => ", $scope.mData.GridDetail[i].QtySO);
                    }

                    if ($scope.mData.GridDetail[i].DPAmount_Temp == $scope.mData.GridDetail[i].DPAmount) {
                        vDP = vDP + 0;
                        //console.log("DPAmount_Temp ", $scope.mData.GridDetail[i].DPAmount_Temp);
                    } else if ($scope.mData.GridDetail[i].DPAmount_Temp > $scope.mData.GridDetail[i].DPAmount) {
                        vDP = vDP + 1;
                        //console.log("DPAmount_Temp ", $scope.mData.GridDetail[i].DPAmount_Temp);
                    }

                    console.log("DPAmount ", $scope.mData.GridDetail[i].DPAmount);
                    console.log("DPAmount_Temp ", $scope.mData.GridDetail[i].DPAmount_Temp);
                }
                console.log("vAppr = ", vAppr);
                console.log("vDP = ", vDP);
                if (vAppr == 0 && vDP == 0) { // tidak ada approval, simpan langsung
                    console.log("gak ada approval ", vAppr);

                    PartsSalesOrder.GetCheckCustomer($scope.mData.OutletLainId).then(function(res) {
                        console.log("GetCheckCustomer=>",res);
                        console.log("$scope.mData=>",$scope.mData);
                        $scope.mData.CustomerId = res.data[0].CustomerId;

                        PartsCurrentUser.getFormatId($scope.mData.ServiceTypeId, 'SO').then(function(res) {
                                var result = res.data;
                                //console.log("FormatId result = ", result);
                                console.log("FormatId = ", result[0].Results);
                                PartsSalesOrder.getDocumentNumber(result[0].Results).then(function(res) {
                                        var DocNo = res.data;
                                        if (typeof DocNo === 'undefined' || DocNo == null) {
                                            bsNotify.show({
                                                title: "Sales Order",
                                                content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                                                type: 'danger'
                                            });
                                            $scope.onProcessSave = false;
                                        } else {
                                            $scope.mData.SalesOrderNo = DocNo[0];
                                            if(DocNo[1]){
                                                var date = new Date(DocNo[1]);
                                                var today = new Date();
                                                date.setHours(today.getHours(),today.getMinutes(),today.getSeconds());
                                            }
                                            $scope.mData.DocDate = date;
                                            // $scope.mData.DocDate = DocNo[1]
                                            console.log("Generating DocNo & DocDate ->");
                                            console.log($scope.mData.SalesOrderNo);
                                            console.log($scope.mData.DocDate);

                                            //here
                                            $scope.mData['RefSOTypeId'] = refTypeSO;
                                            $scope.mData['SalesOrderStatusId'] = statusSO;
                                            $scope.mData['SOStatusId'] = statusSO; // set status = 2 (open)
                                            $scope.mData['DPAmount'] = parseInt($scope.mData.DPAmount, 10);
                                            $scope.mData['TotalAmount'] = parseInt(Math.round($scope.mData.TotalAmount), 10);
                                            // $scope.mData['TotalAmount'] = parseInt($scope.mData.TotalAmount.toFixed(2), 10);

                                            $scope.mData.GridDetail = $scope.gridOptions.data;
                                            console.log('$scope.mData ', $scope.mData);
                                            console.log('$scope.mData.GridDetail ', $scope.mData.GridDetail);

                                            PartsSalesOrder.setSalesOrderHeader($scope.mData);
                                            PartsSalesOrder.setSalesOrderDetail($scope.mData.GridDetail);

                                            $scope.kirimApproval($scope.mData);
                                        }
                                        // here

                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                );
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        ); // end getFormatId
                    });

                } else {
                    PartsSalesOrder.GetCheckCustomer($scope.mData.OutletLainId).then(function(res) {
                        console.log("GetCheckCustomer2=>",res);
                        console.log("$scope.mData=>",$scope.mData);
                        $scope.mData.CustomerId = res.data[0].CustomerId;

                        console.log('*ada approval');
                        $scope.GenerateDocNum();
                        $scope.mData['RefSOTypeId'] = refTypeSO;
                        $scope.mData['SalesOrderStatusId'] = statusSO;
                        //$scope.mData['SOStatusId'] = statusSO; // set status = 2 (opem)
                        $scope.mData.GridDetail = $scope.gridOptions.data;
                        PartsSalesOrder.setSalesOrderHeader($scope.mData);
                        PartsSalesOrder.setSalesOrderDetail($scope.mData.GridDetail);
                        // $scope.mData.WarehouseId = WarehouseData[0].WarehouseId;
                        // $scope.mData.OutletId = WarehouseData[0].OutletId;
                        console.log('mData = ', $scope.mData);
                        console.log('mData.GridDetail = ', $scope.mData.GridDetail);
                        // console.log('mData.WarehouseId = ', $scope.mData.WarehouseId);
                        // console.log('mData.OutletId = ', $scope.mData.OutletId);

                        // butuh approval
                        ngDialog.openConfirm({
                            template: '<div ng-include=\"\'app/parts/directpartssales/salesorder/referensi/dialog_approval.html\'\"></div>',
                            plain: true,
                            controller: 'PartsSalesOrderController',
                        }).catch(function(value) {
                            $scope.isApproved = false;
                            console.log("masuk cancel=>",value);
                        });
                    });
                } // end else
            } // end else
        };
        // ======== end tombol  simpan =========

        // ======== tombol simpan sales order & salesbilling=========
        $scope.SimpanSB = function(refTypeSO, statusSO, mData) {
            //alert('');
            if ($scope.mData.TimeNeeded == null || $scope.mData.TimeNeeded === undefined) {
                $scope.mData.TimeNeeded = $filter('date')(new Date(), 'HH:mm:ss');
            }
            //console.log("Time needed ", $scope.mData.TimeNeeded);
            console.log("mData : ", mData);
            $scope.mData.DN = $filter('date')($scope.mData.DateNeeded, 'yyyy-MM-dd');
            $scope.mData.TN = $filter('date')($scope.mData.TimeNeeded, 'HH:mm:ss');
            $scope.mData.VDate = $scope.mData.DN + 'T' + $scope.mData.TN + 'Z';
            $scope.mData.DateNeeded = $filter('date')($scope.mData.VDate, 'yyyy-MM-ddTHH:mm:ssZ');

            $scope.mData.GridDetail = $scope.gridOptions.data;
            console.log('mData.GridDetail : ', $scope.mData.GridDetail);

            if ($scope.mData.DateNeeded == null || $scope.mData.DateNeeded === 'undefined' || $scope.mData.DateNeeded == '') {
                bsNotify.show({
                    title: "Mandatory",
                    content: "Tanggal Dibutuhkan harus diisi",
                    type: 'danger'
                });
            } else if ($scope.mData.RefSONo == null || $scope.mData.RefSONo === 'undefined' || $scope.mData.RefSONo == '') {
                bsNotify.show({
                    title: "Mandatory",
                    content: "No. PO harus diisi",
                    type: 'danger'
                });
            } else if ($scope.mData.PaymentMethodId == null || $scope.mData.PaymentMethodId === 'undefined' || $scope.mData.PaymentMethodId == '') {
                bsNotify.show({
                    title: "Mandatory",
                    content: "Metode Pembayaran harus diisi",
                    type: 'danger'
                });
    } else if($scope.mData.PaymentPeriod == null && $scope.mData.PaymentMethodId == 2 || $scope.mData.PaymentPeriod == undefined && $scope.mData.PaymentMethodId == 2  ){
                bsNotify.show({
                    title: "Mandatory",
                    content: "Jangka Waktu Bayar harus diisi",
                    type: 'danger'
                });
            } else {
                // validasi approval = jika diskon spesial > 0
                var vDP = 0;
                var vAppr = 0;
                for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                    PartsSalesOrder.GetCheckDiskonAppoval($scope.mData.GridDetail[i].DiscountSpecialPercent, $scope.user.OrgId, $scope.mData.ServiceTypeId).then(function(res) {
                        console.log("somewhere only we know",res)
                        if (res.data != null || res.data != undefined)
                        {
                            vAppr = 2;
                            console.log("somewhere only 5 know",res.data)
                        }
                        else{
                            vAppr = 0;
                        }
                    });
                    if ($scope.mData.GridDetail[i].DPAmount_Temp > $scope.mData.GridDetail[i].DPAmount) {
                        vDP = vDP + 1;
                    }
                }
                console.log("vAppr = ", vAppr);
                console.log("vDP = ", vDP);
                // validasi approval
                if (vAppr > 0 || vDP > 0) {
                    console.log("*ada approval");
                    $scope.GenerateDocNum();
                    $scope.mData['RefSOTypeId'] = refTypeSO;
                    $scope.mData['SalesOrderStatusId'] = statusSO;
                    //$scope.mData['SOStatusId'] = statusSO; // mengeset status =  (open)
                    $scope.mData.GridDetail = $scope.gridOptions.data;
                    PartsSalesOrder.setSalesOrderHeader($scope.mData);
                    PartsSalesOrder.setSalesOrderDetail($scope.mData.GridDetail);

                    console.log('mData = ', $scope.mData);
                    console.log('mData.GridDetail = ', $scope.mData.GridDetail);

                    bsNotify.show({
                        title: "Membutuhkan Approval",
                        content: "Dokumen ini membutuhkan approval. Klik tombol 'Simpan SO'",
                        type: 'danger',
                        size: 'big'
                    });

                    // ngDialog.openConfirm({
                    //   template: '<div ng-include=\"\'app/parts/directpartssales/salesorder/referensi/dialog_approvalBilling.html\'\"></div>',
                    //   plain: true,
                    //   controller: 'PartsSalesOrderController',
                    // });
                    // // jika klik OK/simpan pada dialog approval, selanjutnya ke: $scope.kirimApprovalSB = function
                } else {
                    console.log("gak ada approval ", vAppr);
                    PartsCurrentUser.getFormatId($scope.mData.ServiceTypeId, 'SO').then(function(res) {
                            var result = res.data;
                            //console.log("FormatId result = ", result);
                            console.log("FormatId = ", result[0].Results);
                            PartsSalesOrder.getDocumentNumber(result[0].Results).then(function(res) {
                                    var DocNo = res.data;
                                    if (typeof DocNo === 'undefined' || DocNo == null) {
                                        bsNotify.show({
                                            title: "Sales Order",
                                            content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                                            type: 'danger'
                                        });
                                    } else {
                                        $scope.mData.SalesOrderNo = DocNo[0];
                                        if(DocNo[1]){
                                            var date = new Date(DocNo[1]);
                                            var today = new Date();
                                            date.setHours(today.getHours(),today.getMinutes(),today.getSeconds());
                                        }
                                        $scope.mData.DocDate = date;
                                        // $scope.mData.DocDate = DocNo[1]
                                        console.log("Generating DocNo & DocDate ->");
                                        console.log($scope.mData.SalesOrderNo);
                                        console.log($scope.mData.DocDate);
                                        //here
                                        $scope.mData['RefSOTypeId'] = refTypeSO;
                                        $scope.mData['SalesOrderStatusId'] = statusSO;
                                        $scope.mData['DPAmount'] = parseInt($scope.mData.DPAmount, 10);
                                        $scope.mData['TotalAmount'] = parseInt(Math.round($scope.mData.TotalAmount), 10);
                                        // $scope.mData['TotalAmount'] = parseInt($scope.mData.TotalAmount.toFixed(2), 10);
                                        $scope.mData['SOStatusId'] = statusSO; // set status = 2 (open)

                                        $scope.mData.GridDetail = $scope.gridOptions.data;
                                        console.log('$scope.mData ', $scope.mData);
                                        console.log('$scope.mData.GridDetail ', $scope.mData.GridDetail);

                                        PartsSalesOrder.setSalesOrderHeader($scope.mData);
                                        PartsSalesOrder.setSalesOrderDetail($scope.mData.GridDetail);

                                        $scope.saveSOdanSBLangsung($scope.mData);
                                    }
                                    // here
                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    ); // end format
                }

            }
        };
        // ======== end tombol  simpan =========

        // ======== tombol simpan sales order langsung=========
        var vAppr = 0
            vDP = 0
            vPC4 = 0 // PartsClassId4
            vPartId = 0;
        $scope.SimpanSOLangsung = function(refTypeSO, statusSO, mData) {

            console.log('mdata coy', mData);
            $scope.onProcessSave = true;

            //$scope.GenerateDocNum();
            //console.log("$scope.mData.ServiceTypeId!!! = ", $scope.mData.ServiceTypeId);
            if ($scope.mData.TimeNeeded == null || $scope.mData.TimeNeeded === undefined) {
                $scope.mData.TimeNeeded = $filter('date')(new Date(), 'HH:mm:ss');
            }
            //console.log("Time needed ", $scope.mData.TimeNeeded);
            //console.log("mData = ", mData);
            //if(mData.TotalAmount == null || mData.TotalAmount === undefined){mData.TotalAmount = 0};
            //console.log("mData.TotalAmount = ", mData.TotalAmount);
            //if(mData.DPAmount == null || mData.DPAmount === undefined){mData.DPAmount = 0};
            //console.log("$scope.mData.CustomerId = ", $scope.mData.CustomerId);

            //console.log("$scope.mData.VDate = ", $scope.mData.VDate);
            //console.log("$scope.mData.DateNeeded = ", $scope.mData.DateNeeded);
            $scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
            console.log("$scope.mData.GridDetail = ", $scope.mData.GridDetail);

            // angular.forEach($scope.mData.GridDetail,function(value,key){

            // });

            if ($scope.mData.DateNeeded == null || $scope.mData.DateNeeded === 'undefined' || $scope.mData.DateNeeded == '') {
                bsNotify.show({
                    title: "Mandatory",
                    content: "Tanggal Dibutuhkan harus diisi",
                    type: 'danger'
                });
                $scope.onProcessSave = false;
            } else if ($scope.mData.CustomerId == null || $scope.mData.CustomerId === 'undefined' || $scope.mData.CustomerId == '') {
                bsNotify.show({
                    title: "Mandatory",
                    content: "Data Customer harus diisi",
                    type: 'danger'
                });
                $scope.onProcessSave = false;
            } else if ($scope.mData.PaymentMethodId == null || $scope.mData.PaymentMethodId === 'undefined' || $scope.mData.PaymentMethodId == '') {
                bsNotify.show({
                    title: "Mandatory",
                    content: "Metode Pembayaran harus diisi",
                    type: 'danger'
                });
                $scope.onProcessSave = false;
            } else if($scope.mData.PaymentPeriod == null || $scope.mData.PaymentPeriod == undefined ){
                bsNotify.show({
                    title: "Mandatory",
                    content: "Jangka Waktu Bayar harus diisi",
                    type: 'danger'
                });
                $scope.onProcessSave = false;
            } else if($scope.mData.VATAmount == null || $scope.mData.VATAmount == undefined || $scope.mData.VATAmount == '' || $scope.mData.VATAmount == 0 || $scope.mData.VATAmount == NaN ){
                bsNotify.show({
                    title: "PPN",
                    content: "Nilai PPn tidak boleh 0, harap lakukan input ulang",
                    type: 'danger'
                });
                $scope.onProcessSave = false;
            }else if($scope.ref == 'Customer Langsung' && (mData.PoliceNumber == null || mData.PoliceNumber == undefined 
                || mData.PoliceNumber == '' || mData.PoliceNumber.length < 4 || mData.PoliceNumber.length > 10 )){
                    bsNotify.show({
                        size: 'Mandatory',
                        type: 'danger',
                        title: "Nomor Polisi harus diisi Minimal 4 karakter",
                    });
                    $scope.onProcessSave = false;
            } else {
                $scope.mData.DN = $filter('date')($scope.mData.DateNeeded, 'yyyy-MM-dd');
                $scope.mData.TN = $filter('date')($scope.mData.TimeNeeded, 'HH:mm:ss');
                $scope.mData.VDate = $scope.mData.DN + 'T' + $scope.mData.TN + 'Z';
                $scope.mData.DateNeeded = $filter('date')($scope.mData.VDate, 'yyyy-MM-ddTHH:mm:ssZ');


                var vAppr = 0;
                var vDP = 0;
                var vPC4 = 0; // PartsClassId4
                var vPartId = 0;
                //$scope.loopCheckApprove($scope.mData.GridDetail,0,refTypeSO, statusSO)
				
				console.log('$scope.mData.Npwp at inter : ', $scope.mData.Npwp);
				console.log('$scope.mData.KTPKITAS at inter : ', $scope.mData.KTPKITAS);
					
				if($scope.mData.CustomerTypeId == 3){
					//NPWP/KTP

					if($scope.mData.Npwp == undefined || $scope.mData.KTPKITAS == undefined || $scope.mData.Npwp == "00.000.000.0-000.000" || $scope.mData.KTPKITAS == "0000000000000000"){
						bsAlert.alert({
							title: "Data No. NPWP atau No. KTP Pelanggan diisi 00.000.000.0-000.000 / kosong, Apakah Anda Akan Melanjutkan Transaksi?",
							text: "",
							type: "question",
							showCancelButton: true
						},
						function() {
							console.log('Simpan NPWP/KTP kosong');
							$scope.loopCheckApprove($scope.mData.GridDetail,0,refTypeSO, statusSO);
						},
						function() {
							console.log('Tidak Simpan NPWP/KTP kosong');
							$scope.onProcessSave = false;
						})
					}else{
						$scope.loopCheckApprove($scope.mData.GridDetail,0,refTypeSO, statusSO);
					}
				}else{
					//NPWP
					if($scope.mData.Npwp == undefined || $scope.mData.Npwp == "00.000.000.0-000.000"){
						bsAlert.alert({
							title: "Data No. NPWP atau No. KTP Pelanggan diisi 00.000.000.0-000.000 / kosong, Apakah Anda Akan Melanjutkan Transaksi?",
							text: "",
							type: "question",
							showCancelButton: true
						},
						function() {
							console.log('Simpan NPWP kosong');
							$scope.loopCheckApprove($scope.mData.GridDetail,0,refTypeSO, statusSO);
						},
						function() {
							console.log('Tidak Simpan NPWP kosong');
							$scope.onProcessSave = false;
						})
					}else{
						$scope.loopCheckApprove($scope.mData.GridDetail,0,refTypeSO, statusSO);
					}
				}
				
                // for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                //     $scope.mData.GridDetail[i].TotalAmount = Math.round($scope.mData.GridDetail[i].TotalAmount);
                //     $scope.mData.GridDetail[i].DiscountSpecialAmount = isNaN($scope.mData.GridDetail[i].DiscountSpecialAmount) ? 0 : $scope.mData.GridDetail[i].DiscountSpecialAmount;
                //     $scope.mData.GridDetail[i].DPAmount = isNaN($scope.mData.GridDetail[i].DPAmount) ? 0 : $scope.mData.GridDetail[i].DPAmount;
                //     if ($scope.mData.GridDetail[i].DiscountSpecialAmount == null || $scope.mData.GridDetail[i].DiscountSpecialAmount === undefined || $scope.mData.GridDetail[i].DiscountSpecialAmount === "") {
                //         $scope.mData.GridDetail[i].DiscountSpecialAmount = 0;
                //         console.log("GridDetail[i].DiscountSpecialAmount => ", $scope.mData.GridDetail[i].DiscountSpecialAmount);
                //     }
                //     if ($scope.mData.GridDetail[i].DPAmount_Temp == null || $scope.mData.GridDetail[i].DPAmount_Temp === undefined || $scope.mData.GridDetail[i].DPAmount_Temp === "") {
                //         $scope.mData.GridDetail[i].DPAmount_Temp = 0;
                //         console.log("GridDetail[i].DPAmount_Temp => ", $scope.mData.GridDetail[i].DPAmount_Temp);
                //     }
                //     if ($scope.mData.GridDetail[i].DPAmount == null || $scope.mData.GridDetail[i].DPAmount === undefined || $scope.mData.GridDetail[i].DPAmount === "") {
                //         $scope.mData.GridDetail[i].DPAmount = 0;
                //         console.log("GridDetail[i].DPAmount => ", $scope.mData.GridDetail[i].DPAmount);
                //     }
                //     if ($scope.mData.GridDetail[i].PartsClassId4 == null || $scope.mData.GridDetail[i].PartsClassId4 === undefined || $scope.mData.GridDetail[i].PartsClassId4 === "") {
                //         $scope.mData.GridDetail[i].PartsClassId4 = 0;
                //         console.log("GridDetail[i].PartsClassId4 => ", $scope.mData.GridDetail[i].PartsClassId4);
                //     }

                //     // untuk keperluan approval=======================================================
                //     // if ($scope.mData.GridDetail[i].DiscountSpecialAmount > 0) {
                //     //     vAppr = vAppr + 1;
                //     // }
                //     if ($scope.mData.GridDetail[i].DPAmount_Temp > $scope.mData.GridDetail[i].DPAmount) {
                //         vDP = vDP + 1;
                //     }
                //     if ($scope.mData.GridDetail[i].PartsClassId4 == 47) { // 47 = non stock
                //         vPC4 = vPC4 + 1;
                //     }
                //     if ($scope.mData.GridDetail[i].PartId == null || $scope.mData.GridDetail[i].PartId === undefined || $scope.mData.GridDetail[i].PartId === "") {
                //         vPartId = vPartId + 1;
                //     }
                //     // end - untuk keperluan approval==================================================

                //     // jika grid detail serba kosong
                //     if ($scope.mData.GridDetail[i].SalesOrderItemId === "") { $scope.mData.GridDetail[i].SalesOrderItemId = undefined };
                //     console.log('SOItemId =>', $scope.mData.GridDetail[i].SalesOrderItemId);
                //     if (isNaN($scope.mData.GridDetail[i].TotalAmount)) {
                //         $scope.mData.GridDetail[i].TotalAmount = 0;
                //         console.log("NaN-TotalAmount => ", $scope.mData.GridDetail[i].TotalAmount);
                //     }
                //     $scope.mData.GridDetail[i].QtySO = isNaN($scope.mData.GridDetail[i].QtySO) ? 0 : $scope.mData.GridDetail[i].QtySO;
                //     $scope.mData.GridDetail[i].UnitPrice = isNaN($scope.mData.GridDetail[i].UnitPrice) ? 0 : $scope.mData.GridDetail[i].UnitPrice;

                //     $scope.mData.GridDetail[i].DiscountMaterialPercent = isNaN($scope.mData.GridDetail[i].DiscountMaterialPercent) ? 0 : $scope.mData.GridDetail[i].DiscountMaterialPercent;
                //     $scope.mData.GridDetail[i].DiscountMaterialAmount = isNaN($scope.mData.GridDetail[i].DiscountMaterialAmount) ? 0 : $scope.mData.GridDetail[i].DiscountMaterialAmount;
                //     $scope.mData.GridDetail[i].DiscountGroupMaterialAmount = isNaN($scope.mData.GridDetail[i].DiscountGroupMaterialAmount) ? 0 : $scope.mData.GridDetail[i].DiscountGroupMaterialAmount;
                //     $scope.mData.GridDetail[i].DiscountSpecialPercent = isNaN($scope.mData.GridDetail[i].DiscountSpecialPercent) ? 0 : $scope.mData.GridDetail[i].DiscountSpecialPercent;
                //     $scope.mData.GridDetail[i].VATAmount = isNaN($scope.mData.GridDetail[i].VATAmount) ? 0 : $scope.mData.GridDetail[i].VATAmount;
                //     $scope.mData.GridDetail[i].DPPercent = isNaN($scope.mData.GridDetail[i].DPPercent) ? 0 : $scope.mData.GridDetail[i].DPPercent;
                //     $scope.mData.GridDetail[i].VATAmount = isNaN($scope.mData.GridDetail[i].VATAmount) ? 0 : $scope.mData.GridDetail[i].VATAmount;
                //     console.log("$scope.mData.GridDetail[i] => ", $scope.mData.GridDetail[i]);

                //     if ($scope.mData.GridDetail[i].TotalAmount == null || $scope.mData.GridDetail[i].TotalAmount === undefined || $scope.mData.GridDetail[i].TotalAmount === "") {
                //         $scope.mData.GridDetail[i].TotalAmount = 0;
                //         console.log("GridDetail[i].TotalAmount => ", $scope.mData.GridDetail[i].TotalAmount);
                //     }
                //     if (
                //         $scope.mData.GridDetail[i].QtySO == null ||
                //         $scope.mData.GridDetail[i].QtySO === undefined ||
                //         $scope.mData.GridDetail[i].QtySO === "" ||
                //         $scope.mData.GridDetail[i].QtySO == 0 // tambahan yap
                //     ) {
                //         //yap
                //         // qty so tidak boleh kosong lagi saat di simpan
                //         // $scope.mData.GridDetail[i].QtySO = 0;
                //         // if(value.QtySO < 1){
                //         bsNotify.show({
                //             title: "Mandatory",
                //             content: "Semua Qty SO harus lebih dari 0",
                //             type: 'danger'
                //         });
                //         return;
                //         // }
                //         console.log("GridDetail[i].QtySO => ", $scope.mData.GridDetail[i].QtySO);
                //     }
                //     if ($scope.mData.GridDetail[i].UomId == null || $scope.mData.GridDetail[i].UomId === undefined || $scope.mData.GridDetail[i].UomId === "") {
                //         $scope.mData.GridDetail[i].UomId = 0;
                //         console.log("GridDetail[i].UomId => ", $scope.mData.GridDetail[i].UomId);
                //     }
                //     if ($scope.mData.GridDetail[i].UnitPrice == null || $scope.mData.GridDetail[i].UnitPrice === undefined || $scope.mData.GridDetail[i].UnitPrice === "") {
                //         $scope.mData.GridDetail[i].UnitPrice = 0;
                //         console.log("GridDetail[i].UnitPrice => ", $scope.mData.GridDetail[i].UnitPrice);
                //     }
                //     if ($scope.mData.GridDetail[i].DiscountMaterialPercent == null || $scope.mData.GridDetail[i].DiscountMaterialPercent === undefined || $scope.mData.GridDetail[i].DiscountMaterialPercent === "") {
                //         $scope.mData.GridDetail[i].DiscountMaterialPercent = 0;
                //         console.log("GridDetail[i].DiscountMaterialPercent => ", $scope.mData.GridDetail[i].DiscountMaterialPercent);
                //     }
                //     if ($scope.mData.GridDetail[i].DiscountMaterialAmount == null || $scope.mData.GridDetail[i].DiscountMaterialAmount === undefined || $scope.mData.GridDetail[i].DiscountMaterialAmount === "") {
                //         $scope.mData.GridDetail[i].DiscountMaterialAmount = 0;
                //         console.log("GridDetail[i].DiscountMaterialAmount => ", $scope.mData.GridDetail[i].DiscountMaterialAmount);
                //     }
                //     if ($scope.mData.GridDetail[i].DiscountGroupMaterialAmount == null || $scope.mData.GridDetail[i].DiscountGroupMaterialAmount === undefined || $scope.mData.GridDetail[i].DiscountGroupMaterialAmount === "") {
                //         $scope.mData.GridDetail[i].DiscountGroupMaterialAmount = 0;
                //         console.log("GridDetail[i].DiscountGroupMaterialAmount => ", $scope.mData.GridDetail[i].DiscountGroupMaterialAmount);
                //     }
                //     if ($scope.mData.GridDetail[i].DiscountGroupMaterialPercent == null || $scope.mData.GridDetail[i].DiscountGroupMaterialPercent === undefined || $scope.mData.GridDetail[i].DiscountGroupMaterialPercent === "") {
                //         $scope.mData.GridDetail[i].DiscountGroupMaterialPercent = 0;
                //         console.log("GridDetail[i].DiscountGroupMaterialPercent => ", $scope.mData.GridDetail[i].DiscountGroupMaterialPercent);
                //     }
                //     // if($scope.mData.GridDetail[i].DiscountSpecialAmount == null || $scope.mData.GridDetail[i].DiscountSpecialAmount === undefined || $scope.mData.GridDetail[i].DiscountSpecialAmount === ""){
                //     //   $scope.mData.GridDetail[i].DiscountSpecialAmount = 0;
                //     //   console.log("GridDetail[i].DiscountSpecialAmount => ", $scope.mData.GridDetail[i].DiscountSpecialAmount);
                //     // }
                //     if ($scope.mData.GridDetail[i].DiscountSpecialPercent == null || $scope.mData.GridDetail[i].DiscountSpecialPercent === undefined || $scope.mData.GridDetail[i].DiscountSpecialPercent === "") {
                //         $scope.mData.GridDetail[i].DiscountSpecialPercent = 0;
                //         console.log("GridDetail[i].DiscountSpecialPercent => ", $scope.mData.GridDetail[i].DiscountSpecialPercent);
                //     }
                //     if ($scope.mData.GridDetail[i].VATAmount == null || $scope.mData.GridDetail[i].VATAmount === undefined || $scope.mData.GridDetail[i].VATAmount === "") {
                //         $scope.mData.GridDetail[i].VATAmount = 0;
                //         console.log("GridDetail[i].VATAmount => ", $scope.mData.GridDetail[i].VATAmount);
                //     }
                //     if ($scope.mData.GridDetail[i].DPPercent == null || $scope.mData.GridDetail[i].DPPercent === undefined || $scope.mData.GridDetail[i].DPPercent === "") {
                //         $scope.mData.GridDetail[i].DPPercent = 0;
                //         console.log("GridDetail[i].DPPercent => ", $scope.mData.GridDetail[i].DPPercent);
                //     }

                //     console.log("$scope.mData.GridDetail[i] =>>> ", $scope.mData.GridDetail[i]);

                //     PartsSalesOrder.GetCheckDiskonAppoval($scope.mData.GridDetail[i].DiscountSpecialPercent, $scope.user.OrgId, $scope.mData.ServiceTypeId).then(function(res) {
                //         console.log("somewhere only we know",res)
                //         if (res.data!= null || res.data!= undefined)
                //         {
                //             vAppr = vAppr + 1;
                //             console.log("somewhere only we",res.data,vAppr)
                //         }
                //         else{
                //             vAppr = 0;
                //         }
                //     });
                // }
                 // validasi approval = jika diskon spesial > 0
                // console.log("mData = ", mData);
                // console.log("TotalAmount = ", $scope.mData.GridDetail[0].TotalAmount);
                // console.log("UomId = ", $scope.mData.GridDetail[0].UomId);
                // console.log("UnitPrice = ", $scope.mData.GridDetail[0].UnitPrice);

                // console.log("vAppr = ", vAppr);
                // console.log("vDP = ", vDP);
                // console.log("vPC4 = ", vPC4);
                // console.log("vPartId = ", vPartId);
                // //console.log("mData.DPAmount = ", mData.DPAmount);
                // //console.log("mData.DPAmount_Temp = ", mData.DPAmount_Temp);
                // if (vPartId > 0) {
                //     bsNotify.show({
                //         title: "Special Order",
                //         //content: "test",
                //         type: 'info',
                //         size: 'small'
                //     });
                // }

                // if (vAppr > 0 || vDP > 0 || vPC4 > 0) {
                //     // ada approval
                //     $scope.GenerateDocNum();
                //     $scope.mData['RefSOTypeId'] = refTypeSO;
                //     $scope.mData['SalesOrderStatusId'] = statusSO;
                //     $scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
                //     PartsSalesOrder.setSalesOrderHeader($scope.mData);
                //     PartsSalesOrder.setSalesOrderDetail($scope.mData.GridDetail);

                //     console.log('mData => ', $scope.mData);
                //     console.log('mData.GridDetail => ', $scope.mData.GridDetail);

                //     ngDialog.openConfirm({
                //         template: '<div ng-include=\"\'app/parts/directpartssales/salesorder/referensi/dialog_approval.html\'\"></div>',
                //         plain: true,
                //         controller: 'PartsSalesOrderController',
                //     });
                // } if (vAppr == 0 && vDP == 0 & vPC4 == 0) {
                //     console.log("gak ada approval ", vAppr);
                //     PartsCurrentUser.getFormatId($scope.mData.ServiceTypeId, 'SO').then(function(res) {
                //             var result = res.data;
                //             //console.log("FormatId result = ", result);
                //             console.log("FormatId = ", result[0].Results);
                //             PartsSalesOrder.getDocumentNumber(result[0].Results).then(function(res) {
                //                     var DocNo = res.data;
                //                     if (typeof DocNo === 'undefined' || DocNo == null) {
                //                         bsNotify.show({
                //                             title: "Sales Order",
                //                             content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                //                             type: 'danger'
                //                         });
                //                     } else {
                //                         $scope.mData.SalesOrderNo = DocNo[0];
                //                         $scope.mData.DocDate = DocNo[1]
                //                         console.log("Generating DocNo & DocDate ->");
                //                         console.log($scope.mData.SalesOrderNo);
                //                         console.log($scope.mData.DocDate);

                //                         //here
                //                         $scope.mData['RefSOTypeId'] = refTypeSO;
                //                         $scope.mData['SalesOrderStatusId'] = statusSO;
                //                         $scope.mData['DPAmount'] = parseInt($scope.mData.DPAmount, 10);
                //                         $scope.mData['TotalAmount'] = parseInt(Math.round($scope.mData.TotalAmount), 10);
                //                         $scope.mData['SOStatusId'] = statusSO; // ini hanya untuk validasi

                //                         $scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
                //                         console.log('$scope.mData ', $scope.mData);
                //                         console.log('$scope.mData.GridDetail ', $scope.mData.GridDetail);

                //                         PartsSalesOrder.setSalesOrderHeader($scope.mData);
                //                         PartsSalesOrder.setSalesOrderDetail($scope.mData.GridDetail);

                //                         $scope.kirimApproval($scope.mData);
                //                     }
                //                     // here

                //                 },
                //                 function(err) {
                //                     console.log("err=>", err);
                //                 }
                //             );
                //         },
                //         function(err) {
                //             console.log("err=>", err);
                //         }
                //     ); // end getFormatId
                // } // end else vAppr
            } // end else
            // [END] Get Current User Warehouse
        };

        $scope.loopCheckApprove = function(item,x,refTypeSO, statusSO){
            vAppr = 0;
            $scope.isApproved = true;
            if(x > item.length - 1){
                return item
            }
            // item[x].TotalAmount = Math.round(item[x].TotalAmount);
            item[x].DiscountSpecialAmount = isNaN(item[x].DiscountSpecialAmount) ? 0 : item[x].DiscountSpecialAmount;
            item[x].DPAmount = isNaN(item[x].DPAmount) ? 0 : item[x].DPAmount;
            if (item[x].DiscountSpecialAmount == null || item[x].DiscountSpecialAmount === undefined || item[x].DiscountSpecialAmount === "") {
                item[x].DiscountSpecialAmount = 0;
                console.log("GridDetail[i].DiscountSpecialAmount => ", item[x].DiscountSpecialAmount);
            }
            if (item[x].DPAmount_Temp == null || item[x].DPAmount_Temp === undefined || item[x].DPAmount_Temp === "") {
                item[x].DPAmount_Temp = 0;
                console.log("GridDetail[i].DPAmount_Temp => ", item[x].DPAmount_Temp);
            }
            if (item[x].DPAmount == null || item[x].DPAmount === undefined || item[x].DPAmount === "") {
                item[x].DPAmount = 0;
                console.log("GridDetail[i].DPAmount => ", item[x].DPAmount);
            }
            if (item[x].PartsClassId4 == null || item[x].PartsClassId4 === undefined || item[x].PartsClassId4 === "") {
                item[x].PartsClassId4 = 0;
                console.log("GridDetail[i].PartsClassId4 => ", item[x].PartsClassId4);
            }


            if (item[x].DPAmount_Temp > item[x].DPAmount) {
                vDP = vDP + 1;
            }
            if (item[x].PartsClassId4 == 47) { // 47 = non stock
                vPC4 = vPC4 + 1;
            }
            if (item[x].PartId == null || item[x].PartId === undefined || item[x].PartId === "") {
                vPartId = vPartId + 1;
            }
            // end - untuk keperluan approval==================================================

            // jika grid detail serba kosong
            if (item[x].SalesOrderItemId === "") { item[x].SalesOrderItemId = undefined };
            console.log('SOItemId =>', item[x].SalesOrderItemId);
            if (isNaN(item[x].TotalAmount)) {
                item[x].TotalAmount = 0;
                console.log("NaN-TotalAmount => ", item[x].TotalAmount);
            }
            item[x].QtySO = isNaN(item[x].QtySO) ? 0 : item[x].QtySO;
            item[x].UnitPrice = isNaN(item[x].UnitPrice) ? 0 : item[x].UnitPrice;

            item[x].DiscountMaterialPercent = isNaN(item[x].DiscountMaterialPercent) ? 0 : item[x].DiscountMaterialPercent;
            item[x].DiscountMaterialAmount = isNaN(item[x].DiscountMaterialAmount) ? 0 : item[x].DiscountMaterialAmount;
            item[x].DiscountGroupMaterialAmount = isNaN(item[x].DiscountGroupMaterialAmount) ? 0 : item[x].DiscountGroupMaterialAmount;
            item[x].DiscountSpecialPercent = isNaN(item[x].DiscountSpecialPercent) ? 0 : item[x].DiscountSpecialPercent;
            item[x].VATAmount = isNaN(item[x].VATAmount) ? 0 : item[x].VATAmount;
            item[x].DPPercent = isNaN(item[x].DPPercent) ? 0 : item[x].DPPercent;
            item[x].VATAmount = isNaN(item[x].VATAmount) ? 0 : item[x].VATAmount;
            console.log("item[x] => ", item[x]);

            if (item[x].TotalAmount == null || item[x].TotalAmount === undefined || item[x].TotalAmount === "") {
                item[x].TotalAmount = 0;
                console.log("GridDetail[i].TotalAmount => ", item[x].TotalAmount);
            }
            if (
                item[x].QtySO == null ||
                item[x].QtySO === undefined ||
                item[x].QtySO === "" ||
                item[x].QtySO == 0 // tambahan yap
            ) {
                bsNotify.show({
                    title: "Mandatory",
                    content: "Semua Qty SO harus lebih dari 0",
                    type: 'danger'
                });
                $scope.onProcessSave = false;
                return;
            }
            if (item[x].UomId == null || item[x].UomId === undefined || item[x].UomId === "") {
                item[x].UomId = 0;
                console.log("GridDetail[i].UomId => ", item[x].UomId);
            }
            if (item[x].UnitPrice == null || item[x].UnitPrice === undefined || item[x].UnitPrice === "") {
                item[x].UnitPrice = 0;
                console.log("GridDetail[i].UnitPrice => ", item[x].UnitPrice);
            }
            if (item[x].DiscountMaterialPercent == null || item[x].DiscountMaterialPercent === undefined || item[x].DiscountMaterialPercent === "") {
                item[x].DiscountMaterialPercent = 0;
                console.log("GridDetail[i].DiscountMaterialPercent => ", item[x].DiscountMaterialPercent);
            }
            if (item[x].DiscountMaterialAmount == null || item[x].DiscountMaterialAmount === undefined || item[x].DiscountMaterialAmount === "") {
                item[x].DiscountMaterialAmount = 0;
                console.log("GridDetail[i].DiscountMaterialAmount => ", item[x].DiscountMaterialAmount);
            }
            if (item[x].DiscountGroupMaterialAmount == null || item[x].DiscountGroupMaterialAmount === undefined || item[x].DiscountGroupMaterialAmount === "") {
                item[x].DiscountGroupMaterialAmount = 0;
                console.log("GridDetail[i].DiscountGroupMaterialAmount => ", item[x].DiscountGroupMaterialAmount);
            }
            if (item[x].DiscountGroupMaterialPercent == null || item[x].DiscountGroupMaterialPercent === undefined || item[x].DiscountGroupMaterialPercent === "") {
                item[x].DiscountGroupMaterialPercent = 0;
                console.log("GridDetail[i].DiscountGroupMaterialPercent => ", item[x].DiscountGroupMaterialPercent);
            }
            // }
            if (item[x].DiscountSpecialPercent == null || item[x].DiscountSpecialPercent === undefined || item[x].DiscountSpecialPercent === "") {
                item[x].DiscountSpecialPercent = 0;
                console.log("GridDetail[i].DiscountSpecialPercent => ", item[x].DiscountSpecialPercent);
            }
            if (item[x].VATAmount == null || item[x].VATAmount === undefined || item[x].VATAmount === "") {
                item[x].VATAmount = 0;
                console.log("GridDetail[i].VATAmount => ", item[x].VATAmount);
            }
            if (item[x].DPPercent == null || item[x].DPPercent === undefined || item[x].DPPercent === "") {
                item[x].DPPercent = 0;
                console.log("GridDetail[i].DPPercent => ", item[x].DPPercent);
            }

            console.log("item[x] =>>> ", item[x]);

            PartsSalesOrder.GetCheckDiskonAppoval(item[x].DiscountSpecialPercent, $scope.user.OrgId, $scope.mData.ServiceTypeId).then(function(res) {
                console.log("somewhere only we know",res)
                console.log("New perspective",res.data)
                console.log("vAppr",vAppr)
                if (res.data != null && res.data != undefined && res.data != "" && res.data.length > 0 )
                {
                    vAppr = vAppr + 1;
                    console.log("somewhere only we",res.data,vAppr)
                }
                if(x < (item.length - 1)){
                    $scope.loopCheckApprove(item,x+1,refTypeSO, statusSO);
                }else if(x == (item.length - 1 )){
                    if (vPartId > 0) {
                        bsNotify.show({
                            title: "Special Order",
                            //content: "test",
                            type: 'info',
                            size: 'small'
                        });
                    }

                    if (vAppr > 0 || vDP > 0 || vPC4 > 0) {
                        // ada approval
                        $scope.GenerateDocNum();
                        $scope.mData['RefSOTypeId'] = refTypeSO;
                        $scope.mData['SalesOrderStatusId'] = statusSO;
                        $scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
                        PartsSalesOrder.setSalesOrderHeader($scope.mData);
                        PartsSalesOrder.setSalesOrderDetail($scope.mData.GridDetail);

                        console.log('mData => ', $scope.mData);
                        console.log('mData.GridDetail => ', $scope.mData.GridDetail);

                        ngDialog.openConfirm({
                            template: '<div ng-include=\"\'app/parts/directpartssales/salesorder/referensi/dialog_approval.html\'\"></div>',
                            plain: true,
                            controller: 'PartsSalesOrderController',
                        }).catch(function(value) {
                            $scope.isApproved = false;
                            console.log("masuk cancel=>",value);
                        });
                    }
                    if (vAppr == 0 && vDP == 0 & vPC4 == 0) {
                        console.log("gak ada approval ", vAppr);
                        PartsCurrentUser.getFormatId($scope.mData.ServiceTypeId, 'SO').then(function(res) {
                                var result = res.data;
                                //console.log("FormatId result = ", result);
                                console.log("FormatId = ", result[0].Results);
                                PartsSalesOrder.getDocumentNumber(result[0].Results).then(function(res) {
                                        var DocNo = res.data;
                                        if (typeof DocNo === 'undefined' || DocNo == null) {
                                            bsNotify.show({
                                                title: "Sales Order",
                                                content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                                                type: 'danger'
                                            });
                                        } else {
                                            $scope.mData.SalesOrderNo = DocNo[0];
                                            if(DocNo[1]){
                                                var date = new Date(DocNo[1]);
                                                var today = new Date();
                                                date.setHours(today.getHours(),today.getMinutes(),today.getSeconds());
                                            }
                                            $scope.mData.DocDate = date;
                                            // $scope.mData.DocDate = DocNo[1]
                                            if( $scope.mData.DocDate == '-' ||  $scope.mData.DocDate == null || $scope.mData.DocDate == undefined)
                                            {
                                                bsNotify.show({
                                                    title: "Sales Order",
                                                    content: "Format Dokumen Salah Tolong Refresh",
                                                    type: 'danger'
                                                });
                                                $scope.onProcessSave = false;
                                                return;
                                            }
                                            console.log("Generating DocNo & DocDate ->");
                                            console.log($scope.mData.SalesOrderNo);
                                            console.log($scope.mData.DocDate);

                                            //here
                                            $scope.mData['RefSOTypeId'] = refTypeSO;
                                            $scope.mData['SalesOrderStatusId'] = statusSO;
                                            $scope.mData['DPAmount'] = parseInt($scope.mData.DPAmount, 10);
                                            $scope.mData['TotalAmount'] = parseInt(Math.round($scope.mData.TotalAmount), 10);
                                            // $scope.mData['TotalAmount'] = parseInt($scope.mData.TotalAmount.toFixed(2), 10);
                                            $scope.mData['SOStatusId'] = statusSO; // ini hanya untuk validasi

                                            $scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
                                            console.log('$scope.mData ', $scope.mData);
                                            console.log('$scope.mData.GridDetail ', $scope.mData.GridDetail);

                                            PartsSalesOrder.setSalesOrderHeader($scope.mData);
                                            PartsSalesOrder.setSalesOrderDetail($scope.mData.GridDetail);

                                            $scope.kirimApproval($scope.mData);
                                        }
                                        // here

                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                );
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        ); // end getFormatId
                    } // end else vAppr
                }
            });
        }
        // ======== end tombol  simpan =========

        $scope.kirimApproval = function(data) {
                console.log("kirimApproval ", data);
                   
                // $scope.mData = data;
                // console.log('kirimApproval mData = ', $scope.mData); SalesOrderStatusId
                $scope.mData = PartsSalesOrder.getSalesOrderHeader();
                $scope.mData.GridDetail = PartsSalesOrder.getSalesOrderDetail();

                if (typeof $scope.mData.SalesOrderNo === 'undefined' || $scope.mData.SalesOrderNo == null) {
                    bsNotify.show({
                    title: "Sales Order",
                    content: "Terjadi kesalahan koneksi, Silahkan simpan kembali",
                    type: 'danger'
                    });
                    $scope.isApproved = false;
                    return ;
                }
                
                //validasi jika simpan so tanpa approval
                if ($scope.mData.SOStatusId != null || $scope.mData.SOStatusId !== undefined) {
                    $scope.mData.SalesOrderStatusId = 2;
                } else {
                    $scope.mData.SalesOrderStatusId = 1;
                }
                console.log("kirimApproval SalesOrderStatusId", $scope.mData.SalesOrderStatusId);
                console.log('$scope.mData => ', $scope.mData);
                console.log('$scope.mData.SalesOrderNo => ', $scope.mData.SalesOrderNo);
                console.log('$scope.mData.GridDetail => ', $scope.mData.GridDetail);

                for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                    //console.log($scope.mData.GridDetail[i].PartsName);
                    $scope.mData.GridDetail[i].SalesOrderId = $scope.mData.SalesOrderId;
                    $scope.mData.GridDetail[i].SalesOrderStatusId = $scope.mData.SalesOrderStatusId;
                    $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
                    $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;
                    $scope.mData.GridDetail[i].UnitPrice = $scope.mData.GridDetail[i].UnitPrice;
                    $scope.mData.GridDetail[i].UnitCost = $scope.mData.GridDetail[i].UnitPrice;
                    $scope.mData.GridDetail[i].TotalCost = Math.round($scope.mData.GridDetail[i].TotalAmount);
                    //$scope.mData.GridDetail[i].TotalCost = parseInt($scope.mData.GridDetail[i].TotalAmount.toFixed(2));
                        //validasi jika simpan so tanpa approval
                    if ($scope.mData.SOStatusId != null || $scope.mData.SOStatusId !== undefined) {
                        $scope.mData.GridDetail[i].MaterialRequestStatusId = 2;
                    } else {
                        $scope.mData.GridDetail[i].MaterialRequestStatusId = 1;
                    }
                }
                console.log('$scope.mData.GridDetail => ', $scope.mData.GridDetail);

                PartsSalesOrder.create($scope.mData, $scope.mData.GridDetail).then(function(res) {
                        var insertResponse = res.data;
                        console.log("Thank you, next",$scope.mData);
                        console.log("insertResponse => ", res.data);
                        bsNotify.show({
                            title: "SalesOrder",
                            content: "Sales Order berhasil disimpan",
                            type: 'success'
                        });
                        $scope.SalesOrderIdnya = 0;
                        var idSO = insertResponse.ResponseMessage.split('#');
                        $scope.SalesOrderIdnya = idSO[1].substr(1, idSO[1].length-2)
                        $scope.SalesOrderIdnya = parseInt($scope.SalesOrderIdnya )
                        // $scope.SalesOrderIdnya = insertResponse.ResponseMessage.substring(18, 16);
                        $scope.cetakPPL($scope.SalesOrderIdnya);
                        $scope.alertAfterSave(insertResponse);

                        $scope.onProcessSave = false;

                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                ); // end PartsSalesOrder.create

                $scope.ngDialog.close();
                PartsSalesOrder.formApi.setMode("grid");
            } // end POST

        // ======== tombol simpan sales order langsung beserta sales billingnya =========
        $scope.SimpanSOdanSBLangsung = function(refTypeSO, statusSO, mData) {
            //
            //console.log("$scope.mData.ServiceTypeId!!! = ", $scope.mData.ServiceTypeId);
            console.log("mData = ", mData);
            //console.log("$scope.mData.CustomerId = ", $scope.mData.CustomerId);
            if ($scope.mData.TimeNeeded == null || $scope.mData.TimeNeeded === undefined) {
                $scope.mData.TimeNeeded = $filter('date')(new Date(), 'HH:mm:ss');
            }
            console.log("Time needed ", $scope.mData.TimeNeeded);
            if ($scope.mData.DateNeeded === undefined || $scope.mData.DateNeeded == null || $scope.mData.DateNeeded == '') {
                $scope.mData.DateNeeded = $filter('date')(new Date(), 'yyyy-MM-dd');
            }
            $scope.mData.DN = $filter('date')($scope.mData.DateNeeded, 'yyyy-MM-dd');
            $scope.mData.TN = $filter('date')($scope.mData.TimeNeeded, 'HH:mm:ss');
            $scope.mData.VDate = $scope.mData.DN + 'T' + $scope.mData.TN + 'Z';
            $scope.mData.DateNeeded = $filter('date')($scope.mData.VDate, 'yyyy-MM-ddTHH:mm:ssZ');
            //console.log("$scope.mData.VDate = ", $scope.mData.VDate);
            console.log("$scope.mData.DateNeeded = ", $scope.mData.DateNeeded);
            $scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
            console.log("$scope.mData.GridDetail = ", $scope.mData.GridDetail);

            var QtySOKosong = false;
            for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                //validasi qTY SO
                if ($scope.mData.GridDetail[i].QtySO == null || $scope.mData.GridDetail[i].QtySO === undefined || $scope.mData.GridDetail[i].QtySO == 0) {
                    QtySOKosong = true;
                }
                if ($scope.mData.GridDetail[i].DPAmount == null || $scope.mData.GridDetail[i].DPAmount === undefined || isNaN($scope.mData.GridDetail[i].DPAmount)) {
                    $scope.mData.GridDetail[i].DPAmount = 0;
                }
                if ($scope.mData.GridDetail[i].DiscountSpecialPercent == null || $scope.mData.GridDetail[i].DiscountSpecialPercent === undefined || isNaN($scope.mData.GridDetail[i].DiscountSpecialPercent)) {
                    $scope.mData.GridDetail[i].DiscountSpecialPercent = 0;
                    //alert('nan');
                }
            };
            console.log('QtySOKosong', QtySOKosong);

            if ($scope.mData.DateNeeded == null || $scope.mData.DateNeeded === 'undefined' || $scope.mData.DateNeeded == '') {
                bsNotify.show({
                    title: "Mandatory",
                    content: "Tanggal Dibutuhkan harus diisi",
                    type: 'danger'
                });
            } else if ($scope.mData.CustomerId == null || $scope.mData.CustomerId === 'undefined' || $scope.mData.CustomerId == '') {
                bsNotify.show({
                    title: "Mandatory",
                    content: "Data Customer harus diisi",
                    type: 'danger'
                });
            } else if ($scope.mData.PaymentMethodId == null || $scope.mData.PaymentMethodId === 'undefined' || $scope.mData.PaymentMethodId == '') {
                bsNotify.show({
                    title: "Mandatory",
                    content: "Metode Pembayaran harus diisi",
                    type: 'danger'
                });
    } else if($scope.mData.PaymentPeriod == null && $scope.mData.PaymentMethodId == 2 || $scope.mData.PaymentPeriod == undefined && $scope.mData.PaymentMethodId == 2  ){
                bsNotify.show({
                    title: "Mandatory",
                    content: "Jangka Waktu Bayar harus diisi",
                    type: 'danger'
                });
            } else if (QtySOKosong) {
                console.log('Validasi QtySO kosong');
                bsNotify.show({
                    title: "Mandatory",
                    content: "Quantity SO harus diisi",
                    type: 'danger'
                });
            } else {
                // validasi approval = jika diskon spesial > 0
                var vDP = 0;
                var vAppr = 0;
                var vPartId = 0;
                for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                    // untuk keperluan approval======================================
                    // if ($scope.mData.GridDetail[i].DiscountSpecialAmount > 0) {
                    //     vAppr = vAppr + 1;
                    // }
                    PartsSalesOrder.GetCheckDiskonAppoval($scope.mData.GridDetail[i].DiscountSpecialPercent, $scope.user.OrgId, $scope.mData.ServiceTypeId).then(function(res) {
                        console.log("somewhere only we know",res)
                        if (res.data!= null || res.data!= undefined)
                        {
                            vAppr = vAppr + 1;
                            console.log("somewhere we know",res.data.result)
                        }
                        else{
                            vAppr = 0;
                        }
                    });
                    if ($scope.mData.GridDetail[i].DPAmount_Temp > $scope.mData.GridDetail[i].DPAmount) {
                        vDP = vDP + 1;
                    }
                    if ($scope.mData.GridDetail[i].PartId == null || $scope.mData.GridDetail[i].PartId === undefined || $scope.mData.GridDetail[i].PartId === "") {
                        vPartId = vPartId + 1;
                    }
                    // end - untuk keperluan approval================================
                }
                console.log("vAppr = ", vAppr);
                console.log("vDP = ", vDP);
                console.log("vPartId = ", vPartId);
                // end - validasi approval
                if (vPartId > 0) {
                    bsNotify.show({
                        title: "Material Belum Diketahui",
                        content: "Untuk material yang belum diketahui, klik tombol 'Simpan'. ",
                        type: 'info',
                        size: 'big'
                    });
                } else
                if (vAppr > 0 || vDP > 0) {

                    $scope.GenerateDocNum();
                    $scope.mData['RefSOTypeId'] = refTypeSO;
                    $scope.mData['SalesOrderStatusId'] = statusSO;
                    //$scope.mData.GridDetail = $scope.gridOptions.data;
                    //$scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
                    PartsSalesOrder.setSalesOrderHeader($scope.mData);
                    PartsSalesOrder.setSalesOrderDetail($scope.mData.GridDetail);

                    console.log('mData = ', $scope.mData);
                    console.log('mData.GridDetail = ', $scope.mData.GridDetail);

                    bsNotify.show({
                        title: "Membutuhkan Approval",
                        content: "Dokumen ini membutuhkan approval. Klik tombol 'Simpan SO'",
                        type: 'danger',
                        size: 'big'
                    });

                    // ngDialog.openConfirm ({
                    //   template:'<div ng-include=\"\'app/parts/directpartssales/salesorder/referensi/dialog_approvalBilling.html\'\"></div>',
                    //   plain: true,
                    //   controller: 'PartsSalesOrderController',
                    // });
                    //// jika klik OK/simpan pada dialog approval, selanjutnya ke: $scope.kirimApprovalSB = function
                } else {
                    console.log("gak ada approval ", vAppr);
                    PartsCurrentUser.getFormatId($scope.mData.ServiceTypeId, 'SO').then(function(res) {
                            var result = res.data;
                            //console.log("FormatId result = ", result);
                            console.log("FormatId = ", result[0].Results);
                            PartsSalesOrder.getDocumentNumber(result[0].Results).then(function(res) {
                                    var DocNo = res.data;
                                    if (typeof DocNo === 'undefined' || DocNo == null) {
                                        bsNotify.show({
                                            title: "Sales Order",
                                            content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                                            type: 'danger'
                                        });
                                    } else {
                                        $scope.mData.SalesOrderNo = DocNo[0];
                                        if(DocNo[1]){
                                            var date = new Date(DocNo[1]);
                                            var today = new Date();
                                            date.setHours(today.getHours(),today.getMinutes(),today.getSeconds());
                                        }
                                        $scope.mData.DocDate = date;
                                        // $scope.mData.DocDate = DocNo[1]
                                        console.log("Generating DocNo & DocDate ->");
                                        console.log($scope.mData.SalesOrderNo);
                                        console.log($scope.mData.DocDate);
                                        //here
                                        $scope.mData['RefSOTypeId'] = refTypeSO;
                                        $scope.mData['SalesOrderStatusId'] = statusSO;
                                        $scope.mData['DPAmount'] = parseInt($scope.mData.DPAmount, 10);
                                        $scope.mData['SOStatusId'] = statusSO; // ini hanya untuk validasi

                                        $scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
                                        console.log('$scope.mData ', $scope.mData);
                                        console.log('$scope.mData.GridDetail ', $scope.mData.GridDetail);

                                        PartsSalesOrder.setSalesOrderHeader($scope.mData);
                                        PartsSalesOrder.setSalesOrderDetail($scope.mData.GridDetail);

                                        $scope.saveSOdanSBLangsung($scope.mData);
                                    }
                                    // here
                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    ); // end format
                } // end else

            } // end else

        };
        // ======== end tombol  simpan =========
        $scope.saveSOdanSBLangsung = function(data) {
                console.log("kirimApproval ", data);
                // $scope.mData = data;
                // console.log('kirimApproval mData = ', $scope.mData); SalesOrderStatusId
                $scope.mData = PartsSalesOrder.getSalesOrderHeader();
                $scope.mData.GridDetail = PartsSalesOrder.getSalesOrderDetail();

                console.log("SOStatusId :", $scope.mData.SOStatusId);
                //$scope.mData.SalesOrderStatusId = 1;
                //validasi jika simpan so tanpa approval
                if ($scope.mData.SOStatusId != null || $scope.mData.SOStatusId !== undefined) {
                    $scope.mData.SalesOrderStatusId = 2;
                } else {
                    $scope.mData.SalesOrderStatusId = 1;
                }

                console.log("kirimApproval SalesOrderStatusId", $scope.mData.SalesOrderStatusId);
                console.log('$scope.mData => ', $scope.mData);
                console.log('$scope.mData.SalesOrderNo => ', $scope.mData.SalesOrderNo);
                console.log('$scope.mData.GridDetail => ', $scope.mData.GridDetail);

                for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                    //console.log($scope.mData.GridDetail[i].PartsName);
                    $scope.mData.GridDetail[i].SalesOrderId = $scope.mData.SalesOrderId;
                    $scope.mData.GridDetail[i].SalesOrderStatusId = $scope.mData.SalesOrderStatusId;
                    $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
                    $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;
                    $scope.mData.GridDetail[i].UnitPrice = $scope.mData.GridDetail[i].UnitPrice;
                    $scope.mData.GridDetail[i].UnitCost = $scope.mData.GridDetail[i].UnitPrice;
                    $scope.mData.GridDetail[i].TotalCost = Math.round($scope.mData.GridDetail[i].TotalAmount);
                    //$scope.mData.GridDetail[i].TotalCost = parseInt($scope.mData.GridDetail[i].TotalAmount.toFixed(2));

                    $scope.mData.GridDetail[i].QtySB = $scope.mData.GridDetail[i].QtySO
                    $scope.mData.GridDetail[i].UomId = $scope.mData.GridDetail[i].UomId

                    //$scope.mData.GridDetail[i].MaterialRequestStatusId = 1;
                    //validasi jika simpan so tanpa approval
                    if ($scope.mData.SOStatusId != null || $scope.mData.SOStatusId !== undefined) {
                        $scope.mData.GridDetail[i].MaterialRequestStatusId = 2;
                    } else {
                        $scope.mData.GridDetail[i].MaterialRequestStatusId = 1;
                    }
                }
                console.log('$scope.mData.GridDetail => ', $scope.mData.GridDetail);

                //if($scope.mData.SalesOrderNo.Results != null){
                PartsSalesOrder.create($scope.mData, $scope.mData.GridDetail).then(function(res) {
                        var insertResponse = res.data;
                        console.log("OKE ari",$scope.mData );
                        console.log("insertResponse => ", res.data);
                        bsNotify.show({
                            title: "SalesOrder",
                            content: "Sales Order berhasil disimpan",
                            type: 'success'
                        });
                        $scope.SalesOrderIdnya = 0;
                        var idSO = insertResponse.ResponseMessage.split('#');
                        $scope.SalesOrderIdnya = idSO[1].substr(1, idSO[1].length-2)
                        $scope.SalesOrderIdnya = parseInt($scope.SalesOrderIdnya )
                        // $scope.SalesOrderIdnya = insertResponse.ResponseMessage.substring(18, 16);
                        $scope.cetakPPL($scope.SalesOrderIdnya);
                        //$scope.alertAfterSave(insertResponse);
                        //here
                        $scope.mData.xSalesOrderNo = $scope.mData.SalesOrderNo;
                        PartsSalesOrder.getData2($scope.mData).then(function(res) {
                                var gridData = res.data.Result;
                                console.log("<controller getDataSO> GridData => ", gridData);
                                console.log("$scope.mData.vSalesOrderNo => ", $scope.mData.vSalesOrderNo);
                                //$scope.grid.data = gridData;
                                //$scope.loading = false;
                                //POST sales billing
                                $scope.mData.SalesOrderId = gridData[0].SalesOrderId;
                                console.log('mdata.salesorderid =>', $scope.mData.SalesOrderId);
                                PartsSalesBilling.createApprove($scope.mData, $scope.mData.GridDetail).then(function(res) {
                                        var create = res.data;
                                        console.log('res.data = ', res.data);
                                        bsNotify.show({
                                            title: "Sales Billing",
                                            content: "Data berhasil disimpan",
                                            type: 'success'
                                        });
                                        $scope.SalesOrderIdnya = 0;
                                        var idSO = insertResponse.ResponseMessage.split('#');
                                        $scope.SalesOrderIdnya = idSO[1].substr(1, idSO[1].length-2)
                                        $scope.SalesOrderIdnya = parseInt($scope.SalesOrderIdnya )
                                        // $scope.SalesOrderIdnya = insertResponse.ResponseMessage.substring(18, 16);
                                        $scope.cetakPPL($scope.SalesOrderIdnya);
                                        $scope.alertAfterSave2(insertResponse, create);
                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                );

                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );

                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                ); // end PartsSalesOrder.create
                //}

                $scope.ngDialog.close();
                PartsSalesOrder.formApi.setMode("grid");
            } // end POST

        // POST approval SO & SB
        $scope.kirimApprovalSB = function(data) {
                console.log("kirimApproval ", data);
                // $scope.mData = data;
                // console.log('kirimApproval mData = ', $scope.mData); SalesOrderStatusId
                $scope.mData = PartsSalesOrder.getSalesOrderHeader();
                $scope.mData.GridDetail = PartsSalesOrder.getSalesOrderDetail();

                // //validasi jika simpan so tanpa approval
                // if($scope.mData.SOStatusId != null || $scope.mData.SOStatusId !== undefined){
                //   $scope.mData.SalesOrderStatusId = 2;
                // } else {
                //   $scope.mData.SalesOrderStatusId = 1;
                // }
                $scope.mData.SalesOrderStatusId = 1;

                console.log("kirimApproval SalesOrderStatusId", $scope.mData.SalesOrderStatusId);
                console.log('$scope.mData => ', $scope.mData);
                console.log('$scope.mData.GridDetail => ', $scope.mData.GridDetail);

                for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                    //console.log($scope.mData.GridDetail[i].PartsName);
                    $scope.mData.GridDetail[i].SalesOrderId = $scope.mData.SalesOrderId;
                    $scope.mData.GridDetail[i].SalesOrderStatusId = $scope.mData.SalesOrderStatusId;
                    $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
                    $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;
                    $scope.mData.GridDetail[i].UnitPrice = $scope.mData.GridDetail[i].UnitPrice
                        // //validasi jika simpan so tanpa approval
                        // if($scope.mData.SOStatusId != null || $scope.mData.SOStatusId !== undefined){
                        //   $scope.mData.GridDetail[i].MaterialRequestStatusId = 2;
                        // } else {
                        //   $scope.mData.GridDetail[i].MaterialRequestStatusId = 1;
                        // }
                    $scope.mData.GridDetail[i].MaterialRequestStatusId = 1;
                }
                console.log('$scope.mData.GridDetail => ', $scope.mData.GridDetail);

                PartsSalesOrder.create($scope.mData, $scope.mData.GridDetail).then(function(res) {
                        var insertResponse = res.data;
                        console.log("insertResponse => ad ", $scope.mData);
                        console.log("insertResponse => ", res.data);
                        bsNotify.show({
                            title: "SalesOrder",
                            content: "Data SO berhasil disimpan",
                            type: 'success'
                        });
                        $scope.SalesOrderIdnya = 0;
                        var idSO = insertResponse.ResponseMessage.split('#');
                        $scope.SalesOrderIdnya = idSO[1].substr(1, idSO[1].length-2)
                        $scope.SalesOrderIdnya = parseInt($scope.SalesOrderIdnya )
                        // $scope.SalesOrderIdnya = insertResponse.ResponseMessage.substring(18, 16);
                        $scope.cetakPPL($scope.SalesOrderIdnya);
                        $scope.alertAfterSave(insertResponse);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                ); // end PartsSalesOrder.create

                // Sekarang buat SB nya setelah SO nya "di-approve"
                // //POST sales billing
                // PartsSalesBilling.createApprove($scope.mData, $scope.mData.GridDetail).then(function (res) {
                //     var create = res.data;
                //     console.log('res.data = ', res.data);
                //     bsNotify.show({
                //       title: "Sales Billing",
                //       content: "Data berhasil disimpan",
                //       type: 'success'
                //     });
                //   },
                //   function (err) {
                //     console.log("err=>", err);
                //   }
                // );

                $scope.ngDialog.close();
                PartsSalesOrder.formApi.setMode("grid");
            } // end POST

        // ======== tombol batal - dari grid =========
        $scope.batalGrid = function(row) {
            console.log('batalSO - row =>', row);
            PartsSalesOrder.setSalesOrderHeader(row);
            console.log('PartsSalesOrder.getSalesOrderHeader() = ', PartsSalesOrder.getSalesOrderHeader());

            PartsSalesOrder.AvailableInSB(row).then(function(res) { // check id sales billing available
                    var hasil = res.data[0].AvailableInSB;
                    console.log('<batalGrid> isavailable hasil', hasil);
                    $scope.mData.isAvailableInSB = hasil;
                    console.log('$scope.mData.isAvailableInSB', $scope.mData.isAvailableInSB);
                    PartsSalesOrder.getDetail(row).then(function(res) {
                        $scope.detail = res.data;
                        console.log('batalSO -  $scope.mData.GridDetail',$scope.detail);
                        console.log('PartsSalesOrder.getSalesOrderHeader() = ', PartsSalesOrder.getSalesOrderHeader());
                        console.log('$scope.mData.isAvailableInSB', $scope.mData.isAvailableInSB);
                        var utm = 0;
                        for (var i = 0; i < $scope.detail.length; i++) {
                            if ($scope.detail[i].MaterialRequestStatusId == 80) {
                                utm = utm + 1;
                            }
                            else{
                            }

                            if ($scope.detail[i].CreatedDate != null) {
                                $scope.detail[i].CreatedDate = $scope.localeDate($scope.detail[i].CreatedDate);
                            }
                        }
                    if (row.SalesOrderStatusId == 5) {
                        bsNotify.show({
                            size: 'small',
                            type: 'warning',
                            title: "Tidak Bisa Dibatalkan",
                            content: "Status Sales Order sudah 'Cancelled'"
                        });
                    } else
                    if ($scope.mData.isAvailableInSB > 0) {
                        bsNotify.show({
                            size: 'small',
                            type: 'warning',
                            title: "Tidak Bisa Dibatalkan",
                            content: "Sales Order ini sudah dibuatkan Sales Billing"
                        });
                    }
                    else
                    if (utm > 0) {
                        bsNotify.show({
                            size: 'small',
                            type: 'warning',
                            title: "Tidak Bisa Dibatalkan",
                            content: "Harap Batalkan GI Terlebih dahulu"
                        });
                    }
                    else {
                        ngDialog.openConfirm({
                            template: '<div ng-include=\"\'app/parts/directpartssales/salesorder/referensi/dialog_batal.html\'\"></div>',
                            plain: true,
                            scope: $scope
                        });
                    } // end else
                });
                },
                function(err) {
                    console.log("err=>", err);
                }

            );
        };

        // ======== tombol Complete - dari grid =========
        $scope.completeGrid = function(row) {
            console.log('completeSO - row =>', row);
            //PartsSalesOrder.setSalesOrderComplete(row);
            console.log('PartsSalesOrder.getSalesOrderHeader() = ', PartsSalesOrder.getSalesOrderHeader(row));
            $scope.mData = row;
            
            console.log('yakinBatal mData = ', $scope.mData.SalesOrderId);

            PartsSalesOrder.setSalesOrderComplete($scope.mData.SalesOrderId);
            $scope.getData($scope.tmpFilter);
            $scope.formApi.setMode("grid");
            console.log("ok",row);


            PartsSalesOrder.AvailableInSB(row).then(function(res) { // check id sales billing available
                    var hasil = res.data[0].AvailableInSB;
                    console.log('<completeGrid> isavailable hasil', hasil);
                    $scope.mData.isAvailableInSB = hasil;
                    console.log('$scope.mData.isAvailableInSB', $scope.mData.isAvailableInSB);
                    if (row.SalesOrderStatusId == 4) {
                        bsNotify.show({
                            size: 'small',
                            type: 'warning',
                            title: "Status sudah Completed",
                            content: "Status Sales Order sudah 'Completed'"
                        });
                    }
                        if (row.SalesOrderStatusId == 5) {
                            bsNotify.show({
                                size: 'small',
                                type: 'warning',
                                title: "Status Tidak Bisa Dibuat 'Completed'",
                                content: "Sales Order dalam status cancel"
                            });
                        }

                         // end else
                },
                function(err) {
                    console.log("err=>", err);
                    
                }

            );
            
        };


        $scope.UbahData = function(mData) {
            // [START] Get Search Appointment
            console.log("mData ====",mData);
            console.log("$scope.mData.GridDetail",$scope.mData.GridDetail);
            console.log("$scope.mData ====",$scope.mData);
            console.log("mData ====",mData);
            // $scope.mData.CreatedUserId = $scope.user.UserId;
            // return

            switch ($scope.mData.RefSOTypeId) {
                case 1:
                    $scope.mData.GridDetail = $scope.gridOptions.data;
                    break;
                case 2:
                    $scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
                    break;
            }

            for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                //console.log($scope.mData.GridDetail[i].PartsName);
                if($scope.mData.GridDetail[i].SalesOrderItemId == '' || $scope.mData.GridDetail[i].SalesOrderItemId == null ){
                    $scope.mData.GridDetail[i].SalesOrderId = $scope.mData.SalesOrderId;
                    $scope.mData.GridDetail[i].SalesOrderStatusId = $scope.mData.SalesOrderStatusId;
                    $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
                    $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;
                    $scope.mData.GridDetail[i].UnitPrice = $scope.mData.GridDetail[i].UnitPrice;
                    $scope.mData.GridDetail[i].UnitCost = $scope.mData.GridDetail[i].UnitPrice;
                    $scope.mData.GridDetail[i].TotalCost = Math.round($scope.mData.GridDetail[i].TotalAmount);
                    delete $scope.mData.GridDetail[i].SalesOrderItemId;
                    $scope.mData.GridDetail[i].DiscountGroupMaterialPercent = 0;
                    $scope.mData.GridDetail[i].DiscountMaterialPercent = 0;
                    if ($scope.mData.GridDetail[i].DiscountGroupMaterialPercent == null || $scope.mData.GridDetail[i].DiscountGroupMaterialPercent === undefined || $scope.mData.GridDetail[i].DiscountGroupMaterialPercent === "") {
                        $scope.mData.GridDetail[i].DiscountGroupMaterialPercent = 0;
                    }
                    if ($scope.mData.GridDetail[i].DiscountMaterialPercent == null || $scope.mData.GridDetail[i].DiscountMaterialPercent === undefined || $scope.mData.GridDetail[i].DiscountMaterialPercent === "") {
                        $scope.mData.GridDetail[i].DiscountMaterialPercent = 0;
                    }
                    if ($scope.mData.GridDetail[i].DPPercent == null || $scope.mData.GridDetail[i].DPPercent === undefined || $scope.mData.GridDetail[i].DPPercent === "") {
                        $scope.mData.GridDetail[i].DPPercent = 0;
                    }
                    //$scope.mData.GridDetail[i].TotalCost = parseInt($scope.mData.GridDetail[i].TotalAmount.toFixed(2));
                        //validasi jika simpan so tanpa approval
                    if ($scope.mData.SalesOrderStatusId != null || $scope.mData.SalesOrderStatusId !== undefined) {
                        $scope.mData.GridDetail[i].MaterialRequestStatusId = 2;
                    } else {
                        $scope.mData.GridDetail[i].MaterialRequestStatusId = 1;
                    }
                }
            }   

            PartsSalesOrder.update($scope.mData, $scope.mData.GridDetail).then(function(res) {
                    var create = res.data;
                    var noPo = res.data.Response?res.data.Response:res.data;
                    console.log("res.data=>",res.data);
                    var tmpSalesOrderId = res.data.ResponseMessage;
                    console.log("resu Response", res.data);
                    tmpSalesOrderId = tmpSalesOrderId.split('#');
                    // console.log("mdataGoodissue", $scope.mData);
                    // $scope.dataGI = tmpSalesOrderId[1];

                    if (res.data.ResponseCode == 100001) {
                        bsNotify.show({
                            title: "Sales Order",
                            // content: tmpSalesOrderId[1],
                            content: "Sales Order tidak dapat diubah, harap batalkan Billing Sales Order dahulu",
                            type: 'danger'
                        });
                    } else {
                        if (res.data.ResponseCode == 23) {
                            bsNotify.show({
                                title: "Sales Order ",
                                content: "Data berhasil diubah",
                                type: 'success'
                            });
                        } else {
                            bsNotify.show({
                                title: "Sales Order ",
                                content: "Data gagal diubah",
                                type: 'danger'
                            });
                        }

                        $scope.goBack();
                        $scope.mData.startDate = $scope.tmpFilter.startDate;
                        $scope.mData.endDate = $scope.tmpFilter.endDate;
                        $scope.getData($scope.tmpFilter);
                    }
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        // ======== tombol batal - form  =========
        $scope.pembatalan = function(mData) {
            console.log('batalSO - mData =>', mData);
            // $scope.mData.GridDetail = PartsSalesOrder.setSalesOrderDetail($scope.mData.GridDetail);;
            // PartsSalesOrder.setSalesOrderHeader(mData);
            PartsSalesOrder.getDetail(mData).then(function(res) {
                $scope.detail = res.data;
                console.log('batalSO -  $scope.mData.GridDetail',$scope.detail);
                console.log('PartsSalesOrder.getSalesOrderHeader() = ', PartsSalesOrder.getSalesOrderHeader());
                console.log('$scope.mData.isAvailableInSB', $scope.mData.isAvailableInSB);
                var utm = 0;
                for (var i = 0; i < $scope.detail.length; i++) {
                    if ($scope.detail[i].MaterialRequestStatusId == 4) {
                        bsNotify.show({
                            size: 'small',
                            type: 'warning',
                            title: "Sales Order Tidak Dapat Dibatalkan",
                            content: "Parts Masih Dalam Proses Order"
                        });
                        return -1;
                    }

                    if ($scope.detail[i].MaterialRequestStatusId == 80) {
                        utm = utm + 1;
                    }
                    else{
                    }

                    if ($scope.detail[i].CreatedDate != null) {
                        $scope.detail[i].CreatedDate = $scope.localeDate($scope.detail[i].CreatedDate);
                    }
                }
                console.log('$scope complete', utm);
                if (mData.SalesOrderStatusId == 5) {
                    bsNotify.show({
                        size: 'small',
                        type: 'warning',
                        title: "Tidak Bisa Dibatalkan",
                        content: "Status Sales Order sudah 'Cancelled'"
                    });
                } else if ($scope.mData.isAvailableInSB > 0) {
                    bsNotify.show({
                        size: 'small',
                        type: 'warning',
                        title: "Tidak Bisa Dibatalkan",
                        content: "Sales Order ini sudah dibuatkan Sales Billing"
                    });
                } else if (utm > 0) {
                    bsNotify.show({
                        size: 'small',
                        type: 'warning',
                        title: "Tidak Bisa Dibatalkan",
                        content: "Harap Batalkan GI Terlebih dahulu"
                    });
                } else {
                    ngDialog.openConfirm({
                        template: '<div ng-include=\"\'app/parts/directpartssales/salesorder/referensi/dialog_batal.html\'\"></div>',
                        plain: true,
                        scope: $scope
                    });
                }
            });
        };

        // dialog konfirmasi
        // $scope.tmpFilter = {};
        $scope.okBatalSO = function(data) {
            console.log('okBatalSO');

            console.log('tes batal', data);
            // $scope.tmpFilter = angular.copy(data);
            if ((data.CancelReasonId == null) || (data.CancelReasonId == undefined) || (data.CancelReasonId == "")) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "error",
                    content: "Isi Alasan Batal"
                });
            } else {


                $scope.BatalData = PartsSalesOrder.getSalesOrderHeader();

                $scope.BatalData.CancelReasonDesc = data.CancelReasonDesc;
                $scope.BatalData.CancelReasonId = data.CancelReasonId;

                //$scope.BatalData.Notes = 'Alasan: ' + data.Batal + ', Catatan: ' + data.Catatan;

                console.log($scope.BatalData);

                PartsSalesOrder.setSalesOrderHeader($scope.BatalData);

                ngDialog.openConfirm({
                    //template:'<div ng-include=\"\'app/parts/directpartssales/salesorder/referensi/dialog_konfirmasi_batal.html\'\"></div>',
                    template: '\
                   <div align="center" class="ngdialog-buttons">\
                   <p><b>Konfirmasi</b></p>\
                   <p>Anda yakin akan membatalkan Sales Order ini?</p>\
                   <div class="ngdialog-buttons" align="center">\
                     <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                     <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinBatal(BatalData,tmpFilter)">Ya</button>\
                   </div>\
                   </div>',
                    plain: true,
                    scope: $scope
                });
                //console.log('okBatalSO  = ', $scope.BatalData.Notes);
            }
        };

        $scope.yakinBatal = function(data, filter) {
            console.log('SetujuBatal', filter);
            $scope.mData = data;
            $scope.ngDialog.close();
            $scope.mData.SalesOrderStatusId = 5;

            console.log('yakinBatal mData = ', $scope.mData);
            // [START] Get
            PartsSalesOrder.cancel($scope.mData).then(function(res) {
                    console.log('res cancel = ', res);
                    //alert("Sales Order berhasil dibatalkan");
                    bsNotify.show({
                        size: 'small',
                        type: 'success',
                        title: "Success",
                        content: "Sales Order berhasil dibatalkan"
                    });
                    // if(filter.SalesOrderStatusId !== undefined){
                    //   $scope.mData.SalesOrderStatusId = filter.SalesOrderStatusId;
                    // }
                    // if (filter.startDate !== undefined) {
                    //     $scope.mData.startDate = filter.startDate;
                    // }
                    // if (filter.endDate !== undefined) {
                    //     $scope.mData.endDate = filter.endDate;
                    // }
                    // if (filter.vSalesOrderNo !== undefined) {
                    //     $scope.mData.vSalesOrderNo = filter.vSalesOrderNo;
                    // }
                    // if (filter.vRefSONo !== undefined) {
                    //     $scope.mData.vRefSONo = filter.vRefSONo;
                    // }
                    // if (filter.vBengkel !== undefined) {
                    //     $scope.mData.vBengkel = filter.vBengkel;
                    // }
                    // if (filter.vCustomerName !== undefined) {
                    //     $scope.mData.vCustomerName = filter.vCustomerName;
                    // }
                    console.log("$scope.tmpFilter==>",$scope.tmpFilter);
                    console.log("filter==>",filter);
                    // $scope.getData(filter);
                    $scope.goBack();
                    $scope.mData.startDate = $scope.tmpFilter.startDate;
                    $scope.mData.endDate = $scope.tmpFilter.endDate;
                    $scope.getData($scope.tmpFilter);
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            // [END] Get
        }

        // ======== end tombol batal - parts claim =========

        // Report =========================================
        $scope.laporan = function() {
            ngDialog.openConfirm({
                template: '<div ng-include=\"\'app/parts/disposal/referensi/template_laporan.html\'\"></div>',
                plain: true,
                // width: 800px,
                controller: 'DisposalController',
            });
        };

        $scope.tambahCustomer = function() {
            //console.log("tambahCustomer()");
            //console.log("$scope.custAddress before=> ", $scope.custAddress);
            //$scope.custAddress = [];
            console.log('CCategory', $scope.CCategory);
            console.log('KodeJenisTransaksi', $scope.KodeJenisTransaksi);
            console.log('DataProvinsi', $scope.DataProvinsi);
            // default customertypeid for individu
            $scope.custAddress = {}
            $scope.custAddress.CustomerTypeId = 3
            
            $scope.addcustomer = true;
            if ($scope.CCategory == '' || $scope.CCategory == null) {
                $scope.CCategory = $scope.CCategoryTemp;
            }
            if ($scope.KodeJenisTransaksi == '' || $scope.KodeJenisTransaksi == null) {
                $scope.KodeJenisTransaksi = $scope.KodeJenisTransaksiTemp;
            }
            if ($scope.DataProvinsi == '' || $scope.DataProvinsi == null) {
                $scope.DataProvinsi = $scope.DataProvinsiTemp;
            }
            //$scope.DataProvinsi
            console.log('CCategoryTemp', $scope.CCategoryTemp);
            console.log('KodeJenisTransaksiTemp', $scope.KodeJenisTransaksiTemp);
            console.log('DataProvinsiTemp', $scope.DataProvinsiTemp);

            

            // $scope.DataProvinsi.ProvinceId = "";
            // $scope.DataKabupaten.CityRegencyId = "";
            // $scope.DataRayon.DistrictId = "";
            // $scope.custAddress.VillageId = "";
            //console.log("$scope.custAddress after=> ", $scope.custAddress);
        };

        $scope.ubahCustomer = function() {
            console.log("EditCust = item => ", $scope.EditCust);

            console.log('CCategory', $scope.CCategory);
            console.log('KodeJenisTransaksi', $scope.KodeJenisTransaksi);
            console.log('DataProvinsi', $scope.DataProvinsi);
            console.log('DataKabupaten', $scope.DataRayon);
            $scope.ubah_customer = true;
          
            if ($scope.CCategory == '' || $scope.CCategory == null) {
                $scope.CCategory = $scope.CCategoryTemp;
            }
            if ($scope.KodeJenisTransaksi == '' || $scope.KodeJenisTransaksi == null) {
                $scope.KodeJenisTransaksi = $scope.KodeJenisTransaksiTemp;
            }
            if ($scope.DataProvinsi == '' || $scope.DataProvinsi == null) {
                $scope.DataProvinsi = $scope.DataProvinsiTemp;
            }
            console.log('CCategoryTemp', $scope.CCategoryTemp);
            console.log('KodeJenisTransaksiTemp', $scope.KodeJenisTransaksiTemp);
            console.log('DataProvinsiTemp', $scope.DataProvinsiTemp);

            $scope.copyDataCust = angular.copy($scope.ubahCust)

            PartsSalesOrder.DataDesa($scope.ubahCust.DistrictId).then(function(res) {
                console.log("<ctrl>res.data DataDesa ==>", res.data.Result);
                $scope.DataDesa = res.data.Result;
            });

        };

        $scope.cancel = function() {
            $scope.addcustomer = false;
            $scope.custAddress = [];
            $scope.CCategory = "";
            $scope.KodeJenisTransaksi = "";
            $scope.DataProvinsi.ProvinceId = undefined;
            if ($scope.DataKabupaten.CityRegencyId !== undefined) { $scope.DataKabupaten.CityRegencyId = undefined };
            $scope.DataRayon.DistrictId = undefined;
            $scope.custAddress.VillageId = undefined;
            $scope.DataDesa.PostalCode = "";

            // if($scope.custAddress.ProvinceId !== undefined) {$scope.custAddress.ProvinceId = undefined};
            // $scope.custAddress.CityRegencyId = undefined;
            // if($scope.custAddress.CityRegencyId !== undefined) {$scope.custAddress.ProviCityRegencyIdnceId = undefined};
            // $scope.custAddress.DistrictId = undefined;
            // if($scope.custAddress.DistrictId !== undefined) {$scope.custAddress.DistrictId = undefined};
            // $scope.custAddress.PostalCode = "";

            $scope.viewBengkel = false;
            $scope.viewBengkelLain = false;
        }
        $scope.cancelUbah = function() {
            $scope.ubah_customer = false;
            if ($scope.copyDataCust != undefined && $scope.copyDataCust != null){
                $scope.ubahCust = angular.copy($scope.copyDataCust)
            }
        }
        $scope.Bengkel = {}

        // Cetakan
        $scope.cetakPPL = function(SalesOrderId) {
            console.log('SalesOrderId =', SalesOrderId);
            console.log('isavailable in SB', $scope.mData.isAvailableInSB);

            //yap
            //ga perlu ada validasi lagi kata pak Ari -- 6 maret 2018
            /*
            if($scope.mData.isAvailableInSB > 0){
              //console.log('OutletId =', $scope.user.OrgId);
              var data = $scope.mData;
              $scope.printPPL = 'as/PrintPenjualanPartLangsung/' + SalesOrderId;
              $scope.cetakan($scope.printPPL);
            } else {
              bsNotify.show({
                size: 'small',
                type: 'warning',
                title: "Tidak Bisa Cetak",
                content: "Dokumen ini belum dibuatkan Billing-nya"
              });
            }
            */

            $scope.printPPL = 'as/PrintPenjualanPartLangsung/' + SalesOrderId;
            $scope.cetakan($scope.printPPL);
        };

        $scope.cetakPPLPO = function(SalesOrderId) {
            // $scope.printPPLPO = 'as/PrintPOBengkelLain/' + SalesOrderId;
            $scope.printPPLPO = 'as/PrintPenjualanPartLangsung/' + SalesOrderId;
            $scope.cetakanPO($scope.printPPLPO);
        };

        $scope.cetakan = function(data) {
            var pdfFile = null;

            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    }
                    else {
                        printJS(pdfFile);
                    }
                }
                else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };

        $scope.cetakanPO = function(data) {
            var pdfFile = null;

            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    }
                    else {
                        printJS(pdfFile);
                    }
                }
                else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };

        $scope.onBeforeNewMode = function(mode) {
            $scope.formode = 1;
            //console.log('$scope.ubahCust', $scope.ubahCust);
            $scope.isOverlayForm = true;
            $scope.isApproved = true;
            $scope.isUbahBengkelLain = false;
            $scope.isUbahCustomerLangsung = false;
            $scope.isLihatBengkelLain = false;
            $scope.isLihatCustomerLangsung = false;
            $scope.onProcessSave = false;
            $scope.mData.CustomerTypeId = 3;
            //$scope.waktuSekarang();
            //var vTime = $filter('date')(new Date(), 'yyyy-MM-ddTHH:mm:ss');
            //$scope.mData.TimeNeeded = vTime;
            //console.log("$scope.mData.TimeNeeded = ", $scope.mData.TimeNeeded);

            //$scope.mData.TimeNeeded = PartsGlobal.getCurrTime();
            PartsSalesOrder.getCCustomerCategory().then(function(res) { // start getdata kategori customer
                    var CCategory = res.data.Result;
                    console.log('CCategory =', CCategory);
                    $scope.CCategory = CCategory;
                    $scope.CCategoryTemp = CCategory;
                },
                function(err) {
                    console.log("err=>", err);
                }
            ); // end getdata
            PartsSalesOrder.getKodeJenisTransaksi().then(function(res) { // start getdata kode jenis transaksi
                    var KodeJenis = res.data;
                    console.log('KodeJenis =', KodeJenis);
                    //console.log('KodeJenis =', KodeJenis);
                    $scope.KodeJenisTransaksi = KodeJenis;
                    $scope.KodeJenisTransaksiTemp = KodeJenis;
                },
                function(err) {
                    console.log("err=>", err);
                }
            ); // end getdata
            $scope.gridOptions.data = [];
            $scope.gridOptionsLangsung.data = [];

            PartsSalesOrder.formApi = $scope.formApi;

            $scope.mData.ServiceTypeId = $scope.getRightServiceType();
            console.log('ServiceType = ', $scope.mData.ServiceTypeId);
            $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
            console.log("MaterialTypeId = ", $scope.mData.MaterialTypeId);

            $scope.selection = null;
            var Doctype = '';
            var DocName = '';
            var param_warehouse = {};
            param_warehouse = {
                OutletId: $scope.user.OrgId,
                MaterialTypeId: $scope.mData.MaterialTypeId,
                ServiceTypeId: $scope.mData.ServiceTypeId
            }
            PartsGlobal.getMyWarehouse(param_warehouse).then(function(res) {
                    var loginData = res.data;
                    console.log("loginData", loginData.Result[0]);
                    $scope.mData.OutletId = $scope.user.OrgId;
                    $scope.mData.WarehouseId = loginData.Result[0].WarehouseId;
                    $scope.mData.WarehouseName = loginData.Result[0].WarehouseName.split(" ").pop();
                    var loginData = res.data;
                    if ($scope.mData.WarehouseName == "GR") {
                        console.log("gr");
                        Doctype = 1
                        DocName = 'SB'
                    } else {
                        console.log("bp");
                        Doctype = 2
                        DocName = 'SB'
                    }


                    //PartsCurrentUser.getFormatId(Doctype,DocName).then(function (res) {
                    console.log("test", loginData[0]);
                    // $scope.mData.OutletId = loginData[0].OutletId;
                    // $scope.mData.WarehouseId = loginData[0].WarehouseId;
                    //$scope.mData.MaterialTypeId = loginData[0].MaterialTypeId;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

            var UserId;
            var DocNo = '';
            if (LocalService.get('BSLocalData')) {
                LoginObject = angular.fromJson(LocalService.get('BSLocalData')).LoginData;
            }
            UserId = angular.fromJson(LoginObject).UserId;
            // PartsCurrentUser.getFormatId(Doctype, DocName).then(function(res) {
            //         DocNo = res.data;
            //         $scope.mData.SalesOrderNo = DocNo[0];
            //         $scope.mData.DocDate = DocNo[1];
            //         console.log("Generating DocNo & DocDate ->");
            //         console.log($scope.mData.SalesOrderNo);
            //         console.log($scope.mData.DocDate);
            //     },
            //     function(err) {
            //         console.log("err=>", err);
            //     }
            // );

            console.log('ApprovalProcessId', $scope.ApprovalProcessId);
            PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res) {
                    var Approvers = res.data.Result[0].Approvers;
                    console.log("getApprover res = ", Approvers);
                    $scope.mData.Approvers = Approvers;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

            console.log("onBeforeNewMode = ", mode);
            //$scope.GenerateDocNum();
        }
        $scope.onBeforeEditMode = function(row, mode) {
            $scope.formode = 2;
            console.log("onBeforeEdit=>", mode);
            // $scope.onShowDetail(row, mode);
        }
        $scope.onBeforeDeleteMode = function() {
            $scope.formode = 3;
            $scope.isNewForm = false;
            $scope.isEditForm = false;
            console.log("mode=>", $scope.formmode);
        }
        $scope.onShowDetail = function(row, mode) {

            console.log('onShowDetail mode = ', mode);
            console.log('onShowDetail row = ', row);
            $scope.onProcessSave = false;
            $scope.ubahCust = {};
            

            // validasi UI ToyotaId
            if (row.ToyotaId_CustList == null) {
                $scope.AdaToyotaId = false;
            } else if (row.ToyotaId_CustList === 'undefined') {
                $scope.AdaToyotaId = false;
            } else {
                $scope.AdaToyotaId = true;
            }
            console.log('row.ToyotaId_CustList = ', row.ToyotaId_CustList);
            console.log('$scope.AdaToyotaId = ', $scope.AdaToyotaId);

            if (mode !== 'new') {
                PartsSalesOrder.AvailableInSB(row).then(function(res) { // check id sales billing available
                        var hasil = res.data[0].AvailableInSB;
                        console.log('<onShowDetail> isavailable hasil', hasil);
                        $scope.mData.isAvailableInSB = hasil;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

                $scope.isView = false;
                $scope.isEdit = false;
            } // end if

            if (mode == 'view') { //jika mode = view
                $scope.isUbahBengkelLain = false;
                $scope.isUbahCustomerLangsung = false;
                if (row.RefSOTypeId == 1) { // bengkel lain
                    $scope.isLihatBengkelLain = true;
                    $scope.isLihatCustomerLangsung = false;
                } else if (row.RefSOTypeId == 2) { //'Penjualan Langsung'
                    $scope.isLihatBengkelLain = false;
                    $scope.isLihatCustomerLangsung = true;
                }

                $scope.isView = true;
                $scope.isEdit = false;

            } else if (mode == 'edit') { //jika mode edit
                // cek, apakah SO.Id sudah ada di sales billing
                // PartsSalesOrder.AvailableInSB(row).then(function (res){
                //   var hasil = res.data[0].AvailableInSB;
                //     console.log('isavailable hasil', hasil);
                //     $scope.mData.isAvailableInSB = hasil;
                //   },
                //   function (err){
                //     console.log("err=>", err);
                //   }
                // );
                PartsSalesOrder.setSalesOrderHeader(row);
                console.log('OnShowDetail Mode Edit getSalesOrderHeader() = ', PartsSalesOrder.getSalesOrderHeader());

                $scope.isLihatBengkelLain = false;
                $scope.isLihatCustomerLangsung = false;
                if (row.RefSOTypeId == 1) {
                    $scope.isUbahBengkelLain = true;
                    $scope.isUbahCustomerLangsung = false;
                } else if (row.RefSOTypeId == 2) {
                    $scope.isUbahBengkelLain = false;
                    $scope.isUbahCustomerLangsung = true;
                }

                $scope.isView = false;
                $scope.isEdit = true;
            } else
            // mode New
            if (mode == 'new') {
                
                // alert('mode new');
            }
            $scope.ref = '';
            //log
            console.log("onShowDetail=>", mode);

            if (mode != 'new') {
                if (row.GoodsIssueStatusId == 0) {
                    $scope.isGIDone = 1;
                } else {
                    $scope.isGIDone = 0;
                }
                console.log("row.GoodsIssueStatusId==>",row.GoodsIssueStatusId);
                console.log("$scope.isGIDone==>",$scope.isGIDone);
                console.log("$scope.mData1==>>",$scope.mData);

                PartsSalesOrder.getDetail(row).then(function(res) {
                        // var gridData = res.data.Result;
                        var gridData = res.data;
                        $scope.isApproved = false;
                        //console.log('<controller> row.RefSDNo = ', row.PartsCode);
                        for (var i = 0; i < gridData.length; i++) {
                            var hasil = gridData[i].DPAmount - gridData[i].DPReceived;
                            gridData[i].NilaiDPAmount = angular.copy(gridData[i].DPAmount);
                            if (gridData[i].DPAmount > 0 && hasil <= 0) {
                                console.log('MR=', gridData[i].MaterialRequestStatusId);
                                gridData[i].isLunas = true;
                                console.log('islunas=', gridData[i].isLunas);
                            }

                            // if(gridData[i].MaterialRequestStatusId == 3 || gridData[i].MaterialRequestStatusId == 80){
                            //   // jika status material request = Menunggu Order || Completed Request
                            //   console.log('MR=', gridData[i].MaterialRequestStatusId);
                            //   gridData[i].isLunas = true;
                            //   console.log('islunas=', gridData[i].isLunas);
                            // }

                            if (gridData[i].CreatedDate != null) {
                                gridData[i].CreatedDate = $scope.localeDate(gridData[i].CreatedDate);
                            }
                        }

                        $scope.mData.MaterialTypeId = $scope.materialtypes;
                        $scope.mData.ServiceTypeId = $scope.servicetypes;

                        console.log('<controller> getDetail = ', gridData);
                        console.log("$scope.mData==>>",$scope.mData);
                        console.log("$scope.mData.RefSOTypeId==>>",$scope.mData.RefSOTypeId);

                        // console.log('<controller> gridOptionsLangsung = ', gridData); //$scope.gridOptionsLangsung.data);
                        switch ($scope.mData.RefSOTypeId) {
                            case 1:
                                $scope.gridOptionsLihat.data = gridData;
                                console.log("$scope.gridOptionsLihat.data 1==>>",$scope.gridOptionsLihat.data);
                                break;
                            case 2:
                                if (mode == 'edit') {
                                    $scope.gridOptionsLangsung.data = gridData;
                                    console.log("$scope.gridOptionsLangsung.data 2==>>",$scope.gridOptionsLangsung.data);
                                } else {
                                    $scope.gridOptionsLihat.data = gridData;
                                    console.log("$scope.gridOptionsLihat.data 2==>>",$scope.gridOptionsLihat.data);
                                }
                                break;
                        }
                        // $scope.gridOptionsLihat.data
                        console.log("$scope.gridOptionsLihat.data==>>",$scope.gridOptionsLihat.data);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }

            console.log("$scope.isView=>",$scope.isView);
            console.log("$scope.isEdit=>",$scope.isEdit);
        }

        //----------------------------------
        // [Start] Button Handler
        //----------------------------------
        // Button Search WO/Appointment
        $scope.SearchNomorPO = function(RefSONo) {
            var param_warehouse = {};
            param_warehouse = {
                OutletId: $scope.user.OrgId,
                MaterialTypeId: $scope.mData.MaterialTypeId,
                ServiceTypeId: $scope.mData.ServiceTypeId
            }
            console.log('$scope.mData.OutletLainId => ', $scope.mData.OutletLainId);
            //console.log('$scope.mData.OutletLainId.length => ', $scope.mData.OutletLainId.length);
            console.log('$scope.mData.RefSONo => ', $scope.mData.RefSONo);
            if ($scope.mData.OutletLainId == null || $scope.mData.OutletLainId === 'undefined' || $scope.mData.OutletLainId.length == 0) {
                bsNotify.show({
                    size: 'medium',
                    type: 'warning',
                    title: "Mandatory",
                    content: "Pilih Bengkel Lain"
                });
            } else if ($scope.mData.RefSONo == null || $scope.mData.RefSONo === 'undefined') {
                bsNotify.show({
                    size: 'medium',
                    title: "Mandatory",
                    content: "isi Nomor PO",
                    type: 'warning'
                });
            } else {
                PartsGlobal.getMyWarehouse(param_warehouse).then(function(res) {
                        var loginData = res.data;
                        console.log("loginData", loginData.Result[0]);
                        $scope.mData.OutletId = $scope.user.OrgId;
                        $scope.mData.WarehouseId = loginData.Result[0].WarehouseId;
                        $scope.mData.WarehouseName = loginData.Result[0].WarehouseName.split(" ").pop();
                        var loginData = res.data;
                        console.log('loginData[0] => ', loginData);
                        // $scope.mData.OutletId = loginData[0].OutletId;
                        // $scope.mData.WarehouseId = loginData[0].WarehouseId;
                        //$scope.mData.MaterialTypeId = loginData[0].MaterialTypeId;
                        // [START] Get Search Appointment Grid Detail
                        console.log('$scope.mData.OutletId => ', $scope.mData.OutletId);
                        console.log('$scope.mData.OutletLainId => ', $scope.mData.OutletLainId);
                        var outlet = $scope.mData.OutletId;
                        var outletlain = $scope.mData.OutletLainId;
                        if (outlet == outletlain) {
                            bsNotify.show({
                                size: 'small',
                                type: 'warning',
                                title: "Benkel",
                                content: "Data Bengkel diri sendiri"
                            });
                        }

                        PartsSalesOrder.SearchNoPurchaseOrder(RefSONo, outletlain, outlet).then(function(res) {
                                var DataPO = res.data[0];
                                console.log('res.data ==>', res.data);
                                console.log('DataPO ==>', DataPO);
                                if (DataPO == undefined) {
                                    console.log("DataPO", DataPO);
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'warning',
                                        title: "No. PO Tidak Sesuai",
                                        content: "Pastikan data Bengkel dan No. PO sudah sesuai"
                                    });
                                } else {

                                    $scope.mData.PaymentMethodId = DataPO.PaymentMethodId;
                                    $scope.mData.PaymentPeriod = DataPO.PaymentPeriod;
                                    $scope.mData.CustomerOutletId = DataPO.OutletId;
                                    $scope.Bengkel.BengkelName = DataPO.fullBengkel;
                                    $scope.Bengkel.BengkelAddress = DataPO.Address;
                                    $scope.Bengkel.BengkelPhone = DataPO.PhoneNumber;
                                    // $scope.mData.TotalAmount = DataPO.TotalAmount;
                                    $scope.mData.Bengkel = DataPO.Bengkel;
                                    // [START] Get Search Appointment Grid Detail
                                    PartsSalesOrder.SearchNoPurchaseOrderItem(RefSONo, outletlain, outlet).then(function(res) {
                                            var DataPO = res.data;
                                            console.log('DataPOnya list resdata ==>', res.data);
                                            angular.forEach(DataPO, function(value, key) {
                                                    console.log('forEach value = ', value);
                                                    console.log('forEach key = ', key);
                                                    console.log('forEach value.length = ', value.length);
                                                    value.DiscountSpecialPercent = value.DiscountSpecialPercent != null || value.DiscountSpecialPercent != undefined ? value.DiscountSpecialPercent : 0;
                                                    value.DiscountGroupMaterialAmount = value.DiscountGroupMaterialAmount != null || value.DiscountGroupMaterialAmount != undefined ? value.DiscountGroupMaterialAmount : 0;
                                                })
                                                //$scope.mData.TotalAmount = DataPO[0].SumTotalAmount;
                                                //$scope.mData.DPAmount = DataPO[0].SumDPAmount;
                                            var vTotalAmount = 0;
                                            var vDPAmount = 0;
                                            //console.log('DataPO.length ==>', DataPO.length);
                                            for (var i = 0; i < DataPO.length; i++) {
                                                console.log("i => ", i);
                                                vTotalAmount = vTotalAmount + DataPO[i].TotalAmount;
                                                vDPAmount = vDPAmount + DataPO[i].DPAmount;
                                                DataPO[i].DPAmount_Temp = DataPO[i].DPAmount;
                                                console.log('vTotalAmount =>', vTotalAmount);
                                                console.log('vDPAmount =>', vDPAmount);
                                            }
                                            $scope.mData.TotalAmount = Math.round(vTotalAmount);
                                            // $scope.mData.TotalAmount = parseInt(vTotalAmount.toFixed(2));
                                            $scope.mData.DPAmount = vDPAmount;
                                            $scope.mData.TotalCost = Math.round(vTotalAmount);
                                            // $scope.mData.TotalCost = parseInt(vTotalAmount.toFixed(2));

                                            console.log('DataPOnya list ==>', DataPO);
                                            $scope.gridOptions.data = DataPO;
                                            $scope.mData.GridDetail = DataPO;
                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                        }
                                    );
                                    // [END] Get Search Appointment Grid
                                }
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                        // [END] Get Search Appointment Grid
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            } // end else
        }

        $scope.searchCustomerCode = function(CustomerCode) {
            // //original
            // var tempdata = {};
            // tempdata = {
            //   CustomerCode : data
            // }
            // var CustomerCode =[];
            // CustomerCode.push(tempdata);

            ////  angular.forEach(DataPO, function (value, key) {
            ////   value.DiscountSpecialPercent = value.DiscountSpecialPercent != null || value.DiscountSpecialPercent != undefined ? value.DiscountSpecialPercent : 0;
            ////   value.DiscountGroupMaterialAmount = value.DiscountGroupMaterialAmount != null || value.DiscountGroupMaterialAmount != undefined ? value.DiscountGroupMaterialAmount : 0;
            //// })
            //// CustomerCode = data;

            console.log("customercode => ", CustomerCode);
            // if (CustomerCode === "" || CustomerCode == null) {
            //   bsNotify.show({
            //     title: "Sales Order",
            //     content: "Customer Code tidak boleh kosong",
            //     type: 'danger'
            //   });
            // }
            if (CustomerCode === "" || CustomerCode == null) {
                bsNotify.show({
                    title: "Sales Order",
                    content: "Customer Code tidak boleh kosong",
                    type: 'danger'
                });
            } else {
                PartsSalesOrder.SearchCustomerByCode(CustomerCode).then(function(res) {
                        var resData = res.data[0];
                        //console.log('resData[0] => ', resData[0]);
                        console.log('resData => ', resData);
                        //if (resData[0] == null) {
                        if (resData == null) {
                            bsNotify.show({
                                title: "Sales Order",
                                content: "Toyota ID tidak ditemukan",
                                type: 'danger'
                            });
                        } else {
                            // $scope.mData.CustomerName = resData[0].Name;
                            // $scope.mData.CustomerId = resData[0].CustomerOwnerId;
                            // $scope.mData.Phone1 = resData[0].Handphone1;
                            // $scope.mData.Phone2 = resData[0].Handphone2;
                            alert('ok');
                        }
                    },
                    function(err) {
                        console.log("err=>", err);
                        bsNotify.show({
                            title: "Sales Order",
                            content: "Toyota ID tidak ditemukan",
                            type: 'danger'
                        });
                    }
                );
            }
        }

        // $scope.GetCustomer = function(key)
        // {
        //   //search material request
        //   PartsSalesOrder.SearchCustomerByCode().then(function(res){
        //     console.log("List Customer ====>", res);
        //     $scope.CustData = res.data;
        //   });
        // }

        // $scope.selectCust = function(key)
        // {
        //   console.log("Isi Cust = ", key);
        //   //$scope.mData.MaterialRequestId = key.MaterialRequestId;
        //   $scope.mData.CustomerCode = key.CustomerCode;
        // }

        $scope.GetCustomer = function() {
            console.log("GetCustomer");
            PartsSalesOrder.SearchCustomerByCode().then(function(res) {
                console.log("Res Customer ====>", res);
                console.log("res.data.Result Customer ====>", res.data);
                $scope.ubah_customer = true;
                if(res.data.length > 0){
                    //  Npwp: null
                    // KTPKITAS
                    $scope.mData.CustomerCode = res.data[0].CustomerCode;
                    $scope.mData.CustomerId = res.data[0].CustomerId;
                    $scope.mData.Phone1 = res.data[0].Phone1;
                    $scope.mData.Phone2 = res.data[0].Phone2;
                    $scope.mData.KTPKITAS = res.data[0].KTPKITAS;
                    $scope.mData.Npwp = res.data[0].Npwp;
                    $scope.CustData = res.data;
                }
                
            });
        };
        $scope.selectCust = function(row) {
            console.log('row selectCust', row);
            if (row == 'undefined' || row == null) {
                $scope.mData.CustomerName = [];
                $scope.mData.CustomerId = [];
                $scope.mData.Phone1 = [];
                $scope.mData.Phone2 = [];
            } else {
                $scope.mData.CustomerName = row.Name ? row.Name:row.CustomerName;
                $scope.mData.CustomerId = row.CustomerId;
                $scope.mData.Phone1 = row.Phone1;
                $scope.mData.Phone2 = row.Phone2;
            }

            //$scope.selectedProvinnce = angular.copy(row);
        };

        $scope.GetOutletLain = function() {
            console.log("GetOutletLain");
            PartsSalesOrder.OutletLain().then(function(res) {
                console.log("Res OutletLain ==>", res);
                console.log("res.data.Result OutletLain ==>", res.data);
                $scope.DataOutletLain = res.data;
            });
        };
        $scope.selectOutletLain = function(row) {
            console.log('row selectOutletLain ', row);
            if (row === 'undefined' || row == null) {
                $scope.mData.OutletLainId = [];
                $scope.mData.BengkelLain = null;
                console.log('$scope.BengkelLain = ', $scope.BengkelLain);
            } else {
                $scope.mData.OutletLainId = row.OutletLainId;
                $scope.mData.BengkelLain = row.OutletLainId;
                //console.log('$scope.BengkelLain = ', $scope.BengkelLain);
                console.log('$scope.mData.OutletLainId = ', $scope.mData.OutletLainId);
                $scope.Bengkel.BengkelName = row.Name;
                $scope.Bengkel.BengkelAddress = row.Address;
                $scope.Bengkel.BengkelPhone = row.PhoneNumber;
                BengkelNm=row.Name;
                BengkelAddr=row.Address;
                BengkelPhn=row.PhoneNumber;
            }
            //$scope.selectedProvinnce = angular.copy(row);
        };
        // select kategory customer
        $scope.selectCCategory = function(row) {
            console.log('row selectCCategory ', row);
            //$scope.mData.CustomerTypeId = row.CustomerTypeId;
            //console.log('CustomerTypeId ', $scope.mData.CustomerTypeId);
        };
        $scope.selectKodeJenisTransaksi = function(row) {
            console.log('row KodeJenisTransaksi', row);
        };
        // data provinsi
        $scope.GetDataProvinsi = function() {
            //console.log("GetDataProvinsi");
            PartsSalesOrder.DataProvinsi().then(function(res) {
                //console.log("<ctrl>res GetDataProvinsi ==>", res);
                console.log("<ctrl>res.data GetDataProvinsi ==>", res.data);
                $scope.DataProvinsi = res.data.Result;
                $scope.DataProvinsiTemp = res.data.Result;
            });
        };
        $scope.selectProvinsi = function(row) {
            console.log('row selectProvinsi ', row);
            //console.log('$scope.DataProvinsi.ProvinceId ', $scope.DataProvinsi.ProvinceId);
            // get data kabupaten

            // reset drop down bawah nya ----------------------------------------------- start
            // $scope.DataRayon = [];
            // $scope.DataDesa = [];
            if ($scope.DataRayon == undefined){
                $scope.DataRayon = []
            }
            if ($scope.DataDesa == undefined){
                $scope.DataDesa = []
            }
            $scope.custAddress.CityRegencyId = undefined
            $scope.ubahCust.CityRegencyId = undefined

            $scope.custAddress.DistrictId = undefined
            $scope.ubahCust.DistrictId = undefined

            $scope.custAddress.VillageId = undefined
            $scope.ubahCust.VillageId = undefined

            $scope.DataDesa.PostalCode = undefined
            $scope.ubahCust.PostalCode = undefined
            // reset drop down bawah nya ----------------------------------------------- end
            
            
            
            PartsSalesOrder.DataKabupaten(row.ProvinceId).then(function(res) {
                console.log("<ctrl>res.data DataKabupaten ==>", res.data.Result);
                $scope.DataKabupaten = res.data.Result;
                if ($scope.DataKabupaten.length == 0) {
                    bsNotify.show({
                        size: 'big',
                        type: 'notification',
                        title: "Data Provinsi yang dipilih tidak lengkap. Silahkan hubungi Administrator",
                    });
                } else {
                    $scope.DataProvinsi.ProvinceId = row.ProvinceId;
                    $scope.custAddress.ProvinceId = row.ProvinceId;
                    $scope.ubahCust.ProvinceId = row.ProvinceId;
                    console.log('ubahCust.ProvinceId ', $scope.ubahCust.ProvinceId);
                }
            });
        };
        $scope.GetDataRayon = function() {
            //console.log("GetDataProvinsi");
            PartsSalesOrder.DataRayon().then(function(res) {
                //console.log("<ctrl>res GetDataProvinsi ==>", res);
                console.log("<ctrl>res.data GetDataRayon ==>", res.data);
                $scope.DataRayon = res.data.Result;
                $scope.DataRayonTemp = res.data.Result;
            });
        };
        // select kabupaten
        $scope.selectKabupaten = function(row) {
            console.log('row selectKabupaten', row);

            // reset drop down bawah nya ----------------------------------------------- start

            $scope.custAddress.DistrictId = null
            $scope.ubahCust.DistrictId = null

            $scope.custAddress.VillageId = null
            $scope.ubahCust.VillageId = null

            $scope.DataDesa.PostalCode = null
            $scope.ubahCust.PostalCode = null
            // reset drop down bawah nya ----------------------------------------------- end


            // get data rayon
            PartsSalesOrder.DataRayon(row.CityRegencyId).then(function(res) {
                console.log("<ctrl>res.data DataRayon ==>", res.data.Result);
                $scope.DataRayon = res.data.Result;
                $scope.DataRayon.CityRegencyId = row.CityRegencyId;
                $scope.custAddress.CityRegencyId = row.CityRegencyId;
                $scope.ubahCust.CityRegencyId = row.CityRegencyId;
                console.log('ubahCust.CityRegencyId ', $scope.ubahCust.CityRegencyId);
            });
        };
        // select rayon
        $scope.selectRayon = function(row) {
            console.log('row selectRayon', row);

            // reset drop down bawah nya ----------------------------------------------- start

            $scope.custAddress.VillageId = null
            $scope.ubahCust.VillageId = null

            $scope.DataDesa.PostalCode = null
            $scope.ubahCust.PostalCode = null
            // reset drop down bawah nya ----------------------------------------------- end

            //get data desa
            PartsSalesOrder.DataDesa(row.DistrictId).then(function(res) {
                console.log("<ctrl>res.data DataDesa ==>", res.data.Result);
                $scope.DataDesa = res.data.Result;
                $scope.DataRayon.DistrictId = row.DistrictId;
                $scope.custAddress.DistrictId = row.DistrictId;
                $scope.ubahCust.DistrictId = row.DistrictId;
                console.log('ubahCust.DistrictId ', $scope.ubahCust.DistrictId);
            });
        };
        // select rayon
        $scope.selectRayonUbah = function(row) {
            console.log('row selectRayon', row);
            $scope.DataDesa = [];
            console.log('$scope.DataDesa', $scope.DataDesa);
            //get data desa
            PartsSalesOrder.DataDesa(row.DistrictId).then(function(res) {
                console.log("<ctrl>res.data DataDesa ==>", res.data.Result);
                $scope.DataDesa = res.data.Result;
                $scope.DataRayon.DistrictId = row.DistrictId;
                $scope.custAddress.DistrictId = row.DistrictId;
                $scope.ubahCust.DistrictId = row.DistrictId;
                console.log('ubahCust.DistrictId ', $scope.ubahCust.DistrictId);
            });
        };
        // select Desa
        $scope.selectDesa = function(row) {
            console.log('row selectDesa', row);
            $scope.DataDesa.VillageId = row.VillageId;
            $scope.DataDesa.PostalCode = row.PostalCode;
            $scope.custAddress.PostalCode = row.PostalCode;

            $scope.ubahCust.PostalCode = row.PostalCode;
        };

        //----------------------------------
        // Select Option Referensi GI Setup
        //----------------------------------
        // debugger;
        $scope.changedValue = function(item) {
            console.log(item);
            if (item != null) {
                $scope.isOverlayForm = false;
                $scope.ref = item;
                $scope.disableSelect = true;
                $scope.filterFieldChange($scope.filterField[0]);
            }
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 20, 30, 40, 50, 100, 150],
            paginationPageSize: 15,
            columnDefs: [{
                    name: 'SalesOrderId',
                    field: 'SalesOrderId',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'no. Sales Order',
                    displayName: 'No. Sales Order',
                    field: 'SalesOrderNo',
                    width: '13%'
                },
                // {
                //   name: 'tanggal Sales Order',
                //   displayName: 'Tanggal Sales Order',
                //   field: 'DocDate',
                //   cellFilter: 'date:\'yyyy-MM-dd\'',
                //   width: '9%'
                // },
                {
                    name: 'tanggal Sales Order',
                    displayName: 'Tanggal',
                    field: 'DocDateDesc',
                    // cellFilter: 'date:\'yyyy-MM-dd\'',
                    width: '8%'
                },
                // {
                //   name: 'referensi Sales Order',
                //   displayName: 'Referensi Sales Order',
                //   field: 'RefSOTypeId',
                //   width: '12%',
                //   cellFilter: 'filterTypeSalesOrder'
                // },
                {
                    name: 'referensi Sales Order',
                    displayName: 'Referensi Sales Order',
                    field: 'RefSOTypeIdDesc',
                    width: '12%',
                    // cellFilter: 'filterTypeSalesOrder'
                },
                {
                    name: 'no. PO',
                    field: 'RefSONo',
                    displayName: 'No. PO',
                    width: '12%'
                },
                {
                    name: 'bengkel',
                    field: 'Bengkel',
                    width: '12%'
                },
                {
                    name: 'customer',
                    //field: 'CustomerName',
                    field: 'CustomerName',
                    width: '12%'
                },
                // {
                //   name: 'status',
                //   field: 'SalesOrderStatusId',
                //   width: '10%',
                //   cellFilter: 'filterStatusSalesOrder'
                // },
                {
                    name: 'status',
                    field: 'SalesOrderStatusIdDesc',
                    width: '10%',
                    // cellFilter: 'filterStatusSalesOrder'
                },
                {
                    name: 'CustomerId',
                    displayName: 'CustomerId',
                    field: 'CustomerId',
                    width: '10%',
                    visible: false
                },
                {
                    name: 'CustomerCode',
                    displayName: 'CustomerCode',
                    field: 'CustomerCode',
                    width: '10%',
                    visible: false
                },
                {
                    name: 'Phone1',
                    displayName: 'Phone1',
                    field: 'Phone1',
                    width: '10%',
                    visible: false
                },
                {
                    name: 'Phone2',
                    displayName: 'Phone2',
                    field: 'Phone2',
                    width: '10%',
                    visible: false
                },
                {
                    name: 'Phone',
                    displayName: 'Phone',
                    field: 'Phone',
                    width: '10%',
                    visible: false
                },
                {
                    name: 'ToyotaId_CustList',
                    displayName: 'ToyotaId_CustList',
                    field: 'ToyotaId_CustList',
                    width: '10%',
                    visible: false
                },
                {
                    name: '-',
                    headerCellClass: "middle",
                    displayName: '',
                    width: '6%',
                    pinnedRight: true,
                    //enableCellEdit: false,
                    // visible : true,
                    //cellTemplate: '<div style="padding-top: 5px;"><u><a href="#" ng-click="grid.appScope.Delete(row)">Hapus</a></u></div>',
                    cellTemplate: '<div style="text-align:left; margin-left: 5px;"><button class="btn rbtn btn-xs" ng-show="row.entity.GoodsIssueStatusId==0 && row.entity.paid==1 && grid.appScope.$parent.user.RoleId!=1128 " ng-click="grid.appScope.$parent.completeGrid(row.entity)">Complete</button></div>',
                    enableCellEdit: false
                },
                {
                    name: '_',
                    headerCellClass: "middle",
                    displayName: '',
                    width: '6%',
                    pinnedRight: true,
                    //enableCellEdit: false,
                    // visible : true,
                    //cellTemplate: '<div style="padding-top: 5px;"><u><a href="#" ng-click="grid.appScope.Delete(row)">Hapus</a></u></div>',
                    cellTemplate: '<div style="text-align:left; margin-left: 5px;"><button class="btn rbtn btn-xs" ng-show="row.entity.GoodsIssueStatusId==0 && row.entity.SalesOrderStatusId!=4 && grid.appScope.$parent.user.RoleId!=1128 " ng-click="grid.appScope.$parent.batalGrid(row.entity)">Batal</button></div>',
                    enableCellEdit: false
                }
            ]
        };


        //----------------------------------
        // Dummy
        //----------------------------------

        $scope.TotalAmount = function() {
            var tmpTotal = 0;
            var tmpDP = 0;
            var tmpVAT = 0;
            for (var i in $scope.gridOptionsLangsung.data) {
                console.log("$scope.gridOptionsLangsung ",i,"==>",$scope.gridOptionsLangsung);
                tmpTotal += $scope.gridOptionsLangsung.data[i].TotalAmount;
                tmpDP += $scope.gridOptionsLangsung.data[i].DPAmount;
                tmpVAT += $scope.gridOptionsLangsung.data[i].VATAmount;
            }
            $scope.mData.TotalAmount = Math.floor(tmpTotal);
            $scope.mData.DPAmount = tmpDP;
            $scope.mData.VATAmount = Math.floor(tmpVAT);
        }

        $scope.showMe = function() {
            var result = confirm('Apakah Anda yakin menghapus data ini?');
            console.log("result==>", result);
            if (result) {
                console.log('atas gridOptionsLangsung', $scope.gridOptionsLangsung.data);
                console.log('scope.gridApi.selection', $scope.gridApi.selection.getSelectedRows());
                var data = $scope.gridApi.selection.getSelectedRows();
                console.log("selected", data);
                angular.forEach($scope.gridApi.selection.getSelectedRows(), function(data, index) {
                    $scope.gridOptions.data.splice($scope.gridOptions.data.lastIndexOf(data), 1);
                    $scope.gridOptionsLangsung.data.splice($scope.gridOptionsLangsung.data.lastIndexOf(data), 1);
                });
                $scope.TotalAmount();
            }
        };

        $scope.bsAlertConfirm = function (title, text) {
            var defer = $q.defer();
            bsAlert.alert({
                title: title || 'Sales Order',
                text: text || '',
                type: "warning",
                showCancelButton: true
            },
                function () {
                    defer.resolve(true);
                },
                function () {
                    defer.resolve(false);
                }
            )
            return defer.promise;
        };

        $scope.hapusPart = function(data, param) {
            $scope.bsAlertConfirm("Apakah Anda yakin menghapus part ini?").then(function (res) {
                if (res) {
                    var data = $scope.gridApi.selection.getSelectedRows();
                    console.log("selected", data);
                    angular.forEach($scope.gridApi.selection.getSelectedRows(), function(data, index) {
                        if(data.SalesOrderStatusId == 2 && data.QtyOrder > 0){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: 'Tidak dapat dihapus, sedang dalam proses order. Batalkan order atau tunggu order complete',
                                timeout: 2000
                            });
                        }else if(data.SalesOrderStatusId == 3 && data.QtyGI > 0){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: 'Tidak dapat dihapus, batalkan Goods Issue dahulu',
                                timeout: 2000
                            });
                        }else if(data.SalesOrderStatusId >= 4){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: 'No. Parts '+(data.PartsCode ? data.PartsCode :'-') +" tidak dapat di hapus",
                                timeout: 2000
                            });
                        }else{
                            console.log('===>', data, index);
                            $scope.gridOptions.data.splice($scope.gridOptions.data.lastIndexOf(data), 1);
                            $scope.gridOptionsLangsung.data.splice($scope.gridOptionsLangsung.data.lastIndexOf(data), 1);
                        }
                    });
                    $scope.TotalAmount();
                }else{
                    console.log("ini kalo klik cancle", res);
                }
            });
        };

        $scope.Delete = function(row) {
            console.log('row', row);
            //console.log('atas length', $scope.gridOptionsLangsung.data.length);
            console.log('atas gridOptionsLangsung', $scope.gridOptionsLangsung.data);
            var lengthAtas = $scope.gridOptionsLangsung.data.length

            var index = $scope.gridOptions.data.indexOf(row.entity);
            var index2 = $scope.gridOptionsLangsung.data.indexOf(row.entity);

            console.log('gridOptions', $scope.gridOptions.data);
            console.log(index);

            $scope.gridOptions.data.splice(index, 1);
            $scope.gridOptionsLangsung.data.splice(index2, 1);

            console.log('bawah gridOptionsLangsung', $scope.gridOptionsLangsung.data);
            console.log(index2);

            //console.log('bawah length', $scope.gridOptionsLangsung.data.length);
            // var lengthBawah = $scope.gridOptionsLangsung.data.length;
            // if (lengthAtas !== lengthBawah) {
            //     console.log('**lengthAtas', lengthAtas);
            //     console.log('**lengthBawah', lengthBawah);
            //     $scope.mData.TotalAmount = Math.floor($scope.mData.TotalAmount - row.entity.TotalAmount);
            //     $scope.mData.DPAmount = $scope.mData.DPAmount - row.entity.SumDPAmount;
            //     $scope.mData.VATAmount = Math.floor($scope.mData.VATAmount - row.entity.VATAmount);
            //     console.log('mData.TotalAmount', $scope.mData.TotalAmount);
            //     console.log('mData.DPAmount', $scope.mData.DPAmount);
            //     if ($scope.mData.TotalAmount < 0 || $scope.mData.DPAmount < 0) {
            //         $scope.mData.TotalAmount = 0;
            //         $scope.mData.DPAmount = 0;
            //         $scope.mData.VATAmount = 0;
            //     }
            // }

            $scope.TotalAmount();
        };

        $scope.gridOptions = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableFiltering: true,
            //selectedItems: console.log($scope.mySelections),
            enableSelectAll: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 5,
            showColumnFooter: false,
            showGridFooter: false,
            // onRegisterApi: function(gridApi) {
            //   $scope.gridApi = gridApi;
            // },

            columnDefs: [{
                    name: 'SalesOrderItemId',
                    field: 'SalesOrderItemId',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'SalesOrderId',
                    field: 'SalesOrderId',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'PartId',
                    field: 'PartId',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'SalesOrderStatusId',
                    field: 'SalesOrderStatusId',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'WarehouseId',
                    field: 'WarehouseId',
                    width: '7%',
                    visible: false
                },
                // {
                //   name: 'RetailPriceDP', field: 'RetailPriceDP', width: '7%'
                // },
                {
                    name: 'No Material',
                    displayName: 'No. Material',
                    field: 'PartCode',
                    width: '12%',
                    headerCellClass: "middle",
                    enableCellEdit: false
                        // enableCellEditOnFocus: true,
                        // cellTemplate:'\
                        // <div class="ui-grid-cell-contents"><div class="input-group">\
                        // <input ng-model="mData.PartsCode" ng-value="row.entity.PartsCode" required>\
                        //   <button class="btn btn-default btn-xs" type="button" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)"><span class="glyphicon glyphicon-search"></span></button>\
                        // </div></div>'
                },
                {
                    name: 'Nama Material',
                    displayName: 'Nama Material',
                    field: 'PartName',
                    enableCellEdit: false,
                    width: '18%',
                    headerCellClass: "middle"
                },
                {
                    name: 'Qty Free',
                    displayName: 'Qty Free',
                    field: 'QtyFree',
                    width: '8%',
                    headerCellClass: "middle",
                    enableCellEdit: false
                },
                {
                    name: 'Qty SO',
                    displayName: 'Qty SO',
                    field: 'QtySO',
                    type: 'number',
                    cellFilter: 'number',
                    width: '8%',
                    enableCellEdit: true,
                    editableCellTemplate: '<input type="number" min="1" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                    headerCellClass: "middle"
                },
                {
                    name: 'UomId',
                    displayName: 'UomId',
                    field: 'UomId',
                    width: '6%',
                    visible: false
                },
                {
                    name: 'Satuan',
                    displayName: 'Satuan',
                    field: 'satuanName',
                    width: '6%',
                    headerCellClass: "middle",
                    enableCellEdit: false
                },
                {
                    name: 'Harga Satuan',
                    displayName: 'Harga Satuan',
                    field: 'UnitPrice',
                    enableCellEdit: false,
                    cellFilter: 'number',
                    width: '10%',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationHideLabel: true,
                    headerCellClass: "middle"
                },
                {
                    name: 'DM Persen',
                    displayName: '%',
                    field: 'DiscountMaterialPercent',
                    width: '5%',
                    enableCellEdit: false,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:260%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Material</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'DM Jumlah',
                    displayName: 'Jumlah',
                    field: 'DiscountMaterialAmount',
                    width: '8%',
                    cellFilter: 'number',
                    enableCellEdit: false,
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationHideLabel: true,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
                },
                {
                    name: 'DGM Persen',
                    displayName: '%',
                    field: 'DiscountGroupMaterialPercent',
                    width: '8%',
                    enableCellEdit: false,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:220%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Group Material</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'DGM Jumlah',
                    displayName: 'Jumlah',
                    field: 'DiscountGroupMaterialAmount',
                    width: '10%',
                    cellFilter: 'number',
                    enableCellEdit: false,
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationHideLabel: true,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
                },
                {
                    name: 'DS Persen',
                    displayName: '%',
                    field: 'DiscountSpecialPercent',
                    width: '5%',
                    enableCellEdit: true,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:260%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Spesial</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'DS Jumlah',
                    displayName: 'Jumlah',
                    field: 'DiscountSpecialAmount',
                    width: '8%',
                    cellFilter: 'number',
                    enableCellEdit: false,
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationHideLabel: true,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
                },
                {
                    name: 'ppn',
                    displayName: 'PPN',
                    field: 'VATAmount',
                    cellFilter: 'number',
                    enableCellEdit: false,
                    width: '7%',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationHideLabel: true,
                    headerCellClass: "middle"
                },
                {
                    name: 'harga Total',
                    displayName: 'Harga Total',
                    field: 'TotalAmount',
                    cellFilter: 'number',
                    enableCellEdit: false,
                    width: '10%',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationHideLabel: true,
                    headerCellClass: "middle"
                },
                {
                    name: 'Unit Cost',
                    displayName: 'Unit Cost',
                    field: 'UnitCost',
                    width: '10%',
                    headerCellClass: "middle",
                    enableCellEdit: false,
                    visible: false
                },
                {
                    name: 'Total Cost',
                    displayName: 'Total Cost',
                    field: 'TotalCost',
                    //field: 'getTotalCost()',
                    width: '10%',
                    headerCellClass: "middle",
                    cellFilter: 'number',
                    enableCellEdit: false,
                    visible: false
                },
                {
                    name: 'DPJumlah',
                    displayName: 'Jumlah',
                    field: 'DPAmount',
                    width: '5%',
                    cellFilter: 'number',
                    enableCellEdit: true,
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationHideLabel: true,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:260%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2' style='text-align:left;padding-left:10px;'>DP</td></tr></tbody></table></div>Jumlah</div>"
                },
                // {
                //   name: 'DPLunas',
                //   displayName: 'Lunas (DP)',
                //   field: 'DPLunas',
                //   width: '8%',
                //   type: 'boolean',
                //   enableCellEdit: false,
                //   headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Lunas</div>",
                //   cellTemplate: '<input type="checkbox" ng-model="row.entity.DPLunas">'
                // },
                {
                    name: 'Lunas',
                    displayName: 'Lunas',
                    field: 'isLunas',
                    width: '8%',
                    type: 'boolean',
                    enableCellEdit: false,
                    cellTemplate: '<input type="checkbox" ng-model="row.entity.isLunas" disabled>',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Lunas</div>"
                },
                {
                    name: 'DP Temp',
                    displayName: 'DP Temp',
                    field: 'DPAmount_Temp',
                    enableCellEdit: false,
                    cellFilter: 'number',
                    width: '10%',
                    headerCellClass: "middle",
                    visible: false
                },
                {
                    name: '_',
                    headerCellClass: "middle",
                    displayName: 'Action',
                    width: '10%',
                    pinnedRight: true,
                    //enableCellEdit: false,
                    // visible : true,
                    //cellTemplate: '<div style="padding-top: 5px;"><u><a href="#" ng-click="grid.appScope.Delete(row)">Hapus</a></u></div>',
                    cellTemplate: '<div style="text-align:left; margin-left: 5px;"><button class="btn wbtn btn-xs" ng-click="grid.appScope.$parent.Delete(row)">Hapus</button></div>',
                    enableCellEdit: false
                }
            ]

        };

        $scope.gridOptions.onRegisterApi = function(gridApi) {
            // console.log('gridApi',gridApi);
            $scope.gridApi = gridApi;

            gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {
                // $scope.mData.TotalAmount = "";
                // $scope.mData.DPAmount = "";
                $scope.mData.TotalAmount = parseFloat(Math.round($scope.mData.TotalAmount));
                // $scope.mData.TotalAmount = parseInt($scope.mData.TotalAmount.toFixed(2));
                //$scope.mData.TotalCost = parseFloat($scope.mData.TotalCost);
                $scope.mData.TotalCost = parseFloat(Math.round($scope.mData.TotalCost));
                // $scope.mData.TotalCost = parseInt($scope.mData.TotalCost.toFixed(2));
                $scope.mData.DPAmount = parseFloat($scope.mData.DPAmount);
                // $scope.mData.TotalAmount = parseFloat($scope.mData.TotalAmount).toFixed(2);
                // $scope.mData.DPAmount = parseFloat($scope.mData.DPAmount).toFixed(2);
                console.log('rowEntity', rowEntity);
                console.log('colDef', colDef);
                console.log('newValue', newValue);
                console.log('oldValue', oldValue);

                if ($scope.isEdit == true) {
                    console.log("rowEntity.CreatedDate=>",rowEntity.CreatedDate);
                    rowEntity.LastModifiedDate = $scope.localeDate(new Date());
                    rowEntity.LastModifiedUserId = $scope.UserId;
                }

                var vTotalAmount = 0;
                var vDPAmount = 0;
                vTotalAmount = parseFloat(Math.round(vTotalAmount));
                // vTotalAmount = parseInt(vTotalAmount.toFixed(2));
                vDPAmount = parseFloat(vDPAmount);
                ASPricingEngine.getPPN(1, 4).then(function (res) {
                    vVATPercent = res.data
                });
                for (var i = 0; i < $scope.gridOptions.data.length; i++) {
                    console.log("i => ", i);
                    $scope.gridOptions.data[i].TotalAmount = parseFloat(Math.round($scope.gridOptions.data[i].TotalAmount));
                    $scope.gridOptions.data[i].DPAmount = parseFloat($scope.gridOptions.data[i].DPAmount);
                    $scope.gridOptions.data[i].DPAmount_Temp = parseFloat($scope.gridOptions.data[i].DPAmount_Temp);
                    console.log("$scope.gridOptions.data[i].TotalAmount => ", $scope.gridOptions.data[i].TotalAmount);
                    console.log("$scope.gridOptions.data[i].DPAmount => ", $scope.gridOptions.data[i].DPAmount);
                    // jika ada perubahan
                    if (rowEntity.PartsCode == $scope.gridOptions.data[i].PartsCode) { // colDef.DPJumlah = newValue;
                        console.log("row diubah");
                        // start perhitungan
                        rowEntity.DiscountSpecialAmount =
                            (rowEntity.DiscountSpecialPercent *
                                (rowEntity.QtySO * rowEntity.UnitPrice)) / 100; // Diskon Spesial

                        rowEntity.DiscountGroupMaterialAmount =
                            (rowEntity.DiscountGroupMaterialPercent *
                                (rowEntity.QtySO * rowEntity.UnitPrice)) / 100; // Diskon Grup Material

                        rowEntity.DiscountMaterialAmount =
                            (rowEntity.DiscountMaterialPercent *
                                (rowEntity.QtySO * rowEntity.UnitPrice)) / 100; // Diskon Material Amount

                        var totalDiskon = 0;
                        var totalDiskon = rowEntity.DiscountSpecialAmount + rowEntity.DiscountGroupMaterialAmount + rowEntity.DiscountMaterialAmount;

                        //rowEntity.TotalCost = rowEntity.QtySO * rowEntity.UnitCost;
                        // rowEntity.VATAmount = (rowEntity.VATAmount != null || rowEntity.VATAmount != undefined ? rowEntity.VATAmount : 0);
                        // rowEntity.VATAmount = ((rowEntity.QtySO * rowEntity.UnitPrice) - totalDiskon) * 0.1;
                        rowEntity.VATAmount = ASPricingEngine.calculate({
                            InputPrice: (rowEntity.QtySO * rowEntity.UnitPrice) - totalDiskon,
                            // Discount: 0,
                            // Qty: 0,
                            Tipe: 32, //32:PPN Satuan Setelah Diskon 
                            PPNPercentage: vVATPercent,
                        });
                        // rowEntity.TotalAmount = Math.round((rowEntity.TotalAmount != null || rowEntity.TotalAmount != undefined ? rowEntity.TotalAmount : 0));
                        rowEntity.TotalAmount = Math.round(((rowEntity.QtySO * rowEntity.UnitPrice) - (totalDiskon)) + rowEntity.VATAmount);
                        // rowEntity.TotalAmount = parseInt((rowEntity.TotalAmount != null || rowEntity.TotalAmount != undefined ? rowEntity.TotalAmount : 0).toFixed(2));
                        // rowEntity.TotalAmount = parseInt((((rowEntity.QtySO * rowEntity.UnitPrice) - (totalDiskon)) + rowEntity.VATAmount).toFixed(2));
                        rowEntity.TotalCost = Math.round(rowEntity.TotalAmount);
                        //rowEntity.TotalCost = parseInt(rowEntity.TotalAmount.toFixed(2));

                        console.log('rowEntity ==> ', rowEntity);
                        console.log('rowEntity.TotalAmount => ', rowEntity.TotalAmount);
                        //end perhitungan

                        // validasi QtySO
                        if (rowEntity.QtySO != $scope.gridOptions.data[i].QtySO) {
                            console.log("Qty SO diubah");
                            $scope.gridOptions.data[i].TotalAmount =
                                newValue *
                                ($scope.gridOptions.data[i].UnitPrice -
                                    (gridOptions.data[i].DPAmount +
                                        $scope.gridOptions.data[i].DiscountSpecialAmount +
                                        $scope.gridOptions.data[i].DiscountGroupMaterialAmount +
                                        $scope.gridOptions.data[i].DiscountMaterialAmount));

                            $scope.gridOptions.data[i].TotalCost = Math.round($scope.gridOptions.data[i].TotalAmount);
                        }

                        if (rowEntity.DiscountSpecialPercent != $scope.gridOptions.data[i].DiscountSpecialPercent) {
                            console.log("rowEntity.DiscountSpecialPercent => ", rowEntity.DiscountSpecialPercent);
                            console.log("$scope.gridOptions.data[i].DiscountSpecialPercent => ", $scope.gridOptions.data[i].DiscountSpecialPercent);
                            $scope.gridOptions.data[i].DiscountSpecialAmount =
                                (($scope.gridOptions.data[i].UnitPrice * newValue) / 100);
                            $scope.gridOptions.data[i].TotalAmount =
                                $scope.gridOptions.data[i].QtySO *
                                ($scope.gridOptions.data[i].UnitPrice -
                                    ($scope.gridOptions.data[i].DPAmount +
                                        $scope.gridOptions.data[i].DiscountSpecialAmount +
                                        $scope.gridOptions.data[i].DiscountGroupMaterialAmount +
                                        $scope.gridOptions.data[i].DiscountMaterialAmount));
                            $scope.gridOptions.data[i].TotalAmount = Math.round($scope.gridOptions.data[i].TotalAmount );
                            $scope.gridOptions.data[i].TotalCost = Math.round($scope.gridOptions.data[i].TotalAmount);
                        }
                        console.log('$scope.gridOptions.data[i] =>', $scope.gridOptions.data[i]);
                        console.log('$scope.gridOptions.data[i].DPLunas => ', $scope.gridOptions.data[i].DPLunas);
                        console.log('$scope.gridOptions.data[i].DPAmount => ', $scope.gridOptions.data[i].DPAmount);
                        console.log('$scope.gridOptions.data[i].DPPercent => ', $scope.gridOptions.data[i].DPPercent);
                    }
                    vTotalAmount = vTotalAmount + $scope.gridOptions.data[i].TotalAmount;
                    vDPAmount = vDPAmount + $scope.gridOptions.data[i].DPAmount;
                    //$scope.mData.DPAmount = parseFloat($scope.mData.vDPAmount).toFixed(2);
                    //console.log('bawah: ');
                    console.log('vTotalAmount =>', vTotalAmount);
                    console.log('vDPAmount =>', vDPAmount);
                }
                $scope.mData.TotalAmount = Math.round(vTotalAmount);
                // $scope.mData.TotalAmount = parseInt(vTotalAmount.toFixed(2));
                console.log('vTotalAmount =>',  $scope.mData.TotalAmount );
                //$scope.mData.TotalCost = vTotalAmount;
                $scope.mData.DPAmount = vDPAmount;
            });

            gridApi.selection.on.rowSelectionChanged($scope,
                function(row) {

                    $scope.selectedRow = row.entity;

                });
            if (gridApi.selection.selectRow && ($scope.gridOptions.data[0] !== undefined || $scope.gridOptions.data[0] !== null)) {
                gridApi.selection.selectRow($scope.gridOptions.data[0]);
            }
        }; // end of $scope.gridOptions.onRegisterApi


        $scope.Add = function() {
            console.log("add = ", $scope.gridOptionsLangsung.data.length);
            var n = $scope.gridOptionsLangsung.data.length + 1;
            console.log("add = ", n);
            $scope.gridOptionsLangsung.data.push({
                "SalesOrderItemId": "",
                "SalesOrderId": "",
                "PartId": "",
                "PartName": "",
                "QtySO": "",
                "UomId": "",
                "UnitPrice": "",
                "DiscountMaterialPercent": "",
                "DiscountMaterialAmount": "",
                "DiscountGroupMaterialPercent": "",
                "DiscountGroupMaterialAmount": "",
                "DiscountSpecialPercent": "",
                "DiscountSpecialAmount": "",
                "VATAmount": "",
                "TotalAmount": "",
                "DPPercent": "",
                "DPAmount": "",
                "MaterialRequestStatusId": "",
                "SalesOrderStatusId": "",
                "CreatedDate": $scope.localeDate(new Date()),
                "CreatedUserId": $scope.UserId
            });
            // console.log("$scope.gridOptions.data = ", $scope.gridOptions.data[0]);
        }

        $scope.SearchMaterial = function (PartsCode) {
                // added by sss on 2017-12-18
                // var checkFreeze = [
                //     {
                //         "PartId": 1044,
                //         "PartsCode": "09150BZ040"
                //     }
                // ];
                console.log("Partscode",PartsCode);
                console.log("$scope.gridOptionsLangsung",$scope.gridOptionsLangsung);
                var flagPartsCode = 0;
                var tempQtySO = 0;
                var tempDP = 0;
                var tempRow = 0;
                for(var i in $scope.gridOptionsLangsung.data){
                    if($scope.gridOptionsLangsung.data[i].PartsCode == PartsCode){
                        flagPartsCode ++;
                    }
                }
				$scope.isApproved = false;
                for(var i in $scope.gridOptionsLangsung.data){
                    console.log("$scope.gridOptionsLangsung.data[i].PartsCode==>",$scope.gridOptionsLangsung.data[i].PartsCode);
                    if($scope.gridOptionsLangsung.data[i].PartsCode == PartsCode){
                        // tempQtySO = $scope.gridOptionsLangsung.data[i].QtySO;
                        console.log("$scope.gridOptionsLangsung.data[i].QtySO",$scope.gridOptionsLangsung.data[i].QtySO);
                        console.log("tempQtySO1",tempQtySO);
                        tempDP = $scope.gridOptionsLangsung.data[i].DPAmount;
                        tempRow = i;
                        break;
                    }else{
                        tempRow = i;
                    }
                }
                console.log("tesss",flagPartsCode);
                console.log("tempQtySO",tempQtySO);
                console.log("tempRow",tempRow);
                console.log("$scope.mData=>",$scope.mData);
                console.log("$scope.materialtypes=>",$scope.materialtypes);
                console.log("$scope.materialtypes=>",$scope.servicetypes);

                if(flagPartsCode <= 1){
                    PartsSalesOrder.getPartsFreeze(PartsCode).then(function(res){
                        var checkFreeze = res.data.Result;
                        if (typeof _.find(checkFreeze, { "PartsCode": PartsCode }) !== 'undefined' ) {
                            bsNotify.show({
                                size: 'small',
                                type: 'danger',
                                title: "Material "+PartsCode+" sedang dalam keadaan freeze",
                            });
                            var toDel = _.find($scope.gridOptions.data, { "PartsCode": PartsCode });
                            var index = $scope.gridOptions.data.indexOf(toDel);
                            var index2 = $scope.gridOptionsLangsung.data.indexOf(toDel);

                            $scope.gridOptions.data.splice(index, 1);
                            $scope.gridOptionsLangsung.data.splice(index2, 1);
                            return;
                        }
                        var param_warehouse = {};
                        param_warehouse = {
                            OutletId :$scope.user.OrgId,
                            MaterialTypeId:$scope.mData.MaterialTypeId,
                            ServiceTypeId:$scope.mData.ServiceTypeId
                        }
                        PartsGlobal.getMyWarehouse(param_warehouse).then(function (res) {
                            var loginData = res.data;
                            console.log("loginData", loginData);
                            $scope.mData.OutletId = $scope.user.OrgId;
                            $scope.mData.WarehouseId = loginData.Result[0].WarehouseId;
                            $scope.mData.WarehouseName = loginData.Result[0].WarehouseName.split(" ").pop();

                            var outlet = $scope.mData.OutletId;

                            if (PartsCode.length > 32) {
                                PartsGlobal.showAlert({
                                    title: "Sales Order",
                                    content: "Karakter telalu banyak maksimal 32",
                                    type: 'warning'
                                });
                                console.log("Coba sini deh dib =>",PartsCode.length)
                                return -1;
                            }

                            PartsSalesOrder.getDetailByMaterialNo(PartsCode, outlet).then(function (res) {
                                var gridData = res.data; //.Result;
                                console.log('gridData isinya =>> ', gridData);


                                if(gridData.length == 0){
                                    bsNotify.show({
                                        size: 'small',
                                        type: 'notif',
                                        title: "Kode Material '"+PartsCode+"' Tidak Ditemukan",
                                    });
                                };

                                if(gridData[0].ShelfName == null)
                                {
                                    PartsGlobal.showAlert({
                                        title: "Sales Order",
                                        content: "Mohon Lengkapi Data Rak Terlebih Dahulu. No Material '"+PartsCode+"'",
                                        type: 'warning'
                                    });
                                    console.log("Coba sini deh =>",gridData[0].ShelfName)
                                    return -1;
                                }

                              
                                //end if
                                // gridData.DiscountSpecialPercent = gridData.DiscountSpecialPercent != null || gridData.DiscountSpecialPercent != undefined ? gridData.DiscountSpecialPercent : 0;
                                // gridData.DiscountGroupMaterialAmount = gridData.DiscountGroupMaterialAmount != null || gridData.DiscountGroupMaterialAmount != undefined ? gridData.DiscountGroupMaterialAmount : 0;
                                // gridData[0].VATAmount = 0;

                                //catatan: totalamount di grid sudah disuplai(hasil kalkulasi) dari backend/sp
                                // gridData[0].QtySO = (gridData[0].QtySO != null || gridData[0].QtySO != undefined ? gridData[0].QtySO : 0);
                                gridData[0].DPAmount = gridData[0].SumDPAmount;
                                gridData[0].QtySO = tempQtySO;
                                if(gridData[0].QtySO == 0){
                                    gridData[0].VATAmount = 0
                                    //gridData[0].DiscountSpecialPercent = 0;
                                    // gridData[0].DiscountMaterialPercent = 0;
                                    // gridData[0].DiscountMaterialAmount = 0;
                                    //gridData[0].DiscountGroupMaterialAmount = 0;
                                    gridData[0].NilaiDPAmount = angular.copy(gridData[0].DPAmount);
                                    //gridData[0].DPAmount = 0;
                                    if(gridData[0].NilaiDPAmount != 0){
                                        gridData[0].DPAmount = gridData[0].QtySO * gridData[0].NilaiDPAmount;
                                        console.log("gridData[0].DPAmount1", gridData[0].DPAmount);
                                    } else{
                                        gridData[0].DPAmount = 0;
                                        console.log("gridData[0].DPAmount2", gridData[0].DPAmount);
                                    }
                                    gridData[0].TotalCost = 0;
                                    gridData[0].TotalAmount = 0;
                                }
                                else
                                {
                                    gridData.DiscountSpecialPercent = gridData.DiscountSpecialPercent != null || gridData.DiscountSpecialPercent != undefined ? gridData.DiscountSpecialPercent : 0;
                                    gridData.DiscountGroupMaterialAmount = gridData.DiscountGroupMaterialAmount != null || gridData.DiscountGroupMaterialAmount != undefined ? gridData.DiscountGroupMaterialAmount : 0;
                                    gridData[0].VATAmount = 0;
                                    gridData[0].TotalCost = (gridData[0].TotalCost != null || gridData[0].TotalCost != undefined ? gridData[0].TotalCost : 0);
                                    gridData[0].TotalAmount = (gridData[0].TotalAmount != null || gridData[0].TotalAmount != undefined ?  Math.round(gridData[0].TotalAmount) : 0);
                                    //gridData[0].TotalCost = gridData[0].QtySO * gridData[0].UnitCost;
                                    gridData[0].TotalCost =  Math.round(gridData[0].TotalAmount);
                                    // gridData[0].NilaiDPAmount = gridData[0].QtySO * gridData[0].DPAmount; // variabel untuk penjumlahan DPAMount, aftereditcell
                                    console.log("gridData[0].DPAmount3", gridData[0].DPAmount);
                                    gridData[0].NilaiDPAmount = angular.copy(gridData[0].DPAmount);
                                    //gridData[0].DPAmount = 0;
                                    if(gridData[0].NilaiDPAmount != 0){
                                        gridData[0].DPAmount = gridData[0].QtySO * gridData[0].NilaiDPAmount;
                                        console.log("gridData[0].DPAmount1", gridData[0].DPAmount);
                                    } else{
                                        gridData[0].DPAmount = 0;
                                        console.log("gridData[0].DPAmount2", gridData[0].DPAmount);
                                    }
                                }
                                // gridData[0].TotalCost = (gridData[0].TotalCost != null || gridData[0].TotalCost != undefined ? gridData[0].TotalCost : 0);
                                // gridData[0].TotalAmount = (gridData[0].TotalAmount != null || gridData[0].TotalAmount != undefined ? gridData[0].TotalAmount : 0);
                                // //gridData[0].TotalCost = gridData[0].QtySO * gridData[0].UnitCost;
                                // gridData[0].TotalCost = gridData[0].TotalAmount;
                                // gridData[0].NilaiDPAmount = gridData[0].DPAmount; // variabel untuk penjumlahan DPAMount, aftereditcell
                                // $scope.gridOptionsLangsung.data[$scope.gridOptionsLangsung.data.length - 1] = gridData[0];
                                console.log("tempRow==>>",tempRow);
                                // $scope.gridOptionsLangsung.data[tempRow] = gridData[0];
                                $scope.gridOptionsLangsung.data[tempRow].PartId = gridData[0].PartId;
                                $scope.gridOptionsLangsung.data[tempRow].PartName = gridData[0].PartName;
                                $scope.gridOptionsLangsung.data[tempRow].UomId = gridData[0].UomId;
                                $scope.gridOptionsLangsung.data[tempRow].Satuan = gridData[0].Satuan;
                                $scope.gridOptionsLangsung.data[tempRow].UnitPrice = gridData[0].UnitPrice;
                                $scope.gridOptionsLangsung.data[tempRow].DPAmount = gridData[0].DPAmount;
                                $scope.gridOptionsLangsung.data[tempRow].QtySO = gridData[0].QtySO;
                                $scope.gridOptionsLangsung.data[tempRow].QtyFree = gridData[0].QtyFree;
                                $scope.gridOptionsLangsung.data[tempRow].VATAmount = gridData[0].VATAmount;
                                $scope.gridOptionsLangsung.data[tempRow].DiscountSpecialPercent = gridData[0].DiscountSpecialPercent;
                                $scope.gridOptionsLangsung.data[tempRow].DiscountSpecialAmount = 0; //gridData[0].DiscountSpecialAmount;
                                $scope.gridOptionsLangsung.data[tempRow].DiscountMaterialPercent = gridData[0].DiscountMaterialPercent;
                                $scope.gridOptionsLangsung.data[tempRow].DiscountMaterialAmount = 0; //gridData[0].DiscountMaterialAmount;
                                $scope.gridOptionsLangsung.data[tempRow].DiscountGroupMaterialPercent = gridData[0].DiscountGroupMaterialPercent;
                                $scope.gridOptionsLangsung.data[tempRow].DiscountGroupMaterialAmount = 0; //gridData[0].DiscountGroupMaterialAmount;
                                $scope.gridOptionsLangsung.data[tempRow].NilaiDPAmount = gridData[0].NilaiDPAmount;
                                $scope.gridOptionsLangsung.data[tempRow].TotalCost = gridData[0].TotalCost;
                                $scope.gridOptionsLangsung.data[tempRow].TotalAmount = gridData[0].TotalAmount;
                                $scope.gridOptionsLangsung.data[tempRow].ShelfId = gridData[0].ShelfId;
                                $scope.gridOptionsLangsung.data[tempRow].ShelfName = gridData[0].ShelfName;
                                $scope.gridOptionsLangsung.data[tempRow].Price = gridData[0].Price;
                                $scope.gridOptionsLangsung.data[tempRow].Discount = gridData[0].DiscountSpecialPercent +
                                    gridData[0].DiscountMaterialPercent + gridData[0].DiscountGroupMaterialPercent;
                                $scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
                                console.log('gridData[0] = ', gridData[0]);
                                console.log('$scope.mData.GridDetail = ', $scope.mData.GridDetail);

                                // start calculation total amount header
                                //console.log('gridApi.grid.columns[11].getAggregationValue() =', gridApi.grid.columns[11].getAggregationValue());
                                //console.log('$scope.gridOptionsLangsung.onRegisterApi =', $scope.gridOptionsLangsung.onRegisterApi);
                                // $scope.gridOptionsLangsung.onRegisterApi = function (gridApi) {
                                //   $scope.gridApi = gridApi;
                                //   console.log('gridApi', $scope.gridApi);
                                //   console.log('gridApi.grid.columns[11].getAggregationValue() =', gridApi.grid.columns[11].getAggregationValue());
                                // };
                                // console.log('gridApi', $scope.gridApi);
                                // console.log('$scope.gridApi.grid.columns[11].aggregationValue =', $scope.gridApi.grid.columns[11].getAggregationValue());
                            },
                            function (err) {
                                console.log("err=>", err);
                            }); // end - PartsSalesOrder.getDetailByMaterialNo
                        },
                        function (err) {
                            console.log("err=>", err);
                        }); // end - PartsGlobal.getMyWarehouse
                    });
                }else{
                  bsNotify.show({
                    size: 'small',
                    type: 'notif',
                    title: "Data material sudah ada",
                  });
                }
                //

            } // end - $scope.SearchMaterial

        // angular.forEach($scope.GridDetail,function(row){
        //   row.getTotalCost = function(){
        //     console.log('UC = ',this.UnitCost);
        //     console.log('QtySO = ',this.QtySO);
        //     return this.UnitCost + this.QtySO;
        //   }
        // });

        $scope.gridOptionsLangsung = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableFiltering: true,
            //selectedItems: console.log($scope.mySelections),
            enableSelectAll: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 10,
            showColumnFooter: true,
            showGridFooter: false,
            // onRegisterApi: function(gridApi) {
            //   $scope.gridApi = gridApi;
            // },

            columnDefs: [{
                    name: 'SalesOrderItemId',
                    field: 'SalesOrderItemId',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'SalesOrderId',
                    field: 'SalesOrderId',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'PartId',
                    field: 'PartId',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'RetailPriceDP',
                    field: 'RetailPriceDP',
                    width: '7%',
                    visible: false,
                    aggregationType: uiGridConstants.aggregationTypes.sum
                },
                {
                    name: 'No Material',
                    displayName: 'No. Material',
                    field: 'PartsCode',
                    width: '12%',
                    headerCellClass: "middle",
                    enableCellEditOnFocus: true,
                    // cellTemplate:
                    // <div class="ui-grid-cell-contents"><div class="input-group">\
                    // <input ng-model="mData.PartsCode" ng-value="row.entity.PartsCode" required>\
                    //   <button class="btn btn-default btn-xs" type="button" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)"><span class="glyphicon glyphicon-search"></span></button>\
                    // </div></div>',
                    cellTemplate: '<div class="ui-grid-cell-contents"><div class="input-group">\
                      <input  ng-model="mData.PartsCode" ng-value="row.entity.PartsCode" required>\
                      <label class="input-group-btn">\
                          <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
                              <i class="fa fa-search fa-1"></i>\
                          </span>\
                      </label>\
                  </div></div>'
                },
                {
                    name: 'Nama Material',
                    displayName: 'Nama Material',
                    field: 'PartName',
                    width: '18%',
                    headerCellClass: "middle",
                    enableCellEdit: true, // untuk special order
                    cellClass: 'Editable',
                    editableCellTemplate: '<input type="text" ng-maxlength="50" maxlength="50" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                    cellTooltip: 'Selain untuk Special Order, Nama Material tidak diinput manual'
                },
                {
                    name: 'Qty Free',
                    displayName: 'Qty Free',
                    field: 'QtyFree',
                    width: '8%',
                    headerCellClass: "middle",
                    enableCellEdit: false,
                    // aggregationHideLabel: true,
                    // aggregationType: uiGridConstants.aggregationTypes.sum,
                    cellClass: 'notEditable',
                    cellTooltip: 'Disabled'
                },
                {
                    name: 'Qty SO',
                    displayName: 'Qty SO',
                    field: 'QtySO',
                    width: '8%',
                    type: 'number',
                    cellFilter: 'number',
                    headerCellClass: "middle",
                    aggregationHideLabel: true,
                    editableCellTemplate: '<input type="number" min="1" step="1" max="99999" ng-keyup="grid.appScope.CheckIsNumber($event, MODEL_COL_FIELD)" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    cellClass: 'editable',
                    cellTooltip: 'Input Sales Order Quantity'
                },
                {
                    name: 'UomId',
                    displayName: 'UomId',
                    field: 'UomId',
                    width: '6%',
                    visible: false
                },
                {
                    name: 'Satuan',
                    displayName: 'Satuan',
                    // field: 'satuanName',
                    field: 'Satuan',
                    width: '6%',
                    headerCellClass: "middle",
                    enableCellEdit: false,
                    cellClass: 'notEditable',
                    cellTooltip: 'Disabled'
                },
                {
                    name: 'Harga Satuan',
                    displayName: 'Harga Satuan',
                    // field: 'UnitPrice',
                    field: 'UnitPrice',
                    width: '10%',
                    headerCellClass: "middle",
                    enableCellEdit: false,
                    cellFilter: 'number',
                    cellClass: 'notEditable',
                    cellTooltip: 'Disabled',
                    // aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationType: function() {
                        if(!$scope.gridApi)
                            return 0;
                        var sum = 0;

                        //if it is filtered/sorted you want the filtered/sorted rows only)
                        var visibleRows = $scope.gridApi.core.getVisibleRows($scope.gridApi.grid);
                        for (var i = 0; i < visibleRows.length; i++) {
                              //you have to parse just in case the data is in string otherwise it will concatenate
                              sum += visibleRows[i].entity.QtySO * visibleRows[i].entity.UnitPrice;
                          }

                        console.log('VAT column ==> ',sum);  
                        return parseInt(Math.round(sum));
                      },
                    aggregationHideLabel: true,
                    footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'                    
                },
                {
                    name: 'Unit Cost',
                    displayName: 'Unit Cost',
                    field: 'UnitCost',
                    width: '10%',
                    headerCellClass: "middle",
                    enableCellEdit: false,
                    visible: false
                },
                {
                    name: 'Total Cost',
                    displayName: 'Total Cost',
                    field: 'TotalCost',
                    //field: 'getTotalCost()',
                    width: '10%',
                    headerCellClass: "middle",
                    cellFilter: 'number',
                    enableCellEdit: false,
                    visible: false
                },
                {
                    name: 'DM Persen',
                    displayName: '%',
                    field: 'DiscountMaterialPercent',
                    width: '5%',
                    enableCellEdit: false,
                    cellClass: 'notEditable',
                    cellTooltip: 'Disabled',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:240%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Material</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'DM Jumlah',
                    displayName: 'Jumlah',
                    field: 'DiscountMaterialAmount',
                    width: '7%',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>",
                    enableCellEdit: false,
                    cellTooltip: 'Disabled',
                    cellFilter: 'number',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationHideLabel: true,
                    cellClass: 'notEditable',
                    footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
                },
                {
                    name: 'DGM Persen',
                    displayName: '%',
                    field: 'DiscountGroupMaterialPercent',
                    width: '5%',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:240%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Group Material</td></tr></tbody></table></div>%</div>",
                    enableCellEdit: false,
                    cellClass: 'notEditable',
                    cellTooltip: 'Disabled'
                },
                {
                    name: 'DGM Jumlah',
                    displayName: 'Jumlah',
                    field: 'DiscountGroupMaterialAmount',
                    width: '7%',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>",
                    enableCellEdit: false,
                    cellTooltip: 'Disabled',
                    cellFilter: 'number',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationHideLabel: true,
                    cellClass: 'notEditable',
                    footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
                },
                {
                    name: 'DS Persen',
                    displayName: '%',
                    field: 'DiscountSpecialPercent',
                    width: '5%',
                    enableCellEdit: true,
                    cellClass: 'editable',
                    cellTooltip: 'Discount Special',
                    editableCellTemplate: '<input type="number" placeholder="Max 100" max="100" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:240%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Spesial</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'DS Jumlah',
                    displayName: 'Jumlah',
                    field: 'DiscountSpecialAmount',
                    width: '7%',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>",
                    enableCellEdit: false,
                    cellClass: 'notEditable',
                    cellFilter: 'number',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationHideLabel: true,
                    footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
                },
                {
                    name: 'ppn',
                    displayName: 'PPN',
                    field: 'VATAmount',
                    width: '10%',
                    headerCellClass: "middle",
                    enableCellEdit: false,
                    cellClass: 'notEditable',
                    cellFilter: 'number',
                    // aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationType: function() {
                        if(!$scope.gridApi)
                            return 0;
                        var sum = 0;

                        console.log('$scope.gridApi',$scope.gridApi);
  
                        //if it is filtered/sorted you want the filtered/sorted rows only)
                        var visibleRows = $scope.gridApi.core.getVisibleRows($scope.gridApi.grid);
                        console.log('visibleRows==>',visibleRows);
                        for (var i = 0; i < visibleRows.length; i++) {
                              //you have to parse just in case the data is in string otherwise it will concatenate
                              sum += parseFloat(visibleRows[i].entity.VATAmount);
                              console.log('visibleRows==>',visibleRows[i].entity.VATAmount);
                          }

                        console.log('VAT column ==> ',sum);
						console.log('$scope.isApproved at new : ', $scope.isApproved);
                        return parseInt(Math.floor(sum));
                    },
                    aggregationHideLabel: true,
                    footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
                },
                {
                    name: 'harga Total',
                    displayName: 'Harga Total',
                    field: 'TotalAmount',
                    width: '12%',
                    headerCellClass: "middle",
                    enableCellEdit: false,
                    cellClass: 'notEditable',
                    cellFilter: 'number',
                    // aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationType: function() {
                        if(!$scope.gridApi)
                            return 0;
                        var sum = 0;

                        console.log('$scope.gridApi',$scope.gridApi);
  
                        //if it is filtered/sorted you want the filtered/sorted rows only)
                        var visibleRows = $scope.gridApi.core.getVisibleRows($scope.gridApi.grid);
                        console.log('visibleRows==>',visibleRows);
                        for (var i = 0; i < visibleRows.length; i++) {
                              //you have to parse just in case the data is in string otherwise it will concatenate
                              sum += visibleRows[i].entity.TotalAmount*10;
                              console.log('visibleRows==>',visibleRows[i].entity.TotalAmount);
                          }

                        console.log('TotalAmount column ==> ',sum);  
                        return parseInt(Math.floor(sum/10));
                      },
                    aggregationHideLabel: true,
                    footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
                },
                {
                    name: 'DPJumlah',
                    displayName: 'Jumlah',
                    field: 'DPAmount',
                    width: '10%',
                    enableCellEdit: true,
                    cellClass: 'editable',
                    cellFilter: 'number',
                    // aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationType: function() {
                        if(!$scope.gridApi)
                            return 0;
                        var sum = 0;

                        console.log('$scope.gridApi',$scope.gridApi);
  
                        //if it is filtered/sorted you want the filtered/sorted rows only)
                        var visibleRows = $scope.gridApi.core.getVisibleRows($scope.gridApi.grid);
                        console.log('visibleRows==>',visibleRows);
                        for (var i = 0; i < visibleRows.length; i++) {
                              //you have to parse just in case the data is in string otherwise it will concatenate
                              sum += visibleRows[i].entity.DPAmount;
                              console.log('visibleRows==>',visibleRows[i].entity.DPAmount);
                          }

                        console.log('DPAmount column ==> ',sum);  
                        return parseInt(Math.floor(sum));
                      },
                    aggregationHideLabel: true,
                    editableCellTemplate: '<input type="number" max="999999999999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:300%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2' style='text-align:left;padding-left:10px;'>DP</td></tr></tbody></table></div>Jumlah</div>",
                    footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
                },

                // {
                //   name: 'DPLunas',
                //   displayName: 'Lunas (DP)',
                //   field: 'DPLunas',
                //   width: '8%',
                //   type: 'boolean',
                //   enableCellEdit: false,
                //   headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Lunas</div>",
                //   cellTemplate: '<input type="checkbox" ng-model="row.entity.DPLunas">'
                // },
                {
                    name: 'Lunas',
                    displayName: 'Lunas',
                    field: 'isLunas',
                    width: '5%',
                    type: 'boolean',
                    enableCellEdit: false,
                    cellClass: 'notEditable',
                    cellTemplate: '<input type="checkbox" ng-model="row.entity.isLunas" disabled>',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Lunas</div>"
                },
                {
                    name: 'DP Temp',
                    displayName: 'DP Temp',
                    field: 'DPAmount_Temp',
                    enableCellEdit: false,
                    cellFilter: 'number',
                    width: '10%',
                    headerCellClass: "middle",
                    visible: false
                },
                {
                    name: 'PartsClassId4',
                    field: 'PartsClassId4',
                    width: '7%',
                    visible: false
                },
                {
                    name: '_',
                    headerCellClass: "middle",
                    displayName: 'Action',
                    width: '10%',
                    pinnedRight: true,
                    //enableCellEdit: false,
                    // visible : true,
                    //cellTemplate: '<div style="padding-top: 5px;"><u><a href="#" ng-click="grid.appScope.Delete(row)">Hapus</a></u></div>',
                    cellTemplate: '<div style="text-align:left; margin-left: 5px;"><button class="btn wbtn btn-xs" ng-click="grid.appScope.$parent.Delete(row)">Hapus</button></div>',
                    enableCellEdit: false
                }
            ]

        };

        $scope.gridOptionsLangsung.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            // $scope.gridApi = $scope.gridOptionsLangsung;
            console.log('$scope.gridApi langsung = ',$scope.gridApi);
            console.log('$scope.gridApi langsung2 = ',$scope.gridOptionsLangsung);
            gridApi.edit.on.beginCellEdit($scope, function(rowEntity, colDef, triggerEvent) {
                console.log('===Ada===');
                console.log('rowEntity begin', rowEntity);
                console.log('colDef begin', colDef);
                console.log('triggerEvent begin', triggerEvent);
            });

            gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {
                //console.log('$scope.gridApi = ',$scope.gridApi);
                console.log('$scope.gridApi.grid = ', $scope.gridApi.grid);
                console.log('$scope.gridApi.data = ', $scope.gridApi.data);
                console.log('$scope.gridApi.data.length = ', $scope.gridApi.data);
                console.log('rowEntity = ', rowEntity);
                console.log("$scope.isEdit=>",$scope.isEdit);

                if ($scope.isEdit == true) {
                    console.log("rowEntity.CreatedDate=>",rowEntity.CreatedDate);
                    rowEntity.LastModifiedDate = $scope.localeDate(new Date());
                    rowEntity.LastModifiedUserId = $scope.UserId;
                }

                //console.log('$scope.gridApi.grid.columns[22].aggregationValue =', $scope.gridApi.grid.columns[22].getAggregationValue());
                // $scope.mData.TotalAmount = "";
                // $scope.mData.DPAmount = "";
                console.log('data[i] = ', $scope.gridOptionsLangsung.data);
                $scope.mData.TotalAmount = 0; //parseFloat($scope.mData.TotalAmount);
                $scope.mData.VATAmount = 0; //parseFloat($scope.mData.TotalAmount);
                $scope.mData.TotalCost = 0; //parseFloat($scope.mData.TotalCost);
                $scope.mData.DPAmount = 0; //parseFloat($scope.mData.DPAmount);
                $scope.roundTo = 1;

                // console.log('rowEntity.DiscountSpecialAmount', rowEntity.DiscountSpecialAmount);
                // console.log('rowEntity.DiscountSpecialPercent', rowEntity.DiscountSpecialPercent);

                // rowEntity.DiscountSpecialAmount =
                //     Math.round((rowEntity.DiscountSpecialPercent / 100 *
                //         rowEntity.UnitPrice)) * rowEntity.QtySO; // Diskon Spesial

                // rowEntity.DiscountGroupMaterialAmount =
                //     (rowEntity.DiscountGroupMaterialPercent *
                //         (rowEntity.QtySO * rowEntity.UnitPrice)) / 100; // Diskon Grup Material

                // rowEntity.DiscountMaterialAmount =
                //     (rowEntity.DiscountMaterialPercent *
                //         (rowEntity.QtySO * rowEntity.UnitPrice)) / 100; // Diskon Material Amount

                // var totalDiskon = 0;
                // var totalDiskon = rowEntity.DiscountSpecialAmount + rowEntity.DiscountGroupMaterialAmount + rowEntity.DiscountMaterialAmount;

                //rowEntity.TotalCost = rowEntity.QtySO * rowEntity.UnitCost;
                // rowEntity.VATAmount = (rowEntity.VATAmount != null || rowEntity.VATAmount != undefined ? rowEntity.VATAmount : 0);
                // rowEntity.VATAmount = ((rowEntity.QtySO * rowEntity.UnitPrice) - totalDiskon) * 0.1;
                // rowEntity.TotalAmount = (rowEntity.TotalAmount != null || rowEntity.TotalAmount != undefined ? rowEntity.TotalAmount : 0);
                // rowEntity.TotalAmount = ((rowEntity.QtySO * rowEntity.UnitPrice) - (totalDiskon)) + rowEntity.VATAmount;
                // rowEntity.TotalCost = Math.round(rowEntity.TotalAmount);

                // imron 29okt2020
                if(rowEntity.QtySO == undefined || rowEntity.QtySO == null || rowEntity.QtySO == ''){
                    rowEntity.QtySO = 0
                }
                var DiscountSpecialSatuan = Math.round((rowEntity.DiscountSpecialPercent / 100) * rowEntity.UnitPrice);
                rowEntity.DiscountSpecialAmount = (rowEntity.QtySO * DiscountSpecialSatuan);                      // Diskon Spesial

                var DiscountGroupMaterialSatuan = Math.round((rowEntity.DiscountGroupMaterialPercent / 100) * rowEntity.UnitPrice);
                rowEntity.DiscountGroupMaterialAmount = (rowEntity.QtySO * DiscountGroupMaterialSatuan);          // Diskon Grup Material

                var DiscountMaterialSatuan = Math.round((rowEntity.DiscountMaterialPercent / 100) * rowEntity.UnitPrice);
                rowEntity.DiscountMaterialAmount = (rowEntity.QtySO * DiscountMaterialSatuan);                    // Diskon Material Amount

                console.log("rowEntity.Discount1==>",rowEntity.Discount);
                rowEntity.Discount = rowEntity.DiscountGroupMaterialPercent + rowEntity.DiscountMaterialPercent + rowEntity.DiscountSpecialPercent;
                console.log("rowEntity.Discount2==>",rowEntity.Discount);

                console.log("colDef",colDef);
                if (colDef.field == "DiscountSpecialPercent") {
                    if (rowEntity.QtySO>0 && rowEntity.QtySO>=rowEntity.QtyFree && rowEntity.QtyFree>0) {
                        rowEntity.DPAmount =
                            Math.round(rowEntity.NilaiDPAmount * (rowEntity.QtySO - rowEntity.QtyFree) *
                            (1 - (rowEntity.DiscountSpecialPercent/100)));
                    }
                }

            //console.log("$scope.gridOptionsLangsung.data[i].DiscountSpecialPercent",$scope.gridOptionsLangsung.data[i].DiscountSpecialPercent);
                // var totalDiskon = 0;
                // var totalDiskon = rowEntity.DiscountSpecialAmount + rowEntity.DiscountGroupMaterialAmount + rowEntity.DiscountMaterialAmount;

                // var VATSatuan = (rowEntity.UnitPrice - DiscountSpecialSatuan - DiscountGroupMaterialSatuan - DiscountMaterialSatuan) * 0.1;
                // rowEntity.VATAmount = rowEntity.QtySO * Math.round(VATSatuan);
                // rowEntity.TotalAmount = Math.round((rowEntity.QtySO * rowEntity.UnitPrice) - totalDiskon + rowEntity.VATAmount);
                // rowEntity.TotalCost = Math.round(rowEntity.TotalAmount);

                // var DPPAmount = (rowEntity.QtySO * rowEntity.UnitPrice) - totalDiskon;
                // var VATAmount = DPPAmount * 0.1;
                // rowEntity.VATAmount = VATAmount.toFixed(1);
                // rowEntity.TotalAmount = DPPAmount + VATAmount;
                // rowEntity.TotalCost = Math.round(rowEntity.TotalAmount);

                // if (rowEntity.TotalAmount >= rowEntity.NumVal1) {
                //     rowEntity.DPAmount = Math.round((rowEntity.NumVal2 / 100) * rowEntity.TotalAmount);
                // } else { rowEntity.DPAmount = 0; }

                // console.log('totalDiskon', totalDiskon);
                // console.log('DPPAmount', DPPAmount);
                // console.log('VATAmount.toFixed(1)', VATAmount.toFixed(1));
                // console.log('rowEntity.VATAmount', rowEntity.VATAmount);
                // console.log('TotalAmount', rowEntity.TotalAmount);

                // console.log('rowEntity after', rowEntity);
                // console.log('colDef after', colDef);
                // console.log('newValue after', newValue);
                // console.log('oldValue after', oldValue);
                // //console.log('row after', row);
                // console.log('22 atas =>>', $scope.gridApi.grid.columns[22].getAggregationValue());
                // console.log('22 atas =>>', $scope.gridApi.grid.columns[22].aggregationValue);

                // var vTotalAmount = 0; //rowEntity.TotalAmount;
                // var vVATAmount = 0; //rowEntity.TotalAmount;
                // var vTotalCost = 0; //rowEntity.TotalCost;
                // var vDPAmount = 0; //rowEntity.DPAmount;
                // for (var i = 0; i < $scope.gridOptionsLangsung.data.length; i++) {
                //     vTotalAmount = vTotalAmount + $scope.gridOptionsLangsung.data[i].TotalAmount;
                //     vVATAmount = vVATAmount + parseFloat($scope.gridOptionsLangsung.data[i].VATAmount);
                //     vTotalCost = vTotalCost + $scope.gridOptionsLangsung.data[i].TotalCost;
                //     vDPAmount = vDPAmount + $scope.gridOptionsLangsung.data[i].DPAmount;
                //     $scope.mData.TotalAmount = vTotalAmount;
                //     $scope.mData.VATAmount = vVATAmount;
                //     $scope.mData.TotalCost = vTotalCost;
                //     $scope.mData.DPAmount = vDPAmount;
                // }
                // $scope.mData.TotalAmount = Math.floor($scope.mData.TotalAmount);
                // $scope.mData.VATAmount = Math.floor($scope.mData.VATAmount);

                var vTotalAmount = 0;
                var vDPAmount = 0;
                var vVATAmount = 0;
                ASPricingEngine.getPPN(1, 4).then(function (res) {
                    vVATPercent = res.data;
                    console.log("vVATPercent1==>",vVATPercent);
                });
                console.log("vVATPercent2==>",vVATPercent);
                //vTotalAmount = parseFloat(vTotalAmount);
                //vDPAmount = parseFloat(vDPAmount);
                for (var i = 0; i < $scope.gridOptionsLangsung.data.length; i++) {
                    console.log('for');
                    // Jika QtySO diubah, QtySO * Jumlah DP
                    if (colDef.field == 'QtySO') {
                        console.log('colDef.field QtySO', colDef.field);
                        if ($scope.gridOptionsLangsung.data[i].QtyFree > 0 && $scope.gridOptionsLangsung.data[i].QtyFree >= $scope.gridOptionsLangsung.data[i].QtySO){
                            $scope.gridOptionsLangsung.data[i].DPAmount = 0;
                            //DPAmount_Temp untuk validasi
                            $scope.gridOptionsLangsung.data[i].DPAmount_Temp = $scope.gridOptionsLangsung.data[i].DPAmount;
                        } else {
                            console.log("rowEntity.DiscountSpecialPercent",rowEntity.DiscountSpecialPercent);
                            console.log("Nilai DPAmount",$scope.gridOptionsLangsung.data[i].NilaiDPAmount);
                            var iQtyFree = $scope.gridOptionsLangsung.data[i].QtyFree;
                            if (iQtyFree<0) { iQtyFree = 0; }
                            $scope.gridOptionsLangsung.data[i].DPAmount =
                                // Math.round($scope.gridOptionsLangsung.data[i].NilaiDPAmount * ($scope.gridOptionsLangsung.data[i].QtySO - $scope.gridOptionsLangsung.data[i].QtyFree) *
                                // (1 - (rowEntity.DiscountSpecialPercent/100)));
                                // parseFloat($scope.gridOptionsLangsung.data[i].NilaiDPAmount * ($scope.gridOptionsLangsung.data[i].QtySO - $scope.gridOptionsLangsung.data[i].QtyFree) *
                                parseFloat($scope.gridOptionsLangsung.data[i].NilaiDPAmount * ($scope.gridOptionsLangsung.data[i].QtySO - iQtyFree) *
                                (1 - (rowEntity.DiscountSpecialPercent/100)).toFixed($scope.roundTo));
                            //DPAmount_Temp untuk validasi
                            $scope.gridOptionsLangsung.data[i].DPAmount_Temp = $scope.gridOptionsLangsung.data[i].DPAmount;
                        }
                    }

                    if (colDef.field == 'DPAmount') {
                        console.log('colDef.field DPAmount', colDef.field);
                        if (rowEntity.DPAmount <= rowEntity.TotalAmount) {
                            $scope.gridOptionsLangsung.data[i].DPAmount = parseFloat($scope.gridOptionsLangsung.data[i].DPAmount);
                            console.log('Masuk ==> DPAmount', $scope.gridOptionsLangsung.data[i].DPAmount);
                            console.log('Masuk ==> vDPAmount', vDPAmount);
                        } else { 
                            console.log("oldValue==>",i,oldValue);
                            console.log("rowEntity.PartsCode==>",i,rowEntity.PartsCode);
                            console.log("$scope.gridOptionsLangsung.data[i].PartsCode==>",i,$scope.gridOptionsLangsung.data[i].PartsCode);
                            if (rowEntity.PartsCode == $scope.gridOptionsLangsung.data[i].PartsCode) {
                                $scope.gridOptionsLangsung.data[i].DPAmount = oldValue;
                            }
                            console.log("$scope.gridOptionsLangsung.data[i].DPAmount==>",i,$scope.gridOptionsLangsung.data[i].DPAmount);
                            bsNotify.show({
                                size: 'small',
                                type: 'warning',
                                title: "Jumlah DP",
                                content: "Jumlah DP tidak boleh lebih dari Harga Total"
                            });
                        }
                        //$scope.gridOptionsLangsung.data[i].DPAmount_Temp = $scope.gridOptionsLangsung.data[i].DPAmount;
                    }

                    if ($scope.gridOptionsLangsung.data[i].PartsCode == $scope.gridOptionsLangsung.data[i].PartsCode) {
                        console.log('Partscode sama');
						console.log('$scope.isApproved at grid search part : ', $scope.isApproved);
                        console.log('i', i);
                        if (newValue != oldValue) {

                        }
                    }

                    // perhitungan ppn per row
                    // $scope.gridOptionsLangsung.data[i].VATAmount =
                    //     parseFloat(((Math.round($scope.gridOptionsLangsung.data[i].QtySO *
                    //         $scope.gridOptionsLangsung.data[i].UnitPrice
                    //     ) - ( //$scope.gridOptionsLangsung.data[i].DPAmount +
                    //         $scope.gridOptionsLangsung.data[i].DiscountSpecialAmount +
                    //         $scope.gridOptionsLangsung.data[i].DiscountGroupMaterialAmount +
                    //         $scope.gridOptionsLangsung.data[i].DiscountMaterialAmount
                    //     )) * 0.1).toFixed($scope.roundTo));
                    var iPrice = (Math.round($scope.gridOptionsLangsung.data[i].QtySO *
                        $scope.gridOptionsLangsung.data[i].UnitPrice
                    ) - ( //$scope.gridOptionsLangsung.data[i].DPAmount +
                        $scope.gridOptionsLangsung.data[i].DiscountSpecialAmount +
                        $scope.gridOptionsLangsung.data[i].DiscountGroupMaterialAmount +
                        $scope.gridOptionsLangsung.data[i].DiscountMaterialAmount
                    ));
                    console.log("iPrice==>",iPrice);
                    $scope.gridOptionsLangsung.data[i].VATAmount = parseFloat(ASPricingEngine.calculate({
                        InputPrice: iPrice*((100+vVATPercent)/100),
                        Discount: 0,
                        // Qty: 0,
                        Tipe: 32, //32:PPN Satuan Setelah Diskon 
                        PPNPercentage: vVATPercent,
                    }).toFixed($scope.roundTo));
                    console.log("$scope.gridOptionsLangsung.data[i].VATAmount==>",$scope.gridOptionsLangsung.data[i].VATAmount);

                    // perhitungan Harga Total per row
                    $scope.gridOptionsLangsung.data[i].TotalAmount =
                        parseFloat(((Math.round($scope.gridOptionsLangsung.data[i].QtySO *
                                $scope.gridOptionsLangsung.data[i].UnitPrice
                            ) -
                            ( //$scope.gridOptionsLangsung.data[i].DPAmount +
                                $scope.gridOptionsLangsung.data[i].DiscountSpecialAmount +
                                $scope.gridOptionsLangsung.data[i].DiscountGroupMaterialAmount +
                                $scope.gridOptionsLangsung.data[i].DiscountMaterialAmount
                            )) + $scope.gridOptionsLangsung.data[i].VATAmount).toFixed($scope.roundTo));
                    //$scope.gridOptionsLangsung.data[i].TotalAmount = Math.round($scope.gridOptionsLangsung.data[i].TotalAmount);
                    $scope.gridOptionsLangsung.data[i].TotalCost = $scope.gridOptionsLangsung.data[i].TotalAmount;

                    console.log('data[i].DPAmount = ', $scope.gridOptionsLangsung.data[i].DPAmount);
                    console.log('data[i].DPAmount = ', $scope.gridOptionsLangsung.data[i].DPAmount_Temp);
                    // console.log('data[i].VATAmount = ', $scope.gridOptionsLangsung.data[i].VATAmount);
                    // console.log('data[i].TotalAmount = ', $scope.gridOptionsLangsung.data[i].TotalAmount);

                    //if header
                    if (rowEntity.PartsCode == $scope.gridOptionsLangsung.data[i].PartsCode) {
                        console.log('if PartsCode == PartsCode');
                        console.log('QtySO =', $scope.gridOptionsLangsung.data[i].QtySO);
                        console.log('i =', i);
                        //console.log('$scope.gridOptionsLangsung.data[i].QtySO', $scope.gridOptionsLangsung.data[i].QtySO);
                        //if 1
                        if (rowEntity.QtySO != $scope.gridOptionsLangsung.data[i].QtySO) {
                            console.log('Qty SO, diubah');
                            $scope.gridOptionsLangsung.data[i].TotalAmount =
                                parseFloat((newValue *
                                ($scope.gridOptionsLangsung.data[i].UnitPrice -
                                    ($scope.gridOptionsLangsung.data[i].DPAmount +
                                        $scope.gridOptionsLangsung.data[i].DiscountSpecialAmount +
                                        $scope.gridOptionsLangsung.data[i].DiscountGroupMaterialAmount +
                                        $scope.gridOptionsLangsung.data[i].DiscountMaterialAmount
                                    )
                                )).toFixed($scope.roundTo));
                            //$scope.gridOptionsLangsung.data[i].TotalAmount = Math.round($scope.gridOptionsLangsung.data[i].TotalAmount);
                            $scope.gridOptionsLangsung.data[i].TotalCost = Math.round($scope.gridOptionsLangsung.data[i].TotalAmount);
                        }
                        if (rowEntity.DiscountSpecialAmount != $scope.gridOptionsLangsung.data[i].DiscountSpecialAmount) {
                            console.log('Discount spesial amount, diubah');
                            // console.log('rowEntity.DiscountSpecialAmount = ',rowEntity.DiscountSpecialAmount);
                            // console.log('gridOptionsLangsung.data[i].DiscountSpecialAmount = ',$scope.gridOptionsLangsung.data[i].DiscountSpecialAmount);
                        }
                        console.log("rowEntity.DiscountSpecialPercent",rowEntity.DiscountSpecialPercent);
                        console.log("$scope.gridOptionsLangsung.data[i].DiscountSpecialPercent",$scope.gridOptionsLangsung.data[i].DiscountSpecialPercent);
                        //if 2
                        if (rowEntity.DiscountSpecialPercent != $scope.gridOptionsLangsung.data[i].DiscountSpecialPercent) {
                            console.log('if 2');
                            $scope.gridOptionsLangsung.data[i].DiscountSpecialAmount = (($scope.gridOptionsLangsung.data[i].UnitPrice * newValue) / 100);
                            $scope.gridOptionsLangsung.data[i].TotalAmount =
                                parseFloat(($scope.gridOptionsLangsung.data[i].QtySO *
                                ($scope.gridOptionsLangsung.data[i].UnitPrice -
                                    ($scope.gridOptionsLangsung.data[i].DPAmount +
                                        $scope.gridOptionsLangsung.data[i].DiscountSpecialAmount +
                                        $scope.gridOptionsLangsung.data[i].DiscountGroupMaterialAmount +
                                        $scope.gridOptionsLangsung.data[i].DiscountMaterialAmount
                                    )
                                )).toFixed($scope.roundTo));
                            //$scope.gridOptionsLangsung.data[i].TotalAmount =  Math.round($scope.gridOptionsLangsung.data[i].TotalAmount);
                            $scope.gridOptionsLangsung.data[i].TotalCost = $scope.gridOptionsLangsung.data[i].TotalAmount;

                            var iQtyFree = $scope.gridOptionsLangsung.data[i].QtyFree;
                            if (iQtyFree<0) { iQtyFree = 0; }
                            //Recalculate DPAmount
                            $scope.gridOptionsLangsung.data[i].DPAmount =
                            // $scope.gridOptionsLangsung.data[i].NilaiDPAmount * ($scope.gridOptionsLangsung.data[i].QtySO - $scope.gridOptionsLangsung.data[i].QtyFree) *
                            $scope.gridOptionsLangsung.data[i].NilaiDPAmount * ($scope.gridOptionsLangsung.data[i].QtySO - iQtyFree) *
                            (1 - (rowEntity.DiscountSpecialPercent/100));
                        }
                        console.log('$scope.gridOptionsLangsung.data[i] = ', $scope.gridOptionsLangsung.data[i]);
                    } // end if PartsCode == PartsCode
                    vTotalAmount = vTotalAmount + $scope.gridOptionsLangsung.data[i].TotalAmount*10; //*10 for avoid error round
                    //vDPAmount = Math.round(vDPAmount);
                    vDPAmount = vDPAmount + $scope.gridOptionsLangsung.data[i].DPAmount*10; //*10 for avoid error round
                    vVATAmount = vVATAmount + $scope.gridOptionsLangsung.data[i].VATAmount*10; //*10 for avoid error round
                    // console.log('vTotalAmount =>', vTotalAmount);
                    console.log('vDPAmount =?', vDPAmount);
                    if (vTotalAmount === 'NaN' || vDPAmount === 'NaN') {
                        vTotalAmount = 0;
                        vDPAmount = 0;
                        vVATAmount = 0;
                    } else //end if
                    if (vTotalAmount == null || vDPAmount == null) {
                        vTotalAmount = 0;
                        vDPAmount = 0;
                        vVATAmount = 0;
                    } //end if
                } // end - for
                if (vTotalAmount === 'NaN' || vDPAmount === 'NaN') {
                    vTotalAmount = 0;
                    vDPAmount = 0;
                    vVATAmount = 0;
                }
                console.log('vTotalAmount',  Math.floor(vTotalAmount));

                $scope.mData.TotalAmount = Math.floor(vTotalAmount/10);
                // $scope.mData.TotalAmount = parseInt(vTotalAmount.toFixed(2));
                //$scope.mData.TotalAmount = Math.floor($scope.mData.TotalAmount);
                $scope.mData.TotalCost = vTotalAmount/10; //back to normal after *10
                $scope.mData.DPAmount = Math.floor(vDPAmount/10); //back to normal after *10
                $scope.mData.VATAmount = Math.floor(vVATAmount/10); //back to normal after *10
                //$scope.mData.TotalAmount = $scope.gridApi.grid.columns[21].aggregationValue;
                //$scope.mData.DPAmount = $scope.gridApi.grid.columns[22].aggregationValue;
                // if ($scope.mData.TotalAmount == null || $scope.mData.TotalAmount === undefined || $scope.mData.TotalAmount === 'NaN') { $scope.mData.TotalAmount = 0 };
                console.log('$scope.mData.TotalAmount =>>', $scope.mData.TotalAmount);
                console.log('$scope.mData.DPAmount =>>', $scope.mData.DPAmount);
                console.log('$scope.mData.VATAmount =>>', $scope.mData.VATAmount);
                console.log('22 =>>', $scope.gridApi.grid.columns[22].getAggregationValue());

            }); // end - gridApi.edit.on.afterCellEdit

            //$scope.gridOptionsLangsung
            gridApi.selection.on.rowSelectionChanged($scope,
                function(row) {

                    $scope.selectedRow = row.entity;

                });

            // if
            if (gridApi.selection.selectRow &&
                ($scope.gridOptionsLangsung.data[0] !== undefined || $scope.gridOptionsLangsung.data[0] !== null)
            ) { // then
                gridApi.selection.selectRow($scope.gridOptionsLangsung.data[0]);
            }
            // if ($scope.gridOptionsLangsung.selection.selectRow && ($scope.gridOptionsLangsung.data[0] !== undefined || $scope.gridOptionsLangsung.data[0] !== null)) {
            //   $scope.gridOptionsLangsung.selection.selectRow($scope.gridOptionsLangsung.data[0]);
            // }
        };


        $scope.gridOptionsLihat = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableFiltering: true,
            //selectedItems: console.log($scope.mySelections),
            enableSelectAll: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 5,
            showColumnFooter: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApiLihat = gridApi;
            },

            columnDefs: [{
                    name: 'SalesOrderItemId',
                    field: 'SalesOrderItemId',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'SalesOrderId',
                    field: 'SalesOrderId',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'No Material',
                    displayName: 'No. Material',
                    field: 'PartsCode',
                    width: '10%',
                    headerCellClass: "middle",
                    enableCellEdit: false
                },
                {
                    name: 'Nama Material',
                    displayName: 'Nama Material',
                    field: 'PartName',
                    width: '11%',
                    headerCellClass: "middle",
                    enableCellEdit: false
                },
                {
                    name: 'Qty Free',
                    displayName: 'Qty Free',
                    field: 'QtyFree',
                    width: '5%',
                    headerCellClass: "middle",
                    enableCellEdit: false
                },
                {
                    name: 'Qty SO',
                    displayName: 'Qty SO',
                    field: 'QtySO',
                    width: '5%',
                    headerCellClass: "middle",
                    enableCellEdit: false
                },
                {
                    name: 'Satuan',
                    displayName: 'Satuan',
                    field: 'Satuan',
                    width: '6%',
                    headerCellClass: "middle",
                    enableCellEdit: false
                },
                {
                    name: 'Harga Satuan',
                    displayName: 'Harga Satuan',
                    field: 'UnitPrice',
                    width: '10%',
                    cellFilter: 'number',
                    headerCellClass: "middle",
                    enableCellEdit: false,
                    // aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationType: function() {
                        if(!$scope.gridApiLihat)
                            return 0;
                        var sum = 0;
  
                        console.log('gridApiLihat==>>',$scope.gridApiLihat);
                        //if it is filtered/sorted you want the filtered/sorted rows only)
                        var visibleRows = $scope.gridApiLihat.core.getVisibleRows($scope.gridApiLihat.grid);
                        console.log("visibleRows before==>>",visibleRows);
                        console.log("visibleRows length==>>",visibleRows.length);
                        for (var i = 0; i < visibleRows.length; i++) {
                              console.log("visibleRows inner==>>",visibleRows);
                              //you have to parse just in case the data is in string otherwise it will concatenate
                              sum += visibleRows[i].entity.QtySO * visibleRows[i].entity.UnitPrice;
                              console.log("sum==>>",sum);
                              console.log("visibleRows[i].entity==>>",visibleRows[i].entity);
                          }

                        console.log('<GridLihat> HargaSatuan column ==> ',sum);  
                        return parseInt(Math.round(sum));
                    },
                    footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
                },
                {
                    name: 'DM Persen',
                    displayName: '%',
                    field: 'DiscountMaterialPercent',
                    width: '5%',
                    enableCellEdit: false,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:240%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Material</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'DM Jumlah',
                    displayName: 'Jumlah',
                    field: 'DiscountMaterialAmount',
                    width: '7%',
                    cellFilter: 'number',
                    enableCellEdit: false,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
                },
                {
                    name: 'DGM Persen',
                    displayName: '%',
                    field: 'DiscountGroupMaterialPercent',
                    width: '5%',
                    enableCellEdit: false,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:240%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Group Material</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'DGM Jumlah',
                    displayName: 'Jumlah',
                    field: 'DiscountGroupMaterialAmount',
                    width: '7%',
                    cellFilter: 'number',
                    enableCellEdit: false,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
                },
                {
                    name: 'DS Persen',
                    displayName: '%',
                    field: 'DiscountSpecialPercent',
                    width: '5%',
                    enableCellEdit: false,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:240%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Spesial</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'DS Jumlah',
                    displayName: 'Jumlah',
                    field: 'DiscountSpecialAmount',
                    width: '7%',
                    cellFilter: 'number',
                    enableCellEdit: false,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
                },
                {
                    name: 'ppn',
                    displayName: 'PPN',
                    field: 'VATAmount',
                    width: '10%',
                    cellFilter: 'number',
                    aggregationHideLabel: true,
                    headerCellClass: "middle",
                    enableCellEdit: false,
                    // aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationType: function() {
                        if(!$scope.gridApiLihat)
                            return 0;
                        var sum = 0;
  
                        console.log('gridApiLihat==>>',$scope.gridApiLihat);
                        //if it is filtered/sorted you want the filtered/sorted rows only)
                        var visibleRows = $scope.gridApiLihat.core.getVisibleRows($scope.gridApiLihat.grid);
                        console.log("visibleRows==>>",visibleRows);
                        for (var i = 0; i < visibleRows.length; i++) {
                              //you have to parse just in case the data is in string otherwise it will concatenate
                              sum += visibleRows[i].entity.VATAmount;
                              console.log("sum==>>",sum);
                              console.log("visibleRows[i].entity==>>",visibleRows[i].entity);
                          }

                        console.log('<GridLihat> VAT column ==> ',sum);  
                        return parseInt(Math.floor(sum));
                    },
                    footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
                },
                {
                    name: 'harga Total',
                    displayName: 'Harga Total',
                    field: 'TotalAmount',
                    width: '12%',
                    cellFilter: 'number',
                    aggregationHideLabel: true,
                    headerCellClass: "middle",
                    enableCellEdit: false,
                    // aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationType: function() {
                        if(!$scope.gridApiLihat)
                            return 0;
                        var sum = 0;
  
                        console.log('gridApiLihat==>>',$scope.gridApiLihat);
                        //if it is filtered/sorted you want the filtered/sorted rows only)
                        var visibleRows = $scope.gridApiLihat.core.getVisibleRows($scope.gridApiLihat.grid);
                        console.log("visibleRows==>>",visibleRows);
                        for (var i = 0; i < visibleRows.length; i++) {
                              //you have to parse just in case the data is in string otherwise it will concatenate
                              sum += visibleRows[i].entity.TotalAmount*10;
                              console.log("sum[",i,"]==>>",sum);
                              console.log("visibleRows[i].entity==>>",visibleRows[i].entity);
                          }

                        console.log('<GridLihat> TotalAmount column ==> ',sum);  
                        return parseInt(Math.floor(sum/10));
                    },
                    footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
                },
                // {
                //   name:'DP Persen',
                //   displayName:'%',
                //   field: 'DPPercent',
                //   width: '5%',
                //   headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:260%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2' style='text-align:left;padding-left:10px;'>DP</td></tr></tbody></table></div>%</div>"
                // },
                {
                    name: 'DP Jumlah',
                    displayName: 'Jumlah',
                    field: 'DPAmount',
                    width: '10%',
                    cellFilter: 'number',
                    aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationHideLabel: true,
                    headerCellClass: "middle",
                    enableCellEdit: false,
                    // aggregationType: uiGridConstants.aggregationTypes.sum,
                    aggregationType: function() {
                        if(!$scope.gridApiLihat)
                            return 0;
                        var sum = 0;
  
                        console.log('gridApiLihat==>>',$scope.gridApiLihat);
                        //if it is filtered/sorted you want the filtered/sorted rows only)
                        var visibleRows = $scope.gridApiLihat.core.getVisibleRows($scope.gridApiLihat.grid);
                        console.log("visibleRows==>>",visibleRows);
                        for (var i = 0; i < visibleRows.length; i++) {
                              //you have to parse just in case the data is in string otherwise it will concatenate
                              sum += visibleRows[i].entity.TotalAmount;
                              console.log("sum[",i,"]==>>",sum);
                              console.log("visibleRows[i].entity==>>",visibleRows[i].entity);
                          }

                        console.log('<GridLihat> DPAmount column ==> ',sum);  
                        return parseInt(Math.floor(sum));
                    },
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:300%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2' style='text-align:left;padding-left:10px;'>DP</td></tr></tbody></table></div>Jumlah</div>",
                    footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
                },
                {
                    name: 'Lunas',
                    displayName: 'Lunas',
                    field: 'isLunas',
                    width: '5%',
                    type: 'boolean',
                    //cellFilter: 'filterLunas',
                    enableCellEdit: false,
                    cellTemplate: '<input type="checkbox" ng-model="row.entity.isLunas" disabled>',
                    // cellTemplate: '<input type="checkbox" ng-true-value="3" ng-false-value="2" ng-model="row.entity.MaterialRequestStatusId">',
                    //cellTemplate: '<input type=\"checkbox\" value=\"\">',
                    // '<input type="checkbox" name="select_item" value="true" ng-model="row.entity.selected"/>`'
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Lunas</div>"
                }
            ]
        };

        $scope.gridOptionsUbah = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableFiltering: true,
            //selectedItems: console.log($scope.mySelections),
            enableSelectAll: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 5,

            columnDefs: [{
                    name: 'No Material',
                    displayName: 'No. Material',
                    field: 'NoMaterial',
                    width: '10%',
                    headerCellClass: "middle"
                },
                {
                    name: 'Nama Material',
                    displayName: 'Nama Material',
                    field: 'NamaMaterial',
                    width: '11%',
                    headerCellClass: "middle"
                },
                {
                    name: 'Qty Free',
                    displayName: 'Qty Free',
                    field: 'QtyFree',
                    width: '5%',
                    headerCellClass: "middle"
                },
                {
                    name: 'Qty SO',
                    displayName: 'Qty SO',
                    field: 'QtySO',
                    width: '5%',
                    headerCellClass: "middle"
                },
                {
                    name: 'Satuan',
                    displayName: 'Satuan',
                    field: 'Satuan',
                    width: '6%',
                    headerCellClass: "middle"
                },
                {
                    name: 'Harga Satuan',
                    displayName: 'Harga Satuan',
                    field: 'HargaSatuan',
                    width: '10%',
                    headerCellClass: "middle"
                },
                {
                    name: 'DM Persen',
                    displayName: '%',
                    field: 'DMPersen',
                    width: '5%',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:260%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Material</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'DM Jumlah',
                    displayName: 'Jumlah',
                    field: 'DMJumlah',
                    width: '8%',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
                },
                {
                    name: 'DGM Persen',
                    displayName: '%',
                    field: 'DGMPersen',
                    width: '8%',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:220%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Group Material</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'DGM Jumlah',
                    displayName: 'Jumlah',
                    field: 'DGMJumlah',
                    width: '10%',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
                },
                {
                    name: 'DS Persen',
                    displayName: '%',
                    field: 'DSPersen',
                    width: '5%',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:260%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Spesial</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'DS Jumlah',
                    displayName: 'Jumlah',
                    field: 'DSJumlah',
                    width: '8%',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
                },
                {
                    name: 'ppn',
                    displayName: 'PPN',
                    field: 'ppn',
                    width: '7%',
                    headerCellClass: "middle"
                },
                {
                    name: 'harga Total',
                    displayName: 'Harga Total',
                    field: 'hargaTotal',
                    width: '7%',
                    headerCellClass: "middle"
                },
                {
                    name: 'DP Persen',
                    displayName: '%',
                    field: 'DPPersen',
                    width: '5%',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:260%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2' style='text-align:left;padding-left:10px;'>DP</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'DP Jumlah',
                    displayName: 'Jumlah',
                    field: 'DPJumlah',
                    width: '8%',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
                }
            ]
        };

        $scope.hello = function() {
            alert('Ok');
            //console.log("");
        }

        // ////////////////search customer
        $scope.forget = function() {
            console.log("forgot=>");
            $scope.mForgot.show = true;
            // $scope.forgotPass = false;
            // $scope.search2 = true;
            // $scope.dimmerForgot = true;
        }
        $scope.CheckModal = function() {
            console.log("checkSent", $scope.mForgot);
            var xParam = [];
            if ($scope.mForgot.flag.flag == 1) {
                xParam.push({ Phone: $scope.mForgot.valueHP, Email: "-" });
                console.log("xparam neeh ==>", xParam);
            } else {
                xParam.push({ Phone: "-", Email: $scope.mForgot.valueEmail });
                console.log("xparam neeh ==>", xParam);
            };

            CustData.GetForgotPass(xParam).then(function(res) {
                    console.log("res nih kudu sampe disini jangan undefined wae", res.data.Result.length);
                    if (res.data.Result.length > 0) {
                        bsAlert.alert({
                            title: "Kirim ToyotaId ?",
                            text: "milik : " + res.data.Result[0].CustomerName, // tambah disini
                            type: "question",
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Ya, Kirim ToyotaId !',
                            cancelButtonText: 'Bukan, Batalkan !',
                            confirmButtonClass: 'btn btn-success',
                            cancelButtonClass: 'btn btn-danger'
                                // buttonsStyling: false
                        }, function() {
                            console.log("sampai kemarin paman datang");
                            bsAlert.alert({
                                title: "Berhasil !",
                                text: "ToyotaId telah dikirim.", // tambah disini
                                type: "success",
                                showCancelButton: false,
                                confirmButtonText: 'OK'
                            })
                        }, function(dismiss) {
                            console.log("sampai kemarin paman datang2");
                            bsAlert.alert({
                                title: "Dibatalkan !",
                                text: "ToyotaId tidak dikirim.", // tambah disini
                                type: "error",
                                showCancelButton: false,
                                confirmButtonText: 'OK'
                            })
                        });
                    } else {
                        bsAlert.alert({
                            title: "Data Tidak Ditemukan !",
                            text: "Pastikan Nomor Handphone atau Email yang diinput benar.", // tambah disini
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonText: 'OK'
                        });
                    }
                    $scope.mForgot.show = false;


                    // $scope.grid.data = angular.copy(res.data.Result);
                    // $scope.loading=false;
                    // console.log("======>",$scope.grid.data);
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.CariCustomer = function(value) {
            $scope.onProcessSearch = true;
            console.log("value", value);
            $scope.mDataCrm = [];
            $scope.mData.ToyotaId = value.TID;
            console.log('value search', value);
            $scope.mData.PaymentPeriod = 0;
            // $scope.mData.PaymentMethodId = 1;
            $scope.mData.PaymentMethodId = 202;
            $scope.DisabledWaktu = false; // true
            $scope.DisabledPayment = false; // true
            // console.log('flag search', flag);
            // if (value.filterValue == "") {
            //   bsNotify.show({
            //     size: 'big',
            //     type: 'danger',
            //     title: "Mohon input filter terlebih dahulu",
            //   });
            // } else {

            // if (flag == 1) {
            // PartsSalesOrder.getCustomerList(value).then(function(resu) {
            //         console.log('search cust >>>', resu);
            //         console.log('resu.data customer>', resu.data.Result[0]);
            //         var data = resu.data.Result[0];
            //         if (data === undefined) {
            //             bsNotify.show({
            //                 size: 'big',
            //                 type: 'danger',
            //                 title: "Data Tidak Ditemukan",
            //             });
            //             $scope.mData.CustomerId = "";
            //             $scope.mData.CustomerName = "";
            //             $scope.mData.Phone1 = "";
            //             $scope.mData.Phone2 = "";
            //             $scope.mData.KTPKITAS = "";
            //             $scope.mData.Npwp = "";
            //         } else {
            //             var clp = data.CustomerList.CustomerListPersonal[0];
            //             console.log('clp =>', clp);
            //             // if (clp == null || clp === 'undefined') {
            //             if(data.CustomerList.CustomerTypeId == 2 || data.CustomerList.CustomerTypeId == 4 || data.CustomerList.CustomerTypeId == 5 || data.CustomerList.CustomerTypeId == 6   ){
            //                 $scope.mData.CustomerId = data.CustomerList.CustomerListInstitution[0].CustomerId;
            //                 $scope.mData.CustomerName = data.CustomerList.CustomerListInstitution[0].PICName + " / " + data.CustomerList.CustomerListInstitution[0].Name;
            //                 $scope.mData.Phone1 = data.CustomerList.CustomerAddress[0].Phone1;
            //                 $scope.mData.Phone2 = data.CustomerList.CustomerAddress[0].Phone2;
            //                 $scope.mData.KTPKITAS = data.CustomerList.CustomerListPersonal[0].KTPKITAS;
            //                 $scope.mData.Npwp = data.CustomerList.Npwp;
            //             } else {
            //                 $scope.mData.CustomerId = clp.CustomerId;
            //                 $scope.mData.CustomerName = clp.CustomerName;
            //                 $scope.mData.Phone1 = clp.Handphone1;
            //                 $scope.mData.Phone2 = clp.Handphone2;
            //                 $scope.mData.Handphone1 = clp.Handphone1;
            //                 $scope.mData.Handphone2 = clp.Handphone2;
            //                 $scope.mData.KTPKITAS = clp.KTPKITAS;
            //                 $scope.mData.Npwp = data.CustomerList.Npwp;
            //             }
            //             //$scope.mData.CustomerName = (data.CustomerList.CustomerListPersonal[0].CustomerName);
            //         }
            //         $scope.onProcessSearch = false;
            //     },function(err){
            //         $scope.onProcessSearch = false;
            //     })
            //     // }
            //     // }

            // PartsSalesOrder.getCustomerListSO(value).then(function(resu) {
                // console.log('search cust >>>', resu);
                // console.log('resu.data customer>', resu.data.Result[0]);
                // var data = resu.data.Result[0];
                // if (data === undefined) {
                    // bsNotify.show({
                        // size: 'big',
                        // type: 'danger',
                        // title: "Data Tidak Ditemukan",
                    // });
                    // $scope.mData.CustomerId = "";
                    // $scope.mData.CustomerName = "";
                    // $scope.mData.Phone1 = "";
                    // $scope.mData.Phone2 = "";
                    // $scope.mData.Handphone1 = "";
                    // $scope.mData.Handphone2 = "";
                    // $scope.mData.KTPKITAS = "";
                    // $scope.mData.Npwp = "";
                // } else {
                    // var clp = data; //data.CustomerList.CustomerListPersonal[0];
                    // console.log('clp =>', clp);
                    // // if (clp == null || clp === 'undefined') {
                    // if (data.CustomerTypeId == 2 || data.CustomerTypeId == 4 || data.CustomerTypeId == 5 || data.CustomerTypeId == 6) {
                        // $scope.mData.CustomerId = clp.CustomerId;
                        // $scope.mData.CustomerName = clp.CustomerName;
                        // $scope.mData.Phone1 = clp.Phone1;
                        // $scope.mData.Phone2 = clp.Phone2;
                        // $scope.mData.Handphone1 = clp.Phone1;
                        // $scope.mData.Handphone2 = clp.Phone2;
                        // $scope.mData.KTPKITAS = clp.KTPKITAS;
                        // $scope.mData.Npwp = clp.Npwp;
                    // } else {
                        // var clp = data.CustomerList.CustomerListPersonal[0];
                        // console.log('clp =>', clp);
                        // // if (clp == null || clp === 'undefined') {
                        // if(data.CustomerList.CustomerListPersonal.length > 0){        
                            // $scope.mData.KTPKITAS = data.CustomerList.CustomerListPersonal[0].KTPKITAS;
                        // }
                        // $scope.mData.Npwp = data.CustomerList.Npwp;
                        // if(data.CustomerList.CustomerTypeId == 2 || data.CustomerList.CustomerTypeId == 4 || data.CustomerList.CustomerTypeId == 5 || data.CustomerList.CustomerTypeId == 6   ){
                            // $scope.mData.CustomerId = data.CustomerList.CustomerListInstitution[0].CustomerId;
                            // $scope.mData.CustomerName = data.CustomerList.CustomerListInstitution[0].PICName + " / " + data.CustomerList.CustomerListInstitution[0].Name;
                            // $scope.mData.Phone1 = data.CustomerList.CustomerAddress[0].Phone1;
                            // $scope.mData.Phone2 = data.CustomerList.CustomerAddress[0].Phone2;
                        // } else {
                            // $scope.mData.CustomerId = clp.CustomerId;
                            // $scope.mData.CustomerName = clp.CustomerName;
                            // $scope.mData.Phone1 = clp.Phone1;
                            // $scope.mData.Phone2 = clp.Phone2;
                            // $scope.mData.Handphone1 = clp.Phone1;
                            // $scope.mData.Handphone2 = clp.Phone2;
                            // $scope.mData.KTPKITAS = clp.KTPKITAS;
                            // $scope.mData.Npwp = clp.Npwp;
                        // }
                        // //$scope.mData.CustomerName = (data.CustomerList.CustomerListPersonal[0].CustomerName);
                    // }
                    // //$scope.mData.CustomerName = (data.CustomerList.CustomerListPersonal[0].CustomerName);
                // }
                // $scope.onProcessSearch = false;
            // },function(err){
                // $scope.onProcessSearch = false;
            // })
			
            if (value.filterValue == '') {
                $scope.onProcessSearch = false;
                bsAlert.warning("Filter tidak boleh kosong !!");
            } else {
                PartsSalesOrder.getCustomerListSO(value).then(function(resu) {
                    console.log('search cust >>>', resu);
                    console.log('resu.data customer>', resu.data.Result[0]);
                    var data = resu.data.Result[0];
                    if (data === undefined) {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Data Tidak Ditemukan",
                        });
                        $scope.mData.CustomerId = "";
                        $scope.mData.CustomerName = "";
                        $scope.mData.Phone1 = "";
                        $scope.mData.Phone2 = "";
                        $scope.mData.Handphone1 = "";
                        $scope.mData.Handphone2 = "";
                        $scope.mData.KTPKITAS = "";
                        $scope.mData.Npwp = "";
                    } else {
                        var clp = data; //data.CustomerList.CustomerListPersonal[0];
                        console.log('clp =>', clp);
                        // if (clp == null || clp === 'undefined') {
                        if (data.CustomerTypeId == 2 || data.CustomerTypeId == 4 || data.CustomerTypeId == 5 || data.CustomerTypeId == 6) {
                            $scope.mData.CustomerId = clp.CustomerId;
                            $scope.mData.CustomerName = clp.CustomerName;
                            $scope.mData.Phone1 = clp.Phone1;
                            $scope.mData.Phone2 = clp.Phone2;
                            $scope.mData.Handphone1 = clp.Phone1;
                            $scope.mData.Handphone2 = clp.Phone2;
                            $scope.mData.KTPKITAS = clp.KTPKITAS;
                            $scope.mData.Npwp = clp.Npwp;
                        } else {
                            $scope.mData.CustomerId = clp.CustomerId;
                            $scope.mData.CustomerName = clp.CustomerName;
                            $scope.mData.Phone1 = clp.Phone1;
                            $scope.mData.Phone2 = clp.Phone2;
                            $scope.mData.Handphone1 = clp.Phone1;
                            $scope.mData.Handphone2 = clp.Phone2;
                            $scope.mData.KTPKITAS = clp.KTPKITAS;
                            $scope.mData.Npwp = clp.Npwp;
                        }
                        //$scope.mData.CustomerName = (data.CustomerList.CustomerListPersonal[0].CustomerName);
                    }
                    $scope.onProcessSearch = false;
                },function(err){
                    $scope.onProcessSearch = false;
                })
            }
        };
        $scope.filterSearch = {
            flag: "",
            TID: "",
            filterValue: "",
            choice: false
        };
        $scope.filterField = [{
                key: "Nopol",
                desc: "Nomor Polisi",
                flag: 1,
                value: ""
            },
            {
                key: "NoRank",
                desc: "Nomor Rangka",
                flag: 2,
                value: ""
            },
            {
                key: "NoMesin",
                desc: "Nomor Mesin",
                flag: 3,
                value: ""
            },
            {
                key: "TTL",
                desc: "Tanggal Lahir",
                flag: 7,
                value: ""
            },
            {
                key: "Phone",
                desc: "Nomor Handphone",
                flag: 4,
                value: ""
            },
            {
                key: "NoKTP",
                desc: "Nomor KTP/KITAS",
                flag: 5,
                value: ""
            },
            {
                key: "NPWP",
                desc: "NPWP",
                flag: 6,
                value: ""
            }
        ];
        $scope.CheckIsNumber = function(event, item) {
            console.log(item);
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            var patternRegex = /\d/i;
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };
        $scope.mForgot = {
            show: false,
            flag: "",
            name: "",
            valueHp: "",
            valueEmail: ""
        };
        $scope.filterForgot = [{
            key: "phone",
            desc: "Nomor Handphone",
            flag: 1,
            value: ""
        }, {
            key: "Email",
            desc: "Alamat Email",
            flag: 2,
            value: ""
        }];
        //----------------------------------
        $scope.filterFieldChange = function(item) {
            $scope.filterSearch.flag = item.flag;
            $scope.filterSearch.filterValue = "";
            $scope.filterFieldSelected = item.desc;

        }

        // $scope.CustomerAddress = [];

        $scope.getAddressList = function() {
            console.log("asad");
            CMaster.GetCAddressCategory().then(function(res) {
                $scope.categoryContact = res.data.Result;
            });

            CMaster.GetLocation().then(function(res) {
                console.log("List Province====>", res.data.Result);
                // ProvinceId == row.ProvinceId
                $scope.provinsiData = res.data.Result;
                // $scope.tempProvinsiData = angular.copy($scope.provinsiData);
            });
        };

        $scope.selectProvince = function(row) {
            console.log('row selectProvince', row);
            $scope.kabupatenData = row.MCityRegency;
            $scope.selectedProvinnce = angular.copy(row);
        };
        $scope.selectRegency = function(row) {
            console.log('row selectRegency', row);
            $scope.kecamatanData = row.MDistrict;
            $scope.selectedRegency = angular.copy(row);
        };
        $scope.selectDistrict = function(row) {
            console.log('row selectDistrict', row);
            $scope.kelurahanData = row.MVillage;
            $scope.selectedDistrict = angular.copy(row);
        };
        $scope.selectVillage = function(row) {
            console.log('row selectVillage', row);
            $scope.selecedtVillage = angular.copy(row);
            $scope.custAddress.PostalCode = row.PostalCode ? row.PostalCode : '-';
        };

        $scope.saveCustnonToyota = function(item) {
            console.log("saveCustnonToyota item => ", item);
            $scope.ubahCust = item
            console.log("EditCust = item => ", $scope.ubahCust);
            // $scope.mData.CustomerId = item.CustomerId;
            // $scope.mData.CustomerCode = item.CustomerCode;
            $scope.mData.CustomerName = item.nama;
            $scope.mData.Phone1 = item.phone1;
            $scope.mData.Phone2 = item.phone2;
            $scope.mData.Handphone1 = item.phone1;
            $scope.mData.Handphone2 = item.phone2;
            $scope.mData.Npwp = item.Npwp;
            $scope.mData.KTPKITAS = item.KTPKITAS;

            if(item.CustomerTypeId == 3){

                var tempcustomer = {}
                tempcustomer = {
                    OutletId: $scope.user.OrgId,
                    CustomerTypeId: item.CustomerTypeId,
                    TypeInvTax: item.TypeInvTax,
                    KTPKITAS: item.KTPKITAS,
                    Npwp: item.Npwp,
                    StatusCode: '1',
                    // LastModifiedDate : newDate(),
                    // LastModifiedUserId : $scope.user.UserId
                }
                var newjson = {
                    CustomerName: item.nama,
                    Phone: item.phone1,
                    KTP: item.KTPKITAS
                }
                console.log("saveCustnonToyota tempcustomer => ", tempcustomer);
                // PartsSalesOrder.checkIfPhoneExists($scope.mData.Phone1,$scope.mData.CustomerName).then(function(res){
                //   var phoneResult = res.data.Result;
                //   // if(phoneResult.data.length > 0){
                //   //     $scope.ngDialog.close();
                //   //     bsNotify.show({
                //   //         title: "Customer Data",
                //   //         content: "No Handphone sudah pernah terdaftar",
                //   //         type: 'danger'
                //   //     });
                //   //     return;
                //   // }
                //   //PartsSalesOrder.savecustomer(tempcustomer).then(function (res) {

                // });

                PartsSalesOrder.savecustomer(tempcustomer, newjson).then(function(res) {
                        //console.log("savecustomer res = ", res.data.Result);
                        //console.log("savecustomer res.data = ", res.data);
                        //console.log("savecustomer res.data[0] = ", res.data[0]);
                        $scope.ubahCust.CustomerId = res.data[0].CustomerId;
                        console.log("ubahCust.CustomerId = ", $scope.ubahCust.CustomerId);

                        var dataCust = res.data.Result;
                        tempcustomer = {
                            CustomerId: res.data[0].CustomerId,
                            CustomerName: item.nama,
                            Handphone1: item.phone1,
                            Handphone2: item.phone2,
                            Npwp: item.Npwp,
                            KTPKITAS: item.KTPKITAS,
                            // LastModifiedDate : newDate(),
                            // LastModifiedUserId : $scope.user.UserId
                        }

                        PartsSalesOrder.savecustomerpersonal(tempcustomer).then(function(res1) {
                            console.log("res1", res1);
                            console.log("res.data[0].CustomerId => ", res.data[0].CustomerId);
                            console.log("res.data[0].CustomerCode => ", res.data[0].CustomerCode);
                            console.log("res.data[0] => ", res.data[0]);
                            $scope.CustomerId = res.data[0].CustomerId;
                            $scope.CustomerCode = res.data[0].CustomerCode;
                            $scope.mData.CustomerId = res.data[0].CustomerId;
                            $scope.mData.CustomerCode = res.data[0].CustomerCode;
                            $scope.mData.CustomerTypeId = item.CustomerTypeId;
                            // $scope.mData.Npwp = res.data[0].Npwp;
                            // $scope.mData.KTPKITAS = res.data[0].KTPKITAS;
                            $scope.mData.Npwp = res.data[0].Npwp ? res.data[0].Npwp : $scope.mData.Npwp;
                            $scope.mData.KTPKITAS = res.data[0].KTPKITAS ? res.data[0].KTPKITAS : $scope.mData.KTPKITAS;
                            $scope.filterSearch.customercode = res.data[0].CustomerCode;

                            tempcustomer = {
                                CustomerId: res.data[0].CustomerId,
                                Address: item.alamat,
                                RT: item.rt,
                                RW: item.rw,
                                ProvinceId: item.ProvinceId,
                                CityRegencyId: item.CityRegencyId,
                                DistrictId: item.DistrictId,
                                VillageId: item.VillageId,
                                // LastModifiedDate : newDate(),
                                // LastModifiedUserId : $scope.user.UserId,
                                MainAddress: true,
                                StatusCode: 1
                            }

                            console.log("tempcustomer=> ", tempcustomer);
                            PartsSalesOrder.savecustomeraddress(tempcustomer).then(function(res2) {
                                console.log("res2", res2);
                                $scope.custAddress = [];
                                // $scope.DataDesa = [];
                                $scope.DataProvinsi.ProvinceId = [];
                                // $scope.DataKabupaten.CityRegencyId = [];
                                // $scope.DataRayon.DistrictId = [];
                                $scope.addcustomer = false; // close form
                                //$scope.cancel(); // close form & refresh form
                                //kosonging inputan customer baru
                                bsNotify.show({
                                    size: 'small',
                                    type: 'success',
                                    title: "Data Customer",
                                    content: "&#10004; berhasil ditambah"
                                });
                                //console.log("custAddress.nama = ", $scope.custAddress.nama);
                            }, function(err2) {
                                console.log("err2=>", err2);
                            })
                        }, function(err1) {
                            console.log("err1=>", err1);
                        })
                    },
                    function(err) {
                        console.log("err=>", err);
                    });

            }
            else{
                var tempcustomer = {}
                tempcustomer = {
                    OutletId: $scope.user.OrgId,
                    CustomerTypeId: item.CustomerTypeId,
                    TypeInvTax: item.TypeInvTax,
                    KTPKITAS: item.KTPKITAS,
                    Npwp: item.Npwp,
                    StatusCode: '1',
                    // LastModifiedDate : newDate(),
                    // LastModifiedUserId : $scope.user.UserId
                }
                console.log("saveCustnonToyota tempcustomer => ", tempcustomer);
                var newjson = {
                    CustomerName: item.nama,
                    Phone: item.PICHp,
                    KTP: '-'
                }
                PartsSalesOrder.savecustomer(tempcustomer, newjson).then(function(res) {
                    //console.log("savecustomer res = ", res.data.Result);
                    //console.log("savecustomer res.data = ", res.data);
                    //console.log("savecustomer res.data[0] = ", res.data[0]);
                    $scope.ubahCust.CustomerId = res.data[0].CustomerId;
                    console.log("ubahCust.CustomerId = ", $scope.ubahCust.CustomerId);

                    var dataCust = res.data.Result;
                    tempcustomer = {
                        CustomerId: res.data[0].CustomerId,
                        CustomerName: item.nama,
                        PICName: item.PICName,
                        PICHp: item.PICHp,
                        Npwp: item.Npwp,
                        KTPKITAS: item.KTPKITAS,
                        // LastModifiedDate : newDate(),
                        // LastModifiedUserId : $scope.user.UserId
                    }

                        PartsSalesOrder.savecustomerInstitusi(tempcustomer).then(function(res1) {

                            console.log("res1", res1);
                            console.log("res.data[0].CustomerId => ", res.data[0].CustomerId);
                            console.log("res.data[0].CustomerCode => ", res.data[0].CustomerCode);
                            console.log("res.data[0] => ", res.data[0]);
                            $scope.CustomerId = res.data[0].CustomerId;
                            $scope.CustomerCode = res.data[0].CustomerCode;
                            $scope.mData.CustomerId = res.data[0].CustomerId;
                            $scope.mData.CustomerCode = res.data[0].CustomerCode;
                            $scope.mData.Npwp = res.data[0].Npwp ? res.data[0].Npwp : $scope.mData.Npwp;
                            $scope.mData.CustomerTypeId = item.CustomerTypeId;
                            $scope.mData.PICName = item.PICName;
                            $scope.mData.PICHp = item.PICHp;
                            $scope.mData.KTPKITAS = res.data[0].KTPKITAS ? res.data[0].KTPKITAS : $scope.mData.KTPKITAS;
                            $scope.filterSearch.customercode = res.data[0].CustomerCode;

                            tempcustomer = {
                                CustomerId: res.data[0].CustomerId,
                                Address: item.alamat,
                                RT: item.rt,
                                RW: item.rw,
                                ProvinceId: item.ProvinceId,
                                CityRegencyId: item.CityRegencyId,
                                DistrictId: item.DistrictId,
                                VillageId: item.VillageId,
                                MainAddress: true,
                                StatusCode: 1
                            }

                            console.log("tempcustomer=> ", tempcustomer);
                            PartsSalesOrder.savecustomeraddress(tempcustomer).then(function(res2) {
                                console.log("res2", res2);
                                $scope.custAddress = [];
                                // $scope.DataDesa = [];

                                $scope.DataProvinsi.ProvinceId = [];
                                $scope.addcustomer = false; // close form
                                bsNotify.show({
                                    size: 'small',
                                    type: 'success',
                                    title: "Data Customer",
                                    content: "&#10004; berhasil ditambah"
                                });

                            }, function(err2) {
                                console.log("err2=>", err2);
                            })
                        }, function(err1) {
                            console.log("err1=>", err1);
                        })
                    },
                    function(err) {
                        console.log("err=>", err);
                    });


            }



        }; // end savecustomer

        $scope.ubahDataCustomer = function(item) {
            console.log('item', item);
            PartsSalesOrder.ubahCustomer(item).then(function(res) {
                    console.log("<controller> temp res", res);
                    $scope.ubah_customer = false; // close form

                    var hasil = res.data["0"];
                    $scope.mData.CustomerName = hasil.nama;
                    $scope.mData.Phone1 = hasil.phone1;
                    $scope.mData.Phone2 = hasil.phone2;
                    $scope.mData.KTPKITAS = hasil.KTPKITAS;
                    $scope.mData.Npwp = hasil.Npwp;

                    bsNotify.show({
                        size: 'small',
                        type: 'success',
                        title: "Data Customer",
                        content: "&#10004; berhasil diubah"
                    });
                },
                function(err) {
                    console.log("err=>", err);
                });
        };


        $scope.gridActionTemplate = '<div class="ui-grid-cell-contents"> \
            <a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat" tooltip-placement="bottom" onclick="this.blur()" ng-if="!row.groupHeader" ng-click="grid.appScope.gridClickViewDetailHandler(row.entity)" tabindex="0"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
            <a href="" style="color:#777;" ng-show="row.entity.SalesOrderStatusId==2" class="trlink ng-scope" uib-tooltip="Ubah" tooltip-placement="bottom" onclick="this.blur()" ng-if="grid.appScope.allowEdit &amp;&amp; !grid.appScope.hideEditButton &amp;&amp; !row.groupHeader &amp;&amp; !grid.appScope.defHideEditButtonFunc(row)" ng-click="grid.appScope.gridClickEditHandler(row.entity)" tabindex="0"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
            </div>';
        var BengkelNm="", BengkelAddr="", BengkelPhn="";

        $scope.ViewBengkel = function() {
                $scope.viewBengkel = true
                $scope.Bengkel.BengkelName= BengkelNm;
                $scope.Bengkel.BengkelAddress = BengkelAddr;
                $scope.Bengkel.BengkelPhone = BengkelPhn;
            }

        $scope.ViewBengkelLain = function() {
            $scope.viewBengkelLain = true
        }


    })
    .filter('filterTypeSalesOrder', function() {
        var xtipe = {
            '1': 'PO Bengkel Lain',
            '2': 'Customer Langsung'
        };
        return function(input) {
            if (!input) {
                return '';
            } else {
                return xtipe[input];
            }
        };
    })
    .filter('filterStatusSalesOrder', function() {
        var xstatus = {
            '1': 'Request Approval',
            '2': 'Open',
            '3': 'Partial',
            '4': 'Completed',
            '5': 'Cancelled',
            '13': 'Rejected'
        };
        return function(input) {
            if (!input) {
                return '';
            } else {
                return xstatus[input];
            }
        };
    })
    .filter('filterLunas', function() {
        var xlunas = {
            3: true,
            2: false
        };
        return function(input) {
            if (!input) {
                return '';
            } else {
                return xlunas[input];
            }
        };
    })
    // .filter('calculateTotalAmount', function () {
    //   var totalamount = 0;
    //   return function (input, QtySO, UnitPrice) {
    //     return totalamount = input[QtySO] * input[UnitPrice];
    //   };
    // })



.directive('format', ['$filter', function($filter) { // for thousand separator
        return {
            require: '?ngModel',
            link: function(scope, elem, attrs, ctrl) {
                if (!ctrl) return;


                ctrl.$formatters.unshift(function(a) {
                    return $filter(attrs.format)(ctrl.$modelValue)
                });


                ctrl.$parsers.unshift(function(viewValue) {
                    var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                    elem.val($filter(attrs.format)(plainNumber));
                    return plainNumber;
                });
            }
        };
    }])
    .$inject = ['$scope']; // end format thousand separator
;
