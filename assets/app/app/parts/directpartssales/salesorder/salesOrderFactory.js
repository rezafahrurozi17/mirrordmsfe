angular.module('app')
.constant("constantSO", {
    "tipeReferensi": [{"id": 1, "name": "PO Bengkel Lain"}, {"id": 2, "name": " Customer Langsung"}]
  }) 
  .factory('PartsSalesOrder', function($http, CurrentUser, LocalService, $filter) {
    var currentUser = CurrentUser.user();
    //var DHeader = {};
    var SalesOrderHeader = {};
    console.log("==========>currentUser",currentUser);
    var SalesOrderDetail = {};
    this.formApi = {};
    return {
      getData: function(data) {
        console.log("<factory> data => ", data);
        //filter tanggal
        var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
        var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');
        console.log("<factory> endDate => ", endDate);

        var res = $http.get('/api/as/SalesOrder', {params: {
                                                          OutletId : (data.OutletId == null ? "n/a" : data.OutletId),
                                                          SalesOrderStatusId : (data.SalesOrderStatusId == null ? "n/a" : data.SalesOrderStatusId),
                                                          startDate: (data.startDate == null ? "n/a" : startDate),
                                                          endDate: (data.endDate == null ? "n/a" : endDate),
                                                          SalesOrderNo : (data.vSalesOrderNo == null ? "n/a" : data.vSalesOrderNo),
                                                          RefSONo : (data.vRefSONo == null ? "n/a" : data.vRefSONo),
                                                          Bengkel : (data.vBengkel == null ? "n/a" : data.vBengkel),
                                                          CustomerName : (data.vCustomerName == null ? "n/a" : data.vCustomerName)
                                                      } });
        console.log('<factory> hasil => ', res);
        //res.data.Result = null;
        return res;
      },getDataApprove: function(data) {
        //filter tanggal
        // var startDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        // var endDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        var startDate = "n/a";
        var endDate = "n/a";

        var res = $http.get('/api/as/SalesOrder', {params: {
                                                          OutletId : (data.OutletId == null ? "n/a" : data.OutletId),
                                                          SalesOrderStatusId : (data.SalesOrderStatusId == null ? "n/a" : data.SalesOrderStatusId),
                                                          startDate: startDate,
                                                          endDate: endDate,
                                                          SalesOrderNo : (data.vSalesOrderNo == null ? "n/a" : data.vSalesOrderNo),
                                                          RefSONo : (data.vRefSONo == null ? "n/a" : data.vRefSONo),
                                                          Bengkel : (data.vBengkel == null ? "n/a" : data.vBengkel),
                                                          CustomerName : (data.vCustomerName == null ? "n/a" : data.vCustomerName)
                                                      } });
        return res;
      },
      getData2: function(data) {
        console.log("<factory> data => ", data);
        //filter tanggal
        var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
        var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');
        console.log("<factory> endDate => ", endDate);

        var res = $http.get('/api/as/SalesOrder', {params: {
                                                          OutletId : (data.OutletId == null ? "n/a" : data.OutletId),
                                                          SalesOrderStatusId : (data.SalesOrderStatusId == null ? "n/a" : data.SalesOrderStatusId),
                                                          startDate: (data.startDate == null ? "n/a" : startDate),
                                                          endDate: (data.endDate == null ? "n/a" : endDate),
                                                          SalesOrderNo : (data.xSalesOrderNo == null ? "n/a" : data.xSalesOrderNo),
                                                          RefSONo : (data.vRefSONo == null ? "n/a" : data.vRefSONo),
                                                          Bengkel : (data.vBengkel == null ? "n/a" : data.vBengkel),
                                                          CustomerName : (data.vCustomerName == null ? "n/a" : data.vCustomerName)
                                                      } });
        console.log('<factory> hasil => ', res);
        //res.data.Result = null;
        return res;
      },
      setSalesOrderComplete: function(data){
        console.log('Update dong => ', data);
        var vdocDate = $filter('date')(data.DocDate, 'yyyy-MM-ddT00:00:00Z');
        return $http.put('/api/as/SalesOrder/CompleteOrder?SalesOrderId='+ data
        // {params: {
        //   SalesOrderId: data
        // } }
        );
      },

      getDetail: function(data){
        console.log("<factory> getData detail => ", data);
        console.log("<factory> SOId => ", data.SalesOrderId);
        console.log("<factory> RefSDNo => ", data.RefSONo);
        var res=$http.get('/api/as/SalesOrder/GetSalesOrderItem', {params: {
                                                          SalesOrderId: data.SalesOrderId,
                                                          RefSONo: (data.RefSONo == null ? '-' : data.RefSONo)
                                                          //RefSONo: data.RefSONo
                                                        } });
        console.log('<factory> dataDetail =>',res);
        return res;

      },
      getDocumentNumber: function(data) {
        console.log("[getDocumentNumber]");
        console.log(data);
        // var res=$http.get('/api/as/SalesOrder/GetDocumentNumber', {params: {
        //                                                   UserId : data,
        //                                                 } });
        var res=$http.get('/api/as/PartsUser/GetDocumentNumber', {params: {
                                                          FormatId : data,
                                                        } });
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      create: function(data, detail) {
        // var result = average();
        // if (isNaN(result)) result = 0;
        data.TotalAmount = isNaN(data.TotalAmount) ? 0 : data.TotalAmount;
        data.DPAmount = isNaN(data.DPAmount) ? 0 : data.DPAmount;
        detail.TotalAmount = isNaN(detail.TotalAmount) ? 0 : detail.TotalAmount;
        detail.TotalAmount = (detail.TotalAmount == null || detail.TotalAmount === undefined ? 0 : detail.TotalAmount);
        console.log('tambah data => ', data);
        console.log('tambah data-detail => ', detail);
        //var vdocDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
        var vdocDate = $filter('date')(new Date(), 'yyyy-MM-dd');
        //$scope.currentDate = new Date();
        return $http.post('/api/as/SalesOrder', [{
                                            //SalesOrderId: (data.SalesOrderId == null ? 0 : data.SalesOrderId),
                                            SalesOrderNo: (data.SalesOrderNo == null ? "-" : data.SalesOrderNo),
                                            OutletId: (data.OutletId == null ? 0 : data.OutletId),
                                            WarehouseId: (data.WarehouseId == null ? 0 : data.WarehouseId),
                                            DocDate: vdocDate, //(data.DocDate == null ? "2017-04-01" : data.DocDate),
                                            RefSOTypeId: (data.RefSOTypeId == null ? 0 : data.RefSOTypeId),
                                            RefSONo: (data.RefSONo == null ? "-" : data.RefSONo),
                                            DateNeeded: ((data.DateNeeded == null || data.DateNeeded === undefined) ? vdocDate : data.DateNeeded),
                                            CustomerId: ((data.CustomerId == null || data.CustomerId === undefined) ? null : data.CustomerId),
                                            ToyotaId: (data.ToyotaId == null ? "-" : data.ToyotaId),
                                            CustomerOutletId: data.CustomerOutletId, //(data.CustomerOutletId == null ? 0 : data.CustomerOutletId),
                                            PaymentMethodId: (data.PaymentMethodId == null ? 0 : data.PaymentMethodId),
                                            PaymentPeriod: (data.PaymentPeriod == null ? 0 : data.PaymentPeriod),
                                            DPMin: (data.DPMin == null ? 0 : data.DPMin),
                                            DPAmount: (data.DPAmount == null ? 0 : data.DPAmount),
                                            TotalAmount: (data.TotalAmount == null ? 0 : data.TotalAmount),
                                            Notes: (data.Notes == null ? '' : data.Notes),
                                            SalesOrderStatusId: (data.SalesOrderStatusId == null ? 0 : data.SalesOrderStatusId),
                                            MaterialRequestNo: '-', //(data.MaterialRequestNo == null ? '-' : data.MaterialRequestNo),
                                            MaterialRequestStatusId: (data.MaterialRequestStatusId == null ? 0 : data.MaterialRequestStatusId),
                                            CancelReasonDesc: data.CancelReasonDesc,
                                            VATAmount: (data.VATAmount == null ? 0 : data.VATAmount),
                                            Price: (data.Price == null ? 0 : data.Price),
                                            Discount: (data.Discount == null ? 0 : data.Discount),
                                            GridDetail: detail,
                                            PoliceNumber: (data.PoliceNumber == null ? '' : data.PoliceNumber)
                                        }]);
      },
      createDetail: function(data) {
        console.log('tambah data grid detail=>', data);
        return $http.post('/api/as/SalesOrder/InsertSalesOrderItem', data);
      },
      update: function(data, detail){
        console.log('Update => ', data);
        var today = new Date();
        var vdocDate = $filter('date')(data.DocDate, 'yyyy-MM-ddT00:00:00Z');
        return $http.put('/api/as/SalesOrder', [{
                                            SalesOrderId: (data.SalesOrderId == null ? 0 : data.SalesOrderId),
                                            SalesOrderNo: data.SalesOrderNo,
                                            OutletId: data.OutletId,
                                            WarehouseId: data.WarehouseId,
                                            DocDate: vdocDate, //data.DocDate,
                                            RefSOTypeId: data.RefSOTypeId,
                                            RefSONo: (data.RefSONo == null ? "-" : data.RefSONo),
                                            DateNeeded: data.DateNeeded,
                                            CustomerId: data.CustomerId,
                                            ToyotaId: data.ToyotaId,
                                            CustomerOutletId: data.CustomerOutletId,
                                            PaymentMethodId: data.PaymentMethodId,
                                            PaymentPeriod: data.PaymentPeriod,
                                            DPMin: data.DPMin,
                                            DPAmount: data.DPAmount,
                                            TotalAmount: data.TotalAmount,
                                            Notes: data.Notes,
                                            SalesOrderStatusId: data.SalesOrderStatusId,
                                            MaterialRequestStatusId: (data.MaterialRequestStatusId == null ? "9" : data.MaterialRequestStatusId),
                                            CancelReasonDesc: data.CancelReasonDesc,
                                            CancelReasonId: data.CancelReasonId,
                                            VATAmount: data.VATAmount,
                                            CreatedDate: (data.CreatedDate==null?today:data.CreatedDate),
                                            CreatedUserId: (data.CreatedUserId == null ? 0 : data.CreatedUserId),                        
                                            GridDetail: detail
                                          }]);
      },
      cancel: function(data){
        console.log('Update => ', data);
        var vdocDate = $filter('date')(data.DocDate, 'yyyy-MM-ddT00:00:00Z');
        return $http.put('/api/as/SalesOrder/CancelSalesOrder', [{
                                            SalesOrderId: (data.SalesOrderId == null ? 0 : data.SalesOrderId),
                                            SalesOrderNo: data.SalesOrderNo,
                                            OutletId: data.OutletId,
                                            WarehouseId: data.WarehouseId,
                                            DocDate: vdocDate, //data.DocDate,
                                            RefSOTypeId: data.RefSOTypeId,
                                            RefSONo: (data.RefSONo == null ? "-" : data.RefSONo),
                                            DateNeeded: data.DateNeeded,
                                            CustomerId: data.CustomerId,
                                            ToyotaId: data.ToyotaId,
                                            CustomerOutletId: data.CustomerOutletId,
                                            PaymentMethodId: data.PaymentMethodId,
                                            PaymentPeriod: data.PaymentPeriod,
                                            DPMin: data.DPMin,
                                            DPAmount: data.DPAmount,
                                            TotalAmount: data.TotalAmount,
                                            Notes: data.Notes,
                                            SalesOrderStatusId: data.SalesOrderStatusId,
                                            MaterialRequestStatusId: (data.MaterialRequestStatusId == null ? "9" : data.MaterialRequestStatusId),
                                            CancelReasonDesc: data.CancelReasonDesc,
                                            CancelReasonId: data.CancelReasonId,
                                            VATAmount: data.VATAmount
                                          }]);
      },
      SearchNoPurchaseOrder: function(RefPONo, outletlain, VendorOutletId) {
        console.log("<factory> SearchNoPO => ");
        var res = $http.get('/api/as/SalesOrder/SearchNoPO', {params: {RefPONo : RefPONo, OutletId : outletlain, VendorOutletId : VendorOutletId }});
        console.log('<factory> SearchNoPO - hasil => ', res);
        return res;
      },
      // SearchNoPurchaseOrderItem: function(RefPONo, OutletId,WarehouseId) {
      //   console.log("<factory> SearchNoPOItem => ");
      //   var res = $http.get('/api/as/SalesOrder/SearchNoPOItem', {params: {RefPONo : RefPONo, OutletId : OutletId, WarehouseId:WarehouseId }});
      //   console.log('<factory> SearchNoPOItem - hasil => ', res);
      //   return res;
      // },
      SearchNoPurchaseOrderItem: function(RefPONo, OutletId, VendorOutletId) {
        console.log("<factory> SearchNoPOItem => ");
        var res = $http.get('/api/as/SalesOrder/SearchNoPOItem', {params: {RefPONo : RefPONo, OutletId : OutletId, VendorOutletId : VendorOutletId }});
        console.log('<factory> SearchNoPOItem - hasil => ', res);
        return res;
      },
      SearchCustomer: function(ToyotaId) {
        console.log("<factory> SearchCustomer => ");
        var res = $http.get('/api/as/SalesOrder/SearchCustomer', {params: {ToyotaId : ToyotaId}});

        console.log('<factory> res => ', res);
        return res;
      },
      // SearchCustomerByCode: function() {
      //   console.log("<factory> SearchCustByCode=> ");
      //   //var res = $http.get('/api/as/SalesOrder/SearchCustByCode', {params: {CustomerCode : CustomerCode}});
      //   var res = $http.get('/api/as/SalesOrder/SearchCustByCode');
      //   console.log('<factory> res => ', res);
      //   return res;
      // },
      SearchCustomerByCode: function(item) {
        console.log("<factory> SearchCustByCode=> ");
        //var res = $http.get('/api/as/SalesOrder/SearchCustByCode', {params: {CustomerCode : CustomerCode}});
        var res = $http.get('/api/as/SalesOrder/GetCustomerWithPhone',
        { params: {
          CustPhone:item,
          OutletID:currentUser.OutletId
        }});
        console.log('<factory> res => ', res);
        return res;
      },
      getCustomerList:function(value){
        var param1 = value.TID ? value.TID : '-';
        var param2 = '-';
        var param3 = '-';
        var param4 = '-';
        var param5 = '-';
        var param6 = '-';
        var param7 = '-';
        var param8 = '-';

        switch (value.flag) {
            case 1:
                param2 = value.filterValue ? value.filterValue : '-';
                break;
            case 2:
                param3 = value.filterValue ? value.filterValue : '-';
                break;
            case 3:
                param4 = value.filterValue ? value.filterValue : '-';
                break;
            case 4:
                param5 = value.filterValue ? value.filterValue : '-';
                break;
            case 5:
                param6 = value.filterValue ? value.filterValue : '-';
                break;
            case 6:
                param7 = value.filterValue ? value.filterValue : '-';
                break;
            case 7:
                // param8 = value.filterValue ? value.filterValue : '-';
                if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                    param8 = '-'
                } else {
                    param8 = angular.copy(value.filterValue.replace(/\./g, "z")) 
                    param8 = angular.copy(param8.replace(/\-/g, "x"))

                }

        };

        //return $http.get('/api/crm/GetCustomerListFilter/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7);
        return $http.get('/api/crm/GetCustomerListFilter/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7 + '/' + param8);
    },
    getCustomerListSO:function(value) {
      console.log("<factory> getCustomerListSO => ",value);

      var param1 = value.TID ? value.TID : '-';
      var param2 = '-';
      var param3 = '-';
      var param4 = '-';
      var param5 = '-';
      var param6 = '-';
      var param7 = '-';
      var param8 = '-';
      switch (value.flag) {
          case 1:
              param2 = value.filterValue ? value.filterValue : '-';
              break;
          case 2:
              param3 = value.filterValue ? value.filterValue : '-';
              break;
          case 3:
              param4 = value.filterValue ? value.filterValue : '-';
              break;
          case 4:
              param5 = value.filterValue ? value.filterValue : '-';
              break;
          case 5:
              param6 = value.filterValue ? value.filterValue : '-';
              break;
          case 6:
              param7 = value.filterValue ? value.filterValue : '-';
              break;
          case 7:
              param8 = value.filterValue ? value.filterValue : '-';
              break;
      };

      res = $http.get('/api/as/SalesOrder/GetCustomerListFilter/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7 + '/' + param8);
      return res;
    },
    OutletLain: function() {
      console.log("<factory> OutletLain => ");
      var res = $http.get('/api/as/SalesOrder/OutletLain');
      console.log('<factory> res OutletLain => ', res);
      return res;
    },
    DataProvinsi: function() {
      console.log("<factory> SelectProvince => ");
      // var res = $http.get('/api/as/SalesOrder/SelectProvince'); // ganti pake yg dari sales
      var res = $http.get('/api/sales/MLocationProvince');
      console.log('<factory> res SelectProvince => ', res);
      return res;
    },
    DataKabupaten: function(data) {
      console.log("<factory> DataKabupaten => ", data);
      // var res = $http.get('/api/as/SalesOrder/SelectCityRegency', {params:{ProvinceId: data}}); // ganti pake yg dari sales
      return $http.get('/api/sales/MLocationCityRegency?start=1&limit=100&filterData=ProvinceId|' + data);
      // console.log('<factory> res DataKabupaten => ', res);
      // return res;
    },
    DataRayon: function(data) {
      console.log("<factory> DataRayon => ", data);
      // var res = $http.get('/api/as/SalesOrder/SelectDistrict', {params:{CityRegencyId: data}}); // ganti pake yg dari sales
      return $http.get('/api/sales/MLocationKecamatan?start=1&limit=100&filterData=CityRegencyId|' + data);
      // console.log('<factory> res DataRayon => ', res);
      // return res;
    },
    DataDesa: function(data) {
      console.log("<factory> DataDesa => ", data);
      // var res = $http.get('/api/as/SalesOrder/SelectVillage', {params:{DistrictId: data}}); // ganti pake yg dari sales
      return $http.get('/api/sales/MLocationKelurahan?start=1&limit=100&filterData=DistrictId|' + data)
      // console.log('<factory> res DataDesa => ', res);
      // return res;
    },

      // savecustomer:function(data){
      //   console.log('data savecustomer -> ', data)
      //   return $http.post('/api/crm/PostCustomerList/', [{
      //       OutletId: data.OutletId ,
      //       CustomerTypeId: data.CustomerTypeId ,
      //       StatusCode: data.StatusCode ,
      //       // LastModifiedDate: data.LastModifiedDate ,
      //       // LastModifiedUserId: data.LastModifiedUserId
      //   }]);
      // },
      ubahCustomer: function(data) {
        console.log("<factory> ubahCustomer", data);
        var res = $http.get('/api/as/SalesOrder/UbahDataCustomer', {params: {
                                                          CustomerId : data.CustomerId,
                                                          CustomerTypeId : data.CustomerTypeId,
                                                          TypeInvTax : data.TypeInvTax,
                                                          nama : this.checkIsCharacter(data.nama),
                                                          phone1 : data.phone1,
                                                          phone2 : data.phone2,
                                                          Npwp : data.Npwp,
                                                          KTPKITAS : data.KTPKITAS,
                                                          alamat : data.alamat,
                                                          rt : data.rt,
                                                          rw : data.rw,
                                                          ProvinceId : data.ProvinceId,
                                                          DistrictId : data.DistrictId,
                                                          CityRegencyId : data.CityRegencyId,
                                                          VillageId : data.VillageId
                                                      } });
        console.log('<factory> hasil => ', res);
        return res;
      },

      savecustomer: function(data, newdata) {
        console.log("<factory> savecustomer data => ", data);
        var OutletId = data.OutletId
        var CustomerTypeId = data.CustomerTypeId
        var TypeInvTax = data.TypeInvTax
        var Npwp = (data.Npwp == null ? '-' : data.Npwp)
        
        var res = $http.put('/api/as/SalesOrder/InsCustNonToyota?CustomerTypeId=' + CustomerTypeId + '&Npwp=' + Npwp + '&OutletId=' + OutletId + '&TypeInvTax=' + TypeInvTax , [newdata]);
        

        console.log('<factory> hasil => ', res);
        return res;
      },
      savecustomerpersonal:function(data){
        console.log('data savecustomerpersonal -> ', data)
         return $http.post('/api/crm/PostCustomerListPersonal/', [{
              CustomerId: data.CustomerId ,
              CustomerName:this.checkIsCharacter(data.CustomerName),
              Npwp:data.Npwp,
              KTPKITAS:data.KTPKITAS,
              Handphone1:data.Handphone1 ,
              Handphone2:data.Handphone2 ,
              // LastModifiedDate: data.LastModifiedDate ,
              // LastModifiedUserId: data.LastModifiedUserId
          }]);
      },
      savecustomerInstitusi:function(data){
        console.log('data savecustomerInstitusi -> ', data)
         return $http.post('/api/crm/PostCustomerListInstitution/', [{
              CustomerId: data.CustomerId ,
              Name:this.checkIsCharacter(data.CustomerName),
              PICName:this.checkIsCharacter(data.PICName),
              PICHp:data.PICHp ,
              PICAddress:data.PICAddress ,
              PICVillageId:data.PICVillageId ,
              PICRT:data.PICRT ,
              PICRW:data.PICRW 
          }]);
      },
      savecustomeraddress:function(data){
        console.log('data savecustomeraddress -> ', data)
         return $http.post('/api/crm/PostCCustomerAddress/', [{
            CustomerId : data.CustomerId ,
            Address: data.Address ,
            RT : data.RT ,
            RW : data.RW ,
            VillageId : data.VillageId,
            ProvinceId : data.ProvinceId,
            CityRegencyId : data.CityRegencyId,
            DistrictId : data.DistrictId,
            // LastModifiedDate : data.LastModifiedDate ,
            // LastModifiedUserId : data.LastModifiedUserId ,
            MainAddress : data.MainAddress,
            StatusCode : data.StatusCode
          }]);
      },
      getCCustomerCategory: function(){
        var res=$http.get('/api/crm/GetCCustomerCategory/');
        //console.log('getCCustomerCategory res =>', res);
        return res;
      },
      getKodeJenisTransaksi: function(){
        var res=$http.get('/api/as/SalesOrder/GetKodeJenisTransaksi/');
        //console.log('GetKodeJenisTransaksi res =>', res);
        return res;
      },
      // Detail
      getDetailByMaterialNo: function(data,  OutletId){
        //console.log("[factory] getDetailByMaterialNo data => ", data);
        var res=$http.get('/api/as/SalesOrder/GetSOItemFromMaterial', {params: {
                                                          PartCode : data,
                                                          OutletId : OutletId,
                                                        } });
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;

      },
      AvailableInSB: function(data) {
        console.log("<factory> AvailableInSB => ", data);
        var res = $http.get('/api/as/SalesOrder/CheckSB', {params:{
                                                            SOId: data.SalesOrderId,
                                                            OutletId: data.OutletId
                                                          }});
        console.log('<factory> res AvailableInSB => ', res);
        return res;
      },
      setSalesOrderHeader : function(data){
        SalesOrderHeader = data;
      },
      getSalesOrderHeader : function(){
        return SalesOrderHeader;
      },

      setSalesOrderDetail : function(data){
        SalesOrderDetail = data;
      },
      getSalesOrderDetail : function(){
        return SalesOrderDetail;
      },

      // added by sss on 2017-12-18
      getPartsFreeze : function(PartCode){
        var res=$http.get('/api/as/GetStockOpnameFreeze', {params: {
                                                          partsCode: PartCode,
                                                        } });
        console.log('<factory> dataDetail =>',res);
        return res;
      },
      //added by Yap 2018-01-11
      checkIfPhoneExists : function(noHandphone,customerName){
          var res=$http.get('/api/crm/CustomerListPersonal/CheckIfCustomerPhoneExists',{
              params : {
                NoHandphone : noHandphone,
                CustomerName : customerName
              }
          });

          return res;
      },
      sendNotif: function(data) {
          // console.log("model", IdSA);
          // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
          return $http.post('/api/as/SendNotification', [{
              Message: 'Sales Order ' + data.SalesOrderNo + ' telah di approve',
              RecepientId: data.CreatedUserId,
              Param : 1
          }]);
      },
      getserviceandmaterial: function() {
        console.log("YOU DONT OWN ME");
        var res = $http.get('/api/as/PartsUser/GetServiceWarehouse');
        return res;
      },
      GetCheckDiskonAppoval: function(DiscountSpecialPercent,OutletId,ServiceTypeId) {
        console.log("HELLO YOU");
        var res = $http.get('/api/as/SalesOrder/GetCheckDiskonAppoval',{
          params : {
            DiscountSpecialPercent : DiscountSpecialPercent,
            OutletId : OutletId,
            ServiceTypeId : ServiceTypeId
          }
        });
        return res;
      },
      GetCheckCustomer: function(OutletId) {
        console.log("[Factory] GetCheckCustomer");
        var res = $http.get('/api/as/SalesOrder/GetCheckCustomer',{
          params : {
            OutletId : OutletId
          }
        });
        return res;
      },
      checkIsCharacter: function(data){
        if(typeof data == 'string'){
            data = data.replace(/[^\x20-\x7f\xA\xD]/g, '');
        }
        return data;
      },
    }
  });
