angular.module('app')
  .factory('PartsSalesBilling', function($http, CurrentUser, $filter) {
    this.formApi = {};
    var currentUser = CurrentUser.user;
    // console.log(currentUser);
    var SalesBillingHeader = {};
    var SalesBillingDetail = {};
    // var MRHeader = {};
    return {
      setSBHeader : function(data){
        for(var k in data) SalesBillingHeader[k]=data[k];
        //var vDate = $filter('date')(data.CancelDate, 'yyyy-MM-dd');
        //MRHeader['MRstatusName'] = C_MATERIAL.Status[MRHeader.GoodsReceiptStatusId].name;
        //if (SalesBillingHeader.CancelDate == '0001-01-01') SalesBillingHeader.CancelDate = "";
      },
      getSBHeader : function(){
        return SalesBillingHeader;
      },
      // getData: function() {
      //   // var res=$http.get('countries.json');
      //   // return res;
      // }
      getData: function(data, n) {
        console.log("<factory> data => ", data);
        //filter tanggal
        var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
        var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');
        //console.log("<factory> startDate => ", startDate);
        //console.log("<factory> endDate => ", endDate);
        console.log("<factory> n => ", n);
        console.log("<factory> SalesOrderNo => ", data.SalesOrderNo);
        console.log("<factory> Bengkel => ", data.Bengkel);
        console.log("<factory> CustomerName => ", data.CustomerName);



        var res = $http.get('/api/as/SalesBilling', {params: {
                                                          startDate: (data.startDate == null ? "n/a" : startDate),
                                                          endDate: (data.endDate == null ? "n/a" : endDate),
                                                          SalesOrderNo : (data.fSalesOrderNo == null ? "n/a" : data.fSalesOrderNo),
                                                          Bengkel : (data.fBengkel == null ? "n/a" : data.fBengkel),
                                                          CustomerName : (data.fCustomerName == null ? "n/a" : data.fCustomerName),
                                                          isSalesBilling : (n == null ? "n/a" : n) //n
                                                      } });
        console.log('<factory> hasil => ', res);
        //res.data.Result = null;
        return res;
      },
      getDataApproval: function(data) {
        console.log("<factory> data for approval => ", data);
        //filter tanggal
        var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
        var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');

        var res = $http.get('/api/as/SalesBilling', {params: {
                                                          startDate: (data.startDate == null ? "n/a" : startDate),
                                                          endDate: (data.endDate == null ? "n/a" : endDate),
                                                          SalesOrderNo : (data.fSalesOrderNo == null ? "n/a" : data.fSalesOrderNo),
                                                          Bengkel : (data.fBengkel == null ? "n/a" : data.fBengkel),
                                                          CustomerName : (data.fCustomerName == null ? "n/a" : data.fCustomerName),
                                                          isSalesBilling : -1
                                                      } });
        console.log('<factory> hasil => ', res);
        //res.data.Result = null;
        return res;
      },
      getDetail: function(data){
        console.log("<factory> getData detail => ", data);
        console.log("<factory> SalesBillingId => ", data.SalesBillingId);
        var res=$http.get('/api/as/SalesBilling/SalesBillingItem', {params: {
                                                          SalesBillingId: (data.SalesBillingId == null ? 0 : data.SalesBillingId)
                                                        } });
        console.log('<factory> dataDetail =>',res);
        return res;
      },
      getDetailSO: function(data){
        console.log("<factory> getData detail SO => ", data);
        console.log("<factory> SalesOrderId => ", data.SalesOrderId);
        var res=$http.get('/api/as/SalesBilling/SalesBillingItemSO', {params: {
                                                          SalesOrderId: data.SalesOrderId
                                                        } });
        console.log('<factory> dataDetail =>',res);
        return res;
      },
      createApprove: function(data, detail) {
        console.log('create=>', data);
        console.log('detail=>', detail);
        var sementara = Math.floor((Math.random() * 10000000) + 1);
        //var vdocDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
        return $http.post('/api/as/SalesBilling', [{
                                            SalesBillingNo: (data.SalesBillingNo == null ? 0 : data.SalesBillingNo),
                                            OutletId: (data.OutletId == null ? 0 : data.OutletId),
                                            WarehouseId: (data.WarehouseId == null ? 0 :data.WarehouseId),
                                            DocDate: (data.DocDate == null ? "1901-01-01" : data.DocDate),
                                            SalesOrderId: (data.SalesOrderId == null ? 0 : data.SalesOrderId),
                                            // CustomerOutletId: (data.CustomerOutletId == null ? 0 :data.CustomerOutletId),
                                            CustomerOutletId: data.CustomerOutletId,
                                            // CustomerId: (data.CustomerId == null ? 0 : data.CustomerId),
                                            CustomerId: data.CustomerId,
                                            ToyotaId: (data.ToyotaId == null ? 0 : data.ToyotaId),
                                            PaymentMethodId: (data.PaymentMethodId == null ? 0 : data.PaymentMethodId),
                                            TotalAmount: (data.TotalAmount == null ? 0 : data.TotalAmount),
                                            PaymentPeriod: (data.PaymentPeriod == null ? 0 : data.PaymentPeriod),
                                            SalesBillingStatusId: (data.PurchaseOrderStatusId == null ? 1 : data.PurchaseOrderStatusId),
                                            Notes: (data.Notes == null ? 0 : data.Notes),
                                            VATAmount: (data.VATAmount == null ? 0 : data.VATAmount),
                                            AmountReceived: (data.TotalAmount == data.DPReceived ? data.TotalAmount : data.DPReceived),
                                            Paid: (data.TotalAmount <= data.DPReceived ? 1 : null),
                                            GridDetail: detail
                                          }]);
      },
      
      
      BatalBillingLangsung: function(data, detail){
        console.log('[BatalBillingLangsung] Update data => ', data);
        console.log('[BatalBillingLangsung] Update detail=> ', detail);
        return $http.put('/api/as/SalesBilling/CancelBillingLangsung', [{
                                            SalesBillingId: (data.SalesBillingId == null ? 0 : data.SalesBillingId),
                                            SalesBillingNo: data.SalesBillingNo,
                                            OutletId: data.OutletId,
                                            WarehouseId: data.WarehouseId,
                                            DocDate: data.DocDate,
                                            SalesOrderId: data.SalesOrderId,
                                            SalesOrderNo: data.SalesOrderNo,
                                            CustomerId: data.CustomerId,
                                            ToyotaId: data.ToyotaId,
                                            CustomerOutletId: data.CustomerOutletId,
                                            PaymentMethodId: data.PaymentMethodId,
                                            PaymentPeriod: data.PaymentPeriod,
                                            TotalAmount: data.TotalAmount,
                                            Notes: data.Notes,
                                            SalesBillingStatusId: data.SalesBillingStatusId,
                                            SalesBillingCancelNo: data.SalesBillingCancelNo,
                                            Bengkel: data.Bengkel,
                                            CustomerName: this.checkIsCharacter(data.CustomerName),
                                            DateNeeded: data.DateNeeded,
                                            Alamat: data.Alamat,
                                            Phone: data.Phone,
                                            SalesOrderDate: data.SalesOrderDate,
                                            SalesBillingStatusName: data.SalesBillingStatusName,
                                            CancelDate: data.CancelDate,
                                            SalesBillingCancelStatusId: data.SalesBillingCancelStatusId,
                                            SalesBillingCancelStatusName: data.SalesBillingCancelStatusName,
                                            VATAmount: data.VATAmount,
                                            ApprovalStatus : data.ApprovalStatus,
                                            CreatedDate: data.CreatedDate,
                                            CreatedUserId: data.CreatedUserId,
                                            GridDetail: [detail],
                                          }]);
      },

      cancel: function(data, detail){
        console.log('[Cancel] Update data => ', data);
        console.log('[Cancel] Update detail=> ', detail);
        return $http.put('/api/as/SalesBilling', [{
                                            SalesBillingId: (data.SalesBillingId == null ? 0 : data.SalesBillingId),
                                            SalesBillingNo: data.SalesBillingNo,
                                            OutletId: data.OutletId,
                                            WarehouseId: data.WarehouseId,
                                            DocDate: data.DocDate,
                                            SalesOrderId: data.SalesOrderId,
                                            SalesOrderNo: data.SalesOrderNo,
                                            CustomerId: data.CustomerId,
                                            ToyotaId: data.ToyotaId,
                                            CustomerOutletId: data.CustomerOutletId,
                                            PaymentMethodId: data.PaymentMethodId,
                                            PaymentPeriod: data.PaymentPeriod,
                                            TotalAmount: data.TotalAmount,
                                            Notes: data.Notes,
                                            SalesBillingStatusId: data.SalesBillingStatusId,
                                            SalesBillingCancelNo: data.SalesBillingCancelNo,
                                            Bengkel: data.Bengkel,
                                            // CustomerName: this.checkIsCharacter(data.CustomerName),
                                            DateNeeded: data.DateNeeded,
                                            Alamat: data.Alamat,
                                            Phone: data.Phone,
                                            SalesOrderDate: data.SalesOrderDate,
                                            SalesBillingStatusName: data.SalesBillingStatusName,
                                            CancelDate: data.CancelDate,
                                            SalesBillingCancelStatusId: data.SalesBillingCancelStatusId,
                                            SalesBillingCancelStatusName: data.SalesBillingCancelStatusName,
                                            VATAmount: data.VATAmount,
                                            GridDetail: detail,
                                            ApprovalStatus : data.ApprovalStatus,
                                            CreatedDate: data.CreatedDate,
                                            CreatedUserId: data.CreatedUserId
                                          }]);
      },
      //[Route("api/as/SalesBilling/ActApprovalBatal")]
      actApprovalBatal: function (data) {
        console.log("Data approval : ", data);
        var res = $http.get('/api/as/SalesBilling/ActApprovalBatal', {params: {
               ProcessId: data.ProcessId, DataId: data.DataId, ApprovalStatus: data.ApprovalStatus
               }
        });
        return res;
      },
      ActApproval: function(data) {
        console.log("Param Data for Approve : ", data);
        var res=$http.get('/api/as/PartGlobal/SBActApproval', {params: {DataId : data.SalesBillingId,
                                      ApprovalStatus : data.ApprovalStatus,
                                      ListSBIds : data.ListDataIds
                                      } });
        console.log('res.=>',res);
        return res;
      },
      sendNotify: function(mData) {
        console.log("data notif", mData[0].SalesBillingNo);
        // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
        return $http.post('/api/as/SendNotificationForRole', [{
            Message: '"Hasil Pengajuan Approval Batal Sales Billing Anda Ditolak ' + mData[0].SalesBillingNo + '"', // id sementara hardcode
            // RecepientId: 1122,  //role id partsman GR
            RecepientId: 1135,  //role id Parts Sales
            Param : 38
        }]);
      },
      sendNotifikasi: function(data) {
        return $http.post('/api/as/SendNotificationForRole', [{
            Message: data.Message,
            RecepientId: data.RecepientId,
            Param :data.Param // 38 - 39
        }]);
      },
      create: function(data, detail){
        console.log('[Create] Update data => ', data);
        console.log('[Create] Update detail=> ', detail);
        return $http.put('/api/as/SalesBilling', [{
                                            SalesBillingId: (data.SalesBillingId == null ? 0 : data.SalesBillingId),
                                            SalesBillingNo: data.SalesBillingNo,
                                            OutletId: data.OutletId,
                                            WarehouseId: data.WarehouseId,
                                            DocDate: data.DocDate,
                                            SalesOrderId: data.SalesOrderId,
                                            SalesOrderNo: data.SalesOrderNo,
                                            CustomerId: data.CustomerId,
                                            ToyotaId: data.ToyotaId,
                                            CustomerOutletId: data.CustomerOutletId,
                                            PaymentMethodId: data.PaymentMethodId,
                                            PaymentPeriod: data.PaymentPeriod,
                                            TotalAmount: data.TotalAmount,
                                            Notes: data.Notes,
                                            SalesBillingStatusId: data.SalesBillingStatusId,
                                            SalesBillingCancelNo: data.SalesBillingCancelNo,
                                            Bengkel: data.Bengkel,
                                            CustomerName: this.checkIsCharacter(data.CustomerName),
                                            DateNeeded: data.DateNeeded,
                                            Alamat: data.Alamat,
                                            Phone: data.Phone,
                                            SalesOrderDate: data.SalesOrderDate,
                                            SalesBillingStatusName: data.SalesBillingStatusName,
                                            // CancelDate: data.CancelDate,
                                            SalesBillingCancelStatusName: data.SalesBillingCancelStatusName,
                                            VATAmount: data.VATAmount,
                                            GridDetail: detail
                                          }]);
      },
      SearchSOInGI: function(data) {
        console.log("<factory> SearchSOInGI data.SalesOrderNo => ", data.SalesOrderNo);
        var res = $http.get('/api/as/SalesBilling/IsGoodIssue', {params: {
                                                                SalesOrderNo : data.SalesOrderNo || data.SalesBillingNo // == null ? 0 : data.SalesOrderNo)
                                                                }});
        console.log('<factory> SearchSOInGI - hasil => ', res);
        return res;
      },
      setSalesBillingHeader : function(data){
        SalesBillingHeader = data;
      },
      getSalesBillingHeader : function(){
        return SalesBillingHeader;
      },
      setSalesBillingDetail : function(data){
        SalesBillingDetail = data;
      },
      getSalesBillingDetail : function(){
        return SalesBillingDetail;
      },
      checkIsCharacter: function(data){
        console.log("[SB-Factory] checkIsCharacter==>",data);
        var tempdata;
        if(typeof data == 'string'){
            tempdata = data.replace(/[^\x20-\x7f\xA\xD]/g, '');
        }
        return tempdata;
      },
      CekStatusSalesBilling: function(data) {
        return $http.put('/api/as/CekStatusSalesBilling/', data);
      },
      CheckValdationSalesBillingCancelApproval: function(data) {
        // api/as/CheckValdationBillingCancelApproval/{BillingId}
        return $http.put('/api/as/CheckValidationSalesBillingCancelApproval/', data); // kiriman billingid na bentuk array karena bs sekaligus print banyak
      },
      CheckInvoiceInfoForSalesBilling: function(data) {
        // api/as/CheckValdationBillingCancelApproval/{BillingId}
        return $http.put('/api/as/GetInvoiceInfoForSalesBilling/', data); // kiriman billingid na bentuk array karena bs sekaligus print banyak
      },
      ApprovalReprintSalesBilling: function(data) {
        
        return $http.post('/api/as/ApprovalReprintSalesBilling', [
          {
            OutletId : data.OutletId,
            TransactionId : data.TransactionId,
            SalesBillingId : data.SalesBillingId,
            IsGR : 0,
            GroupNo : 0,
            ApprovalCategoryId : 0,
            seq : 0,
            ApproverId : 0,
            RequesterId : 0,
            ApproverRoleId : data.ApproverRoleId,
            RequestorRoleId : data.RoleId,
            StatusApprovalId : data.StatusApprovalId,
            RequestReason : data.Message,
            RequestDate : data.date,
            Reason : data.Reason,
            ApproveRejectDate: data.ApproveRejectDate,
            StatusCode : 0,
            LastModifiedDate : data.date,
            LastModifiedUserId : 0
            }
        ]);
      },
      GetInfoSalesBillingApprove: function(id,trnsId) {
        console.log(id)
        return $http.get('/api/as/SalesBilling/'+ id +'/'+ trnsId);
      },
      UpdateApprovalReprintSalesBilling: function(data) {
        
        return $http.put('/api/as/UpdateApprovalReprintSalesBilling', [
          {
            OutletId : data.OutletId,
            TransactionId : data.TransactionId,
            SalesBillingId : data.SalesBillingId,
            IsGR : 0,
            GroupNo : 0,
            ApprovalCategoryId : 0,
            seq : 0,
            ApproverId : 0,
            RequesterId : 0,
            ApproverRoleId : data.ApproverRoleId,
            RequestorRoleId : data.RoleId,
            StatusApprovalId : data.StatusApprovalId,
            RequestReason : data.Message,
            RequestDate : data.date,
            Reason : data.Reason,
            ApproveRejectDate: data.ApproveRejectDate,
            StatusCode : 0,
            LastModifiedDate : data.date,
            LastModifiedUserId : 0
            }
        ]);
      },
    }
  });
