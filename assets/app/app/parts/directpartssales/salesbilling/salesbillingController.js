angular.module('app')
  .controller('PartsSalesBillingController', function($scope, $http, $filter, CurrentUser, PartsSalesBilling, ngDialog, $timeout, //parApproval
    PartsGlobal, $state, bsNotify, bsTab, bsAlert,PartsCurrentUser,PartsSalesOrder,uiGridConstants,PrintRpt) {
    //Options Switch
    $scope.user = CurrentUser.user();
    $scope.items = ['PO Bengkel Lain', 'Customer Langsung'];
    $scope.selection = $scope.items[0];
    //Tombol disable ketika blm GI
    $scope.AlreadyGI = false;
    //$scope.disableSelect = false;
    $scope.actionVisible = false;
    $scope.mySelections = [];
    $scope.gridHideActionColumn= true;
    $scope.newSalesBill = false;
    $scope.batalSalesBill = false;
    $scope.SBHeader = PartsSalesBilling.getSBHeader();
    $scope.ApprovalProcessId = 8040;
    $scope.BtnSimpann = false;
    $scope.LoadingData = true;

    $scope.isOverlayForm = false;
    $scope.dsoTampil = [
      {id:'1', name:'Outstanding Sales Order (Belum sales billing)'},
      {id:'2', name:'Sales Order (Sudah sales billing)'},
      // {id:'3', name:'List Data Reprint Billing'}
    ];

    $scope.isKabeng = false;
    if($scope.user.RoleId == 1128 || $scope.user.RoleId == 1129){
      $scope.dsoTampil = [
        {id:'1', name:'Outstanding Sales Order (Belum sales billing)'},
        {id:'2', name:'Sales Order (Sudah sales billing)'},
        {id:'3', name:'List Data Reprint Billing'}
      ];
      $scope.isKabeng = true;
    }else{
      $scope.dsoTampil = [
        {id:'1', name:'Outstanding Sales Order (Belum sales billing)'},
        {id:'2', name:'Sales Order (Sudah sales billing)'}
      ];
      $scope.isKabeng = false;
    }

    $scope.checkRights = function(bit) {
      console.log("Rights : ", $scope.myRights);
      var p = $scope.myRights & Math.pow(2, bit);
      var res = (p == Math.pow(2, bit));
      console.log("myRights => ", $scope.myRights);
      return res;
    }
    $scope.getRightX = function(a, b) {
        var i = 0;
        if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
        else if ($scope.checkRights(b)) i = 2;
        else if ($scope.checkRights(a)) i = 1;
        return i;
    }
    $scope.getRightMaterialType = function() {
        PartsSalesOrder.getserviceandmaterial().then(function(res) {
            $scope.materialtypes = res.data[0].MaterialType;
        });
        var materialTypeId = $scope.materialtypes;
        return materialTypeId;
    }
    $scope.getRightServiceType = function() {
        PartsSalesOrder.getserviceandmaterial().then(function(res) {
            $scope.servicetypes = res.data[0].Servicetype;
        });
        var serviceTypeId =  $scope.servicetypes;
        return serviceTypeId;
    }
    $scope.localeDate = function(data) {
      var tmpDate = new Date(data);
      var resDate = new Date(tmpDate.toISOString().replace("Z","-0700")).toISOString(); 
      return resDate;
    }

    // 1.  Outstanding Sales Order (Belum sales biling)
    // 2.  Sales Order (Sudah sales billing)

    var dateFormat = 'dd/MM/yyyy';
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
    };
    //$scope.ref = '';
    $scope.mydsoTampil= $scope.dsoTampil[0];//'';
    $scope.isOutstandingSO = true;
    $scope.isBengkel= false;
    $scope.disablebtn = true;
    $scope.isViewForm = false;
    $scope.btnCaption='Buat Sales Billing Baru';
    var SB = PartsGlobal.getSB();
    $scope.viewBL= false;
    $scope.viewCL= false;
    $scope.batalBL= false;
    $scope.batalCL= false;
    $scope.cancelledDoc =false;
    $scope.formApi = {};


    $scope.optPaymentMethod = [
      {
          id: 201,
          name: 'Bank Transfer'
      },
      {
          id: 202,
          name: 'Cash'
      },
      {
          id: 203,
          name: 'Credit/Debit Card'
      }
      ];

     $scope.onRegisterApi = function(gridApi) {
        $scope.myGridApi = gridApi;
      };

    $scope.afterRegisterGridApi = function(gridApi){
        console.log("grid apik>>>",gridApi);
    }

    $scope.Alasan = {};
    $scope.BatalData = PartsSalesBilling.getSalesBillingHeader();
    $scope.BatalDataDetail = PartsSalesBilling.getSalesBillingDetail();

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
      $scope.loading = false;
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.IsApproval = true;
    console.log("user=>",$scope.user);
    //console.log("mode =>");

    $scope.mData = {}; //Model
    $scope.mData.dataTanggal = "Sales Order";
    $scope.xRole = {
      selected: []
    };

    $scope.ngDialog = ngDialog;
    //button kembali pada overlay

    $scope.ApproveData = PartsSalesBilling.getSalesBillingHeader();
    $scope.ApproveDataDetail = PartsSalesBilling.getSalesBillingDetail();
    $scope.getSBCancelStatusName= function(idstatus) {
    var statusName = ""
      switch (idstatus) {
        case 1: statusName= "Aju Batal Billing" ; break;
        case 2: statusName= "Rejected" ; break;
        case 3: statusName= "Approved" ; break;
      }
      return statusName;
    }
    $scope.setNewStatus = function(newStatus) {
      //$scope.MIPStatus= PartsLookup.lkp_MIPStatusDocName(newStatus);
      $scope.mData['SalesBillingCancelStatusId'] = newStatus; // 1=Aju Batal Billing ; 2= Rejected; 3= Approved
      $scope.mData['SalesBillingCancelStatusName'] = $scope.getSBCancelStatusName(newStatus);
    }

    $scope.goBack = function()
    {
      $scope.isOverlayForm = false;
      $scope.viewBL= false;
      $scope.viewCL= false;
      $scope.batalBL= false;
      $scope.batalCL= false;
      $scope.isViewForm = false;
      //$scope.batalSalesBill = false;
      //$scope.gridMode = true;
      //$scope.formApi.setMode("grid");
      console.log('goBack');
      //$scope.formApi.setMode("grid");
      //$scope.selection = [];
    }
    $scope.goBackToMe = function(nocancelso)
    {
      $scope.isOverlayForm = false;
      $scope.viewBL= false;
      $scope.viewCL= false;
      $scope.batalBL= false;
      $scope.batalCL= false;
      $scope.isViewForm = false;
      $scope.mData.startDate = $scope.startDate;
      $scope.mData.endDate = $scope.endDate;
      $scope.getData($scope.mData);
      //$scope.batalSalesBill = false;
      //$scope.gridMode = true;
      //$scope.formApi.setMode("grid");
      console.log('goBack');
      //$scope.formApi.setMode("grid");
      //$scope.selection = [];
        bsAlert.alert({
          title:  "Data dengan nomor billing " + $scope.mData.SalesBillingNo + " sudah dibatalkan dengan nomor pembatalan ",
          text:  nocancelso,
          type: "success",
          showCancelButton: false
      });
    }
    $scope.goBack0 = function()
    {
      // $scope.formApi.setMode("grid");
      bsTab.add('app.parts_salesbilling');
    }
    $scope.goBack2 = function()
    {
      // PartsSalesBilling.formApi.setMode("grid");
      $scope.isOverlayForm = false;
      $scope.viewBL= false;
      $scope.viewCL= false;
      $scope.batalBL= false;
      $scope.batalCL= false;
      $scope.isViewForm = false;
      console.log('goBack Second Version');
      $scope.cancelledDoc = ($scope.mData['SalesBillingCancelStatusId'] == 1 || $scope.mData['SalesBillingCancelStatusId'] == 3);
      // $scope.formApi.setMode("grid");
      $scope.goBack0();
    }
    $scope.goBack3 = function()
    {
      $state.go('app');
      $scope.goBack4();
    }
    $scope.goBack4 = function()
    {
      $state.go('app.parts_salesbilling');
    }
    $scope.changedValue = function (item){
        //console.log('changedValue item = ', item);
        console.log('pil ', $scope.mydsoTampil);
        //alert('berubah');
        $scope.isOutstandingSO = item.id == 1;
        var n = null;
        console.log('$scope.isOutstandingSO = ', $scope.isOutstandingSO);
        $scope.IsApproval = false;
        if ($scope.isOutstandingSO) {
          $scope.mData.dataTanggal = "Sales Order";
        } else {
          $scope.mData.dataTanggal = "Sales Billing";
        }
        $scope.mydsoTampil = item.id;
        console.log('mydsoTampil ', $scope.mydsoTampil);
    }

    $scope.onBulkApproveSB= function(mData) {
      if($scope.mydsoTampil != 3){
        console.log("mData : ", mData);
        console.log("di approve ");
        var TidakValid = 0;
        for (var i=0; i<mData.length; i++) {
          if (mData[i].SalesBillingCancelStatusId != 1) {
            TidakValid++;
          }
        }

        if (TidakValid > 0){
          bsNotify.show({
            title: "Approval",
            content: "Ada data sales billing yang tidak dalam pengajuan cancel. Silahkan dicek kembali",
            type: 'danger'
          });
          return

        } else {
          console.log('ok boleh lanjut')
          if (mData[0].SalesBillingCancelStatusId == 1) {
            var ListIds = ';';
            for (var i=0; i<mData.length; i++) {
              ListIds += mData[i].SalesBillingId +';';
            }
            if (ListIds == ';') {ListIds = null ;
              alert("No selected data to Approve");
              return 0;
            };
            console.log("Data Id : ", ListIds);
            var notif = {
              Message: "Permohonan batal SO Billing " + mData[0].SalesBillingNo + " Disetujui",
              RecepientId: 1135,
              Param: 39
            }
            console.log(notif);
            PartsSalesBilling.sendNotifikasi(notif).then(function (res) {});
            PartsSalesBilling.ActApproval({SalesBillingId: 0, ApprovalStatus: 1, //ProcessId : 8040,
                                        ListDataIds: ListIds}).then(function(res) {
              if (mData[0].SalesBillingCancelNo != undefined || mData[0].SalesBillingCancelNo != null )
              {
                bsNotify.show({
                  title: "Approval",
                  content: "Cancel Sales Billing No. "+ mData[0].SalesBillingCancelNo +" has been successfully approved.",
                  type: 'success'
                });
                $scope.goBack0();
              }
            });
          } else if (mData[0].SalesBillingCancelStatusId == 3) {
            bsNotify.show({
              title: "Approval",
              content: "Sales Billing sudah di-cancel.",
              type: 'danger'
            });
          } else if (mData[0].SalesBillingCancelStatusId == 2) {
            bsNotify.show({
              title: "Approval",
              content: "Cancel Sales Billing sudah di-reject.",
              type: 'danger'
            });
          } else if (mData[0].SalesBillingCancelStatusId == null) {
            bsNotify.show({
              title: "Approval",
              content: "Cancel Sales Billing belum diajukan.",
              type: 'danger'
            });
          } 
        }
      }else{
        if(mData.length > 1){
          bsNotify.show({
            title: "Approval",
            content: "Tidak Boleh Multiple Data. Silahkan dicek kembali",
            type: 'danger'
          });
          return
        }
        $scope.DataReprint = [];
        $scope.DataReprint = mData;

        $scope.isReprintedStatus = 3; //1: ReqReprint 3:ApproveReprint 2:RejectReprint
        console.log('Selected', mData[0].SalesBillingId)
        PartsSalesBilling.GetInfoSalesBillingApprove(mData[0].SalesBillingId, mData[0].TransactionId).then(function(res) {
          $scope.mDataReqReprint = res.data.Result[0];
          var date = new Date();
          $scope.mDataReqReprint.DocDate = date.getDate() + '/' + (date.getMonth()+1) + '/' +  date.getFullYear() +' ' + date.getHours()+':'+date.getMinutes();
          $scope.mDataReqReprint.Approved = $scope.mDataReqReprint.ApproverName + ' ('+$scope.mDataReqReprint.ApproverRoleName +')';
          $scope.show_modalReprint = { show: true };

        });
      }
    }

    $scope.onBulkRejectSB= function(mData) {

      if($scope.mydsoTampil != 3){
        console.log("mData : ", mData);
        console.log("di approve ");
        var TidakValid = 0;
        for (var i=0; i<mData.length; i++) {
          if (mData[i].SalesBillingCancelStatusId != 1) {
            TidakValid++;
          }
        }
  
        if (TidakValid > 0){
          bsNotify.show({
            title: "Approval",
            content: "Ada data sales billing yang tidak dalam pengajuan cancel. Silahkan dicek kembali",
            type: 'danger'
          });
          return
  
        } else {
          console.log('ok boleh lanjut')
          if (mData[0].SalesBillingCancelStatusId == 1) {
            var notif = {
              Message: "Permohonan batal SO Billing " + mData[0].SalesBillingNo + " Ditolak",
              RecepientId: 1135,
              Param: 39
            }
            console.log(notif);
            PartsSalesBilling.sendNotifikasi(notif).then(function (res) {});
              bsNotify.show({
                title: "Approval",
                content: "Cancel Sales Billing has been successfully rejected.",
                type: 'success'
              });
            console.log("Reject mData ", mData);
            var ListIds = ';';
            for (var i=0; i<mData.length; i++) {
              ListIds += mData[i].SalesBillingId +';';
            }
            if (ListIds == ';') {ListIds = null ;
              alert("No selected data to Reject");
              return 0;
            };
            console.log("Data Id : ", ListIds);
            PartsSalesBilling.ActApproval({SalesBillingId: 0, ApprovalStatus: 2, //ProcessId : 8040,
                                        ListDataIds: ListIds}).then(function(res) {
              if (mData[0].SalesBillingCancelNo != undefined || mData[0].SalesBillingCancelNo != null )
              {
                bsNotify.show({
                  title: "Approval",
                  content: "Cancel Sales Billing No. "+ mData[0].SalesBillingCancelNo +" has been successfully rejected.",
                  type: 'success'
                });
                $scope.goBack0();
              }
            });
          } else if (mData[0].SalesBillingCancelStatusId == 3) {
            bsNotify.show({
              title: "Approval",
              content: "Sales Billing sudah di-cancel.",
              type: 'danger'
            });
          } else if (mData[0].SalesBillingCancelStatusId == 2) {
            bsNotify.show({
              title: "Approval",
              content: "Cancel Sales Billing sudah di-reject.",
              type: 'danger'
            });
          } else if (mData[0].SalesBillingCancelStatusId == null) {
            bsNotify.show({
              title: "Approval",
              content: "Cancel Sales Billing belum diajukan.",
              type: 'danger'
            });
          } 
  
        }
      }else{
        if(mData.length > 1){
          bsNotify.show({
            title: "Approval",
            content: "Tidak Boleh Multiple Data. Silahkan dicek kembali",
            type: 'danger'
          });
          return
        }

        $scope.DataReprint = [];
        $scope.DataReprint = mData;

        $scope.isReprintedStatus = 2;//1: ReqReprint 3:ApproveReprint 2:RejectReprint
        console.log('Selected', mData)
        PartsSalesBilling.GetInfoSalesBillingApprove(mData[0].SalesBillingId, mData[0].TransactionId).then(function(res) {
          $scope.mDataReqReprint = res.data.Result[0];
          var date = new Date();
          $scope.mDataReqReprint.DocDate = date.getDate() + '/' + (date.getMonth()+1) + '/' +  date.getFullYear() +' ' + date.getHours()+':'+date.getMinutes();
          $scope.mDataReqReprint.Approved = $scope.mDataReqReprint.ApproverName + ' ('+$scope.mDataReqReprint.ApproverRoleName +')';
          $scope.show_modalReprint = { show: true };

        });
      }
    }

    $scope.onBulkRowApproveSB= function(Data) {
      console.log("Approve Row ", Data);
      // PartsGlobal.actApproval({
      PartsSalesBilling.actApprovalBatal({
        ProcessId: $scope.ApprovalProcessId, DataId: Data.SalesBillingId, ApprovalStatus: 1
      })
    }
    $scope.onBulkRowRejectSB= function(Data) {
      console.log("Reject Row", Data);
      // PartsGlobal.actApproval({
      PartsSalesBilling.actApprovalBatal({
        ProcessId: $scope.ApprovalProcessId, DataId: Data.SalesBillingId, ApprovalStatus: 2
      })
    }

    $scope.localeDate = function(data) {
      var tmpDate = new Date(data);
      var resDate = new Date(tmpDate.toISOString().replace("Z","-0700")).toISOString(); 
      return resDate;
    }
  
    //----------------------------------
    // Get Data
    //----------------------------------
    //var gridData = [];

    $scope.getData = function(mData) {
      $scope.IsApproval = false;
      console.log("getData mData1 = ", $scope.mData);
      $scope.startDate = $scope.mData.startDate;
      $scope.endDate = $scope.mData.endDate;
      // combobox - data SO yang ditampilkan
      // if (mData.isSalesBilling == 2) {
      //   $scope.grid.data.length = 0;
      //   $scope.grid.columnDefs = $scope.columnsBSB;
      //   $scope.btnCaption= 'Buat Sales Billing Baru';
      // } else {
      //   $scope.grid.data.length = 0;
      //   $scope.grid.columnDefs = $scope.columnsSSB;
      //   $scope.btnCaption= 'Batal Billing';
      // }
      var n = 0;
      //$scope.isOutstandingSO = {};
      $scope.grid.columnDefs=[];

      //var n = 0;
      // $scope.gridApi.grid.refresh();
      // $scope.grid.columnDefs = new Array();
      if ($scope.isOutstandingSO) {
        console.log('isOutstandingSO true = ', $scope.isOutstandingSO);
        $scope.grid.columnDefs = $scope.columnsBSB;
        //$scope.grid.columnDefs = $scope.columnsSSB;
        //$scope.btnCaption= 'Buat Sales Billing Baru';
        n = 1;
      } else {
        console.log('isOutstandingSO else = ', $scope.isOutstandingSO);
        $scope.grid.columnDefs = $scope.columnsSSB2;
        $scope.grid.columnDefs[9].visible = false;
        // $scope.mData.dataTanggal = "Sales Billing";
        //$scope.grid.columnDefs = $scope.columnsBSB;
        //$scope.btnCaption= 'Batal Billing';
        n = 2;
      }
  
      if($scope.mydsoTampil == 3){
        $scope.grid.columnDefs = $scope.columnsSSB2;
        $scope.grid.columnDefs[9].visible = true;
        n = 3;
      }

      var gridData = [];
      //} // changed value
      // getdata
      PartsSalesBilling.getData(mData, n).then(function(res) {
          console.log('isOutstandingSO di getData = ', $scope.isOutstandingSO);       
          // if ($scope.isOutstandingSO) {
          //   var gridData = res.data.Result;
          // } else {
          //   var gridData = res.data;
          // }
          gridData = res.data.Result;
          //var gridData = res.data;
          console.log("<controller getData> GridData => ", gridData);
          // if(gridData.length > 0){
          //   $scope.grid.data = gridData;
          //   //$scope.loading = false;
          // } else {

          for (var i in gridData) {
            gridData[i]["CreatedDate"] = $scope.localeDate(gridData[i]["CreatedDate"]);
            gridData[i]["DocDate"] = $scope.localeDate(gridData[i]["DocDate"]);
            console.log("gridData i =>",i);
            console.log("gridData i =>",gridData[i]["CreatedDate"]);
            console.log("gridData i =>",gridData[i]["DocDate"]);
          }
    
          // }
          // for (var i in gridData) {
          //   console.log("gridData[i].CreatedDate=>",gridData[i].CreatedDate);
          //   gridData[i].CreatedDate = $scope.localeDate(gridData[i].CreatedDate);
          // }

          console.log("$scope.grid.data before=>",$scope.grid.data);
          console.log("gridData before=>",gridData);
          console.log("$scope.mData before=>",$scope.mData);
          // //$scope.grid.data = gridData;
          $scope.grid.data = angular.copy(gridData);
          $scope.mData = angular.copy(gridData);
          $scope.mData.startDate = $scope.startDate;
          $scope.mData.endDate = $scope.endDate;
          $scope.loading = false;
          // $scope.IsApproval = false;
          console.log("$scope.grid.data after=>",$scope.grid.data);
          console.log("$scope.mData after=>",$scope.mData);

        },
        function(err) {
          console.log("err=>", err);
        }
      );

      console.log("<controller getData> GridData bawah => ", gridData);

    }
    $scope.getDataApproval = function(mData) {
      $scope.IsApproval = true;
      console.log("getData mData = ", $scope.mData);
      PartsSalesBilling.getDataApproval(mData).then(function(res) {
        console.log('isOutstandingSO di getData = ', $scope.isOutstandingSO);
          var gridData = res.data.Result;
          console.log("<controller getDataApproval> GridData => ", gridData);
          $scope.grid.data = gridData;
          $scope.loading = false;
          // $scope.IsApproval = true;
        },
        function(err) {
          console.log("err=>", err);
        }
      );
    }
    $scope.getCellTempForSSB= function() {
      var sCellTmp= '';
      if ($scope.IsApproval) {
        sCellTmp= '<div style="text-align:center"><button class="btn btn-xs" style="color:white; background-color:#D53337" ng-click="grid.appScope.$parent.onBulkRowApproveSB(row.entity)">Approve </button>\
           <button class="btn btn-xs" style="color:white; background-color:#D53337" ng-click="grid.appScope.$parent.onBulkRowRejectSB(row.entity)">Reject </button> </div>';
          console.log("approval");
          $scope.IsApproval = false;
      } else {
        sCellTmp= '<div style="text-align:center"> <button class="btn btn-xs" style="color:white; background-color:#D53337" ng-click="grid.appScope.$parent.btnList0Click(row)">Batal Billing</button></div>';
        console.log("NON approval");
      }
      return sCellTmp;
    }

    // Hapus Filter
    $scope.onDeleteFilter = function(){
      //Pengkondisian supaya seolaholah kosong
      $scope.mData.startDate = null;
      $scope.mData.endDate = null;
      $scope.mData.fSalesOrderNo = null;
      $scope.mData.fBengkel = null;
      $scope.mData.fCustomerName = null;
      $scope.mydsoTampil = null;
      console.log("pil",$scope.mData);

    }
    $scope.alertAfterSave = function(item) {
      var nosb = item.ResponseMessage;
      nosb = nosb.substring(nosb.lastIndexOf('#')+1);
        bsAlert.alert({
                title: "Data tersimpan dengan Nomor Sales Billing",
                text: nosb,
                type: "success",
                showCancelButton: false
            },
            function() {
                // $scope.formApi.setMode('grid');
                // $scope.grid.data=[];
                // $scope.goBack();
            },
            function() {

            }
        )
    }
    // $scope.gridDataTree=[];
    // $scope.getData = function() {
    //   // if (scope.mydsoTampil.id==0) {
    //   //    alert('Belum Sales Billing');
    //   // }else {
    //   //   alert('Sudah Sales Billing');
    //   // };
    //   console.log("pilihan : => ", $scope.mydsoTampil);
    //   if ($scope.isOutstandingSO) {
    //     $scope.data = [
    //           { "id":"1",
    //             "noSalesOrder": "SO/20160717010",
    //             "tanggalSalesOrder": "16/Juli/2016",
    //             // "referensiSalesOrder": "Bengkel Lain",
    //             // "noPO": "PO/20160717001",
    //             "bengkel": "T053 - Toyota B&P Yasmin",
    //             "customer": "-"
    //           },
    //           { "id":"2",
    //             "noSalesOrder": "SO/2016071709",
    //             "tanggalSalesOrder": "16/Juli/2016",
    //             // "referensiSalesOrder": "Penjualan Langsung",
    //             // "noPO": "-",
    //             "bengkel": "-",
    //             "customer": "CST00211 - Rio"
    //           },
    //           { "id":"3",
    //             "noSalesOrder": "SO/2016071708",
    //             "tanggalSalesOrder": "16/Juli/2016",
    //             // "referensiSalesOrder": "Bengkel Lain",
    //             // "noPO": "PO/20160717011",
    //             "bengkel": "T053 - Toyota B&P Yasmin",
    //             "customer": "-"
    //           }
    //       ];
    //     } else {
    //       $scope.data = [
    //           { "id":"1",
    //             "noSalesBilling": "12",
    //             "noSalesOrder": "SO/20160717010",
    //             "tanggalSalesOrder": "16/Juli/2016",
    //             // "referensiSalesOrder": "Bengkel Lain",
    //             // "noPO": "PO/20160717001",
    //             "bengkel": "T053 - Toyota B&P Yasmin",
    //             "customer": "-"
    //           },
    //           { "id":"2",
    //             "noSalesBilling": "13",
    //             "noSalesOrder": "SO/2016071709",
    //             "tanggalSalesOrder": "16/Juli/2016",
    //             // "referensiSalesOrder": "Penjualan Langsung",
    //             // "noPO": "-",
    //             "bengkel": "GR - Gading",
    //             "customer": "-"
    //           },
    //           { "id":"3",
    //             "noSalesBilling": "15",
    //             "noSalesOrder": "SO/2016071708",
    //             "tanggalSalesOrder": "16/Juli/2016",
    //             // "referensiSalesOrder": "Bengkel Lain",
    //             // "noPO": "PO/20160717011",
    //             "bengkel": "-",
    //             "customer": "Special Customer"
    //           }
    //       ];
    //     }
    //     $scope.grid.data = $scope.data;

    //     $scope.loading = false;
    // }

    $scope.simpan = function(isPrint){
      $scope.DisableCekGI = true;
      $scope.mData.GridDetail = $scope.gridOptions.data;
      if ($scope.mData.GridDetail == null || $scope.mData.GridDetail == '' || $scope.mData.GridDetail == undefined ) {
        bsNotify.show(
              {
                  title: "Sales Billing",
                 content: "Terdapat Masalah Koneksi, Harap Lakukan Refresh",
                 type: 'Warning'
             });
             setTimeout(function(){
               $scope.DisableCekGI = false;
           },1000);
           return 0;
     }
      PartsSalesBilling.setSalesBillingHeader($scope.mData);
      PartsSalesBilling.setSalesBillingDetail($scope.mData.GridDetail);

      console.log('mdata', $scope.mData);
      var tglData = $scope.mData.dataTanggal ;
      PartsSalesBilling.createApprove($scope.mData, $scope.mData.GridDetail).then(function(res) {
            var create = res.data;
            console.log('res.data = ', res.data);

            if (create.ResponseCode == 100001) {
              bsNotify.show({
                  title: "Sales Billing",
                  // content: tmpSalesOrderId[1],
                  content: "Sales Order tersebut sudah dibuatkan Sales Billing !!",
                  type: 'danger'
              });
            } else {
              // bsNotify.show(
              //   {
              //       title: "Sales Billing",
              //       content: "Data berhasil disimpan ----->",
              //       type: 'success'
              //   }
              // );

              setTimeout(function(){
                $scope.DisableCekGI = false;
              },1000);
              $scope.alertAfterSave(create);
              $scope.goBack();
              $scope.mData.startDate = $scope.startDate;
              $scope.mData.endDate = $scope.endDate;
              $scope.getData($scope.mData);
              $scope.mData.dataTanggal = tglData;
              if(isPrint == 1){ //Simpan & Cetak
                if($scope.mData.RefSOTypeId == 2){$scope.CetakSalesBillingCustLangsung(create,false)} //Cetak sales billing untuk type customerlangsung
              }
            }
          },
          function(err) {
            console.log("err=>", err);
          }
        );
      // ngDialog.openConfirm ({
      //   template:'<div ng-include=\"\'app/parts/directpartssales/salesbilling/helper/approval.html\'\"></div>',
      //   plain: true,
      //   controller: 'PartsSalesBillingController',
      //  });
    };

    $scope.CetakSalesBillingCustLangsung = function(data,isView){
      var SbId;
      if(!isView){
          SbId = data.ResponseMessage;
          SbId = SbId.substring(SbId.indexOf('[')+1, SbId.lastIndexOf("]"));
      }else{
          SbId = data;
      }

      $scope.printSalesBilling = 'as/PrintSalesBillingParts/' + SbId;
      var pdfFile = null;

      PrintRpt.print($scope.printSalesBilling).success(function(res) {
          var file = new Blob([res], { type: 'application/pdf' });
          var fileURL = URL.createObjectURL(file);

          console.log("pdf", fileURL);
          //$scope.content = $sce.trustAsResourceUrl(fileURL);
          pdfFile = fileURL;

          if (pdfFile != null) {
              //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame
              var ua = navigator.userAgent;
              if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                  var link = document.createElement('a');
                  link.href = fileURL;
                  //link.download="erlangga_file.pdf";
                  link.click();
              }
              else {
                  printJS(pdfFile);
              }
          }
          else
              console.log("error cetakan", pdfFile);
      }).error(function(res) {
          console.log("error cetakan", pdfFile);
      });
      $scope.getData($scope.mData);
    }

    $scope.CetakSalesBatalBillingCustLangsung = function(SbId){

      $scope.printSalesBilling = 'as/PrintBatalBillingParts/' + SbId;
      var pdfFile = null;

      PrintRpt.print($scope.printSalesBilling).success(function(res) {
          var file = new Blob([res], { type: 'application/pdf' });
          var fileURL = URL.createObjectURL(file);

          console.log("pdf", fileURL);
          //$scope.content = $sce.trustAsResourceUrl(fileURL);
          pdfFile = fileURL;

          if (pdfFile != null) {
              //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame
              var ua = navigator.userAgent;
              if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                  var link = document.createElement('a');
                  link.href = fileURL;
                  //link.download="erlangga_file.pdf";
                  link.click();
              }
              else {
                  printJS(pdfFile);
              }
          }
          else
              console.log("error cetakan", pdfFile);
      }).error(function(res) {
          console.log("error cetakan", pdfFile);
      });
    }

    // ======== tombol  simpan =========
    $scope.SimpanApproval = function(data)
    {
      console.log("Data untuk Approval ", data);

      $scope.mData = PartsSalesBilling.getSalesBillingHeader();
      $scope.mData.GridDetail = PartsSalesBilling.getSalesBillingDetail();

      console.log("mData => ", $scope.mData);
      console.log("mData Detail=> ", $scope.mData.GridDetail);

      for(var i = 0; i < $scope.mData.GridDetail.length; i++)
      {
        $scope.mData.GridDetail[i].SalesBillingStatusId = 2; //for approval
        $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
        //$scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;
      }

      PartsSalesBilling.createApprove($scope.mData, $scope.mData.GridDetail).then(function(res) {
            var create = res.data;
            console.log('res.data = ', res.data);
            $scope.goBackToMe($scope.mData.CancelNo);
          },
          function(err) {
            console.log("err=>", err);
          }
        );

      $scope.ngDialog.close();

    }

    $scope.onSelectRows = function(rows) {
        console.log("onSelectRows =>", rows);
        //console.log("rows[0].Bengkel =>", rows[0].Bengkel);
        $scope.testmodel = rows;
        //$scope.isBengkel = false;

        //$scope.mData = rows[0];

        if($scope.mData == undefined){
          console.log("Tidak ada Rows");
          //$scope.mData = rows[0];
          console.log("$scope.mData = ", $scope.mData);
          $scope.disablebtn = true;
        } else {
          console.log("Rows ada");
          //$scope.mData = rows[0];
          console.log("$scope.mData = ", $scope.mData);
          console.log('isBengkel==>>',$scope.isBengkel);
          if (rows[0].Bengkel != "-") {
            $scope.isBengkel = true;
          } else { $scope.isBengkel = false; }
          $scope.disablebtn = false;
        }
        //$scope.isBengkel = (rows[0].Bengkel != "-");
        //$scope.isBengkel = ($scope.mData.Bengkel != "-");

        //$scope.disablebtn = false;
        console.log('$scope.isOutstandingSO =>', $scope.isOutstandingSO);
        if ($scope.isOutstandingSO && $scope.isBengkel) {
           $scope.newSalesBill= true;
        }
        //console.log("Isi test component =>", $scope.testmodel.VehicleName);
      }


    // ======== tombol  simpan =========
    $scope.simpanAlert = function(){
      alert("Data Billing disimpan");
      // ngDialog.openConfirm ({
      //   template:'<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Permohonan Approval</h3></div> <div class="panel-body"><form method="post"><div class="row"><div class="col-md-10"><p><i>Disposal ini membutuhkan Approval.</i></p></div></div><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nama Kegiatan</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">Disposal</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nomor Request</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">D1/20160717001</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Tanggal dan Jam</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">-</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Approve(s)</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4"></textarea></div></div> <br><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Pesan</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4">Mohon untuk approve list dari item Disposal berikut. Terima kasih.</textarea></div></div> <br><div class="row"><div class="col-md-12"><div class="form-group"><div><button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Batal</button> <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()" style="color:white; background-color:#D53337">Kirim Permohonan</button> </div></div></div></div></form></div></div>',
      //   //templateUrl: '/app/parts/pomaintenance/alasan.html',
      //   plain: true,
      //   controller: 'DisposalController',
      //  });
    };
    // ======== end tombol  simpan =========
    $scope.showApprovalSB = function(vparApp){ // ada tambahan vparApp.procNext dan pake form frmApproval02
      vparApp['TypeId']= 0;
      console.log("Show Approval SB");
      PartsGlobal.setparApproval(vparApp);
      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/templates01/frmApproval02.html\'\"></div>',
        // template:'<div ng-include=\"\'app/parts/directpartssales/salesbilling/helper/frmApproval02.html\'\"></div>',
        plain: true,
        controller: 'PartsSalesBillingController', // vparApp.controller, //'MIPMaintenanceController',
        showClose: false,
        closeByEscape : false,
        closeByNavigation: false,
        closeByDocument: false,
       }).then(
          function (value) {
              // click cancel
              console.log('click cancel', value);
              // console.log(" t/f : ", PartsGlobal.RequestApprovalSent);
              // var RequestApprovalSentYes = PartsGlobal.RequestApprovalSent;
              // console.log("ada : ? ", RequestApprovalSentYes)
              // return RequestApprovalSentYes;
          }, 
          function (value) {
            // click ok
            console.log('click ok', value);
            console.log("Get here do after request approval", PartsGlobal.RequestApprovalSentPG, PartsGlobal.RequestApprovalSent);

            if(PartsGlobal.RequestApprovalSent){
              var notif = {
                Message: "Permohonan Approval Pembatalan Sales billing " +$scope.mData.SalesBillingCancelNo+ " ",
                RecepientId: 1128,
                Param :39
              }
              console.log(notif);
              PartsSalesBilling.sendNotifikasi(notif).then(function(res){
              });
              var RequestApprovalSentYes = PartsGlobal.RequestApprovalSent;
              if (PartsGlobal.RequestApprovalSentPG) {
                console.log("Get here do after request approval");
                vparApp.procUpdateData(vparApp.headerData , vparApp.detailData);
                // $scope.formApi.setMode("grid");
                $scope.goBack0();
              }

              vparApp.procUpdateData(vparApp.headerData , vparApp.detailData);
              console.log("Data TerCancel dengan Nomor ", vparApp);
              console.log("Data TerCancel dengan Nomor ", vparApp.nosb);
              $scope.btnKembali();
              bsAlert.alert({
                title: "Data dengan nomor billing " + $scope.mData.SalesBillingNo + " sudah dibatalkan dengan nomor pembatalan ",
                text: vparApp.noreq,
                type: "success",
                showCancelButton: false
              });
              return RequestApprovalSentPG;//RequestApprovalSentYes;
            }
          });
                // =====
      },

    // ======== tombol batal - parts claim - alasan =========
    $scope.batalSB = function(mData){
      var temporarymData = angular.copy($scope.mData);
      console.log('batalSB - mData =>', mData);
      console.log('mData.SalesOrderNo =>', mData.SalesOrderNo);

      PartsSalesBilling.setSalesBillingHeader(mData);
      console.log('Disposal.getDisposalHeader() = ', PartsSalesBilling.getSalesBillingHeader());
      $scope.mData.GridDetail = $scope.gridOptionsView.data;
      PartsSalesBilling.setSalesBillingDetail($scope.mData.GridDetail);
      console.log('batalSB - mDataDetail =>', $scope.mData.GridDetail);

      $scope.BatalData =  PartsSalesBilling.setSalesBillingHeader(mData);

      $scope.BatalDataDetail =  PartsSalesBilling.setSalesBillingDetail($scope.mData.GridDetail);
      //sini ma

      var HariIni = new Date();
      var TglSB = new Date($scope.mData.DocDate);

      var etaa
      var yyyyeta = HariIni.getFullYear().toString();
      var mmeta = (HariIni.getMonth() + 1).toString(); // getMonth() is zero-based
      var ddeta = HariIni.getDate().toString();
      etaa = (ddeta[1] ? ddeta : "0" + ddeta[0]) + '/' + (mmeta[1] ? mmeta : "0" + mmeta[0]) + '/' + yyyyeta;
      console.log("changeFormatDate finalDate", etaa);
      HariIni = etaa;

      var now
      var yyyyeta = TglSB.getFullYear().toString();
      var mmeta = (TglSB.getMonth() + 1).toString(); // getMonth() is zero-based
      var ddeta = TglSB.getDate().toString();
      now = (ddeta[1] ? ddeta : "0" + ddeta[0]) + '/' + (mmeta[1] ? mmeta : "0" + mmeta[0]) + '/' + yyyyeta;
      console.log("changeFormatDate finalDate", etaa);
      TglSB = now;

      console.log("HariIni==>",HariIni);
      console.log("TglSB==>",TglSB);
      
      if (TglSB != HariIni) {
        ngDialog.openConfirm ({
          template:'<div ng-include=\"\'app/parts/directpartssales/salesbilling/helper/dialog_batal.html\'\"></div>',
          plain: true,
          controller: 'PartsSalesBillingController',
          showClose: false,
          closeByEscape : false,
          closeByNavigation: false,
          closeByDocument: false,
        }).then(function (success) {
          // Success logic here
          console.log('sucsesa', success);
          $scope.GenerateDocNum();
        }, function (error) {
            // Error logic here
            $scope.mData = angular.copy(temporarymData);
            console.log('error', error);
        });
        console.log("$scope.BatalData", $scope.BatalData);
        console.log("$scope.BatalData", $scope.mData);
      } else {


        if ($scope.BatalDataDetail == null || $scope.BatalDataDetail == undefined )
        {
          $scope.BatalDataDetail =  $scope.mData.GridDetail[0];
        }else if ( $scope.BatalData == null || $scope.BatalData == undefined )
        {
          $scope.BatalData =  PartsSalesBilling.getSalesBillingHeader();
        }
        var aa = PartsSalesBilling.getSalesBillingHeader();
        aa.GridDetail = null;
        console.log("COBA masuk mData.GridDetail[0] 0",PartsSalesBilling.getSalesBillingHeader());
        console.log("COBA masuk mData.GridDetail[0] 1",PartsSalesBilling.setSalesBillingHeader($scope.mData));
        console.log("COBA masuk $scope.BatalData ",aa,$scope.BatalData, $scope.BatalDataDetail);
        aa.SalesBillingStatusId = 2;
        aa.SalesBillingCancelStatusId = 3;
        $scope.GenerateDocNumTwo(aa,$scope.BatalDataDetail);
      }

      // ngDialog.openConfirm ({
      //   //template:'<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Input Alasan Pembatalan</h3></div> <div class="panel-body"><form method="post"><div class="form-group "> <div class="row"><div class="col-md-8"><label class="control-label requiredField" style="color:black;">Alasan<span class="asteriskField">*</span></label> <select class="form-control" style="widht: 100px><option value="Kesalahan Administrasi">Kesalahan Administrasi</option><option value="Kesalahan Pilih Transaksi">Kesalahan Pilih Transaksi</option><option value="Lainnya">Lainnya</option></select><br></div></div><div class="row"><div class="col-md-12"><label class="control-label" style="color:black;">Notes</label> <textarea class="form-control" cols="40" id="message" name="message" rows="10"></textarea></div></div></div> <div class="form-group"><div class="row"><div class="col-md-12"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="okBatalSB()" style="color:white; background-color:#D53337">OK</button> <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Kembali</button> </div></div></div></form></div></div>',
      //   template:'<div ng-include=\"\'app/parts/directpartssales/salesbilling/helper/dialog_batal.html\'\"></div>',
      //   plain: true,
      //   controller: 'PartsSalesBillingController',
      // });
    //   PartsSalesBilling.SearchSOInGI(mData).then(function(res) { AKU COMMENT KATA PAK ARI TAK BUTUH VALIDASI DIANTARA KITA
    //     var hasil = res.data;
    //     console.log("<controller getData> hasil[0].IsGoodIssue => ", hasil[0].IsGoodIssue);
    //     if(hasil[0].IsGoodIssue > 0){
    //       bsNotify.show(
    //           {
    //               title: "Sales Billing",
    //               content: "Tidak bisa dibatalkan karena sudah dibuatkan Good Issue",
    //               type: 'danger'
    //           }
    //         );
    //     } else {
    //       ngDialog.openConfirm ({
    //         //template:'<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Input Alasan Pembatalan</h3></div> <div class="panel-body"><form method="post"><div class="form-group "> <div class="row"><div class="col-md-8"><label class="control-label requiredField" style="color:black;">Alasan<span class="asteriskField">*</span></label> <select class="form-control" style="widht: 100px><option value="Kesalahan Administrasi">Kesalahan Administrasi</option><option value="Kesalahan Pilih Transaksi">Kesalahan Pilih Transaksi</option><option value="Lainnya">Lainnya</option></select><br></div></div><div class="row"><div class="col-md-12"><label class="control-label" style="color:black;">Notes</label> <textarea class="form-control" cols="40" id="message" name="message" rows="10"></textarea></div></div></div> <div class="form-group"><div class="row"><div class="col-md-12"><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="okBatalSB()" style="color:white; background-color:#D53337">OK</button> <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Kembali</button> </div></div></div></form></div></div>',
    //         template:'<div ng-include=\"\'app/parts/directpartssales/salesbilling/helper/dialog_batal.html\'\"></div>',
    //         plain: true,
    //         controller: 'PartsSalesBillingController',
    //       });
    //     }
    //   },
    //   function(err) {
    //     console.log("err=>", err);
    //   }
    // );



    };

    // dialog konfirmasi
    $scope.okBatalSB = function(data){
      // // ngDialog.openConfirm ({
      // //   template:'\
      // //                <div align="center" class="ngdialog-buttons">\
      // //                <p><b>Konfirmasi</b></p>\
      // //                <p>Anda yakin akan membatalkan Sales Billing ini ?</p>\
      // //                <div class="ngdialog-buttons" align="center">\
      // //                  <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
      // //                  <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()">Ya</button>\
      // //                </div>\
      // //                </div>',
      // //   plain: true,
      // //   controller: 'PartsSalesBillingController',
      // //  });
      // var str0 = 'Sales Billing';
      // // parApproval.subtitle = str0 +' ini';
      // // parApproval.kegiatan = str0;
      // // parApproval.noreq = 'SB 321';
      // // parApproval.approver = 'Petugas SB : Farhan, Jihan';
      // // parApproval.pesan = 'Mohon di approve ya';
      // PartsGlobal.setparApproval({
      //       subtitle: str0 +' ini',
      //       kegiatan: str0,
      //       noreq: 'SB 0212',
      //       approver: 'Jeni, Rini',
      //       pesan: 'Mohon di approve segera'
      //       // btnOK: 'OK '
      //   });
      // // console.log("parApproval 2 : ", parApproval);
      // ngDialog.openConfirm ({
      //   //template:'<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Permohonan Approval</h3></div> <div class="panel-body"><form method="post"><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nama Kegiatan</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">MIP Maintenance</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nomor Request</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">-</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Tanggal dan Jam</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">-</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Approve(s)</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4"></textarea></div></div> <br><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Pesan</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4">Mohon untuk approve list dari item Perhitungan MIP berikut. Terima kasih.</textarea></div></div> <br><div class="row"><div class="col-md-12"><div class="form-group"><div> <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Kembali</button> <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()" style="color:white; background-color:#D53337">OK</button> </div></div></div></div></form></div></div>',
      //   template:'<div ng-include=\"\'app/parts/templates01/frmApproval01.html\'\"></div>',
      //   plain: true,
      //   controller: 'PartsSalesBillingController', //'MIPMaintenanceController',
      //  });
      // return 0;

      console.log('okBatalSO');
      if ((data.Batal == null) || (data.Batal == undefined) || (data.Batal == "")) {
        bsNotify.show({
          size: 'big',
          type: 'danger',
          title: "error",
          content: "Isi Alasan Batal"
        });
      } else {

        $scope.BatalData = PartsSalesBilling.getSalesBillingHeader();
        $scope.BatalData.Notes = 'Alasan: ' + data.Batal + '\n' + data.Catatan;
        console.log('$scope.BatalData = ', $scope.BatalData);
        PartsSalesBilling.setSalesBillingHeader($scope.BatalData);

        $scope.BatalDataDetail = PartsSalesBilling.getSalesBillingDetail();
        console.log('$scope.BatalDataDetail = ', $scope.BatalDataDetail);
        PartsSalesBilling.setSalesBillingDetail($scope.BatalDataDetail);

        ngDialog.openConfirm({
          template: '\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Anda yakin akan membatalkan Sales Billing ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinBatal(BatalData, BatalDataDetail)">Ya</button>\
                     </div>\
                     </div>',
          plain: true,
          controller: 'PartsSalesBillingController',
          showClose: false,
          closeByEscape : false,
          closeByNavigation: false,
          closeByDocument: false,
        });
        console.log('okBatalSB  = ', $scope.BatalData);
        console.log('mdata  = ', $scope.mData);


      }
    };

    $scope.yakinBatal = function(data, detail){
      console.log('SetujuBatal');
      $scope.mData = data;
      $scope.mData.GridDetail = detail;
      $scope.ngDialog.close();
      $scope.mData.CancelDate = new Date();
      // $scope.mData.SalesBillingStatusId = 2; // kalo belum diapprove jangan dulu rubah status

      console.log('yakinBatal mData = ', $scope.mData);
      console.log('yakinBatal mData  Detail = ', $scope.mData.GridDetail);
      // [START] Get
      // -> disini mestinya ada sent approval dulu .. ddg 10072017
      //-> PartsGlobal.showApproval(); /// ....
      //-> if Req Approval sent then .. do this ..
      //*
      // $scope.mData.SalesBillingCancelNo = PartsGlobal.getCancelNo(mData);  cek 25-08-2017
      var str0 = 'Batal Sales Billing';
      var parCancelNo = {doctype : 'SB', dataId : $scope.mData.SalesBillingId};
      PartsGlobal.getCancelNo(parCancelNo).then(function(res){
      // PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res){  // tested and worked
        console.log("No Pembatalan ==>",res);
        // $scope.CancelNo = res.data.Result[0].GoodsIssueNo; // ini sebenarnya ngambil no Pambatalan Dokumen
        $scope.CancelNo = res.data.Result[0].DocNo;
        $scope.Approver = res.data.Result[0].Approvers;
        console.log("No Batal : ", $scope.CancelNo);
        PartsGlobal.RequestApprovalSent = false;
        PartsGlobal.setReqAppSentPG(false);

        $scope.mData['ApprovalStatus'] = 0;
        $scope.mData['SalesBillingCancelStatusId'] = 1; // 1=Aju Batal Billing ; 2= Rejected; 3= Approved
        $scope.mData['SalesBillingCancelNo'] = $scope.CancelNo;
        $scope.showApprovalSB({
        // PartsGlobal.showApproval02({
          subtitle: str0 +' ini',
          kegiatan: str0,
          noreq: $scope.CancelNo,  // mestinya berisi no Pembatalan
          approver: $scope.Approver, //'Factory Judit, Factory Tini',
          pesan: 'Tolong approve segera ',
          controller: 'PartsSalesBillingController',
          ProcessId: $scope.ApprovalProcessId,
          userid: 0,
          dataId: $scope.mData.SalesBillingId,
          headerData: $scope.mData,
          detailData: $scope.mData.GridDetail,
          procUpdateData : PartsSalesBilling.cancel  //($scope.MRHeader, $scope.gridDetail.data)
          // procNext : $scope.goBack2
        });
        // $scope.goBack2();


      });


      // PartsGlobal.RequestApprovalSent = false;
      // PartsGlobal.setReqAppSentPG(false);

      // $scope.mData['ApprovalStatus'] = 0;
      // $scope.mData['SalesBillingCancelStatusId'] = 1; // 1=Aju Batal Billing ; 2= Rejected; 3= Approved
      // PartsGlobal.showApproval({
      //   subtitle: str0 +' ini',
      //   kegiatan: str0,
      //   noreq: $scope.CancelNo,  // mestinya berisi no Pembatalan
      //   approver: 'Factory Judit, Factory Tini',
      //   pesan: 'Tolong approve segera ',
      //   controller: 'PartsSalesBillingController',
      //   ProcessId: $scope.ApprovalProcessId,
      //   userid: 0,
      //   dataId: $scope.mData.SalesBillingId,
      //   headerData: $scope.mData,
      //   detailData: $scope.mData.GridDetail,
      //   procUpdateData : PartsSalesBilling.cancel //($scope.MRHeader, $scope.gridDetail.data)
      // });
      //*/
      /*/
      PartsSalesBilling.cancel($scope.mData, $scope.mData.GridDetail).then(function(res) {
          console.log('res cancel = ', res);
          alert("Sales Billing berhasil dibatalkan");
        },
        function(err) {
          console.log("err=>", err);
        }
      );
      */
      // [END] Get
    }

    // ======== end tombol batal - parts claim =========

    // Report =========================================
    $scope.laporan = function(){
      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/disposal/referensi/template_laporan.html\'\"></div>',
        plain: true,
        // width: 800px,
        controller: 'DisposalController',
       });
    };


    $scope.onBeforeNewMode = function(){
        $scope.isOverlayForm = true;
        $scope.isEditPartsClaim = false;
        $scope.isEditLangsung = false;
        $scope.isViewPartsClaim = false;
        $scope.isViewLangsung = false;
        PartsSalesBilling.formApi = $scope.formApi;
        $scope.selection = null;

        console.log("onBeforeNewMode = ");
    }
    $scope.onBeforeEditMode = function(row, mode){
        console.log("onBeforeEdit=>", mode);
    }
    $scope.onBeforeDeleteMode = function(){
        $scope.formode = 3;
        $scope.isNewForm = false;
        $scope.isEditForm = false;
        console.log("mode=>",$scope.formmode);
    }
    $scope.onShowDetail = function(row, mode){
      if(row.RefSOTypeId == 2){
        $scope.customerlgsg = 1;
      } else {
        $scope.customerlgsg = 0;
      }
      if (row.ToyotaId == '-' || row.ToyotaId == null || row.ToyotaId == undefined) {
        $scope.AdaToyotaId = false;
      } else {
        $scope.AdaToyotaId = true;
      }
      console.log("$scope.customerlgsg=>",$scope.customerlgsg);
      console.log("$scope.AdaToyotaId=>",$scope.AdaToyotaId);
      $scope.mData = row;
      $scope.mData.CancelDate = new Date($scope.mData.DocDate.getFullYear(),$scope.mData.DocDate.getMonth(),$scope.mData.DocDate.getDate()+1);
      console.log('showdetailllll',$scope.mData.CancelDate);
      PartsSalesBilling.setSBHeader(row);
      console.log("onShowDetail mode row => ", row);
      console.log("onShowDetail bengkel => ", row.bengkel, row.Bengkel);
      PartsSalesBilling.formApi = $scope.formApi;
      if(mode == 'view'){ //jika mode = view
        if (row.Bengkel != "-") {
          $scope.isBengkel = true;
        } else { $scope.isBengkel = false; }
        $scope.isViewForm = true;
        console.log("onShowDetail mode view => ", mode);
      } else if(mode == 'edit'){ //jika mode edit
        $scope.isViewForm = false;
        console.log("onShowDetail mode edit => ", mode);
      } else if(mode == 'new'){
        console.log("onShowDetail mode new => ", mode);
      }
      console.log("onShowDetail=> ", mode , $scope.isViewForm);
      console.log("isBengkel=> ", $scope.isBengkel);
      console.log("isViewForm=> ", $scope.isViewForm);

      // untuk mengisi / (get data) grid detail saat mode view
      PartsSalesBilling.getDetail($scope.mData).then(function(res) {
            var gridData = res.data.Result;

            console.log('gridData.CancelDate', gridData.CancelDate);

            console.log('<controller> getDetail = ', gridData);
            //$scope.gridOptionsView.data = gridData;
            console.log("onShowDetail getDetail => ", mode);
            switch(mode)
            {
              case 'view':
                $scope.gridOptionsView.data = gridData;
                break;
              case 'edit':
                $scope.gridOptionsView.data = gridData;
                break;
              default:
                $scope.gridOptionsView.data = gridData;
            }
          },
          function(err) {
            console.log("err=>", err);
          }
        ); // end getDetail

    }
    // var cellTempActSSB='<div style="padding-top: 5px;"><u><a href="#" ng-click="grid.appScope.$parent.lihat()">Lihat</a></u></div>';
    var PaidTemplate = ' <input type="checkbox" ng-checked="row.entity.Paid > 0" onclick="return false">';
    var PaidDPTemplate = ' <input type="checkbox" ng-checked="row.entity.PaidByDP > 0" onclick="return false">';
    var cellTempActSSB='<div style="text-align:center"><u><a href="#" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat" tooltip-placement="bottom" ng-click="grid.appScope.$parent.onShowDetail(row.entity, \'view\')"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a></u></div>';
    // var cellSSB01 = '<div style="text-align:center"><button ng-show="row.entity.SalesBillingStatusId != 2 && row.entity.Paid !== 1" ng-if="COL_FIELD != 1" class="btn btn-xs" style="color:white; background-color:#D53337" ng-click="grid.appScope.$parent.btnList0Click(row)">Batal Billing</button></div>';
    var cellSSB01 = '<div class="ui-grid-cell-contents">' +
    '<a href="#" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat" tooltip-placement="bottom" ng-click="grid.appScope.$parent.onShowDetail(row.entity, \'view\')"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
    '<a href="#" ng-show="row.entity.SalesBillingStatusId != 2 && (row.entity.Paid !== 1 || (row.entity.Paid == 1 && row.entity.PaidByDP == 1)) && grid.appScope.$parent.isKabeng == false" ng-if="COL_FIELD != 1" ng-click="grid.appScope.$parent.btnList0Click(row)" uib-tooltip="Batal Billing" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-times-circle fa-lg" style="padding:5px 8px 8px 0px;margin-left:10px;"></i></a>' +
    '<a href="#" ng-click="grid.appScope.$parent.CetakSalesBatalBillingCustLangsung(row.entity.SalesBillingId)" ng-show="grid.appScope.$parent.isKabeng == false && row.entity.SalesBillingCancelStatusId == 3 && row.entity.RefSOTypeId == 2" uib-tooltip="Cetak Batal Billing" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-id-card-o fa-lg" style="padding:5px 8px 8px 0px;margin-left:10px;"></i></a>' +
    '<a href="#" ng-show="grid.appScope.$parent.isKabeng == false && row.entity.RefSOTypeId == 2 && (row.entity.SalesBillingCancelStatusId == null || row.entity.SalesBillingCancelStatusId == 2 || row.entity.SalesBillingCancelStatusId == 1)" ng-if="COL_FIELD != 1" style="color:#777;" class="trlink ng-scope" uib-tooltip="Cetak Faktur Parts" tooltip-placement="bottom" ng-click="grid.appScope.$parent.bulkPrint(row.entity)"><i class="fa fa-print fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>'+
    '</div>';
    var cellSSB02 = '<div style="text-align:center"><button class="btn btn-xs" style="color:white; background-color:#D53337" ng-click="grid.appScope.$parent.btnList0Click(row)">Approve </button>\
           <button class="btn btn-xs" style="color:white; background-color:#D53337" ng-click="grid.appScope.$parent.btnList0Click(row)">Reject </button> </div>';
    $scope.columnsSSB = [
        {
          name: 'Id', field: 'SalesBillingId', width: '7%', visible: false
        },
        { name: 'No. Sales Billing', field: 'SalesBillingNo', width: '15%'},
        { name: 'No. Sales Order', field: 'SalesOrderNo', width: '15%' },
        { name: 'Tgl. Sales Order', field: 'DocDate', cellFilter: 'date:\'dd-MM-yyyy\'', width: '10%'},
        { name: 'Bengkel', field: 'Bengkel', width: '15%'},
        { name: 'Pelanggan', field: 'CustomerName', width: '15%'},
        { name: 'Status Sales Billing', field: 'SalesBillingStatusName', width: '10%'},
        { name: 'No Batal Sales Billing', field: 'SalesBillingCancelNo', width: '15%'},
        { name: 'Status Batal Sales Billing', field: 'SalesBillingCancelStatusName', width: '10%'},
        {
          name: 'View', enableFiltering:false,
          //field: 'action',
          field: '_',
          pinnedRight:true,
          cellTemplate: cellTempActSSB,
          width: '5%'
        },
        {
          name: 'Action', enableFiltering:false,
          field:'__',
          width: '10%',
          pinnedRight:true,
          cellTemplate: $scope.getCellTempForSSB() // cellSSB01 //$scope.getCellTempForSSB()
        }
      ];
    $scope.columnsSSB2 = [
        {
          name: 'Id', field: 'SalesBillingId', width: '7%', visible: false
        },
        { name: 'No. Faktur Sales Order', field: 'SalesBillingNo', width: '15%', enableCellEdit:false},
        { name: 'No. Sales Order', field: 'SalesOrderNo', width: '15%', enableCellEdit:false },
        { name: 'Tgl. Sales Billing', field: 'DocDate', cellFilter: 'date:\'dd-MM-yyyy\'', width: '10%',
          displayName: "Tgl. Sales Billing"},
        { name: 'No. Faktur Pajak', field: 'NoFakturPajak', width: '15%'},
        { name: 'Bengkel', field: 'Bengkel', width: '15%'},
        // { name: 'stat', field: 'SalesBillingCancelStatusId', width: '20%'},
        { name: 'Pelanggan', field: 'CustomerName', width: '15%', displayName: "Pelanggan"},
        { name: 'Status Sales Billing', field: 'SalesBillingStatusName', width: '12%'},
        { name: 'Status Reprint', field: 'SalesBillingReprintStatus', width: '15%'},
        { name: 'Reprint Faktur', field: 'PrintCount', width: '15%', visible: false},
        { name: 'No Batal Sales Billing', field: 'SalesBillingCancelNo', width: '15%'},
        { name: 'ID Batal Sales Billing', field: 'SalesBillingStatusId', width: '15%', displayName: "ID Batal Sales Billing", visible: false},
        { name: 'Tgl. Batal Sales Billing', field: 'CancelDate', visible: false, cellFilter: 'date:\'dd-MM-yyyy\''},
        { name: 'Status Batal Sales Billing', field: 'SalesBillingCancelStatusName', width: '12%'},
        { name: 'Lunas', field: 'Paid', cellTemplate: PaidTemplate, enableCellEdit: false, width: 70, displayName: "Paid"},
        { name: 'LunasDP', field: 'PaidByDP', cellTemplate: PaidDPTemplate, enableCellEdit: false, width: 70, displayName: "Paid by DP"},
        // {
        //   //name: 'Action',
        //   //field: 'action',
        //   field: '_',
        //   pinnedRight:true,
        //   cellTemplate: cellTempActSSB,
        //   width: '5%'
        // },
        {
          name: 'Action',
          field:'SalesBillingCancelStatusId',
          width: '15%',
          disablebtn: true,
          pinnedRight:true,
          displayName: "Action",
          cellTemplate: cellSSB01 // cellSSB01 //$scope.getCellTempForSSB()
        }
      ];
    $scope.columnsBSB = [
        {
          name: 'Id', field: 'SalesOrderId', width: '7%', visible: false
        },
        { name: 'No Sales Order', field: 'SalesOrderNo', width: '20%' },
        { name: 'Tgl. Sales Order', field: 'DocDate', cellFilter: 'date:\'yyyy-MM-dd\'', width: '13%',
          displayName: "Tgl. Sales Order"},
        { name: 'Bengkel', field: 'Bengkel', width: '25%'},
        { name: 'Pelanggan', field: 'CustomerName', width: '25%', displayName: "Pelanggan"},  //displayName: "Pelanggan"
        { name: '',
          field:'___',
          width: '15%',
          pinnedRight:true,
          //yap : nambahin ng-show nya karena ada flagnya
          cellTemplate:'<div style="text-align:center"><button ng-show="row.entity.SalesOrderStatusId== 2" class="btn btn-xs" style="color:white; background-color:#D53337" ng-click="grid.appScope.$parent.btnList0Click(row)">Buat Sales Billing Baru</button></div>'
          //cellTemplate:'<div style="text-align:center"><button ng-show="row.entity.IsBilling > 0 && row.entity.SalesOrderStatusId== 2" class="btn btn-xs" style="color:white; background-color:#D53337" ng-click="grid.appScope.$parent.btnList0Click(row)">Buat Sales Billing Baru</button></div>' --Dicomment Rizma karena tombolnya bisa keluar yg belom billing
        }
      ];

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
      enableSorting: true,
      enableRowSelection: true,
      multiSelect: true,
      enableSelectAll: true,
      //showTreeExpandNoChildren: true,
      paginationPageSizes: [10, 30, 40, 50, 60, 70, 80, 90, 100, 200],
      paginationPageSize: 10,
      // columnDefs:  $scope.columnsSSB, //$scope.columnsBSB,
      columnDefs:  $scope.columnsBSB, //$scope.columnsBSB,
      data : $scope.getDataApproval({})
      // [
      //   {
      //     name: 'Id', field: 'Id', width: '7%', visible: false
      //   },
      //   { name: 'No. Sales Billing', field: 'noSalesBilling', //width: '25%',
      //     visible: $scope.isOutstandingSO },
      //   { name: 'noSalesOrder', field: 'noSalesOrder', width: '25%' },
      //   { name: 'tanggalSalesOrder', field: 'tanggalSalesOrder', width: '25%'},
      //   { name: 'bengkel', field: 'bengkel', width: '25%'},
      //   { name: 'customer', field: 'customer'}
      // ]
    };



    //----------------------------------
    // Dummy
    //----------------------------------
    //  $scope.changedValue = function (item){
    //   console.log(item);
    //   console.log('pil ', $scope.mydsoTampil);
    //   $scope.isOutstandingSO = item.id == 1;
    //   // $scope.gridApi.grid.refresh();
    //   // $scope.grid.columnDefs = new Array();
    //   if ($scope.isOutstandingSO) {
    //     $scope.grid.columnDefs = $scope.columnsBSB;
    //     //$scope.grid.columnDefs = $scope.columnsSSB;
    //     $scope.btnCaption= 'Buat Sales Billing Baru';
    //     // $scope.gridHideActionColumn= true; //234
    //   }else {
    //     $scope.grid.columnDefs = $scope.columnsSSB;
    //     //$scope.grid.columnDefs = $scope.columnsBSB;
    //     $scope.btnCaption= 'Batal Billing';
    //     // $scope.gridHideActionColumn= false; //234
    //   }
    //   //$scope.isOverlayForm = false;
    //   //$scope.ref = item;
    //   //$scope.disableSelect  = true;
    // }
    $scope.btnList0Click = function(row){
      $scope.LoadingData = true;
      console.log('$scope.mData = ', $scope.mData);
      console.log('onshowdetail row before = ', row);
      console.log('row.entity.ToyotaId2 = ', row.entity.ToyotaId2);
      if (row.entity.RefSOTypeId == 2) {
        $scope.customerlgsg = 1;
      } else {
        $scope.customerlgsg = 0;
      }
      if (row.entity.ToyotaId2 == '-' || row.entity.ToyotaId2 == null || row.entity.ToyotaId2 == undefined) {
        $scope.AdaToyotaId = false;
      } else {
        $scope.AdaToyotaId = true;
      }
      console.log("$scope.customerlgsg=>",$scope.customerlgsg);
      console.log("$scope.AdaToyotaId=>",$scope.AdaToyotaId);
      PartsSalesBilling.setSBHeader(row);      
      PartsSalesBilling.formApi = $scope.formApi;
      console.log('onshowdetail row = ', row);
      console.log('$scope.grid.data = ', $scope.grid.data);
      $scope.mData = row.entity;
      $scope.DisableCekGI = false;
      var obj={};
      obj.RefGITypeID=2;
      obj.RefGINo=row.entity.SalesOrderNo;
      console.log("Pusinggggggg=>",row.entity.SalesOrderNo)
      PartsGlobal.isGIDone(obj).then(function(resu) { 
        if(resu.data.Result[0].GIAlreadyDone == 1) {
          $scope.DisableCekGI=false;
        }else{
          $scope.DisableCekGI=true;
        }
      });
      // $scope.mData['SalesOrderDate'] = row.entity['DocDate'];
      // $scope.mData['DocDate'] = PartsGlobal.getCurrDate();
      console.log("$scope.DisableCekGI :> ", $scope.DisableCekGI);
      console.log("masuk row :> ", row);
      console.log("data entity :> ", $scope.mData);
      $scope.statCancel = row.entity.SalesBillingCancelStatusId;
      console.log("STAT CANCEL :> ", $scope.statCancel);
      // ==========================
      //$scope.isBengkel = (rows[0].Bengkel != "-");
      if ($scope.mData.Bengkel != "-") {
        $scope.isBengkel = true;
      } else { $scope.isBengkel = false; }
      console.log('$scope.isBengkel =>', $scope.isBengkel);
      console.log('$scope.isOutstandingSO =>', $scope.isOutstandingSO);
      if ($scope.isOutstandingSO && $scope.isBengkel) {
          $scope.newSalesBill= true;
      }
      // ==========================

      if ($scope.mData.length == 0) {return 0};
      console.log('$scope.newSalesBill => ', $scope.newSalesBill);

      if ($scope.isOutstandingSO) {
        $scope.newSalesBill = true;
        PartsGlobal.setSB({viewbl:true});
        var SB = PartsGlobal.getSB();
        $scope.mData['SalesOrderDate'] = row.entity['DocDate'];
        $scope.mData['DocDate'] = PartsGlobal.getCurrDate();
        $scope.viewBL= SB.viewbl;
        $scope.batalSalesBill = false;
        $scope.isOverlayForm = true;
        console.log('btnList0Click -> $scope.newSalesBill = ', $scope.newSalesBill);
        if ($scope.isBengkel) {
          console.log("Go to : [bengkel] " + $scope.btnCaption + ' '+ $scope.mData.bengkel);
        } else {
          console.log("Go to : [customer] " + $scope.btnCaption + ' '+ $scope.mData.customer);
          $scope.viewBL= false;
          $scope.viewCL= true;
        }
        // get data detail
        PartsSalesBilling.getDetailSO($scope.mData).then(function(res) {
            var gridData = res.data;
            console.log('<controller> getDetail = ', gridData);
            console.log('<controller> getDetail $scope.batalSalesBill = ', $scope.batalSalesBill);
            $scope.gridOptions.data = gridData;
            // if($scope.batalSalesBill){
            //   console.log('<controller> ini mode batal');
            //   $scope.gridOptionsView.data = gridData;
            // } else {
            //   $scope.gridOptions.data = gridData;
            // }
            $scope.LoadingData = false;
          },
          function(err) {
            console.log("err=>", err);
          }
        ); // end getDetail

      } else {
        // $scope.newSalesBill = false;
        // $scope.batalSalesBill = true;
        //alert("Batal billing");
        console.log('Batal Billing');
        // if (!$scope.isBengkel) {return 0; }  // baris ini bilang, kalo BUKAN Bengkel lain, maka gak bisa batal
        // $scope.batalBL = true;
        if ($scope.mData['SalesBillingCancelStatusId']== 1 || $scope.mData['SalesBillingCancelStatusId']== 3) {
          PartsGlobal.showAlert({
                    title: "Sales Billing",
                    content: "Data Sales Billing Sudah Batal Atau Sedang Diajukan Batal",
                    type: 'danger'
                  });
          return 0;
        }
        $scope.newSalesBill = false;
        $scope.batalSalesBill = true;
        $scope.batalBL = true;
        // get data detail
        PartsSalesBilling.getDetail($scope.mData).then(function(res) {
            var gridData = res.data.Result;
            console.log('<controller> getDetail = ', gridData);
            console.log('<controller> getDetail $scope.batalSalesBill = ', $scope.batalSalesBill);
            $scope.gridOptionsView.data = gridData;
            // if($scope.batalSalesBill){
            //   console.log('<controller> ini mode batal');
            //   $scope.gridOptionsView.data = gridData;
            // } else {
            //   $scope.gridOptions.data = gridData;
            // }
          },
          function(err) {
            console.log("err=>", err);
          }
        ); // end getDetail

      }

      // PartsSalesBilling.getDetail($scope.mData).then(function(res) {
      //     var gridData = res.data;
      //     console.log('<controller> getDetail = ', gridData);
      //     console.log('<controller> getDetail $scope.batalSalesBill = ', $scope.batalSalesBill);
      //     //$scope.gridOptions.data = gridData;
      //     if($scope.batalSalesBill){
      //       console.log('<controller> ini mode batal');
      //       $scope.gridOptionsView.data = gridData;
      //     } else {
      //       $scope.gridOptions.data = gridData;
      //     }
      //   },
      //   function(err) {
      //     console.log("err=>", err);
      //   }
      // ); // end getDetail

    } // end function btnList0Click

    $scope.btnKembali = function(){
      console.log("Kembali");
      $scope.newSalesBill = false;
      $scope.batalSalesBill = false;
      // PartsGlobal.setSB({viewbl:false});
      //  var SB = PartsGlobal.getSB();
      $scope.viewBL= false;
      $scope.viewCL= false;
      console.log($scope.viewBL);
      $scope.isOverlayForm = false;
      $scope.isViewForm = false;
      $state.go('app.parts_salesbilling');
      $state.reload();
      console.log('Ok kembali');
    }

    $scope.showMe = function(){
        confirm('Apakah Anda yakin menghapus data ini?');
    };

    $scope.gridOptions = {
        onRegisterApi: function(gridApi) {
          $scope.gridApi = gridApi;
        },
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableFiltering : true,
        //selectedItems: console.log($scope.mySelections),
        enableSelectAll: true,
        paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 5,
        showColumnFooter: true,

        columnDefs: [
          {
          name: 'Id', field: 'SalesOrderId', width: '7%', visible: false
          },
          { name:'PartId', field: 'PartId', width: '18%', visible: false},
          { name:'UomId', field: 'UomId', width: '18%', visible: false},
          //{ name:'PartId', field: 'PartId', width: '18%', visible: false},
          //{ name:'PartId', field: 'PartId', width: '18%', visible: false},
          { name:'NoMaterial', displayName:'No. Material', field: 'PartsCode', width: '10%', enableCellEdit: false, headerCellClass: "middle"},
          { name:'NamaMaterial', displayName:'Nama Material', field: 'PartName', width: '18%', enableCellEdit: false, headerCellClass: "middle"},
          { name:'QtySalesBilling', displayName:'Qty Sales Billing', field: 'QtySB', width: '5%', enableCellEdit: false, headerCellClass: "middle"},
          { name:'Satuan', displayName:'Satuan', field: 'SatuanName', width: '6%', enableCellEdit: false, headerCellClass: "middle"},
          { 
            name:'Harga Satuan', 
            displayName:'Harga Satuan', 
            field: 'UnitPrice', 
            width: '10%', 
            enableCellEdit: false, 
            headerCellClass: "middle" , 
            cellFilter: 'currency:"":0',
            // aggregationType: uiGridConstants.aggregationTypes.sum,
            aggregationType: function() {
              if(!$scope.gridApi)
                  return 0;
              var sum = 0;

              //if it is filtered/sorted you want the filtered/sorted rows only)
              var visibleRows = $scope.gridApi.core.getVisibleRows($scope.gridApi.grid);
              for (var i = 0; i < visibleRows.length; i++) {
                    //you have to parse just in case the data is in string otherwise it will concatenate
                    sum += visibleRows[i].entity.QtySB * visibleRows[i].entity.UnitPrice;
                }

              console.log('VAT column ==> ',sum);  
              return parseInt(Math.round(sum));
            },
            aggregationHideLabel: true,
            footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
          },
          {
            name:'DM Persen',
            displayName:'%',
            field: 'DiscountMaterialPercent',
            width: '5%', enableCellEdit: false,
            headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:240%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Material</td></tr></tbody></table></div>%</div>"
          },
          {
            name:'DM Jumlah',
            displayName:'Jumlah',
            field: 'DiscountMaterialAmount',
            width: '7%', enableCellEdit: false,
            headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
          },
          {
            name:'DGM Persen',
            displayName:'%',
            field: 'DiscountGroupMaterialPercent',
            width: '5%', enableCellEdit: false,
            headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:240%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon SCC</td></tr></tbody></table></div>%</div>"
          },
          {
            name:'DGM Jumlah',
            displayName:'Jumlah',
            field: 'DiscountGroupMaterialAmount',
            width: '7%', enableCellEdit: false,
            headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
          },
          {
            name:'DS Persen',
            displayName:'%',
            field: 'DiscountSpecialPercent',
            width: '5%', enableCellEdit: false,
            headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:240%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Spesial</td></tr></tbody></table></div>%</div>"
          },
          {
            name:'DS Jumlah',
            displayName:'Jumlah',
            field: 'DiscountSpecialAmount',
            cellFilter: 'number',
            width: '7%', enableCellEdit: false,
            headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
          },
          // { name:'Diskon Material', displayName:'Diskon Material', field: 'DiscountGroupMaterialPercent', width: '10%'},
          // { name:'Diskon Group Material', displayName:'Diskon Group Material', field: 'DiscountGroupMaterialAmount', width: '10%'},
          // { name:'Diskon Special', displayName:'Diskon Special', field: 'DiscountSpecialAmount', width: '10%'},
          { 
            name:'PPN', 
            displayName:'PPN', 
            field: 'VATAmount', 
            width: '10%', 
            enableCellEdit: false, 
            headerCellClass: "middle", 
            cellFilter: 'number',
            // aggregationType: uiGridConstants.aggregationTypes.sum,
            aggregationType: function() {
              if(!$scope.gridApi)
                  return 0;
              var sum = 0;

              //if it is filtered/sorted you want the filtered/sorted rows only)
              var visibleRows = $scope.gridApi.core.getVisibleRows($scope.gridApi.grid);
              for (var i = 0; i < visibleRows.length; i++) {
                    //you have to parse just in case the data is in string otherwise it will concatenate
                    sum += visibleRows[i].entity.VATAmount;
                }

              console.log('VAT column ==> ',sum);  
              return parseInt(Math.floor(sum));
            },
            aggregationHideLabel: true,
            footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
          },
          { 
            name:'Harga Total', 
            displayName:'Harga Total', 
            field: 'TotalAmount', 
            width: '10%', 
            enableCellEdit: false, 
            headerCellClass: "middle", 
            cellFilter: 'number',
            // aggregationType: uiGridConstants.aggregationTypes.sum,
            aggregationType: function() {
              if(!$scope.gridApi)
                  return 0;
              var sum = 0;

              //if it is filtered/sorted you want the filtered/sorted rows only)
              var visibleRows = $scope.gridApi.core.getVisibleRows($scope.gridApi.grid);
              for (var i = 0; i < visibleRows.length; i++) {
                    //you have to parse just in case the data is in string otherwise it will concatenate
                    sum += visibleRows[i].entity.TotalAmount;
                }

              console.log('VAT column ==> ',sum);  
              return parseInt(Math.floor(sum));
            },
            aggregationHideLabel: true,
            footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
          },
          {
              name:'_',
              displayName:'Action',
              enableCellEdit: false,
              visible : false,
              cellTemplate:'<div style="padding-top: 5px;"><u><a href="#" ng-click="grid.appScope.Delete(row)">Hapus</a></u></div>',
              cellFilter: 'currency:"":0'
              // tidak ada di fsd
          }
        ]

      };

      $scope.hello = function(){
        alert('Data telah dihapus');
        //console.log("");
      }
      $scope.lihat = function(){
        alert('Detail Data');
        //console.log("");
      }
      $scope.Delete = function(row) {
        var index = $scope.gridOptions.data.indexOf(row.entity);
       // var index2 = $scope.gridOptionsLangsung.data.indexOf(row.entity);

        $scope.gridOptions.data.splice(index, 1);
        //$scope.gridOptionsLangsung.data.splice(index2, 1);

        console.log($scope.gridOptions.data);
        console.log(index);

        //console.log($scope.gridOptionsLangsung.data);
        //console.log(index2);
    };

    //----------------------------------
    // Grid Detail untuk mode View
    //----------------------------------
    $scope.gridOptionsView = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableFiltering : true,
        //selectedItems: console.log($scope.mySelections),
        enableSelectAll: true,
        paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 5,
        showColumnFooter: true,

        onRegisterApi: function(gridApi) {
          $scope.gridApi = gridApi;
        },

        columnDefs: [
          {
          name: 'SalesBillingItemId', field: 'SalesBillingItemId', width: '7%', visible: false
          },
          {
          name: 'SalesBillingId', field: 'SalesBillingId', width: '7%', visible: false
          },
          {
          name: 'Id', field: 'SalesOrderId', width: '7%', visible: false
          },
          {
          name: 'OutletId', field: 'OutletId', width: '7%', visible: false
          },
          { name:'PartId', field: 'PartId', width: '18%', visible: false},
          { name:'UomId', field: 'UomId', width: '18%', visible: false},
          { name:'NoMaterial', displayName:'No. Material', field: 'PartsCode', width: '10%', headerCellClass: "middle", enableCellEdit: false},
          { name:'NamaMaterial', displayName:'Nama Material', field: 'PartName', width: '18%', headerCellClass: "middle", enableCellEdit: false},
          { name:'QtySalesBilling', displayName:'Qty Sales Billing', field: 'QtySB', width: '5%', headerCellClass: "middle", enableCellEdit: false},
          { name:'Satuan', displayName:'Satuan', field: 'SatuanName', width: '6%', headerCellClass: "middle", enableCellEdit: false},
          { 
            name:'Harga Satuan', 
            displayName:'Harga Satuan', 
            field: 'UnitPrice', 
            width: '10%', 
            headerCellClass: "middle", 
            enableCellEdit: false, 
            cellFilter: 'currency:"":0',
            // aggregationType: uiGridConstants.aggregationTypes.sum,
            aggregationType: function() {
              if(!$scope.gridApi)
                  return 0;
              var sum = 0;

              //if it is filtered/sorted you want the filtered/sorted rows only)
              var visibleRows = $scope.gridApi.core.getVisibleRows($scope.gridApi.grid);
              for (var i = 0; i < visibleRows.length; i++) {
                    //you have to parse just in case the data is in string otherwise it will concatenate
                    sum += visibleRows[i].entity.QtySB * visibleRows[i].entity.UnitPrice;
                }

              console.log('VAT column ==> ',sum);  
              return parseInt(Math.round(sum));
            },
            aggregationHideLabel: true,
            footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
          },
          {
            name:'DM Persen',
            displayName:'%',
            field: 'DiscountMaterialPercent',
            width: '5%',
            enableCellEdit: false,
            headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:240%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Material</td></tr></tbody></table></div>%</div>"
          },
          {
            name:'DM Jumlah',
            displayName:'Jumlah',
            field: 'DiscountMaterialAmount',
            width: '7%',
            enableCellEdit: false,
            headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
          },
          {
            name:'DGM Persen',
            displayName:'%',
            field: 'DiscountGroupMaterialPercent',
            width: '5%',
            enableCellEdit: false,
            headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:240%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon SCC</td></tr></tbody></table></div>%</div>"
          },
          {
            name:'DGM Jumlah',
            displayName:'Jumlah',
            field: 'DiscountGroupMaterialAmount',
            width: '7%',
            enableCellEdit: false,
            headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
          },
          {
            name:'DS Persen',
            displayName:'%',
            field: 'DiscountSpecialPercent',
            width: '5%',
            enableCellEdit: false,
            headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:240%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon Spesial</td></tr></tbody></table></div>%</div>"
          },
          {
            name:'DS Jumlah',
            displayName:'Jumlah',
            field: 'DiscountSpecialAmount',
            cellFilter: 'number',
            width: '7%',
            enableCellEdit: false,
            headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>"
          },
          { 
            name:'PPN', 
            displayName:'PPN', 
            field: 'VATAmount', 
            width: '10%', 
            headerCellClass: "middle", 
            enableCellEdit: false , 
            cellFilter: 'number',
            // aggregationType: uiGridConstants.aggregationTypes.sum,
            aggregationType: function() {
              if(!$scope.gridApi)
                  return 0;
              var sum = 0;

              //if it is filtered/sorted you want the filtered/sorted rows only)
              var visibleRows = $scope.gridApi.core.getVisibleRows($scope.gridApi.grid);
              for (var i = 0; i < visibleRows.length; i++) {
                    //you have to parse just in case the data is in string otherwise it will concatenate
                    sum += visibleRows[i].entity.VATAmount;
                }

              console.log('VAT column ==> ',sum);  
              return parseInt(Math.floor(sum));
            },
            aggregationHideLabel: true,
            footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
          },
          { 
            name:'Harga Total', 
            displayName:'Harga Total', 
            field: 'TotalAmount', 
            width: '10%', 
            headerCellClass: "middle", 
            enableCellEdit: false, 
            cellFilter: 'number',
            // aggregationType: uiGridConstants.aggregationTypes.sum,
            aggregationType: function() {
              if(!$scope.gridApi)
                  return 0;
              var sum = 0;

              //if it is filtered/sorted you want the filtered/sorted rows only)
              var visibleRows = $scope.gridApi.core.getVisibleRows($scope.gridApi.grid);
              for (var i = 0; i < visibleRows.length; i++) {
                    //you have to parse just in case the data is in string otherwise it will concatenate
                    sum += visibleRows[i].entity.TotalAmount;
                }

              console.log('VAT column ==> ',sum);  
              return parseInt(Math.floor(sum));
            },
            aggregationHideLabel: true,
            footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number:0 }}</div>'
          }
        ]
      };


      //------------------------------GENARATE NO BATAL SB----------------------------
      $scope.GenerateDocNum = function() {
        PartsSalesOrder.getserviceandmaterial().then(function(res) {
          var resulti = res.data[0].Servicetype;
          console.log("What Will be Will Be = ", resulti);
          $scope.servicetypes = resulti;
          PartsCurrentUser.getFormatId($scope.servicetypes, 'SB-').then(function(res) {
            var result = res.data;
            console.log("FormatId = ", result[0].Results);

            PartsCurrentUser.getDocumentNumber2(result[0].Results).then(function(res) {
                    var DocNo = res.data;

                    if (typeof DocNo === 'undefined' || DocNo == null) {
                        bsNotify.show({
                            title: "Purchase Order",
                            content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                            type: 'danger'
                        });
                    } else {
                        $scope.mData.BatalBillingNo = DocNo[0];
                        // $scope.mData.DocDate = DocNo[1];
                        // $scope.mData.DocDate = setDate(DocNo[1]);
                        $scope.mData.DocDate = new Date(DocNo[1]);
                        $scope.NoBatalBilling = '';
                        $scope.NoBatalBilling = $scope.mData.BatalBilling;
                        console.log("NoBatalBilling", $scope.NoBatalBilling);
                    }


                },
                function(err) {
                    console.log("err=>", err);
                }
            );


        },
        function(err) {

        }
        );

        });


        // [END] Get Current User Warehouse
        console.log("NoBatalBilling", $scope.NoBatalBilling);
    }


    //------------------------------GENARATE NO BATAL SB----------------------------
    $scope.GenerateDocNumTwo = function(Header,Detail) {
      PartsSalesOrder.getserviceandmaterial().then(function(res) {
        var resulti = res.data[0].Servicetype;
        console.log("What Will be Will Be = ", resulti);
        $scope.servicetypes = resulti;
        PartsCurrentUser.getFormatId($scope.servicetypes, 'SB-').then(function(res) {
          var result = res.data;
          console.log("FormatId = ", result[0].Results);
          PartsCurrentUser.getDocumentNumber2(result[0].Results).then(function(res) {
                  var DocNo = res.data;
                  console.log("DocNo = ", DocNo);
                  if (typeof DocNo === 'undefined' || DocNo == null) {
                      bsNotify.show({
                          title: "Purchase Order",
                          content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                          type: 'danger'
                      });
                  } else {
                      console.log("Masuk sini ngga",DocNo[0],$scope.mData.DocDate);
                      $scope.mData.BatalBillingNo = DocNo[0];
                      $scope.mData.DocDate = DocNo[1];
                      Header.BatalBillingNo =  $scope.mData.BatalBillingNo;
                      Header.SalesBillingCancelNo =  $scope.mData.BatalBillingNo;
                      
                    //   console.log("NoBatalBilling", $scope.NoBatalBilling);
                    //   PartsSalesBilling.BatalBillingLangsung(Header,Detail).then(function(res) {
                    //     $scope.nocancelso = '';
                    //     $scope.nocancelso = Header.BatalBillingNo;
                    //     $scope.goBackToMe($scope.nocancelso);
                    // });
                    //New Code

                    var parCancelNo = {doctype : 'SB', dataId : $scope.mData.SalesBillingId};
                      PartsGlobal.getCancelNo(parCancelNo).then(function(res){
                        console.log("NoBatalBilling", $scope.CancelNo);
                        $scope.CancelNo = res.data.Result[0].DocNo;
                        Header['SalesBillingCancelNo'] = $scope.CancelNo;
                        // return false
                          PartsSalesBilling.BatalBillingLangsung(Header,Detail).then(function(res) {
                            $scope.nocancelso = '';
                            $scope.nocancelso = $scope.CancelNo;
                            $scope.goBackToMe($scope.nocancelso);
                        });
                      });

                      // New Code
                      // var parCancelNo = {doctype : 'SB', dataId : $scope.mData.SalesBillingId};
                      // PartsGlobal.getCancelNo(parCancelNo).then(function(res){
                      //   console.log("NoBatalBilling", $scope.NoBatalBilling);
                      //   $scope.CancelNo = res.data.Result[0].DocNo;
                      //   Header['SalesBillingCancelNo'] = $scope.CancelNo;
                      //   // return false
                      //     PartsSalesBilling.BatalBillingLangsung(Header,Detail).then(function(res) {
                      //       $scope.nocancelso = '';
                      //       $scope.nocancelso = Header.BatalBillingNo;
                      //       $scope.goBackToMe($scope.nocancelso);
                      //   });
                      // });
                  }
              },
              function(err) {
                  console.log("err=>", err);
              }
            );
          },
          function(err) {}
        );
      });
    }

    $scope.bulkPrint = function(data){

      // if(data.length > 2){
      //   bsNotify.show({
      //     title: "Approval",
      //     content: "Tidak Boleh Multiple Data. Silahkan dicek kembali",
      //     type: 'danger'
      //   });
      //   return
      // }

      
      // $scope.show_modalReprint = { show: true };
      $scope.DataReprint = [];
      $scope.DataReprint = [data];
      $scope.isReprintedStatus = 1; //1: ReqReprint 3:ApproveReprint 2:RejectReprint
      console.log('bulkprint', $scope.DataReprint);
      // var isNoCustlangsung = 0;
      // _.map(data, function(datax) {
      //   if(datax.RefSOTypeId != 2){
      //     isNoCustlangsung++;
      //   }
      // });

      // if(isNoCustlangsung == 0 ){
      //   //Valiadai untuk nama pelanggan tidak boleh beda
      //   var countDiffPelanggan = 0;
      //   _.map(data, function(val1) {
      //       _.map(data, function(val2) {
      //           if (val1.CustomerName !== val2.CustomerName) {
      //               countDiffPelanggan++;
      //           }
      //           console.log('countDiffPelanggan 1 ==>', countDiffPelanggan);
  
      //       })
      //       console.log('countDiffPelanggan 2 ==>', countDiffPelanggan);
  
      //   });

        // var countDiffStatus = 0;
        // _.map(data, function(val1) {
        //   _.map(data, function(val2) {
        //       if (val1.SalesBillingStatusId !== val2.SalesBillingStatusId) {
        //         countDiffStatus++;
        //       }
        //       console.log('countDiffStatus 1 ==>', countDiffStatus);

        //   })
        //   console.log('countDiffStatus 2 ==>', countDiffStatus);

        // });
  
        // if (countDiffPelanggan > 0) {
        //     console.log('GAK BISA PRINT ==>')
        //     bsAlert.alert({
        //             title: "Pelanggan harus sama, Silahkan cek kembali",
        //             text: "",
        //             type: "warning"
        //         },
        //         function() {},
        //         function() {}
        //     )
        // }else if(countDiffStatus > 0){
        //     console.log('GAK BISA PRINT ==>')
        //     bsAlert.alert({
        //             title: "Status sales billing harus sama, Silahkan cek kembali",
        //             text: "",
        //             type: "warning"
        //         },
        //         function() {},
        //         function() {}
        //       )
        // } else {
          var dataCekSalesBilling = [];
          for(var i in $scope.DataReprint){
            var temp =  {
              'SalesBillingId' : $scope.DataReprint[i].SalesBillingId,
              'CurrentStatus' : $scope.DataReprint[i].SalesBillingStatusId
            };
            dataCekSalesBilling.push(temp);
          }
  
          PartsSalesBilling.CekStatusSalesBilling(dataCekSalesBilling).then(function(resucek) {
            if (resucek.data == 666){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Ada perubahan status Sales billing, mohon refresh data terlebih dahulu",
                });
            } else if (resucek.data == 888) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Sales Billing tidak dapat di proses reprint, harap selesaikan approval cancel sales billing dahulu",
                });
            } else {
                var datacncleSalesBilling = []
                for(var x in $scope.DataReprint){
                  var temp1 =  {
                    'SalesBillingId' : $scope.DataReprint[x].SalesBillingId
                  };
                  datacncleSalesBilling.push(temp1);
                }
                PartsSalesBilling.CheckValdationSalesBillingCancelApproval(datacncleSalesBilling).then(function(resuv) {
                  var cekres = resuv.data.toString();
                  var dtCancel = ''
                  var haselRes = null;
                  if (cekres.includes('#')){
                      var seplit = cekres.split('#')
                      haselRes = seplit[0]
                      dtCancel = seplit[1]
                  } else {
                      haselRes = cekres
                  }
                  if (haselRes == '666'){
                      bsNotify.show({
                          size: 'big',
                          type: 'warning',
                          title: "Data tidak dapat dicetak.",
                          content: "Terdapat data sales billing [ " + dtCancel + " ] sedang dalam proses pengajuan cancel.",
                      });
                  } else if(haselRes == '0') {
    
                    var needReprint = 0;
                    var total = 0;
                    var statusBilling = 0;
                    var statusReprint = 0;
                    var statusCancel = 0;
                    _.map($scope.DataReprint, function(val) {
                        total = total + val.Total;
                        if (val.IsPrinted == 1) {
                            needReprint = 1;
                        };
                        statusBilling = val.SalesBillingStatusId;
                        statusReprint = val.SalesBillingReprintStatusId;
                        statusCancel = val.SalesBillingCancelStatusId;
                    });
    
                    if (needReprint == 1) {
      
                      if (statusReprint == 1) {
                          bsNotify.show({
                              size: 'small',
                              type: 'warning',
                              title: "Data tidak dapat dicetak, data sedang dalam proses approval"
                          });
                          console.log('Status Request Reprint Billing');
                      }
      
                      if (statusCancel == 1 || statusCancel == 3 || statusBilling == 2) {
                          bsNotify.show({
                              size: 'small',
                              type: 'warning',
                              title: "Data tidak dapat dicetak, data sudah di cancel"
                          });
                          console.log('Status Request Reprint Billing');
                      }
      
                      if (statusReprint == null || statusReprint == 2) {
                        var concatSalesBillingId = "";
                        for(var y in $scope.DataReprint){
                          if(concatSalesBillingId != ""){
                            concatSalesBillingId += ","
                            }
                          concatSalesBillingId += $scope.DataReprint[y].SalesBillingId
                        }
                        var dataInvoice = {
                          'ArrBilling': concatSalesBillingId
                        };

                        PartsSalesBilling.CheckInvoiceInfoForSalesBilling(dataInvoice).then(function(res) {
                          $scope.show_modalReprint = { show: true };
                          $scope.mDataReqReprint = res.data.Result[0];
                          var date = new Date();
                          $scope.mDataReqReprint.DocDate = date.getDate() + '/' + (date.getMonth()+1) + '/' +  date.getFullYear() +' ' + date.getHours()+':'+date.getMinutes();
                          $scope.mDataReqReprint.Approved = $scope.mDataReqReprint.EmployeeName + ' ('+$scope.mDataReqReprint.RoleName +')';
                        });
                      }
      
                      if (statusReprint == 3) {
                        if ($scope.DataReprint.length == 1) {
                          $scope.CetakSalesBillingCustLangsung($scope.DataReprint[0].SalesBillingId,true); //Cetak
                        } else if ($scope.DataReprint.length > 1) {
        
                        };
                      }

                    } else {
                        if ($scope.DataReprint.length == 1) {
                          $scope.CetakSalesBillingCustLangsung($scope.DataReprint[0].SalesBillingId,true); //Cetak
                        } else if ($scope.DataReprint.length > 1) {
        
                        };
                    }
                }
    
              });
            }
          });
      // }
     // }
      
    }

    
    $scope.show_modalReprint = { show: false };
    $scope.modalMode = 'new';
    $scope.customButtonReprint = {
      save: {
          text: "Kirim Permohonan"
      }
    };

    $scope.customButtonReprintApprov = {
      save: {
          text: "Simpan"
      }
    };

    $scope.reprintCancel = function() {
      $scope.show_modalReprint = { show: false };
    };

    $scope.reprintApproval = function() {
      var date = new Date();
      var data = {
        'OutletId': $scope.user.OutletId,
        'RoleId': $scope.user.RoleId,
        'Message': $scope.mDataReqReprint.Message,
        'SalesBillingId': $scope.DataReprint[0].SalesBillingId,
        'StatusApprovalId': $scope.isReprintedStatus, //1: ReqReprint 3:ApproveReprint 2:RejectReprint
        'TransactionId': 0,
        'ApproverRoleId': 0,
        'RequestDate': date,
        'ApproveRejectDate': null,
        'LastModifiedDate': date
      };
  
      PartsSalesBilling.ApprovalReprintSalesBilling(data).then(function(res) {
        $scope.show_modalReprint = { show: false };
        var notif = {
          Message: "Permohonan Reprint Faktur SO : " + $scope.DataReprint[0].SalesBillingNo,
          RecepientId: $scope.user.OrgId >= 2000000 ? 1129 : 1128 ,
          Param: 50
        }
        console.log(notif);
        PartsSalesBilling.sendNotifikasi(notif).then(function (res) {});

        setTimeout(function() {
          $scope.getData($scope.mData);
        }, 500);
      },
      function(err) {
        bsNotify.show({
          title: "Warning",
          content: err.data.Message,
          type: 'warning'
        });
      });
      
    };

    $scope.reprintCancelKabeng = function(){
      $scope.show_modalReprint = { show: false };
    };

    $scope.reprintApprovalKabeng = function(){

      if($scope.mDataReqReprint.Message == undefined || $scope.mDataReqReprint.Message == null){
        $scope.mDataReqReprint.Message = "";
      }
      
      var date = new Date();
      var data = {
        'OutletId': $scope.user.OutletId,
        'RoleId': '',
        'Reason': $scope.mDataReqReprint.Message,
        'Message': null,
        'SalesBillingId': $scope.DataReprint[0].SalesBillingId,
        'StatusApprovalId': $scope.isReprintedStatus, //1: ReqReprint 3:ApproveReprint 2:RejectReprint
        'TransactionId': $scope.DataReprint[0].TransactionId,
        'ApproverRoleId': $scope.mDataReqReprint.ApproverRoleId,
        'RequestDate': null,
        'ApproveRejectDate': date,
        'LastModifiedDate': date
      };
      
      var msgg = $scope.isReprintedStatus == 2 ? "Permohonan Reprint Faktur SO : "+ $scope.DataReprint[0].SalesBillingNo + " Ditolak - " + $scope.mDataReqReprint.Message
      : "Permohonan Reprint Faktur SO : "+ $scope.DataReprint[0].SalesBillingNo + " Disetujui - " + $scope.mDataReqReprint.Message;

      PartsSalesBilling.UpdateApprovalReprintSalesBilling(data).then(function(res) {
        $scope.show_modalReprint = { show: false };

        var notif = {
          Message: msgg,
          RecepientId: $scope.user.OrgId >= 2000000 ? 1150 : 1135 ,
          Param: 51
        }
        console.log(notif);
        PartsSalesBilling.sendNotifikasi(notif).then(function (res) {});

        setTimeout(function() {
          $scope.getData($scope.mData);
        }, 500);
      },
      function(err) {
        bsNotify.show({
          title: "Warning",
          content: err.data.Message,
          type: 'warning'
        });
      });
    };

    $scope.modal_modelReprint = [];
    $scope.modal_modelReprintKabeng = [];

  });
