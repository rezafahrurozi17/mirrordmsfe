angular.module('app')
    .controller('POMaintenanceController', function($scope, $http, CurrentUser, ngDialog, bsNotify, POMaintenance, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading = false;
        //$scope.gridData=[];
        $timeout(function(){
          var parts = $scope.checkRights(9);
          console.log("parts = ", parts);
          console.log('user=',$scope.user);
          console.log('$scope.user.OrgId =',$scope.user.OrgId);
          $scope.mData.OutletId = $scope.user.OrgId
          console.log('$scope.mData.OutletId =',$scope.mData.OutletId);
        });
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mData = {}; //Model
    $scope.xPOMaintenance = { selected: [] };
    $scope.xRole = {selected:[]};
    $scope.mData.OutletId = {};
    //var gridData = [];
    //$scope.xPOMaintenance.selected=[];

    $scope.gridHideActionColumn= true;
    $scope.isDetail1 = false;
    $scope.isDetail2 = false;
    $scope.Alasan = {};
    $scope.BatalData = POMaintenance.getPOMHeader();
    $scope.CloseData = POMaintenance.getPOMHeader();
    //$scope.isOverlayForm = false;

    $scope.checkRights = function(bit){
        var p=$scope.myRights & Math.pow(2,bit);
        var res= (p==Math.pow(2,bit));
        console.log("myRights => ", $scope.myRights);
        return res;
    }
    $scope.getRightX= function(a, b){
      var i = 0;
      if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
      else if ($scope.checkRights(b)) i= 2;
      else if ($scope.checkRights(a)) i= 1;
      return i;
    }
    $scope.getRightMaterialType= function(){
      var i = $scope.getRightX(8, 9);
      if (($scope.checkRights(8) && $scope.checkRights(9))) i = 3;
            else if ($scope.checkRights(9)) i = 2;
            else if ($scope.checkRights(8)) i = 1;
      return i;
    }
    $scope.getRightServiceType= function(){
      var i = $scope.getRightX(10, 11);
      return i;
    }

    $scope.localeDate = function(data) {
      var tmpDate = new Date(data);
      var resDate = new Date(tmpDate.toISOString().replace("Z","-0700")).toISOString(); 
      return resDate;
    }

    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function(mData) {
      $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
      console.log("Nyoba", $scope.mData.MaterialTypeId);
      console.log("ServiceTypeId : ", $scope.mData.ServiceTypeId);
      console.log("mData = ", $scope.mData);
      switch ($scope.user.RoleId) {
        case 1135:
            $scope.mData.MaterialTypeId = 1;
            $scope.mData.ServiceTypeId = 1;
            break;
        case 1122:
            $scope.mData.MaterialTypeId = 1;
            $scope.mData.ServiceTypeId = 1;
            break;
        case 1123:
            $scope.mData.MaterialTypeId = 1;
            $scope.mData.ServiceTypeId = 0;
            break;
        case 1124:
            $scope.mData.MaterialTypeId = 2;
            $scope.mData.ServiceTypeId = 0;
            break;
        case 1125:
            $scope.mData.MaterialTypeId = 2;
            $scope.mData.ServiceTypeId = 1;
            break;
    }
      // console.log("fHasilSOQ = ", $scope.mData.fHasilSOQ === false);
      console.log("PO.No. = ", $scope.mData.fRefPONo != "");
      console.log("PO.No. =2 ", $scope.mData.fRefPONo == " ");
      console.log("PO.No. =3 ", $scope.mData.fRefPONo === "");
      console.log("PO.No. =4 ", $scope.mData.fRefPONo === " ");
      // console.log("mData.fPOTypeId = ", $scope.mData.fPOTypeId);
      // console.log("mData.fRefPOTypeId = ", $scope.mData.fRefPOTypeId);
      // console.log("mData.fPurchaseOrderStatusId = ", $scope.mData.fPurchaseOrderStatusId);
      if(($scope.mData.fPOTypeId === undefined &&
        $scope.mData.fRefPOTypeId === undefined &&
        $scope.mData.fPurchaseOrderStatusId === undefined &&
        $scope.mData.startDate === undefined &&
        $scope.mData.endDate === undefined &&
        $scope.mData.fRefPONo === undefined &&
        $scope.mData.fLicensePlate === undefined &&
        $scope.mData.fPurchaseOrderNo === undefined &&
        $scope.mData.fName === undefined &&
        ($scope.mData.fHasilSOQ === false || $scope.mData.fHasilSOQ === undefined) &&
        ($scope.mData.fSPLDBit === false || $scope.mData.fSPLDBit === undefined) &&
        ($scope.mData.fNonSPLDBit === false || $scope.mData.fNonSPLDBit === undefined) &&
        ($scope.mData.fInvoiceBelumLengkap === false || $scope.mData.fInvoiceBelumLengkap === undefined) &&
        ($scope.mData.fInvoiceLengkap === false || $scope.mData.fInvoiceLengkap === undefined))
        ||
        ($scope.mData.fPOTypeId == null &&
          $scope.mData.fRefPOTypeId == null &&
          $scope.mData.fPurchaseOrderStatusId === undefined &&
          ($scope.mData.startDate == null || $scope.mData.startDate === undefined) &&
          ($scope.mData.endDate == null || $scope.mData.endDate === undefined) &&
          $scope.mData.fRefPONo == null &&
          $scope.mData.fLicensePlate == null &&
          $scope.mData.fPurchaseOrderNo == null &&
          $scope.mData.fName == null &&
          ($scope.mData.fHasilSOQ === false || $scope.mData.fHasilSOQ == null) &&
          ($scope.mData.fSPLDBit === false || $scope.mData.fSPLDBit == null) &&
          ($scope.mData.fNonSPLDBit === false || $scope.mData.fNonSPLDBit == null) &&
          ($scope.mData.fInvoiceBelumLengkap === false || $scope.mData.fInvoiceBelumLengkap == null) &&
          ($scope.mData.fInvoiceLengkap === false || $scope.mData.fInvoiceLengkap == null))
        ||
        (
          ($scope.mData.fPOTypeId == null || $scope.mData.fPOTypeId === undefined) &&
          ($scope.mData.fRefPOTypeId == null || $scope.mData.fRefPOTypeId === undefined) &&
          ($scope.mData.fPurchaseOrderStatusId == null || $scope.mData.fPurchaseOrderStatusId === undefined) &&
          ($scope.mData.startDate == null || $scope.mData.startDate === undefined) &&
          ($scope.mData.endDate == null || $scope.mData.endDate === undefined) &&
          ($scope.mData.fRefPONo === "" || $scope.mData.fRefPONo === " " || $scope.mData.fRefPONo === undefined)&&
          ($scope.mData.fLicensePlate === "" || $scope.mData.fLicensePlate === " " || $scope.mData.fLicensePlate === undefined) &&
          ($scope.mData.fPurchaseOrderNo === "" || $scope.mData.fPurchaseOrderNo === " " || $scope.mData.fPurchaseOrderNo === undefined) &&
          ($scope.mData.fName === "" || $scope.mData.fName === " " || $scope.mData.fName === undefined) &&
          ($scope.mData.fHasilSOQ === false || $scope.mData.fHasilSOQ == null || $scope.mData.fHasilSOQ === undefined) &&
          ($scope.mData.fSPLDBit === false || $scope.mData.fSPLDBit == null || $scope.mData.fSPLDBit === undefined) &&
          ($scope.mData.fNonSPLDBit === false || $scope.mData.fNonSPLDBit == null || $scope.mData.fNonSPLDBit === undefined) &&
          ($scope.mData.fInvoiceBelumLengkap === false || $scope.mData.fInvoiceBelumLengkap == null || $scope.mData.fInvoiceBelumLengkap === undefined) &&
          ($scope.mData.fInvoiceLengkap === false || $scope.mData.fInvoiceLengkap == null || $scope.mData.fInvoiceLengkap === undefined))
      ){
        bsNotify.show(
          {
              title: "Mandatory",
              content: "Isi salah satu filter terlebih dahulu",
              type: 'danger'
          }
        );
      } else

      if((($scope.mData.fPOTypeId === undefined &&
          $scope.mData.fRefPOTypeId === undefined &&
          $scope.mData.fPurchaseOrderStatusId === undefined &&
          $scope.mData.startDate === undefined &&
          $scope.mData.endDate === undefined)
          ||
         ($scope.mData.fPOTypeId == null &&
          $scope.mData.fRefPOTypeId == null &&
          $scope.mData.fPurchaseOrderStatusId == null &&
          $scope.mData.startDate == null &&
          $scope.mData.endDate == null))
          &&
          (($scope.mData.fRefPONo !== null || $scope.mData.fRefPONo !== undefined || $scope.mData.fRefPONo !== "") &&
            ($scope.mData.fLicensePlate !== null || $scope.mData.fLicensePlate !== undefined || $scope.mData.fLicensePlate !== "") &&
            ($scope.mData.fPurchaseOrderNo !== null || $scope.mData.fPurchaseOrderNo !== undefined || $scope.mData.fPurchaseOrderNo !== "") &&
            ($scope.mData.fName !== null || $scope.mData.fName !== undefined || $scope.mData.fName !== "") &&
            ($scope.mData.fHasilSOQ !== false || $scope.mData.fHasilSOQ !== null || $scope.mData.fHasilSOQ !== undefined) &&
            ($scope.mData.fSPLDBit !== false || $scope.mData.fSPLDBit !== null || $scope.mData.fSPLDBit !== undefined) &&
            ($scope.mData.fNonSPLDBit !== false || $scope.mData.fNonSPLDBit !== null || $scope.mData.fNonSPLDBit !== undefined) &&
            ($scope.mData.fInvoiceBelumLengkap !== false || $scope.mData.fInvoiceBelumLengkap !== null || $scope.mData.fInvoiceBelumLengkap !== undefined) &&
            ($scope.mData.fInvoiceLengkap !== false || $scope.mData.fInvoiceLengkap !== null || $scope.mData.fInvoiceLengkap !== undefined)
          )
        )
      {
        //alert('ok');
        console.log("filter is non mandatory");
        POMaintenance.getData(mData).then(function(res) { // start getdata
            var gridData = res.data.Result;
            console.log("<controller getData> GridData => ", gridData);
            for(var i in gridData) {
              gridData[i].DocDate = $scope.localeDate(gridData[i].DocDate);
              gridData[i].RequiredDate = $scope.localeDate(gridData[i].RequiredDate);        
            }
            console.log("<controller getData> GridData2 => ", gridData);
            $scope.grid.data = gridData;
            $scope.loading = false;
          },
          function(err) {
            console.log("err=>", err);
          }
        ); // end getdata
      } else {
        if($scope.mData.startDate == null || $scope.mData.startDate == 'undefined' || $scope.mData.startDate == ""){
          bsNotify.show(
              {
                  title: "Mandatory",
                  content: "Tanggal PO belum diisi",
                  type: 'danger'
              }
            );
        } else
        if($scope.mData.endDate == null || $scope.mData.endDate == 'undefined' || $scope.mData.endDate == ""){
          bsNotify.show(
              {
                  title: "Mandatory",
                  content: "Tanggal PO (end date) belum diisi",
                  type: 'danger'
              }
            );
        } else {
          POMaintenance.getData(mData).then(function(res) { // start getdata
              var gridData = res.data.Result;
              console.log("<controller getData> GridData => ", gridData);
              for(var i in gridData) {
                gridData[i].DocDate = $scope.localeDate(gridData[i].DocDate);
                gridData[i].RequiredDate = $scope.localeDate(gridData[i].RequiredDate);        
              }
              console.log("<controller getData> GridData2a => ", gridData);
                $scope.grid.data = gridData;
              $scope.loading = false;
            },
            function(err) {
              console.log("err=>", err);
            }
          ); // end getdata
        }
      }

      // POMaintenance.getData(mData).then(function(res) { // start getdata
      //     var gridData = res.data.Result;
      //     console.log("<controller getData> GridData => ", gridData);
      //     $scope.grid.data = gridData;
      //     $scope.loading = false;
      //   },
      //   function(err) {
      //     console.log("err=>", err);
      //   }
      // ); // end getdata
    }

    // Hapus Filter
    $scope.onDeleteFilter = function(){
      //Pengkondisian supaya seolaholah kosong
      $scope.mData.startDate = undefined;
      $scope.mData.endDate = undefined;
      $scope.mData.fPOTypeId = undefined;
      $scope.mData.fRefPOTypeId = undefined;
      $scope.mData.fPurchaseOrderStatusId = undefined;
      $scope.mData.fRefPONo = undefined;
      $scope.mData.fLicensePlate = undefined;
      $scope.mData.fPurchaseOrderNo = undefined;
      $scope.mData.fName = undefined;
      $scope.mData.fHasilSOQ = undefined;
      $scope.mData.fInvoiceBelumLengkap = undefined;
      $scope.mData.fInvoiceLengkap = undefined;
      $scope.mData.fSPLDBit = undefined;
      $scope.mData.fNonSPLDBit = undefined;
    }

    $scope.toViewETA = function(row, option){
      $scope.mData = row.entity;
      console.log("toViewETA $scope.mData = ", $scope.mData);
      console.log("toViewETA $scope.mData.PurchaseOrderId = ", $scope.mData.PurchaseOrderId);

      $scope.isOverlayForm = true;

      $scope.isOverlayForm = true;
      if(option == 1){
        $scope.isDetail1 = true;
        $scope.isDetail2 = false;
        //console.log("$scope.isDetail1 ? = ", $scope.isDetail1);
      } else if(option == 2){
        $scope.isDetail2 = true;
        $scope.isDetail1 = false;
        //console.log("$scope.isDetail2 ? = ", $scope.isDetail2);
      }

      // get data detail
      POMaintenance.getDetail($scope.mData, option).then(function(res) {
          var gridData = res.data;
          console.log('<controller> getDetail = ', gridData);  
          for(var i=0; i<gridData.length; i++) {
            if (gridData[i].BODate = "0001-01-01T00:00:00"){
              gridData[i].BODate = null;             
              };
            if (gridData[i].ETALast = "0001-01-01T00:00:00"){
              gridData[i].ETALast = null;             
              };
            if (gridData[i].ETAPrev = "0001-01-01T00:00:00"){
              gridData[i].ETAPrev = null;             
              };
            if (gridData[i].ETDLast = "0001-01-01T00:00:00"){
              gridData[i].ETDLast = null;             
              };
            if (gridData[i].ETDPrev = "0001-01-01T00:00:00"){
              gridData[i].ETDPrev = null;             
              };
            if (gridData[i].InvoiceDate = "0001-01-01T00:00:00"){
              gridData[i].InvoiceDate = null;             
              };
            
          }     
          console.log("someone else", gridData[i] );  
          //$scope.gridDetail.data = gridData;
          if(option == 1){
            $scope.gridDetail.data = gridData; 
          } else if (option == 2){
            $scope.gridDetailGR.data = gridData;
          }
        },
        function(err) {
          console.log("err=>", err);
        }
      ); // end getDetail

    }

    $scope.goBack = function()
    {
      //$scope.isOverlayForm = false;
      $scope.isDetail1 = false;
      $scope.isDetail2 = false;
      console.log('goBack');
    }

    $scope.onBeforeEditMode = function(row, mode){

    }

    $scope.onShowDetail = function(row, mode){
      console.log("mode apa ? = ", mode);
      // $scope.isDetailETA = true;
      // console.log("$scope.isDetailETA ? = ", $scope.isDetailETA);
      // $scope.bsForm = false;

    }

    $scope.hello = function(){
        alert('OK, PO telah dihapus');
        //console.log("");
    }

    $scope.cancelPO = function(row){
      $scope.mData = row.entity;
      console.log('row = ', $scope.mData);

      POMaintenance.setPOMHeader($scope.mData);
      console.log('POMaintenance.getPOMHeader() = ', POMaintenance.getPOMHeader());

      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/pomaintenance/referensi/cancelpo.html\'\"></div>',
        plain: true,
        controller: 'POMaintenanceController',
       });
    };

    // dialog konfirmasi
    $scope.okCancel = function(data){
      console.log('okCancel data = ', data);
      //$scope.mData = data.res;
      //console.log('okCancel $scope.mData = ', $scope.mData);
      $scope.BatalData = POMaintenance.getPOMHeader();
      $scope.BatalData.CancelReasonDesc = data.CancelReasonDesc;
      //$scope.BatalData.CancelReasonId = data.CancelReasonId;
      console.log('$scope.BatalData = ', $scope.BatalData);
      POMaintenance.setPOMHeader($scope.BatalData);

      ngDialog.openConfirm ({
        template:'\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Anda yakin akan membatalkan PO Maintenance ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinBatal(BatalData)">Ya</button>\
                     </div>\
                     </div>',
        plain: true,
        controller: 'POMaintenanceController',
       });
       console.log('okCancel CancelReasonDesc = ', $scope.BatalData.CancelReasonDesc);
       //console.log('okCancel mdata  = ', $scope.mData.CancelReasonDesc);
       //console.log('okCancel CancelReasonId  = ', $scope.BatalData.CancelReasonId);
    };

    $scope.yakinBatal = function(data){
      console.log('yakinBatal');
      $scope.mData = data;
      $scope.ngDialog.close();
      $scope.mData.PurchaseOrderStatusIdOld = $scope.mData.PurchaseOrderStatusId || 3;
      $scope.mData.PurchaseOrderStatusIdNew = 6;
      //$scope.mData.CancelReasonDesc;
      console.log('yakinBatal mData = ', $scope.mData);

      // [START] Get
      POMaintenance.cancel($scope.mData).then(function(res) {
          console.log('res cancel = ', res);
          bsNotify.show(
              {
                  title: "PO Maitenance",
                  content: "PO Maintenance has been successfully canceled.",
                  type: 'succes'
              }
            );
        },
        function(err) {
          console.log("err=>", err);
        }
      );
      // [END] Get
    }

    $scope.closePO = function(row){
      $scope.mData = row.entity;
      console.log('row = ', $scope.mData);

      POMaintenance.setPOMHeader($scope.mData);
      console.log('POMaintenance.getPOMHeader() = ', POMaintenance.getPOMHeader());

      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/pomaintenance/referensi/closepo.html\'\"></div>',
        plain: true,
        controller: 'POMaintenanceController',
       });
    };

    // dialog konfirmasi
    $scope.okClose = function(data){
      console.log('okCancel data = ', data);
      //$scope.mData = data.res;
      //console.log('okCancel $scope.mData = ', $scope.mData);
      $scope.CloseData = POMaintenance.getPOMHeader();
      $scope.CloseData.CloseReasonDesc = data.CloseReasonDesc;
      //$scope.CloseData.CloseReasonId = data.CloseReasonId;
      console.log('$scope.CloseData = ', $scope.CloseData);
      POMaintenance.setPOMHeader($scope.CloseData);

      ngDialog.openConfirm ({
        template:'\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Anda yakin akan Close PO Maintenance ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinClose(CloseData)">Ya</button>\
                     </div>\
                     </div>',
        plain: true,
        controller: 'POMaintenanceController',
       });
       console.log('okClose  = ', $scope.CloseData.CloseReasonDesc);
       //console.log('okClose mdata  = ', $scope.mData.CloseReasonId);
    };

    $scope.yakinClose = function(data){
      console.log('yakinClose');
      $scope.mData = data;
      $scope.ngDialog.close();
      $scope.mData.PurchaseOrderStatusIdOld = $scope.mData.PurchaseOrderStatusId;
      $scope.mData.PurchaseOrderStatusIdNew = 7;
      //$scope.mData.CloseReasonDesc;
      console.log('yakinClose mData = ', $scope.mData);
      console.log('yakinCloseCoba mData = ', $scope.mData.PurchaseOrderStatusIdOld);
      console.log('yakinCloseCoba2 mData = ', $scope.mData.PurchaseOrderStatusIdNew);

      // [START] Get
      POMaintenance.cancel($scope.mData).then(function(res) {
          console.log('res cancel = ', res);
          bsNotify.show(
              {
                  title: "PO Maitenance",
                  content: "PO Maintenance has been successfully closed.",
                  type: 'succes'
              }
            );
            $scope.goBack();
        },
        function(err) {
          console.log("err=>", err);
        }
      );
      // [END] Get
    }

    $scope.hello2 = function(){
        alert('PO telah di-Cancel');
        //console.log("");
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 5,

        columnDefs: [
            {
              name:'ID',
              field:'PurchaseOrderId',
              visible: false
            },
            {
              name:'Tipe Material',
              width:'7%',
              field:'Description'
            },
            {
              name:'Tipe PO',
              width:'10%',
              displayName: 'Tipe PO',
              field:'POTypeId',
              // cellFilter: 'filterTipePO'
              cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameTypePo(row.entity.POTypeId,row.entity.CampaignId)}}</div>'
            },
            {
              name:'Referensi PO',
              displayName: 'Referensi PO',
              width:'12%',
              field:'RefPOTypeId',
              cellFilter: 'filterRefPOType'
            },
            {
              name:'No. Referensi',
              width:'12%',
              field: 'RefPONo',
            },
            {
              name:'No. Polisi',
              field: 'LicensePlate',
              width:'8%'
            },
            {
              name:'hasil SOQ.',
              displayName: 'Hasil SOQ',
              field: 'HasilSOQ',
              cellTemplate: '<input type="checkbox" ng-model="row.entity.HasilSOQ" disabled>',
              cellFilter: 'filterHasilSOQ',
              width:'6%'
              //type: 'boolean'
            }, //type: 'boolean',
            {
              name:'SPLD',
              displayName: 'SPLD',
              field: 'SPLDBit',
              cellTemplate: '<input type="checkbox" ng-model="row.entity.SPLDBit" disabled>',
              cellFilter: 'filterSPLD',
              width:'5%'
              //type: 'boolean'
            }, //type: 'boolean',
            {
              name:'No. PO',
              displayName: 'No. PO',
              field: 'PurchaseOrderNo',
              width:'10%' },
            {
              name:'Tanggal PO',
              displayName: 'Tanggal PO',
              field: 'DocDate',
              cellFilter: 'date:\'yyyy-MM-dd\'',
              width:'10%' },
            {
              name:'Vendor',
              field: 'Name',
              width:'15%'},
            {
              name:'BO TAM Follow',
              displayName: 'BO TAM Follow',
              field: 'BackOrderFollowId',
              cellFilter: 'filterBOTAMFollow',
              width:'10%'
            },
            { name:'Status',
              field: 'PurchaseOrderStatusId',
              cellFilter: 'filterStatusPOMaintain',
              width:'15%'
            },
            {
              //field:'_',
              name:'_',
              visible: true, // ini membuat didropdown bulk action tidak tampil
              pinnedRight:true,
              width:'8%',
              cellTemplate:'<div style="text-align:center"><button class="btn btn-xs" style="color:white; background-color:#D53337" ng-if="((row.entity.PurchaseOrderStatusId == 3 || row.entity.PurchaseOrderStatusId == 4) && (row.entity.POTypeId != 6  || row.entity.POTypeId != 7)) || ( row.entity.BackOrderFollowId == 1 && (row.entity.POTypeId == 6 || row.entity.POTypeId == 7) && row.entity.PurchaseOrderStatusId != 1 && row.entity.PurchaseOrderStatusId != 7) || ((row.entity.POTypeId == 6 || row.entity.POTypeId == 7) && row.entity.PurchaseOrderStatusId != 1 && row.entity.StatusTPOS == null && row.entity.PurchaseOrderStatusId != 7)" ng-click="grid.appScope.$parent.closePO(row)">Close PO</button></div>'
            },
            {
              name:'__',
              //field:'__',
              visible: true,
              pinnedRight:true,
              width:'8%',
              cellTemplate:'<div style="text-align:center"><button class="btn btn-xs" style="color:white; background-color:#D53337" ng-if="((row.entity.PurchaseOrderStatusId == 1 || row.entity.PurchaseOrderStatusId == 3) && row.entity.BackOrderFollowId == 1 && (row.entity.POTypeId != 6 || row.entity.POTypeId != 7)) || (row.entity.POTypeId != 6 || row.entity.POTypeId != 7) ng-click="grid.appScope.$parent.cancelPO(row)">Cancel PO</button></div>'
            },
            {
              name:'___',
              displayName:'',
              enableCellEdit: false,
              visible: true,
              pinnedRight:true,
              width:'8%',
              cellTemplate:'<div style="padding-top: 5px;" class="ui-grid-cell-contents"><u><a href="#" ng-if="row.entity.POTypeId !== 4" ng-click="grid.appScope.$parent.toViewETA(row, 1)">ETD & ETA</a></u></div>'
            },
            {
              name:'____',
              displayName:'',
              enableCellEdit: false,
              visible: true,
              pinnedRight:true,
              width:'8%',
              cellTemplate:'<div style="padding-top: 5px;" class="ui-grid-cell-contents"><u><a href="#" ng-if="row.entity.POTypeId !== 4" ng-click="grid.appScope.$parent.toViewETA(row, 2)">Shipping</a></u></div>'
            }
        ]
    };

    //grid detail
    $scope.gridDetail = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            enableFiltering : true,
            //enableHorizontalScrollbar: 2,
            enableVerticalScrollbar: 2,
            paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            //enableHorizontalScrollbar : 0,
            paginationPageSize: 15,
            columnDefs: [
                { name:'No. Material', width:100, field:'PartsCode', headerCellClass: "middle", pinnedLeft:true},
                { name:'Nama Material', width:100, field:'PartsName', headerCellClass: "middle", pinnedLeft:true},
                { name:'Qty PO', width:100, displayName: 'QtyPO',  field:'QtyPO', headerCellClass: "middle" },
                { name:'Total Qty GR', width:100, displayName: 'QtyGR', field:'TotalQtyGR', headerCellClass: "middle" },
                { name:'Qty Remain', width:100, field:'QtyRemain', headerCellClass: "middle" },
                {
                  name:'BO Date',
                  width:100,
                  displayName: 'BO Date',
                  field:'BODate',
                  cellFilter: 'date:\'yyyy-MM-dd\'',
                  headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:200%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>BO</td></tr></tbody></table></div>BO Date</div>"
                },
                {
                  name:'BO Qty',
                  width:100,
                  displayName: 'BO QTY',
                  field:'BOQty',
                  headerCellTemplate: "<div class='ui-grid-spilt-header-main'>BO Qty</div>"
                },
                {
                  name:'Original E.T.D.',
                  width:150,
                  displayName: 'Original ETD',
                  field:'ETDOri',
                  cellFilter: 'date:\'yyyy-MM-dd HH:mm\'',
                  headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:300%'><table class='ui-grid-header-table'><tbody><tr><td colspan='3'>Original</td></tr></tbody></table></div>ETD</div>"
                },
                {
                  name:'Original E.T.A.',
                  width:150,
                  displayName: 'Original ETA',
                  field:'ETAOri',
                  cellFilter: 'date:\'yyyy-MM-dd HH:mm\'',
                  headerCellTemplate: "<div class='ui-grid-spilt-header-main'>ETA</div>"
                },
                {
                  name:'qtyGROriginal',
                  width:150,
                  displayName: 'QTY GR Original',
                  field:'QtyOri',
                  headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty GR</div>"
                },
                {
                  name:'Sebelumnya ETD',
                  width:100,
                  displayName: 'Sebelumnya ETD',
                  field:'ETDPrev',
                  cellFilter: 'date:\'yyyy-MM-dd\'',
                  headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:300%'><table class='ui-grid-header-table'><tbody><tr><td colspan='3'>Sebelumnya</td></tr></tbody></table></div>ETD</div>"
                },
                {
                  name:'Sebelumnya ETA',
                  width:100,
                  displayName: 'Sebelumnya ETA',
                  field:'ETAPrev',
                  cellFilter: 'date:\'yyyy-MM-dd\'',
                  headerCellTemplate: "<div class='ui-grid-spilt-header-main'>ETA</div>"
                },
                {
                  name:'qtyGRSebelumnya',
                  width:100,
                  displayName: 'QTY GR Sebelumnya',
                  field:'QtyPrev',
                  headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty GR</div>"
                },
                {
                  name:'Terakhir ETD',
                  width:100,
                  displayName: 'Terakhir ETD',
                  field:'ETDLast',
                  cellFilter: 'date:\'yyyy-MM-dd\'',
                  headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:300%'><table class='ui-grid-header-table'><tbody><tr><td colspan='3'>Terakhir</td></tr></tbody></table></div>ETD</div>"
                },
                {
                  name:'Terakhir ETA',
                  width:100,
                  displayName: 'Terakhir ETA',
                  field:'ETALast',
                  cellFilter: 'date:\'yyyy-MM-dd\'',
                  headerCellTemplate: "<div class='ui-grid-spilt-header-main'>ETA</div>"
                },
                {
                  name:'qtyGRTerakhir',
                  width:100,
                  displayName: 'QTY GR Terakhir',
                  field:'QtyLast',
                  headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty GR</div>"
                },
                { name:'Alasan Perubahan ETD & ETA', width:200, displayName: 'Alasan Perubahan ETD & ETA', field:'ETDETAChangeReason', headerCellClass: "middle" },

                { name:'Satuan', width:100, field:'Satuan', headerCellClass: "middle" },
                { name:'Grand Total', width:100, field:'TotalAmount', headerCellClass: "middle" }
            ],
    };

    //grid detail 2 - good receipt
    $scope.gridDetailGR = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            enableFiltering : true,
            //enableHorizontalScrollbar: 2,
            enableVerticalScrollbar: 2,
            paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            //enableHorizontalScrollbar : 0,
            paginationPageSize: 15,
            columnDefs: [
                { name:'No. Material', width:100, field:'PartsCode', headerCellClass: "middle", pinnedLeft:true},
                { name:'Nama Material', width:100, field:'PartsName', headerCellClass: "middle", pinnedLeft:true},
                { name:'Qty PO', width:100, displayName: 'QtyPO',  field:'QtyPO', headerCellClass: "middle" },
                { name:'Total Qty GR', width:100, displayName: 'QtyGR', field:'TotalQtyGR', headerCellClass: "middle" },
                { name:'Qty Remain', width:100, field:'QtyRemain', headerCellClass: "middle" },

                { name:'Nomor G.R.', width:100, displayName: 'Nomor GR', field:'GoodsReceiptNo', headerCellClass: "middle" },
                { name:'Nomor Shipping', width:100, field:'ShippingNo', headerCellClass: "middle" },
                { name:'Tanggal GR', width:100, displayName: 'Tanggal GR', field:'TanggalGR', cellFilter: 'date:\'yyyy-MM-dd\'', headerCellClass: "middle" },
                { name:'Nomor Vendor Invoice', width:100, field:'InvoiceNo', headerCellClass: "middle" },
                { name:'Tanggal Invoice', width:100, field:'InvoiceDate', cellFilter: 'date:\'yyyy-MM-dd\'', headerCellClass: "middle" },
                { name:'qtyGR2', width:100, displayName: 'QTY GR', field:'QtyGR', headerCellClass: "middle" },
                { name:'Satuan', width:100, field:'Satuan', headerCellClass: "middle" },
                { name:'Grand Total', width:100, field:'TotalAmount', headerCellClass: "middle" }
            ],
    };

    $scope.ToNameTypePo = function(POTypeId,idCampaign) {
      var GiName;
      idCampaign = idCampaign == undefined ? null : idCampaign;

      if(parseInt(POTypeId) == 1){ //Type 1
          GiName = "PO TAM Tipe Order 1";
      }else if (parseInt(POTypeId) == 2){ //Type 2
          GiName = "PO TAM Tipe Order 2";
      }else if (parseInt(POTypeId) == 3 && idCampaign == null){ //Type 3
          GiName = "PO TAM Tipe Order 3";
      }else if(parseInt(POTypeId) == 3 && idCampaign != null){  //Type 3 menjadi Campaign karena ada campaignid
          GiName = "PO TAM Campaign 3 & C";
      }else if (parseInt(POTypeId) == 6){ //Type 6
          GiName = "PO TAM Tipe Order T";
      }else if (parseInt(POTypeId) == 7 && idCampaign == null){ //Type Campaign dengan sub type kosong
          GiName = "PO TAM Campaign 3 & C";
      }else if (parseInt(POTypeId) == 7 && idCampaign != null){ //Type Campaign dengan sub type C
          GiName = "PO TAM Campaign 3 & C";
      }else if(parseInt(POTypeId) == 4){ //Type PO NON TAM
          GiName = "PO NON TAM";
      }else if(parseInt(POTypeId) == 5){ //Type PO SUBLET
          GiName = "PO SUBLET";
      }
      return GiName;
  }

})

.filter('filterTipePO', function () {
  var xtipePO = {
    '1': 'PO TAM Tipe Order 1',
    '2': 'PO TAM Tipe Order 2',
    '3': 'PO TAM Tipe Order 3',
    '6': 'PO TAM Tipe Order T',
    '7': 'PO TAM Campaign 3 & C',
    '4': 'PO NON TAM',
    '5': 'PO SUBLET'
  };
  return function(input) {
    if (!input){
      return '';
    } else {
      return xtipePO[input];
    }
  };
})

.filter('filterRefPOType', function () {
  var xRefPOType = {
    '1': 'SOP Appointment',
    '2': 'SOP WO',
    '3': 'SOP Sales Order',
    '4': 'NON SOP - Hasil SOQ',
    '5': 'NON SOP - Tanpa SOQ'
  };
  return function(input) {
    if (!input){
      return '';
    } else {
      return xRefPOType[input];
    }
  };
})

.filter('filterBOTAMFollow', function () {
  var xBOTAMFollow = {
    '1': 'Fill',
    '2': 'Kill'
  };
  return function(input) {
    if (!input){
      return '';
    } else {
      return xBOTAMFollow[input];
    }
  };
})

.filter('filterStatusPOMaintain', function () {
  var xstatus = {
    '1': 'Draft',
    '2': 'Request Approval',
    '3': 'Open',
    '4': 'Partial',
    '5': 'Completed',
    '6': 'Cancelled',
    '7': 'Closed'
  };
  return function(input) {
    if (!input){
      return '';
    } else {
      return xstatus[input];
    }
  };
})

.filter('filterHasilSOQ', function () {
  var xHasilSOQ = {
    0: false,
    1: true
  };
  return function(input) {
    if (!input){
      return '';
    } else {
      return xHasilSOQ[input];
    }
  };
})

.filter('filterSPLD', function () {
  var xSPLD = {
    0: false,
    1: true
  };
  return function(input) {
    if (!input){
      return '';
    } else {
      return xSPLD[input];
    }
  };
})
;
