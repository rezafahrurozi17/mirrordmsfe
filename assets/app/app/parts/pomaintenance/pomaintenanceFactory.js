angular.module('app')
  .factory('POMaintenance', function($http, CurrentUser, $window, $filter) {
    var currentUser = CurrentUser.user;   
    var POMHeader = {}; 

    return { 
      getData: function(data) {
        console.log("[Factory]", data);
        // console.log('<factory> data.fHasilSOQ => ',data.fHasilSOQ);
        // console.log('<factory> data.fInvoiceBelumLengkap => ',data.fInvoiceBelumLengkap);
        // console.log('<factory> data.fSPLDBit => ',data.fSPLDBit);
        // console.log('<factory> data.fNonSPLDBit => ',data.fNonSPLDBit);
        var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
        var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');
        // console.log("<factory> endDate => ", endDate);
        //console.log('<factory> data.fName => ',data.fName);
        var res=$http.get('/api/as/POMaintenance', {params: {
                                                          POTypeId : (data.fPOTypeId == null ? "n/a" : data.fPOTypeId),
                                                          RefPOTypeId : (data.fRefPOTypeId == null ? "n/a" : data.fRefPOTypeId),
                                                          PurchaseOrderStatusId: (data.fPurchaseOrderStatusId == null ? "n/a" : data.fPurchaseOrderStatusId),
                                                          startDate: (data.startDate == null ? "n/a" : startDate),
                                                          endDate: (data.endDate == null ? "n/a" : endDate),
                                                          RefPONo : (data.fRefPONo == null ? "n/a" : data.fRefPONo),
                                                          MaterialTypeId : 1,
                                                          LicensePlate : (data.fLicensePlate == null ? "n/a" : data.fLicensePlate),
                                                          PurchaseOrderNo : (data.fPurchaseOrderNo == null ? "n/a" : data.fPurchaseOrderNo), 
                                                          MaterialTypeId : (data.MaterialTypeId == null ? "n/a" : data.MaterialTypeId),   
                                                          Name : (data.fName == null ? "n/a" : data.fName),
                                                          HasilSOQ: data.fHasilSOQ, //(typeof(data.fHasilSOQ) == 'undefined' ? false : data.fHasilSOQ),
                                                          InvoiceBelumLengkap: data.fInvoiceBelumLengkap, //(typeof(data.fInvoiceBelumLengkap) == 'undefined' ? false : data.fInvoiceBelumLengkap)
                                                          InvoiceLengkap: data.fInvoiceLengkap,
                                                          SPLDBit: data.fSPLDBit,
                                                          NonSPLDBit: data.fNonSPLDBit
                                                        } });
        console.log('<factory> hasil => ',res);
        //console.log('<factory> HasilSOQ => ', HasilSOQ);
        //console.log('<factory> InvoiceBelumLengkap => ', InvoiceBelumLengkap);
        //res.data.Result = null;
        return res;
      },
      getDetail: function(data, option) {
        console.log("[Factory] getDetail data = ", data);
        console.log("[Factory] getDetail option = ", option);
        var res=$http.get('/api/as/POMaintenance/POMaintenanceDetail', {params: {                                                          
                                                          PurchaseOrderNo : data.PurchaseOrderNo,
                                                          Option : option,
                                                          OutletId : data.OutletId
                                                        } });
        console.log('<factory> getDetail res => ',res);        
        return res;
      },
      cancel: function(data){
        console.log('Update => ', data);        
        return $http.put('/api/as/POMaintenance', [{
                                            PurchaseOrderId: (data.PurchaseOrderId == null ? 0 : data.PurchaseOrderId),
                                            PurchaseOrderNo: data.PurchaseOrderNo, 
                                            OutletId: data.OutletId, 
                                            WarehouseId: data.WarehouseId, 
                                            DocDate: data.DocDate, 
                                            RequiredDate: data.RequiredDate,
                                            MaterialTypeId: data.MaterialTypeId, 
                                            POTypeId: data.POTypeId, 
                                            RefPOTypeId: data.RefPOTypeId,
                                            RefPONo: data.RefPONo,
                                            VendorId: data.VendorId,
                                            VendorOutletId: data.VendorOutletId,
                                            VehicleId: data.VehicleId,
                                            CustomerId: data.CustomerId,
                                            BackOrderFollowId: data.BackOrderFollowId,
                                            PaymentMethodId: data.PaymentMethodId,
                                            PaymentPeriod: data.PaymentPeriod,
                                            TotalAmount: data.TotalAmount,
                                            PurchaseOrderStatusIdOld: data.PurchaseOrderStatusIdOld,
                                            PurchaseOrderStatusIdNew: data.PurchaseOrderStatusIdNew,
                                            PurchaseOrderStatusId: data.PurchaseOrderStatusIdNew,
                                            CancelReasonDesc: data.CancelReasonDesc,
                                            CancelReasonId: data.CancelReasonId,
                                            CloseReasonDesc: data.CloseReasonDesc,
                                            CloseReasonId: data.CloseReasonId,
                                            StatusTPOS: data.StatusTPOS,
                                          }]);
      },
      setPOMHeader : function(data){
        POMHeader = data;
      },
      getPOMHeader : function(){
        return POMHeader;
      },
    }
  });