angular.module('app')
    .factory('PartsStatusStockCabLain', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getDataNon: function() {
                var res=$http.get('/api/as/StatusStock');
                console.log('hasil getData =>',res);
                return res;
            },

            getData: function(data) {
                // console.log("[Factory] = ", data);
                // var res = $http.get('/api/as/StatusStock', {params: {
                //                                                   OutletId : (data.OutletId == null ? "n/a" : data.OutletId),
                //                                                   MaterialTypeId : (data.MaterialTypeId == null ? "n/a" : data.MaterialTypeId),
                //                                                   WarehouseId : (data.WarehouseId == null ? "n/a" : data.WarehouseId),
                //                                                   PartsCode : (data.PartsCode == null ? "n/a" : data.PartsCode),
                //                                                   PartsName : (data.PartsName == null ? "n/a" : data.PartsName)
                //                                                 } });
                // console.log('[Factory] OutletId =>', data.OutletId);
                // console.log('[Factory] OrgId =>', data.OrgId);
                // console.log('[Factory] WarehouseId =>', data.WarehouseId);
                // console.log('hasil=>',res);
                // //res.data.Result = null;
                // return res;

                console.log("[Factory] = ", data);
                var res = $http.get('/api/as/StatusStock/cablain', {params: {
                                                                  OutletId : data.OutletId,
                                                                  PartsCode : data.PartsCode,
                                                                  PartsName : data.PartsName
                                                                } });
                return res;
            },
            getWarehouse: function(data) {
                console.log("[Factory] getWarehouse data = ", data);
                var res=$http.get('/api/as/StatusStock/GetWarehouse2', {params: {OrgId : data.OrgId}});
                console.log('getWarehouse =>',res);
                return res;
            },
            // added by sss on 2018-01-09
            getOtherOutlet: function(orgId) {
                var res=$http.get('/api/as/StatusStock/getOutletLain');
                return res;
            }
            // 
        }
});
