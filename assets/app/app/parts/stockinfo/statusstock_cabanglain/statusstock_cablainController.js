angular.module('app')
    .controller('PartsStatusStockCabLainController', function($scope, $http, CurrentUser, PartsStatusStockCabLain,$timeout, ngDialog, bsNotify,//, parApproval
      PartsGlobal, bsTab
      ) {
    //----------------------------------
    // Start-Up
    //----------------------------------

    $scope.$on('$viewContentLoaded', function() {
        $scope.loading = false;
        console.log('$scope.user = ', $scope.user );
        var uData = $scope.user;
        //$scope.gridData=[];
        // untuk menyuplai data combobox filter Warehouse
        // $timeout(function(){            
        //     PartsStatusStockCabLain.getWarehouse(uData).then(
        //         function(res) {
        //             var parts = $scope.checkRights();
        //             console.log("on, parts = ", parts);
        //             console.log('on, $scope.user =',$scope.user);

        //             //var data = res.data.Result;
        //             var data = res.data;
        //             console.log("data wrhs = ", data);
        //             console.log("uData = ", uData);
        //             //console.log(" $scope.warehouseData.WarehouseId = ",  data[1].WarehouseId);
        //             $scope.mData.OutletId = data[0].OrgId;
        //             //$scope.mData.OutletId = data.OrgId;
        //             $scope.mData.WarehouseData = data;
        //             console.log('$scope.mData.OutletId => ', $scope.mData.OutletId);
        //             console.log('uData.OrgId => ', uData.OrgId);
        //             console.log('$scope.mData.WarehouseData => ', $scope.mData.WarehouseData);
        //         },
        //         function(err) {
        //             console.log("err=>", err);
        //         }
        //     );
              
        // });

    });

    $scope.checkRights = function(bit){
        var p=$scope.myRights & Math.pow(2,bit);
        var res= (p==Math.pow(2,bit));
        console.log("myRights => ", $scope.myRights);
        return res;
    }
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mData = {}; //Model
    $scope.xRole = {selected:[]};
    $scope.fno_material = '';
    $scope.fnama_material = '123';
    $scope.gridHideActionColumn = true;
    $scope.newTab = function(strmenu){
        console.log("newTab=>");
        // bsTab.add("app.purchaseorderparts");
        bsTab.add(strmenu);
    } 

    $scope.outletData = [];
    PartsStatusStockCabLain.getOtherOutlet().then(
        function(res) {
            $scope.outletData = res.data.Result;
        },
        function(err) {
            console.log("err=>", err);
        }
    );            

    $scope.hapusFilter = function(){
        //var str1='Hapus Filter ';
        //var str2= this.fno_material;
        //var str3= this.fnama_material;
        //alert (str1.concat(str2, str3));
        this.fno_material = '';
        this.fnama_material ='';
    };

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.btnBuatTO = function(){
        // alert ("Test TO");
        PartsGlobal.showAlert({
            title:'Buat TO',
            content:'Ke Menu Trx TO',
            type:'info'
        });
        $scope.newTab("app.parts_transferorder"); 
    };
    var gridData = [];

    $scope.InfoCabang = function(data){
        // alert ('Detail info : '+ data.cabang);
        PartsGlobal.getInfoCabang(data);            
    };

    $scope.getData = function(mData) {
        //console.log("Filter Object => ")
        console.log("mData = ", $scope.mData);
        console.log('$scope.user = ', $scope.user );
        // $scope.mData.OutletId = $scope.user.OrgId;
        PartsStatusStockCabLain.getData(mData).then(function(res) {
            var gridData = res.data.Result;
            console.log("GridData = ", gridData);
            $scope.grid.data = gridData;
            $scope.loading = false;
            if (gridData == "" || gridData == undefined || gridData == null || gridData == []){
                bsNotify.show(
                    {
                        title: "Info",
                        content: "Data tidak ditemukan",
                        type: 'warning'
                    }
                );
            }
        },
        function(err) {
            console.log("err=>", err);
        });
    }

    // Hapus Filter
    $scope.onDeleteFilter = function(){
        //Pengkondisian supaya seolaholah kosong
        $scope.mData.MaterialTypeId = null;
        $scope.mData.WarehouseId = null;
        $scope.mData.PartsCode = null;
        $scope.mData.PartsName = null;
    }

        // $scope.loading = false;
    //} kurung tutup getData

    // $scope.getData = function() {
    //     PartsStatusStock.getData()
    //     .then(
    //         function(res){
    //             gridData = [];
    //             //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
    //             //-console.log("role=>", res.data.Result); //-
    //             $scope.grid.data = res.data.Result;
    //             console.log("role=>",res.data.Result);
    //             //console.log("grid data=>",$scope.grid.data);
    //             //$scope.roleData = res.data;
    //             $scope.loading=false;
    //         },
    //         function(err){
    //             console.log("err=>",err);
    //         }
    //     );
    // }

    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);

    }
    $scope.qtyReserved = function(data){
        if (data.QtyReserved == 0) { 
            PartsGlobal.showMessage('No Reserved Material to show');
            return 0;
        }
        console.log("on qty Reserved (data)=>", data);
        // parApproval.objMaterial = data;
        //for(var k in data) parApproval.objMaterial[k]=data[k];
        PartsGlobal.setparobjMaterial(data);
        console.log("on objMaterial (data) :>", PartsGlobal.getparobjMaterial());
        ngDialog.openConfirm ({
            //template:'<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Input Alasan Close PO</h3></div> <div class="panel-body"><form method="post"><div class="form-group "> <label class="control-label requiredField" style="color:black;" for="message">Alasan<span class="asteriskField">*</span></label> <textarea class="form-control" cols="40" id="message" name="message" rows="10"></textarea></div> <div class="form-group"><div> <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Kembali</button> <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()" style="color:white; background-color:#D53337">OK</button> </div></div></form></div></div>',
            //template:'<div ng-include=\"\'app/parts/inquiry/orderStatus/templates/pendingDetail.html\'\"></div>',
            template:'<div ng-include=\"\'app/parts/stockinfo/statusstock/ss_listqtyreserved.html\'\"></div>',
            plain: true,
            controller: 'PartsStatusStockController',
        });
    };

    $scope.cekDeadStock = function(row){
        alert("Cek Dead Stock ", row); return 0;
        ngDialog.openConfirm ({
            //template:'<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Input Alasan Close PO</h3></div> <div class="panel-body"><form method="post"><div class="form-group "> <label class="control-label requiredField" style="color:black;" for="message">Alasan<span class="asteriskField">*</span></label> <textarea class="form-control" cols="40" id="message" name="message" rows="10"></textarea></div> <div class="form-group"><div> <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Kembali</button> <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()" style="color:white; background-color:#D53337">OK</button> </div></div></form></div></div>',
            //template:'<div ng-include=\"\'app/parts/inquiry/orderStatus/templates/pendingDetail.html\'\"></div>',
            template:'<div ng-include=\"\'app/parts/stockinfo/deadstock/dead_stock.html\'\"></div>',
            plain: true,
            controller: 'PartsDeadStockController',//'PartsStatusStockController',
        });
    };

    $scope.onBeforeEditMode = function(mode, row){
        console.log("test", row);
    }

    $scope.locationDetail = function(row){
        $scope.tempPopup = {};
        $scope.tempPopup = angular.copy(row);
        console.log("data row dari grid, cek alamat outlet", row);
        ngDialog.openConfirm({
      //template:'<div ng-include=\"\'app/parts/directpartssales/salesorder/referensi/dialog_konfirmasi_batal.html\'\"></div>',
          template: '\
                       <div align="center" class="ngdialog-buttons">\
                       <h4><b>Information</b></h4>\
                       <br>\
                       <p>{{tempPopup.Cabang}}</p>\
                       <p>{{tempPopup.AddressOutlet}}</p>\
                       <br>\
                       <p>Telp. {{tempPopup.PhoneOutlet}}</p>\
                       <div class="ngdialog-buttons" align="center">\
                       </div>\
                       </div>',
          plain: true,
          controller: 'PartsStatusStockController',
          scope:$scope
        });
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    // var cellTemp =  '<div class="ui-grid-cell-contents">'+
    //                 '   <div>{{row.entity.Cabang}}  <a href="#" ng-click="grid.appScope.$parent.InfoCabang(row.entity)"><i class="fa fa-map-marker" aria-hidden="true"></i></a></div>'+
    //                 '</div>';
    var cellTemp =  '<div class="ui-grid-cell-contents">'+
                    '   <table><td>{{row.entity.Cabang}}</td>  <td><i class="fa fa-map-marker" style="font-size:20px; margin-left:15px;" ng-click="grid.appScope.$parent.locationDetail(row.entity)"></i></td></table>'+
                    // '<div style="text-align:left; margin-left: 5px;"><button class="btn wbtn btn-xs" ng-click="grid.appScope.$parent.locationDetail(row)">Lokasi</button></div> </div>'+
                    '</div>';

                    // '<div class="ui-grid-cell-contents">'+
                    // '   <div>{{row.entity.Cabang}}  <div style="text-align:left; margin-left: 5px;"><button class="btn wbtn btn-xs" ng-click="grid.appScope.$parent.locationDetail(row)">Lokasi</button></div> </div>'+
                    // '</div>';

    // '<div>{{row.entity.Cabang}}  <a href="#" ng-click="grid.appScope.$parent.InfoCabang(row.entity)"><i class="fa fa-map-marker" aria-hidden="true"></i></a></div>';
    var gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
    '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Buat" ng-hide="row.entity.StatusCode == 0" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-plus-circle" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
    '</div>';

    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200],
        paginationPageSize: 10,
        columnDefs: [
            { displayName: "No. Material", name:'No. Material', field:'PartsCode', width:'10%'},
            { displayName: "Nama Material", name:'Nama Material', field:'PartsName', width:'15%'},
            {
                displayName: "Status Stok",
                name:'Free',
                width:'15%',
                field:'QtyFree',
                cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                    if (row.entity.QtyFree == 0) {
                        return 'red';
                    }
                    else return 'testnot';
                }
            },
            { displayName: "Satuan", name:'Satuan', field:'SatuanName', width:'15%'},
            { displayName: "Cabang", name:'Lokasi Rak', field:'Cabang', cellTemplate: cellTemp, width:'45%'},
            //{ displayName: "Action", name:'Lokasi Rak', field:'Cabang', cellTemplate: '<div style="text-align:left; margin-left: 5px;"><button class="btn wbtn btn-xs" ng-click="grid.appScope.$parent.locationDetail(row)">Lokasi</button></div>'}
         ]
    };
});
