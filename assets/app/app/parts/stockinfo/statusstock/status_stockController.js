angular.module('app')
    .controller('PartsStatusStockController', function($scope, $http, PartsCurrentUser, CurrentUser, PartsStatusStock,$timeout, ngDialog, bsNotify,//, parApproval
      bsTab, PartsGlobal, PartsLookup
      ) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    console.log('user yg login', $scope.user);
    $scope.mData = {}; //Model
    $scope.xRole = {selected:[]};
    $scope.fno_material = '';
    $scope.fnama_material = '123';
    $scope.gridHideActionColumn = true;

    $scope.checkRights=function(bit){
      console.log("Rights : ", $scope.myRights);
        var p= $scope.myRights & Math.pow(2,bit);
        var res=(p==Math.pow(2,bit));
        console.log("myRights => ", $scope.myRights);
        return res;
    }
    $scope.getRightX= function(a, b){
      var i = 0;
      if (($scope.checkRights(a) && $scope.checkRights(b))) i = 2;
      else if ($scope.checkRights(b)) i = 1;
      else if ($scope.checkRights(a)) i = 0;
      return i;
    }
    $scope.getRightMaterialType= function(){
      //var i = $scope.getRightX(8, 9);
      // if (($scope.checkRights(8) && $scope.checkRights(9))) i = 3;
      // else if ($scope.checkRights(9)) i = 2;
      // else if ($scope.checkRights(8)) i = 1;
      // return i;
      PartsStatusStock.getServiceAndMaterial().then(function(res) {
        $scope.materialtypes = res.data[0].MaterialType;
      });
      var materialTypeId = $scope.materialtypes;
      return materialTypeId;
    }
    $scope.getRightServiceType= function(){
      // var i = $scope.getRightX(10, 11);
      // return i;
      PartsStatusStock.getServiceAndMaterial().then(function(res) {
        $scope.servicetypes = res.data[0].Servicetype;
      });
      var serviceTypeId =  $scope.servicetypes;
      return serviceTypeId;
    }

    $scope.$on('$viewContentLoaded', function() {
        $scope.loading = false;
        console.log('$scope.user = ', $scope.user );
        PartsGlobal.partVersion=  "17.12.17.022.21"; //"17.12.13.020.25" ;//"17.12.13.011.27" ;//"17.12.12.020.03" ;
        var uData = $scope.user;
        $scope.getWHID();
        //$scope.gridData=[];
      // untuk menyuplai data combobox filter Warehouse
      $timeout(function(){
        console.log('user=',$scope.user);
        console.log("Rights : ", $scope.myRights);
        // // $scope.getWHID();
        // $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
        // $scope.mData.MaterialName= PartsLookup.lkp_MaterialTypeName($scope.mData.MaterialTypeId);
        // $scope.mData.ServiceTypeId = $scope.getRightServiceType();
        console.log("M T : ", $scope.mData.MaterialTypeId);
        console.log("S T : ", $scope.mData.ServiceTypeId);
        console.log("Mat Name : ", $scope.mData.MaterialName);
        console.log("FE Part Version : ", PartsGlobal.partVersion) ; //getPartVersion
        PartsGlobal.getPartVersion().then(function(res) {
          var pVer = res.data.Result;
          console.log("BE Part Version : ", pVer) ;
        });
        PartsCurrentUser.getWarehouseIdi().then(function(res) {        
          for (var i=0; i < res.data.length; i++) {  
            $scope.mData.WarehouseId = res.data[i].warehouseid;
          }       
    
        });

        if (PartsGlobal.myWarehouse = {}) {
          if ($scope.mData.MaterialTypeId > 0 && $scope.mData.ServiceTypeId > 0) {
            // PartsGlobal.getMyWarehouse({ //getWarehouse
            PartsGlobal.getWarehouse({
              OutletId : $scope.user.OrgId, MaterialTypeId : $scope.mData.MaterialTypeId,
              ServiceTypeId : $scope.mData.ServiceTypeId}).then(function(res) {
                var gridData = res.data.Result;
                console.log("warehouse ", gridData[0]);
                PartsGlobal.myWarehouses = gridData;
                PartsGlobal.myWarehouse = gridData[0];
                $scope.mData.WarehouseId = gridData[0].WarehouseId;
                if ($scope.mData.MaterialTypeId = 3) {
                  $scope.mData.WarehouseName = "Gudang GR (Parts dan Bahan)";
                } else {
                $scope.mData.WarehouseName = gridData[0].WarehouseName;
                }
              },
              function(err) {
                console.log("err=>", err);
              }
            );
          }
          $scope.mData.OutletId = $scope.user.OrgId;
         
          PartsCurrentUser.getWarehouseIdi().then(function(res) {
            console.log("Part pak ari",res.data[0].result); 
            for (var i=0; i < res.data.length; i++) {
              console.log("Part pak ari",res.data[i].warehouseid);   
              $scope.mData.WarehouseId = res.data[i].warehouseid;
              $scope.mData.OutletId = $scope.user.OrgId;
            }       
    
          });
          // PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.mData.OutletId).then(function(res) {
          //   var gridData = res.data;
          //   console.log('gridData[0] = ', gridData[0]);    
          //   $scope.mData.WarehouseId = gridData[0].WarehouseId;
          //   $scope.mData.OutletId = gridData[0].OutletId;
          //   $scope.mData.WarehouseData = gridData;
          // })
          // ;
        } else {
          PartsCurrentUser.getWarehouseIdi().then(function(res) {        
            for (var i=0; i < res.data.length; i++) {  
              $scope.mData.WarehouseId = res.data[i].warehouseid;
            }       
      
          });
          //$scope.mData.WarehouseId = PartsGlobal.myWarehouse.WarehouseId;
          if (PartsGlobal.myWarehouse.WarehouseName != null || PartsGlobal.myWarehouse.WarehouseName != undefined){
            $scope.mData.WarehouseName = PartsGlobal.myWarehouse.WarehouseName;
          }
          else{

          }
        }

        // PartsStatusStock.getWarehouse(uData).then(function(res)
        //   {
        //     var parts = $scope.checkRights();
        //     console.log("on, parts = ", parts);
        //     console.log('on, $scope.user =',$scope.user);

        //     //var data = res.data.Result;
        //     var data = res.data;
        //     console.log("data wrhs = ", data);
        //     console.log("uData = ", uData);
        //     //console.log(" $scope.warehouseData.WarehouseId = ",  data[1].WarehouseId);
        //     $scope.mData.OutletId = data[0].OrgId;
        //     //$scope.mData.OutletId = data.OrgId;
        //     $scope.mData.WarehouseData = data;
        //     console.log('$scope.mData.OutletId => ', $scope.mData.OutletId);
        //     console.log('uData.OrgId => ', uData.OrgId);
        //     console.log('$scope.mData.WarehouseData => ', $scope.mData.WarehouseData);
        //   },
        //   function(err) {
        //       console.log("err=>", err);
        //   }
        // );
      },1000);
    });
    $scope.newTab = function(strmenu){
        console.log("newTab=>");
        // bsTab.add("app.purchaseorderparts");
        bsTab.add(strmenu);
    }

    $scope.checkRights = function(bit){
        var p=$scope.myRights & Math.pow(2,bit);
        var res= (p==Math.pow(2,bit));
        console.log("myRights => ", $scope.myRights);
        return res;
    }


    $scope.hapusFilter = function(){
      //var str1='Hapus Filter ';
      //var str2= this.fno_material;
      //var str3= this.fnama_material;
      //alert (str1.concat(str2, str3));
      this.fno_material = '';
      this.fnama_material ='';
    };
    $scope.PartId = 1; // sementara !!!!!! 01-12-2017
    $scope.getDataApproval = function(mData) {
      $scope.IsApproval = true;
      console.log("getData mData = ", $scope.mData);
      PartsStatusStock.getListStockReservedReqApproval({PartId : $scope.PartId}).then(function(res) {
        console.log('isOutstandingSO di getData = ', $scope.isOutstandingSO);
          var gridData = res.data.Result;
          console.log("<controller getDataApproval> GridData => ", gridData);
          PartsGlobal.formLookupGrid.data = $scope.gridLookUpApprovalAlokasi.data = gridData;
          $scope.loading = false;
          return gridData;
          // $scope.IsApproval = true;
        },
        function(err) {
          console.log("err=>", err);
        }
      );
    }

    $scope.ListApproval = function(){
      // do something
      console.log("Mesti call Pop Up Grid for Approval ?");
      PartsGlobal.formLookupTitle = "Daftar Approval Alokasi Stok";
      PartsGlobal.formLookupSubTitle1 = "Daftar Alokasi Stok : ";
      PartsGlobal.formLookupSubTitle2 = PartsGlobal.getCurrDate();
      PartsGlobal.formLookupGrid = $scope.gridLookUpApprovalAlokasi; //"gridLookUpPO"; $scope.getDaftarPO()
      PartsGlobal.formLookupGrid.multiSelect = false;
      // PartsGlobal.formLookupGrid.data = $scope.getDataApproval();//[];//$scope.getVendor();
      $scope.getDataApproval();
      PartsGlobal.cancelledLookupGrid = true;

      PartsGlobal.formLookupGrid.data.push({
          MaterialRequestItemId : 1,
          RefMRTypeId : 1,
          RefMRNo : '214/ANG/1712-000071',
          VehicleId : "dsadsa",
          LicensePlate : "dsadsa",
          RequiredDate : "dsadsa",
          RequestBy : "dsadsa",
          idpo : "dsadsa",
          idgr : "dsadsa",
          PartId : "dsadsa",
          QtyReserved : "dsadsa",
          RefAllocTypeId : "dsadsa",
          RefAllocId : "dsadsa",
          RefAllocNo : "dsadsa",
          Qty : "dsadsa",
          StockReservedStatusId : 1
      });


      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/templates01/frm_lookup02.html\'\"></div>',
        plain: true,
        controller: 'PartsStatusStockController',
       }).then(function (value) {
                    // $scope.selectedVendor = PartsGlobal.selectedLookupGrid;
                    console.log(value);
                    console.log("Data selected : ");
                    if (PartsGlobal.cancelledLookupGrid) return 1;

                    var dataArray = [];
                    dataArray.push(value);

                    if(value.FlagApproval != 1){
                      bsNotify.show({
                            title: "Status Stock : Approval",
                            content: "Anda Hanya bisa Melihat, Tidak bisa Melakukan Appproval",
                            type: 'danger'
                      });
                    }
                    // else if(PartsGlobal.statusApproval == 'approve')
                    //   PartsStatusStock.doApproveAlokasiReserved(dataArray)
                    // else if(PartsGlobal.statusApproval == 'reject')
                    //   PartsStatusStock.doRejectAlokasiReserved(dataArray)

                }, function (value) {
                  // $scope.selectedVendor = PartsGlobal.selectedLookupGrid;
                  console.log(value);
                  console.log("No Data fetched ");
                  if (PartsGlobal.cancelledLookupGrid) return 1;
                  // $scope.mData.VendorId= $scope.selectedVendor[0].VendorCode;
                });
    };


    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];

    // $scope.getDataWH = function() {
    //   PartsStatusStock.getWarehouse().then(function(res)
    //     {
    //       // bsNotify.show(
    //       //   {
    //       //       title: "Warehouse",
    //       //       content: "Data Warehouse",
    //       //       type: 'success'
    //       //   }
    //       // );
    //       var data = res.data.Result;
    //       console.log("data wrhs = ", data);
    //       $scope.warehouseData.WarehouseId = data.WarehouseId;
    //       console.log(" $scope.warehouseData.WarehouseId = ",  $scope.warehouseData.WarehouseId);
    //     },
    //     function(err) {
    //       console.log("err=>", err);
    //     }
    //     );
    // }


    $scope.getWHID = function() {
      PartsCurrentUser.getWarehouseIdi().then(function(res) {
        console.log("Part pak ari",res.data[0].result); 
        for (var i=0; i < res.data.length; i++) {
          console.log("Part pak ari",res.data[i].warehouseid);   
          $scope.mData.WarehouseId = res.data[i].warehouseid;
          $scope.mData.MaterialTypeId = res.data[i].MaterialType;
          $scope.mData.ServiceTypeId = res.data[i].Servicetype;
          $scope.mData.MaterialName= PartsLookup.lkp_MaterialTypeName($scope.mData.MaterialTypeId);
          $scope.mData.OutletId = $scope.user.OrgId;
        }       

      });
    }
    $scope.getData = function(mData) {
      $scope.getWHID();

      //login 41 -> RoleId: 1135 -> GR
      //login 25 -> RoleId: 1122 -> GR
      //login 26 -> RoleId: 1123 -> BP
      //login 27 -> RoleId: 1125 -> bahan
    //   if ($scope.user.RoleId == 1135) {
    //     $scope.mData.MaterialTypeId = 1;
    //     $scope.mData.ServiceTypeId = 1;
    //     console.log('1135', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
    //     $scope.FunctionGetData(mData);
    //   } else if ($scope.user.RoleId == 1122) {
    //     $scope.mData.MaterialTypeId = 1;
    //     $scope.mData.ServiceTypeId = 1;
    //     console.log('1122', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
    //     $scope.FunctionGetData(mData);
    //   } else if ($scope.user.RoleId == 1123) {
    //     $scope.mData.MaterialTypeId = 1;
    //     $scope.mData.ServiceTypeId = 0;
    //     console.log('1123', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
    //     $scope.FunctionGetData(mData);
    //   } else if ($scope.user.RoleId == 1125) {
    //     $scope.mData.MaterialTypeId = 2;
    //     $scope.mData.ServiceTypeId = 1;
    //     $scope.FunctionGetData(mData);
    //   }  else if ($scope.user.RoleId == 1124) {
    //     $scope.mData.MaterialTypeId = 2;
    //     $scope.mData.ServiceTypeId = 0;
    //     $scope.FunctionGetData(mData);
    //   } else if ($scope.user.RoleId == 1128) {
    //     $scope.mData.MaterialTypeId = 1;
    //     $scope.mData.ServiceTypeId = 1;
    //     $scope.FunctionGetData(mData);
    //   }


    // } // end getData

    // $scope.FunctionGetData = function(mData) {
    //   console.log("$ane bos ");
    //   if ($scope.user.RoleId == 1135) {
    //     $scope.mData.MaterialTypeId = 1;
    //     $scope.mData.ServiceTypeId = 1;
    //     console.log('1135', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
    //   } else if ($scope.user.RoleId == 1122) {
    //     $scope.mData.MaterialTypeId = 1;
    //     $scope.mData.ServiceTypeId = 1;
    //     console.log('1122', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
    //   } else if ($scope.user.RoleId == 1123) {
    //     $scope.mData.MaterialTypeId = 1;
    //     $scope.mData.ServiceTypeId = 0;
    //     console.log('1123', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
    //   } else if ($scope.user.RoleId == 1125) {
    //     $scope.mData.MaterialTypeId = 2;
    //     $scope.mData.ServiceTypeId = 1;
    //     console.log('1125', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
    //   } else if ($scope.user.RoleId == 1124) {
    //     $scope.mData.MaterialTypeId = 2;
    //     $scope.mData.ServiceTypeId = 0;
    //     console.log('1124', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
    //   } else if ($scope.user.RoleId == 1128) {
    //     $scope.mData.MaterialTypeId = 1;
    //     $scope.mData.ServiceTypeId = 1;
    //     console.log('1124', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
    //   }
    //   $scope.mData.OutletId = $scope.user.OrgId;
    //   console.log("$scope.user.OrgId => ", $scope.mData.OutletId);
    //   console.log("$scope.mData.ServiceTypeId ", $scope.mData.ServiceTypeId);
    //   console.log("$scope.mData.MaterialTypeId ", $scope.mData.MaterialTypeId);
    //   PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.mData.OutletId).then(function(res) {
    //       var gridData = res.data;          
    //         $scope.mData.WarehouseId = gridData[0].WarehouseId;
    //         $scope.mData.OutletId = gridData[0].OutletId;          
    //     },
    //     function(err) {
    //       console.log("err=>", err);
    //     }
    //   );
    //   console.log("$scope.mData ok => ", $scope.mData);
    //   console.log("$scope.mData.PartsCode => ", $scope.mData.PartsCode);

      // if($scope.mData.MaterialTypeId == null || $scope.mData.MaterialTypeId == 'undefined' || $scope.mData.MaterialTypeId == ""){
      //   bsNotify.show(
      //         {
      //             title: "Mandatory",
      //             content: "Material Type belum diisi",
      //             type: 'danger'
      //         }
      //       );
      // } else 
      
      if($scope.mData.PartsCode == null || $scope.mData.PartsCode == 'undefined' || $scope.mData.PartsCode == ""){
        if($scope.mData.PartsName == null || $scope.mData.PartsName == 'undefined' || $scope.mData.PartsName == ""){
          bsNotify.show(
              {
                  title: "Mandatory",
                  content: "Parts Code belum diisi",
                  type: 'danger'
              }
            );
        } else {
          console.log("PartsCode OK");
          console.log("mData = ", $scope.mData);
          console.log('$scope.user = ', $scope.user);

          //$scope.mData.OutletId = $scope.user.OrgId;
          PartsStatusStock.getData(mData).then(function(res) {
              var gridData = res.data.Result;
              console.log("GridData = ", gridData);
              $scope.grid.data = gridData;
              $scope.loading = false;
              if (gridData == "" || gridData == undefined || gridData == null || gridData == []){
                bsNotify.show(
                    {
                        title: "Info",
                        content: "Data tidak ditemukan",
                        type: 'warning'
                    }
                );
              }
            },
            function(err) {
              console.log("err=>", err);
            }
          );
        }

      } else if($scope.mData.PartsName == null || $scope.mData.PartsName == 'undefined' || $scope.mData.PartsName == ""){
        if($scope.mData.PartsCode == null || $scope.mData.PartsCode == 'undefined' || $scope.mData.PartsCode == ""){
          bsNotify.show(
              {
                  title: "Mandatory",
                  content: "Parts Name belum diisi",
                  type: 'danger'
              }
            );
        } else {
          $scope.getWHID();
          console.log("PartsName OK");
          console.log("mData = ", $scope.mData);
          console.log('$scope.user = ', $scope.user);
          //$scope.mData.OutletId = $scope.user.OrgId;
          
          PartsStatusStock.getData(mData).then(function(res) {
              var gridData = res.data.Result;
              console.log("GridData = ", gridData);
              $scope.grid.data = gridData;
              $scope.loading = false;
              if (gridData == "" || gridData == undefined || gridData == null || gridData == []){
                bsNotify.show(
                    {
                        title: "Info",
                        content: "Data tidak ditemukan",
                        type: 'warning'
                    }
                );
              }
            },
            function(err) {
              console.log("err=>", err);
            }
          );
        }

      } else {
        console.log("mData = ", $scope.mData);
        console.log('$scope.user = ', $scope.user);
        //$scope.mData.OutletId = $scope.user.OrgId;
        $scope.getWHID();
        PartsStatusStock.getData(mData).then(function(res) {
            var gridData = res.data.Result;
            console.log("GridData = ", gridData);
            $scope.grid.data = gridData;
            $scope.loading = false;
            if (gridData == "" || gridData == undefined || gridData == null || gridData == []){
              bsNotify.show(
                  {
                      title: "Info",
                      content: "Data tidak ditemukan",
                      type: 'warning'
                  }
              );
            }
          },
          function(err) {
            console.log("err=>", err);
          }
        );
      } // end else
    }


    $scope.FunctionGetData2 = function(mData) {
      // $scope.mData.ServiceTypeId = 1;
      // $scope.mData.ServiceTypeId = 0;
      $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
      $scope.mData.ServiceTypeId = $scope.getRightServiceType();
      console.log("$ane bos 2");
      console.log("$scope.mData.PartsCode => ", $scope.mData.PartsCode);
      if($scope.mData.MaterialTypeId == null || $scope.mData.MaterialTypeId == 'undefined' || $scope.mData.MaterialTypeId == ""){
        bsNotify.show(
              {
                  title: "Mandatory",
                  content: "Material Type belum diisi",
                  type: 'danger'
              }
            );
      } else if($scope.mData.PartsCode == null || $scope.mData.PartsCode == 'undefined' || $scope.mData.PartsCode == ""){
        if($scope.mData.PartsName == null || $scope.mData.PartsName == 'undefined' || $scope.mData.PartsName == ""){
          bsNotify.show(
              {
                  title: "Mandatory",
                  content: "Parts Code belum diisi",
                  type: 'danger'
              }
            );
        } else {
          console.log("PartsCode OK");
          console.log("mData = ", $scope.mData);
          console.log('$scope.user = ', $scope.user);



          $scope.mData.OutletId = $scope.user.OrgId;

          $scope.mData.ServiceTypeId = 1;
          $scope.getWHID();
          PartsStatusStock.getData(mData).then(function(res) {
              var gridData = res.data.Result;
              console.log("GridData = ", gridData);
              if (gridData.length != 0){
                $scope.grid.data = gridData;
                $scope.loading = false;
                if (gridData == "" || gridData == undefined || gridData == null || gridData == []){
                  bsNotify.show(
                      {
                          title: "Info",
                          content: "Data tidak ditemukan",
                          type: 'warning'
                      }
                  );
                }
              } else {
                $scope.mData.ServiceTypeId = 0;
                $scope.getWHID();
                PartsStatusStock.getData(mData).then(function(res) {
                  gridData = res.data.Result;
                  $scope.grid.data = gridData;
                  $scope.loading = false;
                  if (gridData == "" || gridData == undefined || gridData == null || gridData == []){
                    bsNotify.show(
                        {
                            title: "Info",
                            content: "Data tidak ditemukan",
                            type: 'warning'
                        }
                    );
                  }

                });
              }

            },
            function(err) {
              console.log("err=>", err);
            }
          );
        }

      } else if($scope.mData.PartsName == null || $scope.mData.PartsName == 'undefined' || $scope.mData.PartsName == ""){
        if($scope.mData.PartsCode == null || $scope.mData.PartsCode == 'undefined' || $scope.mData.PartsCode == ""){
          bsNotify.show(
              {
                  title: "Mandatory",
                  content: "Parts Name belum diisi",
                  type: 'danger'
              }
            );
        } else {
          console.log("PartsName OK");
          console.log("mData = ", $scope.mData);
          console.log('$scope.user = ', $scope.user);
          $scope.mData.OutletId = $scope.user.OrgId;

          $scope.mData.ServiceTypeId = 1;
          $scope.getWHID();
          PartsStatusStock.getData(mData).then(function(res) {
              var gridData = res.data.Result;
              console.log("GridData = ", gridData);
              if (gridData.length != 0){
                $scope.grid.data = gridData;
                $scope.loading = false;
                if (gridData == "" || gridData == undefined || gridData == null || gridData == []){
                  bsNotify.show(
                      {
                          title: "Info",
                          content: "Data tidak ditemukan",
                          type: 'warning'
                      }
                  );
                }

              } else {
                $scope.mData.ServiceTypeId = 0;
                $scope.getWHID();
                PartsStatusStock.getData(mData).then(function(res) {
                  gridData = res.data.Result;
                  $scope.grid.data = gridData;
                  $scope.loading = false;
                  if (gridData == "" || gridData == undefined || gridData == null || gridData == []){
                    bsNotify.show(
                        {
                            title: "Info",
                            content: "Data tidak ditemukan",
                            type: 'warning'
                        }
                    );
                  }
                });
              }

            },
            function(err) {
              console.log("err=>", err);
            }
          );
        }

      } else {
        console.log("mData = ", $scope.mData);
        console.log('$scope.user = ', $scope.user);
        $scope.mData.OutletId = $scope.user.OrgId;

        $scope.mData.ServiceTypeId = 1;
        $scope.getWHID();
        PartsStatusStock.getData(mData).then(function(res) {
            var gridData = res.data.Result;
            console.log("GridData = ", gridData);
            if (gridData.length != 0){
              $scope.grid.data = gridData;
              $scope.loading = false;
              if (gridData == "" || gridData == undefined || gridData == null || gridData == []){
                bsNotify.show(
                    {
                        title: "Info",
                        content: "Data tidak ditemukan",
                        type: 'warning'
                    }
                );
              }
            } else {
              $scope.mData.ServiceTypeId = 0;
              $scope.getWHID();
              PartsStatusStock.getData(mData).then(function(res) {
                gridData = res.data.Result;
                $scope.grid.data = gridData;
                $scope.loading = false;
                if (gridData == "" || gridData == undefined || gridData == null || gridData == []){
                  bsNotify.show(
                      {
                          title: "Info",
                          content: "Data tidak ditemukan",
                          type: 'warning'
                      }
                  );
                }
              });
            }

          },
          function(err) {
            console.log("err=>", err);
          }
        );
      } // end else
    }






    // Hapus Filter
    $scope.onDeleteFilter = function(){
      //Pengkondisian supaya seolaholah kosong
      $scope.mData.MaterialTypeId = null;
      $scope.mData.WarehouseId = null;
      $scope.mData.PartsCode = null;
      $scope.mData.PartsName = null;
    }

        // $scope.loading = false;
    //} kurung tutup getData

    // $scope.getData = function() {
    //     PartsStatusStock.getData()
    //     .then(
    //         function(res){
    //             gridData = [];
    //             //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
    //             //-console.log("role=>", res.data.Result); //-
    //             $scope.grid.data = res.data.Result;
    //             console.log("role=>",res.data.Result);
    //             //console.log("grid data=>",$scope.grid.data);
    //             //$scope.roleData = res.data;
    //             $scope.loading=false;
    //         },
    //         function(err){
    //             console.log("err=>",err);
    //         }
    //     );
    // }

    $scope.selectRole = function(rows){
        console.log("onSelectRows=> a",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=> b",rows);

    }
    $scope.qtyReserved = function(data){
      if (data.QtyReserved == 0) {
        PartsGlobal.showMessage('No Reserved Material to show');
        return 0;
      }
      console.log("on qty Reserved (data)=>", data);
      // parApproval.objMaterial = data;
      //for(var k in data) parApproval.objMaterial[k]=data[k];
      PartsGlobal.setparobjMaterial(data);
      console.log("on objMaterial (data) :>", PartsGlobal.getparobjMaterial());
      ngDialog.openConfirm ({
        //template:'<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Input Alasan Close PO</h3></div> <div class="panel-body"><form method="post"><div class="form-group "> <label class="control-label requiredField" style="color:black;" for="message">Alasan<span class="asteriskField">*</span></label> <textarea class="form-control" cols="40" id="message" name="message" rows="10"></textarea></div> <div class="form-group"><div> <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Kembali</button> <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()" style="color:white; background-color:#D53337">OK</button> </div></div></form></div></div>',
        //template:'<div ng-include=\"\'app/parts/inquiry/orderStatus/templates/pendingDetail.html\'\"></div>',
        template:'<div ng-include=\"\'app/parts/stockinfo/statusstock/ss_listqtyreserved.html\'\"></div>',
        plain: true,
        controller: 'PartsStatusStockController',
       });
    };

    $scope.cekDeadStock = function(row){
      // alert("Cek Dead Stock ", row); //return 0;
      // ngDialog.openConfirm ({
      //   template:'<div ng-include=\"\'app/parts/stockinfo/deadstock/dead_stock.html\'\"></div>',
      //   plain: true,
      //   controller: 'PartsDeadStockController',//'PartsStatusStockController',
      //  });
       $scope.newTab("app.parts_deadstock");
    };

    $scope.onBeforeEditMode = function(mode, row){
        console.log("test", row);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        enableFullRowSelection:true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200],
        paginationPageSize: 10,
        columnDefs: [
            { name:'StockPositionId', field:'StockPositionId', width:'7%', visible:false },
            { displayName: 'Tipe Material', name:'Tipe Material', field:'TipeMaterial', width:'10%', enableCellEdit: false},
            { name:'PartId', field:'PartId', visible:false},
            { name:'No. Material', displayName: "No. Material", allowCellFocus:false,  field:'PartsCode', width:'10%' },
            { displayName: "Nama Material", name:'Nama Material', field:'PartsName', width:'15%'},
            { displayName: "Retail Price", name:'RetailPrice', field:'RetailPrice', width:'10%', cellFilter: 'number: 2'},
            { displayName: "Standard Cost", name:'StandardCost', field:'StandardCost', width:'10%', cellFilter: 'number: 2'},
            { displayName: "Lokasi Material", name:'Lokasi Material', field:'WarehouseName', width:'15%'},
            { displayName: "SCC", name:'SCC', field:'SCC', width:'10%'},
            { displayName: "ICC", name:'ICC', field:'ICC', width:'5%'},
            { displayName: "Lokasi Rak", name:'Lokasi Rak', field:'ShelfName', width:'8%'},
            { displayName: "On Hand", name:'On Hand', field:'QtyOnHand', width:'8%'},
            {
              displayName: "Free",
              name:'Free',
              //field:'{{QtyOnHand - QtyReserved}}',
              width:'5%',
              field:'QtyFree'
              ,
              cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                   if (row.entity.QtyFree == 0 || row.entity.QtyFree < 0) {
                    row.entity.QtyFree = 0
                     return 'red';
                     }
                     else return 'testnot';
                 }
                 
            },
            {
              displayName: "Reserved",
              name:'Reserved',
              field:'QtyReserved',
              width:'10%',
              cellTemplate://'<div style="text-align:center"><button class="btn btn-xs" style="color:white; background-color:#D53337" ng-click="grid.appScope.$parent.closePO()">Close PO</button></div>'
                        '<p class="ui-grid-cell-contents"><a href="#" ng-click="grid.appScope.$parent.qtyReserved(row.entity)"><u>{{row.entity.QtyReserved}}</u></a></p>'
            },
            { displayName: "Blocked Stock", name:'Blocked Stock', field:'QtyBlocked', width:'10%'},
            { displayName: "Order", name:'Order', field:'QtyOrder', width:'7%'},
            { displayName: "On Order", name:'On Order', field:'QtyOnOrder', width:'10%'},
            { displayName: "On Transfer Order", name:'On Transfer Order', field:'QtyOnTransferOrder', width:'15%'},
            { displayName: "Satuan", name:'Satuan', field:'SatuanName', width:'10%'},            
            {
              displayName: "Suggestion",
              name:'',
              field:'Suggestion_',
              width:'10%',
              enableFiltering:false,
              //cellTemplate:'<div ng-show="{{row.entity.QtyOnHand - row.entity.QtyReserved - row.entity.QtyBlocked}}==0">' +
              cellTemplate:'<div ng-show="row.entity.QtyFree == 0">' +
                        '<a href="#" ng-click="grid.appScope.$parent.cekDeadStock(row)">Cek Dead Stock</a>' +
                        '</div>'
            }

            //{ displayName: "", name:'PartCode', field:'PartCode', displayName: 'Part Code', width:'10%'},
            //{ displayName: "", name:'PartName', field:'PartName', displayName: 'Part Name', width:'10%'}

            // { name:'id',    field:'Id', width:'7%', visible:false },
            // { name:'Tipe material', field:'type_material' },
            // { name:'No. Material',  field: 'no_material' },
            // { name:'Nama Material',  field: 'nama_material' },
            // { name:'Lokasi Gudang',  field: 'lok_gudang' },
            // { name:'SCC', field: 'scc', displayName: 'SCC' }, //scc
            // { name:'ICC',  field: 'icc', displayName: 'ICC' }, // icc
            // { name:'Lokasi Rak',  field: 'lok_rak' }, // lokasi rak
            // { name:'On Hand',  field: 'onhand' }, // on hand
            // { name:'Free',  field: 'free', //background-color: 'red'} ,
            //  cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
            //     if (grid.getCellValue(row ,col) == 0) {
            //         return 'red';
            //         }
            //         else return 'testnot';
            //     }
            // }, // on hand
            // { name:'Reserved',  field: 'reserved',
            // cellTemplate://'<div style="text-align:center"><button class="btn btn-xs" style="color:white; background-color:#D53337" ng-click="grid.appScope.$parent.closePO()">Close PO</button></div>'
            //            '<a href="#" ng-click="grid.appScope.$parent.qtyReserved(row.entity)"><u>{{row.entity.reserved}}</u></a>'
            //            },//'</div>' }, // on hand
            // { name:'Blocked Stock',  field: 'qtyblock' },
            // { name:'Order ',  field: 'qtyorder' },
            // { name:'On Order ',  field: 'qtyonorder' },
            // { name:'On Transfer Order ',  field: 'qtyotorder' },
            // { name:'Satuan ',  field: 'satuan' },
            // { name:'Suggestion ',  field: 'suggest',
            // cellTemplate:'<div ng-show="{{row.entity.free}}==0">' +
            //            '<a href="#" ng-click="grid.appScope.$parent.cekDeadStock(row)">Cek Dead Stock</a>' +
            //            '</div>' }, // on hand
         ]
    };

    var dateFilter = 'date:"dd/MM/yyyy HH:mm:ss"';
    var cellTrxNo = '<div><div class="input-group"><input type="text" ng-model="row.entity.RefAllocNo" ng-value="row.entity.RefAllocNo">\
                      <label class="input-group-btn"><span class="btn wbtn" ng-click="grid.appScope.SearchDestinationAllocate(row.entity)">\
                      <i class="fa fa-search fa-1"></i></span>\
                      </label></div></div>';
    $scope.gridLookUpApprovalAlokasi = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        onRegisterApi: function (gridApi) {
          $scope.gridApi = gridApi;
          gridApi.selection.on.rowSelectionChanged($scope,function(rows){
              $scope.mySelections = gridApi.selection.getSelectedRows();
              console.log("Item(s) : ", $scope.mySelections); // ...
              PartsGlobal.selectedLookupGrid = $scope.mySelections;
          });
        },
        columnDefs: [
            { name:'id',    field:'MaterialRequestItemId', width:'7%', visible:false },
            { name:'Referensi Transaksi', field:'RefMRTypeId', enableCellEdit: false,
              editableCellTemplate: 'ui-grid/dropdownEditor', cellFilter: 'mapTrx',
              editDropdownValueLabel: 'trx',
              editDropdownOptionsArray: [{ id: 1, trx: 'Appointment' }, { id: 2, trx: 'WO' },
              { id: 3, trx: 'Sales Order' }] },
            { name:'No Referensi ',  field: 'RefMRNo', enableCellEdit: false, width: "15%" },
            { name:'VehicleId',  field: 'VehicleId', enableCellEdit: false, visible:false },
            { name:'No Polisi',  field: 'LicensePlate', enableCellEdit: false },
            { name:'Tgl Dibutuhkan',  field: 'RequiredDate', enableCellEdit: false, cellFilter: dateFilter  },
            { name:'Petugas', field: 'RequestBy', enableCellEdit: false },
            { name:'No PO',  field: 'idpo', displayName: 'No. PO', visible:false },
            { name:'No GR',  field: 'idgr', displayName: 'No. GR', visible:false },
            { name:'PartId', field:'PartId', visible:false },
            { name:'Qty Reserved',  field: 'QtyReserved', enableCellEdit: false, width: "6%" },
            { name:'Transaksi',  field: 'RefAllocTypeId', enableCellEdit: false,
              editableCellTemplate: 'ui-grid/dropdownEditor', cellFilter: 'mapTrx',
              editDropdownValueLabel: 'trx',
              editDropdownOptionsArray: [{ id: 1, trx: 'Appointment' }, { id: 2, trx: 'WO' },
              { id: 3, trx: 'Sales Order' }] },
            { name:'id trx',  field: 'RefAllocId', visible: false, enableCellEdit: false},
            { name:'No Transaksi',  field: 'RefAllocNo', enableCellEdit: false, //cellTemplate: cellTrxNo, 
              width: "15%" },
            { name:'Qty Alokasi',  field: 'Qty', enableCellEdit: true, width: "6%" },
            { name:'Status ',  field: 'StockReservedStatusId', enableCellEdit: false,
              editableCellTemplate: 'ui-grid/dropdownEditor', cellFilter: 'mapStatusReserved',
              editDropdownValueLabel: 'trx',
              editDropdownOptionsArray: [{ id: 0, trx: 'Request Approval' }, { id: 1, trx: 'Completed' }]
            }
        ]
        ,
         data : []//$scope.getDataReserved({MaterialRequestId : -1})
    };
})
.filter('mapStatusReserved', function() {
  var trxHash = {
      1: 'Request Approval',
      2: 'Completed'
  };

return function(input) {
  if (!input){
    return '';
  } else {
    return trxHash[input];
  }
};
})
;
