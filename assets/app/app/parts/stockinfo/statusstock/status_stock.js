angular.module('app')
  .factory('PartsStatusStock', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getDataNon: function() {
        //-var res=$http.get('/api/as/Parts');

        //var res=$http.get('/api/fw/UserPeople'); //- Nanti Harus diganti yg atas (kalo back end udah jalan)
        //var res=$http.get('/api/as/TestVehicle');
        //console.log('res=>',res); */
        //      */
      //console.log('res.=>',res);
      //return res;
        //return this.parts_status;

        var res=$http.get('/api/as/StatusStock');
        console.log('hasil getData =>',res);
        //res.data.Result = null;
        return res;
      },      
      getListQtyReserved: function(data) {
        console.log('param Data =>', data); 
        var res=$http.get('/api/as/StatusStock/StockReserved', {params: { PartId: data.PartId} });
        console.log('hasil getData =>',res);        
        return res;
      }, 
      // api/as/StatusStock/StockReserved/ReqApproval
      getListStockReservedReqApproval: function(data) {
        console.log('param Data =>', data); 
        var res=$http.get('/api/as/StatusStock/StockReserved/ReqApproval');
        console.log('hasil getData =>',res);        
        return res;
      },
      getApprovalStock: function(data) {
        var res=$http.get('/api/as/PartGlobal/GetApprovalStockPosition',{params: { NoRef: data} });
        return res;
      },
      getListDestinationAllocateTo: function(data) {        
        var res=$http.get('/api/as/StatusStock/StockReserved/AllocateTo', {params: { PartId: data.PartId,
                        MaterialRequestItemId: data.MaterialRequestItemId} });
        console.log('hasil getData =>',res);        
        return res;
      }, // AskApprovalForAllocate
      AskApprovalForAllocate: function(data) {        
/*
        var res=$http.get('/api/as/StatusStock/StockReserved/ApprovalForAllocate', {params: { afi: data.afi,
            ati: data.ati, qty: data.qty, reqby: data.reqby, ProcessId: data.ProcessId, 
               Message: data.Message, UserIdForApproval: data.UserIdForApproval} });
        */

        var res=$http.post('/api/as/StatusStock/StockReserved/ApprovalForAllocate', data);

        // console.log('hasil getData =>',res);        
        return res;
      },
      getData: function(data) {
        //console.log("[Factory]");
        console.log("[Factory] = ", data);
        var res = $http.get('/api/as/StatusStock', {params: {
                                                          OutletId : (data.OutletId == null ? "n/a" : data.OutletId),
                                                          MaterialTypeId : (data.MaterialTypeId == null ? "n/a" : data.MaterialTypeId),
                                                          WarehouseId : (data.WarehouseId == null ? "n/a" : data.WarehouseId),
                                                          PartsCode : ((data.PartsCode == null || data.PartsCode == "") ? "n/a" : data.PartsCode),
                                                          PartsName : ((data.PartsName == null || data.PartsName == "") ? "n/a" : data.PartsName),
                                                          ServiceTypeId : data.ServiceTypeId
                                                        } });
        console.log('[Factory] OutletId =>', data.OutletId);
        console.log('[Factory] WarehouseId =>', data.WarehouseId);
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      getWarehouse: function(data) {
        console.log("[Factory] getWarehouse data = ", data);
        var res=$http.get('/api/as/StatusStock/GetWarehouse2', {params: {OrgId : data.OrgId}});
        console.log('getWarehouse =>',res);
        return res;
      },
      getServiceAndMaterial: function() {
        console.log("YOU DONT OWN ME");
        var res = $http.get('/api/as/PartsUser/GetServiceWarehouse');
        return res;
      },
    }
  });
