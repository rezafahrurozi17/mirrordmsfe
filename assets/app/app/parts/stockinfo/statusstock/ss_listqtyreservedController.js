angular.module('app')
    .controller('SS_ListQtyReservedController', function($scope, $http, CurrentUser, PartsStatusStock,$timeout, ngDialog, bsAlert, bsNotify, //parApproval
        PartsGlobal) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mRole = null; //Model 
    var objMaterial = PartsGlobal.getparobjMaterial();
    console.log("data : ", objMaterial);
    // $scope.no_material= '01234-00312';
    // $scope.nama_material= 'ASSASERR';
    $scope.PartId= objMaterial.PartId;
    $scope.no_material= objMaterial.PartsCode;
    $scope.nama_material= objMaterial.PartsName;
    $scope.reserved= objMaterial.QtyReserved;
    $scope.xRole={selected:[]};
    $scope.ApprovalProcessId= 8020;
    PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res){  // tested and worked                 
        // $scope.CancelNo = res.data.Result[0].DocNo;
        console.log("data Reserved =>");
        //$scope.Approver = res.data.Result[0].Approvers;
    });

    //----------------------------------
    // Get Data
    //----------------------------------
    // $scope.getDataReserved
    
    var gridData = [];
    $scope.onListSelectRowsAlokasi = function(rows){
        console.log("onSelectRows=> untuk AlokasiA", rows);       
    }
    $scope.getDataReserved = function() {
        // PartsStatusStock.getData()
        PartsStatusStock.getListQtyReserved({PartId : $scope.PartId})
        .then(
            function(res){ 
                gridData = [];
                //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0); 
                //-console.log("role=>", res.data.Result); //-
                $scope.grid.data = res.data.Result;
                console.log("data Reserved =>",res.data.Result);
                //console.log("grid data=>",$scope.grid.data);
                //$scope.roleData = res.data;
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }    
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    } //getListDestinationAllocateTo
    $scope.updateGrid= function(data, sourcedata){      
      var index = $scope.grid.data.indexOf(data);    
      // sourcedata[0]['RefAllocNo']= sourcedata[0]['RefMRNo'];
      // sourcedata[0]['RefAllocTypeId']= sourcedata[0]['RefMRTypeId'];
      // for(var k in sourcedata[0]) $scope.grid.data[index][k]=sourcedata[0][k]; 
      console.log("ini apa", sourcedata);           
      $scope.grid.data[index]['RefAllocNo']=sourcedata['RefMRNo'];
      $scope.grid.data[index]['RefAllocTypeId']=sourcedata['RefMRTypeId'];
      $scope.grid.data[index]['StockReservedStatusId']= ((sourcedata['RefMRTypeId'] == null || sourcedata['RefMRTypeId'] == undefined) ? 0 : sourcedata['RefMRTypeId'] );
      $scope.grid.data[index]['RefAllocId'] = sourcedata['MaterialRequestItemId'];
    }
    $scope.getAllocateTo = function(rows){
        PartsStatusStock.getListDestinationAllocateTo(rows)
        .then(
            function(res){ 
                gridData = [];                
                // $scope.grid.data = res.data.Result;
                gridData = res.data.Result;
                console.log("data Allocate To =>",res.data.Result);    
                $scope.gridLookUpAllocateTo.data = gridData;            
                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    } 
    $scope.showAlokasiError = false;
    $scope.checkAlokasi = true;
    $scope.SearchDestinationAllocate = function(data){
      PartsGlobal.formLookupTitle = "Daftar Material Request ";      
      PartsGlobal.formLookupSubTitle1 = "Daftar MR Untuk Part : (" + data.PartId +")";
      PartsGlobal.formLookupSubTitle2 = PartsGlobal.getCurrDate();
      PartsGlobal.formLookupGrid = $scope.gridLookUpAllocateTo; //"gridLookUpPO"; $scope.getDaftarPO()
      PartsGlobal.formLookupGrid.multiSelect = false;
      PartsGlobal.formLookupGrid.data = $scope.getAllocateTo(data);
      PartsGlobal.cancelledLookupGrid = true;
      ngDialog.openConfirm ({         
        template:'<div ng-include=\"\'app/parts/templates01/frm_lookup01.html\'\"></div>',
        plain: true,
        controller: 'GoodsReceiptController',
       }).then(function (value) {
                    var selDest = PartsGlobal.selectedLookupGrid;
                    $scope.selectedDestination = [];
                    // $scope.selectedDestination = PartsGlobal.selectedLookupGrid;
                    console.log("selDest : ", selDest);
                    console.log("Destination selected : ", $scope.selectedDestination);
                    //if (PartsGlobal.cancelledLookupGrid) return 1;   
                    console.log("data: ", data);

                    if(selDest[0].MaterialRequestStatusId >= 3) {
                      $scope.showAlokasiError = false;
                      $scope.checkAlokasi = false;
                      $scope.selectedDestination = PartsGlobal.selectedLookupGrid;
                      $scope.updateGrid(data, $scope.selectedDestination[0]);     
                    } else if(selDest[0].MaterialRequestStatusId = 2) {
                      $scope.noTransaksi = selDest[0].RefMRNo;
                      $scope.showAlokasiError = true;
                      $scope.checkAlokasi = true;
                      $scope.selectedDestination = PartsGlobal.selectedLookupGrid;
                      $scope.selectedDestination[0].RefMRNo = null;
                      $scope.selectedDestination[0].RefMRTypeId = null;
                      $scope.selectedDestination[0].MaterialRequestItemId = null;
                      $scope.updateGrid(data, $scope.selectedDestination[0]);     
                  }

                }
                // , function (value) {
                //   $scope.selectedDestination = PartsGlobal.selectedLookupGrid;
                //   console.log("No Data fetched ", $scope.selectedDestination); 
                //   if (PartsGlobal.cancelledLookupGrid) return 1;
                //   // $scope.mData.VendorId= $scope.selectedVendor[0].VendorCode;
                //   $scope.updateGrid(data, $scope.selectedDestination);
                // }
            );
    }
    $scope.alokasireserved = function(){
        //alert('Alokasi kan'); // call approval
        console.log("Data alokasi : ", $scope.grid.data);
        var str0 = 'Alokasi Qty Reserved';        
        var DataAlokasi = $scope.grid.data;  
        var vData = [];
        for(var i in DataAlokasi){             
          if(DataAlokasi[i]["RefAllocId"] !== undefined && DataAlokasi[i]["RefAllocId"] != null){
            vData.push({
              afi : DataAlokasi[i].MaterialRequestItemId,
              ati : DataAlokasi[i].RefAllocId,
              qty : DataAlokasi[i].Qty,
              // reqby : 1,
              // UserIdForApproval : -1,
              UserIdForApproval : DataAlokasi[i].MaterialRequestCreatorId,
              ProcessId : $scope.ApprovalProcessId,
              Message: ""
            });
          } 
        }     
              
        var i;
        var invalidQty = false;
        for (i=0; i< vData.length; i++) {         
          if (vData[i]['QtyReserved'] < vData[i]['Qty'] && vData[i]['QtyReserved'] > 0) { // cek validasi qtyGR
            invalidQty = true;
            break;
          } 
        }   
       if (invalidQty) { 
        PartsGlobal.showAlert({
                      title: "Alokasi Stock",
                      content: "Invalid data Quantity Alokasi", 
                      type: 'danger'
                    }); 
        return -1;
        }
       
       //  ngDialog.openConfirm ({
       //  template:'<div ng-include=\"\'app/parts/templates01/frmApproval01.html\'\"></div>',
       //  plain: true,
       //  controller: 'SS_ListQtyReservedController', //'MIPMaintenanceController',
       // });
        // PartsGlobal.showApproval({ // baris ini menggantikan kode di atas 
        PartsGlobal.setReqAppSentPG(false);
        PartsStatusStock.getApprovalStock(DataAlokasi[0].RefMRNo).then(function(res) {
            $scope.IniyangApprove = res.data.Result[0].Approvers;
            PartsGlobal.showApprovalNoDataId({
                subtitle: str0 +' ini',
                kegiatan: str0,
                noreq: DataAlokasi[0].RefMRNo , 
                approver: $scope.IniyangApprove ,
                pesan: 'Tolong approve segera',
                controller: 'PartsStatusStockController', //'SS_ListQtyReservedController'
                ProcessId: $scope.ApprovalProcessId,
                userid: 0,             
                dataId: -999,  // tidak dipakai (data id belum ada)
                dataPar: vData, 
                ProcessData: PartsStatusStock.AskApprovalForAllocate
                //   headerData: $scope.MRHeader,
                //   detailData: $scope.gridDetail.data,
                //   procUpdateData : MaterialRequest.updateData
            });
        });
        

        
        
    }

    $scope.hello2 = function(){
        alert('PO telah di-Cancel');
        //console.log("");
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    
    var dateFilter = 'date:"dd/MM/yyyy HH:mm:ss"';
    var cellTrxNo = '<div><div class="input-group"><input type="text" ng-model="row.entity.RefAllocNo" ng-value="row.entity.RefAllocNo">\
                      <label class="input-group-btn"><span class="btn wbtn" ng-click="grid.appScope.SearchDestinationAllocate(row.entity)">\
                      <i class="fa fa-search fa-1"></i></span>\
                      </label></div></div>';
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        enableHorizontalScrollbar: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'MaterialRequestItemId', width:'7%', visible:false },
            { name:'Referensi Transaksi', field:'RefMRTypeId', enableCellEdit: false, headerCellClass: "middle",
              editableCellTemplate: 'ui-grid/dropdownEditor', cellFilter: 'mapTrx',  
              editDropdownValueLabel: 'trx',              
              editDropdownOptionsArray: [{ id: 1, trx: 'Appointment' }, { id: 2, trx: 'WO' },
              { id: 3, trx: 'Sales Order' }] }, 
            { name:'No Referensi ',  field: 'RefMRNo', enableCellEdit: false, minWidth: 170, width: "15%", headerCellClass: "middle" }, 
            { name:'VehicleId',  field: 'VehicleId', enableCellEdit: false, visible:false }, 
            { name:'No Polisi',  field: 'LicensePlate', enableCellEdit: false, headerCellClass: "middle", width: "8%" }, 
            { name:'Tgl Dibutuhkan',  field: 'RequiredDate', enableCellEdit: false, headerCellClass: "middle", cellFilter: dateFilter }, 
            { name:'Petugas', field: 'RequestBy', enableCellEdit: false, headerCellClass: "middle" }, 
            { name:'No PO',  field: 'idpo', displayName: 'No. PO', visible:false }, 
            { name:'No GR',  field: 'idgr', displayName: 'No. GR', visible:false }, 
            { name:'PartId', field:'PartId', visible:false},
            { name:'Qty Reserved',  field: 'QtyReserved', enableCellEdit: false, headerCellClass: "middle", width: "8%" }, 
            { name:'Transaksi',  field: 'RefAllocTypeId', enableCellEdit: false, //headerCellTemplate: '<div class="">Alokasi Ke <br> Transaksi</div>', 
              editableCellTemplate: 'ui-grid/dropdownEditor', cellFilter: 'mapTrx',  width: "10%",
              headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:352%'><table class='ui-grid-header-table'><tbody><tr><td colspan='3'>Alokasi Ke</td></tr></tbody></table></div>Transaksi</div>",
              editDropdownValueLabel: 'trx',              
              editDropdownOptionsArray: [{ id: 1, trx: 'Appointment' }, { id: 2, trx: 'WO' },
              { id: 3, trx: 'Sales Order' }] }, 
            { name:'id trx',  field: 'RefAllocId', visible: false, enableCellEdit: false, 
              headerCellTemplate: "<div class='ui-grid-spilt-header-main'>id trx</div>"}, 
            { name:'No Transaksi',  field: 'RefAllocNo', enableCellEdit: false, cellTemplate: cellTrxNo, minWidth: 170, width: "*",
              headerCellTemplate: "<div class='ui-grid-spilt-header-main'>No Transaksi</div>", width: "17%"}, 
            { name:'Qty Alokasi',  field: 'Qty', enableCellEdit: true,
              headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Qty Alokasi</div>", width: "8%"}
              /*
            { name:'Status',  field: 'StockReservedStatusDescription', enableCellEdit: false, headerCellClass: "middle", 
              editableCellTemplate: 'ui-grid/dropdownEditor', //cellFilter: 'mapStatusReserved',  
              editDropdownValueLabel: 'trx',              
              editDropdownOptionsArray: [{ id: 0, trx: 'Request Approval' }, { id: 1, trx: 'Completed' }]
            },
            */
        ] 
        ,
         data : $scope.getDataReserved({MaterialRequestId : -1}),
         onRegisterApi: function(gridApi) {
            // set gridApi on $scope
            $scope.gridApiAlokasi = gridApi;
            $scope.gridApiAlokasi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selectedRows = $scope.gridApiAlokasi.selection.getSelectedRows();
                // console.log("selected=>",$scope.selectedRows);
                if ($scope.onListSelectRowsAlokasi) {
                    $scope.onListSelectRowsAlokasi($scope.selectedRows);
                }
            });
        }
    };
    $scope.gridLookUpAllocateTo = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        onRegisterApi: function (gridApi) {
          $scope.gridApi = gridApi;
          gridApi.selection.on.rowSelectionChanged($scope,function(rows){
              $scope.mySelections = gridApi.selection.getSelectedRows();
              console.log("Item(s) : ", $scope.mySelections); // ...              
              PartsGlobal.selectedLookupGrid = $scope.mySelections;              
          });
        },
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'MaterialRequestItemId', width:'7%', visible:false },
            { name:'Referensi Transaksi', field:'RefMRTypeId', enableCellEdit: false }, 
            { name:'No Referensi ',  field: 'RefMRNo', enableCellEdit: false },             
            { name:'Transaksi',  field: 'RefAllocTypeId', enableCellEdit: true, 
              editableCellTemplate: 'ui-grid/dropdownEditor', cellFilter: 'mapTrx',  
              editDropdownValueLabel: 'trx',              
              editDropdownOptionsArray: [{ id: 1, trx: 'Appointment' }, { id: 2, trx: 'WO' },
              { id: 3, trx: 'Sales Order' }] }, 
            { name:'id trx',  field: 'MaterialRequestId', visible: true, enableCellEdit: false}, 
            { name:'No Transaksi',  field: 'RefAllocNo', enableCellEdit: true}            
        ] 
        ,data : []
    };
})
.filter('mapTrx', function() {
    var trxHash = {
        1: 'Appointment',
        2: 'WO',
        3: 'Sales Order' 
    };
 
  return function(input) {
    if (!input){
      return '';
    } else {
      return trxHash[input];
    }
  };
})
;