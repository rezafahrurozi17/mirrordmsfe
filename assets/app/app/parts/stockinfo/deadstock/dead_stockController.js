angular.module('app')
    .controller('PartsDeadStockController', function($scope, $http, CurrentUser, PartsDeadStock, $timeout, bsTab, PartsGlobal, bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading = false;
        //$scope.gridData=[];
        $scope.GetProvinceId(null);  //--28-08-2017
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();    
    //$scope.mDeadStock = null; //Model
    $scope.mData = {};
    $scope.xDeadStock = {};
    $scope.IsCheckStockTAM = false;
    $scope.gridHideActionColumn=true; 
    $scope.xDeadStock.selected=[];

    $scope.btnDis = true;
    // *
    $scope.newTab = function(strmenu){
        console.log("newTab=>");
        // bsTab.add("app.purchaseorderparts");
        bsTab.add(strmenu);
    }    
    $scope.GetProvinceId = function(key)
    { //getProvince
      // PartsDeadStock.SearchProvince().then(function(res){
      PartsGlobal.getProvince().then(function(res){
        console.log("List Provinsi ====>",res);
        $scope.Provincedata = res.data.Result;
      });
    }
    $scope.selectProvince = function(key)
    {
      console.log("Isi Provinsi = ", key);
      $scope.mData.ProvinceId = key.ProvinceId;
    }
    // */
    var linkSaveCb = function(mode, model) {
        console.log("mode", mode);
        console.log("mode", model);
    };
    var linkBackCb = function(mode, model) {
        console.log("mode", mode);
        console.log("mode", model);
    };
    $scope.btnBuatTO = function(){
      // alert ("Test TO");
      PartsGlobal.showAlert({
        title:'Buat TO',
        content:'Ke Menu Trx TO',
        type:'info'
      });
      // $scope.newTab("app.parts_transferorder"); 
      var temp = {"linksref":"app.parts_transferorder", "linkview":"parts_transferorder@app"};
      $scope.formApi.showLinkView(linkSaveCb, linkBackCb, 'new', {}, 'true', 'true', temp, {"PartsCode":$scope.mData.PartsCode, "fromOtherModule":true});
    };
    $scope.btnBuatPO = function(){
      // alert ("Test Purchase Order");
      PartsGlobal.showAlert({
        title:'Buat PO',
        content:'Ke Menu Trx Purchase Order',
        type:'info' 
      });
      // $scope.newTab("app.purchaseorderparts"); 
      var temp = {"linksref":"app.purchaseorderparts", "linkview":"purchaseorderparts@app"};  
      $scope.formApi.showLinkView(linkSaveCb, linkBackCb, 'new', {}, 'true', 'true', temp, {"PartsCode":$scope.mData.PartsCode,"NoDeadStock":$scope.Apaisinya[0].DeadStockNo, "VendorIdnya":$scope.Apaisinya[0].VendorId,"PaymentMethodId":$scope.Apaisinya[0].PaymentMethodId, "fromOtherModule":true, "moduleName":"ds"});
    };
    $scope.hapusFilter= function() {
      // $scope.mDataFilter.RefMRTypeId='';
      $scope.mData.PartsCode= null;
      $scope.mData.PartsName=null;
      $scope.mData.ProvinceId=null;
      $scope.mData.OutletDealerCode=null;
      $scope.mData.DiscFrom = null;
      $scope.mData.DiscTo = null;
      $scope.mData.BrandName= null;
      // $scope.mData.LicensePlate=null;
      // alert("Hapus filter");

      $scope.btnDis = true;
    }
    $scope.InfoCabang = function(data){
      // alert ('Detail info : '+ data.cabang);
      PartsGlobal.showAlert({
        size:'big',
        title:'Info Cabang',
        content: data.Cabang +'<br />' +data.Address+'<br /> Telp.' +data.PhoneNumber+'<br /> Fax.' + data.FaxNumber, 
        type:'info'
      });
        //return 1;
    };
    $scope.toPartMasterInquiry = function(){
      // alert ('Jump to Part Master Inquiry');
      PartsGlobal.showAlert({
        title:'Cek Stok TAM',
        content:'Jump to Part Master Inquiry',
        type:'info'
      });        
      $scope.newTab("app.parts_inquiry"); 
    };
    //---------------------------------- 
    // Get Data
    //----------------------------------
    //var gridData = [];

    $scope.getData = function(mData) {
      console.log("mData = ", $scope.mData);
      if(($scope.mData.PartsCode == null || $scope.mData.PartsCode == "") && $scope.mData.ProvinceId == null && $scope.mData.OutletDealerCode == null
        && ($scope.mData.BrandName == null || $scope.mData.BrandName =="") && ($scope.mData.PartsName == null || $scope.mData.PartsName =="") )
        {
          PartsGlobal.showAlert(
              {
                title: "Deadstock Info",
                content: "Filter tidak boleh kosong",
                type: 'danger'
              } 
              );
          return 0;
        } else if (($scope.mData.PartsCode == null || $scope.mData.PartsCode == "") || $scope.mData.ProvinceId == null ) {
            PartsGlobal.showAlert(
              {
                title: "Deadstock Info",
                content: "Filter Mandatori (No. Material dan Provinsi) tidak boleh kosong",
                type: 'danger'
              }
              );
            return 0;
        }
        mData.Outletsekarang = $scope.user.OrgId;
      PartsDeadStock.getData(mData).then(function(res) {         
          var gridData = res.data.Result;
          console.log("GridData .= ", gridData); 
          $scope.grid.data = gridData;
          $scope.loading = false;
          if (gridData == "" || gridData == undefined || gridData == null || gridData == []){
            bsNotify.show(
                {
                    title: "Info",
                    content: "Data tidak ditemukan",
                    type: 'warning'
                }
            );
          }
        }, 
        function(err) {
          console.log("err=>", err);
        }
      ); 
    }
    // $scope.getData = function() {
    //     $scope.data = [
    //         {
    //           "noDS": "WS20160712002",
    //           "startdate": "17-02-2016",
    //           "enddate": "27-02-2016",
    //           "nomaterial": "FFGkjsiu",
    //           "materialname": "GFhgshdg",
    //           "provinsi": 'DKI Jakarta',
    //           "dealer": "Jaktim",
    //           "company": "",
    //           "cabang": "fgh",
    //           "status": "Available",
    //           "satuan": "Pieces",
    //           "diskon": "10"
    //         },
    //         {
    //           "noDS": "WO/20160712052",
    //           "startdate": "10-02-2016",
    //           "enddate": "17-02-2016",
    //           "nomaterial": "FRewytyw",
    //           "materialname": "shdgsjfh",
    //           "provinsi": 'Banten',
    //           "dealer": "Cilegon",
    //           "company": "",
    //           "cabang": "cde",
    //           "status": "Not Available",
    //           "satuan": "kg"
    //         },
    //         {
    //           "noDS": "WO/20160712002",
    //           "startdate": "11-02-2016",
    //           "enddate": "17-02-2016",
    //           "nomaterial": "AG23563fg6",
    //           "materialname": "Test",
    //           "provinsi": 'Jawa Barat',
    //           "dealer": "Bandung timur",
    //           "company": "",
    //           "cabang": "Bandung",
    //           "status": "Available",
    //           "satuan": "Pieces"
    //         }
    //     ];
    //     $scope.grid.data = $scope.data; 

    //     $scope.dataDetail = [
    //         {
    //           "JobName": "Partsman",
    //           "active": true
    //         }
    //     ];
    //     $scope.gridDetail.data = $scope.dataDetail;         

    //     $scope.loading = false;
    // }

    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
        $scope.Apaisinya=[];
        $scope.Apaisinya = rows;
        if (rows.length>0) {
          $scope.btnDis = false;
        } else {
          $scope.btnDis = true;
        }        
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    var //cellTemp = '<i class="fa fa-map-marker" aria-hidden="true"><a href="#" ng-click="grid.appScope.$parent.InfoCabang(row.entity)">{{row.entity.cabang}}</a></i>';
        cellTemp = '<div>{{row.entity.Cabang}}  <a href="#" ng-click="grid.appScope.$parent.InfoCabang(row.entity)"><i class="fa fa-map-marker" aria-hidden="true"></i></a></div>';
    
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200], 
        // paginationPageSize: 15,

        columnDefs: [
          
            { name:'Nomor DS', field:'DeadStockNo', displayName: 'Nomor DS', minWidth: 150, width: "*"},
            { name:'Berlaku Dari', field:'ValidFrom', cellFilter: 'date:\'yyyy-MM-dd\'',minWidth: 120, width: "*", headerCellTemplate: '<div class="">Berlaku <br> Dari</div>'},
            { name:'Berlaku Sampai', field:'ValidTo', cellFilter: 'date:\'yyyy-MM-dd\'', minWidth: 120, width: "*", headerCellTemplate: '<div class="">Berlaku <br> Sampai</div>'},
            { name:'No. Material',  field: 'PartsCode', minWidth: 100, width: "*" },
            { name:'Nama Material',  field: 'PartsName', minWidth: 150, width: "*" },
            { name:'Provinsi',  field: 'ProvinceName', minWidth: 150, width: "*"},
            { name:'Dealer',  field: 'GroupDealerName', minWidth: 150, width: "*" }, 
            { name:'Perusahaan',  field: 'CompanyName', minWidth: 150, width: "*" },
            { name:'Cabang',  field: 'Cabang', minWidth: 150, width: "*",  // },
              cellTemplate: cellTemp},
            { name:'Address',  field: 'Address', minWidth: 150, width: "*", visible:false },
            { name:'PhoneNumber ',  field: 'PhoneNumber', minWidth: 150, width: "*", visible:false },
            { name:'FaxNumber',  field: 'FaxNumber', minWidth: 150, width: "*", visible:false },
              //cellTemplate: '<i class="fa fa-{{COL_FIELD}}"></i>{{COL_FIELD}}' },             
            { name:'Status',  field: 'Status', minWidth: 150, width: "*", //background-color: 'red'} ,
             cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                //if (grid.getCellValue(row ,col).toLowerCase() == 'not available') {
                  // if (grid.getCellValue(row ,col) == 1) {
                  if (grid.getCellValue(row ,col) == 'Tidak Tersedia') {
                    grid.appScope.$parent.IsCheckStockTAM = true; 
                    return 'red';
                  } /*else { 
                     grid.appScope.$parent.IsCheckStockTAM= false;
                     //if (this.IsCheckStockTAM) alert ("ok");
                     return 'red';
                  }*/
                } 
                //, cellTemplate:'<div>{{row.entity.status="not available"}}</div>'
            },
            { name:'Satuan',  field: 'Satuan', minWidth: 120, width: "*" },
            { name:'Diskon (%)',  field: 'Discount', minWidth: 100, width: "*"}, /* 
            { name:'Satuan',  field: 'satuan'}, /*
             cellTemplate:'<div>' +                      
                       //'<a href="http://stackoverflow.com">{{row.entity.satuan}}</a>' +
                       '<a href="../statusstock/ss_listqtyreserved.html">..temp..</a>'+
                       '</div>' } */
        ]
    };

    //grid detail
    /*
    $scope.gridDetail = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //enableFiltering : true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [             
                { name:'No. Material', field:'noMaterial' },
                { name:'Nama Material', field:'namaMaterial' },
                { name:'Qty P.O.', field:'' },
                { name:'', field:'' },
                
            ]
      // 
            //  $scope.gridOptions.columnDefs = [
            //     { name: 'id', enableCellEdit: false },
            //     { name: 'name', displayName: 'Name (editable)' },
            //     { name: 'gender' },
            //     { name: 'age', displayName: 'Age' , type: 'number'},
            //     { name: 'registered', displayName: 'Registered' , type: 'date', cellFilter: 'date:"yyyy-MM-dd"'},
            //     { name: 'address', displayName: 'Address', type: 'object', cellFilter: 'address'},
            //     { name: 'address.city', displayName: 'Address (even rows editable)',
            //          cellEditableCondition: function($scope){
            //          return $scope.rowRenderIndex%2
            //          }
            //     },
            //     { name: 'isActive', displayName: 'Active', type: 'boolean'}
            // ];
    };
    */

});
