angular.module('app')
  .controller('PartsDeadStockMaintainController', function($scope, $http, $filter, WoHistoryGR, CurrentUser, PartsCurrentUser, PartsGlobal, PartsDeadStockMaintain, ngDialog, bsNotify, $timeout, $window) {
    //Options Switch
    $scope.items = ['Suggestion By System', 'Manual'];
    $scope.selection = $scope.items[0];
    $scope.actionVisible = false;
    $scope.mySelections = [];
    $scope.isOverlayForm = false;
    $scope.ApprovalProcessId = 8140;
    $scope.ApprovalProcessId_Add = 8141;
    $scope.gridHideActionColumn = false;
    $scope.isShowEditButton = true;

    //----------------------------------
    // Start-Up
    //----------------------------------
    // added by sss on 2018-03-14
    var loadedContent = false;
    //
    $scope.$on('$viewContentLoaded', function() {
      $scope.loading = false;
      $timeout(function(){
        // added by sss on 2018-03-14
        
        loadedContent = true;
        //
        // data need approval
        $scope.getDataNA();
        $scope.GetStatusDeadStock();
        $scope.getGridFirstLoad();
        $scope.getEmployeeId();
        // $scope.getStatusDS();
        var parts = $scope.checkRights(9);
        console.log("parts = ", parts);
        console.log('user=', $scope.user);
        var OutletId = $scope.user.OrgId;
        $scope.mData.OutletId = $scope.user.OrgId;


        if($scope.user.RoleId != 1128) {
          $scope.getDataDraft();
          console.log("ini",$scope.user.RoleId);
        }else{
          $scope.mData.vDeadStockStatusId = 2;
        }
        // var vICC = 2;
        // PartsDeadStockMaintain.getICC(vICC).then(function(res){
        //     var data = res.data.Result;
        //     console.log("data ICC = ", data);
        //     $scope.mData.DataICC = data;
        //     //console.log('$scope.mData.DataICC => ', $scope.mData.DataICC);
        //   },
        //   function(err) {
        //       console.log("err=>", err);
        //   }
        // );

        // var vSCC = 3;
        // PartsDeadStockMaintain.getSCC(vSCC).then(function(res){
        //     var data = res.data.Result;
        //     console.log("data SCC = ", data);
        //     $scope.mData.DataSCC = data;
        //     //console.log('$scope.mData.DataSCC => ', $scope.mData.DataSCC);
        //   },
        //   function(err) {
        //       console.log("err=>", err);
        //   }
        // );

        //$scope.obj.test = 1;

      });

    });


    $scope.EmployeeId = null;
    $scope.getEmployeeId = function() {
        WoHistoryGR.getEmployeeId($scope.user.UserName).then(function(resu) {
            console.log('resu getemployeeid', resu.data);
            $scope.EmployeeId = resu.data[0].UserId;
        });
    };

    $scope.getGridFirstLoad = function () {
      $scope.mData.vValidFrom = new Date("2018-01-01");
      $scope.mData.vValidTo = new Date();
      $scope.mData.vDeadStockStatusId = 2;
      $scope.mData.OutletId = $scope.user.OrgId;
      console.log("$scope.mData1=>", $scope.mData);
      console.log("ValidFrom",$scope.mData.vValidFrom);
      console.log("ValidTo",$scope.mData.vValidTo);
      if ($scope.user.RoleId == 1135) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        console.log('1135', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      } else if ($scope.user.RoleId == 1122) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        console.log('1122', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      } else if ($scope.user.RoleId == 1123) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 0;
        console.log('1123', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      } else if ($scope.user.RoleId == 1125) {
        $scope.mData.MaterialTypeId = 2;
        $scope.mData.ServiceTypeId = 1;
        console.log('1125', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      } else if ($scope.user.RoleId == 1124) {
        $scope.mData.MaterialTypeId = 2;
        $scope.mData.ServiceTypeId = 0;
        console.log('1124', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      } else if ($scope.user.RoleId == 1128) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        console.log('1124', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      }
      
      PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.mData.OutletId).then(function(res) {
        var gridData = res.data;
        console.log('gridData[0] = ', gridData[0]);

        $scope.mData.WarehouseId = gridData[0].WarehouseId;
        $scope.mData.OutletId = gridData[0].OutletId;
        $scope.mData.WarehouseData = gridData;

        PartsDeadStockMaintain.getData($scope.mData).then(function(res) { // start getdata
          var gridData = res.data.Result;
          //var gridData = res.data;
          console.log("gridData123",gridData);
          $scope.grid.data = gridData;
          $scope.loading = false;
          $scope.mData.vValidFrom = undefined;
          $scope.mData.vValidTo = undefined;
          $scope.mData.vDeadStockStatusId = "";
        },
        function(err) {
          console.log("err=>", err);
        }
      ); // end getdata

      },
      function(err) {
        console.log("err=>", err);
      }
      );
      console.log("$scope.mData.WarehouseId", $scope.mData.WarehouseId);
      console.log("err=>", $scope.mData);
        
    }

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    //console.log("user=>",$scope.user);
    //console.log("mode =>");
    $scope.mData = {}; //Model
    $scope.mData.GridDetail = {};
    $scope.xRole = {
      selected: []
    };

    $scope.formApi = {};
    $scope.checkRights = function(bit){
        var p=$scope.myRights & Math.pow(2,bit);
        var res= (p==Math.pow(2,bit));
        console.log("myRights => ", $scope.myRights);
        return res;
    }
    $scope.getRightX= function(a, b){
      var i = 0;
      if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
      else if ($scope.checkRights(b)) i= 2;
      else if ($scope.checkRights(a)) i= 1;
      return i;
    }
    $scope.getRightMaterialType= function(){
      var i = $scope.getRightX(8, 9);
      return i;
    }
    $scope.getRightServiceType= function(){
      var i = $scope.getRightX(10, 11);
      return i;
    }    
   

    $scope.GetStatusDeadStock = function () {
      PartsDeadStockMaintain.getStatusDeadStock().then(function(res) {
        console.log("Res Status DeadStock ====>", res.data.Result);
        $scope.ValDeadStockStatusId = res.data.Result;
        console.log("ValDeadStockStatusId", $scope.ValDeadStockStatusId);
    });
    }

    $scope.onBulkApproveDSM = function(mData){
      console.log("mData : ", mData);
      //alert("Approve donk");
      $scope.ApproveDSM(mData);
      // PartsGlobal.actApproval({
      //   ProcessId: $scope.ApprovalProcessId, DataId: mData[0].DeadStockId, ApprovalStatus: 1
      // })
    }
    // approve
    $scope.ApproveDSM = function(data){
      console.log('ApproveDSM data = ', data);
      $scope.ApproveData = data[0];
      console.log('$scope.ApproveData = ', $scope.ApproveData);
      PartsDeadStockMaintain.setDeadStockHeader($scope.ApproveData);
      ngDialog.openConfirm ({
        template:'\
                    <div align="center" class="ngdialog-buttons">\
                    <p><b>Konfirmasi</b></p>\
                    <p>Approve dokumen Dead Stock Maintain ini?</p>\
                    <div class="ngdialog-buttons" align="center">\
                      <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                      <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinApprove(ApproveData)">Ya</button>\
                    </div>\
                    </div>',
        plain: true,
        scope:$scope
        // controller: 'PartsDeadStockMaintainController',
      });
      console.log('$scope.ApproveData bawah = ', $scope.ApproveData);

    };

    $scope.yakinApprove = function(data){
      console.log('yakinApprove => ', data);
      console.log("EmployeeId", $scope.EmployeeId)
      if(Array.isArray(data)){
        _.map(data, function(val) {
          val.userId = $scope.EmployeeId;
        })
      }else{
          data.userId = $scope.EmployeeId;
      }
      // _.map(data, function(val) {
      //    val.userId = $scope.EmployeeId;
      // })
      $scope.mData = data;
      $scope.ngDialog.close();
      $scope.mData.DeadStockStatusId = 3; // Published
      console.log('yakinApprove mData = ', $scope.mData);
      // for approval
      PartsGlobal.actApproval({
        ProcessId: $scope.ApprovalProcessId, DataId: $scope.mData.DeadStockId, ApprovalStatus: 1
      })
            console.log('res.data = ', data);
            PartsDeadStockMaintain.sendNotifToPartsman(data).then(function(res){
                console.log('notif send to Partsman')
            });

            // bsNotify.show(
            //   {
            //       title: "Dead Stock Maintain",
            //       content: "Data berhasil disimpan",
            //       type: 'success'
            //   }
            // );
      // [START] PUT
      PartsDeadStockMaintain.cancel($scope.mData).then(function(res) {
          console.log('res cancel = ', res);
          bsNotify.show(
              {
                  title: "Approval",
                  content: "Dead Stock Maintain has been successfully approved.",
                  type: 'success'
              }
            );
        },
        function(err) {
          console.log("err=>", err);
        }
      );
      //PartsTransferOrder.formApi.setMode("grid");
    }

    $scope.onBulkRejectDSM = function(mData) {
      PartsGlobal.actApproval({
        ProcessId: $scope.ApprovalProcessId, DataId: mData[0].DeadStockId, ApprovalStatus: 2
      })
    }
    $scope.getApprover = function(){
      console.log('ApprovalProcessId_Add', $scope.ApprovalProcessId_Add);
      PartsDeadStockMaintain.getApproverDeadStock($scope.ApprovalProcessId_Add).then(function(res){
          var approvers = res.data.Result[0].Approvers;
          $scope.mData.Approvers = approvers;
          console.log('$scope.mData.Approvers =>', $scope.mData.Approvers);
        },
        function(err) {
            console.log("err=>", err);
        }
      );
    } // end of getApprover
    $scope.getStatusDS = function(){
      PartsDeadStockMaintain.getStatus().then(function(res){
          // var data = res.data;
          var data = res.data.Result;
          console.log("data = ", data);
          $scope.mData.Status = data;
          //console.log('$scope.mData.Status => ', $scope.mData.Status);
        },
        function(err) {
            console.log("err=>", err);
        }
      );
    } // end of getStatusDS

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.gridDataTree=[];
    $scope.getData = function() {
      console.log("mData = ", $scope.mData);
      
      // $scope.gridHideActionColumn = false;
      //$scope.gridHideActionColumn=!$scope.gridHideActionColumn;
      // console.log("$scope.gridHideActionColumn = ", $scope.gridHideActionColumn);
      //$scope.grid.columnDefs = $scope.columnsDeadStock;

      // console.log("mData vDeadStockStatusId = ", $scope.mData.vDeadStockStatusId);

      if(($scope.mData.vDeadStockTypeId === undefined &&
        $scope.mData.vDeadStockStatusId === undefined &&
        $scope.mData.vValidFrom === undefined &&
        $scope.mData.vValidTo === undefined &&
        $scope.mData.vDeadStockNo === undefined)
        ||
        ($scope.mData.vDeadStockTypeId == null &&
        $scope.mData.vDeadStockStatusId == null &&
        $scope.mData.vValidFrom == null &&
        $scope.mData.vValidTo == null &&
        $scope.mData.vDeadStockNo == null)
        ||
        (($scope.mData.vDeadStockTypeId === undefined || $scope.mData.vDeadStockTypeId == null) &&
        ($scope.mData.vDeadStockStatusId === undefined || $scope.mData.vDeadStockStatusId == null) &&
        ($scope.mData.vValidFrom === undefined || $scope.mData.vValidFrom == null) &&
        ($scope.mData.vValidTo === undefined || $scope.mData.vValidTo == null) &&
        $scope.mData.vDeadStockNo == "")
      ){
        if (loadedContent) {
          bsNotify.show(
            {
                title: "Mandatory",
                content: "Isi salah satu filter terlebih dahulu",
                type: 'danger'
            }
          );
        }
      } else

      if((($scope.mData.vDeadStockTypeId === undefined &&
          $scope.mData.vDeadStockStatusId === undefined)
          ||
          ($scope.mData.vDeadStockTypeId == null &&
          $scope.mData.vDeadStockStatusId == null))
          &&
          ($scope.mData.vDeadStockNo !== undefined && $scope.mData.vDeadStockNo != null)
          &&
          ($scope.mData.vValidFrom !== undefined && $scope.mData.vValidFrom != null)
          &&
          ($scope.mData.vValidTo !== undefined && $scope.mData.vValidTo != null)
        )
      {
        console.log('filter non mandatory');
        PartsDeadStockMaintain.getData($scope.mData).then(function(res) { //start getdata
          console.log('Masuk sini ngga?');
            var tempdata =[];
            for (var i in res.data.Result){
                if (res.data.Result[i].StatusCode != 0 & res.data.Result[i].Id !=0){
                    tempdata.push(res.data.Result[i]);
                }

            }
            $scope.grid.data = tempdata;
          var gridData = res.data.Result;
            //var gridData = res.data;
            console.log("gridData",gridData);
            $scope.grid.data = gridData;
            $scope.loading = false;
          },
          function(err) {
            console.log("err=>", err);
          }
        ); // end getdata
      } else {
        console.log('filter is mandatory');
        if($scope.mData.vValidFrom == null || $scope.mData.vValidFrom == 'undefined' || $scope.mData.vValidFrom == ""){
          bsNotify.show(
              {
                  title: "Mandatory",
                  content: "Tanggal Pengajuan DS belum diisi",
                  type: 'danger'
              }
            );
        } else
        if($scope.mData.vValidTo == null || $scope.mData.vValidTo == 'undefined' || $scope.mData.vValidTo == ""){
          bsNotify.show(
              {
                  title: "Mandatory",
                  content: "Tanggal Pengajuan DS (end date) belum diisi",
                  type: 'danger'
              }
            );
        } else {
            PartsDeadStockMaintain.getData($scope.mData).then(function(res) { // start getdata
              var gridData = res.data.Result;
              //var gridData = res.data;
              console.log("gridData123",gridData);
              $scope.grid.data = gridData;
              $scope.loading = false;
            },
            function(err) {
              console.log("err=>", err);
            }
          ); // end getdata
        }
      } // end else atas

      //$scope.loading = false;
    } // end getdata all



    $scope.getDataDraft = function() {
      $scope.mData.DeadStockStatusId = 1;      
      if ($scope.user.RoleId == 1135) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        console.log('1135', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      } else if ($scope.user.RoleId == 1122) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        console.log('1122', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      } else if ($scope.user.RoleId == 1123) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 0;
        console.log('1123', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      } else if ($scope.user.RoleId == 1125) {
        $scope.mData.MaterialTypeId = 2;
        $scope.mData.ServiceTypeId = 1;
        console.log('1125', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      } else if ($scope.user.RoleId == 1124) {
        $scope.mData.MaterialTypeId = 2;
        $scope.mData.ServiceTypeId = 0;
        console.log('1124', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      } else if ($scope.user.RoleId == 1128) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        console.log('1124', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      }
      PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.mData.OutletId).then(function(res) {
        var gridData = res.data;
        console.log('gridData[0] = ', gridData[0]);

        $scope.mData.WarehouseId = gridData[0].WarehouseId;
        $scope.mData.OutletId = gridData[0].OutletId;
        $scope.mData.WarehouseData = gridData;
        PartsDeadStockMaintain.getDataDraft($scope.mData).then(function(res) { //start getdata
          var tempdata =[];
          for (var i in res.data.Result){
              if (res.data.Result[i].StatusCode != 0 & res.data.Result[i].Id !=0){
                  tempdata.push(res.data.Result[i]);
              }

          }
          $scope.grid.data = tempdata;
        var gridData = res.data.Result;
        console.log("Punya gua", $scope.mData.WarehouseId);
          //var gridData = res.data;
          console.log("gridData",gridData);
          $scope.grid.data = gridData;
          $scope.loading = false;
        },
        function(err) {
          console.log("err=>", err);
        }
      );

      },
      function(err) {
        console.log("err=>", err);
      }
      );
      
    
   
    }

    // Hapus Filter
    $scope.onDeleteFilter = function(){
      //Pengkondisian supaya seolaholah kosong
      $scope.mData.vDeadStockTypeId = null;
      $scope.mData.vDeadStockStatusId = null;
      $scope.mData.vValidFrom = null;
      $scope.mData.vValidTo = null;
      $scope.mData.vDeadStockNo = null;
    }
    // Hapus/refresh Search
    $scope.hapusSearch = function(){
      //Pengkondisian supaya seolah-olah kosong
      $scope.mData.PartsClassId3 = "";
      $scope.mData.PartsClassId2 = "";
      $scope.mData.PartsCode = null;
      $scope.mData.PartsName = null;
    }

    $scope.onSelectRows = function(rows) {
      console.log("onSelectRows=>", rows);
      $scope.testmodel = rows;
      console.log("Isi test component =>", $scope.testmodel.VehicleName);
    }

    //----------------------------------
    // [Start] Button Handler
    //----------------------------------
    //$scope.SearchParts = function (PartsClassId2, PartsClassId3, PartsCode, PartsName) {
    $scope.SearchParts = function (mData) {
      //console.log('$scope.user.OrgId ==>', $scope.user.OrgId);      
      //console.log('PartsClassId2 ==>', PartsClassId2);
      if ($scope.user.RoleId == 1135) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        console.log('1135', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      } else if ($scope.user.RoleId == 1122) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        console.log('1122', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      } else if ($scope.user.RoleId == 1123) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 0;
        console.log('1123', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      } else if ($scope.user.RoleId == 1125) {
        $scope.mData.MaterialTypeId = 2;
        $scope.mData.ServiceTypeId = 1;
        console.log('1125', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      } else if ($scope.user.RoleId == 1124) {
        $scope.mData.MaterialTypeId = 2;
        $scope.mData.ServiceTypeId = 0;
        console.log('1124', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      } else if ($scope.user.RoleId == 1128) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        console.log('1124', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
      }
      PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.user.OrgId).then(function(res) {
        var gridData = res.data;
        console.log('mDataaa = ', gridData);
        var OutletId = $scope.user.OrgId;
        $scope.mData.OutletId = OutletId;
        $scope.mData.WarehouseData = gridData;
        PartsDeadStockMaintain.searchParts(mData).then(function (res) {
          console.log('mData = cek ', mData);
          var DataParts = res.data.Result;
          console.log('DataParts ==>', DataParts);
          $scope.gridTambah.data = DataParts;
          $scope.mData.GridDetail = DataParts;
        },
        function (err) {
          console.log("err=>", err);
        }
      );

      },
      function(err) {
        console.log("err=>", err);
      }
      );
      // [START] Get Search Appointment Grid Detail     
      
      // [END] Get Search Appointment Grid
    }

    $scope.ApproveData = PartsDeadStockMaintain.getDeadStockHeader();
    console.log ("coba",$scope.ApproveData);
    $scope.ApproveDataDetail = PartsDeadStockMaintain.getDeadStockDetail();

    $scope.BatalData = PartsDeadStockMaintain.getDeadStockHeader();

    // ======== tombol  simpan =========
    $scope.simpan = function(){
      //$scope.mData.GridDetail = $scope.gridTambah.data;
      //console.log("gridApi =>", $scope.gridApi);
      //console.log("$scope.gridTambahSelections =>", $scope.gridTambahSelections);
      //$scope.mData.GridDetail = $scope.gridApi.selection.getSelectedRows(); // ini tidak berfungsi lagi
      $scope.mData.GridDetail = $scope.gridTambahSelections;
      var datetimeNow = new Date();
      $scope.mData.FilingDate = $filter('date')(datetimeNow, 'yyyy-MM-dd HH:mm:ss');
      $scope.mData.DeadStockTypeId = 2; // TIpe = Manual
      //console.log("mData => ", $scope.mData);
      //console.log("mData OutletId => ", $scope.mData.OutletId);
      //console.log("mData Detail=> ", $scope.mData.GridDetail);
      //console.log("$scope.mData.GridDetail.length=> ", $scope.mData.GridDetail.length);
      //console.log("getSelectedRows() =>", $scope.gridApi.selection.getSelectedRows());
      PartsDeadStockMaintain.setDeadStockHeader($scope.mData);
      PartsDeadStockMaintain.setDeadStockDetail($scope.mData.GridDetail);
      console.log("GridDetail->", $scope.mData.GridDetail);

      if($scope.mData.GridDetail.length < 1){
        bsNotify.show(
              {
                  title: "Peringatan",
                  content: "Data material belum dipilih (dicentang) atau kosong",
                  type: 'error'
              }
            );
      } else {
        // cek qty free
        var QFree = 0;
        var QDS = 0;
        var Dscnt = 0;
        for(var i = 0; i < $scope.mData.GridDetail.length; i++){
          //console.log('QtyFree', $scope.mData.GridDetail[i].QtyFree);
          if($scope.mData.GridDetail[i].QtyFree == 0 || $scope.mData.GridDetail[i].QtyFree == null){
            QFree += 1;
          } // end if QtyFree
          if($scope.mData.GridDetail[i].QtyDS == 0 || $scope.mData.GridDetail[i].QtyDS == null || $scope.mData.GridDetail[i].QtyDS === undefined){
            QDS += 1;
          } // end if QtyDS
          if($scope.mData.GridDetail[i].Discount == 0 || $scope.mData.GridDetail[i].Discount == null || $scope.mData.GridDetail[i].Discount === undefined){
            Dscnt += 1;
          } // end if QtyDS
        } // end for
        // console.log('QFree', QFree);
        // console.log('QDS', QDS);
        // console.log('Dscnt', Dscnt);
        if(QFree > 0 ){
          bsNotify.show(
            {
                title: "Peringatan",
                content: "Qty Free tidak boleh '0'",
                type: 'danger'
            }
          );
        } else if(QDS > 0){
          bsNotify.show(
            {
                title: "Peringatan",
                content: "Qty Dead Stock tidak boleh kosong atau 0 (noll)",
                type: 'danger'
            }
          );
        } else if(Dscnt > 0){
          ngDialog.openConfirm ({
            template:'\
                         <div align="center" class="ngdialog-buttons">\
                         <p><b>Konfirmasi</b></p>\
                         <p><b>Diskon</b> belum diisi! Yakin akan melanjutkan proses simpan?</p>\
                         <div class="ngdialog-buttons" align="center">\
                           <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                           <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="DialogSimpan()">Ya</button>\
                         </div>\
                         </div>',
            plain: true,
            controller: 'PartsDeadStockMaintainController',
           });
        } else {
          $scope.DialogSimpan();
          // ngDialog.openConfirm ({
          //   template:'<div ng-include=\"\'app/parts/deadstockmaintain/referensi/dialog_simpan.html\'\"></div>',
          //   plain: true,
          //   controller: 'PartsDeadStockMaintainController',
          // });
        } // end else
      } // end else
    }; // end of simpan
    // ======== end tombol  simpan =========
    $scope.DialogSimpan = function(){
      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/deadstockmaintain/referensi/dialog_simpan.html\'\"></div>',
        plain: true,
        controller: 'PartsDeadStockMaintainController',
      });
      $scope.ngDialog.close();
    }

    // ======== tombol  simpan Approval=========
    $scope.SimpanApproval = function(data)
    {
      console.log("Data untuk Approval ", data);

      $scope.mData = PartsDeadStockMaintain.getDeadStockHeader();
      $scope.mData.GridDetail = PartsDeadStockMaintain.getDeadStockDetail();
      //$scope.ApproveData = PartsDeadStockMaintain.getDeadStockHeader(); 
      //console.log("mData CancelReasonDesc => ", $scope.mData.CancelReasonDesc);
      console.log("mData => ", $scope.mData);
      console.log("mData.DeadStockTypeId => ", $scope.mData.DeadStockTypeId);
      console.log("mData.OuletId => ", $scope.mData.OuletId);
      console.log("mData Detail=> ", $scope.mData.GridDetail);

      for(var i = 0; i < $scope.mData.GridDetail.length; i++)
      {
        //$scope.mData.GridDetail[i].StockAdjustmentStatusId = 2; //for approval
        //$scope.mData.GridDetail[i].OutletId = 655; //$scope.mData.OutletId;
        $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
        //$scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;
      }

      PartsDeadStockMaintain.create($scope.mData, $scope.mData.GridDetail).then(function(res) {
            var create = res.data;
            console.log('res.data = ', res.data);
            PartsDeadStockMaintain.sendNotif(data).then(function(res){
                console.log('notif send to Kabeng')
            });

            bsNotify.show(
              {
                  title: "Dead Stock Maintain",
                  content: "Data berhasil disimpan",
                  type: 'success'
              }
            );
          },
          function(err) {
            console.log("err=>", err);
          }
        );

      $scope.ngDialog.close();
      PartsDeadStockMaintain.formApi.setMode("grid");

    }
    // ======== end tombol simpan Approval=========

    // ======== tombol batal - parts claim - alasan =========

    $scope.pembatalan = function(){
      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/deadstockmaintain/referensi/dialog_batal.html\'\"></div>',
        plain: true,
        controller: 'PartsDeadStockMaintainController',
       });
    };

    // dialog konfirmasi
    $scope.okCancel = function(data){
      console.log('okCancel data = ', data);
      //$scope.mData = data.res;
      //console.log('okCancel $scope.mData = ', $scope.mData);
      $scope.BatalData = PartsDeadStockMaintain.getDeadStockHeader();
      $scope.BatalData.CancelReasonDesc = data.CancelReasonDesc;
      $scope.BatalData.CancelReasonId = data.CancelReasonId;
      console.log('$scope.BatalData = ', $scope.BatalData);
      PartsDeadStockMaintain.setDeadStockHeader($scope.BatalData);

      ngDialog.openConfirm ({
        //template:'<div ng-include=\"\'app/parts/deadstockmaintain/referensi/dialog_konfirmasi_batal.html\'\"></div>',
        template:'\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Anda yakin akan membatalkan Dead Stock ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinBatal(BatalData)">Ya</button>\
                     </div>\
                     </div>',
        plain: true,
        controller: 'PartsDeadStockMaintainController',
       });

       console.log('okCancel CancelReasonDesc = ', $scope.BatalData.CancelReasonDesc);
       //console.log('okCancel mdata  = ', $scope.mData.CancelReasonDesc);
       console.log('okCancel CancelReasonId  = ', $scope.BatalData.CancelReasonId);

    };

    $scope.yakinBatal = function(data){
      console.log('yakinBatal');
      $scope.mData = data;
      $scope.ngDialog.close();
      $scope.mData.DeadStockStatusId = 4;
      //$scope.mData.CancelReasonDesc;
      console.log('yakinBatal mData = ', $scope.mData);
      // [START] Get
      PartsDeadStockMaintain.cancel($scope.mData).then(function(res) {
          console.log('res cancel = ', res);
          bsNotify.show(
              {
                  title: "Dead Stock Maintain",
                  content: "Dead Stock Maintain has been successfully canceled.",
                  type: 'succes'
              }
            );
        },
        function(err) {
          console.log("err=>", err);
        }
      );
      PartsDeadStockMaintain.formApi.setMode("grid");
    }

    // ======== end tombol batal - parts claim =========
    $scope.ubah = function(){
      $scope.mData.GridDetail = $scope.gridEdit.data;
      $scope.mData.DeadStockStatusId = 2; // Draft to Need Approval
      PartsDeadStockMaintain.setDeadStockHeader($scope.mData);
      PartsDeadStockMaintain.setDeadStockDetail($scope.mData.GridDetail);
      console.log("setDeadStockHeader ", $scope.mData);
      console.log("$scope.mData.DeadStockStatusId ", $scope.mData.DeadStockStatusId);
      console.log("$scope.mData.GridDetail ri",$scope.mData.GridDetail);
      $scope.ApproveData = PartsDeadStockMaintain.getDeadStockHeader(); 

      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/deadstockmaintain/referensi/dialog_ubah.html\'\"></div>',
        plain: true,
        // controller: 'PartsDeadStockMaintainController',
        scope: $scope
       });
    };

    $scope.SimpanUbah = function(data){
      console.log("Data untuk Approval ", data);
      $scope.mData = PartsDeadStockMaintain.getDeadStockHeader();
      $scope.mData.GridDetail = PartsDeadStockMaintain.getDeadStockDetail();
      //$scope.ApproveData = PartsDeadStockMaintain.getDeadStockHeader(); 
      //console.log("mData CancelReasonDesc => ", $scope.mData.CancelReasonDesc);
      console.log("mData => ", $scope.mData);
      console.log("mData Detail=> ", $scope.mData.GridDetail);
      console.log('data terhapus', $scope.DeletedDataDeadStock);

      // for(var i = 0; i < $scope.mData.GridDetail.length; i++)
      // {
      //   //$scope.mData.GridDetail[i].StockAdjustmentStatusId = 2; //for approval
      //   $scope.mData.GridDetail[i].OutletId = 655; //$scope.mData.OutletId;
      //   //$scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;
      // }

      // PartsDeadStockMaintain.ubah($scope.mData, $scope.mData.GridDetail).then(function(res) { di komen karena griddetail terlalu panjang, jadi yang dikirim adalah data yang dihapus(updated)
      PartsDeadStockMaintain.ubah($scope.mData, $scope.DeletedDataDeadStock).then(function(res) {
          var create = res.data;
          console.log('res.data = ', res.data);
          bsNotify.show(
            {
                title: "Dead Stock Maintain",
                content: "Data berhasil diubah",
                type: 'success'
            }
          );
        },
        function(err) {
          console.log("err=>", err);
        }
      );
      ngDialog.close();
      PartsDeadStockMaintain.formApi.setMode("grid");
    }

    // Report =========================================
    $scope.laporan = function(){
      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/disposal/referensi/template_laporan.html\'\"></div>',
        plain: true,
        // width: 800px,
        controller: 'DisposalController',
       });
    };

    $scope.tambahCustomer = function(){
      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/directpartssales/salesorder/referensi/dialog_tambahCustomer.html\'\"></div>',
        plain: true,
        // width: 800px,
        controller: 'PartsSalesOrderController',
       });
    };

    $scope.GenerateDocNum = function(){
        console.log("$scope.mData.ServiceTypeId = ", $scope.mData.ServiceTypeId);
        PartsCurrentUser.getFormatId($scope.mData.ServiceTypeId, 'DS').then(function(res){
          var result = res.data;
          //console.log("FormatId result = ", result);
          console.log("FormatId = ", result[0].Results);

          PartsDeadStockMaintain.getDocumentNumber(result[0].Results).then(function(res) {
              var DocNo = res.data;
              if(typeof DocNo === 'undefined' || DocNo == null)
              {
                bsNotify.show(
                {
                    title: "Transfer Order",
                    content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                    type: 'danger'
                }
                );
              }
              else
              {
                $scope.mData.DeadStockNo = DocNo[0];
                $scope.mData.FilingDate = DocNo[1];
                console.log("Generating DeadStock & FilingDate ->");
                console.log($scope.mData.DeadStockNo);
                console.log($scope.mData.FilingDate);
              }
            },
            function(err) {
              console.log("err=>", err);
            }
          );
        },
        function(err){
          console.log("err=>", err);
        }
        );
        // [END] Get Current User Warehouse
      }

    $scope.onBeforeNewMode = function(mode){     
        // $scope.mData.ServiceTypeId = $scope.getRightServiceType();
        // console.log('ServiceType = ', $scope.mData.ServiceTypeId);
        // $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
        // console.log("MaterialTypeId = ", $scope.mData.MaterialTypeId);
        // approvers
        $scope.getApprover();

        PartsDeadStockMaintain.formApi = $scope.formApi;
        $scope.gridTambah.data = [];
        $scope.isModeTambah = true;
        $scope.isEditSuggestion = false;
        $scope.isViewSuggestion = false;
        $scope.isEditManual = false;
        $scope.isViewManual = false;
        //$scope.selection = null;
        //console.log("onBeforeNewMode = ", mode);

        var vICC = 2;
        PartsDeadStockMaintain.getICC(vICC).then(function(res){
            var data = res.data.Result;
            console.log("data ICC = ", data);
            $scope.mData.DataICC = data;
            //console.log('$scope.mData.DataICC => ', $scope.mData.DataICC);
          },
          function(err) {
              console.log("err=>", err);
          }
        );

        var vSCC = 3;
        PartsDeadStockMaintain.getSCC(vSCC).then(function(res){
            var data = res.data.Result;
            console.log("data SCC = ", data);
            $scope.mData.DataSCC = data;
            //console.log('$scope.mData.DataSCC => ', $scope.mData.DataSCC);
          },
          function(err) {
              console.log("err=>", err);
          }
        );
        if ($scope.user.RoleId == 1135) {
          $scope.mData.MaterialTypeId = 1;
          $scope.mData.ServiceTypeId = 1;
          console.log('1135', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
        } else if ($scope.user.RoleId == 1122) {
          $scope.mData.MaterialTypeId = 1;
          $scope.mData.ServiceTypeId = 1;
          console.log('1122', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
        } else if ($scope.user.RoleId == 1123) {
          $scope.mData.MaterialTypeId = 1;
          $scope.mData.ServiceTypeId = 0;
          console.log('1123', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
        } else if ($scope.user.RoleId == 1125) {
          $scope.mData.MaterialTypeId = 2;
          $scope.mData.ServiceTypeId = 1;
          console.log('1125', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
        } else if ($scope.user.RoleId == 1124) {
          $scope.mData.MaterialTypeId = 2;
          $scope.mData.ServiceTypeId = 0;
          console.log('1124', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
        } else if ($scope.user.RoleId == 1128) {
          $scope.mData.MaterialTypeId = 1;
          $scope.mData.ServiceTypeId = 1;
          console.log('1124', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);
        }
        console.log('$scope.mData.ServiceTypeId 0', $scope.mData.ServiceTypeId);
        console.log('$scope.mData.MaterialTypeId 0', $scope.mData.MaterialTypeId);
        console.log('$scope.user.OrgId 0', $scope.user.OrgId);
        PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.user.OrgId).then(function(res) {
            var gridData = res.data;
            console.log('gridData[0] = ', gridData[0]);

            $scope.mData.WarehouseId = gridData[0].WarehouseId;
            $scope.mData.OutletId = gridData[0].OutletId;
            $scope.mData.WarehouseData = gridData;

          },
          function(err) {
            console.log("err=>", err);
          }
        );

        $scope.GenerateDocNum();
    }
    $scope.onBeforeEditMode = function(row, mode){
        $scope.isModeTambah = false;
        $scope.isDisabled = false;
        console.log("onBeforeEdit=>", mode);
    }
    $scope.onBeforeDeleteMode = function(){
        $scope.formode = 3;
        $scope.isNewForm = false;
        $scope.isEditForm = false;
        console.log("mode=>",$scope.formmode);
    }

    $scope.isDisabled = false;
    $scope.onShowDetail = function(row, mode, xparam){
      console.log("row",row);
      console.log("INIa",xparam);
      if (xparam != null || xparam != undefined){  
        row.DeadStockId = xparam.DeadstockId;
      }
      else{
        row.DeadStockId = row.DeadStockId;
      }      
      console.log("harusnya udh ke isi", row.DeadStockId);
      PartsDeadStockMaintain.formApi = $scope.formApi;
      //console.log("row.DeadStockTypeId =>", row.DeadStockTypeId);
      //console.log("row.DeadStockId =>", row.DeadStockId);
      //console.log("isEditManual =>", isEditManual);
      PartsDeadStockMaintain.setDeadStockHeader(row);
      console.log("mode apa? =>", mode);

      if(mode != 'new'){
        console.log('mode new ? = ', mode);
        PartsDeadStockMaintain.getDetail(row.DeadStockId).then(function(res) {
            var gridData = res.data.Result;
            console.log('PartsDeadStockMaintain.getDetail = ', gridData);
            if(mode == 'view'){
              $scope.gridView.data = gridData;
            } else if(mode == 'edit'){
              $scope.gridEdit.data = gridData;
              console.log('gridddd woiiiiiii', $scope.gridEdit.data);
            }
          },
          function(err) {
            console.log("err=>", err);
          }
        );
      }

      if(mode == 'view'){
        $scope.isModeTambah = false;
        $scope.isEditSuggestion = false;
        $scope.isEditManual = false;
        if(row.DeadStockTypeId == 1){
          $scope.isViewSuggestion = true;
          $scope.isViewManual = false;
          $scope.isDisabled = true;
          //console.log('isDisabled = ', $scope.isDisabled);
        } else
        if(row.DeadStockTypeId == 2){
          $scope.isViewSuggestion = false;
          $scope.isViewManual = true;
          $scope.isDisabled = false;
        }
      } else
      if(mode == 'edit') {
        $scope.isModeTambah = false;
        $scope.isViewSuggestion = false;
        $scope.isViewManual = false;
        if(row.DeadStockTypeId == 1){
          $scope.isEditSuggestion = true;
          $scope.isEditManual = false;
          $scope.isDisabled = true;
          //console.log('isDisabled = ', $scope.isDisabled);
        } else
        if(row.DeadStockTypeId == 2){
          $scope.isEditSuggestion = false;
          $scope.isEditManual = true;
          $scope.isEditSuggestion = false;
          $scope.isDisabled = true;
        } else
        if(mode == 'new') {
          $scope.isModeTambah = true;
          $scope.isViewSuggestion = false;
          $scope.isViewManual = false;
        }
      }

      console.log("onShowDetail=>", mode);
    }

    //----------------------------------
    // Select Option Referensi GI Setup
    //----------------------------------
    $scope.changedValue = function (item){
      console.log(item);
      $scope.isOverlayForm = false;
      $scope.ref = item;
      $scope.disableSelect  = true;
    }

    var dateFormat='dd/MM/yyyy';
    // $scope.dateOptions = {
    //   startingDay: 1,
    //   format: dateFormat
    // };

    $scope.getDataNA = function() {
      PartsDeadStockMaintain.getNeededApproval().then(function(res) { // start getdata NA
          var gridData = res.data;
          console.log('controller-getData-gridData = ', gridData);
          $scope.grid.data = gridData;
          $scope.loading = false;
        },
        function(err) {
          console.log("err=>", err);
        }
      ); // end getdata NA
    } // end

    //  column defs
    $scope.columnsDeadStock = [
      {
        name: 'Id', field: 'DeadStockId', width: 100, visible: false
      },
      {
        name: 'tipe Dead Stock',
        field: 'DeadStockTypeId',
        displayName: 'Tipe Dead Stock Maintain',
        width: '18%',
        cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameTipeDS(row.entity.DeadStockTypeId)}}</div>',
      },
      { name: 'no DS', field: 'DeadStockNo', displayName: 'No. DS', width: '18%'},
      {
        name: 'tanggal Pengajuan DS',
        field: 'FilingDate',
        displayName: 'Tanggal Pengajuan DS',
        width: '15%',
        cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToDate(row.entity.FilingDate)}}</div>',
      },
      {
        name: 'masa Berlaku Dari',
        field: 'ValidFrom',
        displayName: 'Masa Berlaku Dari',
        width: '12%',
        cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToDate(row.entity.ValidFrom)}}</div>',
      },
      {
        name: 'masa Berlaku Sampai',
        field: 'ValidTo',
        displayName: 'Masa Berlaku Sampai',
        width: '14%',
        cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToDate(row.entity.ValidTo)}}</div>',
      },
      {
        name: 'status',
        field: 'Status',
        //field: 'DeadStockStatusId',
        width: '15%',
        //cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatus(row.entity.DeadStockStatusId)}}</div>',
      }
    ];

    //  need approval column definition
    $scope.columnsNeedApproval = [
      {
        name: 'Id', field: 'DeadStockId', width: 100, visible: false
      },
      {
        name: 'tipe Dead Stock',
        field: 'DeadStockTypeId',
        displayName: 'Tipe Dead Stock Maintain',
        width: '18%',
        cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameTipeDS(row.entity.DeadStockTypeId)}}</div>',
      },
      { name: 'no DS', field: 'DeadStockNo', displayName: 'No. DS', width: '18%'},
      {
        name: 'tanggal Pengajuan DS',
        field: 'FilingDate',
        displayName: 'Tanggal Pengajuan DS',
        width: '15%',
        cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToDate(row.entity.FilingDate)}}</div>',
      },
      {
        name: 'masa Berlaku Dari',
        field: 'ValidFrom',
        displayName: 'Masa Berlaku Dari',
        width: '12%',
        cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToDate(row.entity.ValidFrom)}}</div>',
      },
      {
        name: 'masa Berlaku Sampai',
        field: 'ValidTo',
        displayName: 'Masa Berlaku Sampai',
        width: '14%',
        cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToDate(row.entity.ValidTo)}}</div>',
      },
      {
        name: 'status',
        //field: 'Status',
        field: 'DeadStockStatusId',
        width: '15%',
        cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatus(row.entity.DeadStockStatusId)}}</div>',
      }
    ];

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
      enableSorting: true,
      enableRowSelection: true,
      multiSelect: true,
      enableSelectAll: true,
      //showTreeExpandNoChildren: true,
      // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
      // paginationPageSize: 15,
      columnDefs: $scope.columnsNeedApproval
    };


    //----------------------------------
    // Dummy
    //----------------------------------

    $scope.showMe = function(){
        confirm('Apakah Anda yakin menghapus data ini?');
    };

    $scope.gridOptions = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableFiltering : true,
        //selectedItems: console.log($scope.mySelections),
        enableSelectAll: true,
        paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 5,

        columnDefs: [
          {
          name: 'idDetail', field: 'idDetail', width: 100, visible: false
          },
          { name:'NoMaterial', displayName:'No. Material', field: 'NoMaterial', width: '15%'},
          { name:'NamaMaterial', displayName:'Nama Material', field: 'NamaMaterial', width: '15%'},
          { name:'icc', displayName:'ICC', field: 'icc', width: '8%'},
          { name:'aging', displayName:'Aging (Hari)', field: 'aging', width: '11%'},
          { name:'QtyFree', displayName:'Qty Free', field: 'QtyFree', width: '10%'},
          { name:'QtyDS', displayName:'Qty Dead Stock', field: 'QtyDS', width: '13%'},
          { name:'Satuan', displayName:'Satuan', field: 'Satuan', width: '9%'},
          { name:'diskon', displayName:'Diskon', field: 'diskon', width: '9%'},
          {
            name:'_',
            displayName:'Action',
            width: '10%',
            //enableCellEdit: false,
            // visible : true,
            cellTemplate:'<div style="padding-top: 5px;"><u><a href="#" ng-click="grid.appScope.MainDeadStock_Delete(row)">Hapus</a></u></div>'

          }
        ],
        data : [{
                  "idDetail": 1,
                  "NoMaterial": "09905-00012",
                  "NamaMaterial": "EXPANDER",
                  "icc": "K",
                  "aging": 200,
                  "QtyFree": 2,
                  "QtyDS": 1,
                  "Satuan": "Pieces",
                  "diskon": 15
                },
                {
                  "idDetail": 2,
                  "NoMaterial": "48520-09530",
                  "NamaMaterial": "ABSORBER A/S FR LH",
                  "icc": "K",
                  "aging": 300,
                  "QtyFree": 1,
                  "QtyDS": 1,
                  "Satuan": "Pieces",
                  "diskon": 25
                }
            ]
      };

      $scope.Add = function(){
        var n = $scope.gridTambah.data.length + 1;
        $scope.gridTambah.data.push({
          "idDetail": "",
          "PartId": "",
          "NoMaterial": "",
          "NamaMaterial": ""
        });
      }

      /*$scope.Delete = function(row) {
        console.log('tesssss')
          var index = $scope.gridTambah.data.indexOf(row.entity);
          //var index2 = $scope.gridOptionsLangsung.data.indexOf(row.entity);
          $scope.gridTambah.data.splice(index, 1);
          //$scope.gridOptionsLangsung.data.splice(index2, 1);
          console.log($scope.gridTambah.data);
          console.log(index);
          // console.log($scope.gridOptionsLangsung.data);
          // console.log(index2);
      };*/

      $scope.MainDeadStock_Delete = function(row){
        DeadStockFactory.DeleteDeadStock(row.entity.DeadstockId)
        .then(
          function(res){
            alert("Data berhasil dihapus");

          }
        );
      }


      $scope.DeletedDataDeadStock = [];
    $scope.DeleteGridEdit = function(row) {
        var index = $scope.gridEdit.data.indexOf(row.entity);
        $scope.gridEdit.data.splice(index, 1);
        $scope.DeletedDataDeadStock.push(row.entity);
        console.log('data yang dihapus', $scope.DeletedDataDeadStock);
        console.log($scope.gridEdit.data);
        console.log(index);
    };

      $scope.gridTambah = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableFiltering : true,
        //selectedItems: console.log($scope.mySelections),
        enableSelectAll: true,
        paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
        paginationPageSize: 10,

        // onRegisterApi: function(gridApi) {
        //   $scope.gridApi = gridApi;
        //   console.log("getSelectedRows", $scope.gridApi.selection.getSelectedGridRows());
        // },

        columnDefs: [
          {
            name: 'DeadStockItemId', field: 'DeadStockItemId', width: 100, visible: false
          },
          {
            name: 'DeadStockItemId', field: 'DeadStockItemId', width: 100, visible: false
          },
          {
            name: 'OutletId', field: 'OutletId', width: 100, visible: false
          },
          { name:'PartId', displayName:'Part ID', field: 'PartId', width: '15%', visible: false},
          { name:'NoMaterial', displayName:'No. Material', field: 'PartsCode', enableCellEdit: false, width: '15%'},
          { name:'NamaMaterial', displayName:'Nama Material', field: 'PartsName', enableCellEdit: false, width: '15%'},
          { name:'icc', displayName:'ICC', field: 'ICC', enableCellEdit: false, width: '8%'},
          { name:'aging', displayName:'Aging (Hari)', field: 'Aging', enableCellEdit: false, width: '11%'},
          { name:'QtyFree', displayName:'Qty Free', field: 'QtyFree',enableCellEdit: false, width: '10%'},
          { name:'QtyDS', displayName:'Qty Dead Stock', field: 'QtyDS', width: '13%'},
          //{ name:'QtyOrder', displayName:'Qty Order', field: 'QtyOrder', width: '13%'},
          { name:'Satuan', displayName:'Satuan', field: 'Satuan', enableCellEdit: false, width: '9%'},
          { name:'diskon', displayName:'Diskon', field: 'Discount', width: '9%'},
          {
            name:'_',
            displayName:'Action',
            width: '10%',
            enableCellEdit: false,
            // visible : true,
            cellTemplate:'<div style="padding-top: 5px;"><u><a href="#" ng-click="grid.appScope.Delete(row)">Hapus</a></u></div>'
          }
        ],
        //data : []
      };

      $scope.gridTambahSelections = [];
      $scope.gridTambah.onRegisterApi = function(gridApi){
        //set gridApi on scope
        $scope.gridApi = gridApi;
        console.log("getSelectedRows", $scope.gridApi.selection.getSelectedGridRows());
        //$scope.selectedRow = $scope.gridApi.selection.getSelectedGridRows();
        gridApi.selection.on.rowSelectionChanged($scope, function(row) {
          $scope.gridTambahSelections = gridApi.selection.getSelectedRows();
          console.log('gridTambahSelections', $scope.gridTambahSelections);
        });
        // gridApi.selection.on.rowSelectionChanged($scope,function(row){
        //   var msg = 'row selected ' + row.isSelected;
        //   //$log.log(msg);
        //   console.log('row selected ', row.isSelected);
        //   console.log('row ', row);
        // });
        // gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
        //   var msg = 'rows changed ' + rows.length;
        //   //$log.log(msg);
        //   console.log('row changed ', row.length);
        //   console.log('row ', row);
        // });
      }; //end of onRegisterApi

      $scope.gridEdit = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableFiltering : true,
        //selectedItems: console.log($scope.mySelections),
        enableSelectAll: true,
        paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 5,

        // onRegisterApi: function(gridApi) {
        //   $scope.gridApi = gridApi;
        // },

        columnDefs: [
          {
            name: 'DeadStockItemId', field: 'DeadStockItemId', width: 100, visible: false
          },
          {
            name: 'DeadStockId', field: 'DeadStockId', width: 100, visible: false
          },
          { name:'NoMaterial', displayName:'No. Material', field: 'PartsCode', width: '13%', enableCellEdit: false },
          { name:'NamaMaterial', displayName:'Nama Material', field: 'PartsName', width: '15%', enableCellEdit: false},
          { name:'icc', displayName:'ICC', field: 'ICC', width: '13%', enableCellEdit: false },
          { name:'aging', displayName:'Aging (Hari)', field: 'Aging', width: '10%', enableCellEdit: false },
          { name:'QtyFree', displayName:'Qty Free', field: 'QtyFree', width: '9%', enableCellEdit: false },
          { name:'QtyDS', displayName:'Qty Dead Stock', field: 'QtyDS', type: 'number', width: '13%', enableCellEdit: true },
          { name:'Satuan', displayName:'Satuan', field: 'Satuan', width: '9%', enableCellEdit: false },
          { name:'diskon', displayName:'Diskon', field: 'Discount', type: 'number', width: '8%', enableCellEdit: true },
          {
            name:'_',
            displayName:'Action',
            width: '10%',
            enableCellEdit: false,
            // visible : true,
            cellTemplate:'<div style="padding-top: 5px;"><u><a href="#" ng-click="grid.appScope.$parent.DeleteGridEdit(row)">Hapus</a></u></div>'
          }
        ]
      };

      // $scope.deleteSelected = function(row){
      //   console.log('row', row);
      //   console.log('row.entity', row.entity);
      //   console.log('getSelectedRows', $scope.gridApi.selection.getSelectedRows(row.entity));
      //   console.log('lastIndexOf', $scope.gridEdit.data.lastIndexOf());
      //   angular.forEach($scope.gridApi.selection.getSelectedRows(), function (data, index) {
      //     $scope.gridEdit.data.splice($scope.gridEdit.data.lastIndexOf(data), 1);
      //   });
      //   //console.log('index', index);
      //   //alert('delete');
      // }
      $scope.mySelections = [];
      $scope.deleteSelected = function () {
        console.log('cb ini')
        // console.log('data grid', $scope.gridEdit.data);
        console.log(selected);
        var gridEditData = $scope.gridEdit.data;
        //console.log('gridEditData', gridEditData);
        //var selectedRow = $scope.selectedRow;
        //console.log('$scope.mySelections', $scope.mySelections);
        //console.log("getSelectedRows", $scope.gridApi.selection.getSelectedRows(selectedRow));
        var result = confirm('Apakah Anda yakin menghapus data ini?');
        if (result) {

          var data = $scope.mySelections;//$scope.gridApi.selection.getSelectedRows();
          //console.log("data getSelectedRows", data);
          angular.forEach(data, function (gridEditData, index) {
            $scope.gridEdit.data.splice($scope.gridEdit.data.lastIndexOf(gridEditData), 1);
            //$scope.gridOptionsLangsung.data.splice($scope.gridOptionsLangsung.data.lastIndexOf(data), 1);
          });
        }
      };
      $scope.gridEdit.onRegisterApi = function(gridApi){
        //set gridApi on scope
        $scope.gridApi = gridApi;
        //console.log("getSelectedRows", $scope.gridApi.selection.getSelectedGridRows());
        //$scope.selectedRow = $scope.gridApi.selection.getSelectedGridRows();
        gridApi.selection.on.rowSelectionChanged($scope, function(row) {
          $scope.mySelections = gridApi.selection.getSelectedRows();
          // if (row.isSelected) {
          //   $scope.mySelectionsInOrder.push(row.entity);
          // } else {
          //   $scope.mySelectionsInOrder.splice($scope.mySelectionsInOrder.indexOf(row.entity), 1);
          // }
          //console.log('mySelections', $scope.mySelections);
        });
        // gridApi.selection.on.rowSelectionChanged($scope,function(row){
        //   var msg = 'row selected ' + row.isSelected;
        //   //$log.log(msg);
        //   console.log('row selected ', row.isSelected);
        //   console.log('row ', row);
        // });
        // gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
        //   var msg = 'rows changed ' + rows.length;
        //   //$log.log(msg);
        //   console.log('row changed ', row.length);
        //   console.log('row ', row);
        // });
      }; //end of onRegisterApi

      $scope.gridView = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableFiltering : true,
        //selectedItems: console.log($scope.mySelections),
        enableSelectAll: true,
        paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 5,

        columnDefs: [
          {
            name: 'DeadStockItemId', field: 'DeadStockItemId', width: 100, visible: false
          },
          {
            name: 'DeadStockId', field: 'DeadStockId', width: 100, visible: false
          },
          { name:'NoMaterial', displayName:'No. Material', field: 'PartsCode', width: '13%', enableCellEdit: false },
          { name:'NamaMaterial', displayName:'Nama Material', field: 'PartsName', width: '15%', enableCellEdit: false},
          { name:'icc', displayName:'ICC', field: 'ICC', width: '13%', enableCellEdit: false },
          { name:'aging', displayName:'Aging (Hari)', field: 'Aging', width: '10%', enableCellEdit: false },
          { name:'QtyFree', displayName:'Qty Free', field: 'QtyFree', width: '9%', enableCellEdit: false },
          { name:'QtyDS', displayName:'Qty Dead Stock', field: 'QtyDS', width: '13%', enableCellEdit: false },
          { name:'Satuan', displayName:'Satuan', field: 'Satuan', width: '9%', enableCellEdit: false },
          { name:'diskon', displayName:'Diskon', field: 'Discount', width: '8%', enableCellEdit: false }
        ]
      };

      $scope.hello = function(){
        alert('OK');
        //console.log("");
      }

      $scope.ToNameTipeDS = function(DeadStockTypeId){
      var GiName;
      switch(DeadStockTypeId)
      {
        case 1:
        case "1":
          GiName = "Suggestion By System";
          break;
        case 2:
        case "2":
          GiName = "Manual";
          break;
      }
      return GiName;
    }

    $scope.ToNameStatus = function(DeadStockStatusId){
      var statusName;
      switch(DeadStockStatusId)
      {
        case 1:
          statusName = "Draft";
          break;
        case 2:
          statusName = "Request Approval";
          break;
        case 3:
          statusName = "Published";
          break;
        case 4:
          statusName = "Cancelled";
          break;
      }
      return statusName;
    }

    $scope.ToDate = function(data)
    {
      var vDate = $filter('date')(data, 'yyyy-MM-dd');
      return vDate
    }

    $scope.gridActionTemplate = '<div class="ui-grid-cell-contents"> \
  	<a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat" tooltip-placement="bottom" onclick="this.blur()" ng-if="!row.groupHeader" ng-click="grid.appScope.gridClickViewDetailHandler(row.entity)" tabindex="0"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
  	<a href="" style="color:#777;" ng-show="row.entity.DeadStockStatusId==1||row.entity.DeadStockStatusId==2" class="trlink ng-scope" uib-tooltip="Ubah" tooltip-placement="bottom" onclick="this.blur()" ng-if="grid.appScope.allowEdit &amp;&amp; !grid.appScope.hideEditButton &amp;&amp; !row.groupHeader &amp;&amp; !grid.appScope.defHideEditButtonFunc(row)" ng-click="grid.appScope.gridClickEditHandler(row.entity)" tabindex="0"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
  	</div>';

  });
