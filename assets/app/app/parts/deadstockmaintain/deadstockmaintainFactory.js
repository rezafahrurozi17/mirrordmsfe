angular.module('app')
  .factory('PartsDeadStockMaintain', function($http, CurrentUser, $filter) {
    var currentUser = CurrentUser.user;
    var DeadStockHeader = {};
    var DeadStockDetail = {};
    var currentDate = new Date();
    this.formApi = {};
    return {
      getData: function(data) {
        console.log("[Factory] getData", data);
        //filter tanggal
        var vValidFrom = $filter('date')(data.vValidFrom, 'yyyy-MM-dd');
        var vValidTo = $filter('date')(data.vValidTo, 'yyyy-MM-dd');
        // var vValidFrom = $filter('date')(data.vValidFrom, 'yyyy-MM-ddT00:00:00Z');
        // var vValidTo = $filter('date')(data.vValidTo, 'yyyy-MM-ddT00:00:00Z');
        //console.log("[Factory] vValidFrom", vValidFrom);
        //console.log("[Factory] vValidFrom", vValidTo);
        console.log("[Factory] OutletId", data.OutletId);
        var res=$http.get('/api/as/DeadStock', {params: {
                                                          OutletId: data.OutletId,
                                                          DeadStockTypeId : (data.vDeadStockTypeId==null?"n/a":data.vDeadStockTypeId),
                                                          DeadStockStatusId: (data.vDeadStockStatusId==null?"n/a":data.vDeadStockStatusId),
                                                          ValidFrom: (vValidFrom==null?"n/a":vValidFrom),
                                                          ValidTo: (vValidTo==null?"n/a":vValidTo),
                                                          DeadStockNo: (data.vDeadStockNo==null?"n/a":data.vDeadStockNo),
                                                          WarehouseId: (data.WarehouseId==null?"n/a":data.WarehouseId)
                                                        } });
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      getDataDraft: function(data) {
        console.log("[Factory] getData", data);
        //filter tanggal
        var vValidFrom = $filter('date')(data.vValidFrom, 'yyyy-MM-dd');
        var vValidTo = $filter('date')(data.vValidTo, 'yyyy-MM-dd');
        // var vValidFrom = $filter('date')(data.vValidFrom, 'yyyy-MM-ddT00:00:00Z');
        // var vValidTo = $filter('date')(data.vValidTo, 'yyyy-MM-ddT00:00:00Z');
        //console.log("[Factory] vValidFrom", vValidFrom);
        //console.log("[Factory] vValidFrom", vValidTo);
        console.log("[Factory] OutletId", data.OutletId);
        var res=$http.get('/api/as/DeadStock', {params: {
                                                          OutletId: data.OutletId,
                                                          DeadStockTypeId : (data.vDeadStockTypeId==null?"n/a":data.vDeadStockTypeId),
                                                          DeadStockStatusId: (data.DeadStockStatusId==null?"n/a":data.DeadStockStatusId),
                                                          ValidFrom: (vValidFrom==null?"n/a":vValidFrom),
                                                          ValidTo: (vValidTo==null?"n/a":vValidTo),
                                                          DeadStockNo: (data.vDeadStockNo==null?"n/a":data.vDeadStockNo),
                                                          WarehouseId: (data.WarehouseId==null?"n/a":data.WarehouseId)
                                                        } });
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      getDetail: function(data){
        console.log('<factory> getDetail = ', data);
        var res=$http.get('/api/as/DeadStock/GetDeadStockItem', {params: {
                                                          DeadStockId : data,
                                                        } });
        console.log('hasil=>',res);
        return res;
      },
      create: function(data, detail) {
        console.log('tambah data=>', data);
        console.log('tambah data.DeadStockNo=>', data.DeadStockNo);
        console.log('tambah data.OutletId=>', data.OutletId);
        console.log('tambah detail=>', detail);
        var ValidFrom = $filter('date')(data.ValidFrom, 'yyyy-MM-dd');
        var ValidTo = $filter('date')(data.ValidTo, 'yyyy-MM-dd');
        var CurrDate = $filter('date')(currentDate, 'yyyy-MM-dd');
        return $http.post('/api/as/DeadStock', [{
                                            DeadStockTypeId: data.DeadStockTypeId,
                                            DeadStockStatusId: 2, //(data.DeadStockStatusId==null?"n/a":data.DeadStockStatusId),
                                            ValidFrom: (ValidFrom == null ? "n/a" : ValidFrom),
                                            ValidTo: (ValidTo == null ? "n/a" : ValidTo),
                                            DeadStockNo: (data.DeadStockNo == null ? "n/a" : data.DeadStockNo),
                                            OutletId: (data.OutletId == null ? 9999 : data.OutletId),
                                            FilingDate: (data.FilingDate == null ? CurrDate : data.FilingDate),
                                            Notes: (data.Notes == null ? "n/a" : data.Notes),
                                            CancelReasonDesc: (data.CancelReasonDesc == null ? "n/a" : data.CancelReasonDesc),
                                            CreateDate: CurrDate,
                                            ApprovalStatus: 0, // 0 = not approved yet
                                            GridDetail: detail
                                          }]);
      },
      //searchParts: function(OutletId, PartsClassId2, PartsClassId3, PartsCode, PartsName) {
        searchParts: function(data) {
        console.log("[Factory] searchParts=> ", data);
        // console.log("data.OutletId", OutletId);
        // console.log("data.PartsClassId2", PartsClassId2);
        // console.log("data.PartsClassId3", PartsClassId3);
        // console.log("data.PartsCode", PartsCode);
        // console.log("data.PartsName", PartsName);

        var res=$http.get('/api/as/DeadStock/SearchParts', {params: {
                                                          OutletId: data.OutletId,
                                                          PartsClassId2: ((data.PartsClassId2==null) || (data.PartsClassId2 === undefined) ? "" : data.PartsClassId2),
                                                          PartsClassId3: ((data.PartsClassId3==null) || (data.PartsClassId3 === undefined) ? "" : data.PartsClassId3),
                                                          PartsCode: ((data.PartsCode==null) || (data.PartsCode === undefined) ? "" : data.PartsCode), //(data.PartsCode==null?"n/a":data.PartsCode),
                                                          PartsName: ((data.PartsName==null) || (data.PartsName === undefined) ? "" : data.PartsName), //(data.PartsName==null?"n/a":data.PartsName)
                                                          WarehouseId : ((data.WarehouseId==null) || (data.WarehouseId === undefined) ? 0 : data.WarehouseId)
                                                        } });
        console.log('hasil=>', res);
        return res;
      },
      cancel: function(data){
        console.log('batal data=>', data);
        var vFilingDate = $filter('date')(data.FilingDate, 'yyyy-MM-ddT00:00:00Z');
        var vValidFrom = $filter('date')(data.ValidFrom, 'yyyy-MM-ddT00:00:00Z');
        var vValidTo = $filter('date')(data.ValidTo, 'yyyy-MM-ddT00:00:00Z');
        return $http.put('/api/as/DeadStock/Batal', [{
                                            DeadStockId: data.DeadStockId,
                                            DeadStockNo: data.DeadStockNo,
                                            OutletId: data.OutletId,
                                            FilingDate: vFilingDate,
                                            ValidFrom: vValidFrom,
                                            ValidTo: vValidTo,
                                            DeadStockStatusId: data.DeadStockStatusId,
                                            Notes: data.Notes,
                                            DeadStockTypeId: data.DeadStockTypeId
                                          }]);
      },
      ubah: function(data, detail){
        console.log('ubah data=>', data);
        var vFilingDate = $filter('date')(data.FilingDate, 'yyyy-MM-ddT00:00:00Z');
        var vValidFrom = $filter('date')(data.ValidFrom, 'yyyy-MM-ddT00:00:00Z');
        var vValidTo = $filter('date')(data.ValidTo, 'yyyy-MM-ddT00:00:00Z');
        return $http.put('/api/as/DeadStock', [{
                                            DeadStockId: data.DeadStockId,
                                            DeadStockNo: data.DeadStockNo,
                                            OutletId: data.OutletId,
                                            FilingDate: vFilingDate,
                                            ValidFrom: vValidFrom,
                                            ValidTo: vValidTo,
                                            DeadStockStatusId: data.DeadStockStatusId,
                                            Notes: data.Notes,
                                            DeadStockTypeId: data.DeadStockTypeId,
                                            CancelReasonDesc: (data.CancelReasonDesc==null?"n/a":data.CancelReasonDesc),
                                            GridDetail: detail
                                          }]);
      },
      DeleteDeadStock: function(id) {
        console.log("delete id==>",id);
        var url = 'api/as/DeadStock/DeleteDeadStock/?DeadStockId='+DeadStockId;
        console.log("delete id==>", url);
        var res=$http.delete(url);
        return res;
      },

      getStatus: function() {
        console.log("[Factory] getStatus data = ");
        var res=$http.get('/api/as/DeadStock/Status');
        console.log('getStatus res =>', res);
        return res;
      },
      getDocumentNumber: function(FormatId) {
        console.log("[getDocumentNumber]");
        console.log('FormatId', FormatId);
        var res=$http.get('/api/as/PartsUser/GetDocumentNumber', {params: {
                                                          FormatId : FormatId,
                                                        } });
        console.log('hasil=>',res);
        return res;
      },
      getICC: function(data) {
        console.log("[Factory] getICC data = ", data);
        var res=$http.get('/api/as/DeadStock/LookupClass', {params: {
                                                          ClassTypeId : data,
                                                        } });
        console.log('getICC res =>', res);
        return res;
      },
      getSCC: function(data) {
        console.log("[Factory] getSCC data = ", data);
        var res=$http.get('/api/as/DeadStock/LookupClass', {params: {
                                                          ClassTypeId : data,
                                                        } });
        console.log('getSCC res =>', res);
        return res;
      },
      getApproverDeadStock: function(data) {
        console.log('getApproverDeadStock', data);
        var res=$http.get('/api/as/PartGlobal/GetApprover', {params: {
                                                          ProcessId : data,
                                                        } });
        console.log('hasil=>',res);
        return res;
      },
      getNeededApproval: function() {
        console.log("getNeededApproval");
        var res=$http.get('/api/as/DeadStock/GetNeedApproval');
        console.log('hasil=>',res);
        return res;
      },
      setDeadStockHeader : function(data){
        DeadStockHeader = data;
      },
      getDeadStockHeader : function(){
        return DeadStockHeader;
      },
      setDeadStockDetail : function(data){
        DeadStockDetail = data;
      },
      getDeadStockDetail : function(){
        return DeadStockDetail;
      },
      getStatusDeadStock: function() {
        var res=$http.get('/api/as/DeadStock/Status');
        return res;
      },
      sendNotif: function(data) {
          console.log("data notif", data.DeadStockNo);
          // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
          return $http.post('/api/as/SendNotification', [{
              Message: 'DeadStock baru ' + data.DeadStockNo + ', butuh di approve',
              RecepientId: 1128,  //kirim ke id kabeng sementara hardcode
              Param : 1
          }]);
      },
      sendNotifToPartsman: function(data) {
          console.log("data notif ke partmans", data.DeadStockNo);
          // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
          return $http.post('/api/as/SendNotification', [{
              Message: 'Notif approval ' + data.DeadStockNo + ', data sudah diapprove',
              // RecepientId: CreatedUserId,
              RecepientId: 11590,  //kirim ke id kabeng sementara hardcode
              Param : 2
          }]);
      },

    }
  });
