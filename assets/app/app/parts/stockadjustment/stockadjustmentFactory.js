angular.module('app')
  .factory('StockAdjustment', function($http, CurrentUser, $filter) {
    var currentUser = CurrentUser.user;
    var StockAdjustmentHeader = {};
    var StockAdjustmentDetail = {}; 
    this.formApi = {};   
    // console.log(currentUser);
    return {
      getData: function(data) {
        console.log("[Factory]");
        console.log(data);

        //filter tanggal
        var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
        var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');

        var res=$http.get('/api/as/StockAdjustment', {params: {
                                                          WarehouseId: data.WarehouseId,
                                                          RefSATypeId : (data.fRefSATypeId==null?"n/a":data.fRefSATypeId), 
                                                          StockAdjustmentStatusId: (data.fStockAdjustmentStatusId==null?"n/a":data.fStockAdjustmentStatusId),
                                                          StockAdjustmentNo: (data.fStockAdjustmentNo==null?"n/a":data.fStockAdjustmentNo),
                                                          StockOpnameNo: (data.fStockOpnameNo==null?"n/a":data.fStockOpnameNo),
                                                          startDate: (startDate==null?"n/a":startDate),
                                                          endDate: (endDate==null?"n/a":endDate),
                                                          WarehouseName: (data.fWarehouseName==null?"n/a":data.fWarehouseName)
                                                        } });
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },

      // Detail digunakan saat View dan Edit
      getDetail: function(data){
        
        console.log('<factory> getDetail = ', data);
        var res=$http.get('/api/as/StockAdjustment/GetStockAdjustmentDetail', {params: {
                                                          StockAdjustmentId : data, 
                                                        } });
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;

      },

      // Detail digunakan saat View dan Edit
      getDetailByMaterialNo: function(data, materialtype, servicetype, warehouse){
        console.log("[getDetail]");
        console.log(data);
        var res=$http.get('/api/as/StockAdjustment/GetStockAdjustmentDetailFromMaterial', {params: {
                                                          PartsCode : data,
                                                          ServiceTypeId : servicetype,
                                                          PartsClassId:materialtype.toString(),
                                                          nMatchBit: 1
                                                          // WarehouseId : warehouse,
                                                          // MaterialTypeId : materialtype,
                                                        } });
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;

      },

      getDocumentNumber: function(FormatId) {
        console.log("[getDocumentNumber]");
        console.log('FormatId', FormatId);
        var res=$http.get('/api/as/PartsUser/GetDocumentNumber', {params: {
                                                          FormatId : FormatId,
                                                        } });
        console.log('hasil=>',res);
        return res;
      },

      create: function(data, detail) {
        console.log('tambah data=>', data);
        console.log('tambah detail=>', detail);
        var startDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
        return $http.post('/api/as/StockAdjustment', [{
                                            //GoodsIssueId: (data.GoodsIssueId==null?0:data.GoodsIssueId),
                                            StockAdjustmentNo: data.StockAdjustmentNo,
                                            OutletId: data.OutletId,
                                            WarehouseId: data.WarehouseId,
                                            DocDate: startDate,
                                            MaterialTypeId: data.MaterialTypeId,
                                            RefSATypeId: data.RefSATypeId,
                                            RefSANo: (data.RefSANo==null?"-":data.RefSANo),
                                            //LocationId: data.LocationId,
                                            LocationId: data.LocationId,
                                            StockAdjustmentReasonId: data.StockAdjustmentReasonId,
                                            Notes: data.Notes,
                                            StockAdjustmentStatusId: (data.StockAdjustmentStatusId == null ? "1": data.StockAdjustmentStatusId),
                                            //Message: data.Message, // for message approval
                                            CancelReasonDesc: data.CancelReasonDesc, // temporary, for message approval
                                            GridDetail: detail
                                          }]);
      },
      createDetail: function(data) {
        console.log('tambah data grid detail=>', data);

        return $http.post('/api/as/StockAdjustment/InsertStockAdjustmentDetail', data);
      },

      cancel: function(data){
        console.log('[SAFactory] batal data=>', data);
        var vdocDate = $filter('date')(data.DocDate, 'yyyy-MM-ddT00:00:00Z');
        return $http.put('/api/as/StockAdjustment', [{
                                            StockAdjustmentId: data.StockAdjustmentId,
                                            StockAdjustmentNo: data.StockAdjustmentNo,
                                            OutletId: data.OutletId,
                                            WarehouseId: data.WarehouseId,
                                            DocDate: vdocDate, //data.DocDate,
                                            MaterialTypeId: data.MaterialTypeId,
                                            RefSATypeId: data.RefSATypeId,
                                            RefSANo: (data.RefSANo==null?"-":data.RefSANo),
                                            LocationId: data.LocationId,
                                            StockAdjustmentReasonId: data.StockAdjustmentReasonId,
                                            Notes: data.Notes,
                                            StockAdjustmentStatusId: data.StockAdjustmentStatusId,
                                            CreatedDate: data.CreatedDate,
                                            CreatedUserId: data.CreatedUserId,
                                            StatusCode: data.StatusCode,
                                            CancelReasonId: data.Batal,
                                            CancelReasonDesc: data.Catatan
                                          }]);
      },

      getLastApproval: function(DataId) {
        console.log('DataId', DataId);
        var res=$http.get('/api/as/StockAdjustment/LastApproval', {params: {
                                                          DataId : DataId,
                                                        } });
        console.log('hasil=>',res);
        return res;
      },
      getNeedApprKabeng: function(DataId) {
        console.log('DataId', DataId);
        var res=$http.get('/api/as/StockAdjustment/NeedApprKabeng', {params: {
                                                          DataId : DataId,
                                                        } });
        console.log('hasil=>',res); 
        return res;
      },
      actApprSA: function (ProcessId, DataId, ApprovalStatus) { 
        //console.log("Data approval : ", data);
        var res = $http.get('/api/as/PartGlobal/ActApproval', {params: {               
               ProcessId: ProcessId, DataId: DataId, ApprovalStatus: ApprovalStatus
               }
        });
        return res;
      },

      setStockAdjustmentHeader : function(data){
        StockAdjustmentHeader = data;
      },
      getStockAdjustmentHeader : function(){
        return StockAdjustmentHeader;
      },            
      setStockAdjustmentDetail : function(data){
        StockAdjustmentDetail = data;
      },
      getStockAdjustmentDetail : function(){
        return StockAdjustmentDetail;
      },
      sendNotif: function(data, recepient, Param) {
        console.log("recepient", recepient);
        return $http.post('/api/as/SendNotificationForRole', [{
            Message: "Request Approval untuk Pengajuan Stock Adjutment" + 
                "<br>" + data.StockAdjustmentNo,
            RecepientId: recepient,
            Param: Param
        }]);
      },
      sendNotifADH: function(data, recepient, Param) {
        return $http.post('/api/as/SendNotificationForRole', [{
            Message: "Request Approval untuk Pengajuan Stock Adjutment" + 
                "<br>" + data.StockAdjustmentNo,
            RecepientId: recepient,
            Param: Param
        }]);
      },
      sendNotifACC: function(data, recepient, Param) {
        return $http.post('/api/as/SendNotificationForRole', [{
            Message: "Hasil Pengajuan Approval Stock Adjutment Anda Disetujui" + 
                "<br>" + data.StockAdjustmentNo,
            RecepientId: recepient,
            Param: Param
        }]);
      },  
      sendNotifReject: function(data, recepient, Param) {
        return $http.post('/api/as/SendNotificationForRole', [{
            Message: "Hasil Pengajuan Approval Stock Adjutment Anda Ditolak" + 
                "<br>" + data.StockAdjustmentNo,
            RecepientId: recepient,
            Param: Param
        }]);
      },
      // setAlasan : function(data){
      //   GabAlasan = data;
      // },
      // getAlasan : function(){
      //   return GabAlasan;
      // }

      getCurrentUser: function() {
        console.log("[getCurrentUser]");
        var res=$http.get('/api/as/PartsUser');
        console.log('hasil=>',res);
        return res;
      },


    }
  });
