angular.module('app')
  .controller('StockAdjustmentController', function($scope, $http, CurrentUser, bsAlert,StockAdjustment, PartsCurrentUser, PartsGlobal, ngDialog, bsNotify, LocalService, $timeout, $window, $filter, PrintRpt) {
    //Options Switch
    //$scope.items = ['BAG', 'Stock Opname'];
    $scope.items = ['BAG'];
    $scope.selection = $scope.items[0];
    //$scope.disableSelect = false;
    $scope.actionVisible = false;
    $scope.mySelections = [];
    $scope.ref = '';
    $scope.isOverlayForm = false;
    $scope.ngDialog = ngDialog;
    $scope.user = CurrentUser.user();
    $scope.mData = {}; //Model
    $scope.mData.selected = [];
    $scope.gridBAG = {};
    $scope.mData.GridDetail = {};
    $scope.xRole = {
      selected: []
    };
    $scope.formApi = {};
    $scope.gridHideActionColumn = true;
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
      $scope.loading = false;

      $timeout(function(){
        var parts = $scope.checkRights(10);
        var bahan = $scope.checkRights(11);
        console.log("parts = ", parts, bahan);
        console.log('user=',$scope.user);
        console.log('$scope.user.OrgId =',$scope.user.OrgId);
        $scope.mData.OutletId = $scope.user.OrgId
        console.log('$scope.mData.OutletId =',$scope.mData.OutletId);
      });
    });
    //----------------------------------
    // Initialization
    //----------------------------------

    var dateFormat='dd/MM/yyyy';
    $scope.SdateOptions = {
      startingDay: 1,
      format: dateFormat,
    };
    $scope.EdateOptions = {
        startingDay: 1,
        format: dateFormat,
    };

     //===============validasi date start and date end===========
     var today = new Date();
     $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(), today.getDate());
     $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());
     $scope.minDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());


     $scope.changeStartDate = function(tgl){
         var today = new Date();
         var dayOne = new Date($scope.mData.startDate);
         var dayTwo = new Date($scope.mData.endDate);

         $scope.EdateOptions.minDate = new Date(dayOne.getFullYear(),dayOne.getMonth(), dayOne.getDate());
         if (tgl == null || tgl == undefined){
             $scope.mData.endDate = null;
         } else {
             if ($scope.mData.startDate < $scope.mData.endDate){

             } else {
                 if (dayOne > today){
                     $scope.mData.startDate = today;
                     $scope.mData.endDate = $scope.mData.startDate;
                 } else {
                     $scope.mData.endDate = $scope.mData.startDate;
                 }
             }
         }
     }

     $scope.changeEndDate = function(tgl){
      var today = new Date();
      var dayOne = new Date($scope.mData.startDate);
      var dayTwo = new Date($scope.mData.endDate);

      if (dayTwo > today){
          $scope.EdateOptions.minDate = $scope.mData.startDate;
          $scope.mData.endDate = today;
      }
  }

     //=============== End of validasi date start and date end===========





    $scope.checkRights = function(bit){
        var p=$scope.myRights & Math.pow(2,bit);
        console.log('cek aja', p , Math.pow(2,bit));
        var res= (p==Math.pow(2,bit));
        console.log("myRights => ", $scope.myRights);
        return res;
    }
    $scope.getRightX= function(a, b){
      var i = 0;
      if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
      else if ($scope.checkRights(b)) i= 2;
      else if ($scope.checkRights(a)) i= 1;
      return i;
    }
    $scope.getRightMaterialType= function(){
      var i = $scope.getRightX(8, 10);
      return i;
    }
    $scope.getRightServiceType= function(){
      var i = $scope.getRightX(10, 11);
      return i;
    }

    //model batal
    $scope.Alasan = {};

    $scope.BatalData = StockAdjustment.getStockAdjustmentHeader();
    // $scope.BatalData = StockAdjustment.getAlasan();

    $scope.onRegisterApi = function(gridApi) {
        $scope.myGridApi = gridApi;
      };

    $scope.afterRegisterGridApi = function(gridApi){
        console.log(gridApi);
    }

    $scope.localeDate = function(data) {
      var tmpDate = new Date(data);
      var resDate = new Date(tmpDate.toISOString().replace("Z","-0700")).toISOString(); 
      return resDate;
    }
  
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.gridDataTree=[];
    $scope.getData = function(mData) {

      console.log("mData : ", mData);
      console.log("$scope.mData.endDate",$scope.mData.endDate);
      console.log("$scope.user.RoleId=>",$scope.user.RoleId);
      //$scope.mData.MaterialTypeId = $scope.getRightMaterialType();
      //$scope.mData.ServiceTypeId = $scope.getRightServiceType();
      // console.log("MaterialTypeId : ", $scope.mData.MaterialTypeId);
      // console.log("ServiceTypeId : ", $scope.mData.ServiceTypeId);
      // console.log("OutletId : ", $scope.mData.OutletId);
      //console.log("OrgId : ", $scope.user.OrgId);

      //login 41 -> RoleId: 1135 -> GR
      //login 25 -> RoleId: 1122 -> GR
      //login 26 -> RoleId: 1123 -> BP
      //login 27 -> RoleId: 1125 -> bahan
      if ($scope.user.RoleId == 1135) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        console.log('1135', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);

      } else if ($scope.user.RoleId == 1122) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        console.log('1122', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);

      } else if ($scope.user.RoleId == 1123) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 0;
        console.log('1123', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);

      } else if ($scope.user.RoleId == 1125) {
        $scope.mData.MaterialTypeId = 2;
        $scope.mData.ServiceTypeId = 1;
        console.log('1125', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);

      } else if ($scope.user.RoleId == 1124) {
        $scope.mData.MaterialTypeId = 2;
        $scope.mData.ServiceTypeId = 0;
        console.log('1124', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);

      } else if ($scope.user.RoleId == 1128) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        console.log('1124', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);

      } else if ($scope.user.RoleId == 1129) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        console.log('1124', $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId);

      }
        // $scope.FunctionGetData(mData);

      console.log("$scope.mData==>>",$scope.mData);
      PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.mData.OutletId).then(function(res) {
          var gridData = res.data;
          //console.log('gridData = ', gridData);
          //console.log('getWarehouseId gridData[0] = ', gridData[0]);
          if(gridData[0] === undefined){
            console.log('*Warehouse data is not found');
          } else {
            $scope.mData.WarehouseId = gridData[0].WarehouseId;
            $scope.mData.OutletId = gridData[0].OutletId;
          }
        },
        function(err) {
          console.log("err=>", err);
        }
      );
      // console.log("mData fRefSATypeId = ", $scope.mData.fRefSATypeId);
      // console.log("mData fStockAdjustmentStatusId = ", $scope.mData.fStockAdjustmentStatusId);
      if(($scope.mData.fRefSATypeId === undefined &&
        $scope.mData.fStockAdjustmentStatusId === undefined &&
        $scope.mData.startDate === undefined &&
        $scope.mData.endDate === undefined &&
        $scope.mData.fStockAdjustmentNo === undefined &&
        $scope.mData.fStockOpnameNo === undefined &&
        $scope.mData.fWarehouseName === undefined)
        ||
        ($scope.mData.fRefSATypeId == null &&
          $scope.mData.fStockAdjustmentStatusId == null &&
          $scope.mData.startDate == null &&
          $scope.mData.endDate == null &&
          $scope.mData.fStockAdjustmentNo == null &&
          $scope.mData.fStockOpnameNo == null &&
          $scope.mData.fWarehouseName == null)
        ||
        (
          ($scope.mData.fRefSATypeId == null || $scope.mData.fRefSATypeId === undefined) &&
          ($scope.mData.fStockAdjustmentStatusId == null || $scope.mData.fStockAdjustmentStatusId === undefined) &&
          ($scope.mData.startDate == null || $scope.mData.startDate === undefined) &&
          ($scope.mData.endDate == null || $scope.mData.endDate === undefined) &&
          $scope.mData.fStockAdjustmentNo == "" &&
          $scope.mData.fStockOpnameNo == "" &&
          $scope.mData.fWarehouseName == "")
      ){
        bsNotify.show(
          {
              title: "Mandatory",
              content: "Isi salah satu filter terlebih dahulu",
              type: 'danger'
          }
        );
      } else
      // Tipe & Status
      if(($scope.mData.fRefSATypeId === undefined &&
          $scope.mData.fStockAdjustmentStatusId === undefined)
          ||
          ($scope.mData.fRefSATypeId == null &&
          $scope.mData.fStockAdjustmentStatusId == null)
          &&
          (($scope.mData.fStockAdjustmentNo != null || $scope.mData.fStockAdjustmentNo !== undefined) &&
          ($scope.mData.fStockOpnameNo != null || $scope.mData.fStockOpnameNo !== undefined) &&
          ($scope.mData.fWarehouseName != null || $scope.mData.fWarehouseName !== undefined))
          &&
          (($scope.mData.startDate == null || $scope.mData.startDate === undefined) &&
          ($scope.mData.endDate == null || $scope.mData.endDate === undefined))
        )
      {
            var tempDate1 = new Date($scope.mData.startDate);
            var tempDate2 = new Date($scope.mData.endDate);
            var monthCount = 0;
            while((tempDate1.getMonth()+''+tempDate1.getFullYear()) != (tempDate2.getMonth()+''+tempDate2.getFullYear())) {
              monthCount++;
              tempDate1.setMonth(tempDate1.getMonth()+1);
            }
            console.log('hasil bulan', monthCount);
            if (monthCount >= 6){
              bsNotify.show(
                {
                    title: "Mandatory",
                    content: "Maksimal Range Tanggal Adjustment 6 bulan",
                    type: 'danger'
                }
              );
            } else {
              //alert('ok');
              console.log("filter is non mandatory");
              StockAdjustment.getData(mData).then(function(res) { // start getdata
                  var gridData = res.data.Result;
                  console.log("GetData gridData ==>",gridData);
                  for (var i in gridData) {
                    gridData[i].CreatedDate = $scope.localeDate(gridData[i].CreatedDate);
                  }

                  // ---------- convert data id / tanggal jadi string --------------- start
                  for (var i in gridData) {
                    gridData[i].xDocDate = $filter('date')(gridData[i].DocDate, 'yyyy-MM-dd');

                    if (gridData[i].StockAdjustmentStatusId == 1) {
                      gridData[i].xStockAdjustmentStatusId = 'Request Approval'
                    } else if (gridData[i].StockAdjustmentStatusId == 2) {
                      gridData[i].xStockAdjustmentStatusId = 'Completed'
                    } else if (gridData[i].StockAdjustmentStatusId == 3) {
                      gridData[i].xStockAdjustmentStatusId = 'Canceled'
                    } else if (gridData[i].StockAdjustmentStatusId == 13) {
                      gridData[i].xStockAdjustmentStatusId = 'Rejected'
                    }
                  }
                  // ---------- convert data id / tanggal jadi string --------------- end

                  $scope.grid.data = gridData;
                  $scope.loading = false;
                },
                function(err) {
                  console.log("err=>", err);
                }
              ); // end getdata
              $scope.loading = false;
          }

      } else {
        if($scope.mData.startDate == null || $scope.mData.startDate === undefined || $scope.mData.startDate == ""){
          bsNotify.show(
              {
                  title: "Mandatory",
                  content: "Tanggal Adjustment belum diisi",
                  type: 'danger'
              }
            );
        } else
        if($scope.mData.endDate == null || $scope.mData.endDate === undefined || $scope.mData.endDate == ""){
          bsNotify.show(
              {
                  title: "Mandatory",
                  content: "Tanggal Adjustment (end date) belum diisi",
                  type: 'danger'
              }
            );
        // }
        // if(
        //   ($scope.mData.startDate != null || $scope.mData.startDate !== undefined || $scope.mData.startDate != "") &&
        //   ($scope.mData.endDate == null || $scope.mData.endDate === undefined || $scope.mData.endDate == "")
        //   ){
        //   bsNotify.show(
        //       {
        //           title: "Mandatory",
        //           content: "Tanggal Adjustment (end date) belum diisi",
        //           type: 'danger'
        //       }
        //     );
        // }
        // if(($scope.mData.startDate == null || $scope.mData.startDate === undefined || $scope.mData.startDate == "") &&
        //   ($scope.mData.endDate != null || $scope.mData.endDate !== undefined || $scope.mData.endDate != "")){
        //   bsNotify.show(
        //       {
        //           title: "Mandatory",
        //           content: "Tanggal Adjustment (start date) belum diisi",
        //           type: 'danger'
        //       }
        //     );
        } else {
          StockAdjustment.getData(mData).then(function(res) { // start getdata
            var tempDate1 = new Date($scope.mData.startDate);
            var tempDate2 = new Date($scope.mData.endDate);
            var monthCount = 0;
            while((tempDate1.getMonth()+''+tempDate1.getFullYear()) != (tempDate2.getMonth()+''+tempDate2.getFullYear())) {
              monthCount++;
              tempDate1.setMonth(tempDate1.getMonth()+1);
            }
            console.log('hasil bulan', monthCount);
            if (monthCount >= 6){
              bsNotify.show(
                {
                    title: "Mandatory",
                    content: "Maksimal Range Tanggal Adjustment 6 bulan",
                    type: 'danger'
                }
              );
            } else {
              var gridData = res.data.Result;
              console.log("GetData gridData ==>",gridData);
              for (var i in gridData) {
                gridData[i].CreatedDate = $scope.localeDate(gridData[i].CreatedDate);
              }

              // ---------- convert data id / tanggal jadi string --------------- start
              for (var i in gridData) {
                gridData[i].xDocDate = $filter('date')(gridData[i].DocDate, 'yyyy-MM-dd');

                if (gridData[i].StockAdjustmentStatusId == 1) {
                  gridData[i].xStockAdjustmentStatusId = 'Request Approval'
                } else if (gridData[i].StockAdjustmentStatusId == 2) {
                  gridData[i].xStockAdjustmentStatusId = 'Completed'
                } else if (gridData[i].StockAdjustmentStatusId == 3) {
                  gridData[i].xStockAdjustmentStatusId = 'Canceled'
                } else if (gridData[i].StockAdjustmentStatusId == 13) {
                  gridData[i].xStockAdjustmentStatusId = 'Rejected'
                }
              }
              // ---------- convert data id / tanggal jadi string --------------- end
              
              console.log("GetData gridData bawah ==>",gridData);
              $scope.grid.data = gridData;
              $scope.loading = false;
            }
            },
            function(err) {
              console.log("err=>", err);
            }
          ); // end getdata
          $scope.loading = false;
        }
      }

      // StockAdjustment.getData(mData).then(function(res) { // start getdata
      //     var gridData = res.data.Result;
      //     //console.log(gridData);
      //     $scope.grid.data = gridData;
      //     $scope.loading = false;
      //   },
      //   function(err) {
      //     console.log("err=>", err);
      //   }
      // ); // end getdata
      // $scope.loading = false;


    } // end getdata all

    $scope.ApproveData = StockAdjustment.getStockAdjustmentHeader();
    $scope.ApproveDataDetail = StockAdjustment.getStockAdjustmentDetail();
    $scope.RejectData = StockAdjustment.getStockAdjustmentHeader();
    $scope.RejectDataDetail = StockAdjustment.getStockAdjustmentDetail();

    // Hapus Filter
    $scope.onDeleteFilter = function(){
      //Pengkondisian supaya seolaholah kosong
      $scope.mData.fRefSATypeId = null;
      $scope.mData.fStockAdjustmentStatusId = null;
      $scope.mData.fStockAdjustmentNo = null;
      $scope.mData.fStockOpnameNo = null;
      $scope.mData.fWarehouseName = null;
      $scope.mData.startDate = null;
      $scope.mData.endDate = null;

    }

    $scope.goBack = function()
    {
      $scope.isOverlayForm = false;
      $scope.formApi.setMode("grid");
      $scope.gridMode = true;
      console.log('goBack = ', $scope.formApi);
    }

    // Generate Nomor Dokumen
    $scope.GenerateDocNum = function(){
      console.log("$scope.mData.ServiceTypeId = ", $scope.mData.ServiceTypeId);
      PartsCurrentUser.getFormatId($scope.mData.ServiceTypeId, 'SA').then(function(res){
        var result = res.data;
        //console.log("FormatId result = ", result);
        console.log("FormatId = ", result[0].Results);

        StockAdjustment.getDocumentNumber(result[0].Results).then(function(res) {
            var DocNo = res.data;
            console.log('res.data',res.data);
            if(typeof DocNo === 'undefined' || DocNo == null)
            {
              bsNotify.show(
              {
                  title: "Stock Adjustment",
                  content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                  type: 'danger'
              }
              );
            }
            else
            {
              $scope.mData.StockAdjustmentNo = DocNo[0];
              $scope.mData.DocDate = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss');
              console.log("Generating DocNo & DocDate ->");
              console.log($scope.mData.StockAdjustmentNo);
              console.log($scope.mData.DocDate);
            }
          },
          function(err) {
            console.log("err=>", err);
          }
        );
      },
      function(err){
        console.log("err=>", err);
      }
      );
      // [END] Get Current User Warehouse
    }

    // cetak - HTML (sudah tidak terpakai)
    $scope.CetakLaporan = function(){
        var table = document.getElementById('laporan').innerHTML;
        var myWindow = $window.open('', '', 'width=1200, height=720');
        myWindow.document.write(table);
        myWindow.print();
        $scope.ngDialog.close();
    };

    // cetak - SSRS
    $scope.printSABAGbulk = function(param) {
        console.log('StockAdjustmentId =', param);
        //var data = $scope.mData;
        $scope.printSABeritaAcaraGudangBulk = 'as/SABeritaAcaraGudang/' + param[0].StockAdjustmentId;
        $scope.cetakan($scope.printSABeritaAcaraGudangBulk);
    };

    $scope.printSABAG = function(StockAdjustmentId, OutletId) {
        console.log('StockAdjustmentId =', StockAdjustmentId);
        //var data = $scope.mData;
        $scope.printSABeritaAcaraGudang = 'as/SABeritaAcaraGudang/' + StockAdjustmentId + '/' + OutletId;
        $scope.cetakan($scope.printSABeritaAcaraGudang);
    };

    $scope.cetakan = function(data) {
        var pdfFile = null;

        PrintRpt.print(data).success(function(res) {
            var file = new Blob([res], { type: 'application/pdf' });
            var fileURL = URL.createObjectURL(file);

            console.log("pdf", fileURL);
            //$scope.content = $sce.trustAsResourceUrl(fileURL);
            pdfFile = fileURL;

          if (pdfFile != null) {
            //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame
            var ua = navigator.userAgent;
            if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
              var link = document.createElement('a');
              link.href = fileURL;
              //link.download="erlangga_file.pdf";
              link.click();
            }
            else {
              printJS(pdfFile);
            }
          }
          else
            console.log("error cetakan", pdfFile);
        }).error(function(res) {
            console.log("error cetakan", pdfFile);
        });
    };

    $scope.onSelectRows = function(selected) {
        console.log("onSelectRows=>", selected);
      }


    // ======== tombol  simpan =========
    $scope.simpan = function () {
    	var errorCount = 0;
    	if ($scope.mData.StockAdjustmentReasonId == 'undefined' || $scope.mData.StockAdjustmentReasonId == null || $scope.mData.StockAdjustmentReasonId == 0) {
    		bsNotify.show({
    			title: "Mandatory",
    			content: "Alasan belum dipilih",
    			type: 'danger'
    		});
    		errorCount += 1;
    	}
    	if ($scope.mData.LocationId == 'undefined' || $scope.mData.LocationId == null) {
    		bsNotify.show({
    			title: "Mandatory",
    			content: "Lokasi Gudang belum dipilih",
    			type: 'danger'
    		});
    		errorCount += 1;
    	}
    	if ($scope.gridOptions.data.length == 0) {
    		bsNotify.show({
    			title: "Mandatory",
    			content: "Belum ada Material yang ditambahkan",
    			type: 'danger'
    		});
    		errorCount += 1;
      }
      for (var i = 0; i < $scope.gridOptions.data.length; i++) {

        if (($scope.gridOptions.data[i].QtyFree <= 0 || $scope.gridOptions.data[i].QtyFree == null || $scope.gridOptions.data[i].QtyFree == undefined) && $scope.mData.StockAdjustmentReasonId == '1' ) {
          bsNotify.show({
            title: "Mandatory",
            content: "Material Tidak Tersedia",
            type: 'danger'
          });
          errorCount += 1;
        }
        if ($scope.gridOptions.data[i].QtyAdjustment <= 0 || $scope.gridOptions.data[i].QtyAdjustment == null || $scope.gridOptions.data[i].QtyAdjustment == undefined) {
          bsNotify.show({
            title: "Mandatory",
            content: "Qty Material Tidak boleh kosong",
            type: 'danger'
          });
          errorCount += 1;
        }
      };

     console.log("Apa hasilnya?", $scope.gridOptions.data);
    	if (errorCount == 0) {
    		$scope.mData.GridDetail = $scope.gridOptions.data;
    		StockAdjustment.setStockAdjustmentHeader($scope.mData);
    		StockAdjustment.setStockAdjustmentDetail($scope.mData.GridDetail);

    		ngDialog.openConfirm({
    			template: '<div ng-include=\"\'app/parts/stockadjustment/referensi/template_simpan.html\'\"></div>',
    			plain: true,
    			controller: 'StockAdjustmentController',
    		});
    	} //end else
    };
    // ======== end tombol  simpan =========

    // ======== tombol  simpan Approval=========
    $scope.SimpanApproval = function(data)
    {
      console.log("Data untuk Approval ", data);

      $scope.mData = StockAdjustment.getStockAdjustmentHeader();
      $scope.mData.GridDetail = StockAdjustment.getStockAdjustmentDetail();

      console.log("mData CancelReasonDesc => ", $scope.mData.CancelReasonDesc);
      console.log("mData => ", $scope.mData);
      console.log("mData Detail=> ", $scope.mData.GridDetail);

      for(var i = 0; i < $scope.mData.GridDetail.length; i++)
      {
        $scope.mData.GridDetail[i].StockAdjustmentStatusId = 1; //for approval
        $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
        $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;
        if ( $scope.mData.GridDetail[i].PartId == null || $scope.mData.GridDetail[i].PartId == undefined || $scope.mData.GridDetail[i].PartId == 0)
        {
          $scope.mData.GridDetail[i].PartId = $scope.mData.GridDetail[i].PartsId
        }
        else if ( $scope.mData.GridDetail[i].PartId  != null ||  $scope.mData.GridDetail[i].PartId != undefined ||  $scope.mData.GridDetail[i].PartId != 0){

        }
      }

      if (data.WarehouseId==1016 || data.WarehouseId==1018){
        StockAdjustment.sendNotif(data, 1128, 49).then(
          function(res) {
            console.log('terkirim ke KABENG GR')
          });
      }
      else if (data.WarehouseId==1017 || data.WarehouseId==1019){
        StockAdjustment.sendNotif(data, 1129, 49).then(
        function(res) {
          console.log('terkirim ke KABENG BP')
        });
      }

      StockAdjustment.create($scope.mData, $scope.mData.GridDetail).then(function(res) {
            var create = res.data;
            console.log('res.data = ', res.data);


            bsNotify.show(
              {
                  title: "Stock Adjustment",
                  content: "Data berhasil disimpan",
                  type: 'success'
              }
            );
          },
          function(err) {
            console.log("err=>", err);
          }
        );

      $scope.ngDialog.close();
      StockAdjustment.formApi.setMode("grid");

    }
    // ======== end tombol simpan Approval=========

    // ======== tombol batal - parts claim - alasan =========
    $scope.batalSA = function(Data){
      // $scope.BatalData = StockAdjustment.getStockAdjustmentHeader();
      console.log('Data => ', Data);
      console.log('mData => ', $scope.mData);
      console.log("$scope.gridBAGView.data => ", $scope.gridBAGView.data);
      console.log("$scope.mData.GridDetail=>",$scope.mData.GridDetail);
      StockAdjustment.setStockAdjustmentDetail($scope.mData.GridDetail);
      if(Data == 2){
        bsNotify.show(
          {
              title: "Tidak Bisa Dibatalkan",
              content: "Status sudah 'Complete'",
              size: "big",
              type: "info"
          }
        );
      } else {
        ngDialog.openConfirm ({
          template:'<div ng-include=\"\'app/parts/stockadjustment/referensi/template_batal.html\'\"></div>',
          plain: true,
          controller: 'StockAdjustmentController',
        });
      } // end else
    };

    // dialog konfirmasi
    $scope.okBatalSA = function(data){
      console.log('okBatalSA');
      console.log("$scope.gridBAGView.data => ", $scope.gridBAGView.data);
      console.log("$scope.mData.GridDetail=>",$scope.mData.GridDetail);
      if(data.Batal == null || data.Batal === undefined){
        bsNotify.show(
          {
              title: "Mandatory",
              content: "Alasan harus diisi",
              size: "big",
              type: "danger"
          }
        );
      } else {
      $scope.BatalData = StockAdjustment.getStockAdjustmentHeader();

      // $scope.BatalData.Notes = 'Alasan: ' + data.Batal + ', Catatan: ' + data.Catatan;
      $scope.BatalData.CancelReasonId = data.Batal;
      $scope.BatalData.CancelReasonDesc = $scope.ToNameCancelReason(data.Batal);

      console.log($scope.BatalData);

      StockAdjustment.setStockAdjustmentHeader($scope.BatalData);

      ngDialog.openConfirm ({
        template:'\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Anda yakin akan membatalkan Stock Adjustment ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="SetujuBatal(BatalData)">Ya</button>\
                     </div>\
                     </div>',
        plain: true,
        controller: 'StockAdjustmentController',
       });
      } // end else
    };

    $scope.SetujuBatal = function(data){
      console.log("SetujuBatal");
      $scope.mData = data;

      $scope.ngDialog.close();
      $scope.mData.StockAdjustmentStatusId = 3; //Cancelled

      console.log($scope.mData);
      // [START] Get Search Appointment
      StockAdjustment.cancel($scope.mData).then(function(res) {
          console.log(res);
          //alert("StockAdjustment berhasil dibatalkan");
          bsNotify.show(
              {
                  title: "Stock Adjustment",
                  content: "Dokumen telah dibatalkan",
                  type: 'success'
              }
            );
          //$scope.goBack();
        },
        function(err) {
          console.log("err=>", err);
        }
      );
      // [END] Get Search Appointment
      //$scope.ngDialog.close();
      StockAdjustment.formApi.setMode("grid");
    }

    // Report =========================================
    $scope.laporan = function(){
      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/stockadjustment/referensi/template_laporan.html\'\"></div>',
        plain: true,
        // width: 800px,
        controller: 'StockAdjustmentController',
       });
    };

    $scope.mData.WarehouseData = {}; // test
    $scope.ApprovalProcessId = 8110;
    //$scope.mData.selectedWarehouseData = $scope.mData.WarehouseData[0].WarehouseId;
    //console.log('$scope.selectedWarehouseData = ', $scope.selectedWarehouseData);

    $scope.onBulkApproveSA= function(data) {
      console.log("data : ", data);
      var mData = {};
      mData = data[0];
      var DataId = mData.StockAdjustmentId;
      StockAdjustment.getNeedApprKabeng(DataId).then(function(res) {
          console.log('res getNeedApprKabeng = ', res);
          var ApprovalStatus = res.data.Result["0"].ApprovalStatus;
          var NeedApprovedKabeng = res.data.Result["0"].NeedApprovedKabeng;

          if (mData.StockAdjustmentStatusId == 2) {
            bsNotify.show(
              {
                  title: "Stock Adjustment - Approved",
                  content: "Stock adjustment sudah disetujui",
                  type: 'danger'
              });
            return;
            // bsAlert.warning("Stock Adjustment Sudah di Setujui", "");
          } else if (mData.StockAdjustmentStatusId == 3) {
            bsNotify.show(
              {
                  title: "Stock Adjustment - Canceled",
                  content: "Stock adjustment sudah dibatalkan",
                  type: 'danger'
              });
            return;
            // bsAlert.warning("Stock Adjustment Sudah di Batalkan", "");
          } else if (mData.StockAdjustmentStatusId == 13) {
            bsNotify.show(
              {
                  title: "Stock Adjustment - Rejected",
                  content: "Stock adjustment sudah ditolak",
                  type: 'danger'
              });
            return;
            // bsAlert.warning("Stock Adjustment Sudah di Tolak", "");
          } else if (NeedApprovedKabeng == 1) {
            bsNotify.show(
              {
                  title: "Need Approval",
                  content: "Need Approve Kepala Bengkel",
                  type: 'warning'
              }
            );
          } else if (ApprovalStatus == 1) {
            bsNotify.show(
              {
                  title: "Approved",
                  content: "Sudah di-Approve",
                  type: 'warning'
              }
            );
          } else if (mData.StockAdjustmentStatusId == 1) {
            $scope.ApproveSA(data);
          } 
        },
        function(err) {
          console.log("err=>", err);
        }
      );
    }

    // approve
    $scope.ApproveSA = function(data){
      console.log('ApproveDS data = ', data);
      $scope.ApproveData = data[0];
      console.log('$scope.ApproveData = ', $scope.ApproveData);
      StockAdjustment.setStockAdjustmentHeader($scope.ApproveData);
      ngDialog.openConfirm ({
        template:'\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Approve dokumen Stock Adjustment ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinApprove(ApproveData)">Ya</button>\
                     </div>\
                     </div>',
        plain: true,
        controller: 'StockAdjustmentController',
       });
       console.log('$scope.ApproveData bawah = ', $scope.ApproveData);
    };

    $scope.yakinApprove = function(data){
      console.log('yakinApprove => ', data);
      $scope.mData = data;
      console.log('yakinApprove mData = ', $scope.mData);

      // // for approval
      // PartsGlobal.actApproval({
      //   ProcessId: $scope.ApprovalProcessId, DataId: $scope.mData.StockAdjustmentId, ApprovalStatus: 1
      // })
      var APId = $scope.ApprovalProcessId;
      var SAId = $scope.mData.StockAdjustmentId;

      if ($scope.user.RoleId==1128 || $scope.user.RoleId==1129) {
        StockAdjustment.sendNotifADH(data, 1021, 49).then(
          function(res) {
            console.log('terkirim ke ADH')
        });
      }
      else if ($scope.user.RoleId==1021 && data.WarehouseId==1016) {
        StockAdjustment.sendNotifACC(data, 1122, 49).then(
          function(res) {
            console.log('terkirim ke Partsman GR')
        });
      }
      else if ($scope.user.RoleId==1021 && data.WarehouseId==1017) {
        StockAdjustment.sendNotifACC(data, 1123, 49).then(
          function(res) {
            console.log('terkirim ke Partsman BP')
        });
      }
      else if ($scope.user.RoleId==1021 && data.WarehouseId==1018) {
        StockAdjustment.sendNotifACC(data, 1125, 49).then(
          function(res) {
            console.log('terkirim ke Gudang GR')
        });
      }
      else if ($scope.user.RoleId==1021 && data.WarehouseId==1019) {
        StockAdjustment.sendNotifACC(data, 1124, 49).then(
          function(res) {
            console.log('terkirim ke Gudang BP')
        });
      }

      StockAdjustment.actApprSA(APId, SAId, 1).then(function(res) {
          console.log('res PartsGlobal.actApproval = ', res);

          var DataId = $scope.mData.StockAdjustmentId;
          StockAdjustment.getLastApproval(DataId).then(function(res) {
              console.log('res getLastApproval = ', res);
              var WFSeq = res.data.Result["0"].WorkFlowSequence;
              var IsApproved = res.data.Result["0"].IsApproved;
              console.log('WFSeq ',WFSeq,'IsApproved ',IsApproved);

              if(WFSeq == 2 && IsApproved == 1) {

                // $scope.mData.StockAdjustmentStatusId = 2; //status = Complete
                // [START] PUT
                // StockAdjustment.cancel($scope.mData).then(function(res) {
                //     console.log('res cancel = ', res);
                //     bsNotify.show(
                //         {
                //             title: "Approved",
                //             content: "Stock Adjustment has been successfully approved.",
                //             type: 'succes'
                //         }
                //       );
                //   },
                //   function(err) {
                //     console.log("err=>", err);
                //   }
                // );

                bsNotify.show(
                  {
                      title: "Approved",
                      content: "Stock Adjustment has been successfully approved.",
                      type: 'succes'
                  }
                );

              } else {
                  console.log('Not Last Approval');

                  //ditambahkan
                  // $scope.mData.StockAdjustmentStatusId = 1; //status = Complete

                  // StockAdjustment.cancel($scope.mData).then(function(res) {
                  // console.log('res cancel = ', res);
                  //     bsNotify.show(
                  //       {
                  //           title: "Approved",
                  //           content: "Stock Adjusment has been successfully approved. Next approval to ADH",
                  //           type: 'succes'
                  //       }
                  //     );
                  // },
                  // function(err) {
                  //   console.log("err=>", err);
                  // });

                  bsNotify.show(
                    {
                        title: "Approved",
                        content: "Stock Adjusment has been successfully approved. Next approval to ADH",
                        type: 'succes'
                    }
                  );
              
              }

            },
            function(err) {
              console.log("err=>", err);
            }
          );

        },
        function(err) {
          console.log("err=>", err);
        }
      );

      $scope.ngDialog.close();
      //PartsTransferOrder.formApi.setMode("grid");
    }

    // Reject
    $scope.onBulkRejectSA= function(mData) {
      console.log("data : ", mData);
      var DataId = mData[0].StockAdjustmentId;
      StockAdjustment.getNeedApprKabeng(DataId).then(function(res) {
          console.log('res getNeedApprKabeng = ', res);
          var ApprovalStatus = res.data.Result["0"].ApprovalStatus;
          var NeedApprovedKabeng = res.data.Result["0"].NeedApprovedKabeng;

          if (mData[0].StockAdjustmentStatusId == 2) {
            bsNotify.show(
              {
                  title: "Stock Adjustment - Approved",
                  content: "Stock adjustment sudah disetujui",
                  type: 'danger'
              });
            return;
            // bsAlert.warning("Stock Adjustment Sudah di Setujui", "");
          } else if (mData[0].StockAdjustmentStatusId == 3) {
            bsNotify.show(
              {
                  title: "Stock Adjustment - Canceled",
                  content: "Stock adjustment sudah dibatalkan",
                  type: 'danger'
              });
            return;
            // bsAlert.warning("Stock Adjustment Sudah di Tolak", "");
          } else if (mData[0].StockAdjustmentStatusId == 13) {
            bsNotify.show(
              {
                  title: "Stock Adjustment - Rejected",
                  content: "Stock adjustment sudah ditolak",
                  type: 'danger'
              });
            return;
            // bsAlert.warning("Stock Adjustment Sudah di Tolak", "");
          } else if (NeedApprovedKabeng == 1) {
            bsNotify.show(
              {
                  title: "Need Approval",
                  content: "Need Approve Kepala Bengkel",
                  type: 'warning'
              }
            );
          } else if (ApprovalStatus == 1) {
            bsNotify.show(
              {
                  title: "Approved",
                  content: "Sudah di-Approve",
                  type: 'warning'
              }
            );
          } else if (mData[0].StockAdjustmentStatusId == 1)  {
            $scope.RejectSA(mData);
          }      
        },
        function(err) {
          console.log("err=>", err);
        }
      );
    }

    $scope.RejectSA = function(data){
      console.log('RejectDS data = ', data);
      $scope.RejectData = data[0];
      console.log('$scope.RejectData = ', $scope.RejectData);
      StockAdjustment.setStockAdjustmentHeader($scope.RejectData);
      ngDialog.openConfirm ({
        template:'\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Reject dokumen Stock Adjustment ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinReject(RejectData)">Ya</button>\
                     </div>\
                     </div>',
        plain: true,
        controller: 'StockAdjustmentController',
       });
       console.log('$scope.RejectData bawah = ', $scope.RejectData);
    };

    $scope.yakinReject = function(data){
      console.log('yakinReject => ', data);
      $scope.mData = data;
      console.log('yakinReject mData = ', $scope.mData);

      $scope.mData.StockAdjustmentStatusId = 13; // Rejected

      if ($scope.user.RoleId==1128 && data.WarehouseId==1016) {
        StockAdjustment.sendNotifReject(data, 1122, 49).then(
          function(res) {
            console.log('terkirim ke Partsman GR')
        });
      }
      else if ($scope.user.RoleId==1129 && data.WarehouseId==1017) {
        StockAdjustment.sendNotifReject(data, 1123, 49).then(
          function(res) {
            console.log('terkirim ke Partsman BP')
        });
      }
      else if ($scope.user.RoleId==1128 && data.WarehouseId==1018) {
        StockAdjustment.sendNotifReject(data, 1125, 49).then(
          function(res) {
            console.log('terkirim ke Gudang GR')
        });
      }
      else if ($scope.user.RoleId==1129 && data.WarehouseId==1019) {
        StockAdjustment.sendNotifReject(data, 1124, 49).then(
          function(res) {
            console.log('terkirim ke Gudang BP')
        });
      }

      // for approval
      PartsGlobal.actApproval({
        ProcessId: $scope.ApprovalProcessId, DataId: $scope.mData.StockAdjustmentId, ApprovalStatus: 2
      }).then(function(res) {
        
        // [START] PUT
        // StockAdjustment.cancel($scope.mData).then(function(res) {
        //     console.log('res cancel = ', res);
        //     bsNotify.show(
        //         {
        //             title: "Rejected",
        //             content: "Stock Adjustment has been successfully rejected.",
        //             type: 'succes'
        //         }
        //       );
        //   },
        //   function(err) {
        //     console.log("err=>", err);
        //   }
        // );
  
        bsNotify.show(
          {
            title: "Rejected",
            content: "Stock Adjustment has been successfully rejected.",
            type: 'succes'
          }
        );

        },
        function(err) {
          console.log("err=>", err);
        }
      
      );

      $scope.ngDialog.close();
      //PartsTransferOrder.formApi.setMode("grid");
    }

    $scope.onBeforeNewMode = function(mode){
      $scope.HideSaveButton = true;
      $scope.HideCancelButton = true;
      $scope.isOverlayForm = true;
      $scope.isEditBAG = false;
      StockAdjustment.formApi = $scope.formApi;
      $scope.isEditStockOpname = false;
      $scope.isViewBAG = false;
      $scope.isViewStockOpname = false;
      $scope.selection = null;
      $scope.gridOptions.data = [];
      console.log("onBeforeNewMode =>", mode);
      console.log('$scope.mData.WarehouseData = ', $scope.mData.WarehouseData); //test
      PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res) {
          var Approvers = res.data.Result[0].Approvers;
          console.log("getApprover res = ", Approvers);
          $scope.mData.Approvers = Approvers;
        },
        function(err) {
          console.log("err=>", err);
        }
      );
      // $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
      // $scope.mData.ServiceTypeId = $scope.getRightServiceType();
      console.log("MaterialTypeId : ", $scope.mData.MaterialTypeId);
      console.log("ServiceTypeId : ", $scope.mData.ServiceTypeId);
      console.log("$scope.mData.OutletId : ", $scope.mData.OutletId);
      console.log("orgid => ",$scope.user.OrgId);

      //login 41 -> RoleId: 1135 -> GR
      //login 25 -> RoleId: 1122 -> GR
      //login 26 -> RoleId: 1123 -> BP
      //login 27 -> RoleId: 1125 -> bahan
      if($scope.user.RoleId == 1135) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        // $scope.FunctionGetData(mData);
      } else if ($scope.user.RoleId == 1122) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        // $scope.FunctionGetData(mData);
      } else if ($scope.user.RoleId == 1123) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 0;
      }else if ($scope.user.RoleId == 1125) {
        $scope.mData.MaterialTypeId = 2;
         $scope.mData.ServiceTypeId = 1;
        // $scope.mData.ServiceTypeId = 0;
        //$scope.FunctionGetData2(mData);
      } else if ($scope.user.RoleId == 1124) {
        $scope.mData.MaterialTypeId = 2;
        $scope.mData.ServiceTypeId = 0;
      }
        // $scope.FunctionGetData(mData);
      // } else if ($scope.user.RoleId == 1125) {
      //   $scope.mData.MaterialTypeId = 2;
      //   // $scope.mData.ServiceTypeId = 1;
      //   // $scope.mData.ServiceTypeId = 0;
      //   $scope.FunctionGetData2(mData);
      // }

      PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.user.OrgId).then(function(res) {
            var gridData = res.data;
            console.log('gridData[0] = ', gridData[0]);
            console.log('gridData = ', gridData);

            $scope.mData.WarehouseId = gridData[0].WarehouseId;
            $scope.mData.OutletId = gridData[0].OutletId;
            $scope.mData.WarehouseData = gridData;
            $scope.mData.LocationId = gridData[0].WarehouseId;
            $scope.mData.selectedWarehouseData = $scope.mData.WarehouseData[0].WarehouseId;
            $scope.mData.SelectedWarehouseId = gridData[0].WarehouseId;
            console.log('$scope.mData.WarehouseData = ', $scope.mData.WarehouseData);
            console.log('$scope.mData.WarehouseId = ', $scope.mData.WarehouseId);
            console.log('gridData[0].WarehouseId = ', gridData[0].WarehouseId);
          },
          function(err) {
            console.log("err=>", err);
          }
        );

      $scope.GenerateDocNum();
    }

    $scope.onBeforeEditMode = function(row, mode){
        // console.log("onBeforeEdit=>", row);
        // StockAdjustment.setStockAdjustmentHeader(row);
        // console.log(StockAdjustment.getStockAdjustmentHeader());
    }
    $scope.onBeforeDeleteMode = function(){
        console.log("mode=>",$scope.formmode);
    }
    $scope.onShowDetail = function(row, mode){
      StockAdjustment.formApi = $scope.formApi;
      console.log("Saya harus di onShowDetail -> ", row);
      StockAdjustment.setStockAdjustmentHeader(row);
      if(mode == 'view'){ //jika mode = view
        console.log(mode);
        $scope.isEditBAG = false;
        $scope.isEditStockOpname = false;
        console.log($scope.mData.RefSATypeId);
        if(row.RefSATypeId == 1){ //& data referensiDisposal-nya = 'BAG'
          $scope.isViewBAG = true;
          $scope.isViewStockOpname = false;

          StockAdjustment.getDetail(row.StockAdjustmentId).then(function(res) {
              var gridData = res.data.Result;
              console.log('StockAdjustment.getDetail = ', gridData);
              $scope.gridBAGView.data = gridData;
              $scope.mData.GridDetail = $scope.gridBAGView.data;
              // switch($scope.mData.RefGITypeID)
              // {
              //   case 1:
              //     $scope.gridStockOpname.data = gridData;
              //     break;
              //   case 2:
              //     $scope.gridSOView.data = gridData;
              //     break;
              // }
            },
            function(err) {
              console.log("err=>", err);
            }
          );

        } else if (row.RefSATypeId == 2){ //& data referensiDisposal-nya = 'StockOpname'
          $scope.isViewBAG = false;
          $scope.isViewStockOpname = true;

          StockAdjustment.getDetail(row.StockAdjustmentId).then(function(res) {
              var gridData = res.data.Result;
              console.log('StockAdjustment.getDetail row.RefSATypeId == 2 ', gridData);
              $scope.gridStockOpname.data = gridData;
              // switch($scope.mData.RefGITypeID)
              // {
              //   case 1:
              //     $scope.gridStockOpname.data = gridData;
              //     break;
              //   case 2:
              //     $scope.gridSOView.data = gridData;
              //     break;
              // }
            },
            function(err) {
              console.log("err=>", err);
            }
          );

        }
      } else
      if(mode == 'edit'){ //jika mode edit

        if(row.StockAdjustmentStatusId == 1){                   // jika status 'REQUEST APPROVAL'
          console.log("Masuk kondisi Stok Adjustment REQUEST APPROVAL");

          console.log('mode edit ?', mode);
          $scope.isViewBAG = false;
          $scope.isViewStockOpname = false;

          //console.log("Get From factory => ");
          console.log("row.StockAdjustmentId ", row.StockAdjustmentId);

          console.log('StockAdjustment.getStockAdjustmentHeader() =', StockAdjustment.getStockAdjustmentHeader());

          if(row.RefSATypeId == '1'){ //& data referensiDisposal-nya = 'Parts Claim'
            $scope.isEditBAG = true;
            $scope.isEditStockOpname = false;
            //getDetail
            StockAdjustment.getDetail(row.StockAdjustmentId).then(function(res) {
                var gridData = res.data.Result;
                console.log('StockAdjustment.getDetail edit', gridData);
                $scope.gridBAGView.data = gridData;
                $scope.mData.GridDetail = angular.copy($scope.gridBAGView.data);
                console.log("$scope.gridBAGView.data", $scope.gridBAGView.data);
                console.log("$scope.mData.GridDetail", $scope.mData.GridDetail);

                for (var i in gridData) {
                  gridData[i].CreatedDate = $scope.localeDate(gridData[i].CreatedDate);
                }
    
                // switch($scope.mData.RefGITypeID)
                // {
                //   case 1:
                //     $scope.gridStockOpname.data = gridData;
                //     break;
                //   case 2:
                //     $scope.gridSOView.data = gridData;
                //     break;
                // }
              },
              function(err) {
                console.log("err=>", err);
              }
            );
          } else if(row.RefSATypeId == '2'){ //& data referensiDisposal-nya = 'StockOpname'
            $scope.isEditBAG = false;
            $scope.isEditStockOpname = false;
            alert("Referensi Stock Opname tidak memiliki mode Edit");

          }
        } else {
          console.log("Masuk kondisi Stok Adjustment COMPLETED, CANCELED");

          bsNotify.show(
            {
                title: "Peringatan",
                content: "Data dengan status 'Completed' dan 'Canceled' tidak dapat diubah",
                type: 'danger'
            }
          );
          StockAdjustment.formApi.setMode("grid");
        }


      } else
      if(mode == 'new'){ // mode New

      }
      $scope.ref = '';
      //log
      console.log("onShowDetail=>", mode);
  }
    //----------------------------------
    // Select Option Referensi GI Setup
    //----------------------------------
    $scope.changedValue = function (item){
      if(item == 'undefined' || item == null){
        bsNotify.show(
          {
              title: "Mandatory",
              content: "Referensi Stock Adjustment belum dipilih",
              type: 'danger'
          }
        );
      } else {
        console.log(item);
        $scope.isOverlayForm = false;
        $scope.ref = item;
        $scope.disableSelect  = true;
      }
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.ToNameRefSA = function(RefSATypeId){
      var GiName;
      switch(RefSATypeId)
      {
        case 1:
        case "1":
          GiName = "BAG";
          break;
        case 2:
        case "2":
          GiName = "Stock Opname";
          break;
      }
      return GiName;
    }

    $scope.ToNameStatus = function(StockAdjustmentStatusId){
      var statusName;
      switch(StockAdjustmentStatusId)
      {
        case 1:
          statusName = "Request Approval";
          break;
        case 2:
          statusName = "Completed";
          break;
        case 3:
          statusName = "Canceled";
          break;
        case 13:
          statusName = "Rejected";
          break;
      }
      return statusName;
    }

    $scope.ToNameMaterial = function(MaterialTypeId){
      var materialName;
      switch(MaterialTypeId)
      {
        case 1:
          materialName = "Parts";
          break;
        case 2:
          materialName = "Bahan";
          break;
      }
      return materialName;
    }

    $scope.ToNameReason = function(StockAdjustmentReasonId){
      var reasonName;
      switch(StockAdjustmentReasonId)
      {
        case 1:
          reasonName = "Barang Hilang";
          break;
        case 2:
          reasonName = "Barang Kelebihan";
          break;
        // case 3:
        //   reasonName = "Lainnya";
        //   break;
      }
      return reasonName;
    }

    $scope.ToNameCancelReason = function(CancelReasonId){
      var reasonName;
      switch(CancelReasonId)
      {
        case 1:
          reasonName = "Kesalahan Administrasi";
          break;
        case 2:
          reasonName = "Kesalahan Pilih Transaksi";
          break;
        case 3:
          reasonName = "Lainnya";
          break;
      }
      return reasonName;
    }

    $scope.ToNameUom = function(UomId){
      var reasonName;
      switch(UomId)
      {
        case 4:
          reasonName = "Pieces";
          break;
        case 1:
          reasonName = "Pieces";
          break;
        case 3:
          reasonName = "Pieces";
          break;
        case 2:
          reasonName = "Pieces";
          break;
      }
      return reasonName;
    }

    $scope.ToDate = function(data)
    {
      var endDate = $filter('date')(data, 'yyyy-MM-dd');
      return endDate
    }

    $scope.IfEmpty = function(data)
    {
      var strip;
      if(data == null || data === undefined){
        // strip = "-"
        strip = "*"
      } else {
        strip = data;
      }
      return strip;
    }

    $scope.gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
      '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Lihat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
      '<a href="#" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Edit" ng-hide="row.entity.StockAdjustmentStatusId > 1 || (row.entity.StockAdjustmentStatusId == 1 && grid.appScope.$parent.user.RoleId != 1122 && grid.appScope.$parent.user.RoleId != 1125)" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
      '</div>';

    $scope.grid = {
      enableSorting: true,
      enableRowSelection: true,
      multiSelect: false,
      enableSelectAll: false,
      //showTreeExpandNoChildren: true,
      // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
      // paginationPageSize: 15,
      columnDefs: [
        // {
        //   name: 'StockAdjusmentId',
        //   field: 'Id',
        //   width: '7%',
        //   visible: false
        // },
        // {
        //   name: 'Referensi Stock',
        //   field: 'RefSATypeId',
        //   width: '15%',
        //   visible: false
        // },
        // {
        //   name: 'OutletId',
        //   field: 'OutletId',
        //   width: '15%',
        //   visible: false
        // },
        // {
        //   name: 'WarehouseId',
        //   field: 'WarehouseId',
        //   width: '15%',
        //   visible: false
        // },
        {
          name: 'Referensi Stock',
          displayName: 'Referensi Stock',
          cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameRefSA(row.entity.RefSATypeId)}}</div>',
          width: '10%'
        },
        {
          name: 'No. Stock Opname',
          diplayName: 'No. Stock Opname',
          cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.user.RoleId}}</div>',
          // field: 'StockOpnameNo',
          // cellTemplate:
          //   '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.IfEmpty(row.entity.StockOpnameNo)}}</div>',
          width: '15%'
        },
        {
          name: 'No. Adjustment',
          displayName: 'No. Adjustment',
          field: 'StockAdjustmentNo',
          width: '15%'
        },
        // {
        //   name: 'Tanggal Adjustment',
        //   field: 'DocDate',
        //   width: '13%',
        //   visible: false
        // },
        {
          name: 'Tanggal Adjustment',
          displayName: 'Tanggal Adjustment',
          field: 'xDocDate',
          // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToDate(row.entity.DocDate)}}</div>',
          width: '12%'
        },
        {
          name: 'Lokasi Gudang',
          displayName: 'Lokasi Gudang',
          field: 'WarehouseName',
          width: '13%'
        },
        {
          name: 'Notes',
          field: 'Notes',
          width: '15%'
        },
        // {
        //   name: 'Status',
        //   field: 'StockAdjustmentStatusId',
        //   width: '15%',
        //   visible: true
        // },
        {
          name: 'Status',
          displayName: 'Status',
          field: 'xStockAdjustmentStatusId',
          // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatus(row.entity.StockAdjustmentStatusId)}}</div>',
          width: '10%'
        },
        {
          name: 'Action',
          width: '8%',
          pinnedRight: true,
          cellTemplate: $scope.gridActionButtonTemplate
        }
      ]
    };

    // $scope.gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
    // '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="View" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
    // '<a href="#" ng-click="grid.appScope.$parent.ifStatusStock(row.entity)" uib-tooltip="Edit" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
    // '</div>';


    //----------------------------------
    // Dummy BAG
    //----------------------------------

    $scope.showMe = function(){
        var result = confirm('Apakah Anda yakin menghapus data ini?');
        if(result){
          console.log($scope.gridApi.selection.getSelectedRows());
          angular.forEach($scope.gridApi.selection.getSelectedRows(), function (data, index) {
            $scope.gridOptions.data.splice($scope.gridOptions.data.lastIndexOf(data), 1);
          });
        }

    };

      $scope.gridBAGView = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableFiltering : true,
        //selectedItems: console.log($scope.mySelections),
        enableSelectAll: true,
        enableCellEdit: false,
        paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 5,

        columnDefs: [
          { name:'No Material', displayName:'No. Material', field: 'PartsCode', width: '15%'},
          { name:'Nama Material', displayName:'Nama Material', field: 'PartsName', width: '30%'},
          { name:'Qty Adjustment', displayName:'Qty Adjustment', field: 'QtyAdjustment', width: '20%', visible: true},
          { name:'Satuan', displayName:'Satuan', field: 'Name', width: '20%', visible: true},
          {
            name:'Satuan1',
            displayName:'Satuan',
            cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameUom(row.entity.UomId)}}</div>',
            width: '20%',
            visible : false
          },
        ]
      };

    //----------------------------------
    // Dummy Grid 'Stock Opname'
    //----------------------------------
   $scope.gridStockOpname = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableFiltering : true,
        //selectedItems: console.log($scope.mySelections),
        enableSelectAll: true,
        paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 5,

        columnDefs: [
          { name:'No Material', displayName:'No. Material', field: 'PartsCode', width: '15%'},
          { name:'Nama Material', displayName:'Nama Material', field: 'PartsName', width: '30%'},
          { name:'Qty Adjustment', displayName:'Qty Adjustment', field: 'QtyAdjustment', width: '20%', visible: true},
          {
            name:'Satuan',
            displayName:'Satuan',
            field: 'Name',
            width: '20%',
            visible: true
          },
          // {
          //   name:'Satuan',
          //   displayName:'Satuan',
          //   cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameUom(row.entity.UomId)}}</div>',
          //   width: '20%'
          // },

        ]
      };
      $scope.enablegrid = true;
      $scope.gridOptions = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableFiltering : true,
        onRegisterApi: function(gridApi) {
          $scope.gridApi = gridApi;
        },
        //selectedItems: console.log($scope.mySelections),
        enableSelectAll: true,
        paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 5,
        // rowHeight: 40
      };

        $scope.Delete = function(row) {
            var index = $scope.gridOptions.data.indexOf(row.entity);
            $scope.gridOptions.data.splice(index, 1);
            console.log($scope.gridOptions.data);
            console.log(index);
        };
        $scope.gridOptions.columnDefs = [
        { name:'PartId', displayName:'PartId', field: 'PartId', visible: false },
        {
          name:'PartsCode',
          displayName:'No. Material',
          enableCellEdit:true,
          field: 'PartsCode',
          width: '20%',
          cellTemplate:'<div><div class="input-group">\
                    <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.PartsCode" required>\
                    <label class="input-group-btn">\
                        <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
                            <i class="fa fa-search fa-1"></i>\
                        </span>\
                    </label>\
                </div></div>'
        },
        { name:'Nama Material', displayName:'Nama Material', field: 'PartsName', width: '30%'},
        { name:'Qty Adjustment', displayName:'Qty Adjustment', field: 'QtyAdjustment', width: '20%',  enableCellEdit:true, visible:'grid.appScope.enablegrid', editableCellTemplate: '<input type="number" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">'},
        { name:'Satuan', displayName:'Satuan', field: 'Name', width: '20%', visible: true},
        {
            name:'Satuan1',
            displayName:'Satuan',
            cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameUom(row.entity.UomId)}}</div>',
            width: '20%',
            visible: false
        }
        ,
        {
          name:'_',
          displayName:'Action',
          enableCellEdit: false,
          // visible : true,
          cellTemplate:'<div style="padding-top: 5px;" class="ui-grid-cell-contents"><u><a href="#" ng-click="grid.appScope.Delete(row)">Hapus</a></u></div>'
          }
        ];

        $scope.gridOptions.data = [];

      $scope.Add = function(){
        var n = $scope.gridOptions.data.length + 1;
        $scope.gridOptions.data.push({
          "NoMaterial": "",
          "PartsName": "",
          "QtyAdjustment": "",
          "UomId": ""
      });
    }

    $scope.SearchMaterial = function(NoMaterial){
      //kesini bill

      //login 41 -> RoleId: 1135 -> GR
      //login 25 -> RoleId: 1122 -> GR
      //login 26 -> RoleId: 1123 -> BP
      //login 27 -> RoleId: 1125 -> bahan
      if($scope.user.RoleId == 1135) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        // $scope.FunctionGetData(mData);
      } else if ($scope.user.RoleId == 1122) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 1;
        // $scope.FunctionGetData(mData);
      } else if ($scope.user.RoleId == 1123) {
        $scope.mData.MaterialTypeId = 1;
        $scope.mData.ServiceTypeId = 0;
      }
        // $scope.FunctionGetData(mData);
      // } else if ($scope.user.RoleId == 1125) {
      //   $scope.mData.MaterialTypeId = 2;
      //   // $scope.mData.ServiceTypeId = 1;
      //   // $scope.mData.ServiceTypeId = 0;
      //   $scope.FunctionGetData2(mData);
      // }


      StockAdjustment.getDetailByMaterialNo(NoMaterial, $scope.mData.MaterialTypeId, $scope.mData.ServiceTypeId, $scope.mData.LocationId).then(function(res) {
          var gridData = res.data.Result;
          console.log("FEEL LIKE HOME",gridData[0]);
          // || gridData[0].StockOpnameStatusId == 4 || gridData[0].StockOpnameStatusId == 1 removed
          if (gridData[0].StockOpnameStatusId == 2 || gridData[0].StockOpnameStatusId == 3){
            // $scope.enablegrid = false;
            $scope.gridOptions.columnDefs[3].enableCellEdit = false;
            bsNotify.show(
              {
                  title: "FREEZE",
                  content: "Parts Sedang digunakan. Harap gunakan parts yang lain.",
                  type: 'denger'
              }
            );
          }
          if (gridData[0].StockPositionId != null) {
            $scope.gridOptions.data[$scope.gridOptions.data.length - 1] = gridData[0];
            $scope.mData.GridDetail = $scope.gridOptions.data;
            console.log('masuk sini yah boys');
          }else {
            bsNotify.show(
              {
                  title: "Stock Adjustment",
                  content: "Parts Tidak terdaftar di gudang. ",
                  type: 'denger'
              }
            );
          }

        },
        function(err) {
          console.log("err=>", err);
          confirm("Nomor Material Tidak Ditemukan.");
        }
      );
    }


  }); // end line
