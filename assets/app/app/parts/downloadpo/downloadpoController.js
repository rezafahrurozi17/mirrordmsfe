angular.module('app')
    .controller('PartsDownloadPOController', function($scope, $http, CurrentUser, PartsDownloadPO, ngDialog, $timeout, $filter, bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading = true;
        console.log("dev_parts 2 Oktober 2017 " , document.lastModified);
        //$scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mData = {}; //Model
    $scope.xData = {};
    $scope.xData.selected=[];

    //---------------------------------- 
    // Get Data
    //----------------------------------
   
    $scope.getData = function(date) {
        if(typeof date === 'undefined' || date == null)
        {
            bsNotify.show(
            {
                title: "Download PO",
                content: "Tanggal harus diisi",
                type: 'danger'
            }
            );
        }
        else
        {
            PartsDownloadPO.getData(date).then(function(res) {
                  var gridData = res.data;
                  console.log(gridData);

                  if(gridData.length < 1)
                  {
                    bsNotify.show(
                        {
                          title: "Download PO",
                          content: "Data tidak ditemukan",
                          type: 'danger'
                        }
                        );
                  }
                  else
                  {
                    var labelDate = $filter('date')(date, 'yyyy-MM-dd');
                    $scope.JSONToCSVConvertor(gridData, labelDate, 'PO DEPO');
                  }

                  
                },
                function(err) {
                  console.log("err=>", err);
                }
              );    
        }
        
    }

    $scope.JSONToCSVConvertor = function(JSONData, ReportTitle, ShowLabel) {
        var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
        
        var CSV = '';    
        
        CSV += ReportTitle + '\r\n\n';

        if (ShowLabel) {
            var row = "";
            
            for (var index in arrData[0]) {
                
                row += index + ',';
            }

            row = row.slice(0, -1);
            
            CSV += row + '\r\n';
        }
        
        
        for (var i = 0; i < arrData.length; i++) {
            var row = "";
            
            
            for (var index in arrData[i]) {
                row += '"' + arrData[i][index] + '",';
            }

            row.slice(0, row.length - 1);
            
            
            CSV += row + '\r\n';
        }

        if (CSV == '') {        
            alert("Invalid data");
            return;
        }   
        
       
        var fileName = "DownloadDepo";
        fileName += ReportTitle.replace(/ /g,"_");   
        
        
        var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
        
        var link = document.createElement("a");    
        link.href = uri;
        
        link.style = "visibility:hidden";
        link.download = fileName + ".txt";
        
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
});
