angular.module('app')
  .factory('PartsDownloadPO', function($http, CurrentUser, $filter) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(date) {
      	var startDate = $filter('date')(date, 'yyyy-MM-dd');
        var res=$http.get('/api/as/PartsUser/DownloadDepo', {params: {
                                                          Date : (startDate==null?"n/a":startDate)
                                                        } });
        return res;
      }
    }
  });