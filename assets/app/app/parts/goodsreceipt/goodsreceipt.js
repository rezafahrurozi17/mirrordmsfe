angular.module('app')
  .constant("C_MATERIAL", {
    "Tipe": ['Parts', 'Bahan'],
    "Status": [{"id":"", "name":"Choose status"}, {"id":"1", "name":"Completed"}, {"id":"2", "name":"Cancelled"}]
  })
  .factory('GoodsReceipt', function($http, CurrentUser, C_MATERIAL, PartsCurrentUser, $filter) {
    var currentUser = CurrentUser.user;
    var GRHeader = {};
    var vUser= {};
    var tipemat=0;
    this.DataList= {};
    this.DataIndex= -1;
    this.formApi = {};
    // var GoodsReceipt = {};
    //return {      test..
    return{
      // getWOUser : function() {
      //   varUser = PartsCurrentUser.getWarehouseData();
      //   return varUser;
      // },
      getWOUser : function () {
      return PartsCurrentUser.getWarehouseData()
        .then(function (response) {
          return response.data;
        });
      },
      getDataNoFilter : function() {
        // var res=$http.get('/api/as/VendorTM'); //GoodsReceipt..
        var res=$http.get('/api/as/GoodsReceipt'); //GoodsReceipt
        console.log('res.=>',res);
        return res;
      },

      getData: function(data) {
        console.log("[Factory]");
        console.log("Get dataa :",data);
        //filter tanggal
        var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
        var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');

        var res=$http.get('/api/as/GoodsReceipt', {params: {grid : (data.GoodsReceiptId==null?"":data.GoodsReceiptId),
                                                            refgr : (data.RefGRTypeId==null?"":data.RefGRTypeId.id),
                                                            grstatusid : (data.GoodsReceiptStatusId==null?"":data.GoodsReceiptStatusId.id),
                                                            shipno : (data.ShippingNo==null?"" :data.ShippingNo),
                                                            outletid : (data.OutletId==null?"":data.OutletId),
                                                            ShipmentQty : (data.ShipmentQty==null?"":data.ShipmentQty),
                                                            tgl1 : (data.startDate==null?"":startDate),
                                                            tgl2 : (data.endDate==null?"":endDate),
                                                            PartsCode : (data.PartsCode==null?"":data.PartsCode)
                                                        } });
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      getDataItem: function(data) {
        console.log("[Factory Item]");
        console.log(data);
        var res=$http.get('/api/as/GoodsReceipt', {params: {grid : (data.GoodsReceiptId==null?"":data.GoodsReceiptId),
                                                            refgr : 0,
                                                            grstatusid : 0,
                                                            shipno : "",
                                                            outletid : 0, tgl1 : "", tgl2 : ""
                                                        } });
        console.log('hasil=>',res);
        return res;
      },
      deleteDataItem: function(data) {
        console.log(data);
        var res=$http.get('/api/as/DeleteGoodsReceiptItem', {params: {itemid : data.GoodsReceiptItemId, itemids : "-"
                                                        } });
        console.log('hasil=>',res);
        return res;
      },
      deleteMultipleItems: function(data) {
        console.log(data);
        var res=$http.get('/api/as/DeleteGoodsReceiptItem', {params: {itemid : "-999", itemids : data.itemids
                                                        } });
        console.log('hasil ... =>',res);
        return res;
      },
      create: function(data,detail) {
        console.log('tambah data=>', data);
        //var vUser={};// = this.getWOUser();// ..
        this.getWOUser().then(function(res) {
          console.log("??? : ", res);
          if (res!= {}) {
           vUser = res.Result[0];
          }
          console.log("warehouse data .. : ", vUser);
          console.log("tipe material ", data.MaterialTypeId);
          switch (data.MaterialTypeId) {
            case C_MATERIAL.Tipe[0] : tipemat =1; break; // Jika Parts
            case C_MATERIAL.Tipe[1] : tipemat =2; break; // Jika Bahan
          }
          console.log("tipemat : ", tipemat);
          return $http.post('/api/as/GoodsReceipt?QtyGr='+detail[0].QtyGR+'&shipping='+data.ShippingNo, [{ // not yet
                                            //GoodsReceiptId: (data.GoodsReceiptId==null?0:data.GoodsReceiptId),
                                            GoodsReceiptNo: '-999',
                                            OutletId: (vUser==null?'99':vUser.OutletId), //1,
                                            WarehouseId: (vUser==null?'99':vUser.WarehouseId),//1,
                                            DocDate: data.DocDate, //"2017-03-22",
                                            MaterialTypeId: tipemat, //1,
                                            RefGRTypeId: data.RefGRTypeId,
                                            VendorId: data.VendorId,
                                            ShippingNo: data.ShippingNo,
                                            ReceiptDate: data.ReceiptDate, //"2017-03-20",
                                            Notes: data.Notes,
                                            GoodsReceiptStatusId: 1
                                          }]);
        });
      },
      createData : function (data, detail){
        switch (data.MaterialType) {
            case C_MATERIAL.Tipe[0] : data.MaterialTypeId =1; break; // Jika Parts
            case C_MATERIAL.Tipe[1] : data.MaterialTypeId =2; break; // Jika Bahan
        }
        detail['WarehouseId'] = data.WarehouseId;
        console.log("data a :", data, detail);
        // data['APart_GoodsReceiptItem_TTs'] = detail;
        data['GridDetail'] = detail;
        var jData = [];
        jData.push(data);
        console.log("data b :", jData);
        return $http.post('/api/as/GoodsReceipt', jData);
        // return $http.put('/api/as/PartMR', [{id: "1", name: "satu"}, {id: "2", name: "Dua"}]);
      },
      createDataTO : function (data, detail){
        switch (data.MaterialType) {
            case C_MATERIAL.Tipe[0] : data.MaterialTypeId =1; break; // Jika Parts
            case C_MATERIAL.Tipe[1] : data.MaterialTypeId =2; break; // Jika Bahan
        }
        detail['WarehouseId'] = data.WarehouseId;
        console.log("data a :", data, detail);
        data['APart_GoodsReceiptItem_TTs'] = detail;
        var jData = [];
        jData.push(data);
        console.log("data b :", jData);
        return $http.post('/api/as/CreateGoodsReceiptTO?QtyGr='+detail[0].QtyGR+'&ToNo='+detail[0].TransferOrderNo, jData);
        // return $http.put('/api/as/PartMR', [{id: "1", name: "satu"}, {id: "2", name: "Dua"}]);
      },
      setGRHeader : function(data){
        for(var k in data) GRHeader[k]=data[k];
        GRHeader['GRstatusName'] = C_MATERIAL.Status[GRHeader.GoodsReceiptStatusId].name;
      },
      getGRHeader : function(){
        return GRHeader;
      },

      callPrint : function(RefGR,NoShipping,Vendor,Date,TypeMateral){
        return $http.get('/api/rpt/AS_GoodReceiptParts/'+RefGR+"/"+NoShipping+"/"+Vendor+"/"+Date+"/"+TypeMateral);
      },
      // getDataPONO: function(param) {
      //   var res=$http.get('/api/as/ClaimInquiry/SearchPODetail/'+param);
      getDataPONO: function(grtype,param) {
        // var res=$http.get('/api/as/ClaimInquiry/SearchPODetail/'+param+'/'+grtype);
        var res=$http.get('/api/as/ClaimInquiry/SearchPODetail/'+grtype, {
          params: {
            PONo: param
          }
        });
        console.log('hasil=>',res);
        return res;
      },
      cekShipping: function(param) {
        var res=$http.get('/api/as/GoodsReceipt/CekShipping?ShipNo='+param);
        // var res=$http.get('',{params: { ShipNo :} })
        console.log('hasil=>',res);
        return res;
      },
      cekStock: function(param) {
        var res=$http.get('/api/as/GoodsReceipt/CekStock?GRId='+param);
        // var res=$http.get('',{params: { ShipNo :} })
        console.log('hasil=>',res);
        return res;
      },
      cekInvoice: function(param) {
        var res=$http.get('/api/as/GoodsReceipt/CekInvoice?GoodsReceiptNo='+param);
        // var res=$http.get('',{params: { ShipNo :} })
        console.log('hasil=>',res);
        return res;
      },
      print: function(url,noShipping) {
        var res=$http.get('/api/'+url, {
          params: {
            NoShipping: noShipping
          },
          responseType: 'arraybuffer'
        });
        return res;
      },
    }
  });
