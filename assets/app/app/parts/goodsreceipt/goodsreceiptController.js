angular.module('app')
    .controller('GoodsReceiptController', //['C_MATERIAL',
        function($scope, $http, CurrentUser, PartsGlobal, PartsLookup, LocalService,
            GoodsReceipt, C_MATERIAL, ngDialog, $timeout, $filter, bsAlert, uiGridConstants,PrintRpt) {

            $scope.dateOptions = {
              startingDay: 1,
              format: 'dd-MM-yyyy',
              // disableWeekend: 1
            };

            var dateFilter = 'date:"dd-MM-yyyy"';

              //Options Switch
            $scope.viewButton = {save:{text:'Ok'}};
            $scope.modeModal = 'new';
            $scope.required = true;
            $scope.disableSelect = false;
            $scope.actionVisible = false;
            $scope.mySelections = [];
            $scope.user = CurrentUser.user();
            $scope.countShippingNo = 0;

            if($scope.user.RoleId == 1122 || $scope.user.RoleId == 1123){
                $scope.items = ['PO TAM', 'PO NON TAM', 'TRANSFER ORDER', 'CLAIM']; // PartsLookup
            }else{
                $scope.items = ['PO TAM', 'PO NON TAM', 'PO SUBLET', 'TRANSFER ORDER', 'CLAIM']; // PartsLookup
            }

            $scope.selection = $scope.items[0];
            $scope.IsVisible = true;
            $scope.formApi = {};
            $scope.isTO = 0;
            $scope.tampungNoShipping_simpanPO = null;
            $scope.flagGR = 0;
            $scope.ShowHide = function() {
                //If DIV is visible it will be hidden and vice versa.
                $scope.IsVisible = $scope.IsVisible ? false : true;
            };
            $scope.captionLabel = {};
            $scope.ReceiptTime = PartsGlobal.getCurrTime();
            $scope.captionLabel.new = "Buat";
            $scope.hapusFilter = function() {
                // $scope.mDataFilter.RefMRTypeId='';
                $scope.mData.RefGRTypeId = null;
                $scope.mData.GoodsReceiptStatusId = null;
                $scope.mData.startDate = null;
                $scope.mData.endDate = null;
                $scope.mData.OutletId = null;
                $scope.mData.ShippingNo = null;
                $scope.mData.PartsCode = null;
                $scope.mFilter = {};
                // alert("Hapus filter");
            }
            var today = new Date();
            console.log("Time : ", today.getUTCHours());
            console.log("TIME 2 : ", today.getHours());
            console.log("TIME 3 : ", PartsGlobal.getCurrTime()); //
            console.log("TIME 4 : ", PartsGlobal.getCurrDateTime());
            $scope.checkRights = function(bit) {
                console.log("Rights : ", $scope.myRights);
                var p = $scope.myRights & Math.pow(2, bit);
                var res = (p == Math.pow(2, bit));
                console.log("myRights => ", $scope.myRights);
                return res;
            }
            $scope.getRightX = function(a, b) {
                var i = 0;
                if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
                else if ($scope.checkRights(b)) i = 2;
                else if ($scope.checkRights(a)) i = 1;
                return i;
            }

            $scope.getRightMaterialType = function() {
                // var i = $scope.getRightX(8, 9);
                // if (($scope.checkRights(10) && $scope.checkRights(11))) i = 3;
                // else if ($scope.checkRights(11)) i= 2;
                // else if ($scope.checkRights(10)) i= 1;
                // return i;
                var materialTypeId = 3;
                switch($scope.user.RoleId)
                {
                    case 1135:
                    case 1122:
                    case 1123:
                        materialTypeId = 1;
                        break;
                    case 1124:
                    case 1125:
                        materialTypeId = 2;
                        break;
                }
                return materialTypeId;
            }

            $scope.getRightServiceType = function() {
                // var i = $scope.getRightX(10, 11);
                // return i;
                var serviceTypeId = 0;
                switch($scope.user.RoleId)
                {
                    case 1135:
                    case 1125:
                    case 1122:
                        serviceTypeId = 1;
                        break;
                    case 1123:
                    case 1124:
                        serviceTypeId = 0;
                        break;
                }
                return serviceTypeId;
            }

            $scope.ToNameMaterial = function(MaterialTypeId) {
                var materialName;
                switch (MaterialTypeId) {
                    case 1:
                        materialName = "Parts";
                        break;
                    case 2:
                        materialName = "Bahan";
                        break;
                }
                return materialName;
            }

            //Form State
            $scope.isNewForm = false;
            $scope.isEditForm = '';
            $scope.isEditState = false;
            $scope.isOverlayForm = false;
            $scope.isViewForm = '';
            $scope.isEditClicked = false;
            $scope.isPOTAM = false;
            //$scope.grStatus = {};
            $scope.refGR = [];
            $scope.flagIsPart = $scope.getRightMaterialType();
            var defaultArray = angular.copy(PartsLookup.lkp_RefGRType());
            for(var i in defaultArray){
                if( $scope.flagIsPart == 1 ||  $scope.flagIsPart == '1'){ // parts
                    if(defaultArray[i].id !== '3'){
                        $scope.refGR.push(defaultArray[i]);
                    }
                    $scope.items = ['PO TAM', 'PO NON TAM', 'TRANSFER ORDER', 'CLAIM']; // PartsLookup
                }else{ // bahan
                    $scope.refGR.push(defaultArray[i]);
                    $scope.items = ['PO TAM', 'PO NON TAM', 'PO SUBLET', 'TRANSFER ORDER', 'CLAIM']; // PartsLookup
                }
            }
            $scope.selection = $scope.items[0];
            $scope.MaterialType = PartsLookup.lkp_MaterialType();
            // console.log("ref GR :", $scope.refGR);//[0].name);
            $scope.grStatus = C_MATERIAL.Status;
            $scope.TipeMat = C_MATERIAL.Tipe;
            $scope.GRHeader = GoodsReceipt.getGRHeader();
            if ($scope.GRHeader == {}) {
                $scope.GRid = 0;
            } else {
                $scope.GRid = $scope.GRHeader.GoodsReceiptId;
            };
            // $scope.grStatusName= C_MATERIAL.Status[$scope.GRHeader.GoodsReceiptStatusId].name;

            //New Form State
            $scope.ref = '';

            $scope.resizeLayout = function() {
                console.log("Make sure this function called");
                $timeout(function() {
                    var staticHeight =
                        $("#page-footer").outerHeight() +
                        $(".nav.nav-tabs").outerHeight() +
                        $(".ui.breadcrumb").outerHeight() + 85;
                    var advsearch = $(".advsearch");
                    var advsearchHeight = $(".advsearch").outerHeight();
                    if (!advsearchHeight) advsearchHeight = 0;
                    $("#layoutContainer_GRBaru").height($(window).height() - 5);
                }, 0);
            };
            //----------------------------------
            // Start-Up
            //----------------------------------
            // declaration variable 
            $scope.materialTypeIdDefault = 1;
            $scope.serviceTypeIdDefault = 1;
            // ====================
            $scope.$on('$viewContentLoaded', function() {
                $scope.loading = false;
                $timeout(function() {
                    $scope.resizeLayout();
                    var parts = $scope.checkRights(2);
                    console.log("edit ?= ", parts);
                    console.log('user=', $scope.user);
                    console.log("Rights : ", $scope.myRights);
                    $scope.materialTypeIdDefault = $scope.getRightMaterialType();
                    $scope.serviceTypeIdDefault = $scope.getRightServiceType();
                    // $scope.mData.OutletId = $scope.user.OrgId;
                    $scope.vOutletId = $scope.user.OrgId;
                    console.log("M T : ", $scope.materialTypeIdDefault);
                    console.log("S T : ", $scope.serviceTypeIdDefault);
                    if (PartsGlobal.myWarehouse = {}) {
                        if ($scope.materialTypeIdDefault > 0 && $scope.serviceTypeIdDefault > 0) {
                            // PartsGlobal.getMyWarehouse({ //getWarehouse
                            PartsGlobal.getWarehouse({
                                OutletId: $scope.user.OrgId,
                                MaterialTypeId: $scope.materialTypeIdDefault,
                                ServiceTypeId: $scope.serviceTypeIdDefault
                            }).then(function(res) {
                                    var gridData = res.data.Result;
                                    console.log("warehouse ", gridData[0]);
                                    PartsGlobal.myWarehouses = gridData;
                                    PartsGlobal.myWarehouse = gridData[0];
                                    $scope.mData.WarehouseId = gridData[0].WarehouseId;
                                    $scope.mData.WarehouseName = gridData[0].WarehouseName;
                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
                        }
                    } else {
                        $scope.mData.WarehouseId = PartsGlobal.myWarehouse.WarehouseId;
                        $scope.mData.WarehouseName = PartsGlobal.myWarehouse.WarehouseName;
                    }
                });

            });
            // ------- 12-10-2017
            var strTemplateAction = '<div> <center><button class="btn rbtn" ng-click="grid.appScope.$parent.deleteItem(row)"><i class="fa fa-trash"></i></button></center></div>';
            $scope.columnsPOTAM = [
                { name: 'No. GR', field: 'GoodsReceiptId', visible: false },
                { name: 'No. GR', field: 'GoodsReceiptItemId', visible: false },
                { name: 'No. PO', field: 'PurchaseOrderId', visible: false },
                { name: 'nopo', field: 'RefGRNo', suppressRemoveSort: true, defaultSort: { direction: uiGridConstants.ASC },  displayName: 'No. PO', enableCellEdit: $scope.potamNew, minWidth: 150, width: "*" }, //cellEditableCondition: $scope.isPOTAM},
                // { name:'No. PO', field: 'RefGRNo', displayName: 'No. PO', cellTemplate: cellPONo, enableCellEdit: $scope.potamNew, minWidth: 150, width: "*"},
                { name: 'No. Inv', field: 'InvoiceId', visible: false }, //ReceivedInvoiceNo
                { name: 'No. Inv', field: 'InvoiceNo', cellTemplate: cellInvoiceNo, enableCellEdit: $scope.potamNew, minWidth: 150, width: "*" },
                { name: 'Tanggal Inv', field: 'InvoiceDate', enableCellEdit: false, cellFilter: dateFilter, minWidth: 100 },
                // { name:'No. Referensi SOP', field: 'RefPONo', displayName: 'No. Referensi SOP', enableCellEdit : $scope.potamNew, minWidth: 150, width: "*" },
                { name: 'No. Referensi SOP', field: 'NoRefSOP', displayName: 'No. Referensi SOP', enableCellEdit: $scope.potamNew, minWidth: 150, width: "*" }, //
                { name: 'No. Material', field: 'PartId', visible: false },
                { name: 'No. Material', field: 'PartsCode', sort: { direction: uiGridConstants.ASC, priority: 1 }, enableCellEdit: true, minWidth: 100 },
                // { name:'No. Material', field: 'PartsCode', cellTemplate: cellPartNo, enableCellEdit: true, minWidth: 100}, //$scope.isNewForm},
                { name: 'Nama Material', field: 'PartsName', enableCellEdit: false, minWidth: 150, width: "*", sort: { priority: 1 } },
                { name: 'Qty PO', field: 'QtyPO', displayName: 'Qty PO', enableCellEdit: false, minWidth: 100, width: "*" },
                { name: 'Qty Remain', field: 'QtyRemain', enableCellEdit: false, minWidth: 100, width: "*" },
                { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', enableCellEdit: true, minWidth: 100, width: "*" },
                // { name:'Satuan', field: 'UomPOId', enableCellEdit : false, minWidth: 100, width: "*"},
                { name: 'Satuan', field: 'UomGR', minWidth: 100, width: "*", visible: false }, //SatuanPO
                { name: 'Satuan', field: 'SatuanGR', enableCellEdit: false, minWidth: 100, width: "*" },
                { name: 'Lokasi Gudang', field: 'WarehouseName', enableCellEdit: false, minWidth: 150, width: "*" },
                { name: 'Lokasi Rak', field: 'LokasiRak', enableCellEdit: false, minWidth: 150, width: "*" },
                { name: 'GoodsReceiptStatusId', field: 'GoodsReceiptStatusId', visible: false, minWidth: 150, width: "*" }
                // ,
                // { name: 'Action',
                //   cellTemplate: strTemplateAction, minWidth: 100, width: "*"//,
                //   //visible : true //grid.appScope.$parent.isPOTAM //true //grid.appScope.$parent.ref!='PO TAM'
                // }
            ];
            $scope.columnsPONONTAM = [
                { name: 'No. GR', field: 'GoodsReceiptId', visible: false },
                { name: 'No. GR', field: 'GoodsReceiptItemId', visible: false },
                { name: 'No. PO', field: 'PurchaseOrderId', visible: false },
                { name: 'nopo', field: 'RefGRNo', suppressRemoveSort: true, defaultSort: { direction: uiGridConstants.ASC }, displayName: 'No. PO', enableCellEdit: $scope.potamNew, minWidth: 150, width: "*" }, //cellEditableCondition: $scope.isPOTAM},
                // { name:'No. PO', field: 'RefGRNo', displayName: 'No. PO', cellTemplate: cellPONo, enableCellEdit: $scope.potamNew, minWidth: 150, width: "*"},
                { name: 'No. Inv', field: 'InvoiceId', visible: false }, //ReceivedInvoiceNo
                { name: 'No. Inv', field: 'InvoiceNo', cellTemplate: cellInvoiceNo, enableCellEdit: $scope.potamNew, minWidth: 150, width: "*" },
                { name: 'Tanggal Inv', field: 'InvoiceDate', enableCellEdit: false, cellFilter: dateFilter, minWidth: 100 },
                // { name:'No. Referensi SOP', field: 'RefPONo', displayName: 'No. Referensi SOP', enableCellEdit : $scope.potamNew, minWidth: 150, width: "*" },
                { name: 'No. Referensi SOP', field: 'NoRefSOP', displayName: 'No. Referensi SOP', enableCellEdit: $scope.potamNew, minWidth: 150, width: "*" }, //
                { name: 'No. Material', field: 'PartId', visible: false },
                { name: 'No. Material', field: 'PartsId', visible: false },
                { name: 'No. Material', field: 'PartsCode', enableCellEdit: true, minWidth: 100 },
                // { name:'No. Material', field: 'PartsCode', cellTemplate: cellPartNo, enableCellEdit: true, minWidth: 100}, //$scope.isNewForm},
                { name: 'Nama Material', field: 'PartsName', sort: { priority: 1 }, enableCellEdit: false, minWidth: 150, width: "*" },
                { name: 'Qty PO', field: 'QtyPO', displayName: 'Qty PO', enableCellEdit: false, minWidth: 100, width: "*" },
                { name: 'Qty Remain', field: 'QtyRemain', enableCellEdit: false, minWidth: 100, width: "*" },
                { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', enableCellEdit: true, minWidth: 100, width: "*" },
                // { name:'Satuan', field: 'UomPOId', enableCellEdit : false, minWidth: 100, width: "*"},
                { name: 'Satuan', field: 'UomGR', minWidth: 100, width: "*", visible: false }, //SatuanPO
                { name: 'Satuan', field: 'SatuanGR', enableCellEdit: false, minWidth: 100, width: "*" },
                { name: 'Lokasi Gudang', field: 'WarehouseName', enableCellEdit: false, minWidth: 150, width: "*" },
                { name: 'Lokasi Rak', field: 'LokasiRak', enableCellEdit: false, minWidth: 150, width: "*" },
                { name: 'GoodsReceiptStatusId', field: 'GoodsReceiptStatusId', visible: false, minWidth: 150, width: "*" },
                // {
                //     name: 'Action',
                //     cellTemplate: strTemplateAction,
                //     minWidth: 100,
                //     width: "*" //,
                //         //visible : true //grid.appScope.$parent.isPOTAM //true //grid.appScope.$parent.ref!='PO TAM'
                // }
            ];
            //----------------------------------
            // Initialization
            //----------------------------------
            // $scope.user = CurrentUser.user();
            $scope.mData = {}; //Model
            $scope.mFilter = {};
            $scope.xRole = {
                selected: []
            };
            $scope.myGRStatus = '';
            $scope.myrefGR = '';
            $scope.myTipeMat = '';
            $scope.Type = "GAsjkags";

            //----------------------------------
            // Get Data
            //---------------------------------f-


            $scope.gridDataTree = [];

            $scope.onSearchPurchaseNo = function(potype, noship) {

                console.log('onSearchPurchaseNo | potype ===>',potype)
                console.log('onSearchPurchaseNo | noship ===>',noship)

                $scope.mData.ShippingNo = noship;
                if (noship == null) {
                    PartsGlobal.showAlert({
                        title: "Goods Receipt",
                        content: "No PO tidak boleh kosong",
                        type: 'danger'
                    });
                    return false;
                }

                GoodsReceipt.getDataPONO(potype,noship).then(function(res){
                    if(res.data.Result.length > 0){
                        console.log('onSearchPurchaseNo |  getDataPONO ===>', res.data.Result);
                        // Checking PO ref. Type
                        $scope.mData.Name = res.data.Result[0].VendorName;
                            switch(potype)  { // PO TAM
                                case 1:
                                    $scope.mData.RefPONo = res.data.Result[0].RefPONo == undefined || res.data.Result[0].RefPONo == null ? "" : res.data.Result[0].RefPONo;
                                    $scope.mData.VendorId = res.data.Result[0].VendorId == undefined || res.data.Result[0].VendorId == null ? $scope.mData.VendorId : res.data.Result[0].VendorId;
                                    $scope.mData.WarehouseId = res.data.Result[0].WarehouseId == undefined || res.data.Result[0].WarehouseId == null ? "" : res.data.Result[0].WarehouseId;
                                    $scope.mData.Name = res.data.Result[0].VendorName == undefined || res.data.Result[0].VendorName == null ? "" : res.data.Result[0].VendorName;
                                    $scope.mData.HasilSOQ = res.data.Result[0].HasilSOQ == undefined || res.data.Result[0].HasilSOQ == null ? "" : res.data.Result[0].HasilSOQ;
                                    $scope.gridOptionsTAM.data = $scope.sortingData($scope.gridDataShippingTAM);
                                    $scope.checkRefGINo = false;
                                    break;
                                case 2: // PO NON TAM
                                    $scope.mData.RefPONo = res.data.Result[0].RefPONo == undefined || res.data.Result[0].RefPONo == null ? "" : res.data.Result[0].RefPONo;
                                    $scope.mData.VendorId = res.data.Result[0].VendorId == undefined || res.data.Result[0].VendorId == null ? $scope.mData.VendorId : res.data.Result[0].VendorId;
                                    $scope.mData.WarehouseId = res.data.Result[0].WarehouseId == undefined || res.data.Result[0].WarehouseId == null ? "" : res.data.Result[0].WarehouseId;
                                    $scope.mData.Name = res.data.Result[0].VendorName == undefined || res.data.Result[0].VendorName == null ? "" : res.data.Result[0].VendorName;
                                    var tmpData = res.data.Result;
                                    $scope.checkRefGINo = false;
                                    var tmpArr = [];
                                    for(var i in tmpData){
                                        tmpData[i].openedDate = false;
                                        tmpData[i].SatuanPO = tmpData[i].Satuan;
                                        // tmpData[i].xQtyRemain = tmpData[i].QtyPO - tmpData[i].GRQty;
                                        if(tmpData[i].QtyGR < tmpData[i].QtyPO){
                                            // if(tmpData[i].QtyGR == 0){
                                                tmpData[i].QtyGR = tmpData[i].QtyRemain;
                                            // }
                                            tmpArr.push(tmpData[i]);
                                        }
                                    }

                                    console.log('onSearchPurchaseNo | PO NON TAM | data grid ===>',tmpArr)
                                    $scope.gridOptions.data = tmpArr;
                                    break;
                                case 3: // PO SUBLET
                                    $scope.mData.RefPONo = res.data.Result[0].RefPONo == undefined || res.data.Result[0].RefPONo == null ? "" : res.data.Result[0].RefPONo;
                                    $scope.mData.VendorId = res.data.Result[0].VendorId == undefined || res.data.Result[0].VendorId == null ? $scope.mData.VendorId : res.data.Result[0].VendorId;
                                    $scope.mData.WarehouseId = res.data.Result[0].WarehouseId == undefined || res.data.Result[0].WarehouseId == null ? "" : res.data.Result[0].WarehouseId;
                                    $scope.mData.Name = res.data.Result[0].VendorName == undefined || res.data.Result[0].VendorName == null ? "" : res.data.Result[0].VendorName;
                                    var tmpData = res.data.Result;
                                    $scope.checkRefGINo = false;
                                    var tmpArr = [];
                                    for(var i in tmpData){
                                        tmpData[i].openedDate = false;
                                        tmpData[i].SatuanPO = tmpData[i].Satuan;
                                        // tmpData[i].xQtyRemain = tmpData[i].QtyPO - tmpData[i].GRQty;
                                        if(tmpData[i].QtyGR < tmpData[i].QtyPO){
                                            // if(tmpData[i].QtyGR == 0){
                                                tmpData[i].QtyGR = tmpData[i].QtyRemain;
                                            // }
                                            tmpArr.push(tmpData[i]);
                                        }
                                    }
                                    $scope.gridSublet.data = tmpArr;
                                    break;
                                case 4: // TO
                                    $scope.gridTO.data = res.data.Result;
                                    $scope.checkRefGINo = false;
                                    break;
                                default:
                                    break;
                            }
                    }else{
                        PartsGlobal.showAlert({
                            title: "Goods Receipt",
                            content: "No PO ini tidak ditemukan",
                            type: 'danger'
                        });
                        return false;
                    }
                });
            }
            $scope.showUploadedError = false;
            $scope.onOk = function(){
                $scope.showUploadedError = false;
            }
            $scope.onCancel = function(){
                $scope.showUploadedError = false;
            }
            $scope.onShippingNoChanged = function() {
                $scope.checkRefGINo = true;
            }
            $scope.errorOnly = false;
            $scope.onSearchShippingNo = function(potype, noship) {
                // alert ("No Shipping ", noship);
                var no_shipping = $scope.mData.ShippingNo;
                if (no_shipping == null) {
                    PartsGlobal.showAlert({
                        title: "Goods Receipt",
                        content: "No Shipping tidak boleh kosong",
                        type: 'danger'
                    });
                    return 0;
                }

                // $scope.mData.VendorId = 777; // dummy
                // $scope.mData.MaterialTypeId = $scope.TipeMat[0];
                // console.log("no ship", $scope.mData.ShippingNo)
                
                if ($scope.clicked) {
                    $scope.cancelClick = true;
                    return;
                }
            
                $scope.clicked = true;
            
                $timeout(function () {
                    if ($scope.cancelClick) {
                        $scope.cancelClick = false;
                        $scope.clicked = false;
                        return;
                    }
            
                    //do something with your single click here
                    $scope.gridDataShipping = [];
                    $scope.gridDataShippingTAM = [];
                    var lengthCekShipping = 0;

                    GoodsReceipt.cekShipping(no_shipping).then(function(resShip){
                        $scope.dataError = [];
                        lengthCekShipping = resShip.data.length;
                        if(lengthCekShipping > 0){
                            $scope.dataError = resShip.data;
                            if($scope.dataError.length == 1){
                                $scope.errorOnly = true
                            }else{
                                $scope.errorOnly = false
                            }
                        }
                        PartsGlobal.getNoShipping(no_shipping).then(function(res) {
                            $scope.countShippingNo = 0;
                            if (res.data.Result.length == 0 && lengthCekShipping == 0) {
                                PartsGlobal.showAlert({
                                    title: "Goods Receipt",
                                    content: "No Shipping tidak ditemukan",
                                    type: 'danger'
                                });
                                $scope.gridOptionsTAM.data = []
                                return 0;
                            }else {
                                // $scope.dataError = [];
                                // GoodsReceipt.cekShipping(no_shipping).then(function(xres){
                                //     console.log('===>', xres.data);
                                //     if(xres.data.length > 0){
                                //         $scope.dataError = xres.data;
                                //         if($scope.dataError.length == 1){
                                //             $scope.errorOnly = true
                                //         }else{
                                //             $scope.errorOnly = false
                                //         }
                                //         $scope.showUploadedError = true;
                                //     }else{
                                //         $scope.showUploadedError = false;
                                //         $scope.errorOnly = false;
                                //     }
                                // });
                                if(lengthCekShipping > 0){
                                    $scope.showUploadedError = true;
                                }else{
                                    $scope.showUploadedError = false;
                                    $scope.errorOnly = false;
                                }

                                if(res.data.Result.length > 0){
                                    for (var i in res.data.Result) {
                                        // if (res.data.Result[i].GRQty != res.data.Result[i].QtyPO) 
                                        res.data.Result[i].xQtyRemain = res.data.Result[i].QtyPO - res.data.Result[i].GRQty;
                                        if (res.data.Result[i].GRShipmentQty != res.data.Result[i].ShipmentQty) {
                                            $scope.gridDataShippingTAM.push(res.data.Result[i]);
                                        }
                                        console.log('res.data.Result[i].xQtyRemain ===>', res.data.Result[i].xQtyRemain);
                                    }
                                    if ($scope.gridDataShippingTAM.length < 1) {
                                        PartsGlobal.showAlert({
                                            title: "Goods Receipt",
                                            content: "Proses Goods Receipt dengan No. Shipping "+no_shipping+" sudah dilakukan",
                                            type: 'danger'
                                        });
                                        $scope.gridOptionsTAM.data = []
                                        return 0;
                                    }
                                }
                            }
                            $scope.countShippingNo = 0;
                            $scope.checkRefGINo = false;
                            console.log('$scope.gridDataShippingTAM===>',$scope.gridDataShippingTAM);
                            console.log("$scope.countShippingNo dalam=>",$scope.countShippingNo);
                            // Checking PO ref. Type
                            switch(potype)  { // PO TAM
                                case 1:
                                    $scope.mData.RefPONo = res.data.Result[0].RefPONo == undefined || res.data.Result[0].RefPONo == null ? "" : res.data.Result[0].RefPONo;
                                    $scope.mData.VendorId = res.data.Result[0].VendorId == undefined || res.data.Result[0].VendorId == null ? $scope.mData.VendorId : res.data.Result[0].VendorId;
                                    $scope.mData.WarehouseId = res.data.Result[0].WarehouseId == undefined || res.data.Result[0].WarehouseId == null ? "" : res.data.Result[0].WarehouseId;
                                    $scope.mData.Name = res.data.Result[0].VendorName == undefined || res.data.Result[0].VendorName == null ? "" : res.data.Result[0].VendorName;
                                    $scope.mData.HasilSOQ = res.data.Result[0].HasilSOQ == undefined || res.data.Result[0].HasilSOQ == null ? "" : res.data.Result[0].HasilSOQ;
                                    $scope.gridOptionsTAM.data = $scope.sortingData($scope.gridDataShippingTAM);
                                    break;
                                case 2: // PO NON TAM
                                    $scope.mData.RefPONo = res.data.Result[0].RefPONo == undefined || res.data.Result[0].RefPONo == null ? "" : res.data.Result[0].RefPONo;
                                    $scope.mData.VendorId = res.data.Result[0].VendorId == undefined || res.data.Result[0].VendorId == null ? $scope.mData.VendorId : res.data.Result[0].VendorId;
                                    $scope.mData.WarehouseId = res.data.Result[0].WarehouseId == undefined || res.data.Result[0].WarehouseId == null ? "" : res.data.Result[0].WarehouseId;
                                    $scope.mData.Name = res.data.Result[0].VendorName == undefined || res.data.Result[0].VendorName == null ? "" : res.data.Result[0].VendorName;
                                    $scope.gridOptions.data = res.data.Result;
                                    break;
                                case 3: // PO SUBLET
                                    $scope.gridSublet.data = res.data.Result;
                                    break;
                                case 4: // TO
                                    $scope.gridTO.data = res.data.Result;
                                    break;
                                default:
                                    break;
                            }
                        });
                    });
                
                    //clean up
                    $scope.cancelClick = false;
                    $scope.clicked = false;
                }, 500);

            }

            $scope.sortingData = function (data){
                data.sort(function(a, b) {
                    if(a.PurchaseOrderNo){
                        return a.PurchaseOrderNo - b.PurchaseOrderNo;
                    }
                });
                // console.log('=====', data );
                // console.log('ahhh', $scope.gridDataShippingTAM)
                // _.map(data, function(val){
                //     if(val.PurchaseOrderNo){
                //         val.PurchaseOrderNo = parseInt(val.PurchaseOrderNo);
                //     }
                //     if(val.RefGRNo){
                //         val.RefGRNo = parseInt(val.RefGRNo);

                //     }
                // });

                console.log('sortingData | data ===>',data)
                return data;
            }
            $scope.onSearchShippingNoFilterMain = function(shippingNo) {
                var ress = PartsGlobal.getNoShippingFilterMain(shippingNo).then(function(resTask) {
                    return resTask.data.Result;
                });
                console.log("ress", ress);
                return ress
            };

            $scope.onNoResult = function() {

            };

            $scope.onGotResult = function() {

            };

            $scope.onSelectWork = function() {

            };

            $scope.getData = function(mFilter) {
                // $scope.grid.data = [];
                // if (mData.GoodsReceiptStatusId=={}) {mData.GoodsReceiptStatusId.id=null };
                if ($scope.mFilter.RefGRTypeId == null && $scope.mFilter.GoodsReceiptStatusId == null && $scope.mFilter.startDate == null && $scope.mFilter.endDate == null &&
                    ($scope.mFilter.ShippingNo == null || $scope.mFilter.ShippingNo == "") && ($scope.mFilter.OutletId == null || $scope.mFilter.OutletId == "" || $scope.mFilter.PartsCode == "")) {
                    PartsGlobal.showAlert({
                        title: "Goods Receipt",
                        content: "Filter tidak boleh kosong",
                        type: 'danger'
                    });
                    return 0;
                }
                if ($scope.mFilter.startDate == null && $scope.mFilter.endDate == null && ($scope.mFilter.GoodsReceiptStatusId != null || $scope.mFilter.RefGRTypeId != null)) {
                    PartsGlobal.showAlert({
                        title: "Goods Receipt",
                        content: "Filter Referensi GR dan Tanggal GR harus diisi bersamaan",
                        type: 'danger'
                    });
                    return 0;
                }
                console.log('=====>', mFilter);
                GoodsReceipt.getData($scope.mFilter).then(function(res) {
                        var gridData = res.data.Result;
                        // var childData = gridData[0];//.Test_Component;
                        // console.log(childData);
                        _.map(gridData,function(val){
                            if(val.ShippingNo == "000000"){
                                val.ShippingNo = "-"
                            }
                        })
                        console.log(gridData);
                        console.log("Testing");
                        if (gridData.length == 0) {
                            $scope.grid.data = [];
                        } else {
                            $scope.grid.data = [];
                            for (var i = 0; i < gridData.length; i++) {
                                if ($scope.materialTypeIdDefault == gridData[i].MaterialTypeId) {
                                    $scope.grid.data.push(gridData[i]);
                                }
                            }
                        }
                        $scope.loading = false;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
            $scope.setNewDataItem = function() {
                $scope.dataNewItem = [{
                    PurchaseOrderId: 0,
                    InvoiceId: 0,
                    PartId: 0,
                    QtyBase: 0,
                    RefGRTypeId: 0,
                    QtyGR: 0,
                    UomGR: ""
                }];
            }
            $scope.addNewDataItem = function(caller) {
                $scope.setNewDataItem(); //SEMENTAWIS DISABLED .....
                console.log("Form Mode (IsNewForm) 2: ", $scope.isNewForm);
                $scope.dataNewItem[0].GoodsReceiptId = $scope.GRHeader.GoodsReceiptId;
                $scope.dataNewItem[0].OutletId = $scope.GRHeader.OutletId;
                $scope.dataNewItem[0].WarehouseId = $scope.mData.WarehouseId; //$scope.GRHeader.WarehouseId;
                $scope.dataNewItem[0].RefGRTypeId = caller / 10;
                switch (caller) {
                    case 10:
                        // $scope.SearchPO({ RefGRTypeId: 1 });
                        $scope.gridOptionsTAM.data.push($scope.dataNewItem[0]);
                        break;
                    case 20:
                        $scope.SearchPO({ RefGRTypeId: 2 });
                        break;
                    case 30:
                        $scope.gridSublet.data.push($scope.dataNewItem[0]);
                        break;
                        // case 40: $scope.gridTO.data.push($scope.dataNewItem[0]); break;
                    case 40:
                        $scope.SearchPO({ RefGRTypeId: 4 });
                        break;
                    case 50:
                        $scope.SearchPO({ RefGRTypeId: 5 });
                        break;
                }
                console.log("Form Mode (IsNewForm) 3: ", $scope.isNewForm);
            }
            $scope.getDataItem = function(dataHeader, mode) {
                $scope.setNewDataItem(); // 14-09-2017 cek
                GoodsReceipt.getDataItem(dataHeader).then(function(res) {
                        var gridData = res.data.Result;
                        for(var a in gridData){
                            gridData[a].SatuanPO = gridData[a].Satuan;
                        }
                        console.log(gridData);
                        console.log("Testing Item dari : ", dataHeader);
                        if (gridData.length == 0) {
                            switch (dataHeader.RefGRTypeId) {
                                case 1:
                                    $scope.gridOptionsTAM.data = [];
                                    break;
                                case 2:
                                case 5:
                                    $scope.gridOptions.data = [];
                                    break;
                                case 3:
                                    $scope.gridSublet.data = [];
                                    break;
                                case 4:
                                    $scope.gridTO.data = [];
                                    break;
                            }

                        } else {
                            /* */
                            gridData = $scope.sortingData(gridData);
                            switch (dataHeader.RefGRTypeId) {
                                case 1:
                                    $scope.gridOptionsTAM.data = gridData;
                                    break;
                                case 2:
                                case 5:
                                    $scope.gridOptions.data = gridData; // jika po tam gak ditambah baris kosong
                                    // if (mode=='edit' && dataHeader.RefGRTypeId!= 1) {$scope.gridOptions.data.push($scope.dataNewItem[0]);}
                                    //-if (mode=='edit') {$scope.gridOptions.data.push($scope.dataNewItem[0]);}
                                    break;
                                case 3:
                                    $scope.gridSublet.data = gridData;
                                    //-if (mode=='edit') {$scope.gridSublet.data.push($scope.dataNewItem[0]);}
                                    break;
                                case 4:
                                    $scope.gridTO.data = gridData;
                                    //-if (mode=='edit') {$scope.gridTO.data.push($scope.dataNewItem[0]);}
                                    break;
                            } /* */
                            console.log("getdetail");
                            console.log("$scope.gridOptionsTAM.data=>",$scope.gridOptionsTAM.data);
                            console.log("$scope.gridOptions.data=>",$scope.gridOptions.data);
                            console.log("$scope.gridSublet.data=>",$scope.gridSublet.data);
                            console.log("$scope.gridTO.data=>",$scope.gridTO.data);
                        }
                        // $scope.loading = false;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
            $scope.getDaftarPO = function(data) {
                // var vVendorId = $scope.mData.VendorId;
                // if (vVendorId== null || vVendorId="") vVendorId = "0";
                console.log('dataaOPB', data);
                console.log('data outlet',$scope.selectedVendor);
                $scope.vendoridi = $scope.selectedVendor[0].VendorId;
                //var parDataPO = { OutletId: $scope.mData.VendorOutletid, PoNumber: (data.RefGRNo == null || data.RefGRNo == "" ? 0 : data.RefGRNo) };
                var parDataPO = { OutletId: $scope.vOutletId, PoNumber: (data.RefGRNo == null || data.RefGRNo == "" ? 0 : data.RefGRNo), OPB:(data.IsOPB  == null || data.IsOPB  == "" ? 0 : data.IsOPB ), VendorId: ($scope.vendoridi == null || $scope.vendoridi  == "" ? 0 : $scope.vendoridi )};
                //Siini ma
                console.log('parDataPO',parDataPO);
                PartsGlobal.getDaftarPOOPB(parDataPO).then(function(res) {
                        var gridData = res.data.Result;
                        console.log("Daftar PO : ", gridData);
                        PartsGlobal.formLookupGrid = $scope.gridLookUpPO;
                        PartsGlobal.formLookupGrid.multiSelect = false;
                        if (gridData.length == 0) {
                            //$scope.gridLookUpPO.data = [];
                            PartsGlobal.formLookupGrid.data = [];
                        } else {
                            console.log("gridData=>", gridData);
                            for(var i in gridData){
                                for(var j in $scope.gridLookUpVendor.data){
                                    if(gridData[i].VendorId === $scope.gridLookUpVendor.data[j].VendorId){
                                        gridData[i].VendorName = $scope.gridLookUpVendor.data[j].Name 
                                    }
                                }
                            }
                            PartsGlobal.formLookupGrid.data = gridData;
                        }
                        $scope.loading = false;
                        console.log('$scope.filterColumn', $scope.filterColumn);
                        console.log('PartsGlobal.filterColumn', PartsGlobal.filterColumn);
                        $scope.openDialog(data);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
            $scope.getDaftarClaim = function(dataVendor) {
                // PartsGlobal.getDaftarClaim()
                PartsGlobal.getDaftarClaim(dataVendor).then(function(res) {
                        var gridData = res.data.Result;
                        console.log("Daftar Claim : ", gridData);
                        if (gridData.length == 0) {
                            $scope.gridLookUpClaim.data = [];
                        } else {
                            $scope.gridLookUpClaim.data = gridData;
                        }
                        $scope.loading = false;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
            $scope.getDaftarTO = function() {
                $scope.isTO = 1;
                var parDataPO = { OutletId: $scope.vOutletId, VendorId: ($scope.mData.VendorId == null || $scope.mData.VendorId == "" ? 0 : $scope.mData.VendorId),MaterialTypeId: $scope.mData.MaterialTypeId  };
                PartsGlobal.getDaftarTO(parDataPO).then(function(res) {
                        var gridData = res.data.Result;
                        console.log("Daftar TO : ", gridData);
                        if (gridData.length == 0) {
                            $scope.gridLookUpTO.data = [];
                        } else {
                            $scope.gridLookUpTO.data = gridData;
                            $scope.formLookupGrid.data = gridData;
                            console.log("Daftar TO 2: ", gridData);
                            console.log("Daftar TO 3: ", $scope.formLookupGrid);
                            console.log("Daftar TO 4: ", $scope.gridLookUpTO);
                            // $scope.openDialog();
                        }
                        $scope.loading = false;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
            $scope.getVendor = function(RefGRType) {
                switch (RefGRType) {
                    case 1:
                        PartsGlobal.getVendorPO({ POType: 1 }).then(function(res) {
                                var gridData = res.data.Result;
                                console.log("Daftar Vendor 1: ", gridData);
                                if (gridData.length == 0) {
                                    $scope.gridLookUpVendor.data = [];
                                } else {
                                    $scope.gridLookUpVendor.data = gridData;
                                }
                                $scope.loading = false;
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                        break;
                    case 2:
                    case 3: // POType diisi 10, yg penting diisi selain 1,2,3 dan 99
                        PartsGlobal.getVendorPO({ POType: 10 }).then(function(res) {
                                var gridData = res.data.Result;
                                var gridDataOPB = [];
                                console.log("Daftar Vendor 2: ", gridData);
                                //KataPak Rully
                                // if(RefGRType != 3){
                                    if(RefGRType != 3){
                                        if (gridData.length == 0) {
                                            $scope.gridLookUpVendor.data = [];
                                        } else {
                                            $scope.gridLookUpVendor.data = gridData;
                                        }
                                    }else {
                                    //   var tmpIndex = _.findIndex(gridDataOPB,{Name:"OPB"})
                                    //   if(tmpIndex == -1){
                                    //     gridDataOPB.push({
                                    //           VendorId: 999999,
                                    //           VendorOutletid: 999999,
                                    //           Name:"OPB",
                                    //           IsOutlet:0,
                                    //           Address:"OPB",
                                    //           VendorCode:"OPB",
                                    //           SPLDBit:0
                                    //     });
                                    //   }
                                      for(var i=0; i<gridData.length;i++){
                                          gridDataOPB.push(gridData[i]);
                                      }
                                      $scope.gridLookUpVendor.data = gridDataOPB;
                                    }

                                $scope.loading = false;
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                        break;
                    case 4: // sebenarnya tipe 4 (TO) ini, tidak dipanggil disini, dipanggil di vendor outlet
                        // PartsGlobal.getVendor().then(function(res) {
                        PartsGlobal.getVendorPO({}).then(function(res) {
                                var gridData = res.data.Result;
                                console.log("Daftar Vendor 3: ", gridData);
                                if (gridData.length == 0) {
                                    $scope.gridLookUpVendor.data = [];
                                } else {
                                    console.log("Daftar Vendor X: ", gridData);
                                    $scope.gridLookUpVendor.data = gridData;
                                }
                                $scope.loading = false;
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                        break;
                    case 5: // POType diisi 99, di define sebagai PO yg ada Claim
                        PartsGlobal.getVendorPO({ POType: 99 }).then(function(res) {
                                var gridData = res.data.Result;
                                console.log("Daftar Vendor 4: ", gridData);
                                if (gridData.length == 0) {
                                    $scope.gridLookUpVendor.data = [];
                                } else {
                                    $scope.gridLookUpVendor.data = gridData;
                                }
                                $scope.loading = false;
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                        break;
                }
            }
            $scope.getVendorOutlet = function() {
                PartsGlobal.getVendorOutlet().then(function(res) {
                        var gridData = res.data.Result;
                        console.log("Daftar Vendor 5: ", gridData);
                        if (gridData.length == 0) {
                            $scope.gridLookUpVendorOutlet.data = [];
                        } else {
                            $scope.gridLookUpVendorOutlet.data = gridData;
                        }
                        $scope.loading = false;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }

            $scope.onSelectRows = function(rows) {
                console.log("onSelectRows=>", rows);
                $scope.testmodel = rows;
            }

            //------------------------------------------------------------
            $scope.formmode = { mode: '' };
            $scope.doGeneratePass = function() {
                $scope.mUser.password = Math.random().toString(36).slice(-8);
            }
            $scope.appendGrid = function(data, sourcedata) {
                // $scope.mUser.password = Math.random().toString(36).slice(-8);
                console.log("selectedItems : ", sourcedata, data);
                console.log("Last Modif 21-12-2017 20:55 ");
                if (data.RefGRTypeId == 1) {
                    for (i = 0; i < sourcedata.length; i++) {
                        sourcedata[i]['RefGRNo'] = sourcedata[i]['PurchaseOrderNo']; //
                        sourcedata[i]['NoRefSOP'] = sourcedata[i]['RefPONo']; //
                        sourcedata[i]['SatuanGR'] = sourcedata[i]['SatuanPO']; //  //SatuanGR
                        sourcedata[i]['UomGR'] = sourcedata[i]['UomPOId']; //
                        $scope.gridOptionsTAM.data.push(sourcedata[i]);
                    }
                } else if (data.RefGRTypeId == 2) {
                    for (i = 0; i < sourcedata.length; i++) {
                        sourcedata[i]['RefGRNo'] = sourcedata[i]['PurchaseOrderNo'];
                        sourcedata[i]['NoRefSOP'] = sourcedata[i]['RefPONo']; //
                        sourcedata[i]['SatuanGR'] = sourcedata[i]['SatuanPO']; //  //SatuanGR
                        sourcedata[i]['UomGR'] = sourcedata[i]['UomPOId'];
                        sourcedata[i]['QtyGR'] = sourcedata[i]['QtyRemain'];
                        $scope.gridOptions.data.push(sourcedata[i]);
                    }
                } else if (data.RefGRTypeId == 4) {
                    for (i = 0; i < sourcedata.length; i++) {
                        sourcedata[i]['RefGRNo'] = sourcedata[i]['TransferOrderNo'];
                        sourcedata[i]['NoRefSOP'] = sourcedata[i]['RefPONo']; //
                        sourcedata[i]['SatuanGR'] = sourcedata[i]['SatuanPO']; //  //SatuanGR
                        sourcedata[i]['UomGR'] = sourcedata[i]['UomPOId']; //LokasiRak
                        sourcedata[i]['QtyPO'] = sourcedata[i]['QtyTransfer']; //QtyPO
                        sourcedata[i]['LokasiRak'] = sourcedata[i]['ShelfName'];
                        $scope.gridTO.data.push(sourcedata[i]);
                    }
                }
                console.log("$scope.appendGrid : ", $scope.gridOptions.data);
            }
            $scope.updateGrid = function(data, sourcedata) {
                switch (data.RefGRTypeId) {
                    case 1:
                        var index = $scope.gridOptionsTAM.data.indexOf(data);
                        for (var k in sourcedata[0]) $scope.gridOptionsTAM.data[index][k] = sourcedata[0][k];
                        break;
                    case 2:
                        var index = $scope.gridOptions.data.indexOf(data);
                        for (var k in sourcedata[0]) $scope.gridOptions.data[index][k] = sourcedata[0][k];
                        break;
                    case 3:
                        var index = $scope.gridSublet.data.indexOf(data);
                        //sourcedata[index]['RefGRNo'] = sourcedata[index]['PurchaseOrderNo'];
                        for (var k in sourcedata[0]) $scope.gridSublet.data[index][k] = sourcedata[0][k];
                        $scope.gridSublet.data[index].QtyGR = sourcedata[0].QtyRemain;
                    case 4:
                        var index = $scope.gridTO.data.indexOf(data);
                        //sourcedata[index]['RefGRNo'] = sourcedata[index]['TransferOrderNo'];
                        for (var k in sourcedata[0]) $scope.gridTO.data[index][k] = sourcedata[0][k];
                    default:
                        break;
                }
            }
            $scope.SearchMaterial = function(data) {
                console.log("Part Code : ", data.PartsCode);
                PartsGlobal.getDetailMaterial(data.PartsCode).then(function(res) {
                        var gridData = res.data.Result;
                        if (typeof gridData == 'undefined') return 0;
                        gridData[0]['ShippingNo'] = $scope.mData.ShippingNo;
                        gridData[0]['VendorId'] = $scope.mData.VendorId;
                        gridData[0]['RefGRNo'] = $scope.mData.RefPONo;
                        gridData[0]['PartsCode'] = gridData[0]['PartsCode'];
                        gridData[0]['PartName'] = gridData[0]['PartsName'];
                        gridData[0]['UnitPrice'] = gridData[0]['RetailPrice'];
                        gridData[0]['UomGR'] = gridData[0]['UomPOId'];
                        gridData[0]['Satuan'] = gridData[0]['SatuanPO'];
                        gridData[0]['WarehouseName'] = gridData[0]['WarehouseName'];
                        gridData[0]['LokasiRak'] = gridData[0]['ShelfName'];

                        console.log(gridData[0]);
                        $scope.updateGrid(data, gridData);
                    },
                    function(err) {
                        console.log("err=>", err);
                        confirm("Nomor Material Tidak Ditemukan.");
                    }
                );
            }
            $scope.SearchVendor = function(data) {
                    PartsGlobal.formLookupTitle = "Daftar Vendor ";
                    PartsGlobal.formLookupSubTitle1 = "Daftar Vendor Untuk Outlet : (GR type " + data.RefGRTypeId + ")";
                    $scope.filterableColumns2 = [{ field: "Name", fieldDisplay: "Nama Vendor" }];
                    PartsGlobal.formLookupSubTitle2 = PartsGlobal.getCurrDate();
                    PartsGlobal.formLookupGrid = $scope.gridLookUpVendor; //"gridLookUpPO"; $scope.getDaftarPO()
                    PartsGlobal.formLookupGrid.multiSelect = false;
                    PartsGlobal.formLookupGrid.data = $scope.getVendor(data.RefGRTypeId);
                    PartsGlobal.cancelledLookupGrid = true;
                    // PartsGlobal.formLookupShowFilter = false;
                    PartsGlobal.formLookupShowFilter2 = true; //[{field: "PurchaseOrderNo"}, {field: "PartsCode"}]                    
                    console.log(" PartsGlobal.formLookupGrid: ", PartsGlobal.formLookupGrid);
                    ngDialog.openConfirm({
                        template: '<div ng-include=\"\'app/parts/goodsreceipt/frm_lookup01Me.html\'\"></div>',
                        plain: true,
                        // controller: 'GoodsReceiptController',
                        scope: $scope,                        
                    }).then(function(value) {
                        $scope.selectedVendor = PartsGlobal.selectedLookupGrid;
                        $scope.gridOptions.data = [];
                        console.log("Vendor selected SearchVendor: ", $scope.selectedVendor);
                        $scope.mData.VendorOutletid = $scope.selectedVendor[0].Outletid;
                        $scope.mData.VendorId = $scope.selectedVendor[0].VendorId;
                        $scope.mData.VendorCode = $scope.selectedVendor[0].VendorCode;
                        $scope.mData.Name = $scope.selectedVendor[0].Name;
                        $scope.mData.OutletCode = $scope.selectedVendor[0].VendorId;
                        if (PartsGlobal.cancelledLookupGrid) return 1;
                    }, function(value) {
                        $scope.selectedVendor = PartsGlobal.selectedLookupGrid;
                        console.log("No Data fetched ", $scope.selectedVendor);
                        if (PartsGlobal.cancelledLookupGrid) return 1;
                        // $scope.mData.VendorId= $scope.selectedVendor[0].VendorCode;
                        $scope.mData.VendorId = $scope.selectedVendor[0].VendorId;
                        $scope.mData.VendorCode = $scope.selectedVendor[0].VendorCode;
                        $scope.mData.Name = $scope.selectedVendor[0].Name;
                        PartsGlobal.VendorId = $scope.mData.VendorId; //OutletId
                    });
                }
                //SearchOutlet(mData)
            $scope.SearchVendorOutlet = function(data) {
                PartsGlobal.formLookupTitle = "Daftar Bengkel ";
                PartsGlobal.formLookupSubTitle1 = "Daftar Bengkel Untuk Outlet : (GR type " + data.RefGRTypeId + ")";
                PartsGlobal.formLookupSubTitle2 = PartsGlobal.getCurrDate();
                PartsGlobal.formLookupGrid = $scope.gridLookUpVendorOutlet; //"gridLookUpPO"; $scope.getDaftarPO()
                PartsGlobal.formLookupGrid.multiSelect = false;
                PartsGlobal.formLookupGrid.data = $scope.getVendorOutlet();
                PartsGlobal.cancelledLookupGrid = true;
                console.log("X PartsGlobal.formLookupGrid: ", PartsGlobal.formLookupGrid);
                ngDialog.openConfirm({
                    template: '<div ng-include=\"\'app/parts/templates01/frm_lookup01.html\'\"></div>',
                    plain: true,
                    controller: 'GoodsReceiptController',
                }).then(function(value) {
                    $scope.selectedVendor = PartsGlobal.selectedLookupGrid;
                    console.log("Vendor selected SearchVendorOutlet: ", $scope.selectedVendor);
                    $scope.mData.VendorId = $scope.selectedVendor[0].OutletId;
                    $scope.mData.VendorCode = $scope.selectedVendor[0].OutletCode;
                    $scope.mData.Name = $scope.selectedVendor[0].Name;
                    $scope.mData.OutletCode = $scope.selectedVendor[0].OutletCode;
                    $scope.mData.Name = $scope.selectedVendor[0].Name;

                    if (PartsGlobal.cancelledLookupGrid) return 1;
                }, function(value) {
                    $scope.selectedVendor = PartsGlobal.selectedLookupGrid;
                    console.log("No Data fetched ", $scope.selectedVendor);
                    if (PartsGlobal.cancelledLookupGrid) return 1;
                    // $scope.mData.VendorId= $scope.selectedVendor[0].VendorCode;
                    $scope.mData.VendorId = $scope.selectedVendor[0].OutletId;
                    $scope.mData.Name = $scope.selectedVendor[0].Name;
                    // $scope.mData.VendorId= $scope.selectedVendor[0].VendorId;
                    $scope.mData.OutletCode = $scope.selectedVendor[0].OutletCode;
                    $scope.mData.Name = $scope.selectedVendor[0].Name;

                });
            };
            $scope.formLookupGrid = {};
            $scope.SearchPO = function(data) {
              console.log('data SearchPO',data);
                $scope.isTO = 0;
                var s1, s2
                console.log("PO No : ", data.RefGRNo);
                if (data.RefGRTypeId == 4) {
                    s1 = "Daftar TO";
                    s2 = "Daftar TO Untuk Outlet : (GR type " + data.RefGRTypeId + ")";
                } else {
                    s1 = "Daftar PO";
                    s2 = "Daftar PO Untuk Outlet : (GR type " + data.RefGRTypeId + ")";
                }
                //$scope.qtyReserved = function(data){
                console.log("on (data)=>", data);
                PartsGlobal.setparobjMaterial(data);
                PartsGlobal.formLookupTitle = s1; //"Daftar PO ";
                PartsGlobal.formLookupSubTitle1 = s2; //"Daftar PO Untuk Outlet : (GR type " + data.RefGRTypeId +")";
                PartsGlobal.formLookupSubTitle2 = PartsGlobal.getCurrDate();
                PartsGlobal.formLookupShowFilter = true; //[{field: "PurchaseOrderNo"}, {field: "PartsCode"}]
                PartsGlobal.filterableColumns = [{ field: "PurchaseOrderNo", fieldDisplay: "No. PO" }, { field: "PartsCode", fieldDisplay: "No. Part" }];
                // PartsGlobal.formLookupGrid = $scope.gridLookUpPO; //gridLookUpTO
                PartsGlobal.formLookupGrid.multiSelect = true;
                if (data.RefGRTypeId == 4) {
                    PartsGlobal.formLookupGrid = $scope.gridLookUpTO;
                    PartsGlobal.formLookupGrid.data = $scope.getDaftarTO();
                    $scope.formLookupGrid = $scope.gridLookUpTO;
                    $scope.openDialog(data);
                    // $scope.getDaftarTO();
                } else if (data.RefGRTypeId == 5) {
                    // alert("Should Get data Claim Outstanding");
                    PartsGlobal.formLookupGrid = $scope.gridLookUpClaim;
                    PartsGlobal.formLookupGrid.data = $scope.getDaftarClaim({ VendorId: $scope.mData.VendorId }); //getDaftarClaim
                    $scope.openDialog(data);
                } else if (data.RefGRTypeId == 2) {
                    $scope.IsOPB = 0;
                    data.IsOPB = $scope.IsOPB
                    console.log('datanih',data);
                    $scope.getDaftarPO(data);
                } else if (data.RefGRTypeId == 3) {
                    $scope.IsOPB = 1;
                    data.IsOPB = $scope.IsOPB
                    console.log('datanih',data);
                   $scope.getDaftarPO(data);
                }else {
                    //PartsGlobal.formLookupGrid = $scope.gridLookUpPO;
                    //PartsGlobal.formLookupGrid.data = $scope.getDaftarPO();
                    //$scope.openDialog(data);
                    $scope.getDaftarPO(data);
                }
                PartsGlobal.cancelledLookupGrid = true;

                console.log("on objMaterial (data) :>", PartsGlobal.getparobjMaterial());

            }
            $scope.openDialog = function(data) {
                ngDialog.openConfirm({
                    //template:'<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Input Alasan Close PO</h3></div> <div class="panel-body"><form method="post"><div class="form-group "> <label class="control-label requiredField" style="color:black;" for="message">Alasan<span class="asteriskField">*</span></label> <textarea class="form-control" cols="40" id="message" name="message" rows="10"></textarea></div> <div class="form-group"><div> <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Kembali</button> <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()" style="color:white; background-color:#D53337">OK</button> </div></div></form></div></div>',
                    //template:'<div ng-include=\"\'app/parts/inquiry/orderStatus/templates/pendingDetail.html\'\"></div>',
                    template: '<div ng-include=\"\'app/parts/templates01/frm_lookup01b.html\'\"></div>',
                    plain: true,
                    // controller: 'GoodsReceiptController',
                    scope: $scope,
                }).then(function(value) {
                    $scope.selectedPO = PartsGlobal.selectedLookupGrid;
                    console.log("PO value : ", value);
                    console.log("PartsGlobal.cancelledLookupGrid: ", PartsGlobal.cancelledLookupGrid);
                    console.log("PO selected : ", $scope.selectedPO);
                    if (data.RefGRTypeId == 1 || data.RefGRTypeId == 2 || data.RefGRTypeId == 4)
                        $scope.appendGrid(data, $scope.selectedPO);
                    else
                        $scope.updateGrid(data, $scope.selectedPO);
                    if (PartsGlobal.cancelledLookupGrid) return 1;
                }, function(value) {
                    $scope.selectedPO = PartsGlobal.selectedLookupGrid;
                    console.log("No Data fetched ", $scope.selectedPO);
                    if (PartsGlobal.cancelledLookupGrid) return 1;
                    if (data.RefGRTypeId == 1 || data.RefGRTypeId == 2 || data.RefGRTypeId == 4)
                        $scope.appendGrid(data, $scope.selectedPO);
                    else
                        $scope.updateGrid(data, $scope.selectedPO);
                });
            }

            $scope.SearchInvoiceNo = function(data) {
                console.log("Invoice No : ", data.ReceivedInvoiceNo);
                if (data.RefGRTypeId == 1) {
                    var index = $scope.gridOptionsTAM.data.indexOf(data);
                    $scope.gridOptionsTAM.data[index]['InvoiceReceivedDate'] = PartsGlobal.getCurrDate();
                } else if (data.RefGRTypeId == 2) {
                    var index = $scope.gridOptions.data.indexOf(data);
                    $scope.gridOptions.data[index]['InvoiceReceivedDate'] = PartsGlobal.getCurrDate();
                } else if (data.RefGRTypeId == 3) {
                    var index = $scope.gridSublet.data.indexOf(data);
                    $scope.gridSublet.data[index]['InvoiceReceivedDate'] = PartsGlobal.getCurrDate();;
                }
            }
            $scope.onBeforeNew = function() {
                // [START] Gateway Page
                $scope.potamEdit = false;
                $scope.potamView = false;
                $scope.ponontamEdit = false;
                $scope.ponontamView = false;
                $scope.potamedit = false;
                $scope.potamview = false;
                $scope.posubletEdit = false;
                $scope.posubletView = false;
                $scope.toEdit = false;
                $scope.toView = false;
                $scope.isOverlayForm = true;
                $scope.Warehouse = {};
                GoodsReceipt.formApi = $scope.formApi;
                console.log("test formapi");
                // [END] Gateway Page

                $scope.formode = 1;
                $scope.isNewForm = true;
                $scope.isViewForm = '';
                $scope.isEditForm = '';

                if ($scope.potamNew || $scope.ponontamNew) {
                    $scope.gridOptionsTAM.data = [];
                    $scope.gridOptions.data = [];
                    $scope.addNewDataItem(10); // 14-09-2017 cek
                } else if ($scope.posubletNew) {
                    $scope.gridSublet.data = [];
                    $scope.addNewDataItem(30);
                } else if ($scope.toNew) {
                    $scope.gridTO.data = [];
                    $scope.addNewDataItem(40);
                }
                // $scope.gridOptions.data = $scope.dataNewItem;
                $scope.selection = '--'; //null;
                // var date = new Date();
                // $scope.FromDate = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                // $scope.mData.ReceiptDate = $scope.FromDate;
                $scope.mData.ReceiptDate = PartsGlobal.getCurrDate(); //new Date("2011-07-14 11:23:00".replace(/-/g,"/"));
                $scope.mData.GoodsReceiptNo = '-999';
                // $scope.mData.OutletId= 655; // SEMENTARA
                // $scope.mData.WarehouseId= 1;
                // if ($scope.checkRights(10)) {
                //   $scope.mData.MaterialTypeId = 1;
                // } else {
                //   $scope.mData.MaterialTypeId = 2;
                // }
                // $scope.mData.MaterialTypeId = $scope.getRightX(8, 9);


                // PartsGlobal.getWarehouse({
                //     OutletId : $scope.user.OrgId, MaterialTypeId : $scope.mData.MaterialTypeId,
                //     ServiceTypeId : $scope.mData.ServiceTypeId
                //      }).then(function(res) {
                //       $timeout(function(){
                //        $scope.Warehouse = res.data.Result[0];
                //      }, 3000)
                // });
                $scope.mData.GoodsReceiptStatusId = 1;


                $scope.newUserMode = true;
                $scope.editMode = false;
                //$scope.mode='new';
            }
            $scope.onBeforeEditMode = function() {
                console.log("---> onBeforeEditMode");
                $scope.formode = 2;
                $scope.isViewForm = '';
                $scope.potamEdit = false;
                $scope.potamView = false;
                $scope.ponontamEdit = false;
                $scope.ponontamView = false;
                $scope.posubletEdit = false;
                $scope.posubletView = false;
                $scope.toEdit = false;
                $scope.toView = false;
                $scope.potamNew = false;
                $scope.ponontamNew = false;
                $scope.posubletNew = false;
                $scope.toNew = false;
                $scope.claimEdit = false;
                $scope.claimView = false;
                $scope.claimNew = false;
                // GoodsReceipt.formApi = $scope.formApi;

                console.log("Data dari onBeforeEditMode => " + $scope.mData.RefGRTypeId);
                switch ($scope.mData.RefGRTypeId) {
                    case "PO TAM":
                        $scope.isEditForm = 'po_tam';
                        $scope.potamEdit = true;
                        break;
                    case "PO NON TAM":
                        $scope.isEditForm = 'po_nontam';
                        $scope.ponontamEdit = true;
                        break;
                    case "PO SUBLET":
                        $scope.isEditForm = 'po_sublet'
                        $scope.posubletEdit = true;
                        break;
                    case "TO":
                        $scope.isEditForm = 'grrefto';
                        $scope.toEdit = true;
                        break;
                    case "CLAIM":
                        $scope.claimEdit = true;
                        break;
                }

                $scope.isNewForm = false;
                $scope.isEditState = true;
                $scope.newUserMode = false;
                console.log("data id =>", $scope.mData.GoodsReceiptId);
                $scope.editMode = true;
            }

            $scope.onBeforeDeleteMode = function() {
                $scope.formode = 3;
                $scope.isNewForm = false;
                $scope.isEditForm = false;
                $scope.isEditClicked = true;
                console.log("mode=>", $scope.formmode);
                //$scope.mode='del';
            }


            $scope.onRefernsiGRchange = function (selected){
                console.log('onRefernsiGRchange ===>',selected);
            }

            $scope.deleteRow = function(row) {
                var dataHeader = GoodsReceipt.getGRHeader();
                console.log("Mdata",$scope.mData);
                if (dataHeader.length == 0 || typeof dataHeader == 'undefined') dataHeader = row.entity;
                console.log("Data Header :", dataHeader);
                //dataHeader.RefGITypeID = $scope.mData.RefGITypeID;
                var index;
                switch ($scope.mData.RefGRTypeId) { //gridOptionsTAM
                    case 1:
                        index = $scope.gridOptionsTAM.data.indexOf(row.entity);
                        $scope.gridOptionsTAM.data.splice(index, 1);
                        console.log('aaaaaaw');
                        break;
                    case 2:
                        index = $scope.gridOptions.data.indexOf(row.entity);
                        $scope.gridOptions.data.splice(index, 1);
                        break;
                    case 3:
                        index = $scope.gridSublet.data.indexOf(row.entity);
                        $scope.gridSublet.data.splice(index, 1);
                        break;
                    case 4:
                        index = $scope.gridTO.data.indexOf(row.entity);
                        $scope.gridTO.data.splice(index, 1);
                        // console.log('scope.gridApi.selection', $scope.gridApi.selection.getSelectedRows());
                        // var data = $scope.gridApi.selection.getSelectedRows();
                        // console.log("selected", data);
                        // angular.forEach($scope.gridApi.selection.getSelectedRows(), function(data, index) {
                        //     $scope.gridTO.data.splice($scope.gridTO.data.indexOf(data), 1);
                        // });
                        break;
                }
                // var index = $scope.gridSublet.data.indexOf(row.entity);
                console.log("Row # ", index, row);
                // if (index > 0) {index = index - 1};
                // $scope.gridSublet.data.splice(index, 1);  // gridSublet  gridOptions
            };
            $scope.goBack = function() {
                $scope.isOverlayForm = false;
                $scope.mData
                GoodsReceipt.formApi.setMode("grid");
                $scope.resetForm();
            }
            $scope.resetForm = function(){
                $scope.mData = {};
                $scope.mFilter = {};
            }

            $scope.onShowDetail = function(row, mode) {
                console.log("---> onShowDetail");
                // console.log("---> isEditClicked = " + $scope.isEditClicked);
                $scope.potamEdit = false;
                $scope.potamView = false;
                $scope.ponontamEdit = false;
                $scope.ponontamView = false;
                $scope.posubletEdit = false;
                $scope.claimEdit = false;
                $scope.posubletView = false;
                $scope.toView = false;
                $scope.claimView = false;
                $scope.potamNew = false;
                $scope.ponontamNew = false;
                $scope.posubletNew = false;
                $scope.mySelections = [];
                $scope.toNew = false;
                $scope.claimNew = false;
                GoodsReceipt.formApi = $scope.formApi;
                console.log("status GR : ", row.GoodsReceiptStatusId, $scope.grStatus);
                //$scope.grStatusName = $scope.grStatus[row.GoodsReceiptStatusId].name;
                //console.log("status GR 2 : ", $scope.grStatusName);
                if(row.VendorId == 999999){
                    row.VendorName = "OPB";
                }
                GoodsReceipt.setGRHeader(row);
                $scope.isPOTAM = false;
                console.log("data : ", $scope.GRHeader);
                $scope.CurrRowIndex = $scope.grid.data.indexOf(row);
                console.log("index data : ", $scope.CurrRowIndex);
                $scope.getDataItem(row, mode);
				
				switch ($scope.GRHeader.MaterialTypeId) {
					case 1:
						$scope.MaterialTypeS = $scope.TipeMat[0];
						break;
					case 2:
						$scope.MaterialTypeS = $scope.TipeMat[1];
						break;
				}

				console.log('$scope.MaterialType stv: ', $scope.MaterialTypeS);
						
                if (mode == 'edit') {
                    console.log("--->[Active] onBeforeEditMode");
                    $scope.formode = 3;
                    $scope.isNewForm = false;
                    $scope.isViewForm = '';
                    // console.log("Data dari onShowDetail => " + $scope.mData.RefGRTypeId);
                    // switch($scope.mData.RefGRTypeId)
                    switch (row.RefGRTypeId) { //--
                        case 1:
                        case "PO TAM":
                            $scope.isEditForm = 'po_tam';
                            $scope.potamEdit = true;
                            $scope.isPOTAM = true;
                            // $scope.gridOptions.columnDefs= $scope.columnsPOTAM ;
                            console.log("PO TAM WO");
                            break;
                        case 2:
                        case "PO NON TAM":
                            $scope.isEditForm = 'po_nontam';
                            $scope.ponontamEdit = true;
                            // $scope.gridOptions.columnDefs= $scope.columnsPONONTAM ;
                            console.log("Sales Orders");
                            break;
                        case 3:
                        case "PO SUBLET":
                            $scope.isEditForm = 'po_sublet';
                            $scope.posubletEdit = true;
                            console.log("PO SUBLET");
                            break;
                        case 4:
                        case "TO":
                            $scope.isEditForm = 'grrefto';
                            $scope.toEdit = true;
                            console.log("Transfer Order");
                            break;
                        case 5:
                        case "CLAIM":
                            // $scope.isEditForm = 'grrefto';
                            $scope.claimEdit = true;
                            console.log("Referensi Claim");
                            break;
                    } //-->
                    // console.log("mode=>",$scope.formmode);
                    //$scope.mode='del';
                    $scope.isEditClicked = false;
                } else {
                    //---------------------
                    //Preparation View Mode
                    //---------------------
                    $scope.isPOTAM = true; //.. mode view button hapus tidak muncul
                    console.log("--->[Active] onShowDetail");
                    // console.log("Data dari onShowDetail => " + $scope.mData.RefGRTypeId);
                    console.log("Data dari onShowDetail (ShippingNo)=> " + $scope.mData.ShippingNo);
                    // switch($scope.mData.RefGRTypeId)
                    switch (row.RefGRTypeId) { //--
                        case 1:
                        case "PO TAM":
                            $scope.isEditForm = 'po_tam';
                            $scope.potamView = true;
                            // $scope.gridOptions.columnDefs= $scope.columnsPOTAM ;
                            console.log("WO");
                            break;
                        case 2:
                        case "PO NON TAM":
                            $scope.isEditForm = 'po_nontam';
                            $scope.ponontamView = true;
                            // $scope.gridOptions.columnDefs= $scope.columnsPONONTAM ;
                            console.log("Sales Orders");
                            break;
                        case 3:
                        case "PO SUBLET":
                            $scope.isEditForm = 'po_sublet';
                            $scope.posubletView = true;
                            console.log("PO SUBLET");
                            break;
                        case 4:
                        case "TO":
                            $scope.isEditForm = 'grrefto';
                            $scope.toView = true;
                            console.log("Transfer Order");
                            break;
                        case 5:
                        case "CLAIM":
                            // $scope.isEditForm = 'grrefto';
                            $scope.claimView = true;
                            console.log("Referensi Claim");
                            break;
                    }
                }
            }

            //----------------------------------
            // Select Handler Gateway Form
            //----------------------------------
            $scope.changedValue = function(item) { //'PO TAM', 'PO NON TAM', 'PO SUBLET', 'TRANSFER ORDER'
                console.log("---> Overlay Form Status = " + item);
                var refid = 0;
                $scope.isOverlayForm = false;
                $scope.ref = item;
                $scope.potamNew = false;
                $scope.ponontamNew = false;
                $scope.posubletNew = false;
                $scope.toNew = false;
                $scope.isNewForm = true;
                $scope.gridOptions.data = []
                $scope.checkRefGINo = true;
                console.log("Form Mode (IsNewForm) : ", $scope.isNewForm);
                if (item == 'PO TAM') {
                    $scope.flagGR = 1;
                    $scope.isPOTAM = true;
                    // $scope.gridOptions.columnDefs= $scope.columnsPOTAM ;
                    $scope.gridOptionsTAM.data = [];
                    refid = 1;
                    $scope.potamNew = true;
                } else if (item == 'PO NON TAM') {
                    $scope.flagGR = 2;
                    $scope.ponontamNew = true;
                    // $scope.gridOptions.columnDefs= $scope.columnsPONONTAM ;
                    $scope.gridOptions.data = [];
                    refid = 2;
                } else if (item == 'PO SUBLET') {
                    $scope.flagGR = 3;
                    $scope.posubletNew = true;
                    refid = 3;
                    $scope.mData.MaterialTypeId = 1; // sementara utk cek btn save framework
                    $scope.gridSublet.data = [];
                } else if (item == 'TRANSFER ORDER') {
                    $scope.flagGR = 4;
                    $scope.toNew = true;
                    $scope.gridTO.data = [];
                    refid = 4;
                } else {
                    $scope.flagGR = 5;
                    $scope.claimNew = true;
                    $scope.gridOptions.data = [];
                    refid = 5;
                }
                //$scope.disableSelect  = true;
                $scope.mData['RefGRTypeId'] = refid;
                $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
                $scope.mData.ServiceTypeId = $scope.getRightServiceType();
                console.log("Mat Type Id. : ", $scope.mData.MaterialTypeId);
                console.log("Service Type Id. : ", $scope.mData.ServiceTypeId);
                PartsGlobal.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.vOutletId).then(function(res) {
                        var gridData = res.data.Result;
                        console.log(gridData[0]);

                        $scope.mData.WarehouseId = gridData[0].WarehouseId;
                        $scope.Warehouse = gridData[0];
                        PartsGlobal.myWarehouse = gridData[0];
                        console.log("Warehouse =>", PartsGlobal.myWarehouse);
                        switch ($scope.mData.MaterialTypeId) {
                            case 1:
                                $scope.mData.MaterialType = "Parts";
                                break;
                            case 2:
                                $scope.mData.MaterialType = "Bahan";
                                break;
                        }
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
            $scope.closeGateway = function() {
                $scope.isOverlayForm = false;
            }

            $scope.bulkAction = function(baId) {
                if (baId == 1) { // hapus
                    // if (confirm("Hapus dokumen, yakin ?")) {
                    //     // delete data items deleteMultipleItems (itemids)
                    //     GoodsReceipt.deleteMultipleItems({
                    //         itemids: $scope.itemids
                    //     }).then(function(res) { //
                    //         // PartsGlobal.showAlert({
                    //         //   title:'Penghapusan data item',
                    //         //   content : 'Data sudah terhapus (mungkin masih perlu refresh)',
                    //         //   type:'info'
                    //         // });
                    //         alert("Data terhapus");
                    //     });
                    // }
                    var result = confirm('Apakah Anda yakin menghapus data ini?');
                    console.log("result==>", result);
                    console.log("$scope.gridOptions.data",$scope.gridOptions.data);
                    if (result) {
                        console.log('scope.gridApi.selection', $scope.gridApi.selection.getSelectedRows());
                        console.log('scope.mySelections', $scope.mySelections);                        
                        // var data = $scope.gridApi.selection.getSelectedRows();
                        var data = $scope.mySelections;
                        console.log("selected", data, $scope.flagGR);
                        angular.forEach($scope.mySelections, function(data, index) {
                            if($scope.flagGR == 1){
                                var indexTask = _.findIndex($scope.gridOptionsTAM.data, {'PartsId':data.PartsId});
                                if(indexTask > -1){
                                    $scope.gridOptionsTAM.data.splice(indexTask, 1);
                                    $scope.mySelections = [];
                                }else{
                                    $scope.mySelections = [];
                                }
                            }else if($scope.flagGR == 2 || $scope.flagGR == 5 ){
                                var indexTask = _.findIndex($scope.gridOptions.data, {'PartsId':data.PartsId});
                                if(indexTask > -1){
                                    $scope.gridOptions.data.splice(indexTask, 1);
                                    $scope.mySelections = [];
                                }else{
                                    $scope.mySelections = [];
                                }
                            }else if($scope.flagGR == 3){
                                var indexTask = _.findIndex($scope.gridSublet.data, {'PartsId':data.PartsId});
                                if(indexTask > -1){
                                    $scope.gridSublet.data.splice(indexTask, 1);
                                    $scope.mySelections = [];
                                }else{
                                    $scope.mySelections = [];
                                }
                            }else if($scope.flagGR == 4){
                                var indexTask = _.findIndex($scope.gridTO.data, {'PartsId':data.PartsId});
                                if(indexTask > -1){
                                    $scope.gridTO.data.splice(indexTask, 1);
                                    $scope.mySelections = [];
                                }else{
                                    $scope.mySelections = [];
                                }
                            }
                            // $scope.gridOptionsTAM.data.splice($scope.gridOptionsTAM.data.lastIndexOf(data), 1);
                            // $scope.gridOptions.data.splice($scope.gridOptions.data.lastIndexOf(data), 1);
                            // $scope.gridSublet.data.splice($scope.gridSublet.data.lastIndexOf(data), 1);
                            // $scope.gridTO.data.splice($scope.gridTO.data.lastIndexOf(data), 1);
                        });
                    }        
                }
            }


            $scope.deleteItem = function(data) {
                console.log("data item yg akan dihapus :", data.entity);
                if ($scope.isPOTAM == 99) { alert("PO TAM Tidak boleh edit/hapus/delete"); } // krn ng-show=isPOTAM belum jalan, maka pake ini
                else
                if (confirm('Apakah Anda yakin menghapus data ini?')) {
                    $scope.deleteRow(data); //
                    //-var index = $scope.gridOptions.data.indexOf(data.entity);
                    //-$scope.gridOptions.data.splice(index, 1);
                    // GoodsReceipt.deleteDataItem(data.entity).then(function(res) {//
                    //  // PartsGlobal.showAlert({
                    //  //   title:'Penghapusan data item',
                    //  //   content : 'Data sudah terhapus (mungkin masih perlu refresh)',
                    //  //   type:'info'
                    //  // });
                    //  alert("Data terhapus");
                    // });
                    // $scope.getDataItem(data); // .. belum refresh
                    // $scope.deleteRow(data); //
                }
            };

            //----------------------------------
            // Report
            //----------------------------------
            $scope.laporan = function() { //--> perlu disesuikan dgn GR (ini masih copas dari GI)
                ngDialog.openConfirm({
                    template: '<div ng-include=\"\'app/parts/goodsissue/helper/report_dialog.html\'\"></div>',
                    plain: true,
                    // width: 800px,
                    controller: 'GoodsIssueController',
                });
            }; //-->
            $scope.simpan = function() {
                var asaldata = '';
                //alert(this.ref);
                switch ($scope.ref) { //--
                    case "PO TAM":
                        asaldata = 'po tam';
                        break;
                    case "PO NON TAM":
                        asaldata = 'po non tam';
                        break;
                    case "TRANSFER ORDER":
                        asaldata = 'Transfer Order';
                        break;
                    case "PO SUBLET":
                        asaldata = 'po sublet';
                        break;
                }
                alert("simpan data : " + asaldata);
            };


            $scope.cetakan = function(data,shippingNo) {
                var pdfFile = null;
    
                // PrintRpt.print(data).success(function(res) {
                GoodsReceipt.print(data,shippingNo).success(function(res) {
                    var file = new Blob([res], { type: 'application/pdf' });
                    var fileURL = URL.createObjectURL(file);
    
                    console.log("pdf", fileURL);
                    //$scope.content = $sce.trustAsResourceUrl(fileURL);
                    pdfFile = fileURL;
    
                    if (pdfFile != null) {
                        //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame 
                        var ua = navigator.userAgent;
                        if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                            var link = document.createElement('a');
                            link.href = fileURL;
                            //link.download="erlangga_file.pdf";
                            link.click();
                            console.log('Android If ====>',fileURL)
                        } else {
                            printJS(pdfFile);
    
                            console.log('printJS Else ====>',pdfFile)
                        }
                    } else
                        console.log("error cetakan pdfFile null", pdfFile);
                }).error(function(res) {
                    console.log("error pas nembak service", pdfFile);
                });
            };

            $scope.filterDateDDMMYYYY = function(date){
                // data yg masuk yyyy-mm-dd
                var split = date.split('-');
                return split[2] +"-"+split[1]+"-"+split[0]; // jadi dd-mm-yyyy
            }


            $scope.changeFormatDate = function(item) {
                var tmpAppointmentDate = item;
                console.log("changeFormatDate item", item);
                tmpAppointmentDate = new Date(tmpAppointmentDate);
                var finalDate
                var yyyy = tmpAppointmentDate.getFullYear().toString();
                var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                var dd = tmpAppointmentDate.getDate().toString();
                finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                console.log("changeFormatDate finalDate", finalDate);
                return finalDate;
            }

            $scope.simpanPO = function(refid, mWO) {
                console.log('simpanPO | field shipping ->', $scope.mData.ShippingNo);
                console.log('simpanPO | mwo ->', mWO);
                console.log('simpanPO | flagGR ->', $scope.flagGR);
                console.log('simpanPO | ShippingNo ->', $scope.mData.ShippingNo);
                console.log('refid', refid);

                $scope.checkRefGINo = true;

                mWO.ShippingNo = $scope.mData.ShippingNo;

                // tampung noShipping (keperluan simpanPO po-tam)
                console.log('field tampung noshipping ->', $scope.tampungNoShipping_simpanPO);
                $scope.tampungNoShipping_simpanPO = $scope.mData.ShippingNo;
                console.log('field tampung noshipping 2 ->', $scope.tampungNoShipping_simpanPO);

                console.log('$scope.mData ===>',$scope.mData);
                var detail = [];
                console.log("data (from detil form):", mWO);
                $scope.mData['DocDate'] = PartsGlobal.getCurrDate();
                $scope.mData.ReceiptDate = PartsGlobal.getCurrDate();
                $scope.mData['RefGRTypeId'] = refid;
                // if ($scope.mData.OutletId == null) $scope.mData.OutletId = $scope.vOutletId ;
                switch(refid) {
                    case 1:
                        detail = $scope.gridOptionsTAM.data;
                        break;
                    case 2:
                        detail = $scope.gridOptions.data;
                        break;
                    case 3:
                        detail = $scope.gridSublet.data;
                        break;
                    case 4:
                        $scope.mData['ShippingNo'] = '000000';
                        detail = $scope.gridTO.data;
                        break;
                }
                console.log("Data -> ... ", $scope.mData);
                if(refid == 1 || refid == 2){
                    for(var idx in detail){
                        if(detail[idx].RefPONo !== undefined && (detail[idx].RefPONo == 0 || detail[idx].RefPONo == '0' || detail[idx].RefPONo == '' || detail[idx].RefPONo == null )){
                            bsAlert.warning("Tidak bisa lanjut proses","Harap hubungi application support, data bermasalah karena RefPoNo 0 atau null");
                            return -1;
                        }
                    }
                }
                console.log('===>', refid);
                if ($scope.mData.ShippingNo == null || $scope.mData.ShippingNo === 'undefined' || $scope.mData.ShippingNo == '' ) {
                    if($scope.flagGR == 2 || $scope.flagGR == 3){
                        console.log('simpanPO | masuk sini kalo flag GR == 2 atau 3')
                    }else{
                        console.log('simpanPO | masuk sini kalo flag GR != 2 atau 3')
                        PartsGlobal.showAlert({
                            title: "Mandatory",
                            content: "No Shipping harus diisi",
                            type: 'danger'
                        });
                        return -1;
                    }

                } else if ($scope.mData.Name == null || $scope.mData.Name === 'undefined' || $scope.mData.Name == '') {
                    PartsGlobal.showAlert({
                        title: "Mandatory",
                        content: "Vendor harus diisi",
                        type: 'danger'
                    });
                    return -1;
                } else if ($scope.mData.PartsCode != $scope.mData.PartsCode ) {
                    PartsGlobal.showAlert({
                        title: "Mandatory",
                        content: "Ada Parts Subtitute",
                        type: 'danger'
                    });
                    return -1;
                } else if ($scope.mData.ReceiptDate == null || $scope.mData.ReceiptDate === 'undefined' || $scope.mData.ReceiptDate == '') {
                    PartsGlobal.showAlert({
                        title: "Mandatory",
                        content: "Tgl Terima harus diisi",
                        type: 'danger'
                    });
                    return -1;

                } else if (detail.length == 0) {
                    PartsGlobal.showAlert({
                        title: "Mandatory",
                        content: "Item goods receipt harus diisi",
                        type: 'danger'
                    });
                    return -1;
                }
                detail['PurchaseOrderId'] = 0;
                var i; // var j=0;
                var invalidQty = false;
                var invalidQtyGR = false;
                var QtyBigger = false;
                console.log("<<<Detail >>>: ", detail);
                // alert("hello  QtyGR : ", detail[0]['QtyGR']);a
                for (i = 0; i < detail.length; i++) {

                    if(detail[i]['ShelfName'] == null)
                    {

                        PartsGlobal.showAlert({
                            title: "Goods Receipt",
                            content: "Mohon lengkapi data rak terlebih dahulu. No Material "+ detail[i]['PartsCode'],
                            type: 'warning'
                        });
                        return -1;
                        
                    }
                    if(detail[i]['PurchaseOrderNo'] == null && ($scope.mData.RefGRTypeId != 4))
                    {

                        PartsGlobal.showAlert({
                            title: "Goods Receipt",
                            content: "Data Purchase Order tidak ditemukan. No Material "+ detail[i]['PartsCode'],
                            type: 'warning'
                        });
                        return -1;
                        
                    }
                    if(detail[i]['QtyOnHand'] < detail[i]['QtyGR'])
                    {

                        PartsGlobal.showAlert({
                            title: "Goods Receipt",
                            content: "No Material "+ detail[i]['PartsCode'] +" Tidak Dapat Batal GR",
                            type: 'warning'
                        });
                        return -1;
                        
                    }
                    if (detail[i]['InvoiceId'] == null) {
                        detail[i]['InvoiceId'] = -99;
                    }
                    detail[i]['UomGR'] = detail[i]['SatuanId'];
                    detail[i]['QtyBase'] = detail[i]['QtyGR'];
                    detail[i]['UomBase'] = detail[i]['UomGR'];
                    detail[i]['GoodsReceiptStatusId'] = mWO['GoodsReceiptStatusId'];
                    detail[i]['RefGRNo'] = detail[i]['RefPONo'];
                    // If PO NON SOP
                    if (detail[i]['RefPOTypeId'] == 4) {
                        detail[i]['RefGRNo'] = detail[i]['PurchaseOrderNo'];
                    }

                    if (refid == 4) {
                        detail[i]['RefGRNo'] = detail[i]['TransferOrderNo'];
                    }
                    detail[i]['WarehouseId'] =  mWO['WarehouseId'];
                    detail[i]['PartId'] = detail[i]['PartsId'] == undefined || detail[i]['PartsId'] == null ? detail[i]['PartId'] : detail[i]['PartsId'];

                    if (detail[i]['QtyGR'] == undefined || detail[i]['QtyGR'] == null || detail[i]['QtyGR'] == '') { // cek qtyGR if empty
                        invalidQtyGR = true;
                        break;
                    }
                    if (detail[i]['QtyRemain'] < detail[i]['QtyGR']) { // cek qtyGR dibandig Qty Remain
                        QtyBigger = true;
                        // break;
                    }
                    if (detail[i]['QtyGR'] < 0) { // cek qtyGR dibandig Qty Remain
                        invalidQty = true;
                        //break;
                    }

                    if(refid == 1){
                       

                        detail[i]['InvoiceNo'] = detail[i]['VendorInvoiceNo'];
                        detail[i]['InvoiceDate'] = detail[i]['VendorInvoiceDate'];
                    }
                    if(detail[i]['InvoiceDate']){
                        detail[i]['InvoiceDate'] = $scope.changeFormatDate(detail[i]['InvoiceDate'])
                    }
                }

                // if qtyGR is empty
                if (invalidQtyGR) {
                    PartsGlobal.showAlert({
                        title: "Goods Receipt",
                        content: "Data Quantity GR ada yg tidak valid ",
                        type: 'warning'
                    });
                    setTimeout(function(){
                        $scope.checkRefGINo = false;
                    },1000);
                    return -1;
                }

                /*  Pembatasan QtyGR < QtyPO  dinonaktifkan, diganti alert aja -- 21-11-2017 */
                /* DI NON AKTIFKAN DAN SUDAH KONFIRMASI ANALAIS UNTUK KASUS GR, QTY GR BOLEH LEBIH DARI QTY PO --20-09-2018 */
                // if (QtyBigger) {
                //     PartsGlobal.showAlert({
                //         title: "Goods Receipt",
                //         content: "Data Quantity GR lebih besar dari Qty Remain",
                //         type: 'danger'
                //     });
                //     return -1;
                // }

                /*  Pembatasan QtyGR < QtyPO  dinonaktifkan, diganti alert aja -- 21-11-2017 */
                /* DI NON AKTIFKAN DAN SUDAH KONFIRMASI ANALAIS UNTUK KASUS GR, QTY GR BOLEH LEBIH DARI QTY PO --09-10-2018 */
                // if (invalidQty) {
                //     PartsGlobal.showAlert({
                //         title: "Goods Receipt",
                //         content: "Data Quantity GR ada yg tidak valid ",
                //         type: 'warning'
                //     });
                //     return -1;
                // }
                console.log("data sebelum kirim :", $scope.mData);
                console.log("Taraktakdung =>",detail)

                //$scope.mData = mWO;
                // $scope.mData.MaterialTypeId = PartsLookup.lkp_MaterialTypeId($scope.mData.MaterialTypeId);
                $scope.mData['MaterialTypeId'] = PartsLookup.lkp_MaterialTypeId($scope.mData['MaterialTypeId']) //$scope.mData.MaterialType.id;
                console.log("Data to Backend ==>", mWO);
                console.log($scope.mData);
                // GoodsReceipt.create($scope.mData);


                if (refid != 4) {
                    GoodsReceipt.createData($scope.mData, detail).then(function(res) {
                            var insertResponse = res.data;
                            console.log("insertResponse => ", res.data);
                            PartsGlobal.showAlert({
                              title: "Goods Receipt",
                              content: "Data berhasil disimpan",
                              type: 'success'
                              });
                            $scope.alertAfterSave(insertResponse);
                            setTimeout(function(){
                                $scope.checkRefGINo = false;
                            },1000);
                            GoodsReceipt.formApi.setMode("grid");
                            $scope.hapusFilter();

                            var tmp = insertResponse.ResponseMessage.split("[");
                            var resultGRid = tmp[1].split("]");


                            //tambahan cr4
                            if (refid == 1) {
                                // panggil tampung noshipping + pasang ke mWO
                                console.log("tampung 1650 => ", $scope.tampungNoShipping_simpanPO);
                                console.log("mwo 1651 => ", mWO);
                                mWO.ShippingNo = $scope.tampungNoShipping_simpanPO;
                                console.log("mwo.shipping 1653 => ", mWO.ShippingNo);
                                
                                // panggil service cetakan
                                console.log("refid 1 | res.data ======>",res.data);
                                console.log("refid 1 | mWO ===========>",mWO);
                                console.log("refid 1 | refid =========>",refid);
                                console.log("refid 1 | DocDate =======>",mWO.DocDate);
                                console.log("refid 1 | RefGRTypeId ===>",mWO.RefGRTypeId);
                                console.log("refid 1 | VendorId ======>",mWO.VendorId);
                                console.log("refid 1 | VendorName ====>",mWO.VendorName);
                                console.log("refid 1 | ShippingNo ====>",mWO.ShippingNo);
                                console.log("refid 1 | MaterialTypeId=>",mWO.MaterialTypeId);

                                var DateDDMMYYYY = $scope.filterDateDDMMYYYY(mWO.DocDate); 
                                
                                // https://dms-sit.toyota.astra.co.id/api/as/PrintGRP/1/S0419/1306/2020-06-18/1
								// var GenerateURLPrint = "as/PrintGRP/"+ refid+ "/"+mWO.ShippingNo+"/"+mWO.VendorId+"/"+mWO.DocDate+"/"+mWO.MaterialTypeId+"/"+resultGRid[0];
                                //var GenerateURLPrint = "as/PrintGRP/"+ refid+ "/"+mWO.ShippingNo+"/"+mWO.VendorId+"/"+mWO.DocDate+"/"+mWO.MaterialTypeId;
                                // var GenerateURLPrint = "rpt/AS_GoodReceiptParts/" + refid+ "/"+mWO.ShippingNo+"/"+mWO.VendorName+"/"+mWO.DocDate+"/"+mWO.MaterialTypeId;
                                // $scope.cetakan(GenerateURLPrint);

                                var GenerateURLPrint = "as/PrintGRP/"+ refid+ "/"+mWO.VendorId+"/"+mWO.DocDate+"/"+mWO.MaterialTypeId+"/"+resultGRid[0];
                                $scope.cetakan(GenerateURLPrint,mWO.ShippingNo);
                            }
                            //tambahan cr4

                            // tambahan cr 920
                            else if (refid == 2) {
                                // panggil tampung noshipping + pasang ke mWO
                                console.log("tampung 1694 => ", $scope.tampungNoShipping_simpanPO);
                                console.log("mwo 1695 => ", mWO);
                                mWO.ShippingNo = $scope.tampungNoShipping_simpanPO;
                                console.log("mwo.shipping 1697 => ", mWO.ShippingNo);
                                
                                // panggil service cetakan
                                console.log("refid 1 | res.data ======>",res.data);
                                console.log("refid 1 | mWO ===========>",mWO);
                                console.log("refid 1 | refid =========>",refid);
                                console.log("refid 1 | DocDate =======>",mWO.DocDate);
                                console.log("refid 1 | RefGRTypeId ===>",mWO.RefGRTypeId);
                                console.log("refid 1 | VendorId ======>",mWO.VendorId);
                                console.log("refid 1 | VendorName ====>",mWO.VendorName);
                                console.log("refid 1 | ShippingNo ====>",mWO.ShippingNo);
                                console.log("refid 1 | MaterialTypeId=>",mWO.MaterialTypeId);

                                var DateDDMMYYYY = $scope.filterDateDDMMYYYY(mWO.DocDate); 
                                
								// var GenerateURLPrint = "as/PrintGRP/"+ refid+ "/"+mWO.ShippingNo+"/"+mWO.VendorId+"/"+mWO.DocDate+"/"+mWO.MaterialTypeId+"/"+resultGRid[0];
                                //var GenerateURLPrint = "as/PrintGRP/"+ refid+ "/"+mWO.ShippingNo+"/"+mWO.VendorId+"/"+mWO.DocDate+"/"+mWO.MaterialTypeId;
                                // $scope.cetakan(GenerateURLPrint);
                               
                                var GenerateURLPrint = "as/PrintGRP/"+ refid+ "/"+mWO.VendorId+"/"+mWO.DocDate+"/"+mWO.MaterialTypeId+"/"+resultGRid[0];
                                $scope.cetakan(GenerateURLPrint,mWO.ShippingNo);
                            }
                            //tambahan cr4

                            // tambahan cr 920
                            else if (refid == 3) {
                                // panggil tampung noshipping + pasang ke mWO
                                console.log("tampung 1694 => ", $scope.tampungNoShipping_simpanPO);
                                console.log("mwo 1695 => ", mWO);
                                mWO.ShippingNo = $scope.tampungNoShipping_simpanPO;
                                console.log("mwo.shipping 1697 => ", mWO.ShippingNo);
                                
                                // panggil service cetakan
                                console.log("refid 1 | res.data ======>",res.data);
                                console.log("refid 1 | mWO ===========>",mWO);
                                console.log("refid 1 | refid =========>",refid);
                                console.log("refid 1 | DocDate =======>",mWO.DocDate);
                                console.log("refid 1 | RefGRTypeId ===>",mWO.RefGRTypeId);
                                console.log("refid 1 | VendorId ======>",mWO.VendorId);
                                console.log("refid 1 | VendorName ====>",mWO.VendorName);
                                console.log("refid 1 | ShippingNo ====>",mWO.ShippingNo);
                                console.log("refid 1 | MaterialTypeId=>",mWO.MaterialTypeId);

                                var DateDDMMYYYY = $scope.filterDateDDMMYYYY(mWO.DocDate); 
                                
                                // var GenerateURLPrint = "as/PrintGRP/"+ refid+ "/"+mWO.ShippingNo+"/"+mWO.VendorId+"/"+mWO.DocDate+"/"+mWO.MaterialTypeId+"/"+resultGRid[0];
								//var GenerateURLPrint = "as/PrintGRP/"+ refid+ "/"+mWO.ShippingNo+"/"+mWO.VendorId+"/"+mWO.DocDate+"/"+mWO.MaterialTypeId;
                                // $scope.cetakan(GenerateURLPrint);
                               
                                var GenerateURLPrint = "as/PrintGRP/"+ refid+ "/"+mWO.VendorId+"/"+mWO.DocDate+"/"+mWO.MaterialTypeId+"/"+resultGRid[0];
                                $scope.cetakan(GenerateURLPrint,mWO.ShippingNo);
                            }
                            //tambahan cr 920

                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
                }else {

                    GoodsReceipt.createDataTO($scope.mData, detail).then(function(res) {
                            var insertResponse = res.data;
                            console.log("insertResponse => ", res.data);
                            // PartsGlobal.showAlert({
                            //   title: "Goods Receipt",
                            //   content: "Data berhasil disimpan",
                            //   type: 'success'
                            //   });
                            $scope.alertAfterSave(insertResponse);
                            setTimeout(function(){
                                        $scope.checkRefGINo = false;
                                    },1000);
                            // GoodsReceipt.formApi.setMode("grid");
                            $scope.hapusFilter();

                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
                }

                GoodsReceipt.formApi.setMode("grid");
            }

            $scope.GRBatalNext = function() {
                console.log("Current Header b4 b4: ", $scope.grid.data[$scope.CurrRowIndex]);
                $scope.grid.data[$scope.CurrRowIndex]['GoodsReceiptStatusId'] = 2;
                console.log("Current Header b4 : ", $scope.grid.data[$scope.CurrRowIndex]);
            }
            $scope.batalDOk = function(refid) {
                $scope.doubleclickbatal = 0;
                console.log("data batal ", $scope.GRHeader);
                console.log("Data -1: ", $scope.grid.data);

                console.log("$scope.gridOptionsTAM.data=>",$scope.gridOptionsTAM.data);
                console.log("$scope.gridOptions.data=>",$scope.gridOptions.data);
                console.log("$scope.gridSublet.data=>",$scope.gridSublet.data);
                console.log("$scope.gridTO.data=>",$scope.gridTO.data);
                console.log("$scope.gridApi.data=>",$scope.gridApi.data);

                var detail = [];
                switch (refid) {
                    case 10: 
                        detail = $scope.gridOptionsTAM.data;
                        break;
                    case 20: 
                        detail = $scope.gridOptions.data;
                        break;
                    case 30: 
                        detail = $scope.gridSublet.data;
                        break;
                    case 40: 
                        detail = $scope.gridTO.data;
                        break;
                }
                   
                
                console.log("Data -> ... ", $scope.mData);
                console.log("detail=>",detail);
                var i = 0;

                PartsGlobal.DataList = $scope.grid.data;
                PartsGlobal.DataIndex = $scope.CurrRowIndex;

                if($scope.mData.RefGRTypeId == 1 || $scope.mData.RefGRTypeId == 2 ){

                    if ($scope.GRHeader.GoodsReceiptStatusId == 2) {
                        PartsGlobal.showAlert({
                            title: "Goods Receipt",
                            content: "Dokumen sudah dibatalkan ",
                            type: 'info'
                        });
                        return 0;
                    }

                    // for (i = 0; i < detail.length; i++) {
                    //     if(detail[i]['QtyOnHand'] < detail[i]['QtyGR']) {
                    //         PartsGlobal.showAlert({
                    //                 title: "Goods Receipt",
                    //                 content: "Batal GR Tidak Bisa Dilakukan",
                    //                 type: 'warning'
                    //         });
                    //         return 0;                                
                    //     }
                    //     console.log("sini =>",$scope.CurrRowIndex)
                    // }
                }
            
                GoodsReceipt.cekInvoice($scope.mData.GoodsReceiptNo).then(function(res){
                    console.log("res=>",res);
                    var resInv = res.data;
                    var lengthInv = resInv.length;

                    if (lengthInv > 0) {
                        bsAlert.warning({
                            title: "Goods Receipt",
                            text: "Goods Receipt tidak dapat dibatalkan, sudah ada invoice masuk nomor "+resInv[0].InvoiceNo+". Batalkan invoice masuk terlebih dahulu",
                            type: "warning",
                            showCancelButton: false
                        });
                    } else {
                        GoodsReceipt.cekStock($scope.mData.GoodsReceiptId).then(function(res){
                            console.log("res=>",res);
                            var resData = res.data;
                            var lengthData = resData.length;
                            if(lengthData > 0) {
                                if(resData[0].IsGI == 1) {
                                    PartsGlobal.showAlert({
                                        title: "Goods Receipt",
                                        content: "Batal GR Tidak Bisa Dilakukan. Sudah proses GI!!",
                                        type: 'warning'
                                });
                                } else {
                                    PartsGlobal.showAlert({
                                        title: "Goods Receipt",
                                        content: "Batal GR Tidak Bisa Dilakukan. QtyFree kurang!!",
                                        type: 'warning'
                                });
                                }                        
                            } else {
                                console.log("inputAlasanBatal");          
                                PartsGlobal.inputAlasanBatal({
                                    controller: 'GoodsReceiptController',
                                    dokdata: $scope.GRHeader,
                                    doctype: 'GR' //, docid : $scope.GRHeader.GoodsReceiptId
                                });
                            }
                        });
                    }
                });

                // if (refid==10) { // potam
                //   console.log("po tam");
                //   PartsGlobal.batalDok({
                //     doctype : 'GR', docid : $scope.GRHeader.GoodsReceiptId
                //   });
                // }
            }
            $scope.alertAfterSave = function(item) {
                var noGR = item.ResponseMessage;
                noGR = noGR.substring(noGR.lastIndexOf('#') + 1);
                bsAlert.alert({
                        title: "Data tersimpan dengan Nomor Goods Receipt",
                        text: noGR,
                        type: "success",
                        showCancelButton: false
                    },
                    function() {
                        $scope.formApi.setMode('grid');
                        $scope.hapusFilter();
                        //$scope.goBack();
                    },
                    function() {

                    }
                )
            }

            $scope.doubleclickbatal = 0;
            $scope.executeBatalDok = function() {
                // alert("Dokumen dibatalkan ") ;
                // console.log("Data index : ", $scope.CurrRowIndex); //PartsGlobal.DataList
                if($scope.doubleclickbatal == 0){
                    $scope.doubleclickbatal++
                    console.log("Data 0B : ", PartsGlobal.DataList);
                    $scope.grid.data = PartsGlobal.DataList;
                    $scope.CurrRowIndex = PartsGlobal.DataIndex;
                    PartsGlobal.batalDok({
                        doctype: 'GR',
                        docid: $scope.GRHeader.GoodsReceiptId
                    }).then(function(res) {
                        // $scope.ngDialog.close();
                        // console.log("Data 1: ", $scope.grid.data);
                        $scope.resetForm();
                        PartsGlobal.showAlert({
                            title: "Goods Receipt",
                            content: "Dokumen dibatalkan ",
                            type: 'info'
                        });
                        $scope.mData = {};
                        $scope.hapusFilter();
                        GoodsReceipt.formApi.setMode("grid");
                        // $scope.GRBatalNext();
                        $scope.ngDialog.close();
                        // console.log("Data 2: ", $scope.grid.data);
                        $scope.grid.data[$scope.CurrRowIndex]['GoodsReceiptStatusId'] = 2;
                        
    
                    });

                }
          
                // $scope.GRHeader.GoodsReceiptStatusId = 0;
            };
            $scope.getSelectedItemIds = function(item) {
                var res = ';';
                for (var i = 0; i < item.length; i++) {
                    res += item[i].GoodsReceiptItemId + ';'; //.toString;
                }
                return res;
            };

            /*
    var strTemplateAction= '<div ng-show="!grid.appScope.$parent.isPOTAM"> <center><button class="btn rbtn" ng-click="grid.appScope.$parent.deleteItem(row)"><i class="fa fa-edit"></i></button></center></div>';
    $scope.columnsPOTAM = [
          { name:'No. GR', field: 'GoodsReceiptId', visible: false },
          { name:'No. GR', field: 'GoodsReceiptItemId', visible: false },
          { name:'No. PO', field: 'PurchaseOrderId', visible: false },
          { name:'No. PO', field: 'RefGRNo', displayName: 'No. PO', enableCellEdit: $scope.potamNew, minWidth: 150, width: "*"}, //cellEditableCondition: $scope.isPOTAM},
          // { name:'No. PO', field: 'RefGRNo', displayName: 'No. PO', cellTemplate: cellPONo, enableCellEdit: $scope.potamNew, minWidth: 150, width: "*"},
          { name:'No. Inv', field: 'InvoiceId', visible: false}, //ReceivedInvoiceNo
          { name:'No. Inv', field: 'InvoiceNo', cellTemplate: cellInvoiceNo, enableCellEdit: $scope.potamNew, minWidth: 150, width: "*"},
          { name:'Tanggal Inv', field: 'InvoiceDate', enableCellEdit : false, minWidth: 100 },
          // { name:'No. Referensi SOP', field: 'RefPONo', displayName: 'No. Referensi SOP', enableCellEdit : $scope.potamNew, minWidth: 150, width: "*" },
          { name:'No. Referensi SOP', field: 'NoRefSOP', displayName: 'No. Referensi SOP', enableCellEdit : $scope.potamNew, minWidth: 150, width: "*" },//
          { name:'No. Material', field: 'PartId', visible: false},
          { name:'No. Material', field: 'PartsCode', enableCellEdit: true, minWidth: 100},
          // { name:'No. Material', field: 'PartsCode', cellTemplate: cellPartNo, enableCellEdit: true, minWidth: 100}, //$scope.isNewForm},
          { name:'Nama Material', field: 'PartsName', enableCellEdit : false, minWidth: 150, width: "*"},
          { name:'Qty PO', field: 'QtyPO', displayName: 'Qty PO', enableCellEdit : false, minWidth: 100, width: "*"},
          { name:'Qty Remain', field: 'QtyRemain', enableCellEdit : false, minWidth: 100, width: "*"},
          { name:'Qty GR', field: 'QtyGR', displayName: 'Qty GR', enableCellEdit :true, minWidth: 100, width: "*"},
          { name:'Satuan', field: 'UomPOId', enableCellEdit : false, minWidth: 100, width: "*"},
          { name:'Lokasi Gudang', field: 'WarehouseName', enableCellEdit : false, minWidth: 150, width: "*"},
          { name:'Lokasi Rak', field: 'LokasiRak', enableCellEdit : false, minWidth: 150, width: "*"},
          { name:'GoodsReceiptStatusId', field: 'GoodsReceiptStatusId', visible: false, minWidth: 150, width: "*"}
          // ,
          // { name: 'Action',
          //   cellTemplate: strTemplateAction, minWidth: 100, width: "*"//,
          //   //visible : true //grid.appScope.$parent.isPOTAM //true //grid.appScope.$parent.ref!='PO TAM'
          // }
        ];
    $scope.columnsPONONTAM = [
          { name:'No. GR', field: 'GoodsReceiptId', visible: false },
          { name:'No. GR', field: 'GoodsReceiptItemId', visible: false },
          { name:'No. PO', field: 'PurchaseOrderId', visible: false },
          { name:'No. PO', field: 'RefGRNo', displayName: 'No. PO', enableCellEdit: $scope.potamNew, minWidth: 150, width: "*"}, //cellEditableCondition: $scope.isPOTAM},
          // { name:'No. PO', field: 'RefGRNo', displayName: 'No. PO', cellTemplate: cellPONo, enableCellEdit: $scope.potamNew, minWidth: 150, width: "*"},
          { name:'No. Inv', field: 'InvoiceId', visible: false}, //ReceivedInvoiceNo
          { name:'No. Inv', field: 'InvoiceNo', cellTemplate: cellInvoiceNo, enableCellEdit: $scope.potamNew, minWidth: 150, width: "*"},
          { name:'Tanggal Inv', field: 'InvoiceDate', enableCellEdit : false, minWidth: 100 },
          // { name:'No. Referensi SOP', field: 'RefPONo', displayName: 'No. Referensi SOP', enableCellEdit : $scope.potamNew, minWidth: 150, width: "*" },
          { name:'No. Referensi SOP', field: 'NoRefSOP', displayName: 'No. Referensi SOP', enableCellEdit : $scope.potamNew, minWidth: 150, width: "*" },//
          { name:'No. Material', field: 'PartId', visible: false},
          { name:'No. Material', field: 'PartsCode', enableCellEdit: true, minWidth: 100},
          // { name:'No. Material', field: 'PartsCode', cellTemplate: cellPartNo, enableCellEdit: true, minWidth: 100}, //$scope.isNewForm},
          { name:'Nama Material', field: 'PartsName', enableCellEdit : false, minWidth: 150, width: "*"},
          { name:'Qty PO', field: 'QtyPO', displayName: 'Qty PO', enableCellEdit : false, minWidth: 100, width: "*"},
          { name:'Qty Remain', field: 'QtyRemain', enableCellEdit : false, minWidth: 100, width: "*"},
          { name:'Qty GR', field: 'QtyGR', displayName: 'Qty GR', enableCellEdit :true, minWidth: 100, width: "*"},
          { name:'Satuan', field: 'UomPOId', enableCellEdit : false, minWidth: 100, width: "*"},
          { name:'Lokasi Gudang', field: 'WarehouseName', enableCellEdit : false, minWidth: 150, width: "*"},
          { name:'Lokasi Rak', field: 'LokasiRak', enableCellEdit : false, minWidth: 150, width: "*"},
          { name:'GoodsReceiptStatusId', field: 'GoodsReceiptStatusId', visible: false, minWidth: 150, width: "*"}
          ,
          { name: 'Action',
            cellTemplate: strTemplateAction, minWidth: 100, width: "*"//,
            //visible : true //grid.appScope.$parent.isPOTAM //true //grid.appScope.$parent.ref!='PO TAM'
          }
        ];
*/
            //----------------------------------
            // Grid Setup
            //----------------------------------
            var celltmp = '<div> {{grid.appScope.$parent.grStatus[row.entity.GoodsReceiptStatusId].name}} </div>';
            var celltmprefGR = '<div> {{grid.appScope.$parent.refGR[row.entity.RefGRTypeId-1].name}} </div>';
            var celltmpMatType = '<div> {{grid.appScope.$parent.MaterialType[row.entity.MaterialTypeId-1].name}} </div>';
            $scope.gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
                // '<a href="#" uib-tooltip="Buat" tooltip-placement="bottom" ng-click="grid.appScope.actView(row.entity)" style="">Appointment</a>'+
                '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Lihat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
                '<a href="#" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Edit" ng-hide="row.entity.GoodsReceiptStatusId==2 || grid.appScope.$parent.user.RoleId==1128"  tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
                '</div>';
            $scope.grid = {
                enableSorting: true,
                enableRowSelection: true,
                multiSelect: true,
                enableSelectAll: true,
                //enableColumnResizing: true,
                //showTreeExpandNoChildren: true,
                // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
                // paginationPageSize: 15,
                columnDefs: [{
                            name: 'Id',
                            field: 'Id',
                            width: '7%',
                            visible: false
                        },
                        {
                            name: 'Tipe Material',
                            field: 'MaterialTypeId',
                            width: '6%',
                            cellTemplate: celltmpMatType
                        },
                        {
                            name: 'Referensi GR',
                            field: 'RefGRTypeId',
                            width: '10%',
                            displayName: 'Referensi GR',
                            cellTemplate: celltmprefGR
                        },
                        {
                            name: 'Tanggal GR',
                            field: 'DocDate',
                            width: '8%',
                            cellFilter: dateFilter,
                            displayName: 'Tanggal GR'
                        },
                        {
                            name: 'No. GR',
                            field: 'GoodsReceiptNo',
                            width: '13%',
                            allowCellFocus: false,
                            displayName: 'No. GR'
                        },
                        {
                            name: 'No. Cancel GR',
                            field: 'CancelNo',
                            width: '13%',
                            displayName: 'No. Cancel GR'
                        },
                        {
                            name: 'No. Shipping / Surat Jalan',
                            field: 'ShippingNo',
                            allowCellFocus: false,
                            width: '7%'
                        },
                        {
                            name: 'VendorId',
                            field: 'VendorId',
                            width: '20%',
                            visible: false
                        },
                        {
                            name: 'Vendor',
                            field: 'VendorName',
                            width: '13%'
                        },
                        {
                            name: 'Bengkel',
                            field: 'OutletName', //'OutletId',
                            width: '13%'
                        },
                        {
                            name: 'Status',
                            field: 'GoodsReceiptStatusId',
                            cellTemplate: celltmp,
                            width: '6%'
                        },
                        // {
                        //     name: 'Action',
                        //     // width: '8%',
                        //     width: 99,
                        //     pinnedRight: true,
                        //     cellTemplate: $scope.gridActionButtonTemplate
                        // }        
                    ]
                    
                    // ,
                    // data : [
                    //           {
                    //             "Id": 1,
                    //             "MaterialTypeId": "Parts",
                    //             "RefGRTypeId": "1",
                    //             "DocDate" : "WO",
                    //             "GoodsReceiptNo" : "1234567890",
                    //             "ShippingNo" : "12345",
                    //             "VendorId" : "SPLD",
                    //             "OutletId" : "Kill",
                    //             "GoodsReceiptStatusId" : "Completed"
                    //           },
                    //           {
                    //             "Id": 2,
                    //             "MaterialTypeId": "Parts",
                    //             "RefGRTypeId": "2",
                    //             "DocDate" : "WO",
                    //             "GoodsReceiptNo" : "1234567890",
                    //             "ShippingNo" : "",
                    //             "VendorId" : "SPLD",
                    //             "OutletId" : "Kill",
                    //             "GoodsReceiptStatusId" : "Completed"
                    //           },
                    //           {
                    //             "Id": 3,
                    //             "MaterialTypeId": "Parts",
                    //             "RefGRTypeId": "1",
                    //             "DocDate" : "Sales Orders",
                    //             "GoodsReceiptNo" : "1234567890",
                    //             "ShippingNo" : "",
                    //             "VendorId" : "SPLD",
                    //             "OutletId" : "Kill",
                    //             "GoodsReceiptStatusId" : "Completed"
                    //           },
                    //           {
                    //             "Id": 4,
                    //             "MaterialTypeId": "Parts",
                    //             "RefGRTypeId": "3",
                    //             "DocDate" : "Transfer Order",
                    //             "GoodsReceiptNo" : "1234567890",
                    //             "ShippingNo" : "",
                    //             "VendorId" : "SPLD",
                    //             "OutletId" : "Kill",
                    //             "GoodsReceiptStatusId" : "Completed"
                    //           },
                    //           {
                    //             "Id": 5,
                    //             "MaterialTypeId": "Parts",
                    //             "RefGRTypeId": "4",
                    //             "DocDate" : "Parts Return",
                    //             "GoodsReceiptNo" : "1234567890",
                    //             "ShippingNo" : "00123",
                    //             "VendorId" : "SPLD",
                    //             "OutletId" : "Kill",
                    //             "GoodsReceiptStatusId" : "Completed"
                    //           },
                    //           {
                    //             "Id": 6,
                    //             "MaterialTypeId": "Parts",
                    //             "RefGRTypeId": "2",
                    //             "DocDate" : "WO",
                    //             "GoodsReceiptNo" : "1234567890",
                    //             "ShippingNo" : "",
                    //             "VendorId" : "SPLD",
                    //             "OutletId" : "Kill",
                    //             "GoodsReceiptStatusId" : "Completed"
                    //           }

                //       ]
            };


            //tambahan cr4
            $scope.gridActionTemplate = '<div class="ui-grid-cell-contents">' +
            '<a href="#" ng-click="grid.appScope.actView(row.entity)" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:5px 8px 8px 0px;margin-left:10px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.actEdit(row.entity)" ng-hide="row.entity.GoodsReceiptStatusId==2 || grid.appScope.$parent.user.RoleId==1128" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:5px 8px 8px 0px;margin-left:10px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.$parent.printEstimationBtn(row.entity)" ng-hide="row.entity.RefGRTypeId != 1 && row.entity.RefGRTypeId != 2 && row.entity.RefGRTypeId != 3" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-print fa-lg" style="padding:5px 8px 8px 0px;margin-left:10px;"></i></a>' +
            '</div>';

            // BLM FIX
            // '<a href="#" ng-click="grid.appScope.$parent.printEstimationBtn(row.entity)" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-print fa-lg" style="padding:5px 8px 8px 0px;margin-left:10px;"></i></a>' +

            $scope.formatDate = function (date) {
                var d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();
        
                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;
        
                // return [day, month, year].join('-');
                return [ year, month, day].join('-');
            };

            $scope.printEstimationBtn = function(data){
                data.DocDate = $scope.formatDate(data.DocDate)
                console.log('printEstimationBtn | Data ===>',data)
                console.log("printEstimationBtn | RefGRTypeId ===>",data.RefGRTypeId);
                console.log("printEstimationBtn | DocDate =======>",data.DocDate);
                console.log("printEstimationBtn | VendorId ======>",data.VendorId);
                console.log("printEstimationBtn | VendorName ====>",data.VendorName);
                console.log("printEstimationBtn | ShippingNo ====>",data.ShippingNo);
                console.log("printEstimationBtn | MaterialTypeId=>",data.MaterialTypeId);
                
				// var GenerateURLPrint = "as/PrintGRP/"+ data.RefGRTypeId+ "/"+encodeURIComponent(data.ShippingNo)+"/"+data.VendorId+"/"+data.DocDate+"/"+data.MaterialTypeId+"/"+data.GoodsReceiptId;
				// var GenerateURLPrint = "as/PrintGRP/"+ data.RefGRTypeId+ "/"+data.ShippingNo+"/"+data.VendorId+"/"+data.DocDate+"/"+data.MaterialTypeId+"/"+data.GoodsReceiptId;
                //var GenerateURLPrint = "as/PrintGRP/"+ data.RefGRTypeId+ "/"+data.ShippingNo+"/"+data.VendorId+"/"+data.DocDate+"/"+data.MaterialTypeId;
                // var GenerateURLPrint = "as/PrintGoodReceipt?RefGR=" + data.RefGRTypeId+ "&NoShipping="+data.ShippingNo+"&Vendor="+data.VendorId+"&Date="+data.DocDate+"&TypeMaterial="+data.MaterialTypeId;
                // $scope.cetakan(GenerateURLPrint);

				var GenerateURLPrint = "as/PrintGRP/"+ data.RefGRTypeId+ "/"+data.VendorId+"/"+data.DocDate+"/"+data.MaterialTypeId+"/"+data.GoodsReceiptId;
                $scope.cetakan(GenerateURLPrint,data.ShippingNo);
            }
            //tambahan cr4


            //----------------------------------
            // Dummy WO ..
            //----------------------------------

            // var strTemplateAction= '<div ng-show="!grid.appScope.$parent.isPOTAM"> <center><button class="btn rbtn" ng-click="grid.appScope.$parent.deleteItem(row.entity)"><i class="fa fa-edit"></i></button></center></div>';
            //-var strTemplateAction= '<div ng-show="!grid.appScope.$parent.isPOTAM"> <center><button class="btn rbtn" ng-click="grid.appScope.$parent.deleteItem(row)"><i class="fa fa-edit"></i></button></center></div>';
            var cellPartNo = '<div><div class="input-group"><input type="text"  ng-model="row.entity.PartsCode" ng-value="row.entity.PartsCode">\
                      <label class="input-group-btn"><span class="btn wbtn"  ng-click="grid.appScope.SearchMaterial(row.entity)">\
                      <i class="fa fa-search fa-1"></i></span>\
                      </label></div></div>'; //isNewForm
            var cellPONo = '<div><div class="input-group"><input type="text" ng-show="grid.appScope.isNewForm" ng-model="row.entity.RefGRNo" ng-value="row.entity.RefGRNo">\
                      <label class="input-group-btn"><span class="btn wbtn" ng-show="grid.appScope.isNewForm" ng-click="grid.appScope.SearchPO(row.entity)">\
                      <i class="fa fa-search fa-1"></i></span>\
                      </label></div></div>';
            var cellInvoiceNo = '<div><div class="input-group"><input type="text" ng-show="grid.appScope.isNewForm" ng-model="row.entity.ReceivedInvoiceNo" ng-value="row.entity.ReceivedInvoiceNo">\
                      <label class="input-group-btn"><span class="btn wbtn" ng-show="grid.appScope.isNewForm" ng-click="grid.appScope.SearchInvoiceNo(row.entity)">\
                      <i class="fa fa-search fa-1"></i></span>\
                      </label></div></div>';

            $scope.gridOptionsTAM = { // yg ini untuk TAM
                enableSorting: true,
                enableRowSelection: true,
                multiSelect: true,
                enableColumnResize: true,
                enableColumnResizing: true,
                // selectedItems: console.log($scope.mySelections),
                enableSelectAll: true,
                onRegisterApi: function(gridApi) {
                    $scope.gridApi = gridApi;
                    gridApi.selection.on.rowSelectionChanged($scope, function(rows) {
                        $scope.mySelections = gridApi.selection.getSelectedRows();
                        console.log($scope.mySelections); // ...
                        //build aray string
                        $scope.itemids = $scope.getSelectedItemIds($scope.mySelections);
                        console.log("item ids : ", $scope.itemids);
                    });
                    gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
                        $scope.mySelections = gridApi.selection.getSelectedRows();
                        console.log($scope.mySelections);
                    });
                    gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {
                        if(rowEntity.QtyGR > rowEntity.xQtyRemain){
                            PartsGlobal.showAlert({
                                title: "Goods Receipt",
                                content: "Quantity GR tidak boleh melebihi Quantity Remain",
                                type: 'danger'
                            });
                            rowEntity.QtyGR = rowEntity.QtyRemain
                            return false;
                        }else if(rowEntity.QtyGR > rowEntity.ShipmentQty){
                            PartsGlobal.showAlert({
                                title: "Goods Receipt",
                                content: "Quantity GR tidak boleh melebihi Quantity Shipping",
                                type: 'danger'
                            });
                            rowEntity.QtyGR = rowEntity.QtyRemain
                            return false;
                        }
                    });
                },
                // backto first 12102017 /*
                columnDefs: [
                        { name: 'No. GR', field: 'GoodsReceiptId', visible: false },
                        { name: 'No. GR', field: 'GoodsReceiptItemId', visible: false },
                        { name: 'No. PO', field: 'PurchaseOrderId', visible: false },
                        //Kata Pak Rully seharusnya tidak boleh
                        { name: 'nopo', field: 'PurchaseOrderNo', suppressRemoveSort: true, sort: { direction: uiGridConstants.ASC, priority: 0 }, displayName: 'No. PO', enableCellEdit: false, minWidth: 120, width: "*" },
                        // { name: 'No. PO', field: 'PurchaseOrderNo', displayName: 'No. PO', enableCellEdit: $scope.potamNew, minWidth: 120, width: "*" }, //cellEditableCondition: $scope.isPOTAM},
                        // { name:'No. PO', field: 'RefGRNo', displayName: 'No. PO', cellTemplate: cellPONo, enableCellEdit: $scope.potamNew, minWidth: 150, width: "*"},
                        { name: 'No. Inv', field: 'InvoiceId', visible: false }, //ReceivedInvoiceNo
                        { name: 'No. Vendor', field: 'VendorId', visible: false }, //VendorNo
                        { name: 'No. Inv', field: 'VendorInvoiceNo', enableCellEdit: false, minWidth: 120, width: "*" },
                        { name: 'Tanggal Inv', field: 'VendorInvoiceDate', enableCellEdit: false, cellFilter: dateFilter, minWidth: 100 },
                        // { name:'No. Referensi SOP', field: 'RefPONo', displayName: 'No. Referensi SOP', enableCellEdit : $scope.potamNew, minWidth: 150, width: "*" },
                        { name: 'No. Referensi SOP', field: 'RefPONo', displayName: 'No. Referensi', enableCellEdit: false, minWidth: 150, width: "*" }, //
                        { name: 'No. Material', field: 'PartId', visible: false },
                        {
                            name: 'No. Material',
                            field: 'PartsCode',
                            minWidth: 150,
                            enableCellEdit: true,
                            cellTemplate: cellPartNo
                        },
                        // { name:'No. Material', field: 'PartsCode', enableCellEdit: true, minWidth: 100}, //$scope.isNewForm},
                        { name: 'Nama Material', field: 'PartsName', sort: { priority: 1 },  enableCellEdit: false, minWidth: 150, width: "*" },
                        { name: 'Qty PO', field: 'QtyPO', displayName: 'Qty PO', enableCellEdit: false, minWidth: 100, width: "*" },
                        { name: 'Qty Remain', field: 'QtyRemain', enableCellEdit: false, minWidth: 100, width: "*", visible:false },
                        { name: 'QtyRemain', field: 'xQtyRemain', enableCellEdit: false, minWidth: 100, width: "*" },
                        { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', enableCellEdit: true, minWidth: 100, width: "*" },
                        // { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', enableCellEdit: true, minWidth: 100, width: "*" },
                        // { name:'Satuan', field: 'UomPOId', enableCellEdit : false, minWidth: 100, width: "*"},
                        { name: 'Satuan', field: 'UomGR', minWidth: 100, width: "*", visible: false }, //SatuanPO
                        { name: 'Satuan', field: 'Satuan', enableCellEdit: false, minWidth: 100, width: "*" },
                        // { name:'Satuan', field: 'UomGR', enableCellEdit : false, minWidth: 100, width: "*"},
                        { name: 'Lokasi Gudang ID', field: 'WarehouseId', visible: false },
                        { name: 'Lokasi Gudang', field: 'WarehouseName', enableCellEdit: false, minWidth: 150, width: "*" },
                        { name: 'Lokasi Rak', field: 'ShelfName', enableCellEdit: false, minWidth: 150, width: "*" },
                        { name: 'GoodsReceiptStatusId', field: 'GoodsReceiptStatusId', visible: false, minWidth: 150, width: "*" },
                        // {
                        //     name: 'Action',
                        //     cellTemplate: strTemplateAction,
                        //     minWidth: 100,
                        //     width: "*" //,
                        //         //visible : true //grid.appScope.$parent.isPOTAM //true //grid.appScope.$parent.ref!='PO TAM'
                        // }
                    ] // backto first */
            };
            $scope.popup1 = {
                opened: false
            };
            $scope.open = function(param, id) {
                // $scope.popup1.opened = true;
                // param = true;
                // return param;
                console.log('open | param ========>', param);
                console.log('open | id ===========>', id);
                console.log('open | myrefGR ======>', $scope.myrefGR);
                console.log('open | gridOptions ==>', $scope.gridOptions.data);
                console.log('open | gridSublet ===>', $scope.gridSublet.data);

                if($scope.gridOptions.data.length > 0){
                    var indexTask = _.findIndex($scope.gridOptions.data, {'PurchaseOrderItemId':id});
                    console.log('open | indexTask 1 ========>', indexTask);
                    if(indexTask > -1){
                        $scope.gridOptions.data[indexTask].openedDate = true;
                    }
                }else {
                    var indexTask = _.findIndex($scope.gridSublet.data, {'PurchaseOrderItemId':id});
                    console.log('open | indexTask 2 ========>', indexTask);
                    if(indexTask > -1){
                        $scope.gridSublet.data[indexTask].openedDate = true;
                    }
                }
            };
            var editDate=true;
            var dateForTable2='<div><bsdatepicker style="position:absolute; z-index:999" name="startDate"  date-options="grid.appScope.dateOptions" ng-model="MODEL_COL_FIELD" > </bsdatepicker></div>'
            // var dateForTable = '<p class="input-group"><input type="text" class="form-control" uib-datepicker-popup="dd-MM-yyyy" datepicker-append-to-body="true" ng-model="MODEL_COL_FIELD" is-open="grid.appScope.popup1.opened" datepicker-options="grid.appScope.dateOptions" close-text="Close" /><span class="input-group-btn"><button type="button" class="btn btn-default" ng-click="grid.appScope.open1()"><i class="glyphicon glyphicon-calendar"></i></button></span></p>'
            var dateForTable = '<p class="input-group">\
            <input type="text" class="form-control" uib-datepicker-popup="dd-MM-yyyy" datepicker-append-to-body="true" ng-model="MODEL_COL_FIELD" is-open="row.entity.openedDate" datepicker-options="grid.appScope.dateOptions" close-text="Close" />\
                <span class="input-group-btn">\
                    <button type="button" class="btn btn-default" ng-click="grid.appScope.open(row.entity.openedDate, row.entity.PurchaseOrderItemId)">\
                        <i class="glyphicon glyphicon-calendar"></i>\
                    </button>\
                </span>\
            </p>'
            // '<input type="date" ng-model="MODEL_COL_FIELD" ng-change="grid.appScope.saveEdit(row)">'
            // '<bsdatepicker name="startDate"  date-options="grid.appScope.dateOptions" ng-model="MODEL_COL_FIELD" > </bsdatepicker>'
            // </div>';

            $scope.gridOptions = { // yg ini untuk NON TAM
                enableSorting: true,
                enableRowSelection: true,
                multiSelect: true,
                enableColumnResize: true,
                enableColumnResizing: true,
                // selectedItems: console.log($scope.mySelections),
                enableSelectAll: true,
                onRegisterApi: function(gridApi) {
                    $scope.gridApi = gridApi;
                    gridApi.selection.on.rowSelectionChanged($scope, function(rows) {
                        $scope.mySelections = gridApi.selection.getSelectedRows();
                        console.log($scope.mySelections); // ...
                        //build aray string
                        $scope.itemids = $scope.getSelectedItemIds($scope.mySelections);
                        console.log("item ids : ", $scope.itemids);
                    });
                    gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
                        $scope.mySelections = gridApi.selection.getSelectedRows();
                        console.log($scope.mySelections);
                    });
                    gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {
                        if(rowEntity.QtyGR > rowEntity.QtyRemain){
                            PartsGlobal.showAlert({
                                title: "Goods Receipt",
                                content: "Quantity GR tidak boleh melebihi Quantity Remain",
                                type: 'danger'
                            });
                            rowEntity.QtyGR = rowEntity.QtyRemain
                            return false;
                        }
                    });
                },
                // backto first 12102017 /*
                columnDefs: [
                        { name: 'No. GR', field: 'GoodsReceiptId', visible: false },
                        { name: 'No. GR', field: 'GoodsReceiptItemId', visible: false },
                        { name: 'No. PO', field: 'PurchaseOrderId', visible: false },
                        { name: 'nopo', field: 'PurchaseOrderNo',suppressRemoveSort: true, defaultSort: { direction: uiGridConstants.ASC },sort: { direction: uiGridConstants.ASC, priority: 0 }, displayName: 'No. PO', enableCellEdit: $scope.potamNew, minWidth: 150, width: "*" }, //cellEditableCondition: $scope.isPOTAM},
                        // { name:'No. PO', field: 'RefGRNo', displayName: 'No. PO', cellTemplate: cellPONo, enableCellEdit: $scope.potamNew, minWidth: 150, width: "*"},
                        { name: 'No. Inv', field: 'InvoiceId', visible: false }, //ReceivedInvoiceNo
                        { name: 'No. Inv', field: 'InvoiceNo', enableCellEdit: $scope.potamNew, minWidth: 150, width: "*" },
                        { name: 'Tanggal Inv', field: 'InvoiceDate', editableCellTemplate: dateForTable, enableCellEdit: editDate, cellFilter: dateFilter, minWidth: 150, width: "*" },
                        // { name: 'Tanggal Inv2', field: 'InvoiceDatekedua', editableCellTemplate: dateForTable2, enableCellEdit: editDate, cellFilter: dateFilter, minWidth: 150, width: "*" },
                        
                        // { name:'No. Referensi SOP', field: 'RefPONo', displayName: 'No. Referensi SOP', enableCellEdit : $scope.potamNew, minWidth: 150, width: "*" },
                        { name: 'No. Referensi SOP', field: 'RefPONo', displayName: 'No. Referensi', enableCellEdit: $scope.potamNew, minWidth: 150, width: "*" }, //
                        { name: 'No. Material', field: 'PartId', visible: false },
                        { name: 'No. Material', field: 'PartsId', visible: false },
                        { name: 'No. Material', field: 'PartsCode', enableCellEdit: true, minWidth: 100 },
                        // { name:'No. Material', field: 'PartsCode', cellTemplate: cellPartNo, enableCellEdit: true, minWidth: 100}, //$scope.isNewForm},
                        { name: 'Nama Material', field: 'PartsName', enableCellEdit: false, minWidth: 150, width: "*",sort: { priority: 1 } },
                        { name: 'Qty PO', field: 'QtyPO', displayName: 'Qty PO', enableCellEdit: false, minWidth: 100, width: "*" },
                        { name: 'Qty Remain', field: 'QtyRemain', enableCellEdit: false, minWidth: 100, width: "*" },
                        // { name: 'QtyRemain', field: 'xQtyRemain', enableCellEdit: false, minWidth: 100, width: "*" },
                        {
                            name: 'Qty GR',
                            field: 'QtyGR',
                            displayName: 'Qty GR',
                            //Konfirmasi Pak Rully
                            //enableCellEdit: false,
                            enableCellEdit: true,
                            minWidth: 100,
                            width: "*",
                            editableCellTemplate: '<input type="number" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">'
                        },
                        // { name:'Satuan', field: 'UomPOId', enableCellEdit : false, minWidth: 100, width: "*"},
                        { name: 'Satuan', field: 'UomGR', minWidth: 100, width: "*", visible: false }, //SatuanPO
                        { name: 'Satuan', field: 'Satuan', enableCellEdit: false, minWidth: 100, width: "*" },
                        // { name: 'Satuan', field: 'SatuanPO', enableCellEdit: false, minWidth: 100, width: "*" },
                        { name: 'Lokasi Gudang ID', field: 'WarehouseId', visible: false },
                        { name: 'Lokasi Gudang', field: 'WarehouseName', enableCellEdit: false, minWidth: 150, width: "*" },
                        { name: 'Lokasi Rak', field: 'ShelfName', enableCellEdit: false, minWidth: 150, width: "*" },
                        { name: 'GoodsReceiptStatusId', field: 'GoodsReceiptStatusId', visible: false, minWidth: 150, width: "*" },
                        // {
                        //     name: 'Action',
                        //     cellTemplate: strTemplateAction,
                        //     minWidth: 100,
                        //     width: "*" //,
                        //         //visible : true //grid.appScope.$parent.isPOTAM //true //grid.appScope.$parent.ref!='PO TAM'
                        // }
                    ] // backto first */
            };

            $scope.gridSublet = {
                enableSorting: true,
                enableRowSelection: true,
                multiSelect: true,
                enableColumnResize: true,
                enableColumnResizing: true,
                // selectedItems: console.log($scope.mySelections),
                enableSelectAll: true,
                onRegisterApi: function(gridApi) {
                    $scope.gridApi = gridApi;
                    gridApi.selection.on.rowSelectionChanged($scope, function(rows) {
                        $scope.mySelections = gridApi.selection.getSelectedRows();
                        console.log($scope.mySelections); // ...
                        //build aray string
                        $scope.itemids = $scope.getSelectedItemIds($scope.mySelections);
                        console.log("item ids : ", $scope.itemids);
                    });
                    gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
                        $scope.mySelections = gridApi.selection.getSelectedRows();
                        console.log($scope.mySelections);
                    });
                    gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {
                        if(rowEntity.QtyGR > rowEntity.QtyRemain){
                            PartsGlobal.showAlert({
                                title: "Goods Receipt",
                                content: "Quantity GR tidak boleh melebihi Quantity Remain",
                                type: 'danger'
                            });
                            rowEntity.QtyGR = rowEntity.QtyRemain
                            return false;
                        }
                    });
                },
                columnDefs: [
                        { name: 'No. PO', field: 'PurchaseOrderId', visible: false },
                        { name: 'nopo', field: 'RefGRNo',suppressRemoveSort: true, defaultSort: { direction: uiGridConstants.ASC }, displayName: 'No. PO', cellTemplate: cellPONo, minWidth: 150, width: "*", visible: false },
                        { name: 'No. PO', field: 'RefGRNo', displayName: 'No. PO', cellTemplate: cellPONo, minWidth: 150, width: "*", visible: false },
                        { 
                            name: 'No. PO', 
                            field: 'PurchaseOrderNo', 
                            suppressRemoveSort: true, 
                            defaultSort: { direction: uiGridConstants.ASC }, 
                            displayName: 'No. PO', 
                            cellTemplate: '<div><div class="input-group"><input type="text" ng-disabled="!grid.appScope.isNewForm" ng-model="row.entity.PurchaseOrderNo" ng-value="row.entity.PurchaseOrderNo">\
                            <label class="input-group-btn"><span class="btn wbtn" ng-show="grid.appScope.isNewForm" ng-click="grid.appScope.SearchPO(row.entity)">\
                            <i class="fa fa-search fa-1"></i></span>\
                            </label></div></div>',
                            minWidth: 150, 
                            width: "*" 
                        },
                        { name: 'No. Inv', field: 'InvoiceId', visible: false }, //ReceivedInvoiceNo
                        // { name: 'No. Inv', field: 'InvoiceNo', cellTemplate: cellInvoiceNo, minWidth: 150, width: "*" }, //
                        // { name: 'Tanggal Inv', field: 'InvoiceDate', cellFilter: dateFilter, minWidth: 100, width: "*" },
                        { name: 'No. Inv', field: 'InvoiceNo', enableCellEdit: $scope.potamNew, minWidth: 150, width: "*" },
                        { name: 'Tanggal Inv', field: 'InvoiceDate', editableCellTemplate: dateForTable, enableCellEdit: editDate, cellFilter: dateFilter, minWidth: 150, width: "*" },
                        // { name:'No. Referensi SOP', field: 'friends[0]', displayName: 'No. Referensi SOP'},
                        { name: 'No. Material', field: 'PartId', visible: false },
                        { name: 'No. Material', field: 'PartsCode', cellTemplate: cellPartNo, minWidth: 100, width: "*", visible: false },
                        { name: 'No. Material', field: 'PartsCode', enableCellEdit: false, minWidth: 100, width: "*" },
                        { name: 'Nama Material', field: 'PartsName', enableCellEdit: false, minWidth: 250, width: "*", sort: { priority: 1 } },
                        { name: 'Qty PO', field: 'QtyPO', displayName: 'Qty PO', enableCellEdit: false, minWidth: 70, width: "*" },
                        { name: 'Qty Remain', field: 'QtyRemain', enableCellEdit: false, minWidth: 70, width: "*" },
                        // { name: 'QtyRemain', field: 'xQtyRemain', enableCellEdit: false, minWidth: 70, width: "*" },
                        { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', minWidth: 70, width: "*" },
                        // { name:'Satuan', field: 'UomPOId', minWidth: 100, width: "*"},
                        { name: 'Satuan', field: 'UomGR', enableCellEdit: false, minWidth: 70, width: "*", visible: false }, //SatuanPO
                        // { name:'Satuan', field: 'SatuanGR', enableCellEdit : false, minWidth: 100, width: "*"},
                        // added by sss on 2018-01-04
                        { name: 'Satuan', field: 'SatuanPO', enableCellEdit: false, minWidth: 70, width: "*" },
                        //
                        { name: 'GoodsReceiptStatusId', field: 'GoodsReceiptStatusId', visible: false },
                        //{ name:'Lokasi Rak', field: 'friends[0]' },
                        // {
                        //     name: 'Action',
                        //     cellTemplate: strTemplateAction //,
                        //         //visible : true //grid.appScope.$parent.isPOTAM //true //grid.appScope.$parent.ref!='PO TAM'
                        // }
                    ]
                    // ,
                    // data : [      {
                    //                    "first-name": "Cox",
                    //                    "friends": ["friend0"],
                    //                    "address": {street:"301 Dove Ave", city:"Laurel", zip:"39565"},
                    //                    "getZip" : function() {return this.address.zip;}
                    //                },
                    //                {
                    //                    "first-name": "Cox",
                    //                    "friends": ["friend0"],
                    //                    "address": {street:"301 Dove Ave", city:"Laurel", zip:"39565"},
                    //                    "getZip" : function() {return this.address.zip;}
                    //                }
                    //            ]
            };

            $scope.gridTO = {
                // enableRowHeaderSelection: false,
                enableSorting: true,
                enableRowSelection: true,
                multiSelect: true,
                enableHorizontalScrollbar: 1,
                enableColumnResize: true,
                enableColumnResizing: true,
                // selectedItems: console.log($scope.mySelections),
                enableSelectAll: true,
                onRegisterApi: function(gridApi) {
                    $scope.gridApi = gridApi;
                    gridApi.selection.on.rowSelectionChanged($scope, function(rows) {
                        $scope.mySelections = gridApi.selection.getSelectedRows();
                        console.log($scope.mySelections); //
                        //build aray string
                        $scope.itemids = $scope.getSelectedItemIds($scope.mySelections);
                        console.log("item ids : ", $scope.itemids);
                    });
                    gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
                        $scope.mySelections = gridApi.selection.getSelectedRows();
                        console.log($scope.mySelections);
                    });
                },
                columnDefs: [
                        { name: 'No. PO', field: 'TransferOrderId', visible: false },
                        // { name:'No. TO', field: 'RefGRNo', displayName: 'No. TO', cellTemplate: cellPONo, minWidth: 150, width: "*"},
                        { name: 'No. TO', field: 'RefGRNo', displayName: 'No. TO', minWidth: 180, width: "*", enableCellEdit: false },
                        // { name:'No. Inv', field: 'address.city'},
                        { name: 'No. Material', field: 'PartId', visible: false },
                        // { name:'No. Material', field: 'PartsCode', cellTemplate: cellPartNo, minWidth: 100, width: "*"},
                        { name: 'No. Material', field: 'PartsCode', enableCellEdit: false, minWidth: 100 },
                        { name: 'Nama Material', field: 'PartsName', enableCellEdit: false, minWidth: 200, width: "*", sort: { priority: 1 } },
                        { name: 'Qty TO', field: 'QtyPO', displayName: 'Qty TO', enableCellEdit: false, minWidth: 80, width: "*" },
                        { name: 'Qty Remain', field: 'QtyRemain', enableCellEdit: false, minWidth: 80, width: "*" },
                        // { name: 'QtyRemain', field: 'xQtyRemain', enableCellEdit: false, minWidth: 100, width: "*" },
                        { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', minWidth: 80, width: "*", enableCellEdit: false },
                        // { name:'Satuan', field: 'UomTOId', minWidth: 100, width: "*"},
                        { name: 'Satuan', field: 'UomGR', minWidth: 80, width: "*", visible: false }, //SatuanPO
                        { name: 'Satuan', field: 'SatuanGR', enableCellEdit: false, minWidth: 80, width: "*" },
                        { name: 'Lokasi Gudang', field: 'WarehouseName', minWidth: 150, width: "*", enableCellEdit: false},
                        { name: 'Lokasi Rak', field: 'LokasiRak', minWidth: 100, width: "*", enableCellEdit: false },
                        { name: 'GoodsReceiptStatusId', field: 'GoodsReceiptStatusId', visible: false },
                        // {
                        //     name: 'Action',
                        //     cellTemplate: strTemplateAction //,
                        //         //visible : true //grid.appScope.$parent.isPOTAM //true //grid.appScope.$parent.ref!='PO TAM'
                        // }
                    ]
                    // ,
                    // data : [      {
                    //                    "first-name": "Cox",
                    //                    "friends": ["friend0"],
                    //                    "address": {street:"301 Dove Ave", city:"Laurel", zip:"39565"},
                    //                    "getZip" : function() {return this.address.zip;}
                    //                },
                    //                {
                    //                    "first-name": "Cox",
                    //                    "friends": ["friend0"],
                    //                    "address": {street:"301 Dove Ave", city:"Laurel", zip:"39565"},
                    //                    "getZip" : function() {return this.address.zip;}
                    //                }
                    //            ]
            };


            //----------------------------------
            // Dummy SO
            //----------------------------------
            $scope.gridSO = {
                enableSorting: true,
                enableRowSelection: true,
                multiSelect: true,
                enableSelectAll: true,
                enableColumnResizing: true,
                columnDefs: [
                    { name: 'No. Material', field: 'friends[0]' },
                    { name: 'Nama Material', field: 'address.city' },
                    { name: 'Qty Free', field: 'friends[0]' },
                    { name: 'Qty Request', field: 'address.city' },
                    { name: 'Qty Remain', field: 'friends[0]' },
                    { name: 'Qty GI', field: 'address.city' },
                    { name: 'Harga Satuan', field: 'friends[0]' },
                    { name: 'Satuan', field: 'address.city' },
                    { name: 'Harga Total', field: 'friends[0]' },
                    { name: 'Lokasi Gudang', field: 'address.city' },
                    { name: 'Lokasi Rak', field: 'friends[0]' },
                    { name: 'Keterangan Pre Picking', field: 'address.city' },
                    { name: 'No. Material', field: 'friends[0]' },
                    { name: 'Penerima ', field: 'address.city' },
                    { name: 'Status', field: 'getZip()', enableCellEdit: false }
                ],
                data: [{
                        "first-name": "Cox",
                        "friends": ["friend0"],
                        "address": { street: "301 Dove Ave", city: "Laurel", zip: "39565" },
                        "getZip": function() { return this.address.zip; }
                    },
                    {
                        "first-name": "Cox",
                        "friends": ["friend0"],
                        "address": { street: "301 Dove Ave", city: "Laurel", zip: "39565" },
                        "getZip": function() { return this.address.zip; }
                    }
                ]
            };

            //----------------------------------
            // Dummy TO
            //----------------------------------
            $scope.singleFilter = function(renderableRows) {
                var matcher = new RegExp($scope.filterValue);
                var field = $scope.filterColumn

                renderableRows.forEach(function(row) {
                    var match = row.entity[field].match(matcher);

                    if (!match) {
                        row.visible = false;
                    }
                });
                return renderableRows;
            };
            $scope.filterValue2 = {};
            $scope.singleFilter2 = function(renderableRows) {
                $scope.filterValue2 = '';
                $scope.filterValue2.filter = "";
                // var matcher = new RegExp($scope.filterValue2);
                // var field = $scope.filterColumn2

                // renderableRows.forEach(function(row) {
                //     var match = row.entity[field].match(matcher);

                //     if (!match) {
                //         row.visible = false;
                //     }
                // });
                // return renderableRows;
            };
            $scope.filter = function() {
                $scope.gridApi.grid.refresh();
            };
            $scope.filter2 = function() {
                console.log("$scope.filterColumn2",$scope.filterColumn2);
                console.log("$scope.filterValue2 ====",$scope.filterValue2);
                console.log("=============",$scope.gridApiLookUpVendor.grid.columns);
                $scope.gridApiLookUpVendor.grid.columns[3].filters[0].term = $scope.filterValue2.filter;
            }
            $scope.gridLookUpPO = {
                enableSorting: true,
                enableRowSelection: true,
                multiSelect: true,
                enableSelectAll: false,
                enableColumnResize: true,
                enableColumnResizing: true,
                
                // enableHorizontalScrollbar: 1,
                onRegisterApi: function(gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.grid.registerRowsProcessor($scope.singleFilter, 200);
                    gridApi.selection.on.rowSelectionChanged($scope, function(rows) {
                        // $scope.mySelections = gridApi.selection.getSelectedRows();
                        var selection = gridApi.selection.getSelectedRows();
                        console.log("Item(s) : ", selection); // ...
                        // PartsGlobal.selectedPO = $scope.mySelections;
                        PartsGlobal.selectedLookupGrid = selection;
                        //build aray string  selectedLookupGrid
                        //$scope.itemids= $scope.getSelectedItemIds($scope.mySelections);
                        //console.log("item ids : ", $scope.itemids);
                    });
                },
                columnDefs: [
                    { name: 'No. PO', field: 'PurchaseOrderId', visible: false },
                    { name: 'nopo', displayName:'No. PO', field: 'PurchaseOrderNo', minWidth: 150, width: "*" , suppressRemoveSort: true, sort: { direction: uiGridConstants.ASC, priority: 0 }},
                    { name: 'No. PO Item', field: 'PurchaseOrderItemId', visible: false },
                    { name: 'Tgl. PO', field: 'DocDate', minWidth: 100, width: "*", cellFilter: dateFilter },
                    { name: 'Outlet', field: 'OutletId', minWidth: 100, width: "*" },
                    { name: 'Vendor', field: 'VendorId', minWidth: 100, width: "*", visible: false },
                    { name: 'Vendor', field: 'VendorName', minWidth: 100, width: "*" },
                    { name: 'Material Type', field: 'MaterialTypeId', minWidth: 100, width: "*" },
                    { name: 'Ref No PO', displayName: 'No. Referensi SOP', field: 'RefPONo', minWidth: 150, width: "*" },
                    { name: 'No. Material', displayName: 'No. Material', field: 'PartId', visible: false },
                    { name: 'No. Material', displayName: 'No. Material', field: 'PartsId', visible: false },
                    { name: 'No. Material', displayName: 'No. Material', field: 'PartsCode',  minWidth: 100, width: "*" },
                    { name: 'Nama Material', displayName: 'Nama Material', field: 'PartsName', minWidth: 150, width: "*", sort: { priority: 1 }},
                    { name: 'Qty PO', field: 'QtyPO', minWidth: 100, width: "*" },
                    { name: 'Qty Remain', field: 'QtyRemain', minWidth: 100, width: "*" },
                    { name: 'Qty GR', field: 'QtyGR', minWidth: 100, width: "*" },
                    { name: 'Satuan', field: 'UomPOId', minWidth: 100, width: "*", visible: false }, //SatuanPO
                    { name: 'Satuan', field: 'SatuanPO', minWidth: 100, width: "*" },
                    { name: 'POTypeId', field: 'POTypeId', widht: 100, visible: false },
                    { name: 'Lokasi Gudang', field: 'WarehouseId', widht: 100, visible: false },
                    { name: 'Lokasi Gudang', field: 'WarehouseCode', widht: 100, visible: false },
                    { name: 'Lokasi Gudang', field: 'WarehouseName', minWidth: 150, width: "*" },
                    //{ name:'Lokasi Rak', field: 'friends[0]' },
                ],
                data: [] //$scope.getDaftarPO()
                    /*[      { //$scope.getDataPO();
                                     "first-name": "Cox",
                                     "friends": ["friend0"],
                                     "address": {street:"301 Dove Ave", city:"Laurel", zip:"39565"},
                                     "getZip" : function() {return this.address.zip;}
                                 }
                             ]*/
            };
            // $scope.filterColumn = $scope.gridOptions.columnDefs[0].field;
            $scope.filterColumn = $scope.gridLookUpPO.columnDefs[0].field;
          
            $scope.gridLookUpTO = {
                enableSorting: true,
                enableRowSelection: true,
                multiSelect: true,
                enableSelectAll: false,
                enableColumnResize: true,
                enableColumnResizing: true,
                // enableHorizontalScrollbar: 1,
                onRegisterApi: function(gridApi) {
                    $scope.gridApi = gridApi;
                    gridApi.selection.on.rowSelectionChanged($scope, function(rows) {
                        // $scope.mySelections = gridApi.selection.getSelectedRows();
                        var selection = gridApi.selection.getSelectedRows();
                        console.log("Item(s) : ", selection); // ...
                        // PartsGlobal.selectedTO = $scope.mySelections;
                        PartsGlobal.selectedLookupGrid = selection;
                    });
                },
                columnDefs: [
                    { name: 'No. TO', field: 'TransferOrderId', visible: false },
                    { name: 'No. TO', field: 'TransferOrderNo', minWidth: 150, width: "*" },
                    { name: 'No. TO Item', field: 'TransferOrderItemId', visible: false },
                    { name: 'Tgl. TO', field: 'DocDate', minWidth: 100, width: "*", cellFilter: dateFilter },
                    { name: 'Outlet', field: 'OutletId', minWidth: 100, width: "*" },
                    { name: 'Vendor', field: 'VendorId', minWidth: 100, width: "*", visible: false },
                    { name: 'Vendor', field: 'VendorCode', minWidth: 100, width: "*" },
                    { name: 'Material Type', field: 'MaterialTypeId', minWidth: 100, width: "*" },
                    { name: 'Ref No TO', displayName: 'No. Referensi SOP', field: 'RefTONo', minWidth: 150, width: "*" },
                    { name: 'No. Material', displayName: 'No. Material', field: 'PartId', visible: false },
                    { name: 'No. Material', displayName: 'No. Material', field: 'PartsCode', minWidth: 100, width: "*" },
                    { name: 'Nama Material', displayName: 'Nama Material', field: 'PartsName', minWidth: 150, width: "*", sort: { priority: 1 } },
                    { name: 'Qty TO', field: 'QtyTransfer', minWidth: 100, width: "*" },
                    { name: 'Qty Remain', field: 'QtyRemain', minWidth: 100, width: "*" },
                    { name: 'Qty GR', field: 'QtyGR', minWidth: 100, width: "*" },
                    { name: 'Satuan', field: 'UomId', minWidth: 100, width: "*", visible: false }, //SatuanTO
                    { name: 'Satuan', field: 'SatuanPO', minWidth: 100, width: "*" },
                    { name: 'TOTypeId', field: 'TOTypeId', widht: 100, visible: false },
                    { name: 'Lokasi Gudang', field: 'WarehouseId', widht: 100, visible: false },
                    { name: 'Lokasi Gudang', field: 'WarehouseCode', widht: 100, visible: false },
                    { name: 'Lokasi Gudang', field: 'WarehouseName', minWidth: 150, width: "*" },
                    { name: 'Lokasi Rak', field: 'ShelfName', minWidth: 150, width: "*" },
                    //{ name:'Lokasi Rak', field: 'friends[0]' },
                ],
                data: []
            };

            $scope.gridLookUpClaim = {
                enableSorting: true,
                enableRowSelection: true,
                multiSelect: true,
                enableSelectAll: false,
                enableColumnResize: true,
                enableColumnResizing: true,
                // enableHorizontalScrollbar: 1,
                onRegisterApi: function(gridApi) {
                    $scope.gridApi = gridApi;
                    gridApi.selection.on.rowSelectionChanged($scope, function(rows) {
                        // $scope.mySelections = gridApi.selection.getSelectedRows();
                        // $scope.mySelections = gridApi.selection.getSelectedRows();
                        var selection = gridApi.selection.getSelectedRows();
                        console.log("Item(s) : ", selection); // ...
                        PartsGlobal.selectedLookupGrid = selection;
                    });
                },
                columnDefs: [
                    { name: 'No. Claim', field: 'StockReturnClaimId', visible: false },
                    { name: 'No. Claim', field: 'StockReturnClaimNo', minWidth: 150, width: "*" },
                    { name: 'No. Claim Item', field: 'StockReturnClaimItemId', visible: false },
                    // { name:'Tgl. Claim', field: 'DocDate', minWidth: 100, width: "*", cellFilter: 'date:\'yyyy-MM-dd\''},
                    { name: 'Outlet', field: 'OutletId', minWidth: 100, width: "*" },
                    { name: 'Vendor', field: 'VendorId', minWidth: 100, width: "*", visible: false },
                    { name: 'Vendor', field: 'VendorCode', minWidth: 100, width: "*" },
                    { name: 'Material Type', field: 'MaterialTypeId', minWidth: 100, width: "*" },
                    { name: 'Ref No TO', displayName: 'No. Referensi SOP', field: 'RefTONo', minWidth: 150, width: "*" },
                    { name: 'No. Material', displayName: 'No. Material', field: 'PartId', visible: false },
                    { name: 'No. Material', displayName: 'No. Material', field: 'PartsCode', minWidth: 100, width: "*" },
                    { name: 'Nama Material', displayName: 'Nama Material', field: 'PartsName', minWidth: 150, width: "*", sort: { priority: 1 } },
                    { name: 'Qty Minus', field: 'QtyMinus', minWidth: 100, width: "*" },
                    { name: 'Qty Plus', field: 'QtyPlus', minWidth: 100, width: "*" },
                    // { name:'Qty GR', field: 'QtyGR', minWidth: 100, width: "*"},
                    { name: 'Satuan', field: 'UomId', minWidth: 100, width: "*", visible: false }, //SatuanTO
                    { name: 'Satuan', field: 'Satuan', minWidth: 100, width: "*" },
                    { name: 'Lokasi Gudang', field: 'WarehouseId', widht: 100, visible: false },
                    { name: 'Lokasi Gudang', field: 'WarehouseCode', widht: 100, visible: false },
                    { name: 'Lokasi Gudang', field: 'WarehouseName', minWidth: 150, width: "*" },
                    { name: 'Lokasi Rak', field: 'ShelfName', minWidth: 150, width: "*" },
                    //{ name:'Lokasi Rak', field: 'friends[0]' },
                ],
                data: []
            };

            //----------------------------------
            // Dummy PR
            //----------------------------------
            $scope.gridLookUpVendor = {
                enableSorting: true,
                enableRowSelection: true,
                multiSelect: true,
                enableSelectAll: false,
                enableColumnResizing: true,
                enableFiltering: true,
                onRegisterApi: function(gridApi) {
                    $scope.gridApiLookUpVendor = gridApi;
                    // $scope.gridApi.grid.registerRowsProcessor($scope.singleFilter2, 200);
                    gridApi.selection.on.rowSelectionChanged($scope, function(rows) {
                        var select = gridApi.selection.getSelectedRows();
                        console.log("Item(s) : ", select); // ...
                        PartsGlobal.selectedLookupGrid = select;
                    });
                },
                columnDefs: [
                    { name: 'VendorId', field: 'VendorId', visible: false },
                    { name: 'Vendor Code', field: 'VendorCode' },
                    { name: 'Vendor Name', field: 'Name' },
                    { name: 'SPLDBit', field: 'SPLDBit', visible: false },
                    { name: 'Address', field: 'Address' },
                    { name: 'Is Outlet', field: 'IsOutlet' },
                    { name: 'NPWP', field: 'NPWP' },
                    { name: 'Qty GI', field: 'address.city', visible: false },
                    { name: 'Harga Satuan', field: 'friends[0]', visible: false },
                    { name: 'Satuan', field: 'address.city', visible: false },
                    { name: 'Harga Total', field: 'friends[0]', visible: false },
                    { name: 'Lokasi Gudang', field: 'address.city', visible: false },
                    { name: 'Lokasi Rak', field: 'friends[0]', visible: false },
                    { name: 'Keterangan Pre Picking', field: 'address.city', visible: false },
                    { name: 'No. Material', field: 'friends[0]', visible: false },
                    { name: 'Penerima ', field: 'address.city', visible: false },
                    { name: 'Status', field: 'getZip()', enableCellEdit: false, visible: false }
                ],
                data: []
            };
            $scope.filterColumn2 = 1;            

            $scope.gridLookUpVendorOutlet = {
                enableSorting: true,
                enableRowSelection: true,
                multiSelect: true,
                enableSelectAll: false,
                enableColumnResizing: true,
                onRegisterApi: function(gridApi) {
                    $scope.gridApi = gridApi;
                    gridApi.selection.on.rowSelectionChanged($scope, function(rows) {
                        var select = gridApi.selection.getSelectedRows();
                        console.log("Item(s) : ", select); // ...
                        PartsGlobal.selectedLookupGrid = select;
                    });
                },
                columnDefs: [
                    { name: 'VendorId', field: 'VendorId', visible: false },
                    { name: 'OutletId', field: 'OutletId', visible: false },
                    { name: 'Vendor Code', field: 'OutletCode' },
                    { name: 'Vendor Name', field: 'Name' },
                    { name: 'Address', field: 'Address' },
                    { name: 'CompanyId', field: 'CompanyId', visible: false },
                    { name: 'CityRegencyId', field: 'CityRegencyId', visible: false }
                ],
                data: []
            };
            //-->

        } //]
    );
