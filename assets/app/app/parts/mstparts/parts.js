angular.module('app')
    .controller('MstPartsController', function($scope, $http, CurrentUser, MstParts, MstParts, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mParts = {}; //Model
    $scope.xParts = {};
    $scope.xParts.selected=[];
    $scope.classTypeData = [];
    $scope.filter = {ClassTypeId:0, PartsCode:'', PartsName:''};
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        Parts.getData($scope.filter).then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data.Result;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        columnDefs: [
            { name:'PartsId', field:'PartsId', visible:false },
            { name:'Kode Parts', field:'PartsCode' },
            { name:'Nama Parts', field:'PartsName' },
        ]
    };

});
