angular.module('app')
  .factory('MstParts', function($http, CurrentUser, $httpParamSerializer) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {
        return $http.get('/api/as/MstParts?'+$httpParamSerializer(filter));
      },
      create: function(obj) {
        return $http.post('/api/as/MstParts', [obj]);
      },
      update: function(obj){
        return $http.put('/api/as/MstParts', [obj]);
      },
      delete: function(arrId) {
        return $http.delete('/api/as/MstParts', {data:arrId,headers: {'Content-Type': 'application/json'}});
      },
    }
  });