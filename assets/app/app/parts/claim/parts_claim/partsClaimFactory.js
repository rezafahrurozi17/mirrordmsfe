angular.module('app')
    .factory('PartsClaim', function($http, CurrentUser, $filter) {
        var currentUser = CurrentUser.user;
        var StockReturnClaimHeader = {};
        this.formApi = {};

        this.ApprovalData = {};
        // console.log(currentUser);
        return {
            getData: function(data) {
                console.log("[Factory]");
                console.log(data);
                var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
                var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');
                var res = $http.get('/api/as/StockReturnClaim', {
                    params: {
                        StockReturnClaimStatusId: (data.StockReturnClaimStatusId == null ? "n/a" : data.StockReturnClaimStatusId),
                        StockReturnClaimNo: (data.StockReturnClaimNo == null ? "n/a" : data.StockReturnClaimNo),
                        ShippingNo: (data.ShippingNo == null ? "n/a" : data.ShippingNo),
                        PurchaseOrderNo: (data.PurchaseOrderNo == null ? "n/a" : data.PurchaseOrderNo),
                        InvoiceNo: (data.InvoiceNo == null ? "n/a" : data.InvoiceNo),
                        CaseNo: (data.CaseNo == null ? "n/a" : data.CaseNo),
                        StartDate: (startDate == null ? "n/a" : startDate),
                        EndDate: (endDate == null ? "n/a" : endDate)
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },
            create: function(data, detail) {
                console.log(' tambah data=>', data);
                console.log(' tambah data.Attachment=>', data.Attachment);
                console.log(' detail data=>', detail);
                if (data.Attachment === undefined) {

                };

                var DocDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
                return $http.post('/api/as/StockReturnClaim', [{
                    // StockReturnClaimId: (data.StockReturnClaimId || null),
                    StockReturnClaimNo: data.StockReturnClaimNo,
                    StockReturnClaimNoTAM: (data.StockReturnClaimNoTAM == null ? 0 : data.StockReturnClaimNoTAM),
                    DocDate: (DocDate == null ? 0 : DocDate),
                    ShippingNo: (data.ShippingNo == null ? 0 : data.ShippingNo),
                    PurchaseOrderId: (data.PurchaseOrderId == null ? 0 : data.PurchaseOrderId),
                    InvoiceId: (data.InvoiceId == null ? 0 : data.InvoiceId),
                    //InvoiceNo: (data.InvoiceNo == null ? "n/a" : data.InvoiceNo),
                    CaseNo: (data.CaseNo == null ? 0 : data.CaseNo),
                    PartClaimTypeId: (data.PartClaimTypeId == null ? 0 : data.PartClaimTypeId),
                    Partsman: (data.Partsman == null ? 0 : data.Partsman),
                    Notes: (data.Notes == null ? 0 : data.Notes),
                    OutletId: data.OutletId,
                    WarehouseId: data.WarehouseId,
                    CancelReasonDesc: data.CancelReasonDesc,
                    // Attachment: data.Attachment.name, //(data.Attachment==null?"":data.Attachment), 
                    Attachment: data.Attachment === undefined ? "" : data.Attachment.name, //(data.Attachment==null?"":data.Attachment), 
                    BAP: data.BAP === undefined ? "" : data.BAP.name, //(data.BAP==null?"":data.BAP), 
                    StockReturnClaimStatusId: (data.StockReturnClaimStatusId == null ? 0 : data.StockReturnClaimStatusId),
                    APart_StockReturnClaimItem_TT: detail
                }]);
            },
            update: function(data, detail) {
                console.log('rubah data=>', data);

                var DocDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
                return $http.put('/api/as/StockReturnClaim', [{
                    StockReturnClaimId: (data.StockReturnClaimId == null ? 0 : data.StockReturnClaimId),
                    StockReturnClaimNo: (data.StockReturnClaimNo == null ? 0 : data.StockReturnClaimNo),
                    StockReturnClaimNoTAM: (data.StockReturnClaimNoTAM == null ? 0 : data.StockReturnClaimNoTAM),
                    DocDate: (data.DocDate == null ? 0 : DocDate),
                    ShippingNo: (data.ShippingNo == null ? 0 : data.ShippingNo),
                    PurchaseOrderId: (data.PurchaseOrderId == null ? 0 : data.PurchaseOrderId),
                    // InvoiceId: (data.InvoiceId == null ? 0 : data.InvoiceId),
                    CaseNo: (data.CaseNo == null ? 0 : data.CaseNo),
                    PartClaimTypeId: (data.PartClaimTypeId == null ? 0 : data.PartClaimTypeId),
                    Partsman: (data.Partsman == null ? 0 : data.Partsman),
                    Notes: (data.Notes == null ? 0 : data.Notes),
                    Attachment: (data.Attachment == null ? 0 : data.Attachment),
                    BAP: (data.BAP == null ? 0 : data.BAP),
                    StockReturnClaimStatusId: (data.StockReturnClaimStatusId == null ? 0 : data.StockReturnClaimStatusId),
                    OutletId: data.OutletId,
                    WarehouseId: data.WarehouseId,
                    APart_StockReturnClaimItem_TT: detail
                }]);
            },
            // delete: function(id) {
            //   console.log("delete id==>",id);
            //   return $http.delete('/api/as/TestVehicle',{data:id,headers: {'Content-Type': 'application/json'}});
            // },

            cancel: function(data) {
                console.log('batal data=>', data);
                var DocDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
                return $http.put('/api/as/StockReturnClaim', [{
                    StockReturnClaimId: (data.StockReturnClaimId == null ? 0 : data.StockReturnClaimId),
                    StockReturnClaimNo: (data.StockReturnClaimNo == null ? 0 : data.StockReturnClaimNo),
                    StockReturnClaimNoTAM: (data.StockReturnClaimNoTAM == null ? 0 : data.StockReturnClaimNoTAM),
                    DocDate: (data.DocDate == null ? 0 : DocDate),
                    ShippingNo: (data.ShippingNo == null ? 0 : data.ShippingNo),
                    PurchaseOrderId: (data.PurchaseOrderId == null ? 0 : data.PurchaseOrderId),
                    // InvoiceId: (data.InvoiceId == null ? 0 : data.InvoiceId),
                    CaseNo: (data.CaseNo == null ? 0 : data.CaseNo),
                    PartClaimTypeId: (data.PartClaimTypeId == null ? 0 : data.PartClaimTypeId),
                    Partsman: (data.Partsman == null ? 0 : data.Partsman),
                    Notes: (data.Notes == null ? 0 : data.Notes),
                    //Attachment: (data.Attachment==null?0:data.Attachment),
                    StockReturnClaimStatusId: (data.StockReturnClaimStatusId == null ? '5' : '5'),
                    OutletId: data.OutletId,
                    WarehouseId: data.WarehouseId,
                    APart_StockReturnClaimItem_TT: []

                }]);
            },

            getDocumentNumber: function(data) {
                console.log("[getDocumentNumber]");
                console.log(data);
                var res = $http.get('/api/as/StockReturnClaim/GetDocumentNumber', {
                    params: {
                        UserId: data,
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },

            getDocDateByShippingNo: function(data) {
                console.log("[getDocDateByShippingNo]");
                console.log(data);
                var res = $http.get('/api/as/StockReturnClaim/SearchGR', {
                    params: {
                        ShippingNo: data,
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },

            // Detail digunakan saat View dan Edit
            getDetailByMaterialNo: function(data, pono, partsClaimTypeId, shippingNo) {
                console.log("[getDetailByMaterialNo]");
                console.log('data', data);
                console.log('pono', pono);
                console.log('partsClaimTypeId', partsClaimTypeId);
                var res = $http.get('/api/as/StockReturnClaim/StockReturnClaimMaterialOnNew', {
                    params: {
                        PartsCode: data,
                        PurchaseOrderNo: pono,
                        PartClaimTypeId: partsClaimTypeId,
                        ShippingNo: shippingNo
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;

            },

            createDetail: function(data) {
                console.log('tambah data grid detail=>', data);

                return $http.post('/api/as/StockReturnClaim/InsertStockReturnClaimDetail', data);
            },

            setStockReturnClaimHeader: function(data) {
                StockReturnClaimHeader = data;
            },
            getStockReturnClaimHeader: function() {
                return StockReturnClaimHeader;
            },

            // Detail digunakan saat View dan Edit
            getDetail: function(data) {
                console.log("[getDetail]");
                console.log(data);
                var res = $http.get('/api/as/StockReturnClaim/GetStockReturnClaimDetail', {
                    params: {
                        StockReturnClaimId: data,
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;

            },
            getDocumentNumber2: function() {
                console.log("[getDocumentNumber]");
                var res = $http.get('/api/as/PartsUser/GetDocumentNumber', {
                    params: {
                        FormatId: '92',
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },
            getPurchaseOrderNo: function(key) {
                console.log("[getPurchaseOrderNo]");
                var res = $http.get('/api/as/PartsUser/PurchaseOrderNo', {
                    params: {
                        PurchaseOrderNo: (key == null ? 0 : key),
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;

            },
            sendNotifToKabeng: function(data) {
                console.log("data notif", data.NoClaim);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: 'Request Approval untuk Pengajuan Parts Claim (' + data.NoClaim + ')',
                    RecepientId: data.RecepientId,  //kirim ke id kabeng sementara hardcode
                    Param : 47
                }]);
            },
            sendNotifFromKabeng: function(data) {
                console.log("data notif", data.NoClaim);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: 'Hasil Pengajuan Appoval Parts Claim Anda ' + data.Desc + ' ('+ data.NoClaim + ')',
                    RecepientId: data.RecepientId,  //kirim ke id kabeng sementara hardcode
                    Param : 48
                }]);
            },
            getPurchaseOrderNoNew: function(key) {
                console.log("[getPurchaseOrderNo]", key);
                return $http.get('/api/as/StockReturnClaim/SearchPO/' + key);
            },
            getPurchaseOrderPC: function(key) {
                console.log("[getPurchaseOrderPC]", key);
                return $http.get('/api/as/StockReturnClaim/SearchPOPartsClaim/' + key);
            },
            getVendorInvoicePC: function(key, poid) {
                console.log("[getVendorInvoicePC]", poid);
                return $http.get('/api/as/StockReturnClaim/SearchInvoicePartsClaim/' + key + '/' + poid);
            },
            getCaseNoPC: function(key, poid, invoiceno) {
                console.log("[getCaseNoPC]", invoiceno);
                return $http.get('/api/as/StockReturnClaim/SearchCaseNoPartsClaim/' + key + '/' + poid + '/' + invoiceno);
            },
            createApproveKabeng : function(data, detail) {
                console.log(' tambah data=>', data);
                console.log(' tambah data.Attachment=>', data.Attachment);
                console.log(' detail data=>', detail);
                

                var DocDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
                var GRDate = $filter('date')(data.GRDate, 'yyyy-MM-dd');
                return $http.post('/api/as/PostTPOSUsingClaim', [{
                    // StockReturnClaimId: (data.StockReturnClaimId || null),
                    StockReturnClaimNo: data.StockReturnClaimNo,
                    StockReturnClaimNoTAM: (data.StockReturnClaimNoTAM == null ? 0 : data.StockReturnClaimNoTAM),
                    DocDate: (DocDate == null ? 0 : DocDate),
                    ShippingNo: (data.ShippingNo == null ? 0 : data.ShippingNo),
                    PurchaseOrderId: (data.PurchaseOrderId == null ? 0 : data.PurchaseOrderId),
                    InvoiceId: (data.InvoiceId == null ? 0 : data.InvoiceId),
                    //InvoiceNo: (data.InvoiceNo == null ? "n/a" : data.InvoiceNo),
                    CaseNo: (data.CaseNo == null ? 0 : data.CaseNo),
                    PartClaimTypeId: (data.PartClaimTypeId == null ? 0 : data.PartClaimTypeId),
                    Partsman: (data.Partsman == null ? 0 : data.Partsman),
                    Notes: (data.Notes == null ? 0 : data.Notes),
                    OutletId: data.OutletId,
                    WarehouseId: data.WarehouseId,
                    CancelReasonDesc: data.CancelReasonDesc,
                    // Attachment: data.Attachment.name, //(data.Attachment==null?"":data.Attachment), 
                    Attachment: data.Attachment === undefined ? "" : data.Attachment, //(data.Attachment==null?"":data.Attachment), 
                    BAP: data.BAP === undefined ? "" : data.BAP, //(data.BAP==null?"":data.BAP), 
                    StockReturnClaimStatusId: (data.StockReturnClaimStatusId == null ? 0 : data.StockReturnClaimStatusId),
                    GRDate: (GRDate == null ? 0 : GRDate),
                    APart_StockReturnClaimItem_TT: detail
                }]);
            },
            getserviceandmaterial: function() {
                console.log("YOU DONT OWN ME");
                var res = $http.get('/api/as/PartsUser/GetServiceWarehouse');
                return res;
            },
            getStatusStockPC: function(data){
                var res=$http.get('/api/as/PC/CekStockClaim?StockReturnClaimId='+data);
                return res;
            },
        
        }
    });