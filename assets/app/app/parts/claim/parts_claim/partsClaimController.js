angular.module('app')
    .controller('PartsClaimController', function($rootScope, $scope, $http, uiGridConstants, CurrentUser, PartsClaim, ngDialog, $timeout, bsNotify, bsAlert, PartsCurrentUser, LocalService, PartsGlobal, $filter, Upload) {

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mData = {}; //Model
        $scope.mData.APart_StockReturnClaimItem_TT = {};
        $scope.mFilter = {};
        //model batal
        $scope.Alasan = {};
        $scope.ngDialog = ngDialog;
        $scope.BatalData = PartsClaim.getStockReturnClaimHeader();
        $scope.ApprovalProcessId = 8904;
        $scope.isAttachBAP=0;
        $scope.afterNextStep = false;
        $scope.progress = 0;
        $scope.progressStatus = '';
        $scope.progressDone = 0;
        $scope.progressTotal = 100;
        $scope.progressBAP = 0;
        $scope.progressStatusBAP = '';
        $scope.progressDoneBAP = 0;
        $scope.progressTotalBAP = 100;

        $scope.ApprovalData = PartsClaim.ApprovalData;

        $scope.xRole = {
            selected: []
        };

        $scope.localeDate = function(data) {
            var tmpDate = new Date(data);
            var resDate = new Date(tmpDate.toISOString().replace("Z","-0700")).toISOString(); 
            return resDate;
        }

        //----------------------------------
        // Start-Up
        //----------------------------------
        // added by sss on 2018-03-14
        var loadedContent = false;
        //
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;

            $timeout(function() {
                // added by sss on 2018-03-14
                loadedContent = true;
                //
                console.log("Approve = ", $scope.checkRights(4));
                console.log("dev_parts 10 November 2017 ", document.lastModified);
                console.log('mFilter on load', $scope.mFilter);
                if ($scope.checkRights(4)) {
                    $scope.mFilter.StockReturnClaimStatusId = 2;
                    var today = new Date();
                    $scope.mFilter.startDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                    $scope.mFilter.endDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

                    $scope.getData();
                } else {
                    //$scope.mFilter.StockReturnClaimStatusId = 1;
                    var today = new Date();
                    $scope.mFilter.startDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                    $scope.mFilter.endDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

                    $scope.getData();
                }

            });
        });

        //----------------------------------
        // Claim Gateway Entry Form
        //----------------------------------
        $scope.isOverlayForm = false;

        $scope.mData.kodeClaimtypes = ['Sht (Barang Kurang)', 'Ex (Barang Lebih)', 'U (Barang Salah - Rcv Diff from Inv)', 'P1 (Barang Salah - Diff part with label)', 'P2 (Barang Salah - Incomplete Part)', 'P3 (Barang Salah -  Can\'t be installed)', 'S1 (Barang Rusak - Parts Rusak)', 'S2 (Barang Rusak - Parts & Peti Rusak)', 'S3 (Barang Rusak - Broken Body & Kaca)', 'P/N (Barang Salah - Miss Inform)', 'MKT (Masalah Harga)', 'OTH (Double Supply)'];
        $scope.mData.mkodeClaimtype = $scope.mData.kodeClaimtypes[0];

        $scope.isEditState = false;

        //----------------------------------
        // CRUD state C = 1, R = 2, U = 3, D = 4
        // CRUD state + Form Number = 111
        //----------------------------------
        $scope.crudState = '';

        $scope.checkRights = function(bit) {
            var p = $scope.myRights & Math.pow(2, bit);
            var res = (p == Math.pow(2, bit));
            //console.log("myRights => ", $scope.myRights);
            return res;
        }

        $scope.getRightX = function(a, b) {
            var i = 0;
            if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
            else if ($scope.checkRights(b)) i = 2;
            else if ($scope.checkRights(a)) i = 1;
            return i;
        }
        $scope.getRightMaterialType = function() {
            // var i = $scope.getRightX(8, 9);
            // return i;
            PartsClaim.getserviceandmaterial().then(function(res) {
                $scope.materialtypes = res.data[0].MaterialType;
            });
            var materialTypeId = $scope.materialtypes;
            return materialTypeId;
        }
        $scope.getRightServiceType = function() {
            // var i = $scope.getRightX(10, 11);
            // return i;
            PartsClaim.getserviceandmaterial().then(function(res) {
                $scope.servicetypes = res.data[0].Servicetype;
            });
            var serviceTypeId =  $scope.servicetypes;
            return serviceTypeId;
        }

        $scope.goBack = function() {
            $scope.isOverlayForm = false;
            $scope.formApi.setMode("grid");
            if ($scope.mFilter != [] || $scope.mFilter != null || $scope.mFilter != undefined || Object.keys($scope.mFilter).length != 0) {
                $scope.getData();
            }
            $scope.afterNextStep = false;
            // PartsClaim.formApi.setMode("grid");
            // $scope.crudState = '000k';
        }
        $scope.goBack2 = function() {
            $scope.isOverlayForm = false;
            $scope.formApi.setMode("grid");
            if ($scope.mFilter != [] || $scope.mFilter != null || $scope.mFilter != undefined || Object.keys($scope.mFilter).length != 0) {
                $scope.getData();
            }
            // PartsClaim.formApi.setMode("grid");
            // $scope.crudState = '000k';
        }

        $scope.onBulkApproveSA = function(row) {
            $scope.ApproveData = row;
            var claimStatus = row[0].StockReturnClaimStatusId;
            console.log('Approve data = ', row);
            console.log('Status = ', claimStatus);
            console.log('Approval = ', $scope.ApproveData[0].ApprovalStatus);
            console.log('Invoice = ', row[0].InvoiceNo);
            if (row[0].StockReturnClaimStatusId == 2) { 
                ngDialog.openConfirm ({
                    template:'\
                                <div align="center" class="ngdialog-buttons">\
                                <p><b>Konfirmasi</b></p>\
                                <p>Approve Parts Claim ini?</p>\
                                <div class="ngdialog-buttons" align="center">\
                                <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                                <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinApprove(ApproveData)">Ya</button>\
                                </div>\
                                </div>',
                    plain: true,
                    scope : $scope
                });          
            } else if (row[0].StockReturnClaimStatusId == 0) {
                bsNotify.show(
                    {
                        title: "Parts Claim",
                        content: "Data masih draft, tidak bisa disetujui",
                        type: 'danger'
                    });
                return;
            } else if (row[0].StockReturnClaimStatusId == 3 && row[0].ApprovalStatus == 1) {
                bsNotify.show(
                    {
                        title: "Parts Claim",
                        content: "Data telah disetujui",
                        type: 'danger'
                    });
                return;
            } else if (row[0].StockReturnClaimStatusId == 3 && row[0].ApprovalStatus == 2) {
                bsNotify.show(
                    {
                        title: "Parts Claim",
                        content: "Data telah ditolak",
                        type: 'danger'
                    });
                return;
            } else if (row[0].StockReturnClaimStatusId == 4) {
                bsNotify.show(
                    {
                        title: "Parts Claim",
                        content: "Data telah direspon TPOS",
                        type: 'danger'
                    });
                return;
            } else if (row[0].StockReturnClaimStatusId == 5) {
                bsNotify.show(
                    {
                        title: "Parts Claim",
                        content: "Data telah dibatalkan",
                        type: 'danger'
                    });
                return;
            }
    
            console.log('$scope.ApproveData bawah = ', $scope.ApproveData);

            // console.log("Approved => ", row);
            // PartsCurrentUser.actApproval({
            //     ProcessId: 8901,
            //     DataId: row[0].StockReturnClaimId,
            //     ApprovalStatus: 1
            // }).then(function(res){
            //     $scope.getData();

            //     bsNotify.show(
            //         {
            //             title: "Parts Claim",
            //             content: "Data telah disetujui",
            //             type: 'success'
            //         }
            //       );
            // });

            // row[0].StockReturnClaimStatusId = 3;

            // PartsClaim.update(row[0]).then(function(res) {
            //       var create = res.data;
            //       console.log(res.data);
            //       bsNotify.show(
            //         {
            //             title: "Parts Claim",
            //             content: "Data berhasil di-approve",
            //             type: 'success'
            //         }
            //       );
            //     },
            //     function(err) {
            //       console.log("err=>", err);
            //     }
            //   );

        }

        $scope.yakinApprove = function(row){
            console.log("yakinApprove => ", row);

            PartsClaim.getStatusStockPC(row[0].StockReturnClaimId).then(function(res){
                console.log('res ==>', res);
                var countZero = 0;
                if(res.data.length > 0){
                    for(var i in res.data){
                        if(res.data[i].Status == 0){
                            countZero++;
                            // $scope.respondforMessage.push(res.data[i]);
                        }
                    }
                    if(countZero > 0){
                        $scope.ngDialog.close();
                        // PartsTransferOrder.setRespond($scope.respondforMessage);
                        // console.log("asdfadfadfa ==>",$scope.respondforMessage);
                        // ngDialog.openConfirm ({
                        //     // scope:$scope,
                        //     controller: 'PartsTransferOrderController',
                        //     template:'app/parts/transferorder/templates/responApproval.html'
                        // }).then(function(item) {
                        //     $scope.sendapprove();
                            // console.log("itemmm ->", item, 'asdfadfadfa ==>');
                        // });
                        bsAlert.alert({
                            title: res.data[0].PartsCode + " - " + res.data[0].PartsName,
                            text: "Parts yang akan di claim sudah terpakai, harap batalkan atau reject claim",
                            type: "warning",
                            showCancelButton: false
                        },
                        function() {

                        });

                    } else {

                        for (var i = 0; i < row.length; i++) {
                            $scope.ISGR = "";
                            console.log(row[i].StockReturnClaimNo);
                            var IsBPGR = row[i].StockReturnClaimNo;                
                            var hasilnya = IsBPGR.substring(IsBPGR.length - 12, 6);                           
                            console.log(hasilnya);
                            if (hasilnya == 'G'){$scope.ISGR = 1}
                            else{$scope.ISGR = 0}
                            if ($scope.ISGR == 1){
                                var data = {NoClaim : row[i].StockReturnClaimNo, RecepientId : 1122, Desc : "Disetujui"}
                                PartsClaim.sendNotifFromKabeng(data).then(function(res){
                                    console.log('notif send to KABENG GR')
                                });                    
                            }
                            else if ($scope.ISGR == 0){
                                var data = {NoClaim : row[i].StockReturnClaimNo, RecepientId : 1123, Desc : "Disetujui"}
                                PartsClaim.sendNotifFromKabeng(data).then(function(res){
                                    console.log('notif send to KABENG BP')
                                });                
                            }
                        }
                        // =====
                        $scope.mData = row[0];
                        PartsClaim.getDetail(row[0].StockReturnClaimId).then(function(res) {
                            var gridData = res.data.Result;
                            console.log('edit-gridData', gridData);
                            gridData[0].DocDate = $scope.localeDate(gridData[0].DocDate);
                            gridData[0].GRDate = $scope.localeDate(gridData[0].GRDate);
                            // $scope.mData.PurchaseOrderNo = gridData[0].PurchaseOrderNo;
                            // $scope.mData.InvoiceNo = gridData[0].InvoiceNo;
                            $scope.mData.Notes = gridData[0].Notes;
                            $scope.mData.InvoiceNo = gridData[0].InvoiceNo;
                            $scope.mData.GRDate = angular.copy(gridData[0].GRDate);
                            console.log("$scope.mData.GRDate==>",$scope.mData.GRDate);
                            if($scope.mData.Attachment == null){
                                delete $scope.mData.Attachment
                            }
                            // =========================
                            $scope.ngDialog.close();
                            PartsClaim.createApproveKabeng($scope.mData, gridData).then(function(res){
                                PartsCurrentUser.actApproval({
                                    // ProcessId: 8901,
                                    ProcessId: $scope.ApprovalProcessId,
                                    DataId: row[0].StockReturnClaimId,
                                    ApprovalStatus: 1
                                }).then(function(res){
                                    $scope.getData();
                
                                    bsNotify.show(
                                        {
                                            title: "Parts Claim",
                                            content: "Data telah disetujui",
                                            type: 'success'
                                        }
                                    );
                                });
                            });
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                        );
                        // =====
                    }
                }
            });
        }

        $scope.onBulkRejectSA = function(row) {
            console.log("Rejected => ", row);
            if (row[0].StockReturnClaimStatusId == 2) {
                for (var i = 0; i < row.length; i++) {
                    $scope.ISGR = "";
                    console.log(row[i].StockReturnClaimNo);
                    var IsBPGR = row[i].StockReturnClaimNo;                
                    var hasilnya = IsBPGR.substring(IsBPGR.length - 12, 6);                           
                    console.log(hasilnya);
                    if (hasilnya == 'G'){$scope.ISGR = 1}
                    else{$scope.ISGR = 0}
                    if ($scope.ISGR == 1){
                        var data = {NoClaim : row[i].StockReturnClaimNo, RecepientId : 1122, Desc : "Ditolak"}
                        PartsClaim.sendNotifFromKabeng(data).then(function(res){
                            console.log('notif send to KABENG GR')
                        });                    
                    }
                    else if ($scope.ISGR == 0){
                        var data = {NoClaim : row[i].StockReturnClaimNo, RecepientId : 1123, Desc : "Ditolak"}
                        PartsClaim.sendNotifFromKabeng(data).then(function(res){
                            console.log('notif send to KABENG BP')
                        });
                    }
                }

                PartsCurrentUser.actApproval({
                    // ProcessId: 8901,
                    ProcessId: $scope.ApprovalProcessId,
                    DataId: row[0].StockReturnClaimId,
                    ApprovalStatus: 2
                });
                
            } else if (row[0].StockReturnClaimStatusId == 0) {
                bsNotify.show(
                    {
                        title: "Parts Claim",
                        content: "Data masih draft, tidak bisa ditolak",
                        type: 'danger'
                    });
                return;
            } else if (row[0].StockReturnClaimStatusId == 3 && row[0].ApprovalStatus == 1) {
                bsNotify.show(
                    {
                        title: "Parts Claim",
                        content: "Data telah disetujui",
                        type: 'danger'
                    });
                return;
            } else if (row[0].StockReturnClaimStatusId == 3 && row[0].ApprovalStatus == 2) {
                bsNotify.show(
                    {
                        title: "Parts Claim",
                        content: "Data telah ditolak",
                        type: 'danger'
                    });
                return;
            } else if (row[0].StockReturnClaimStatusId == 4) {
                bsNotify.show(
                    {
                        title: "Parts Claim",
                        content: "Data telah direspon TPOS",
                        type: 'danger'
                    });
                return;
            } else if (row[0].StockReturnClaimStatusId == 5) {
                bsNotify.show(
                    {
                        title: "Parts Claim",
                        content: "Data telah dibatalkan",
                        type: 'danger'
                    });
                return;
            }
        }

        $scope.formApi = {};


        $scope.onOverlayMode = function(kodeClaim) {

            //validasi pre
            if (typeof $scope.mData.ShippingNo === undefined || $scope.mData.ShippingNo == null) {
                bsNotify.show({
                    title: "No. Shipping tidak boleh kosong",
                    type: 'danger'
                });
            } else if (typeof $scope.mData.PurchaseOrderId === undefined || $scope.mData.PurchaseOrderId == null) {
                bsNotify.show({
                    title: "No. Purchase Order tidak boleh kosong",
                    type: 'danger'
                });
            } else if (typeof $scope.mData.InvoiceId === undefined || $scope.mData.InvoiceId == null) {
                bsNotify.show({
                    title: "No. Invoice tidak boleh kosong",
                    type: 'danger'
                });
            } else if (typeof $scope.mData.CaseNo === undefined || $scope.mData.CaseNo == null) {
                bsNotify.show({
                    title: "No. Case tidak boleh kosong",
                    type: 'danger'
                });
            } else if (typeof $scope.mData.PartClaimTypeId === undefined || $scope.mData.PartClaimTypeId == null) {
                bsNotify.show({
                    title: "Kode Claim tidak boleh kosong",
                    type: 'danger'
                });
            } else {

                // added by sss on 2018-01-09
                var tempM = _.find($scope.grid_parts_claim.columnDefs, { "field": "QtyMinus" });
                var tempP = _.find($scope.grid_parts_claim.columnDefs, { "field": "QtyPlus" });
                var tempNG = _.find($scope.grid_parts_claim.columnDefs, { "field": "QtyNG" });
                var tempOth = _.find($scope.grid_parts_claim.columnDefs, { "field": "QtyOth" });

                var tempQtyRcv = _.find($scope.grid_parts_claim.columnDefs, { "field": "QtyReceived" });
                console.log("$scope.mData.PartClaimTypeId : ", $scope.mData.PartClaimTypeId);

                if (typeof tempM !== 'undefined') {
                    tempM.visible = true;
                }
                if (typeof tempP !== 'undefined') {
                    tempP.visible = true;
                }
                if (typeof tempNG !== 'undefined') {
                    tempNG.visible = false;
                }
                if (typeof tempOth !== 'undefined') {
                    tempOth.visible = false;
                }
                if (typeof tempQtyRcv !== 'undefined') {
                    tempQtyRcv.enableCellEdit = true;
                }

                if ($scope.mData.PartClaimTypeId >= 4 && $scope.mData.PartClaimTypeId <= 9) {
                    console.log("P1 s/d S3");
                    if (typeof tempM !== 'undefined') {
                        tempM.visible = false;
                    }
                    if (typeof tempP !== 'undefined') {
                        tempP.visible = false;
                    }
                    if (typeof tempNG !== 'undefined') {
                        tempNG.visible = true;
                        tempNG.enableCellEdit = true;
                    }
                    if (typeof tempQtyRcv !== 'undefined') {
                        tempQtyRcv.enableCellEdit = false;
                    }
                } else if ($scope.mData.PartClaimTypeId >= 10 && $scope.mData.PartClaimTypeId <= 12) {
                    console.log("P/N s/d OTH");
                    if (typeof tempM !== 'undefined') {
                        tempM.visible = false;
                    }
                    if (typeof tempP !== 'undefined') {
                        tempP.visible = false;
                    }
                    if (typeof tempOth !== 'undefined') {
                        tempOth.visible = true;
                        tempOth.enableCellEdit = true;
                    }
                    if (typeof tempQtyRcv !== 'undefined') {
                        tempQtyRcv.enableCellEdit = false;
                    }
                }

                //

                console.log("[$scope] onOverlayMode");
                console.log(kodeClaim);
                $scope.mData.selectedPartsClaim = kodeClaim;
                $scope.isOverlayForm = false;
                var newnum = '1';

                $scope.crudState = newnum + $scope.formShowHandler(kodeClaim);
                console.log("[OVERLAY] is " + $scope.isOverlayForm);
                console.log("[OVERLAY] " + $scope.crudState);

                // $scope.Add();
                $scope.afterNextStep = true;    
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
            }

        }

        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.gridDataTree = [];
        $scope.getData = function() {
            console.log("filter", $scope.mFilter);
            console.log("isinyaah", Object.keys($scope.mFilter));
            if (Object.keys($scope.mFilter).length == 0) {
                if (loadedContent) {
                    bsNotify.show({
                        title: "Parts Claim",
                        content: "Filter tidak boleh kosong",
                        type: 'warning'
                    });
                }
            } else if ((('StockReturnClaimStatusId' in $scope.mFilter)) && (!('startDate' in $scope.mFilter) || !('endDate' in $scope.mFilter))) {
                bsNotify.show({
                    title: "Parts Claim",
                    content: "Tanggal harus diisi",
                    type: 'warning'
                });
            } else {
                if ($scope.mFilter.StockReturnClaimStatusId=="0") {
                    $scope.mFilter.StockReturnClaimStatusId=null;
                }
                PartsClaim.getData($scope.mFilter).then(function(res) {
                    var gridData = res.data.Result;
                    $scope.grid.data = gridData;
                    $scope.loading = false;
                    console.log(gridData);
                },
                function(err) {
                    console.log("err=>", err);
                });
            }

        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        //------------------------------------------------------------
        $scope.formmode = { mode: '' };
        $scope.doGeneratePass = function() {
            $scope.mUser.password = Math.random().toString(36).slice(-8);
        }
        //APart_StockReturnClaim_TT, StockReturnClaimId, id in PGlobal_TP
        $scope.ApprovalProcessId = 8901;

        $scope.onBeforeNewMode = function() {
            $scope.progress = 0;
            $scope.progressStatus = '';
            $scope.progressDone = 0;
            $scope.progressTotal = 100;
            $scope.progressBAP = 0;
            $scope.progressStatusBAP = '';
            $scope.progressDoneBAP = 0;
            $scope.progressTotalBAP = 100;
    
            $scope.searchShO = true;
            // $scope.searchNoMaterial = false;
            $scope.searchNoMaterial = true;
            $scope.isAttachBAP=0;
            PartsClaim.formApi = $scope.formApi;
            $('#uploadBAP').val("");
            //cari purcaseorder
            // PartsCurrentUser.getPurchaseOrder().then(function(res) {
            //     console.log("List PurchaseOrder====>", res);
            //     $scope.PurchaseOrderData = res.data;
            // });

            PartsCurrentUser.getClaimCodeData().then(function(res) {
                console.log("List claim code====>", res);
                //bentuk query orm
                $scope.PartClaimTypeData = res.data.Result;
            });

            PartsClaim.getserviceandmaterial().then(function(res) {
                console.log("resres==>",res);
                resdata = angular.copy(res);
                console.log("resdata==>",resdata);
                $scope.mData.MaterialTypeId = res.data[0].MaterialType;
                $scope.mData.ServiceTypeId = res.data[0].Servicetype;
                $scope.mData.WarehouseId = res.data[0].warehouseid;
            });

            //show gateway page
            $scope.isOverlayForm = true;
            $scope.checkRefGINo = true;
            console.log("onBeforeNewMode");
            $scope.formode = 1;
            console.log("mode=>", $scope.formmode);
            $scope.newUserMode = true;
            $scope.editMode = false;
            //$scope.mode='new';
            $scope.mData.OutletId = $scope.user.OrgId;
            // $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
            // $scope.mData.ServiceTypeId = $scope.getRightServiceType();
            console.log("MaterialTypeId : ", $scope.mData.MaterialTypeId);
            console.log("ServiceTypeId : ", $scope.mData.ServiceTypeId);

            // PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.user.OrgId).then(function(res) {
            //         var loginData = res.data;
            //         console.log(loginData[0]);
            //         $scope.mData.WarehouseId = loginData[0].WarehouseId;

            //         console.log("OutletId = ", $scope.mData.OutletId);
            //         console.log("WarehouseId = ", $scope.mData.WarehouseId);
            //     },
            //     function(err) {
            //         console.log("err=>", err);
            //     }
            // );

            PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res) {
                var Approvers = res.data.Result[0].Approvers;
                console.log("getApprover res = ", Approvers);
                $scope.mData.Approvers = Approvers;
                $scope.ApprovalData.Approvers = Approvers;
              },
              function(err) {
                console.log("err=>", err);
              }
            );

            // var UserId;
            // var DocNo = '';
            // if (LocalService.get('BSLocalData')) {
            //     LoginObject = angular.fromJson(LocalService.get('BSLocalData')).LoginData;
            // }
            // UserId = angular.fromJson(LoginObject).UserId;

            // PartsClaim.getDocumentNumber2().then(function(res) {
            //   DocNo = res.data;
            //   //$scope.mData.StockReturnClaimNo = DocNo[0];
            //   $scope.mData.StockReturnClaimNo = '000/PCG/1706-00142';
            //   $scope.mData.DocDate = DocNo[1];
            //   console.log("Generating DocNo & DocDate ->");
            //   console.log($scope.mData.StockReturnClaimNo);
            //   console.log($scope.mData.DocDate);
            //   },
            //   function(err) {
            //     console.log("err=>", err);
            //   }
            // );

            // $scope.GenerateDocNum();

            $scope.mData.StockReturnClaimNo = '000/PCG/1706-00034';
            //$scope.mData.DocDate = '2017-06-22';
            var currentDate = new Date();
            $scope.mData.DocDate = $filter('date')(currentDate, 'yyyy-MM-dd');
            console.log('$scope.mData.DocDate', $scope.mData.DocDate);

            $scope.grid_parts_claim.data = [];
        }
        $scope.selectPartClaimType = function(data){
            console.log("===== selected ===", data);
            if(data.ClaimCodeId == 9 || data.ClaimCodeId == 8 ){

            }else{
                $scope.isAttachBAP=0;
                $('#uploadBAP').val("");
            }
        }
        $scope.onBeforeEditMode = function(row) {

            if (row.StockReturnClaimStatusId != 1) {
                bsNotify.show({
                    title: "Parts Claim",
                    content: "Tidak Bisa Edit karena status sudah tidak di draft lagi",
                    type: 'warning'
                });
                $scope.formApi.setMode('grid');
                // return;
            } else {
                $scope.formode = 2;
                //$scope.newUserMode=false;
                //console.log("mode=>",$scope.formmode);
                //$scope.editMode=true;

                console.log("[$scope] onBeforeEditMode");
                var editnum = '3'

                //pemilihan form edit
                console.log("Data dari onBeforeEditMode => ");
                $scope.isEditState = true;
                console.log("[EDIT] ");
                if(row.BAP !== null ){
                    $scope.isAttachBAP = 1;
                }

                $scope.crudState = editnum + '00';
                console.log("[EDIT] " + $scope.crudState);
            }
        }
        $scope.onBeforeDeleteMode = function() {
            $scope.formode = 3;
            console.log("mode=>", $scope.formmode);
            //$scope.mode='del';
        }

        $scope.UbahData = function(mdata) {

        }

        $scope.GenerateDocNum = function() {

            PartsCurrentUser.getFormatId($scope.mData.ServiceTypeId, 'PC').then(function(res) {
                    var result = res.data;
                    console.log("FormatId = ", result[0].Results);

                    PartsCurrentUser.getDocumentNumber2(result[0].Results).then(function(res) {
                            var DocNo = res.data;

                            if (typeof DocNo === 'undefined' || DocNo == null) {
                                bsNotify.show({
                                    title: "Parts Claim",
                                    content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                                    type: 'danger'
                                });
                            } else {
                                $scope.mData.StockReturnClaimNo = DocNo[0];
                                $scope.mData.DocDate = DocNo[1];
                                console.log("Generating DocNo & DocDate ->");
                                console.log($scope.mData.StockReturnClaimNo);
                                console.log($scope.mData.DocDate);
                            }


                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );


                },
                function(err) {

                }
            );
            // [END] Get Current User Warehouse
        }


        $scope.onShowDetail = function(row, mode) {
            PartsClaim.setStockReturnClaimHeader(row);
            PartsClaim.formApi = $scope.formApi;
            //$scope.mData = row;
            var shownum = '2';

            if (mode == 'view') { //jika mode = view
                console.log("--->[Active] onShowDetail");
                console.log("Data dari onShowDetail => ");

                PartsClaim.getDetail(row.StockReturnClaimId).then(function(res) {
                        var gridData = res.data.Result;
                        console.log('view-gridData', gridData);
                        // $scope.mData.PurchaseOrderNo = gridData[0].PurchaseOrderNo;
                        // $scope.mData.InvoiceNo = gridData[0].InvoiceNo;
                        $scope.mData.Notes = gridData[0].Notes;
                        $scope.mData.InvoiceNo = gridData[0].InvoiceNo;
                        // added by sss on 2018-01-09
                        var tempM = _.find($scope.grid_parts_claim_lihat.columnDefs, { "field": "QtyMinus" });
                        var tempP = _.find($scope.grid_parts_claim_lihat.columnDefs, { "field": "QtyPlus" });
                        var tempNG = _.find($scope.grid_parts_claim_lihat.columnDefs, { "field": "QtyNG" });
                        var tempOth = _.find($scope.grid_parts_claim_lihat.columnDefs, { "field": "QtyOth" });

                        var tempQtyRcv = _.find($scope.grid_parts_claim_lihat.columnDefs, { "field": "QtyReceived" });
                        console.log("$scope.mData.PartClaimTypeId : ", $scope.mData.PartClaimTypeId);

                        if (typeof tempM !== 'undefined') {
                            tempM.visible = true;
                        }
                        if (typeof tempP !== 'undefined') {
                            tempP.visible = true;
                        }
                        if (typeof tempNG !== 'undefined') {
                            tempNG.visible = false;
                        }
                        if (typeof tempOth !== 'undefined') {
                            tempOth.visible = false;
                        }
                        if (typeof tempQtyRcv !== 'undefined') {
                            tempQtyRcv.enableCellEdit = true;
                        }

                        if (row.PartClaimTypeId >= 4 && row.PartClaimTypeId <= 9) {
                            console.log("P1 s/d S3");
                            if (typeof tempM !== 'undefined') {
                                tempM.visible = false;
                            }
                            if (typeof tempP !== 'undefined') {
                                tempP.visible = false;
                            }
                            if (typeof tempNG !== 'undefined') {
                                tempNG.visible = true;
                                tempNG.enableCellEdit = true;
                            }
                            if (typeof tempQtyRcv !== 'undefined') {
                                tempQtyRcv.enableCellEdit = false;
                            }
                        } else if (row.PartClaimTypeId >= 10 && row.PartClaimTypeId <= 12) {
                            console.log("P/N s/d OTH");
                            if (typeof tempM !== 'undefined') {
                                tempM.visible = false;
                            }
                            if (typeof tempP !== 'undefined') {
                                tempP.visible = false;
                            }
                            if (typeof tempOth !== 'undefined') {
                                tempOth.visible = true;
                                tempOth.enableCellEdit = true;
                            }
                            if (typeof tempQtyRcv !== 'undefined') {
                                tempQtyRcv.enableCellEdit = false;
                            }
                        }
                        $scope.gridApiLihat.core.notifyDataChange(uiGridConstants.dataChange.ALL);
                        //
                        $scope.grid_parts_claim_lihat.data = gridData;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

                $scope.crudState = shownum + '00';
                console.log("[VIEW] " + $scope.crudState);
                $scope.isEditState = false;

            } else if (mode == 'edit') { //jika mode edit
                if (row.StockReturnClaimStatusId != 1) {

                    $scope.formApi.setMode('grid');
                    // return;
                } else {
                    $scope.searchShO = false;
                    $scope.formode = 0;
                    PartsClaim.getDetail(row.StockReturnClaimId).then(function(res) {
                            var gridData = res.data.Result;
                            console.log('edit-gridData', gridData);
                            // $scope.mData.PurchaseOrderNo = gridData[0].PurchaseOrderNo;
                            // $scope.mData.InvoiceNo = gridData[0].InvoiceNo;
                            $scope.mData.Notes = gridData[0].Notes;
                            $scope.mData.InvoiceNo = gridData[0].InvoiceNo;
                            // added by sss on 2018-01-09
                            var tempM = _.find($scope.grid_parts_claim_ubah.columnDefs, { "field": "QtyMinus" });
                            var tempP = _.find($scope.grid_parts_claim_ubah.columnDefs, { "field": "QtyPlus" });
                            var tempNG = _.find($scope.grid_parts_claim_ubah.columnDefs, { "field": "QtyNG" });
                            var tempOth = _.find($scope.grid_parts_claim_ubah.columnDefs, { "field": "QtyOth" });

                            var tempQtyRcv = _.find($scope.grid_parts_claim_lihat.columnDefs, { "field": "QtyReceived" });
                            console.log("$scope.mData.PartClaimTypeId : ", $scope.mData.PartClaimTypeId);

                            if (typeof tempM !== 'undefined') {
                                tempM.visible = true;
                            }
                            if (typeof tempP !== 'undefined') {
                                tempP.visible = true;
                            }
                            if (typeof tempNG !== 'undefined') {
                                tempNG.visible = false;
                            }
                            if (typeof tempOth !== 'undefined') {
                                tempOth.visible = false;
                            }
                            if (typeof tempQtyRcv !== 'undefined') {
                                tempQtyRcv.enableCellEdit = true;
                            }

                            if (row.PartClaimTypeId >= 4 && row.PartClaimTypeId <= 9) {
                                console.log("P1 s/d S3");
                                if (typeof tempM !== 'undefined') {
                                    tempM.visible = false;
                                }
                                if (typeof tempP !== 'undefined') {
                                    tempP.visible = false;
                                }
                                if (typeof tempNG !== 'undefined') {
                                    tempNG.visible = true;
                                    tempNG.enableCellEdit = true;
                                }
                                if (typeof tempQtyRcv !== 'undefined') {
                                    tempQtyRcv.enableCellEdit = false;
                                }
                            } else if (row.PartClaimTypeId >= 10 && row.PartClaimTypeId <= 12) {
                                console.log("P/N s/d OTH");
                                if (typeof tempM !== 'undefined') {
                                    tempM.visible = false;
                                }
                                if (typeof tempP !== 'undefined') {
                                    tempP.visible = false;
                                }
                                if (typeof tempOth !== 'undefined') {
                                    tempOth.visible = true;
                                    tempOth.enableCellEdit = true;
                                }
                                if (typeof tempQtyRcv !== 'undefined') {
                                    tempQtyRcv.enableCellEdit = false;
                                }
                            }
                            $scope.gridApiUbah.core.notifyDataChange(uiGridConstants.dataChange.ALL);
                            //
                            $scope.grid_parts_claim_ubah.data = gridData;
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
                }
            } else if (mode == 'new') { // mode New

            }
            $scope.ref = '';
            //log
            console.log("onShowDetail=>", mode);
        }

        //----------------------------------------------
        // ACTION BUTTON PADA FORM
        //----------------------------------------------
        // event button 'Lihat Vendor'
        $scope.onShowVendorPO = function() {
            ngDialog.openConfirm({
                template: '<div ng-include=\"\'app/parts/Claim/tam_sop_appointment/view_vendor.html\'\"></div>',
                //templateUrl: '/app/parts/pomaintenance/alasan.html',
                plain: true,
                controller: 'PartsClaimController'
            });
        }

        // event button 'Input Alasan'
        $scope.SimpanEdit = function(mData) {

            console.log("Master Data => ", mData);
            $scope.mData.APart_StockReturnClaimItem_TT = $scope.grid_parts_claim_ubah.data;
            for (var i = 0; i < $scope.mData.APart_StockReturnClaimItem_TT.length; i++) {
                //console.log($scope.mData.GridDetail[i].PartsName);
                // $scope.mData.GridDetail[i].StockReturnClaimId = insertResponse.StockReturnClaimId;



                $scope.mData.APart_StockReturnClaimItem_TT[i].StockReturnClaimStatusId = 0;
                $scope.mData.APart_StockReturnClaimItem_TT[i].OutletId = $scope.mData.OutletId;
                $scope.mData.APart_StockReturnClaimItem_TT[i].WarehouseId = $scope.mData.WarehouseId;
                $scope.mData.APart_StockReturnClaimItem_TT[i].PartId = $scope.mData.APart_StockReturnClaimItem_TT[i].PartsId;

                if ($scope.mData.APart_StockReturnClaimItem_TT[i].PartsCode == "") {

                    bsNotify.show({
                        title: "Parts Claim",
                        content: "No Material Tidak Boleh Ada Yang Kosong",
                        type: 'danger'
                    });

                    return;
                }

               
                // delete $scope.mData.APart_StockReturnClaimItem_TT[i];
            }
            console.log("Detail Data => ", $scope.mData.APart_StockReturnClaimItem_TT);

            //single transaction
            //PartsClaim.create($scope.mData, $scope.mData.APart_StockReturnClaimItem_TT);

            PartsClaim.update(mData, mData.APart_StockReturnClaimItem_TT).then(function(res) {
                    console.log('abis updatee cuy',res.data.Result);
                    bsNotify.show({
                        title: "Parts Claim",
                        content: "Data berhasil di update",
                        type: 'success'
                    });
                    $scope.goBack();
                },
                function(err) {
                    console.log("err=>", err);
                });
        }

        $scope.formShowHandler = function(kodeClaim) {
            $scope.formState = '';
            if (kodeClaim == kodeClaim) {
                $scope.formState = '00';
            } else {
                console.log("[Error]");
            }

            console.log($scope.formState);
            return $scope.formState;
        }

        // event button 'Input Alasan'
        $scope.SimpanDraft = function(mData) {
            $scope.mData.StockReturnClaimNo = [];

            // // END - PENOMORAN DRAFT=======================================================
            // $scope.mData.StockReturnClaimNo = [];
            // var d = new Date();
            // var vTahun = d.getFullYear().toString();
            // var vBulan = (d.getMonth()+1).toString();
            // var vHari = d.getDate().toString();
            // var vJam = d.getHours().toString();
            // var vMenit = d.getMinutes().toString();
            // var vDetik = d.getSeconds().toString();
            // var vTgl = vTahun + vBulan + vHari + vJam + vMenit + vDetik;

            // var strOutlet = mData.OutletId;
            // var lengthOutlet = strOutlet.length;
            // if(lengthOutlet > 6){
            //   strOutlet = strOutlet.substring(2);
            // }
            // console.log('vTgl', vTgl);
            // console.log('strOutlet', strOutlet);
            // $scope.mData.StockReturnClaimNo = 'DRAFT/' + strOutlet + '/'+ vTgl;
            // // END - PENOMORAN DRAFT=======================================================
            // $scope.GenerateDocNum();

            // Start Create
            console.log("Master Data => ", mData);
            if ((typeof mData.ShippingNo === 'undefined') || (typeof mData.PurchaseOrderNo === 'undefined') || (typeof mData.InvoiceId === 'undefined') || (typeof mData.CaseNo === 'undefined') || (typeof mData.PartClaimTypeId === 'undefined')) {
                bsNotify.show({
                    title: "Parts Claim",
                    content: "Data gagal disimpan, periksa kembali data field mandatory",
                    type: 'danger'
                });
            } else {
                PartsCurrentUser.getFormatId($scope.mData.ServiceTypeId, 'PC').then(function(res) {
                        var result = res.data;
                        console.log("FormatId = ", result[0].Results);

                        PartsCurrentUser.getDocumentNumber2(result[0].Results).then(function(res) {
                                var DocNo = res.data;

                                if (typeof DocNo === 'undefined' || DocNo == null) {
                                    bsNotify.show({
                                        title: "Parts Claim",
                                        content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                                        type: 'danger'
                                    });
                                } else {
                                    //yap : kalau create draft, tambahin stockreturnclaimstatusid nya jadi draft
                                    $scope.mData.StockReturnClaimStatusId = 1; // 1 = Draft
                                    $scope.mData.StockReturnClaimNo = DocNo[0];
                                    //$scope.mData.DocDate = DocNo[1];
                                    console.log("Generating DocNo & DocDate ->");
                                    console.log($scope.mData.StockReturnClaimNo);


                                    // start for create
                                    $scope.mData.APart_StockReturnClaimItem_TT = $scope.grid_parts_claim.data;
                                    console.log("$scope.grid_parts_claim.data : ", $scope.grid_parts_claim.data);
                                    for (var i = 0; i < $scope.mData.APart_StockReturnClaimItem_TT.length; i++) {
                                        //console.log($scope.mData.GridDetail[i].PartsName);
                                        // $scope.mData.GridDetail[i].StockReturnClaimId = insertResponse.StockReturnClaimId;
                                        $scope.mData.APart_StockReturnClaimItem_TT[i].StockReturnClaimStatusId = 0;
                                        $scope.mData.APart_StockReturnClaimItem_TT[i].OutletId = $scope.mData.OutletId;
                                        $scope.mData.APart_StockReturnClaimItem_TT[i].WarehouseId = $scope.mData.WarehouseId;
                                        $scope.mData.APart_StockReturnClaimItem_TT[i].PartId = $scope.mData.APart_StockReturnClaimItem_TT[i].PartsId;
                                        if ($scope.mData.APart_StockReturnClaimItem_TT[i].QtyMinus == null || $scope.mData.APart_StockReturnClaimItem_TT[i].QtyMinus === undefined) {
                                            $scope.mData.APart_StockReturnClaimItem_TT[i].QtyMinus = 0;
                                        }
                                        if ($scope.mData.APart_StockReturnClaimItem_TT[i].QtyPlus == null || $scope.mData.APart_StockReturnClaimItem_TT[i].QtyPlus === undefined) {
                                            $scope.mData.APart_StockReturnClaimItem_TT[i].QtyPlus = 0;
                                        }
                                        // added by sss on 2018-01-12
                                        if ($scope.mData.APart_StockReturnClaimItem_TT[i].QtyNG == null || $scope.mData.APart_StockReturnClaimItem_TT[i].QtyNG === undefined) {
                                            $scope.mData.APart_StockReturnClaimItem_TT[i].QtyNG = 0;
                                        }
                                        if ($scope.mData.APart_StockReturnClaimItem_TT[i].QtyOth == null || $scope.mData.APart_StockReturnClaimItem_TT[i].QtyOth === undefined) {
                                            $scope.mData.APart_StockReturnClaimItem_TT[i].QtyOth = 0;
                                        }
                                        //
                                        if ($scope.mData.APart_StockReturnClaimItem_TT[i].PartId === undefined) {
                                            var idx = $scope.mData.APart_StockReturnClaimItem_TT.indexOf(i);
                                            $scope.mData.APart_StockReturnClaimItem_TT.splice(idx, 1);
                                            console.log('spliceee');
                                        }
                                    }
                                    console.log("Detail Data => ", $scope.mData.APart_StockReturnClaimItem_TT);

                                    // single transaction
                                    // PartsClaim.create($scope.mData, $scope.mData.APart_StockReturnClaimItem_TT);
                                    PartsClaim.create($scope.mData, $scope.mData.APart_StockReturnClaimItem_TT).then(function(res) {
                                            var insertResponse = res.data;
                                            console.log(res.data);

                                            if (insertResponse.ResponseCode == 13) {
                                                bsNotify.show({
                                                    title: "Parts Claim",
                                                    content: "Data berhasil disimpan",
                                                    type: 'success'
                                                });
                                            } else {
                                                bsNotify.show({
                                                    title: "Parts Claim",
                                                    content: "Data gagal disimpan, silakan hubungi administrator",
                                                    type: 'danger'
                                                });
                                            }

                                            $scope.goBack();
                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                        }
                                    );

                                } // end else


                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );


                    },
                    function(err) {

                    }
                );


                // ngDialog.openConfirm ({
                //   template:'<div ng-include=\"\'app/parts/stockadjustment/referensi/template_simpan.html\'\"></div>',
                //   plain: true,
                //   controller: 'StockAdjustmentController',
                //  });
            }
        }

        // Hapus Filter
        $scope.onDeleteFilter = function(mData) {
            //Pengkondisian supaya seolaholah kosong
            $scope.mFilter.StockReturnClaimStatusId = null;
            $scope.mFilter.StockReturnClaimNo = null;
            $scope.mFilter.ShippingNo = null;
            $scope.mFilter.PurchaseOrderNo = null;
            $scope.mFilter.InvoiceNo = null;
            $scope.mFilter.startDate = null;
            $scope.mFilter.endDate = null;
            $scope.mFilter.NoCase = null;

        }
        $scope.checkValidation = function(data){
            console.log("data", data);
            var countError = 0;
            var countErrorField = 0;
            // ============
            var cekGrid = ($scope.mData.StockReturnClaimId === undefined) ? $scope.grid_parts_claim.data : $scope.grid_parts_claim_ubah.data;
            for(var i in cekGrid){
                if(cekGrid[i].SRCDealerRequestId == undefined || cekGrid[i].SRCDealerRequestId == null ){
                    countError++
                }
            }
            if(countError > 0){
                bsNotify.show({
                    title: "Parts Claim",
                    content: "Kolom dealer Request harus diisi, periksa kembali data field mandatory",
                    type: 'danger'
                });
                console.log("checkRefGINo: ", $scope.checkRefGINo);
                $scope.checkRefGINo = false;
                console.log("checkRefGINo: ", $scope.checkRefGINo);
                return false
            }
            // ===================
            if(($scope.mData.PartClaimTypeId == 9 || $scope.mData.PartClaimTypeId == 8) && $scope.isAttachBAP == 0){
                bsNotify.show({
                    title: "Parts Claim",
                    content: "Upload BAP terlebih dahulu, periksa kembali data field mandatory",
                    type: 'danger'
                });
                console.log("checkRefGINo: ", $scope.checkRefGINo);
                $scope.checkRefGINo = false;
                console.log("checkRefGINo: ", $scope.checkRefGINo);
                return false
            }
            // ================== validation for field
            // added by faisal on 2020-08-14
            if(data.StockReturnClaimId === undefined){
                var tempM = _.find($scope.grid_parts_claim.columnDefs, { "field": "QtyMinus" });
                var tempP = _.find($scope.grid_parts_claim.columnDefs, { "field": "QtyPlus" });
                var tempNG = _.find($scope.grid_parts_claim.columnDefs, { "field": "QtyNG" });
                var tempOth = _.find($scope.grid_parts_claim.columnDefs, { "field": "QtyOth" });
                var tmpField = null
                var tmpField2 = null
                var tipeField = 0;
                if(tempP.visible){
                    tmpField = 'QtyPlus'
                    tmpField2 = 'QtyMinus'
                    tipeField = 0;
                }else if(tempNG.visible){
                    tmpField = 'QtyNG'
                    tipeField = 1;
                }else if(tempOth.visible){
                    tmpField = 'QtyOth'
                    tipeField = 1;
                }
                for(var i in $scope.grid_parts_claim.data){
                    if(tipeField == 0){
                        if(($scope.grid_parts_claim.data[i][tmpField] <= 0 || $scope.grid_parts_claim.data[i][tmpField] == null) ){
                            countErrorField++
                        }
                        if(($scope.grid_parts_claim.data[i][tmpField2] <= 0 || $scope.grid_parts_claim.data[i][tmpField2] == null) ){
                            countErrorField++
                        }
                    }else{
                        if($scope.grid_parts_claim.data[i][tmpField] <= 0 || $scope.grid_parts_claim.data[i][tmpField] == null){
                            countErrorField++
                        }
                    }
                }

                if($scope.grid_parts_claim.data[0] == null){
                    bsNotify.show({
                        title: "Parts Claim",
                        content: "Material harus diisi !!",
                        type: 'danger'
                    });
                    console.log("checkRefGINo: ", $scope.checkRefGINo);
                    $scope.checkRefGINo = false;
                    console.log("checkRefGINo: ", $scope.checkRefGINo);
                    return false
                } else if(countErrorField > 0){
                    if(tipeField == 0){
                    bsNotify.show({
                        title: "Parts Claim",
                        content: "Kolom "+tmpField+" dan "+tmpField2+" minimal 1, Silahkan cek kembali",
                        type: 'danger'
                    });
                    }else{
                        bsNotify.show({
                            title: "Parts Claim",
                            content: "Kolom "+tmpField+" minimal 1, Silahkan cek kembali",
                            type: 'danger'
                        });
                    }
                    console.log("checkRefGINo: ", $scope.checkRefGINo);
                    $scope.checkRefGINo = false;
                    console.log("checkRefGINo: ", $scope.checkRefGINo);
                    return false
                }
            }
            return true
            
        }

        // ======== tombol  simpan =========
        $scope.simpan = function(mData) {
            $scope.checkRefGINo = true;
            var checkValidation = $scope.checkValidation(mData);
            if(!checkValidation){
                return false
            }
            if ((typeof mData.ShippingNo === 'undefined') || (typeof mData.PurchaseOrderNo === 'undefined') || (typeof mData.InvoiceId === 'undefined') || (typeof mData.CaseNo === 'undefined') || (typeof mData.PartClaimTypeId === 'undefined')) {
                bsNotify.show({
                    title: "Parts Claim",
                    content: "Data gagal disimpan, periksa kembali data field mandatory",
                    type: 'danger'
                });
            } else {

                if (!$scope.mData.StockReturnClaimId)
                    $scope.GenerateDocNum();

                console.log('StockReturnClaimNo', $scope.mData.StockReturnClaimNo);
                console.log('DOcDate', $scope.mData.DocDate);
                console.log('Simpan - mData', mData);
              
                PartsClaim.ApprovalData = mData;
                mData.APart_StockReturnClaimItem_TT = (mData.StockReturnClaimId === undefined) ? $scope.grid_parts_claim.data : $scope.grid_parts_claim_ubah.data;

                for (var i in mData.APart_StockReturnClaimItem_TT) {

                    // harus di kasih default karena klo tidak error dia
                    mData.APart_StockReturnClaimItem_TT[i].QtyMinus = mData.APart_StockReturnClaimItem_TT[i].QtyMinus || 0;
                    mData.APart_StockReturnClaimItem_TT[i].QtyPlus = mData.APart_StockReturnClaimItem_TT[i].QtyPlus || 0;

                    // added by sss on 2018-01-12
                    mData.APart_StockReturnClaimItem_TT[i].QtyNG = mData.APart_StockReturnClaimItem_TT[i].QtyNG || 0;
                    mData.APart_StockReturnClaimItem_TT[i].QtyOth = mData.APart_StockReturnClaimItem_TT[i].QtyOth || 0;
                    //

                    if (typeof $scope.mData.APart_StockReturnClaimItem_TT[i].PartsId !== 'undefined') {
                        $scope.mData.APart_StockReturnClaimItem_TT[i].PartId = $scope.mData.APart_StockReturnClaimItem_TT[i].PartsId;
                    }

                    // $scope.mData.APart_StockReturnClaimItem_TT[i].PartId = $scope.mData.APart_StockReturnClaimItem_TT[i].PartsId;
                    $scope.mData.APart_StockReturnClaimItem_TT[i].OutletId = $scope.mData.OutletId;
                    $scope.mData.APart_StockReturnClaimItem_TT[i].WarehouseId = $scope.mData.WarehouseId;

                    if (mData.APart_StockReturnClaimItem_TT[i].PartsCode == "") {

                        bsNotify.show({
                            title: "Parts Claim",
                            content: "No Material Tidak Boleh Ada Yang Kosong",
                            type: 'danger'
                        });

                        return;
                    }
                }


                console.log("mData.APart_StockReturnClaimItem_TT : ", mData.APart_StockReturnClaimItem_TT);

                if (mData.APart_StockReturnClaimItem_TT.length > 1) {

                    bsNotify.show({
                        title: "Parts Claim",
                        content: "Parts hanya bisa 1 record saja",
                        type: 'danger'
                    });

                    return;
                }
                console.log("mData.APart_StockReturnClaimItem_TT lebih 1 : ", mData.APart_StockReturnClaimItem_TT);


                if ($scope.mData.Attachment == "" || $scope.mData.Attachment == null || $scope.mData.Attachment == undefined ) {

                    bsNotify.show({
                        title: "Parts Claim",
                        content: "Belum ada file attachment !!",
                        type: 'danger'
                    });

                    $scope.checkRefGINo = false;
                    return;
                }
                


                // PartsClaim.setPartsClaimHeader(mData);

                //yap
                //scope ini dimasukkan ke data karena begitu melewati popup permohonanapproval di bawah, scope nya melupakan fungsi gobacknya
                //jadi harus di restore scopenya di proses berikutnya yaitu fungsi kirimApproval
                mData.scope = $scope;

                PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res){
                    $scope.Approver = res.data.Result[0].Approvers;
                    InputHasil.ApprovalData['Approver'] = $scope.Approver;
                    console.log("Approver : ", $scope.Approver);
                  });

                ngDialog.openConfirm({
                    template: '<div ng-include=\"\'app/parts/claim/parts_claim/crud/permohonanapproval.html\'\"></div>',
                    //templateUrl: '/app/parts/pomaintenance/alasan.html',
                    plain: true,
                    // scope: $scope,
                    controller: 'PartsClaimController',
                });
            }
        };

       
        $scope.KirimApproval = function(mData) {

                console.log("KirimApproval ", mData);
                console.log("faisal ", mData);
                $scope.Dataini = mData;

                // mData.APart_StockReturnClaimItem_TT = (mData.StockReturnClaimId === undefined) ? $scope.grid_parts_claim.data : $scope.grid_parts_claim_ubah.data;

                mData.StockReturnClaimStatusId = 2; // 2 = request approval
                $scope.ngDialog.close();
                
                var tempFunction = !mData.StockReturnClaimId ? PartsClaim.create : PartsClaim.update;                 
                

                console.log($scope.Dataini);
                tempFunction(mData, mData.APart_StockReturnClaimItem_TT).then(function(res) {
                        var insertResponse = res.data;
                        console.log('insertResponse', $scope.Dataini);                          
                        if (insertResponse.ResponseCode == 13 || insertResponse.ResponseCode == 23) {                            
                            $scope.ISGR = "";
                            console.log($scope.Dataini.StockReturnClaimNo);
                            var IsBPGR = $scope.Dataini.StockReturnClaimNo;                
                            var hasilnya = IsBPGR.substring(IsBPGR.length - 12, 6);                           
                            console.log(hasilnya);
                            if (hasilnya == 'G'){$scope.ISGR = 1}
                            else{$scope.ISGR = 0} 
                            if ($scope.ISGR == 1){
                                var data = {NoClaim : $scope.Dataini.StockReturnClaimNo, RecepientId : 1128}
                                PartsClaim.sendNotifToKabeng(data).then(function(res){
                                    console.log('notif send to KABENG GR')
                                });                    
                            }
                            else if ($scope.ISGR == 0){
                                var data = {NoClaim : $scope.Dataini.StockReturnClaimNo, RecepientId : 1129}
                                PartsClaim.sendNotifToKabeng(data).then(function(res){
                                    console.log('notif send to KABENG BP')
                                });
                
                            }

                            console.log ("is GR",  $scope.ISGR);                             
                            bsNotify.show({
                                title: "Parts Claim",
                                content: "Data berhasil disimpan",
                                type: 'success'
                            });
                        } else {
                            bsNotify.show({
                                title: "Parts Claim",
                                content: "Data gagal disimpan, silakan hubungi administrator",
                                type: 'danger'
                            });
                        }


                        $scope = mData.scope;
                        $scope.goBack();
                        //PartsClaim.formApi.setMode("grid");
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
                /*
                  PartsClaim.create(mData, mData.APart_StockReturnClaimItem_TT).then(function(res) {
                      var insertResponse = res.data;
                      console.log('insertResponse', insertResponse);

                      if(insertResponse.ResponseCode == 13)
                      {
                        bsNotify.show(
                        {
                          title: "Parts Claim",
                          content: "Data berhasil disimpan",
                          type: 'success'
                        }
                        );
                      }
                      else
                      {
                        bsNotify.show(
                        {
                          title: "Parts Claim",
                          content: "Data gagal disimpan, silakan hubungi administrator",
                          type: 'danger'
                        }
                        );
                      }


                      $scope = mData.scope;

                      $scope.goBack();
                      PartsClaim.formApi.setMode("grid");
                    },
                    function(err) {
                      console.log("err=>", err);
                      }
                  );
                */

                //$scope.goBack();
                PartsClaim.formApi.setMode("grid");

            }
            // ======== end tombol  simpan =========

        // ======== tombol batal - Claim- alasan =========
        $scope.batalPC = function(mData) {
            ngDialog.openConfirm({
                template: '<div ng-include=\"\'app/parts/claim/parts_claim/template/template_batal.html\'\"></div>',
                plain: true,
                controller: 'PartsClaimController',
            });
        };

        // dialog konfirmasi
        $scope.okBatalPC = function(data) {
            $scope.BatalData = PartsClaim.getStockReturnClaimHeader();
            $scope.BatalData.Notes = 'Alasan: ' + data.Batal + ', Catatan: ' + data.Catatan;
            console.log($scope.BatalData);

            PartsClaim.setStockReturnClaimHeader($scope.BatalData);

            ngDialog.openConfirm({
                template: '\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Anda yakin akan membatalkan Parts Claim ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="SetujuBatal(BatalData)">Ya</button>\
                     </div>\
                     </div>',
                plain: true,
                controller: 'PartsClaimController',
            });
        };

        $scope.SetujuBatal = function(data) {
            console.log('SetujuBatal');
            $scope.mData = data;
            $scope.ngDialog.close();
            $scope.mData.StockReturnClaimStatusId = 5;
            $scope.formApi = {};

            console.log($scope.mData);
            // [START] Get Search Appointment
            PartsClaim.cancel($scope.mData).then(function(res) {
                    console.log(res);
                    // alert("PartsClaim berhasil dibatalkan");
                    bsNotify.show({
                        title: "Parts Claim",
                        content: "Parts Claim has been successfully canceled.",
                        type: 'succes'
                    });

                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            PartsClaim.formApi.setMode("grid");
            // [END] Get Search Appointment
        }

        $scope.searchInv = true;
        //search po no
        $scope.onSearchPurchaseOrderNo = function(PurchaseOrderNo) {
            console.log("PurchaseOrderNo",PurchaseOrderNo);
            $scope.mData.PurchaseOrderId = PurchaseOrderNo.PurchaseOrderId;
            $scope.mData.PurchaseOrderNo = PurchaseOrderNo.PurchaseOrderNo;
            // $scope.mData.InvoiceId = PurchaseOrderNo.VendorInvoiceNo;
            // $scope.mData.CaseNo = PurchaseOrderNo.CaseNo;
            
            // PartsCurrentUser.getInvoiceNumber(PurchaseOrderNo.InvoiceId).then(function(res) {
            //         var result = res.data.Results
            //         console.log("result",result);
            //     },
            //     function(err) {
            //     }
            // );

            PartsClaim.getVendorInvoicePC(PurchaseOrderNo.GoodsReceiptId,PurchaseOrderNo.PurchaseOrderId).then(function(res) {
                var result = res.data;
                console.log("result invoice==>",result);
                $scope.mData.InvoiceId = null;
                $scope.mData.CaseNo = null;
                $scope.VendorInvoiceData = result;
                $scope.searchInv = false;
                $scope.searchCase = true;

            },
            function(err) {
            });
        }

        $scope.searchCase = true;
        //search po no
        $scope.onSearchVendorInvoiceNo = function(PurchaseOrderNo) {
            console.log("PurchaseOrderNo",PurchaseOrderNo);

            PartsClaim.getCaseNoPC(PurchaseOrderNo.GoodsReceiptId,PurchaseOrderNo.PurchaseOrderId,PurchaseOrderNo.VendorInvoiceNo).then(function(res) {
                var result = res.data;
                console.log("result",result);
                $scope.mData.CaseNo = null;
                $scope.CaseNoData = result;
                $scope.searchCase = false;

            },
            function(err) {
            });
        }

        $scope.searchShO = true;
        //
        $scope.onSearchShippingNo = function(ShippingNo) {
            if (ShippingNo === "" || ShippingNo == null) {
                bsNotify.show({
                    title: "Parts Claim",
                    content: "No Referensi tidak boleh kosong",
                    type: 'danger'
                });
            }
            // Eksekusi Nomor Dokumen
            else {
                PartsClaim.getDocDateByShippingNo(ShippingNo).then(function(res) {
                        var data = res.data;
                        console.log('cek booosss',data);

                        if (data == null) {
                            bsNotify.show({
                                title: "Parts Claim",
                                content: "No Shipping tidak ditemukan",
                                type: 'danger'
                            });
                        } else {
                            $scope.mData.GRDocDate = data.GRDocDate;
                            $scope.mData.GRDocTime = '00:00';

                            // PartsClaim.getPurchaseOrderNoNew(data.GoodsReceiptId).then(function(resu) {
                            PartsClaim.getPurchaseOrderPC(data.GoodsReceiptId).then(function(resu) {
                                var PurchaseOrderData = resu.data;
                                console.log('PurchaseOrderData', PurchaseOrderData);
                                // for (var i = 0; i < PurchaseOrderData.length; i++){
                                //     $scope.mData.InvoiceId = PurchaseOrderData[i].VendorInvoiceNo;
                                //     $scope.mData.CaseNo = PurchaseOrderData[i].CaseNo;
                                // }
                                $scope.PurchaseOrderData = PurchaseOrderData;
                                $scope.searchShO = false;
                                $scope.searchInv = true;
                                $scope.searchCase = true;

                            });
                        }
                    },
                    function(err) {
                        console.log("err=>", err);
                        bsNotify.show({
                            title: "Parts Claim",
                            content: "No Shipping tidak ditemukan",
                            type: 'danger'
                        });
                    });
            }
        }

        $scope.Upload = function(attachment) {
            console.log("Ini dari Upload Button =>");
            console.log($rootScope.Attachment);
        }

        // upload on file select or drop
        $scope.uploadPic = function(file) {
            $scope.checkRefGINo = true;
            console.log('file: ' + file);
            Upload.upload({
                url: '/api/as/PC/Upload',
                data: { file: file },
            }).then(function(resp) {
                console.log('Success: ' + resp.config.data.file.name + ' Uploaded. Response: ' + resp.data);
                console.log('resp', resp);
                $scope.progressStatus = 'Completed';
                $scope.checkRefGINo = false;
            }, function(resp) {
                console.log('Error status: ' + resp.status);
                console.log('Error respon: ',resp);
                $scope.progressStatus = 'Gagal !!';
            }, function(evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                $scope.progress = progressPercentage;
                $scope.progressDone = evt.loaded;
                $scope.progressTotal = evt.total;
                console.log("$scope.progressDone=>",$scope.progressDone);
                console.log("$scope.progressTotal=>",$scope.progressTotal);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        };
        $scope.uploadBAP = function(file) {
            // $scope.checkRefGINo = false;
            console.log('file: ' + file);
            Upload.upload({
                url: '/api/as/PC/Upload',
                data: { file: file },
            }).then(function(resp) {
                console.log('Success ' + resp.config.data.file.name + ' ploaded. Response: ' + resp.data);
                console.log('resp', resp);
                $scope.isAttachBAP=1;
                $scope.progressStatusBAP = 'Completed';
            }, function(resp) {
                console.log('Error status: ' + resp.status);
                $scope.progressStatusBAP = 'Gagal !!';
            }, function(evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                $scope.progressBAP = progressPercentage;
                $scope.progressDoneBAP = evt.loaded;
                $scope.progressTotalBAP = evt.total;
                console.log("$scope.progressDoneBAP=>",$scope.progressDoneBAP);
                console.log("$scope.progressTotalBAP=>",$scope.progressTotalBAP);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        };

        $scope.loadFileExcel = function(ExcelFile){
            $scope.progressBAP = 0;
            $scope.progressStatusBAP = '';
            $scope.progressDoneBAP = 0;
            $scope.progressTotalBAP = 100;
            var ExcelData = [];
            var myEl = angular.element( document.querySelector( '#uploadBAP' ) ); //ambil elemen dari dokumen yang di-upload
            console.log('myEl', myEl);
            var allowedExtensions =/(\.xlsx)$/i; 
            var tmpName = myEl[0].files[0].name;
              
            if (!allowedExtensions.exec(tmpName)) { 
                bsNotify.show({
                    title: "Format Dokumen Tidak Diziinkan",
                    content: "Format hanya bisa excel .XLSX, Harap ubah format file",
                    type: 'danger'
                });
                $('#uploadBAP').val("")
                return false; 
            }  
        }

        $scope.loadFileExceluploadAttach = function(ExcelFile){
            $scope.checkRefGINo = true;
            $scope.progress = 0;
            $scope.progressStatus = '';
            $scope.progressDone = 0;
            $scope.progressTotal = 100;
            console.log("$scope.progress=>",$scope.progress);
            console.log("$scope.progressDone=>",$scope.progressDone);
            var ExcelData = [];
            var myEl = angular.element( document.querySelector( '#uploadAttach' ) ); //ambil elemen dari dokumen yang di-upload
            console.log('myEl', myEl);
            var allowedExtensions =/(\.xlsx)$/i; 
            var tmpName = myEl[0].files[0].name;
              
            if (!allowedExtensions.exec(tmpName)) { 
                bsNotify.show({
                    title: "Format Dokumen Tidak Diziinkan",
                    content: "Format hanya bisa excel .XLSX, Harap ubah format file",
                    type: 'danger'
                });
                $('#uploadAttach').val("")
                return false; 
            }  
        }

        $scope.HandleFileSelect = function(evt) {
            var files = evt.target.files;
            var file = files[0];

            if (files && file) {
                var reader = new FileReader();

                reader.onload = function(readerEvt) {
                    var binaryString = readerEvt.target.result;
                    console.log(btoa(binaryString));
                };

                reader.readAsBinaryString(file);
            }
        }

        // ======== end tombol batal - Claim =========
        $scope.ToNameClaim = function(kodeClaim) {
            var statusName;
            switch (kodeClaim) {
                case 1:
                    statusName = "Sht (Barang Kurang)";
                    break;
                case 2:
                    statusName = "Ex (Barang Lebih)";
                    break;
                case 3:
                    statusName = "U (Barang Salah - Rcv Diff from Inv)";
                    break;
                case 4:
                    statusName = "P1 (Barang Salah - Diff part with label)";
                    break;
                case 5:
                    statusName = "P2 (Barang Salah - Incomplete Part)";
                    break;
                case 6:
                    statusName = "P3 (Barang Salah -  Can't be installed)";
                    break;
                case 7:
                    statusName = "S1 (Barang Rusak - Parts Rusak)";
                    break;
                case 8:
                    statusName = "S2 (Barang Rusak - Parts & Peti Rusak)";
                    break;
                case 9:
                    statusName = "S3 (Barang Rusak - Broken Body & Kaca)";
                    break;
                case 10:
                    statusName = "P/N (Barang Salah - Miss Inform)";
                    break;
                case 11:
                    statusName = "MKT (Masalah Harga)";
                    break;
                case 12:
                    statusName = "OTH (Double Supply)";
                    break;
            }
            return statusName;
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.ToNameStatus = function(StockReturnClaimStatusId) {
            var statusName;
            switch (StockReturnClaimStatusId) {
                case 1:
                    statusName = "Draft";
                    break;
                case 2:
                    statusName = "Request Approval";
                    break;
                case 3:
                    statusName = "Outstanding Claim"; //"Open";
                    break;
                case 4:
                    statusName = "Answered Claim"; //Partial";
                    break;
                case 5:
                    statusName = "Cancelled"; //Completed";
                    break;
                    // case 6:
                    //   statusName = "Cancelled";
                    //   break;
            }
            return statusName;
        }

        $scope.ToNameApproval = function(ApprovalStatus) {
            var approvalName;
            switch (ApprovalStatus) {
                case 1:
                    approvalName = "Disetujui";
                    break;
                case 2:
                    approvalName = "Ditolak";
                    break;
                case 3:
                    approvalName = "Diajukan"; 
                    break;
            }
            return approvalName;
        }

        $scope.ToSRCName = function(SRCId) {
            var statusName;
            // console.log("SRCId ", SRCId);
            switch (SRCId) {
                case '1':
                case 1:
                    statusName = "C - TAM Credit";
                    break;
                case '2':
                case 2:
                    statusName = "D - Dealer Credit";
                    break;
                case '3':
                case 3:
                    statusName = "N - Qty Plus, Can't Sold";
                    break;
                default:
                    statusName = " - "
            }
            return statusName;
        }

        $scope.ToSRCNameNew = function(SRCId) {
            var statusName = '';
            console.log("SRCId ", SRCId.SRCDealerRequestId);
            switch (SRCId.SRCDealerRequestId) {
                case '1':
                case 1:
                    statusName = "C - TAM Credit";
                    break;
                case '2':
                case 2:
                    statusName = "D - Dealer Credit";
                    break;
                case '3':
                case 3:
                    statusName = "N - Qty Plus, Can't Sold";
                    break;
                default:
                    statusName = " - "
            }
            return statusName;
        }

        $scope.TotalAmount = function(qty_invoice, harga_net_dealer, row) {
            if (qty_invoice == null || harga_net_dealer == null) {
                return 0;
            } else {
                var totalAmount = qty_invoice * harga_net_dealer;
                // console.log(qty_invoice);
                // console.log(harga_net_dealer);
                $scope.mData.APart_StockReturnClaimItem_TT[$scope.grid_parts_claim.data.indexOf(row.entity)].TotalAmount = totalAmount;

                return totalAmount;
            }

        }

        $scope.Minus = function(qty_invoice, qty_received, row) {
            // if(qty_old_received == null || qty_received == null)
            // {
            //   return 0;
            // }
            // else
            // {
            //   if(!$scope.mData.APart_StockReturnClaimItem_TT[$scope.grid_parts_claim.data.indexOf(row.entity)].OldQtyReceived)
            //     $scope.mData.APart_StockReturnClaimItem_TT[$scope.grid_parts_claim.data.indexOf(row.entity)].OldQtyReceived = $scope.mData.APart_StockReturnClaimItem_TT[$scope.grid_parts_claim.data.indexOf(row.entity)].QtyReceived

            //   var tempQty = qty_old_received - qty_received;

            //   console.log(qty_invoice);
            //   console.log(qty_received);
            //   $scope.mData.APart_StockReturnClaimItem_TT[$scope.grid_parts_claim.data.indexOf(row.entity)].QtyMinus = tempQty < 0 ? tempQty : 0;
            //   $scope.mData.APart_StockReturnClaimItem_TT[$scope.grid_parts_claim.data.indexOf(row.entity)].QtyPlus = tempQty > 0 ? tempQty : 0;

            //   return qtyminus;
            // }

            if (qty_invoice - qty_received > 0) {
                row.entity.QtyMinus = qty_invoice - qty_received;
                return qty_invoice - qty_received;
            } else {
                return 0;
            }
        }

        $scope.Plus = function(qty_invoice, qty_received, row) {
            if (qty_received - qty_invoice > 0) {
                row.entity.QtyPlus = qty_received - qty_invoice;
                return qty_received - qty_invoice;
            } else {
                return 0;
            }
        }

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'Id',
                    field: 'StockReturnClaimId',
                    width: '7%',
                    visible: false
                },
                {
                    displayName: 'No PC DMS',
                    name: 'No Parts Claim DMS',
                    field: 'StockReturnClaimNo',
                    width: '10%'
                },
                {
                    displayName: 'No PC TAM',
                    name: 'No Parts Claim TAM',
                    field: 'StockReturnClaimNoTAM',
                    width: '10%'
                },
                {
                    displayName: 'Tanggal PC',
                    name: 'Tanggal Parts Claim',
                    field: 'DocDate',
                    cellFilter: 'date:\'yyyy-MM-dd\'',
                    width: '9%'
                },
                {
                    displayName: 'No. Shipping',
                    name: 'No. Shipping',
                    field: 'ShippingNo',
                    width: '9%'
                },
                {
                    displayName: 'No. PO',
                    name: 'No. PO',
                    field: 'PurchaseOrderNo',
                    width: '9%'
                },
                {
                    displayName: 'No. Vendor Inv.',
                    name: 'No. Vendor Inv.',
                    field: 'InvoiceNo',
                    width: '9%'
                },
                {
                    displayName: 'No. Case',
                    name: 'No. Case',
                    field: 'CaseNo',
                    width: '9%'
                },
                {
                    displayName: 'Tanggal GR',
                    name: 'Tanggal GR',
                    field: 'GRDate',
                    cellFilter: 'date:\'yyyy-MM-dd\'',
                    width: '9%'
                },
                {
                    displayName: 'Status',
                    name: 'Status',
                    field: 'StockReturnClaimStatusId',
                    width: '15%',
                    visible: false
                },
                // {
                //     displayName: 'Stock Return Claim Id',
                //     name: 'Stock Return Claim Id',
                //     field: 'StockReturnClaimId',
                //     width: '15%',
                //     visible: false
                // }, di minta di hide 
                {
                    displayName: 'Status',
                    name: 'Status',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatus(row.entity.StockReturnClaimStatusId)}}</div>',
                    width: '9%'
                },
                {
                    displayName: 'Approval',
                    name: 'Approval',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameApproval(row.entity.ApprovalStatus)}}</div>',
                    width: '6%',
                    visible: false
                },
                {
                    displayName: 'TAM Approval',
                    name: 'ApprovalTAM',
                    field: 'SRCJudgementCode',
                    width: '8%'
                }
            ]

        };

        $scope.showMe = function() {
            confirm('Apakah Anda yakin menghapus data ini?');
        };

        $scope.SearchMaterial = function(NoMaterial, gridName) {
            if (NoMaterial === "") {
                bsNotify.show({
                    title: "Parts Claim",
                    content: "No Material tidak boleh kosong",
                    type: 'danger'
                });
            } else {
                PartsClaim.getDetailByMaterialNo(NoMaterial, $scope.mData.PurchaseOrderNo, $scope.mData.PartClaimTypeId, 
                    $scope.mData.ShippingNo).then(function(res) {
                        var gridData = res.data;
                        console.log(gridData[0]);

                        if (gridData[0] == null) {
                            bsNotify.show({
                                title: "Parts Claim",
                                content: "No Material tidak ditemukan, periksa No.Material, No.PO, No.Vendor Invoice, dan No.Case",
                                type: 'danger'
                            });
                            // $scope.searchNoMaterial = false;
                            $scope[gridName].data[0].PartsName = null;
                            $scope[gridName].data[0].QtyInvoice = null
                            $scope[gridName].data[0].QtyReceived = null;
                            $scope[gridName].data[0].QtyMinus = null;
                            $scope[gridName].data[0].QtyPlus = null;
                            $scope[gridName].data[0].QtyNG = null;
                            $scope[gridName].data[0].QtyOth = null;
                            $scope[gridName].data[0].Name = null;
                            $scope[gridName].data[0].DealerNetPrice = null;
                            $scope[gridName].data[0].TotalAmount = 0;
                            $scope[gridName].data[0].SRCDealerRequestId = null;
                            return;
                        }

                        // added by sss on 2018-01-12
                        gridData[0].QtyMinus = 0;
                        gridData[0].QtyPlus = 0;
                        gridData[0].QtyNG = 0;
                        gridData[0].QtyOth = 0;
                        //

                        if ($scope.mData.PartClaimTypeId >= 10 && $scope.mData.PartClaimTypeId <= 12) {
                            gridData[0].QtyOth = gridData[0].QtyReceived;
                        }

                        var detail = gridData[0];

                        var rowIndex = $scope[gridName].data.findIndex(function(obj) {
                            return obj.PartsCode == NoMaterial;
                        });

                        var countMaterial = 0;

                        for (var z = 0; z < $scope[gridName].data.length; z++)
                            if ($scope[gridName].data[z].PartsCode == NoMaterial)
                                countMaterial++;
                        console.log('countMaterial: '+countMaterial);

                        if (countMaterial > 1) {
                            bsNotify.show({
                                title: "Parts Claim",
                                content: "Material Tidak Boleh Double",
                                type: 'danger'
                            });
                            return;
                        } else if (gridData[0].PurchaseOrderNo != $scope.mData.PurchaseOrderNo) {
                            console.log('No PO: '+gridData[0].PurchaseOrderNo);
                            console.log('PO: '+$scope.mData.PurchaseOrderNo);
                            bsNotify.show({
                                title: "Parts Claim",
                                content: "No. PO salah !!",
                                type: 'danger'
                            });
                            return;
                        } else if (gridData[0].VendorInvoiceNo != $scope.mData.InvoiceId) {
                            console.log('No Vendor Invoice: '+gridData[0].VendorInvoiceNo);
                            console.log('InvoiceId: '+$scope.mData.InvoiceId);
                            bsNotify.show({
                                title: "Parts Claim",
                                content: "No. Vendor Invoice salah !!",
                                type: 'danger'
                            });
                            return;
                        } else if (gridData[0].CaseNo != $scope.mData.CaseNo) {
                            console.log('No Case: '+gridData[0].CaseNo);
                            console.log('Case No: '+$scope.mData.CaseNo);
                            bsNotify.show({
                                title: "Parts Claim",
                                content: "No. Case salah !!",
                                type: 'danger'
                            });
                            return;
                        }

                        if (gridData[0] == null) {
                            bsNotify.show({
                                title: "Parts Claim",
                                content: "No Material tidak ditemukan",
                                type: 'danger'
                            });
                        } else {
                            // $scope.grid_parts_claim.data[$scope.grid_parts_claim.data.length - 1] = gridData[0];

                            //untuk cari index yang mw di replace di gridnya
                            var rowIndex = $scope[gridName].data.findIndex(function(obj) {
                                return obj.PartsCode == NoMaterial;
                            });

                            $scope[gridName].data[rowIndex] = gridData[0];
                            $scope.mData.APart_StockReturnClaimItem_TT = $scope.grid_parts_claim.data;
                            $scope.searchNoMaterial = true;
                            console.log("checkRefGINo: ", $scope.checkRefGINo);
                            // $scope.grid_parts_claim.data.push({});
                            // added by sss on 2018-01-04
                            // $scope.Add(detail,index,gridName);
                            //
                        }

                    },
                    function(err) {
                        console.log("err=>", err);
                        confirm("Nomor Material Tidak Ditemukan.");
                    }
                );
            }
        }
//naro
        $scope.Add = function(gridName) {
             //var n = $scope.grid_parts_claim.data.length + 1;

            if($scope[gridName].data.length == 0){
            $scope[gridName].data.push({
                "PartsCode": "",
                "PartsName": "",
                "QtyAdjustment": "",
                "UomId": ""

            });
         } else if($scope[gridName].data.length == 1){
            bsNotify.show({
                title: "Parts Claim",
                content: "Parts hanya bisa 1 record saja",
                type: 'danger'
            });
        
            return;
         }

            // $scope.grid_parts_claim.data.push(detail);
            // $scope.grid_parts_claim_ubah.data.push(detail);
            // $scope.grid_parts_claim_lihat.data.push(detail);
        }

        $scope.Delete = function(row) {
            var index = $scope.grid_parts_claim.data.indexOf(row.entity);
            $scope.grid_parts_claim.data.splice(index, 1);
            console.log($scope.grid_parts_claim.data);
            console.log(index);
        };

        //----------------------------------
        // Dummy grid_parts_claim
        //----------------------------------
        $scope.gridApi = {};
        $scope.gridApiLihat = {};
        $scope.gridApiUbah = {};
        $scope.grid_parts_claim = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                $scope.gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {
                    // console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    // console.log('newValue',newValue);
                    // console.log('oldValue',oldValue);
                    // console.log('Position',row);

                    // rowEntity.QtyMinus = rowEntity.QtyInvoice - rowEntity.QtyReceived;

                    if (colDef.field == 'QtyNG' || colDef.field == 'QtyOth') {
                        if (newValue > rowEntity.QtyReceived) {
                            rowEntity[colDef.field] = oldValue;
                        } else {
                            rowEntity[colDef.field] = newValue;
                        }
                    }
                    rowEntity.TotalAmount = (rowEntity.QtyMinus * rowEntity.DealerNetPrice);
                });
            },
            columnDefs: [{
                        name: 'PartsCode',
                        displayName: 'No. Material',
                        enableCellEdit: true,
                        //field: 'NoMaterial',
                        width: '13%',
                        cellTemplate: '<div><div class="input-group">\
                      <input type="text" ng-value="row.entity.PartsCode" required>\
                      <label class="input-group-btn">\
                          <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode,\'grid_parts_claim\')">\
                              <i class="fa fa-search fa-1"></i>\
                          </span>\
                      </label>\
                  </div></div>'
                    },
                    { name: 'Nama Material', field: 'PartsName', width: '20%', enableCellEdit: false },
                    // { name:'Qty Invoice', cellTemplate:'<center><input type="number" style="width: 50%"></center> ' },
                    // { name:'Qty Received', cellTemplate:'<center><input type="number" style="width: 50%"></center> ' },
                    { name: 'Qty Invoice', field: 'QtyInvoice', enableCellEdit: false },
                    { name: 'Qty Received', field: 'QtyReceived', type: 'number', },
                    {
                        name: 'Qty Minus',
                        field: 'QtyMinus',
                        cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.Minus(row.entity.QtyInvoice, row.entity.QtyReceived, row)}}</div>',
                        // added by sss on 2018-01-09
                        visible: true,
                        enableCellEdit: false
                            //
                    },
                    {
                        name: 'Qty Plus',
                        field: 'QtyPlus',
                        cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.Plus(row.entity.QtyInvoice, row.entity.QtyReceived, row)}}</div>',
                        // added by sss on 2018-01-09
                        visible: true,
                        enableCellEdit: false
                            //
                    },
                    // added by sss on 2018-01-09
                    {
                        name: 'Qty NG',
                        displayName: 'Qty NG',
                        field: 'QtyNG',
                        type: 'number',
                        // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.Plus(row.entity.QtyInvoice, row.entity.QtyReceived, row)}}</div>',
                        // cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.QtyNG}}<form name="inputForm"><input type="INPUT_TYPE" ng-class="\'colt\' + col.uid" ui-grid-editor ng-model="MODEL_COL_FIELD"></form></div>',
                        visible: false,
                        enableCellEdit: false
                    },
                    {
                        name: 'Qty Oth',
                        field: 'QtyOth',
                        type: 'number',
                        // cellTemplate: '<div><form name="inputForm"><input type="INPUT_TYPE" ng-class="\'colt\' + col.uid" ui-grid-editor ng-model="MODEL_COL_FIELD"></form></div>',
                        visible: false,
                        enableCellEdit: false
                    },
                    //
                    { name: 'Satuan', field: 'Name', enableCellEdit: false },
                    { name: 'Harga Net Dealer', field: 'DealerNetPrice', cellFilter: 'number', enableCellEdit: false },
                    {
                        name: 'Total Harga Net Dealer',
                        field: 'TotalAmount',
                        cellFilter: 'number',
                        cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.TotalAmount(row.entity.QtyInvoice, row.entity.DealerNetPrice, row)}}</div>',
                        enableCellEdit: false
                    },
                    {
                        name: 'SRCDealerRequestId',
                        field: 'SRCDealerRequestId',
                        visible: true,
                        width: 100,
                        displayName: 'Dealer Request',
                        enableCellEdit: true,
                        editableCellTemplate: 'ui-grid/dropdownEditor',
                        cellFilter: 'mapAdj',
                        editDropdownValueLabel: 'adj',
                        editDropdownOptionsArray: [{ id: 1, adj: 'C - TAM Credit' }, { id: 2, adj: 'D - Dealer Credit' }, { id: 3, adj: 'N - Qty Plus, Can\'t Sold' }],
                        cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.SRCDealerRequestId">{{grid.appScope.$parent.ToSRCName(row.entity.SRCDealerRequestId)}}</div>',
                    },
                    {
                        name: 'Action',
                        cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>',
                        visible: true
                    }
                ]
                // ,
                // data : [      {
                //                    "no_material": "35677-33996",
                //                    "nama_material": "EXPANDER",
                //                    "qty_invoice": "3",
                //                    "qty_received": "3",
                //                    "qty_minus": "3",
                //                    "satuan": "Pieces",
                //                    "harga_net_dealer": "150.000",
                //                    "total_harga_net_dealer" : "450.000"
                //                },
                //                {
                //                    "no_material": "35677-64438",
                //                    "nama_material": "SHOCK BREAKER",
                //                    "qty_invoice": "3",
                //                    "qty_received": "3",
                //                    "qty_minus": "3",
                //                    "satuan": "Pieces",
                //                    "harga_net_dealer": "150.000",
                //                    "total_harga_net_dealer" : "450.000"
                //                }
                //            ]
        };

        //----------------------------------
        // Dummy grid_parts_claim
        //----------------------------------
        $scope.grid_parts_claim_lihat = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            onRegisterApi: function(gridApi) {
                $scope.gridApiLihat = gridApi;
                $scope.gridApiLihat.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {
                    // console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    // console.log('newValue',newValue);
                    // console.log('oldValue',oldValue);
                    // console.log('Position',row);

                    // rowEntity.QtyMinus = rowEntity.QtyInvoice - rowEntity.QtyReceived;

                    if (colDef.field == 'QtyNG' || colDef.field == 'QtyOth') {
                        if (newValue > rowEntity.QtyReceived) {
                            rowEntity[colDef.field] = oldValue;
                        } else {
                            rowEntity[colDef.field] = newValue;
                        }
                    }
                    rowEntity.TotalAmount = (rowEntity.QtyMinus * rowEntity.DealerNetPrice);
                });
            },
            columnDefs: [{
                    name: 'PartsCode',
                    displayName: 'No. Material',
                    enableCellEdit: true,
                    //field: 'NoMaterial',
                    height : '10%',
                    width: '13%',
                    cellTemplate: '<div><div class="input-group">\
                      <input type="text" ng-value="row.entity.PartsCode" required>\
                      <label class="input-group-btn">\
                          <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode,index,\'grid_parts_claim\')">\
                              <i class="fa fa-search fa-1"></i>\
                          </span>\
                      </label>\
                  </div></div>'
                },
                { name: 'Nama Material', field: 'PartsName', width: '20%', enableCellEdit: false },
                // { name:'Qty Invoice', cellTemplate:'<center><input type="number" style="width: 50%"></center> ' },
                // { name:'Qty Received', cellTemplate:'<center><input type="number" style="width: 50%"></center> ' },
                { name: 'Qty Invoice', field: 'QtyInvoice', enableCellEdit: false },
                { name: 'Qty Received', field: 'QtyReceived' },
                {
                    name: 'Qty Minus',
                    field: 'QtyMinus',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.Minus(row.entity.QtyInvoice, row.entity.QtyReceived, row)}}</div>',
                    // added by sss on 2018-01-09
                    visible: true,
                    enableCellEdit: false
                        //
                },
                {
                    name: 'Qty Plus',
                    field: 'QtyPlus',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.Plus(row.entity.QtyInvoice, row.entity.QtyReceived, row)}}</div>',
                    // added by sss on 2018-01-09
                    visible: true,
                    enableCellEdit: false
                        //
                },
                // added by sss on 2018-01-09
                {
                    name: 'Qty NG',
                    field: 'QtyNG',
                    displayName: 'Qty NG',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.Plus(row.entity.QtyInvoice, row.entity.QtyReceived, row)}}</div>',
                    visible: false,
                    enableCellEdit: false
                },
                {
                    name: 'Qty Oth',
                    field: 'QtyOth',
                    visible: false,
                    enableCellEdit: false
                },
                //
                { name: 'Satuan', field: 'Name', enableCellEdit: false },
                { name: 'Harga Net Dealer', field: 'DealerNetPrice', enableCellEdit: false },
                {
                    name: 'Total Harga Net Dealer',
                    field: 'TotalAmount',
                    enableCellEdit: false
                },
                {
                    name: 'Dealer Request',
                    field: 'SRCDealerRequestId',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToSRCName(row.entity.SRCDealerRequestId)}}</div>',
                    enableCellEdit: false
                },
                {
                    name: 'TAM Judgement',
                    field: 'SRCJudgementCode',
                    enableCellEdit: false
                },
                {
                    name: 'Qty TAM Judgement',
                    field: 'SRCJudgementQTY',
                    enableCellEdit: false
                }
                // {
                //   name:'Dealer Request',
                //   cellTemplate: ' <div class="ui-grid-cell-contents input-group">\
                //                   <select style="width: 70%">\
                //                     <option>D - Dealer Credit</option>\
                //                     <option>N - Qty Plus, can\'t sold</option>\
                //                   </select></div>'
                // },
            ]
        };

        $scope.grid_parts_claim_ubah = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            onRegisterApi: function(gridApi) {
                $scope.gridApiUbah = gridApi;
                $scope.gridApiUbah.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {
                    // console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    // console.log('newValue',newValue);
                    // console.log('oldValue',oldValue);
                    // console.log('Position',row);

                    // rowEntity.QtyMinus = rowEntity.QtyInvoice - rowEntity.QtyReceived;

                    if (colDef.field == 'QtyNG' || colDef.field == 'QtyOth') {
                        if (newValue > rowEntity.QtyReceived) {
                            rowEntity[colDef.field] = oldValue;
                        } else {
                            rowEntity[colDef.field] = newValue;
                        }
                    }
                    rowEntity.TotalAmount = (rowEntity.QtyMinus * rowEntity.DealerNetPrice);
                });
            },
            columnDefs: [{
                    name: 'PartsCode',
                    displayName: 'No. Material',
                    enableCellEdit: true,
                    //field: 'NoMaterial',
                    height:'10%',
                    width: '13%',
                    cellTemplate: '<div><div class="input-group">\
                      <input type="text" ng-value="row.entity.PartsCode" required>\
                      <label class="input-group-btn">\
                          <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode,index,\'grid_parts_claim_ubah\')">\
                              <i class="fa fa-search fa-1"></i>\
                          </span>\
                      </label>\
                  </div></div>'
                },
                { name: 'Nama Material', field: 'PartsName', width: '20%', enableCellEdit: false },
                // { name:'Qty Invoice', cellTemplate:'<center><input type="number" style="width: 50%"></center> ' },
                // { name:'Qty Received', cellTemplate:'<center><input type="number" style="width: 50%"></center> ' },
                { name: 'Qty Invoice', field: 'QtyInvoice', enableCellEdit: false },
                { name: 'Qty Received', field: 'QtyReceived' },
                // {
                //   name:'Qty Minus',
                //   field: 'QtyMinus'
                // },
                {
                    name: 'Qty Minus',
                    field: 'QtyMinus',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.Minus(row.entity.QtyInvoice, row.entity.QtyReceived, row)}}</div>',
                    // added by sss on 2018-01-09
                    visible: true,
                    enableCellEdit: false
                        //
                },
                {
                    name: 'Qty Plus',
                    field: 'QtyPlus',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.Plus(row.entity.QtyInvoice, row.entity.QtyReceived, row)}}</div>',
                    // added by sss on 2018-01-09
                    visible: true,
                    enableCellEdit: false
                        //
                },
                // added by sss on 2018-01-09
                {
                    name: 'Qty NG',
                    field: 'QtyNG',
                    displayName: 'Qty NG',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.Plus(row.entity.QtyInvoice, row.entity.QtyReceived, row)}}</div>',
                    visible: false,
                    enableCellEdit: false
                },
                {
                    name: 'Qty Oth',
                    field: 'QtyOth',
                    visible: false,
                    enableCellEdit: false
                },
                //
                { name: 'Satuan', field: 'Name', enableCellEdit: false },
                { name: 'Harga Net Dealer', field: 'DealerNetPrice', enableCellEdit: false },
                {
                    name: 'Total Harga Net Dealer',
                    field: 'TotalAmount',
                    enableCellEdit: false
                },
                {
                    name: 'Dealer Request',
                    field: 'SRCDealerRequestId',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToSRCName(row.entity.SRCDealerRequestId)}}</div>',
                    enableCellEdit: false
                }
            ]
        };

    })

.directive('fileModel', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function() {
                scope.$apply(function() {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}])

.directive('upload', function($rootScope) {
        return {
            restrict: 'EA',
            link: function(scope, elem, attrs) {
                elem.on("change", function(evt) {
                    var file = evt.currentTarget.files[0];
                    var reader = new FileReader();
                    reader.onload = function(evt) {
                        scope.$watch(function($scope) {
                            $rootScope.myImage = evt.target.result;
                            console.log($rootScope.myImage);
                            $rootScope.Attachment = $rootScope.myImage;
                        });
                    };
                    reader.readAsDataURL(file);
                });
            }
        };
    })
    .filter('mapAdj', function() {
        var adjHash = {
            1: 'C - TAM Credit',
            2: 'D - Dealer Credit',
            3: 'N - Qty Plus, Can\'t Sold'
        };

        return function(input) {
            if (!input) {
                return '';
            } else {
                return adjHash[input];
            }
        };
    });
