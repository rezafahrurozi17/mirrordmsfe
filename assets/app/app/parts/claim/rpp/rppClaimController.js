angular.module('app')
    .controller('RppClaimController', function($scope, $http, CurrentUser, RppClaim, ngDialog, bsNotify, $timeout, PartsCurrentUser, PartsGlobal, PrintRpt, Upload, bsAlert) {

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mData = {}; //Model
        $scope.mFilter = {};
        $scope.xRole = {
            selected: []
        };
        $scope.uploadFiles = [];
        $scope.captionLabel = {};
        $scope.captionLabel.new = "Buat";
        //model batal
        $scope.ngDialog = ngDialog;
        $scope.ApprovalData = RppClaim.ApprovalData;
        $scope.mData.Attachment = '1';
        $scope.ApprovalProcessId = 8902;

        var itIsLink = false;
        var paramNew;
        var GRDate2 = null; //var global for grdate

        //----------------------------------
        // Start-Up
        //----------------------------------
        // added by sss on 2018-03-14
        var loadedContent = false;
        //
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $timeout(function() {
                // added by sss on 2018-03-14
                loadedContent = true;
                //
                console.log("Approve = ", $scope.checkRights(4));
                console.log("dev_parts 6 Desember 2017 ", document.lastModified);
                RppClaim.formApi = $scope.formApi;
                if ($scope.checkRights(4)) {
                    $scope.mFilter.StockReturnRPPStatusId = 2;
                    var today = new Date();
                    $scope.mFilter.startDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                    $scope.mFilter.endDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

                    $scope.getData();
                }

                if (itIsLink) {
                    $scope.mData.AppointmentNo = paramNew.AppointmentNo;
                    $scope.onSearchRefRPPNo(paramNew.AppointmentNo);

                    itIsLink = false;
                }

            });
        });

        //----------------------------------
        // Claim Gateway Entry Form
        //----------------------------------
        $scope.isOverlayForm = false;

        $scope.mData.kodeClaimtypes = ['Sht (Barang Kurang)', 'Ex (Barang Lebih)', 'U (Barang Salah - Rcv Diff from Inv)', 'P1 (Barang Salah - Diff part with label)', 'P2 (Barang Salah - Incomplete Part)', 'P3 (Barang Salah -  Can\'t be installed)', 'S1 (Barang Rusak - Parts Rusak)', 'S2 (Barang Rusak - Parts & Peti Rusak)', 'S3 (Barang Rusak - Broken Body & Kaca)', 'P/N (Barang Salah - Miss Inform)', 'MKT (Masalah Harga)', 'OTH (Double Supply)'];
        $scope.mData.mkodeClaimtype = $scope.mData.kodeClaimtypes[0];

        $scope.isEditState = false;

        //model batal
        $scope.Alasan = {};

        $scope.BatalData = RppClaim.getStockReturnRPPHeader();

        //----------------------------------
        // CRUD state C = 1, R = 2, U = 3, D = 4
        // CRUD state + Form Number = 111
        //----------------------------------
        $scope.crudState = '';

        $scope.checkRights = function(bit) {
            var p = $scope.myRights & Math.pow(2, bit);
            var res = (p == Math.pow(2, bit));
            //console.log("myRights => ", $scope.myRights);
            return res;
        }

        $scope.getRightX = function(a, b) {
            var i = 0;
            if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
            else if ($scope.checkRights(b)) i = 2;
            else if ($scope.checkRights(a)) i = 1;
            return i;
        }
        $scope.getRightMaterialType = function() {
            // var i = $scope.getRightX(8, 9);
            // return i;
            var materialTypeId = 3;
            switch($scope.user.RoleId)
            {
                case 1135:
                case 1122:
                case 1123:
                    materialTypeId = 1;
                    break;
                case 1124:
                case 1125:
                    materialTypeId = 2;
                    break;
            }
            return materialTypeId;

        }
        $scope.getRightServiceType = function() {
            // var i = $scope.getRightX(10, 11);
            // return i;
            var serviceTypeId = 0;
            switch($scope.user.RoleId)
            {
                case 1135:
                case 1125:
                case 1122:
                    serviceTypeId = 1;
                    break;
                case 1123:
                case 1124:
                    serviceTypeId = 0;
                    break;
            }
            return serviceTypeId;
        }

        //button kembali pada overlay
        $scope.goBack = function() {
            $scope.isOverlayForm = false;
            RppClaim.formApi.setMode("grid");
        }

        $scope.formApi = {};

        // $scope.$watch('mData.Attachment', function() {
        //     bsNotify.show({
        //         title: "RPP Claim",
        //         content: "Upload berhasil",
        //         type: 'success'
        //       });
        // });

        $scope.DownloadFile = function(attachment) {
            console.log("Ini dari DownloadFile Button =>");
            console.log(attachment);
        }

        // upload on file select or drop
        $scope.uploadPic = function(file) {
            console.log('file: ' + file);
            Upload.upload({
                url: '/api/as/RPP/Upload',
                data: { file: file },
            }).then(function(resp) {
                console.log('Success ' + resp.config.data.file.name + ' ploaded. Response: ' + resp.data);
                $scope.mData.Filename = resp.config.data.file.name;
                console.log('$scope.mData.Filename', $scope.mData.Filename);
            }, function(resp) {
                console.log('Error status: ' + resp.status);
            }, function(evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        };

        $scope.loadFileExceluploadAttach = function(ExcelFile){
            var ExcelData = [];
            var myEl = angular.element( document.querySelector( '#uploadAttach' ) ); //ambil elemen dari dokumen yang di-upload
            console.log('myEl', myEl);
            var allowedExtensions =/(\.xlsx)$/i; 
            var tmpName = myEl[0].files[0].name;
              
            if (!allowedExtensions.exec(tmpName)) { 
                bsNotify.show({
                    title: "Format Dokumen Tidak Diziinkan",
                    content: "Format hanya bisa excel .XLSX, mohon ubah format file",
                    type: 'danger'
                });
                $('#uploadAttach').val("")
                return false; 
            }  
        }

        $scope.HandleFileSelect = function(evt) {
            var x = $scope.uploadFiles[0].strBase64.split(',');
            $scope.mData.Attachment = x[1];
            $scope.mData.Filename = $scope.uploadFiles[0].FileName;
            console.log('uploadFiles[0]', $scope.uploadFiles[0]);

            if ($scope.mData.Attachment != '1') {
                bsNotify.show({
                    title: "RPP Claim",
                    content: "Dokumen berhasil dilampirkan",
                    type: 'success'
                });
            }

            // $scope.uploadFiles[0].Branch = '/Report/Attachment/RPP/';
            // $scope.uploadFiles[0].NameTable = 'APart_StockReturnRPP_TT';
            // $scope.uploadFiles[0].DocumentTypeId = 1;
            // $scope.uploadFiles[0].TableRefId = 1;
            // // $scope.uploadFiles[0].StrBase64 = $scope.mData.Attachment;
            // console.warn("upload file", $scope.uploadFiles);
            // console.warn("byte file", $scope.mData.Attachment);
            // RppClaim.UploadFile($scope.uploadFiles[0]).then(function(res) {
            //       var gridData = res.data;
            //       console.log('RppClaim.UploadFile', gridData);
            //       $scope.loading = false;
            //     },
            //     function(err) {
            //       console.log("err=>", err);
            //     }
            //   );
        }

        $scope.UploadFile = function() {

            for (var i = 0; i < $scope.fileList.length; i++) {

                $scope.UploadFileIndividual($scope.fileList[i].file,
                    $scope.fileList[i].file.name,
                    $scope.fileList[i].file.type,
                    $scope.fileList[i].file.size,
                    i);
            }

        }


        $scope.UploadFileIndividual = function(fileToUpload, name, type, size, index) {
            var reqObj = new XMLHttpRequest();
            reqObj.upload.addEventListener("progress", uploadProgress, false)
            reqObj.addEventListener("load", uploadComplete, false)
            reqObj.addEventListener("error", uploadFailed, false)
            reqObj.addEventListener("abort", uploadCanceled, false)
            reqObj.open("POST", "/FileUpload/UploadFiles", true);
            reqObj.setRequestHeader("Content-Type", "multipart/form-data");
            reqObj.setRequestHeader('X-File-Name', name);
            reqObj.setRequestHeader('X-File-Type', type);
            reqObj.setRequestHeader('X-File-Size', size);
            reqObj.send(fileToUpload);

            function uploadProgress(evt) {
                if (evt.lengthComputable) {
                    var uploadProgressCount = Math.round(evt.loaded * 100 / evt.total);
                    document.getElementById('P' + index).innerHTML = uploadProgressCount;
                    if (uploadProgressCount == 100) {
                        document.getElementById('P' + index).innerHTML =
                            '<i class="fa fa-refresh fa-spin" style="color:green;"></i>';
                    }
                }
            }

            function uploadComplete(evt) {
                document.getElementById('P' + index).innerHTML = '<span style="color:Green;font-weight:bold;font-style: oblique">Saved..</span>';
                $scope.NoOfFileSaved++;
                $scope.$apply();
            }

            function uploadFailed(evt) {
                document.getElementById('P' + index).innerHTML = '<span style="color:Red;font-weight:bold;font-style: oblique">Upload Failed..</span>';
            }

            function uploadCanceled(evt) {
                document.getElementById('P' + index).innerHTML = '<span style="color:Red;font-weight:bold;font-style: oblique">Canceled..</span>';
            }
        }


        $scope.onOverlayMode = function(kodeClaim) {
            console.log("[$scope] onOverlayMode");
            console.log(kodeClaim);
            $scope.mData.selectedRPP = kodeClaim;
            $scope.isOverlayForm = false;
            $scope.mData.SRPDealerRequestId = '1';
            var newnum = '1';

            $scope.crudState = newnum + $scope.formShowHandler(kodeClaim);
            console.log("[OVERLAY] is " + $scope.isOverlayForm);
            console.log("[OVERLAY] " + $scope.crudState);

        }

        $scope.onSearchAppointment = function() {
            console.log("[$scope] onOverlayMode");
            $scope.isOverlayForm = false;
            var newnum = '1';

            $scope.crudState = newnum + $scope.formShowHandler();
            console.log("[OVERLAY] is " + $scope.isOverlayForm);
            console.log("[OVERLAY] " + $scope.crudState);

        }

        $scope.onDeleteFilter = function(filter) {
            console.log('isi filter', filter);
            $scope.mFilter = {}
        }

        $scope.localeDate = function(data) {
            var tmpDate = new Date(data);
            var resDate = new Date(tmpDate.toISOString().replace("Z","-0700")).toISOString(); 
            return resDate;
        }

        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.gridDataTree = [];
        $scope.getData = function() {
            if (Object.keys($scope.mFilter).length == 0) {
                if (loadedContent) {
                    bsNotify.show({
                        title: "RPP Claim",
                        content: "Filter tidak boleh kosong",
                        type: 'warning'
                    });
                }
            } else if ((('StockReturnRPPStatusId' in $scope.mFilter)) && (!('startDate' in $scope.mFilter) || !('endDate' in $scope.mFilter))) {
                bsNotify.show({
                    title: "RPP Claim",
                    content: "Tanggal harus diisi",
                    type: 'warning'
                });
            } else {
                RppClaim.getData($scope.mFilter).then(function(res) {
                        var gridData = res.data.Result;
                        $scope.grid.data = gridData;
                        $scope.loading = false;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }

        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);

        }

        //------------------------------------------------------------
        $scope.formmode = { mode: '' };
        $scope.doGeneratePass = function() {
            $scope.mUser.password = Math.random().toString(36).slice(-8);
        }
        $scope.onBeforeNewMode = function() {
            //show gateway page
            //$scope.isOverlayForm = true;
            // added by sss on 2017-12-14
            $scope.grid_rpp.data = [];
            //

            $scope.onSearchAppointment();

            $scope.formode = 1;
            console.log("mode=>", $scope.formmode);
            $scope.newUserMode = true;
            $scope.editMode = false;
            $scope.mData.OutletId = $scope.user.OrgId;
            $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
            $scope.mData.ServiceTypeId = $scope.getRightServiceType();
            console.log("MaterialTypeId : ", $scope.mData.MaterialTypeId);
            console.log("ServiceTypeId : ", $scope.mData.ServiceTypeId);
            console.log("$scope.mData : ", $scope.mData);

            PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.user.OrgId).then(function(res) {
                    var loginData = res.data;
                    console.log(loginData[0]);
                    $scope.mData.WarehouseId = loginData[0].WarehouseId;

                    console.log("OutletId = ", $scope.mData.OutletId);
                    console.log("WarehouseId = ", $scope.mData.WarehouseId);
                },
                function(err) {
                    console.log("err=>", err);
                }
            );


            //$scope.mode='new';

            $scope.mData.StockReturnRPPNo = '000/RPG/1707-00004';
            $scope.mData.DocDate = '2017-07-05';
        }
        $scope.onBeforeEditMode = function(row) {
            if (row.StockReturnRPPStatusId != 1) {
                bsNotify.show({
                    title: "Parts Claim",
                    content: "Tidak bisa di-edit karena status bukan Draft",
                    type: 'warning'
                });
                $scope.formApi.setMode('grid');
                // return;
            } else {
                $scope.formode = 2;
                //$scope.newUserMode=false;
                //console.log("mode=>",$scope.formmode);
                //$scope.editMode=true;

                console.log("[$scope] onBeforeEditMode");
                var editnum = '3'

                //pemilihan form edit
                console.log("Data dari onBeforeEditMode => ");
                $scope.isEditState = true;
                console.log("[EDIT] ");

                $scope.crudState = editnum + $scope.formShowHandler();

                console.log("[EDIT] " + $scope.crudState);
            }
        }
        $scope.onBeforeDeleteMode = function() {
            $scope.formode = 3;
            console.log("mode=>", $scope.formmode);
            //$scope.mode='del';
        }
        $scope.onShowDetail = function(row, mode, xparam) {
            console.log('row dongs', row);
            // added by sss on 2017-12-14
            $scope.grid_rpp.data = [];
            //
            //$scope.formode = 3;
            //console.log("mode=>",$scope.formmode);
            // console.log("onShowDetail mode: " + mode);
            // console.log("Isi row => " + row.no_rpp_dms);

            // console.log("[$scope] onShowDetail");
            // var shownum = '2';

            // console.log("[SHOW]");

            // if($scope.formode == 2){
            //   console.log("--->[Active] onBeforeEditMode");
            //   $scope.formode = 0;
            // }
            // else{
            //   console.log("--->[Active] onShowDetail");
            //   console.log("Data dari onShowDetail => ");

            //   $scope.crudState = shownum + $scope.formShowHandler();

            //   console.log("[VIEW] " + $scope.crudState);
            //   $scope.isEditState = false;
            // }
            RppClaim.setStockReturnRPPHeader(row);

            if (mode == 'view') {
                RppClaim.SearchReference(row.StockReturnRPPId, 2).then(function(res) {
                        var AppointmentData = res.data[0];
                        console.log(AppointmentData);
                        if (typeof AppointmentData === 'undefined' || AppointmentData == null) {
                            bsNotify.show({
                                title: "RPP",
                                content: "No Referensi tidak ditemukan",
                                type: 'danger'
                            });
                        } else {
                            console.log("$scope.mData.GRDate=>",$scope.mData.GRDate);
                            console.log("$scope.localeDate(AppointmentData.GRDate)=>",$scope.localeDate(AppointmentData.GRDate));
                            //masukkan ke dalam model
                            $scope.mData.PurchaseOrderNo = AppointmentData.PurchaseOrderNo;
                            $scope.mData.PurchaseOrderId = AppointmentData.PurchaseOrderId;
                            $scope.mData.LicensePlate = AppointmentData.LicensePlate;
                            $scope.mData.AppointmentNo = AppointmentData.AppointmentNo;
                            $scope.mData.DocDate = $scope.localeDate(AppointmentData.DocDate);
                            $scope.mData.GRDate = $scope.localeDate(AppointmentData.GRDate);
                            $scope.mData.InvoiceId = AppointmentData.InvoiceId;
                            $scope.mData.InvoiceNo = AppointmentData.InvoiceNo;
                            $scope.mData.PartClaimTypeId = AppointmentData.PartClaimTypeId;
                            $scope.mData.SRPDealerRequestId = AppointmentData.SRPDealerRequestId;
                            $scope.mData.CreatedDate = $scope.localeDate(AppointmentData.CreatedDate);
                            GRDate2 = $scope.localeDate(AppointmentData.GRDate);
                            console.log('coeg', GRDate2);
                            console.log("$scope.mData.GRDate=>",$scope.mData.GRDate);

                            // search detail dari po id
                            // [START] Get Search Appointment Grid Detail
                            RppClaim.getDataDetail(row.StockReturnRPPId).then(function(res) {
                                    // $scope.grid_rpp.data = res.data.Result;
                                    $scope.mData.GridDetail = res.data.Result;
                                    console.log("$scope.mData.GridDetail=>",$scope.mData.GridDetail);
                                    for (var i in $scope.mData.GridDetail) {
                                        $scope.mData.GridDetail[i].CreatedDate = $scope.localeDate($scope.mData.GridDetail[i].CreatedDate);
                                    }

                                    $scope.grid_rpp_lihat.data = $scope.mData.GridDetail;
                                    console.log($scope.mData.GridDetail);
                                    $scope.loading = false;
                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
                            // [END] Get Search Appointment Grid
                        }
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
                var shownum = '2';
                $scope.crudState = shownum + $scope.formShowHandler();
            } else if (mode == 'edit') {
                if (row.StockReturnRPPStatusId != 1) {
                    $scope.formApi.setMode('grid');
                    // return;
                } else {
                    RppClaim.SearchReference(row.StockReturnRPPId, 2).then(function(res) {
                            var AppointmentData = res.data[0];
                            console.log(AppointmentData);
                            if (typeof AppointmentData === 'undefined' || AppointmentData == null) {
                                bsNotify.show({
                                    title: "RPP",
                                    content: "No Referensi tidak ditemukan",
                                    type: 'danger'
                                });
                            } else {
                                console.log("$scope.mData.GRDate=>",$scope.mData.GRDate);
                                console.log("$scope.localeDate(AppointmentData.GRDate)=>",$scope.localeDate(AppointmentData.GRDate));
    
                                //masukkan ke dalam model
                                $scope.mData.PurchaseOrderNo = AppointmentData.PurchaseOrderNo;
                                $scope.mData.PurchaseOrderId = AppointmentData.PurchaseOrderId;
                                $scope.mData.LicensePlate = AppointmentData.LicensePlate;
                                $scope.mData.AppointmentNo = AppointmentData.AppointmentNo;
                                $scope.mData.DocDate = $scope.localeDate(AppointmentData.DocDate);
                                $scope.mData.GRDate = $scope.localeDate(AppointmentData.GRDate);
                                $scope.mData.InvoiceId = AppointmentData.InvoiceId;
                                $scope.mData.InvoiceNo = AppointmentData.InvoiceNo;
                                $scope.mData.PartClaimTypeId = AppointmentData.PartClaimTypeId;
                                $scope.mData.SRPDealerRequestId = '1';
                                $scope.mData.CreatedDate = $scope.localeDate(AppointmentData.CreatedDate);
                                GRDate2 = $scope.localeDate(AppointmentData.GRDate);
                                console.log('coeg', GRDate2);
                                console.log("$scope.mData.GRDate=>",$scope.mData.GRDate);


                                // search detail dari po id
                                // [START] Get Search Appointment Grid Detail
                                RppClaim.getDataDetail(row.StockReturnRPPId).then(function(res) {
                                    $scope.mData.GridDetail = res.data.Result;
                                    console.log("$scope.mData.GridDetail=>",$scope.mData.GridDetail);
                                    for (var i in $scope.mData.GridDetail) {
                                        $scope.mData.GridDetail[i].CreatedDate = $scope.localeDate($scope.mData.GridDetail[i].CreatedDate);
                                    }

                                    $scope.grid_rpp.data = $scope.mData.GridDetail;

                                    console.log($scope.mData.GridDetail);
                                    $scope.loading = false;
                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                );
                                // [END] Get Search Appointment Grid
                            }
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
                    var shownum = '3';
                    $scope.crudState = shownum + $scope.formShowHandler();
                }
            } else if (mode == 'new') {
                if (typeof xparam !== 'undefined') {
                    if (typeof xparam.fromOtherModule !== 'undefined' || xparam.fromOtherModule) {
                        itIsLink = true;
                        paramNew = angular.copy(xparam);
                    }
                }
            }
        }
        //----------------------------------
        // ACTION BUTTON PADA FORM
        //----------------------------------
        $scope.SimpanRPP = function(mData) {
            console.log("--> SimpanRPP");
            console.log(mData);
            RppClaim.create(mData).then(function(res) {
                    //alert("Data berhasil disimpan");
                    bsNotify.show({
                        title: "RPP",
                        content: "Data berhasil disimpan",
                        type: 'success'
                    });
                    $scope.goBack();
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.UbahRPP = function(mData) {
            console.log("--> UbahRPP");
            console.log(mData);
            for (var key in mData.GridDetail) {
                mData.GridDetail[key].StockReturnRPPId = mData.StockReturnRPPId;
            }
            RppClaim.update(mData, mData.GridDetail).then(function(res) {
                    //alert("Data berhasil diupdate");
                    bsNotify.show({
                        title: "RPP",
                        content: "Data berhasil diubah",
                        type: 'success'
                    });
                    $scope.goBack();
                    $scope.getData();
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.SimpanData = function(mData) {
            // Start Create
            mData.StockReturnRPPStatusId = 1;
            console.log("Master Data => ", mData);
            if ((typeof mData.ShippingNo === 'undefined') || (typeof mData.PurchaseOrderNo === 'undefined') || (typeof mData.InvoiceId === 'undefined') || (typeof mData.LicensePlate === 'undefined')) {
                bsNotify.show({
                    title: "RPP Claim",
                    content: "Data gagal disimpan, periksa kembali data field mandatory",
                    type: 'danger'
                });
            } else if (mData.PartClaimTypeId == 0) {
                bsNotify.show({
                    title: "RPP Claim",
                    content: "Kode Claim harus diisi",
                    type: 'danger'
                });
            } else if (mData.Filename == "" || mData.Filename == null || mData.Filename == undefined ) {
                bsNotify.show({
                    title: "RPP Claim",
                    content: "Belum ada file attachment !!",
                    type: 'danger'
                });
            } else {
                $scope.mData.OutletId = $scope.user.OrgId;
                PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.user.OrgId).then(function(res) {
                        var loginData = res.data;
                        console.log(loginData[0]);
                        $scope.mData.WarehouseId = loginData[0].WarehouseId;

                        console.log("OutletId = ", $scope.mData.OutletId);
                        console.log("WarehouseId = ", $scope.mData.WarehouseId);

                        console.log("Persiapan Create => ");
                        console.log("Head ", $scope.mData);
                        console.log("Tails ", $scope.mData.GridDetail);

                        RppClaim.create($scope.mData, $scope.mData.GridDetail).then(function(res) {
                                var create = res.data.Result;
                                console.log(res.data.Result);
                                if (res.data.ResponseCode == 13) {
                                    bsNotify.show({
                                        title: "RPP",
                                        content: "Data berhasil disimpan",
                                        type: 'success'
                                    });
                                } else {
                                    bsNotify.show({
                                        title: "RPP",
                                        content: "Data gagal disimpan, silakan hubungi administrator",
                                        type: 'danger'
                                    });
                                }

                                $scope.goBack();
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );




                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }

            //console.log("Grid ", $scope.non_tam_sop_wo.data);
            // // [START] Get Search Appointment

        }

        $scope.onBulkApproveRPP = function(row){
            // console.log("Approve");
            row = row[0]
            console.log("row : ",row);
            console.log("stockReturnRPPId : ",row.StockReturnRPPId);
            console.log("GR",row.GRDate);
            if (row.StockReturnRPPStatusId == 2) { 
                $scope.ApproveData = row;
                $scope.ApproveData1 = row.StockReturnRPPId;
                RppClaim.getDataDetail(row.StockReturnRPPId).then(function(res3) {
                    console.log("coba sini".res3);
                    $scope.ApproveData.GridDetail=res3.data.Result;
                    $scope.ApproveData1 =res3.data.Result;
                    for (var i in $scope.ApproveData1) {
                        $scope.ApproveData1[i].CreatedDate = $scope.localeDate($scope.ApproveData1[i].CreatedDate);
                    }

                    PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res) {
                        var Approvers = res.data.Result[0].Approvers;
                        console.log("getApprover res = ", Approvers);
                        console.log("$scope.mData = ", $scope.mData);
                        console.log("$scope.ApprovalData = ", $scope.ApprovalData);
                        $scope.mData.Approvers = '';
                        // $scope.ApprovalData.Approvers = '';
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                    );

                    ngDialog.openConfirm ({
                        template:'\
                                    <div align="center" class="ngdialog-buttons">\
                                    <p><b>Konfirmasi</b></p>\
                                    <p>Approve dokumen RPP ini?</p>\
                                    <div class="ngdialog-buttons" align="center">\
                                    <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinApprove(ApproveData,ApproveData1)">Ya</button>\
                                    </div>\
                                    </div>',
                        plain: true,
                        // controller: 'PartsStockOpnameController',
                        scope:$scope
                    });

                    //row[0].StockReturnRPPStatusId = 2;
                    row.StockReturnRPPStatusId = 2;
                })    
            } else if (row.StockReturnRPPStatusId == 1) {
                bsNotify.show(
                    {
                        title: "RPP",
                        content: "Data masih draft, tidak bisa disetujui",
                        type: 'danger'
                    });
                return;
            } else if (row.StockReturnRPPStatusId == 3 && row.ApprovalStatus == 1) {
                bsNotify.show(
                    {
                        title: "RPP",
                        content: "Data telah disetujui",
                        type: 'danger'
                    });
                return;
            } else if (row.StockReturnRPPStatusId == 3 && row.ApprovalStatus == 2) {
                bsNotify.show(
                    {
                        title: "RPP",
                        content: "Data telah ditolak",
                        type: 'danger'
                    });
                return;
            } else if (row.StockReturnRPPStatusId == 4) {
                bsNotify.show(
                    {
                        title: "RPP",
                        content: "Data telah direspon TPOS",
                        type: 'danger'
                    });
                return;
            } else if (row.StockReturnRPPStatusId == 5) {
                bsNotify.show(
                    {
                        title: "RPP",
                        content: "Data telah dibatalkan",
                        type: 'danger'
                    });
                return;
            }
        };

        $scope.yakinApprove = function(ApproveData,ApproveData1){
            console.log('yakinApprove => ', ApproveData);
            console.log('yakinApprove => ', ApproveData1);
            console.log("Approve");

            RppClaim.sendTPOS(ApproveData,ApproveData1).then(function(res1){
                console.log("coba sini".res1);

                PartsCurrentUser.actApproval({
                    ProcessId: 8902, 
                    DataId: ApproveData.StockReturnRPPId, 
                    ApprovalStatus: 1
                }).then(function(res){
                    $scope.getData();
      
                    // bsNotify.show(
                    // {
                    //     title: "RPP",
                    //     content: "Data berhasil di-approve",
                    //     type: 'success'
                    // });

                    bsAlert.alert({
                            title: "Data berhasil di-approve",
                            text: "Untuk proses selanjutnya, lakukan approval pengajuan RPP oleh Kepala Bengkel pada Sistem TPOS",
                            type: "success",
                            showCancelButton: false
                        },
                        function() {
                        }
                    );
                });
      
            });

            var DataNotif = {};
            var messagetemp = "";
            // messagetemp = "Hasil Pengajuan Approval RPP Anda Disetujui ( RPP / " + ApproveData.StockReturnRPPNo + " )";
            messagetemp = "Untuk proses selanjutnya, lakukan approval pengajuan RPP ( "+ ApproveData.StockReturnRPPNo +" ) oleh Kepala Bengkel pada Sistem TPOS";

            DataNotif.Message = messagetemp;
                
            if ($scope.user.RoleId==1128) {
                // RppClaim.sendNotif(DataNotif, 1122, 46).then(
                RppClaim.sendNotif(DataNotif, 1128, 46).then(
                    function(res) {
    
                    },
                    function(err) {
                        //console.log("err=>", err);
                    });
            } else {
                // RppClaim.sendNotif(DataNotif, 1123, 46).then(
                RppClaim.sendNotif(DataNotif, 1129, 46).then(
                    function(res) {
    
                    },
                    function(err) {
                        //console.log("err=>", err);
                    });
            }
    
            $scope.ngDialog.close();
        };

        // $scope.onBulkApproveRPP = function(row) {
        //     console.log("Approved => ", row);
        //     PartsGlobal.actApproval({
        //         ProcessId: 8902,
        //         DataId: row[0].StockReturnRPPId,
        //         ApprovalStatus: 1
        //     });

        //     row[0].StockReturnRPPStatusId = 2;

        //     // RppClaim.update(row[0]).then(function(res) {
        //     //       var create = res.data;
        //     //       console.log(res.data);
        //     //       bsNotify.show({
        //     //             title: "RPP",
        //     //             content: "Data berhasil di-approve",
        //     //             type: 'success'
        //     //         });
        //     //     },function(err) {
        //     //       console.log("err=>", err);
        //     // });

        // }

        $scope.onBulkRejectRPP = function(row){
            console.log("row : ",row);
            if (row[0].StockReturnRPPStatusId == 2) {
                $scope.ApproveData = row;
                PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res) {
                    var Approvers = res.data.Result[0].Approvers;
                    console.log("getApprover res = ", Approvers);
                    $scope.mData.Approvers = Approvers;
                    // $scope.ApprovalData.Approvers = Approvers;
                    },
                    function(err) {
                    console.log("err=>", err);
                    }
                );
                ngDialog.openConfirm ({
                    template:'\
                                    <div align="center" class="ngdialog-buttons">\
                                    <p><b>Konfirmasi</b></p>\
                                    <p>Tolak dokumen RPP ini?</p>\
                                    <div class="ngdialog-buttons" align="center">\
                                    <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="RejectApprove(ApproveData)">Ya</button>\
                                    </div>\
                                    </div>',
                    plain: true,
                    // controller: 'PartsStockOpnameController',
                    scope:$scope
                    });
                
                //    var DataNotif = {};
                //    var messagetemp = "";
                //    messagetemp = "Hasil Pengajuan Approval RPP Anda Ditolak ( RPP / " + row[0].StockReturnRPPNo + " )";
        
                //    DataNotif.Message = messagetemp;
        
                //    if ($scope.user.RoleId==1128) {
                //        RppClaim.sendNotif(DataNotif, 1122, 46).then(
                //            function(res) {
            
                //            },
                //            function(err) {
                //                //console.log("err=>", err);
                //            });
                //    } else {
                //        RppClaim.sendNotif(DataNotif, 1123, 46).then(
                //            function(res) {
            
                //            },
                //            function(err) {
                //                //console.log("err=>", err);
                //            });
                //    }
            } else if (row[0].StockReturnRPPStatusId == 1) {
                bsNotify.show(
                    {
                        title: "RPP",
                        content: "Data masih draft, tidak bisa ditolak",
                        type: 'danger'
                    });
                return;
            } else if (row[0].StockReturnRPPStatusId == 3 && row[0].ApprovalStatus == 1) {
                bsNotify.show(
                    {
                        title: "RPP",
                        content: "Data telah disetujui",
                        type: 'danger'
                    });
                return;
            } else if (row[0].StockReturnRPPStatusId == 3 && row[0].ApprovalStatus == 2) {
                bsNotify.show(
                    {
                        title: "RPP",
                        content: "Data telah ditolak",
                        type: 'danger'
                    });
                return;
            } else if (row[0].StockReturnRPPStatusId == 4) {
                bsNotify.show(
                    {
                        title: "RPP",
                        content: "Data telah direspon TPOS",
                        type: 'danger'
                    });
                return;
            } else if (row[0].StockReturnRPPStatusId == 5) {
                bsNotify.show(
                    {
                        title: "RPP",
                        content: "Data telah dibatalkan",
                        type: 'danger'
                    });
                return;
            }
        };
    
        $scope.RejectApprove = function(ApproveData){
            console.log('yakinApprove => ', ApproveData);
            console.log("Approve");
            PartsCurrentUser.actApproval({
                ProcessId: 8902, DataId: ApproveData[0].StockReturnRPPId, ApprovalStatus: 2
                });                      
                bsNotify.show(
                {
                    title: "Input Hasil",
                    content: "Data berhasil di tolak",
                    type: 'success'
                });

                $scope.ngDialog.close();

            var DataNotif = {};
            var messagetemp = "";
            messagetemp = "Hasil Pengajuan Approval RPP Anda Ditolak ( RPP / " + ApproveData[0].StockReturnRPPNo + " )";

            DataNotif.Message = messagetemp;

            if ($scope.user.RoleId==1128) {
                RppClaim.sendNotif(DataNotif, 1122, 46).then(
                    function(res) {
    
                    },
                    function(err) {
                        //console.log("err=>", err);
                    });
            } else {
                RppClaim.sendNotif(DataNotif, 1123, 46).then(
                    function(res) {
    
                    },
                    function(err) {
                        //console.log("err=>", err);
                    });
            }
        };    

        $scope.ToSRCName = function(SRCId) {
            var statusName;
            switch (SRCId) {
                case 1:
                    statusName = "C - TAM Credit";
                    break;
                case 2:
                    statusName = "D - Dealer Credit";
                    break;
                case 3:
                    statusName = "N - Qty Plus, Can't Sold";
                    break;
                default:
                    statusName = " - "
            }
            return statusName;
        }

        $scope.ToNameClaim = function(kodeClaim) {
            var statusName;
            switch (kodeClaim) {
                case 1:
                    statusName = "A - Cancel Booking";
                    break;
                case 2:
                    statusName = "B - Salah Diagnosa";
                    break;
            }
            return statusName;
        }

        // Button Search Generik
        $scope.onSearchRefRPPNo = function(NomorWo) {

            if (NomorWo === "" || NomorWo == null) {
                bsNotify.show({
                    title: "RPP",
                    content: "No Referensi tidak boleh kosong",
                    type: 'danger'
                });
            }
            // Eksekusi Nomor Dokumen
            else {
                console.log("ada");
                // [START] Get Search Reference
                RppClaim.SearchReference(NomorWo, 1).then(function(res) {
                        var AppointmentData = res.data[0];
                        console.log('AppointmentData', AppointmentData);
                        console.log('res', res);
                        if (typeof AppointmentData === 'undefined' || AppointmentData == null) {
                            bsNotify.show({
                                title: "RPP",
                                content: "No Referensi tidak ditemukan",
                                type: 'danger'
                            });
                        } else {
                            $scope.GenerateDocNum();
                            //masukkan ke dalam model
                            $scope.mData.PurchaseOrderNo = AppointmentData.PurchaseOrderNo;
                            $scope.mData.PurchaseOrderId = AppointmentData.PurchaseOrderId;
                            $scope.mData.LicensePlate = AppointmentData.LicensePlate;
                            $scope.mData.InvoiceNo = AppointmentData.InvoiceNo;
                            $scope.mData.ShippingNo = AppointmentData.ShippingNo;
                            $scope.mData.InvoiceId = AppointmentData.InvoiceId;
                            $scope.mData.GRDate = $scope.localeDate(AppointmentData.GRDate);
                            $scope.mData.PartClaimTypeId = AppointmentData.PartClaimTypeId;
                            $scope.mData.GRTime = '00:00';
                            $scope.mData.SRPDealerRequestId = '1';
                            // $scope.mData.CreatedDate = $scope.localeDate(AppointmentData.CreatedDate);
                            console.log("$scope.mData.GRDate=>",$scope.mData.GRDate);

                            // search detail dari po id
                            // [START] Get Search Appointment Grid Detail
                            RppClaim.SearchReferenceDetail($scope.mData.PurchaseOrderId, 1).then(function(res) {
                                    var AppointmentData = res.data;
                                    console.log('searchAppointmentDetail ==>');

                                    $scope.grid_rpp.data = AppointmentData;
                                    //$scope.gridOptionsView.data = AppointmentData;
                                    $scope.mData.GridDetail = AppointmentData;

                                    console.log($scope.mData.GridDetail);

                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
                            // [END] Get Search Appointment Grid
                        }





                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
                // [END] Get Search Appointment


            }

        }

        $scope.GenerateDocNum = function() {

            PartsCurrentUser.getFormatId($scope.mData.ServiceTypeId, 'RP').then(function(res) {
                    var result = res.data;
                    console.log("FormatId = ", result[0].Results);

                    PartsCurrentUser.getDocumentNumber2(result[0].Results).then(function(res) {
                            var DocNo = res.data;

                            if (typeof DocNo === 'undefined' || DocNo == null) {
                                bsNotify.show({
                                    title: "RPP",
                                    content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                                    type: 'danger'
                                });
                            } else {
                                $scope.mData.StockReturnRPPNo = DocNo[0];
                                // $scope.mData.DocDate = DocNo[1]
                                $scope.mData.DocDate = setDate(DocNo[1]);
                                console.log("Generating DocNo & DocDate ->");
                                console.log($scope.mData.StockReturnRPPNo);
                                console.log($scope.mData.DocDate);
                            }


                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );


                },
                function(err) {

                }
            );
            // [END] Get Current User Warehouse
        }

        function setDate(dt) {
            var d = "";
            dt = new Date(dt);
            d = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
            return d;
        }


        // fungsi semi-generik untuk handle halaman tampil
        $scope.formShowHandler = function() {
            $scope.formState = '';
            $scope.formState = '00';

            console.log($scope.formState);
            return $scope.formState;
        }

        // ======== tombol  simpan =========
        $scope.simpan = function(mData) {

            console.log("mData.PartClaimTypeId==>>",mData.PartClaimTypeId);
            console.log("typeof mData.PartClaimTypeId==>>",typeof mData.PartClaimTypeId);
            console.log("mData.Attachment==>>",mData.Attachment);
            console.log("mData.Filename==>>",mData.Filename);
            var ValidRPP = false;
            if ((typeof mData.ShippingNo === 'undefined') || (typeof mData.PurchaseOrderNo === 'undefined') || (typeof mData.InvoiceId === 'undefined') || (typeof mData.LicensePlate === 'undefined')) {
                bsNotify.show({
                    title: "RPP Claim",
                    content: "Data gagal disimpan, periksa kembali data field mandatory",
                    type: 'danger'
                });
            } else if (mData.PartClaimTypeId == 0) {
                bsNotify.show({
                    title: "RPP Claim",
                    content: "Kode Claim harus diisi",
                    type: 'danger'
                });
            } else if (mData.Filename == "" || mData.Filename == null || mData.Filename == undefined ) {
                bsNotify.show({
                    title: "RPP Claim",
                    content: "Belum ada file attachment !!",
                    type: 'danger'
                });
            } else if (mData.GridDetail.length <= 0) {
                bsNotify.show({
                    title: "RPP Claim",
                    content: "Detail tidak boleh kosong",
                    type: 'danger'    
                });
            } else {

                var dataGridParts = angular.copy(mData.GridDetail);
                var countValidRPP = 0;
                for (var i in mData.GridDetail) {
                    if (mData.GridDetail[i].QtyRPP > mData.GridDetail[i].QtyReceived || mData.GridDetail[i].QtyRPP == null || mData.GridDetail[i].QtyRPP == 0 ) {
                        ValidRPP = false;
                        break;
                    } else {
                        ValidRPP = true;
                        countValidRPP++;
                    }
                    console.log("masuk pak eko");
                };
            
                if (ValidRPP === false && (countValidRPP == 0 || countValidRPP < dataGridParts.length)) {
                    bsNotify.show({
                        title: "RPP Claim",
                        content: "Quantity RPP tidak valid",
                        type: 'danger'
                    });
                    return 0;
                }

                console.log('mData', mData);

                RppClaim.ApprovalData = mData;
                PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res){
                    console.log("Updated : 14-02-2018 11:29");
                    $scope.Approver = res.data.Result[0].Approvers;
                    RppClaim.ApprovalData['Approver'] = $scope.Approver;
                    console.log("Approver : ", $scope.Approver);
                  });

                ngDialog.openConfirm({
                    template: '<div ng-include=\"\'app/parts/claim/rpp/crud/permohonanapproval.html\'\"></div>',
                    //templateUrl: '/app/parts/pomaintenance/alasan.html',
                    plain: true,
                    controller: 'RppClaimController',
                });
            }
        };
        // ======== end tombol  simpan =========

        // ======== tombol batal - Claim- alasan ========= ini ada 2 batalPC nya
        // $scope.batalPC = function(mData) {
        //     // console.log('mdata nih', mData);
        //     ngDialog.openConfirm({
        //         template: '<div ng-include=\"\'app/parts/claim/rpp/template/template_batal.html\'\"></div>',
        //         plain: true,
        //         controller: 'RppClaimController',
        //     });
        // };

        $scope.ubah = function(mData) {

            if ((typeof mData.ShippingNo === 'undefined') || (typeof mData.PurchaseOrderNo === 'undefined') || (typeof mData.InvoiceId === 'undefined') || (typeof mData.LicensePlate === 'undefined') || (typeof mData.PartClaimTypeId === 'undefined')) {
                bsNotify.show({
                    title: "RPP Claim",
                    content: "Data gagal disimpan, periksa kembali data field mandatory",
                    type: 'danger'
                });
            } else {

                console.log(mData);
                $scope.ApprovalData= {};
                RppClaim.ApprovalData = mData;
                PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res){
                    console.log("Updated : 14-02-2018 11:29");
                    $scope.Approver = res.data.Result[0].Approvers;
                    RppClaim.ApprovalData['Approver'] = $scope.Approver;
                    console.log("Approver : ", $scope.Approver);
                  });
                $scope.ApprovalData = angular.copy(mData);
                ngDialog.openConfirm({
                    // template: '<div ng-include=\"\'app/parts/claim/rpp/crud/permohonanapprovalubah.html\'\"></div>',
                    template:'app/parts/claim/rpp/crud/permohonanapprovalubah.html',
                    //templateUrl: '/app/parts/pomaintenance/alasan.html',
                    // plain: true,
                    // controller: 'RppClaimController',
                    scope : $scope
                }).then(function(item) {
                    // console.log("item",item,$scope.ApprovalData);
                    $scope.UbahKirimApproval($scope.ApprovalData);
                });
            }
        };
        // ======== end tombol  simpan =========

        // ======== tombol batal - Claim- alasan =========
        $scope.batalPC = function(mData) {
            console.log('mdata nih', mData);
            GRDate2 = angular.copy(mData.GRDate);
            console.log('grdate2 nih', GRDate2);
            ngDialog.openConfirm({
                template: '<div ng-include=\"\'app/parts/claim/rpp/template/template_batal.html\'\"></div>',
                plain: true,
                // controller: 'RppClaimController',
                scope: $scope
            });
        };

        // dialog konfirmasi
        $scope.okBatalPC = function(data) {
            console.log('grdate2 nih 2', GRDate2);
            console.log(data);
            $scope.BatalData = RppClaim.getStockReturnRPPHeader();


            // $scope.BatalData.Notes = 'Alasan: ' + data.Batal + ', Catatan: ' + data.Catatan;
            $scope.BatalData.CancelReasonId = data.Batal
            $scope.BatalData.CancelReasonDesc = data.Catatan


            console.log("Batal data okBatalPC ", $scope.BatalData);

            RppClaim.setStockReturnRPPHeader($scope.BatalData);

            ngDialog.openConfirm({
                template: '\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Anda yakin akan membatalkan RPP Claim ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="SetujuBatal(BatalData)">Ya</button>\
                     </div>\
                     </div>',
                plain: true,
                // controller: 'RppClaimController',
                scope: $scope
            });
        };

        $scope.SetujuBatal = function(data) {
            console.log('SetujuBatal',GRDate2);
            $scope.mData = data;
            $scope.ngDialog.close();
            $scope.mData.StockReturnRPPStatusId = 5;
            $scope.mData.GRDate = GRDate2;

            console.log('curiga',$scope.mData);
            // [START] Get Search Appointment
            RppClaim.cancel2($scope.mData).then(function(res) {
                    console.log(res);
                    bsNotify.show({
                        title: "RPP",
                        content: "Data berhasil dibatalkan",
                        type: 'success'
                    });
                    $scope.goBack();
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            // [END] Get Search Appointment
        }

        // ======== end tombol batal - Claim =========

        $scope.KirimApproval = function(mData) {
            console.log("KirimApproval-mData", mData);
            mData.StockReturnRPPStatusId = 2;
            $scope.ngDialog.close();
            RppClaim.create(mData, mData.GridDetail).then(function(res) {
                    var insertResponse = res.data;
                    console.log(res.data);
                    if (res.data == 666 ) {
                      bsNotify.show({
                          title: "RPP",
                          content: "Data tidak dapat diprosess karena tidak ditemukan di TPOS",
                          type: 'warning'
                      });
                    }else {
                      if (insertResponse.ResponseCode == 13) {
                          bsNotify.show({
                              title: "RPP",
                              content: "Data berhasil disimpan",
                              type: 'success'
                          });
                        } else {
                          bsNotify.show({
                              title: "RPP",
                              content: "Data gagal disimpan, silakan hubungi administrator",
                              type: 'danger'
                          });
                        }
                    }


                    $scope.goBack();
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            var DataNotif = {};
            var messagetemp = "";
            messagetemp = "Request Approval untuk Pengajuan RPP ( RPP / " + mData.StockReturnRPPNo + " )";

            DataNotif.Message = messagetemp;

            if ($scope.user.RoleId==1122) {
                RppClaim.sendNotif(DataNotif, 1128, 46).then(
                    function(res) {
    
                    },
                    function(err) {
                        //console.log("err=>", err);
                    });
            } else {
                RppClaim.sendNotif(DataNotif, 1129, 46).then(
                    function(res) {
    
                    },
                    function(err) {
                        //console.log("err=>", err);
                    });
            }
            // RppClaim.sendNotif(DataNotif, 1128, 46).then(
            //     function(res) {

            //     },
            //     function(err) {
            //         //console.log("err=>", err);
            //     });

        }

        $scope.UbahKirimApproval = function(mData) {
            console.log("KirimApproval ", mData);
            mData.StockReturnRPPStatusId = 2;
            $scope.ngDialog.close();
            for (var key in mData.GridDetail) {
                mData.GridDetail[key].StockReturnRPPId = mData.StockReturnRPPId;
            }
            RppClaim.update(mData, mData.GridDetail).then(function(res) {
                    var insertResponse = res.data;
                    console.log(res.data);

                    if (insertResponse.ResponseCode == 23) {
                        bsNotify.show({
                            title: "RPP",
                            content: "Data berhasil disimpan",
                            type: 'success'
                        });
                    } else {
                        bsNotify.show({
                            title: "RPP",
                            content: "Data gagal disimpan, silakan hubungi administrator",
                            type: 'danger'
                        });
                    }

                    $scope.goBack();
                    $scope.getData();
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.ToNameStatus = function(StockReturnRPPStatusId) {
            var statusName;
            switch (StockReturnRPPStatusId) {
            case 1:
                statusName = "Draft";
                break;
            case 2:
                statusName = "Request Approval";
                break;
            case 3:
                statusName = "Outstanding Claim"; //"Open";
                break;
            case 4:
                statusName = "Answered Claim"; //Partial";
                break;
            case 5:
                statusName = "Cancelled"; //Completed";
                break;
            }
            return statusName;
        }

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'StockReturnRPPId',
                    field: 'StockReturnRPPId',
                    width: '7%',
                    visible: false
                },
                {
                    displayName: 'No RPP DMS',
                    name: 'No RPP DMS',
                    field: 'StockReturnRPPNo',
                    width: '12%'
                },
                {
                    displayName: 'No RPP TAM',
                    name: 'No RPP TAM',
                    field: 'StockReturnRPPNoTAM',
                    width: '12%'
                },
                {
                    displayName: 'Tanggal RPP',
                    name: 'Tanggal RPP',
                    field: 'DocDate',
                    cellFilter: 'date:\'yyyy-MM-dd\'',
                    width: '8%'
                },
                {
                    name: 'No. Appointment',
                    field: 'AppointmentNo',
                    width: '12%'
                },
                {
                    name: 'No. Polisi',
                    field: 'LicensePlate',
                    width: '8%'
                },
                {
                    displayName: 'No. PO',
                    name: 'No. PO',
                    field: 'PurchaseOrderNo',
                    width: '8%'
                },
                {
                    name: 'No. Vendor Inv.',
                    field: 'InvoiceNo',
                    width: '10%'
                },
                {
                    name: 'No. Shipping',
                    field: 'ShippingNo',
                    width: '8%'
                },
                {
                    name: 'Status',
                    field: 'StockReturnRPPStatusId',
                    width: '10%',
                    visible: false
                },
                {
                    displayName: 'Status',
                    name: 'Status',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatus(row.entity.StockReturnRPPStatusId)}}</div>',
                    // width: '12%'
                }
            ]
        };

        var rowSelDel = null;
        $scope.showMe = function(row) {
            // confirm('Apakah Anda yakin menghapus data ini?');
            console.log("row selected : ", row);
            rowSelDel = null;
            rowSelDel = angular.copy(row);
            ngDialog.openConfirm({
                template: '\
                  <p>Are you sure you want to delete this material?</p>\
                  <div class="ngdialog-buttons">\
                      <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
                      <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="DeleteMaterial();closeThisDialog(0)">Yes</button>\
                  </div>',
                plain: true,
                // controller: 'RppClaimController',
                scope: $scope
            });
        };

        $scope.DeleteMaterial = function() {
            console.log("delete");
            if (rowSelDel != null) {
                var temp = _.find($scope.grid_rpp.data, { "PartId": rowSelDel.PartId, "PartsCode": rowSelDel.PartsCode });
                if (typeof temp !== 'undefined') {
                    var idx = $scope.grid_rpp.data.indexOf(temp);
                    $scope.grid_rpp.data.splice(idx, 1);
                    $scope.mData.GridDetail.splice(idx, 1);
                }
            }
            console.log("$scope.mData : ", $scope.mData);
        }

    // Cetakan
    $scope.cetakRPP = function(GoodsIssueId) {
        console.log('GoodsIssueId =', GoodsIssueId);
        //console.log('OutletId =', $scope.user.OrgId);
        var data = $scope.mData;
        $scope.printRPP = 'as/SuratPengantarPengembalianBarang/' + GoodsIssueId;
        $scope.cetakan($scope.printRPP);
    };

    $scope.cetakan = function(data) {
        var pdfFile = null;

        PrintRpt.print(data).success(function(res) {
            var file = new Blob([res], { type: 'application/pdf' });
            var fileURL = URL.createObjectURL(file);

            console.log("pdf", fileURL);
            //$scope.content = $sce.trustAsResourceUrl(fileURL);
            pdfFile = fileURL;

            if (pdfFile != null) {
                //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame 
                var ua = navigator.userAgent;
                if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                    var link = document.createElement('a');
                    link.href = fileURL;
                    //link.download="erlangga_file.pdf";
                    link.click();
                }
                else {
                    printJS(pdfFile);
                }
            }
            else
                console.log("error cetakan", pdfFile);
        }).error(function(res) {
            console.log("error cetakan", pdfFile);
        });
    };

        //----------------------------------
        // Dummy grid_parts_claim
        //----------------------------------
        $scope.grid_rpp = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,

            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {
  
                  console.log('rowEntity', rowEntity);
                  console.log('colDef', colDef);
                  console.log('newValue', newValue);
                  console.log('oldValue', oldValue);
                  console.log('Position', row);
                  console.log('QtyGR', rowEntity.QtyReceived, rowEntity.QtyRPP);
  
                  if ( rowEntity.QtyReceived < rowEntity.QtyRPP ) {
                    bsNotify.show(
                      {
                          title: "RPP",
                          content: "Qty RPP tidak boleh lebih dari Qty Received ",
                          type: 'danger'
                      }
                    );
                    rowEntity.QtyRPP = oldValue;
                    return;
                  }
                });
            },
  
            columnDefs: [
                    { name: 'No Material', field: 'PartsCode' },
                    { name: 'Nama Material', field: 'PartsName' },
                    { name: 'Qty Received', field: 'QtyReceived' },
                    { displayName: 'Qty RPP', name: 'Qty RPP', field: 'QtyRPP' },
                    { name: 'Satuan', field: 'UomName' },
                    {
                        name: 'Action',
                        cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.showMe(row.entity)"><u>Hapus</u></a></center>',
                        visible: true
                    }
                ]
                // ,
                // data : [      {
                //                    "no_material": "35677-33996",
                //                    "nama_material": "EXPANDER",
                //                    "qty_received": "3",
                //                    "qty_rpp": "3",
                //                    "satuan": "Pieces",
                //                },
                //                {
                //                    "no_material": "35677-64438",
                //                    "nama_material": "SHOCK BREAKER",
                //                    "qty_received": "3",
                //                    "qty_rpp": "3",
                //                    "satuan": "Pieces",
                //                }
                //            ]
        };

        //----------------------------------
        // Dummy grid_parts_claim
        //----------------------------------
        $scope.grid_rpp_lihat = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            selectedItems: console.log($scope.mySelections),
            enableSelectAll: true,
            columnDefs: [
                { name: 'No Material', field: 'PartsCode' },
                { name: 'Nama Material', field: 'PartsName' },
                { name: 'Qty Received', field: 'QtyReceived' },
                { displayName: 'Qty RPP', name: 'Qty RPP', field: 'QtyRPP' },
                { name: 'Satuan', field: 'UomName' },
                { displayName: 'TAM Judgement', name: 'TAM Judgement', field: 'SRCJudgementCode' },
                // { displayName: 'TAM Judgement', name: 'TAM Judgement', field: 'SRPJudgementId' },
                // {
                //     name: 'TAM Judgement',
                //     field: 'SRPJudgementId',
                //     visible: true,
                //     width: 100,
                //     displayName: 'TAM Judgement',
                //     enableCellEdit: true,
                //     editableCellTemplate: 'ui-grid/dropdownEditor',
                //     cellFilter: 'mapAdj',
                //     editDropdownValueLabel: 'adj',
                //     editDropdownOptionsArray: [{ id: 1, adj: 'C - TAM Credit' }, { id: 2, adj: 'D - Dealer Credit' }, { id: 3, adj: 'N - Qty Plus, Can\'t Sold' }]
                // },
                { displayName: 'Qty TAM Judgement', name: 'Qty TAM Judgement', field: 'SRPJudgementQty' },

            ]
        };

    })
    .filter('mapAdj', function() {
        var adjHash = {
            1: 'C - TAM Credit',
            2: 'D - Dealer Credit',
            3: 'N - Qty Plus, Can\'t Sold'
        };

        return function(input) {
            if (!input) {
                return '';
            } else {
                return adjHash[input];
            }
        };
    });
