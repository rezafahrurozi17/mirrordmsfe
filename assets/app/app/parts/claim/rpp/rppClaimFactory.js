angular.module('app')
    .factory('RppClaim', function($http, CurrentUser, $filter) {
        var currentUser = CurrentUser.user;
        var StockReturnRPPHeader = {};
        this.ApprovalData = {};
        this.formApi = {};
        // console.log(currentUser);
        return {
            getData: function(data) {
                console.log("[Factory]");
                console.log(data);
                var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
                var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');
                var res = $http.get('/api/as/StockReturnRPP', {
                    params: {
                        StockReturnRPPStatusId: (data.StockReturnRPPStatusId == null ? "n/a" : data.StockReturnRPPStatusId),
                        StockReturnRPPNo: (data.StockReturnRPPNo == null ? "n/a" : data.StockReturnRPPNo),
                        ShippingNo: (data.ShippingNo == null ? "n/a" : data.ShippingNo),
                        PurchaseOrderId: (data.PurchaseOrderId == null ? "n/a" : data.PurchaseOrderId),
                        InvoiceId: (data.InvoiceId == null ? "n/a" : data.InvoiceId),
                        AppointmentNo: (data.AppointmentNo == null ? "n/a" : data.AppointmentNo),
                        StartDate: (startDate == null ? "n/a" : startDate),
                        EndDate: (endDate == null ? "n/a" : endDate)
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },
            // added by sss on 2018-01-05 
            getDataDetail: function(stockReturnRPPId) {
                var res = $http.get('/api/as/StockReturnRPP/StockReturnRPPDetail?StockReturnRPPId=' + stockReturnRPPId);
                console.log('hasil=>', res);
                return res;
            },
            //  
            create: function(data, detail) {
                console.log(' tambah data=> ', data);
                console.log(' tambah detail=> ', detail)
                var vdocDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                return $http.post('/api/as/StockReturnRPP', [{
                    //StockReturnClaimId: (data.Id==null?0:data.Id),
                    StockReturnRPPNo: (data.StockReturnRPPNo == null ? 0 : data.StockReturnRPPNo),
                    StockReturnRPPNoTAM: (data.StockReturnRPPNoTAM == null ? 0 : data.StockReturnRPPNoTAM),
                    DocDate: vdocDate, //(data.DocDate==null?0:data.DocDate),
                    AppointmentNo: (data.AppointmentNo == null ? 0 : data.AppointmentNo),
                    ShippingNo: (data.ShippingNo == null ? 0 : data.ShippingNo),
                    PurchaseOrderId: (data.PurchaseOrderId == null ? 0 : data.PurchaseOrderId),
                    InvoiceId: (data.InvoiceId == null ? 0 : data.InvoiceId),
                    InvoiceNo: (data.InvoiceNo == null ? 0 : data.InvoiceNo),
                    GRDate: (data.GRDate == null ? 0 : data.GRDate),
                    SRPDealerRequestId: (data.SRPDealerRequestId == null ? 0 : data.SRPDealerRequestId),
                    PartClaimTypeId: (data.PartClaimTypeId == null ? 0 : data.PartClaimTypeId),
                    Partsman: (data.Partsman == null ? 0 : data.Partsman),
                    Notes: (data.Notes == null ? 0 : data.Notes),
                    OutletId: (data.OutletId == null ? 0 : data.OutletId),
                    WarehouseId: (data.WarehouseId == null ? 0 : data.WarehouseId),
                    JobId: (data.JobId == null ? 0 : data.JobId),
                    //Attachment: (data.Attachment==null?0:data.Attachment),
                    //Filename: (data.Filename==null?'report.xlsx':data.Filename),
                    Filename: (data.Filename == null ? "-" : data.Filename),
                    StockReturnRPPStatusId: (data.StockReturnRPPStatusId == null ? 1 : data.StockReturnRPPStatusId),
                    GridDetail: detail


                }]);
            },
            sendTPOS: function(data, detail) {
                console.log(' tambah data=> ', data);
                console.log(' tambah detail=> ', detail)
                var vdocDate = $filter('date')(new Date(), 'yyyy-MM-dd');
                return $http.post('/api/as/PostPOSUsingRPP', [{
                    //StockReturnClaimId: (data.Id==null?0:data.Id),
                    StockReturnRPPNo: (data.StockReturnRPPNo == null ? 0 : data.StockReturnRPPNo),
                    StockReturnRPPNoTAM: (data.StockReturnRPPNoTAM == null ? 0 : data.StockReturnRPPNoTAM),
                    DocDate: vdocDate, //(data.DocDate==null?0:data.DocDate),
                    AppointmentNo: (data.AppointmentNo == null ? 0 : data.AppointmentNo),
                    ShippingNo: (data.ShippingNo == null ? 0 : data.ShippingNo),
                    PurchaseOrderId: (data.PurchaseOrderId == null ? 0 : data.PurchaseOrderId),
                    InvoiceId: (data.InvoiceId == null ? 0 : data.InvoiceId),
                    InvoiceNo: (data.InvoiceNo == null ? 0 : data.InvoiceNo),
                    GRDate: (data.GRDate == null ? 0 : data.GRDate),
                    SRPDealerRequestId: (data.SRPDealerRequestId == null ? 0 : data.SRPDealerRequestId),
                    PartClaimTypeId: (data.PartClaimTypeId == null ? 0 : data.PartClaimTypeId),
                    Partsman: (data.Partsman == null ? 0 : data.Partsman),
                    Notes: (data.Notes == null ? 0 : data.Notes),
                    OutletId: (data.OutletId == null ? 0 : data.OutletId),
                    WarehouseId: (data.WarehouseId == null ? 0 : data.WarehouseId),
                    JobId: (data.JobId == null ? 0 : data.JobId),
                    //Attachment: (data.Attachment==null?0:data.Attachment),
                    //Filename: (data.Filename==null?'report.xlsx':data.Filename),
                    Filename: (data.Filename == null ? "-" : data.Filename),
                    StockReturnRPPStatusId: (data.StockReturnRPPStatusId == null ? 1 : data.StockReturnRPPStatusId),
                    GridDetail: detail


                }]);
            },
            update: function(data, detail) {
                console.log('rubah data=>', data);
                return $http.put('/api/as/StockReturnRPP', [{
                    StockReturnRPPId: (data.StockReturnRPPId == null ? 0 : data.StockReturnRPPId),
                    StockReturnRPPNo: (data.StockReturnRPPNo == null ? 0 : data.StockReturnRPPNo),
                    StockReturnRPPNoTAM: (data.StockReturnRPPNoTAM == null ? 0 : data.StockReturnRPPNoTAM),
                    DocDate: (data.DocDate == null ? 0 : data.DocDate),
                    AppointmentNo: (data.AppointmentNo == null ? 0 : data.AppointmentNo),
                    ShippingNo: (data.ShippingNo == null ? 0 : data.ShippingNo),
                    PurchaseOrderId: (data.PurchaseOrderId == null ? 0 : data.PurchaseOrderId),
                    InvoiceId: (data.InvoiceId == null ? 0 : data.InvoiceId),
                    InvoiceNo: (data.InvoiceNo == null ? 0 : data.InvoiceNo),
                    GRDate: (data.GRDate == null ? 0 : data.GRDate),
                    SRPDealerRequestId: (data.SRPDealerRequestId == null ? 0 : data.SRPDealerRequestId),
                    PartClaimTypeId: (data.PartClaimTypeId == null ? 0 : data.PartClaimTypeId),
                    Partsman: (data.Partsman == null ? 0 : data.Partsman),
                    Notes: (data.Notes == null ? 0 : data.Notes),
                    OutletId: (data.OutletId == null ? 0 : data.OutletId),
                    WarehouseId: (data.WarehouseId == null ? 0 : data.WarehouseId),
                    JobId: (data.JobId == null ? 0 : data.JobId),
                    Attachment: (data.Attachment == null ? 0 : data.Attachment),
                    Filename: (data.Filename == null ? 'report.xlsx' : data.Filename),
                    StockReturnRPPStatusId: (data.StockReturnRPPStatusId == null ? 1 : data.StockReturnRPPStatusId),
                    CreatedDate: data.CreatedDate,
                    CreatedUserId: (data.CreatedUserId == null ? 0 : data.CreatedUserId),
                    GridDetail: detail
                }]);
            },
            delete: function(id) {
                console.log("delete id==>", id);
                return $http.delete('/api/as/TestVehicle', { data: id, headers: { 'Content-Type': 'application/json' } });
            },

            cancel: function(data) {
                console.log('batal data=>', data);
                return $http.put('/api/as/StockReturnRPP', [{
                    StockReturnRPPId: (data.StockReturnRPPId == null ? 0 : data.StockReturnRPPId),
                    StockReturnRPPNo: (data.StockReturnRPPNo == null ? 0 : data.StockReturnRPPNo),
                    StockReturnRPPNoTAM: (data.StockReturnRPPNoTAM == null ? 0 : data.StockReturnRPPNoTAM),
                    DocDate: (data.DocDate == null ? 0 : data.DocDate),
                    AppointmentNo: (data.AppointmentNo == null ? 0 : data.AppointmentNo),
                    ShippingNo: (data.ShippingNo == null ? 0 : data.ShippingNo),
                    PurchaseOrderId: (data.PurchaseOrderId == null ? 0 : data.PurchaseOrderId),
                    InvoiceId: (data.InvoiceId == null ? 0 : data.InvoiceId),
                    InvoiceNo: (data.InvoiceNo == null ? 0 : data.InvoiceNo),
                    GRDate: (data.GRDate == null ? 0 : data.GRDate),
                    SRPDealerRequestId: (data.SRPDealerRequestId == null ? 0 : data.SRPDealerRequestId),
                    PartClaimTypeId: (data.PartClaimTypeId == null ? 0 : data.PartClaimTypeId),
                    Partsman: (data.Partsman == null ? 0 : data.Partsman),
                    Notes: (data.Notes == null ? 0 : data.Notes),
                    //Attachment: (data.Attachment==null?0:data.Attachment),
                    StockReturnRPPStatusId: (data.StockReturnRPPStatusId == null ? 5 : 5)
                }]);
            },

            cancel2: function(data) {
                console.log('batal data=>', data);
                return $http.put('/api/as/PutCancelReturnRPP', [{
                    StockReturnRPPId: (data.StockReturnRPPId == null ? 0 : data.StockReturnRPPId),
                    StockReturnRPPNo: (data.StockReturnRPPNo == null ? 0 : data.StockReturnRPPNo),
                    StockReturnRPPNoTAM: (data.StockReturnRPPNoTAM == null ? 0 : data.StockReturnRPPNoTAM),
                    DocDate: (data.DocDate == null ? 0 : data.DocDate),
                    AppointmentNo: (data.AppointmentNo == null ? 0 : data.AppointmentNo),
                    ShippingNo: (data.ShippingNo == null ? 0 : data.ShippingNo),
                    PurchaseOrderId: (data.PurchaseOrderId == null ? 0 : data.PurchaseOrderId),
                    InvoiceId: (data.InvoiceId == null ? 0 : data.InvoiceId),
                    InvoiceNo: (data.InvoiceNo == null ? 0 : data.InvoiceNo),
                    GRDate: (data.GRDate == null ? 0 : data.GRDate),
                    SRPDealerRequestId: (data.SRPDealerRequestId == null ? 0 : data.SRPDealerRequestId),
                    PartClaimTypeId: (data.PartClaimTypeId == null ? 0 : data.PartClaimTypeId),
                    Partsman: (data.Partsman == null ? 0 : data.Partsman),
                    Notes: (data.Notes == null ? 0 : data.Notes),
                    //Attachment: (data.Attachment==null?0:data.Attachment),
                    StockReturnRPPStatusId: (data.StockReturnRPPStatusId == null ? 5 : 5),
                    CancelReasonId: data.CancelReasonId,
                    CancelReasonDesc: data.CancelReasonDesc,                    
                }]);
            },

            getDocumentNumber2: function() {
                console.log("[getDocumentNumber]");
                var res = $http.get('/api/as/PartsUser/GetDocumentNumber', {
                    params: {
                        FormatId: '92',
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },

            UploadFile: function(data) {
                console.log("[UploadFile]");
                var res = $http.post('/api/sales/FTP', data);
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },

            SearchReference: function(no, state) {
                console.log("[SearchReference]");
                var res = $http.get('/api/as/StockReturnRPP/SearchReference', {
                    params: {
                        AppointmentNo: no,
                        State: state
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },

            SearchReferenceDetail: function(no, state) {
                console.log("[searchReferenceDetail]");
                var res = $http.get('/api/as/StockReturnRPP/SearchReferenceDetail', {
                    params: {
                        AppointmentNo: no,
                        State: state
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },

            setStockReturnRPPHeader: function(data) {
                StockReturnRPPHeader = data;
            },
            getStockReturnRPPHeader: function() {
                return StockReturnRPPHeader;
            },
            sendNotif: function(data, recepient, param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param: param
                }]);
            },
        }
    });