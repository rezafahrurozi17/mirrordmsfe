angular.module('app')
    .controller('PrePickingController', function($scope, $http, CurrentUser, PrePicking, ngDialog, $timeout, $location, $filter, PartsCurrentUser, bsNotify, PrintRpt) {

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mData = {}; //Model
        $scope.mFilter = {};
        $scope.xRole = {
            selected: []
        };
        $scope.selectedRows = {};
        $scope.button = true;
        // $scope.myRights = {};
        $scope.tampungData = [];

        $scope.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
        };

        $scope.afterRegisterGridApi = function(gridApi) {
            console.log(gridApi);
        }

        $scope.Selesai = function() {
            // console.log("Header ", $scope.gridApiHeader.selection.getSelectedRows());
            // console.log("Detail ", $scope.gridApiDetail.selection.getSelectedRows());

            var dataHeader = $scope.gridApiHeader.selection.getSelectedRows();
            // var dataDetail = $scope.gridApiDetail.selection.getSelectedRows();

            console.log("dataHeader => ",dataHeader);

            //validasi tambelan :--------------------------------------------------------------------------------------------
            /*
                Added By : Yap
                untuk memilih minimal satu data di header.
                setiap detil di header tersebut akan diambil.
                validasi untuk di detill nya adalah QtyPrepickingnya harus lebih dari 0 untuk minimal 1 data di detil di setiap header

                *note : semua detil akan di kirim
            */
            if (dataHeader.length == 0) {
                bsNotify.show({
                    title: "PrePicking",
                    content: "Data Header harus di pilih minimal 1",
                    type: 'danger'
                });

                return;
            }

            for (var i in dataHeader) {
                if (dataHeader[i].RequestBy == null || dataHeader[i].RequestBy == undefined ){
                    PrePicking.getDataNamaSA(dataHeader[i].AppointmentNo).then(function(res) {
                        dataHeader[i].RequestBy = res.data.Result;
                    });
                }

                if (dataHeader[i].MaterialRequestStatusId != 5) {
                    bsNotify.show({
                        title: "PrePicking",
                        content: "Data Header Sudah Selesai di Prepecking",
                        type: 'danger'
                    });

                    return;
                }
                for (var j in dataHeader[i].APart_MaterialRequestItemTs) {
                    if (dataHeader[i].APart_MaterialRequestItemTs[j].QtyPrepicking > 0) {
                        for (var z = 0; z < dataHeader[i].APart_MaterialRequestItemTs.length; z++)
                            if (dataHeader[i].APart_MaterialRequestItemTs[z].QtyPrepicking < 1)
                                delete dataHeader[i].APart_MaterialRequestItemTs[z];
                        break;
                    } else if (j == dataHeader[i].APart_MaterialRequestItemTs.length - 1) {
                        bsNotify.show({
                            title: "PrePicking",
                            content: "QtyPrepicking harus ada yang lebih dari 0 dan kurang dari atau sama dengan QtyRequest-nya untuk detil per headernya",
                            type: 'danger'
                        });
                        return;
                    }


                }
            }
            //END validasi tambelan :----------------------------------------------------------------------------------------


            //Proses Insert
            PrePicking.create($scope.gridApiHeader.selection.getSelectedRows()).then(function(res) {
                    var resData = res.data;
                    if (res.data.ResponseCode == "13") {
                        console.log("create ", resData);
                        bsNotify.show({
                            title: "PrePicking",
                            content: "Data berhasil disimpan",
                            type: 'success'
                        });
                    } else {
                        bsNotify.show({
                            title: "PrePicking",
                            content: "Periksa kembali field mandatory",
                            type: 'danger'
                        });
                    }


                },
                function(err) {
                    console.log("err=>", err);
                });
        }

        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            // for checking version

            $timeout(function() {
                console.log("Approve = ", $scope.checkRights(4));
                //get material type and service type
                $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
                $scope.mData.ServiceTypeId = $scope.getRightServiceType();
                console.log("MaterialTypeId : ", $scope.mData.MaterialTypeId);
                console.log("ServiceTypeId : ", $scope.mData.ServiceTypeId);
                console.log("dev_parts 16 November 2017 ", document.lastModified);
                console.log("User = ", $scope.user);
                PartsCurrentUser.getWarehouseIdi().then(function(res) {
                    for (var i=0; i < res.data.length; i++) {
                      $scope.mData.WarehouseId = res.data[i].warehouseid;
                      //$scope.mData.WarehouseId = gridData[0].WarehouseId;
                      $scope.mFilter.WarehouseId = res.data[i].WarehouseId;
                      $scope.mData.OutletId = $scope.user.OrgId;
                      $scope.mFilter.OutletId = $scope.user.OrgId;
                      console.log("WarehouseId : ", $scope.mData.WarehouseId);
                    }

                  });

                // PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.user.OrgId).then(function(res) {
                //         var gridData = res.data;
                //         console.log(gridData[0]);

                //         $scope.mData.WarehouseId = gridData[0].WarehouseId;
                //         $scope.mFilter.WarehouseId = gridData[0].WarehouseId;
                //         $scope.mData.OutletId = $scope.user.OrgId;
                //         $scope.mFilter.OutletId = $scope.user.OrgId;
                //     },
                //     function(err) {
                //         console.log("err=>", err);
                //     }
                // );
            });
        });

        $scope.checkRights = function(bit) {
            var p = $scope.myRights & Math.pow(2, bit);
            var res = (p == Math.pow(2, bit));
            //console.log("myRights => ", $scope.myRights);
            return res;
        }

        $scope.getRightMaterialType = function() {
            var i = 0;
            var a = 9, b = 7;
            if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
            else if ($scope.checkRights(b)) i = 2;
            else if ($scope.checkRights(a)) i = 1;
            return i;
        }
        $scope.getRightServiceType = function() {
            var i = 0;
            var a = 10, b = 11;
            if ($scope.checkRights(a)) i = 1;
            else if ($scope.checkRights(b)) i = 0;
            return i;
        }

        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.gridDataTree = [];
        // $scope.getData = function(mFilter) {
        //   console.log("Dari filter ", mFilter);
        //   PrePicking.getData(mFilter).then(function(res) {
        //       var data = res.data.Result;
        //       $scope.gridData = data;
        //       console.log("List ", data);
        //       for(i = 0; i < data.length; i++){
        //         data[i].subGridOptions = {
        //           columnDefs: [
        //           {
        //             name:"Pekerjaan",
        //             field:"TaskId",
        //             cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.TaskId">{{grid.appScope.$parent.SearchDetail(row.entity)}}</div>',
        //           },
        //           {
        //             name:"No. Material",
        //             field:"PartId",
        //             cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.PartId">{{grid.appScope.$parent.SearchDetail(row.entity)}}</div>',
        //           },
        //           {name:"Nama Material", field:"PartName"},
        //           {name:"Qty Free", field:"QtyReq"},
        //           {name:"Qty Reserved", field:"QtyReq"},
        //           {name:"Qty Order", field:"QtyReq"},
        //           {name:"Qty Request", field:"QtyReq"},
        //           {name:"Qty Remain", field:"QtyReq"},
        //           {name:"Qty Prepicking", field:"QtyPrepicking"},
        //           {
        //             name:"Satuan",
        //             field:"UomId",
        //             cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.UomId">{{grid.appScope.$parent.SearchDetail(row.entity)}}</div>',
        //           },
        //           {
        //             name:"Lokasi Rak",
        //             field:"WarehouseId",
        //             cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.WarehouseId">{{grid.appScope.$parent.SearchDetail(row.entity)}}</div>',
        //           },
        //           {name:"Keterangan", field:"Notes"},
        //           {name:"Status", field:"StatusCode"}
        //           ],
        //           data: data[i].APart_MaterialRequestItemTs
        //       }
        //     }
        //     $scope.gridOptions.data = data;
        //     },
        //     function(err) {
        //       console.log("err=>", err);
        //     }
        //   );
        // }

        $scope.onDeleteFilter = function(Filter) {
            $scope.mFilter.AppointmentNo = null;
            $scope.mFilter.NoPol = null;
            $scope.mFilter.AppointmentDate = null;
        }

        $scope.getData = function(mFilter) {
            console.log("Dari filter ", mFilter);
            PartsCurrentUser.getWarehouseIdi().then(function(res) {
                for (var i=0; i < res.data.length; i++) {
                  $scope.mData.WarehouseId = res.data[i].warehouseid;
                  $scope.mData.OutletId = $scope.user.OrgId;
                  $scope.mFilter.OutletId = $scope.user.OrgId;
                }
                $scope.mFilter.WarehouseId =  $scope.mData.WarehouseId;
                console.log("Dari filter ", mFilter);
            if ($scope.mFilter.AppointmentNo == null && $scope.mFilter.NoPol == null && $scope.mFilter.AppointmentDate == null) {
                bsNotify.show({
                    title: "Prepicking",
                    content: "Filter tidak boleh kosong",
                    type: 'danger'
                });
            } else if ($scope.mFilter.WarehouseId == null || $scope.mFilter.OutletId == null) {
                bsNotify.show({
                    title: "Prepicking",
                    content: "Sedang dalam proses pengambilan data Warehouse",
                    type: 'warning'
                });
            } else {
                PrePicking.getData(mFilter).then(function(res) {
                    console.log("Sapa",res.data.Result[0].AppointmentNo);
                    var SA = res.data.Result[0].AppointmentNo;
                        PrePicking.getDataNamaSA(SA).then(function(resu) {
                            $scope.namasa = resu.data.Result[0].employeename;
                            res.data.Result[0].RequestBy =  $scope.namasa;
                            console.log ("nama sa", data);
                        });
                    console.log("hope", res.data.Result);
                        var data = res.data.Result;

                        if (data.length == 0)
                            bsNotify.show({
                                title: "PrePicking",
                                content: "Data Yang Anda Cari Tidak di Temukan",
                                type: 'warning'
                            });

                        $scope.gridData = data;
                        console.log("List ", data);
                        $scope.button = false;
                        for (i = 0; i < data.length; i++) {
                            data[i].subGridOptions = {
                                onRegisterApi: function(gridApi) {
                                    $scope.gridApiDetail = gridApi;
                                    $scope.gridApiDetail.selection.on.rowSelectionChanged($scope, function(row) {
                                        $scope.selectedRows = $scope.gridApiDetail.selection.getSelectedRows();
                                        // console.log("selected=>",$scope.selectedRows);
                                        if ($scope.onSelectRows) {
                                            $scope.onSelectRows($scope.selectedRows);
                                            console.log("=============================>", $scope.gridApiDetail);
                                            console.log("$scope.gridApiHeader=>", $scope.gridApiHeader);
                                            $scope.gridApiHeader.grid.rows[0].entity.APart_MaterialRequestItemTs = $scope.selectedRows;
                                            console.log("$scope.gridApiHeader.grid.rows[0].entity.APart_MaterialRequestItemTs=>", $scope.gridApiHeader.grid.rows[0].entity.APart_MaterialRequestItemTs);
                                        }
                                    });

                                    gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {

                                        console.log('rowEntity', rowEntity);
                                        console.log('colDef', colDef);
                                        console.log('newValue', newValue);
                                        console.log('oldValue', oldValue);
                                        // if((newValue > rowEntity.QtyRequest) || (newValue > rowEntity.QtyFree))
                                        if ((newValue > rowEntity.QtyRequest)) {
                                            rowEntity.QtyPrepicking = 0;
                                            bsNotify.show({
                                                title: "Prepicking",
                                                content: "Qty Prepicking tidak boleh lebih besar dari Qty Request atau Qty Free",
                                                type: 'danger'
                                            });
                                        }

                                        if ((rowEntity.QtyRequest == rowEntity.QtyPrepicking) && rowEntity.QtyPrepicking != 0) {
                                            rowEntity.Status = 7;
                                        }


                                    });


                                },
                                columnDefs: [{
                                        name: "Pekerjaan",
                                        field: "TaskName",
                                        enableCellEdit: false
                                            //cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.subGridOptions.TaskId">{{grid.appScope.Halo}}</div>',
                                    },
                                    {
                                        name: "No. Material",
                                        field: "PartsCode",
                                        enableCellEdit: false
                                            //cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.subGridOptions.PartId">{{grid.appScope.$parent.SearchDetail(row.entity)}}</div>',
                                    },
                                    { name: "Nama Material", field: "PartName", enableCellEdit: false },
                                    { name: "Qty Free", field: "QtyFree", enableCellEdit: false },
                                    { name: "Qty Reserved", field: "QtyReserved", enableCellEdit: false },
                                    { name: "Qty Order", field: "QtyOrder", enableCellEdit: false },
                                    { name: "Qty Request", field: "QtyRequest", enableCellEdit: false },
                                    { name: "Qty Remain", field: "QtyRemain", enableCellEdit: false },
                                    { name: "Qty Prepicking", field: "QtyPrepicking" },
                                    {
                                        name: "Satuan",
                                        field: "UomName",
                                        enableCellEdit: false
                                            //cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.subGridOptions.UomId">{{grid.appScope.$parent.SearchDetail(row.entity)}}</div>',
                                    },
                                    {
                                        name: "Lokasi Rak",
                                        field: "WarehouseCode",
                                        enableCellEdit: false
                                            //cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.subGridOptions.WarehouseId">{{grid.appScope.$parent.SearchDetail(row.entity)}}</div>',
                                    },
                                    { name: "Keterangan", field: "Notes" },
                                    { name: "Status", field: "MaterialRequestStatusId", cellFilter: 'filterStatusPrepicking', enableCellEdit: false }
                                ],
                                data: data[i].APart_MaterialRequestItemTs
                            }
                        }
                        $scope.gridOptions.data = data;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
            });
        }

        $scope.refreshData = function() {
            $scope.gridOptions.data = $filter('filter')($scope.gridData, $scope.searchText, undefined);
        };

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);

        }

        $scope.detailActionButtonSetting = [{
                func: function(row, formScope) {
                    $scope.mData.JobTask = gridTemp;
                    console.log("mData", $scope.mData);

                },
                title: 'Simpan Draft',
                icon: '',
                //rightsBit: 1
            },
            {
                func: function(row, formScope) {
                    $scope.mData.JobTask = gridTemp;
                    console.log("mData", $scope.mData);

                },
                title: 'Simpan & Kirim Email',
                icon: '',
                //rightsBit: 1
            },
            {
                func: function(row, formScope) {

                },
                title: 'Simpan',
                icon: 'fa fa-fw fa-save',
                //rightsBit: 1
            },
        ];

        //------------------------------------------------------------
        $scope.formmode = { mode: '' };
        $scope.doGeneratePass = function() {
            $scope.mUser.password = Math.random().toString(36).slice(-8);
        }
        $scope.onBeforeNewMode = function() {}
        $scope.onBeforeEditMode = function() {


        }
        $scope.onBeforeDeleteMode = function() {

        }


        $scope.onShowDetail = function() {

        }

        // cetak - SSRS
        $scope.printPrePicking = function(mFilter) {
            // var dataHeader = $scope.gridApiHeader.selection.getSelectedRows();
            // console.log('dataHeader', dataHeader[0]);

            // console.log('mFilter =', mFilter);
            // console.log('$scope.mData =', $scope.mData);

            // //var d = mFilter.AppointmentDate;
            // //var n = d.toString();
            // //console.log('n =', n);
            // var ApDate = mFilter.AppointmentDate;
            // var ApNo = mFilter.AppointmentNo;
            // var LPlate = mFilter.NoPol;
            // if (mFilter.AppointmentDate === undefined) { ApDate = dataHeader[0].AppointmentDate };
            // if (mFilter.AppointmentNo === undefined) { ApNo = null };
            // if (mFilter.NoPol === undefined) { LPlate = null };

            // if (mFilter.AppointmentNo == '' || mFilter.AppointmentNo == null) {
            //     ApNo = dataHeader[0].AppointmentNo;
            // }
            // ApDate = $filter('date')(ApDate, 'yyyy-MM-dd');
            // //console.log('mFilter =', mFilter);
            // //var ApNo = encodeURIComponent(mFilter.AppointmentNo);
            // console.log('ApDate =', ApDate);
            // console.log('ApNo =', ApNo);
            // console.log('LPlate =', LPlate);
            // // var data = $scope.mData;
            // $scope.dataPrePicking = 'as/PrintPrePicking?OutletId=' + $scope.user.OrgId +
            //     '&WarehouseId=' + $scope.mData.WarehouseId +
            //     '&MaterialTypeId=' + $scope.mData.MaterialTypeId +
            //     '&AppointmentDate=' + ApDate +
            //     '&AppointmentNo=' + ApNo +
            //     '&LicensePlate=' + LPlate;

            // $scope.cetakan($scope.dataPrePicking);

            // var data = $scope.mData;
            if ($scope.tampungData == [] || $scope.tampungData == [null] || $scope.tampungData.length == 0) {
                bsNotify.show({
                    title: "Warning Message",
                    content: "Ceklis data header Appointment",
                    type: 'warning',
                    timeout: 3000,
                });
            } else {
                $scope.cetakan2($scope.tampungData);

            }
        };

        $scope.cetakan2 = function (dataParts) {
            var pdfFile = null;
            PrePicking.printPrePicking(dataParts).success(function (res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    }
                    else {
                        printJS(pdfFile);
                    }
                }
                else
                    console.log("error cetakan", pdfFile);
            }).error(function (res) {
                console.log("error cetakan", pdfFile);
            })
        }

        $scope.cetakan = function(data) {
            var pdfFile = null;

            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null)
                    printJS(pdfFile);
                else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };

        // ======== end tombol batal - Claim =========

        //----------------------------------
        // Grid Setup
        //----------------------------------
        //$scope.grid.onRegisterApi = function(gridApi){
        //  $scope.gridApi = gridApi;
        //};
        $scope.highlightFilteredHeader = function(row, rowRenderIndex, col, colRenderIndex) {
            if (col.filters[0].term) {
                return 'header-filtered';
            } else {
                return '';
            }
        };

        $scope.gridOptions = {
            expandableRowTemplate: 'app/parts/prepicking/prePickingET.html',
            expandableRowHeight: 150,
            expandableRowScope: {
                subGridVariable: 'subGridScopeVariable'
            },
            rowSelection: 'multiple',
            onRegisterApi: function(gridApi) {
                $scope.gridApiHeader = gridApi;

                $scope.gridApiHeader.expandable.on.rowExpandedStateChanged($scope, function (row, event) {

                    $timeout(function () {
                        // $scope.gridApiDetail.grid.selection.selectAll = true;
                        if (row.isExpanded && row.isSelected) {
                            $scope.gridApiDetail.grid.selection.selectAll = true;
                            for (var j in $scope.gridApiDetail.grid.rows) {
                                $scope.gridApiDetail.grid.rows[j].isSelected = true;
                            }
                            console.log("----------1");
                        } else if (row.isExpanded == false && row.isSelected) {
                            // $scope.gridApiDetail.grid.selection.selectAll = true;
                            for (var j in $scope.gridApiDetail.grid.rows) {
                                $scope.gridApiDetail.grid.rows[j].isSelected = true;
                            }
                            console.log("----------2");
                        }
                    }, 500);
                 })
                $scope.gridApiHeader.selection.on.rowSelectionChanged($scope, function(row) {
                    console.log("ANJAAYAYYYYYYY ====>", row);
                    $scope.selectedRows = $scope.gridApiHeader.selection.getSelectedRows();
                    console.log("Header selected=>",$scope.selectedRows);

                    var dataParts = $scope.selectedRows;
                    console.log('dataParts', dataParts);
                    var arrData = {};

                    if ($scope.selectedRows.length == 0) {
                        $scope.tampungData = [];
                    }

                    for (var i in dataParts) {
                        var endDate = $filter('date')(dataParts[i].AppointmentDate, 'yyyy-MM-dd');
                        arrData = {
                            "AppointmentDate": endDate,
                            "WarehouseId": dataParts[i].WarehouseId,
                            "MaterialTypeId": dataParts[i].RefMRTypeId,
                            "OutletId": dataParts[i].APart_MaterialRequestItemTs[i].OutletId,
                            "MaterialRequestItemId": dataParts[i].APart_MaterialRequestItemTs
                        }

                        if (row.isSelected) {
                            if ($scope.tampungData.length == 0) {
                                $scope.tampungData.push(arrData);
                            }
                            else {
                                for (var j in $scope.tampungData) {
                                    for (var k = i; k < $scope.tampungData; k++) {
                                        if ($scope.selectedRows[k].APart_MaterialRequestItemTs[k].MaterialRequestId !=
                                            $scope.tampungData[j].MaterialRequestItemId[j].MaterialRequestId) {
                                            $scope.tampungData.push(arrData);
                                        }
                                    }
                                }
                            }
                        } else if (row.isSelected == false) {
                            for (var j in $scope.tampungData) {
                                for (var k = i; k < $scope.tampungData; k++) {
                                    if ($scope.selectedRows[k].APart_MaterialRequestItemTs[k].MaterialRequestId ==
                                        $scope.tampungData[j].MaterialRequestItemId[j].MaterialRequestId) {
                                        $scope.tampungData.splice(i, 1);
                                    }
                                }
                            }
                        }
                    }


                    if ($scope.selectedRows.length > 0) {

                        console.log('arrData', arrData);
                        console.log('tampungData', $scope.tampungData);

                        row.isExpanded = true;
                        // $scope.mFilter.NoPol = $scope.selectedRows[0].LicensePlate;
                        // $scope.mFilter.AppointmentNo = $scope.selectedRows[0].AppointmentNo;
                        row.expandedRowHeight = 150;
                        $timeout(function () {
                            $scope.gridApiDetail.grid.selection.selectAll = true;
                            for (var i in row.entity.subGridOptions.data) {
                                console.log("NYESELKAANNNN ===>1", $scope.gridApiDetail);
                                console.log("NYESELKAANNNN ===>2", $scope.gridApiDetail.grid);
                                for (var j in $scope.gridApiDetail.grid.rows) {
                                    $scope.gridApiDetail.grid.rows[j].isSelected = true;
                                }
                            }
                        }, 500);




                    }else{
                        console.log("tes emng");
                        $scope.gridApiDetail.grid.selection.selectAll = false;
                        for (var i in row.entity.subGridOptions.data) {
                            console.log("tes emng 2");
                            for (var j in $scope.gridApiDetail.grid.rows) {
                                console.log("tes emng3");
                                $scope.gridApiDetail.grid.rows[j].isSelected = false;
                                delete $scope.gridApiDetail.grid.rows[j].isSelected;
                            }
                        }
                    }

                    // $scope.gridApiDetail.selection.on.rowSelectionChanged($scope, function (row) {
                    //     // for(var i in row){
                    //     //     row[i].isSelected = true;
                    //     // }
                    //     // console.log("ANJAAYAYYYYYYY ====>", row);
                    //     // $scope.grid1Api.selection.getSelectedRows().forEach(function (row) {
                    //     //     //Do something
                    //     // });
                    //     return true
                    // });
                }   );
            },

            enableSorting: true,
            enableRowSelection: true,
            enableFiltering: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: false,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'Id',
                    field: 'Id',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'No. Appointment',
                    //field: 'no_appointment'
                    field: 'AppointmentNo',
                    headerCellClass: $scope.highlightFilteredHeader
                        //width: '10%'
                },
                {
                    name: 'No. Polisi',
                    //field: 'no_polisi'
                    field: 'LicensePlate',
                    headerCellClass: $scope.highlightFilteredHeader
                        //width: '15%'
                },
                {
                    name: 'Request Oleh',
                    field: 'RequestBy',
                    headerCellClass: $scope.highlightFilteredHeader
                        // field: 'request_oleh'
                        //width: '15%'
                }
            ]
        };

        //------------------------------------------------------
        //[WARNING] JUST FOR MOCK UP, NOT REFER TO DMS FRAMEWORK
        //------------------------------------------------------
        $scope.grid = {
            expandableRowTemplate: 'app/parts/prepicking/prePickingET.html',
            expandableRowHeight: 150,
            //subGridVariable will be available in subGrid scope
            expandableRowScope: {
                subGridVariable: 'subGridScopeVariable'
            },
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            visible: false,
            enableSelectAll: true,
        }

        $scope.grid.columnDefs = [{
                name: 'Id',
                field: 'Id',
                width: '7%',
                visible: false
            },
            {
                name: 'No. Appointment',
                //field: 'no_appointment'
                field: 'RefMRNo'
                    //width: '10%'
            },
            {
                name: 'No. Polisi',
                //field: 'no_polisi'
                field: 'RefMRNo'
                    //width: '15%'
            },
            {
                name: 'Request Oleh',
                field: 'RequestBy'
                    // field: 'request_oleh'
                    //width: '15%'
            }
        ];

    })
    .filter('filterStatusPrepicking', function() {
        var xstatus = {
            '5': 'Menunggu PrePicking',
            '6': 'Partial PrePicking',
            '7': 'Completed PrePicking',
        };
        return function(input) {
            if (!input) {
                return '';
            } else {
                return xstatus[input];
            }
        };
    });
