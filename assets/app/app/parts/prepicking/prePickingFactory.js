angular.module('app')
  .factory('PrePicking', function($http, CurrentUser, $filter) {
    var currentUser = CurrentUser.user;
    // console.log(currentUser);
    return {
      getData: function(data) {
        // alert(data);
        var endDate = $filter('date')(data.AppointmentDate, 'yyyy-MM-dd');
        var res=$http.get('/api/as/Prepicking', {params: {
                                                          AppointmentDate : (endDate==null?"n/a":endDate), 
                                                          AppointmentNo: (data.AppointmentNo==null?"n/a":data.AppointmentNo),
                                                          NoPol: (data.NoPol==null?"n/a":data.NoPol),
                                                          WarehouseId: (data.WarehouseId==null?"n/a":data.WarehouseId),
                                                          OutletId: (data.OutletId==null?"n/a":data.OutletId),
                                                        } });
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      getDataNamaSA: function(data) {
        var res=$http.get('/api/as/GetUsernameSAforAppointment', {params: { AppointmentNo: (data==null?"n/a":data)} });
        console.log('hasil=>',res);
        return res;
      },

      create: function(data) {
        console.log('tambah data=> ', data);

        return $http.post('/api/as/Prepicking', data);
      },
      update: function(data){
        console.log('rubah data=> ', data);
        return $http.put('/api/as/Prepicking', [{
                                            Id: (data.Id==null?0:data.Id),
                                            VehicleName: data.VehicleName,
                                            VehicleType: data.VehicleType
                                          }]);
      },

      printPrePicking: function (dataParts){
        console.log('data => ', dataParts);
        // var endDate = $filter('date')(dataParts.AppointmentDate, 'yyyy-MM-dd');
        // return $http.put('/api/as/PrintPrePicking2', [{
        //                                     AppointmentDate: endDate,
        //                                     WarehouseId: dataParts.WarehouseId,
        //                                     MaterialTypeId: dataParts.RefMRTypeId,
        //                                     OutletId: dataParts.APart_MaterialRequestItemTs.OutletId,
        //                                     MaterialRequestItemId: dataParts.APart_MaterialRequestItemTs
        // }],{headers: {'Content-Type': 'application/json'},responseType: 'arraybuffer'});
        return $http.put('/api/as/PrintPrePicking2', dataParts, { headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer' });
      }
     
    }
  });
