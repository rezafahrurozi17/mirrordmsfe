angular.module('app')
    .controller('GoodsIssueController', function($scope, CurrentUser, GoodsIssue, ngDialog, $timeout, PartsCurrentUser,
        LocalService, PrintRpt, $filter, bsNotify, PartsGlobal, PartsStatusStock,PartsSalesOrder, bsAlert) {
        $scope.LoginData = {};
        $scope.isDebug = true;
        $scope.formState = '';
        $scope.captionLabel = {};
        $scope.captionLabel.new = "Buat";
        $scope.show_btnBatal = true;
        $scope.stat = 0;
        $scope.mData = {};
        $scope.BengkelHide = true;
        $scope.CustomerHide = false;
        $scope.IsOPB = '';
        $scope.formApiGoodIssue = {};
        //----------------------------------
        // Start-Up
        //----------------------------------
        // added by sss on 2018-03-14
        var loadedContent = false;
        //
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.changingFilterButton();
            // Approval verification

            //get material type and service type
            //login 41 -> RoleId: 1135 -> GR
            //login 25 -> RoleId: 1122 -> GR
            //login 26 -> RoleId: 1123 -> BP
            //login 27 -> RoleId: 1125 -> bahan GR
            //login 28 -> RoleId: 1124 -> bahan BP
            if ($scope.user.RoleId == 1135) {
                $scope.MaterialTypeId = 1;
                $scope.ServiceTypeId = 1;
                console.log('1135', $scope.MaterialTypeId, $scope.ServiceTypeId);
            } else if ($scope.user.RoleId == 1122) {
                $scope.MaterialTypeId = 1;
                $scope.ServiceTypeId = 1;
                console.log('1122', $scope.MaterialTypeId, $scope.ServiceTypeId);
            } else if ($scope.user.RoleId == 1123) {
                $scope.MaterialTypeId = 1;
                $scope.ServiceTypeId = 0;
                console.log('1123', $scope.MaterialTypeId, $scope.ServiceTypeId);
            } else if ($scope.user.RoleId == 1125) {
                $scope.MaterialTypeId = 2;
                $scope.ServiceTypeId = 1;
                console.log('1125', $scope.MaterialTypeId, $scope.ServiceTypeId);
                // $scope.ServiceTypeId = 1;
                // $scope.ServiceTypeId = 0;
                // $scope.FunctionGetData2(mData);
            } else if ($scope.user.RoleId == 1124) {
                $scope.MaterialTypeId = 2;
                $scope.ServiceTypeId = 0;
                console.log('1124', $scope.MaterialTypeId, $scope.ServiceTypeId);
            }

            $timeout(function() {
                // added by sss on 2018-03-14
                loadedContent = true;
                //

                console.log("Approve = ", $scope.checkRights(4));
                console.log("dev_parts 24 November 2017 ", document.lastModified);
                if ($scope.checkRights(4)) {
                    $scope.mFilter.GoodsIssueStatusId = 1;

                    $scope.getData();
                } else {
                    $scope.mFilter.RefGITypeID = null;
                    $scope.mFilter.GoodsIssueStatusId = null;
                    $scope.mFilter.startDate = null;
                    $scope.mFilter.endDate = null;
                    $scope.mFilter.GoodsIssueNo = null;
                    $scope.mFilter.RefGINo = null;
                    $scope.mFilter.NoPol = null;
                    $scope.mFilter.PartsCode = null;
                }

                if (itIsLink) {
                    var x;
                    switch (paramNew.TransactionType) {
                        case "Work Order":
                            x = "1";
                            break;
                        case "Sales Order":
                            x = "2";
                            break;
                        case "Transfer Order":
                            x = "3";
                            break;
                    }
                    if (typeof x !== 'undefined') {
                        console.log("x : ", x);
                        $scope.MaterialTypeId = paramNew.MaterialTypeId;
                        $scope.ServiceTypeId = paramNew.ServiceTypeId;
                        $scope.IsOPB = paramNew.Irregular;
                        var tempOrg = angular.copy($scope.user.OrgId);
                        console.log('paramNew', paramNew);
                        console.log('$scope.IsOPB', $scope.IsOPB);

                        PartsCurrentUser.getWarehouseId(paramNew.ServiceTypeId, paramNew.MaterialTypeId, tempOrg).then(function(res) {
                                $scope.mData.WarehouseId = res.data[0].WarehouseId;
                                console.log('$scope.mData.WarehouseId', $scope.mData.WarehouseId);

                                $scope.changedValue(x);
                                $scope.mData.RefGINo = paramNew.RefGINo;
                                $scope.onSearchRefGINo(paramNew.RefGINo,paramNew.RefGITypeID,paramNew.Irregular);
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    }

                    itIsLink = false;
                }
            });

            // [START] Get Current User Warehouse
            // PartsCurrentUser.getWarehouseData().then(function(res) {
            //     var loginData = res.data;
            //     console.log(loginData[0]);
            //     $scope.mData.OutletId = loginData[0].OutletId;
            //     $scope.mData.WarehouseId = loginData[0].WarehouseId;

            //   },
            //   function(err) {
            //     console.log("err=>", err);
            //   }
            // );
            // [END] Get Current User Warehouse


        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        console.log('DG', $scope.user);
        $scope.mData = {}; //Model
        $scope.mFilter = {}; //Model
        $scope.mData.GridDetail = {}; //Model
        $scope.xRole = {
            selected: []
        };
        $scope.AlreadyGI = false;
        $scope.PrintableGI = false;

        //Options Switch
        $scope.items = ['WO', 'Sales Order', 'Transfer Order', 'Return - Parts Claim', 'Return - RPP'];
        $scope.selection = $scope.items[0];
        $scope.disableSelect = false;
        $scope.actionVisible = false;
        $scope.mySelections = [];

        //Form State
        $scope.isNewForm = false;
        $scope.isEditForm = '';
        $scope.isEditState = false;
        $scope.isOverlayForm = false;
        $scope.isViewForm = '';
        $scope.isEditClicked = false;
        $scope.crudState = '';
        $scope.bsForm = true;

        $scope.dateOptionsStart = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0
        };

        $scope.dateOptionsEnd = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0,
        }

        $scope.dateOptions = {
            startingDay: 1,
            format: "dd/MM/yyyy",
            disableWeekend: 0
        };

        //model batal
        $scope.Alasan = {};
        $scope.BatalData = GoodsIssue.getGoodsIssueHeader();
        $scope.CancelReasonId = 0;
        $scope.CancelReasonDesc = '';

        //New Form State
        $scope.ref = '';

        var itIsLink = false;
        var paramNew;

        $scope.resizeLayout = function () {
            console.log("Make sure this function called");
            $timeout(function () {
                $("#layoutContainer_GIBaru").height($(window).height());
            }, 100);
        };

        $scope.onBeforeCancel = function (hiya) {
            $timeout(function () {
                $("#layoutContainer_GIBaru").height($(window).height());
            }, 100)
        }




        $scope.changingFilterButton = function() {
                var tmpGridCols = [];
                var x = -1;
                for (var i = 0; i < $scope.grid.columnDefs.length; i++) {
                    if ($scope.grid.columnDefs[i].visible == undefined && $scope.grid.columnDefs[i].name !== 'batalGI') {
                        x++;
                        tmpGridCols.push($scope.grid.columnDefs[i]);
                        tmpGridCols[x].idx = i;
                    }
                }
                console.log("tmpGridCols", tmpGridCols);
                console.log(" $scope.formApiGoodIssue", $scope.formApiGoodIssue);
                if ($scope.formApiGoodIssue.changeDropdown !== undefined) {
                    $scope.formApiGoodIssue.changeDropdown(tmpGridCols);
                }
            }
            //----------------------------------
            // Get Data
            //----------------------------------
        $scope.changeFormatDate = function(item) {
            var tmpAppointmentDate = item;
            console.log("changeFormatDate item", item);
            tmpAppointmentDate = new Date(tmpAppointmentDate);
            var finalDate
            var yyyy = tmpAppointmentDate.getFullYear().toString();
            var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = tmpAppointmentDate.getDate().toString();
            // finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
            finalDate = (dd[1] ? dd : "0" + dd[0]) + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + yyyy;
            // finalDate = (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + '/' + yyyy;

            console.log("changeFormatDate finalDate", finalDate);
            return finalDate;
        };
        $scope.gridDataTree = [];
        $scope.getData = function() {
            if ($scope.mFilter.RefGITypeID == null && $scope.mFilter.GoodsIssueStatusId == null && $scope.mFilter.startDate == null && $scope.mFilter.endDate == null &&
                $scope.mFilter.GoodsIssueNo == null && $scope.mFilter.RefGINo == null && $scope.mFilter.NoPol == null && $scope.mFilter.PartsCode == null) {
                if (loadedContent) {
                    bsNotify.show({
                        title: "Goods Issue",
                        content: "Filter tidak boleh kosong",
                        type: 'danger'
                    });
                }
            } else if ($scope.mFilter.startDate == null && $scope.mFilter.endDate == null && ($scope.mFilter.GoodsIssueStatusId !== null || $scope.mFilter.RefGITypeID !== null)) {
                bsNotify.show({
                    title: "Goods Issue",
                    content: "Filter Referensi GI dan Tanggal harus diisi bersamaan",
                    type: 'danger'
                });
            } else {
                GoodsIssue.getData($scope.mFilter).then(function(res) {
                        var gridData = res.data.Result;
                        //console.log(gridData);
                        _.map(gridData, function(val) {
                            console.log("ini apaan", val);
                            val.tipeMaterial = $scope.ToNameMaterial(val.MaterialTypeId);
                            val.refGIType = $scope.ToNameRefGi(val.RefGITypeID);
                            val.status = $scope.ToNameStatus(val.GoodsIssueStatusId);
                            // val.DocDate = $scope.changeFormatDate(val.DocDate);
                        });
                        $scope.grid.data = gridData;
                        $scope.loading = false;
                        $scope.changingFilterButton();
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
        }

        $scope.checkRights = function(bit) {
            console.log("$scope.myRights : ", $scope.myRights);
            var p = $scope.myRights & Math.pow(2, bit);
            var res = (p == Math.pow(2, bit));
            console.log("myRights res=> ", res);
            return res;
        }

        $scope.getRightX = function(a, b) {
            var i = 0;
            if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
            else if ($scope.checkRights(b)) i = 2;
            else if ($scope.checkRights(a)) i = 1;
            return i;
        }

        $scope.getRightMaterialType = function() {
            var i = $scope.getRightX(8, 9);
            return i;
        }

        $scope.getRightServiceType = function() {
            var i = $scope.getRightX(10, 11);
            return i;
        }


        $scope.goBack = function() {
            console.log("$scope.formApiGoodIssue=>",$scope.formApiGoodIssue);
            console.log("GoodsIssue.formApi=>",GoodsIssue.formApi);
            $scope.isOverlayForm = false;
            GoodsIssue.formApi.setMode("grid");
        }

        //button kembali pada overlay
        $scope.goBack2 = function() {
            console.log("$scope.formApiGoodIssue=>",$scope.formApiGoodIssue);
            console.log("GoodsIssue.formApi=>",GoodsIssue.formApi);
            $scope.bsForm = true;
            $scope.crudState = '';
            $scope.isOverlayForm = false;
            $scope.formApiGoodIssue.setMode("grid");
        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
            $scope.testmodel = rows;
        }

        $scope.onRegisterApi = function(gridApi) {
            $scope.myGridApi = gridApi;
        };

        $scope.afterRegisterGridApi = function(gridApi) {
            console.log(gridApi);
        }

        //------------------------------------------------------------
        $scope.formmode = { mode: '' };
        $scope.doGeneratePass = function() {
                $scope.mUser.password = Math.random().toString(36).slice(-8);
            }
            // added by sss on 2017
        $scope.checkFreeze = [];
        //
        $scope.onBeforeNewMode = function() {
            $scope.mData = {};


            $scope.mData.GridDetail = {};
            GoodsIssue.formApi = $scope.formApiGoodIssue;
            //get material type and service type
            GoodsIssue.MaterialTypeId = $scope.MaterialTypeId;
            GoodsIssue.ServiceTypeId = $scope.ServiceTypeId;
            console.log("MaterialTypeId : ", GoodsIssue.MaterialTypeId);
            console.log("ServiceTypeId : ", GoodsIssue.ServiceTypeId);
            // if ($scope.MaterialTypeId == 0 || $scope.ServiceTypeId == 0 || $scope.MaterialTypeId == 3 || $scope.ServiceTypeId == 3) {
            //     alert('Material Type atau Service Type salah. Silakan hubungi administrator');
            //     $scope.goBack();
            // }
            // //take parts
            // PartsCurrentUser.getWarehouseId(GoodsIssue.ServiceTypeId, GoodsIssue.MaterialTypeId, 655).then(function(res) {
            //         var gridData = res.data;
            //         console.log(gridData[0]);

            //         $scope.mData.WarehouseId = gridData[0].WarehouseId;
            //         $scope.MaterialTypeId = $scope.getRightMaterialType();
            //         $scope.ServiceTypeId = $scope.getRightServiceType();

            //     },
            //     function(err) {
            //         console.log("err=>", err);
            //     }
            // );
            // [START] Gateway Page
            $scope.isOverlayForm = true;
            $scope.disableSelect = false;
            // [END] Gateway Page

            $scope.formode = 1;
            $scope.isNewForm = true;
            $scope.isViewForm = '';
            $scope.isEditForm = '';
            $scope.selection = null;

            console.log("mode=>", $scope.formmode);
            console.log("$scope.mData.WarehouseId=>", $scope.mData.WarehouseId);
            $scope.newUserMode = true;
            $scope.editMode = false;
            if ($scope.mData.WarehouseId > 0) { // ddg
                // added by sss on 2017-12-19
                GoodsIssue.CekPartsStockOpname().then(function(resu) {
                    $scope.checkFreeze = resu.data.Result;
                });
            }
            //
            //$scope.mode='new';
        }

        // added by sss on 2017-12-22
        $scope.onBeforeEditMode = function() {

        }

        $scope.onBeforeDeleteMode = function() {

            }
            //

        $scope.onShowDetail = function(row, mode, xparam) {
                console.log("---> onShowDetail");

                //$scope.onSearchWoAppointment(row.RefGINo);

                if (mode == 'view') {
                    console.log("[VIEW]");
                    console.log(row);

                    if (row.GoodsIssueStatusId == 0)
                        $scope.show_btnBatal = false;

                    // from older version
                    var shownum = '2';


                    GoodsIssue.getDetail(row.GoodsIssueId, row.RefGINo, row.RefGITypeID).then(function(res) {
                            var gridData = res.data;
                            console.log(gridData);

                            switch (row.RefGITypeID) {
                                case 1:
                                    $scope.gridOptionsView.data = gridData;
                                    break;
                                case 2:
                                    $scope.gridSOView.data = gridData;
                                    break;
                                case 3:
                                    $scope.gridTOView.data = gridData;
                                    break;
                                case 4:
                                    $scope.gridClaimView.data = gridData;
                                    break;
                                case 5:
                                    $scope.mData.VendorName = gridData[0].VendorName;
                                    $scope.mData.NoPol = gridData[0].NoPol;
                                    $scope.mData.AppointmentNo = gridData[0].AppointmentNo;
                                    $scope.gridRPPView.data = gridData;
                                    break;
                            }
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );

                    // if($scope.mData.RefGITypeID === 2)
                    // {
                    //   console.log("===> on Sales Order Region");
                    //   if($scope.mData.FirstName.length > 1)
                    //   {
                    //     //REGION CUSTOMER
                    //     console.log("==> CUSTOMER REGION");
                    //     $scope.crudState = shownum + $scope.formShowHandler($scope.mData.RefGITypeID) + '0';
                    //   }
                    //   if($scope.mData.VendorName.length > 1)
                    //   {
                    //     //REGION BENGKEL
                    //     console.log("==> BENGKEL REGION");
                    //     $scope.crudState = shownum + $scope.formShowHandler($scope.mData.RefGITypeID) + '1';
                    //   }

                    //   else
                    //   {
                    //     $scope.crudState = shownum + $scope.formShowHandler($scope.mData.RefGITypeID) + '1';
                    //   }
                    // }
                    // else{

                    $scope.crudState = shownum + $scope.formShowHandler(row.RefGITypeID);
                    $scope.resizeLayout();

                    // }
                    console.log('[VIEW] ' + $scope.crudState);

                }

                // No Edit in GoodsIssue
                if (mode == 'edit') {
                    console.log("[MODE] Edit ", $scope.crudState);
                    console.log("$scope.bsForm",$scope.bsForm);
                    console.log(row);
                    GoodsIssue.getDetail(row.GoodsIssueId, row.RefGINo, row.RefGITypeID).then(function(res) {
                            var gridData = res.data;
                            console.log(gridData);
                            switch (row.RefGITypeID) {
                                case 1:
                                    $scope.gridOptionsView.data = gridData;
                                    $scope.crudState = '30';
                                    break;
                                case 2:
                                    $scope.gridSOView.data = gridData;
                                    $scope.crudState = '31';
                                    break;
                                case 3:
                                    $scope.gridTOView.data = gridData;
                                    $scope.crudState = '32';
                                    break;
                                case 4:
                                    $scope.gridClaimView.data = gridData;
                                    $scope.crudState = '33';
                                    break;
                                case 5:
                                    $scope.gridRPPView.data = gridData;
                                    $scope.crudState = '34';
                                    break;
                            }
                        },
                        function(err) {
                            console.log("err=>", err);
                        });
                    $scope.bsForm = false;
                }
                if (mode == 'new') {
                    if (typeof xparam !== 'undefined') {
                        if (typeof xparam.fromOtherModule !== 'undefined' || xparam.fromOtherModule) {
                            itIsLink = true;
                            console.log('xparam', xparam);
                            paramNew = angular.copy(xparam);
                        }
                    }
                }
            }
            //----------------------------------
            // Form State Handler
            //----------------------------------
        $scope.formShowHandler = function(refPO) {


            switch (refPO) {
                case "WO":
                case 1:
                    $scope.isViewForm = 'wo_view';
                    $scope.formState = '0';
                    console.log("WO");
                    break;
                case "Sales Order":
                case 2:
                    $scope.isViewForm = 'so_view';
                    $scope.formState = '1';
                    console.log("Sales Orders");
                    break;
                case "Transfer Order":
                case 3:
                    $scope.isViewForm = 'to_view';
                    $scope.formState = '2';
                    console.log("Transfer Order");
                    break;
                case "Return - Parts Claim":
                case 4:
                    $scope.isViewForm = 'rpc_view'
                    $scope.formState = '3';
                    console.log("Return - Parts Claim");
                    break;
                case "Return - RPP":
                case 5:
                    $scope.isViewForm = 'rpp_view'
                    $scope.formState = '4';
                    console.log("Return - RPP");
                    break;
            }

            return $scope.formState;

        }

        $scope.GetMaterialRequestNo = function() {
            //search material request
            GoodsIssue.SearchMaterialRequest().then(function(res) {
                console.log("List Material Request ====>", res);
                $scope.MRdata = res.data;
            });
        }

        $scope.selectMR = function(key) {
            console.log("Isi MR = ", key);
            $scope.mData.MaterialRequestId = key.MaterialRequestId;
        }


        //----------------------------------
        // Select Handler Gateway Form
        //----------------------------------
        $scope.changedValue = function(item) {
            $scope.checkRefGINo = true;
            $scope.mData.RefGITypeID = item;
            $scope.gridOptions.data = [];
            $scope.gridSO.data = [];
            $scope.gridTO.data = [];
            $scope.gridClaim.data = [];
            $scope.gridRPP.data = [];
            console.log("Pilihan ", item);
            if (typeof $scope.mData.RefGITypeID === undefined || $scope.mData.RefGITypeID == null) {
                bsNotify.show({
                    title: "Goods Issue",
                    content: "Referensi tidak boleh kosong",
                    type: 'danger'
                });
                setTimeout(function(){
                    $scope.checkRefGINo = false;
                },1000);
            } else {
                $scope.mData.OutletId = $scope.user.OrgId;
                //console.log("OutletId ", $scope.mData.OutletId);
                console.log("---> Overlay Form Status = " + item);
                $scope.mData.RefGITypeID = item;

                $scope.GetMaterialRequestNo();
                PartsCurrentUser.getVendorOutlet().then(function(res) {
                    console.log("List VendorOutlet====>", res);
                    $scope.vendorOutletData = res.data;
                });

                //tahan tombol simpan
               

                GoodsIssue.SearchWorkOrder().then(function(res) {
                    //console.log("List WorkOrder ====>",res);
                    $scope.WOdata = res.data;
                })

                // GoodsIssue.SearchMaterialRequest().then(function(res){
                //   console.log("List Material Request ====>",res);
                //   $scope.MRdata = res.data;
                // });


                console.log("MaterialTypeId : ", $scope.MaterialTypeId);
                console.log("ServiceTypeId : ", $scope.ServiceTypeId);

                PartsCurrentUser.getWarehouseId($scope.ServiceTypeId, $scope.MaterialTypeId, $scope.user.OrgId).then(function(res) {
                        var gridData = res.data;
                        //console.log(gridData[0]);

                        $scope.mData.WarehouseId = gridData[0].WarehouseId;
                        if ($scope.mData.WarehouseId > 0) { // ddg
                            // added by sss on 2017-12-19
                            GoodsIssue.CekPartsStockOpname().then(function(resu) {
                                $scope.checkFreeze = resu.data.Result;
                            });
                        }
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

                // $scope.mData.mWO.referensi_gi = item;
                // console.log($scope.mData.mWO.referensi_gi);
                var nama = $scope.ToNameRefGi(item);
                console.log(nama);

                $scope.isOverlayForm = false;
                var newnum = '1';
                $scope.crudState = newnum + $scope.formShowHandler(nama);
                $scope.ref = nama;
                console.log('[OVERLAY] ' + $scope.crudState);

                $scope.disableSelect = true;

            }

        }
        $scope.closeGateway = function() {
            $scope.isOverlayForm = false;
        }

        //----------------------------------
        // Report
        //----------------------------------
        $scope.laporan = function() {
            // ngDialog.openConfirm ({
            //   template:'<div ng-include=\"\'app/parts/goodsissue/helper/report_dialog.html\'\"></div>',
            //   plain: true,
            //   // width: 800px,
            //   controller: 'GoodsIssueController',
            //  });
            console.log($scope.mData);
            $scope.printGIBatal($scope.mData.GoodsIssueId);
        };
        // ======== tombol  simpan =========
        $scope.simpan = function() {
            ngDialog.openConfirm({
                template: '<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Permohonan Approval</h3></div> <div class="panel-body"><form method="post"><div class="row"><div class="col-md-10"><p><i>GI ini membutuhkan Approval.</i></p></div></div><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nama Kegiatan</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">GI</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nomor Request</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">D1/20160717001</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Tanggal dan Jam</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">-</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Approve(s)</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4"></textarea></div></div> <br><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Pesan</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4">Mohon untuk approve list dari item Goods Issue berikut. Terima kasih.</textarea></div></div> <br><div class="row"><div class="col-md-12"><div class="form-group"><div><button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Batal</button> <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()" style="color:white; background-color:#D53337">Kirim Permohonan</button> </div></div></div></div></form></div></div>',
                //templateUrl: '/app/parts/pomaintenance/alasan.html',
                plain: true,
                controller: 'GoodsIssueController',
            });
        };
        // ======== end tombol  simpan =========

        // ======== tombol batal - purchase order- alasan =========
        $scope.batalPC = function(data) {
            console.log("batalPC");
            console.log($scope.mData);

            GoodsIssue.setGoodsIssueHeader($scope.mData);

            // Permintaan Pak Sahat
            //   if ($scope.mData.RefGITypeID == 2 && $scope.mData.isCancelBilling == null){
            //       bsNotify.show({
            //           title: "Goods Issue",
            //           content: "Harap Batalkan Sales Billing Terlebih dahulu",
            //       type: 'danger'
            //       });
            //   }
            //else
            //if ($scope.mData.RefGITypeID == 2 && $scope.mData.isCancelBilling == 3){

            if ($scope.mData.RefGITypeID == 2 && $scope.mData.isCancelBilling == 3 || $scope.mData.RefGITypeID == 2 && $scope.mData.SalesBillingStatusId == null || $scope.mData.RefGITypeID == 2 && $scope.mData.SalesBillingStatusId == 2 ){
                ngDialog.openConfirm({
                    template: '<div ng-include=\"\'app/parts/goodsissue/helper/batalgi.html\'\"></div>',
                    plain: true,
                    // controller: 'GoodsIssueController',
                    scope: $scope,
                });
            } 
            else if($scope.mData.RefGITypeID == 2 && $scope.mData.SalesBillingStatusId == 1) {
                bsNotify.show({
                        title: "Goods Issue",
                        content: "Batalkan Sales Billing terlebih dahulu.",
                        type: 'danger'
                        });
            }
            else if ($scope.mData.RefGITypeID == 1) {
                GoodsIssue.getStatusWOTeco($scope.mData.RefGINo).then(function(res) {
                    var statusWO = res.data.Result[0].Status;
                    console.log('res==>',res);
                    console.log('statusWO==>',statusWO);

                    if(statusWO >= 14 && statusWO != 21 && statusWO != 22 && statusWO != 23 && statusWO != 25) {
                        bsAlert.alert({
                            title: "Batal GI",
                            text: "WO sudah proses TECO, tidak bisa Batal GI",
                            type: "error",
                            showCancelButton: false
                        });
                    } else {
                        ngDialog.openConfirm({
                            template: '<div ng-include=\"\'app/parts/goodsissue/helper/batalgi.html\'\"></div>',
                            plain: true,
                            // controller: 'GoodsIssueController',
                            scope: $scope,
                        });        
                    }
                });
            }
            // else if ($scope.mData.RefGITypeID != 2){
            else {
                ngDialog.openConfirm({
                    template: '<div ng-include=\"\'app/parts/goodsissue/helper/batalgi.html\'\"></div>',
                    plain: true,
                    // controller: 'GoodsIssueController',
                    scope: $scope,
                });
            }
           
        };

        // dialog konfirmasi
        $scope.okBatalPC = function(data, mData) {
            console.log('alasan aja', data.Batal);

            if (data.Batal == '' || data.Batal == undefined || data.Batal == null) {
                bsNotify.show({
                    title: "Goods Issue",
                    content: "Isi Alasan Pembatalan",
                    type: 'danger'
                });
            } else {
                console.log("okBatalPC");

                console.log($scope.mData);

                $scope.BatalData = GoodsIssue.getGoodsIssueHeader();
                $scope.BatalData.Notes = 'Alasan: ' + data.Batal + ', Catatan: ' + data.Catatan;

                console.log($scope.BatalData);

                GoodsIssue.setGoodsIssueHeader($scope.BatalData);

                console.log(data);
                $scope.BatalData.CancelReasonId = parseInt(data.Batal);
                $scope.BatalData.CancelReasonDesc = data.Catatan;
                ngDialog.openConfirm({
                    template: '\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Anda yakin akan membatalkan GI ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="SetujuBatal(BatalData)">Ya</button>\
                     </div>\
                     </div>',
                    plain: true,
                    // controller: 'GoodsIssueController',
                    scope: $scope,
                });
            }
        };
        // ======== end tombol batal - purchase order =========
        
        $scope.changeFormatDate = function(item) {
            var tmpAppointmentDate = item;
            console.log("changeFormatDate item", item);
            tmpAppointmentDate = new Date(tmpAppointmentDate);
            var finalDate
            var yyyy = tmpAppointmentDate.getFullYear().toString();
            var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = tmpAppointmentDate.getDate().toString();
            // finalDate = (dd[1] ? dd : "0" + dd[0]) + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + yyyy;
            finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
            console.log("changeFormatDate finalDate", finalDate);
            return finalDate;
        }
        
        $scope.SetujuBatal = function(data) {
            var d = new Date();
            console.log('SetujuBatal $scope.mData', $scope.mData);
            console.log('SetujuBatal data', data);
            // [START] Get Search Appointment
            $scope.mData = GoodsIssue.getGoodsIssueHeader();
            $scope.mData.CancelDate = d;
            $scope.mData.GoodsIssueStatusId = 1;
            $scope.mData.DocDate = $scope.changeFormatDate($scope.mData.DocDate)
            $scope.ngDialog.close();
            $scope.GenerateDocNumstwo(data);
            console.log("$scope.formApiGoodIssue=>",$scope.formApiGoodIssue);
            console.log("GoodsIssue.formApi=>",GoodsIssue.formApi);
            $scope.goBack2();
            // [END] Get Search Appointment
        }

        // Generate Nomor Dokumen
        $scope.GenerateDocNum = function() {

            PartsCurrentUser.getFormatId($scope.ServiceTypeId, 'GI').then(function(res) {
                    var result = res.data;
                    console.log("FormatId = ", result[0].Results);

                    PartsCurrentUser.getDocumentNumber2(result[0].Results).then(function(res) {
                            var DocNo = res.data;

                            if (typeof DocNo === 'undefined' || DocNo == null) {
                                bsNotify.show({
                                    title: "Goods Issue",
                                    content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                                    type: 'danger'
                                });
                            } else {
                                $scope.mData.GoodsIssueNo = DocNo[0];
                                console.log("Doc Date : ", DocNo[1]);
                                $scope.mData.DocDate = DocNo[1]; //PartsGlobal.getXDate(DocNo[1].toString); //DocNo[1]
                                console.log("Generating DocNo & DocDate ->");
                                console.log($scope.mData.GoodsIssueNo);
                                console.log($scope.mData.DocDate);
                            }


                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );


                },
                function(err) {

                }
            );
            // [END] Get Current User Warehouse
        }

        //----------------------------------
        // [Start] Button Handler
        //----------------------------------
        // Button Search WO/Appointment
        $scope.onSearchWoAppointment = function(NomorWo) {
            // PartsCurrentUser.getWarehouseData().then(function(res) {
            //     var loginData = res.data;
            //     console.log(loginData[0]);
            //     $scope.mData.OutletId = loginData[0].OutletId;
            //     $scope.mData.WarehouseId = loginData[0].WarehouseId;
            //     $scope.MaterialTypeId = loginData[0].MaterialTypeId;

            //   },
            //   function(err) {
            //     console.log("err=>", err);
            //   }
            // );
            // Eksekusi Nomor Dokumen

            // [START] Get Search Appointment
            GoodsIssue.searchAppointment(NomorWo).then(function(res) {
                    var AppointmentData = res.data[0];
                    //console.log(AppointmentData);

                    //masukkan ke dalam model
                    $scope.mData.ColorId = AppointmentData.ColorId;
                    // $scope.mData.MachineNo = AppointmentData.MachineNo;
                    $scope.mData.MachineNo = AppointmentData.VIN;
                    $scope.mData.NoPol = AppointmentData.NoPol;
                    $scope.mData.Year = AppointmentData.Year;
                    $scope.mData.VehicleId = AppointmentData.VehicleId;
                    $scope.mData.VehicleModelId = AppointmentData.VehicleModelId;
                    $scope.mData.FullModel = AppointmentData.FullModel;
                    $scope.mData.VehicleColourCode = AppointmentData.VehicleColourCode;
                    $scope.mData.ModelName = AppointmentData.ModelName;
                    $scope.mData.Name = AppointmentData.Name;
                    $scope.mData.FrameNo = AppointmentData.FrameNo;
                    $scope.mData.RequestBy = AppointmentData.RequestBy;


                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            // [END] Get Search Appointment

            // [START] Get Search Appointment Grid Detail
            GoodsIssue.searchAppointmentDetail(NomorWo, $scope.mData.WarehouseId, "").then(function(res) {
                    var AppointmentData = res.data;
                    console.log("Ini dib1=>",AppointmentData.PartId)
                    //console.log('searchAppointmentDetail ==>')
                    //console.log(AppointmentData);

                    $scope.gridOptions.data = AppointmentData;
                    $scope.gridOptionsView.data = AppointmentData;
                    $scope.mData.GridDetail = AppointmentData;

                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            // [END] Get Search Appointment Grid


        }

        // Button Search Generik
        $scope.onSearchRefGINo = function(NomorWo, GiType, IsOPB) {
            console.log("onSearchRefGINo : ", NomorWo, GiType);
            console.log("$scope.crudState : ", $scope.crudState);
            console.log("$scope.IsOPB : ", IsOPB);

            PartsGlobal.isGIDone($scope.mData).then(function(res) {
                var isGIDone = res.data.Result;
                console.log("isGIDone : ", isGIDone);
                console.log("isGIDone[0].GIAlreadyDone : ", isGIDone[0].GIAlreadyDone);
                if (isGIDone[0].GIAlreadyDone == 1) { // GIAlreadyDone
                    bsNotify.show({
                        title: "Goods Issue",
                        content: "GI untuk referensi ini sudah selesai dilakukan",
                        type: 'danger'
                    });
                    $scope.AlreadyGI = true;
                    console.log("$scope.AlreadyGI 1==>>",$scope.AlreadyGI);
                    return 0;
                }

                console.log("Belum GI ");
                $scope.AlreadyGI = false;
                GoodsIssue.IsPrintableGI($scope.mData).then(function(res) {
                    var PrintableGI = res.data.Result[0].Results;
                    console.log("Printable ? : ", res.data.Result[0].Results);
                    $scope.PrintableGI = PrintableGI == 1; //true
                    console.log("code di sini");

                    //yap
                    /*
                    if (!$scope.PrintableGI) {
                        bsNotify.show(
                            {
                            title: "Goods Issue",
                            content: "Belum ada pembayaran billing, Cetak GI tidak bisa dilakukan",
                            type: 'danger'
                            });
                        return 0;
                    }*/
                });
                console.log("$scope.AlreadyGI 2==>>",$scope.AlreadyGI);

                // Jika Sales Order Belum Billing
                //yap
                // di lepas kata pak ari validasinya
                // 5 maret 2018
                /*
                if($scope.mData.RefGITypeID === '2')
                {

                GoodsIssue.SalesBillingGICheck(NomorWo).then(function(res) {
                        var resp = res.data;
                        console.log('SalesBillingGICheck' , resp);
                        if(resp === 1)
                        {
                        }
                        else
                        {
                        bsNotify.show(
                        {
                            title: "Goods Issue",
                            content: "Sales Order belum Sales Billing",
                            type: 'danger'
                        }
                        );
                        }

                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

                }
                */

               console.log("$scope.AlreadyGI Proses==>>",$scope.AlreadyGI);

               if ($scope.AlreadyGI == false) {
                    $scope.GenerateDocNum();
                    // $scope.mData.DocDate = '2017-07-13';
                    // $scope.mData.GoodsIssueNo = '000/GIG/1707-00003'
                    if (_.isEmpty(NomorWo) || _.isNull(NomorWo)) {
                        $scope.mData = [];
                        $scope.mData.GridDetail = [];
                        $scope.gridOptions.data = [];
                        $scope.gridOptionsView.data = [];

                        bsNotify.show({
                            title: "Goods Issue",
                            content: "No Referensi tidak boleh kosong",
                            type: 'danger'
                        });

                        return;
                    }
                    // Eksekusi Nomor Dokumen
                    $scope.checkRefGINo = false;
                    if ($scope.crudState === '10') {
                        // [START] Get Search Appointment
                        GoodsIssue.searchAppointment(NomorWo, $scope.MaterialTypeId).then(function(res) {
                                var AppointmentData = res.data[0];
                                //console.log(AppointmentData);
                                if (_.isUndefined(AppointmentData) || _.isNull(AppointmentData)) {
                                    $scope.mData = [];
                                    $scope.mData.GridDetail = [];
                                    $scope.gridOptions.data = [];
                                    $scope.gridOptionsView.data = [];

                                    bsNotify.show({
                                        title: "Goods Issue",
                                        content: "No Referensi tidak ditemukan",
                                        type: 'danger'
                                    });

                                    return;
                                }

                                //masukkan ke dalam model
                                $scope.mData.ColorId = AppointmentData.ColorId;
                                $scope.mData.MachineNo = AppointmentData.VIN;
                                // $scope.mData.MachineNo = AppointmentData.MachineNo;
                                $scope.mData.NoPol = AppointmentData.NoPol;
                                $scope.mData.Year = AppointmentData.Year;
                                $scope.mData.VehicleId = AppointmentData.VehicleId;
                                $scope.mData.VehicleModelId = AppointmentData.VehicleModelId;
                                $scope.mData.FullModel = AppointmentData.FullModel;
                                $scope.mData.VehicleColourCode = AppointmentData.VehicleColourCode;
                                $scope.mData.ModelName = AppointmentData.ModelName;
                                $scope.mData.Name = AppointmentData.Name;
                                $scope.mData.FrameNo = AppointmentData.FrameNo;
                                $scope.mData.MaterialRequestId = AppointmentData.MaterialRequestId;
                                $scope.mData.MaterialRequestNo = AppointmentData.MaterialRequestNo;
                                $scope.mData.RequestBy = AppointmentData.RequestBy;
                                $scope.mData.MaterialTypeId = $scope.MaterialTypeId;
                                $scope.mData.ServiceTypeId = $scope.ServiceTypeId;

                                console.log("$scope.mData.MaterialTypeId", $scope.mData.MaterialTypeId);
                                console.log("$scope.mData.ServiceTypeId", $scope.ServiceTypeId);

                                //take parts
                                var getMaterialTypeId = $scope.MaterialTypeId;
                                PartsCurrentUser.getWarehouseId(AppointmentData.IsGr, getMaterialTypeId, $scope.user.OrgId).then(function(res) {
                                        var gridData = res.data;
                                        console.log(gridData[0]);

                                        $scope.mData.WarehouseId = gridData[0].WarehouseId;
                                        $scope.MaterialTypeId = $scope.MaterialTypeId;
                                        $scope.ServiceTypeId = $scope.ServiceTypeId;
                                        console.log("$scope.IsOPB==>", IsOPB);
                                        // [START] Get Search Appointment Grid Detail
                                        GoodsIssue.searchAppointmentDetail(NomorWo, $scope.mData.WarehouseId, IsOPB).then(function(res) {
                                                var AppointmentData = res.data;
                                                console.log('searchAppointmentDetail2 ==>',AppointmentData.PartId)
                                                //console.log(AppointmentData);

                                                // added by sss on 2017-12-19
                                                for (var i in AppointmentData) {
                                                    if (typeof _.find($scope.checkFreeze, { "PartsCode": AppointmentData[i].PartsCode }) !== 'undefined') {
                                                        AppointmentData[i].QtyGI = 'Freeze';
                                                    }
                                                }
                                                //
                                                $scope.gridOptions.data = AppointmentData;
                                                $scope.gridOptionsView.data = AppointmentData;
                                                $scope.mData.GridDetail = AppointmentData;

                                            },
                                            function(err) {
                                                console.log("err=>", err);
                                            }
                                        );

                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                );

                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                        // [END] Get Search Appointment


                        // [END] Get Search Appointment Grid
                    } else if ($scope.crudState === '11') {
                        console.log("Sales Order ==>");
                        // PartsCurrentUser.getWarehouseId(AppointmentData.IsGr, getMaterialTypeId, $scope.user.OrgId).then(function(val) {

                        // });
                        // [START] Get Search Appointment
                        GoodsIssue.searchSalesOrder(NomorWo).then(function(res) {
                                var SalesOrderData = res.data;
                                console.log('data search SO DG050918', SalesOrderData);
                        for (var i in SalesOrderData) {
                                if (SalesOrderData[i].RefSOTypeId == 2){
                                $scope.BengkelHide = true;
                                $scope.CustomerHide = false;
                                }else{
                                    $scope.BengkelHide = false;
                                    $scope.CustomerHide = true;
                                }

                                if (_.isUndefined(SalesOrderData[0]) || _.isNull(SalesOrderData[i])) {
                                    $scope.mData.VendorName = "";
                                    $scope.mData.MaterialRequestId = 0;
                                    $scope.mData.MaterialRequestNo = "";
                                    $scope.mData.VendorId = 0;
                                    $scope.gridSO.data = [];
                                    $scope.gridSOView.data = [];
                                    // if (SalesOrderData[0].StockOpnameStatusId == 2){
                                    //     $scope.gridSO.columnDefs[3].editableCellTemplate = false;
                                    //     bsNotify.show(
                                    //     {
                                    //         title: "FREEZE",
                                    //         content: "Parts Sedang digunakan. Harap gunakan parts yang lain.",
                                    //         type: 'denger'
                                    //     }
                                    //     );
                                    // }
                                    $scope.mData.GridDetail = [];

                                    bsNotify.show({
                                        title: "Goods Issue",
                                        content: "No Referensi tidak ditemukan",
                                        type: 'danger'
                                    });

                                    return;
                                }

                                //masukkan ke dalam model
                                $scope.mData.VendorName = SalesOrderData[i].VendorName;
                                $scope.mData.CustomerName = SalesOrderData[i].CustomerName;
                                $scope.mData.MaterialRequestId = SalesOrderData[i].MaterialRequestId;
                                $scope.mData.MaterialRequestNo = SalesOrderData[i].MaterialRequestNo;
                                $scope.mData.VendorId = SalesOrderData[i].CustomerOutletId;
                                SalesOrderData[i].QtyRemain = SalesOrderData[i].QtySO;
                                console.log('src SO DG090518', SalesOrderData[i].QtyRemain , SalesOrderData[i].QtySO);
                            }
                                //masukan ke dalam grid
                                // added by sss on 2017-12-19
                                // for (var i in SalesOrderData) {
                                //     if (typeof _.find($scope.checkFreeze, { "PartsCode": SalesOrderData[i].PartsCode }) !== 'undefined') {
                                //         SalesOrderData[i].QtyGI = 'Freeze';
                                //     }
                                // }
                                _.map(SalesOrderData, function(a) {
                                    a.QtyGI = 0;
                                });
                                //edited by annk
                                console.log("Sales tenpData ==>", $scope.checkFreeze);
                                var tenpData = angular.copy(SalesOrderData);
                                _.map(SalesOrderData, function(a) {
                                    _.map($scope.checkFreeze, function(o) {
                                        if (a.PartsCode == o.PartsCode) {
                                            a.QtyGI = 'Freeze';
                                        }
                                    })
                                });
                                //
                                console.log("Sales tenpData ==>", tenpData);
                                console.log("Sales Order ==>", SalesOrderData);

                                $scope.gridSO.data = SalesOrderData;
                                $scope.gridSOView.data = SalesOrderData;
                                $scope.mData.GridDetail = SalesOrderData;
                                for (var i in SalesOrderData) {
                                    if (SalesOrderData[i].StockOpnameStatusId == 2 || SalesOrderData[i].StockOpnameStatusId == 1){
                                        $scope.gridSO.columnDefs[7].editableCellTemplate = false;
                                        SalesOrderData[i].QtyGI = 'Freeze';
                                        ExistsItemFreeze = true;
                                        console.log("oke u",SalesOrderData[i].QtyGI);
                                        console.log("oke eu",$scope.gridSO.columnDefs[7].editableCellTemplate);
                                    }
                                }

                                //[START] plus satu row untuk penambahan
                                // var n = $scope.gridSO.data.length + 1;
                                // $scope.gridSO.data.push({
                                //   "PartsCode": "",
                                //   "PartsName": "",
                                //   "QtySO": "",
                                //   "QtySO": "",
                                //   "QtySO": "",
                                //   "QtySO": "",
                                //   "QtySO": "",
                                //   "WarehouseName": "",
                                //   "ShelfCode": ""
                                // });
                                // [END] plus satu row untuk penambahan

                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    } else if ($scope.crudState === '12') {
                        console.log("Transfer Order ==>");

                        // [START] Get Search Appointment
                        GoodsIssue.SearchReference(NomorWo, $scope.mData.RefGITypeID).then(function(res) {
                                var RefGIData = res.data;
                                //console.log("Hasil Transfer Order ==>");
                                //console.log(RefGIData);

                                if (_.isUndefined(RefGIData[0]) || _.isNull(RefGIData[0])) {
                                    RefGIData = [];
                                    $scope.mData.VendorName = "";
                                    $scope.gridTOView.data = [];
                                    $scope.mData.GridDetail = [];

                                    bsNotify.show({
                                        title: "Goods Issue",
                                        content: "No Referensi tidak ditemukan",
                                        type: 'danger'
                                    });

                                    return;
                                }

                                //masukkan ke dalam model
                                $scope.mData.VendorName = RefGIData[0].VendorName;
                                RefGIData[0].QtyRemain = RefGIData[0].QtyReserved;

                                //masukan ke dalam grid
                                // added by sss on 2017-12-22
                                for (var i in RefGIData) {
                                    RefGIData[i].QtyRemain = RefGIData[i].QtyReserved;
                                    if (typeof _.find($scope.checkFreeze, { "PartsCode": RefGIData[i].PartsCode }) !== 'undefined') {
                                        RefGIData[i].QtyGI = 'Freeze';
                                    }
                                }

                                //SORRY code bawah di comment dulu, karena ganggu proses

                                // GoodsIssue.SearchReference(NomorWo, 3).then(
                                //     function(res) {
                                //         getGoodIssueInfo = res.data;
                                //
                                //         console.log('getGoodIssueInfo',getGoodIssueInfo);
                                //         var index = "",
                                //             PartsCode = "",
                                //             QtyTransfer = 0,
                                //             status = 5;
                                //         for (var i = 0; i < getGoodIssueInfo.length; i++) {
                                //             index = i.toString();
                                //             PartsCode = res.data[index].PartsCode;
                                //             QtyTransfer = res.data[index].QtyTransfer;
                                //             PartsStatusStock.getData(PartsCode).then(
                                //                 function(res) {
                                //                     getParts = res.data.Result;
                                //                     var QtyFree = getParts["0"].QtyFree;
                                //                     if (QtyTransfer > QtyFree || QtyFree == 0) {
                                //                         status = 0;
                                //                         setStatus(status);
                                //                     } else {
                                //                         status = 1; // stock ada, bisa good issue
                                //                         setStatus(status);
                                //                     }
                                //                 }
                                //             );
                                //         }
                                //
                                //         function setStatus(sts) {
                                //             status = sts;
                                //             console.log("popopop" + status);
                                //             if (status == 0) {
                                //                 bsNotify.show({
                                //                     title: "Goods Issue",
                                //                     content: "Stock Kurang",
                                //                     type: 'danger'
                                //                 });
                                //                 $scope.gridTO.data = [];
                                //             } else {
                                //                 $scope.gridTO.data = RefGIData;
                                //             }
                                //         }
                                //     }
                                // );

                                //--------------New Add for TO-vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
                                //--------------New Add for TO-vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
                                // var index = "",
                                //     PartsCode = "",
                                //     QtyTransfer = 0,
                                //     status = 0;
                                // for (var i = 0; i < getGoodIssueInfo.length; i++) {
                                //     index = i.toString();
                                //     PartsCode = res.data[index].PartsCode;
                                //     QtyTransfer = res.data[index].QtyTransfer;
                                //     PartsStatusStock.getData(PartsCode).then(function(res) {
                                //         getParts = res.data.Result;
                                //         var QtyFree = getParts["0"].QtyFree;
                                //         if (QtyTransfer > QtyFree || QtyFree == 0) {
                                //             status = 0; // stock kurang, tidak bisa good issue
                                //         } else {
                                //             status = 1; // stock ada, bisa good issue
                                //         }
                                //     });
                                // }
                                // if (status == 0) {
                                //     bsNotify.show({
                                //         title: "Goods Issue",
                                //         content: "Stock Kurang",
                                //         type: 'danger'
                                //     });
                                // }

                                //--------------New Add for TO-^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                                //--------------New Add for TO^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^                        /
                                $scope.gridTO.data = RefGIData;
                                $scope.gridTOView.data = RefGIData;
                                $scope.mData.GridDetail = RefGIData;

                                //[START] plus satu row untuk penambahan
                                // var n = $scope.gridTO.data.length + 1;
                                // $scope.gridTO.data.push({
                                //   "PartsCode": "",
                                //   "PartsName": "",
                                //   "QtyTransfer": "",
                                //   "QtyTransfer": "",
                                //   "QtyTransfer": "",
                                //   "QtyTransfer": "",
                                //   "QtyTransfer": "",
                                //   "WarehouseName": "",
                                //   "ShelfCode": ""
                                // });
                                // [END] plus satu row untuk penambahan

                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    } else if ($scope.crudState === '13') {
                        console.log("Return Claim ==>");

                        // [START] Get Search Appointment
                        GoodsIssue.SearchReference(NomorWo, $scope.mData.RefGITypeID).then(function(res) {
                                var RefGIData = res.data;
                                //console.log("Hasil Return Claim ==>");
                                //console.log(RefGIData);

                                if (_.isUndefined(RefGIData[0]) || _.isNull(RefGIData[0])) {
                                    RefGIData = [];
                                    $scope.mData.VendorName = "";
                                    $scope.gridClaim.data = RefGIData;
                                    $scope.gridClaimView.data = [];
                                    $scope.mData.GridDetail = [];

                                    bsNotify.show({
                                        title: "Goods Issue",
                                        content: "No Referensi tidak ditemukan",
                                        type: 'danger'
                                    });

                                    return;
                                }

                                //masukkan ke dalam model
                                $scope.mData.VendorName = RefGIData[0].VendorName;
                                console.log ("$scope.mData Part claim",$scope.mData);
                                //masukan ke dalam grid
                                // added by sss on 2017-12-22
                                for (var i in RefGIData) {
                                    if (typeof _.find($scope.checkFreeze, { "PartsCode": RefGIData[i].PartsCode }) !== 'undefined') {
                                        RefGIData[i].QtyGI = 'Freeze';
                                    }
                                }
                                //
                                $scope.gridClaim.data = RefGIData;
                                $scope.gridClaimView.data = RefGIData;
                                $scope.mData.GridDetail = RefGIData;

                                //[START] plus satu row untuk penambahan
                                // var n = $scope.gridClaim.data.length + 1;
                                // $scope.gridClaim.data.push({
                                //   "PartsCode": "",
                                //   "PartsName": "",
                                //   "QtyInvoice": "",
                                //   "QtyInvoice": "",
                                //   "QtyInvoice": "",
                                //   "WarehouseName": "",
                                //   "ShelfCode": ""
                                // });
                                // [END] plus satu row untuk penambahan

                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    } else if ($scope.crudState === '14') {
                        console.log("RPP ==>");

                        // [START] Get Search Appointment
                        GoodsIssue.SearchReference(NomorWo, $scope.mData.RefGITypeID).then(function(res) {
                                var RefGIData = res.data;
                                //console.log("Hasil Return RPP ==>");
                                //console.log(RefGIData);

                                if (_.isUndefined(RefGIData[0]) || _.isNull(RefGIData[0])) {
                                    RefGIData = [];
                                    $scope.mData.VendorName = "";
                                    $scope.mData.NoPol = "";
                                    $scope.mData.AppointmentNo = "";
                                    $scope.gridRPP.data = [];
                                    $scope.gridRPPView.data = [];
                                    $scope.mData.GridDetail = [];

                                    bsNotify.show({
                                        title: "Goods Issue",
                                        content: "No Referensi tidak ditemukan",
                                        type: 'danger'
                                    });

                                    return;
                                }

                                //masukkan ke dalam model
                                $scope.mData.VendorName = RefGIData[0].VendorName;
                                $scope.mData.NoPol = RefGIData[0].NoPol;
                                $scope.mData.AppointmentNo = RefGIData[0].AppointmentNo;

                                //masukan ke dalam grid
                                // added by sss on 2017-12-22
                                for (var i in RefGIData) {
                                    if (typeof _.find($scope.checkFreeze, { "PartsCode": RefGIData[i].PartsCode }) !== 'undefined') {
                                        RefGIData[i].QtyGI = 'Freeze';
                                    }
                                }
                                //
                                $scope.gridRPP.data = RefGIData;
                                $scope.gridRPPView.data = RefGIData;
                                $scope.mData.GridDetail = RefGIData;

                                //[START] plus satu row untuk penambahan
                                // var n = $scope.gridRPP.data.length + 1;
                                // $scope.gridRPP.data.push({
                                //   "PartsCode": "",
                                //   "PartsName": "",
                                //   "QtyReceived": "",
                                //   "QtyRPP": "",
                                //   "QtyGI": "",
                                //   "WarehouseName": "",
                                //   "ShelfCode": ""
                                // });
                                // [END] plus satu row untuk penambahan

                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    }
                }
            });
        }

        // Hapus Filter
        $scope.onDeleteFilter = function(mData) {
            //Pengkondisian supaya seolaholah kosong
            $scope.mFilter.RefGITypeID = null;
            $scope.mFilter.GoodsIssueStatusId = null;
            $scope.mFilter.GoodsIssueNo = null;
            $scope.mFilter.RefGINo = null;
            $scope.mFilter.NoPol = null;
            $scope.mFilter.startDate = null;
            $scope.mFilter.endDate = null;
            $scope.mFilter.PartsCode = null;

        };
        // [START] Button Simpan & Cetak Bukti Pengambilan Material
        $scope.simpancetakWO = function(mWO){
            console.log('$scope.mData ==>', $scope.mData);

            if($scope.checkRefGINo == false){
                
            $scope.checkRefGINo = true;
           
            if($scope.mData.RefGITypeID === '1' || $scope.mData.RefGITypeID === 1){
                GoodsIssue.getStatusWO($scope.mData.RefGINo).then(function(res){
                    if(res.data === 666){
                        bsAlert.warning('WO dengan nomor ' + $scope.mData.RefGINo +' sedang menunggu approval', 'silahkan cek kembali')
                        return false
                    }else{
                        $scope.gosimpancetakWO();
                    }
                  }, function(err){
                    $scope.checkRefGINo = false;
                });
            }else{
                $scope.gosimpancetakWO();
            }
          } 
        }
        $scope.gosimpancetakWO = function(mWO) {
            $scope.checkRefGINo = true;

                var ValidGI = false;
                var ExistsItemFreeze = false;
                console.log("$scope.mData ini",$scope.mData);
                    //validasi pre
                if ($scope.AlreadyGI) {
                    bsNotify.show({
                        title: "Goods Issue",
                        content: "GI untuk referensi ini sudah selesai dilakukan",
                        type: 'danger'
                    });
                    setTimeout(function(){
                        $scope.checkRefGINo = false;
                    },1000);
                    return 0;
                }


                /*
                if (!$scope.PrintableGI) {
                  bsNotify.show(
                      {
                        title: "Goods Issue",
                        content: "Belum ada pembayaran billing, Cetak GI tidak bisa dilakukan",
                        type: 'danger'
                      });
                  return 0;
                }*/
                if (typeof $scope.mData.RefGINo === undefined || $scope.mData.RefGINo == null) {
                    bsNotify.show({
                        title: "Goods Issue",
                        content: "No. Referensi tidak boleh kosong",
                        type: 'danger'
                    });
                    setTimeout(function(){
                        $scope.checkRefGINo = false;
                    },1000);
                }

                //yap
                // tambahin validasi penerimanya tidak boleh kosong
                if (!$scope.mData.ReceivedBy || $scope.mData.ReceivedBy.length == 0) {
                    bsNotify.show({
                        title: "Goods Issue",
                        content: "Penerima tidak boleh kosong",
                        type: 'danger'
                    });

                    setTimeout(function(){
                        $scope.checkRefGINo = false;
                    },1000);

                    return;
                }

                if ($scope.mData.ReceivedBy.length > 50) {
                    bsNotify.show({
                        title: "Goods Issue",
                        content: "Nama Penerima Tidak Boleh Lebih Dari 50 Karakter",
                        type: 'danger'
                    });

                    setTimeout(function(){
                        $scope.checkRefGINo = false;
                    },1000);
                    return;
                }

              


                // else if((typeof $scope.mData.MaterialRequestId === undefined || $scope.mData.MaterialRequestId == null) && ($scope.mData.RefGITypeID == '1' || $scope.mData.RefGITypeID == '2'))
                // {
                //   bsNotify.show(
                //       {
                //          title: "Goods Issue",
                //         content: "No. Material Request tidak boleh kosong",
                //         type: 'danger'
                //       }
                //       );
                // }
                else if ($scope.mData.GridDetail.length <= 0) {
                    bsNotify.show({
                        title: "Goods Issue",
                        content: "Detail tidak boleh kosong",
                        type: 'danger'    
                    });
                    setTimeout(function(){
                        $scope.checkRefGINo = false;
                    },1000);
                } else {
                    //Cek PARTS DI STOK OPNAME. Apakah Freeze atau Tidak. If Freeze gk bole Simpan
                    var objParts = {};
                    var dataGridParts = angular.copy($scope.mData.GridDetail);
                    console.log("Data to dataGridParts ==>", dataGridParts);
                    objParts.PartsId = _.map(dataGridParts, 'PartId');
                    // GoodsIssue.CekPartsStockOpname().then(function(resu) {
                    //     if (resu.data == 1) {
                    console.log("Data to Backend ==>", $scope.crudState);
                    // $scope.mData = mWO;
                    console.log($scope.mData);
                    // added by sss on 2018-01-05
                    var countValidGi = 0;
                    var notValidQtyOnHand = 0;
                    var notValidPartName = "";
                    for (var i in $scope.mData.GridDetail) {
                        // console.log ("frezee",$scope.mData.GridDetail[i]);
                        // if ($scope.mData.GridDetail[i].QtyGI == 'Freeze') {
                        //     bsNotify.show(
                        //         {
                        //             title: "FREEZE",
                        //             content: "Parts Sedang distock opname. Parts tidak dapat di keluarkan.",
                        //             type: 'danger'
                        //         }
                        //         );
                        //     // var index = $scope.mData.GridDetail.indexOf($scope.mData.GridDetail[i]); //$scope.crudState
                        //     // $scope.mData.GridDetail.splice(index, 1);
                        // };
                        console.log('$scope.crudState',$scope.crudState);

                        if ($scope.crudState === '11') {
                            if ($scope.mData.GridDetail[i].QtyGI <= 0 || $scope.mData.GridDetail[i].QtyGI >= 0
                                || $scope.mData.GridDetail[i].QtyRemain <= 0 ||
                                // $scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyReserved ||
                                $scope.mData.GridDetail[i].QtyGI >= $scope.mData.GridDetail[i].QtySO
                                || $scope.mData.GridDetail[i].QtyGI <= $scope.mData.GridDetail[i].QtySO) { // edit beberapa logic yg membuat rancu pada saat mau save dan cetak.
                                    if ($scope.mData.GridDetail[i].QtyReserved <= 0 || $scope.mData.GridDetail[i].QtyReserved == null || $scope.mData.GridDetail[i].QtyReserved == undefined){
                                        if ($scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyFree || $scope.mData.GridDetail[i].QtyGI != $scope.mData.GridDetail[i].QtyReserved){
                                            ValidGI = false;
                                            // break;
                                        }else{
                                            ValidGI = true;
                                            countValidGi++;
                                        }
                                    }else{
                                        if ($scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyReserved || $scope.mData.GridDetail[i].QtyGI != $scope.mData.GridDetail[i].QtyReserved){
                                            ValidGI = false;
                                            // break;
                                        }else{
                                            ValidGI = true;
                                            countValidGi++;
                                        }
                                    }


                                console.log("masuk pak eko");
                            };
                        } else if ($scope.crudState == 10) {
                            console.log('masuk $scope.crudState 10');
                            if ($scope.mData.GridDetail[i].QtyGI <= 0 || typeof $scope.mData.GridDetail[i].QtyGI === 'undefined' || _.isNull($scope.mData.GridDetail[i].QtyGI) ||
                                $scope.mData.GridDetail[i].QtyRemain <= 0 || $scope.mData.GridDetail[i].QtyGI >= 0) {
                                //$scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyReserved) { // || $scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyTransfer
                                console.log("QtyGI to mData ==>", i, $scope.mData.GridDetail[i].QtyGI);
                                console.log("QtyReserved to mData ==>", i, $scope.mData.GridDetail[i].QtyReserved);
                                console.log("isEmpty to mData ==>", i, typeof $scope.mData.GridDetail[i].QtyGI === 'undefined');
                                console.log("isNull to mData ==>", i, _.isNull($scope.mData.GridDetail[i].QtyGI));
                                console.log("Data to mData ==>", i, $scope.mData.GridDetail[i]);
                                

                                if ($scope.mData.GridDetail[i].QtyGI >= $scope.mData.GridDetail[i].QtyFree || $scope.mData.GridDetail[i].QtyGI <= $scope.mData.GridDetail[i].QtyFree) { // edit beberapa logic yg membuat rancu pada saat mau save dan cetak.
                                    if ($scope.mData.GridDetail[i].QtyReserved <= 0 || $scope.mData.GridDetail[i].QtyReserved == null || $scope.mData.GridDetail[i].QtyReserved == undefined){
                                        if ($scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyFree || $scope.mData.GridDetail[i].QtyGI != $scope.mData.GridDetail[i].QtyReserved){ //menambahkan validasi qty GI = qty reserved agar tidak bisa lebih dari dan kurang dari
                                            console.log("masuk Sini utami1");
                                            ValidGI = false;
                                            // break;
                                        } else {
                                            console.log("masuk Sini utami2");
                                            ValidGI = true;
                                            countValidGi++;
                                        }
                                    } else {
                                        if ($scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyReserved || $scope.mData.GridDetail[i].QtyGI != $scope.mData.GridDetail[i].QtyReserved){
                                            console.log("masuk Sini utami3");
                                            ValidGI = false;
                                            // break;
                                        } else {
                                            console.log("masuk Sini utami4");
                                            ValidGI = true;
                                            countValidGi++;
                                        }
                                    }

                                }
                                else {
                                    ValidGI = false;
                                    // break;
                                }
                                console.log("masuk pak eko2");
                            }
                        } else if ($scope.crudState == 12) {
                            console.log('masuk $scope.crudState 12');
                            if ($scope.mData.GridDetail[i].QtyGI <= 0 || typeof $scope.mData.GridDetail[i].QtyGI === 'undefined' || _.isNull($scope.mData.GridDetail[i].QtyGI) ||
                                $scope.mData.GridDetail[i].QtyRemain <= 0 || $scope.mData.GridDetail[i].QtyGI >= 0) {
                                //$scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyReserved) { // || $scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyTransfer
                                console.log("QtyGI to mData ==>", i, $scope.mData.GridDetail[i].QtyGI);
                                console.log("QtyReserved to mData ==>", i, $scope.mData.GridDetail[i].QtyReserved);
                                console.log("isEmpty to mData ==>", i, typeof $scope.mData.GridDetail[i].QtyGI === 'undefined');
                                console.log("isNull to mData ==>", i, _.isNull($scope.mData.GridDetail[i].QtyGI));
                                console.log("Data to mData ==>", i, $scope.mData.GridDetail[i]);
                                

                                if ($scope.mData.GridDetail[i].QtyGI >= $scope.mData.GridDetail[i].QtyFree || $scope.mData.GridDetail[i].QtyGI <= $scope.mData.GridDetail[i].QtyFree) { // edit beberapa logic yg membuat rancu pada saat mau save dan cetak.
                                    if ($scope.mData.GridDetail[i].QtyReserved <= 0 || $scope.mData.GridDetail[i].QtyReserved == null || $scope.mData.GridDetail[i].QtyReserved == undefined){
                                        if ($scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyFree || $scope.mData.GridDetail[i].QtyGI != $scope.mData.GridDetail[i].QtyReserved){ //menambahkan validasi qty GI = qty reserved agar tidak bisa lebih dari dan kurang dari
                                            console.log("masuk Sini utami1");
                                            ValidGI = false;
                                            // break;
                                        } else {
                                            console.log("masuk Sini utami2");
                                            ValidGI = true;
                                            countValidGi++;
                                        }
                                    } else {
                                        if ($scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyReserved || $scope.mData.GridDetail[i].QtyGI != $scope.mData.GridDetail[i].QtyReserved){
                                            console.log("masuk Sini utami3");
                                            ValidGI = false;
                                            // break;
                                        } else {
                                            console.log("masuk Sini utami4");
                                            ValidGI = true;
                                            countValidGi++;
                                        }
                                    }

                                }
                                else {
                                    ValidGI = false;
                                    // break;
                                }
                                console.log("masuk pak eko2");
                            }
                        } else if ($scope.crudState == 13) {
                            console.log('masuk $scope.crudState 13');
                            if ($scope.mData.GridDetail[i].QtyGI <= 0 || typeof $scope.mData.GridDetail[i].QtyGI === 'undefined' || _.isNull($scope.mData.GridDetail[i].QtyGI) ||
                                $scope.mData.GridDetail[i].QtyInvoice <= 0 || $scope.mData.GridDetail[i].QtyGI >= 0) {
                                //$scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyReserved) { // || $scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyTransfer
                                console.log("QtyGI to mData ==>", i, $scope.mData.GridDetail[i].QtyGI);
                                console.log("QtyReserved to mData ==>", i, $scope.mData.GridDetail[i].QtyInvoice);
                                console.log("isEmpty to mData ==>", i, typeof $scope.mData.GridDetail[i].QtyGI === 'undefined');
                                console.log("isNull to mData ==>", i, _.isNull($scope.mData.GridDetail[i].QtyGI));
                                console.log("Data to mData ==>", i, $scope.mData.GridDetail[i]);
                                

                                if ($scope.mData.GridDetail[i].QtyGI >= $scope.mData.GridDetail[i].QtyInvoice || $scope.mData.GridDetail[i].QtyGI <= $scope.mData.GridDetail[i].QtyInvoice) { // edit beberapa logic yg membuat rancu pada saat mau save dan cetak.
                                    if ($scope.mData.GridDetail[i].QtyInvoice <= 0 || $scope.mData.GridDetail[i].QtyInvoice == null || $scope.mData.GridDetail[i].QtyInvoice == undefined){
                                        if ($scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyInvoice || $scope.mData.GridDetail[i].QtyGI != $scope.mData.GridDetail[i].QtyInvoice){ //menambahkan validasi qty GI = qty reserved agar tidak bisa lebih dari dan kurang dari
                                            console.log("masuk Sini utami1");
                                            ValidGI = false;
                                            // break;
                                        } else {
                                            console.log("masuk Sini utami2");
                                            ValidGI = true;
                                            countValidGi++;
                                        }
                                    } else {
                                        if ($scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyInvoice || $scope.mData.GridDetail[i].QtyGI != $scope.mData.GridDetail[i].QtyInvoice){
                                            console.log("masuk Sini utami3");
                                            ValidGI = false;
                                            // break;
                                        } else {
                                            console.log("masuk Sini utami4");
                                            ValidGI = true;
                                            countValidGi++;
                                        }
                                    }

                                }
                                else {
                                    ValidGI = false;
                                    // break;
                                }
                                console.log("masuk pak eko2");
                            }
                        } else if ($scope.crudState == 14) {
                            console.log('masuk $scope.crudState 14');
                            if ($scope.mData.GridDetail[i].QtyGI <= 0 || typeof $scope.mData.GridDetail[i].QtyGI === 'undefined' || _.isNull($scope.mData.GridDetail[i].QtyGI) ||
                                $scope.mData.GridDetail[i].QtyReceived <= 0 || $scope.mData.GridDetail[i].QtyGI >= 0) {
                                //$scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyReserved) { // || $scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyTransfer
                                console.log("QtyGI to mData ==>", i, $scope.mData.GridDetail[i].QtyGI);
                                console.log("QtyReserved to mData ==>", i, $scope.mData.GridDetail[i].QtyReceived);
                                console.log("isEmpty to mData ==>", i, typeof $scope.mData.GridDetail[i].QtyGI === 'undefined');
                                console.log("isNull to mData ==>", i, _.isNull($scope.mData.GridDetail[i].QtyGI));
                                console.log("Data to mData ==>", i, $scope.mData.GridDetail[i]);
                                

                                if ($scope.mData.GridDetail[i].QtyGI >= $scope.mData.GridDetail[i].QtyRPP || $scope.mData.GridDetail[i].QtyGI <= $scope.mData.GridDetail[i].QtyRPP) { // edit beberapa logic yg membuat rancu pada saat mau save dan cetak.
                                    if ($scope.mData.GridDetail[i].QtyReceived <= 0 || $scope.mData.GridDetail[i].QtyReceived == null || $scope.mData.GridDetail[i].QtyReceived == undefined){
                                        if ($scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyRPP || $scope.mData.GridDetail[i].QtyGI != $scope.mData.GridDetail[i].QtyReceived){ //menambahkan validasi qty GI = qty reserved agar tidak bisa lebih dari dan kurang dari
                                            console.log("masuk Sini utami1");
                                            ValidGI = false;
                                            // break;
                                        } else {
                                            console.log("masuk Sini utami2");
                                            ValidGI = true;
                                            countValidGi++;
                                        }
                                    } else {
                                        if ($scope.mData.GridDetail[i].QtyGI > $scope.mData.GridDetail[i].QtyReceived || $scope.mData.GridDetail[i].QtyGI != $scope.mData.GridDetail[i].QtyReceived){
                                            console.log("masuk Sini utami3");
                                            ValidGI = false;
                                            // break;
                                        } else {
                                            console.log("masuk Sini utami4");
                                            ValidGI = true;
                                            countValidGi++;
                                        }
                                    }

                                }
                                else {
                                    ValidGI = false;
                                    // break;
                                }
                                console.log("masuk pak eko2");
                            }
                        }

                        //Cek status QtyOnHand
                        console.log("$scope.mData.GridDetail=>",$scope.mData.GridDetail);
                        console.log("$scope.mData.GridDetail[i].PartsId=>",$scope.mData.GridDetail[i].PartId);
                        console.log("ValidGI=>",ValidGI);
                        // setTimeout(function(){
                       

                    };


                    
                    var cekStockGi = function (data, x){
                        console.log("sini sih=>",data.length, x)
                        if(data.length <= x){
                            $scope.finishingSave($scope.mData,ValidGI,countValidGi,notValidQtyOnHand,data);
                            return false;
                        } else {
                            console.log("sini sini =>",data[x].PartId)
                            GoodsIssue.getStatusStockGI(data[x].PartId,data[x].WarehouseId).then(function(res) {
                                var resData = res.data.Result;
                                console.log("res=>",res);
                                console.log("res.data.Result.length=>",res.data.Result.length);
                                console.log("$scope.IsOPB=>",$scope.IsOPB);
                                if (res.data.Result.length>0 && $scope.IsOPB!='OPB') {
                                    console.log("res.data.Result[0].QtyOnHand=>",res.data.Result[0].QtyOnHand);
                                    console.log("resData=>",resData[0]);
                                    if ((res.data.Result[0].QtyOnHand <  data[x].QtyGI)) {
                                        notValidQtyOnHand++;
                                        notValidPartName += $scope.mData.GridDetail[i].PartsCode + ' ' + $scope.mData.GridDetail[i].PartsName;
                                        ValidGI = false;
                                        console.log("notValidQtyOnHand=>",notValidQtyOnHand);
                                        console.log("masuk sini, QtyOnHand kurang!!");
                                        
                                    }
                                }
                                if ($scope.mData.GridDetail[i].StockOpnameStatusId == 2 || $scope.mData.GridDetail[i].StockOpnameStatusId == 1){
                                    //ExistsItemFreeze = true;
                                    //$scope.mData.GridDetail[i].QtyGI == 'Freeze'
                                    console.log("$scope.mData riu: ", $scope.mData);
                                    $scope.gridSO.columnDefs[7].editableCellTemplate = false;
                                        bsNotify.show(
                                        {
                                            title: "FREEZE",
                                            content: "Parts Sedang distock opname. Parts tidak dapat di keluarkan.",
                                            type: 'danger'
                                        }
                                        );
                                        setTimeout(function(){
                                            $scope.checkRefGINo = false;
                                        },1000);
                                    //ValidGI = false;
                                } else {
        
                                    if($scope.mData.MaterialTypeId == null ||$scope.mData.MaterialTypeId == undefined || $scope.mData.MaterialTypeId == 0){
                                        $scope.mData.MaterialTypeId = $scope.mData.GridDetail[0].MaterialTypeId;
                                    }
                                }
                                cekStockGi($scope.mData.GridDetail,x+1);
                            });

                        }
                        

                    }
                    cekStockGi($scope.mData.GridDetail,0);
                }
            }
            // [END] Button [WO] Simpan & Cetak Bukti Pengambilan Material
            $scope.finishingSave = function(data,ValidGI,countValidGi,notValidQtyOnHand,dataGridParts,ExistsItemFreeze){

                setTimeout(function(){
                    console.log("nyoba sini=>",countValidGi);
                    console.log("dataGridParts.length=>",dataGridParts.length);
                    console.log("ValidGI=>",ValidGI);
                    console.log("notValidQtyOnHand=>",notValidQtyOnHand);
                    if (ValidGI == false || countValidGi == 0 || (countValidGi < dataGridParts.length)) {
                        bsNotify.show({
                            title: "Goods Issue",
                            content: "Quantity GI tidak valid",
                            type: 'danger'
                        });
                        setTimeout(function(){
                            $scope.checkRefGINo = false;
                        },1000);
                        return 0;
                    }
                    if (notValidQtyOnHand > 0) {
                        bsNotify.show({
                            title: "Goods Issue",
                            // content: notValidPartName + " QtyOnHand tidak mencukupi",
                            content: "Quantity GI tidak valid",
                            type: 'danger'
                        });
                        setTimeout(function(){
                            $scope.checkRefGINo = false;
                        },1000);
                        return 0;
                    }
                    if (ExistsItemFreeze) {
                        bsNotify.show({
                            title: "Goods Issue",
                            content: "Terdapat Item yg sedang difreeze, (item itu) tidak disave",
                            type: 'danger'
                        });
                        setTimeout(function(){
                            $scope.checkRefGINo = false;
                        },1000);
                        // return 0;
                    }

                    console.log("$scope.mData 2018-01-21 : ", $scope.mData);
                    PartsGlobal.isGIDone($scope.mData).then(function(res) {
                        var isGIDone = res.data.Result;
                        console.log("isGIDone : ", isGIDone);
                        if (isGIDone[0].GIAlreadyDone == 1) { // GIAlreadyDone
                            bsNotify.show({
                                title: "Goods Issue",
                                content: "GI untuk referensi ini sudah selesai dilakukan",
                                type: 'danger'
                            });
                            setTimeout(function(){
                                $scope.checkRefGINo = false;
                            },1000);
                            return 0;
                        }
                        // if ($scope.mData.GridDetail[i].StockOpnameStatusId == 2){
                        //     ExistsItemFreeze = true;
                        //     ValidGI = false;
                        // }
                        console.log("$scope.mData.GridDetail ri: ", $scope.mData.GridDetail);
                        console.log("$scope.mData ri: ", $scope.mData);
             

                        GoodsIssue.create($scope.mData, $scope.mData.GridDetail).then(function(res) {
                                var insertResponse = res.data;
                                //console.log(res.data);
                                var tmpGoodIssueId = res.data.ResponseMessage;
                                console.log("resu Response", res.data);
                                tmpGoodIssueId = tmpGoodIssueId.split('#');
                                console.log("mdataGoodissue", $scope.mData);
                                $scope.dataGI = tmpGoodIssueId[1];

                                if (insertResponse.ResponseCode == 44) {
                                    bsNotify.show({
                                        title: "Goods Issue",
                                        content: "Gagal Simpan Goods Issue, Refresh Data Terlebih Dahulu ",
                                        type: 'danger'
                                    });

                                    setTimeout(function(){
                                        $scope.checkRefGINo = false;
                                    },1000);
                                    return -1;
                                }

                                if (insertResponse.ResponseCode == 100001) {
                                    // bsNotify.show({
                                    //     title: "Goods Issue",
                                    //     content: tmpGoodIssueId[1],
                                    //     type: 'danger'
                                    // });
                                    bsAlert.alert({
                                        title: "Goods Issue",
                                        text: tmpGoodIssueId[1],
                                        type: "error",
                                        showCancelButton: false
                                    });

                                    setTimeout(function(){
                                        $scope.checkRefGINo = false;
                                    },1000);
                                    return -1;
                                }

                                if (insertResponse.ResponseCode !== 13) {
                                    bsNotify.show({
                                        title: "Goods Issue",
                                        content: "Data gagal disimpan, silakan hubungi administrator",
                                        type: 'danger'
                                    });

                                    setTimeout(function(){
                                        $scope.checkRefGINo = false;
                                    },1000);
                                    return -1;
                                }

                                if(insertResponse.ResponseCode == 13 && ValidGI != false && countValidGi == dataGridParts.length){
                                    bsNotify.show({
                                        title: "Goods Issue",
                                        content: "Data berhasil disimpan",
                                        type: 'success'
                                    });
                                    setTimeout(function(){
                                        $scope.checkRefGINo = false;
                                    },1000);
                                    $scope.printGIKeluarMaterial($scope.dataGI);
    
                                    $scope.goBack();
                                }
                                
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    
                    });
                    // } else {
                    //     bsNotify.show({
                    //         title: "Goods Issue",
                    //         content: "Tidak BIsa Simpan, Parts Sedang dalam Proses Stock Opname",
                    //         type: 'warning'
                    //     });
                    // };
                    // });

                    // GoodsIssue.create($scope.mData, $scope.mData.GridDetail).then(function(res){
                    //   var insertResponse = res.data;
                    //   //console.log(res.data);

                    //   if(insertResponse.GoodsIssueId != null)
                    //   {
                    //     bsNotify.show(
                    //     {
                    //       title: "Goods Issue",
                    //       content: "Data berhasil disimpan",
                    //       type: 'success'
                    //     }
                    //     );
                    //     $scope.printGIKeluarMaterial(insertResponse.GoodsIssueId);
                    //   }
                    //   else
                    //   {
                    //     bsNotify.show(
                    //     {
                    //       title: "Goods Issue",
                    //       content: "Data gagal disimpan, silakan hubungi administrator",
                    //       type: 'danger'
                    //     }
                    //     );
                    //   }

                    //   $scope.goBack();
                    // },
                    // function(err) {
                    //   console.log("err=>", err);
                    //   }
                    // );
                    // [END] Get Current User Warehouse
                    },5000);
                
            } 
        // Button kembali wo_edit.html
        $scope.kembali = function(mWo) {
            console.log(mWo);
        }

        // [START] Button Batal GI di GRID
        $scope.batalGI = function(row) {

                if (row.GoodsIssueStatusId == 1) {
                    $scope.stat = 1;
                } else {
                    $scope.stat = 0;
                }

                // Masukan data ke detail
                $scope.bsForm = false;
                $scope.mData = row;
                GoodsIssue.setGoodsIssueHeader(row);
                console.log($scope.mData);
                var editnum = '3';

                GoodsIssue.getDetail(row.GoodsIssueId, row.RefGINo, row.RefGITypeID).then(function(res) {
                        var gridData = res.data;
                        //console.log("dalam getDetail");
                        //console.log(gridData);
                        switch (row.RefGITypeID) {
                            case 1:
                                $scope.gridOptionsView.data = gridData;
                                break;
                            case 2:
                                $scope.gridSOView.data = gridData;
                                break;
                            case 3:
                                $scope.gridTOView.data = gridData;
                                break;
                            case 4:
                                $scope.gridClaimView.data = gridData;
                                break;
                            case 5:
                                //masukkan ke dalam model
                                $scope.mData.VendorName = gridData[0].VendorName;
                                $scope.mData.NoPol = gridData[0].NoPol;
                                $scope.mData.AppointmentNo = gridData[0].AppointmentNo;
                                $scope.gridRPPView.data = gridData;
                                break;
                        }
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );


                //-----------------------------------------------------------------
                // Back end Mode
                $scope.crudState = editnum + $scope.formShowHandler(row.RefGITypeID);

                console.log("Cek crudState >>", $scope.crudState, "rowTypeId >> ", row.RefGITypeID);

            }
            // [END] Button Batal GI di GRID

        //kembali pada List Batal GI
        $scope.KembaliBatalGI = function() {
                $scope.bsForm = true;
                $scope.crudState = '';
                console.log($scope.crudState);

                $scope.mData = {};
            }
            //kembali pada form Batal GI
        $scope.batalGiOnForm = function(data) {

            console.log("Status GI saat ini " + data);
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.ToNameRefGi = function(RefGITypeID) {
            var GiName;
            switch (RefGITypeID) {
                case 1:
                case "1":
                    GiName = "WO";
                    break;
                case 2:
                case "2":
                    GiName = "Sales Order";
                    break;
                case 3:
                case "3":
                    GiName = "Transfer Order";
                    break;
                case 4:
                case "4":
                    GiName = "Return - Parts Claim";
                    break;
                case 5:
                case "5":
                    GiName = "Return - RPP";
                    break;
            }
            return GiName;
        }

        $scope.Tas = function() {
            return 'berhasil';
        }

        $scope.ToNameStatus = function(GoodsIssueStatusId) {
            var statusName;
            switch (GoodsIssueStatusId) {
                case 0:
                    statusName = "Completed";
                    break;
                case 1:
                    statusName = "Cancelled";
                    break;
            }
            return statusName;
        }

        $scope.ToNameMaterial = function(MaterialTypeId) {
            var materialName;
            switch (MaterialTypeId) {
                case 1:
                    materialName = "Parts";
                    break;
                case 2:
                    materialName = "Bahan";
                    break;
            }
            return materialName;
        }

        $scope.Delete = function(row) {

            if ($scope.crudState === '10') {
                console.log("WO");
                console.log('row', row);
                var index = $scope.gridOptions.data.indexOf(row);
                console.log('idx', index);
                $scope.gridOptions.data.splice(index, 1);
                console.log($scope.gridOptions.data);

            } else if ($scope.crudState === '11') {
                console.log("SO");
                var index = $scope.gridSO.data.indexOf(row.entity);
                $scope.gridSO.data.splice(index, 1);
                console.log($scope.gridSO.data);
            } else if ($scope.crudState === '12') {
                console.log("TO");
                var index = $scope.gridTO.data.indexOf(row.entity);
                $scope.gridTO.data.splice(index, 1);
                console.log($scope.gridTO.data);
            } else if ($scope.crudState === '13') {
                console.log("Claim");
                var index = $scope.gridClaim.data.indexOf(row.entity);
                $scope.gridClaim.data.splice(index, 1);
                console.log($scope.gridClaim.data);
            } else if ($scope.crudState === '14') {
                console.log("RPP");
                var index = $scope.gridRPP.data.indexOf(row.entity);
                $scope.gridRPP.data.splice(index, 1);
                console.log($scope.gridRPP.data);
            }
            console.log(index);
        };

        $scope.showMe = function(r) {
            var result = confirm('Apakah Anda yakin menghapus data ini?');
            if (result) {
                console.log($scope.gridApi.selection.getSelectedRows());
                angular.forEach($scope.gridApi.selection.getSelectedRows(), function(data, index) {
                    console.log("HELLO");
                    if ($scope.crudState === '10') {
                        console.log("WO");
                        $scope.gridOptions.data.splice($scope.gridOptions.data.lastIndexOf(data), 1);
                    } else if ($scope.crudState === '11') {
                        console.log("SO");
                        $scope.gridSO.data.splice($scope.gridSO.data.lastIndexOf(data), 1);
                    } else if ($scope.crudState === '12') {
                        console.log("TO");
                        $scope.gridTO.data.splice($scope.gridTO.data.lastIndexOf(data), 1);
                    } else if ($scope.crudState === '13') {
                        console.log("PartsClaim");
                        $scope.gridClaim.data.splice($scope.gridClaim.data.lastIndexOf(data), 1);
                    } else if ($scope.crudState === '14') {
                        console.log("RPP");
                        $scope.gridRPP.data.splice($scope.gridRPP.data.lastIndexOf(data), 1);
                    }
                });
            }

        };

        $scope.Add = function() {
            if ($scope.crudState === '10') {
                console.log("WO");
                var n = $scope.gridOptions.data.length + 1;
                $scope.gridOptions.data.push({
                    "NoMaterial": "",
                    "PartsName": "",
                    "QtyAdjustment": "",
                    "UomId": ""
                });
            } else if ($scope.crudState === '11') {
                console.log("SO");
                var n = $scope.gridSO.data.length + 1;
                $scope.gridSO.data.push({
                    "PartsCode": "",
                    "PartsName": "",
                    "QtySO": "",
                    "QtySO": "",
                    "QtySO": "",
                    "QtySO": "",
                    "QtySO": "",
                    "WarehouseName": "",
                    "ShelfCode": ""
                });
            } else if ($scope.crudState === '12') {
                console.log("SO");
                var n = $scope.gridTO.data.length + 1;
                $scope.gridTO.data.push({
                    "PartsCode": "",
                    "PartsName": "",
                    "QtyTransfer": "",
                    "QtyTransfer": "",
                    "QtyTransfer": "",
                    "QtyTransfer": "",
                    "QtyTransfer": "",
                    "WarehouseName": "",
                    "ShelfCode": ""
                });
            } else if ($scope.crudState === '13') {
                console.log("SO");
                var n = $scope.gridClaim.data.length + 1;
                $scope.gridClaim.data.push({
                    "PartsCode": "",
                    "PartsName": "",
                    "QtyInvoice": "",
                    "QtyInvoice": "",
                    "QtyInvoice": "",
                    "WarehouseName": "",
                    "ShelfCode": ""
                });
            } else if ($scope.crudState === '14') {
                console.log("SO");
                var n = $scope.gridRPP.data.length + 1;
                $scope.gridRPP.data.push({
                    "PartsCode": "",
                    "PartsName": "",
                    "QtyReceived": "",
                    "QtyRPP": "",
                    "QtyGI": "",
                    "WarehouseName": "",
                    "ShelfCode": ""
                });
            }
        }

        $scope.ToDate = function(data) {
            var endDate = $filter('date')(data, 'dd/MM/yyyy');
            return endDate
        }

        $scope.SearchMaterial = function(NoMaterial) {
            GoodsIssue.getDetailByMaterialNo(NoMaterial).then(function(res) {
                    var gridData = res.data.Result;
                    console.log(gridData[0]);
                    if ($scope.crudState === '10') {
                        $scope.gridOptions.data[$scope.gridOptions.data.length - 1] = gridData[0];
                        $scope.mData.GridDetail = $scope.gridOptions.data;
                    } else if ($scope.crudState === '11') {
                        $scope.gridSO.data[$scope.gridSO.data.length - 1] = gridData[0];
                        $scope.mData.GridDetail = $scope.gridSO.data;
                    } else if ($scope.crudState === '12') {
                        $scope.gridTO.data[$scope.gridTO.data.length - 1] = gridData[0];
                        $scope.mData.GridDetail = $scope.gridTO.data;
                    } else if ($scope.crudState === '13') {
                        $scope.gridClaim.data[$scope.gridClaim.data.length - 1] = gridData[0];
                        $scope.mData.GridDetail = $scope.gridClaim.data;
                    } else if ($scope.crudState === '14') {
                        $scope.gridRPP.data[$scope.gridRPP.data.length - 1] = gridData[0];
                        $scope.mData.GridDetail = $scope.gridRPP.data;
                    }

                },
                function(err) {
                    console.log("err=>", err);
                    confirm("Nomor Material Tidak Ditemukan.");
                }
            );
        }

        $scope.getTableHeightgridWOPart = function() {
            var rowHeight = 30; // your row height
            var headerHeight = 50; // your header height
            //var filterHeight = 40; // your filter height
            var pageSizegridWOPart =  $scope.gridOptions.paginationPageSize;

            // if ( $scope.gridOptions.paginationPageSize >  $scope.gridOptions.data.length) {
            //     pageSizegridWOPart =  $scope.gridOptions.data.length;
            // }
            if (pageSizegridWOPart < 20) {
                pageSizegridWOPart =  20;
            }

            return {
                height: (pageSizegridWOPart * rowHeight + headerHeight) + 50 + "px"
            };

        };


        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,

            columnDefs: [{
                    name: 'Id',
                    field: 'GoodsIssueId',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'Tipe Material Id',
                    displayName: 'Tipe Material',
                    field: 'MaterialTypeId',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'Tipe Material',
                    displayName: 'Tipe Material',
                    field: 'tipeMaterial',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameMaterial(row.entity.MaterialTypeId)}}</div>',
                    width: '7%'
                },
                {
                    name: 'Referensi GI ID',
                    displayName: 'Referensi GI ID',
                    field: 'RefGITypeID',
                    width: '5%',
                    visible: false
                },
                {
                    name: 'Referensi GI',
                    displayName: 'Referensi GI',
                    enableCellEdit: true,
                    field: 'refGIType',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameRefGi(row.entity.RefGITypeID)}}</div>',
                    width: '7%'
                },
                {
                    name: 'No. GI',
                    displayName: 'No. GI',
                    field: 'GoodsIssueNo',
                    width: '14%'
                },
                {
                    name: 'Tanggal GI',
                    displayName: 'Tanggal GI',
                    field: 'DocDate',
                    cellFilter: 'date:\'dd/MM/yyyy\'',

                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToDate(row.entity.DocDate)}}</div>',
                    width: '7%'
                },
                {
                    name: 'No. Referensi',
                    displayName: 'No. Referensi',
                    field: 'RefGINo',
                    width: '14%'
                },
                {
                    name: 'No. Polisi',
                    displayName: 'No. Polisi',
                    field: 'NoPol', //data ini belum ada
                    width: '7%'
                },
                {
                    name: 'Customer',
                    displayName: 'Customer',
                    field: 'CustomerName', //data ini belum ada
                    width: '10%'
                },
                {
                    name: 'Bengkel',
                    displayName: 'Bengkel',
                    field: 'VendorName',
                    width: '7%'
                },
                // {
                //     name: 'batalGI',
                //     displayName: 'Batal GI',
                //     //cellTemplate: '<div class="ui-grid-cell-contents"><button class="zbtn wbtn" ng-click="grid.appScope.$parent.batalGI(row.entity)"><i class="fa fa-edit"></i></button></div>',
                //     //cellTemplate: '<div class="ui-grid-cell-contents  change-color" title="Cancel" ng-hide="(row.entity.RefGITypeID == 2 && row.entity.SalesBillingStatusId == 1) || (row.entity.RefGITypeID == 1 && (row.entity.StatusWO >= 14 && row.entity.StatusWO !== 22) ) || row.entity.GoodsIssueStatusId == 1 " ng-click="grid.appScope.$parent.batalGI(row.entity)"><i class="fa fa-edit"></i></div>',
                //     width: '7%'
                // },
                {
                    name: 'StatusId',
                    displayName: 'Status',
                    field: 'GoodsIssueStatusId',
                    width: '10%',
                    visible: false
                },
                {
                    name: 'Status',
                    displayName: 'Status',
                    field: "status",
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatus(row.entity.GoodsIssueStatusId)}}</div>',
                    width: '7%'
                }
            ]
        };


        //----------------------------------
        // Dummy WO
        //----------------------------------


        // added by : yap
        // objective : cell GI itu tidak boleh di edit kalau qtyfree nya itu 0
        // var QtyGICellTemplate = '<div class="email" ng-edit-cell-if="isFocused && '
        // + 'row.entity.QtyFree != 0">'
        // + '<input ng-class="\'colt\' + col.index" ng-input="COL_FIELD" '
        // + 'ng-model="COL_FIELD" />'
        // + '</div>'
        // + '<div ng-edit-cell-if="isFocused">'
        // + '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>'
        // + '{{COL_FIELD}}</span></div>'
        // + '</div>'
        // + '</div>';

        $scope.gridOptions = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {

                    console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);
                    if (rowEntity.QtyGI <= 0 || rowEntity.QtyGI == "" ) {
                        bsNotify.show({
                            title: "Goods Issue",
                            content: "Quantity GI tidak valid.",
                            type: 'danger'
                        });
                        rowEntity.QtyGI = oldValue;
                        return
                    } else if (rowEntity.QtyFree + rowEntity.QtyReserved == 0 && rowEntity.QtyGI > 0) {
                        bsNotify.show({
                            title: "Goods Issue",
                            content: "Item tidak tersedia di gudang",
                            type: 'danger'
                        });
                        rowEntity.QtyGI = oldValue;
                        return
                    }

                    
                    //else {
                    //     // rowEntity.QtyRemain = rowEntity.QtySO - rowEntity.QtyGI;
                    //     rowEntity.QtyRemain = rowEntity.QtyReserved;
                    // }
                    // new adding code 28/09/2020
                    rowEntity.TotalCost = rowEntity.UnitCost * rowEntity.QtyGI;
                    rowEntity.TotalStandardCost = rowEntity.StandardCost * rowEntity.QtyGI;

                });
            },
            enableSelectAll: true,
            paginationPageSizes: [5, 10, 20, 25, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 25,
            cellEditableCondition: function($scope) {
                if ($scope.col.field == "QtyGI") {
                    //if ($scope.row.entity[$scope.col.field] == 'Freeze' || $scope.row.entity["QtyReserved"] < 1) { //Dikoment karena untuk PO juga bisa (PO Tidak ada reserved)
                    if ($scope.row.entity[$scope.col.field] == 'Freeze') { 
                        return false;
                    } else {
                        return true;
                    }
                }
            },
            columnDefs: [
                { name: 'JobId', field: 'JobId', visible: false },
                { name: 'Pekerjaan', field: 'TaskName', width: '25%' },
                // {
                //   field:'PartsCode',
                //   displayName:'No. Material',
                //   enableCellEdit:true,
                //   //field: 'NoMaterial',
                //   width: '20%',
                //   cellTemplate:'<div><div class="input-group">\
                //             <input type="text" ng-model="mData.RefGINo" ng-value="row.entity.PartsCode" required>\
                //             <label class="input-group-btn">\
                //                 <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
                //                     <i class="fa fa-search fa-1"></i>\
                //                 </span>\
                //             </label>\
                //         </div></div>'
                // },
                { name: 'No Material', field: 'PartsCode', width: '15%' },
                { name: 'Nama Material', cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.PartName}} {{row.entity.PartsName}}</div>', enableCellEdit: false, width: '15%' },
                { name: 'Qty Free', field: 'QtyFree', enableCellEdit: false, width: '7%' },
                { name: 'Qty Reserved', field: 'QtyReserved', enableCellEdit: false, width: '7%' },
                { name: 'Qty Request', field: 'QtyRequest', enableCellEdit: false , width: '7%'},
                { name: 'Qty Remain', field: 'QtyRemain', enableCellEdit: false, width: '7%' },
                // { displayName: 'Qty GI', name:'Qty GI', field: 'QtyGI' , //bbbbbbb
                //yap
                {
                    displayName: 'Qty GI',
                    name: 'Qty GI',
                    field: 'QtyGI',
                    width: '7%',
                    editableCellTemplate: '<input type="number" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                    headerCellClass: 'colorUiGrid'
                },
                { name: 'Lokasi Gudang', field: 'WarehouseName', enableCellEdit: false , width: '15%'},
                { name: 'Lokasi Rak', field: 'ShelfCode', enableCellEdit: false, width: '10%'},
                { name: 'Keterangan Pre Picking', field: 'PrePickingNote', width: '7%' },
                {
                    name: 'Action',
                    width: '7%',
                    cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row.entity)"><u>Hapus</u></a></center>',
                }
            ]
        };



        $scope.gridOptionsView = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            cellEditableCondition: function($scope) {
                if ($scope.col.field == "QtyGI") {
                    if ($scope.row.entity[$scope.col.field] == 'Freeze') {
                        return false;
                    } else {
                        return true;
                    }
                }
            },
            columnDefs: [
                { name: 'Pekerjaan', field: 'TaskName', width: '17%' },
                { name: 'No Material', field: 'PartsCode', width: '7%' },
                { name: 'Nama Material', field: 'PartName', width: '15%' },
                { name: 'Qty Free', field: 'QtyFree' },
                { name: 'Qty Reserved', field: 'QtyReserved' },
                { name: 'Qty Request', field: 'QtyRequest' },
                { name: 'Qty Remain', field: 'QtyRemain' },
                { displayName: 'Qty GI', name: 'Qty GI', field: 'QtyGI' },
                { name: 'Lokasi Gudang', field: 'WarehouseName', width: '10%'},
                { name: 'Lokasi Rak', field: 'ShelfCode' },
                { name: 'Keterangan Pre Picking', field: 'PrePickingNote' }
            ]
        };

        //----------------------------------
        // Dummy SO
        //----------------------------------
        $scope.gridSO = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {


                    console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);

                    if (rowEntity.QtyGI <= 0 || rowEntity.QtyGI == "") {
                        bsNotify.show({
                            title: "Goods Issue",
                            content: "Quantity GI tidak valid",
                            type: 'danger'
                        });
                        rowEntity.QtyGI = oldValue;
                        return
                    } else {
                        // rowEntity.QtyRemain = rowEntity.QtySO - rowEntity.QtyGI;
                    }
                    // new adding code 28/09/2020
                    rowEntity.TotalCost = rowEntity.UnitCost * rowEntity.QtyGI;
                    rowEntity.TotalStandardCost = rowEntity.StandardCost * rowEntity.QtyGI;
                });
            },
            cellEditableCondition: function($scope) {
                if ($scope.col.field == "QtyGI") {
                    if ($scope.row.entity[$scope.col.field] == 'Freeze') {
                        return false;
                    } else {
                        return true;
                    }
                }
            },
            enableSelectAll: true,
            paginationPageSizes: [5, 10, 20, 25, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 25,
            columnDefs: [
                { name: 'No Material', field: 'PartsCode' },
                { name: 'Nama Material', field: 'PartName', enableCellEdit: false, width: '15%' },
                { name: 'Qty Free', field: 'QtyFree', enableCellEdit: false },
                { name: 'Qty Reserved', field: 'QtyReserved', enableCellEdit: false },
                { displayName: 'Qty SO', name: 'Qty SO', field: 'QtySO', enableCellEdit: false },
                { name: 'Qty Remain', field: 'QtyRemain', enableCellEdit: false },
                {
                    displayName: 'Qty GI',
                    name: 'Qty GI',
                    field: 'QtyGI',
                    editableCellTemplate: '<input type="number" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">'
                },
                { name: 'Lokasi Gudang', field: 'WarehouseName', enableCellEdit: false },
                { name: 'Lokasi Rak', field: 'ShelfCode', enableCellEdit: false },
                {
                    name: 'Action',
                    cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row.entity)"><u>Hapus</u></a></center>',
                }
            ]
        };
        $scope.gridSOView = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            paginationPageSizes: [5, 10, 20, 25, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 25,
            cellEditableCondition: function($scope) {
                if ($scope.col.field == "QtyGI") {
                    if ($scope.row.entity[$scope.col.field] == 'Freeze') {
                        return false;
                    } else {
                        return true;
                    }
                }
            },
            columnDefs: [
                { name: 'No Material', field: 'PartsCode' },
                { name: 'Nama Material', field: 'PartName', enableCellEdit: false, width: '15%' },
                { name: 'Qty Free', field: 'QtyFree', enableCellEdit: false },
                { name: 'Qty Reserved', field: 'QtyReserved', enableCellEdit: false },
                { displayName: 'Qty SO', name: 'Qty SO', field: 'QtySO', enableCellEdit: false },
                { name: 'Qty Remain', field: 'QtyRemain', enableCellEdit: false },
                { displayName: 'Qty GI', name: 'Qty GI', field: 'QtyGI' },
                { name: 'Lokasi Gudang', field: 'WarehouseName' },
                { name: 'Lokasi Rak', field: 'ShelfCode' }
            ]
        };

        //----------------------------------
        // Dummy TO
        //----------------------------------
        $scope.gridTO = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            paginationPageSizes: [5, 10, 20, 25, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 25,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {


                    console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);

                    if (rowEntity.QtyGI <= 0 || rowEntity.QtyGI == "") {
                        bsNotify.show({
                            title: "Goods Issue",
                            content: "Quantity GI tidak valid",
                            type: 'danger'
                        });
                        rowEntity.QtyGI = oldValue;
                        return
                    } else {
                        // rowEntity.QtyRemain = rowEntity.QtySO - rowEntity.QtyGI;
                    }
                    // new adding code 28/09/2020
                    rowEntity.TotalCost = rowEntity.UnitCost * rowEntity.QtyGI;
                    rowEntity.TotalStandardCost = rowEntity.StandardCost * rowEntity.QtyGI;
                });
            },
            cellEditableCondition: function($scope) {
                if ($scope.col.field == "QtyGI") {
                    if ($scope.row.entity[$scope.col.field] == 'Freeze') {
                        return false;
                    } else {
                        return true;
                    }
                }
            },
            columnDefs: [
                { displayName: 'No Material', field: 'PartsCode' },
                { displayName: 'Nama Material', field: 'PartName', enableCellEdit: false },
                { displayName: 'Qty Free', field: 'QtyFree', enableCellEdit: false },
                { displayName: 'Qty Reserved', field: 'QtyReserved', enableCellEdit: false },
                { displayName: 'Qty TO', field: 'QtyTransfer', enableCellEdit: false },
                { displayName: 'Qty Remain', field: 'QtyRemain', enableCellEdit: false },
                {
                    displayName: 'Qty GI',
                    field: 'QtyGI',
                    editableCellTemplate: '<input type="number" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">'
                },
                { displayName: 'Lokasi Gudang', field: 'WarehouseName', enableCellEdit: false },
                { displayName: 'Lokasi Rak', field: 'ShelfCode', enableCellEdit: false },
                {
                    name: 'Action',
                    cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row.entity)"><u>Hapus</u></a></center>',
                }
            ]
        };

        //----------------------------------
        // Dummy TO
        //----------------------------------
        $scope.gridTOView = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            paginationPageSizes: [5, 10, 20, 25, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 25,
            cellEditableCondition: function($scope) {
                if ($scope.col.field == "QtyGI") {
                    if ($scope.row.entity[$scope.col.field] == 'Freeze') {
                        return false;
                    } else {
                        return true;
                    }
                }
            },
            columnDefs: [
                { displayName: 'No Material', field: 'PartsCode' },
                { displayName: 'Nama Material', field: 'PartName' },
                { displayName: 'Qty Free', field: 'QtyFree' },
                { displayName: 'Qty Reserved', field: 'QtyReserved' },
                { displayName: 'Qty TO', field: 'QtyTransfer' },
                { displayName: 'Qty Remain', field: 'QtyRemain' },
                { displayName: 'Qty GI', field: 'QtyGI' },
                { displayName: 'Lokasi Gudang', field: 'WarehouseName' },
                { displayName: 'Lokasi Rak', field: 'ShelfCode' },
            ]
        };

        //----------------------------------
        // Dummy PR
        //----------------------------------
        $scope.gridRPP = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            paginationPageSizes: [5, 10, 20, 25, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 25,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {


                    console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);

                    if (rowEntity.QtyGI <= 0 || rowEntity.QtyGI == "") {
                        bsNotify.show({
                            title: "Goods Issue",
                            content: "Quantity GI tidak valid",
                            type: 'danger'
                        });
                        rowEntity.QtyGI = oldValue;
                        return
                    } else {
                        // rowEntity.QtyRemain = rowEntity.QtySO - rowEntity.QtyGI;
                    }
                    // new adding code 28/09/2020
                    rowEntity.TotalCost = rowEntity.UnitCost * rowEntity.QtyGI;
                    rowEntity.TotalStandardCost = rowEntity.StandardCost * rowEntity.QtyGI;
                });
            },
            cellEditableCondition: function($scope) {
                if ($scope.col.field == "QtyGI") {
                    if ($scope.row.entity[$scope.col.field] == 'Freeze') {
                        return false;
                    } else {
                        return true;
                    }
                }
            },
            columnDefs: [
                { displayName: 'No Material', field: 'PartsCode' },
                { displayName: 'Nama Material', field: 'PartName', enableCellEdit: false },
                { displayName: 'Qty RPP', field: 'QtyRPP', enableCellEdit: false },
                { displayName: 'Qty Remain', field: 'QtyReceived', enableCellEdit: false },
                {
                    displayName: 'Qty GI',
                    field: 'QtyGI',
                    editableCellTemplate: '<input type="number" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">'
                },
                { displayName: 'Lokasi Gudang', field: 'WarehouseName', enableCellEdit: false },
                { displayName: 'Lokasi Rak', field: 'ShelfCode', enableCellEdit: false },
                {
                    name: 'Action',
                    cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row.entity)"><u>Hapus</u></a></center>',
                    visible: true
                }
            ]
        };
        $scope.gridRPPView = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            paginationPageSizes: [5, 10, 20, 25, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 25,
            cellEditableCondition: function($scope) {
                if ($scope.col.field == "QtyGI") {
                    if ($scope.row.entity[$scope.col.field] == 'Freeze') {
                        return false;
                    } else {
                        return true;
                    }
                }
            },
            columnDefs: [
                { displayName: 'No Material', field: 'PartsCode' },
                { displayName: 'Nama Material', field: 'PartName' },
                { displayName: 'Qty RPP', field: 'QtyRPP' },
                { displayName: 'Qty Remain', field: 'QtyReceived' },
                { displayName: 'Qty GI', field: 'QtyGI' },
                { displayName: 'Lokasi Gudang', field: 'WarehouseName' },
                { displayName: 'Lokasi Rak', field: 'ShelfCode' },
            ]
        };

        //----------------------------------
        // Dummy PR
        //----------------------------------
        $scope.gridClaimView = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
            },
            paginationPageSizes: [5, 10, 20, 25, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 25,
            cellEditableCondition: function($scope) {
                if ($scope.col.field == "QtyGI") {
                    if ($scope.row.entity[$scope.col.field] == 'Freeze') {
                        return false;
                    } else {
                        return true;
                    }
                }
            },
            columnDefs: [
                { name: 'No Material', field: 'PartsCode' },
                { name: 'Nama Material', field: 'PartName' },
                { name: 'Qty Parts Claim', field: 'QtyInvoice' },
                { name: 'Qty Remain', field: 'QtyInvoice' },
                {
                    displayName: 'Qty GI',
                    name: 'Qty GI',
                    field: 'QtyGI',
                    editableCellTemplate: '<input type="number" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">'
                },
                { name: 'Lokasi Gudang', field: 'WarehouseName' },
                { name: 'Lokasi Rak', field: 'ShelfCode' },
            ]
        };

        $scope.gridClaim = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {


                    console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);

                    if (rowEntity.QtyGI <= 0 || rowEntity.QtyGI == "") {
                        bsNotify.show({
                            title: "Goods Issue",
                            content: "Quantity GI tidak valid",
                            type: 'danger'
                        });
                        rowEntity.QtyGI = oldValue;
                        return
                    } else {
                        // rowEntity.QtyRemain = rowEntity.QtySO - rowEntity.QtyGI;
                    }
                    // new adding code 28/09/2020
                    rowEntity.TotalCost = rowEntity.UnitCost * rowEntity.QtyGI;
                    rowEntity.TotalStandardCost = rowEntity.StandardCost * rowEntity.QtyGI;
                });
            },
            paginationPageSizes: [5, 10, 20, 25, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 25,
            cellEditableCondition: function($scope) {
                if ($scope.col.field == "QtyGI") {
                    if ($scope.row.entity[$scope.col.field] == 'Freeze') {
                        return false;
                    } else {
                        return true;
                    }
                }
            },
            columnDefs: [
                { name: 'No Material', field: 'PartsCode' },
                { name: 'Nama Material', field: 'PartName', enableCellEdit: false },
                { name: 'Qty Parts Claim', field: 'QtyInvoice', enableCellEdit: false },
                { name: 'Qty Remain', field: 'QtyInvoice', enableCellEdit: false },
                {
                    displayName: 'Qty GI',
                    name: 'Qty GI',
                    field: 'QtyGI',
                    editableCellTemplate: '<input type="number" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">'
                },
                { name: 'Lokasi Gudang', field: 'WarehouseName', enableCellEdit: false },
                { name: 'Lokasi Rak', field: 'ShelfCode', enableCellEdit: false },
                {
                    name: 'Action',
                    cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row.entity)"><u>Hapus</u></a></center>',
                    visible: true
                }
            ]
        };

        // cetak - SSRS - SURAT PENGANTAR PENGEMBALIAN BARANG
        $scope.printGIMaterial = function(GoodsIssueId) {
            console.log("$scope.mData",$scope.mData);
            $scope.printGIKeluarMaterial($scope.mData.GoodsIssueId);
        }

        $scope.printGIKeluarMaterial = function(GoodsIssueId) {
            console.log('GoodsIssueId =', GoodsIssueId);
            var data = $scope.mData;
            //$scope.dataGIKeluarMaterial = 'as/SuratPengantarPengembalianBarang/' + GoodsIssueId;
            $scope.dataGIKeluarMaterial = 'as/BuktiPengeluaranMaterial/' + GoodsIssueId;
            $scope.cetakan($scope.dataGIKeluarMaterial);
        };
        $scope.printBatalGIMaterial = function() {
            console.log("$scope.mData",$scope.mData);
            $scope.printGIBatal($scope.mData.GoodsIssueId);
        }
        $scope.printGIBatal = function(GoodsIssueId) {
            console.log('GoodsIssueId =', GoodsIssueId);
            var data = $scope.mData;
            $scope.dataGIBatalMaterial = 'as/BuktiPembatalanMaterial/' + GoodsIssueId;
            $scope.cetakan($scope.dataGIBatalMaterial);
        };

        $scope.cetakan = function(data) {
            var pdfFile = null;

            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    }
                    else {
                        printJS(pdfFile);
                    }
                }
                else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };

        $scope.GenerateDocNumstwo = function(data) {
            $scope.servicetypes = 0;
            PartsSalesOrder.getserviceandmaterial().then(function(res) {
                var resulti = res.data[0].Servicetype;
                console.log("What Will be Will Be = ", resulti);
                $scope.servicetypes = resulti;
                PartsCurrentUser.getFormatId($scope.servicetypes, 'GI-').then(function(res) {
                    var result = res.data;
                    console.log("FormatId = ", result[0].Results);

                    PartsCurrentUser.getDocumentNumber2(result[0].Results).then(function(res) {
                        var DocNo = res.data;
                        console.log("FormatId = ", DocNo);
                        if (typeof DocNo === 'undefined' || DocNo == null) {
                            bsNotify.show({
                                title: "GOODS ISSUE",
                                content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                                type: 'danger'
                            });
                        } else {
                            console.log("CancelNo = ", DocNo[0]);
                            $scope.mData.CancelNo = DocNo[0];
                            $scope.mData.CancelReasonId = data.CancelReasonId ;
                            $scope.mData.CancelReasonDesc = data.CancelReasonDesc;
                            console.log ("FOLLOW Me", $scope.mData);
                            GoodsIssue.cancel($scope.mData).then(function(resu) {
                                //console.log(res);
                                console.log ("apaini", $scope.mData);
                                //alert("GoodsIssue berhasil dibatalkan");
                                bsNotify.show({
                                    title: "Goods Issue",
                                    content: "Berhasil Di Batalkan dengan No"+ $scope.mData.CancelNo,
                                    type: 'success'
                                });
                                $scope.printGIBatal($scope.mData.GoodsIssueId);
                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
                        }

                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );

                    },
                    function(err) {

                    }
                );

            // [END] Get Current User Warehouse
            console.log("TAKI-TAKI", $scope.mData.CancelNo);
            });
        };

        $scope.GenerateDocNums = function() {
            $scope.servicetypes = 0;
            PartsSalesOrder.getserviceandmaterial().then(function(res) {
                var resulti = res.data[0].Servicetype;
                console.log("What Will be Will Be = ", resulti);
                $scope.servicetypes = resulti;
                PartsCurrentUser.getFormatId($scope.servicetypes, 'GI-').then(function(res) {
                    var result = res.data;
                    console.log("FormatId = ", result[0].Results);

                    PartsCurrentUser.getDocumentNumber2(result[0].Results).then(function(res) {
                            var DocNo = res.data;

                            if (typeof DocNo === 'undefined' || DocNo == null) {
                                bsNotify.show({
                                    title: "GOODS ISSUE",
                                    content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                                    type: 'danger'
                                });
                            } else {
                                $scope.mData.CancelNo = DocNo[0];
                                // $scope.mData.DocDate = DocNo[1];
                                $scope.mData.DocDate = setDate(DocNo[1]);
                                console.log("Generating DocNo & DocDate ->");
                            }


                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );


                },
                function(err) {

                }
            );
            // [END] Get Current User Warehouse
            console.log("TAKI-TAKI", $scope.mData.CancelNo);
            });
        };

    });
