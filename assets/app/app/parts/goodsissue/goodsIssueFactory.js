angular.module('app')
    .factory('GoodsIssue', function($http, CurrentUser, $window, $filter) {
        var currentUser = CurrentUser.user;

        var GoodsIssueHeader = {};
        var GoodsIssueDetail = {};
        this.formApi = {};
        this.MaterialTypeId = 0;
        this.ServiceTypeId = 0;
        this.WarehouseId = 0;

        return {
            getDataNonFilter: function() {
                var res = $http.get('/api/as/GoodsIssue');
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },

            getData: function(data) {
                console.log("[Factory]");
                console.log(data);

                //filter tanggal
                var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
                var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');

                var res = $http.get('/api/as/GoodsIssue', {
                    params: {
                        RefGINo: (data.RefGINo == null ? "n/a" : data.RefGINo),
                        GoodsIssueNo: (data.GoodsIssueNo == null ? "n/a" : data.GoodsIssueNo),
                        GoodsIssueStatusId: (data.GoodsIssueStatusId == null ? "n/a" : data.GoodsIssueStatusId),
                        RefGITypeID: (data.RefGITypeID == null ? "n/a" : data.RefGITypeID),
                        startDate: (startDate == null ? "n/a" : startDate),
                        endDate: (endDate == null ? "n/a" : endDate),
                        NoPol: (data.NoPol == null ? "n/a" : data.NoPol),
                        PartsCode: (data.PartsCode == null ? "n/a" : data.PartsCode)
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },
            IsPrintableGI: function(data) {
                var res = $http.get('/api/as/PartGlobal/IsPrintableGI', {
                    params: {
                        RefGITypeId: data.RefGITypeID,
                        RefGINo: data.RefGINo
                    }
                });
                console.log('hasil=>', res);
                return res;
            },

            create: function(data, detail) {
                console.log('tambah data=>', data);
                var startDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
                return $http.post('/api/as/GoodsIssue', [{
                   // GoodsIssueId: (data.GoodsIssueId==null?0:data.GoodsIssueId),
                    MaterialTypeId: data.MaterialTypeId,
                    RefGITypeID: data.RefGITypeID,
                    OutletId: data.OutletId,
                    DocDate: startDate,
                    WarehouseId: data.WarehouseId,
                    GoodsIssueNo: data.GoodsIssueNo,
                    RefGINo: data.RefGINo,
                    VehicleId: (data.VehicleId == null ? 0 : data.VehicleId),
                    MaterialRequestId: (data.MaterialRequestId == null ? 0 : data.MaterialRequestId),
                    ReceivedBy: (data.ReceivedBy == null ? "" : data.ReceivedBy),
                    Notes: (data.Notes == null ? "" : data.Notes),
                    GoodsIssueStatusId: (data.GoodsIssueStatusId == null ? 0 : data.GoodsIssueStatusId),
                    GridDetail: detail
                }]);
            },
            createDetail: function(data) {
                console.log('tambah data grid detail=>', data);

                return $http.post('/api/as/GoodsIssue/InsertGoodsIssueDetail', data);
            },

            update: function(data) {
                console.log('rubah data=>', data);
                return $http.put('/api/as/TestVehicle', [{
                    GoodsIssueId: (data.GoodsIssueId == null ? 0 : data.GoodsIssueId),
                    GoodsIssueStatusId: 1

                }]);
            },

            cancel: function(data) {
                console.log('batal data=>', data);
                console.log('batal data=> CancelNo', data.CancelNo);
                //var CancelDate = $filter('date')(data.CancelDate, 'yyyy-MM-ddT00:00:00Z');
                // var CancelDate = $filter('date')(data.CancelDate, 'yyyy-MM-ddTHH:mm:ssZ');
                var CancelDate = $filter('date')(data.CancelDate, 'yyyy-MM-dd HH:mm:ss');
                data.DocDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
                console.log('CancelDate', CancelDate);
                return $http.put('/api/as/GoodsIssue', [{
                    GoodsIssueId: data.GoodsIssueId,
                    GoodsIssueNo: data.GoodsIssueNo,
                    OutletId: data.OutletId,
                    WarehouseId: data.WarehouseId,
                    DocDate: data.DocDate,
                    MaterialTypeId: data.MaterialTypeId,
                    RefGITypeID: data.RefGITypeID,
                    RefGINo: data.RefGINo,
                    MaterialRequestId: (data.GoodsIssueId == null ? 0 : data.GoodsIssueId),
                    VehicleId: data.VehicleId,
                    ReceivedBy: data.ReceivedBy,
                    Notes: data.Notes,
                    CancelDate: CancelDate,
                    GoodsIssueStatusId: 1,
                    CancelNo : data.CancelNo,
                    CancelReasonDesc : data.CancelReasonDesc,
                    CancelReasonId : data.CancelReasonId
                }]);
            },

            delete: function(id) {
                console.log("delete id==>", id);
                return $http.delete('/api/as/TestVehicle', { data: id, headers: { 'Content-Type': 'application/json' } });
            },


            searchAppointment: function(data, key) {
                console.log("[searchAppointment]");
                console.log(data);
                var res = $http.get('/api/as/GoodsIssue/SearchWoAppointment', {
                    params: {
                        RefGiNo: data,
                        MaterialTypeId: key
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },

            searchSalesOrder: function(data) {
                console.log("[searchSalesOrder]");
                console.log(data);
                var res = $http.get('/api/as/GoodsIssue/SearchSalesOrder', {
                    params: {
                        RefGiNo: data,
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },
            SearchReference: function(no, type) {
                console.log("[SearchReference]");
                var res = $http.get('/api/as/GoodsIssue/SearchReference', {
                    params: {
                        RefGiNo: no,
                        RefGITypeId: type
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },

            searchAppointmentDetail: function(data, wid, isopb) {
                console.log("[searchAppointmentDetail]");
                console.log("data=>",data);
                console.log("wid=>",wid);
                console.log("isopb1=>",isopb);
                var res = $http.get('/api/as/GoodsIssue/SearchWoAppointmentDetail', {
                    params: {
                        RefGiNo: data,
                        WarehouseId: wid,
                        IsOPB: (isopb == undefined ? "" : isopb),
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },

            getDocumentNumber: function(data) {
                console.log("[getDocumentNumber]");
                console.log(data);
                var res = $http.get('/api/as/GoodsIssue/GetDocumentNumber', {
                    params: {
                        UserId: data,
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },

            getDocumentNumber2: function(FormatId) {
                console.log("[getDocumentNumber]");
                var res = $http.get('/api/as/PartsUser/GetDocumentNumber', {
                    params: {
                        FormatId: FormatId,
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },

            getDetail: function(id, no, type) {
                console.log("[getDetail]");
                var res = $http.get('/api/as/GoodsIssue/GetGoodsIssueDetail', {
                    params: {
                        GoodsIssueId: id,
                        RefGINo: no,
                        RefGITypeID: type
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;

            },
            setGoodsIssueHeader: function(data) {
                GoodsIssueHeader = data;
            },
            setGoodsIssueDetail: function(dataDetail) {
                GoodsIssueDetail = dataDetail;
            },
            getGoodsIssueDetail: function() {
                return GoodsIssueDetail;
            },
            getGoodsIssueHeader: function() {
                return GoodsIssueHeader;
            },
            // Detail digunakan saat View dan Edit
            getDetailByMaterialNo: function(data) {
                console.log("[getDetail]");
                console.log(data);
                var res = $http.get('/api/as/StockAdjustment/GetStockAdjustmentDetailFromMaterial', {
                    params: {
                        PartsCode: data,
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;

            },
            SearchMaterialRequest: function() {
                console.log("[GetMaterialRequestData]");
                var res = $http.get('/api/as/GoodsIssue/GetMaterialRequestData');
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },
            SearchWorkOrder: function() {
                console.log("[SearchWorkOrder]");
                var res = $http.get('/api/as/PartsUser/WorkOrderLookUp');
                // var res = $http.get('/api/as/PartsUser/SearchWorkOrder');
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },
            SalesBillingGICheck: function(data) {
                console.log("[getDetail]");
                console.log(data);
                var res = $http.get('/api/as/GoodsIssue/SalesBillingGIChecking', {
                    params: {
                        SalesOrderNo: data,
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;

            },
            // added by sss on 2017-12-18
            getPartsFreeze: function(PartCode) {
                var res = $http.get('/api/as/GetStockOpnameFreeze?partsCode=' + PartCode.partsCode);
                console.log('<factory> dataDetail =>', res);
                return res;
            },
            //
            CekPartsStockOpname: function(data) {
                return $http.get('/api/as/CekPartsStockOpname')
            },
            getStatusWO: function(param) {
                var WoNo = param.replace(/\//g, '%20');
                var woNoFinal = angular.copy(WoNo);
                if (woNoFinal.includes("*")) {
                    woNoFinal = woNoFinal.split('*');
                    woNoFinal = woNoFinal[0];
                }
                var res = $http.get('/api/as/CheckIsGICancelWO/'+woNoFinal);
                return res;
            },
            getStatusStockGI: function(data,data1) {
                console.log('<factory> cek status stok GI=>', data);

                var res=$http.get('/api/as/GoodsIssue/CekStatusStok/',
                {
                    params: {
                        PartId: data,
                        WarehouseId: data1
                    }
                });

                console.log("<factory> res=>",res);
                return res;
            },
            getStatusWOTeco: function(data) {
                console.log('<factory> cek status WO=>', data);

                var res=$http.get('/api/as/GoodsIssue/CekStatusWO/',
                {
                    params: { WoNo: data }
                });

                console.log("<factory> res=>",res);
                return res;
            },
        }
    });