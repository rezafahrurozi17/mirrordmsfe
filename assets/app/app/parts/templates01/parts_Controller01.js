angular.module('app')
    .controller('Parts_Controller01', function($scope, $http, CurrentUser, $timeout, ngDialog, $state,PartsCurrentUser, //parApproval
        PartsGlobal) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    $scope.mData = {};

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.baseConfirmTemplate= '\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Anda yakin akan membatalkan <<DOC>> ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="executeBatalDok()">Ya</button>\
                     </div>\
                     </div>';
    $scope.user = CurrentUser.user();
    $scope.mRole = null; //Model mData.
    $scope.date = new Date(); 
    $scope.xRole={selected:[]};  
    $scope.formLookupTitle = PartsGlobal.formLookupTitle;
    $scope.formLookupGrid = PartsGlobal.formLookupGrid; 
    $scope.formLookupSubTitle1= PartsGlobal.formLookupSubTitle1;//"  Daftar PO untuk Outlet : " ;//+ PartsGlobal.formLookupGrid.data[0].OutletId; 
    $scope.formLookupSubTitle2= PartsGlobal.formLookupSubTitle2;
    // $scope.subtitle= 'testt';//;parApproval.subtitle;
    $scope.showApproval = function(vst, vkeg, vnoreq, vapprover, vpesan){
        parApproval.setparApproval(vst, vkeg, vnoreq, vapprover, vpesan);
        ngDialog.openConfirm ({
        //template:'<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Permohonan Approval</h3></div> <div class="panel-body"><form method="post"><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nama Kegiatan</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">MIP Maintenance</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nomor Request</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">-</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Tanggal dan Jam</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">-</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Approve(s)</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4"></textarea></div></div> <br><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Pesan</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4">Mohon untuk approve list dari item Perhitungan MIP berikut. Terima kasih.</textarea></div></div> <br><div class="row"><div class="col-md-12"><div class="form-group"><div> <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Kembali</button> <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()" style="color:white; background-color:#D53337">OK</button> </div></div></div></div></form></div></div>',
        template:'<div ng-include=\"\'app/parts/templates01/frmApproval01.html\'\"></div>',
        plain: true,
        controller: 'Parts_Controller01', //'MIPMaintenanceController',
       });
    }
    var parApproval= PartsGlobal.getparApproval();
    $scope.subtitle= parApproval.subtitle;
    $scope.kegiatan= parApproval.kegiatan;
    $scope.mData.noreq= parApproval.noreq;
    $scope.mData.approver= parApproval.approver;
    $scope.mData.pesan= parApproval.pesan + ' Terima kasih.';
    $scope.btnOK= parApproval.btnOK;
    $scope.btnCancel= parApproval.btnCancel;
    $scope.getdocname= function(doctype) {
        var docname='';
        switch (doctype)
        {
            case "GR" : docname = "Goods Receipt"; break;
            case "MR" : docname = "Material Request"; break;
            case "GI" : docname = "Goods Issue"; break;
            case "PO" : docname = "Purchase Order"; break;
            case "SO" : docname = "Sales Order"; break;
        }
        return docname;
     };
    $scope.confirmBatalDok = function(data){
      var strTemplate = $scope.baseConfirmTemplate.replace("<<DOC>>", $scope.getdocname(data.doctype));
      ngDialog.openConfirm ({ 
        template : strTemplate,        
        plain: true,
        controller: data.controller,
       });
    };    
    $scope.batalApproval = function(){
        alert('Permohonan approval batal dikirim'); // 
        $scope.ngDialog.close();
        var vApproval = PartsGlobal.getparApproval();
        if (vApproval.TypeId== 0) PartsGlobal.askApproval({});        
        PartsGlobal.RequestApprovalSent= false;
        PartsGlobal.setReqAppSentPG(false);                
    }; 

    $scope.okSentApproval = function(){
        // alert('Permohonan approval telah dikirim'); ...
        $scope.ngDialog.close();
        var vApproval = PartsGlobal.getparApproval();
        if (vApproval.TypeId== 0) PartsGlobal.askApproval({});        
        PartsGlobal.RequestApprovalSent= true;
        PartsGlobal.setReqAppSentPG(true);            
        //PartsGlobal.setdoProcAfterRequestApprovalSentPG(vApproval.doProc)
        // PartsGlobal.showAlert({
        //     title:'Approval', 
        //     content:'Permohonan approval telah dikirim',
        //     type:'info'
        // });        
    }; 
    $scope.DoApprove = function(approval,data){
        
        var row = data;        
        console.log(PartsGlobal.selectedLookupGrid);  
        var vApproval = PartsGlobal.getparApproval();
        PartsGlobal.statusApproval = approval;
        console.log('Permohonan di approve', row);  
        for (var i in PartsGlobal.selectedLookupGrid){
            var modulProcessId = PartsGlobal.selectedLookupGrid[i].StockReservedId == null || PartsGlobal.selectedLookupGrid[i].StockReservedId == 0 ? 8131 : 8020 ;
            //? 8020 : 8131;
            console.log('Permohonan di approve',modulProcessId,PartsGlobal.selectedLookupGrid[i].StockReservedId );
            PartsCurrentUser.actApproval({ 
                ProcessId: modulProcessId, DataId: PartsGlobal.selectedLookupGrid[i].StockReservedId || PartsGlobal.selectedLookupGrid[i].TransferOrderId , ApprovalStatus: 1
            }).then(function(res) {
                console.log('Permohonan di approve',res);
                alert('Permohonan di approve');
                $scope.ngDialog.close();
            });
        }
     
        // if (vApproval.TypeId== 0) PartsGlobal.askApproval({});        
        // PartsGlobal.RequestApprovalSent= true;
        // PartsGlobal.setReqAppSentPG(true);             
    };
    $scope.DoReject = function(approval,data){

        var row = data;
        
        var vApproval = PartsGlobal.getparApproval();
        PartsGlobal.statusApproval = approval;

        for (var i in PartsGlobal.selectedLookupGrid){
            var modulProcessId = PartsGlobal.selectedLookupGrid[i].StockReservedId == null || PartsGlobal.selectedLookupGrid[i].StockReservedId == 0 ? 8131 : 8020 ;
            //? 8020 : 8131;
            console.log('Permohonan di approve',modulProcessId,PartsGlobal.selectedLookupGrid[i].StockReservedId );
            PartsCurrentUser.actApproval({ 
                ProcessId: modulProcessId, DataId: PartsGlobal.selectedLookupGrid[i].StockReservedId || PartsGlobal.selectedLookupGrid[i].TransferOrderId , ApprovalStatus: 2
            }).then(function(res) {
                alert('Permohonan ditolak');
                $scope.ngDialog.close();
            });
        }
     
        // if (vApproval.TypeId== 0) PartsGlobal.askApproval({});        
        // PartsGlobal.RequestApprovalSent= true;
        // PartsGlobal.setReqAppSentPG(true);             
    };
    $scope.okBatalDok = function(){
        $scope.dokbatal = PartsGlobal.getDokBatal();
        console.log("data batal : ", $scope.dokbatal);
        $scope.confirmBatalDok($scope.dokbatal);        
    }
    $scope.okFormLookup = function(){ 
        var windowIDs = ngDialog.getOpenDialogs();
        // $scope.ngDialog.close();  //(windowIDs[1]
        $scope.ngDialog.close(windowIDs[windowIDs.length -1]);
        PartsGlobal.cancelledLookupGrid = false;
        return 1;
    }
});
