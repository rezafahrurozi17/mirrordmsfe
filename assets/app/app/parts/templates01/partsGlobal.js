angular.module('app')
    .factory('PartsGlobal', function($http, bsNotify, $filter, ngDialog, bsAlert) {
        var myParts = {};
        this.myWarehouses = [];
        this.myWarehouse = {};
        this.myServiceTypeId = 0;
        this.formLookupTitle = "";
        this.formLookupSubTitle1 = "";
        this.formLookupSubTitle2 = "";
        this.formLookupShowFilter = false;
        this.OutletId = -99;
        this.VendorId = -99;
        this.partVersion = ""; //"17.12.12.018.01";
        this.formLookupGrid = {}; //"";
        this.selectedLookupGrid = {};
        this.AllSelectedLookupGrid = [];
        this.filterableColumns = []; //[{field: "PurchaseOrderNo"}, {field: "PartsCode"}];
        this.cancelledLookupGrid = true;
        this.statusApproval = 0;
        this.RequestApprovalSent = false;
        // var RequestApprovalSentPG= false;
        this.RequestApprovalSentPG = false;
        // var doProcAfterRequestApprovalSentPG= true;
        this.doProcAfterRequestApprovalSentPG = true;
        var dokbatal = {};
        var parApproval = {
            subtitle: 'Dia perlu ... ',
            kegiatan: '',
            noreq: '123',
            approver: 'approver 01',
            pesan: 'Mohon approve ',
            dataId: -1,
            btnOK: 'Kirim Permohonan',
            btnCancel: 'Batal',
            objMaterial: {}
        };
        var salesbilling = {
            bl: 0,
            cl: 0,
            viewbl: false,
            viewcl: false
        };
        return {
            showAlert: function(msg) {
                bsNotify.show(msg);
            },
            // toDate = function(data) {
            //   var vDate = $filter('date')(data, 'yyyy-MM-dd');
            //   return vDate ;
            // },
            alertAfterSave: function(item, item2) {
                var str2 = "";
                if (item2 != null) str2 = "dan Nomor " + item2.doctype + " : " + item2.docno;
                bsAlert.alert({
                        title: "Data tersimpan",
                        text: "Dengan Nomor " + item.doctype + " : " + item.docno + str2,
                        type: "success",
                        showCancelButton: false
                    },
                    function() {
                        // $scope.filter.noPolice=null;
                        // $scope.filter.noRangka=null;
                        $scope.formApi.setMode('grid');
                    },
                    function() {

                    }
                );
            },
            getReqAppSentPG: function() {
                return RequestApprovalSentPG;
            },
            setReqAppSentPG: function(mode) {
                RequestApprovalSentPG = mode;
            },
            setdoProcAfterRequestApprovalSentPG: function(mode) {
                doProcAfterRequestApprovalSentPG = mode;
            },
            getCurrDate: function() {
                var date = new Date();
                var res = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                return res;
            },
            getXDate: function(d) {
                var date = new Date(d);
                var res = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                return res;
            },
            IsPartAvailable: function(data) {
                var res = $http.get('/api/as/PartGlobal/IsPartAvailable', {
                    params: {
                        PartId: data.PartId,
                        WarehouseId: data.WarehouseId
                    }
                });
                return res;
            },
            getCurrTime: function() {
                var date = new Date();
                var res = date.getHours() + ':' + date.getMinutes();
                return res;
            },
            getCurrDateTime: function() {
                var date = new Date();
                var date1 = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
                var time1 = date.getHours() + ':' + date.getMinutes();
                // var res = this.getCurrDate() + ' ' + this.getCurrTime();
                var res = date1 + ' ' + time1;
                return res;
            }, //api/as/PartGlobal/PartVersion
            getPartVersion: function() {
                var res = $http.get('/api/as/PartGlobal/PartVersion');
                console.log('(Factory) BE PartVersion => ', res);
                return res;
            },
            getDetailMaterial: function(pcode) {
                // var res=0;
                console.log("P code : ", pcode);
                var res = $http.get('/api/as/PartGlobal', { params: { PartsCode: pcode } });
                console.log('hasil=.>', res);
                return res;
            },
            getDetailMaterialICCSCC: function(data) {
                return $http.get('/api/as/PartGlobal/PartDetail', {
                    params: {
                        WHId: data.WarehouseId,
                        SCC: data.SCC,
                        ICC: data.ICC,
                        PartsCode: data.partscode
                    }
                });

            },
            //api/as/PartGlobal/PartDetail
            getDetailMaterialOneWarehouse: function(part) {
                console.log("P code : ", part);
                var res = $http.get('/api/as/PartGlobal', {
                    params: {
                        PartsCode: part.pcode,
                        WHId: part.WarehouseId
                    }
                });
                console.log('hasil=.>', res);
                return res;
            },
            getWarehouse: function(data) {
                var res = $http.get('/api/as/PartGlobal/GetWarehouse', {
                    params: {
                        outletid: (data.OutletId == null ? 0 : data.OutletId),
                        materialtypeid: (data.MaterialTypeId == null ? 0 : data.MaterialTypeId),
                        servicetypeid: (data.ServiceTypeId == null ? 0 : data.ServiceTypeId)
                    }
                });
                console.log("Whi : ", res);
                // console.log("Whi2 : ", res.$$state);
                // console.log("Whi3 : ", res.$$state.value);
                return res;
            },
            getVendor: function() {
                var res = $http.get('/api/as/Vendor');
                console.log("Vendor : ", res);
                // console.log("Whi2 : ", res.$$state);
                // console.log("Whi3 : ", res.$$state.value);
                return res;
            },
            // 13-01-2018
            getVendorPO: function(parVendor) {
                var res = $http.get('/api/as/PartGlobal/VendorPO', { params: { POType: parVendor.POType } });
                console.log("Vendor PO : ", res);
                return res;
            },
            //update by rizkha 05/07/2018
            getNoShipping: function(ShipNo) {
                var res = $http.get('/api/as/ClaimInquiry/SearchShippingNo/' + ShipNo);
                console.log("List No Shipping : ", res);
                return res;
            },
            getNoShippingFilterMain: function(ShipNo) {
                var res = $http.get('/api/as/GetShippingNoGoodsReceipt?ShipNo=' + ShipNo);
                console.log("List No Shipping filter : ", res);
                return res;
            },

            getVendorOutlet: function() {
                var res = $http.get('/api/as/PartGlobal/VendorOutlet');
                console.log("Vendor Outlet : ", res);
                return res;
            },
            getProvince: function() {
                console.log("[GetProvinceData]");
                var res = $http.get('/api/as/Province');
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            }, // getCancelNo
            getCancelNo: function(data) {
                var res = $http.get('/api/as/PartGlobal/GetCancelNo', {
                    params: {
                        DocType: data.doctype,
                        DataId: data.dataId
                    }
                });
                console.log("CancelNo : ", res);
                return res;
            },
            getPartsGlobal: function(varname, vdefault) {
                if (typeof myParts[varname] == 'undefined') {
                    return vdefault;
                } else {
                    return myParts[varname];
                }
            },
            getInfoCabang: function(data) {
                // alert ('Detail info : '+ data.cabang);
                this.showAlert({
                    size: 'big',
                    title: 'Info Cabang',
                    content: 'Detail info : ' + data.cabang + ' berada di ' + data.provinsi,
                    type: 'info'
                });
            },
            getWarehouseId: function(stid, mtid, oid) {
                var res = $http.get('/api/as/PartGlobal/GetWarehouse', {
                    params: {
                        OutletId: oid,
                        MaterialTypeId: mtid,
                        ServiceTypeId: stid


                    }
                });
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            //Cek apakah sebuah GI Sudah Selesai (Completed) dilakukan atau belum
            isGIDone: function(data) {
                console.log("Data Param PO ", data);
                var res = $http.get('/api/as/PartGlobal/IsGIDone', {
                    params: {
                        RefGIType: data.RefGITypeID,
                        RefGINo: data.RefGINo,
                        MaterialTypeId: data.MaterialTypeId
                    }
                });
                return res;
            },
            getDaftarPO: function(data) {
                console.log("Data Param PO ", data);
                var res = $http.get('/api/as/PartGlobal/GetDaftarPO', {
                    params: {
                        OutletId: data.OutletId,
                        VendorId: data.VendorId
                    }
                });
                return res;
            },
            getDaftarPOOPB: function(data) {
                console.log("Data Param PO ", data);
                var res = $http.get('/api/as/PartGlobal/getDaftarPOOPB', {
                    params: {
                        OutletId: data.OutletId,
                        PoNumber: data.PoNumber
                        , OPB :data.OPB
                        , VendorId : data.VendorId
                    }
                });
                return res;
            },
            // api/as/PartGlobal/GetListClaim
            getDaftarClaim: function(data) {
                console.log("Data Param Claim ", data);
                var res = $http.get('/api/as/PartGlobal/GetListClaim', { params: { VendorId: data.VendorId } });
                return res;
            },
            getDaftarTO: function(data) {
                console.log("Param Data Warehouse : ", data);
                var res = $http.get('/api/as/PartGlobal/GetDaftarPO', {
                    params: {
                        OutletId: data.OutletId,
                        VendorId: data.VendorId,
                        IsTOData: 1,
                        MaterialTypeId: data.MaterialTypeId
                    }
                });
                return res;
            },
            getMyWarehouse: function(data) {
                console.log("Param Data Warehouse : ", data);
                console.log("my Warehouse : ", this.myWarehouse);
                if (this.myWarehouse = {}) {
                    // if (this.myWarehouse.length == 0) {
                    var res = this.getWarehouse(data);
                    this.myWarehouse = res;
                }
                return this.myWarehouse;
            },
            setPartsGlobal: function(varname, value) {
                myParts[varname] = value;
            },
            getparApproval: function() {
                return parApproval;
            },
            setparApproval: function(vparApproval) {
                // parApproval[varname] = value;
                parApproval.subtitle = vparApproval.subtitle;
                parApproval.kegiatan = vparApproval.kegiatan;
                parApproval.noreq = vparApproval.noreq;
                parApproval.approver = vparApproval.approver;
                parApproval.pesan = vparApproval.pesan;
                parApproval.TypeId = vparApproval.TypeId; // 0= With DataId and Send first; 1=Without DataId save first
                parApproval.ProcessId = vparApproval.ProcessId;
                parApproval.dataId = vparApproval.dataId;
                parApproval.userid = vparApproval.userid;
                // parApproval.procNext = vparApproval.procNext;
                if (!vparApproval.btnOK) {} else {
                    parApproval.btnOK = vparApproval.btnOK;
                }
                if (vparApproval.btnCancel) { parApproval.btnCancel = vparApproval.btnCancel; }
                // parApproval.objMaterial:{}
            },
            //api/as/PartGlobal/GetApprover
            getApprover: function(ProcessId) {
                console.log("Data approver : ", ProcessId);
                var res = $http.get('/api/as/PartGlobal/GetApprovers', {
                    params: { processId: ProcessId }
                });
                return res;
            },
            askApproval: function(data) {
                console.log("Data approval : ", parApproval);
                var res = $http.get('/api/as/PartGlobal/AskApproval', {
                    params: {
                        // ProcessId: data.ProcessId, DataId: data.DataId, Message: data.Message, UserIdForApproval: data.userid
                        // ProcessId: 24, DataId: 16, Message: 'Mohon di Approve donk ', UserIdForApproval: 0
                        ProcessId: parApproval.ProcessId,
                        DataId: parApproval.dataId,
                        Message: parApproval.pesan,
                        UserIdForApproval: parApproval.userid
                    }
                });
                return res;
            },
            actApproval: function(data) {
                console.log("Data approval : ", data);
                var res = $http.get('/api/as/PartGlobal/ActApproval', {
                    params: {
                        ProcessId: data.ProcessId,
                        DataId: data.DataId,
                        ApprovalStatus: data.ApprovalStatus
                    }
                });
                return res;
            },
            // api/as/PartGlobal/PrintOutNeedToAsk (String PrintOutDocTypeId, String DocDataId)
            PrintOutNeedToAsk: function(data) {
                var res = $http.get('/api/as/PartGlobal/PrintOutNeedToAsk', {
                    params: {
                        printOutDocTypeId: data.PrintOutDocTypeId,
                        docDataId: data.DocDataId
                    }
                });
                return res;
            },
            // api/as/PartGlobal/PrintOutApproval_Ask (String PrintOutDocTypeId, String DocDataId, String Message)
            PrintOutApproval_Ask: function(data) {
                var res = $http.get('/api/as/PartGlobal/PrintOutApproval_Ask', {
                    params: {
                        printOutDocTypeId: data.PrintOutDocTypeId,
                        docDataId: data.DocDataId,
                        message: data.Message
                    }
                });
                return res;
            },
            // api/as/PartGlobal/PrintOutApproval_Act (String PrintOutDocTypeId, String DocDataId, String ApprovalStatus)
            PrintOutApproval_Act: function(data) {
                var res = $http.get('/api/as/PartGlobal/PrintOutApproval_Act', {
                    params: {
                        printOutDocTypeId: data.PrintOutDocTypeId,
                        docDataId: data.DocDataId,
                        approvalStatus: data.ApprovalStatus
                    }
                });
                return res;
            },
            // api/as/PartGlobal/PrintOutApproval_AfterPrint (String PrintOutDocTypeId, String DocDataId)
            PrintOutApproval_AfterPrint: function(data) {
                var res = $http.get('/api/as/PartGlobal/PrintOutApproval_AfterPrint', {
                    params: {
                        printOutDocTypeId: data.PrintOutDocTypeId,
                        docDataId: data.DocDataId
                    }
                });
                return res;
            },
            getparobjMaterial: function() {
                return parApproval.objMaterial;
            },
            setparobjMaterial: function(data) {
                parApproval.objMaterial = {};
                for (var k in data) parApproval.objMaterial[k] = data[k];
                // return parApproval.objMaterial;
            },
            batalDok: function(doc) {
                var sroute = '';
                console.log("DOk yg Batal", doc);
                switch (doc.doctype) {
                    case "GR":
                        sroute = "/api/as/BatalGoodsReceipt";
                        break;
                    case "MR":
                        sroute = "/api/as/BatalMR";
                        break; // not yet
                }
                var res = $http.get(sroute, { params: { docid: doc.docid } });
                console.log('hasil=>', res);
                return res;
            },
            showMessage: function(msg) {
                bsNotify.show({
                    title: 'Info ',
                    content: msg,
                    type: 'info'
                });
            },
            showApproval: function(vparApp) {
                vparApp['TypeId'] = 0;
                this.setparApproval(vparApp);
                // var RequestApprovalSentYes= false;
                ngDialog.openConfirm({
                    template: '<div ng-include=\"\'app/parts/templates01/frmApproval01.html\'\"></div>',
                    plain: true,
                    controller: vparApp.controller, //'MIPMaintenanceController',
                }).then(function(value) {
                    console.log(" t/f : ", this.RequestApprovalSent);
                    var RequestApprovalSentYes = this.RequestApprovalSent;
                    // RequestApprovalSentYes = this.RequestApprovalSent;
                    // console.log("Data fetched ", $scope.RequestApprovalSent);
                    // if ($scope.RequestApprovalSent) {
                    //   $scope.MRHeader['ApprovalStatus'] = 0;
                    //   MaterialRequest.updateData($scope.MRHeader, $scope.gridDetail.data);
                    // }
                    console.log("ada : ? ", RequestApprovalSentYes)
                    return RequestApprovalSentYes;
                }, function(value) {
                    console.log(" t/f 2 : ", RequestApprovalSentPG);
                    // console.log(" t/f 2 B : ", RequestApprovalSent);
                    var RequestApprovalSentYes = this.RequestApprovalSent;
                    // RequestApprovalSentYes = this.RequestApprovalSent;
                    // console.log("No Data fetched ", $scope.RequestApprovalSent);
                    // if ($scope.RequestApprovalSent) {
                    //     $scope.MRHeader['ApprovalStatus'] = 0;
                    //     MaterialRequest.updateData($scope.MRHeader, $scope.gridDetail.data);
                    // }
                    console.log("ada 2: head ? ", RequestApprovalSentPG);
                    console.log("Request Approval is SENT ")
                    if (RequestApprovalSentPG && doProcAfterRequestApprovalSentPG) {
                        // MaterialRequest.updateData(vparApp.headerData , vparApp.detailData);
                        vparApp.procUpdateData(vparApp.headerData, vparApp.detailData);
                    }
                    return RequestApprovalSentPG; //RequestApprovalSentYes;
                });
            },
            showApproval01: function(vparApp) {
                vparApp['TypeId'] = 0;
                this.setparApproval(vparApp);
                // var RequestApprovalSentYes= false;
                ngDialog.openConfirm({
                    template: '<div ng-include=\"\'app/parts/templates01/frmApproval01.html\'\"></div>',
                    plain: true,
                    controller: vparApp.controller, //'MIPMaintenanceController',
                }).then(function(value) {
                    console.log(" t/f : ", this.RequestApprovalSent);
                    var RequestApprovalSentYes = this.RequestApprovalSent;
                    console.log("ada : ? ", RequestApprovalSentYes)
                    return RequestApprovalSentYes;
                }, function(value) {
                    console.log(" t/f 2 : ", RequestApprovalSentPG);
                    // console.log(" t/f 2 B : ", RequestApprovalSent);
                    var RequestApprovalSentYes = this.RequestApprovalSent;
                    console.log("ada 2: head ? ", doProcAfterRequestApprovalSentPG);
                    if (RequestApprovalSentPG) {
                        // MaterialRequest.updateData(vparApp.headerData , vparApp.detailData);
                        vparApp.procUpdateData(vparApp.detailData);
                        vparApp.procNext();
                    }
                    return RequestApprovalSentPG; //RequestApprovalSentYes;
                });
            },
            showApproval02: function(vparApp) { // ada tambahan vparApp.procNext dan pake form frmApproval02
                vparApp['TypeId'] = 0;
                this.setparApproval(vparApp);
                ngDialog.openConfirm({
                    template: '<div ng-include=\"\'app/parts/templates01/frmApproval02.html\'\"></div>',
                    plain: true,
                    controller: vparApp.controller, //'MIPMaintenanceController',
                }).then(function(value) {
                    console.log(" t/f : ", this.RequestApprovalSent);
                    var RequestApprovalSentYes = this.RequestApprovalSent;
                    console.log("ada : ? ", RequestApprovalSentYes)
                    return RequestApprovalSentYes;
                }, function(value) {
                    console.log(" t/f 2 : ", RequestApprovalSentPG);
                    // console.log(" t/f 2 B : ", RequestApprovalSent);
                    var RequestApprovalSentYes = this.RequestApprovalSent;
                    console.log("ada 2: head ? ", RequestApprovalSentPG);
                    console.log("Request Approval is SENT ")
                    if (RequestApprovalSentPG && doProcAfterRequestApprovalSentPG) {
                        vparApp.procUpdateData(vparApp.headerData, vparApp.detailData);
                        vparApp.procNext();
                        // for (var i=0; i< vparApp.procUpdateData.length; i++) {
                        //   if (i==0) vparApp.procUpdateData[i](vparApp.headerData , vparApp.detailData)
                        //     else {
                        //       vparApp.procUpdateData[i]();
                        //     }
                        // }
                    }
                    return RequestApprovalSentPG; //RequestApprovalSentYes;
                });
            },
            showApprovalNoDataId: function(vparApp) {
                vparApp['TypeId'] = 1;
                this.setparApproval(vparApp);
                ngDialog.openConfirm({
                    template: '<div ng-include=\"\'app/parts/templates01/frmApproval01.html\'\"></div>',
                    plain: true,
                    controller: vparApp.controller, //'MIPMaintenanceController',
                }).then(function(value) {
                    console.log("ada : ? ", RequestApprovalSentPG);
                    console.log("Request Approval is SENT ")
                    if (RequestApprovalSentPG) {
                        // vparApp.procUpdateData(vparApp.headerData , vparApp.detailData);
                        // do something
                        vparApp.ProcessData(vparApp.dataPar);
                    }
                    return RequestApprovalSentPG;
                }, function(value) {
                    console.log("ada 2: ? ", RequestApprovalSentPG);
                    console.log("Request Approval is SENT ")
                    if (RequestApprovalSentPG) {
                        // vparApp.procUpdateData(vparApp.headerData , vparApp.detailData);
                        // do something
                        vparApp.dataPar.Message = vparApp.pesan;
                        vparApp.dataPar.UserIdForApproval = vparApp.userid;
                        console.log("data par : ", vparApp.dataPar);
                        vparApp.ProcessData(vparApp.dataPar);
                    }
                    return RequestApprovalSentPG;
                });
            },
            inputAlasanBatal: function(vdata) {
                ngDialog.openConfirm({
                    template: '<div ng-include=\"\'app/parts/templates01/frmAlasanBatal01.html\'\"></div>',
                    plain: true,
                    controller: vdata.controller //,
                        // dokbatal : vdata.dokdata,
                        // dokbatal['doctype'] : vdata.doctype
                });
                dokbatal = vdata.dokdata;
                dokbatal['doctype'] = vdata.doctype;
                dokbatal['controller'] = vdata.controller;
            },
            getDokBatal: function() {
                return dokbatal;
            },
            // okBatalDok = function(data){
            // console.log("okBatalDok", data);
            // ngDialog.openConfirm ({
            //   template:'\
            //                <div align="center" class="ngdialog-buttons">\
            //                <p><b>Konfirmasi</b></p>\
            //                <p>Anda yakin akan membatalkan GI ini?</p>\
            //                <div class="ngdialog-buttons" align="center">\
            //                  <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
            //                  <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="batalGiOnForm(data)">Ya</button>\
            //                </div>\
            //                </div>',
            //   plain: true,
            //   controller: data.controller
            //  });
            // },
            setSB: function(sb) {
                // if(vparApproval.btnCancel) {parApproval.btnCancel= vparApproval.btnCancel; }
                if (sb.bl) { salesbilling.bl = sb.bl }
                if (sb.cl) { salesbilling.cl = sb.cl }
                if (sb.viewbl) { salesbilling.viewbl = sb.viewbl }
                if (sb.viewcl) { salesbilling.viewcl = sb.viewcl }
            },
            getSB: function() {
                return salesbilling;
            }
        }
    });
