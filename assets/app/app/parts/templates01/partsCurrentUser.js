angular.module('app')
    .factory('PartsCurrentUser', function($http, CurrentUser, LocalService, $window, PrintRpt) {
        var currentUser = CurrentUser.user;
        var VendorData = {};
        this.formApi = {};
        this.number = 0;

        return {
            getWarehouseData: function() {

                var LoginObject;
                var UserId;

                if (LocalService.get('BSLocalData')) {
                    LoginObject = angular.fromJson(LocalService.get('BSLocalData')).LoginData;
                }

                UserId = angular.fromJson(LoginObject).UserId;
                var res = $http.get('/api/as/PartsUser', {
                    params: {
                        UserId: (UserId == null ? "n/a" : UserId)
                    }
                });
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            getWarehouseId: function(stid, mtid, oid) {
                var res = $http.get('/api/as/PartsUser/GetWarehouse', {
                    params: {
                        ServiceTypeId: stid,
                        MaterialTypeId: mtid,
                        OutletId: oid
                    }
                });
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            getWarehouseIdi: function() {
                var res = $http.get('/api/as/PartsUser/GetWarehouseId');
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            getCurrentUserId: function() {
                var res = $http.get('/api/as/PartsUser/GetCurrentUserId');
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            getVendor: function() {
                var res = $http.get('/api/as/PartsUser/VendorLookUp');
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            getVendorOutlet: function() {
                var res = $http.get('/api/as/PartsUser/VendorOutletLookUp');
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            getVendorDetail: function(id) {
                var res = $http.get('/api/as/PartsUser/VendorLookUpDetail', {
                    params: {
                        VendorId: id
                    }
                });
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            setVendorHeader: function(data) {
                VendorData = data;
            },
            getVendorHeader: function() {
                return VendorData;
            },

            actApproval: function(data) {
                console.log("Data approval : ", data);
                var res = $http.get('/api/as/PartsUser/ActApproval', {
                    params: {
                        ProcessId: data.ProcessId,
                        DataId: data.DataId,
                        ApprovalStatus: data.ApprovalStatus
                    }
                });
                return res;
            },

            getFormatId: function(sid, doctype, materialtype) {
                materialtype = typeof materialtype !== 'undefined' ? materialtype : 1;
                console.log("materialtype==>", materialtype);
                var res = $http.get('/api/as/PartsUser/GetDocumentFormat', {
                    params: {
                        ServiceTypeId: sid,
                        DocType: doctype,
                        MaterialTypeId: materialtype
                    }
                });
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            getDocumentNumber2: function(FormatId) {
                console.log("[getDocumentNumber]");
                var res = $http.get('/api/as/PartsUser/GetDocumentNumber', {
                    params: {
                        FormatId: FormatId,
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },
            getPurchaseOrder: function(key) {
                console.log("[getPurchaseOrder]");
                var res = $http.get('/api/as/PartsUser/PurchaseOrderLookUp');
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },
            getClaimCodeData: function() {
                console.log("[getClaimCodeData]");
                var res = $http.get('/api/as/CategoryClaimCode');
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },
            getInvoiceNumber: function(key) {
                console.log("[getInvoiceNumber]");
                var res = $http.get('/api/as/PartsUser/GetReceivedInvoice', {
                    params: {
                        InvoiceId: (key == null ? "n/a" : key),
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },

            PrintOutNeedToAsk: function(PrintOutDocTypeId, DocDataId) {
                var res = $http.get('/api/as/PartGlobal/PrintOutNeedToAsk', {
                    params: {
                        PrintOutDocTypeId: PrintOutDocTypeId,
                        DocDataId: DocDataId
                    }
                });
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },

            PrintOutApproval_Ask: function(PrintOutDocTypeId, DocDataId, Message) {
                var res = $http.get('/api/as/PartGlobal/PrintOutApproval_Ask', {
                    params: {
                        PrintOutDocTypeId: PrintOutDocTypeId,
                        DocDataId: DocDataId,
                        Message: Message
                    }
                });
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },

            PrintOutApproval_Act: function(PrintOutDocTypeId, DocDataId, ApprovalStatus) {
                var res = $http.get('/api/as/PartGlobal/PrintOutApproval_Act', {
                    params: {
                        PrintOutDocTypeId: PrintOutDocTypeId,
                        DocDataId: DocDataId,
                        ApprovalStatus: ApprovalStatus
                    }
                });
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },

            PrintOutApproval_AfterPrint: function(PrintOutDocTypeId, DocDataId) {
                var res = $http.get('/api/as/PartGlobal/PrintOutApproval_AfterPrint', {
                    params: {
                        PrintOutDocTypeId: PrintOutDocTypeId,
                        DocDataId: DocDataId
                    }
                });
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },


        }
    });