// ['PO TAM', 'PO NON TAM', 'PO SUBLET', 'TRANSFER ORDER'];

angular.module('app')
    .factory('PartsLookup', function($http, bsNotify, ngDialog) {
        var RefGRType = [
            { id: '1', name: 'PO TAM' },
            { id: '2', name: 'PO NON TAM' },
            { id: '3', name: 'PO SUBLET' },
            { id: '4', name: 'TRANSFER ORDER' },
            { id: '5', name: 'CLAIM' }
        ];

        var RefGRTypePartsman = [
            { id: '1', name: 'PO TAM' },
            { id: '2', name: 'PO NON TAM' },
            { id: '4', name: 'TRANSFER ORDER' },
            { id: '5', name: 'CLAIM' }
        ];
        var MIPStatusDoc = [{ id: 1, name: "Draft" }, { id: 2, name: "Request Approval" }, { id: 3, name: "Completed" }];
        var RefMRType = [{ id: 1, name: "Appointment" }, { id: 2, name: "WO" }, { id: 3, name: "Sales Order" }];
        // from master table
        var MaterialType = [{ id: 1, name: "Part" }, { id: 2, name: "Bahan" }, { id: 3, name: "All" }];
        var ICC = [{ id: 3, name: "A" }, { id: 4, name: "B" }, { id: 5, name: "C" }, { id: 6, name: "D" }, { id: 7, name: "E" }, { id: 8, name: "F" },
            { id: 9, name: "S" }
        ]; // ClassTypeId = 2
        var SCC = [{ id: 10, name: "Overhaul Item" }, { id: 11, name: "Body Item" }, { id: 12, name: "Accessories Item" },
            { id: 13, name: "CBU Non TAM" }, { id: 14, name: "Discontinue Item" }
        ]; // ClassTypeId = 3
        var StockPolicy = [{ id: 3, name: "Stock" }, { id: 4, name: "Non Stock" }]; // ClassTypeId = 4
        return {
            lkp_truefalse: function(bool) {
                if (bool) return 1
                else return 0;
            },
            lkp_StockPolicy: function() { return StockPolicy },
            lkp_StockPolicyName: function(id) { return StockPolicy[id - 3].name; },
            lkp_MIPStatusDoc: function() { return MIPStatusDoc; },
            lkp_MIPStatusDocName: function(id) { return MIPStatusDoc[id - 1].name; },

            lkp_RefMRType: function() { return RefMRType; },
            lkp_RefMRTypeName: function(id) { return RefMRType[id - 1].name; },
            lkp_RefMRTypeId: function(name) {
                var i;
                for (i = 0; i < RefMRType.length; i++) {
                    if (RefMRType[i].name.toUpperCase() == name.toUpperCase()) {
                        return RefMRType[i].id;
                    }
                }
                return name;
            },

            lkp_MaterialType: function() { return MaterialType; },
            lkp_MaterialTypeName: function(id) { return MaterialType[id - 1].name; },
            lkp_MaterialTypeId: function(name) {
                var i = 0;
                switch (name) {
                    case MaterialType[0].name:
                        i = MaterialType[0].id;
                        break;
                    case MaterialType[1].name:
                        i = MaterialType[1].id;
                        break;
                    case "Parts":
                        i = 1;
                        break;
                }
                return i;
            },

            lkp_RefGRType: function() { return RefGRType; },
            lkp_RefGRTypePartsman: function() { return RefGRTypePartsman; },
            lkp_RefGRTypeName: function(id) { return RefGRType[id].name; },
            lkp_RefGRTypePartsmanName: function(id) { return RefGRTypePartsman[id].name; },

            lkp_ICC: function() { return ICC; },
            lkp_ICCName: function(id) {
                if (id < 3) { return "?" } else {
                    return ICC[id - 3].name;
                }
            },
            lkp_ICCId: function(name) {
                var i; // var j=0;
                for (i = 0; i < ICC.length; i++) {
                    if (ICC[i].name == name) {
                        return ICC[i].id;
                    }
                }
                return name
            },

            lkp_SCC: function() { return SCC; },
            lkp_SCCName: function(id) {
                if (id < 10) { return "?" } else {
                    return SCC[id - 10].name;
                }
            },
            lkp_SCCId: function(name) {
                var i;
                for (i = 0; i < SCC.length; i++) {
                    if (SCC[i].name == name) {
                        return SCC[i].id;
                    }
                }
                return name;
            }
        }
    });