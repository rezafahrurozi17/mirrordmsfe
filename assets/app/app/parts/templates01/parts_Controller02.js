angular.module('app')
    .controller('Parts_Controller02', function($scope, $http, CurrentUser, $timeout, ngDialog, $state, //parApproval
        PartsGlobal) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.formLookupGrid = {}
    $scope.$on('$viewContentLoaded', function() {        
        // $scope.vOutletId = PartsGlobal.OutletId;
        // $scope.VendorId = PartsGlobal.VendorId;
        // $scope.getDaftarPO();
        // $timeout(function(){
        //     $scope.formLookupGrid.data = PartsGlobal.formLookupGrid.data ;
        // });        
        
        $scope.loading=true;
        $scope.gridData=[];
    });
    $scope.mData = {};    
    $scope.filterValue="";
    // $scope.singleFilter = function( renderableRows ){
    //   var matcher = new RegExp($scope.filterValue);
    //   var field = $scope.filterColumn ;
          
    //   renderableRows.forEach( function( row ) {
    //     var match = row.entity[field].match(matcher);
        
    //     if ( !match ){
    //       row.visible = false;
    //     }
    //   });
    //   return renderableRows; 
    // };
    // $scope.filterableColumns = [{field: "PurchaseOrderNo"}, {field: "PartsCode"}];
    $scope.filterableColumns = PartsGlobal.filterableColumns ;
    $scope.getSelectedFilter= function() {
        // return 0;
        console.log("filterColumn : ",$scope.filterColumn);
    } 
    $scope.filter = function() {    
    // alert("Kolom : ", $scope.filterColumn);  
    console.log("Kolom : ", $scope.filterColumn);
    var filtercol = $scope.filterColumn ;
    // console.log("Kolom1 : ", $scope.filterValue);
    // if (typeof ($scope.filterValue) === undefined) $scope.filterValue = "";
    console.log("Filterable Kolom : ", $scope.filterableColumns) ;
    // console.log("Filterable Kolom2 : ", PartsGlobal.filterableColumns);
    var newRowsPO = $scope.formLookupGrid0.data.filter(function(row){            
        return (row.PurchaseOrderNo.lastIndexOf($scope.filterValue.toUpperCase()) >= 0);
        });  // PurchaseOrderNo
    var newRowsPC = $scope.formLookupGrid0.data.filter(function(row){            
        return (row.PartsCode.lastIndexOf($scope.filterValue.toUpperCase()) >= 0);
        });  // PartsCode  

      if ($scope.formLookupGrid.data==[] || $scope.formLookupGrid.data == null || $scope.formLookupGrid.data.length==0) {
        console.log("Mestinya sebelumnya kosong ", $scope.formLookupGrid.data.length);
        if (newRowsPO.length==0 && newRowsPC.length==0) {
            $scope.formLookupGrid.data = $scope.formLookupGrid0.data;
        } else {
            if (filtercol == "PurchaseOrderNo")
                $scope.formLookupGrid.data = newRowsPO ;
            else $scope.formLookupGrid.data = newRowsPC ;
        }
      } else {
        console.log("Mestinya sebelumnya Ada isinya ", $scope.formLookupGrid.data.length);
        if (filtercol == "PurchaseOrderNo")
            $scope.formLookupGrid.data = newRowsPO ;
        else 
            $scope.formLookupGrid.data = newRowsPC ;
      }
      $scope.gridApi.grid.refresh();
    };  
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.baseConfirmTemplate= '\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Anda yakin akan membatalkan <<DOC>> ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="executeBatalDok()">Ya</button>\
                     </div>\
                     </div>';
    $scope.user = CurrentUser.user();
    $scope.mRole = null; //Model mData.
    $scope.date = new Date(); 
    $scope.xRole={selected:[]};  
    // $scope.vOutletId = PartsGlobal.OutletId;
    // $scope.VendorId = PartsGlobal.VendorId;
    $scope.formLookupTitle = PartsGlobal.formLookupTitle;    
    $scope.formLookupGrid0 = PartsGlobal.formLookupGrid; 
    $scope.formLookupSubTitle1= PartsGlobal.formLookupSubTitle1;//"  Daftar PO untuk Outlet : " ;//+ PartsGlobal.formLookupGrid.data[0].OutletId; 
    $scope.formLookupSubTitle2= PartsGlobal.formLookupSubTitle2;
    $scope.formLookupShowFilter= PartsGlobal.formLookupShowFilter;
    // $scope.filterableColumns = PartsGlobal.filterableColumns; 
    $scope.formLookupGrid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: PartsGlobal.formLookupGrid.multiSelect,
        enableSelectAll: false,
        enableColumnResize: true, 
        // enableHorizontalScrollbar: 1,        
        onRegisterApi: function (gridApi) {
          $scope.gridApi = gridApi;          
          // $scope.gridApi.grid.registerRowsProcessor($scope.singleFilter, 200 );
          gridApi.selection.on.rowSelectionChanged($scope,function(rows){
              $scope.mySelections = gridApi.selection.getSelectedRows();
              console.log("Item(s) : ", $scope.mySelections); // ...
              // PartsGlobal.selectedPO = $scope.mySelections;
              PartsGlobal.selectedLookupGrid = $scope.mySelections;
              //build aray string  selectedLookupGrid
              //$scope.itemids= $scope.getSelectedItemIds($scope.mySelections);
              //console.log("item ids : ", $scope.itemids);
          });
        },
        columnDefs: PartsGlobal.formLookupGrid.columnDefs, 
        // [
        //   { name:'No. PO', field: 'PurchaseOrderId', visible: false},
        //   { name:'No. PO', field: 'PurchaseOrderNo', minWidth: 150, width: "*"},          
        //   { name:'No. PO Item', field: 'PurchaseOrderItemId', visible: false},
        //   { name:'Tgl. PO', field: 'DocDate', minWidth: 100, width: "*", cellFilter: 'date:\'yyyy-MM-dd\''},
        //   { name:'Outlet', field: 'OutletId', minWidth: 100, width: "*"},
        //   { name:'Vendor', field: 'VendorId', minWidth: 100, width: "*", visible: false},
        //   { name:'Vendor', field: 'VendorCode', minWidth: 100, width: "*"},
        //   { name:'Material Type', field: 'MaterialTypeId', minWidth: 100, width: "*"},
        //   { name:'Ref No PO', displayName: 'No. Referensi SOP',  field: 'RefPONo', minWidth: 150, width: "*"},
        //   { name:'No. Material', displayName: 'No. Material',  field: 'PartId', visible: false},
        //   { name:'No. Material', displayName: 'No. Material',  field: 'PartsCode', minWidth: 100, width: "*"},
        //   { name:'Nama Material', displayName: 'Nama Material',  field: 'PartsName', minWidth: 150, width: "*"},
        //   { name:'Qty PO', field: 'QtyPO', minWidth: 100, width: "*"},
        //   { name:'Qty Remain', field: 'QtyRemain', minWidth: 100, width: "*"},
        //   { name:'Qty GR', field: 'QtyGR', minWidth: 100, width: "*"},
        //   { name:'Satuan', field: 'UomPOId', minWidth: 100, width: "*", visible: false}, //SatuanPO
        //   { name:'Satuan', field: 'SatuanPO', minWidth: 100, width: "*"},
        //   { name:'POTypeId', field: 'POTypeId', widht: 100, visible: false},
        //   { name:'Lokasi Gudang', field: 'WarehouseId', widht: 100, visible: false},          
        //   { name:'Lokasi Gudang', field: 'WarehouseCode', widht: 100, visible: false},
        //   { name:'Lokasi Gudang', field: 'WarehouseName', minWidth: 150, width: "*"},
        //   //{ name:'Lokasi Rak', field: 'friends[0]' },
        // ],
        data : []//[{"PurchaseOrderItemId":1234,"PurchaseOrderId":2519,"PurchaseOrderNo":"214/POG/1712-000192","OutletId":280,"WarehouseId":1018,"WarehouseCode":"GB001","WarehouseName":"Gudang Bahan GR","DocDate":"2017-12-10T00:00:00","MaterialTypeId":2,"POTypeId":4,"RefPOTypeId":1,"RefPONo":"214/ANG/1712-000056","VendorId":1030,"VendorCode":"V010","VehicleId":0,"PartId":3819,"PartsCode":"VB000-00002","PartsName":"AMPLAS BULAT 220","UomPOId":4,"SatuanPO":"Pieces","QtyPO":2.0000,"QtyRemain":2.0000,"QtyGR":0.0000,"InvoiceId":null,"InvoiceNo":null,"InvoiceDate":null,"ShelfName":"Radin Inten Rak"},{"PurchaseOrderItemId":3182,"PurchaseOrderId":2519,"PurchaseOrderNo":"214/POG/1712-000192","OutletId":280,"WarehouseId":1018,"WarehouseCode":"GB001","WarehouseName":"Gudang Bahan GR","DocDate":"2017-12-10T00:00:00","MaterialTypeId":2,"POTypeId":4,"RefPOTypeId":1,"RefPONo":"214/ANG/1712-000056","VendorId":1030,"VendorCode":"V010","VehicleId":0,"PartId":3820,"PartsCode":"VB000-00003","PartsName":"AMPLAS BULAT 3000","UomPOId":4,"SatuanPO":"Pieces","QtyPO":1.0000,"QtyRemain":1.0000,"QtyGR":0.0000,"InvoiceId":null,"InvoiceNo":null,"InvoiceDate":null,"ShelfName":"Radin Inten Rak"},{"PurchaseOrderItemId":3192,"PurchaseOrderId":2529,"PurchaseOrderNo":"214/POG/1712-000212","OutletId":280,"WarehouseId":1018,"WarehouseCode":"GB001","WarehouseName":"Gudang Bahan GR","DocDate":"2017-12-12T00:00:00","MaterialTypeId":2,"POTypeId":4,"RefPOTypeId":1,"RefPONo":"214/ANG/1712-000069","VendorId":1030,"VendorCode":"V010","VehicleId":0,"PartId":3820,"PartsCode":"VB000-00003","PartsName":"AMPLAS BULAT 3000","UomPOId":4,"SatuanPO":"Pieces","QtyPO":2.0000,"QtyRemain":2.0000,"QtyGR":0.0000,"InvoiceId":null,"InvoiceNo":null,"InvoiceDate":null,"ShelfName":"Radin Inten Rak"}]//$scope.getDaftarPO()  //$scope.getDatax()// PartsGlobal.formLookupGrid.data//[] 
      }; 
      // $scope.filterColumn = $scope.gridOptions.columnDefs[0].field;
      // $scope.filterColumn = $scope.gridLookUpPO.columnDefs[0].field;
    $scope.filterColumn = $scope.formLookupGrid.columnDefs[0].field;
    // $scope.getDatax= function() {
    $scope.formLookupGrid.data = $scope.formLookupGrid0.data ;
    // }
    // $scope.getDaftarPO= function() {
    //   // var vVendorId = $scope.mData.VendorId;
    //   // if (vVendorId== null || vVendorId="") vVendorId = "0"; 
    //   var parDataPO= {OutletId: $scope.vOutletId, VendorId : ($scope.VendorId ==null || $scope.VendorId ==""?0 :$scope.VendorId)};
    //   PartsGlobal.getDaftarPO(parDataPO).then(function(res) {
    //       var gridData = res.data.Result;          
    //       console.log("Daftar PO : ", gridData);          
    //       if (res.length==0) {
    //         $scope.formLookupGrid.data = [];
    //       }else {
    //         $scope.formLookupGrid.data = gridData;
    //       }
    //       $scope.loading = false;
    //     },
    //     function(err) {
    //       console.log("err=>", err);
    //     }
    //   ); 
    // } 
    // $scope.formLookupGrid.data = PartsGlobal.formLookupGrid.data ;
    // PartsGlobal.formLookupGrid.data
    // $scope.subtitle= 'testt';//;parApproval.subtitle; 
    $scope.showApproval = function(vst, vkeg, vnoreq, vapprover, vpesan){
        parApproval.setparApproval(vst, vkeg, vnoreq, vapprover, vpesan);
        ngDialog.openConfirm ({
        //template:'<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Permohonan Approval</h3></div> <div class="panel-body"><form method="post"><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nama Kegiatan</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">MIP Maintenance</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nomor Request</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">-</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Tanggal dan Jam</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">-</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Approve(s)</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4"></textarea></div></div> <br><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Pesan</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4">Mohon untuk approve list dari item Perhitungan MIP berikut. Terima kasih.</textarea></div></div> <br><div class="row"><div class="col-md-12"><div class="form-group"><div> <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Kembali</button> <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()" style="color:white; background-color:#D53337">OK</button> </div></div></div></div></form></div></div>',
        template:'<div ng-include=\"\'app/parts/templates01/frmApproval01.html\'\"></div>',
        plain: true,
        controller: 'Parts_Controller01', //'MIPMaintenanceController',
       });
    }
    var parApproval= PartsGlobal.getparApproval();
    $scope.subtitle= parApproval.subtitle;
    $scope.kegiatan= parApproval.kegiatan;
    $scope.mData.noreq= parApproval.noreq;
    $scope.mData.approver= parApproval.approver;
    $scope.mData.pesan= parApproval.pesan + ' Terima kasih.';
    $scope.btnOK= parApproval.btnOK;
    $scope.btnCancel= parApproval.btnCancel;
    $scope.getdocname= function(doctype) {
        var docname='';
        switch (doctype)
        {
            case "GR" : docname = "Goods Receipt"; break;
            case "MR" : docname = "Material Request"; break;
            case "GI" : docname = "Goods Issue"; break;
            case "PO" : docname = "Purchase Order"; break;
            case "SO" : docname = "Sales Order"; break;
        }
        return docname;
     };
    $scope.confirmBatalDok = function(data){
      var strTemplate = $scope.baseConfirmTemplate.replace("<<DOC>>", $scope.getdocname(data.doctype));
      ngDialog.openConfirm ({ 
        template : strTemplate,        
        plain: true,
        controller: data.controller,
       });
    };    
    $scope.batalApproval = function(){
        alert('Permohonan approval batal dikirim'); // 
        $scope.ngDialog.close();
        var vApproval = PartsGlobal.getparApproval();
        if (vApproval.TypeId== 0) PartsGlobal.askApproval({});        
        PartsGlobal.RequestApprovalSent= false;
        PartsGlobal.setReqAppSentPG(false);                
    }; 

    $scope.okSentApproval = function(){
        // alert('Permohonan approval telah dikirim'); ...
        // $scope.ngDialog.close();
        var vApproval = PartsGlobal.getparApproval();
        if (vApproval.TypeId== 0) PartsGlobal.askApproval({});        
        PartsGlobal.RequestApprovalSent= true;
        PartsGlobal.setReqAppSentPG(true);
        $state.go('app.parts_salesbilling');  
        $scope.ngDialog.close();               
        //PartsGlobal.setdoProcAfterRequestApprovalSentPG(vApproval.doProc)
        // PartsGlobal.showAlert({
        //     title:'Approval', 
        //     content:'Permohonan approval telah dikirim',
        //     type:'info'
        // });        
    }; 
    $scope.DoApprove = function(approval,data){
        
        var row = data;
        alert('Permohonan di approve');
        console.log(PartsGlobal.selectedLookupGrid);
        $scope.ngDialog.close();
        var vApproval = PartsGlobal.getparApproval();
        PartsGlobal.statusApproval = approval;

        var modulProcessId = row[0].StockReservedId ? 8020 : 8131;

        PartsCurrentUser.actApproval({ 
            ProcessId: modulProcessId, DataId: row[0].StockReservedId || row[0].TransferOrderId , ApprovalStatus: 1
        })
        // if (vApproval.TypeId== 0) PartsGlobal.askApproval({});        
        // PartsGlobal.RequestApprovalSent= true;
        // PartsGlobal.setReqAppSentPG(true);             
    };
    $scope.DoReject = function(approval,data){

        var row = data;
        alert('Permohonan ditolak');
        $scope.ngDialog.close();
        var vApproval = PartsGlobal.getparApproval();
        PartsGlobal.statusApproval = approval;

        var modulProcessId = row[0].StockReservedId ? 8020 : 8131;

        PartsCurrentUser.actApproval({ 
            ProcessId: modulProcessId, DataId: row[0].StockReservedId || row[0].TransferOrderId , ApprovalStatus: 2
        })

        // if (vApproval.TypeId== 0) PartsGlobal.askApproval({});        
        // PartsGlobal.RequestApprovalSent= true;
        // PartsGlobal.setReqAppSentPG(true);             
    };
    $scope.okBatalDok = function(){
        $scope.dokbatal = PartsGlobal.getDokBatal();
        console.log("data batal : ", $scope.dokbatal);
        $scope.confirmBatalDok($scope.dokbatal);        
    }
    $scope.okFormLookup = function(){ 
        var windowIDs = ngDialog.getOpenDialogs();
        // $scope.ngDialog.close();  //(windowIDs[1]
        $scope.ngDialog.close(windowIDs[windowIDs.length -1]);
        PartsGlobal.cancelledLookupGrid = false;
        return 1;
    }
});
