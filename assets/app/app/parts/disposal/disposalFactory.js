angular.module('app')
  .constant("constantDisposal", {
    "tipeReferensi": [{"id": 1, "name": "Parts Claim"}, {"id": 2, "name": "Langsung"}],
    "tipeMaterial_Parts" : [{"id": 1, "name":"Parts"}],
    "alasan": [{"id": "1", "name":"Material Rusak"}, {"id": "2", "name":"Scrap"}, {"id": "3", "name":"Lainnya"}],
    "Status": [
      {"id":0, "name":"Cancelled"}, 
      {"id":1, "name":"Completed"}
    ]
  })
  .factory('Disposal', function($http, CurrentUser, constantDisposal, LocalService, $filter) {
    var currentUser = CurrentUser.user;
    var DHeader = {};
    var DisposalHeader = {};
    var DisposalDetail = {};
    this.formApi = {};  
    
    // console.log(currentUser);

    
    return { 
      getData: function(data) {
        console.log("<factory> data => ", data);                        
        //console.log("<factory> n => ", n);
        var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
        var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');
        //console.log("<factory> endDate => ", endDate);
        if(data.MaterialTypeId == 3 || data.ServiceTypeId == 3){
          data.WarehouseId = "n/a";
        }
        if(data.WarehouseId === undefined){
          data.WarehouseId = "n/a";
        }
        console.log("<factory> data.WarehouseId => ", data.WarehouseId);
        var res=$http.get('/api/as/Disposal', {params: {
                                                          WarehouseId: data.WarehouseId,// (data.WarehouseId == null ? "n/a" : data.WarehouseId),
                                                          RefSDTypeId : (data.fRefSDTypeId == null ? "n/a" : data.fRefSDTypeId),
                                                          StockDisposalReasonId : (data.fStockDisposalReasonId == null ? "n/a" : data.fStockDisposalReasonId),
                                                          StockDisposalStatusId: (data.fStockDisposalStatusId == null ? "n/a" : data.fStockDisposalStatusId),
                                                          RefSDNo : (data.fRefSDNo == null ? "n/a" : data.fRefSDNo),
                                                          StockDisposalNo : (data.fStockDisposalNo == null ? "n/a" : data.fStockDisposalNo),
                                                          startDate: (data.startDate == null ? "n/a" : startDate),
                                                          endDate: (data.endDate == null ? "n/a" : endDate)
                                                                                                                    
                                                        } });
        console.log('<factory> hasil => ', res);
        //res.data.Result = null;
        return res;
      },
      getDetail: function(data){
        console.log("<factory> getData detail => ", data); 
        console.log("<factory> StockDisposalId => ", data.StockDisposalId); 
        console.log("<factory> RefSDNo => ", data.RefSDNo); 
        var res=$http.get('/api/as/Disposal/GetStockDisposalItem', {params: {
                                                          StockDisposalId: data.StockDisposalId,
                                                          RefSDNo: data.RefSDNo //(data.RefSDNo == null ? 0 : data.RefSDNo) 
                                                        } });
        console.log('dataDetail =>',res);
        return res;

      },
      create: function(data, detail) {
        console.log('tambah data => ', data);
        var vdocDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
        return $http.post('/api/as/Disposal', [{
                                            //StockDisposalId: (data.StockDisposalId == null ? 0 : data.StockDisposalId),
                                            StockDisposalNo: (data.StockDisposalNo == null ? 0 : data.StockDisposalNo),
                                            OutletId: (data.OutletId == null ? 0 : data.OutletId),
                                            WarehouseId: (data.WarehouseId == null ? 0 : data.WarehouseId),
                                            DocDate: vdocDate, //(data.DocDate == null ? "2017-04-01" : data.DocDate),
                                            MaterialTypeId: (data.MaterialTypeId == null ? 0 : data.MaterialTypeId),
                                            RefSDTypeId: data.RefSDTypeId, //(data.RefSDTypeId == null ? 0 : data.RefSDTypeId),
                                            RefSDNo: (data.RefSDNo == null ? "-" : data.RefSDNo),
                                            StockDisposalReasonId: (data.StockDisposalReasonId == null ? 0 : data.StockDisposalReasonId),
                                            Notes: (data.Notes == null ? '' : data.Notes),
                                            StockDisposalStatusId: (data.StockDisposalStatusId == null ? 1 : data.StockDisposalStatusId),
                                            //MaterialRequestId: (data.MaterialRequestId == null ? 0 : data.MaterialRequestId),
                                            CancelReasonDesc: data.CancelReasonDesc,
                                            GridDetail: detail
                                          }]);
      },
      createDetail: function(data) {
        console.log('tambah data grid detail=>', data);
        return $http.post('/api/as/Disposal/InsertStockDisposalItem', data);
      },
      cancel: function(data){
        console.log('Update => ', data);        
        return $http.put('/api/as/Disposal', [{
                                            StockDisposalId: (data.StockDisposalId == null ? 0 : data.StockDisposalId),
                                            StockDisposalNo: data.StockDisposalNo, //(data.StockDisposalNo == null ? 0 : data.StockDisposalNo),
                                            OutletId: data.OutletId, //(data.OutletId == null ? 0 : data.OutletId),
                                            WarehouseId: data.WarehouseId, //(data.WarehouseId == null ? 0 : data.WarehouseId),
                                            DocDate: data.DocDate, //(data.DocDate == null ? "1901-01-01" : data.DocDate),
                                            MaterialTypeId: data.MaterialTypeId, //(data.MaterialTypeId == null ? 0 : data.MaterialTypeId),
                                            RefSDTypeId: data.RefSDTypeId, //(data.RefSDTypeId == null ? 0 : data.RefSDTypeId),
                                            RefSDNo: (data.RefSDNo == null ? "-" : data.RefSDNo),
                                            StockDisposalReasonId: data.StockDisposalReasonId, //(data.StockDisposalReasonId == null ? '' : data.StockDisposalReasonId),
                                            Notes: data.Notes, //(data.Notes == null ? '' : data.Notes),
                                            StockDisposalStatusId: data.StockDisposalStatusId, //(data.StockDisposalStatusId == null ? 0 : data.StockDisposalStatusId)
                                            CreatedDate: data.CreatedDate,
                                            CreatedUserId: data.CreatedUserId,
                                            StatusCode: data.StatusCode,
                                            CancelReasonId: data.Batal,
                                            CancelReasonDesc: data.Catatan
                                          }]);
      },

      SearchNoPartsClaimDMS: function(data) {
        console.log("<factory> SearchNoPartsClaimDMS => ", data);        
        var res = $http.get('/api/as/Disposal/SearchNoPartsClaimDMS', {params: {RefSDNo : data}});
        console.log('<factory> SearchNoPartsClaimDMS - hasil => ', res);
        return res;
      },
      GetPartsClaim: function() {
        console.log("<factory> GetPartsClaim ");        
        var res = $http.get('/api/as/Disposal/GetPartsClaim');
        console.log('<factory> res', res);
        return res;
      },
      GetPartsClaimItem: function(StockReturnClaimId) {
        console.log("<factory> GetPartsClaimItem ", StockReturnClaimId);        
        var res = $http.get('/api/as/Disposal/GetPartsClaimItem', {params: {StockReturnClaimId : StockReturnClaimId}});
        console.log('<factory> res', res);
        return res;
      },

      getDocumentNumber: function(FormatId) {
        //console.log("[getDocumentNumber]");
        console.log('getDocumentNumber FormatId', FormatId);
        var res=$http.get('/api/as/PartsUser/GetDocumentNumber', {params: {
                                                          FormatId : FormatId,
                                                        } });
        console.log('hasil=>',res);
        return res;
      },

      // Detail 
      getDetailByMaterialNo: function(data){
        console.log("[getDetail]");
        console.log(data);
        var res=$http.get('/api/as/Disposal/GetDisposalItemFromMaterial', {params: {
                                                          PartsCode : data, 
                                                        } });
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;

      },

      //===================
      // Approval Print-out
      //=================== 
      GetPrintOutNeedToAsk: function(PrintOutDocTypeId, StockDisposalId){
        console.log("GetPrintOutNeedToAsk");
        console.log("PrintOutDocTypeId", PrintOutDocTypeId);
        console.log("StockDisposalId", StockDisposalId);
        var res=$http.get('/api/as/PartGlobal/PrintOutNeedToAsk', {params: {
                                                          PrintOutDocTypeId: PrintOutDocTypeId, 
                                                          DocDataId: StockDisposalId
                                                        } });
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      GetPrintOutApproval_Ask: function(PrintOutDocTypeId, StockDisposalId, Message){
        console.log("GetPrintOutApproval_Ask");
        console.log("PrintOutDocTypeId", PrintOutDocTypeId);
        console.log("StockDisposalId", StockDisposalId);
        console.log("Message", Message);
        var res=$http.get('/api/as/PartGlobal/PrintOutApproval_Ask', {params: {
                                                          PrintOutDocTypeId: PrintOutDocTypeId, 
                                                          DocDataId: StockDisposalId,
                                                          Message: Message
                                                        } });
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      getLastApproval: function(DataId, PrintOutDocTypeId) {
        console.log('DataId', DataId);
        var res=$http.get('/api/as/Disposal/LastApproval', {params: {
                                                          DataId : DataId,
                                                          PrintOutDocTypeId : PrintOutDocTypeId
                                                        } });
        console.log('hasil=>',res);
        return res;
      },
      getNeedApprKabeng: function(DataId) {
        console.log('DataId', DataId);
        var res=$http.get('/api/as/Disposal/NeedApprKabeng', {params: {
                                                          DataId : DataId,
                                                        } });
        console.log('hasil=>',res); 
        return res;
      },
      actApprSD: function (ProcessId, DataId, ApprovalStatus) { 
        //console.log("Data approval : ", data);
        var res = $http.get('/api/as/PartGlobal/ActApproval', {params: {               
               ProcessId: ProcessId, DataId: DataId, ApprovalStatus: ApprovalStatus
               }
        });
        return res;
      },

      setDHeader : function(data){
        for(var k in data) DHeader[k]=data[k];
        console.log('data setHeader = ', DHeader.StockDisposalStatusId);
        DHeader['disposalStatusName'] = constantDisposal.Status[DHeader.StockDisposalStatusId].name;
        console.log('data setHeader name = ', DHeader['disposalStatusName']);
      },
      getDHeader : function(){
        return DHeader;
      },

      setDisposalHeader : function(data){
        DisposalHeader = data;
      },
      getDisposalHeader : function(){
        return DisposalHeader;
      },
      setDisposalDetail : function(data){
        DisposalDetail = data;
      },
      getDisposalDetail : function(){
        return DisposalDetail;
      },


      getCurrentUser: function() {
        console.log("[getCurrentUser]");
        var res=$http.get('/api/as/PartsUser');
        console.log('hasil=>',res);
        return res;
      },

      sendNotif: function(data, recepient) {
        // console.log("model", IdSA);
        // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
        return $http.post('/api/as/SendNotification', [{
            Message: data,
            RecepientId: recepient,
            Param : 9
        }]);
      }

    }
  });
