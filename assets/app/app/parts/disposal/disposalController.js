angular.module('app')
  .controller('DisposalController', function ($scope, $http, $filter, CurrentUser, Disposal, ngDialog, constantDisposal, PartsCurrentUser, PartsGlobal, bsNotify, LocalService, $timeout, PrintRpt) {
    //Options Switch
    $scope.items = ['Parts Claim', 'Langsung'];
    $scope.selection = $scope.items[0];

    $scope.actionVisible = false;
    $scope.mySelections = [];
    $scope.fData = [];

    $scope.isOverlayForm = false;

    $scope.dAlasan = constantDisposal.alasan;

    $scope.dStatus = constantDisposal.Status;

    $scope.ApprovalProcessId = 8120;
    $scope.obj = {};
    // $scope.DHeader = Disposal.getDHeader(mData);
    // console.log('Dheader = ', $scope.DHeader)

    //tipe referensi
    $scope.dataTipeReferensi = [
      { id: 1, name: 'Parts Claim' },
      { id: 2, name: 'Langsung' }
    ];

    //tipe material
    $scope.dataTipeMaterial = [
      { id: 1, name: 'Parts' },
      { id: 2, name: 'Bahan' }
    ];

    //tipe material
    $scope.dataJenisAlasan = [
      { id: 1, name: "Material Rusak" },
      { id: 2, name: "Scrap" },
      { id: 3, name: "Lainnya" }
    ];

    // Status
    $scope.dataStatus = [
      { id: 1, name: "Request Approval" },
      { id: 2, name: "Completed" },
      { id: 3, name: "Cancelled" }
    ];
    $scope.disaView = false;
    //model batal
    $scope.Alasan = {};
    $scope.BatalData = Disposal.getDisposalHeader();

    $scope.onRegisterApi = function (gridApi) {
      $scope.myGridApi = gridApi;
    };

    $scope.afterRegisterGridApi = function (gridApi) {
      console.log(gridApi);
    }

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function () {
      $scope.loading = false;
      $scope.disaView = false;
      $scope.mData.WarehouseId = {};

      $timeout(function () {
        $scope.GetNoPartClaim();

        var parts = $scope.checkRights(10);
        var bahan = $scope.checkRights(12);
        console.log("parts = ", parts);
        console.log("bahan = ", bahan);
        console.log('user=', $scope.user);

        if ($scope.user.RoleId == 1135) {
          $scope.mData.MaterialTypeId = 1;
          $scope.mData.ServiceTypeId = 1;
        } else if ($scope.user.RoleId == 1122) {
          $scope.mData.MaterialTypeId = 1;
          $scope.mData.ServiceTypeId = 1;
        } else if ($scope.user.RoleId == 1123) {
          $scope.mData.MaterialTypeId = 1;
          $scope.mData.ServiceTypeId = 0;
        } else if ($scope.user.RoleId == 1125) {
          $scope.mData.MaterialTypeId = 2;
          $scope.mData.ServiceTypeId = 1;
        }  else if ($scope.user.RoleId == 1124) {
          $scope.mData.MaterialTypeId = 2;
          $scope.mData.ServiceTypeId = 0;
        } else if ($scope.user.RoleId == 1128) { //Kabeng-GR
          $scope.mData.MaterialTypeId = 1;
          $scope.mData.ServiceTypeId = 1;
        } else if ($scope.user.RoleId == 1021) { //ADH-GR
          $scope.mData.MaterialTypeId = 1;
          $scope.mData.ServiceTypeId = 1;
        }

        // $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
        // $scope.mData.ServiceTypeId = $scope.getRightServiceType();
        console.log("MaterialTypeId : ", $scope.mData.MaterialTypeId);
        console.log("ServiceTypeId : ", $scope.mData.ServiceTypeId);
        if ($scope.mData.MaterialTypeId == 2) {
          $scope.items = ['Langsung'];
        } else {
          $scope.items = ['Parts Claim', 'Langsung'];
        }
        console.log('$scope.items', $scope.items);
        $scope.mData.OutletId = $scope.user.OrgId;
        console.log("OutetId : ", $scope.mData.OutletId);
        var n = 0;
        PartsCurrentUser.getWarehouseIdi().then(function (res) {
             var gridData = res.data;
            console.log('kamu kayak kobokan gridData[0] = ', gridData[0],gridData);
            $scope.mData.WarehouseId = gridData[0].WarehouseId;
            $scope.mData.MaterialTypeId =  gridData[0].MaterialType;
            $scope.mData.ServiceTypeId =  gridData[0].Servicetype;
            //$scope.mData.WarehouseData = gridData;
            $scope.mData.OutletId = $scope.user.OrgId;
            console.log('<getWarehouseId> WarehouseId = ', $scope.mData.WarehouseId);
            n = $scope.mData.WarehouseId;
            console.log('<getWarehouseId> n = ', n);
        });
        // PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.mData.OutletId).then(function (res) {
        //   var gridData = res.data;

        //   console.log('kamu kayak kobokan gridData[0] = ', gridData[0],gridData);

        //   $scope.mData.WarehouseId = gridData[0].WarehouseId;
        //   //$scope.mData.WarehouseData = gridData;
        //   $scope.mData.OutletId = $scope.user.OrgId;
        //   console.log('<getWarehouseId> WarehouseId = ', $scope.mData.WarehouseId);

        //   n = $scope.mData.WarehouseId;
        //   console.log('<getWarehouseId> n = ', n);
        // },
        //   function (err) {
        //     console.log("err=>", err);
        //   }
        // );
        $scope.obj.test = 1;
        // $scope.getGridFirstLoad();

      });
    });

    function setDate(dt) {
      var d = "";
      dt = new Date(dt);
      d = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
      return d;
    }

    $scope.localeDate = function(data) {
      var tmpDate = new Date(data);
      var resDate = new Date(tmpDate.toISOString().replace("Z","-0700")).toISOString(); 
      return resDate;
    }

    $scope.isoDate = function(data) {
      var tmpDate = new Date(data);
      var resDate = new Date(tmpDate.toISOString().replace("Z","+0700")).toISOString(); 
      return resDate;
    }

    var getDataGlob = [];
    $scope.getGridFirstLoad = function () {
      $scope.fData.fStockDisposalStatusId = 1;
      console.log("$scope.mData=>", $scope.fData);
      $scope.grid.data = [];
      getDataGlob = [];
      Disposal.getData($scope.fData).then(function (res) { //start getdata
        var gridData = res.data.Result;
        console.log("<controller getData> GridData => ", gridData,$scope.user.RoleId);
        for (var i in gridData) {
          if (gridData[i].ApprovalRoleId != $scope.user.RoleId) {
            console.log('masuk ApprovalRoleId',gridData[i]);
            getDataGlob.push(gridData[i]);
          }
          gridData[i].CreatedDate = $scope.localeDate(gridData[i].CreatedDate);
        }
        $scope.grid.data = getDataGlob;
        console.log("$scope.gridApiVC",$scope.gridApiVC);
        // $scope.gridApiVC.core.refresh();
        // $scope.gridApiVC.core.refreshRows();
        console.log('dadatta', getDataGlob,$scope.grid.data);
        $scope.loading = false;
      },
        function (err) {
          console.log("err=>", err);
        }
      );
    }

    $scope.checkRights = function (bit) {
      var p = $scope.myRights & Math.pow(2, bit);
      var res = (p == Math.pow(2, bit));
      console.log("myRights => ", $scope.myRights);
      return res;
    }
    $scope.getRightX = function (a, b) {
      var i = 0;
      if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
      else if ($scope.checkRights(b)) i = 2;
      else if ($scope.checkRights(a)) i = 1;
      return i;
    }
    $scope.getRightMaterialType = function () {
      if ($scope.user.RoleId == 1122) {
        var i = $scope.getRightX(8, 9);
        console.log('masuk 1122');
      } else if ($scope.user.RoleId == 1125) {
        var i = $scope.getRightX(8, 10);
      }

      return i;
    }
    $scope.getRightServiceType = function () {
      var i = $scope.getRightX(10, 11);
      return i;
    }

    $scope.onBulkApproveDS = function (mData) {
      console.log("mData : ", mData);
      var DataId = mData[0].StockDisposalId;
      Disposal.getNeedApprKabeng(DataId).then(function(res) {
        console.log('res getNeedApprKabeng = ', res);
        var NeedApprovedKabeng = res.data.Result["0"].NeedApprovedKabeng;
        if(NeedApprovedKabeng == 1) {
          bsNotify.show(
            {
                title: "Need Approval",
                content: "Need Approve Kepala Bengkel",
                type: 'warning'
            }
          );
        } else {
          if (mData[0].StockDisposalStatusId == 1) {
            $scope.ApproveDS(mData);
          } else if (mData[0].StockDisposalStatusId == 2) {
            bsNotify.show(
                {
                    title: "Disposal - Approved",
                    content: "Disposal sudah disetujui",
                    type: 'danger'
                });
            return;
          } else if (mData[0].StockDisposalStatusId == 3) {
            bsNotify.show(
                {
                    title: "Disposal - Cancelled",
                    content: "Disposal sudah dicancel",
                    type: 'danger'
                });
            return;
          } else if (mData[0].StockDisposalStatusId == 13) {
            bsNotify.show(
                {
                    title: "Disposal - Rejected",
                    content: "Disposal sudah ditolak",
                    type: 'danger'
                });
            return;
          }
        } 
      },
      function(err) {
        console.log("err=>", err);
      });
    }

    // approve
    $scope.ApproveDS = function (data) {
      console.log('ApproveDS data = ', data);
      // data[0].DocDate = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss');
      data[0].DocDate = $scope.localeDate(new Date());
      $scope.ApproveData = data[0];
      //console.log('$scope.ApproveData = ', $scope.ApproveData);
      if ($scope.ApproveData.RefSDTypeId == 1) {
        $scope.ApprovalProcessId = 8120;
      } else if ($scope.ApproveData.RefSDTypeId == 2) {
        $scope.ApprovalProcessId = 8125;
      }
      console.log('$scope.ApproveData = ', $scope.ApproveData);
      Disposal.setDisposalHeader($scope.ApproveData);
      ngDialog.openConfirm({
        template: '\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Approve dokumen Disposal ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinApprove(ApproveData)">Ya</button>\
                     </div>\
                     </div>',
        plain: true,
        scope:$scope
        // controller: 'DisposalController',
      });
      console.log('$scope.ApproveData bawah = ', $scope.ApproveData);
    };

    $scope.yakinApprove = function (data) {
      console.log('yakinApprove => ', data);
      $scope.mData = data;
      console.log('yakinApprove mData = ', $scope.mData);
      // // for approval
      // PartsGlobal.actApproval({
      //   ProcessId: $scope.ApprovalProcessId, DataId: $scope.mData.StockDisposalId, ApprovalStatus: 1
      // })
      if ($scope.mData.RefSDTypeId == 1) {
        $scope.ApprovalProcessId = 8120;
      } else if ($scope.mData.RefSDTypeId == 2) {
        $scope.ApprovalProcessId = 8125;
      }
      var APId = $scope.ApprovalProcessId;
      var SDId = $scope.mData.StockDisposalId;
      console.log('APId => ', APId);
      console.log('SDId => ', SDId);
      Disposal.actApprSD(APId, SDId, 1).then(function (res) {
        console.log('res PartsGlobal.actApproval = ', res);

        var DataId = $scope.mData.StockDisposalId;
        Disposal.getLastApproval(DataId, $scope.PrintOutDocTypeId).then(function (res) {
          console.log('res getLastApproval = ', res);
          var WFSeq = res.data.Result["0"].WorkFlowSequence;
          var IsApproved = res.data.Result["0"].IsApproved;

          if (WFSeq == 2 && IsApproved == 1) {
            $scope.mData.StockDisposalStatusId = 2; //status = Complete
            // [START] PUT
            Disposal.cancel($scope.mData).then(function (res) {
              console.log('res cancel = ', res);
              bsNotify.show(
                {
                  title: "Approved",
                  content: "Stock Disposal has been successfully approved.",
                  type: 'succes'
                }
              );
            },
              function (err) {
                console.log("err=>", err);
              }
            );
          } else {
            console.log('Not Last Approval');
            //kesini RIZMA
            $scope.mData.DocDate = $filter('date')($scope.mData.DocDate, 'yyyy-MM-dd');
            console.log("ini mdata",$scope.mData );
            Disposal.cancel($scope.mData).then(function (res) {
              console.log('res cancel = ', res);
              if (res.data.ResponseCode == 23) {
              $scope.getGridFirstLoad();
              }
              bsNotify.show(
                {
                  title: "Approved",
                  content: "Stock Disposal has been successfully approved. Next approval to ADH.",
                  type: 'succes'
                }
              );
            },
              function (err) {
                console.log("err=>", err);
            }
            );
          }

        },
          function (err) {
            console.log("err=>", err);
          }
        );

      },
        function (err) {
          console.log("err=>", err);
        }
      );


      // //======================================================
      // var DataId = $scope.mData.StockDisposalId;
      // Disposal.getLastApproval(DataId).then(function(res) {
      //     console.log('res getLastApproval = ', res);
      //     var IsLast = res.data.Result["0"].IsLastApproval;
      //     var StatusId = res.data.Result["0"].StockDisposalStatusId;
      //     if(StatusId == 0){
      //       $scope.mData.StockDisposalStatusId = 2; //status = Complete
      //       // [START] PUT
      //       Disposal.cancel($scope.mData).then(function(res) {
      //           console.log('res cancel = ', res);
      //           bsNotify.show(
      //               {
      //                   title: "Approved",
      //                   content: "Stock Disposal has been successfully approved.",
      //                   type: 'succes'
      //               }
      //             );
      //         },
      //         function(err) {
      //           console.log("err=>", err);
      //         }
      //       );
      //     } else {
      //       console.log('Not Last Approval, StatusId = ', StatusId);
      //       bsNotify.show(
      //         {
      //             title: "Approved",
      //             content: "Stock Disposal has been successfully approved. Next approval to Accounting.",
      //             type: 'succes'
      //         }
      //       );
      //     }

      //   },
      //   function(err) {
      //     console.log("err=>", err);
      //   }
      // );
      // //======================================================

      $scope.ngDialog.close();
      //PartsTransferOrder.formApi.setMode("grid");
    }

    // Reject
    $scope.onBulkRejectDS = function (mData) {
      console.log("mData : ", mData);
      if (mData[0].StockDisposalStatusId == 1) {
        $scope.RejectDS(mData);
      } else if (mData[0].StockDisposalStatusId == 2) {
        bsNotify.show(
            {
                title: "Disposal - Approved",
                content: "Disposal sudah disetujui",
                type: 'danger'
            });
        return;
      } else if (mData[0].StockDisposalStatusId == 3) {
        bsNotify.show(
            {
                title: "Disposal - Cancelled",
                content: "Disposal sudah dibatalkan",
                type: 'danger'
            });
        return;
      } else if (mData[0].StockDisposalStatusId == 13) {
        bsNotify.show(
            {
                title: "Disposal - Rejected",
                content: "Disposal sudah ditolak",
                type: 'danger'
            });
        return;
      } 
    }

    $scope.RejectDS = function (data) {
      console.log('ApproveDS data = ', data);
      data[0].DocDate = $filter('date')(data[0].DocDate, 'yyyy-MM-dd');
      $scope.RejectData = data[0];
      //console.log('$scope.RejectData = ', $scope.RejectData);
      if ($scope.ApproveData.RefSDTypeId == 1) {
        $scope.ApprovalProcessId = 8120;
      } else if ($scope.ApproveData.RefSDTypeId == 2) {
        $scope.ApprovalProcessId = 8125;
      }
      console.log('$scope.ApproveData = ', $scope.RejectData);
      //Disposal.setTransferOrderHeader($scope.RejectData);
      ngDialog.openConfirm({
        template: '\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Reject dokumen Disposal ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinReject(RejectData)">Ya</button>\
                     </div>\
                     </div>',
        plain: true,
        scope:$scope
        //controller: 'DisposalController',
      });
      //console.log('$scope.RejectData bawah = ', $scope.RejectData);
    };

    $scope.yakinReject = function (data) {
      console.log('yakinReject => ', data);
      $scope.mData = data;
      $scope.ngDialog.close();
      $scope.mData.StockDisposalStatusId = 13; //status = Reject
      //$scope.mData.StockDisposalStatusId = 13; //status = Reject
      console.log('yakinReject mData = ', $scope.mData);
      // for approval
      PartsGlobal.actApproval({
        ProcessId: $scope.ApprovalProcessId, DataId: $scope.mData.StockDisposalId, ApprovalStatus: 2
      })
      // [START] PUT
      Disposal.cancel($scope.mData).then(function (res) {
        console.log('res cancel = ', res);
        bsNotify.show(
          {
            title: "Approval",
            content: "Disposal has been successfully rejected.",
            type: 'succes'
          }
        );
      },
        function (err) {
          console.log("err=>", err);
        }
      );
      //PartsTransferOrder.formApi.setMode("grid");
    }
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    console.log('user', $scope.user)
    $scope.mData = {}; //Model
    $scope.mData.GridDetail = {}; //Model
    $scope.xRole = {
      selected: []
    };
    $scope.formApi = {};
    $scope.mData.dataPC = {};
    $scope.mData.NomorPC = {};
    $scope.mData.OutletId = {};
    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    $scope.getData = function (mData) {
      console.log("mData getData = ", $scope.mData);
      // console.log("mData fRefSDTypeId = ", $scope.mData.fRefSDTypeId);
      // console.log("mData fRefSDTypeId = ", $scope.mData.fStockDisposalReasonId);
      // console.log("mData fRefSDTypeId = ", $scope.mData.fStockDisposalStatusId);
      if (($scope.mData.fRefSDTypeId === undefined &&
        $scope.mData.fStockDisposalReasonId === undefined &&
        $scope.mData.fStockDisposalStatusId === undefined &&
        $scope.mData.startDate === undefined &&
        $scope.mData.endDate === undefined &&
        $scope.mData.fRefSDNo === undefined &&
        $scope.mData.fStockDisposalNo === undefined)
        ||
        ($scope.mData.fRefSDTypeId == null &&
          $scope.mData.fStockDisposalReasonId == null &&
          $scope.mData.fStockDisposalStatusId === undefined &&
          $scope.mData.startDate == null &&
          $scope.mData.endDate == null &&
          $scope.mData.fRefSDNo == null &&
          $scope.mData.fStockDisposalNo == null)
        ||
        (
          ($scope.mData.fRefSDTypeId == null || $scope.mData.fRefSDTypeId === undefined) &&
          ($scope.mData.fStockDisposalReasonId == null || $scope.mData.fStockDisposalReasonId === undefined) &&
          ($scope.mData.fStockDisposalStatusId == null || $scope.mData.fStockDisposalStatusId === undefined) &&
          ($scope.mData.startDate == null || $scope.mData.startDate === undefined) &&
          ($scope.mData.endDate == null || $scope.mData.endDate === undefined) &&
          $scope.mData.fRefSDNo == "" &&
          $scope.mData.fStockDisposalNo == "")
      ) {
        bsNotify.show(
          {
            title: "Mandatory",
            content: "Isi salah satu filter terlebih dahulu",
            type: 'danger'
          }
        );
      } else

        if (($scope.mData.fRefSDTypeId === undefined &&
          $scope.mData.fStockDisposalReasonId === undefined &&
          $scope.mData.fStockDisposalStatusId === undefined)
          ||
          ($scope.mData.fRefSDTypeId == null &&
            $scope.mData.fStockDisposalReasonId == null &&
            $scope.mData.fStockDisposalStatusId == null)
          &&
          (($scope.mData.fRefSDNo != null || $scope.mData.fRefSDNo !== undefined) ||
            ($scope.mData.fStockDisposalNo != null || $scope.mData.fStockDisposalNo !== undefined))
          &&
          (($scope.mData.startDate == null || $scope.mData.startDate === undefined) &&
            ($scope.mData.endDate == null || $scope.mData.endDate === undefined))
        ) {
          //alert('ok');
          console.log("filter is non mandatory");
          Disposal.getData(mData).then(function (res) { //start getdata
            var gridData = res.data.Result;
            console.log("<controller getData> GridData => ", gridData);
            for (var i in gridData) {
              gridData[i].DocDate = $scope.localeDate(gridData[i].DocDate);
              gridData[i].CreatedDate = $scope.localeDate(gridData[i].CreatedDate);
            }
            $scope.grid.data = gridData;
            $scope.loading = false;
          },
            function (err) {
              console.log("err=>", err);
            }
          ); // end getdata
        } else {
          if ($scope.mData.startDate == null || $scope.mData.startDate == 'undefined' || $scope.mData.startDate == "") {
            bsNotify.show(
              {
                title: "Mandatory",
                content: "Tanggal Disposal belum diisi",
                type: 'danger'
              }
            );
          } else
            if ($scope.mData.endDate == null || $scope.mData.endDate == 'undefined' || $scope.mData.endDate == "") {
              bsNotify.show(
                {
                  title: "Mandatory",
                  content: "Tanggal Disposal (end date) belum diisi",
                  type: 'danger'
                }
              );
            } else {
              Disposal.getData(mData).then(function (res) { //start getdata
                var gridData = res.data.Result;
                console.log("<controller getData> GridData => ", gridData);
                for (var i in gridData) {
                  gridData[i].CreatedDate = $scope.localeDate(gridData.CreatedDate);
                }
                $scope.grid.data = gridData;
                $scope.loading = false;
              },
                function (err) {
                  console.log("err=>", err);
                }
              ); // end getdata
            }
        }

    }// end getData all

    $scope.ApproveData = Disposal.getDisposalHeader();
    $scope.ApproveDataDetail = Disposal.getDisposalDetail();

    // Hapus Filter
    $scope.onDeleteFilter = function () {
      //Pengkondisian supaya seolaholah kosong
      $scope.mData.fRefSDTypeId = null;
      $scope.mData.fStockDisposalReasonId = null;
      $scope.mData.fStockDisposalStatusId = null;
      $scope.mData.startDate = null;
      $scope.mData.endDate = null;
      $scope.mData.fRefSDNo = null;
      $scope.mData.fStockDisposalNo = null;
    }

    // Generate Nomor Dokumen
    $scope.GenerateDocNum = function () {
      console.log("$scope.mData.ServiceTypeId = ", $scope.mData.ServiceTypeId);
      PartsCurrentUser.getFormatId($scope.mData.ServiceTypeId, 'DSP').then(function (res) {
        var result = res.data;
        //console.log("FormatId result = ", result);
        console.log("FormatId = ", result[0].Results);

        Disposal.getDocumentNumber(result[0].Results).then(function (res) {
          var DocNo = res.data;
          if (typeof DocNo === 'undefined' || DocNo == null) {
            bsNotify.show(
              {
                title: "Disposal",
                content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                type: 'danger'
              }
            );
          }
          else {
            $scope.mData.StockDisposalNo = DocNo[0];
            $scope.mData.DocDate = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm');
            console.log("Generating DocNo & DocDate ->" , res.data , new Date() );
            console.log($scope.mData.StockDisposalNo);
            console.log($scope.mData.DocDate);
          }
        },
          function (err) {
            console.log("err=>", err);
          }
        );
      },
        function (err) {
          console.log("err=>", err);
        }
      );
      // [END] Get Current User Warehouse
    }

    $scope.onSelectRows = function (rows) {
      console.log("onSelectRows=>", rows);
      //$scope.testmodel = rows;
      //console.log("Isi test component =>", $scope.testmodel.VehicleName);
    }


    // ======== tombol  simpan =========
    $scope.onValidateSave = function (data) {
      if (data.StockDisposalId !== undefined && data.StockDisposalId !== null) {
        Disposal.create(data, data.GridDetail).then(function (res) {
          Disposal.formApi.setMode("grid");
        });
      } else {
        console.log("data", data);
        var refDisposal = 0;
        if ($scope.ref == 'Parts Claim') {
          refDisposal = 1;
          data.GridDetail = $scope.gridOptions.data;
        } else if ($scope.ref == 'Langsung') {
          refDisposal = 2
          data.GridDetail = $scope.gridOptionsLangsung.data;
        }
        data['RefSDTypeId'] = refDisposal;
        console.log('$scope.mData.GridDetail = ', $scope.mData.GridDetail);
        console.log('refDisposal = ', refDisposal);
        console.log('$scope.mData.RefSDNo = ', $scope.mData.RefSDNo);
        Disposal.setDisposalHeader(data);
        Disposal.setDisposalDetail(data.GridDetail);
        if (refDisposal == 1 && (data.RefSDNo == null || data.RefSDNo === undefined)) {
          bsNotify.show(
            {
              title: "Mandatory",
              content: "No. Parts Claim DMS belum diisi.",
              type: 'danger'
            }
          );
        } else {
          if (data.StockDisposalReasonId === undefined || data.StockDisposalReasonId == null) {
            bsNotify.show(
              {
                title: "Mandatory",
                content: "Alasan belum diisi.",
                type: 'danger'
              }
            );
          } else
            if (data.GridDetail.length == 0) {
              console.log('length 0', data.GridDetail.length == 0);
              bsNotify.show(
                {
                  title: "Mandatory",
                  content: "Data - Item Disposal tidak boleh kosong!",
                  type: 'danger'
                }
              );
            } else
              if (data.GridDetail.length > 0) {
                console.log('length > 0', data.GridDetail.length > 0);
                for (var i = 0; i < data.GridDetail.length; i++) {
                  var vQC = i + 1;
                  console.log('QtyDisposal', data.GridDetail[i].QtyDisposal);
                  if (data.GridDetail[i].QtyDisposal == 0 || data.GridDetail[i].QtyDisposal == null || data.GridDetail[i].QtyDisposal === undefined) {
                    bsNotify.show(
                      {
                        title: "Mandatory",
                        content: "Item ke-" + vQC + ", Qty Claim nya harus diisi minimal 1 (satu).",
                        type: "danger"
                      }
                    );
                  } else {
                    console.log('mData = ', data);
                    //$scope.ApproveData = data;
                    //console.log('ApproveData = ', $scope.ApproveData);
                    ngDialog.openConfirm({
                      template: '<div ng-include=\"\'app/parts/disposal/referensi/template_simpan.html\'\"></div>',
                      plain: true,
                      controller: 'DisposalController',
                    });
                  }
                }
              } else {
                // data.WarehouseId = WarehouseData[0].WarehouseId;
                // data.OutletId = WarehouseData[0].OutletId;
                console.log('mData = ', data);
                //$scope.ApproveData = $scope.mData;
                //console.log('ApproveData = ', $scope.ApproveData);
                ngDialog.openConfirm({
                  template: '<div ng-include=\"\'app/parts/disposal/referensi/template_simpan.html\'\"></div>',
                  plain: true,
                  controller: 'DisposalController',
                });
              } // end else
        };
      }
    }

    $scope.simpan = function (refDisposal) {
      //$scope.mData.GridDetail = $scope.gridOptions.data;
      if (refDisposal == 1) {
        $scope.mData.GridDetail = $scope.gridOptions.data;
      } else if (refDisposal == 2) {
        $scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
      }

      $scope.mData['RefSDTypeId'] = refDisposal;
      console.log('$scope.mData.GridDetail = ', $scope.mData.GridDetail);
      console.log('refDisposal = ', refDisposal);
      //console.log('$scope.mData.dataPC = ', $scope.mData.dataPC);
      //console.log('$scope.mData.NomorPC = ', $scope.mData.NomorPC);
      console.log('$scope.mData.RefSDNo = ', $scope.mData.RefSDNo);
      Disposal.setDisposalHeader($scope.mData);
      Disposal.setDisposalDetail($scope.mData.GridDetail);

      if (refDisposal == 1 && ($scope.mData.RefSDNo == null || $scope.mData.RefSDNo === undefined)) {
        bsNotify.show(
          {
            title: "Mandatory",
            content: "No. Parts Claim DMS belum diisi.",
            type: 'danger'
          }
        );
      } else
        // if(refDisposal == 1 && ($scope.mData.dataPC == 0 || $scope.mData.dataPC == null || $scope.mData.dataPC === undefined)){
        //   bsNotify.show(
        //     {
        //         title: "Mandatory",
        //         content: "No. Parts Claim belum ditemukan.",
        //         type: 'danger'
        //     }
        //   );
        // } else
        // if(refDisposal == 1 && ($scope.mData.NomorPC != $scope.mData.RefSDNo)){
        //   bsNotify.show(
        //     {
        //         title: "Mandatory",
        //         content: "No. Parts Claim salah.",
        //         type: 'danger'
        //     }
        //   );
        // } else
        if ($scope.mData.StockDisposalReasonId === undefined || $scope.mData.StockDisposalReasonId == null) {
          bsNotify.show(
            {
              title: "Mandatory",
              content: "Alasan belum diisi.",
              type: 'danger'
            }
          );
        } else
          if ($scope.mData.GridDetail.length == 0) {
            console.log('length 0', $scope.mData.GridDetail.length == 0);
            bsNotify.show(
              {
                title: "Mandatory",
                content: "Data - Item Disposal tidak boleh kosong!",
                type: 'danger'
              }
            );
          } else
            if ($scope.mData.GridDetail.length > 0) {
              console.log('length > 0', $scope.mData.GridDetail.length > 0);
              for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                var vQC = i + 1;
                console.log('QtyDisposal', $scope.mData.GridDetail[i].QtyDisposal);
                if ($scope.mData.GridDetail[i].QtyDisposal == 0 || $scope.mData.GridDetail[i].QtyDisposal == null || $scope.mData.GridDetail[i].QtyDisposal === undefined) {
                  bsNotify.show(
                    {
                      title: "Mandatory",
                      content: "Item ke-" + vQC + ", Qty Claim nya harus diisi minimal 1 (satu).",
                      type: "danger"
                    }
                  );
                } else {
                  console.log('mData = ', $scope.mData);
                  //$scope.ApproveData = $scope.mData;
                  //console.log('ApproveData = ', $scope.ApproveData);
                  ngDialog.openConfirm({
                    template: '<div ng-include=\"\'app/parts/disposal/referensi/template_simpan.html\'\"></div>',
                    plain: true,
                    controller: 'DisposalController',
                  });
                }
              }
            } else {
              // $scope.mData.WarehouseId = WarehouseData[0].WarehouseId;
              // $scope.mData.OutletId = WarehouseData[0].OutletId;
              console.log('mData = ', $scope.mData);
              //$scope.ApproveData = $scope.mData;
              //console.log('ApproveData = ', $scope.ApproveData);
              ngDialog.openConfirm({
                template: '<div ng-include=\"\'app/parts/disposal/referensi/template_simpan.html\'\"></div>',
                plain: true,
                controller: 'DisposalController',
              });
            } // end else
    };
    // ======== end tombol  simpan =========
    // ======== tombol  simpan Approval=========
    $scope.SimpanApproval = function (data) {
      console.log("Data untuk Approval ", data);

      $scope.mData = Disposal.getDisposalHeader();
      $scope.mData.GridDetail = Disposal.getDisposalDetail();

      console.log("mData => ", $scope.mData);
      console.log("mData Detail=> ", $scope.mData.GridDetail);

      for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
        $scope.mData.GridDetail[i].DisposalStatusId = 2; //for approval
        $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
        $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;
        console.log('$scope.mData.GridDetail[i].WarehouseId',$scope.mData.GridDetail[i].WarehouseId);
        if ($scope.mData.GridDetail[i].StockDisposalItemId == "" || $scope.mData.GridDetail[i].StockDisposalItemId == undefined) {
          $scope.mData.GridDetail[i].StockDisposalItemId = 0;
        }
        if ($scope.mData.GridDetail[i].StockDisposalId == "" || $scope.mData.GridDetail[i].StockDisposalId == undefined) {
          $scope.mData.GridDetail[i].StockDisposalId = 0;
        }
        if ($scope.mData.GridDetail[i].UomId == "" || $scope.mData.GridDetail[i].UomId == undefined) {
          $scope.mData.GridDetail[i].UomId = 0;
        }
      }
      Disposal.create($scope.mData, $scope.mData.GridDetail).then(function (res) {
        var create = res.data;
        Disposal.sendNotif($scope.mData.CancelReasonDesc, 1128).then(
          function(res) {
            console.log('terkirim ke KABENG login 35')
          });
        console.log('res.data = e', res.data);
        bsNotify.show(
          {
            title: "Disposal",
            content: "Data berhasil disimpan",
            type: 'success'
          }
        );
      },
        function (err) {
          console.log("err=>", err);
        }
      );

      $scope.ngDialog.close();
      Disposal.formApi.setMode("grid");
    }
    // ======== end tombol simpan Approval=========

    // UBAH - UPDATE
    // Button yang terdapat di form
    $scope.UbahData = function (mData) {
      Disposal.update(mData).then(function (res) {
        var create = res.data.Result;
        console.log(res.data.Result);
        bsNotify.show(
          {
            title: "Disposal ",
            content: "Data berhasil diubah",
            type: 'success'
          }
        );
      },
        function (err) {
          console.log("err=>", err);
        }
      );
    }

    // ======== tombol batal - parts claim - alasan =========
    $scope.batalPC = function () {
      ngDialog.openConfirm({
        template: '<div ng-include=\"\'app/parts/disposal/referensi/dialog_konfirmasi_batal.html\'\"></div>',
        plain: true,
        controller: 'DisposalController',
      });
      console.log('batalPC mData = ', $scope.mData.Notes);
    }

    // dialog konfirmasi
    $scope.okBatalPC = function (data) {
      $scope.BatalData = Disposal.getDisposalHeader();
      $scope.BatalData.Notes = 'Alasan: ' + data.Batal + ', Catatan: ' + data.Catatan;
      console.log($scope.BatalData);
      Disposal.setDisposalHeader($scope.BatalData);
      ngDialog.openConfirm({
        template: '\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Anda yakin akan membatalkan Disposal ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinBatal(BatalData)">Ya</button>\
                     </div>\
                     </div>',
        plain: true,
        controller: 'DisposalController',
      });
      console.log('okBatalPC  = ', $scope.BatalData.Notes);
    };

    $scope.yakinBatal = function (data) {
      $scope.mData = data;
      $scope.mData.StockDisposalStatusId = 3;
      console.log('yakinBatal mData = ', $scope.mData);
      Disposal.cancel($scope.mData).then(function (res) {
        console.log('res cancel = ', res);
        bsNotify.show(
          {
            title: "Disposal",
            content: "Disposal telah dibatalkan",
            type: 'success'
          }
        );
      },
        function (err) {
          console.log("err=>", err);
        }
      );
      $scope.ngDialog.close();
      Disposal.formApi.setMode("grid");
    }

    // ======== end tombol batal - parts claim =========

    // Report =========================================
    $scope.laporan = function () {
      ngDialog.openConfirm({
        template: '<div ng-include=\"\'app/parts/disposal/referensi/template_laporan.html\'\"></div>',
        plain: true,
        // width: 800px,
        controller: 'DisposalController',
      });
    };

    //------------------------------------------------------------
    //$scope.formmode={mode:''};

    // $scope.doGeneratePass = function(){
    //     $scope.mUser.password = Math.random().toString(36).slice(-8);
    // }
    $scope.goBack = function () {
      $scope.isOverlayForm = false;
      $scope.formApi.setMode("grid");
      $scope.gridMode = true;
      console.log('goBack = ', $scope.formApi);

    }

    $scope.onBeforeNewMode = function (mode) {
      $scope.hideSaveButton = false;
      $scope.disaView = false;
      $scope.isOverlayForm = true;
      $scope.isEditPartsClaim = false;
      $scope.isEditLangsung = false;
      $scope.isViewPartsClaim = false;
      $scope.isViewLangsung = false;
      Disposal.formApi = $scope.formApi;
      //$scope.formode = 1;
      //$scope.isNewForm = true;
      $scope.selection = null;
      //console.log("onBeforeNewMode = ", mode);
      //console.log("mData.RefSDTypeId = ", $scope.mData.RefSDTypeId);
      $scope.gridOptions.data = [];
      $scope.gridOptionsLangsung.data = [];
      //console.log("onBeforeNew grid.data = ", $scope.gridOptions.data);
      console.log("$scope.ApprovalProcessId = ", $scope.ApprovalProcessId);
      //$scope.newUserMode=true;
      $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
      $scope.mData.ServiceTypeId = $scope.getRightServiceType();
      //console.log("MaterialTypeId : ", $scope.mData.MaterialTypeId);
      //console.log("ServiceTypeId : ", $scope.mData.ServiceTypeId);
      $scope.mData.OutletId = $scope.user.OrgId;
      //console.log("OutletId : ", $scope.mData.OutletId);
      PartsCurrentUser.getWarehouseIdi().then(function (res) {
        var gridData = res.data;
        console.log('gridData[0] = ', gridData[0]);
        $scope.mData.WarehouseId = gridData[0].warehouseid;
        $scope.mData.MaterialTypeId =  gridData[0].MaterialType;
        $scope.mData.ServiceTypeId =  gridData[0].Servicetype;
        //$scope.mData.WarehouseData = gridData;
        $scope.mData.OutletId = $scope.user.OrgId;
        //console.log('WarehouseId = ', WarehouseId);
      },
        function (err) {
          console.log("err=>", err);
        }
      );
      // PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.mData.OutletId).then(function (res) {
      //   var gridData = res.data;
      //   console.log('gridData[0] = ', gridData[0]);
      //   $scope.mData.WarehouseId = gridData[0].WarehouseId;
      //   //$scope.mData.WarehouseData = gridData;
      //   $scope.mData.OutletId = $scope.user.OrgId;
      //   //console.log('WarehouseId = ', WarehouseId);
      // },
      //   function (err) {
      //     console.log("err=>", err);
      //   }
      // );

      $scope.GenerateDocNum();
    }
    $scope.onBeforeEditMode = function (row, mode) {
      $scope.hideSaveButton = true;
      console.log("onBeforeEdit=>", mode);
    }
    $scope.onBeforeDeleteMode = function () {
      $scope.formode = 3;
      $scope.isNewForm = false;
      $scope.isEditForm = false;
      console.log("mode=>", $scope.formmode);
    }
    $scope.onShowDetail = function (row, mode) {
      console.log("---> onShowDetail");
      Disposal.formApi = $scope.formApi;
      console.log("row = ", row);
      // console.log("mode apa ? = ", mode);
      // console.log("RefSDTypeId ? = ", row.RefSDTypeId);
      // console.log("$scope.mData.RefSDTypeId ? = ", $scope.mData.RefSDTypeId);
      //Disposal.setDHeader(row);
      // console.log('setDHeader');
      //$scope.dStatusName= $scope.dStatus[row.StockDisposalStatusId].name;

      if (mode == 'view') { //jika mode = view

        Disposal.getDetail(row).then(function (res) {
          var gridData = res.data.Result;
          console.log('<controller> gridData = ', gridData);
          //console.log('<controller> row.RefSDNo = ', row.PartsCode);
          //$scope.gridOptionsLangsung.data = gridData;
          //$scope.gridOptionsLangsung_view.data = gridData;
          //$scope.gridOptions_view.data = gridData;
          //console.log('row = ', row); //$scope.gridOptionsLangsung.data);
          switch (row.RefSDTypeId) {
            case 1:
              $scope.gridOptions_view.data = gridData;
              break;
            case 2:
              $scope.gridOptionsLangsung_view.data = gridData;
              break;
          }
        },
          function (err) {
            console.log("err=>", err);
          }
        );

        $scope.isEditPartsClaim = false;
        $scope.isEditLangsung = false;
        if (row.RefSDTypeId == 1) { //& data referensiDisposal-nya = 'Parts Claim'
          $scope.isViewPartsClaim = true;
          $scope.isViewLangsung = false;
        } else if (row.RefSDTypeId == 2) { //& data referensiDisposal-nya = 'Langsung'
          $scope.isViewPartsClaim = false;
          $scope.isViewLangsung = true;
          console.log('$scope.isViewLangsung = ', $scope.isViewLangsung);
          $scope.disaView = true;
          //console.log('$scope.mData.RefSDTypeId = ', $scope.mData.RefSDTypeId);
        }
      } else
        if (mode == 'edit') { //jika mode edit

          Disposal.getDetail(row).then(function (res) {
            var gridData = res.data.Result;
            console.log('<controller> getDetail = ', gridData);
            //console.log('<controller> row.RefSDNo = ', row.PartsCode);
            //$scope.gridOptionsLangsung.data = gridData;
            //$scope.gridOptionsLangsung_view.data = gridData;
            //$scope.gridOptions_view.data = gridData;
            //console.log('<controller> gridOptionsLangsung = ', gridData); //$scope.gridOptionsLangsung.data);
            switch (row.RefSDTypeId) {
              case 1:
                $scope.gridOptions_view.data = gridData;
                break;
              case 2:
                $scope.gridOptionsLangsung_view.data = gridData;
                break;
            }
          },
            function (err) {
              console.log("err=>", err);
            }
          );

          $scope.isViewPartsClaim = false;
          $scope.isViewLangsung = false;
          $scope.disaView = true;

          console.log("Get From factory => ");
          Disposal.setDisposalHeader(row);
          console.log(Disposal.getDisposalHeader());

          //if($scope.mData.referensiDisposal == 'Parts Claim'){
          if (row.RefSDTypeId == 1) { //& data referensiDisposal-nya = 'Parts Claim'
            $scope.isEditPartsClaim = true;
            $scope.isEditLangsung = false;
            //} else if($scope.mData.referensiDisposal == 'Langsung'){
          } else if (row.RefSDTypeId == 2) { //& data referensiDisposal-nya = 'Langsung'
            $scope.isEditPartsClaim = false;
            $scope.isEditLangsung = true;
          }
        } else
          // mode New
          if (mode == 'new') {
            //alert('mode new');
            console.log("onShowDetail new =>", mode);
          }
      $scope.ref = '';
      //log
      console.log("onShowDetail=>", mode);
    }

    // search No Part Claim
    $scope.GetNoPartClaim = function () {
      //console.log("GetNoPartClaim");
      Disposal.GetPartsClaim().then(function (res) {
        //console.log("Res GetPartsClaim ==>", res);
        console.log("res.data GetPartsClaim ==>", res.data);
        $scope.DataPartsClaim = res.data;
      });
    };
    $scope.selectNoPartClaim = function (row) {
      console.log('row selectNoPartClaim', row);
      console.log('$scope.mData.MaterialTypeId', $scope.mData.MaterialTypeId);
      if (row === 'undefined' || row == null) {
        $scope.mData.RefSDNo = null;
        $scope.gridOptions.data = [];
        //$scope.mData.dataPC = 0;
        $scope.mData.GridDetail = [];
        //console.log('$scope.mData.RefSDNo = ', $scope.mData.RefSDNo);
        //console.log("$scope.gridOptions.data", $scope.gridOptions.data);
        //console.log("$scope.mData.GridDetail", $scope.mData.GridDetail);
      } else {
        // if($scope.mData.MaterialTypeId !== row.PartClaimTypeId){
        //   bsNotify.show(
        //     {
        //         title: "Tipe Material Tidak Sesuai!",
        //         //size: "big",
        //         content: "Tipe Material No.Parts Claim tidak sesuai dengan hak akses User.",
        //         type: 'danger'
        //     }
        //   );
        // } else {
        $scope.mData.RefSDNo = row.StockReturnClaimNo;
        //$scope.mData.dataPC = 0;
        //console.log('$scope.mData.RefSDNo = ', $scope.mData.RefSDNo);
        Disposal.GetPartsClaimItem(row.StockReturnClaimId).then(function (res) {
          //console.log("res.data GetPartsClaim Item==>", res.data);
          $scope.gridOptions.data = res.data;
          $scope.mData.GridDetail = res.data;
          //console.log("$scope.gridOptions.data", $scope.gridOptions.data);
          //console.log("$scope.mData.GridDetail", $scope.mData.GridDetail);
        });
        // } // end else
      } // end else
    };

    $scope.onSearchNoPartsClaimDMS = function (NomorPC) {
      if (NomorPC === undefined || NomorPC == null) {
        bsNotify.show(
          {
            title: "Mandatory ",
            content: "No. Parts Claim DMS belum diisi",
            type: 'danger'
          }
        );
      } else {
        //Get Search parts claim
        Disposal.SearchNoPartsClaimDMS(NomorPC).then(function (res) {
          var PartsClaimtData = res.data;
          console.log('SearchNoPartsClaimDMS ==>', PartsClaimtData);
          console.log('SearchNoPartsClaimDMS.lenght ==>', PartsClaimtData.length);
          $scope.gridOptions.data = PartsClaimtData;
          //$scope.gridOptionsLangsung.data = PartsClaimtData;
          $scope.mData.GridDetail = PartsClaimtData;
          $scope.mData.dataPC = PartsClaimtData.length;
          $scope.mData.NomorPC = NomorPC;
          console.log('$scope.mData.dataPC ==>', $scope.mData.dataPC);
          if (PartsClaimtData.length == 0) {
            bsNotify.show(
              {
                title: "Tidak Ditemukan",
                content: "Data Parts Claim tidak ditemukan",
                type: 'danger'
              }
            );
          }
        },
          function (err) {
            console.log("err=>", err);
          }
        );
      } // end else
    } // end function

    //----------------------------------
    // Select Option Referensi GI Setup
    //----------------------------------
    $scope.changedValue = function (item) {
      console.log("cek", $scope.disaView);
      console.log('item', item);
      if (item != null) {
        $scope.isOverlayForm = false;
        $scope.ref = item;
        // $scope.disableSelect = true;
        //$scope.filterFieldChange($scope.filterField[0]);
      }
      if (item == 'Parts Claim') {
        $scope.ApprovalProcessId = 8120;
      } else if (item == 'Langsung') {
        $scope.ApprovalProcessId = 8125;
      }
      console.log('$scope.ApprovalProcessId', $scope.ApprovalProcessId);
      PartsGlobal.getApprover($scope.ApprovalProcessId).then(function (res) {
        var Approvers = res.data.Result[0].Approvers;
        console.log("getApprover res = ", Approvers);
        $scope.mData.Approvers = Approvers;
        $scope.disaView = false;
        // $scope.formApi.setFormReadOnly(false);
      },
        function (err) {
          console.log("err=>", err);
        }
      );
    } // end changedValue

    $scope.ToNameRefSD = function (RefSDTypeId) {
      var GiName;
      switch (RefSDTypeId) {
        case 1:
        case "1":
          GiName = "Parts Claim";
          break;
        case 2:
        case "2":
          GiName = "Langsung";
          break;
      }
      return GiName;
    }

    $scope.ToNameMaterial = function (MaterialTypeId) {
      var materialName;
      switch (MaterialTypeId) {
        case 1:
          materialName = "Parts";
          break;
        case 2:
          materialName = "Bahan";
          break;
      }
      return materialName;
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------

    $scope.grid = {
      enableSorting: true,
      enableRowSelection: true,
      multiSelect: false,
      enableSelectAll: true,
      //showTreeExpandNoChildren: true,
      paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100, 200],
      paginationPageSize: 15,
      columnDefs: [{
        name: 'Id',
        field: 'StockDisposalId',
        width: '7%',
        visible: false
      },
      {
        name: 'Tipe Material',
        field: 'Description',
        width: '10%'
      },
      {
        name: 'Referensi Disposal',
        field: 'RefSDTypeId',
        width: '15%',
        cellFilter: 'filterRefTypeDisposal'
      },
      {
        name: 'No. Referensi',
        field: 'RefSDNo',
        width: '15%'
      },
      {
        name: 'No. Disposal',
        field: 'StockDisposalNo',
        width: '15%'
      },
      {
        name: 'Tanggal Disposal',
        field: 'DocDate',
        cellFilter: 'date:\'yyyy-MM-dd\'',
        width: '15%'
      },
      {
        name: 'Alasan',
        field: 'StockDisposalReasonId',
        width: '10%',
        cellFilter: 'filterAlasanDisposal'
      },
      {
        name: 'Status',
        field: 'StockDisposalStatusId',
        width: '10%',
        cellFilter: 'filterStatusDisposal'
      }
    ],
    onRegisterApi: function(gridApi) {
        $scope.gridApiVC = gridApi;
    },

    };


    //----------------------------------
    // Dummy
    //----------------------------------
    $scope.showMe = function () {
      var result = confirm('Apakah Anda yakin menghapus data ini?');
      if (result) {
        console.log($scope.gridApi.selection.getSelectedRows());
        angular.forEach($scope.gridApi.selection.getSelectedRows(), function (data, index) {
          $scope.gridOptions.data.splice($scope.gridOptions.data.lastIndexOf(data), 1);
        });
      }

    };

    $scope.Delete = function (row) {
      var index = $scope.gridOptions.data.indexOf(row.entity);
      var index2 = $scope.gridOptionsLangsung.data.indexOf(row.entity);

      $scope.gridOptions.data.splice(index, 1);
      $scope.gridOptionsLangsung.data.splice(index2, 1);

      console.log($scope.gridOptions.data);
      console.log(index);

      console.log($scope.gridOptionsLangsung.data);
      console.log(index2);
    };

    $scope.PrintOutDocTypeId = 20;
    $scope.printBAG = function (StockDisposalId) {
      var data = $scope.mData;
      console.log('StockDisposalId =', StockDisposalId);
      console.log('$scope.mData =', $scope.mData);
      Disposal.GetPrintOutNeedToAsk($scope.PrintOutDocTypeId, StockDisposalId).then(function (res) {
        var NeedToAsk = res.data.Result[0].Results;
        console.log('NeedToAsk ==>', NeedToAsk);
        console.log('NeedToAsk == -1', NeedToAsk == -1);
        if (NeedToAsk == -1) { // sedang menunggu approval
          bsNotify.show(
            {
              title: "Sedang Menunggu Approval",
              content: "Pengajuan approval cetak belum disetujui",
              type: 'info'
            }
          );
        } else if (NeedToAsk == -2) { // sudah di-approve
          $scope.printBeritaAcaraGudang = 'as/SDBeritaAcaraGudang/' + StockDisposalId;
          $scope.cetakan($scope.printBeritaAcaraGudang);
        } else if (NeedToAsk == 0) { // belum pernah print & harus bisa print
          $scope.printBeritaAcaraGudang = 'as/SDBeritaAcaraGudang/' + StockDisposalId;
          $scope.cetakan($scope.printBeritaAcaraGudang);
          // Count
          Disposal.GetPrintOutApproval_Ask($scope.PrintOutDocTypeId, StockDisposalId, 'hallo').then(function (res) {
            var Approval_Ask = res.data.Result[0].Results;
            console.log('Approval_Ask ==>', Approval_Ask);
          },
            function (err) {
              console.log("err=>", err);
            }
          ); // end Disposal.GetPrintOutApproval_Ask
        } else if (NeedToAsk == 1) { // perlu approval & muncul dialog pengajuan approval
          console.log('$scope.mData =', $scope.mData);
          ngDialog.openConfirm({
            template: '<div ng-include=\"\'app/parts/disposal/referensi/dialog_approval_print.html\'\"></div>',
            plain: true,
            controller: 'DisposalController',
          });
        } // end else if

      },
        function (err) {
          console.log("err=>", err);
        }
      ); // end Disposal.GetPrintOutNeedToAsk
    }; // end PrintBAG

    $scope.KirimApprovalCetak = function (data) {
      console.log('data => ', data);
      $scope.mData = data;
      Disposal.GetPrintOutApproval_Ask($scope.PrintOutDocTypeId, data.StockDisposalId, 'hallo').then(function (res) {
        var Approval_Ask = res.data.Result[0].Results;
        console.log('Approval_Ask ==>', Approval_Ask);
      },
        function (err) {
          console.log("err=>", err);
        }
      ); // end Disposal.GetPrintOutApproval_Ask
      $scope.ngDialog.close();
    }

    $scope.cetakan = function (data) {
      var pdfFile = null;

      PrintRpt.print(data).success(function (res) {
        var file = new Blob([res], { type: 'application/pdf' });
        var fileURL = URL.createObjectURL(file);

        console.log("pdf", fileURL);
        //$scope.content = $sce.trustAsResourceUrl(fileURL);
        pdfFile = fileURL;

        if (pdfFile != null) {
          //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame
          var ua = navigator.userAgent;
          if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
            var link = document.createElement('a');
            link.href = fileURL;
            //link.download="erlangga_file.pdf";
            link.click();
          }
          else {
            printJS(pdfFile);
          }
        }
        else
          console.log("error cetakan", pdfFile);
      }).error(function (res) {
        console.log("error cetakan", pdfFile);
      });
    };

    $scope.gridOptions = {
      enableSorting: true,
      enableRowSelection: true,
      multiSelect: true,
      enableFiltering: true,
      //selectedItems: console.log($scope.mySelections),
      enableSelectAll: true,
      paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
      paginationPageSize: 5,

      onRegisterApi: function (gridApi) {
        $scope.gridApi = gridApi;
      },

      columnDefs: [

        {
          name: 'StockDisposalItemId',
          field: 'StockDisposalItemId',
          width: '7%',
          visible: false
        },
        {
          name: 'PartId',
          field: 'PartId',
          width: '7%',
          visible: false
        },
        {
          name: 'PartsId',
          field: 'PartsId',
          width: '7%',
          visible: false
        },
        {
          name: 'StockDisposalId',
          field: 'StockDisposalId',
          width: '7%',
          visible: false
        },
        { name: 'NoMaterial', displayName: 'No. Material', field: 'PartsCode', width: '20%', enableCellEdit: true },
        { name: 'NamaMaterial', displayName: 'Nama Material', field: 'PartsName', width: '20%', enableCellEdit: false },
        { name: 'QtyClaim', displayName: 'Qty Claim', field: 'QtyClaim', width: '10%', enableCellEdit: false },
        { name: 'QtyDisposal', displayName: 'Qty Disposal', field: 'QtyDisposal', enableCellEditOnFocus: true, width: '10%', enableCellEdit: true },
        { name: 'UomId', displayName: 'UomId', field: 'UomId', width: '10%', visible: false },
        { name: 'Satuan', displayName: 'Satuan', field: 'satuanName', width: '10%', enableCellEdit: false },
        {
          name: '_',
          displayName: 'Action',
          enableCellEdit: false,
          // visible : true,
          //cellTemplate:'<div style="padding-top: 5px;"><u><a href="#" ng-click="grid.appScope.$parent.hello()">Hapus</a></u></div>'
          cellTemplate: '<div style="padding-top: 5px;" class="ui-grid-cell-contents"><u><a href="#" ng-click="grid.appScope.Delete(row)">Hapus</a></u></div>'
        }
      ]
      // data : [      {
      //                    "PartsCode": "09905-00012",
      //                    "PartsName": "EXPANDER",
      //                    "TotalAmount": 5,
      //                    "QtyDisposal": 1,
      //                    "Satuan": "Pieces"
      //                },
      //                {
      //                    "PartsCode": "48520-09530",
      //                    "PartsName": "ABSORBER A/S FR LH",
      //                    "TotalAmount": 1,
      //                    "QtyDisposal": 1,
      //                    "Satuan": "Pieces"
      //                }
      //            ]
    };

    $scope.hello = function () {
      alert('Data telah dihapus');
      //console.log("");
    }




    //----------------------------------
    // Dummy Grid 'Langsung'
    //----------------------------------
    $scope.gridOptionsLangsung = {
      enableSorting: true,
      enableRowSelection: true,
      multiSelect: true,
      enableFiltering: true,
      //selectedItems: console.log($scope.mySelections),
      enableSelectAll: true,
      paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
      paginationPageSize: 5,

      columnDefs: [
        {
          name: 'StockDisposalItemId',
          field: 'StockDisposalItemId',
          width: '7%',
          visible: false
        },
        {
          name: 'StockDisposalId',
          field: 'StockDisposalId',
          width: '7%',
          visible: false
        },
        {
          name: 'PartId',
          field: 'PartId',
          width: '7%',
          visible: false
        },
        {
          name: 'PartsId',
          field: 'PartsId',
          width: '7%',
          visible: false
        },
        {
          name: 'NoMaterial',
          displayName: 'No. Material',
          field: 'PartsCode',
          width: '20%',
          enableCellEditOnFocus: true,
          cellTemplate: '\
            <div class="ui-grid-cell-contents"><div class="input-group">\
            <input ng-model="mData.PartsCode" ng-value="row.entity.PartsCode">\
              <button class="btn btn-default btn-xs" type="button" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)"><span class="glyphicon glyphicon-search"></span></button>\
            </div></div>'
        },
        { name: 'NamaMaterial', displayName: 'Nama Material', field: 'PartsName', width: '30%', enableCellEdit: false },
        //{ name:'QtyClaim', displayName:'Qty Claim', field: 'QtyClaim', width: '10%'},
        { name: 'QtyDisposal', displayName: 'Qty Disposal', field: 'QtyDisposal', enableCellEditOnFocus: true, width: '10%' },
        { name: 'UomId', displayName: 'UomId', field: 'UomId', width: '10%', visible: false },
        { name: 'Satuan', displayName: 'Satuan', field: 'satuanName', width: '20%', enableCellEdit: false },
        {
          name: '_',
          displayName: 'Action',
          enableCellEdit: false,
          cellTemplate: '<div style="padding-top: 5px;" class="ui-grid-cell-contents"><u><a href="#" ng-click="grid.appScope.Delete(row)">Hapus</a></u></div>'
        }
      ]
      // data : [      {
      //                    "NoMaterial": "09905-00012",
      //                    "NamaMaterial": "EXPANDER",
      //                    "QtyClaim": 5,
      //                    "QtyDisposal": 1,
      //                    "Satuan": "Pieces"
      //                },
      //                {
      //                    "NoMaterial": "48520-09530",
      //                    "NamaMaterial": "ABSORBER A/S FR LH",
      //                    "QtyClaim": 1,
      //                    "QtyDisposal": 1,
      //                    "Satuan": "Pieces"
      //                }
      //            ]
    };

    $scope.Add = function () {
      var n = $scope.gridOptionsLangsung.data.length + 1;
      console.log("add = ", n);
      $scope.gridOptionsLangsung.data.push({
        "StockDisposalItemId": "",
        "StockDisposalId": "",
        "PartsCode": "",
        "PartsName": "",
        "QtyDisposal": "",
        "UomId": "",
        "satuanName": ""
      });
    }

    $scope.SearchMaterial = function (PartsCode) {
      Disposal.getDetailByMaterialNo(PartsCode).then(function (res) {
        var gridData = res.data.Result;
        console.log(gridData[0]);
        $scope.gridOptionsLangsung.data[$scope.gridOptionsLangsung.data.length - 1] = gridData[0];
        $scope.mData.GridDetail = $scope.gridOptionsLangsung.data;
      },
        function (err) {
          console.log("err=>", err);
        }
      );
    }

    //----------------------------------
    // Dummy Grid 'Langsung'
    //----------------------------------
    $scope.gridOptionsLangsung_view = {
      enableSorting: true,
      enableRowSelection: true,
      multiSelect: true,
      enableFiltering: true,
      //selectedItems: console.log($scope.mySelections),
      enableSelectAll: true,
      paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
      paginationPageSize: 5,

      columnDefs: [
        {
          name: 'StockDisposalItemId',
          field: 'StockDisposalItemId',
          width: '7%',
          visible: false
        },
        {
          name: 'StockDisposalId',
          field: 'StockDisposalId',
          width: '7%',
          visible: false
        },
        {
          name: 'NoMaterial',
          displayName: 'No. Material',
          field: 'PartsCode',
          width: '20%',
          enableCellEdit: false
        },
        { name: 'NamaMaterial', displayName: 'Nama Material', field: 'PartsName', width: '30%', enableCellEdit: false },
        { name: 'QtyDisposal', displayName: 'Qty Disposal', field: 'QtyDisposal', width: '10%', enableCellEdit: false },
        { name: 'Satuan', displayName: 'Satuan', field: 'satuanName', width: '20%', enableCellEdit: false }
      ]
    };


    //----------------------------------
    // Dummy Grid 'option' view
    //----------------------------------
    $scope.gridOptions_view = {
      enableSorting: true,
      enableRowSelection: true,
      multiSelect: true,
      enableFiltering: true,
      //selectedItems: console.log($scope.mySelections),
      enableSelectAll: true,
      paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
      paginationPageSize: 5,

      columnDefs: [
        {
          name: 'StockDisposalItemId',
          field: 'StockDisposalItemId',
          width: '7%',
          visible: false
        },
        {
          name: 'StockDisposalId',
          field: 'StockDisposalId',
          width: '7%',
          visible: false
        },
        {
          name: 'NoMaterial',
          displayName: 'No. Material',
          field: 'PartsCode',
          width: '20%',
          enableCellEdit: false
        },
        { name: 'NamaMaterial', displayName: 'Nama Material', field: 'PartsName', width: '30%', enableCellEdit: false },
        { name: 'QtyClaim', displayName: 'Qty Claim', field: 'QtyClaim', width: '10%', enableCellEdit: false },
        { name: 'QtyDisposal', displayName: 'Qty Disposal', field: 'QtyDisposal', width: '10%', enableCellEdit: false },
        { name: 'Satuan', displayName: 'Satuan', field: 'satuanName', width: '20%', enableCellEdit: false }
      ]
    };


  })
  .filter('filterStatusDisposal', function () {
    var xstatus = {
      '1': 'Request Approval',
      '2': 'Completed',
      '3': 'Cancelled',
      '13': 'Reject'
    };
    return function (input) {
      if (!input) {
        return '';
      } else {
        return xstatus[input];
      }
    };
  })
  .filter('filterAlasanDisposal', function () {
    var xalasan = {
      '1': 'Material Rusak',
      '2': 'Scrap',
      '3': 'Lainnya'
    };
    return function (input) {
      if (!input) {
        return '';
      } else {
        return xalasan[input];
      }
    };
  })
  .filter('filterRefTypeDisposal', function () {
    var xtipe = {
      '1': 'Parts Claim',
      '2': 'Langsung'
    };
    return function (input) {
      if (!input) {
        return '';
      } else {
        return xtipe[input];
      }
    };
  })
  ;
