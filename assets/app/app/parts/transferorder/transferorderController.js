angular.module('app')
    .controller('PartsTransferOrderController', function($scope, $http, $filter,bsAlert, CurrentUser, PartsGlobal, PartsTransferOrder, PartsCurrentUser, ngDialog, bsNotify, $timeout, $window) {

    $scope.dsoTampil = [
      {id:'1', name:'Transfer Order'},
      {id:'2', name:'Transfer Request'}
    ];
    $scope.mydsoTampil= $scope.dsoTampil[0];
    $scope.isTransferOrder = true;
    $scope.isTempTO = true;
    $scope.OutletMana = {};
    $scope.actionHide=false;
    $scope.ApprovalProcessId = 8130;
    //$scope.selection = $scope.items[0];

    // mode
    $scope.isModeTambah = false;
    $scope.isModeEdit = false;
    $scope.isViewRequest = false;
    $scope.hideSimpanButton = true;

    $scope.AlreadyGI = false;

    var itIsLink = false;
    var paramNew;
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading = false;
        //$scope.gridData=[];
        //$scope.actionHide=false;
        $timeout(function(){
          var parts = $scope.checkRights(9);
          console.log("parts = ", parts);
          console.log('user=', $scope.user);
          $scope.mData.OutletId = $scope.user.OrgId;
          // $scope.grid.columnDefs = $scope.columnsNeedApproval;
          // $scope.getDataNA();

          if (itIsLink) {
            $scope.Add();
            $scope.SearchMaterial(paramNew.PartsCode);
            itIsLink = false;
          }
        });

        //2019-02-10 Permintaan Pak Rully -  ADIB BISYRI
        // if ($scope.user.RoleId == 1128) {
        //     $scope.getDataApprove();
        // }
        PartsCurrentUser.getWarehouseIdi().then(function(res) {
          console.log("====",res.data); 
          for (var i=0; i < res.data.length; i++) {
            $scope.mData.WarehouseId = res.data[i].warehouseid;
            $scope.mData.MaterialTypeId = res.data[i].MaterialType;
            $scope.mData.ServiceTypeId = res.data[i].Servicetype;
            $scope.mData.OutletId = $scope.user.OrgId;
          }       
  
        });

    });
    $scope.NeedApproval = function(){  // fungsi ini belum dipake, jika dipake, maka masih harus disesuaikan lagi
      // do something
      console.log("Mesti call Pop Up Grid for Approval ?");
      PartsGlobal.formLookupTitle = "Daftar Approval Transfer Order"; //Alokasi Stok";
      PartsGlobal.formLookupSubTitle1 = "Daftar Alokasi Stok : ";
      PartsGlobal.formLookupSubTitle2 = PartsGlobal.getCurrDate();
      PartsGlobal.formLookupGrid = $scope.gridLookUpApprovalAlokasi; //"gridLookUpPO"; $scope.getDaftarPO()
      PartsGlobal.formLookupGrid.multiSelect = false;
      PartsGlobal.formLookupGrid.data = $scope.getDataApproval();//[];//$scope.getVendor();
      PartsGlobal.cancelledLookupGrid = true;
      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/templates01/frm_lookup02.html\'\"></div>',
        plain: true,
        controller: 'PartsTransferOrderController',
       }).then(function (value) {
                    // $scope.selectedVendor = PartsGlobal.selectedLookupGrid;
                    console.log("Data selected : ");
                    if (PartsGlobal.cancelledLookupGrid) return 1;
                }, function (value) {
                  // $scope.selectedVendor = PartsGlobal.selectedLookupGrid;
                  console.log("No Data fetched ");
                  if (PartsGlobal.cancelledLookupGrid) return 1;
                  // $scope.mData.VendorId= $scope.selectedVendor[0].VendorCode;
                });
    }

    $scope.bulkApprove = function(row){
      console.log("Approved => ", $scope.mData.selected);
    }

    $scope.bulkReject = function(row){
      console.log("Rejected => ", $scope.mData.selected);
    }

    $scope.checkRights = function(bit){
        var p=$scope.myRights & Math.pow(2,bit);
        var res= (p==Math.pow(2,bit));
        console.log("myRights => ", $scope.myRights);
        return res;
    }
    $scope.getRightX= function(a, b){
      var i = 0;
      if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
      else if ($scope.checkRights(b)) i= 2;
      else if ($scope.checkRights(a)) i= 1;
      return i;
    }
    $scope.getRightMaterialType= function(){
      var i = $scope.getRightX(8, 9);
      return i;
    }
    $scope.getRightServiceType= function(){
      var i = $scope.getRightX(10, 11);
      return i;
    }
    $scope.onRegisterApi = function(gridApi) {
        $scope.myGridApi = gridApi;
      };

    $scope.afterRegisterGridApi = function(gridApi){
        console.log(gridApi);
    }


    $scope.onBulkApproveTS = function(mData) {
      console.log("mData : ", mData);
      // PartsGlobal.actApproval({
      //   ProcessId: $scope.ApprovalProcessId, DataId: mData[0].TransferOrderId, ApprovalStatus: 1
      // })
      $scope.ApproveTS(mData);
      // bsNotify.show(
      //   {
      //       title: "Approval",
      //       content: "Data berhasil di-Approve",
      //       type: 'success'
      //   }
      // );
    }
    $scope.onBulkRejectTS = function(mData) {
      console.log("mData : ", mData);
      // PartsGlobal.actApproval({
      //   ProcessId: $scope.ApprovalProcessId, DataId: mData[0].TransferOrderId, ApprovalStatus: 2
      // })
      $scope.RejectTS(mData);
      // bsNotify.show(
      //   {
      //       title: "Approval",
      //       content: "Data telah di-Reject",
      //       type: 'success'
      //   }
      // );
    }
    // approve
    $scope.ApproveTS = function(data){
      console.log('ApproveTS data = ', data);
      $scope.ApproveData = data[0];
      console.log('$scope.ApproveData = ', $scope.ApproveData);
      console.log('$scope.isTransferOrder = ', $scope.isTransferOrder);
      if(($scope.ApproveData.TransferOrderStatusId == 1 && $scope.isTransferOrder == true) || 
         ($scope.ApproveData.TransferRequestStatusId == 1 && $scope.isTransferOrder == false)) {
        $scope.respondforMessage = [];
        PartsTransferOrder.setRespond($scope.respondforMessage);
        PartsTransferOrder.setTransferOrderHeader($scope.ApproveData);
        ngDialog.openConfirm ({
          template:'\
                      <div align="center" class="ngdialog-buttons">\
                      <p><b>Konfirmasi</b></p>\
                      <p>Approve Transfer Order ini?</p>\
                      <div class="ngdialog-buttons" align="center">\
                        <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                        <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinApprove(ApproveData)">Ya</button>\
                      </div>\
                      </div>',
          plain: true,
          controller: 'PartsTransferOrderController',
        });
        console.log('$scope.ApproveData bawah = ', $scope.ApproveData);
      } else if ($scope.ApproveData.ApprovalStatus == 2) {
        bsNotify.show(
          {
              title: "Approval",
              content: "Data sudah di-Reject",
              type: 'danger'
          }
        );
      } else {
        bsNotify.show(
          {
              title: "Approval",
              content: "Data sudah di-Approve",
              type: 'danger'
          }
        );
      }
    };

    $scope.yakinApprove = function(data){
      console.log('yakinApprove => ', data);
      $scope.mData = data;
      ngDialog.close();
      PartsTransferOrder.getStatusTO(data.TransferOrderId).then(function(res){
        console.log('res ==>', res);
        var countZero = 0;
        if(res.data.length > 0){
          for(var i in res.data){
            if(res.data[i].Status == 0){
              countZero++;
              $scope.respondforMessage.push(res.data[i]);
            }
          }
          if(countZero > 0){
            PartsTransferOrder.setRespond($scope.respondforMessage);
            console.log("asdfadfadfa ==>",$scope.respondforMessage);
              ngDialog.openConfirm ({
                // scope:$scope,
                controller: 'PartsTransferOrderController',
                template:'app/parts/transferorder/templates/responApproval.html'
              }).then(function(item) {
                $scope.sendapprove();
                // console.log("itemmm ->", item, 'asdfadfadfa ==>');
              });
          }else{
            $scope.sendapprove();
          }
        }
      });
      // [START] PUT
      // pemanggilan Put (PartsTransferOrder.cancel) didisable pre 02-12-2017
      // PartsTransferOrder.cancel($scope.mData).then(function(res) {
      //     console.log('res cancel = ', res);
      //     bsNotify.show(
      //         {
      //             title: "Approval",
      //             content: "Transfer Order has been successfully approved.",
      //             type: 'succes'
      //         }
      //       );
      //   },
      //   function(err) {
      //     console.log("err=>", err);
      //   }
      // );
      //PartsTransferOrder.formApi.setMode("grid");
    }
    
    $scope.sendapprove = function(data){
      if ($scope.mData.TransferOrderStatusId == 1 ) {
          $scope.mData.TransferOrderStatusId = 2; //status = open
          console.log('yakinApprove mData = ', $scope.mData);

          PartsTransferOrder.approveOrder($scope.mData).then(function(res) {
            console.log('sukses update');
            // for approval
            PartsGlobal.actApproval({
              ProcessId: $scope.ApprovalProcessId, DataId: $scope.mData.TransferOrderId, ApprovalStatus: 1
            }).then(function(res) {
                console.log('res cancel = ', res);
                bsNotify.show(
                    {
                        title: "Approval",
                        content: "Transfer Order has been successfully approved.",
                        type: 'succes'
                    }
                  );
              },
              function(err) {
                console.log("err=>", err);
              }
            );
          },
            function(err) {
              console.log("err=>", err);
            }
          );

        }else if($scope.mData.TransferRequestStatusId == 1){
            $scope.mData.TransferRequestStatusId = 2; //status = open
          console.log('yakinApprove mData = ', $scope.mData);

          PartsTransferOrder.approveRequest($scope.mData).then(function(res) {
            console.log('sukses update');
            // for approval
            PartsGlobal.actApproval({
              ProcessId: $scope.ApprovalProcessId, DataId: $scope.mData.TransferRequestId, ApprovalStatus: 1
            }).then(function(res) {
                console.log('res cancel = ', res);
                bsNotify.show(
                    {
                        title: "Approval",
                        content: "Transfer Request has been successfully approved.",
                        type: 'succes'
                    }
                  );
              },
              function(err) {
                console.log("err=>", err);
              }
            );
          },
            function(err) {
              console.log("err=>", err);
            }
          );

        }else {
          bsNotify.show(
              {
                  title: "Approval",
                  content: "Data has been approved. Cannot approve twice",
                  type: 'warning'
              }
            );
        }

      // [START] PUT
      // pemanggilan Put (PartsTransferOrder.cancel) didisable pre 02-12-2017
      // PartsTransferOrder.cancel($scope.mData).then(function(res) {
      //     console.log('res cancel = ', res);
      //     bsNotify.show(
      //         {
      //             title: "Approval",
      //             content: "Transfer Order has been successfully approved.",
      //             type: 'succes'
      //         }
      //       );
      //   },
      //   function(err) {
      //     console.log("err=>", err);
      //   }
      // );
      //PartsTransferOrder.formApi.setMode("grid");
    }

    // Reject
    $scope.RejectTS = function(data){
      console.log('RejectTS data = ', data);
      data[0].DocDate = $filter('date')(data[0].DocDate, 'yyyy-MM-dd');
      $scope.RejectData = data[0];
      //console.log('$scope.RejectData = ', $scope.RejectData);
      // PartsTransferOrder.setTransferOrderHeader($scope.RejectData);

      if(($scope.RejectData.ApprovalStatus == 0 && $scope.isTransferOrder == true) || 
         ($scope.RejectData.ApprovalStatus != 2 && $scope.isTransferOrder == false)) {

        ngDialog.openConfirm ({
          template:'\
                      <div align="center" class="ngdialog-buttons">\
                      <p><b>Konfirmasi</b></p>\
                      <p>Reject Transfer Order ini?</p>\
                      <div class="ngdialog-buttons" align="center">\
                        <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                        <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinReject(RejectData)">Ya</button>\
                      </div>\
                      </div>',
          plain: true,
          //controller: 'PartsTransferOrderController',
          scope:$scope
        });
        //console.log('$scope.RejectData bawah = ', $scope.RejectData);
      } else if ($scope.RejectData.ApprovalStatus == 2) {
        bsNotify.show(
          {
              title: "Approval",
              content: "Data sudah di-Reject",
              type: 'danger'
          }
        );
      } else {
        bsNotify.show(
          {
              title: "Approval",
              content: "Data sudah di-Approve",
              type: 'danger'
          }
        );
      }

    };

    $scope.yakinReject = function(data){
      console.log('yakinReject => ', data);
      $scope.mData = data;
      $scope.ngDialog.close();
      $scope.mData.TransferOrderStatusId = 13; //status = Reject
      $scope.mData.TransferRequestStatusId = 13; //status = Reject
      console.log('yakinReject mData = ', $scope.mData);
      // [START] PUT
      if ($scope.isTransferOrder == true) {
        PartsTransferOrder.cancel($scope.mData).then(function(res) {
            console.log('res cancel = ', res);
            // for approval
            PartsGlobal.actApproval({
              ProcessId: $scope.ApprovalProcessId, DataId: $scope.mData.TransferOrderId, ApprovalStatus: 2
            }).then(function(res) {
                console.log('res cancel = ', res);
                bsNotify.show(
                    {
                        title: "Approval",
                        content: "Transfer Order has been successfully rejected.",
                        type: 'succes'
                    }
                  );
              },
              function(err) {
                console.log("err=>", err);
              }
            );
          },
          function(err) {
            console.log("err=>", err);
          }
        );
      } else {
        PartsTransferOrder.rejectTR($scope.mData).then(function(res) {
            console.log('res cancel = ', res);
            // for approval
            PartsGlobal.actApproval({
              ProcessId: $scope.ApprovalProcessId, DataId: $scope.mData.TransferRequestId, ApprovalStatus: 2
            }).then(function(res) {
                console.log('res cancel = ', res);
                bsNotify.show(
                    {
                        title: "Approval",
                        content: "Transfer Request has been successfully rejected.",
                        type: 'succes'
                    }
                  );
              },
              function(err) {
                console.log("err=>", err);
              }
            );
          },
          function(err) {
            console.log("err=>", err);
          }
        );
      }
      //PartsTransferOrder.formApi.setMode("grid");
    }
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mData = {}; //Model
    $scope.mData.gridDetail = {};
    $scope.mData.gridBengkel = {};
    $scope.xData = {};
    $scope.mData.selected = [];
    $scope.xData.selected = [];
    $scope.xRole = {
      selected: []
    };
    $scope.formApi = {};
    $scope.mData.OutletId = $scope.user.OrgId;
    var uData = $scope.user;
    this.formLookupGrid= {};

    $scope.localeDate = function(data) {
      var tmpDate = new Date(data);
      var resDate = new Date(tmpDate.toISOString().replace("Z","-0700")).toISOString(); 
      return resDate;
    }

    //----------------------------------
    // Get Data
    //----------------------------------
    //var gridData = [];
    $scope.getData = function() {
      //console.log('$scope.columnsTO = ', $scope.columnsTO[4].visible = true);
      console.log('$scope.columnsTO = ', $scope.columnsTO);
      // console.log('$scope.columnsTO.columnDefs[1] = ', $scope.columnsTO.columnDefs[1]);
      // console.log('$scope.columnsTO.columnDefs[2] = ', $scope.columnsTO.columnDefs[2]);
      //$scope.gridOptions.columnDefs[0] = { field: $scope.gridOptions.columnDefs[0].name, visible: false };

      // $scope.isTransferOrder diupdate ketika tekan tombol Search
      $scope.isTransferOrder = $scope.isTempTO;
      console.log("$scope.isTransferOrder=>",$scope.isTransferOrder);

      if ($scope.mData.fTransferOrderStatusId == -1) {
        $scope.getDataNA();
        return 0;
      }
        var n = 0;
        if ($scope.isTransferOrder) {
          console.log('isTransferOrder true = ', $scope.isTransferOrder);
          $scope.grid.columnDefs = $scope.columnsTO;
          $scope.columnsTO[4] = { name:'request Ke Bengkel', displayName: 'Request Ke Bengkel', field: 'ToOutletName', width: '30%'};
          //$scope.columnsTO[5].visible = false;
          n = 1;
        } else {
          console.log('isTransferOrder else = ', $scope.isTransferOrder);
          $scope.grid.columnDefs = $scope.columnsTR;
          n = 2;
        }

        console.log('ctrl OutletId', $scope.mData.OutletId);

      //console.log("mData.fTransferOrderStatusId = ", $scope.mData.fTransferOrderStatusId);
      console.log("mData = ", $scope.mData);
      if(($scope.mData.fTransferOrderStatusId === undefined &&
        $scope.mData.startDate === undefined &&
        $scope.mData.endDate === undefined &&
        $scope.mData.fTransferOrderNo === undefined &&
        $scope.mData.OutletName === undefined)
        ||
        ($scope.mData.fTransferOrderStatusId == null &&
          $scope.mData.startDate == null &&
          $scope.mData.endDate == null &&
          $scope.mData.fTransferOrderNo == null &&
          $scope.mData.OutletName == null)
        ||
        (
          ($scope.mData.fTransferOrderStatusId == null || $scope.mData.fTransferOrderStatusId === undefined) &&
          ($scope.mData.startDate == null || $scope.mData.startDate === undefined) &&
          ($scope.mData.endDate == null || $scope.mData.endDate === undefined) &&
          $scope.mData.fTransferOrderNo == "" &&
          $scope.mData.OutletName == "")
      ){
        bsNotify.show(
          {
              title: "Mandatory",
              content: "Isi salah satu filter terlebih dahulu",
              type: 'danger'
          }
        );
      } else

      if(
        ($scope.mData.fTransferOrderStatusId === undefined || $scope.mData.fTransferOrderStatusId == null)
        &&
        (($scope.mData.fTransferOrderNo != null || $scope.mData.fTransferOrderNo !== undefined) ||
        ($scope.mData.OutletName != null || $scope.mData.OutletName !== undefined))
        &&
        (($scope.mData.startDate == null || $scope.mData.startDate === undefined) &&
        ($scope.mData.endDate == null || $scope.mData.endDate === undefined))
        ){
        console.log("filter is non mandatory", $scope.mData);
        PartsTransferOrder.getData($scope.mData, n, $scope.mData.OutletId).then(function(res) { // start getdata
            var gridData = res.data.Result;
            //var gridData = res.data;
            console.log('controller-getData-gridData = ', gridData);

            for (var i in gridData) {
              gridData[i].CreatedDate = $scope.localeDate(gridData[i].CreatedDate);
              console.log("gridData i =>",i);
            }
            console.log("getData==>",gridData);

            $scope.grid.data = gridData;
            $scope.loading = false;
          },
          function(err) {
            console.log("err=>", err);
          }
        ); // end getdata
        $scope.loading = false;
      } else {
        if($scope.mData.startDate == null || $scope.mData.startDate == 'undefined' || $scope.mData.startDate == ""){
          bsNotify.show(
              {
                  title: "Mandatory",
                  content: "Tanggal TO belum diisi",
                  type: 'danger'
              }
            );
        } else
        if($scope.mData.endDate == null || $scope.mData.endDate == 'undefined' || $scope.mData.endDate == ""){
          bsNotify.show(
              {
                  title: "Mandatory",
                  content: "Tanggal TO (end date) belum diisi",
                  type: 'danger'
              }
            );
        } else {
          PartsTransferOrder.getData($scope.mData, n, $scope.mData.OutletId).then(function(res) { // start getdata
              var gridData = res.data.Result;
              //var gridData = res.data;
              console.log('controller-getData-gridData = ', gridData);

              for (var i in gridData) {
                gridData[i].CreatedDate = $scope.localeDate(gridData[i].CreatedDate);
                console.log("gridData i =>",i);
              }
              console.log("getData==>",gridData);
  
              $scope.grid.data = gridData;
              $scope.loading = false;
            },
            function(err) {
              console.log("err=>", err);
            }
          ); // end getdata
          $scope.loading = false;
        }
      }

      // PartsTransferOrder.getData(mData, n).then(function(res) { // start getdata
      //     var gridData = res.data.Result;
      //     //var gridData = res.data;
      //     console.log('controller-getData-gridData = ', gridData);
      //     $scope.grid.data = gridData;
      //     $scope.loading = false;
      //   },
      //   function(err) {
      //     console.log("err=>", err);
      //   }
      // ); // end getdata

      // $scope.loading = false;
    }

    $scope.getDataApprove = function () {
      console.log('mData', $scope.mData);
      $scope.mData.fTransferOrderStatusId = "n/a";
      PartsTransferOrder.getDataApproveTO($scope.mData, 1, $scope.mData.OutletId).then(function(res) { // start getdata
          var gridData = res.data.Result;
          //var gridData = res.data;
          console.log('getDataApprove-gridData = ', gridData);
          $scope.grid.data = gridData;
          $scope.loading = false;
        },
        function(err) {
          console.log("err=>", err);
        }
      );
    }

    $scope.getDataNA = function() {
    /*
      PartsTransferOrder.getNeededApproval().then(function(res) { // start getdata NA
          var gridData = res.data;
          console.log('controller-getData-gridData NA = ', gridData);
          $scope.grid.data = gridData;
          $scope.loading = false;
        },
        function(err) {
          console.log("err=>", err);
        }
      ); // end getdata NA
    */
    } // end

    // Hapus Filter
    $scope.onDeleteFilter = function(){
      //Pengkondisian supaya seolaholah kosong
      $scope.mData.fTransferOrderStatusId = null;
      $scope.mData.fTransferOrderNo = null;
      $scope.mData.OutletName = null;
      $scope.mData.startDate = null;
      $scope.mData.endDate = null;
    }

    $scope.goBack = function()
    {
      console.log('goBack ');
      $scope.isOverlayForm = false;
      $scope.isViewRequest = false;
      $scope.hideSimpanButton = false;
      $scope.formApi.setMode("grid");
      $scope.gridMode = true;
      console.log('goBack = ', $scope.formApi);
    }

    $scope.Add = function(){
      console.log("[Add] $scope.mData=>",$scope.mData);
      var okInput = false;
      if($scope.mData.ToOutletId == null || $scope.mData.ToOutletId == undefined || $scope.mData.ToOutletId == '') {
        bsNotify.show({
          title: "Transfer Order",
          content: "Request ke Bengkel belum diisi",
          type: 'danger'
        });
      } else if($scope.mData.DateNeeded == null || $scope.mData.DateNeeded == undefined || $scope.mData.DateNeeded == '') {
        bsNotify.show({
          title: "Transfer Order",
          content: "Tanggal dibutuhkan belum diisi",
          type: 'danger'
        });
      } else {
        okInput = true;
      }

      if(okInput) {
        var n = $scope.gridOptions.data.length + 1;
        $scope.gridOptions.data.push({
          "PartId": "",
          "PartsName": "",
          "QtyTransfer": "",
          "UomId": ""
        });
      }
    }

  //$scope.dateOptions = {
        // maxDate: new Date(2016, 9, 29),
        // minDate: new Date(),
        //startingDay: 1,
        //format: dateFormat
        //disableWeekend: 1
        //disabled: true
    //};

    //========= mode form
    //inisiasi
    $scope.editModeDanView = false;
    $scope.editMode = false;
    $scope.isDisabled = false;

    $scope.ApproveData = PartsTransferOrder.getTransferOrderHeader();
    $scope.respondforMessage = PartsTransferOrder.getRespond();
    $scope.ApproveDataDetail = PartsTransferOrder.getTransferOrderDetail();

    $scope.Alasan = {};

    $scope.BatalData = PartsTransferOrder.getTransferOrderHeader();

    $scope.DataBengkel = PartsTransferOrder.getDataBengkel();

    $scope.onSelectRows = function(rows) {
      console.log("onSelectRows =>", rows);
    }


    $scope.alertAfterSave = function(item) {
        var noso = item.ResponseMessage;
        noso = noso.substring(noso.lastIndexOf('#') + 1);
        bsAlert.alert({
                title: "Data tersimpan dengan Nomor Transfer Order",
                text: noso,
                type: "warning",
                showCancelButton: false
            },
            function() {
                // $scope.formApi.setMode('grid');
                $scope.goBack();
            },
            function() {

            }
        )
    }
    // $scope.testHideEditButton = function(){
    //     $scope.hideEditButton=!$scope.hideEditButton;
    // }

    $scope.captionToFrom = 'Ke';
    $scope.changedValue = function (item){
        console.log('changedValue item = ', item);
        //alert('berubah');
        // $scope.isTransferOrder = item.id == 1;
        $scope.isTempTO = item.id == 1;
        var n = null;
        console.log('$scope.isTransferOrder = ', $scope.isTransferOrder);
        $scope.hideNewButton = (item.id == 2 ? true : false);
        $scope.captionToFrom = (item.id == 2 ? 'Dari' : 'Ke');
        $scope.actionHide=false;
        // if(item.id == 1){
        //     $scope.mData.OutletMana = $scope.mData.fToOutletName;
        // } else if (item.id == 2){
        //     $scope.mData.OutletMana = $scope.mData.fFromOutletName;
        // }
        //console.log('$scope.OutletMana = ', $scope.OutletMana);
    } // end changedValue

    // Generate Nomor Dokumen
    $scope.GenerateDocNum = function(){
      console.log("$scope.mData.ServiceTypeId = ", $scope.mData);
      PartsCurrentUser.getFormatId($scope.mData.ServiceTypeId, 'TO').then(function(res){
        var result = res.data;
        //console.log("FormatId result = ", result);
        console.log("FormatId = ", result[0].Results);

        PartsTransferOrder.getDocumentNumber(result[0].Results).then(function(res) {
            var DocNo = res.data;
            if(typeof DocNo === 'undefined' || DocNo == null)
            {
              bsNotify.show(
              {
                  title: "Transfer Order",
                  content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                  type: 'danger'
              }
              );
            }
            else
            {
              $scope.mData.TransferOrderNo = DocNo[0];
              $scope.mData.DocDate = DocNo[1]
              console.log("Generating DocNo & DocDate ->");
              console.log($scope.mData.TransferOrderNo);
              console.log($scope.mData.DocDate);
            }
          },
          function(err) {
            console.log("err=>", err);
          }
        );
      },
      function(err){
        console.log("err=>", err);
      }
      );
      // [END] Get Current User Warehouse
    }

      $scope.ToNameMaterial = function(MaterialTypeId){
        var materialName;
        switch(MaterialTypeId)
        {
          case 1:
            materialName = "Parts";
            break;
          case 2:
            materialName = "Bahan";
            break;
        }
        return materialName;
      }
      $scope.ToNameStatus = function(TransferRequestStatusId){
        var statusName;
        switch(TransferRequestStatusId)
        {
          case 1:
            statusName = "Request Approval";
            break;
          case 2:
            statusName = "Open";
            break;
          case 3:
            statusName = "Partial";
            break;
          case 4:
            statusName = "Completed";
            break;
          case 5:
            statusName = "Cancelled";
            break;
          // reject
          case 13:
            statusName = "Rejected";
            break;
        }
        return statusName;
      }

      $scope.ToNameStatus = function(StockAdjustmentStatusId){
        var statusName;
        switch(StockAdjustmentStatusId)
        {
          case 1:
            statusName = "Request Approval";
            break;
          case 2:
            statusName = "Open";
            break;
          case 3:
            statusName = "Partial";
            break;
          case 4:
            statusName = "Completed";
            break;
          case 5:
            statusName = "Cancelled";
            break;
          // reject
          case 13:
            statusName = "Rejected";
            break;
        }
        return statusName;
      }

    $scope.onBeforeNewMode = function(){ // tidak punya mode
        // alert('onBeforeEditMode');
        console.log("mode new =>");
        //$scope.showNewButton = true;
        PartsTransferOrder.formApi = $scope.formApi;
        $scope.editModeDanView = false;
        $scope.hideSimpanButton = false;
        $scope.editMode = false;
        $scope.isDisabled = false;
        $scope.gridOptions.data = [];

        PartsCurrentUser.getWarehouseIdi().then(function(res) {
          console.log("====",res.data); 
          for (var i=0; i < res.data.length; i++) {
            $scope.mData.WarehouseId = res.data[i].warehouseid;
            $scope.mData.MaterialTypeId = res.data[i].MaterialType;
            $scope.mData.ServiceTypeId = res.data[i].Servicetype;
            $scope.mData.OutletId = $scope.user.OrgId;
          }
            PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.user.OrgId).then(function(res) {
              var gridData = res.data;
              console.log('gridData[0] = ', gridData[0]);

              // $scope.mData.WarehouseId = gridData[0].WarehouseId;
              $scope.mData.OutletId = gridData[0].OutletId;
              $scope.mData.WarehouseData = gridData;

            },
            function(err) {
              console.log("err=>", err);
            }
          );
          $scope.GenerateDocNum();
  
        });

        console.log('ApprovalProcessId', $scope.ApprovalProcessId);
        PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res) {
                var Approvers = res.data.Result[0].Approvers;
                console.log("getApprover res = ", Approvers);
                $scope.mData.Approvers = Approvers;
            },
            function(err) {
                console.log("err=>", err);
            }
        );
        //console.log("before new, mode ? =>", mode);

        // if ($scope.user.RoleId == 1122) {
        //   $scope.mData.MaterialTypeId = 1;
        // }else if ($scope.user.RoleId == 1123) {
        //   $scope.mData.MaterialTypeId = 1;
        // }else if($scope.user.RoleId == 1125){
        //   $scope.mData.MaterialTypeId = 2;
        // }else if($scope.user.RoleId == 1124){
        //   $scope.mData.MaterialTypeId = 2;
        // }else if($scope.user.RoleId == 1128){
        //   $scope.mData.MaterialTypeId = 1;
        // }else if($scope.user.RoleId == 1129){
        //   $scope.mData.MaterialTypeId = 2;
        // }
        // $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
        // $scope.mData.ServiceTypeId = $scope.getRightServiceType();
        console.log("MaterialTypeId : ", $scope.mData.MaterialTypeId);
        console.log("ServiceTypeId : ", $scope.mData.ServiceTypeId);
        console.log("$scope.user.OrgId : ", $scope.user.OrgId);
    }

    $scope.onBeforeEditMode = function(row, mode){
        // alert('onBeforeEditMode');
        // console.log("mode=>"); showDetailButton
        //$scope.editMode = (mode=='edit' ? true:false);
        $scope.hideSimpanButton = true;
        $scope.editModeDanView = true;
        $scope.editMode = true;
        $scope.isDisabled = true;
        //console.log("row edit=>", row);
        console.log("mode edit=>", mode);

    }

    $scope.onShowDetail = function(row, mode,xparam){
        // $scope.gridOptions.data = [];
        //$scope.showViewButton = (mode=='view' ? true:false);
        PartsTransferOrder.setTransferOrderHeader(row);
        PartsTransferOrder.formApi = $scope.formApi;
        $scope.editModeDanView = true;
        $scope.isDisabled = true;
        $scope.disableBatalTO = false;

        console.log('onshowdetail row = ', row);
        $scope.mData = angular.copy(row);
        console.log('onshowdetail mdata notes= ', $scope.mData.Notes);
        if (mode == 'edit'){
            $scope.editMode = true;
            $scope.isModeEdit = true;
            $scope.isModeTambah = false;
            $scope.isModeView = false;
            $scope.isViewRequest = false;

            var obj={};
            obj.RefGITypeID=3;
            obj.RefGINo=row.TransferOrderNo;
            obj.MaterialTypeId=row.MaterialTypeId
            PartsGlobal.isGIDone(obj).then(function(resu) { 
              if(resu.data.Result[0].GIAlreadyDone == 1) {
                $scope.disableBatalTO=true;
              }else{
                $scope.disableBatalTO=false;
              }
            });


            PartsTransferOrder.getDetail(row.TransferOrderId).then(function(res) {
                var gridData = res.data.Result;
                console.log('PartsTransferOrder.getDetail = ', gridData);

                for (var i in gridData) {
                    gridData[i].CreatedDate = $scope.localeDate(gridData[i].CreatedDate);
                    console.log("gridData i =>",i);
                }
                console.log("getData==>",gridData);      

                $scope.gridOptionsView.data = gridData;
                
              },
              function(err) {
                console.log("err=>", err);
              }
            );

        }   else if (mode == 'view') {
            $scope.editMode = false;
            $scope.isModeEdit = false;
            $scope.isModeTambah = false;
            $scope.isModeView = true;
            $scope.isViewRequest = false;
            if ($scope.isTransferOrder == false){
              $scope.isViewRequest = true;
              $scope.isModeView = false;

              PartsTransferOrder.getDetailRequest(row.TransferRequestId).then(function(res) {
                  var gridData = res.data.Result;
                  console.log('PartsTransferOrder.getDetailRequest = ', gridData);
                  $scope.gridOptionsView.data = gridData;
                },
                function(err) {
                  console.log("err=>", err);
                }
              );

            }else{
              PartsTransferOrder.getDetail(row.TransferOrderId).then(function(res) {
                var gridData = res.data.Result;
                console.log('PartsTransferOrder.getDetail = ', gridData);
                $scope.gridOptionsView.data = gridData;
              },
              function(err) {
                console.log("err=>", err);
              }
            );
            }
            // PartsTransferOrder.getDetail(row.TransferOrderId).then(function(res) {
            //     var gridData = res.data.Result;
            //     console.log('PartsTransferOrder.getDetail = ', gridData);
            //     $scope.gridOptionsView.data = gridData;
            //   },
            //   function(err) {
            //     console.log("err=>", err);
            //   }
            // );

        } else if (mode == 'new'){
            $scope.isModeEdit = false;
            $scope.isModeTambah = true;
            $scope.isModeView = false;
            $scope.isViewRequest = false;
            $scope.isGridFilled = false;

            console.log("NEW========");
            console.log("xparam : ",xparam);
           
            if (typeof xparam !== 'undefined') {
              if (typeof xparam.fromOtherModule !== 'undefined' || xparam.fromOtherModule) {
                itIsLink = true;
                paramNew = angular.copy(xparam);
              }
            }
        }
        console.log("mode view=>", mode);
        //console.log("row view=>", row);

    }

    $scope.ToOutlet = function(){

      PartsGlobal.formLookupTitle = "Daftar Bengkel ";
      PartsGlobal.formLookupGrid = $scope.gridBengkel;
      PartsGlobal.formLookupGrid.data = $scope.getBengkel();

      ngDialog.openConfirm ({
        //template:'<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Input Alasan Close PO</h3></div> <div class="panel-body"><form method="post"><div class="form-group "> <label class="control-label requiredField" style="color:black;" for="message">Alasan<span class="asteriskField">*</span></label> <textarea class="form-control" cols="40" id="message" name="message" rows="10"></textarea></div> <div class="form-group"><div> <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Kembali</button> <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()" style="color:white; background-color:#D53337">OK</button> </div></div></form></div></div>',
        //template:'<div ng-include=\"\'app/parts/inquiry/orderStatus/templates/pendingDetail.html\'\"></div>',
        template:'<div ng-include=\"\'app/parts/templates01/frm_lookup01.html\'\"></div>',
        plain: true,
        controller: 'PartsTransferOrderController',
       }).then(function (value) {
              $scope.mySelections = PartsGlobal.selectedLookupGrid;
              console.log("mySelections : ", $scope.mySelections);

              _.map($scope.mySelections, function(val) {
                $scope.mData.ToOutletId = val.OutletId;
                $scope.mData.ToOutletName = val.Name;
                $scope.mData.ToWarehouseId = val.WarehouseId;
              });
              console.log("$scope.mData.ToOutletId : ", $scope.mData.ToOutletId,$scope.mData.ToWarehouseId);
              $scope.iniOutlet = 0;
              $scope.iniOutlet = $scope.mData.ToOutletId;
              //$scope.updateGrid(data, $scope.mySelections);
              $scope.gridOptions;
          }, function (value) {
            $scope.mySelections = PartsGlobal.selectedLookupGrid;
            console.log("No Data fetched ", $scope.mySelections);
            //scope.updateGrid(data, $scope.mySelections);
            $scope.mData.ToOutletId = $scope.mySelections[0].OutletId;
            $scope.mData.ToOutletName = $scope.mySelections[0].Name;
            $scope.mData.ToWarehouseId = $scope.mySelections[0].WarehouseId;
            PartsTransferOrder.getTOApprovers({ToOutletId : $scope.mySelections[0].OutletId}).then(function(res) {
              var gridData = res.data;
              console.log('PartsTransferOrder.Approvers = ', gridData);
              $scope.mData.Approvers = gridData[0].Approvers;
              console.log('Approvers : ', $scope.mData.Approvers);
            },
            function(err) {
              console.log("err=> No Approvers", err);
            });
            console.log("$scope.mySelections.OutletId : ", $scope.mySelections[0].OutletId);
            //console.log("$scope.mySelections[0].OutletId : ", $scope.mySelections[0].OutletId);
            //console.log("$scope.mData.ToOutletId : ", $scope.mData.ToOutletId);
            $scope.gridOptions;
          });
    };

    $scope.getBengkel = function(){

      $timeout(function(){

        $scope.userId = CurrentUser.user();
        console.log('$scope.mData.OutletId = ', $scope.userId.OrgId);
  
        // PartsTransferOrder.getCompanyId( $scope.userId.OrgId).then(function(res) {
        //       var gridData = res.data.Result;
        //       console.log('gridData[0] = ', gridData[0]);

        //       $scope.mData.CompanyId = gridData[0].CompanyId;
        //       $scope.mData.OutletId = gridData[0].OutletId;

        //       PartsTransferOrder.getOutlet($scope.mData.CompanyId, $scope.mData.OutletId).then(function(res) {
        //           var DataOutlet = res.data.Result;

        //           $scope.gridBengkel.data = DataOutlet;
        //           console.log("DataOutlet ", DataOutlet);
        //         },
        //         function(err) {
        //           console.log("err=>", err);
        //         }
        //       );

        //     },
        //     function(err) {
        //       console.log("err=>", err);
        //     }
        //   );
        PartsTransferOrder.getOutletNewVer($scope.mData.MaterialTypeId).then(function(res) {
            var DataOutlet = res.data.Result;

            $scope.gridBengkel.data = DataOutlet;
            console.log("DataOutlet ", DataOutlet);
          },
          function(err) {
            console.log("err=>", err);
          }
        );  
        

        }); // end timeout

    };

    // ======== tombol  simpan =========
    $scope.onValidateSave = function(data){
      data.GridDetail = $scope.gridOptions.data;
      data.DocDate = new Date();
      data.DateNeeded = $filter('date')(data.DateNeeded, 'yyyy-MM-dd');
      data.DocDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
      //$filter('date')(data.DateNeeded, 'yyyy-MM-dd');
      PartsTransferOrder.setTransferOrderHeader(data);
      PartsTransferOrder.setTransferOrderDetail(data.GridDetail);
      console.log('$scope.Data.DateNeeded = ', data.DateNeeded);
      console.log('$scope.Data = ', data);
      console.log('$scope.Data.GridDetail = ', data.GridDetail);
      var validQty = false;

      if(data.ToOutletName == null || data.ToOutletName === undefined){
        bsNotify.show(
          {
              title: "Mandatory",
              content: "Request Ke Bengkel belum diisi.",
              type: 'danger'
          }
        );
      } else
      if(data.DateNeeded == null || data.DateNeeded === undefined){
        bsNotify.show(
          {
              title: "Mandatory",
              content: "Tanggal Dibutuhkan belum diisi.",
              type: 'danger'
          }
        );
      } else {
        var countValidQty = 0;
        for (var i in $scope.mData.GridDetail) {
          if ($scope.mData.GridDetail[i].qtyTransfer > 0 && $scope.mData.GridDetail[i].qtyTransfer <= $scope.mData.GridDetail[i].QtyFree) {
            validQty = true;
            countValidQty++;
          } else {
            validQty = false;
            break;
          }
        }
        console.log('validQty ==> ',validQty);
        console.log('countValidQty ==> ',countValidQty);
        console.log('Grid length ==> ',$scope.mData.GridDetail.length);
        if (validQty === false && (countValidQty == 0 || countValidQty < $scope.mData.GridDetail.length)) {
          bsNotify.show({
              title: "Transfer Order",
              content: "Qty Transfer tidak valid",
              type: 'danger'
          });
          setTimeout(function(){
              $scope.checkRefGINo = false;
          },1000);
          return 0;
        } else {
          // dialog approval
          ngDialog.openConfirm ({
            template:'<div ng-include=\"\'app/parts/transferorder/templates/permohonanApproval.html\'\"></div>',
            plain: true,
            controller: 'PartsTransferOrderController',
          });
        } // end ngDialog
      } 
    };

    $scope.simpan = function(){

      $scope.mData.GridDetail = $scope.gridOptions.data;
      $scope.mData.DocDate = new Date();
      $scope.mData.DateNeeded = $filter('date')($scope.mData.DateNeeded, 'yyyy-MM-dd');
      $scope.mData.DocDate = $filter('date')($scope.mData.DocDate, 'yyyy-MM-dd');
      //$filter('date')(data.DateNeeded, 'yyyy-MM-dd');
      PartsTransferOrder.setTransferOrderHeader($scope.mData);
      PartsTransferOrder.setTransferOrderDetail($scope.mData.GridDetail);
      console.log('$scope.mData.DateNeeded = ', $scope.mData.DateNeeded);
      console.log('$scope.mData = ', $scope.mData);
      console.log('$scope.mData.GridDetail = ', $scope.mData.GridDetail);

      if($scope.mData.ToOutletName == null || $scope.mData.ToOutletName === undefined){
        bsNotify.show(
          {
              title: "Mandatory",
              content: "Request Ke Bengkel belum diisi.",
              type: 'danger'
          }
        );
      } else
      if($scope.mData.DateNeeded == null || $scope.mData.DateNeeded === undefined){
        bsNotify.show(
          {
              title: "Mandatory",
              content: "Tanggal Dibutuhkan belum diisi.",
              type: 'danger'
          }
        );
      } else {
        // dialog approval
        ngDialog.openConfirm ({
          template:'<div ng-include=\"\'app/parts/transferorder/templates/permohonanApproval.html\'\"></div>',
          plain: true,
          controller: 'PartsTransferOrderController',
        });
      } // end ngDialog
    };
    // ======== end tombol  simpan =========
    // ======== tombol  simpan Approval=========
    $scope.SimpanApproval = function(data)
    {
      console.log("Data untuk Approval ", data);

      $scope.mData = PartsTransferOrder.getTransferOrderHeader();
      //$scope.mData.GridDetail = PartsTransferOrder.getTransferOrderDetail();
      $scope.mData.GridDetail = $scope.ApproveDataDetail; //.getTransferOrderDetail();

      console.log("mData => ", $scope.mData);
      console.log("mData Detail=> ", $scope.mData.GridDetail);

      for(var i = 0; i < $scope.mData.GridDetail.length; i++)
      {
        $scope.mData.GridDetail[i].TransferOrderStatusId = 2; //for approval
        $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
        $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;
      }

      PartsTransferOrder.create($scope.mData, $scope.mData.GridDetail).then(function(res) {
            var create = res.data;
            console.log('res.data = ', res.data, $scope.ApproveData);
            if(typeof $scope.ApproveData[0].CancelReasonDesc == undefined){
              $scope.ApproveData[0].CancelReasonDesc = '';
              data.CancelReasonDesc = '';
            }
            PartsTransferOrder.sendNotif($scope.ApproveData[0].CancelReasonDesc, 1128).then(
              function(res) {
                console.log('terkirim ke KABENG')
              });
            bsNotify.show(
              {
                  title: "Transfer Order",
                  content: "Data berhasil disimpan",
                  type: 'success'
              },

              PartsTransferOrder.sendNotif(data.CancelReasonDesc, 1128).then(
                function(res) {
                  console.log('terkirim ke KABENG login 35')
                })
            );
            $scope.alertAfterSave(create);
          },
          function(err) {
            console.log("err=>", err);
          }
        );

      $scope.ngDialog.close();
      PartsTransferOrder.formApi.setMode("grid");

    }
    // ======== end tombol simpan Approval=========

    $scope.batal = function(){
      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/transferorder/templates/batal.html\'\"></div>',
        plain: true,
        controller: 'PartsTransferOrderController',
       });
    };

    $scope.hello = function(){
        alert('OK, PO telah dihapus');
        //console.log("");
    }

    $scope.cancelPO = function(){
      $scope.mData = row.entity;
      console.log('row = ', $scope.mData);

      PartsTransferOrder.setTransferOrderHeader($scope.mData);
      console.log('PartsTransferOrder.setTransferOrderHeader() = ', PartsTransferOrder.getTransferOrderHeader());

      ngDialog.openConfirm ({
        template:'<div class="panel panel-danger" style="color:black; border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;">Konfirmasi</div><div class="panel-body"><label class="control-label requiredField" for="message">Yakin akan di-Cancel?</label><br /><div><button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Kembali</button><button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello2()" style="color:white; background-color:#D53337;">OK</button></div></div></div>',
        plain: true,
        controller: 'PartsTransferOrderController',
       });
    };

    $scope.hello2 = function(){
        alert('PO telah di-Cancel');
        //console.log("");
    }

    // ======== tombol batal
    $scope.batalTO = function(){
       //$scope.mData = row.entity;
       console.log('row = ', $scope.mData);

      PartsTransferOrder.setTransferOrderHeader($scope.mData);
      console.log('PartsTransferOrder.setTransferOrderHeader() = ', PartsTransferOrder.getTransferOrderHeader());
      
      ngDialog.openConfirm ({
        template:'<div ng-include=\"\'app/parts/transferorder/templates/batal.html\'\"></div>',
        plain: true,
        controller: 'PartsTransferOrderController',
       });
       console.log('batalPC mData = ', $scope.mData.Notes);
    };

    // dialog konfirmasi
    $scope.okCancel = function(data){
      console.log('okCancel data = ', data);
      //$scope.mData = data.res;
      //console.log('okCancel $scope.mData = ', $scope.mData);
      $scope.BatalData = PartsTransferOrder.getTransferOrderHeader();
      $scope.BatalData.CancelReasonDesc = data.CancelReasonDesc;
      $scope.BatalData.CancelReasonId = data.CancelReasonId;
      console.log('$scope.BatalData = ', $scope.BatalData);
      PartsTransferOrder.setTransferOrderHeader($scope.BatalData);

      ngDialog.openConfirm ({
        template:'\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Anda yakin akan membatalkan Transfer Order ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinBatal(BatalData)">Ya</button>\
                     </div>\
                     </div>',
        plain: true,
        controller: 'PartsTransferOrderController',
       });
       console.log('okCancel CancelReasonDesc = ', $scope.BatalData.CancelReasonDesc);
       //console.log('okCancel mdata  = ', $scope.mData.CancelReasonDesc);
       console.log('okCancel CancelReasonId  = ', $scope.BatalData.CancelReasonId);
    };

    $scope.yakinBatal = function(data){
      console.log('yakinBatal');
      $scope.mData = data;
      $scope.ngDialog.close();
      $scope.mData.TransferOrderStatusId = 5;
      //$scope.mData.CancelReasonDesc;
      console.log('yakinBatal mData = ', $scope.mData);
      // [START] Get
      PartsTransferOrder.cancel($scope.mData).then(function(res) {
          console.log('res cancel = ', res);
          bsNotify.show(
              {
                  title: "Transfer Order",
                  content: "Transfer Order has been successfully canceled.",
                  type: 'succes'
              }
            );
        },
        function(err) {
          console.log("err=>", err);
        }
      );
      PartsTransferOrder.formApi.setMode("grid");
    }

    // ===================
    // kolom definisi grid
    // ===================

    // //  need approval column definition
    // $scope.columnsNeedApproval =
    // [
    //   {
    //       name: 'Id',
    //       field: 'TransferOrderId',
    //       width: '7%',
    //       visible: false
    //   },
    //   { name:'No TO', displayName: 'No. TO', field: 'TransferOrderNo', width: '20%'},
    //   { name:'tanggal TO',  displayName: 'Tanggal TO', field: 'DocDate', cellFilter: 'date:\'yyyy-MM-dd\'', width: '20%' },
    //   { name:'Date Needed',  displayName: 'Date Needed', field: 'DateNeeded', cellFilter: 'date:\'yyyy-MM-dd\'', width: '20%' },
    //   { name:'request Ke Bengkel', displayName: 'Request Ke Bengkel', field: 'ToOutletName', width: '30%' },
    //   { name:'Status Transfer Order', displayName: 'Status', field: 'TransferOrderStatusId', cellFilter: 'filterStatus', width: '20%'},
    // ];
    $scope.columnsTO =
        [
            {
                name: 'Id',
                field: 'TransferOrderId',
                width: '7%',
                visible: false
            },
            { name:'No TO', displayName: 'No. TO', field: 'TransferOrderNo', width: '20%'},
            { name:'tanggal TO',  displayName: 'Tanggal TO', field: 'DocDate', cellFilter: 'date:\'yyyy-MM-dd\'', width: '10%' },
            { name:'Date Needed',  displayName: 'Date Needed', field: 'DateNeeded', cellFilter: 'date:\'yyyy-MM-dd\'', width: '10%' },
            //{ name:'request Ke Bengkel', displayName: 'Request Ke Bengkel', field: 'ToOutletName', width: '30%', visible: false },
            { name:'request Dari Bengkel', displayName: 'Request Dari Bengkel', field: 'FromOutletName', width: '30%'},
            { name:'Status Transfer Order', displayName: 'Status', field: 'TransferOrderStatusId', cellFilter: 'filterStatus', width: '22%'},
        ];
    $scope.columnsTR =
        [
            {
                name: 'Id',
                field: 'TransferRequestId',
                width: '7%',
                visible: false
            },
            { name:'No TO', displayName: 'No. TO', field: 'TransferOrderNo', width: '20%'},
            { name:'tanggal TO',  displayName: 'Tanggal TO', field: 'DocDate', cellFilter: 'date:\'yyyy-MM-dd\'', width: '10%' },
            { name:'Date Needed',  displayName: 'Date Needed', field: 'DateNeeded', cellFilter: 'date:\'yyyy-MM-dd\'', width: '10%' },
            { name:'request Dari Bengkel', displayName: 'Request Dari Bengkel', field: 'FromOutletName', width: '30%' },
            { name:'Status Transfer Request', displayName: 'Status', field: 'TransferRequestStatusId', cellFilter: 'filterStatus', width: '22%'},
            {
              name: 'Action',
              //field: '_',
              pinnedRight:true,
              //cellTemplate: cellTempActSSB,
              width: '7%',
              cellTemplate: '<div style="text-align:center"><u> \
              <a href="#" ng-click="grid.appScope.gridClickViewDetailHandler(row.entity)">Lihat</a></u></div>'
              // <a href="#" ng-click="grid.appScope.$parent.onShowDetail(row.entity, \'view\')">Lihat</a></u></div>'
            }
        ];

    $scope.getTableHeightgridTO = function() {
      var rowHeight = 25; // your row height
      var headerHeight = 40; // your header height
      //var filterHeight = 40; // your filter height
      var pageSizegridTO =  $scope.gridOptions.paginationPageSize;

      // if ( $scope.gridOptions.paginationPageSize >  $scope.gridOptions.data.length) {
      //     pageSizegridTO =  $scope.gridOptions.data.length;
      // }
      if (pageSizegridTO < 10) {
          pageSizegridTO =  10;
      }

      return {
          height: (pageSizegridTO * rowHeight + headerHeight) + 50 + "px"
      };
    };

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: false,
        enableSelectAll: false,
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100, 200],
        paginationPageSize: 15,
        columnDefs: $scope.columnsTO,
        //columnDefs: $scope.columnsNeedApproval,
        // data: $scope.getData({fTransferOrderStatusId : -1})
    };

    $scope.SearchMaterial = function(NoMaterial){
      //sudah hilang
      console.log ('Isinya apa',$scope.iniOutlet);
      var dataFilter ={ PartsCode : NoMaterial, OutletId :  $scope.mData.ToOutletId};
      PartsTransferOrder.getDetailByMaterialNo(dataFilter).then(function(res) {
        console.log ('Isinya apa',res.data.Result[0]);
        if (res.data.Result[0].IsnullOnVendor == 1 || res.data.Result[0].IsnullOnVendor == '1'){
            var gridData = res.data.Result;
            console.log('co',gridData[0]);
            var tmpIdx = _.findIndex($scope.gridOptions.data, {PartsCode:NoMaterial})
            if(tmpIdx > -1){
              $scope.gridOptions.data[tmpIdx] = gridData[0];
            }
            $scope.mData.GridDetail = $scope.gridOptions.data;
            $scope.isGridFilled = true;
          }else{
            bsNotify.show(
              {
                  title: "Transfer Order",
                  content: "Stock pada dealer tersebut tidak tersedia.",
                  type: 'danger'
              }
            );
            }
          
        },
        function(err) {
          console.log("err=>", err);
          confirm("Nomor Material Tidak Ditemukan.");
        }
      );
    }

    $scope.showMe = function(){
      var result = confirm('Apakah Anda yakin menghapus data ini?');
      if(result){
        console.log($scope.gridApi.selection.getSelectedRows());
        var data = $scope.gridApi.selection.getSelectedRows();
        console.log("selected", $scope.gridApi);
        console.log("selected", data);
        angular.forEach($scope.gridApi.selection.getSelectedRows(), function (data, index) {
          $scope.gridOptions.data.splice($scope.gridOptions.data.lastIndexOf(data), 1);
        });
      }
    };

    //----------------------------------
    // Grid Detail
    //----------------------------------
    $scope.gridOptions = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            enableFiltering : true,

            onRegisterApi: function(gridApi) {
              $scope.gridApi = gridApi;
              gridApi.selection.on.rowSelectionChanged($scope,function(row) {
                $scope.mySelections = gridApi.selection.getSelectedRows();
                console.log("selected gridOptions=>",$scope.mySelections);
                if($scope.onSelectRows){
                    $scope.onSelectRows($scope.mySelections);
                }
              });
              gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {

                console.log('rowEntity', rowEntity);
                console.log('colDef', colDef);
                console.log('newValue', newValue);
                console.log('oldValue', oldValue);
                console.log('Position', row);
                console.log('OutletId', $scope.mData.ToOutletId);
                console.log('QtyTransfer', rowEntity.qtyTransfer, rowEntity.QtyTransfer);

                if ( rowEntity.QtyFree < rowEntity.qtyTransfer ) {
                  bsNotify.show(
                    {
                        title: "Transfer Order",
                        content: "Stock tidak mencukupi, hanya tersedia sebanyak "+rowEntity.QtyFree,
                        type: 'danger'
                    }
                  );
                  rowEntity.qtyTransfer = oldValue;
                  return;
                }
              });
            },

            // enableHorizontalScrollbar: 2,
            // enableVerticalScrollbar: 2,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 10,
            

            columnDefs: [
                {
                  name:'PartsCode',
                  displayName:'No. Material',
                  enableCellEdit: true,
                  //field: 'NoMaterial',
                  width: '20%',
                  enableCellEditOnFocus: true,
                  cellTemplate:'<div class="form-group"><div class="input-group">\
                            <input class="form-control" type="text" ng-model="row.entity.PartsCode" ng-value="row.entity.PartsCode" required>\
                            <label class="input-group-btn">\
                                <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
                                    <i class="fa fa-search fa-1"></i>\
                                </span>\
                            </label>\
                        </div></div>'
                        // <input class="form-control" type="text" ng-model="mData.PartId" ng-value="row.entity.PartsCode" required>\ 1 line ini dipakai sampai 30 mei 2018 tapi error
                  // cellTemplate:'\
                  // <div class="ui-grid-cell-contents"><div class="input-group">\
                  // <input class="form-control" type="text" ng-model="mData.PartsCode" ng-value="row.entity.PartsCode" required>\
                  //   <button class="btn btn-default btn-xs" type="button" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)"><span class="glyphicon glyphicon-search"></span></button>\
                  // </div></div>'
                },
                // { name:'noMaterial', displayName: 'No. Material', width: '20%', field:'PartsCode' },
                { name:'PartId', displayName:'PartId', field: 'PartId', visible: false },
                { name:'nama Material', displayName: 'Nama Material', width: '20%', field:'PartsName', enableCellEdit: false},
                { name:'qty Free', displayName: 'Qty Free', width: '15%', field:'QtyFree', enableCellEdit: false },
                { name:'qty Transfer', displayName: 'Qty Transfer', width: '15%', field:'qtyTransfer', enableCellEdit: true },
                { name:'UomId', displayName: 'UomId', width: '20%', field:'UomId', visible: false },
                { name:'satuan', displayName: 'Satuan', width: '10%', field:'SatuanName', enableCellEdit: false},
                {
                  name: '_',
                  displayName: 'Action',
                  visible: true,
                  cellTemplate: '<div style="padding: 5px;"><u><p><a href="#" ng-click="grid.appScope.Delete(row)">Hapus</a></p></u></div>'
                }
            ]
    };
    //$scope.gridOptions.data = [];
    $scope.Delete = function(row) {
      console.log("row=>",row);
      var index = $scope.gridOptions.data.indexOf(row.entity);
      $scope.gridOptions.data.splice(index, 1);
      console.log($scope.gridOptions.data);
      console.log(index);
      if ($scope.gridOptions.data.length == 0) {
        $scope.isGridFilled = false;
      }
    };

    $scope.getSelectedItemIds= function(item) {
      var res = ';';
      for (var i=0; i<item.length; i++) {
        res += item[i].noMaterial+';'; //.toString;
      }
      return res;
    };
    //$scope.gridBengkel.multiSelect = false;
    $scope.getSelectedRows = function () {
        $scope.mySelectedRows = $scope.gridApi.selection.getSelectedRows();
    };

    //grid detail
    $scope.gridBengkel = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: true,
            enableFiltering : true,
            onRegisterApi: function(gridApi) {
              $scope.gridApiBengkel = gridApi;
              gridApi.selection.on.rowSelectionChanged($scope,function(rows){
                  $scope.mySelections = gridApi.selection.getSelectedRows();
                  console.log('$scope.mySelections', $scope.mySelections);
                  console.log('namaMaterial ', $scope.mySelections[0].namaMaterial);// ...
                  //build aray string
                  PartsGlobal.selectedLookupGrid = $scope.mySelections;
                  //$scope.itemids= $scope.getSelectedItemIds($scope.mySelections);
                  //console.log("item ids : ", $scope.itemids);
              });
            },
            // enableHorizontalScrollbar: 2,
            // enableVerticalScrollbar: 2,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 5,
            columnDefs: [
                // { name:'noMaterial', displayName: 'No. Material', width: '20%', field:'noMaterial' },
                // { name:'namaMaterial', displayName: 'Nama Material', width: '20%', field:'namaMaterial' },
                { name:'Outlet Id', displayName: 'Id', width: '20%', field:'OutletId' },
                { name:'Outlet Code', displayName: 'Outlet Code', width: '20%', field:'OutletCode' },
                { name:'Name', displayName: 'Outlet Name', width: '20%', field:'Name' },
                { name:'WarehouseId', displayName: 'Warehouse Id', width: '20%', field:'WarehouseId', visible : false }

            ],
            data: []

            // data: [
            //     {
            //     "noMaterial": "09095-00012",
            //     "namaMaterial": "Expander",
            //     "qtyTransfer": 3,
            //     "satuan": "Pieces"
            //     },
            //     {
            //     "noMaterial": "49095-00011",
            //     "namaMaterial": "ABSORBER A/S FR LH",
            //     "qtyTransfer": 10,
            //     "satuan": "Pieces"
            //     },
            //     {
            //     "noMaterial": "P9095-00009",
            //     "namaMaterial": "COMPRESSOR A/S",
            //     "qtyTransfer": 5,
            //     "satuan": "Pieces"
            //     },
            //     {
            //     "noMaterial": "89095-00016",
            //     "namaMaterial": "TIRE MONITOR SYSTEM",
            //     "qtyTransfer": 3,
            //     "satuan": "Set"
            //     }
            // ]
    };

    //grid detail
    $scope.gridOptionsView = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            enableFiltering : true,
            // enableHorizontalScrollbar: 2,
            // enableVerticalScrollbar: 2,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 10,

            columnDefs: [
              { name:'PartId', displayName:'PartId', field: 'PartId', visible: false },
              { name:'nomorMaterial', displayName: 'Nomor Material', width: '20%', field:'PartsCode', enableCellEdit: false},
              { name:'namaMaterial', displayName: 'Nama Material', width: '20%', field:'PartsName', enableCellEdit: false},
              { name:'qtyTransfer', displayName: 'QTY Transfer', width: '20%', field:'QtyTransfer', enableCellEdit: false },
              { name:'UomId', displayName: 'UomId', width: '20%', field:'UomId', visible: false },
              { name:'satuan', displayName: 'Satuan', width: '20%', field:'Name', enableCellEdit: false}
            ]
    };
  $scope.getDataApproval = function(mData) { // fungsi ini belum dipake, mungkin malah gak akan dipake
      $scope.IsApproval = true;
      console.log("getData mData = ", $scope.mData);
      // PartsStatusStock.getListStockReservedReqApproval({PartId : $scope.PartId}).then(function(res) {
      PartsTransferOrder.getListNeedApproval().then(function(res) {
        console.log('isOutstandingSO di getData = ', $scope.isOutstandingSO);
          var gridData = res.data.Result;
          console.log("<controller getDataApproval> GridData => ", gridData);
          $scope.gridLookUpApprovalAlokasi.data = gridData;
          $scope.loading = false;
          // $scope.IsApproval = true;
        },
        function(err) {
          console.log("err=>", err);
        }
      );
    }
  $scope.gridLookUpApprovalAlokasi = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        onRegisterApi: function (gridApi) {
          $scope.gridApi = gridApi;
          gridApi.selection.on.rowSelectionChanged($scope,function(rows){
              $scope.mySelections = gridApi.selection.getSelectedRows();
              console.log("Item(s) : ", $scope.mySelections); // ...
              PartsGlobal.selectedLookupGrid = $scope.mySelections;
          });
        },
        columnDefs: [
            { name:'id',    field:'MaterialRequestItemId', width:'7%', visible:false },
            { name:'Referensi Transaksi', field:'RefMRTypeId', enableCellEdit: false,
              editableCellTemplate: 'ui-grid/dropdownEditor', cellFilter: 'mapTrx',
              editDropdownValueLabel: 'trx',
              editDropdownOptionsArray: [{ id: 1, trx: 'Appointment' }, { id: 2, trx: 'WO' },
              { id: 3, trx: 'Sales Order' }] },
            { name:'No Referensi ',  field: 'RefMRNo', enableCellEdit: false },
            { name:'VehicleId',  field: 'VehicleId', enableCellEdit: false, visible:false },
            { name:'No Polisi',  field: 'LicensePlate', enableCellEdit: false },
            { name:'Tgl Dibutuhkan',  field: 'RequiredDate', enableCellEdit: false },
            { name:'Petugas', field: 'RequestBy', enableCellEdit: false },
            { name:'No PO',  field: 'idpo', displayName: 'No. PO', visible:false },
            { name:'No GR',  field: 'idgr', displayName: 'No. GR', visible:false },
            { name:'PartId', field:'PartId', visible:false},
            { name:'Qty Reserved',  field: 'QtyReserved', enableCellEdit: false },
            { name:'Transaksi',  field: 'RefAllocTypeId', enableCellEdit: true,
              editableCellTemplate: 'ui-grid/dropdownEditor', cellFilter: 'mapTrx',
              editDropdownValueLabel: 'trx',
              editDropdownOptionsArray: [{ id: 1, trx: 'Appointment' }, { id: 2, trx: 'WO' },
              { id: 3, trx: 'Sales Order' }] },
            { name:'id trx',  field: 'RefAllocId', visible: true, enableCellEdit: false},
            //{ name:'No Transaksi',  field: 'RefAllocNo', enableCellEdit: true, cellTemplate: cellTrxNo},
            { name:'Qty Alokasi',  field: 'Qty', enableCellEdit: true},
            { name:'Status ',  field: 'StockReservedStatusId', enableCellEdit: false,
              editableCellTemplate: 'ui-grid/dropdownEditor', //cellFilter: 'mapStatusReserved',
              editDropdownValueLabel: 'trx',
              editDropdownOptionsArray: [{ id: 0, trx: 'Request Approval' }, { id: 1, trx: 'Completed' }]
            }
        ]
        ,
         data : []//$scope.getDataReserved({MaterialRequestId : -1})
    };

})
.filter('filterStatus', function () {
  var xstatus = {
    '1': 'Request Approval',
    '2': 'Open',
    '3': 'Partial',
    '4': 'Completed',
    '5': 'Cancelled',
    '13': 'Rejected'
  };
  return function(input) {
    if (!input){
      return '';
    } else {
      return xstatus[input];
    }
  };
})

; // end of all
