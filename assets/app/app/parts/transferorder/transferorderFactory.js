angular.module('app')
  .factory('PartsTransferOrder', function($http, CurrentUser, $filter) {
    var currentUser = CurrentUser.user;
    var elm;
    var TransferOrderHeader = {};
    var TransferOrderDetail = {};
    var DataBengkel = {};
    var arr = [];
    this.formApi = {};
    return {
      getData: function(data, n, OutletId, TransferOrderNo, OutletName) {
        console.log("[Factory] data = ", data);
        console.log("[Factory] n = ", n);
        console.log("[Factory] OutletId = ", OutletId);
        console.log("[Factory] TransferOrderNo = ", TransferOrderNo);
        console.log("[Factory] OutlateName = ", OutletName);
        //filter tanggal
        var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
        var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');

        var res=$http.get('/api/as/TransferOrder', {params: {
                                                          FromTable: (n == null ? "n/a" : n),
                                                          OutletId: (OutletId == null ? "n/a" : OutletId),
                                                          TransferOrderStatusId: (data.fTransferOrderStatusId==null?"n/a":data.fTransferOrderStatusId),
                                                          startDate: (startDate==null?"n/a":startDate),
                                                          endDate: (endDate==null?"n/a":endDate),
                                                          TransferOrderNo: (data.fTransferOrderNo==null?"n/a":data.fTransferOrderNo),
                                                          OutletName: (data.OutletName==null?"n/a":data.OutletName)
                                                          //TransferOrderNo: (data.fTransferOrderNo==null),
                                                          //OutletName: (data.OutletName==null)

                                                          //(n == 1 ? data.fToOutletName : data.fFromOutletName)
                                                          //(data.fToOutletName==null?"n/a":data.fToOutletName)
                                                        } });
        console.log('hasil=>',res);
        return res;
      },
      getDataApproveTO: function(data, n, OutletId) {
        //filter tanggal
        var startDate = $filter('date')('2017-01-01', 'yyyy-MM-dd');
        var endDate = $filter('date')(new Date(), 'yyyy-MM-dd');

        var res=$http.get('/api/as/TransferOrder', {params: {
                                                          FromTable: (n == null ? "n/a" : n),
                                                          OutletId: (OutletId == null ? "n/a" : OutletId),
                                                          TransferOrderStatusId: (data.fTransferOrderStatusId==null?"n/a":data.fTransferOrderStatusId),
                                                          startDate: (startDate==null?"n/a":startDate),
                                                          endDate: (endDate==null?"n/a":endDate),
                                                          TransferOrderNo: (data.fTransferOrderNo==null?"n/a":data.fTransferOrderNo),
                                                          OutletName: (data.OutletName==null?"n/a":data.OutletName)
                                                          //(n == 1 ? data.fToOutletName : data.fFromOutletName)
                                                          //(data.fToOutletName==null?"n/a":data.fToOutletName)
                                                        } });
        console.log('hasil=>',res);
        return res;
      },
      getNeededApproval: function() {
        console.log("getNeededApproval");
        var res=$http.get('/api/as/TransferOrder/GetNeedApproval');
        console.log('hasil=>',res);
        return res;
      },
      getDocumentNumber: function(FormatId) {
        console.log("[getDocumentNumber]");
        console.log('FormatId', FormatId);
        var res=$http.get('/api/as/PartsUser/GetDocumentNumber', {params: {
                                                          FormatId : FormatId,
                                                        } });
        console.log('hasil=>',res);
        return res;
      }, //api/as/TransferOrder/GetTOApprovers
      getTOApprovers: function(data) {
        console.log("get Approvers", data);
        var res=$http.get('/api/as/TransferOrder/GetTOApprovers', {params: {ToOutletId : data.ToOutletId}});
        console.log('hasil=>',res);
        return res;
      },
      create: function(data, detail) {
        console.log('tambah data =>', data);
        console.log('tambah detail =>', detail);
        var vDocDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
        var vDateNeeded = $filter('date')(data.DateNeeded, 'yyyy-MM-dd');
        return $http.post('/api/as/TransferOrder', [{
                                            TransferOrderNo: data.TransferOrderNo,
                                            OutletId: data.OutletId,
                                            WarehouseId: data.WarehouseId,
                                            DocDate: vDocDate,
                                            MaterialTypeId: data.MaterialTypeId,
                                            ToOutletId: data.ToOutletId,
                                            ToWarehouseid: data.ToWarehouseId, //1, // ???
                                            DateNeeded: vDateNeeded,
                                            Notes: data.Notes,
                                            TransferOrderStatusId: (data.TransferOrderStatusId == null ? "1": data.TransferOrderStatusId),
                                            CancelReasonDesc: data.CancelReasonDesc,
                                            ApprovalStatus: 0, // diset static, 0= Request Approval
                                            GridDetail: detail
                                          }]);
      },
      // Detail digunakan saat View dan Edit
      getDetailByMaterialNo: function(data){
        console.log("[getDetail]");
        console.log(data);
        var res=$http.get('/api/as/TransferOrder/GetTransferOrderItemFromMaterial', {params: {
                                                          PartsCode : data.PartsCode,
                                                          OutletId : data.OutletId
                                                        } });
        console.log('hasil=>',res);
        //res.data.Result = null;
        return res;

      },
      getWarehouse: function(data) {
        console.log("[Factory] getWarehouse data = ", data);
        var res=$http.get('/api/as/StatusStock/GetWarehouse2', {params: {OrgId : data.OrgId}});
        console.log('getWarehouse =>',res);
        return res;
      },
      getDetail: function(data){
        console.log('<factory> getDetail = ', data);
        var res=$http.get('/api/as/TransferOrder/GetTransferOrderDetail', {params: {
                                                          TransferOrderId : data,
                                                        } });
        console.log('hasil=>',res);
        return res;
      },
      getDetailRequest: function(data){
        console.log('<factory> getDetailRequest = ', data);
        var res=$http.get('/api/as/TransferOrder/GetTransferRequestDetail', {params: {
                                                          TransferRequestId : data,
                                                        } });
        console.log('hasil=>',res);
        return res;
      },
      cancel: function(data){
        console.log('batal data=>', data);
        var vDocDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
        var vDateNeeded = $filter('date')(data.DateNeeded, 'yyyy-MM-dd');

        return $http.put('/api/as/TransferOrder', [{
                                            TransferOrderId: (data.TransferOrderId==null?0:data.TransferOrderId),
                                            TransferOrderNo: data.TransferOrderNo,
                                            OutletId: data.OutletId,
                                            WarehouseId: data.WarehouseId,
                                            DocDate: vDocDate,
                                            MaterialTypeId: data.MaterialTypeId,
                                            ToOutletId: data.ToOutletId,
                                            ToWarehouseId: data.ToWarehouseId,
                                            DateNeeded: vDateNeeded,
                                            Notes: data.Notes,
                                            TransferOrderStatusId: data.TransferOrderStatusId,
                                            TransferRequestId: data.TransferRequestId,
                                            CancelReasonDesc: data.CancelReasonDesc,
                                            CancelReasonId: data.CancelReasonId,
                                            CreatedDate: data.CreatedDate,
                                            CreatedUserId: data.CreatedUserId
                                          }]);
      },
      rejectTR: function(data){
        console.log('batal data=>', data);
        var vDocDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
        var vDateNeeded = $filter('date')(data.DateNeeded, 'yyyy-MM-dd');

        return $http.put('/api/as/TransferOrder/RejectTR', [{
                                            TransferRequestId: (data.TransferRequestId==null?0:data.TransferRequestId),
                                            TransferOrderNo: data.TransferOrderNo,
                                            OutletId: data.OutletId,
                                            WarehouseId: data.WarehouseId,
                                            DocDate: vDocDate,
                                            MaterialTypeId: data.MaterialTypeId,
                                            FromOutletId: data.FromOutletId,
                                            FromWarehouseId: data.FromWarehouseId,
                                            DateNeeded: vDateNeeded,
                                            Notes: data.Notes,
                                            TransferRequestStatusId: data.TransferRequestStatusId,
                                            TransferOrderId: data.TransferOrderId,
                                            CancelReasonDesc: data.CancelReasonDesc,
                                            CancelReasonId: data.CancelReasonId,
                                            CreatedDate: data.CreatedDate,
                                            CreatedUserId: data.CreatedUserId
                                          }]);
      },
      approveOrder: function(data){
        console.log('approveOrder data=>', data);
        var vDocDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
        var vDateNeeded = $filter('date')(data.DateNeeded, 'yyyy-MM-dd');

        return $http.put('/api/as/TransferOrder/ApproveTO', [{
                                            TransferOrderId: (data.TransferOrderId==null?0:data.TransferOrderId),
                                            TransferOrderNo: data.TransferOrderNo,
                                            OutletId: data.OutletId,
                                            WarehouseId: data.WarehouseId,
                                            DocDate: vDocDate,
                                            MaterialTypeId: data.MaterialTypeId,
                                            ToOutletId: data.ToOutletId,
                                            ToWarehouseid: data.ToWarehouseId,
                                            DateNeeded: vDateNeeded,
                                            Notes: data.Notes,
                                            TransferOrderStatusId: data.TransferOrderStatusId,
                                            CancelReasonDesc: data.CancelReasonDesc,
                                            CancelReasonId: data.CancelReasonId,
                                            CreatedDate: data.CreatedDate,
                                            CreatedUserId: data.CreatedUserId
                                          }]);
      },
      approveRequest: function(data){
        console.log('approveRequest data=>', data);
        var vDocDate = $filter('date')(data.DocDate, 'yyyy-MM-dd');
        var vDateNeeded = $filter('date')(data.DateNeeded, 'yyyy-MM-dd');

        return $http.put('/api/as/TransferOrder/ApproveTR', [{
                                            TransferRequestId: (data.TransferRequestId==null?0:data.TransferRequestId),
                                            TransferOrderNo: data.TransferOrderNo,
                                            OutletId: data.OutletId,
                                            WarehouseId: data.WarehouseId,
                                            DocDate: vDocDate,
                                            MaterialTypeId: data.MaterialTypeId,
                                            FromOutletId: data.FromOutletId,
                                            FromWarehouseId: data.FromWarehouseId,
                                            DateNeeded: vDateNeeded,
                                            Notes: data.Notes,
                                            TransferRequestStatusId: data.TransferRequestStatusId,
                                            CreatedDate: data.CreatedDate,
                                            CreatedUserId: data.CreatedUserId
                                          }]);
      },
      getOutlet: function(data1, data2) {
        console.log("[Factory] getOutlet data1 = ", data1);
        console.log("[Factory] getOutlet data2 = ", data2);
        // var res=$http.get('/api/as/TransferOrder/Outlet', {params: {CompanyId : data1, OutletId : data2}});
        // api/as/PartGlobal/VendorOutlet
        var res=$http.get('/api/as/PartGlobal/VendorOutlet');
        console.log('getOutlet =>', res);
        return res;
      },
      getOutletNewVer: function(data1) {
        var res=$http.get('/api/as/PartGlobal/VendorOutletWarehouse?MaterialTypeId=' + data1);
        console.log('getOutlet =>', res);
        return res;
      },
      getCompanyId: function(data) {
        console.log("[Factory] getCompanyId data = ", data);
        var res=$http.get('/api/as/TransferOrder/CompanyId', {params: {OutletId : data}});
        console.log('getCompanyId =>', res);
        return res;
      },
      getStatusTO: function(data){
        var res=$http.get('/api/as/TransferOrder/CekStockRequest?TransferOrderId='+data);
        return res;
      },
      getSearchParts: function(data){
        var res=$http.get('/api/as/TransferOrder/SearchParts',[{
          PartsCode: data.PartsCode,
          OutletId: data.OutletId
        }]);
        return res;
      },
      setTransferOrderHeader : function(data){
        TransferOrderHeader = data;
      },
      setRespond : function(data){
        arr = data;
      },
      getTransferOrderHeader : function(){
        return TransferOrderHeader;
      },
      getRespond : function(){
        return arr;
      },
      setTransferOrderDetail : function(data){
        TransferOrderDetail = data;
      },
      getTransferOrderDetail : function(){
        return TransferOrderDetail;
      },

      setDataBengkel : function(data){
        DataBengkel = data;
      },
      getDataBengkel : function(){
        return DataBengkel;
      },
      sendNotif: function(data, recepient) {
        // console.log("model", IdSA);
        // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
        return $http.post('/api/as/SendNotificationForRole', [{
            Message: data,
            RecepientId: recepient,
            Param : 12
        }]);
      },
      sendNotif: function(data, recepient) {
        // console.log("model", IdSA);
        // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
        return $http.post('/api/as/SendNotification', [{
            Message: data,
            RecepientId: recepient,
            Param : 5
        }]);
    }
    }
  });
