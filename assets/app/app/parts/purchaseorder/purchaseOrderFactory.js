angular.module('app')
    .factory('PurchaseOrder', function($http, CurrentUser, LocalService, $filter) {
        var currentUser = CurrentUser.user;
        // console.log(currentUser);
        var VendorData = {};

        var PurchaseOrderHeader = {};
        var PurchaseOrderDetail = {};
        var ETDLoc = 0;
        var ETALoc = 0;

        this.gridDetailLength = 0;
        this.formApi = {};
        this.GridDetail = [];
        this.GridData = {};
        this.crudState = '';
        this.MinTotalAmount = 0;
        this.NeedApproval = 0;

        return {
            getListAppointmetNo: function(key) {
                return $http.put('/api/as/GetListAppointmentNo/', [{
                    Key: key
                }]);
            },
            getData: function(data) {
                console.log("[Factory]");
                console.log(data);
                //filter tanggal
                var startDate = $filter('date')(data.startDate, 'yyyy-MM-dd');
                var endDate = $filter('date')(data.endDate, 'yyyy-MM-dd');
                var res = $http.get('/api/as/PurchaseOrder', {
                    params: {
                        POTypeId: (data.POTypeId == null ? "n/a" : data.POTypeId),
                        RefPOTypeId: (data.RefPOTypeId == null ? "n/a" : data.RefPOTypeId),
                        PurchaseOrderStatusId: (data.PurchaseOrderStatusId == null ? "n/a" : data.PurchaseOrderStatusId),
                        RefPONo: (data.RefPONo == null ? "n/a" : data.RefPONo),
                        PurchaseOrderNo: (data.PurchaseOrderNo == null ? "n/a" : data.PurchaseOrderNo),
                        HasilSOQ: (data.HasilSOQ == null ? false : data.HasilSOQ),
                        VendorName: (data.VendorName == null ? "n/a" : data.VendorName),
                        LicensePlate: (data.LicensePlate == null ? "n/a" : data.LicensePlate),
                        PartsCode: (data.PartsCode == null ? "n/a" : data.PartsCode),
                        startDate: (startDate == null ? "n/a" : startDate),
                        endDate: (endDate == null ? "n/a" : endDate)
                    }
                });
                //console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            create: function(data, detail) {
                console.log('create=>', data);
                console.log('detail=>', detail);
                if(data.IsNONPKP == true){
                    data.IsNONPKP = 1;
                }else if(data.IsNONPKP == false){
                    data.IsNONPKP = 0;
                }

                for(var a in detail){
                    if(detail[a].ETA == '-'){
                        detail[a].ETA = '';
                    }

                    if(detail[a].ETD == '-'){
                        detail[a].ETD = '';
                    }

                }

                // if(data.MaterialTypeId == 2){
                //     for(var a in detail){
                //         if(detail[a].newUnitPrice == undefined){
                //             if(detail[a].UnitPriceClean != undefined){   
                //                 // ini dibuat agar data yg disimpan ke table bisa keriting , jadi tidak selalu bulat angkanya
                //                 detail[a].UnitPrice = parseFloat(detail[a].UnitPriceClean);
                //             }
                //         }
                //     }
                // }
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd
                }

                if (mm < 10) {
                    mm = '0' + mm
                }

                today = yyyy + '-' + mm + '-' + dd;
                //data.DocDate = today ;// ddg-12-12-2017

                var vDocDate = $filter('date')(new Date(), 'yyyy-MM-dd HH:MM');
                return $http.post('/api/as/PurchaseOrder', [{
                    PurchaseOrderId: data.PurchaseOrderId || 0,
                    PurchaseOrderNo: (data.PurchaseOrderNo == null ? 0 : data.PurchaseOrderNo),
                    OutletId: (data.OutletId == null ? 0 : data.OutletId),
                    WarehouseId: (data.WarehouseId == null ? 0 : data.WarehouseId),
                    CreatedDate: vDocDate,
                    CreatedUserId: (data.CreatedUserId == null ? 0 : data.CreatedUserId),
                    RequiredDate: (data.DocDate == null ? today : data.DocDate),
                    DocDate: vDocDate, // (data.DocDate==null?today:data.DocDate),
                    MaterialTypeId: (data.MaterialTypeId == null ? 0 : data.MaterialTypeId),
                    POTypeId: (data.POTypeId == null ? 0 : data.POTypeId),
                    RefPOTypeId: (data.RefPOTypeId == null ? 0 : data.RefPOTypeId),
                    RefPONo: (data.RefPONo == null ? 0 : data.RefPONo),
                    VendorId: (data.VendorId == null ? 0 : data.VendorId),
                    VendorOutletId: (data.VendorOutletId == null ? 0 : data.VendorOutletId),
                    VehicleId: (data.VehicleId == null ? 0 : data.VehicleId),
                    CustomerId: (data.CustomerId == null ? 0 : data.CustomerId),
                    BackOrderFollowId: (data.BackOrderFollowId == null ? 0 : data.BackOrderFollowId),
                    PaymentMethodId: (data.PaymentMethodId == null ? 0 : data.PaymentMethodId),
                    TotalAmount: (data.TotalAmount == null ? 0 : data.TotalAmount),
                    PaymentPeriod: (data.PaymentPeriod == null ? 0 : data.PaymentPeriod),
                    Notes: (data.Notes == null ? 0 : data.Notes),
                    PurchaseOrderStatusId: (data.PurchaseOrderStatusId == null ? 3 : data.PurchaseOrderStatusId),
                    VendorSubletId: 0,
                    VendorName: (data.NamaVendor == null ? 0 : data.NamaVendor),
                    Phone: (data.Telephone == null ? 0 : data.Telephone),
                    VendorAddress: (data.Alamat == null ? 0 : data.Alamat),
                    SPLDBit: 9,
                    EstimationNo : data.NoEstimasi,
                    HasilSOQ: (data.HasilSOQ == null ? 0 : data.HasilSOQ),
                    GridDetail: detail,
                    IsNonPKP : data.IsNONPKP,
                    //tire 
                    TireOrderId: (data.TireOrderId == null ? null : data.TireOrderId),
                    NecessaryDate:(data.NecessaryDate == null ? null : $filter('date')(new Date(data.NecessaryDate), 'yyyy-MM-dd')),
                    NecessaryTime:(data.NecessaryTime == null ? null : data.NecessaryTime),
                    PoliceNumber:(data.NoPol == null ? null : data.NoPol),
                    CampaignId: data.CampaignId,
                }]);
            },
            createApprove: function(data, detail) {
                console.log('create=>', data);
                console.log('detail=>', detail);
                
                var sementara = Math.floor((Math.random() * 10000000) + 1);

                function setDate(dt) {
                    var d = "";
                    dt = new Date(dt);
                    d = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
                    return d;
                }

                return $http.post('/api/as/PurchaseOrder', [{
                    PurchaseOrderId: data.PurchaseOrderId || 0,
                    PurchaseOrderNo: (data.PurchaseOrderNo == null ? 0 : data.PurchaseOrderNo),
                    OutletId: (data.OutletId == null ? 0 : data.OutletId),
                    WarehouseId: (data.WarehouseId == null ? 0 : data.WarehouseId),
                    RequiredDate: (data.DocDate == null ? "1901-01-01" : setDate(data.DocDate)),
                    DocDate: (data.DocDate == null ? "1901-01-01" : setDate(data.DocDate)),
                    MaterialTypeId: (data.MaterialTypeId == null ? 0 : data.MaterialTypeId),
                    POTypeId: (data.POTypeId == null ? 0 : data.POTypeId),
                    RefPOTypeId: (data.RefPOTypeId == null ? 0 : data.RefPOTypeId),
                    RefPONo: (data.RefPONo == null ? 0 : data.RefPONo),
                    VendorId: (data.VendorId == null ? 0 : data.VendorId),
                    VendorOutletId: (data.VendorOutletId == null ? 0 : data.VendorOutletId),
                    VehicleId: (data.VehicleId == null ? 0 : data.VehicleId),
                    CustomerId: (data.CustomerId == null ? 0 : data.CustomerId),
                    BackOrderFollowId: (data.BackOrderFollowId == null ? 0 : data.BackOrderFollowId),
                    PaymentMethodId: (data.PaymentMethodId == null ? 0 : data.PaymentMethodId),
                    TotalAmount: (data.TotalAmount == null ? 0 : data.TotalAmount),
                    PaymentPeriod: (data.PaymentPeriod == null ? 0 : data.PaymentPeriod),
                    PurchaseOrderStatusId: (data.PurchaseOrderStatusId == null ? 2 : data.PurchaseOrderStatusId),
                    VendorSubletId: 0,
                    VendorName: (data.NamaVendor == null ? 0 : data.NamaVendor),
                    Phone: (data.Telephone == null ? 0 : data.Telephone),
                    VendorAddress: (data.Alamat == null ? 0 : data.Alamat),
                    SPLDBit: 9,
                    Notes: (data.Notes == null ? 0 : data.Notes),
                    GridDetail: detail
                }]);
            },
            update: function(data, detail, Flag, POStatusOld) {
                console.log('create=>', data);
                console.log('detail=>', detail);
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var yyyy = today.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd
                }

                if (mm < 10) {
                    mm = '0' + mm
                }

                today = yyyy + '-' + mm + '-' + dd;
                //data.DocDate = today ;// ddg-12-12-2017
                if(Flag == undefined || Flag == null){
                    Flag = 0;
                }

                if(data.IsNONPKP == true){
                    data.IsNONPKP = 1;
                }else if(data.IsNONPKP == false){
                    data.IsNONPKP = 0;
                }
                
                for(var a in detail){
                    if(detail[a].ETA == '-'){
                        detail[a].ETA = '';
                    }
                
                    if(detail[a].ETD == '-'){
                        detail[a].ETD = '';
                    }
                
                }
                
                // tidak jadi dipake
                // if(data.MaterialTypeId == 2){
                //     for(var a in detail){
                //         if(detail[a].newUnitPrice == undefined){
                //             if(detail[a].UnitPriceClean != undefined){   
                //                 // ini dibuat agar data yg disimpan ke table bisa keriting , jadi tidak selalu bulat angkanya
                //                 detail[a].UnitPrice = parseFloat(detail[a].UnitPriceClean);
                //             }
                //         }
                //     }
                // }

                return $http.put('/api/as/PurchaseOrder?Flag=' + Flag + '&POStatusOld=' + POStatusOld, [{
                    PurchaseOrderId: data.PurchaseOrderId,
                    PurchaseOrderNo: (data.PurchaseOrderNo == null ? 0 : data.PurchaseOrderNo),
                    OutletId: (data.OutletId == null ? 0 : data.OutletId),
                    WarehouseId: (data.WarehouseId == null ? 0 : data.WarehouseId),
                    RequiredDate: (data.RequiredDate == null ? today : data.RequiredDate),
                    DocDate: (data.DocDate==null?today:data.DocDate),
                    CreatedDate: (data.CreatedDate==null?today:data.CreatedDate),
                    CreatedUserId: (data.CreatedUserId == null ? 0 : data.CreatedUserId),
                    MaterialTypeId: (data.MaterialTypeId == null ? 0 : data.MaterialTypeId),
                    POTypeId: (data.POTypeId == null ? 0 : data.POTypeId),
                    RefPOTypeId: (data.RefPOTypeId == null ? 0 : data.RefPOTypeId),
                    RefPONo: (data.RefPONo == null ? 0 : data.RefPONo),
                    VendorId: (data.VendorId == null ? 0 : data.VendorId),
                    VendorOutletId: (data.VendorOutletId == null ? 0 : data.VendorOutletId),
                    VehicleId: (data.VehicleId == null ? 0 : data.VehicleId),
                    CustomerId: (data.CustomerId == null ? 0 : data.CustomerId),
                    BackOrderFollowId: (data.BackOrderFollowId == null ? 0 : data.BackOrderFollowId),
                    PaymentMethodId: (data.PaymentMethodId == null ? 0 : data.PaymentMethodId),
                    TotalAmount: (data.TotalAmount == null ? 0 : data.TotalAmount),
                    PaymentPeriod: (data.PaymentPeriod == null ? 0 : data.PaymentPeriod),
                    PurchaseOrderStatusId: (data.PurchaseOrderStatusId == null ? 0 : data.PurchaseOrderStatusId),
                    VendorName: (data.NamaVendor == null ? 0 : data.NamaVendor),
                    Phone: (data.Telephone == null ? 0 : data.Telephone),
                    VendorAddress: (data.Alamat == null ? 0 : data.Alamat),
                    SPLDBit: 9,
                    EstimationNo : data.NoEstimasi,
                    StatusTPOS : data.StatusTPOS,
                    Notes: (data.Notes == null ? 0 : data.Notes),
                    GridDetail: detail,
                    IsNonPKP : data.IsNONPKP,
                    //tire
                    TireOrderId: (data.TireOrderId == null ? null : data.TireOrderId),
                    NecessaryDate:(data.NecessaryDate == null ? null : $filter('date')(new Date(data.NecessaryDate), 'yyyy-MM-dd')),
                    NecessaryTime:(data.NecessaryTime == null ? null : data.NecessaryTime),
                    PoliceNumber:(data.NoPol == null ? null : data.NoPol),
                    CampaignId: data.CampaignId,
                }]);
            },
            updateDetail: function(data) {
                console.log('updateDetail=>', data);
                //var now = (new Date).toLocaleFormat("%A, %B %e, %Y");
                return $http.put('/api/as/PurchaseOrder/UpdatePurchaseOrderDetail', data);
            },
            cancelOrDelete: function(data) {
                console.log("delete id==>", data);

                function setDate(dt) {
                    var d = "";
                    dt = new Date(dt);
                    d = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
                    return d;
                }
                return $http.put('/api/as/PurchaseOrder/CancelPurchaseOrder', [{
                    PurchaseOrderId: data.PurchaseOrderId,
                    PurchaseOrderNo: data.PurchaseOrderNo,
                    OutletId: (data.OutletId == null ? 0 : data.OutletId),
                    WarehouseId: (data.WarehouseId == null ? 0 : data.WarehouseId),
                    RequiredDate: (data.RequiredDate == null ? "1901-01-01" : setDate(data.DocDate)),
                    DocDate: (data.DocDate == null ? "1901-01-01" : setDate(data.DocDate)),
                    MaterialTypeId: (data.MaterialTypeId == null ? 0 : data.MaterialTypeId),
                    POTypeId: (data.POTypeId == null ? 0 : data.POTypeId),
                    RefPOTypeId: data.RefPOTypeId,
                    RefPONo: data.RefPONo,
                    VendorId: (data.VendorId == null ? 0 : data.VendorId),
                    VendorOutletId: (data.VendorOutletId == null ? 0 : data.VendorOutletId),
                    VehicleId: (data.VehicleId == null ? 0 : data.VehicleId),
                    CustomerId: (data.CustomerId == null ? 0 : data.CustomerId),
                    BackOrderFollowId: (data.BackOrderFollowId == null ? 0 : data.BackOrderFollowId),
                    PaymentMethodId: (data.PaymentMethodId == null ? 0 : data.PaymentMethodId),
                    TotalAmount: (data.TotalAmount == null ? 0 : data.TotalAmount),
                    PurchaseOrderStatusId: (data.PurchaseOrderStatusIdOld == null ? 6 : 6),
                    PurchaseOrderStatusIdOld: (data.PurchaseOrderStatusIdOld || 3),
                    CancelReasonId: (data.CancelReasonId == null ? 1 : data.CancelReasonId),
                    CancelReasonDesc: (data.CancelReasonDesc == null ? 1 : data.CancelReasonDesc),
                    PaymentPeriod: (data.PaymentPeriod == null ? 0 : data.PaymentPeriod),
                    HasilSOQ: (data.HasilSOQ == null ? 0 : data.HasilSOQ),
                    Notes: (data.Notes == null ? 0 : data.Notes),
                    VendorSubletId: (data.VendorSubletId == null ? 0 : data.VendorSubletId),
                    VendorName: (data.VendorName == null ? 0 : data.VendorName),
                    SPLDBit: (data.SPLDBit == null ? 0 : data.SPLDBit),
                    Phone: (data.Phone == null ? 0 : data.Phone),
                    VendorAddress: (data.VendorAddress == null ? 0 : data.VendorAddress),                                                                                                                        
                    CreatedDate: (data.CreatedDate == null ? 0 : data.CreatedDate),
                    CreatedUserId: (data.CreatedUserId == null ? 0 : data.CreatedUserId),
                }]);
            },

            getDocumentNumber: function(data) {
                console.log("[getDocumentNumber]");
                console.log(data);
                var res = $http.get('/api/as/PurchaseOrderParts/GetDocumentNumber', {
                    params: {
                        UserId: data,
                    }
                });
                //console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },

            getDocumentNumber2: function() {
                console.log("[getDocumentNumber]");
                var res = $http.get('/api/as/PartsUser/GetDocumentNumber', {
                    params: {
                        FormatId: '64',
                    }
                });
                //console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },


            searchReference: function(RefPONo, POTypeId, RefPOTypeId, State, PurchaseOrderId) {
                console.log("[searchReference]");
                var res = $http.get('/api/as/PurchaseOrder/SearchReference', {
                    params: {
                        RefPONo: RefPONo,
                        POTypeId: POTypeId,
                        RefPOTypeId: RefPOTypeId,
                        State: State,
                        PurchaseOrderId: PurchaseOrderId
                    }
                });
                //console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },

            searchReference2: function(RefPONo, POTypeId, RefPOTypeId, State) {
                console.log("[searchReference2]");
                var res = $http.get('/api/as/PurchaseOrder/SearchReference', {
                    params: {
                        RefPONo: '1',
                        POTypeId: 1,
                        RefPOTypeId: 1,
                        State: 1
                    }
                });
                //console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },

            searchReferenceDetail: function(RefPONo, POTypeId, RefPOTypeId, State, MaterialTypeId, VendorId) {
                console.log("[searchReferenceDetail]");
                var res = $http.get('/api/as/PurchaseOrder/SearchReferenceDetail', {
                    params: {
                        RefPONo: RefPONo,
                        POTypeId: POTypeId,
                        RefPOTypeId: RefPOTypeId,
                        State: State,
                        MaterialTypeId: MaterialTypeId,
                        VendorId:(VendorId == null ? 0 : VendorId)
                    }
                });
                //console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            setPurchaseOrderHeader: function(data) {
                PurchaseOrderHeader = data;
            },
            getPurchaseOrderHeader: function() {
                return PurchaseOrderHeader;
            },
            setPurchaseOrderDetail: function(data) {
                PurchaseOrderDetail = data;
            },
            getPurchaseOrderDetail: function() {
                return PurchaseOrderDetail;
            },
            // Detail digunakan saat View dan Edit
            getDetailByMaterialNo: function(data,whid,POTypeId) {
                if(POTypeId == '7'){ //campaign
                    var res = $http.get('/api/as/PartsUser/GetPartsDetailVendor?PartsCode='+data.PartsCode+'&VendorId='+data.VendorId+'&WarehouseId='+data.WarehouseId+'&CampaignId='+data.CampaignId);
                    return res;
                }else{
                    console.log("[getDetailByMaterialNo]");
                    console.log(data);
                    // var res=$http.get('/api/as/StockAdjustment/GetStockAdjustmentDetailFromMaterial', {params: {
                    var res = $http.get('/api/as/PartsUser/GetPartsDetailOutlet', {
                        params: {
                            PartsCode: data,
                            WarehouseId: whid
                        }
                    });
                    //console.log('hasil=>',res);
                    //res.data.Result = null;
                    return res;
                }
            },
            // Detail digunakan saat View dan Edit
            getDetailByMaterialNoVendor: function(data) {
                console.log("[getDetailByMaterialVendor]");
                console.log(data);
                // var res=$http.get('/api/as/StockAdjustment/GetStockAdjustmentDetailFromMaterial', {params: {
                var res = $http.get('/api/as/PartsUser/GetPartsDetailVendor', {
                    params: {
                        PartsCode: data.PartsCode,
                        VendorId: data.VendorId,
                        NoDeadStock: data.NoDeadStock,
                        WarehouseId: data.WarehouseId
                    }
                });
                //console.log('hasil=>',res);
                //res.data.Result = null;
                return res;

            },

            getAppointmentNo: function() {
                console.log("[getAppointmentNo]");
                var res = $http.get('/api/as/PartsUser/GetAppointmentNo');
                return res;
            },
            getAppointmentNoTanpaDP: function() {
                console.log("[getAppointmentNoTanpaDP]");
                var res = $http.get('/api/as/PartsUser/GetAppointmentNoTanpaDP');
                return res;
            }, 
            InsertIntoStockPosition: function(data) {
                console.log("[InsertIntoStockPosition]");               
                var res = $http.get('/api/as/PurchaseOrder/InsertIntoStockPosition', {
                    params: {
                        OutletId: data.OutletId,
                        Partid: data.Partid,
                        WarehouseId: data.WarehouseId
                    }
                });
                return res;
            },

            setETD: function(data) {
                ETDLoc = data;
            },
            getETD: function() {
                return ETDLoc;
            },
            setETA: function(data) {
                ETALoc = data;
            },
            getETA: function() {
                return ETALoc;
            },
            setVendorHeader: function(data) {
                VendorData = data;
            },
            getVendorHeader: function() {
                return VendorData;
            },

            getEstimation: function(data) {
                var res = $http.get('/api/as/AEstimationBPPurchaseOrder/GetDetailParts?EstimationNo='+data);
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },

            updateApprove: function(data) {
                console.log('update=>', data);

                function setDate(dt) {
                    var d = "";
                    dt = new Date(dt);
                    d = dt.getFullYear() + "-" + (dt.getMonth() + 1) + "-" + dt.getDate();
                    return d;
                }
                //var now = (new Date).toLocaleFormat("%A, %B %e, %Y");
                return $http.put('/api/as/PurchaseOrder/CancelPurchaseOrder', [{
                    PurchaseOrderId: data.PurchaseOrderId,
                    PurchaseOrderNo: (data.PurchaseOrderNo == null ? 0 : data.PurchaseOrderNo),
                    OutletId: (data.OutletId == null ? 0 : data.OutletId),
                    WarehouseId: (data.WarehouseId == null ? 0 : data.WarehouseId),
                    RequiredDate: (data.RequiredDate == null ? "1901-01-01" : setDate(data.DocDate)),
                    DocDate: (data.DocDate == null ? "1901-01-01" : setDate(data.DocDate)),
                    MaterialTypeId: (data.MaterialTypeId == null ? 0 : data.MaterialTypeId),
                    POTypeId: (data.POTypeId == null ? 0 : data.POTypeId),
                    RefPOTypeId: (data.RefPOTypeId == null ? 0 : data.RefPOTypeId),
                    RefPONo: (data.RefPONo == null ? 0 : data.RefPONo),
                    VendorId: (data.VendorId == null ? 0 : data.VendorId),
                    VendorOutletId: (data.VendorOutletId == null ? 0 : data.VendorOutletId),
                    VehicleId: (data.VehicleId == null ? 0 : data.VehicleId),
                    CustomerId: (data.CustomerId == null ? 0 : data.CustomerId),
                    BackOrderFollowId: (data.BackOrderFollowId == null ? 0 : data.BackOrderFollowId),
                    PaymentMethodId: (data.PaymentMethodId == null ? 0 : data.PaymentMethodId),
                    TotalAmount: (data.TotalAmount == null ? 0 : data.TotalAmount),
                    PaymentPeriod: (data.PaymentPeriod == null ? 0 : data.PaymentPeriod),
                    HasilSOQ: data.HasilSOQ || false,
                    PurchaseOrderStatusId: (data.PurchaseOrderStatusId == null ? 0 : data.PurchaseOrderStatusId),
                    PurchaseOrderStatusIdOld: (data.PurchaseOrderStatusIdOld),
                    VendorName: (data.NamaVendor == null ? 0 : data.NamaVendor),
                    Phone: (data.Telephone == null ? 0 : data.Telephone),
                    VendorAddress: (data.Alamat == null ? 0 : data.Alamat),
                    CreatedDate: (data.CreatedDate == null ? 0 : data.CreatedDate),
                    CreatedUserId: (data.CreatedUserId == null ? 0 : data.CreatedUserId),
                    SPLDBit: 9,
                }]);
            },
            getCheckBO: function(data) {
                console.log("[getCheckBO] data", data);
                var res = $http.get('/api/as/PurchaseOrder/CheckBO', {
                    params: {
                        PurchaseOrderNo: data,
                    }
                });
                // console.log('hasil=>',res);
                return res;
            },
            getCheckGR: function(data) {
                console.log("[getCheckGR] data", data);
                var res = $http.get('/api/as/PurchaseOrder/CheckGR', {
                    params: {
                        PurchaseOrderNo: data,
                    }
                });
                // console.log('hasil=>',res);
                return res;
            },
            getCheckSource: function(refpotypeid, data) {
                console.log("[getCheckSource] data", data);
                console.log('ihhh datany', refpotypeid)
                var res = $http.post('/api/as/PurchaseOrder/CheckSource/' + refpotypeid, data);
                // console.log('hasil=>',res);
                return res;
            },
            sendNotif: function(data, recepient, Param) {
                console.log("recepient", recepient);
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: "Request Approval untuk Pengajuan PO" + 
                        "<br>" + data.PurchaseOrderNo,
                    RecepientId: recepient,
                    Param: Param
                }]);
              },
              sendNotifACC: function(data, recepient, Param) {
                console.log("recepient", recepient);
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: "Hasil Pengajuan Approval PO Anda Disetujui" + 
                        "<br>" + data.PurchaseOrderNo,
                    RecepientId: recepient,
                    Param: Param
                }]);
              },
              sendNotifReject: function(data, recepient, Param) {
                console.log("recepient", recepient);
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: "Hasil Pengajuan Approval PO Anda Ditolak" + 
                        "<br>" + data.PurchaseOrderNo,
                    RecepientId: recepient,
                    Param: Param
                }]);
              },
            getApprovalCondition: function() {
                var res = $http.get('/api/as/PartsUser/ApprovalPOCondition');
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },

            getMaterialByEstimate:function(noEst){
                // https://dms-sit.toyota.astra.co.id/api/as/AEstimationBPPurchaseOrder/GetDetailPartsExtOPB?EstimationNo=5Y2/ENB/2012-000014
                var res = $http.get('/api/as/AEstimationBPPurchaseOrder/GetDetailPartsExtOPB?EstimationNo='+noEst);
                return res
            },
            
            checkValidationTPOS: function(ponumberId){
                console.log('=====>', ponumberId);
                var res = $http.get('/api/as/PurchaseOrder/CheckStatusTPOS', {
                    params: {
                            PoId : ponumberId,
                        } 
                    }
                );
                
                return res;
            },

            getListCampaignUnknown: function(){
                var res = $http.get('/api/as/PurchaseOrder/ListCampaignUnknown');
                return res;
            },

            getCampaignOrder: function(data) {
                console.log('getCampaignOrder', data)
                var StartDate = $filter('date')(data.StartDate, 'yyyy-MM-dd');
                var EndDate = $filter('date')(data.EndDate, 'yyyy-MM-dd');
                data.CampaignCode = data.CampaignCode == undefined ? '' : data.CampaignCode;
                data.CampaignName = data.CampaignName == undefined ? '' : data.CampaignName;
                data.PartsCode = data.PartsCode == undefined ? '' : data.PartsCode

                var res = $http.get('/api/as/PurchaseOrder/CampaignOrder?CampaignCode='+data.CampaignCode+'&CampaignName='
                                    +data.CampaignName+'&PartsCode='+data.PartsCode+'&StartDate='+StartDate+'&EndDate='+EndDate)
                return res;
            },

            getListParstCampaign: function(CampaignId) {
                var res = $http.get('/api/as/PurchaseOrder/ListParstCampaignDetail?CampaignId='+CampaignId)
                return res;
            },

            getDetailListPartsCampaign: function(PartsCode,VendorId,WarehouseId,CampaignId,isBundle) {
                if(isBundle){
                    var res = $http.get('/api/as/PartsUser/GetPartsDetailVendor?PartsCode=&VendorId='+VendorId+'&WarehouseId='+WarehouseId+'&CampaignId='+CampaignId);
                }else{
                    var res = $http.get('/api/as/PartsUser/GetPartsDetailVendor?PartsCode='+PartsCode+'&VendorId='+VendorId+'&WarehouseId='+WarehouseId+'&CampaignId='+CampaignId);

                }
                return res;
            },

            getDetailCampaingId: function(CampaignId) {
                var res = $http.get('/api/as/PurchaseOrder/ListCampaign?CampaignId='+CampaignId)
                return res;
            },
        }
    });
