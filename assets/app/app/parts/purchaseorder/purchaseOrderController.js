angular.module('app')
    .controller('PurchaseOrderController', function($scope, $http,$q, CurrentUser, PurchaseOrder, ngDialog, bsNotify, $timeout, PartsCurrentUser, LocalService, $filter, PartsGlobal, PrintRpt, bsAlert, OrderLeadTime, uiGridConstants, ASPricingEngine) {
        /*
          Catatan Approval
          1. Approval keluar jika melebihi Total Amount tertentu yang di setting di PGlobal dan PGlobal_Outlet
          2. Approval keluar jika Stocking Policy (PartsClassId4) dari detail terdapat yang Non-Stock

        */
        //----------------------------------
        // Initialization
        //----------------------------------
        var PPNPerc = 0;
        $scope.user = CurrentUser.user();
        console.log("$scope.user", $scope.user)
        console.log("CurrentUser", CurrentUser)
        $scope.UserId = 0;
        $scope.mData = {}; //Model
        $scope.Filter = {};
        $scope.isView = false;
        $scope.isEdit = false;
        $scope.mData.GridDetail = {};
        $scope.mData.Vendor = {};
        $scope.xRole = {
            selected: []
        };
        $scope.ngDialog = ngDialog;
        $scope.formApi = {};
        $scope.captionLabel = {};
        $scope.captionLabel.new = "Buat";
        $scope.mData.selected = [];
        $scope.roundTo = 0;
        $scope.POStatusOld = 0;

        $scope.TAMenabled = false;
        $scope.labelTAM = '';
        // $scope.vcState = "bengkel";
        //$scope.Grid.TotalAmount = [];
		
		$scope.showIcon = false;
		$scope.showCB = true;
        $scope.ApprovalProcessId = 8903;

        $scope.ApproveData = PurchaseOrder.getPurchaseOrderHeader();
        $scope.ApproveDataDetail = PurchaseOrder.getPurchaseOrderDetail();

        $scope.VendorDetail = PurchaseOrder.getVendorHeader();

        $scope.BatalData = PurchaseOrder.getPurchaseOrderHeader();

        $scope.onRegisterApi = function(gridApi) {
            $scope.myGridApi = gridApi;
        };

        $scope.afterRegisterGridApi = function(gridApi) {
            console.log(gridApi);
        };

        $scope.PaymentMethodData = [{ PaymentMethodId: 1, Name: "Tagihan" }, { PaymentMethodId: 2, Name: "Tunai" }];
        $scope.POTypeData = [{ POTypeId: 1, Name: "PO TAM Order 1" }, { POTypeId: 2, Name: "PO TAM Order 2" }, { POTypeId: 3, Name: "PO TAM Order 3" },{ POTypeId: 6, Name: "PO TAM Order T" }, { POTypeId: 4, Name: "PO NON TAM" }, , { POTypeId: 5, Name: "PO SUBLET" },{ POTypeId: 7, Name: "PO TAM Campaign 3 & C" }]; 
        $scope.RefPOTypeData = [{ RefPOTypeId: 1, Name: "SOP - Appointment" }, { RefPOTypeId: 2, Name: "SOP - WO" }, { RefPOTypeId: 3, Name: "SOP - Sales Order" }, { RefPOTypeId: 4, Name: "NON SOP" }];
        //$scope.vcState = 'customer';

        //----------------------------------
        // Purchase Order Gateway Entry Form
        //----------------------------------
        $scope.isOverlayForm = false;

        $scope.POtypes = ['PO TAM Tipe Order 1', 'PO TAM Tipe Order 2', 'PO TAM Tipe Order 3', 'PO NON TAM', 'PO SUBLET'];
        $scope.POrefs = ['SOP - Appointment', 'SOP-WO', 'SOP-Sales Order', 'NON SOP'];
        $scope.mPOtype = $scope.POtypes[0];
        $scope.mPOref = $scope.POrefs[0];

        $scope.disablePOtypes = false;
        $scope.disablePOrefs = false;
        $scope.menuPOtype = '';
        $scope.menuPOref = '';

        $scope.isEditState = false;
        $scope.stateModeView = false;
        $scope.isPoTAM = false;

        $scope.OrderTire = [{Id: 1,Name:"Today"},{Id: 2,Name:"Booking"},{Id: 3,Name:"Konsinyasi"}];
        $scope.minDate = new Date();
        //----------------------------------
        // CRUD state C = 1, R = 2, U = 3, D = 4
        // CRUD state + Form Number = 111
        //----------------------------------
        $scope.crudState = '';
        $scope.State = 0;
        $scope.iniOPB = 0;

        var itIsLink = false;
        var paramNew;
        
        //TIRE
        $scope.hstep = 1;
        $scope.mstep = 30;
        //----------------------------------
        // Start-Up
        //----------------------------------
        // added by sss on 2018-03-14
        var loadedContent = false;
        var enableSelect = function(){
            $timeout(function(){
                $('.ui-grid-pager-panel select').removeAttr('disabled');
            },100);
        }
        //
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;

            console.log("VIEW CONTENT LOADED_________==============");
            $timeout(function() {
                // added by sss on 2018-03-14
                loadedContent = true;
                //
                var vUser = PartsCurrentUser.getCurrentUserId();
                var resData = 0;
                vUser.then(function (res) {
                    console.log("res==>",res);
                    resData = res.data;
                    $scope.UserId = resData;
                    console.log("resData==>",resData);
                });
                console.log("resData==>",resData);
                console.log("vUser==>",vUser);
                console.log("$scope.UserId==>",$scope.UserId);
                //PurchaseOrder.getAppointmentNo();
                PurchaseOrder.getAppointmentNoTanpaDP();
                console.log("dev_parts 6 Desember 2017 ", document.lastModified);

                PartsCurrentUser.getWarehouseIdi().then(function(res) {
                    console.log("getWarehouseIdi====>", res);
                    //get material type and service type
                    $scope.mData.MaterialTypeId = res.data[0].MaterialType;
                    $scope.mData.ServiceTypeId = res.data[0].Servicetype;
                    console.log("MaterialTypeId : ", $scope.mData.MaterialTypeId);
                    console.log("ServiceTypeId : ", $scope.mData.ServiceTypeId);
                });

                PurchaseOrder.formApi = $scope.formApi;
                console.log("Approve = ", $scope.checkRights(4));
                if ($scope.checkRights(4)) {
                    $scope.Filter.PurchaseOrderStatusId = 2;

                    $scope.getData();
                } else {
                    $scope.Filter.POTypeId = null;
                    $scope.Filter.RefPOTypeId = null;
                    $scope.Filter.PurchaseOrderStatusId = null;
                    $scope.Filter.startDate = null;
                    $scope.Filter.endDate = null;
                    $scope.Filter.HasilSOQ = null;
                    $scope.Filter.RefPONo = null;
                    $scope.Filter.LicensePlate = null;
                    $scope.Filter.PurchaseOrderNo = null;
                    $scope.Filter.VendorName = null;
                    $scope.Filter.isAppointment = null;
                    $scope.Filter.PartsCode = null;
                }

                console.log("itIsLink=>",itIsLink);
                if (itIsLink) {
                    console.log("ini isinya",paramNew);
                    if (typeof paramNew.moduleName !== 'undefined' && paramNew.moduleName == "ds") {
                        $scope.onOverlayMode(4, 4);
                        $timeout(function() {
                            $scope.namavendorid = {};
                            $scope.NoDeadStock ={};
                            $scope.namamaterial = {};
                            $scope.namamaterial = paramNew.PartsCode;
                            $scope.namavendorid = paramNew.VendorIdnya;
                            $scope.NoDeadStock = paramNew.NoDeadStock;
                            $scope.PaymentMethodId = paramNew.PaymentMethodId;
                            console.log ("masuk gak",$scope.namavendorid);
                            $scope.Add();
                            $scope.SearchMaterial(paramNew.PartsCode);
                        }, 3000);
                    } else if (typeof paramNew.moduleName !== 'undefined' && paramNew.moduleName == "ltd") {
                        var x;
                        switch (paramNew.TransactionType) {
                            case "Appointment":
                                x = "1";
                                break;
                            case "Reception":
                                x = "2";
                                break;
                            case "Sales Order":
                                x = "3";
                                break;
                        }
                        console.log("$scope.mData",$scope.mData);
                        if ($scope.mData.RoleId == 1125){
                            if(paramNew.Irregular == "OPB"){
                                $scope.onOverlayMode(5, x);
                                console.log("Kesini kah?");
                            }
                            else{
                                $scope.onOverlayMode(4, x);
                                console.log("Atau Kesini kah?");
                            }

                        }
                        else if ($scope.mData.RoleId != 1125){
                          if ($scope.mData.RoleId == 1124) {
                            $scope.onOverlayMode(4, x);
                          }else if(paramNew.Irregular == "Tyre"){
                            $scope.onOverlayMode(6, x);
                        }else {
                            $scope.onOverlayMode(3, x);
                          }

                            console.log("Atau ini kah?");
                          }

                        if (paramNew.Irregular == "OPB"){
                            $scope.iniOPB = 1;
                            $scope.onOverlayMode(5, x);
                        }
                        else{
                            $scope.iniOPB = 0;
                        }
                        // $timeout(function(){
                        // $scope.Add();
                        // $scope.SearchMaterial(paramNew.PartsCode);
                        $scope.mData.RefPONo = angular.copy(paramNew.RefNo);
                        $scope.onSearchRefPONo(paramNew.RefNo);
                        // },3000);
                    }
                    itIsLink = false;
                }
            });


            ASPricingEngine.getPPN(0, 2).then(function (res) {
                PPNPerc = res.data
            });


        });

        $scope.checkRights = function(bit) {
            var p = $scope.myRights & Math.pow(2, bit);
            var res = (p == Math.pow(2, bit));
            console.log("myRights => ", $scope.myRights);
            // alert($scope.myRights);
            return res;
        }

        $scope.getRightX = function(a, b) {
            var i = 0;
            if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
            else if ($scope.checkRights(b)) i = 2;
            else if ($scope.checkRights(a)) i = 1;
            return i;
        }
        $scope.getRightMaterialType = function() {
            console.log("getRightMaterialType**");
            var i = $scope.getRightX(8, 9);
            if (($scope.checkRights(8) && $scope.checkRights(9))) i = 3;
            else if ($scope.checkRights(9)) i = 2;
            else if ($scope.checkRights(8)) i = 1;
            return i;
        }
        $scope.getRightServiceType = function() {
            console.log("getRightServiceType**");
            var i = $scope.getRightX(10, 11);
            return i;
        }

        PurchaseOrder.getAppointmentNo().then(function(res) {
                var AppointmentNo = res.data;
                console.log('AppointmentNo ==>', AppointmentNo);
                $scope.REf2 = AppointmentNo;
                console.log('$scope.REf2 ==>', $scope.REf2);
            },
            function(err) {
                console.log("err=>", err);
            }
        );

        PurchaseOrder.getAppointmentNoTanpaDP().then(function(res) {
                var AppointmentNo = res.data;
                console.log('AppointmentNo ==>', AppointmentNo);
                $scope.REf2 = AppointmentNo;
                console.log('$scope.REf2 ==>', $scope.REf2);
            },
            function(err) {
                console.log("err=>", err);
            }
        );


        $scope.selectPOrefs = function(po) {
            console.log("PO type is " + po);
            if (po === "PO SUBLET") {
                $scope.POrefs = ['SOP-WO'];
            } else {
                $scope.POrefs = ['SOP - Appointment', 'SOP-WO', 'SOP-Sales Order', 'NON SOP'];
            }
            $scope.disablePOtypes = true;
        }

        $scope.onBulkDelete = function(row) {
            console.log("Persiapan hapus ", row);
            console.log("$scope.mData ", $scope.mData);
            var detail = 2;
            PurchaseOrder.searchReference(row.RefPONo, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail).then(function(res) {
                var AppointmentData = res.data[0];
                console.log(AppointmentData);

                var StatusPO = AppointmentData[0].PurchaseOrderStatusId;
                if (StatusPO < 4) {
                    PurchaseOrder.cancelOrDelete(row[0]).then(function(res) {
                        console.log(res);
                        //alert("Purchase Order berhasil dibatalkan");
                        bsNotify.show({
                            title: "Purchase Order ",
                            content: "Data berhasil dihapus",
                            type: 'success'
                        });
                    },
                    function(err) {
                        console.log("err=>", err);
                    });
                } else if (StatusPO = 6) {
                    bsNotify.show({
                        title: "Purchase Order ",
                        content: "Data sudah dibatalkan",
                        type: 'danger'
                    });
                } else if (StatusPO > 4) {
                    bsNotify.show({
                        title: "Purchase Order ",
                        content: "PO sudah ada penerimaan. Batalkan GR terlebih dahulu",
                        type: 'danger'
                    });
                }

            },
            function(err) {
                console.log("err=>", err);
            });

            // PurchaseOrder.cancelOrDelete(row[0]).then(function(res) {
            //         console.log(res);
            //         //alert("Purchase Order berhasil dibatalkan");
            //         bsNotify.show({
            //             title: "Purchase Order ",
            //             content: "Data berhasil dihapus",
            //             type: 'success'
            //         });
            //     },
            //     function(err) {
            //         console.log("err=>", err);
            //     }
            // );
        }

        $scope.onBulkApprovePO = function(row) {
            console.log("Approved => ", row);
            var bulkData = row;
            angular.forEach(bulkData, function(value, key) {
                PartsGlobal.actApproval({
                    ProcessId: 8903,
                    DataId: row[key].PurchaseOrderId,
                    ApprovalStatus: 1
                });

                $scope.mData.selected[key].PurchaseOrderStatusIdOld = $scope.mData.selected[key].PurchaseOrderStatusId;
                $scope.mData.selected[key].PurchaseOrderStatusId = 3;

                if ($scope.user.RoleId==1128 && row[0].WarehouseId==1016) {
                    PurchaseOrder.sendNotifACC(row[0], 1122, 44).then(
                        function(res) {
                          console.log('terkirim ke Partsman GR')
                        });
                }
                else if ($scope.user.RoleId==1129 && row[0].WarehouseId==1017) {
                    PurchaseOrder.sendNotifACC(row[0], 1123, 44).then(
                        function(res) {
                        console.log('terkirim ke Partsman BP')
                        });
                }
                else if ($scope.user.RoleId==1128 && row[0].WarehouseId==1018) {
                    PurchaseOrder.sendNotifACC(row[0], 1125, 44).then(
                        function(res) {
                        console.log('terkirim ke Gudang GR')
                        });
                }
                else if ($scope.user.RoleId==1129 && row[0].WarehouseId==1019) {
                    PurchaseOrder.sendNotifACC(row[0], 1124, 44).then(
                        function(res) {
                        console.log('terkirim ke Gudang BP')
                        });
                }
                PurchaseOrder.updateApprove($scope.mData.selected[key]).then(function(res) {
                        var create = res.data;
                        console.log(res.data);
                        bsNotify.show({
                            title: "Purchase Order ",
                            content: "Data berhasil di-approve",
                            type: 'success'
                        });

                        $scope.getData();
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            });
        }

        $scope.onBulkRejectPO = function(row) {
            console.log("Rejected => ", row);
            var bulkData = row;
            angular.forEach(bulkData, function(value, key) {
                PartsGlobal.actApproval({
                    ProcessId: 8903,
                    DataId: row[key].PurchaseOrderId,
                    ApprovalStatus: 2
                });
                $scope.mData.selected[key].PurchaseOrderStatusIdOld = $scope.mData.selected[key].PurchaseOrderStatusId;
                $scope.mData.selected[key].PurchaseOrderStatusId = 1;
                if ($scope.user.RoleId==1128 && row[0].WarehouseId==1016) {
                    PurchaseOrder.sendNotifReject(row[0], 1122, 44).then(
                        function(res) {
                          console.log('terkirim ke Partsman GR')
                        });
                }
                else if ($scope.user.RoleId==1129 && row[0].WarehouseId==1017) {
                    PurchaseOrder.sendNotifReject(row[0], 1123, 44).then(
                        function(res) {
                        console.log('terkirim ke Partsman BP')
                        });
                }
                else if ($scope.user.RoleId==1128 && row[0].WarehouseId==1018) {
                    PurchaseOrder.sendNotifReject(row[0], 1125, 44).then(
                        function(res) {
                        console.log('terkirim ke Gudang GR')
                        });
                }
                else if ($scope.user.RoleId==1129 && row[0].WarehouseId==1019) {
                    PurchaseOrder.sendNotifReject(row[0], 1124, 44).then(
                        function(res) {
                        console.log('terkirim ke Gudang BP')
                        });
                }

                PurchaseOrder.updateApprove($scope.mData.selected[key]).then(function(res) {
                        var create = res.data;
                        console.log(res.data);
                        bsNotify.show({
                            title: "Purchase Order ",
                            content: "Data berhasil di-tolak",
                            type: 'success'
                        });

                        $scope.getData();
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            });
        }

        // pada saat overlay mode
        $scope.onOverlayMode = function(po, ref) {
            console.log("[$scope] onOverlayMode ", po, ref);
            $scope.non_tam_non_sop_bengkel.columnDefs = [];
            console.log('INI NIHHHH', $scope.non_tam_non_sop_bengkel);
            $scope.showIcon = false;
			$scope.showCB = true;
            $scope.tmpPO = po;
            var xFilter = {};
            //test dlu
            $scope.onDeleteFilter(xFilter);
            // $scope.mData = {};
            // $scope.mData.GridDetail = {};
            //tahan tombol simpan
            $scope.checkRefGINo = true;
            $scope.mData.IsNONPKP = false;
            $scope.isBundle = false;
            console.log("$scope.mData=>",$scope.mData);
            console.log("$scope.mData.GridDetail=>",$scope.mData.GirdDetail);
            
            console.log("$scope.tam_sop_appointment=>",$scope.tam_sop_appointment);
            console.log("$scope.non_tam_sop_wo=>",$scope.non_tam_sop_wo);
            console.log("$scope.tam_sop_wo=>",$scope.tam_sop_wo);
            console.log("$scope.sublet_sop_wo=>",$scope.sublet_sop_wo);
            console.log("$scope.non_tam_sop_so=>",$scope.non_tam_sop_so);
            console.log("$scope.non_tam_non_sop_bengkel=>",$scope.non_tam_non_sop_bengkel);
            console.log("$scope.non_tam_non_sop=>",$scope.non_tam_non_sop);
            var pos = 0;
            if ($scope.mData.POTypeId == '4' && $scope.mData.RefPOTypeId == '4'){
                // $scope.non_tam_sop_so.columnDefs[6].enableCellEdit =true; // #69820 -- remove standard cost edit except sublet

            } else {
                if ($scope.mData.POTypeId == '5' && $scope.mData.RefPOTypeId == '4') {
                    $scope.non_tam_sop_so.columnDefs[6].enableCellEdit = !$scope.stateModeView; //enable field name Standart Cost
                    pos = $scope.non_tam_sop_so.columnDefs.map(function (e) { return e.field; }).indexOf('ETA');
                    console.log("Col ETA=>",pos);
                    $scope.non_tam_sop_so.columnDefs[pos].visible = false;
                    pos = $scope.non_tam_sop_so.columnDefs.map(function (e) { return e.field; }).indexOf('ETD');
                    console.log("Col ETD=>",pos);
                    $scope.non_tam_sop_so.columnDefs[pos].visible = false;
                    pos = $scope.non_tam_sop_so.columnDefs.map(function (e) { return e.field; }).indexOf('StatusTPOS');
                    console.log("Col StatusTPOS=>",pos);
                    $scope.non_tam_sop_so.columnDefs[pos].visible = false;
                    pos = $scope.non_tam_sop_so.columnDefs.map(function (e) { return e.field; }).indexOf('ReasonTPOS');
                    console.log("Col ReasonTPOS=>",pos);
                    $scope.non_tam_sop_so.columnDefs[pos].visible = false;
                } else {
                    $scope.non_tam_sop_so.columnDefs[6].enableCellEdit =false;
                }
            }
            console.log("Sini1=>",$scope.mData.POTypeId)
            console.log("Sini2=>",$scope.mData.RefPOTypeId)

            if($scope.mData.POTypeId == '5' && $scope.mData.RefPOTypeId == '1'){
                $scope.tam_sop_appointment.columnDefs[4].enableCellEdit = !$scope.stateModeView;

            }else{
                $scope.tam_sop_appointment.columnDefs[4].enableCellEdit =false;
            }

            $scope.non_tam_non_sop_bengkel.columnDefs = [];
            console.log('INI NIHHHH', $scope.non_tam_non_sop_bengkel);

            if(($scope.mData.POTypeId == 1 || $scope.mData.POTypeId == 2 || $scope.mData.POTypeId == 3 || $scope.mData.POTypeId == 7) && $scope.mData.RefPOTypeId == 4) {
                console.log("Kesini $scope.columnTAM");
                if($scope.mData.POTypeId == 7){
                    $scope.non_tam_non_sop_bengkel.columnDefs = $scope.columnTAMCampaign;
                    $scope.non_tam_non_sop_bengkel.columnDefs[6].enableCellEdit = $scope.isBundle == true ? false : true;
                }else{
                    $scope.non_tam_non_sop_bengkel.columnDefs = $scope.columnTAM;
                }

            } else if($scope.mData.RefPOTypeId == 4) {
                console.log("Kesini $scope.columnNonTAM");
                $scope.non_tam_non_sop_bengkel.columnDefs = $scope.columnNonTAM;
            }
            console.log('INI NIHHHH X', $scope.non_tam_non_sop_bengkel);

            if ((typeof po === undefined || po == null) || (typeof ref === undefined || ref == null)) {
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Tipe dan Referensi tidak boleh kosong",
                    type: 'danger'
                });
            } else {
                PartsCurrentUser.getVendor().then(function(res) {
                    console.log("List Vendor====>", res);
                    // $scope.vendorData = res.data;
                    $scope.vendorData = [];
                    if($scope.mData.POTypeId == '5' || $scope.mData.POTypeId == '4'){
                        for(var i in res.data){
                            if(res.data[i].SPLDBit == false){
                                $scope.vendorData.push(res.data[i]);
                            }

                        }
                    }else{
                        $scope.vendorData = res.data
                    }
                    // var tmpIndex = _.findIndex($scope.vendorData,{Name:"OPB"})
                    // if(tmpIndex == -1){
                    //   $scope.vendorData.push({
                    //         VendorId: 99999999,
                    //         Name:"OPB"
                    //   });
                    // }
                    console.log("$scope.vendorData ", $scope.vendorData);
                    console.log("SPLDA ======> ", x);
                    if (((po == 1) || (po == '1') || (po == 2) || (po == '2') || (po == 3) || (po == '3')
                        || (po == 6) || (po == '6') || (po == 7) || (po == '7'))) {
                        var x = $filter('filter')($scope.vendorData, { SPLDBit: true }, true);
                        console.log("SPLD ======> ", x);
                        $scope.mData.VendorId = x[0].VendorId;
                        $scope.mData.VendorName = x[0].Name;
                        $scope.mData.PaymentMethodId = x[0].PaymentMethodId;
                        $scope.mData.PaymentMethodId = ((po == 6) || (po == '6')) ? 1 : x[0].PaymentMethodId;
                        if (x.length > 0) {
                            $scope.mData.TermOfPayment = (_.isNull(x[0].TermOfPayment) || _.isUndefined(x[0].TermOfPayment) ? 0 : x[0].TermOfPayment);
                        } else {
                            $scope.mData.TermOfPayment = 0;
                        }
                        $scope.mData.PaymentPeriod = angular.copy($scope.mData.TermOfPayment);
                        $scope.mData.Telephone = x[0].Phone;
                        $scope.mData.Alamat = x[0].Address;

                    } else if ((po == 4) || (po == '4')) {
                        var x = $filter('filter')($scope.vendorData, { SPLDBit: true }, true);
                        console.log("SPLD ======> ", x);
                        // $scope.mData.VendorId = x[0].VendorId;
                        // $scope.mData.PaymentMethodId = x[0].PaymentMethodId;
                        if (x.length >0 ) {
                            $scope.mData.TermOfPayment = (_.isNull(x[0].TermOfPayment) || _.isUndefined(x[0].TermOfPayment) ? 0 : x[0].TermOfPayment);
                        } else {
                            $scope.mData.TermOfPayment = 0;
                        }
                        $scope.mData.PaymentPeriod = angular.copy($scope.mData.TermOfPayment);
                    }else if ((po == 5) || (po == '5')) {
                        var x = $filter('filter')($scope.vendorData, { VendorCode: 'TAM' }, true);
                        var y = $filter('filter')($scope.vendorData, { SPLDBit: true }, true);
                        // $scope.mData.VendorId = 99999999;
                        $scope.mData.PaymentMethodId = 2;
                        $scope.mData.TermOfPayment = 0;
                        $scope.mData.PaymentPeriod = angular.copy($scope.mData.TermOfPayment);
                    }
                });

                PartsCurrentUser.getVendorOutlet().then(function(res) {
                    console.log("List VendorOutlet====>", res);
                    $scope.vendorOutletData = res.data;
                });

                //get material type and service type
                $scope.mData.MaterialTypeId = $scope.getRightMaterialType();
                $scope.mData.ServiceTypeId = $scope.getRightServiceType();
                console.log("MaterialTypeId : ", $scope.mData.MaterialTypeId);
                console.log("ServiceTypeId : ", $scope.mData.ServiceTypeId);
                switch ($scope.user.RoleId) {
                    case 1135:
                        $scope.mData.MaterialTypeId = 1;
                        $scope.mData.ServiceTypeId = 1;
                        break;
                    case 1122:
                        $scope.mData.MaterialTypeId = 1;
                        $scope.mData.ServiceTypeId = 1;
                        break;
                    case 1123:
                        $scope.mData.MaterialTypeId = 1;
                        $scope.mData.ServiceTypeId = 0;
                        break;
                    case 1124:
                        $scope.mData.MaterialTypeId = 2;
                        $scope.mData.ServiceTypeId = 0;
                        break;
                    case 1125:
                        $scope.mData.MaterialTypeId = 2;
                        $scope.mData.ServiceTypeId = 1;
                        break;
                }
                if ($scope.mData.MaterialTypeId == 3 || $scope.mData.ServiceTypeId == 3) {
                    //if ($scope.mData.MaterialTypeId == 0 || $scope.mData.ServiceTypeId == 0 || $scope.mData.MaterialTypeId == 3 || $scope.mData.ServiceTypeId == 3) {
                    alert('Material Type atau Service Type salah. Silakan hubungi administrator');
                    $scope.goBack();
                }
                //Rounding untuk Bahan
                if ($scope.mData.MaterialTypeId == 1) {
                    $scope.roundTo = 0;
                 } else { $scope.roundTo = 3 }
                 console.log("$scope.roundTo ====> ", $scope.roundTo);

                PartsCurrentUser.getWarehouseId($scope.mData.ServiceTypeId, $scope.mData.MaterialTypeId, $scope.user.OrgId).then(function(res) {
                        var gridData = res.data;
                        console.log(gridData[0]);
                        console.log("Iniservise", $scope.mData.ServiceTypeId);
                        console.log("Iniservise1", $scope.mData.MaterialTypeId);
                        console.log("Iniservise2", $scope.user.OrgId);

                        $scope.mData.WarehouseId = gridData[0].WarehouseId;
                        $scope.mData.OutletId = gridData[0].OutletId;

                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

                //$scope.GenerateDocNum();

                console.log(po + '  ' + ref);
                $scope.isOverlayForm = false;
                var newnum = '1';
                $scope.disablePOrefs = true;

                // POTypeId dan RefPOTypeId selama dalam frontend menggunakan nama String
                $scope.mData.POTypeId = po;
                $scope.mData.RefPOTypeId = ref;

                console.log("$scope.user.RoleId=>",$scope.user.RoleId);
                console.log("$scope.mData.POTypeId=>",$scope.mData.POTypeId);
                console.log("$scope.mData.RefPOTypeId=>",$scope.mData.RefPOTypeId);

                $scope.menuPOtype = po;
                $scope.menuPOref = ref;

                // if ((po == 3) || (po == '3')) {
                //     $scope.BoTam = [{ Id: 9, Name: "Fill" }];
                //     $scope.mData.BackOrderFollowId = 9;
                //     $scope.botamstatus = false;
                //     $scope.vendorstatus = true;
                //     $scope.pymmtdstatus = true;
                //     $scope.pymperiodstatus = true;
                // } else {
                //     $scope.BoTam = [{ Id: 1, Name: "Kill" }, { Id: 9, Name: "Fill" }];
                //     $scope.botamstatus = false;
                //     $scope.vendorstatus = false;
                //     $scope.pymmtdstatus = true;
                //     $scope.pymperiodstatus = true;
                // }

                // if (((po == 1) || (po == '1') || (po == 2) || (po == '2') || (po == 3) || (po == '3'))) {
                //     $scope.TAMenabled = true;
                //     $scope.labelTAM = 'Simpan dan Order ke TAM';
                //     $scope.isTAM = true;


                // } else {
                //     $scope.labelTAM = 'Simpan';
                //     $scope.isTAM = false;
                //     $scope.mData.BackOrderFollowId = 0;
                // }

                if ((po == 3) || (po == '3')) {
                    $scope.BoTam = [{ Id: 9, Name: "Fill" }];
                    $scope.mData.BackOrderFollowId = 9;
                    $scope.botamstatus = false;
                    $scope.vendorstatus = true;
                    $scope.pymmtdstatus = true;
                    $scope.pymperiodstatus = true;
                    $scope.PaymentMethodData = [{ PaymentMethodId: 1, Name: "Tagihan" }];

                    $scope.TAMenabled = true;
                    $scope.labelTAM = 'Simpan dan Order ke TAM';
                    $scope.isTAM = true;

                } else if ((po == 1) || (po == '1') || (po == 2) || (po == '2')) {
                    //harusnya kill 1 bukan 0
                    $scope.BoTam = [{ Id: 1, Name: "Kill" }, { Id: 9, Name: "Fill" }];
                   // $scope.mData.BackOrderFollowId = 0;
                    $scope.botamstatus = false;
                    //$scope.vendorstatus = false;
					$scope.vendorstatus = true;
                    $scope.pymmtdstatus = true;
                    $scope.pymperiodstatus = true;
                    $scope.PaymentMethodData = [{ PaymentMethodId: 1, Name: "Tagihan" }];

                    $scope.TAMenabled = true;
                    $scope.labelTAM = 'Simpan dan Order ke TAM';
                    $scope.isTAM = true;

                } else if ((po == 4) || (po == '4')) { // PO NON TAM & PO SUBLET
                    //harusnya kill 1 bukan 0
                    $scope.BoTam = [{ Id: 1, Name: "Kill" }, { Id: 9, Name: "Fill" }];
                    $scope.botamstatus = true;
                    $scope.vendorstatus = false;
                    $scope.pymmtdstatus = true;
                    $scope.pymperiodstatus = true;

                    $scope.labelTAM = 'Simpan';
                    $scope.isTAM = false;
                } else if ((po == 5) || (po == '5')) {
                    //harusnya kill 1 bukan 0
                    $scope.BoTam = [{ Id: 1, Name: "Kill" }, { Id: 9, Name: "Fill" }];
                    $scope.mData.BackOrderFollowId = 1;
                    $scope.botamstatus = false;
                    $scope.vendorstatus = false;
                    $scope.pymmtdstatus = true;
                    $scope.pymperiodstatus = true;

                    $scope.labelTAM = 'Simpan';
                    $scope.isTAM = false;
                } else if ((po == 6) || (po == '6') || (po == 7) || (po == '7')) {
                    //harusnya kill 1 bukan 0
                    $scope.BoTam = [{ Id: 1, Name: "Kill" }];
                    $scope.mData.BackOrderFollowId = 1;
                    if((po == 7) || (po == '7')){
                        $scope.BoTam = [{ Id: 9, Name: "Fill" }];
                        $scope.mData.BackOrderFollowId = 9;
                    }
                   // $scope.mData.BackOrderFollowId = 0;
                    $scope.botamstatus = false;
                    //$scope.vendorstatus = false;
					$scope.vendorstatus = true;
                    $scope.pymmtdstatus = true;
                    $scope.pymperiodstatus = true;
                    $scope.PaymentMethodData = [{ PaymentMethodId: 1, Name: "Tagihan" }];

                    $scope.TAMenabled = true;
                    $scope.labelTAM = 'Simpan dan Order ke TAM';
                    $scope.isTAM = true;
                }


                $scope.crudState = newnum + $scope.formShowHandler(parseInt(po), parseInt(ref));

                //pemisahan non tam non sop vendor bengkel dan bahan
                if ($scope.crudState == '144') {
                    if ($scope.mData.MaterialTypeId == 1) {
                        $scope.crudState = '1441';
                    } else if ($scope.mData.MaterialTypeId == 2) {
                        $scope.crudState = '1442';
                    }
                }

                // $scope.mData.BackOrderFollowId = $scope.BoTam[0];

                console.log('onOverlayMode | POTypeId ======>',po)
                console.log('onOverlayMode | RefPOTypeId ===>',ref)
                console.log('onOverlayMode | newnum ========>',newnum)
                console.log('onOverlayMode | formState =====>',$scope.formShowHandler(parseInt(po), parseInt(ref)));
                console.log('onOverlayMode | crudState =====>',$scope.crudState)

                console.log("[OVERLAY] " + $scope.crudState);
            }
            $scope.mData.qtyGenerate = null; //campaign
            $scope.ModalTambahListCampaign_UIGrid.data = [];
        }

        // Cetakan
        $scope.cetakOPMbulk = function(param) {
            console.log('PurchaseOrderId =', param);
            //console.log('OutletId =', $scope.user.OrgId);
            //var data = $scope.mData;
            $scope.printOPMBulk = 'as/PrintOrderPembelianMaterial/' + param[0].PurchaseOrderId;
            $scope.cetakan($scope.printOPMBulk);
        };

        $scope.cetakOPM = function(PurchaseOrderId, OutletId) {
            console.log('PurchaseOrderId =', PurchaseOrderId);
            console.log('OutletId =', OutletId);
            //console.log('OutletId =', $scope.user.OrgId);
            //var data = $scope.mData;
            $scope.printOPM = 'as/PrintOrderPembelianMaterial/' + PurchaseOrderId + '/' + $scope.user.OrgId;
            $scope.cetakan($scope.printOPM);
        };

        $scope.cetakan = function(data) {
            var pdfFile = null;

            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    }
                    else {
                        printJS(pdfFile);
                    }
                }
                else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };

        //button kembali pada overlay
        $scope.goBack = function() {
            $scope.checkRefGINo = false;
            $scope.isOverlayForm = false;
            PurchaseOrder.formApi.setMode("grid");
            $scope.resetForm();
            $scope.getData();
        }

        $scope.selectVendor = function(item) {
            console.log("bsselect vendor ", item);
            $scope.mData.VendorOutletId = item.VendorOutletId;
            $scope.mData.Telephone = item.Phone;
            $scope.mData.Alamat = item.Address;
            $scope.mData.NamaVendor = item.Name;
            console.log("selectVendor ", $scope.mData);
            if (item.TermOfPayment == null) {
                $scope.mData.PaymentPeriod = 0;

            } else {
                $scope.mData.PaymentPeriod = item.TermOfPayment;
            }
            $scope.mData.PaymentMethodId = item.PaymentMethodId;

			console.log('$scope.sublet_sop_wo.data at recalcGrid stv : ', $scope.sublet_sop_wo.data);
			console.log('$scope.non_tam_sop_so.data at recalcGrid stv : ', $scope.non_tam_sop_so.data);
			console.log('$scope.non_tam_sop_so_ubah.data at recalcGrid stv : ', $scope.non_tam_sop_so_ubah.data);
			console.log('$scope.non_tam_non_sop.data at recalcGrid stv : ', $scope.non_tam_non_sop.data);
			// CR5 #10
			console.log('item stv : ', item);
			console.log('$scope.crudState stv : ', $scope.crudState);
			if(item.IsNonPKP == 0){
				//PKP
				$scope.mData.IsNONPKP = false;

				$scope.showCB = true;
				$scope.showIcon = false;
			}else if(item.IsNonPKP == 1){
				//Non PKP
				$scope.mData.IsNONPKP = true;
				$scope.showCB = false;
				$scope.showIcon = true;
			}

			if ($scope.mData.POTypeId == "4" && $scope.mData.RefPOTypeId == "4") {
				if ($scope.crudState === '142') {
					for (var sv in $scope.non_tam_sop_so.data) {
						if($scope.non_tam_sop_so.data[sv].QtyPO){
							//$scope.non_tam_sop_so.data[sv].DiscountAmount = Math.round((($scope.non_tam_sop_so.data[sv].DiscountPercent / 100)) * ($scope.non_tam_sop_so.data[sv].QtyPO * $scope.non_tam_sop_so.data[sv].UnitPrice));
							// CR5 #10
							console.log('$scope.mData.IsNONPKP at non_tam_sop_so stv: ', $scope.mData.IsNONPKP);
							if($scope.mData.IsNONPKP == false){
								//PKP

								// $scope.non_tam_sop_so.data[sv].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv].StandardCost / 1.1);
								$scope.non_tam_sop_so.data[sv].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv].StandardCost / (1+(PPNPerc/100)));

								// $scope.non_tam_sop_so.data[sv].DiscountAmount = Math.round((($scope.non_tam_sop_so.data[sv].DiscountPercent / 100)) * ($scope.non_tam_sop_so.data[sv].QtyPO * $scope.non_tam_sop_so.data[sv].UnitPrice));
								$scope.non_tam_sop_so.data[sv].DiscountAmount = (Math.round($scope.non_tam_sop_so.data[sv].UnitPrice*$scope.non_tam_sop_so.data[sv].DiscountPercent / 100))*$scope.non_tam_sop_so.data[sv].QtyPO;

                                // $scope.non_tam_sop_so.data[sv].VATAmount = (Math.round(($scope.non_tam_sop_so.data[sv].UnitPrice - $scope.non_tam_sop_so.data[sv].DiscountAmount)*0.1))*$scope.non_tam_sop_so.data[sv].QtyPO;
                                $scope.non_tam_sop_so.data[sv].VATAmount = (Math.round(($scope.non_tam_sop_so.data[sv].UnitPrice - $scope.non_tam_sop_so.data[sv].DiscountAmount)*(PPNPerc/100)))*$scope.non_tam_sop_so.data[sv].QtyPO;

                                // $scope.non_tam_sop_so.data[sv].VATAmount = Math.round((($scope.non_tam_sop_so.data[sv].QtyPO * $scope.non_tam_sop_so.data[sv].UnitPrice) - ($scope.non_tam_sop_so.data[sv].DiscountAmount)) * 0.1);
							}else if($scope.mData.IsNONPKP == true){
								//Non PKP
								$scope.non_tam_sop_so.data[sv].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv].StandardCost);
                                $scope.non_tam_sop_so.data[sv].DiscountAmount = (Math.round($scope.non_tam_sop_so.data[sv].UnitPrice*$scope.non_tam_sop_so.data[sv].DiscountPercent / 100))*$scope.non_tam_sop_so.data[sv].QtyPO;
								// $scope.non_tam_sop_so.data[sv].DiscountAmount = Math.round((($scope.non_tam_sop_so.data[sv].DiscountPercent / 100)) * ($scope.non_tam_sop_so.data[sv].QtyPO * $scope.non_tam_sop_so.data[sv].UnitPrice));
								$scope.non_tam_sop_so.data[sv].VATAmount = 0;
							}
							// CR5 #10
							/*if($scope.mData.NoEstimasi == null || $scope.mData.NoEstimasi == undefined || $scope.mData.NoEstimasi == ''){
								$scope.non_tam_sop_so.data[sv].TotalAmount = ($scope.non_tam_sop_so.data[sv].QtyPO * $scope.non_tam_sop_so.data[sv].UnitPrice) - $scope.non_tam_sop_so.data[sv].DiscountAmount + $scope.non_tam_sop_so.data[sv].VATAmount;
							}else{
								if($scope.mData.IsNONPKP == false){
									//
								}else if($scope.mData.IsNONPKP == true){
									$scope.non_tam_sop_so.data[sv].UnitPrice = $scope.non_tam_sop_so.data[sv].UnitPrice / 1.1;
								}
								$scope.non_tam_sop_so.data[sv].TotalAmount = ($scope.non_tam_sop_so.data[sv].QtyPO * $scope.non_tam_sop_so.data[sv].UnitPrice) - $scope.non_tam_sop_so.data[sv].DiscountAmount;
							}*/
							$scope.non_tam_sop_so.data[sv].TotalAmount = ($scope.non_tam_sop_so.data[sv].QtyPO * $scope.non_tam_sop_so.data[sv].UnitPrice) - $scope.non_tam_sop_so.data[sv].DiscountAmount + $scope.non_tam_sop_so.data[sv].VATAmount;
							//$scope.non_tam_sop_so.data[sv].TotalAmount = ($scope.non_tam_sop_so.data[sv].QtyPO * $scope.non_tam_sop_so.data[sv].UnitPrice) - $scope.non_tam_sop_so.data[sv].DiscountAmount;
						}
					}
					var svTotalAmount = 0;
					for (var si = 0; si < $scope.non_tam_sop_so.data.length; si++) {
						svTotalAmount = svTotalAmount + $scope.non_tam_sop_so.data[si].TotalAmount;
						$scope.mData.TotalAmount = svTotalAmount;
					}
				}else if (($scope.crudState === '114') || ($scope.crudState === '124') || ($scope.crudState === '134')) {
					for (var sv0 in $scope.non_tam_non_sop_bengkel.data) {
						if($scope.non_tam_non_sop_bengkel.data[sv0].QtyPO){
							// CR5 #10
							console.log('$scope.mData.IsNONPKP at non_tam_non_sop_bengkel ($scope.crudState === 114) || ($scope.crudState === 124) || ($scope.crudState === 134) stv: ', $scope.mData.IsNONPKP);
							if($scope.mData.IsNONPKP == false){
								//PPn

								// $scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice =Math.round($scope.non_tam_non_sop_bengkel.data[sv0].StandardCost/1.1);
								$scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice =Math.round($scope.non_tam_non_sop_bengkel.data[sv0].StandardCost/(1+(PPNPerc/100)));

								// $scope.non_tam_non_sop_bengkel.data[sv0].VATAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[sv0].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice) - ($scope.non_tam_non_sop_bengkel.data[sv0].DiscountAmount)) * 0.1);
								
                                // $scope.non_tam_non_sop_bengkel.data[sv0].VATAmount = (Math.round(($scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice - $scope.non_tam_non_sop_bengkel.data[sv0].DiscountAmount)*0.1))*$scope.non_tam_non_sop_bengkel.data[sv0].QtyPO;
                                $scope.non_tam_non_sop_bengkel.data[sv0].VATAmount = (Math.round(($scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice - $scope.non_tam_non_sop_bengkel.data[sv0].DiscountAmount)*(PPNPerc/100)))*$scope.non_tam_non_sop_bengkel.data[sv0].QtyPO;

                                // $scope.non_tam_non_sop_bengkel.data[sv0].DiscountAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[sv0].DiscountPercent / 100)) * ($scope.non_tam_non_sop_bengkel.data[sv0].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice));
                                $scope.non_tam_non_sop_bengkel.data[sv0].DiscountAmount = (Math.round($scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice*$scope.non_tam_non_sop_bengkel.data[sv0].DiscountPercent / 100))*$scope.non_tam_non_sop_bengkel.data[sv0].QtyPO;
                            }else if($scope.mData.IsNONPKP == true){
								//Non PPn
								$scope.non_tam_non_sop_bengkel.data[sv0].VATAmount = Math.round(0);
								$scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv0].StandardCost);
								// $scope.non_tam_non_sop_bengkel.data[sv0].DiscountAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[sv0].DiscountPercent / 100)) * ($scope.non_tam_non_sop_bengkel.data[sv0].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice));
                                $scope.non_tam_non_sop_bengkel.data[sv0].DiscountAmount = (Math.round($scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice*$scope.non_tam_non_sop_bengkel.data[sv0].DiscountPercent / 100))*$scope.non_tam_non_sop_bengkel.data[sv0].QtyPO;
                            }
							// CR5 #10
							
							//$scope.non_tam_non_sop_bengkel.data[sv0].DiscountAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[sv0].DiscountPercent / 100)) * ($scope.non_tam_non_sop_bengkel.data[sv0].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice));
							/*if($scope.mData.NoEstimasi == null || $scope.mData.NoEstimasi == undefined || $scope.mData.NoEstimasi == ''){
								$scope.non_tam_non_sop_bengkel.data[sv0].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[sv0].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv0].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[sv0].VATAmount;
							}else{
								if($scope.mData.IsNONPKP == false){
									//
								}else if($scope.mData.IsNONPKP == true){
									$scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice = $scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice / 1.1;
								}
								$scope.non_tam_non_sop_bengkel.data[sv0].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[sv0].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv0].DiscountAmount;
							}*/
							$scope.non_tam_non_sop_bengkel.data[sv0].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[sv0].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv0].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[sv0].VATAmount;
							//$scope.non_tam_non_sop_bengkel.data[sv0].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[sv0].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv0].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv0].DiscountAmount;
						}
					}
					var svTotalAmount0 = 0;
					for (var si0 = 0; si0 < $scope.non_tam_non_sop_bengkel.data.length; si0++) {
						svTotalAmount0 = svTotalAmount0 + $scope.non_tam_non_sop_bengkel.data[si0].TotalAmount;
						$scope.mData.TotalAmount = svTotalAmount0;
					}
				}else if ($scope.crudState === '1441') {
					for (var sv1 in $scope.non_tam_sop_so.data) {
						if($scope.non_tam_sop_so.data[sv1].QtyPO){
							//var HargaSatuanSV = $scope.non_tam_sop_so.data[sv1].StandardCost / (1.1);
							//var DiscountSatuanSV = Math.round((($scope.non_tam_sop_so.data[sv1].DiscountPercent / 100)) * HargaSatuanSV);
							//$scope.non_tam_sop_so.data[sv1].DiscountAmount = $scope.non_tam_sop_so.data[sv1].QtyPO * DiscountSatuanSV;
							// CR5 #10
							console.log('$scope.mData.IsNONPKP at non_tam_sop_so stv: ', $scope.mData.IsNONPKP);
							if($scope.mData.IsNONPKP == false){
								//PKP

								// var HargaSatuanSV = $scope.non_tam_sop_so.data[sv1].StandardCost / (1.1);
								var HargaSatuanSV = $scope.non_tam_sop_so.data[sv1].StandardCost / (1+(PPNPerc/100));

								var DiscountSatuanSV = Math.round((($scope.non_tam_sop_so.data[sv1].DiscountPercent / 100)) * HargaSatuanSV);
								// $scope.non_tam_sop_so.data[sv1].DiscountAmount = $scope.non_tam_sop_so.data[sv1].QtyPO * DiscountSatuanSV;

								// $scope.non_tam_sop_so.data[sv1].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv1].StandardCost / 1.1);
								$scope.non_tam_sop_so.data[sv1].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv1].StandardCost / (1+(PPNPerc/100)));


                                $scope.non_tam_sop_so.data[sv1].DiscountAmount = (Math.round($scope.non_tam_sop_so.data[sv1].UnitPrice*$scope.non_tam_sop_so.data[sv1].DiscountPercent / 100))*$scope.non_tam_sop_so.data[sv1].QtyPO;
                                // $scope.non_tam_sop_so.data[sv1].VATAmount = Math.round($scope.non_tam_sop_so.data[sv1].QtyPO * Math.round((HargaSatuanSV - DiscountSatuanSV) * 0.1));

								// $scope.non_tam_sop_so.data[sv1].VATAmount = (Math.round(($scope.non_tam_sop_so.data[sv1].UnitPrice -  $scope.non_tam_sop_so.data[sv1].DiscountAmount)*0.1))* $scope.non_tam_sop_so.data[sv1].QtyPO;
								$scope.non_tam_sop_so.data[sv1].VATAmount = (Math.round(($scope.non_tam_sop_so.data[sv1].UnitPrice -  $scope.non_tam_sop_so.data[sv1].DiscountAmount)*(PPNPerc/100)))* $scope.non_tam_sop_so.data[sv1].QtyPO;

							}else if($scope.mData.IsNONPKP == true){
								//Non PKP
								var HargaSatuanSV = $scope.non_tam_sop_so.data[sv1].StandardCost;
								var DiscountSatuanSV = Math.round((($scope.non_tam_sop_so.data[sv1].DiscountPercent / 100)) * HargaSatuanSV);
								// $scope.non_tam_sop_so.data[sv1].DiscountAmount = $scope.non_tam_sop_so.data[sv1].QtyPO * DiscountSatuanSV;
								$scope.non_tam_sop_so.data[sv1].VATAmount = 0;
								$scope.non_tam_sop_so.data[sv1].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv1].StandardCost);
                                $scope.non_tam_sop_so.data[sv1].DiscountAmount = (Math.round($scope.non_tam_sop_so.data[sv1].UnitPrice*$scope.non_tam_sop_so.data[sv1].DiscountPercent / 100))*$scope.non_tam_sop_so.data[sv1].QtyPO;
							}
							// CR5 #10
							/*if($scope.mData.NoEstimasi == null || $scope.mData.NoEstimasi == undefined || $scope.mData.NoEstimasi == ''){
								$scope.non_tam_sop_so.data[sv1].TotalAmount = ($scope.non_tam_sop_so.data[sv1].QtyPO * $scope.non_tam_sop_so.data[sv1].UnitPrice) - $scope.non_tam_sop_so.data[sv1].DiscountAmount + $scope.non_tam_sop_so.data[sv1].VATAmount;
							}else{
								if($scope.mData.IsNONPKP == false){
									//
								}else if($scope.mData.IsNONPKP == true){
									$scope.non_tam_sop_so.data[sv1].UnitPrice = $scope.non_tam_sop_so.data[sv1].UnitPrice / 1.1;
								}
								$scope.non_tam_sop_so.data[sv1].TotalAmount = ($scope.non_tam_sop_so.data[sv1].QtyPO * $scope.non_tam_sop_so.data[sv1].UnitPrice) - $scope.non_tam_sop_so.data[sv1].DiscountAmount;
							}*/
							$scope.non_tam_sop_so.data[sv1].TotalAmount = ($scope.non_tam_sop_so.data[sv1].QtyPO * $scope.non_tam_sop_so.data[sv1].UnitPrice) - $scope.non_tam_sop_so.data[sv1].DiscountAmount + $scope.non_tam_sop_so.data[sv1].VATAmount;
							//$scope.non_tam_sop_so.data[sv1].TotalAmount = ($scope.non_tam_sop_so.data[sv1].QtyPO * $scope.non_tam_sop_so.data[sv1].UnitPrice) - $scope.non_tam_sop_so.data[sv1].DiscountAmount;
						}
					}
					var svTotalAmount1 = 0;
					for (var si1 = 0; si1 < $scope.non_tam_sop_so.data.length; si1++) {
						svTotalAmount1 = svTotalAmount1 + $scope.non_tam_sop_so.data[si1].TotalAmount;
						$scope.mData.TotalAmount = svTotalAmount1;
					}
				}else if ($scope.crudState === '1442') {
					for (var sv2 in $scope.non_tam_sop_so.data) {
						if($scope.non_tam_sop_so.data[sv2].QtyPO){
							//var HargaSatuanSV0 = $scope.non_tam_sop_so.data[sv2].StandardCost / (1.1);
							//var DiscountSatuanSV0 = ($scope.non_tam_sop_so.data[sv2].DiscountPercent / 100) * HargaSatuanSV0;
							//$scope.non_tam_sop_so.data[sv2].DiscountAmount = $scope.non_tam_sop_so.data[sv2].QtyPO * DiscountSatuanSV0.toFixed($scope.roundTo);
							//var VATSatuanSV0 = (HargaSatuanSV0 - DiscountSatuanSV0) * 0.1;
							// CR5 #10
							console.log('$scope.mData.IsNONPKP at non_tam_sop_so stv: ', $scope.mData.IsNONPKP);
							console.log("masuk ke 1442 atas");
                            if($scope.mData.IsNONPKP == false){
                                //PKP
                                if($scope.non_tam_sop_so.data[sv2].newUnitPrice == undefined){
        
                                    // var HargaSatuan    = $scope.non_tam_sop_so.data[sv2].StandardCost/1.1;
                                    var HargaSatuan    = $scope.non_tam_sop_so.data[sv2].StandardCost/(1+(PPNPerc/100));

                                    // $scope.non_tam_sop_so.data[sv2].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv2].StandardCost/1.1);
                                    $scope.non_tam_sop_so.data[sv2].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv2].StandardCost/(1+(PPNPerc/100)));

                                    $scope.non_tam_sop_so.data[sv2].UnitPriceClean = HargaSatuan.toFixed(2);
                                }else{
                                    
                                    var HargaSatuan    = $scope.non_tam_sop_so.data[sv2].newUnitPrice;
                                    $scope.non_tam_sop_so.data[sv2].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv2].newUnitPrice);
                                }
                                // var HargaSatuan    = $scope.non_tam_sop_so.data[sv2].StandardCost/1.1;
                                // var DiscountSatuan = ($scope.non_tam_sop_so.data[sv2].DiscountPercent / 100) * HargaSatuan;
                                // $scope.non_tam_sop_so.data[sv2].DiscountAmount = parseFloat($scope.non_tam_sop_so.data[sv2].QtyPO * DiscountSatuan.toFixed($scope.roundTo)).toFixed();
                                $scope.non_tam_sop_so.data[sv2].DiscountAmount = (Math.round($scope.non_tam_sop_so.data[sv2].UnitPrice*$scope.non_tam_sop_so.data[sv2].DiscountPercent / 100))*$scope.non_tam_sop_so.data[sv2].QtyPO;
                                // var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
                                // var tmpVataAmount = $scope.non_tam_sop_so.data[sv2].QtyPO * VATSatuan;
                                // $scope.non_tam_sop_so.data[sv2].VATAmount = Math.round(tmpVataAmount);

                                // $scope.non_tam_sop_so.data[sv2].VATAmount = (Math.round(($scope.non_tam_sop_so.data[sv2].UnitPrice - $scope.non_tam_sop_so.data[sv2].DiscountAmount)*0.1))*$scope.non_tam_sop_so.data[sv2].QtyPO;
                                $scope.non_tam_sop_so.data[sv2].VATAmount = (Math.round(($scope.non_tam_sop_so.data[sv2].UnitPrice - $scope.non_tam_sop_so.data[sv2].DiscountAmount)*(PPNPerc/100)))*$scope.non_tam_sop_so.data[sv2].QtyPO;

                                // $scope.non_tam_sop_so.data[sv2].UnitPrice = parseFloat($scope.non_tam_sop_so.data[sv2].StandardCost/1.1).toFixed();
                                
                                // var HargaSatuanSV9 = ($scope.non_tam_sop_so.data[sv2].StandardCost / (1.1)).toFixed();
                                // var DiscountSatuanSV9 = Math.round((($scope.non_tam_sop_so.data[sv2].DiscountPercent / 100)) * HargaSatuanSV9);
                                // $scope.non_tam_sop_so.data[sv2].DiscountAmount = $scope.non_tam_sop_so.data[sv2].QtyPO * DiscountSatuanSV9;
                                // $scope.non_tam_sop_so.data[sv2].VATAmount =$scope.non_tam_sop_so.data[sv2].QtyPO *((HargaSatuanSV9 - DiscountSatuanSV9) * 0.1).toFixed();
                                // $scope.non_tam_sop_so.data[sv2].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv2].StandardCost / 1.1);
                            }else if($scope.mData.IsNONPKP == true){
                                //Non PKP
                                if($scope.non_tam_sop_so.data[sv2].newUnitPrice == undefined){
        
                                    var HargaSatuanSV9 = $scope.non_tam_sop_so.data[sv2].StandardCost;
                                    $scope.non_tam_sop_so.data[sv2].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv2].StandardCost);
                                    $scope.non_tam_sop_so.data[sv2].UnitPriceClean = HargaSatuanSV9.toFixed(2);
                                }else{
                                    
                                    var HargaSatuanSV9 = $scope.non_tam_sop_so.data[sv2].newUnitPrice;
                                    $scope.non_tam_sop_so.data[sv2].UnitPrice = $scope.non_tam_sop_so.data[sv2].newUnitPrice;
                                }
                                // var HargaSatuanSV9 = $scope.non_tam_sop_so.data[sv2].StandardCost;
                                // var DiscountSatuanSV9 = Math.round((($scope.non_tam_sop_so.data[sv2].DiscountPercent / 100)) * HargaSatuanSV9);
                                // $scope.non_tam_sop_so.data[sv2].DiscountAmount = $scope.non_tam_sop_so.data[sv2].QtyPO * DiscountSatuanSV9;
                                $scope.non_tam_sop_so.data[sv2].DiscountAmount = (Math.round($scope.non_tam_sop_so.data[sv2].UnitPrice*$scope.non_tam_sop_so.data[sv2].DiscountPercent / 100))*$scope.non_tam_sop_so.data[sv2].QtyPO;
                                $scope.non_tam_sop_so.data[sv2].VATAmount = 0;
                                // $scope.non_tam_sop_so.data[sv2].UnitPrice = $scope.non_tam_sop_so.data[sv2].StandardCost;
                            }
                            // if($scope.mData.IsNONPKP == false){
                            if($scope.non_tam_sop_so.data[sv2].newUnitPrice == undefined){
                                $scope.non_tam_sop_so.data[sv2].TotalAmount =  (($scope.non_tam_sop_so.data[sv2].UnitPrice * $scope.non_tam_sop_so.data[sv2].QtyPO) - $scope.non_tam_sop_so.data[sv2].DiscountAmount)+$scope.non_tam_sop_so.data[sv2].VATAmount;
                            }else{
                                $scope.non_tam_sop_so.data[sv2].TotalAmount =  Math.round(($scope.non_tam_sop_so.data[sv2].QtyPO * $scope.non_tam_sop_so.data[sv2].newUnitPrice) - $scope.non_tam_sop_so.data[sv2].DiscountAmount + $scope.non_tam_sop_so.data[sv2].VATAmount);
                            }
                            // }else{
                            //     // if($scope.non_tam_sop_so.data[sv2].newUnitPrice == undefined){
                            //         $scope.non_tam_sop_so.data[sv2].TotalAmount = ($scope.non_tam_sop_so.data[sv2].QtyPO * $scope.non_tam_sop_so.data[sv2].UnitPrice) - $scope.non_tam_sop_so.data[sv2].DiscountAmount + $scope.non_tam_sop_so.data[sv2].VATAmount;
                            //     // }else{
                                    
                            //     // }
                            // }
						}
					}
					var svTotalAmount2 = 0;
					for (var si2 = 0; si2 < $scope.non_tam_sop_so.data.length; si2++) {
						svTotalAmount2 = svTotalAmount2 + $scope.non_tam_sop_so.data[si2].TotalAmount;
						$scope.mData.TotalAmount = svTotalAmount2;
					}
				}else if ($scope.crudState === '3441') {
					for (var sv3 in $scope.non_tam_non_sop_bengkel.data) {
						if($scope.non_tam_non_sop_bengkel.data[sv3].QtyPO){
							//var HargaSatuanSV1 = $scope.non_tam_non_sop_bengkel.data[sv3].StandardCost / (1.1);
							//var DiscountSatuanSV1 = Math.round((($scope.non_tam_non_sop_bengkel.data[sv3].DiscountPercent / 100)) * HargaSatuanSV1);
							//$scope.non_tam_non_sop_bengkel.data[sv3].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[sv3].QtyPO * DiscountSatuanSV1;
							// CR5 #10
							console.log('$scope.mData.IsNONPKP at non_tam_non_sop_bengkel ($scope.crudState === 3441) stv: ', $scope.mData.IsNONPKP);
							if($scope.mData.IsNONPKP == false){
								//PPn

								// var HargaSatuanSV1 = $scope.non_tam_non_sop_bengkel.data[sv3].StandardCost / (1.1);
								var HargaSatuanSV1 = $scope.non_tam_non_sop_bengkel.data[sv3].StandardCost / (1+(PPNPerc/100));


								var DiscountSatuanSV1 = Math.round((($scope.non_tam_non_sop_bengkel.data[sv3].DiscountPercent / 100)) * HargaSatuanSV1);

								// $scope.non_tam_non_sop_bengkel.data[sv3].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv3].StandardCost / 1.1);
								$scope.non_tam_non_sop_bengkel.data[sv3].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv3].StandardCost / (1+(PPNPerc/100)));

                                // $scope.non_tam_non_sop_bengkel.data[sv3].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[sv3].QtyPO * DiscountSatuanSV1;
								$scope.non_tam_non_sop_bengkel.data[sv3].DiscountAmount = (Math.round($scope.non_tam_non_sop_bengkel.data[sv3].UnitPrice*$scope.non_tam_non_sop_bengkel.data[sv3].DiscountPercent / 100))*$scope.non_tam_non_sop_bengkel.data[sv3].QtyPO;
                                // $scope.non_tam_non_sop_bengkel.data[sv3].VATAmount = Math.round($scope.non_tam_non_sop_bengkel.data[sv3].QtyPO * Math.round((HargaSatuanSV1 - DiscountSatuanSV1) * 0.1));

								// $scope.non_tam_non_sop_bengkel.data[sv3].VATAmount = (Math.round(($scope.non_tam_non_sop_bengkel.data[sv3].UnitPrice - $scope.non_tam_non_sop_bengkel.data[sv3].DiscountAmount)*0.1))*$scope.non_tam_non_sop_bengkel.data[sv3].QtyPO;
								$scope.non_tam_non_sop_bengkel.data[sv3].VATAmount = (Math.round(($scope.non_tam_non_sop_bengkel.data[sv3].UnitPrice - $scope.non_tam_non_sop_bengkel.data[sv3].DiscountAmount)*(PPNPerc/100)))*$scope.non_tam_non_sop_bengkel.data[sv3].QtyPO;

							}else if($scope.mData.IsNONPKP == true){
								//Non PPn
								var HargaSatuanSV1 = $scope.non_tam_non_sop_bengkel.data[sv3].StandardCost;
								var DiscountSatuanSV1 = Math.round((($scope.non_tam_non_sop_bengkel.data[sv3].DiscountPercent / 100)) * HargaSatuanSV1);
								// $scope.non_tam_non_sop_bengkel.data[sv3].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[sv3].QtyPO * DiscountSatuanSV1;
								$scope.non_tam_non_sop_bengkel.data[sv3].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv3].StandardCost);
								$scope.non_tam_non_sop_bengkel.data[sv3].DiscountAmount = (Math.round($scope.non_tam_non_sop_bengkel.data[sv3].UnitPrice*$scope.non_tam_non_sop_bengkel.data[sv3].DiscountPercent / 100))*$scope.non_tam_non_sop_bengkel.data[sv3].QtyPO;
                                $scope.non_tam_non_sop_bengkel.data[sv3].VATAmount = 0;
							}
							// CR5 #10
							/*if($scope.mData.NoEstimasi == null || $scope.mData.NoEstimasi == undefined || $scope.mData.NoEstimasi == ''){
								$scope.non_tam_non_sop_bengkel.data[sv3].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[sv3].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv3].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv3].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[sv3].VATAmount;
							}else{
								if($scope.mData.IsNONPKP == false){
									//
								}else if($scope.mData.IsNONPKP == true){
									$scope.non_tam_non_sop_bengkel.data[sv3].UnitPrice = $scope.non_tam_non_sop_bengkel.data[sv3].UnitPrice / 1.1;
								}
								$scope.non_tam_non_sop_bengkel.data[sv3].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[sv3].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv3].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv3].DiscountAmount;
							}*/
							$scope.non_tam_non_sop_bengkel.data[sv3].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[sv3].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv3].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv3].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[sv3].VATAmount;
							//$scope.non_tam_non_sop_bengkel.data[sv3].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[sv3].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv3].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv3].DiscountAmount;
						}
					}
					var svTotalAmount3 = 0;
					for (var si3 = 0; si3 < $scope.non_tam_non_sop_bengkel.data.length; si3++) {
						svTotalAmount3 = svTotalAmount3 + $scope.non_tam_non_sop_bengkel.data[si3].TotalAmount;
						$scope.mData.TotalAmount = svTotalAmount3;
					}
				}
			}else{
                console.log("$scope.crudState bawah",$scope.crudState);
				if ($scope.crudState === '142') {
					for (var sv4 in $scope.non_tam_sop_so.data) {
						if($scope.non_tam_sop_so.data[sv4].QtyPO){
							//var HargaSatuanSV2 = $scope.non_tam_sop_so.data[sv4].StandardCost / (1.1);
							//var DiscountSatuanSV2 = ($scope.non_tam_sop_so.data[sv4].DiscountPercent / 100) * HargaSatuanSV2;
							//$scope.non_tam_sop_so.data[sv4].DiscountAmount = $scope.non_tam_sop_so.data[sv4].QtyPO * DiscountSatuanSV2.toFixed($scope.roundTo);
							// CR5 #10
							console.log('$scope.mData.IsNONPKP at non_tam_sop_so ($scope.crudState === 142) stv: ', $scope.mData.IsNONPKP);
                            if($scope.mData.IsNONPKP == false){  
                                //PKP
                                if($scope.non_tam_sop_so.data[sv4].newUnitPrice == undefined){
        
                                    // var HargaSatuan    = $scope.non_tam_sop_so.data[sv4].StandardCost/1.1;
                                    var HargaSatuan    = $scope.non_tam_sop_so.data[sv4].StandardCost/(1+(PPNPerc/100));

                                    // $scope.non_tam_sop_so.data[sv4].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv4].StandardCost/1.1);
                                    $scope.non_tam_sop_so.data[sv4].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv4].StandardCost/(1+(PPNPerc/100)));

                                    
                                    $scope.non_tam_sop_so.data[sv4].UnitPriceClean = HargaSatuan.toFixed(2);
                                }else{
                                    
                                    var HargaSatuan    = $scope.non_tam_sop_so.data[sv4].newUnitPrice;
                                    $scope.non_tam_sop_so.data[sv4].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv4].newUnitPrice);
                                
                                    // $scope.non_tam_sop_so.data[sv5].UnitPriceClean = HargaSatuan.toFixed(2);
                                }
                                // var HargaSatuan    = $scope.non_tam_sop_so.data[sv4].StandardCost/1.1;
                                // var DiscountSatuan = ($scope.non_tam_sop_so.data[sv4].DiscountPercent / 100) * HargaSatuan;
                                // $scope.non_tam_sop_so.data[sv4].DiscountAmount = parseFloat($scope.non_tam_sop_so.data[sv4].QtyPO * DiscountSatuan.toFixed($scope.roundTo)).toFixed();
                                $scope.non_tam_sop_so.data[sv4].DiscountAmount = (Math.round($scope.non_tam_sop_so.data[sv4].UnitPrice*$scope.non_tam_sop_so.data[sv4].DiscountPercent / 100))*$scope.non_tam_sop_so.data[sv4].QtyPO;
                                // var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
                                // var tmpVataAmount = $scope.non_tam_sop_so.data[sv4].QtyPO * VATSatuan;

                                // $scope.non_tam_sop_so.data[sv4].VATAmount = (Math.round(($scope.non_tam_sop_so.data[sv4].UnitPrice - $scope.non_tam_sop_so.data[sv4].DiscountAmount)*0.1))*$scope.non_tam_sop_so.data[sv4].QtyPO;
                                $scope.non_tam_sop_so.data[sv4].VATAmount = (Math.round(($scope.non_tam_sop_so.data[sv4].UnitPrice - $scope.non_tam_sop_so.data[sv4].DiscountAmount)*(PPNPerc/100)))*$scope.non_tam_sop_so.data[sv4].QtyPO;

                                // $scope.non_tam_sop_so.data[sv4].VATAmount = Math.round(tmpVataAmount);
                                // $scope.non_tam_sop_so.data[sv4].UnitPrice = parseFloat($scope.non_tam_sop_so.data[sv4].StandardCost/1.1).toFixed();
                                
                                // var HargaSatuanSV9 = ($scope.non_tam_sop_so.data[sv4].StandardCost / (1.1)).toFixed();
                                // var DiscountSatuanSV9 = Math.round((($scope.non_tam_sop_so.data[sv4].DiscountPercent / 100)) * HargaSatuanSV9);
                                // $scope.non_tam_sop_so.data[sv4].DiscountAmount = $scope.non_tam_sop_so.data[sv4].QtyPO * DiscountSatuanSV9;
                                // $scope.non_tam_sop_so.data[sv4].VATAmount =$scope.non_tam_sop_so.data[sv4].QtyPO *((HargaSatuanSV9 - DiscountSatuanSV9) * 0.1).toFixed();
                                // $scope.non_tam_sop_so.data[sv4].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv4].StandardCost / 1.1);
                            }else if($scope.mData.IsNONPKP == true){
                                //Non PKP
                                if($scope.non_tam_sop_so.data[sv4].newUnitPrice == undefined){
        
                                    var HargaSatuanSV9 = $scope.non_tam_sop_so.data[sv4].StandardCost;
                                    $scope.non_tam_sop_so.data[sv4].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv4].StandardCost);
                                    $scope.non_tam_sop_so.data[sv4].UnitPriceClean = HargaSatuanSV9.toFixed(2);
                                }else{
                                    
                                    var HargaSatuanSV9 = $scope.non_tam_sop_so.data[sv4].newUnitPrice;
                                    $scope.non_tam_sop_so.data[sv4].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv4].newUnitPrice);
                                }
                                // var HargaSatuanSV9 = $scope.non_tam_sop_so.data[sv4].StandardCost;
                                var DiscountSatuanSV9 = Math.round((($scope.non_tam_sop_so.data[sv4].DiscountPercent / 100)) * HargaSatuanSV9);
                                // $scope.non_tam_sop_so.data[sv4].DiscountAmount = $scope.non_tam_sop_so.data[sv4].QtyPO * DiscountSatuanSV9;
                                $scope.non_tam_sop_so.data[sv4].DiscountAmount = (Math.round($scope.non_tam_sop_so.data[sv4].UnitPrice*$scope.non_tam_sop_so.data[sv4].DiscountPercent / 100))*$scope.non_tam_sop_so.data[sv4].QtyPO;
                                $scope.non_tam_sop_so.data[sv4].VATAmount = 0;
                                // $scope.non_tam_sop_so.data[sv4].UnitPrice = $scope.non_tam_sop_so.data[sv4].StandardCost;
                            }
                            if($scope.mData.IsNONPKP == false){
                                if($scope.non_tam_sop_so.data[sv4].newUnitPrice == undefined){
                                    $scope.non_tam_sop_so.data[sv4].TotalAmount =  (($scope.non_tam_sop_so.data[sv4].UnitPrice * $scope.non_tam_sop_so.data[sv4].QtyPO) - $scope.non_tam_sop_so.data[sv4].DiscountAmount)+$scope.non_tam_sop_so.data[sv4].VATAmount;
                                }else{
                                    // $scope.non_tam_sop_so.data[sv4].TotalAmount =  Math.round(($scope.non_tam_sop_so.data[sv4].QtyPO * $scope.non_tam_sop_so.data[sv4].newUnitPrice) - $scope.non_tam_sop_so.data[sv4].DiscountAmount + ($scope.non_tam_sop_so.data[sv4].QtyPO * (HargaSatuan - DiscountSatuan) * 0.1));
                                    $scope.non_tam_sop_so.data[sv4].TotalAmount =  Math.round(($scope.non_tam_sop_so.data[sv4].QtyPO * $scope.non_tam_sop_so.data[sv4].newUnitPrice) - $scope.non_tam_sop_so.data[sv4].DiscountAmount + ($scope.non_tam_sop_so.data[sv4].QtyPO * (HargaSatuan - DiscountSatuan) * (PPNPerc/100)));

                                }
                                // $scope.non_tam_sop_so.data[sv4].TotalAmount =  Math.round(($scope.non_tam_sop_so.data[sv4].QtyPO * ($scope.non_tam_sop_so.data[sv4].StandardCost/1.1)) - $scope.non_tam_sop_so.data[sv4].DiscountAmount + ($scope.non_tam_sop_so.data[sv4].QtyPO * (HargaSatuan - DiscountSatuan) * 0.1));
                            }else{
                                $scope.non_tam_sop_so.data[sv4].TotalAmount = ($scope.non_tam_sop_so.data[sv4].QtyPO * $scope.non_tam_sop_so.data[sv4].UnitPrice) - $scope.non_tam_sop_so.data[sv4].DiscountAmount + $scope.non_tam_sop_so.data[sv4].VATAmount;
                            }
						}
					}
					var svTotalAmount4 = 0;
					for (var si4 = 0; si4 < $scope.non_tam_sop_so.data.length; si4++) {
						svTotalAmount4 = svTotalAmount4 + $scope.non_tam_sop_so.data[si4].TotalAmount;
						$scope.mData.TotalAmount = svTotalAmount4;
					}
				}else if ($scope.crudState === '152' || $scope.crudState === '151') {
					for (var sv5 in $scope.sublet_sop_wo.data) {
						if($scope.sublet_sop_wo.data[sv5].QtyPO){
							//var HargaSatuanSV3 = $scope.sublet_sop_wo.data[sv5].StandardCost / (1.1);
							//var DiscountSatuanSV3 = ($scope.sublet_sop_wo.data[sv5].DiscountPercent / 100) * HargaSatuanSV3;
							//$scope.sublet_sop_wo.data[sv5].DiscountAmount = $scope.sublet_sop_wo.data[sv5].QtyPO * DiscountSatuanSV3.toFixed($scope.roundTo);
							// CR5 #10
							console.log('$scope.mData.IsNONPKP at sublet_sop_wo ($scope.crudState === 152 || $scope.crudState === 151) stv: ', $scope.mData.IsNONPKP);
							console.log("aaaaaa masuk dong",$scope.tmpPO);
                            if($scope.tmpPO == 5){
                                var VATSatuanSV3;

                                // var HargaSatuanSV3 = $scope.sublet_sop_wo.data[sv5].StandardCost/1.1;
                                var HargaSatuanSV3 = $scope.sublet_sop_wo.data[sv5].StandardCost/(1+(PPNPerc/100));

                                $scope.sublet_sop_wo.data[sv5].UnitPriceClean = HargaSatuanSV3.toFixed(2);
                                var DiscountSatuanSV3 = ($scope.sublet_sop_wo.data[sv5].DiscountPercent / 100) * HargaSatuanSV3;


                                // ------------------------ pake picing engine dong ------------------------------------- start 

                                var HargaSatuanSV3 =    ASPricingEngine.calculate({
                                                            InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                            Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                            Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                            Tipe: 2, 
                                                            PPNPercentage: PPNPerc,
                                                        }); 

                                $scope.sublet_sop_wo.data[sv5].UnitPriceClean = HargaSatuanSV3

                                var DiscountSatuanSV3 = ASPricingEngine.calculate({
                                                            InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                            Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                            Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                            Tipe: 12, 
                                                            PPNPercentage: PPNPerc,
                                                        }); 


                                // ------------------------ pake picing engine dong ------------------------------------- end 



                                if($scope.mData.IsNONPKP == false){
                                    //PKP
                                    // if($scope.sublet_sop_wo.data[sv5].newUnitPrice == undefined){
                                       
                                    //     $scope.sublet_sop_wo.data[sv5].VATAmount = Math.round((($scope.sublet_sop_wo.data[sv5].QtyPO * ($scope.sublet_sop_wo.data[sv5].StandardCost/1.1))-$scope.sublet_sop_wo.data[sv5].DiscountAmount)*0.1);
                                    // }else{
                                        
                                    //     $scope.sublet_sop_wo.data[sv5].VATAmount = Math.round((($scope.sublet_sop_wo.data[sv5].QtyPO * $scope.sublet_sop_wo.data[sv5].newUnitPrice)-$scope.sublet_sop_wo.data[sv5].DiscountAmount)*0.1);
                                    // }

                                    if($scope.sublet_sop_wo.data[sv5].newUnitPrice == undefined){

                                        // VATSatuanSV3 =  Math.round((HargaSatuanSV3 - DiscountSatuanSV3) * 0.1);
                                        VATSatuanSV3 =  Math.round((HargaSatuanSV3 - DiscountSatuanSV3) * (PPNPerc/100));

                                        // $scope.sublet_sop_wo.data[sv5].VATAmount = (Math.round(($scope.sublet_sop_wo.data[sv5].UnitPrice - $scope.sublet_sop_wo.data[sv5].DiscountAmount)*0.1))*$scope.sublet_sop_wo.data[sv5].QtyPO;
                                        $scope.sublet_sop_wo.data[sv5].VATAmount = (Math.round(($scope.sublet_sop_wo.data[sv5].UnitPrice - $scope.sublet_sop_wo.data[sv5].DiscountAmount)*(PPNPerc/100)))*$scope.sublet_sop_wo.data[sv5].QtyPO;

                                        // $scope.sublet_sop_wo.data[sv5].VATAmount = Math.round((($scope.sublet_sop_wo.data[sv5].QtyPO * ($scope.sublet_sop_wo.data[sv5].StandardCost/1.1))-$scope.sublet_sop_wo.data[sv5].DiscountAmount)*0.1);


                                        // ------------------------ pake picing engine dong ------------------------------------- start 

                                        VATSatuanSV3 =  ASPricingEngine.calculate({
                                                            InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                            Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                            Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                            Tipe: 31, 
                                                            PPNPercentage: PPNPerc,
                                                        }); 

                                        $scope.sublet_sop_wo.data[sv5].VATAmount =  ASPricingEngine.calculate({
                                                                                        InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                                                        Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                                                        Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                                                        Tipe: 132, 
                                                                                        PPNPercentage: PPNPerc,
                                                                                    }); 


                                        // ------------------------ pake picing engine dong ------------------------------------- end 


                                    }else{

                                        // VATSatuanSV3 =  Math.round((HargaSatuanSV3 - DiscountSatuanSV3) * 0.1);
                                        VATSatuanSV3 =  Math.round((HargaSatuanSV3 - DiscountSatuanSV3) * (PPNPerc/100));

                                        // $scope.sublet_sop_wo.data[sv5].VATAmount = Math.round((($scope.sublet_sop_wo.data[sv5].QtyPO * $scope.sublet_sop_wo.data[sv5].newUnitPrice)-$scope.sublet_sop_wo.data[sv5].DiscountAmount)*0.1);
                                        $scope.sublet_sop_wo.data[sv5].VATAmount = Math.round((($scope.sublet_sop_wo.data[sv5].QtyPO * $scope.sublet_sop_wo.data[sv5].newUnitPrice)-$scope.sublet_sop_wo.data[sv5].DiscountAmount)*(PPNPerc/100));



                                        // ------------------------ pake picing engine dong ------------------------------------- start 

                                        VATSatuanSV3 =  ASPricingEngine.calculate({
                                                            InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                            Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                            Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                            Tipe: 31, 
                                                            PPNPercentage: PPNPerc,
                                                        }); 

                                        $scope.sublet_sop_wo.data[sv5].VATAmount =  ASPricingEngine.calculate({
                                                                                        InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                                                        Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                                                        Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                                                        Tipe: 132, 
                                                                                        PPNPercentage: PPNPerc,
                                                                                    }); 


                                        // ------------------------ pake picing engine dong ------------------------------------- end 
                                    }
                                    
                                }else{
                                    VATSatuanSV3 = 0;
                                    $scope.sublet_sop_wo.data[sv5].VATAmount = 0;
                                }
                                console.log("VATSatuanSV3",VATSatuanSV3);
                                $scope.sublet_sop_wo.data[sv5].DiscountAmount = (Math.round($scope.sublet_sop_wo.data[sv5].UnitPrice*$scope.sublet_sop_wo.data[sv5].DiscountPercent / 100))*$scope.sublet_sop_wo.data[sv5].QtyPO;


                                // ------------------------ pake picing engine dong ------------------------------------- start 
                                $scope.sublet_sop_wo.data[sv5].DiscountAmount =     ASPricingEngine.calculate({
                                                                                        InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                                                        Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                                                        Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                                                        Tipe: 112, 
                                                                                        PPNPercentage: PPNPerc,
                                                                                    });
                                // ------------------------ pake picing engine dong ------------------------------------- end 






                                // $scope.sublet_sop_wo.data[sv5].DiscountAmount = $scope.sublet_sop_wo.data[sv5].QtyPO * DiscountSatuanSV3.toFixed($scope.roundTo);
                                // console.log("Math.round(($scope.sublet_sop_wo.data[sv5].QtyPO * ($scope.sublet_sop_wo.data[sv5].StandardCost/1.1))",Math.round(($scope.sublet_sop_wo.data[sv5].QtyPO * ($scope.sublet_sop_wo.data[sv5].StandardCost/1.1)));
                                // $scope.sublet_sop_wo.data[sv5].VATAmount = Math.round($scope.sublet_sop_wo.data[sv5].QtyPO * VATSatuanSV3.toFixed($scope.roundTo));
                                
                                console.log("($scope.sublet_sop_wo.data[sv5].StandardCost/1.1)",($scope.sublet_sop_wo.data[sv5].StandardCost/(1+(PPNPerc/100))));
                                console.log("VATSatuanSV3.toFixed($scope.roundTo)",VATSatuanSV3.toFixed($scope.roundTo));
                                if($scope.mData.IsNONPKP == false){
                                    if($scope.sublet_sop_wo.data[sv5].newUnitPrice == undefined){
                                        $scope.sublet_sop_wo.data[sv5].TotalAmount =  (($scope.sublet_sop_wo.data[sv5].UnitPrice * $scope.sublet_sop_wo.data[sv5].QtyPO) - $scope.sublet_sop_wo.data[sv5].DiscountAmount)+$scope.sublet_sop_wo.data[sv5].VATAmount;
                                        // $scope.sublet_sop_wo.data[sv5].TotalAmount = Math.round((($scope.sublet_sop_wo.data[sv5].StandardCost/1.1)* $scope.sublet_sop_wo.data[sv5].QtyPO) - $scope.sublet_sop_wo.data[sv5].DiscountAmount + ( $scope.sublet_sop_wo.data[sv5].QtyPO * ((HargaSatuanSV3 - DiscountSatuanSV3) * 0.1)));
                                        // $scope.sublet_sop_wo.data[sv5].TotalAmount = Math.round(($scope.sublet_sop_wo.data[sv5].QtyPO * ($scope.sublet_sop_wo.data[sv5].StandardCost/1.1)) - $scope.sublet_sop_wo.data[sv5].DiscountAmount + $scope.sublet_sop_wo.data[sv5].VATAmount);



                                        // ------------------------ pake picing engine dong ------------------------------------- start 
                                        $scope.sublet_sop_wo.data[sv5].TotalAmount =     ASPricingEngine.calculate({
                                                                                                InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                                                                Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                                                                Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                                                                Tipe: 121, 
                                                                                                PPNPercentage: PPNPerc,
                                                                                            });
                                        // ------------------------ pake picing engine dong ------------------------------------- end 
                                    }else{
                                        $scope.sublet_sop_wo.data[sv5].TotalAmount = Math.round(($scope.sublet_sop_wo.data[sv5].QtyPO * $scope.sublet_sop_wo.data[sv5].newUnitPrice) - $scope.sublet_sop_wo.data[sv5].DiscountAmount + $scope.sublet_sop_wo.data[sv5].VATAmount);

                                        // ------------------------ pake picing engine dong ------------------------------------- start 
                                        $scope.sublet_sop_wo.data[sv5].TotalAmount =     ASPricingEngine.calculate({
                                                                                            InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                                                            Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                                                            Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                                                            Tipe: 121, 
                                                                                            PPNPercentage: PPNPerc,
                                                                                        });
                                        // ------------------------ pake picing engine dong ------------------------------------- end 
                                    }
                                }else{
                                    if($scope.sublet_sop_wo.data[sv5].newUnitPrice == undefined){
                                        $scope.sublet_sop_wo.data[sv5].TotalAmount =  (($scope.sublet_sop_wo.data[sv5].UnitPrice * $scope.sublet_sop_wo.data[sv5].QtyPO) - $scope.sublet_sop_wo.data[sv5].DiscountAmount)+$scope.sublet_sop_wo.data[sv5].VATAmount;
                                        // $scope.sublet_sop_wo.data[sv5].TotalAmount = Math.round((($scope.sublet_sop_wo.data[sv5].StandardCost/1.1)* $scope.sublet_sop_wo.data[sv5].QtyPO) - ($scope.sublet_sop_wo.data[sv5].QtyPO *(($scope.sublet_sop_wo.data[sv5].DiscountPercent / 100) * HargaSatuanSV3)) + $scope.sublet_sop_wo.data[sv5].VATAmount);
                                        // $scope.sublet_sop_wo.data[sv5].TotalAmount = Math.round(($scope.sublet_sop_wo.data[sv5].QtyPO * ($scope.sublet_sop_wo.data[sv5].StandardCost/1.1)) - $scope.sublet_sop_wo.data[sv5].DiscountAmount + VATSatuanSV3);

                                        // ------------------------ pake picing engine dong ------------------------------------- start 
                                        $scope.sublet_sop_wo.data[sv5].TotalAmount =     ASPricingEngine.calculate({
                                                                                            InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                                                            Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                                                            Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                                                            Tipe: 121, 
                                                                                            PPNPercentage: PPNPerc,
                                                                                        });
                                        // ------------------------ pake picing engine dong ------------------------------------- end 
                                    }else{
                                        $scope.sublet_sop_wo.data[sv5].TotalAmount = Math.round(($scope.sublet_sop_wo.data[sv5].QtyPO * $scope.sublet_sop_wo.data[sv5].newUnitPrice) - $scope.sublet_sop_wo.data[sv5].DiscountAmount + VATSatuanSV3);
                                        
                                        // ------------------------ pake picing engine dong ------------------------------------- start 
                                        $scope.sublet_sop_wo.data[sv5].TotalAmount =     ASPricingEngine.calculate({
                                                                                            InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                                                            Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                                                            Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                                                            Tipe: 121, 
                                                                                            PPNPercentage: PPNPerc,
                                                                                        });
                                        // ------------------------ pake picing engine dong ------------------------------------- end 
                                    }
                                    
                                }
                                
                                // $scope.sublet_sop_wo.data[sv5].UnitPrice = $scope.sublet_sop_wo.data[sv5].StandardCost;
                            }else{
                                if($scope.mData.IsNONPKP == false){
                                    //PKP
                                    var HargaSatuanSV3 = $scope.sublet_sop_wo.data[sv5].StandardCost;
                                    $scope.sublet_sop_wo.data[sv5].UnitPrice = Math.round($scope.sublet_sop_wo.data[sv5].StandardCost);
                                    var DiscountSatuanSV3 = ($scope.sublet_sop_wo.data[sv5].DiscountPercent / 100) * HargaSatuanSV3;
                                    $scope.sublet_sop_wo.data[sv5].DiscountAmount = (Math.round($scope.sublet_sop_wo.data[sv5].UnitPrice*$scope.sublet_sop_wo.data[sv5].DiscountPercent / 100))*$scope.sublet_sop_wo.data[sv5].QtyPO;
                                    // $scope.sublet_sop_wo.data[sv5].DiscountAmount = $scope.sublet_sop_wo.data[sv5].QtyPO * DiscountSatuanSV3.toFixed($scope.roundTo);

                                    // var VATSatuanSV3 = (HargaSatuanSV3 - DiscountSatuanSV3) * 0.1;
                                    var VATSatuanSV3 = (HargaSatuanSV3 - DiscountSatuanSV3) * (PPNPerc/100);

                                    // $scope.sublet_sop_wo.data[sv5].VATAmount = Math.round($scope.sublet_sop_wo.data[sv5].QtyPO * VATSatuanSV3.toFixed($scope.roundTo));
                                    
                                    // $scope.sublet_sop_wo.data[sv5].VATAmount = (Math.round(($scope.sublet_sop_wo.data[sv5].UnitPrice - $scope.sublet_sop_wo.data[sv5].DiscountAmount)*0.1))*$scope.sublet_sop_wo.data[sv5].QtyPO;
                                    $scope.sublet_sop_wo.data[sv5].VATAmount = (Math.round(($scope.sublet_sop_wo.data[sv5].UnitPrice - $scope.sublet_sop_wo.data[sv5].DiscountAmount)*(PPNPerc/100)))*$scope.sublet_sop_wo.data[sv5].QtyPO;



                                    // ------------------------ pake picing engine dong ------------------------------------- start 

                                    var HargaSatuanSV3 =    ASPricingEngine.calculate({
                                                                InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                                Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                                Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                                Tipe: 2, 
                                                                PPNPercentage: PPNPerc,
                                                            }); 

                                    $scope.sublet_sop_wo.data[sv5].UnitPrice = HargaSatuanSV3

                                    var DiscountSatuanSV3 = ASPricingEngine.calculate({
                                                                InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                                Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                                Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                                Tipe: 12, 
                                                                PPNPercentage: PPNPerc,
                                                            }); 

                                    $scope.sublet_sop_wo.data[sv5].DiscountAmount =     ASPricingEngine.calculate({
                                                                                            InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                                                            Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                                                            Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                                                            Tipe: 112, 
                                                                                            PPNPercentage: PPNPerc,
                                                                                        });

                                    var VATSatuanSV3 =      ASPricingEngine.calculate({
                                                                InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                                Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                                Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                                Tipe: 31, 
                                                                PPNPercentage: PPNPerc,
                                                            }); 

                                    $scope.sublet_sop_wo.data[sv5].VATAmount =  ASPricingEngine.calculate({
                                                                                    InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                                                    Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                                                    Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                                                    Tipe: 132, 
                                                                                    PPNPercentage: PPNPerc,
                                                                                }); 

                                    $scope.sublet_sop_wo.data[sv5].TotalAmount =     ASPricingEngine.calculate({
                                                                                        InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                                                                        Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                                                                        Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                                                                        Tipe: 121, 
                                                                                        PPNPercentage: PPNPerc,
                                                                                    });


                                    // ------------------------ pake picing engine dong ------------------------------------- end 

                                }else if($scope.mData.IsNONPKP == true){
                                    //Non PKP
                                    var HargaSatuanSV3 = $scope.sublet_sop_wo.data[sv5].StandardCost;
                                    var DiscountSatuanSV3 = ($scope.sublet_sop_wo.data[sv5].DiscountPercent / 100) * HargaSatuanSV3;
                                    $scope.sublet_sop_wo.data[sv5].DiscountAmount = (Math.round($scope.sublet_sop_wo.data[sv5].UnitPrice*$scope.sublet_sop_wo.data[sv5].DiscountPercent / 100))*$scope.sublet_sop_wo.data[sv5].QtyPO;
                                    // $scope.sublet_sop_wo.data[sv5].DiscountAmount = $scope.sublet_sop_wo.data[sv5].QtyPO * DiscountSatuanSV3.toFixed($scope.roundTo);
                                    $scope.sublet_sop_wo.data[sv5].VATAmount = 0;
                                    $scope.sublet_sop_wo.data[sv5].UnitPrice = $scope.sublet_sop_wo.data[sv5].StandardCost;


                                    
                                    $scope.sublet_sop_wo.data[sv5].TotalAmount =  (($scope.sublet_sop_wo.data[sv5].UnitPrice * $scope.sublet_sop_wo.data[sv5].QtyPO) - $scope.sublet_sop_wo.data[sv5].DiscountAmount)+$scope.sublet_sop_wo.data[sv5].VATAmount;
                                    
                                
                                }


                                // $scope.sublet_sop_wo.data[sv5].TotalAmount =  (($scope.sublet_sop_wo.data[sv5].UnitPrice * $scope.sublet_sop_wo.data[sv5].QtyPO) - $scope.sublet_sop_wo.data[sv5].DiscountAmount)+$scope.sublet_sop_wo.data[sv5].VATAmount;
                                // $scope.sublet_sop_wo.data[sv5].TotalAmount = Math.round(($scope.sublet_sop_wo.data[sv5].QtyPO * $scope.sublet_sop_wo.data[sv5].UnitPrice) - $scope.sublet_sop_wo.data[sv5].DiscountAmount + $scope.sublet_sop_wo.data[sv5].VATAmount);



                                // // ------------------------ pake picing engine dong ------------------------------------- start 
                                // $scope.sublet_sop_wo.data[sv5].TotalAmount =     ASPricingEngine.calculate({
                                //                                                     InputPrice: $scope.sublet_sop_wo.data[sv5].StandardCost,
                                //                                                     Discount: $scope.sublet_sop_wo.data[sv5].DiscountPercent,
                                //                                                     Qty: $scope.sublet_sop_wo.data[sv5].QtyPO,
                                //                                                     Tipe: 121, 
                                //                                                     PPNPercentage: PPNPerc,
                                //                                                 });
                                // // ------------------------ pake picing engine dong ------------------------------------- end 
                            }
                           
							// CR5 #10
							/*if($scope.mData.NoEstimasi == null || $scope.mData.NoEstimasi == undefined || $scope.mData.NoEstimasi == ''){
								$scope.sublet_sop_wo.data[sv5].TotalAmount = Math.round(($scope.sublet_sop_wo.data[sv5].QtyPO * $scope.sublet_sop_wo.data[sv5].UnitPrice) - $scope.sublet_sop_wo.data[sv5].DiscountAmount + $scope.sublet_sop_wo.data[sv5].VATAmount);
							}else{
								if($scope.mData.IsNONPKP == false){
									//
								}else if($scope.mData.IsNONPKP == true){
									$scope.sublet_sop_wo.data[sv5].UnitPrice = $scope.sublet_sop_wo.data[sv5].UnitPrice / 1.1;
								}
								$scope.sublet_sop_wo.data[sv5].TotalAmount = Math.round(($scope.sublet_sop_wo.data[sv5].QtyPO * $scope.sublet_sop_wo.data[sv5].UnitPrice) - $scope.sublet_sop_wo.data[sv5].DiscountAmount);
							}*/
							
							console.log("$scope.sublet_sop_wo.data[sv5].TotalAmount",$scope.sublet_sop_wo.data[sv5].TotalAmount);
                            //$scope.sublet_sop_wo.data[sv5].TotalAmount = Math.round(($scope.sublet_sop_wo.data[sv5].QtyPO * $scope.sublet_sop_wo.data[sv5].UnitPrice) - $scope.sublet_sop_wo.data[sv5].DiscountAmount);
						}
					}
                    console.log("$scope.sublet_sop_wo.data[sv5]",$scope.sublet_sop_wo.data);
					var svTotalAmount5 = 0;
					for (var si5 = 0; si5 < $scope.sublet_sop_wo.data.length; si5++) {
						svTotalAmount5 = svTotalAmount5 + $scope.sublet_sop_wo.data[si5].TotalAmount;
						$scope.mData.TotalAmount = svTotalAmount5;
					}


                    // ------------------------ pake picing engine dong ------------------------------------- start 
                    if ($scope.mData.IsNONPKP == false) {
                        $scope.mData.TotalAmount =  ASPricingEngine.calculate({
                                                        InputPrice: $scope.mData.TotalAmount,
                                                        Discount: 0,
                                                        Qty: 0,
                                                        Tipe: 25, 
                                                        PPNPercentage: PPNPerc,
                                                    });
                    }
                    
                    // ------------------------ pake picing engine dong ------------------------------------- end 
                    

                    
				}else if ($scope.crudState === '114') {
					for (var sv6 in $scope.non_tam_non_sop_bengkel.data) {
						if($scope.non_tam_non_sop_bengkel.data[sv6].QtyPO){
							//var HargaSatuanSV4    = $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice;
							//var DiscountSatuanSV4 = ($scope.non_tam_non_sop_bengkel.data[sv6].DiscountPercent / 100) * HargaSatuanSV4;
							// CR5 #10
                            var DiscountSatuanSV4
							console.log('$scope.mData.IsNONPKP at non_tam_non_sop_bengkel.data ($scope.crudState === 114) stv: ', $scope.mData.IsNONPKP);
                            console.log("$scope.mData.IsNONPKP",$scope.mData.IsNONPKP);
                            if($scope.mData.IsNONPKP == false){
								//PKP
								var HargaSatuanSV4;  
								if($scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP == undefined ){
                                    if($scope.non_tam_non_sop_bengkel.data[sv6].newUnitPrice == undefined){
                                        
                                        // HargaSatuanSV4  = $scope.non_tam_non_sop_bengkel.data[sv6].StandardCost/1.1;
                                        HargaSatuanSV4  = $scope.non_tam_non_sop_bengkel.data[sv6].StandardCost/(1+(PPNPerc/100));

                                        // $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv6].StandardCost/1.1);
                                        $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv6].StandardCost/(1+(PPNPerc/100)));

                                        $scope.non_tam_non_sop_bengkel.data[sv6].UnitPriceClean = (HargaSatuanSV4).toFixed(2);
                                    }else{
                                        HargaSatuanSV4    = $scope.non_tam_non_sop_bengkel.data[sv6].newUnitPrice;
                                        $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv6].newUnitPrice);
                                        // $scope.non_tam_non_sop_bengkel.data[sv6].UnitPriceClean = (HargaSatuanSV4).toFixed(2);
                                    }
                                }else{
                                    if($scope.non_tam_non_sop_bengkel.data[sv6].newUnitPrice == undefined){
                                        
                                        // HargaSatuanSV4  = $scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/1.1;
                                        HargaSatuanSV4  = $scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/(1+(PPNPerc/100));

                                        // $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/1.1);
                                        $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/(1+(PPNPerc/100)));

                                        $scope.non_tam_non_sop_bengkel.data[sv6].UnitPriceClean = (HargaSatuanSV4).toFixed(2);
                                    }else{
                                        HargaSatuanSV4    = $scope.non_tam_non_sop_bengkel.data[sv6].newUnitPrice;
                                        $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv6].newUnitPrice);
                                        // $scope.non_tam_non_sop_bengkel.data[sv6].UnitPriceClean = (HargaSatuanSV4).toFixed(2);
                                    }
                                    // HargaSatuanSV4 = ($scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/1.1);
                                }
                                
                                // var HargaSatuan    = $scope.non_tam_non_sop_bengkel.data[sv6].StandardCost;
                                // :1.1 di hapus karna sudah di bagi di BE oleh rade;

                                var DiscountSatuan = ($scope.non_tam_non_sop_bengkel.data[sv6].DiscountPercent / 100) * HargaSatuanSV4;
                                // var VATSatuan = (HargaSatuanSV4 - DiscountSatuan) * 0.1;
                                var VATSatuan = (HargaSatuanSV4 - DiscountSatuan) * (PPNPerc/100);

                                var tmpVataAmount = $scope.non_tam_non_sop_bengkel.data[sv6].QtyPO * VATSatuan.toFixed($scope.roundTo);
                                $scope.non_tam_non_sop_bengkel.data[sv6].VATAmount = parseFloat(tmpVataAmount.toFixed());
                                DiscountSatuanSV4 = DiscountSatuan;
								// var VATSatuanSV4      = (HargaSatuanSV4 - DiscountSatuanSV4) * 0.1;
                                $scope.non_tam_non_sop_bengkel.data[sv6].DiscountAmount = Math.round($scope.non_tam_non_sop_bengkel.data[sv6].QtyPO * DiscountSatuanSV4);
								// $scope.non_tam_non_sop_bengkel.data[sv6].VATAmount = Math.floor($scope.non_tam_non_sop_bengkel.data[sv6].QtyPO * VATSatuanSV4);
								// $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice = $scope.non_tam_non_sop_bengkel.data[sv6].StandardCost;
							}else if($scope.mData.IsNONPKP == true){
								//Non PKP
								var HargaSatuanSV4;
                                if($scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP == undefined ){
                                    if($scope.non_tam_non_sop_bengkel.data[sv6].newUnitPrice == undefined){
                                        

                                        // HargaSatuanSV4  = $scope.non_tam_non_sop_bengkel.data[sv6].StandardCost/1.1;
                                        HargaSatuanSV4  = $scope.non_tam_non_sop_bengkel.data[sv6].StandardCost/(1+(PPNPerc/100));

                                        // $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv6].StandardCost/1.1);
                                        $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv6].StandardCost/(1+(PPNPerc/100)));

                                        $scope.non_tam_non_sop_bengkel.data[sv6].UnitPriceClean = (HargaSatuanSV4).toFixed(2);
                                    }else{
                                        HargaSatuanSV4    = $scope.non_tam_non_sop_bengkel.data[sv6].newUnitPrice;
                                        $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv6].newUnitPrice);
                                    }
                                }else{
                                    if($scope.non_tam_non_sop_bengkel.data[sv6].newUnitPrice == undefined){
                                        
                                        // HargaSatuanSV4  = $scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/1.1;
                                        HargaSatuanSV4  = $scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/(1+(PPNPerc/100));

                                        // $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/1.1);
                                        $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/(1+(PPNPerc/100)));

                                        $scope.non_tam_non_sop_bengkel.data[sv6].UnitPriceClean = (HargaSatuanSV4).toFixed(2);
                                    }else{
                                        HargaSatuanSV4    = $scope.non_tam_non_sop_bengkel.data[sv6].newUnitPrice;
                                        $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv6].newUnitPrice);
                                    }
                                    // HargaSatuanSV4 = ($scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/1.1);
                                }
                                // $scope.non_tam_non_sop_bengkel.data[sv6].UnitPriceClean = ($scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/1.1).toFixed(2);
								DiscountSatuanSV4 = ($scope.non_tam_non_sop_bengkel.data[sv6].DiscountPercent / 100) * HargaSatuanSV4;
                                $scope.non_tam_non_sop_bengkel.data[sv6].DiscountAmount = Math.round($scope.non_tam_non_sop_bengkel.data[sv6].QtyPO * DiscountSatuanSV4);
								$scope.non_tam_non_sop_bengkel.data[sv6].VATAmount = 0;
								// $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice = $scope.non_tam_non_sop_bengkel.data[sv6].StandardCost;
							}
							// CR5 #10
							
							/*if($scope.mData.NoEstimasi == null || $scope.mData.NoEstimasi == undefined || $scope.mData.NoEstimasi == ''){
								$scope.non_tam_non_sop_bengkel.data[sv6].TotalAmount = Math.round(($scope.non_tam_non_sop_bengkel.data[sv6].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv6].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[sv6].VATAmount);
							}else{
								if($scope.mData.IsNONPKP == false){
									//
								}else if($scope.mData.IsNONPKP == true){
									$scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice = $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice / 1.1;
								}
								$scope.non_tam_non_sop_bengkel.data[sv6].TotalAmount = Math.round(($scope.non_tam_non_sop_bengkel.data[sv6].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv6].DiscountAmount);
							}*/
                            if($scope.tmpPO == 5){
                                // console.log("Math.round($scope.non_tam_non_sop_bengkel.data[a].StandardCost * $scope.non_tam_non_sop_bengkel.data[a].QtyEstimationBP)",Math.round($scope.non_tam_non_sop_bengkel.data[a].StandardCost * $scope.non_tam_non_sop_bengkel.data[a].QtyEstimationBP));
                                if($scope.non_tam_non_sop_bengkel.data[sv6].newUnitPrice == undefined){
                                    if($scope.mData.IsNONPKP == false){

                                        // $scope.non_tam_non_sop_bengkel.data[sv6].TotalAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/1.1)* $scope.non_tam_non_sop_bengkel.data[sv6].QtyPO) - $scope.non_tam_non_sop_bengkel.data[sv6].DiscountAmount + ( $scope.non_tam_non_sop_bengkel.data[sv6].QtyPO * ((HargaSatuanSV4 - DiscountSatuanSV4) * 0.1)));
                                        $scope.non_tam_non_sop_bengkel.data[sv6].TotalAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/(1+(PPNPerc/100)))* $scope.non_tam_non_sop_bengkel.data[sv6].QtyPO) - $scope.non_tam_non_sop_bengkel.data[sv6].DiscountAmount + ( $scope.non_tam_non_sop_bengkel.data[sv6].QtyPO * ((HargaSatuanSV4 - DiscountSatuanSV4) * (PPNPerc/100))));

                                    }else{
                                        // $scope.non_tam_non_sop_bengkel.data[sv6].TotalAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/1.1)* $scope.non_tam_non_sop_bengkel.data[sv6].QtyPO) - ($scope.non_tam_non_sop_bengkel.data[sv6].QtyPO *(($scope.non_tam_non_sop_bengkel.data[sv6].DiscountPercent / 100) * HargaSatuanSV4)) + $scope.non_tam_non_sop_bengkel.data[sv6].VATAmount);
                                        $scope.non_tam_non_sop_bengkel.data[sv6].TotalAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/(1+(PPNPerc/100)))* $scope.non_tam_non_sop_bengkel.data[sv6].QtyPO) - ($scope.non_tam_non_sop_bengkel.data[sv6].QtyPO *(($scope.non_tam_non_sop_bengkel.data[sv6].DiscountPercent / 100) * HargaSatuanSV4)) + $scope.non_tam_non_sop_bengkel.data[sv6].VATAmount);

                                    }
                                    // $scope.non_tam_non_sop_bengkel.data[sv6].TotalAmount = Math.round(Math.round(($scope.non_tam_non_sop_bengkel.data[sv6].AmountAestimationBP/1.1)* $scope.non_tam_non_sop_bengkel.data[sv6].QtyPO) - $scope.non_tam_non_sop_bengkel.data[sv6].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[sv6].VATAmount);
                                }else{
                                    $scope.non_tam_non_sop_bengkel.data[sv6].TotalAmount = Math.round(Math.round($scope.non_tam_non_sop_bengkel.data[sv6].newUnitPrice * $scope.non_tam_non_sop_bengkel.data[sv6].QtyPO) - $scope.non_tam_non_sop_bengkel.data[sv6].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[sv6].VATAmount);
                                }
                                
                            }else{
                               
                                $scope.non_tam_non_sop_bengkel.data[sv6].TotalAmount = Math.round(($scope.non_tam_non_sop_bengkel.data[sv6].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv6].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[sv6].VATAmount);
                            }
							
							//$scope.non_tam_non_sop_bengkel.data[sv6].TotalAmount = Math.round(($scope.non_tam_non_sop_bengkel.data[sv6].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv6].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv6].DiscountAmount);
						}
					}
					var svTotalAmount6 = 0;
					for (var si6 = 0; si6 < $scope.non_tam_non_sop_bengkel.data.length; si6++) {
						svTotalAmount6 = svTotalAmount6 + $scope.non_tam_non_sop_bengkel.data[si6].TotalAmount;
						$scope.mData.TotalAmount = svTotalAmount6;
					}
				}else if (($scope.crudState === '124') || ($scope.crudState === '134')) {
					for (var sv7 in $scope.non_tam_non_sop_bengkel.data) {
						if($scope.non_tam_non_sop_bengkel.data[sv7].QtyPO){
							//var HargaSatuanSV5 = $scope.non_tam_non_sop_bengkel.data[sv7].StandardCost / (1.1);
							//var DiscountSatuanSV5 = ($scope.non_tam_non_sop_bengkel.data[sv7].DiscountPercent / 100) * HargaSatuanSV5;
							//$scope.non_tam_non_sop_bengkel.data[sv7].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[sv7].QtyPO * DiscountSatuanSV5.toFixed($scope.roundTo);
							// CR5 #10
							console.log('$scope.mData.IsNONPKP at non_tam_non_sop_bengkel ($scope.crudState === 124) || ($scope.crudState === 134) stv: ', $scope.mData.IsNONPKP);
							if($scope.mData.IsNONPKP == false){
								//PKP
								// var HargaSatuanSV5 = $scope.non_tam_non_sop_bengkel.data[sv7].StandardCost / (1.1);
								var HargaSatuanSV5 = $scope.non_tam_non_sop_bengkel.data[sv7].StandardCost / (1+(PPNPerc/100));

								var DiscountSatuanSV5 = ($scope.non_tam_non_sop_bengkel.data[sv7].DiscountPercent / 100) * HargaSatuanSV5;
								$scope.non_tam_non_sop_bengkel.data[sv7].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[sv7].QtyPO * DiscountSatuanSV5.toFixed($scope.roundTo);

								// var VATSatuanSV5 = (HargaSatuanSV5 - DiscountSatuanSV5) * 0.1;
								var VATSatuanSV5 = (HargaSatuanSV5 - DiscountSatuanSV5) * (PPNPerc/100);

								$scope.non_tam_non_sop_bengkel.data[sv7].VATAmount = Math.round($scope.non_tam_non_sop_bengkel.data[sv7].QtyPO * VATSatuanSV5.toFixed($scope.roundTo));

								// $scope.non_tam_non_sop_bengkel.data[sv7].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv7].StandardCost / 1.1);
								$scope.non_tam_non_sop_bengkel.data[sv7].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv7].StandardCost / (1+(PPNPerc/100)));

							}else if($scope.mData.IsNONPKP == true){
								//Non PKP
								var HargaSatuanSV5 = $scope.non_tam_non_sop_bengkel.data[sv7].StandardCost;
								var DiscountSatuanSV5 = ($scope.non_tam_non_sop_bengkel.data[sv7].DiscountPercent / 100) * HargaSatuanSV5;
								$scope.non_tam_non_sop_bengkel.data[sv7].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[sv7].QtyPO * DiscountSatuanSV5.toFixed($scope.roundTo);
								$scope.non_tam_non_sop_bengkel.data[sv7].VATAmount = 0;
								$scope.non_tam_non_sop_bengkel.data[sv7].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv7].StandardCost);
							}
							// CR5 #10
							/*if($scope.mData.NoEstimasi == null || $scope.mData.NoEstimasi == undefined || $scope.mData.NoEstimasi == ''){
								$scope.non_tam_non_sop_bengkel.data[sv7].TotalAmount = Math.round(($scope.non_tam_non_sop_bengkel.data[sv7].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv7].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv7].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[sv7].VATAmount);
							}else{
								if($scope.mData.IsNONPKP == false){
									//
								}else if($scope.mData.IsNONPKP == true){
									$scope.non_tam_non_sop_bengkel.data[sv7].UnitPrice = $scope.non_tam_non_sop_bengkel.data[sv7].UnitPrice / 1.1;
								}
								$scope.non_tam_non_sop_bengkel.data[sv7].TotalAmount = Math.round(($scope.non_tam_non_sop_bengkel.data[sv7].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv7].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv7].DiscountAmount);
							}*/
							$scope.non_tam_non_sop_bengkel.data[sv7].TotalAmount = Math.round(($scope.non_tam_non_sop_bengkel.data[sv7].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv7].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv7].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[sv7].VATAmount);
							//$scope.non_tam_non_sop_bengkel.data[sv7].TotalAmount = Math.round(($scope.non_tam_non_sop_bengkel.data[sv7].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv7].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv7].DiscountAmount);
						}
					}
					var svTotalAmount7 = 0;
					for (var si7 = 0; si7 < $scope.non_tam_non_sop_bengkel.data.length; si7++) {
						svTotalAmount7 = svTotalAmount7 + $scope.non_tam_non_sop_bengkel.data[si7].TotalAmount;
						$scope.mData.TotalAmount = svTotalAmount7;
					}
				}else if ($scope.crudState === '1441') {
					for (var sv8 in $scope.non_tam_sop_so.data) {
						if($scope.non_tam_sop_so.data[sv8].QtyPO){
							//var HargaSatuanSV6 = $scope.non_tam_sop_so.data[sv8].StandardCost / (1.1);
							//var DiscountSatuanSV6 = Math.round((($scope.non_tam_sop_so.data[sv8].DiscountPercent / 100)) * HargaSatuanSV6);
							//$scope.non_tam_sop_so.data[sv8].DiscountAmount = $scope.non_tam_sop_so.data[sv8].QtyPO * DiscountSatuanSV6;
							// CR5 #10
							console.log('$scope.mData.IsNONPKP at non_tam_sop_so ($scope.crudState === 1441) stv: ', $scope.mData.IsNONPKP);
							if($scope.mData.IsNONPKP == false){
								//PKP
								// var HargaSatuanSV6 = $scope.non_tam_sop_so.data[sv8].StandardCost / (1.1);
								var HargaSatuanSV6 = $scope.non_tam_sop_so.data[sv8].StandardCost / (1+(PPNPerc/100));

								var DiscountSatuanSV6 = Math.round((($scope.non_tam_sop_so.data[sv8].DiscountPercent / 100)) * HargaSatuanSV6);
								$scope.non_tam_sop_so.data[sv8].DiscountAmount = $scope.non_tam_sop_so.data[sv8].QtyPO * DiscountSatuanSV6;

								// $scope.non_tam_sop_so.data[sv8].VATAmount = Math.round($scope.non_tam_sop_so.data[sv8].QtyPO * Math.round((HargaSatuanSV6 - DiscountSatuanSV6) * 0.1));
								$scope.non_tam_sop_so.data[sv8].VATAmount = Math.round($scope.non_tam_sop_so.data[sv8].QtyPO * Math.round((HargaSatuanSV6 - DiscountSatuanSV6) * (PPNPerc/100)));

								// $scope.non_tam_sop_so.data[sv8].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv8].StandardCost / 1.1);
								$scope.non_tam_sop_so.data[sv8].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv8].StandardCost / (1+(PPNPerc/100)));

							}else if($scope.mData.IsNONPKP == true){
								//Non PKP
								var HargaSatuanSV6 = $scope.non_tam_sop_so.data[sv8].StandardCost;
								var DiscountSatuanSV6 = Math.round((($scope.non_tam_sop_so.data[sv8].DiscountPercent / 100)) * HargaSatuanSV6);
								$scope.non_tam_sop_so.data[sv8].DiscountAmount = $scope.non_tam_sop_so.data[sv8].QtyPO * DiscountSatuanSV6;
								$scope.non_tam_sop_so.data[sv8].VATAmount = 0;
								$scope.non_tam_sop_so.data[sv8].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv8].StandardCost);
							}
							// CR5 #10
							/*if($scope.mData.NoEstimasi == null || $scope.mData.NoEstimasi == undefined || $scope.mData.NoEstimasi == ''){
								$scope.non_tam_sop_so.data[sv8].TotalAmount = ($scope.non_tam_sop_so.data[sv8].QtyPO * $scope.non_tam_sop_so.data[sv8].UnitPrice) - $scope.non_tam_sop_so.data[sv8].DiscountAmount + $scope.non_tam_sop_so.data[sv8].VATAmount;
							}else{
								if($scope.mData.IsNONPKP == false){
									//
								}else if($scope.mData.IsNONPKP == true){
									$scope.non_tam_sop_so.data[sv8].UnitPrice = $scope.non_tam_sop_so.data[sv8].UnitPrice / 1.1;
								}
								$scope.non_tam_sop_so.data[sv8].TotalAmount = ($scope.non_tam_sop_so.data[sv8].QtyPO * $scope.non_tam_sop_so.data[sv8].UnitPrice) - $scope.non_tam_sop_so.data[sv8].DiscountAmount;
							}*/
							$scope.non_tam_sop_so.data[sv8].TotalAmount = ($scope.non_tam_sop_so.data[sv8].QtyPO * $scope.non_tam_sop_so.data[sv8].UnitPrice) - $scope.non_tam_sop_so.data[sv8].DiscountAmount + $scope.non_tam_sop_so.data[sv8].VATAmount;
							//$scope.non_tam_sop_so.data[sv8].TotalAmount = ($scope.non_tam_sop_so.data[sv8].QtyPO * $scope.non_tam_sop_so.data[sv8].UnitPrice) - $scope.non_tam_sop_so.data[sv8].DiscountAmount;
						}
					}
					var svTotalAmount8 = 0;
					for (var si8 = 0; si8 < $scope.non_tam_sop_so.data.length; si8++) {
						svTotalAmount8 = svTotalAmount8 + $scope.non_tam_sop_so.data[si8].TotalAmount;
						$scope.mData.TotalAmount = svTotalAmount8;
					}
				}else if ($scope.crudState === '1442') {
					for (var sv9 in $scope.non_tam_sop_so.data) {
						if($scope.non_tam_sop_so.data[sv9].QtyPO){
							//var HargaSatuanSV7 = $scope.non_tam_sop_so.data[sv9].StandardCost / (1.1);
							//var DiscountSatuanSV7 = ($scope.non_tam_sop_so.data[sv9].DiscountPercent / 100) * HargaSatuanSV7;
							//$scope.non_tam_sop_so.data[sv9].DiscountAmount = $scope.non_tam_sop_so.data[sv9].QtyPO * DiscountSatuanSV7.toFixed($scope.roundTo);
							// CR5 #10
							console.log('$scope.mData.IsNONPKP at non_tam_sop_so ($scope.crudState === 1442) stv: ', $scope.mData.IsNONPKP);
							if($scope.mData.IsNONPKP == false){
								//PKP
								// var HargaSatuanSV7 = $scope.non_tam_sop_so.data[sv9].StandardCost / (1.1);
								var HargaSatuanSV7 = $scope.non_tam_sop_so.data[sv9].StandardCost / (1+(PPNPerc/100));

								var DiscountSatuanSV7 = ($scope.non_tam_sop_so.data[sv9].DiscountPercent / 100) * HargaSatuanSV7;
								$scope.non_tam_sop_so.data[sv9].DiscountAmount = $scope.non_tam_sop_so.data[sv9].QtyPO * DiscountSatuanSV7.toFixed($scope.roundTo);

								// var VATSatuanSV7 = (HargaSatuanSV7 - DiscountSatuanSV7) * 0.1;
								var VATSatuanSV7 = (HargaSatuanSV7 - DiscountSatuanSV7) * (PPNPerc/100);

								$scope.non_tam_sop_so.data[sv9].VATAmount = Math.round($scope.non_tam_sop_so.data[sv9].QtyPO * VATSatuanSV7.toFixed($scope.roundTo));

								// $scope.non_tam_sop_so.data[sv9].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv9].StandardCost / 1.1);
								$scope.non_tam_sop_so.data[sv9].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv9].StandardCost / (1+(PPNPerc/100)));

							}else if($scope.mData.IsNONPKP == true){
								//Non PKP
								var HargaSatuanSV7 = $scope.non_tam_sop_so.data[sv9].StandardCost;
								var DiscountSatuanSV7 = ($scope.non_tam_sop_so.data[sv9].DiscountPercent / 100) * HargaSatuanSV7;
								$scope.non_tam_sop_so.data[sv9].DiscountAmount = $scope.non_tam_sop_so.data[sv9].QtyPO * DiscountSatuanSV7.toFixed($scope.roundTo);
								$scope.non_tam_sop_so.data[sv9].VATAmount = 0;
								$scope.non_tam_sop_so.data[sv9].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv9].StandardCost);
							}
							// CR5 #10
							/*if($scope.mData.NoEstimasi == null || $scope.mData.NoEstimasi == undefined || $scope.mData.NoEstimasi == ''){
								$scope.non_tam_sop_so.data[sv9].TotalAmount = Math.round(($scope.non_tam_sop_so.data[sv9].QtyPO * $scope.non_tam_sop_so.data[sv9].UnitPrice) - $scope.non_tam_sop_so.data[sv9].DiscountAmount + $scope.non_tam_sop_so.data[sv9].VATAmount);
							}else{
								if($scope.mData.IsNONPKP == false){
									//
								}else if($scope.mData.IsNONPKP == true){
									$scope.non_tam_sop_so.data[sv9].UnitPrice = $scope.non_tam_sop_so.data[sv9].UnitPrice / 1.1;
								}
								$scope.non_tam_sop_so.data[sv9].TotalAmount = Math.round(($scope.non_tam_sop_so.data[sv9].QtyPO * $scope.non_tam_sop_so.data[sv9].UnitPrice) - $scope.non_tam_sop_so.data[sv9].DiscountAmount);
							}*/
							$scope.non_tam_sop_so.data[sv9].TotalAmount = Math.round(($scope.non_tam_sop_so.data[sv9].QtyPO * $scope.non_tam_sop_so.data[sv9].UnitPrice) - $scope.non_tam_sop_so.data[sv9].DiscountAmount + $scope.non_tam_sop_so.data[sv9].VATAmount);
							//$scope.non_tam_sop_so.data[sv9].TotalAmount = Math.round(($scope.non_tam_sop_so.data[sv9].QtyPO * $scope.non_tam_sop_so.data[sv9].UnitPrice) - $scope.non_tam_sop_so.data[sv9].DiscountAmount);
						}
					}
					var svTotalAmount9 = 0;
					for (var si9 = 0; si9 < $scope.non_tam_sop_so.data.length; si9++) {
						svTotalAmount9 = svTotalAmount9 + $scope.non_tam_sop_so.data[si9].TotalAmount;
						$scope.mData.TotalAmount = svTotalAmount9;
					}
				}else if ($scope.crudState === '314') {
					for (var sv10 in $scope.non_tam_non_sop_bengkel.data) {
						if($scope.non_tam_non_sop_bengkel.data[sv10].QtyPO){
							//var HargaSatuanSV8 = $scope.non_tam_non_sop_bengkel.data[sv10].StandardCost / (1.1);
							//var DiscountSatuanSV8 = Math.round((($scope.non_tam_non_sop_bengkel.data[sv10].DiscountPercent / 100)) * HargaSatuanSV8);
							//$scope.non_tam_non_sop_bengkel.data[sv10].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[sv10].QtyPO * DiscountSatuanSV8;
							// CR5 #10
							console.log('$scope.mData.IsNONPKP at non_tam_non_sop_bengkel ($scope.crudState === 314) stv : ', $scope.mData.IsNONPKP);
							if($scope.mData.IsNONPKP == false){
								//PKP
								// var HargaSatuanSV8 = $scope.non_tam_non_sop_bengkel.data[sv10].StandardCost / (1.1);
								var HargaSatuanSV8 = $scope.non_tam_non_sop_bengkel.data[sv10].StandardCost / (1+(PPNPerc/100));

								var DiscountSatuanSV8 = Math.round((($scope.non_tam_non_sop_bengkel.data[sv10].DiscountPercent / 100)) * HargaSatuanSV8);
								$scope.non_tam_non_sop_bengkel.data[sv10].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[sv10].QtyPO * DiscountSatuanSV8;

								// $scope.non_tam_non_sop_bengkel.data[sv10].VATAmount = Math.round($scope.non_tam_non_sop_bengkel.data[sv10].QtyPO * Math.round((HargaSatuanSV8 - DiscountSatuanSV8) * 0.1));
								$scope.non_tam_non_sop_bengkel.data[sv10].VATAmount = Math.round($scope.non_tam_non_sop_bengkel.data[sv10].QtyPO * Math.round((HargaSatuanSV8 - DiscountSatuanSV8) * (PPNPerc/100)));

								// $scope.non_tam_non_sop_bengkel.data[sv10].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv10].StandardCost / 1.1);
								$scope.non_tam_non_sop_bengkel.data[sv10].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv10].StandardCost / (1+(PPNPerc/100)));

							}else if($scope.mData.IsNONPKP == true){
								//Non PKP
								var HargaSatuanSV8 = $scope.non_tam_non_sop_bengkel.data[sv10].StandardCost;
								var DiscountSatuanSV8 = Math.round((($scope.non_tam_non_sop_bengkel.data[sv10].DiscountPercent / 100)) * HargaSatuanSV8);
								$scope.non_tam_non_sop_bengkel.data[sv10].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[sv10].QtyPO * DiscountSatuanSV8;
								$scope.non_tam_non_sop_bengkel.data[sv10].VATAmount = 0;
								$scope.non_tam_non_sop_bengkel.data[sv10].UnitPrice = Math.round($scope.non_tam_non_sop_bengkel.data[sv10].StandardCost);
							}
							// CR5 #10
							/*if($scope.mData.NoEstimasi == null || $scope.mData.NoEstimasi == undefined || $scope.mData.NoEstimasi == ''){
								$scope.non_tam_non_sop_bengkel.data[sv10].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[sv10].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv10].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv10].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[sv10].VATAmount;
							}else{
								if($scope.mData.IsNONPKP == false){
									//
								}else if($scope.mData.IsNONPKP == true){
									$scope.non_tam_non_sop_bengkel.data[sv10].UnitPrice = $scope.non_tam_non_sop_bengkel.data[sv10].UnitPrice / 1.1;
								}
								$scope.non_tam_non_sop_bengkel.data[sv10].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[sv10].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv10].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv10].DiscountAmount;
							}*/
							$scope.non_tam_non_sop_bengkel.data[sv10].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[sv10].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv10].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv10].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[sv10].VATAmount;
							//$scope.non_tam_non_sop_bengkel.data[sv10].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[sv10].QtyPO * $scope.non_tam_non_sop_bengkel.data[sv10].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[sv10].DiscountAmount;
						}
					}
					var svTotalAmount10 = 0;
					for (var si10 = 0; si10 < $scope.non_tam_non_sop_bengkel.data.length; si10++) {
						svTotalAmount10 = svTotalAmount10 + $scope.non_tam_non_sop_bengkel.data[si10].TotalAmount;
						$scope.mData.TotalAmount = svTotalAmount10;
					}
				}else if ($scope.crudState === '313') {
					for (var sv11 in $scope.non_tam_sop_so_ubah.data.data) {
						if($scope.non_tam_sop_so_ubah.data[sv11].QtyPO){
							//var HargaSatuanSV9 = $scope.non_tam_sop_so_ubah.data[sv11].StandardCost / (1.1);
							//var DiscountSatuanSV9 = Math.round((($scope.non_tam_sop_so_ubah.data[sv11].DiscountPercent / 100)) * HargaSatuanSV9);
							//$scope.non_tam_sop_so_ubah.data[sv11].DiscountAmount = $scope.non_tam_sop_so_ubah.data[sv11].QtyPO * DiscountSatuanSV9;
							// CR5 #10
							console.log('$scope.mData.IsNONPKP at non_tam_sop_so_ubah ($scope.crudState === 313) stv: ', $scope.mData.IsNONPKP);
							if($scope.mData.IsNONPKP == false){
								//PKP
								// var HargaSatuanSV9 = $scope.non_tam_sop_so_ubah.data[sv11].StandardCost / (1.1);
								var HargaSatuanSV9 = $scope.non_tam_sop_so_ubah.data[sv11].StandardCost / (1+(PPNPerc/100));

								var DiscountSatuanSV9 = Math.round((($scope.non_tam_sop_so_ubah.data[sv11].DiscountPercent / 100)) * HargaSatuanSV9);
								$scope.non_tam_sop_so_ubah.data[sv11].DiscountAmount = $scope.non_tam_sop_so_ubah.data[sv11].QtyPO * DiscountSatuanSV9;

								// $scope.non_tam_sop_so_ubah.data[sv11].VATAmount = Math.round($scope.non_tam_sop_so_ubah.data[sv11].QtyPO * Math.round((HargaSatuanSV9 - DiscountSatuanSV9) * 0.1));
								$scope.non_tam_sop_so_ubah.data[sv11].VATAmount = Math.round($scope.non_tam_sop_so_ubah.data[sv11].QtyPO * Math.round((HargaSatuanSV9 - DiscountSatuanSV9) * (PPNPerc/100)));

								// $scope.non_tam_sop_so_ubah.data[sv11].UnitPrice = Math.round($scope.non_tam_sop_so_ubah.data[sv11].StandardCost / 1.1);
								$scope.non_tam_sop_so_ubah.data[sv11].UnitPrice = Math.round($scope.non_tam_sop_so_ubah.data[sv11].StandardCost / (1+(PPNPerc/100)));

							}else if($scope.mData.IsNONPKP == true){
								//Non PKP
								var HargaSatuanSV9 = $scope.non_tam_sop_so_ubah.data[sv11].StandardCost;
								var DiscountSatuanSV9 = Math.round((($scope.non_tam_sop_so_ubah.data[sv11].DiscountPercent / 100)) * HargaSatuanSV9);
								$scope.non_tam_sop_so_ubah.data[sv11].DiscountAmount = $scope.non_tam_sop_so_ubah.data[sv11].QtyPO * DiscountSatuanSV9;
								$scope.non_tam_sop_so_ubah.data[sv11].VATAmount = 0;
								$scope.non_tam_sop_so_ubah.data[sv11].UnitPrice = Math.round($scope.non_tam_sop_so_ubah.data[sv11].StandardCost);
							}
							// CR5 #10
							/*if($scope.mData.NoEstimasi == null || $scope.mData.NoEstimasi == undefined || $scope.mData.NoEstimasi == ''){
								$scope.non_tam_sop_so_ubah.data[sv11].TotalAmount = ($scope.non_tam_sop_so_ubah.data[sv11].QtyPO * $scope.non_tam_sop_so_ubah.data[sv11].UnitPrice) - $scope.non_tam_sop_so_ubah.data[sv11].DiscountAmount + $scope.non_tam_sop_so_ubah.data[sv11].VATAmount;
							}else{
								if($scope.mData.IsNONPKP == false){
									//
								}else if($scope.mData.IsNONPKP == true){
									$scope.non_tam_sop_so_ubah.data[sv11].UnitPrice = $scope.non_tam_sop_so_ubah.data[sv11].UnitPrice / 1.1;
								}
								$scope.non_tam_sop_so_ubah.data[sv11].TotalAmount = ($scope.non_tam_sop_so_ubah.data[sv11].QtyPO * $scope.non_tam_sop_so_ubah.data[sv11].UnitPrice) - $scope.non_tam_sop_so_ubah.data[sv11].DiscountAmount;
							}*/
							$scope.non_tam_sop_so_ubah.data[sv11].TotalAmount = ($scope.non_tam_sop_so_ubah.data[sv11].QtyPO * $scope.non_tam_sop_so_ubah.data[sv11].UnitPrice) - $scope.non_tam_sop_so_ubah.data[sv11].DiscountAmount + $scope.non_tam_sop_so_ubah.data[sv11].VATAmount;
							//$scope.non_tam_sop_so_ubah.data[sv11].TotalAmount = ($scope.non_tam_sop_so_ubah.data[sv11].QtyPO * $scope.non_tam_sop_so_ubah.data[sv11].UnitPrice) - $scope.non_tam_sop_so_ubah.data[sv11].DiscountAmount;
						}
					}
					var svTotalAmount11 = 0;
					for (var si11 = 0; si11 < $scope.non_tam_sop_so_ubah.data.length; si11++) {
						svTotalAmount11 = svTotalAmount11 + $scope.non_tam_sop_so_ubah.data[si11].TotalAmount;
						$scope.mData.TotalAmount = svTotalAmount11;
					}
				}else if ($scope.crudState === '143') {
					for (var sv11 in $scope.non_tam_sop_so.data) {
						if($scope.non_tam_sop_so.data[sv11].QtyPO){
							//var HargaSatuanSV9 = $scope.non_tam_sop_so.data[sv11].StandardCost / (1.1);
							//var DiscountSatuanSV9 = Math.round((($scope.non_tam_sop_so.data[sv11].DiscountPercent / 100)) * HargaSatuanSV9);
							//$scope.non_tam_sop_so.data[sv11].DiscountAmount = $scope.non_tam_sop_so.data[sv11].QtyPO * DiscountSatuanSV9;
							// CR5 #10
							console.log('$scope.mData.IsNONPKP at non_tam_sop_so ($scope.crudState === 143) stv: ', $scope.mData.IsNONPKP);
                            if($scope.mData.IsNONPKP == false){  
                                //PKP
                                if($scope.non_tam_sop_so.data[sv11].newUnitPrice == undefined){
        
                                    // var HargaSatuan    = $scope.non_tam_sop_so.data[sv11].StandardCost/1.1;
                                    var HargaSatuan    = $scope.non_tam_sop_so.data[sv11].StandardCost/(1+(PPNPerc/100));

                                    // $scope.non_tam_sop_so.data[sv11].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv11].StandardCost/1.1);
                                    $scope.non_tam_sop_so.data[sv11].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv11].StandardCost/(1+(PPNPerc/100)));

                                    $scope.non_tam_sop_so.data[sv11].UnitPriceClean = HargaSatuan.toFixed(2);
                                }else{
                                    
                                    var HargaSatuan    = $scope.non_tam_sop_so.data[sv11].newUnitPrice;
                                    $scope.non_tam_sop_so.data[sv11].UnitPrice = $scope.non_tam_sop_so.data[sv11].newUnitPrice;
                                }
                                // var HargaSatuan    = $scope.non_tam_sop_so.data[sv11].StandardCost/1.1;
                                // var DiscountSatuan = ($scope.non_tam_sop_so.data[sv11].DiscountPercent / 100) * HargaSatuan;
                                // $scope.non_tam_sop_so.data[sv11].DiscountAmount = parseFloat($scope.non_tam_sop_so.data[sv11].QtyPO * DiscountSatuan.toFixed($scope.roundTo)).toFixed();
                                $scope.non_tam_sop_so.data[sv11].DiscountAmount = (Math.round($scope.non_tam_sop_so.data[sv11].UnitPrice*$scope.non_tam_sop_so.data[sv11].DiscountPercent / 100))*$scope.non_tam_sop_so.data[sv11].QtyPO;
                                // var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
                                // var tmpVataAmount = $scope.non_tam_sop_so.data[sv11].QtyPO * VATSatuan;

                                // $scope.non_tam_sop_so.data[sv11].VATAmount = (Math.round(($scope.non_tam_sop_so.data[sv11].UnitPrice - $scope.non_tam_sop_so.data[sv11].DiscountAmount)*0.1))*$scope.non_tam_sop_so.data[sv11].QtyPO;
                                $scope.non_tam_sop_so.data[sv11].VATAmount = (Math.round(($scope.non_tam_sop_so.data[sv11].UnitPrice - $scope.non_tam_sop_so.data[sv11].DiscountAmount)*(PPNPerc/100)))*$scope.non_tam_sop_so.data[sv11].QtyPO;

                                // $scope.non_tam_sop_so.data[sv11].VATAmount = Math.round(tmpVataAmount);
                                // $scope.non_tam_sop_so.data[sv11].UnitPrice = parseFloat($scope.non_tam_sop_so.data[sv11].StandardCost/1.1).toFixed();
                                
                                // var HargaSatuanSV9 = ($scope.non_tam_sop_so.data[sv11].StandardCost / (1.1)).toFixed();
                                // var DiscountSatuanSV9 = Math.round((($scope.non_tam_sop_so.data[sv11].DiscountPercent / 100)) * HargaSatuanSV9);
                                // $scope.non_tam_sop_so.data[sv11].DiscountAmount = $scope.non_tam_sop_so.data[sv11].QtyPO * DiscountSatuanSV9;
                                // $scope.non_tam_sop_so.data[sv11].VATAmount =$scope.non_tam_sop_so.data[sv11].QtyPO *((HargaSatuanSV9 - DiscountSatuanSV9) * 0.1).toFixed();
                                // $scope.non_tam_sop_so.data[sv11].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv11].StandardCost / 1.1);
                            }else if($scope.mData.IsNONPKP == true){
                                //Non PKP
                                if($scope.non_tam_sop_so.data[sv11].newUnitPrice == undefined){
        
                                    var HargaSatuanSV9 = $scope.non_tam_sop_so.data[sv11].StandardCost;
                                    $scope.non_tam_sop_so.data[sv11].UnitPrice = Math.round($scope.non_tam_sop_so.data[sv11].StandardCost);
                                    $scope.non_tam_sop_so.data[sv11].UnitPriceClean = HargaSatuanSV9.toFixed(2);
                                }else{
                                    
                                    var HargaSatuanSV9 = $scope.non_tam_sop_so.data[sv11].newUnitPrice;
                                    $scope.non_tam_sop_so.data[sv11].UnitPrice = $scope.non_tam_sop_so.data[sv11].newUnitPrice;
                                }
                                // var HargaSatuanSV9 = $scope.non_tam_sop_so.data[sv11].StandardCost;
                                // var DiscountSatuanSV9 = Math.round((($scope.non_tam_sop_so.data[sv11].DiscountPercent / 100)) * HargaSatuanSV9);
                                // $scope.non_tam_sop_so.data[sv11].DiscountAmount = $scope.non_tam_sop_so.data[sv11].QtyPO * DiscountSatuanSV9;
                                $scope.non_tam_sop_so.data[sv11].DiscountAmount = (Math.round($scope.non_tam_sop_so.data[sv11].UnitPrice*$scope.non_tam_sop_so.data[sv11].DiscountPercent / 100))*$scope.non_tam_sop_so.data[sv11].QtyPO;
                                $scope.non_tam_sop_so.data[sv11].VATAmount = 0;
                                // $scope.non_tam_sop_so.data[sv11].UnitPrice = $scope.non_tam_sop_so.data[sv11].StandardCost;
                            }
                            // if($scope.mData.IsNONPKP == false){
                            if($scope.non_tam_sop_so.data[sv11].newUnitPrice == undefined){
                                // $scope.non_tam_sop_so.data[sv11].TotalAmount =  Math.round(($scope.non_tam_sop_so.data[sv11].QtyPO * ($scope.non_tam_sop_so.data[sv11].StandardCost/1.1)) - $scope.non_tam_sop_so.data[sv11].DiscountAmount + ($scope.non_tam_sop_so.data[sv11].QtyPO * (HargaSatuan - DiscountSatuan) * 0.1));
                                $scope.non_tam_sop_so.data[sv11].TotalAmount =  (($scope.non_tam_sop_so.data[sv11].UnitPrice * $scope.non_tam_sop_so.data[sv11].QtyPO) - $scope.non_tam_sop_so.data[sv11].DiscountAmount)+$scope.non_tam_sop_so.data[sv11].VATAmount;
                            }else{
                                // $scope.non_tam_sop_so.data[sv11].TotalAmount =  Math.round(($scope.non_tam_sop_so.data[sv11].QtyPO * $scope.non_tam_sop_so.data[sv11].newUnitPrice) - $scope.non_tam_sop_so.data[sv11].DiscountAmount + ($scope.non_tam_sop_so.data[sv11].QtyPO * (HargaSatuan - DiscountSatuan) * 0.1));
                                $scope.non_tam_sop_so.data[sv11].TotalAmount =  Math.round(($scope.non_tam_sop_so.data[sv11].QtyPO * $scope.non_tam_sop_so.data[sv11].newUnitPrice) - $scope.non_tam_sop_so.data[sv11].DiscountAmount + ($scope.non_tam_sop_so.data[sv11].QtyPO * (HargaSatuan - DiscountSatuan) * (PPNPerc/100)));

                            }
                                // $scope.non_tam_sop_so.data[sv11].TotalAmount =  Math.round(($scope.non_tam_sop_so.data[sv11].QtyPO * ($scope.non_tam_sop_so.data[sv11].StandardCost/1.1)) - $scope.non_tam_sop_so.data[sv11].DiscountAmount + ($scope.non_tam_sop_so.data[sv11].QtyPO * (HargaSatuan - DiscountSatuan) * 0.1));
                            // }else{
                            //     $scope.non_tam_sop_so.data[sv11].TotalAmount = ($scope.non_tam_sop_so.data[sv11].QtyPO * $scope.non_tam_sop_so.data[sv11].UnitPrice) - $scope.non_tam_sop_so.data[sv11].DiscountAmount + $scope.non_tam_sop_so.data[sv11].VATAmount;
                            // }
							
							//$scope.non_tam_sop_so.data[sv11].TotalAmount = ($scope.non_tam_sop_so.data[sv11].QtyPO * $scope.non_tam_sop_so.data[sv11].UnitPrice) - $scope.non_tam_sop_so.data[sv11].DiscountAmount;
						}
					}
					var svTotalAmount11 = 0;
					for (var si11 = 0; si11 < $scope.non_tam_sop_so.data.length; si11++) {
						svTotalAmount11 = svTotalAmount11 + $scope.non_tam_sop_so.data[si11].TotalAmount;
						$scope.mData.TotalAmount = svTotalAmount11;
					}
				}
			}
			if ($scope.crudState === '141') {
				for (var svs in $scope.non_tam_sop_so.data) {
					if($scope.non_tam_sop_so.data[svs].QtyPO){
						//$scope.non_tam_sop_so.data[svs].DiscountAmount = Math.round((($scope.non_tam_sop_so.data[svs].DiscountPercent / 100)) * ($scope.non_tam_sop_so.data[svs].QtyPO * $scope.non_tam_sop_so.data[svs].UnitPrice));
						// CR5 #10
						console.log('$scope.mData.IsNONPKP at non_tam_sop_so stv: ', $scope.mData.IsNONPKP);
                        if($scope.mData.IsNONPKP == false){  
                            //PKP
                            if($scope.non_tam_sop_so.data[svs].newUnitPrice == undefined){
        
                                // var HargaSatuan    = Math.round($scope.non_tam_sop_so.data[svs].StandardCost/1.1);
                                var HargaSatuan    = Math.round($scope.non_tam_sop_so.data[svs].StandardCost/(1+(PPNPerc/100)));

                                // $scope.non_tam_sop_so.data[svs].UnitPrice = Math.round($scope.non_tam_sop_so.data[svs].StandardCost/1.1);
                                $scope.non_tam_sop_so.data[svs].UnitPrice = Math.round($scope.non_tam_sop_so.data[svs].StandardCost/(1+(PPNPerc/100)));

                                $scope.non_tam_sop_so.data[svs].UnitPriceClean = HargaSatuan.toFixed(2);
                            }else{
                                
                                var HargaSatuan    = $scope.non_tam_sop_so.data[svs].newUnitPrice;
                                $scope.non_tam_sop_so.data[svs].UnitPrice = Math.round($scope.non_tam_sop_so.data[svs].newUnitPrice);
                            }
                            // var HargaSatuan    = $scope.non_tam_sop_so.data[svs].StandardCost/1.1;
                            var DiscountSatuan = ($scope.non_tam_sop_so.data[svs].DiscountPercent / 100) * HargaSatuan;
                            // $scope.non_tam_sop_so.data[svs].DiscountAmount = parseFloat($scope.non_tam_sop_so.data[svs].QtyPO * DiscountSatuan.toFixed($scope.roundTo)).toFixed();
                            $scope.non_tam_sop_so.data[svs].DiscountAmount = (Math.round($scope.non_tam_sop_so.data[svs].UnitPrice * $scope.non_tam_sop_so.data[svs].DiscountPercent / 100))*$scope.non_tam_sop_so.data[svs].QtyPO;

                            // var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
                            var VATSatuan = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);

                            var tmpVataAmount = $scope.non_tam_sop_so.data[svs].QtyPO * VATSatuan;

                            // $scope.non_tam_sop_so.data[svs].VATAmount = Math.round(($scope.non_tam_sop_so.data[svs].UnitPrice-$scope.non_tam_sop_so.data[svs].DiscountAmount)*0.1)*$scope.non_tam_sop_so.data[svs].QtyPO;
                            $scope.non_tam_sop_so.data[svs].VATAmount = Math.round(($scope.non_tam_sop_so.data[svs].UnitPrice-$scope.non_tam_sop_so.data[svs].DiscountAmount)*(PPNPerc/100))*$scope.non_tam_sop_so.data[svs].QtyPO;

                            // $scope.non_tam_sop_so.data[svs].UnitPrice = parseFloat($scope.non_tam_sop_so.data[svs].StandardCost/1.1).toFixed();
                            
                            // var HargaSatuanSV9 = ($scope.non_tam_sop_so.data[svs].StandardCost / (1.1)).toFixed();
                            // var DiscountSatuanSV9 = Math.round((($scope.non_tam_sop_so.data[svs].DiscountPercent / 100)) * HargaSatuanSV9);
                            // $scope.non_tam_sop_so.data[svs].DiscountAmount = $scope.non_tam_sop_so.data[svs].QtyPO * DiscountSatuanSV9;
                            // $scope.non_tam_sop_so.data[svs].VATAmount =$scope.non_tam_sop_so.data[svs].QtyPO *((HargaSatuanSV9 - DiscountSatuanSV9) * 0.1).toFixed();
                            // $scope.non_tam_sop_so.data[svs].UnitPrice = Math.round($scope.non_tam_sop_so.data[svs].StandardCost / 1.1);
                        }else if($scope.mData.IsNONPKP == true){
                            //Non PKP
                            if($scope.non_tam_sop_so.data[svs].newUnitPrice == undefined){
        
                                var HargaSatuanSV9 = $scope.non_tam_sop_so.data[svs].StandardCost;
                                $scope.non_tam_sop_so.data[svs].UnitPrice = $scope.non_tam_sop_so.data[svs].StandardCost;
                                $scope.non_tam_sop_so.data[svs].UnitPriceClean = HargaSatuanSV9.toFixed(2);
                            }else{
                                
                                var HargaSatuanSV9 = $scope.non_tam_sop_so.data[svs].newUnitPrice;
                                $scope.non_tam_sop_so.data[svs].UnitPrice = Math.round($scope.non_tam_sop_so.data[svs].newUnitPrice);
                            }
                            // var HargaSatuanSV9 = $scope.non_tam_sop_so.data[svs].StandardCost;
                            var DiscountSatuanSV9 = Math.round((($scope.non_tam_sop_so.data[svs].DiscountPercent / 100)) * HargaSatuanSV9);
                            $scope.non_tam_sop_so.data[svs].DiscountAmount = $scope.non_tam_sop_so.data[svs].QtyPO * DiscountSatuanSV9;
                            $scope.non_tam_sop_so.data[svs].VATAmount = 0;
                            // $scope.non_tam_sop_so.data[svs].UnitPrice = $scope.non_tam_sop_so.data[svs].StandardCost;
                        }
                        if($scope.mData.IsNONPKP == false){
                            if($scope.non_tam_sop_so.data[svs].newUnitPrice == undefined){
                                $scope.non_tam_sop_so.data[svs].TotalAmount =  (($scope.non_tam_sop_so.data[svs].UnitPrice * $scope.non_tam_sop_so.data[svs].QtyPO) - $scope.non_tam_sop_so.data[svs].DiscountAmount)+$scope.non_tam_sop_so.data[svs].VATAmount;
                            }else{
                                // $scope.non_tam_sop_so.data[svs].TotalAmount =  Math.round(($scope.non_tam_sop_so.data[svs].QtyPO * $scope.non_tam_sop_so.data[svs].newUnitPrice) - $scope.non_tam_sop_so.data[svs].DiscountAmount + ($scope.non_tam_sop_so.data[svs].QtyPO * (HargaSatuan - DiscountSatuan) * 0.1));
                                $scope.non_tam_sop_so.data[svs].TotalAmount =  Math.round(($scope.non_tam_sop_so.data[svs].QtyPO * $scope.non_tam_sop_so.data[svs].newUnitPrice) - $scope.non_tam_sop_so.data[svs].DiscountAmount + ($scope.non_tam_sop_so.data[svs].QtyPO * (HargaSatuan - DiscountSatuan) * (PPNPerc/100)));

                            }
                            // $scope.non_tam_sop_so.data[svs].TotalAmount =  Math.round(($scope.non_tam_sop_so.data[svs].QtyPO * ($scope.non_tam_sop_so.data[svs].StandardCost/1.1)) - $scope.non_tam_sop_so.data[svs].DiscountAmount + ($scope.non_tam_sop_so.data[svs].QtyPO * (HargaSatuan - DiscountSatuan) * 0.1));
                        }else{
                            $scope.non_tam_sop_so.data[svs].TotalAmount = ($scope.non_tam_sop_so.data[svs].QtyPO * $scope.non_tam_sop_so.data[svs].UnitPrice) - $scope.non_tam_sop_so.data[svs].DiscountAmount + $scope.non_tam_sop_so.data[svs].VATAmount;
                        }
                        //$scope.non_tam_sop_so.data[sv].TotalAmount = ($scope.non_tam_sop_so.data[sv].QtyPO * $scope.non_tam_sop_so.data[sv].UnitPrice) - $scope.non_tam_sop_so.data[sv].DiscountAmount;
					}
				}
				var svTotalAmounts = 0;
				for (var sis = 0; sis < $scope.non_tam_sop_so.data.length; sis++) {
					svTotalAmounts = svTotalAmounts + $scope.non_tam_sop_so.data[sis].TotalAmount;
					$scope.mData.TotalAmount = svTotalAmounts;
				}
			}
			// CR5 #10
        };

        $scope.selectPaymentMethodId = function(item) {
            console.log("bsselect payment ", item);
        };

        $scope.localeDate = function(data) {
            var tmpDate = new Date(data);
            var resDate = new Date(tmpDate.toISOString().replace("Z","-0700")).toISOString(); 
            return resDate;
        }
        
        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.gridDataTree = [];
        $scope.getData = function() {
            console.log("GET DATA_________==============");
            if ($scope.Filter.POTypeId == null && $scope.Filter.HasilSOQ == null && $scope.Filter.LicensePlate == null && $scope.Filter.PurchaseOrderNo == null &&
                $scope.Filter.PurchaseOrderStatusId == null && $scope.Filter.RefPONo == null && $scope.Filter.RefPOTypeId == null && $scope.Filter.VendorName == null &&
                $scope.Filter.startDate == null && $scope.Filter.endDate == null && $scope.Filter.PartsCode == null) {
                if (loadedContent) {
                    bsNotify.show({
                        title: "Purchase Order",
                        content: "Filter tidak boleh kosong",
                        type: 'danger'
                    });
                }
            } else if ($scope.Filter.startDate == null && $scope.Filter.endDate == null && $scope.Filter.POTypeId !== null && $scope.checkRights(4) == false) {
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Filter Tipe PO dan Tanggal harus diisi bersamaan",
                    type: 'danger'
                });
            } else {
                PurchaseOrder.getData($scope.Filter).then(function(res) {
                        var gridData = res.data.Result;
                        console.log("gridData==>",gridData);
                        // added by sss on 2017-12-20
                        for (var i in gridData) {
                            gridData[i]["DocDate"] = setDate(gridData[i]["DocDate"]);
                            gridData[i]["RequiredDate"] = setDate(gridData[i]["RequiredDate"]);
                            gridData[i]["CreatedDate"] = $scope.localeDate(gridData[i]["CreatedDate"]);

							// CR5 #10
							/*$scope.mData.IsNONPKP = '';
							$scope.mData.IsNONPKP = gridData[i]["IsNonPKP"];*/
							// CR5 #10

                            // if(gridData[i].PurchaseOrderStatusId == 1 ){
                            //     gridData[i].PurchaseOrderNo = '-';
                            // }
                            console.log("gridData i =>",i);

                            // convert data id jadi text -------------------- start
                            
                            gridData[i].xMaterialTypeId = $scope.ToNameMaterial(gridData[i].MaterialTypeId)
                            gridData[i].xPOTypeId = $scope.ToNameTypePo(gridData[i].POTypeId, gridData[i].CampaignId)
                            gridData[i].xRefPOTypeId = $scope.ToNameRefPo(gridData[i].RefPOTypeId)
                            gridData[i].xBackOrderFollowId = $scope.ToNameBoTam(gridData[i].BackOrderFollowId)
                            gridData[i].xPurchaseOrderStatusId = $scope.ToNameStatus(gridData[i].PurchaseOrderStatusId)
                            gridData[i].xStatusTPOS = $scope.ToNameStatusTPOS(gridData[i].POTypeId,gridData[i].StatusTPOS)





                            
                            // convert data id jadi text -------------------- end

                        }
                        console.log("getData==>",gridData);
                        $scope.grid.data = gridData;
                        $scope.loading = false;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);

        }

        //------------------------------------------------------------
        $scope.formmode = { mode: '' };
        $scope.doGeneratePass = function() {
            $scope.mUser.password = Math.random().toString(36).slice(-8);
        }
        $scope.onBeforeNewMode = function() {
            //show gateway page
            // $scope.resetForm();
            $scope.mData.RoleId = $scope.user.RoleId;
            console.log("Percobaaan gue ", $scope.user.RoleId);
            $scope.isOverlayForm = true;
            //console.log($scope.user);
            $scope.mData.OutletId = $scope.user.OrgId;

            $scope.formode = 1;
            console.log("mode=>o", $scope.formmode);
            $scope.newUserMode = true;
            $scope.editMode = false;

            PurchaseOrder.getApprovalCondition().then(function(res) {
                    var gridData = res.data;

                    PurchaseOrder.MinTotalAmount = gridData[0].MinTotalAmount;
                    console.log("Min = ", PurchaseOrder.MinTotalAmount);
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            //$scope.mode='new';

            console.log('ApprovalProcessId', $scope.ApprovalProcessId);
            PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res) {
                    if(res.data.Result.length > 0){
                        var Approvers = res.data.Result[0].Approvers;
                        console.log("getApprover res = ", Approvers);
                        $scope.mData.Approvers = Approvers;
                    }
                    
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.resetForm = function() {
            //PurchaseOrder.formApi.setMode('grid');
            var data = { "data": [] };
            //$scope.mData={};
            //$scope.mData.GridDetail={};

            $scope.tam_sop_appointment.data = data.data;
            $scope.non_tam_sop_wo.data = data.data;
            $scope.tam_sop_wo.data = data.data;
            $scope.sublet_sop_wo.data = data.data;
            $scope.non_tam_sop_so.data = data.data;
            $scope.non_tam_non_sop_bengkel.data = data.data;
            $scope.non_tam_non_sop.data = data.data;

            console.log("$scope.tam_sop_appointment=>",$scope.tam_sop_appointment);
            console.log("$scope.non_tam_sop_wo=>",$scope.non_tam_sop_wo);
            console.log("$scope.tam_sop_wo=>",$scope.tam_sop_wo);
            console.log("$scope.sublet_sop_wo=>",$scope.sublet_sop_wo);
            console.log("$scope.non_tam_sop_so=>",$scope.non_tam_sop_so);
            console.log("$scope.non_tam_non_sop_bengkel=>",$scope.non_tam_non_sop_bengkel);
            console.log("$scope.non_tam_non_sop=>",$scope.non_tam_non_sop);
        };

        $scope.onBeforeEditMode = function(row) {
            if (row.StatusTPOS == 1) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "PO ini tidak dapat diedit, Status TPOS sedang diproses",
                });
                $scope.formApi.setMode('grid');
                // return -1;
            }else{
                $scope.formode = 2;
                //$scope.newUserMode=false;
                //console.log("mode=>",$scope.formmode);
                //$scope.editMode=true;
                // var editnum = '3'

                // //pemilihan form edit
                // $scope.isEditState = true;
                // console.log("[EDIT] " + $scope.mData.POTypeId + " " + $scope.mData.RefPOTypeId);

                // $scope.crudState = editnum+$scope.formShowHandler($scope.mData.POTypeId, $scope.mData.RefPOTypeId);

                // console.log("[EDIT] " + $scope.crudState);
            }
        }
        $scope.onBeforeDeleteMode = function() {
            $scope.formode = 3;
            console.log("mode=>i", $scope.formmode);
            //$scope.mode='del';
        }


        PartsCurrentUser.getVendor().then(function(res) {
            console.log("List Vendor0====>", res);
            $scope.vendorData = res.data;
        });

        

        $scope.PreShowDetail = function(row, mode) {
            var defer = $q.defer();
            //$scope.formode = 3;
            console.log("mode=>e", mode);
            //$scope.mode='del';
            $scope.mData = row;
			
			// CR5 #10
			console.log("row stv",row);
			if(row.IsNonPKP == 0){
				$scope.mData.IsNONPKP = false;
			}else if(row.IsNonPKP == 1){
				$scope.mData.IsNONPKP = true;
			}
			// CR5 #10

            $scope.mData.NamaVendor = ""
            $scope.mData.Telephone = ""
            $scope.mData.Alamat = ""


            console.log("row.Notes",row.Notes);
            $scope.mData.Notes = row.Notes;
            console.log("mode=>oi", $scope.mData);
            // if ($scope.mData.BackOrderFollowId == 9){
            //     document.getElementById("myBtn").disabled = true;
            //     console.log("Masuk sini gak", $scope.mData);
            // }
            console.log("Tipe ", $scope.mData.POTypeId);
            console.log("Referensi ", $scope.mData.RefPOTypeId);
            var detail = 2;

            if (mode != "new") {
                $scope.State = 2;
            }

            //Tentukan tipw PO
            //$scope.menuPOtype = ToPOTypeId;
            $scope.menuPOtype = row.POTypeId;
            $scope.menuPOref = row.RefPOTypeId;
            $scope.BoTam = [{ Id: 1, Name: "Kill" }, { Id: 9, Name: "Fill" }];
            console.log("menuPOtype", $scope.menuPOtype);
            console.log("menuPOtype", $scope.menuPOtype);
            if ((($scope.menuPOtype == 1) || ($scope.menuPOtype == '1') || ($scope.menuPOtype == 2) || ($scope.menuPOtype == '2') || ($scope.menuPOtype == 3) || ($scope.menuPOtype == '3') 
                || ($scope.menuPOtype == 6) || ($scope.menuPOtype == '6') || ($scope.menuPOtype == 7) || ($scope.menuPOtype == '7'))) {
                $scope.TAMenabled = true;
                $scope.labelTAM = 'Simpan dan Order ke TAM';
                $scope.isTAM = true;
            } else {
                $scope.labelTAM = 'Simpan';
                $scope.isTAM = false;
            }

            PartsCurrentUser.getVendor().then(function(res) {
                console.log("List Vendor1====>", res);
                $scope.vendorData = res.data;
            });

            var statedetail = $scope.formShowHandler($scope.menuPOtype, $scope.menuPOref);
            console.log("two cents", statedetail);
            // || (statedetail === '51')
            if ((statedetail === '11') || (statedetail === '21') || (statedetail === '31') || (statedetail === '41') ) {
                console.log('apa');
                // [START] Get Search Appointment
                PurchaseOrder.searchReference(row.PurchaseOrderId, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail).then(function(res) {
                    $scope.SumTotalAmount();
                        var AppointmentData = res.data[0];
                        AppointmentData.CreatedDate = $scope.localeDate(AppointmentData.CreatedDate);
                        console.log("searchReference AppointmentData==>",res);
                        console.log("searchReference AppointmentData[0]==>",AppointmentData);

                        //masukkan ke dalam model
                        $scope.mData.ColorId = AppointmentData.ColorId;
                        $scope.mData.MachineNo = AppointmentData.VIN;
                        $scope.mData.NoPol = AppointmentData.NoPol;
                        $scope.mData.Year = AppointmentData.Year;
                        $scope.mData.VehicleId = AppointmentData.VehicleId;
                        $scope.mData.VehicleModelId = AppointmentData.VehicleModelId;
                        $scope.mData.FullModel = AppointmentData.FullModel;
                        $scope.mData.VehicleColourCode = AppointmentData.VehicleColourCode;
                        $scope.mData.ModelName = AppointmentData.ModelName;
                        $scope.mData.Name = AppointmentData.Name;
                        $scope.mData.FrameNo = AppointmentData.FrameNo;
                        $scope.mData.AppointmentDate = AppointmentData.AppointmentDate;
                        $scope.mData.AppointmentTime = AppointmentData.AppointmentTime;
                        $scope.mData.AppointmentNo = AppointmentData.AppointmentNo;
                        $scope.mData.CreatedDate = AppointmentData.CreatedDate;
                        $scope.mData.CreatedUserId = AppointmentData.CreatedUserId;
                        $scope.mData.BackOrderFollowId = AppointmentData.BackOrderFollowId;
                        $scope.mData.Notes = AppointmentData.Notes;
                        //Tire
                        if($scope.mData.POTypeId == 6){
                            $scope.mData.TireOrderId = AppointmentData.TireOrderId;
                            $scope.mData.NecessaryDate = AppointmentData.NecessaryDate;
                            var tempSplitTime = AppointmentData.NecessaryTime.split(':');
    
                            var tempTime = {    
                                value: new Date($scope.mData.NecessaryDate.getFullYear(), $scope.mData.NecessaryDate.getMonth(), 
                                $scope.mData.NecessaryDate.getDate(), tempSplitTime[0], 
                                tempSplitTime[1], 0)
                                };
                            $scope.mData.NecessaryTime=tempTime.value;
                        }
                        // $scope.mData.NecessaryTime = AppointmentData.NecessaryTime;

                        // $scope.mData.POTypeId = AppointmentData.POTypeId;
                        // $scope.mData.RefPOTypeId = AppointmentData.RefPOTypeId;


                        //console.log("From Factory => ", PurchaseOrder.getPurchaseOrderHeader());

                    },
                    function(err) {
                        console.log("err=>", err);
                        defer.resolve(false);
                    }
                );
                // // [END] Get Search Appointment

                // [START] Get Search Appointment Grid Detail
                PurchaseOrder.searchReferenceDetail(row.PurchaseOrderId, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail, $scope.mData.MaterialTypeId, $scope.mData.VendorId, $scope.UserId).then(function(res) {
                        var AppointmentData = res.data;
                        console.log('searchReferenceDetail ==> Appa?',AppointmentData)
                        console.log(AppointmentData);
                        $scope.SumTotalAmount();
                        for (var i in AppointmentData) {
                            //rounding unitprice pada saat view
                            // AppointmentData[i].UnitPrice = Math.round(AppointmentData[i].UnitPrice);
                            if (AppointmentData[i].ETA != null) {
                                var eta = AppointmentData[i].ETA;
                                eta = new Date(eta);
                                eta = $filter('date')(eta, 'yyyy-MM-dd HH:MM');
                                // var etaa
                                // var yyyyeta = eta.getFullYear().toString();
                                // var mmeta = (eta.getMonth() + 1).toString(); // getMonth() is zero-based
                                // var ddeta = eta.getDate().toString();
                                // etaa = (ddeta[1] ? ddeta : "0" + ddeta[0]) + '/' + (mmeta[1] ? mmeta : "0" + mmeta[0]) + '/' + yyyyeta;
                                console.log("changeFormatDate finalDate", eta);
                                AppointmentData[i].ETA = eta;
                            } else {
                                AppointmentData[i].ETA = null;
                            }
                            if (AppointmentData[i].ETD != null) {
                                var etd = AppointmentData[i].ETD;
                                etd = new Date(etd);
                                etd = $filter('date')(etd, 'yyyy-MM-dd HH:MM');
                                console.log("changeFormatDate finalDate", etd);
                                AppointmentData[i].ETD = etd;
                            } else {
                                AppointmentData[i].ETD = null;
                            }

                            AppointmentData[i].CreatedDate = $scope.localeDate(AppointmentData[i].CreatedDate);
                            if (statedetail == '41') {
                                AppointmentData[i].ETD = '-';
                                AppointmentData[i].ETA = '-';    
                            }
                        }

                        $scope.tam_sop_appointment_lihat.data = AppointmentData;
                        $scope.tam_sop_appointment_ubah.data = AppointmentData;
                        $scope.mData.GridDetail = AppointmentData;
                        $scope.SumTotalAmount();
                        
                        console.log("ini $scope.mData==>",$scope.mData);
                        console.log("ini $scope.crudState",$scope.crudState);
                        if ($scope.crudState === '341' || $scope.crudState === '241') {
                            $scope.non_tam_sop_so.data = $scope.mData.GridDetail;
                            console.log("okkk" , AppointmentData);
                            console.log("next to you" , $scope.mData.GridDetail);
                            // $scope.mData.GridDetail = AppointmentData;
                            console.log("ini $scope.mData 241 341==>",$scope.mData);
                        }
                        defer.resolve(true);
                    },
                    function(err) {
                        console.log("err=>", err);
                        defer.resolve(false);
                    }
                );
                // [END] Get Search Appointment Grid
            } else if ($scope.formState === '42') {
                PurchaseOrder.searchReference(row.PurchaseOrderId, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail).then(function(res) {
                    $scope.SumTotalAmount();
                        var AppointmentData = res.data[0];
                        AppointmentData.CreatedDate = $scope.localeDate(AppointmentData.CreatedDate);
                        console.log("AppointmentData==>",AppointmentData);

                        //masukkan ke dalam model
                        $scope.mData.ColorId = AppointmentData.ColorId;
                        $scope.mData.MachineNo = AppointmentData.VIN;
                        $scope.mData.NoPol = AppointmentData.NoPol;
                        $scope.mData.Year = AppointmentData.Year;
                        $scope.mData.VehicleId = AppointmentData.VehicleId;
                        $scope.mData.VehicleModelId = AppointmentData.VehicleModelId;
                        $scope.mData.FullModel = AppointmentData.FullModel;
                        $scope.mData.VehicleColourCode = AppointmentData.VehicleColourCode;
                        $scope.mData.ModelName = AppointmentData.ModelName;
                        $scope.mData.Name = AppointmentData.Name;
                        $scope.mData.FrameNo = AppointmentData.FrameNo;
                        $scope.mData.AppointmentDate = AppointmentData.AppointmentDate;
                        $scope.mData.AppointmentTime = AppointmentData.AppointmentTime;
                        $scope.mData.CreatedDate = AppointmentData.CreatedDate;
                        $scope.mData.CreatedUserId = AppointmentData.CreatedUserId;
                        $scope.mData.AppointmentNo = AppointmentData.AppointmentNo;
                        $scope.mData.BackOrderFollowId = AppointmentData.BackOrderFollowId;
                        $scope.mData.Notes = AppointmentData.Notes;
                        // $scope.mData.POTypeId = AppointmentData.POTypeId;
                        // $scope.mData.RefPOTypeId = AppointmentData.RefPOTypeId;


                        //console.log("From Factory => ", PurchaseOrder.getPurchaseOrderHeader());

                    },
                    function(err) {
                        console.log("err=>", err);
                        defer.resolve(false);
                    }
                );
                // [END] Get Search Appointment

                // // [START] Get Search Appointment Grid Detail
                PurchaseOrder.searchReferenceDetail(row.PurchaseOrderId, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail, $scope.mData.MaterialTypeId, $scope.mData.VendorId, $scope.UserId).then(function(res) {
                        var AppointmentData = res.data;
                        console.log('searchReferenceDetail =42=>')
                        console.log(AppointmentData);
                        for (var i=0; i<AppointmentData.length; i++) {
                            AppointmentData[i].CreatedDate = $scope.localeDate(AppointmentData[i].CreatedDate);
                            AppointmentData[i].ETD = '-';
                            AppointmentData[i].ETA = '-';
                        }
                        $scope.SumTotalAmount();

                        $scope.non_tam_sop_wo.data = AppointmentData;
                        $scope.non_tam_sop_so.data = AppointmentData;
                        $scope.non_tam_non_sop_bengkel.data = AppointmentData;
                        $scope.mData.GridDetail = AppointmentData;
                        $scope.SumTotalAmount();

                        console.log('<==42=>')
                        defer.resolve(true);
                    },
                    function(err) {
                        console.log("err=>", err);
                        defer.resolve(false);
                    }
                );
            } else if (($scope.formState === '12') || ($scope.formState === '22') || ($scope.formState === '32')) {
                PurchaseOrder.searchReference(row.PurchaseOrderId, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail).then(function(res) {
                    $scope.SumTotalAmount();
                        var AppointmentData = res.data[0];
                        AppointmentData.CreatedDate = $scope.localeDate(AppointmentData.CreatedDate);
                        console.log("AppointmentData 12 22 32=>",AppointmentData);

                        //masukkan ke dalam model
                        $scope.mData.ColorId = AppointmentData.ColorId;
                        $scope.mData.MachineNo = AppointmentData.VIN;
                        $scope.mData.NoPol = AppointmentData.NoPol;
                        $scope.mData.Year = AppointmentData.Year;
                        $scope.mData.VehicleId = AppointmentData.VehicleId;
                        $scope.mData.VehicleModelId = AppointmentData.VehicleModelId;
                        $scope.mData.FullModel = AppointmentData.FullModel;
                        $scope.mData.VehicleColourCode = AppointmentData.VehicleColourCode;
                        $scope.mData.ModelName = AppointmentData.ModelName;
                        $scope.mData.Name = AppointmentData.Name;
                        $scope.mData.FrameNo = AppointmentData.FrameNo;
                        $scope.mData.AppointmentDate = AppointmentData.AppointmentDate;
                        $scope.mData.AppointmentTime = AppointmentData.AppointmentTime;
                        $scope.mData.CreatedDate = AppointmentData.CreatedDate;
                        $scope.mData.CreatedUserId = AppointmentData.CreatedUserId;
                        $scope.mData.AppointmentNo = AppointmentData.AppointmentNo;
                        $scope.mData.BackOrderFollowId = AppointmentData.BackOrderFollowId;
                        $scope.mData.Notes = AppointmentData.Notes;

                        //Tire
                        if($scope.mData.POTypeId == 6){
                            $scope.mData.TireOrderId = AppointmentData.TireOrderId;
                            $scope.mData.NecessaryDate = AppointmentData.NecessaryDate;
                            var tempSplitTime = AppointmentData.NecessaryTime.split(':');

                            var tempTime = {    
                                value: new Date($scope.mData.NecessaryDate.getFullYear(), $scope.mData.NecessaryDate.getMonth(), 
                                $scope.mData.NecessaryDate.getDate(), tempSplitTime[0], 
                                tempSplitTime[1], 0)
                                };
                            $scope.mData.NecessaryTime = tempTime.value;
                            $scope.mData.NoPol = AppointmentData.NoPol;
                        }
                        // $scope.mData.POTypeId = AppointmentData.POTypeId;
                        // $scope.mData.RefPOTypeId = AppointmentData.RefPOTypeId;


                        //console.log("From Factory => ", PurchaseOrder.getPurchaseOrderHeader());

                    },
                    function(err) {
                        console.log("err=>", err);
                        defer.resolve(false);
                    }
                );
                // [END] Get Search Appointment

                // // [START] Get Search Appointment Grid Detail
                PurchaseOrder.searchReferenceDetail(row.PurchaseOrderId, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail, $scope.mData.MaterialTypeId, $scope.mData.VendorId, $scope.UserId).then(function(res) {
                        var AppointmentData = res.data;
                        console.log('searchReferenceDetail ==> as')
                        console.log(AppointmentData);
                        $scope.SumTotalAmount();
                        for (var i in AppointmentData) {
                            if (AppointmentData[i].ETA != null) {
                                var eta = AppointmentData[i].ETA;
                                eta = new Date(eta);
                                // var etaa
                                // var yyyyeta = eta.getFullYear().toString();
                                // var mmeta = (eta.getMonth() + 1).toString(); // getMonth() is zero-based
                                // var ddeta = eta.getDate().toString();
                                // etaa = (ddeta[1] ? ddeta : "0" + ddeta[0]) + '/' + (mmeta[1] ? mmeta : "0" + mmeta[0]) + '/' + yyyyeta;
                                // console.log("changeFormatDate finalDate", etaa);
                                AppointmentData[i].ETA = eta;
                            } else {
                                AppointmentData[i].ETA = null;
                            }
                            AppointmentData[i].CreatedDate = $scope.localeDate(AppointmentData[i].CreatedDate);
                        }

                        for (var i in AppointmentData) {
                            if (AppointmentData[i].ETD != null) {
                                var ETD = AppointmentData[i].ETD;
                                ETD = new Date(ETD);
                                // var ETDa
                                // var yyyyETD = ETD.getFullYear().toString();
                                // var mmETD = (ETD.getMonth() + 1).toString(); // getMonth() is zero-based
                                // var ddETD = ETD.getDate().toString();
                                // ETDa = (ddETD[1] ? ddETD : "0" + ddETD[0]) + '/' + (mmETD[1] ? mmETD : "0" + mmETD[0]) + '/' + yyyyETD;
                                // console.log("changeFormatDate finalDate", ETDa);
                                AppointmentData[i].ETD = ETD;
                            } else {
                                AppointmentData[i].ETD = null;
                            }
                        }
                        $scope.tam_sop_wo_lihat.data = AppointmentData;
                        for(var count = 0; count < AppointmentData.length; count++){
                            AppointmentData[count].idx = count;
                        }
                        if($scope.formState === '12'){
                            console.log('nambahin ---',AppointmentData);
                            $scope.tam_sop_wo_ubah.data = AppointmentData
                        }
                        $scope.mData.GridDetail = AppointmentData;
                        $scope.SumTotalAmount();

                        defer.resolve(true);

                    },
                    function(err) {
                        console.log("err=>", err);
                        defer.resolve(false);
                    }
                );
            } else if ($scope.formState === '52' || $scope.formState === '51' ) {
                PurchaseOrder.searchReference(row.PurchaseOrderId, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail).then(function(res) {
                    $scope.SumTotalAmount();
                        var AppointmentData = res.data[0];
                        AppointmentData.CreatedDate = $scope.localeDate(AppointmentData.CreatedDate);
                        console.log(AppointmentData);

                        //masukkan ke dalam model
                        $scope.mData.ColorId = AppointmentData.ColorId;
                        $scope.mData.MachineNo = AppointmentData.VIN;
                        $scope.mData.NoPol = AppointmentData.NoPol;
                        $scope.mData.Year = AppointmentData.Year;
                        $scope.mData.VehicleId = AppointmentData.VehicleId;
                        $scope.mData.VehicleModelId = AppointmentData.VehicleModelId;
                        $scope.mData.FullModel = AppointmentData.FullModel;
                        $scope.mData.VehicleColourCode = AppointmentData.VehicleColourCode;
                        $scope.mData.ModelName = AppointmentData.ModelName;
                        $scope.mData.Name = AppointmentData.Name;
                        $scope.mData.FrameNo = AppointmentData.FrameNo;
                        $scope.mData.AppointmentDate = AppointmentData.AppointmentDate;
                        $scope.mData.AppointmentTime = AppointmentData.AppointmentTime;
                        $scope.mData.CreatedDate = AppointmentData.CreatedDate;
                        $scope.mData.CreatedUserId = AppointmentData.CreatedUserId;
                        $scope.mData.AppointmentNo = AppointmentData.AppointmentNo;
                        $scope.mData.Phone = AppointmentData.Phone;
                        $scope.mData.BackOrderFollowId = AppointmentData.BackOrderFollowId;
                        $scope.mData.Notes = AppointmentData.Notes;
                        // $scope.mData.POTypeId = AppointmentData.POTypeId;
                        // $scope.mData.RefPOTypeId = AppointmentData.RefPOTypeId;


                        //console.log("From Factory => ", PurchaseOrder.getPurchaseOrderHeader());

                    },
                    function(err) {
                        console.log("err=>", err);
                        defer.resolve(false);
                    }
                );
                // [END] Get Search Appointment

                // // [START] Get Search Appointment Grid Detail
                PurchaseOrder.searchReferenceDetail(row.PurchaseOrderId, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail, $scope.mData.MaterialTypeId, $scope.mData.VendorId, $scope.UserId).then(function(res) {
                        var AppointmentData = res.data;
                        console.log('searchReferenceDetail ==> ass')
                        console.log(AppointmentData);
                        $scope.SumTotalAmount();
                        if(AppointmentData.length > 0){
                            for(var z in AppointmentData){
                                AppointmentData[z].PartsCode = null;
                            }
                        }
                        for (var i in AppointmentData) {
                            AppointmentData[i].CreatedDate = $scope.localeDate(AppointmentData[i].CreatedDate);
                            AppointmentData[i].ETD = '-';
                            AppointmentData[i].ETA = '-';
                        }

                        $scope.sublet_sop_wo_lihat.data = AppointmentData;
                        $scope.sublet_sop_wo.data = AppointmentData;
                        $scope.non_tam_sop_so.data = AppointmentData;
                        $scope.mData.GridDetail = angular.copy(AppointmentData);
                        $scope.SumTotalAmount();

                        defer.resolve(true);
                    },
                    function(err) {
                        console.log("err=>", err);
                        defer.resolve(false);
                    }
                );
            } else if (($scope.formState === '43') || ($scope.formState === '13') || ($scope.formState === '23') || ($scope.formState === '53') || ($scope.formState === '33')) {
                // alert('Here');
                PurchaseOrder.searchReference(row.PurchaseOrderId, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail).then(function(res) {
                    $scope.SumTotalAmount();
                        var AppointmentData = res.data[0];
                        AppointmentData.CreatedDate = $scope.localeDate(AppointmentData.CreatedDate);
                        console.log(AppointmentData);

                        $scope.mData.CreatedDate = AppointmentData.CreatedDate;
                        $scope.mData.CreatedUserId = AppointmentData.CreatedUserId;
                        // $scope.mData.VendorName = AppointmentData.VendorName;
                        // $scope.mData.POTypeId = AppointmentData.POTypeId;
                        // $scope.mData.RefPOTypeId = AppointmentData.RefPOTypeId;
                        $scope.mData.Notes = AppointmentData.Notes;
                        //masukkan ke dalam model
                        if (row.CustomerId == 0) {
                            $scope.mData.BengkelName = AppointmentData.BengkelName;
                        }
                        if (row.VendorOutletId == 0) {
                            $scope.mData.CustomerName = AppointmentData.CustomerName;
                        }

                        if (AppointmentData.RefSOTypeId == 1) {
                            $scope.vcState = 'bengkel';
                        }
                        if (AppointmentData.RefSOTypeId == 2) {
                            $scope.vcState = 'customer';
                        }
                        console.log("$scope.vcState=>",$scope.vcState);
                        //Tire
                        if($scope.mData.POTypeId == 6){
                            $scope.mData.TireOrderId = AppointmentData.TireOrderId;
                            $scope.mData.NecessaryDate = AppointmentData.NecessaryDate;
                            var tempSplitTime = AppointmentData.NecessaryTime.split(':');

                            var tempTime = {    
                                value: new Date($scope.mData.NecessaryDate.getFullYear(), $scope.mData.NecessaryDate.getMonth(), 
                                $scope.mData.NecessaryDate.getDate(), tempSplitTime[0], 
                                tempSplitTime[1], 0)
                                };
                            $scope.mData.NecessaryTime = tempTime.value;
                            $scope.mData.NoPol = AppointmentData.NoPol;
                        }
                        //console.log("From Factory => ", PurchaseOrder.getPurchaseOrderHeader());

                    },
                    function(err) {
                        console.log("err=>", err);
                        defer.resolve(false);
                    }
                );

                // // [START] Get Search Appointment Grid Detail
                PurchaseOrder.searchReferenceDetail(row.PurchaseOrderId, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail, $scope.mData.MaterialTypeId, $scope.mData.VendorId, $scope.UserId).then(function(res) {
                        var AppointmentData = res.data;
                        console.log('searchReferenceDetail ==>ws')
                        console.log(AppointmentData);
                        $scope.SumTotalAmount();
                        for (var i in AppointmentData) {
                            if (AppointmentData[i].ETA != null) {
                                var eta = AppointmentData[i].ETA;
                                eta = new Date(eta);
                                console.log("changeFormatDate finalDate", eta);
                                AppointmentData[i].ETA = eta;
                            } else {
                                AppointmentData[i].ETA = null;
                            }
                            if (AppointmentData[i].ETD != null) {
                                var etd = AppointmentData[i].ETD;
                                etd = new Date(etd);
                                console.log("changeFormatDate finalDate", etd);
                                AppointmentData[i].ETD = etd;
                            } else {
                                AppointmentData[i].ETD = null;
                            }

                            AppointmentData[i].CreatedDate = $scope.localeDate(AppointmentData[i].CreatedDate);
                            if ($scope.formState == '43') {
                                AppointmentData[i].ETD = '-';
                                AppointmentData[i].ETA = '-';
                            }
                        }
                        for(var count = 0; count < AppointmentData.length; count++){
                            AppointmentData[count].idx = count;
                        }
                        $scope.non_tam_sop_so_lihat.data = AppointmentData;
                        $scope.non_tam_sop_so.data = AppointmentData;
                        $scope.non_tam_sop_so_ubah.data = AppointmentData;
                        $scope.mData.GridDetail = AppointmentData;
                        $scope.SumTotalAmount();

                        defer.resolve(true);

                    },
                    function(err) {
                        console.log("err=>", err);
                        defer.resolve(false);
                    }
                );

            } else if (($scope.formState === '44') || ($scope.formState === '14') || ($scope.formState === '24') || ($scope.formState === '34') || ($scope.formState === '54')) {

                PurchaseOrder.searchReference(row.PurchaseOrderId, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail).then(function(res) {
                    console.log('searchReference | result ===>',res.data[0])
                    $scope.SumTotalAmount();
                        var AppointmentData = res.data[0];
                        AppointmentData.CreatedDate = $scope.localeDate(AppointmentData.CreatedDate);
                        console.log(AppointmentData);
                        $scope.mData.CreatedDate = AppointmentData.CreatedDate;
                        $scope.mData.CreatedUserId = AppointmentData.CreatedUserId;
                        $scope.mData.NoEstimasi = AppointmentData.EstimationNo;
                        $scope.mData.Notes = AppointmentData.Notes;


                        var findVendor = _.find($scope.vendorData, function(o){
                            return o.VendorId == AppointmentData.VendorId
                        })

                        console.log('searchReference | findVendor ===>',findVendor)
                        $scope.mData.NamaVendor = findVendor.Name
                        $scope.mData.Telephone = findVendor.Phone
                        $scope.mData.Alamat = findVendor.Address

                        // $scope.mData.POTypeId = AppointmentData.POTypeId;
                        // $scope.mData.RefPOTypeId = AppointmentData.RefPOTypeId;
                        // $scope.mData.VendorName = AppointmentData.VendorName;
                    },
                    function(err) {
                        console.log("err=>", err);
                        defer.resolve(false);
                    }
                );

                // // [START] Get Search Appointment Grid Detail
                PurchaseOrder.searchReferenceDetail(row.PurchaseOrderId, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail, $scope.mData.MaterialTypeId, $scope.mData.VendorId, $scope.UserId).then(function(res) {
                        var AppointmentData = res.data;
                        console.log('searchReferenceDetail ==> asq jancuk',res.data)
                        console.log(AppointmentData);
                        $scope.SumTotalAmount();

                        for (var i in AppointmentData) {
                            if (AppointmentData[i].ETA != null) {
                                var eta = AppointmentData[i].ETA;
                                eta = new Date(eta);
                                eta = $filter('date')(eta, 'yyyy-MM-dd HH:MM');
                                console.log("changeFormatDate finalDate", eta);
                                AppointmentData[i].ETA = eta;
                            } else {
                                AppointmentData[i].ETA = null;
                            }
                            if (AppointmentData[i].ETD != null) {
                                var etd = AppointmentData[i].ETD;
                                etd = new Date(etd);
                                console.log("changeFormatDate finalDate", etd);
                                AppointmentData[i].ETD = etd;
                            } else {
                                AppointmentData[i].ETD = null;
                            }

                            AppointmentData[i].CreatedDate = $scope.localeDate(AppointmentData[i].CreatedDate);
                            if ($scope.formState == '44') {
                                AppointmentData[i].ETD = '-';
                                AppointmentData[i].ETA = '-';
                            }
                        }

                        for(var count = 0; count < AppointmentData.length; count++){
                            AppointmentData[count].idx = count;
                        }
                        $scope.non_tam_sop_so_lihat.data = AppointmentData;
                        $scope.non_tam_sop_so.data = AppointmentData;
                        $scope.non_tam_non_sop.data = AppointmentData;
                        $scope.non_tam_non_sop_bengkel.data = AppointmentData;
                        $scope.tam_sop_appointment_ubah.data = AppointmentData;
                        $scope.mData.GridDetail = AppointmentData;
                        $scope.SumTotalAmount();


                        if (mode == 'new') { //untuk menghindari data griddetail terisi setelah cancelOrDelete
                            $scope.non_tam_sop_so.data = [];
                            $scope.non_tam_non_sop.data = [];
                            $scope.non_tam_non_sop_bengkel.data = [];
                            $scope.mData.GridDetail = [];
                        }
                        defer.resolve(true);

                    },
                    function(err) {
                        console.log("err=>", err);
                        defer.resolve(false);
                    }
                );

            } else if ($scope.formState === '34') {
                // PurchaseOrder.searchReference(row.RefPONo, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail).then(function(res) {
                //     var AppointmentData = res.data[0];
                //     console.log(AppointmentData);

                //     $scope.mData.VendorName = AppointmentData.VendorName;

                //   },
                //   function(err) {
                //     console.log("err=>", err);
                //   }
                // );

                // // [START] Get Search Appointment Grid Detail
                PurchaseOrder.searchReferenceDetail(row.PurchaseOrderId, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail, $scope.mData.MaterialTypeId, $scope.mData.VendorId, $scope.UserId).then(function(res) {
                        var AppointmentData = res.data;
                        console.log('searchReferenceDetail ==> wsr')
                        console.log(AppointmentData);
                        $scope.SumTotalAmount();

                        for (var i in AppointmentData) {
                            if (AppointmentData[i].ETA != null) {
                                var eta = AppointmentData[i].ETA;
                                eta = new Date(eta);
                                // var etaa
                                // var yyyyeta = eta.getFullYear().toString();
                                // var mmeta = (eta.getMonth() + 1).toString(); // getMonth() is zero-based
                                // var ddeta = eta.getDate().toString();
                                // etaa = (ddeta[1] ? ddeta : "0" + ddeta[0]) + '/' + (mmeta[1] ? mmeta : "0" + mmeta[0]) + '/' + yyyyeta;
                                // console.log("changeFormatDate finalDate", etaa);
                                AppointmentData[i].ETA = eta;
                            } else {
                                AppointmentData[i].ETA = null;
                            }
                            AppointmentData[i].CreatedDate = $scope.localeDate(AppointmentData[i].CreatedDate);
                        }

                        for (var i in AppointmentData) {
                            if (AppointmentData[i].ETD != null) {
                                var ETD = AppointmentData[i].ETD;
                                ETD = new Date(ETD);
                                // var ETDa
                                // var yyyyETD = ETD.getFullYear().toString();
                                // var mmETD = (ETD.getMonth() + 1).toString(); // getMonth() is zero-based
                                // var ddETD = ETD.getDate().toString();
                                // ETDa = (ddETD[1] ? ddETD : "0" + ddETD[0]) + '/' + (mmETD[1] ? mmETD : "0" + mmETD[0]) + '/' + yyyyETD;
                                // console.log("changeFormatDate finalDate", ETDa);
                                AppointmentData[i].ETD = ETD;
                            } else {
                                AppointmentData[i].ETD = null;
                            }
                        }
                        $scope.tam_soq_lihat.data = AppointmentData;
                        $scope.mData.GridDetail = AppointmentData;
                        $scope.SumTotalAmount();

                        defer.resolve(true);

                    },
                    function(err) {
                        console.log("err=>", err);
                        defer.resolve(false);
                    }
                );
                console.log("HasilSOQ");

            }

            console.log("[SHOW] " + $scope.menuPOtype + " " + $scope.menuPOref);
            // console.log("idihhhhhh", $scope.gridApi.grid);
            return defer.promise;

        }

        $scope.onShowDetail = function(row, mode, xparam) {
                console.log('row', row);
                console.log('mode', mode);
                console.log('$scope.gridApi=>', $scope.gridApi);
                $scope.CampaignIdNow = row.CampaignId; //campaign Start

                if((row.POTypeId == 1 || row.POTypeId == 2 || row.POTypeId == 3 || row.POTypeId == 7) && row.RefPOTypeId == 4) {
                    if((row.POTypeId == 7 || row.POTypeId == 3) && row.CampaignId != null){
                        $scope.non_tam_non_sop_bengkel.columnDefs = $scope.columnTAMCampaign;
                    }else{
                        $scope.non_tam_non_sop_bengkel.columnDefs = $scope.columnTAM;
                    }
                } else if(row.RefPOTypeId == 4) {
                    $scope.non_tam_non_sop_bengkel.columnDefs = $scope.columnNonTAM;
                } //campaign End
                
                if (row.POTypeId == 5 && row.RefPOTypeId == 4 && row.RefPONo != '') {
                    $scope.isEstimasiBP = true;
                } else {
                    $scope.isEstimasiBP = false;
                }

                if ((row.POTypeId == 1 || row.POTypeId == 3 || row.POTypeId == 3) && row.RefPOTypeID == 4){
                    $scope.flagStatusTPOS = true;
                } else {
                    $scope.flagStatusTPOS = false;
                }
                console.log("$scope.flagStatusTPOS=>",$scope.flagStatusTPOS);

                $scope.stateModeView = mode == 'view' ? true : false;
                if (((row.POTypeId == 1) || (row.POTypeId == '1') || (row.POTypeId == 2) || (row.POTypeId == '2') || (row.POTypeId == 3) || (row.POTypeId == '3'))) {
                    $scope.isPoTAM = true;
                }else{
                    $scope.isPoTAM = false;
                }
                console.log('$scope.stateModeView', $scope.stateModeView, $scope.isPoTAM);
                if (mode == 'new') {
                    console.log("new");
                    console.log("NEW========");
                    console.log("row : ", row);
                    console.log("$scope.user", $scope.user);
                    console.log("xparam : ", xparam);
					$scope.mData.IsNONPKP = false;
                    console.log("$scope.mData.IsNONPKP=>",$scope.mData.IsNONPKP);
                    if (typeof xparam !== 'undefined') {
                        if (typeof xparam.fromOtherModule !== 'undefined' || xparam.fromOtherModule) {
                            itIsLink = true;
                            paramNew = angular.copy(xparam);
                        }
                    }
                    $scope.isView = false;
                    $scope.isEdit = false;

                    console.log("$scope.mData=>",$scope.mData);
                    console.log("$scope.mData.GridDetai=>",$scope.mData.GridDetail);
                }
                if (mode == 'view') {
					console.log('row stv :', row);
					// CR5 #10
					if(row.IsNonPKP == 0){
						$scope.mData.IsNONPKP = false;
					}else if(row.IsNonPKP == 1){
						$scope.mData.IsNONPKP = true;
					}
					// CR5 #10
                    if (row.HasilSOQ === true) {
                        //alert("HasilSOQ");
                        $scope.crudState = 'soq';

                        $scope.mData.BackOrderFollowId = row.BackOrderFollowId;

                        console.log("$scope.mData.BackOrderFollowId kesini", $scope.mData.BackOrderFollowId);
						
                        PurchaseOrder.searchReferenceDetail(row.PurchaseOrderId, row.POTypeId, row.RefPOTypeId, 2, row.MaterialTypeId,$scope.mData.VendorId).then(function(res) {
                                var AppointmentData = res.data;
                                console.log('searchReferenceDetail ==> dad')
                                $scope.SumTotalAmount();
                                for (var i in AppointmentData) {
                                    if (AppointmentData[i].ETA != null) {
                                        var eta = AppointmentData[i].ETA;
                                        eta = new Date(eta);
                                        // var etaa
                                        // var yyyyeta = eta.getFullYear().toString();
                                        // var mmeta = (eta.getMonth() + 1).toString(); // getMonth() is zero-based
                                        // var ddeta = eta.getDate().toString();
                                        // etaa = (ddeta[1] ? ddeta : "0" + ddeta[0]) + '/' + (mmeta[1] ? mmeta : "0" + mmeta[0]) + '/' + yyyyeta;
                                        // console.log("changeFormatDate finalDate", etaa);
                                        AppointmentData[i].ETA = eta;
                                    } else {
                                        AppointmentData[i].ETA = null;
                                    }
                                }

                                for (var i in AppointmentData) {
                                    if (AppointmentData[i].ETD != null) {
                                        var ETD = AppointmentData[i].ETD;
                                        ETD = new Date(ETD);
                                        // var ETDa
                                        // var yyyyETD = ETD.getFullYear().toString();
                                        // var mmETD = (ETD.getMonth() + 1).toString(); // getMonth() is zero-based
                                        // var ddETD = ETD.getDate().toString();
                                        // ETDa = (ddETD[1] ? ddETD : "0" + ddETD[0]) + '/' + (mmETD[1] ? mmETD : "0" + mmETD[0]) + '/' + yyyyETD;
                                        // console.log("changeFormatDate finalDate", ETDa);
                                        AppointmentData[i].ETD = ETD;
                                    } else {
                                        AppointmentData[i].ETD = null;
                                    }
                                }

                                console.log(AppointmentData);
                                $scope.tam_soq_lihat.data = AppointmentData;
                                $scope.mData.GridDetail = AppointmentData;
                                $scope.SumTotalAmount();

                                var total = 0
                                for (var i in AppointmentData) {
                                    total += AppointmentData[i].TotalAmount;
                                    console.log("AppointmentData[",i,"].TotalAmount",AppointmentData[i].TotalAmount);
                                }
                                total = parseFloat(total).toFixed(1);
                                console.log("total==>>",total);
                                total = String(total);
                                console.log("totalstr==>>",total);
                                $scope.mData.TotalAmount = total.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                                console.log("$scope.mData.TotalAmount==>>",$scope.mData.TotalAmount);
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    } else {
                        // $scope.PreShowDetail(row, mode);
                        console.log("view row/mode=>",row,mode);
                        $scope.PreShowDetail(row, mode).then(function(resultPromise){
                            if(resultPromise){
                                console.log("idihhhhhh", $scope.gridApi.grid.columns);
                                if($scope.gridApi){
                                    for(var i in $scope.gridApi.grid.columns){
                                        if($scope.gridApi.grid.columns[i].field == "StatusTPOS" || $scope.gridApi.grid.columns[i].field == "ReasonTPOS"){
                                            if($scope.isPoTAM){
                                                $scope.gridApi.grid.columns[i].showColumn();
                                            }else{
                                                $scope.gridApi.grid.columns[i].hideColumn();
                                            }
                                        }
                                    }         
                                }

                                console.log("$scope.gridApi=>",$scope.gridApi);
                                if ($scope.mData.POTypeId == 5 && $scope.mData.RefPOTypeId == 4) {
                                    for(var i in $scope.gridApi.grid.columns){
                                        if ($scope.gridApi.grid.columns[i].field == "StatusTPOS" || $scope.gridApi.grid.columns[i].field == "ReasonTPOS" ||
                                            $scope.gridApi.grid.columns[i].field == "ETA" || $scope.gridApi.grid.columns[i].field == "ETD") {
                        
                                            console.log("$scope.gridApi.grid.columns[i].field=>",$scope.gridApi.grid.columns[i].field);
                                            console.log("$scope.isPoTAM=>",$scope.isPoTAM);
                                            console.log("$scope.isEstimasiBP=>",$scope.isEstimasiBP);
                
                                            if($scope.isPoTAM){
                                                $scope.gridApi.grid.columns[i].showColumn();
                                            }else{
                                                $scope.gridApi.grid.columns[i].hideColumn();
                                            }
                                        }
                                    }         
                                }
                            }
                        });

                        if (((row.POTypeId == 1) || (row.POTypeId == '1') || (row.POTypeId == 2) || (row.POTypeId == '2') || (row.POTypeId == 3) || (row.POTypeId == '3') ||
                            (row.POTypeId == 6) || (row.POTypeId == '6'))) {
                            //harusnya kill 1 bukan 0
                            $scope.BoTam = [{ Id: 1, Name: "Kill" }, { Id: 9, Name: "Fill" }];
                            // $scope.mData.BackOrderFollowId = 9;
                        }
                        console.log("$scope.mData.BackOrderFollowId kesini", $scope.mData.BackOrderFollowId);

                        var shownum = '2'
                        $scope.crudState = shownum + $scope.formShowHandler($scope.menuPOtype, $scope.menuPOref);

                        if ($scope.crudState === '243') {
                            if (row.CustomerId == 0) {
                                $scope.crudState = '2431';
                            } else if (row.VendorOutletId == 0) {
                                $scope.crudState = '2432';
                            }
                        } else if ($scope.crudState === '244') {
                            if (row.MaterialTypeId == 1) {
                                $scope.crudState = '2441';
                            } else if (row.MaterialTypeId == 2) {
                                $scope.crudState = '2442';
                            }
                        }
                    }
                    $scope.isView = true;
                    $scope.isEdit = false;
                } else if (mode == 'edit') {
					// CR5 #10
					console.log('row stv :', row);
					if(row.IsNonPKP == 0){
						$scope.mData.IsNONPKP = false;
					}else if(row.IsNonPKP == 1){
						$scope.mData.IsNONPKP = true;
					}
					// CR5 #10

                    if(row.POTypeId > 0 && row.POTypeId < 4){
                        if(row.StatusTPOS == 1){
                            $scope.formApi.setMode('grid');
                        }else{
                            // $scope.PreShowDetail(row, mode);
                            $scope.PreShowDetail(row, mode).then(function(resultPromise){
                                if(resultPromise){
                                    console.log("idihhhhhh", $scope.gridApi.grid.columns);
                                    if($scope.gridApi){
                                        for(var i in $scope.gridApi.grid.columns){
                                            if($scope.gridApi.grid.columns[i].field == "StatusTPOS" || $scope.gridApi.grid.columns[i].field == "ReasonTPOS"){
                                                if($scope.isPoTAM){
                                                    $scope.gridApi.grid.columns[i].showColumn();
                                                }else{
                                                    $scope.gridApi.grid.columns[i].hideColumn();
                                                }
                                            }
                                        }         
                                    }
                                }
                            });
                            PurchaseOrder.setPurchaseOrderHeader(row);
                            var editnum = '3'
                            if (((row.POTypeId == 1) || (row.POTypeId == '1') || (row.POTypeId == 2) || (row.POTypeId == '2') || (row.POTypeId == 3) || (row.POTypeId == '3'))) {
                                //harusnya kill 1 bukan 0
                                $scope.BoTam = [{ Id: 1, Name: "Kill" }, { Id: 9, Name: "Fill" }];
                                $scope.botamstatus = true;
                                // $scope.mData.BackOrderFollowId = 9;
                            }
                            $scope.mData.BackOrderFollowId = row.BackOrderFollowId;

                            console.log("$scope.mData.BackOrderFollowId kesini", $scope.mData.BackOrderFollowId);

                            $scope.crudState = editnum + $scope.formShowHandler($scope.mData.POTypeId, $scope.mData.RefPOTypeId);
                            if ($scope.crudState === '342') {
                                console.log("Masuk 342 : ");
                                if (row.CustomerId == 0) {
                                    $scope.crudState = '3421';
                                    console.log("Masuk 342 .=> : ", $scope.crudState);
                                } else if (row.VendorOutletId == 0) {
                                    $scope.crudState = '3422';
                                    console.log("Masuk 342 :=> : ", $scope.crudState);
                                }
                            } else if ($scope.crudState === '343') {
                                if (row.CustomerId == 0) {
                                    $scope.crudState = '3431';
                                } else if (row.VendorOutletId == 0) {
                                    $scope.crudState = '3432';
                                }
                            } else if ($scope.crudState === '344') {
                                if (row.MaterialTypeId == 1) {
                                    $scope.crudState = '3441';
                                } else if (row.MaterialTypeId == 2) {
                                    $scope.crudState = '3442';
                                }
                            }

                            $scope.isView = false;
                            $scope.isEdit = true;
                            console.log("$scope.isEdit==>",$scope.isEdit);
                        }
                    }else{
                        // $scope.PreShowDetail(row, mode);
                        $scope.PreShowDetail(row, mode).then(function(resultPromise){
                            if(resultPromise){
                                console.log("idihhhhhh", $scope.gridApi.grid.columns);
                                if($scope.gridApi){
                                    for(var i in $scope.gridApi.grid.columns){
                                        if($scope.gridApi.grid.columns[i].field == "StatusTPOS" || $scope.gridApi.grid.columns[i].field == "ReasonTPOS"){
                                            if($scope.isPoTAM){
                                                $scope.gridApi.grid.columns[i].showColumn();
                                            }else{
                                                $scope.gridApi.grid.columns[i].hideColumn();
                                            }
                                        }
                                    }         
                                }

                                console.log("$scope.gridApi=>",$scope.gridApi);
                                if ($scope.mData.POTypeId == 5 && $scope.mData.RefPOTypeId == 4) {
                                    for(var i in $scope.gridApi.grid.columns){
                                        if ($scope.gridApi.grid.columns[i].field == "StatusTPOS" || $scope.gridApi.grid.columns[i].field == "ReasonTPOS" ||
                                            $scope.gridApi.grid.columns[i].field == "ETA" || $scope.gridApi.grid.columns[i].field == "ETD") {
                        
                                            console.log("$scope.gridApi.grid.columns[i].field=>",$scope.gridApi.grid.columns[i].field);
                                            console.log("$scope.isPoTAM=>",$scope.isPoTAM);
                                            console.log("$scope.isEstimasiBP=>",$scope.isEstimasiBP);
                
                                            if($scope.isPoTAM){
                                                $scope.gridApi.grid.columns[i].showColumn();
                                            }else{
                                                $scope.gridApi.grid.columns[i].hideColumn();
                                            }
                                        }
                                    }         
                                }

                            }
                        });

                        PurchaseOrder.setPurchaseOrderHeader(row);
                        var editnum = '3'
                        $scope.mData.BackOrderFollowId = row.BackOrderFollowId;

                        console.log("$scope.mData.BackOrderFollowId kesini", $scope.mData.BackOrderFollowId);

                        $scope.crudState = editnum + $scope.formShowHandler($scope.mData.POTypeId, $scope.mData.RefPOTypeId);
                        console.log("$scope.crudState=>",$scope.crudState);
                        console.log("row.CustomerId=>",row.CustomerId);
                        console.log("row.VendorOutletId=>",row.VendorOutletId);

                        if ($scope.crudState === '342') {
                            console.log("Masuk 342 : ");
                            if (row.CustomerId == 0) {
                                $scope.crudState = '3421';
                                console.log("Masuk 342 .=> : ", $scope.crudState);
                            } else if (row.VendorOutletId == 0) {
                                $scope.crudState = '3422';
                                console.log("Masuk 342 :=> : ", $scope.crudState);
                            }
                        } else if ($scope.crudState === '343') {
                            if (row.CustomerId == 0) {
                                $scope.crudState = '3431';
                            } else if (row.VendorOutletId == 0) {
                                $scope.crudState = '3432';
                            }
                        } else if ($scope.crudState === '344') {
                            if (row.MaterialTypeId == 1) {
                                $scope.crudState = '3441';
                            } else if (row.MaterialTypeId == 2) {
                                $scope.crudState = '3442';
                            }
                        }

                        $scope.isView = false;
                        $scope.isEdit = true;
                        console.log("$scope.isEdit==>",$scope.isEdit);

                    }
                    // PurchaseOrder.checkValidationTPOS(row.PurchaseOrderId).then(function(result){
                        
                        
                    // });
                    enableSelect();   
                }else{
                    enableSelect();
                }
                console.log("$scope.crudState",$scope.crudState);

                //campaign
                PurchaseOrder.getDetailCampaingId($scope.CampaignIdNow).then(function(res) {
                    $scope.isBundle = res.data[0].CampaignType.toLowerCase().includes('bundle') ? true : false;
                    $scope.mData.QuotaType = res.data[0].QuotaType;
                    $scope.mData.CampaignName = res.data[0].CampaignName;
                    $scope.mData.CampaignCode = res.data[0].CampaignCode;
                    $scope.mData.ApplyBy = res.data[0].ApplyBy;
                    $scope.mData.CampaignType = res.data[0].CampaignType;
                    $scope.mData.tCampaign = row.POTypeId == 7 ? 'C' : '3';
                    if((row.POTypeId == 3 && row.CampaignId != null ) && row.RefPOTypeId == 4){
                        $scope.mData.POTypeId = 7;
                    }
                    if($scope.mData.POTypeId == 7){
                        $scope.non_tam_non_sop_bengkel.columnDefs[6].enableCellEdit = $scope.isBundle == true ? false : true;
                    }
                },
                function(err) {
                    console.log("err=>", err);
                });
                $scope.mData.qtyGenerate = null;
                $scope.ModalTambahListCampaign_UIGrid.data = [];
        } // end onShowDetail

        $scope.onDeleteFilter = function(Filter) {

            $scope.Filter.POTypeId = null;
            $scope.Filter.RefPOTypeId = null;
            $scope.Filter.PurchaseOrderStatusId = null;
            $scope.Filter.startDate = null;
            $scope.Filter.endDate = null;
            $scope.Filter.HasilSOQ = null;
            $scope.Filter.RefPONo = null;
            $scope.Filter.LicensePlate = null;
            $scope.Filter.PurchaseOrderNo = null;
            $scope.Filter.VendorName = null;
        }

        // event button 'Lihat Vendor'
        $scope.onShowVendorPO = function() {
            if (typeof $scope.mData.VendorId === undefined || $scope.mData.VendorId == null) {
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Vendor tidak boleh kosong",
                    type: 'danger'
                });
            } else {
                PartsCurrentUser.getVendorDetail($scope.mData.VendorId).then(function(res) {
                        var data = res.data[0];
                        PurchaseOrder.setVendorHeader(data);
                        // Append by Faisal
                        $scope.VendorDetail = data;
                        $scope.isView = true;
                        // =================
                        console.log("===== DATA", data);
                        console.log(PurchaseOrder.getVendorHeader());

                        ngDialog.openConfirm({
                            template: '<div ng-include=\"\'app/parts/purchaseorder/tam_sop_appointment/view_vendor.html\'\"></div>',
                            //templateUrl: '/app/parts/pomaintenance/alasan.html',
                            plain: true,
                            scope:$scope,
                            width: 700,
                            // controller: 'PurchaseOrderController'
                        });

                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
        }

        // $scope.GenerateDocNum = function(){
        //   // [START] Get Current User Warehouse
        //   var UserId;
        //   var DocNo = '';
        //   if (LocalService.get('BSLocalData')) {
        //       LoginObject = angular.fromJson(LocalService.get('BSLocalData')).LoginData;
        //   }
        //   UserId = angular.fromJson(LoginObject).UserId;
        //   PurchaseOrder.getDocumentNumber2().then(function(res) {
        //       DocNo = res.data;
        //       $scope.mData.PurchaseOrderNo = DocNo[0];
        //       $scope.mData.DocDate = DocNo[1]
        //       console.log("Generating DocNo & DocDate ->");
        //       console.log($scope.mData.PurchaseOrderNo);
        //       console.log($scope.mData.DocDate);
        //     },
        //     function(err) {
        //       console.log("err=>", err);
        //     }
        //   );
        //   // [END] Get Current User Warehouse
        // }
        $scope.GenerateDocNumWithPromise = function() {
            var defer = $q.defer();
            PartsCurrentUser.getFormatId($scope.mData.ServiceTypeId, 'PO ', $scope.mData.MaterialTypeId).then(function(res) {
                var result = res.data;
                console.log("FormatId = ", result[0].Results);

                PartsCurrentUser.getDocumentNumber2(result[0].Results).then(function(res) {
                        var DocNo = res.data;

                        console.log("DocNo=>",DocNo);
                        if (typeof DocNo === 'undefined' || DocNo == null) {
                            bsNotify.show({
                                title: "Purchase Order",
                                content: "Format Dokumen tidak ditemukan, silakan hubungi administrator",
                                type: 'danger'
                            });
                            defer.resolve(false);
                            console.log('this is --->',defer);
                        } else {
                            $scope.mData.PurchaseOrderNo = DocNo[0];
                            // $scope.mData.DocDate = DocNo[1];
                            $scope.mData.DocDate = setDate(DocNo[1]);
                            defer.resolve(true);
                            console.log("Generating DocNo & DocDate ->");
                            console.log("DocNo ", $scope.mData.PurchaseOrderNo);
                            console.log("DocDate", $scope.mData.DocDate);
                            console.log('this is --->',defer);
                            
                        }


                    },
                    function(err) {
                        console.log("err=>", err);
                        defer.resolve(false);
                    }
                );


            },
                function(err) {
                    defer.resolve(false);
                }
            );
            return defer.promise;
            // [END] Get Current User Warehouse
        }

        function setDate(dt) {
            var d = "";
            dt = new Date(dt);
            var bln = '0'+(dt.getMonth() + 1);
            var tgl = '0'+(dt.getDate());
            d = dt.getFullYear() + "-" + bln.slice(-2) + "-" + tgl.slice(-2);
            return d;
        }

        PurchaseOrder.searchReference2(1, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, 1).then(function(res) {
            //console.log("List WorkOrder ====>",res);
            $scope.REf2 = res.data;
        })

        $scope.searchReferencedetail1 = function(RefPONo) {
            RefPONo = $scope.mData.RefPONo;
            $scope.onSearchRefPONo(RefPONo)
 
             // PurchaseOrder.searchReferenceDetail(row.PurchaseOrderId, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, detail, $scope.mData.MaterialTypeId, $scope.mData.VendorId, $scope.UserId).then(function(res)
             // {
             //     var AppointmentData = res.data;
             // })
             console.log("Masuk adibbbbb",RefPONo)
         }
         
        $scope.onSearchRefPONo = function(RefPONo) {
            if (RefPONo === "" || RefPONo == null) {
                bsNotify.show({
                    title: "Purchase Order ",
                    content: "Referensi tidak boleh kosong",
                    type: 'danger'
                });

                return;
            }
            $scope.State = 1;
            // PartsCurrentUser.getWarehouseData().then(function(res) {
            //     var loginData = res.data;
            //     console.log(loginData[0]);
            //     $scope.mData.OutletId = loginData[0].OutletId;
            //     $scope.mData.WarehouseId = loginData[0].WarehouseId;
            //     $scope.mData.MaterialTypeId = loginData[0].MaterialTypeId;

            //   },
            //   function(err) {
            //     console.log("err=>", err);
            //   }
            // );
            // Eksekusi Nomor Dokumen
            //tahan tombol simpan
            $scope.checkRefGINo = false;


            //if (($scope.crudState === '111') || ($scope.crudState === '112') || ($scope.crudState === '113') || ($scope.crudState === '141')) {
            if (($scope.crudState === '111') || ($scope.crudState === '113') || ($scope.crudState === '141') || ($scope.crudState === '112')) {
                console.log("crudState ", $scope.crudState);
                // [START] Get Search Appointment
                PurchaseOrder.searchReference(RefPONo, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, 1).then(function(res) {
                        var AppointmentData = res.data[0];
                        $scope.SumTotalAmount();
                        console.log("AppointmentData 111 113 141 112", AppointmentData);
                        $scope.mData.Notes = AppointmentData.Notes;
                        if (typeof AppointmentData === 'undefined' || AppointmentData == null) {
                            bsNotify.show({
                                title: "Purchase Order",
                                content: "No Referensi tidak ditemukan",
                                type: 'danger'
                            });

                            return 0;
                        }

                        if (AppointmentData.RefPo === 0) {
                            bsNotify.show({
                                title: "Purchase Order",
                                //content: "No Referensi sudah digunakan",
                                content: "Harap Cek Kembali Data yang anda Gunakan",
                                type: 'danger'
                            });

                            return 0;
                        }

                        //   if (AppointmentData.isAppointment == 1 && $scope.crudState == '112') {
                        //       bsNotify.show({
                        //           title: "Purchase Order",
                        // //          //content: "No Referensi sudah digunakan",
                        //           content: "Sementara Make SOP - Appointment Dulu Coy",
                        //           type: 'danger'
                        //       });
                        //       $scope.tam_sop_wo.data=[]
                        //       return 0;
                        //   };

                        PurchaseOrder.searchReferenceDetail(RefPONo, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, 1, $scope.mData.MaterialTypeId, $scope.mData.VendorId, $scope.UserId).then(function(res) {
                                var AppointmentData = res.data;
                                console.log('searchReferenceDetail ==>frd')
                                console.log(AppointmentData);
                                $scope.SumTotalAmount();
                                for (var i in AppointmentData) {
                                    // AppointmentData[i].StatusTPOS = $scope.StatusTPOS(AppointmentData[i].StatusTPOS);
                                    // AppointmentData[i].TotalAmount = (AppointmentData[i].QtyPO * AppointmentData[i].UnitPrice);
                                    if (AppointmentData[i].ETA != null) {

                                //            if (AppointmentData[i].isAppointment == 1 && $scope.crudState == '112' ) {
                                //       bsNotify.show({
                                //           title: "Purchase Order",
                                //           //content: "No Referensi sudah digunakan",
                                //           content: "Sementara Make SOP - Appointment Dulu Coy",
                                //           type: 'danger'
                                //       });
                                //       $scope.tam_sop_wo.data=[]
                                //       return 0;
                                //   };

                                        var eta = AppointmentData[i].ETA;
                                        eta = new Date(eta);
                                        // var etaa
                                        // var yyyyeta = eta.getFullYear().toString();
                                        // var mmeta = (eta.getMonth() + 1).toString(); // getMonth() is zero-based
                                        // var ddeta = eta.getDate().toString();
                                        // etaa = (ddeta[1] ? ddeta : "0" + ddeta[0]) + '/' + (mmeta[1] ? mmeta : "0" + mmeta[0]) + '/' + yyyyeta;
                                        // console.log("changeFormatDate finalDate", etaa);
                                        AppointmentData[i].ETA = eta;
                                    } else {
                                        AppointmentData[i].ETA = null;
                                    }
                                    AppointmentData[i].CreatedDate = $scope.localeDate(AppointmentData[i].CreatedDate);
                                }

                                for (var i in AppointmentData) {
                                    if (AppointmentData[i].ETD != null) {
                                        var ETD = AppointmentData[i].ETD;
                                        ETD = new Date(ETD);
                                        // var ETDa
                                        // var yyyyETD = ETD.getFullYear().toString();
                                        // var mmETD = (ETD.getMonth() + 1).toString(); // getMonth() is zero-based
                                        // var ddETD = ETD.getDate().toString();
                                        // ETDa = (ddETD[1] ? ddETD : "0" + ddETD[0]) + '/' + (mmETD[1] ? mmETD : "0" + mmETD[0]) + '/' + yyyyETD;
                                        // console.log("changeFormatDate finalDate", ETDa);
                                        AppointmentData[i].ETD = ETD;
                                    } else {
                                        AppointmentData[i].ETD = null;
                                    }
                                }
                                for(var i in AppointmentData ){
                                    if(AppointmentData[i].StandardCost > AppointmentData[i].UnitPrice){
                                        AppointmentData[i].VATAmount = (AppointmentData[i].StandardCost - AppointmentData[i].UnitPrice) * AppointmentData[i].QtyPO;
                                    }
                                    //diminta BU dijadiin - 
                                    AppointmentData[i].ETD = '-';
                                    AppointmentData[i].ETA = '-';
                                }
                                $scope.tam_sop_appointment.data = AppointmentData;
                                $scope.non_tam_sop_so.data = AppointmentData;
                                // $scope.gridOptionsView.data = AppointmentData;
                                $scope.mData.GridDetail = AppointmentData;
                                var getDataParameter = [];
                                var tempParameter = [5];
                                var todayDate = new Date();
                                var checkHours = todayDate.getHours(),
                                    hour;
                                //condition to set currentTime
                                if (checkHours < 10) {
                                    hour = '0' + todayDate.getHours();
                                } else {
                                    hour = todayDate.getHours();
                                }
                                var currentTime = hour + ":" + todayDate.getMinutes() + ":" + todayDate.getSeconds();
                                var ValidThru = '';
                                var EffectiveDate = '';
                                var OrderTypeId = 0;
                                var CutOffTime = '';
                                var ETDDayPlus = '';
                                var ETDHour = '';
                                var LeadTimeArrival = '';
                                var ETDTemp = '',
                                    ETD = '',
                                    ETDFix = '';
                                var ETATemp = '',
                                    ETATempHour = '',
                                    ETAFix = '',
                                    ETAETO = '';
                                OrderLeadTime.getData().then(
                                    function(res) {
                                        getDataParameter = res.data.Result;
                                        console.log('OrderLeadTime ==>', getDataParameter)
                                        var indexOfValidThru = 0;
                                        ValidThru = new Date(getDataParameter[0].ValidThru);
                                        EffectiveDate = new Date(getDataParameter[0].EffectiveDate);
                                        console.log("getDataParameter: ", getDataParameter);
                                        console.log("currentTime: ", currentTime);
                                        console.log("todayDate: ", todayDate);
                                        console.log('initype', $scope.mData.POTypeId);
                                        if ($scope.mData.POTypeId == 1) {
                                            if (ValidThru >= todayDate && EffectiveDate <= todayDate) {
                                                var i = 0;
                                                while (getDataParameter.length > 0 && i < getDataParameter.length) {
                                                    console.log(i, " lll ", getDataParameter[i].OrderTypeId);
                                                    if (getDataParameter[i].OrderTypeId == 1 && $scope.mData.POTypeId == 1) {
                                                        // if (getDataParameter[i].OrderTypeId==1||getDataParameter[i].OrderTypeId==2||getDataParameter[i].OrderTypeId==3){

                                                        //not today ETD
                                                        if (currentTime >= getDataParameter[i].CutOffTime) {
                                                            ETDTemp = new Date(todayDate);
                                                            ETDTemp.setDate(todayDate.getDate() + getDataParameter[i].ETDDayPlus);
                                                            console.log("b");
                                                        } else {
                                                            break;
                                                            ETDTemp = todayDate;
                                                            console.log("test hari");
                                                            
                                                        }
                                                        ETD = $scope.changeFormatDate(ETDTemp);
                                                        ETDFix = ETD + ' ' + getDataParameter[i].ETDHour;
                                                        var ETAtanggal = new Date(ETDFix);
                                                        ETATempHour = ETAtanggal.getHours();
                                                        ETATemp = ETATempHour + getDataParameter[i].LeadTimeArrival;
                                                        // if (ETATemp>24){
                                                        //   ETAETO = ETATemp-24;
                                                        //   if(ETAETO<10){
                                                        //     ETAETO='0'+ETAETO;
                                                        //   }
                                                        //   else{
                                                        //     ETAETO=ETAETO;
                                                        //   }
                                                        //   ETAtanggal = ETAtanggal.setDate(ETD.getDate()+1);
                                                        //   console.log('ETA tanggal', ETAtanggal);
                                                        // }
                                                        var ETAminit = ETAtanggal.getMinutes();
                                                        if (ETAminit < 10) {
                                                            ETAminit = '0' + ETAminit;
                                                        } else {
                                                            ETAminit = ETAminit;
                                                        }
                                                        var ETAdetik = ETAtanggal.getSeconds();
                                                        if (ETAdetik < 10) {
                                                            ETAdetik = '0' + ETAdetik;
                                                        } else {
                                                            ETAdetik = ETAdetik;
                                                        }
                                                        ETAFix = ETD + ' ' + ETATemp + ':' + ETAminit + ':' + ETAdetik;

                                                        console.log(i, "ETAFiax", ETAFix);
                                                        console.log(i, "CutOffTime", getDataParameter[i].CutOffTime);
                                                        console.log(i, "ETDTemp", ETDTemp);
                                                        console.log(i, "ETD", ETD + getDataParameter[i].ETDHour);
                                                        console.log(i, "ETDDayPlus", getDataParameter[i].ETDDayPlus);
                                                        // break;
                                                    } else {
                                                        console.log("data percobaan 1");
                                                    }
                                                    i++;
                                                }
                                            }
                                        } else if ($scope.mData.POTypeId == 2) {
                                            //   console.log('ini yang 2');
                                            //
                                            if (ValidThru >= todayDate && EffectiveDate <= todayDate) {
                                                var i = 0;
                                                while (getDataParameter.length > 0 && i < getDataParameter.length) {
                                                    console.log(i, " lll ", getDataParameter[i].OrderTypeId);
                                                    if (getDataParameter[i].OrderTypeId == 2 && $scope.mData.POTypeId == 2) {
                                                        // if (getDataParameter[i].OrderTypeId==1||getDataParameter[i].OrderTypeId==2||getDataParameter[i].OrderTypeId==3){

                                                        //not today ETD
                                                        if (currentTime >= getDataParameter[i].CutOffTime) {
                                                            ETDTemp = new Date(todayDate);
                                                            ETDTemp.setDate(todayDate.getDate() + getDataParameter[i].ETDDayPlus);
                                                            console.log("b");
                                                        } else {
                                                            break;
                                                            ETDTemp = todayDate;
                                                            console.log("test hari");
                                                        }
                                                        ETD = $scope.changeFormatDate(ETDTemp);
                                                        ETDFix = ETD + ' ' + getDataParameter[i].ETDHour;
                                                        var ETAtanggal = new Date(ETDFix);
                                                        ETATempHour = ETAtanggal.getHours();
                                                        ETATemp = ETATempHour + getDataParameter[i].LeadTimeArrival;
                                                        if (ETATemp>24){
                                                        
                                                          var ETAETO = ETATemp - 24 ;
                                                          if(ETAETO<10){
                                                            ETAETO='0'+ETAETO;
                                                          }
                                                          else{
                                                            ETAETO=ETAETO;
                                                          }
                                                          ETATemp = ETAETO
                                                          ETAtanggal = ETAtanggal.setDate(ETD.getDate()+1);
                                                          console.log('ETA tanggal', ETAtanggal);
                                                        }
                                                        var ETAminit = ETAtanggal.getMinutes();
                                                        if (ETAminit < 10) {
                                                            ETAminit = '0' + ETAminit;
                                                        } else {
                                                            ETAminit = ETAminit;
                                                        }
                                                        var ETAdetik = ETAtanggal.getSeconds();
                                                        if (ETAdetik < 10) {
                                                            ETAdetik = '0' + ETAdetik;
                                                        } else {
                                                            ETAdetik = ETAdetik;
                                                        }
                                                        ETAFix = ETD + ' ' + ETATemp + ':' + ETAminit + ':' + ETAdetik;

                                                        console.log(i, "ETAFixs", ETAFix);
                                                        console.log(i, "CutOffTime", getDataParameter[i].CutOffTime);
                                                        console.log(i, "ETDTemp", ETDTemp);
                                                        console.log(i, "ETD", ETD + getDataParameter[i].ETDHour);
                                                        console.log(i, "ETDDayPlus", getDataParameter[i].ETDDayPlus);
                                                        // break;
                                                    } else {
                                                        console.log("data percobaan 2");
                                                    }
                                                    i++;
                                                }
                                            } else {
                                                console.log("not valid");
                                            }
                                        } else if ($scope.mData.POTypeId == 3 || $scope.mData.POTypeId == 6) {
                                            //   console.log('ini yang 2');
                                            //
                                            if (ValidThru >= todayDate && EffectiveDate <= todayDate) {
                                                var i = 0;
                                                while (getDataParameter.length > 0 && i < getDataParameter.length) {
                                                    console.log(i, " lll ", getDataParameter[i].OrderTypeId);
                                                    if ((getDataParameter[i].OrderTypeId == 3 && $scope.mData.POTypeId == 3) || (getDataParameter[i].OrderTypeId == 6 && $scope.mData.POTypeId == 6)) {
                                                        // if (getDataParameter[i].OrderTypeId==1||getDataParameter[i].OrderTypeId==2||getDataParameter[i].OrderTypeId==3){
                                                        // Dimatikan karena tidak melihat cutofftime dan perhitungannya salah
                                                        //not today ETD
                                                        // if (currentTime >= getDataParameter[i].CutOffTime) {
                                                        //     ETDTemp = new Date(todayDate);
                                                        //     //$scope.ddETD.setDate($scope.ddETD.getDate()+2)
                                                        //     ETDTemp.setDate(todayDate.getDate() + getDataParameter[i].ETDDayPlus);
                                                        //     console.log("b");
                                                        // } else {
                                                        //     ETDTemp = todayDate;
                                                        //     console.log("test hari");
                                                        // }

                                                        ETDTemp = new Date(todayDate);
                                                        ETDTemp.setDate(todayDate.getDate() + getDataParameter[i].ETDDayPlus);
                                                        
                                                        ETD = $scope.changeFormatDate(ETDTemp);

                                                        var ETDHour = getDataParameter[i].ETDHour;
                                                        if (ETDHour>24){
                                                            ETDHour = ETDHour-24;
                                                            if(ETDHour<10){
                                                                ETDHour='0'+ETDHour;
                                                            }
                                                        }
                                                        else{
                                                            ETDHour=ETDHour;
                                                        }
                                                        getDataParameter[i].ETDHour = ETDHour;
                                                        ETDFix = ETD + ' ' + getDataParameter[i].ETDHour;
                                                        var ETAtanggal = new Date(ETDFix);
                                                        ETATempHour = ETAtanggal.getHours();
                                                        ETATemp = ETATempHour + getDataParameter[i].LeadTimeArrival;
                                                        if (ETATemp>24){
                                                            ETATemp = ETATemp-24;
                                                          if(ETATemp<10){
                                                            ETATemp='0'+ETATemp;
                                                          }
                                                          else{
                                                            ETATemp=ETATemp;
                                                          }
                                                        }
                                                        var ETAminit = ETAtanggal.getMinutes();
                                                        if (ETAminit < 10) {
                                                            ETAminit = '0' + ETAminit;
                                                        } else {
                                                            ETAminit = ETAminit;
                                                        }
                                                        var ETAdetik = ETAtanggal.getSeconds();
                                                        if (ETAdetik < 10) {
                                                            ETAdetik = '0' + ETAdetik;
                                                        } else {
                                                            ETAdetik = ETAdetik;
                                                        }

                                                        //Dimatikan karena ETA tidak bisa diprediksi sehingga dimatikan
                                                        ETAFix = ETD + ' ' + ETATemp + ':' + ETAminit + ':' + ETAdetik;
                                                        // ETAFix = ETD;

                                                        console.log(i, "ETAFixq", ETAFix);
                                                        console.log(i, "CutOffTime", getDataParameter[i].CutOffTime);
                                                        console.log(i, "ETDTemp", ETDTemp);
                                                        console.log(i, "ETD", ETD + getDataParameter[i].ETDHour);
                                                        console.log(i, "ETDDayPlus", getDataParameter[i].ETDDayPlus);
                                                        break;
                                                    } else {
                                                        console.log("data percobaan 3");
                                                    }
                                                    i++;
                                                }
                                            } else {
                                                console.log("not valid");
                                            }
                                        }
                                        console.log("crudState:" + $scope.crudState);
                                        PurchaseOrder.crudState = $scope.crudState;
                                        if ($scope.crudState === '111' || $scope.crudState === '112') {
                                            _.map($scope.tam_sop_appointment.data, function(val) {
                                                val.ETD = ETDFix;
                                                val.ETA = ETAFix;
                                            });
                                            //   $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].ETD = ETDFix;
                                            //   $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].ETA = ETAFix;
                                            console.log("next to you1" , $scope.mData.GridDetail);
                                            $scope.mData.GridDetail = $scope.tam_sop_appointment.data;

                                        }

                                        if ($scope.crudState === '113') {
                                            _.map($scope.non_tam_sop_so.data, function(val) {
                                                val.ETD = ETDFix ;
                                                val.ETA = ETAFix;
                                            });
                                            //   $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].ETD = ETDFix;
                                            //   $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].ETA = ETAFix;
                                            console.log("next to you" , $scope.mData.GridDetail);
                                            $scope.mData.GridDetail = $scope.non_tam_sop_so.data;
                                           

                                        }
                                        //sini ma
                                        if ($scope.crudState === '141'||$scope.crudState === '142'||$scope.crudState === '141') {
                                            $scope.non_tam_sop_so.data = $scope.mData.GridDetail;
                                            $scope.non_tam_sop_wo.data = $scope.mData.GridDetail;
                                            console.log("okkk" , AppointmentData);
                                            console.log("next to you" , $scope.mData.GridDetail);
                                            $scope.mData.GridDetail = AppointmentData;

                                        }
                                        $scope.SumTotalAmount();

                                    })

                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );

                        // add data into model
                        $scope.mData.ColorId = AppointmentData.ColorId;
                        $scope.mData.MachineNo = AppointmentData.VIN;
                        $scope.mData.NoPol = AppointmentData.NoPol;
                        $scope.mData.Year = AppointmentData.Year;
                        $scope.mData.VehicleId = AppointmentData.VehicleId;
                        $scope.mData.VehicleModelId = AppointmentData.VehicleModelId;
                        $scope.mData.FullModel = AppointmentData.FullModel;
                        $scope.mData.VehicleColourCode = AppointmentData.VehicleColourCode;
                        $scope.mData.ModelName = AppointmentData.ModelName;
                        $scope.mData.Name = AppointmentData.Name;
                        $scope.mData.FrameNo = AppointmentData.FrameNo;
                        $scope.mData.AppointmentDate = AppointmentData.AppointmentDate;
                        $scope.mData.AppointmentTime = AppointmentData.AppointmentTime;
                        $scope.mData.AppointmentNo = AppointmentData.AppointmentNo;
                        //Tire
                        if($scope.mData.POTypeId == 6){
                            $scope.mData.NecessaryDate = AppointmentData.NecessaryDate;
                            var tempSplitTime = AppointmentData.NecessaryTime.split(':');
    
                            var tempTime = {    
                                value: new Date($scope.mData.NecessaryDate.getFullYear(), $scope.mData.NecessaryDate.getMonth(), 
                                $scope.mData.NecessaryDate.getDate(), tempSplitTime[0], 
                                tempSplitTime[1], 0)
                                };
                            $scope.mData.NecessaryTime = tempTime.value;

                            var dateNowT = new Date();
                                dateNowT.setHours(0,0,0);
                                
                            var dateNecessaryDate = angular.copy(AppointmentData.NecessaryDate)
                                dateNecessaryDate.setHours(0,0,0);
                                
                            if(dateNecessaryDate > dateNowT){
                                $scope.mData.TireOrderId = 2 //booking
                            }else{
                                $scope.mData.TireOrderId = 1 //today
                            }
                        }

                        if ($scope.crudState == '141') {
                            $scope.mData.BackOrderFollowId = 1; //1:Kill
                        } else {
                            $scope.mData.BackOrderFollowId = 9; //9:Fill
                        }

                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
                // // [END] Get Search Appointment

                // [START] Get Search Appointment Grid Detail dipindah ke atas ddg 06012018
                // PurchaseOrder.searchReferenceDetail(RefPONo, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, 1, $scope.mData.MaterialTypeId).then(function(res) {
                //     var AppointmentData = res.data;
                //     console.log('searchReferenceDetail ==>')
                //     console.log(AppointmentData);

                //     $scope.tam_sop_appointment.data = AppointmentData;
                //     // $scope.gridOptionsView.data = AppointmentData;
                //     $scope.mData.GridDetail = AppointmentData;

                //   },
                //   function(err) {
                //     console.log("err=>", err);
                //   }
                // );
                // [END] Get Search Appointment Grid
            } else if ($scope.crudState === '112') {
                // [START] Get Search Appointment
                PurchaseOrder.searchReference(RefPONo, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, 1).then(function(res) {
                    $scope.SumTotalAmount();
                        var AppointmentData = res.data[0];
                        console.log("AppointmentData ", AppointmentData);
                        $scope.mData.Notes = AppointmentData.Notes;

                        if (typeof AppointmentData === 'undefined' || AppointmentData == null) {
                            bsNotify.show({
                                title: "Purchase Order",
                                content: "No Referensi tidak ditemukan",
                                type: 'danger'
                            });

                            return 0;
                        }

                         if (AppointmentData.isAppointment == 1) {
                             bsNotify.show({
                                 title: "Purchase Order",
                                 //content: "No Referensi sudah digunakan",
                                 content: "Kesalahan Refrensi PO, Silahkan Masuk Kembali Menggunakan SOP - Appointment ",
                                 type: 'danger'
                             });

                             console.log("siniiii",$scope.tam_sop_wo.data)
                             console.log("siniiii",$scope.tam_sop_appointment.data)
                             console.log("siniiii", $scope.non_tam_non_sop_bengkel.data)
                             console.log("siniiii", $scope.non_tam_non_sop.data)
                             console.log("siniiii", $scope.non_tam_sop_wo.data)
                             console.log("siniiii",  $scope.sublet_sop_wo.data)
                             console.log("siniiii",  $scope.non_tam_sop_so.data)
                             $scope.tam_sop_wo.data=[]
                             //$scope.resetForm();
                             return 0;
                         };

                        //masukkan ke dalam model
                        $scope.mData.ColorId = AppointmentData.ColorId;
                        $scope.mData.MachineNo = AppointmentData.VIN;
                        $scope.mData.NoPol = AppointmentData.NoPol;
                        $scope.mData.Year = AppointmentData.Year;
                        $scope.mData.VehicleId = AppointmentData.VehicleId;
                        $scope.mData.VehicleModelId = AppointmentData.VehicleModelId;
                        $scope.mData.FullModel = AppointmentData.FullModel;
                        $scope.mData.VehicleColourCode = AppointmentData.VehicleColourCode;
                        $scope.mData.ModelName = AppointmentData.ModelName;
                        $scope.mData.Name = AppointmentData.Name;
                        $scope.mData.FrameNo = AppointmentData.FrameNo;
                        $scope.mData.AppointmentDate = AppointmentData.AppointmentDate;
                        $scope.mData.AppointmentTime = AppointmentData.AppointmentTime;
                        $scope.mData.AppointmentNo = AppointmentData.AppointmentNo;

                        // [START] Get Search Appointment Grid Detail
                        PurchaseOrder.searchReferenceDetail(RefPONo, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, 1, $scope.mData.MaterialTypeId, $scope.mData.VendorId, $scope.UserId).then(function(res) {
                            $scope.SumTotalAmount();
                            var AppointmentData = res.data;
                                console.log('searchReferenceDetail ==>aa')
                                console.log(AppointmentData);

                                for (var i in AppointmentData) {
                                // AppointmentData[i].StatusTPOS = $scope.StatusTPOS(AppointmentData[i].StatusTPOS);
                                    if (AppointmentData[i].ETA != null) {

                                    //    if (AppointmentData[i].isAppointment == 0) {
                                    //        console.log("siniiii", $scope.tam_sop_wo.data)
                                    //        bsNotify.show({
                                    //            title: "Purchase Order",
                                    //            //content: "No Referensi sudah digunakan",
                                    //            content: "Sementara Make SOP - Appointment Dulu Coy",
                                    //            type: 'danger'
                                    //        });
                                    //        $scope.tam_sop_wo.data=[]
                                    //        return 0;
                                    //    };

                                        var eta = AppointmentData[i].ETA;
                                        eta = new Date(eta);
                                    //  var etaa
                                    //  var yyyyeta = eta.getFullYear().toString();
                                    //  var mmeta = (eta.getMonth() + 1).toString(); // getMonth() is zero-based
                                    //  var ddeta = eta.getDate().toString();
                                    //  etaa = (ddeta[1] ? ddeta : "0" + ddeta[0]) + '/' + (mmeta[1] ? mmeta : "0" + mmeta[0]) + '/' + yyyyeta;
                                    //  console.log("changeFormatDate finalDate", etaa);
                                        AppointmentData[i].ETA = eta;
                                    } else {
                                        AppointmentData[i].ETA = null;
                                    }
                                    AppointmentData[i].CreatedDate = $scope.localeDate(AppointmentData[i].CreatedDate);
                                }

                                for (var i in AppointmentData) {
                                    if (AppointmentData[i].ETD != null) {
                                        var ETD = AppointmentData[i].ETD;
                                        ETD = new Date(ETD);
                                    //  var ETDa
                                    //  var yyyyETD = ETD.getFullYear().toString();
                                    //  var mmETD = (ETD.getMonth() + 1).toString(); // getMonth() is zero-based
                                    //  var ddETD = ETD.getDate().toString();
                                    //  ETDa = (ddETD[1] ? ddETD : "0" + ddETD[0]) + '/' + (mmETD[1] ? mmETD : "0" + mmETD[0]) + '/' + yyyyETD;
                                    //  console.log("changeFormatDate finalDate", ETDa);
                                        AppointmentData[i].ETD = ETD;
                                    } else {
                                        AppointmentData[i].ETD = null;
                                    }
                                }
                                for(var i in AppointmentData ){
                                if(AppointmentData[i].StandardCost > AppointmentData[i].UnitPrice){
                                    AppointmentData[i].VATAmount = (AppointmentData[i].StandardCost - AppointmentData[i].UnitPrice) * AppointmentData[i].QtyPO;
                                }
                            }
                                $scope.tam_sop_appointment.data = AppointmentData;
                                // $scope.gridOptionsView.data = AppointmentData;
                                $scope.mData.GridDetail = AppointmentData;
                                $scope.SumTotalAmount();
                            
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                        // [END] Get Search Appointment Grid
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
                // // [END] Get Search Appointment
            } else if ($scope.crudState === '142' || ($scope.crudState === '342')) // 15-12-2017
            {
                console.log("NON TAM SOP WO ==>");

                // [START] Get Search Appointment
                PurchaseOrder.searchReference(RefPONo, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, 1).then(function(res) {
                    $scope.SumTotalAmount();
                        var AppointmentData = res.data[0];
                        console.log(AppointmentData);

                        if (typeof AppointmentData === 'undefined' || AppointmentData == null) {
                            bsNotify.show({
                                title: "Purchase Order",
                                content: "No Referensi tidak ditemukan",
                                type: 'danger'
                            });

                            return 0;
                        }

                        // masukkan ke dalam model
                        $scope.mData.ColorId = AppointmentData.ColorId;
                        $scope.mData.MachineNo = AppointmentData.VIN;
                        $scope.mData.NoPol = AppointmentData.NoPol;
                        $scope.mData.Year = AppointmentData.Year;
                        $scope.mData.VehicleId = AppointmentData.VehicleId;
                        $scope.mData.VehicleModelId = AppointmentData.VehicleModelId;
                        $scope.mData.FullModel = AppointmentData.FullModel;
                        $scope.mData.VehicleColourCode = AppointmentData.VehicleColourCode;
                        $scope.mData.ModelName = AppointmentData.ModelName;
                        $scope.mData.Name = AppointmentData.Name;
                        $scope.mData.FrameNo = AppointmentData.FrameNo;
                        $scope.mData.AppointmentDate = AppointmentData.AppointmentDate;
                        $scope.mData.AppointmentTime = AppointmentData.AppointmentTime;
                        $scope.mData.AppointmentNo = AppointmentData.AppointmentNo;

                        // [START] Get Search Appointment Grid Detail
                        PurchaseOrder.searchReferenceDetail(RefPONo, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, 1, $scope.mData.MaterialTypeId, $scope.mData.VendorId, $scope.UserId).then(function(res) {
                            var AppointmentData = res.data;
                            $scope.SumTotalAmount();
                            console.log('searchReferenceDetail ==>qq')
                            console.log(AppointmentData);

                            for (var i in AppointmentData) {
                                if (AppointmentData[i].ETA != null) {
                                    var eta = AppointmentData[i].ETA;
                                    eta = new Date(eta);
                                    // var etaa
                                    // var yyyyeta = eta.getFullYear().toString();
                                    // var mmeta = (eta.getMonth() + 1).toString(); // getMonth() is zero-based
                                    // var ddeta = eta.getDate().toString();
                                    // etaa = (ddeta[1] ? ddeta : "0" + ddeta[0]) + '/' + (mmeta[1] ? mmeta : "0" + mmeta[0]) + '/' + yyyyeta;
                                    // console.log("changeFormatDate finalDate", etaa);
                                    AppointmentData[i].ETA = eta;
                                } else {
                                    AppointmentData[i].ETA = null;
                                }
                                AppointmentData[i].CreatedDate = $scope.localeDate(AppointmentData[i].CreatedDate);
                            }

                            for (var i in AppointmentData) {
                                if (AppointmentData[i].ETD != null) {
                                    var ETD = AppointmentData[i].ETD;
                                    ETD = new Date(ETD);
                                    // var ETDa
                                    // var yyyyETD = ETD.getFullYear().toString();
                                    // var mmETD = (ETD.getMonth() + 1).toString(); // getMonth() is zero-based
                                    // var ddETD = ETD.getDate().toString();
                                    // ETDa = (ddETD[1] ? ddETD : "0" + ddETD[0]) + '/' + (mmETD[1] ? mmETD : "0" + mmETD[0]) + '/' + yyyyETD;
                                    // console.log("changeFormatDate finalDate", ETDa);
                                    AppointmentData[i].ETD = ETD;
                                } else {
                                    AppointmentData[i].ETD = null;
                                }
                            }
                            var tmpArr = [];
                            for(var i in AppointmentData ){
                                if(AppointmentData[i].StandardCost > AppointmentData[i].UnitPrice){
                                    AppointmentData[i].VATAmount = (AppointmentData[i].StandardCost - AppointmentData[i].UnitPrice) * AppointmentData[i].QtyPO;
                                }
                                // eta & etd diminta bu jadi -
                                AppointmentData[i].ETA = '-';
                                AppointmentData[i].ETD = '-';
                            }
                            $scope.tam_sop_appointment.data = AppointmentData;
                            $scope.non_tam_sop_wo.data = AppointmentData;
                            $scope.non_tam_sop_so.data = AppointmentData;
                            // $scope.gridOptionsView.data = AppointmentData;
                            $scope.mData.GridDetail = AppointmentData;
                            $scope.SumTotalAmount();

                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
                // // [END] Get Search Appointment
            } else if ($scope.crudState === '152' || $scope.crudState === '151') {
                console.log("Sublet SOP WO ==>");
                // [START] Get Search Appointment
                PurchaseOrder.searchReference(RefPONo, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, 1).then(function(res) {
                    $scope.SumTotalAmount();
                        var AppointmentData = res.data[0];
                        console.log(AppointmentData);

                        if (typeof AppointmentData === 'undefined' || AppointmentData == null) {
                            bsNotify.show({
                                title: "Purchase Order",
                                content: "No Referensi tidak ditemukan",
                                type: 'danger'
                            });

                            return 0;
                        }

                        // add data into model
                        $scope.mData.ColorId = AppointmentData.ColorId;
                        $scope.mData.MachineNo = AppointmentData.VIN;
                        $scope.mData.NoPol = AppointmentData.NoPol;
                        $scope.mData.Year = AppointmentData.Year;
                        $scope.mData.VehicleId = AppointmentData.VehicleId;
                        $scope.mData.VehicleModelId = AppointmentData.VehicleModelId;
                        $scope.mData.FullModel = AppointmentData.FullModel;
                        $scope.mData.VehicleColourCode = AppointmentData.VehicleColourCode;
                        $scope.mData.ModelName = AppointmentData.ModelName;
                        $scope.mData.Name = AppointmentData.Name;
                        $scope.mData.FrameNo = AppointmentData.FrameNo;
                        $scope.mData.AppointmentDate = AppointmentData.AppointmentDate;
                        $scope.mData.AppointmentTime = AppointmentData.AppointmentTime;
                        $scope.mData.AppointmentNo = AppointmentData.AppointmentNo;
                        $scope.mData.Notes = AppointmentData.Notes;

                        // [START] Get Search Appointment Grid Detail
                        PurchaseOrder.searchReferenceDetail(RefPONo, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, 1, $scope.mData.MaterialTypeId, $scope.mData.VendorId, $scope.UserId).then(function(res) {
                            var AppointmentData = res.data;
                                console.log('searchReferenceDetail ==>tt')
                                console.log(AppointmentData);

                                $scope.SumTotalAmount();
                                for (var i in AppointmentData) {
                                    //AppointmentData[i].TotalAmount = (AppointmentData[i].QtyPO * AppointmentData[i].UnitPrice);
                                    if (AppointmentData[i].ETA != null) {
                                        var eta = AppointmentData[i].ETA;
                                        eta = new Date(eta);
                                        // var etaa
                                        // var yyyyeta = eta.getFullYear().toString();
                                        // var mmeta = (eta.getMonth() + 1).toString(); // getMonth() is zero-based
                                        // var ddeta = eta.getDate().toString();
                                        // etaa = (ddeta[1] ? ddeta : "0" + ddeta[0]) + '/' + (mmeta[1] ? mmeta : "0" + mmeta[0]) + '/' + yyyyeta;
                                        // console.log("changeFormatDate finalDate", etaa);
                                        AppointmentData[i].ETA = eta;
                                    } else {
                                        AppointmentData[i].ETA = null;
                                    }
                                    AppointmentData[i].CreatedDate = $scope.localeDate(AppointmentData[i].CreatedDate);
                                }

                                for (var i in AppointmentData) {
                                    if (AppointmentData[i].ETD != null) {
                                        var ETD = AppointmentData[i].ETD;
                                        ETD = new Date(ETD);
                                        // var ETDa
                                        // var yyyyETD = ETD.getFullYear().toString();
                                        // var mmETD = (ETD.getMonth() + 1).toString(); // getMonth() is zero-based
                                        // var ddETD = ETD.getDate().toString();
                                        // ETDa = (ddETD[1] ? ddETD : "0" + ddETD[0]) + '/' + (mmETD[1] ? mmETD : "0" + mmETD[0]) + '/' + yyyyETD;
                                        // console.log("changeFormatDate finalDate", ETDa);
                                        AppointmentData[i].ETD = ETD;
                                    } else {
                                        AppointmentData[i].ETD = null;
                                    }
                                }
                                $scope.sublet_sop_wo.data = AppointmentData;
                                $scope.non_tam_sop_so.data = AppointmentData;
                                $scope.mData.GridDetail = AppointmentData;
                                $scope.SumTotalAmount();

                                console.log('$scope.mData.GridDetail',$scope.mData.GridDetail);

                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
                // // [END] Get Search Appointment
            } else if (($scope.crudState === '143') || ($scope.crudState === '113') || ($scope.crudState === '123') || ($scope.crudState === '133')) {
                console.log("NON SOP SALES ORDER ==>");
                // [START] Get Search Appointment
                PurchaseOrder.searchReference(RefPONo, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, 1).then(function(res) {
                    $scope.SumTotalAmount();
                        var AppointmentData = res.data[0];
                        console.log(AppointmentData);

                        if (typeof AppointmentData === 'undefined' || AppointmentData == null) {
                            $scope.vcState = 'hidden';

                            bsNotify.show({
                                title: "Purchase Order",
                                content: "No Referensi tidak ditemukan",
                                type: 'danger'
                            });

                            return 0;
                        }

                        if (AppointmentData.RefSOTypeId == 1) {
                            $scope.vcState = 'bengkel';
                        }

                        if (AppointmentData.RefSOTypeId == 2) {
                            $scope.vcState = 'customer';
                        }
                        // masukkan ke dalam model
                        $scope.mData.BengkelName = AppointmentData.BengkelName;
                        $scope.mData.DateNeeded = AppointmentData.DateNeeded;
                        $scope.mData.CustomerName = AppointmentData.CustomerName;
                        $scope.mData.CustomerId = AppointmentData.CustomerId;
                        $scope.mData.CreatedDate = AppointmentData.CreatedDate;
                        $scope.mData.CreatedUserId = AppointmentData.CreatedUserId;

                        // [START] Get Search Appointment Grid Detail
                        PurchaseOrder.searchReferenceDetail(RefPONo, $scope.mData.POTypeId, $scope.mData.RefPOTypeId, 1, $scope.mData.MaterialTypeId, $scope.mData.VendorId, $scope.UserId).then(function(res) {
                            $scope.SumTotalAmount();
                            var AppointmentData = res.data;
                                console.log('searchReferenceDetail ==>ww')
                                console.log(AppointmentData);

                                for (var i in AppointmentData) {
                                    if (AppointmentData[i].ETA != null) {
                                        var eta = AppointmentData[i].ETA;
                                        eta = new Date(eta);
                                        // var etaa
                                        // var yyyyeta = eta.getFullYear().toString();
                                        // var mmeta = (eta.getMonth() + 1).toString(); // getMonth() is zero-based
                                        // var ddeta = eta.getDate().toString();
                                        // etaa = (ddeta[1] ? ddeta : "0" + ddeta[0]) + '/' + (mmeta[1] ? mmeta : "0" + mmeta[0]) + '/' + yyyyeta;
                                        // console.log("changeFormatDate finalDate", etaa);
                                        AppointmentData[i].ETA = eta;
                                    } else {
                                        AppointmentData[i].ETA = null;
                                    }
                                    AppointmentData[i].CreatedDate = $scope.localeDate(AppointmentData[i].CreatedDate);
                                }

                                for (var i in AppointmentData) {
                                    if (AppointmentData[i].ETD != null) {
                                        var ETD = AppointmentData[i].ETD;
                                        ETD = new Date(ETD);
                                        // var ETDa
                                        // var yyyyETD = ETD.getFullYear().toString();
                                        // var mmETD = (ETD.getMonth() + 1).toString(); // getMonth() is zero-based
                                        // var ddETD = ETD.getDate().toString();
                                        // ETDa = (ddETD[1] ? ddETD : "0" + ddETD[0]) + '/' + (mmETD[1] ? mmETD : "0" + mmETD[0]) + '/' + yyyyETD;
                                        // console.log("changeFormatDate finalDate", ETDa);
                                        AppointmentData[i].ETD = ETD;
                                    } else {
                                        AppointmentData[i].ETD = null;
                                    }
                                    //diminta BU dijadiin -
                                    AppointmentData[i].ETD = '-';
                                    AppointmentData[i].ETA = '-'; 
                                }
                                $scope.non_tam_sop_so.data = AppointmentData;
                                // $scope.gridOptionsView.data = AppointmentData;
                                $scope.mData.GridDetail = AppointmentData;
                                $scope.SumTotalAmount();
                                console.log("$scope.mdata.GridDetail ============>", AppointmentData, $scope.mData.GridDetail);

                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
                // // [END] Get Search Appointment
            }
            console.log('mdata nih', $scope.mData);
        }

        // Fungsi semi-generik untuk handling form berdasarkan input
        $scope.formShowHandler = function(po, ref) {
                $scope.formState = '';
                console.log("[formShowHandler] " + po + " " + ref);
                //---------------------
                //TAM Form Code: 11
                //---------------------
                if ((po === "PO TAM Tipe Order 1" && ref === "SOP - Appointment") || (po === 1 && ref === 1) || (po === '1' && ref === '1')) {
                    $scope.formState = '11';
                }

                // TAM 1 Sales Order
                else if ((po === "PO TAM Tipe Order 1" && ref === "SOP-Sales Order") || (po === 1 && ref === 3) || (po === '1' && ref === '3')) {
                    $scope.formState = '13';
                } else if ((po === "PO TAM Tipe Order 2" && ref === "SOP-Sales Order") || (po === 2 && ref === 3) || (po === '2' && ref === '3')) {
                    $scope.formState = '13';
                } else if ((po === "PO TAM Tipe Order 3" && ref === "SOP-Sales Order") || (po === 3 && ref === 3) || (po === '3' && ref === '3')) {
                    $scope.formState = '13';
                }
                // TAM 1 NON SOP
                else if ((po === "PO TAM Tipe Order 1" && ref === "NON SOP") || (po === 1 && ref === 4) || (po === '1' && ref === '4')) {
                    $scope.formState = '14';
                    console.log('onOverlayMode | 14 dari TAM 1 NON SOP');
                }
                // TAM 2 NON SOP
                else if ((po === "PO TAM Tipe Order 2" && ref === "NON SOP") || (po === 2 && ref === 4) || (po === '2' && ref === '4')) {
                    $scope.formState = '14';
                    console.log('onOverlayMode | 14 dari TAM 2 NON SOP');
                }
                // TAM 3 NON SOP
                else if ((po === "PO TAM Tipe Order 3" && ref === "NON SOP") || (po === 3 && ref === 4) || (po === '3' && ref === '4')) {
                    $scope.formState = '14';
                    console.log('onOverlayMode | 14 dari TAM 3 NON SOP');
                } else if ((po === "PO TAM Tipe Order 2" && ref === "SOP - Appointment") || (po === 2 && ref === 1) || (po === '2' && ref === '1')) {
                    $scope.formState = '11';
                } else if ((po === "PO TAM Tipe Order 3" && ref === "SOP - Appointment") || (po === 3 && ref === 1) || (po === '3' && ref === '1')) {
                    $scope.formState = '11';
                }
                // WO
                else if ((po === "PO TAM Tipe Order 1" && ref === "SOP-WO") || (po === 1 && ref === 2) || (po === '1' && ref === '2')) {
                    $scope.formState = '12';
                } else if ((po === "PO TAM Tipe Order 2" && ref === "SOP-WO") || (po === 2 && ref === 2) || (po === '2' && ref === '2')) {
                    $scope.formState = '12';
                } else if ((po === "PO TAM Tipe Order 3" && ref === "SOP-WO") || (po === 3 && ref === 2) || (po === '3' && ref === '2')) {
                    $scope.formState = '12';
                }
                //SOQ
                else if ((po === "PO TAM Tipe Order 3" && ref === "NON SOP") || (po === 3 && ref === 4) || (po === '3' && ref === '4')) {
                    $scope.formState = '34';
                }
                //---------------------
                //NON TAM Form Code: 42
                //---------------------
                else if ((po === "PO NON TAM" && ref === "SOP - Appointment") || (po === 4 && ref === 1) || (po === '4' && ref === '1')) {
                    $scope.formState = '41';
                } else if ((po === "PO NON TAM" && ref === "SOP-WO") || (po === 4 && ref === 2) || (po === '4' && ref === '2')) {
                    $scope.formState = '42';
                } else if ((po === "PO NON TAM" && ref === "NON SOP") || (po === 4 && ref === 4) || (po === '4' && ref === '4')) {
                    $scope.formState = '44';
                } else if ((po === "PO NON TAM" && ref === "SOP-Sales Order") || (po === 4 && ref === 3) || (po === '4' && ref === '3')) {
                    $scope.formState = '43';
                }
                //---------------------
                //SUBLET Form Code: 51
                //---------------------
                else if ((po === "PO SUBLET" && ref === "SOP - Appointment") || (po === 5 && ref === 1) || (po === '5' && ref === '1')) {
                    // $scope.formState = '11';
                    $scope.formState = '51';
                } else if ((po === "PO SUBLET" && ref === "SOP-WO") || (po === 5 && ref === 2) || (po === '5' && ref === '2')) {
                    $scope.formState = '52';
                } else if ((po === "PO SUBLET" && ref === "NON SOP") || (po === 5 && ref === 4) || (po === '5' && ref === '4')) {
                    $scope.formState = '14';
                    console.log('onOverlayMode | 14 dari SUBLET Form Code: 51');
                } 

                else if ((po === "PO TAM Tipe Order T" && ref === "SOP - Appointment") || (po === 6 && ref === 1) || (po === '6' && ref === '1')) {
                    $scope.formState = '11';
                } else if ((po === "PO TAM Tipe Order T" && ref === "SOP-WO") || (po === 6 && ref === 2) || (po === '6' && ref === '2')) {
                    $scope.formState = '12';
                } else if ((po === "PO TAM Tipe Order T" && ref === "SOP-Sales Order") || (po === 6 && ref === 3) || (po === '6' && ref === '3')) {
                    $scope.formState = '13';
                }else if((po === "PO TAM Campaign 3 & C" && ref === "NON SOP") || (po === 7 && ref === 4) || (po === '7' && ref === '4')){
                    $scope.formState = '14';
                }else {
                    console.log("[Error]");
                }

                console.log('formState : ', $scope.formState);
                return $scope.formState;
            }
            // ======== tombol  simpan =========
        $scope.SimpanApproval = function(data) {

            console.log("Data untuk Approval ", data);

            $scope.mData = PurchaseOrder.getPurchaseOrderHeader();
            $scope.mData.GridDetail = PurchaseOrder.getPurchaseOrderDetail();

            console.log("mData => ", $scope.mData);
            console.log("mData Detail=> ", $scope.mData.GridDetail);
            if ($scope.user.RoleId==1122 || $scope.user.RoleId==1125) {
                PurchaseOrder.sendNotif(data, 1128, 44).then(
                    function(res) {
                      console.log('terkirim ke KABENG GR')
                    });
                // PurchaseOrder.sendNotifRequest(data, 1128, 44).then(
                //     function(res) {
                //         console.log('terkirim ke KABENG GR')
                //     });
            } else {
                PurchaseOrder.sendNotif(data, 1129, 44).then(
                    function(res) {
                      console.log('terkirim ke KABENG BP')
                    });
            }

            for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                $scope.mData.GridDetail[i].PurchaseOrderStatusId = 2; //for approval
                $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
                $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;
                if ($scope.mData.POTypeId >= 1 && $scope.mData.POTypeId <= 3) {
                    $scope.mData.GridDetail[i].StatusTPOS = 1;
                }
            }

            //debugger;
            console.log("Persiapan Create => ");
            console.log("Head ", $scope.mData);
            console.log("Tails ", $scope.mData.GridDetail);
            $scope.GenerateDocNumWithPromise().then(function(resultPromise){
                if(resultPromise){
                    console.log(resultPromise);
                    console.log("$scope.mData=>>",$scope.mData);

                    if ($scope.mData.PurchaseOrderNo == 0 || $scope.mData.PurchaseOrderNo == null || $scope.mData.PurchaseOrderNo.includes('$$$')) {
                        console.log("Masuk Sini -> QtyPo > ", $scope.mData.PurchaseOrderNo);
        
                        bsNotify.show({
                            title: "Purchase Order",
                            // content: " Tolong refresh kembali karena Purchase Order No 0. Silahkan klik kembali dan input ulang data yg ingin dipakai",
                            content: "Nomor PO belum terbentuk. Silahkan klik kembali tombol simpan",
                            type: 'warning'
                        });
                        setTimeout(function(){
                            $scope.checkRefGINo = false;
                        },1000);
                 
                        return 0;
                    }
                    console.log("after promise 1 ", $scope.mData);
                    console.log("after promise 2 ", $scope.mData.GridDetail);

                    PurchaseOrder.createApprove($scope.mData, $scope.mData.GridDetail).then(function(res) {
                            var create = res.data.Response?res.data.Response:res.data;
                            console.log(res.data);


                            $scope.alertAfterSave(create);

                            $scope.goBack();
                            $scope.resetForm();
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );

                    $scope.ngDialog.close();

                }else{
                    return;
                }
            });
        }

        $scope.UbahApproval = function(data) {

            console.log("Data untuk Approval ", data);

            $scope.mData = PurchaseOrder.getPurchaseOrderHeader();
            $scope.mData.GridDetail = PurchaseOrder.getPurchaseOrderDetail();

            console.log("mData => ", $scope.mData);
            console.log("mData Detail=> ", $scope.mData.GridDetail);


            for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                $scope.mData.GridDetail[i].PurchaseOrderStatusId = 2; //for approval
                $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
                $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;
            }

            PurchaseOrder.update($scope.mData, $scope.mData.GridDetail).then(function(res) {
                    var create = res.data;
                    console.log(res.data);
                    bsNotify.show({
                        title: "Purchase Order",
                        content: "Data berhasil disimpan",
                        type: 'success'
                    });

                    $scope.goBack();
                    $scope.resetForm();
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

            $scope.ngDialog.close();


        }

        //approve
        $scope.bulkApprove = function(row) {
            console.log("Approved => ", $scope.mData.selected);
            $scope.mData.selected[0].PurchaseOrderStatusId = 3;
            console.log("row data reza", row);
            PartsGlobal.actApproval({
                ProcessId: 8903,
                DataId: row[0].PurchaseOrderId,
                ApprovalStatus: 1
            })

            // else {
            //     PurchaseOrder.sendNotif(data, 1129, 44).then(
            //         function(res) {
            //           console.log('terkirim ke KABENG BP')
            //         });
            // }

            PurchaseOrder.update($scope.mData.selected[0]).then(function(res) {
                    var create = res.data;
                    console.log(res.data);
                    bsNotify.show({
                        title: "Purchase Order ",
                        content: "Data berhasil di-approve",
                        type: 'success'
                    });
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

        }


        // reject
        $scope.bulkReject = function(row) {
            console.log("Rejected => ", $scope.mData.selected);

            PartsGlobal.actApproval({
                ProcessId: 8903,
                DataId: row[0].PurchaseOrderId,
                ApprovalStatus: 2
            })
        }



        $scope.simpanAppointment = function(mdata) {
            PurchaseOrder.NeedApproval = 0; // default tidak perlu approval
            if (
                ($scope.mData.BackOrderFollowId == null || $scope.mData.BackOrderFollowId == 'undefined' || $scope.mData.BackOrderFollowId == "") &&
                $scope.mData.BackOrderFollowId != 0
            ) {
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Periksa kembali data yang kosong",
                    type: 'warning'
                });
            } else if ($scope.mData.VendorId == null || $scope.mData.VendorId == 'undefined' || $scope.mData.VendorId == "") {
                bsNotify.show({
                    title: "Purchase Order",
                    // content: "Periksa kembali data yang kosong",
                    content: "Vendor belum diisi",
                    type: 'warning'
                });
            } else if ($scope.mData.PaymentMethodId == null || $scope.mData.PaymentMethodId == 'undefined' || $scope.mData.PaymentMethodId == "") {
                bsNotify.show({
                    title: "Purchase Order",
                    // content: "Periksa kembali data yang kosong",
                    content: "Metode Pembayaran belum diisi",
                    type: 'warning'
                });
                // } else if ($scope.mData.PaymentPeriod == null || $scope.mData.PaymentPeriod == 'undefined' || $scope.mData.PaymentPeriod == "") {
                //     bsNotify.show({
                //         title: "Purchase Order",
                //         content: "Periksa kembali data yang kosong",
                //         type: 'warning'
                //     });
            } else if ($scope.mData.AppointmentDate == null || $scope.mData.AppointmentDate == 'undefined' || $scope.mData.AppointmentDate == "") {
                bsNotify.show({
                    title: "Purchase Order",
                    // content: "Periksa kembali data yang kosong",
                    content: "Tanggal Appointment belum diisi",
                    type: 'warning'
                });
            } else if(($scope.mData.POTypeId == 6 ||  $scope.mData.POTypeId == "6") && ($scope.mData.TireOrderId == null || $scope.mData.TireOrderId == undefined || $scope.mData.TireOrderId == "")){
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Jenis Order Tire harus diisi",
                    type: 'warning'
                });
            }else if(($scope.mData.POTypeId == 6 ||  $scope.mData.POTypeId == "6") && 
                (!$scope.mData.NecessaryDate || $scope.mData.NecessaryTime == "NaN:NaN:00" || !$scope.mData.NecessaryTime )){
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Tanggal & Waktu Dibutuhkan harus diisi",
                    type: 'warning'
                });
            }else {
                if ($scope.mData.RefPOTypeId == '1' || $scope.mData.RefPOTypeId == 1) {
                    $scope.mData.GridDetail = $scope.tam_sop_appointment.data;
                } else if ($scope.mData.RefPOTypeId == '4' || $scope.mData.RefPOTypeId == 4 || $scope.mData.MaterialTypeId == 2 || $scope.mData.MaterialTypeId == '2') {
                    $scope.mData.GridDetail = $scope.non_tam_non_sop.data;
                } else if ($scope.mData.RefPOTypeId == '4' || $scope.mData.RefPOTypeId == 4) {
                    $scope.mData.GridDetail = $scope.non_tam_non_sop_bengkel.data;
                }


                PurchaseOrder.setPurchaseOrderHeader($scope.mData);
                PurchaseOrder.setPurchaseOrderDetail($scope.mData.GridDetail);

                //comment : kata pak ari cuma NON SOP yang non stock dan mahal aja yang pake approval -- 200180920
                // for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                //     if ($scope.mData.GridDetail[i].PartsClassId4 == 47) {
                //         PurchaseOrder.NeedApproval = 1;
                //         console.log("Need Approval -> PartsClassId4 = 47");
                //     }
                // }

                // if ($scope.mData.TotalAmount > PurchaseOrder.MinTotalAmount) {
                //     PurchaseOrder.NeedApproval = 1;
                //     console.log("Need Approval -> TotalAmount > ", PurchaseOrder.MinTotalAmount);
                // }

                // if (PurchaseOrder.NeedApproval == 1) {
                //     ngDialog.openConfirm({
                //         template: '<div ng-include=\"\'app/parts/purchaseorder/helper/approval.html\'\"></div>',
                //         //templateUrl: '/app/parts/pomaintenance/alasan.html',
                //         plain: true,
                //         controller: 'PurchaseOrderController',
                //     });
                // } else {
                //     console.log("No Need Approval");
                //     $scope.SimpanData($scope.mData);
                // }

                $scope.SimpanData($scope.mData);

            }
        };
        // ======== end tombol  simpan =========
        $scope.ubah = function(mdata) {
            console.log("Simpan edit mdata ====== ", mdata);
            console.log("Simpan edit mData ====== ", $scope.mData);
            console.log("Simpan edit ====== ", $scope.mData.GridDetail);
            console.log("POTypeId ====== ", $scope.mData.POTypeId);
            console.log("RefPOTypeId ====== ", $scope.mData.RefPOTypeId);
            console.log("crudState ====== ", $scope.crudState);
            PurchaseOrder.NeedApproval = 0; // default tidak perlu approval
            if ($scope.mData.POTypeId == '5') {
                $scope.mData.GridDetail = $scope.sublet_sop_wo.data;
            }
            if ($scope.mData.RefPOTypeId == '1' && $scope.crudState == '311') {
                $scope.mData.GridDetail = $scope.tam_sop_appointment_ubah.data;
            }
            if ($scope.mData.RefPOTypeId == '2' && $scope.crudState == '312') {
                $scope.mData.GridDetail = $scope.tam_sop_wo_ubah.data;
            }
            if ($scope.mData.RefPOTypeId == '3' && $scope.crudState == '313') {
                $scope.mData.GridDetail = $scope.non_tam_sop_so_ubah.data;
            }
            if ($scope.mData.RefPOTypeId == '4' && $scope.mData.POTypeId == '1') {
                $scope.mData.GridDetail = $scope.non_tam_non_sop_bengkel.data;
            }
            if ($scope.mData.RefPOTypeId == '4' && $scope.mData.POTypeId == '4') {
                console.log("44 ", $scope.mData.GridDetail);
                $scope.mData.GridDetail = $scope.non_tam_sop_so.data;
            }
            if (($scope.mData.RefPOTypeId == '4' && $scope.mData.MaterialTypeId == '2') || ($scope.mData.RefPOTypeId == '4' && $scope.mData.MaterialTypeId == '1')) {
                console.log("x4x ", $scope.mData.GridDetail);
                $scope.mData.GridDetail = $scope.non_tam_non_sop.data;
            }

            console.log("$scope.mData==> ", $scope.mData);
            console.log("Panjang grid detail ", $scope.mData.GridDetail);
            //for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                for (var i = 0; i < $scope.mData.GridDetail; i++) {
                console.log("Stocking Policy ", $scope.mData.GridDetail[i].PartsClassId4);
                if ($scope.mData.GridDetail[i].PartsClassId4 == 47) {
                    PurchaseOrder.NeedApproval = 1;
                    $scope.mData.PurchaseOrderStatusId = 2;
                    console.log("Need Approval --> PartsClassId4 == 47");
                } else {
                    console.log("Why here");
                }
            }

            if ($scope.mData.TotalAmount > PurchaseOrder.MinTotalAmount) {

                PurchaseOrder.NeedApproval = 1;
                $scope.mData.PurchaseOrderStatusId = 2;
                console.log("Need Approval -> TotalAmount > ", PurchaseOrder.MinTotalAmount);
            }

            if (PurchaseOrder.NeedApproval == 1) {
                console.log('NeedApproval');
                PurchaseOrder.setPurchaseOrderHeader($scope.mData);
                PurchaseOrder.setPurchaseOrderDetail($scope.mData.GridDetail);
                ngDialog.openConfirm({
                    template: '<div ng-include=\"\'app/parts/purchaseorder/helper/approvalubah.html\'\"></div>',
                    //templateUrl: '/app/parts/pomaintenance/alasan.html',
                    plain: true,
                    controller: 'PurchaseOrderController',
                });
            } else {
                console.log('No NeedApproval');
                console.log('No NeedApproval mData==>',$scope.mData);
                $scope.POStatusOld = $scope.mData.PurchaseOrderStatusId;
                if(($scope.mData.RefPOTypeId == '4' || $scope.mData.RefPOTypeId == 4) && $scope.crudState == '3442'){
                    $scope.UbahData($scope.mData);
                    return 
                }
                // ======
                if ($scope.mData.RefPOTypeId == '2' && $scope.crudState == '312') {
                    $scope.UbahData($scope.mData, 1);
                    return
                }
                $scope.UbahData($scope.mData);
            }
        };

        $scope.simpan = function(mdata) {
            //debugger;
            PurchaseOrder.NeedApproval = 0; // default tidak perlu approval
            console.log("anita", $scope.tam_sop_appointment.data);
            console.log("Picture perfect 2  ", $scope.mData.RefPOTypeId);
            console.log("Picture perfect", $scope.non_tam_sop_so.data);

            if($scope.mData.VendorId == null || $scope.mData.VendorId === undefined){
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Vendor masih kosong",
                    type: 'danger'
                });
                return false
            }


            if (($scope.mData.RefPOTypeId == '1') || ($scope.mData.RefPOTypeId == '2')) {
                $scope.mData.GridDetail = $scope.tam_sop_appointment.data;
            }
            if ($scope.mData.RefPOTypeId == '3') {
                $scope.mData.GridDetail = $scope.non_tam_sop_so.data;
            }
            if ($scope.mData.RefPOTypeId == '4') {
                $scope.mData.GridDetail = $scope.non_tam_non_sop_bengkel.data;
            }
            if (($scope.mData.RefPOTypeId == '4' && $scope.mData.MaterialTypeId == '2') || ($scope.mData.RefPOTypeId == '4' && $scope.mData.MaterialTypeId == 2)) {
                $scope.mData.GridDetail = $scope.non_tam_non_sop.data;
            }
            if ($scope.mData.RefPOTypeId == '4' && $scope.mData.MaterialTypeId == '1') {
              console.log('($scope.mData.RefPOTypeId == 4) C');
                $scope.mData.GridDetail = $scope.non_tam_non_sop_bengkel.data;
            }
            if ($scope.mData.POTypeId == '5' && $scope.mData.RefPOTypeId == '2') {
                $scope.mData.GridDetail = $scope.sublet_sop_wo.data;

                console.log('masuk sublet');
            }
            if ($scope.mData.POTypeId == '5' && $scope.mData.RefPOTypeId == '1') {
                $scope.mData.GridDetail = $scope.sublet_sop_wo.data;

                console.log('masuk sublet');
            }


            if (($scope.mData.RefPOTypeId == '2') || ($scope.mData.RefPOTypeId == '3') || ($scope.mData.POTypeId == '5' && $scope.mData.RefPOTypeId == '1' )) {
                console.log("ini ??", $scope.mData);
                $scope.SimpanData($scope.mData);
            } else if ($scope.mData.RefPOTypeId == '4') {
                console.log("Panjang grid detail ", $scope.mData.GridDetail);
                for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                    console.log("Stocking Policy ", $scope.mData.GridDetail[i].PartsClassId4);
                    
                    if (($scope.mData.GridDetail[i].QtyPO <= 0 && $scope.mData.POTypeId == '7') || $scope.mData.GridDetail[i].QtyPO < 0 || $scope.mData.GridDetail[i].QtyPO == ''|| $scope.mData.GridDetail[i].QtyPO == null) {
                        console.log("Masuk Sini -> QtyPo > ", $scope.mData.GridDetail[i].QtyPO);
    
                        bsNotify.show({
                            title: "Purchase Order",
                            content: "Qty Tidak Valid",
                            type: 'warning'
                        });
                 
                        return -1;
                    }
    

                    if ($scope.mData.GridDetail[i].StockingPolicy == 'Non Stock' && $scope.mData.GridDetail[i].TotalAmount < $scope.mData.GridDetail[i].maxCost) {
                        PurchaseOrder.NeedApproval = 1;
                        $scope.mData.PurchaseOrderStatusId = 2;
                        console.log("Need Approval --> PartsClassId4 == 47");
                    } else {
                        console.log("Why here");
                    }
                }
                //debugger;
               
                if ($scope.mData.TotalAmount > PurchaseOrder.MinTotalAmount) {

                    PurchaseOrder.NeedApproval = 1;
                    $scope.mData.PurchaseOrderStatusId = 2;
                    console.log("Need Approval -> TotalAmount > ", PurchaseOrder.MinTotalAmount);
                }

                if ($scope.mData.VendorId == null || $scope.mData.VendorId == 'undefined' || $scope.mData.VendorId == "") {
                    bsNotify.show({
                        title: "Purchase Order",
                        content: "Vendor kosong",
                        type: 'danger'
                    });
                    setTimeout(function(){
                        $scope.checkRefGINo = false;
                    },1000);
                } else if ($scope.mData.PaymentMethodId == null || $scope.mData.PaymentMethodId == 'undefined' || $scope.mData.PaymentMethodId == "") {
                    bsNotify.show({
                        title: "Purchase Order",
                        content: "Metode Pembayaran kosong",
                        type: 'danger'
                    });
                    setTimeout(function(){
                        $scope.checkRefGINo = false;
                    },1000);
                    // } else if ($scope.mData.PaymentPeriod == null || $scope.mData.PaymentPeriod == 'undefined' || $scope.mData.PaymentPeriod == "") {
                    //     console.log('periode', $scope.mData.PaymentPeriod);
                    //     bsNotify.show({
                    //         title: "Purchase Order",
                    //         content: "Periode Pembayaran kosong",
                    //         type: 'warning'
                    //     });
                } else if ($scope.mData.MaterialTypeId == null || $scope.mData.MaterialTypeId == 'undefined' || $scope.mData.MaterialTypeId == "") {
                    bsNotify.show({
                        title: "Purchase Order",
                        content: "Tipe Material kosong",
                        type: 'danger'
                    });
                    setTimeout(function(){
                        $scope.checkRefGINo = false;
                    },1000);
                } else if (($scope.mData.POTypeId == 1 || $scope.mData.POTypeId == "1" || $scope.mData.POTypeId == 2 || $scope.mData.POTypeId == "2" || $scope.mData.POTypeId == 3 || $scope.mData.POTypeId == "3") &&
                    ($scope.mData.BackOrderFollowId == null || $scope.mData.BackOrderFollowId == 'undefined' || $scope.mData.BackOrderFollowId == "" || $scope.mData.BackOrderFollowId == 0 || $scope.mData.BackOrderFollowId == "0")) {
                    bsNotify.show({
                        title: "Purchase Order",
                        content: "BO TAM Follow kosong",
                        type: 'danger'
                    });
                    setTimeout(function(){
                        $scope.checkRefGINo = false;
                    },1000);
                }else if(($scope.mData.POTypeId == 6 ||  $scope.mData.POTypeId == "6") && ($scope.mData.TireOrderId == null || $scope.mData.TireOrderId == undefined || $scope.mData.TireOrderId == "")){
                    bsNotify.show({
                        title: "Purchase Order",
                        content: "Jenis Order Tire harus diisi",
                        type: 'danger'
                    });
                }else if(($scope.mData.POTypeId == 6 ||  $scope.mData.POTypeId == "6") && 
                    (!$scope.mData.NecessaryDate || $scope.mData.NecessaryTime == "NaN:NaN:00" || !$scope.mData.NecessaryTime )){
                    bsNotify.show({
                        title: "Purchase Order",
                        content: "Tanggal & Waktu Dibutuhkan harus diisi",
                        type: 'danger'
                    });
                }
                else
                {
                    if (PurchaseOrder.NeedApproval == 1) {
                        console.log('NeedApproval');
                        console.log("$scope.mData=>",$scope.mData);
                        PurchaseOrder.setPurchaseOrderHeader($scope.mData);
                        PurchaseOrder.setPurchaseOrderDetail($scope.mData.GridDetail);
                        ngDialog.openConfirm({
                            template: '<div ng-include=\"\'app/parts/purchaseorder/helper/approval.html\'\"></div>',
                            //templateUrl: '/app/parts/pomaintenance/alasan.html',
                            plain: true,
                            controller: 'PurchaseOrderController',
                        });
                    } else {
                        console.log('No NeedApproval');
                        $scope.mData.PurchaseOrderStatusId = 3;
                        $scope.SimpanData($scope.mData);
                    }
                }
            }


        };
        // ======== end tombol  simpan =========

        // ======== tombol batal - purchase order- alasan =========
        $scope.batalPC = function(mData) {
            console.log('Batal, mData: ', mData);
            console.log('Batal, mData.BackOrderFollowId: ', mData.BackOrderFollowId);

            PurchaseOrder.getCheckGR(mData.PurchaseOrderNo).then(function(res) {
                var GR = res.data;
                console.log("res.data GR lenght", GR.length);
                if (GR.length > 0) {
                    // bsNotify.show({
                    //     title: "Sudah proses Goods Receipt",
                    //     content: "Tidak bisa dibatalkan.",
                    //     type: 'warning'
                    // });
                    bsAlert.alert({
                        title: "Sudah proses Goods Receipt",
                        text: "Tidak bisa dibatalkan.",
                        type: "warning",
                        showCancelButton: false
                    },
                    function() {
                    
                    }
                    )
                } else {
                    if (mData.BackOrderFollowId === undefined) { // engga ada Fill atau Kill
                        ngDialog.openConfirm({
                            template: '<div ng-include=\"\'app/parts/purchaseorder/helper/batalpo.html\'\"></div>',
                            plain: true,
                            controller: 'PurchaseOrderController',
                        });
                    } else if (mData.PurchaseOrderStatusId == 6) {
                        bsNotify.show({
                            title: "Purchase Order",
                            content: "Status sudah 'Cancelled'",
                            type: 'warning'
                        });
                    } else {
                        PurchaseOrder.getCheckBO(mData.PurchaseOrderNo).then(function(res) {
                                var BO = res.data;
                                console.log("res.data BO lenght", res.data.length);
                                if (res.data.length > 0) {
                                    bsNotify.show({
                                        title: "Purchase Order (Back Order)",
                                        content: "Tidak bisa dibatalkan.",
                                        type: 'warning'
                                    });
                                } else if (mData.BackOrderFollowId == 9 && mData.PurchaseOrderStatusId > 1) {
                                    bsNotify.show({
                                        title: "Purchase Order (BO TAM Follow = Fill)",
                                        content: "Tidak bisa dibatalkan.",
                                        type: 'warning'
                                    });
                                } else {
                                    ngDialog.openConfirm({
                                        template: '<div ng-include=\"\'app/parts/purchaseorder/helper/batalpo.html\'\"></div>',
                                        plain: true,
                                        controller: 'PurchaseOrderController',
                                    });
                                }
                            },
                            function(err) {
                                console.log("BO err=> ", err);
                            }
                        );
                    }; // end else
                }
                },
                function(err) {
                    console.log("GR err=> ", err);
                }
            );

            // if (mData.BackOrderFollowId == 9){
            //   ngDialog.openConfirm ({
            //     template:'<div ng-include=\"\'app/parts/purchaseorder/helper/batalpo.html\'\"></div>',
            //     plain: true,
            //     controller: 'PurchaseOrderController',
            //   });
            // }else{
            //   bsNotify.show(
            //     {
            //         title: "Purchase Order ",
            //         content: "Tidak bisa dibatalkan.",
            //         type: 'warning'
            //     }
            //   );
            // }
        };

        // dialog konfirmasi
        $scope.okBatalPC = function(data) {
            console.log("okBatalPC");
            console.log("data=>",data);

            $scope.BatalData = PurchaseOrder.getPurchaseOrderHeader();
            //$scope.BatalData.Notes = 'Alasan: ' + data.Batal + ', Catatan: ' + data.Catatan;
            $scope.BatalData.CancelReasonId = data.Batal;
            $scope.BatalData.CancelReasonDesc = data.Catatan;

            console.log("Pasce Insert Batal Data ", $scope.BatalData);

            PurchaseOrder.setPurchaseOrderHeader($scope.BatalData);
            ngDialog.openConfirm({
                template: '\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Anda yakin akan membatalkan PO ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="SetujuBatal(BatalData)">Ya</button>\
                     </div>\
                     </div>',
                plain: true,
                controller: 'PurchaseOrderController',
            });
        };

        $scope.SetujuBatal = function(data) {
            $scope.mData = data;

            $scope.mData.PurchaseOrderStatusIdOld = $scope.mData.PurchaseOrderStatusId || 3;
            $scope.mData.PurchaseOrderStatusId = 6;

            $scope.ngDialog.close();
            console.log('SetujuBatal=>',$scope.mData);

            // [START] Get Search Appointment
            PurchaseOrder.cancelOrDelete($scope.mData).then(function(res) {
                    console.log(res);
                    console.log("$scope.mData=>",$scope.mData);
                    console.log("$scope.mData.gridDetail=>",$scope.mData.GridDetail);
                    $scope.mData = {};
                    $scope.mData.GridDetail = {};

                    $scope.alertAfterCancelOrDelete();
                    $scope.goBack();
                    $scope.resetForm();

                    //alert("Purchase Order berhasil dibatalkan");
                    // bsNotify.show({
                    //     title: "Purchase Order ",
                    //     content: "Data berhasil dibatalkan",
                    //     type: 'success'
                    // });
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

            // [END] Get Search Appointment
        }

        $scope.alertAfterCancelOrDelete = function() {
            // var nosb = item;
            // var nosb = item.ResponseMessage;
            // nosb = nosb.substring(nosb.lastIndexOf('#') + 1);
            bsAlert.alert({
                title: "Purchase Order",
                text: "Data berhasil dibatalkan",
                type: "success",
                showCancelButton: false
                },
                function() {
                    $scope.goBack();
                    $scope.resetForm();
                    $scope.getData();
                }
            )
        };
        $scope.alertAfterSave = function(item) {
            var nosb = item;
            var nosb = item.ResponseMessage;
            nosb = nosb.substring(nosb.lastIndexOf('#') + 1);
            bsAlert.alert({
                    title: "Data tersimpan dengan Nomor Purchase Order",
                    text: nosb,
                    type: "success",
                    showCancelButton: false
                },
                function() {
                    $scope.goBack();
                    $scope.resetForm();
                    $scope.getData();
                }
            )
        };
        $scope.alertAfterSaveDraft = function(item) {
            var nosb = item;
            var nosb = item.ResponseMessage;
            nosb = nosb.substring(nosb.lastIndexOf('#') + 1);
            bsAlert.alert({
                    title: "PO Draft berhasil disimpan",
                    text: 'Nomor PO : '+nosb,
                    type: "success",
                    showCancelButton: false
                },
                function() {
                    $scope.goBack();
                    $scope.resetForm();
                    $scope.getData();
                }
            )
        };
        $scope.alertTPOSResponse = function(item) {

            bsAlert.alert({
                title: "Response From TPOS Application",
                text: "DATA TIDAK DITEMUKAN / TIDAK DISIMPAN DI TPOS",
                type: "warning",
                showCancelButton: false
            },
            function() {
                $scope.goBack();
                $scope.resetForm();
            }
            );
                // var nosb = item;
                // var nosb = item.ResponseMessage;
                // if (item.ResponseMessage == 'Not Found# False }'){
                //     bsAlert.alert({
                //         title: "Response From TPOS Application",
                //         text: "DATA TIDAK DITEMUKAN DI TPOS",
                //         type: "warning",
                //         showCancelButton: false
                //     },
                //     function() {
                //         $scope.goBack();
                //         $scope.resetForm();
                //     }
                // )
                // }else{
                //     nosb = nosb.substring(nosb.lastIndexOf('#') + 1);
                //     bsAlert.alert({
                //             title: "Response From TPOS Application",
                //             text: nosb,
                //             type: "warning",
                //             showCancelButton: false
                //         },
                //         function() {
                //             $scope.goBack();
                //             $scope.resetForm();
                //         }
                //     )
                // }

            }
            // ======== end tombol batal - purchase order =========
            // Button yang terdapat di form
        $scope.SimpanData = function(mData) {
            // $scope.mData = mData;
            if($scope.mData.POTypeId == 6){ //Tire
                var tempTime = new Date($scope.mData.NecessaryTime);
                console.log(tempTime);
                $scope.mData.NecessaryTime = tempTime.getHours() +':'+tempTime.getMinutes() + ':00';
            }
            $scope.mData.CampaignId = $scope.CampaignIdNow != null || $scope.CampaignIdNow != undefined ? $scope.CampaignIdNow : null; //Campaign

            $scope.checkRefGINo = true;
            var UserId = '';
            var DocNo = '';

            $scope.mData.CreatedDate = new Date();
            $scope.mData.CreatedUserId = $scope.user.UserId;

            console.log("Persiapan. ", $scope.mData);
            console.log("punya resa", $scope.non_tam_non_sop);
            var backOrderFollowBit = 0;

            // if ($scope.mData.POTypeId == 2 || $scope.mData.POTypeId == '2') {
            //     console.log("SOP WO");
            //     backOrderFollowBit = 1;
            //     $scope.mData.BackOrderFollowId = 9;
            // }
            // if ($scope.mData.POTypeId == 1 || $scope.mData.POTypeId == '1') {
            //     console.log("SOP SO");
            //     backOrderFollowBit = 1;
            //     $scope.mData.BackOrderFollowId = 9;
            // }
            // if ($scope.mData.POTypeId == 3 && $scope.mData.POTypeId == '3') {
            //     console.log("SOP SO");
            //     backOrderFollowBit = 1;
            //     $scope.mData.BackOrderFollowId = 9;
            // }
            // if ($scope.mData.RefPOTypeId == 4 || $scope.mData.RefPOTypeId == '4') {
            //     console.log("NON SOP");
            //     backOrderFollowBit = 1;
            //     $scope.mData.BackOrderFollowId = 0;
            // }
            // if ($scope.mData.RefPOTypeId == 2 || $scope.mData.RefPOTypeId == '2') {
            //     console.log("SOP WO");
            //     backOrderFollowBit = 1;
            //     $scope.mData.BackOrderFollowId = 0;
            // }
            // if ($scope.mData.RefPOTypeId == 3 || $scope.mData.RefPOTypeId == '3') {
            //     console.log("SOP SO");
            //     backOrderFollowBit = 1;
            //     $scope.mData.BackOrderFollowId = 0;
            // } Gua matiin dulu soalnya ngga tau buat apa ini
            if ($scope.mData.RefPOTypeId == '4' && $scope.mData.MaterialTypeId == '2') {
                $scope.mData.GridDetail = $scope.non_tam_non_sop.data;
            }
            //Untuk sementara besok tanya ke pak ari
            if ($scope.mData.RefPOTypeId == 3 && $scope.mData.MaterialTypeId == 1) {
                if ($scope.mData.GridDetail == 'undefined') {
                    bsNotify.show({
                        title: "Purchase Order",
                        content: "Data Masih Ada yang kosong",
                        type: 'warning'
                    });
                    setTimeout(function(){
                        $scope.checkRefGINo = false;
                    },1000);
                }
            }

            // if ($scope.mData.UnitPrice == 0 || $scope.mData.UnitPrice == 0) {
            //     bsNotify.show({
            //         title: "Purchase Order",
            //         content: "Harga Material Tidak Ditemukan",
            //         type: 'warning'
            //     });
            // }

            //if((('BackOrderFollowId' in $scope.mData) || ('VendorId' in $scope.mData) || ('PaymentMethodId' in $scope.mData) || ('PaymentPeriod' in $scope.mData)) && ('AppointmentDate' in $scope.mData))
            // if(($scope.mData.BackOrderFollowId == null || $scope.mData.BackOrderFollowId == 'undefined' || $scope.mData.BackOrderFollowId == "") && $scope.mData.RefPOTypeId != 4){
            if (($scope.mData.backOrderFollowBit < 1) && $scope.mData.RefPOTypeId != 4) {
                bsNotify.show({
                    title: "Purchase Order",
                    content: "BO TAM kosong",
                    type: 'warning'
                });
                setTimeout(function(){
                    $scope.checkRefGINo = false;
                },1000);
            } else if ($scope.mData.VendorId == null || $scope.mData.VendorId == 'undefined' || $scope.mData.VendorId == "") {
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Vendor kosong",
                    type: 'warning'
                });
                setTimeout(function(){
                    $scope.checkRefGINo = false;
                },1000);
            } else if ($scope.mData.PaymentMethodId == null || $scope.mData.PaymentMethodId == 'undefined' || $scope.mData.PaymentMethodId == "") {
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Metode Pembayaran kosong",
                    type: 'warning'
                });
                setTimeout(function(){
                    $scope.checkRefGINo = false;
                },1000);
                // } else if ($scope.mData.PaymentPeriod == 'undefined') {
                //     bsNotify.show({
                //         title: "Purchase Order",
                //         content: "Periode Pembayaran kosong",
                //         type: 'warning'
                //     });
            } else if ($scope.mData.MaterialTypeId == null || $scope.mData.MaterialTypeId == 'undefined' || $scope.mData.MaterialTypeId == "") {
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Tipe Material kosong",
                    type: 'warning'
                });
                setTimeout(function(){
                    $scope.checkRefGINo = false;
                },1000);
            // } else if ($scope.mData.PartsName == null ) {
            //     bsNotify.show({
            //         title: "Purchase Order",
            //         content: "Nama Parts Tidak Tersedia",
            //         type: 'warning'
            //     });
            } else {
                //$scope.GenerateDocNum();
                console.log("GridDetail", $scope.mData.GridDetail);
                $timeout(function() {
                    console.log("Aloha");

                    if ($scope.mData.GridDetail.length == 0)
                    // if(Object.keys($scope.filter).length==0)
                    {
                        bsNotify.show({
                            title: "Purchase Order",
                            content: "Order Tidak Dapat Dilakukan, Tambahkan Material Yang Akan Diorder Dahulu",
                            type: 'warning'
                        });
                        setTimeout(function(){
                            $scope.checkRefGINo = false;
                        },1000);
                        return 0;
                    }
                    console.log("Rezaaa", $scope.mData.MaterialTypeId);
                    console.log("rezaaaa mr", $scope.mData.RefPOTypeId);
                    console.log("Reza consol", $scope.mData.GridDetail);
                    for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                        if (PurchaseOrder.NeedApproval == 0) {
                            $scope.mData.GridDetail[i].PurchaseOrderStatusId = 3; //for dratf
                        } else {
                            $scope.mData.GridDetail[i].PurchaseOrderStatusId = 2; //for dratf
                        }

                        if ($scope.mData.GridDetail[i].QtyPO == null || $scope.mData.GridDetail[i].QtyPO == '') {
                            console.log("Masuk Sini -> QtyPo > ", $scope.mData.GridDetail[i].QtyPO);
            
                            bsNotify.show({
                                title: "Purchase Order",
                                content: "Qty Tidak Valid",
                                type: 'warning'
                            });
                            setTimeout(function(){
                                $scope.checkRefGINo = false;
                            },1000);
                     
                            return 0;
                        }
                        console.log("Masuk Coba1=>",$scope.mData.GridDetail)
                        // if ($scope.mData.GridDetail == null || $scope.mData.GridDetail == '' || $scope.mData.GridDetail == undefined) {
                        //     console.log("Masuk Sini -> QtyPo > ", $scope.mData.GridDetail[i].Partid);
            
                        //     bsNotify.show({
                        //         title: "Purchase Order",
                        //         content: "Data Masih Ada Yang Kosong",
                        //         type: 'warning'
                        //     });
                        //     setTimeout(function(){
                        //         $scope.checkRefGINo = false;
                        //     },1000);
                     
                        //     return 0;
                        // }

                        if ($scope.mData.GridDetail[i].StandardCost == null || $scope.mData.GridDetail[i].StandardCost == '' || $scope.mData.GridDetail[i].StandardCost == 0) {
                            console.log("Masuk Sini -> StandardCost > ", $scope.mData.GridDetail[i].StandardCost);
            
                            bsNotify.show({
                                title: "Purchase Order",
                                content: "Standard cost belum ada, Harap update terlebih dahulu",
                                type: 'warning'
                            });
                            setTimeout(function(){
                                $scope.checkRefGINo = false;
                            },1000);
                     
                            return 0;
                        }

                        $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
                        $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;

                        var ETA = $filter('date')($scope.mData.GridDetail[0].ETA, 'yyyy-MM-dd HH:MM');
                        var ETD = $filter('date')($scope.mData.GridDetail[0].ETD, 'yyyy-MM-dd HH:MM');

                        $scope.mData.GridDetail[i].ETA = ETA;
                        $scope.mData.GridDetail[i].ETD = ETA;
						
						//Add stv 14/01/2021
						var tmpEstPartsId = 0;
						tmpEstPartsId = $scope.mData.GridDetail[i].estPartsId;
						
						delete $scope.mData.GridDetail[i].estPartsId;
						
						if($scope.mData.POTypeId == "5" && $scope.mData.RefPOTypeId == "4"){
							$scope.mData.GridDetail[i].SourcePurchaseOrderItemId = tmpEstPartsId;
						}
                    }

                    PurchaseOrder.getCheckSource($scope.mData.RefPOTypeId,$scope.mData.GridDetail).then(function(resSource){
                        var resSrc = resSource.data;
                        console.log('resSrc==>',resSrc);
                        console.log('$scope.mData==>',$scope.mData);
                        console.log('$scope.mData.RefPOTypeID==>',$scope.mData.RefPOTypeId);
                        
                        if(resSrc.length>0 && ($scope.mData.RefPOTypeId == 1 || $scope.mData.RefPOTypeId == "1")) {
                            bsAlert.warning({
                                title: "Terjadi perubahan Data atau Qty Parts di Appointment",
                                text: "Mohon lakukan Refresh terlebih dahulu",
                                type: "warning",
                                showCancelButton: false
                            });
                            return 0;
                        } else if(resSrc.length>0 && ($scope.mData.RefPOTypeId == 2 || $scope.mData.RefPOTypeId == "2")) {
                            bsAlert.warning({
                                title: "Terjadi perubahan Data atau Qty Parts di WO",
                                text: "Mohon lakukan Refresh terlebih dahulu",
                                type: "warning",
                                showCancelButton: false
                            });
                            return 0;
                        } else if(resSrc.length>0 && ($scope.mData.RefPOTypeId == 3 || $scope.mData.RefPOTypeId == "3")) {
                            bsAlert.warning({
                                title: "Terjadi perubahan Data atau Qty Parts di Sales Order",
                                text: "Mohon lakukan Refresh terlebih dahulu",
                                type: "warning",
                                showCancelButton: false
                            });
                            return 0;
                        }

                        console.log("Persiapan Create => ");
                        console.log("Head ", $scope.mData);
                        console.log("Tails ", $scope.mData.GridDetail);
                        $scope.GenerateDocNumWithPromise().then(function(resultPromise){
                            if(resultPromise){
                                console.log(resultPromise);
                                console.log("$scope.mData=>>",$scope.mData);

                                if ($scope.mData.PurchaseOrderNo == 0 || $scope.mData.PurchaseOrderNo == null || $scope.mData.PurchaseOrderNo.includes('$$$')) {
                                    console.log("Masuk Sini -> QtyPo > ", $scope.mData.PurchaseOrderNo);
                    
                                    bsNotify.show({
                                        title: "Purchase Order",
                                        // content: " Tolong refresh kembali karena Purchase Order No 0. Silahkan klik kembali dan input ulang data yg ingin dipakai",
                                        content: "Nomor PO belum terbentuk. Silahkan klik kembali tombol simpan",
                                        type: 'warning'
                                    });
                                    setTimeout(function(){
                                        $scope.checkRefGINo = false;
                                    },1000);
                            
                                    return 0;
                                }
                                console.log("after promise 1 ", $scope.mData);
                                console.log("after promise 2 ", $scope.mData.GridDetail);
                                for (var i in $scope.mData.GridDetail){
                                    var datainsert={OutletId : $scope.mData.GridDetail[i].OutletId , Partid : $scope.mData.GridDetail[i].PartId, WarehouseId : $scope.mData.GridDetail[i].WarehouseId};
                                    PurchaseOrder.InsertIntoStockPosition(datainsert).then(function(res) {
                                        console.log("TET TOT");
                                    });
                                    if ($scope.mData.POTypeId >= 1 && $scope.mData.POTypeId <= 3) {
                                        $scope.mData.GridDetail[i].StatusTPOS = 1;
                                    }
                                }

                                //Campaign
                                if($scope.mData.POTypeId == 7 && $scope.mData.tCampaign == 'C'){
                                    $scope.mData.POTypeId = 7;
                                }else if($scope.mData.POTypeId == 7 && $scope.mData.tCampaign == '3'){
                                    $scope.mData.POTypeId = 3;
                                }

                                PurchaseOrder.create($scope.mData, $scope.mData.GridDetail).then(function(res) {
                                        var create = res.data.Response?res.data.Response:res.data;
                                        console.log('RESSSS :', res.data);
                                        if (create.ResponseCode == 19) {
                                            $scope.alertTPOSResponse(create);
                                        }else if(create.ResponseCode == 404){
                                            bsAlert.alert({
                                                title: create.ResponseMessage,
                                                text:'',
                                                type: "error",
                                                showCancelButton: false
                                            },
                                            function() {
                                            
                                            }
                                            )
                                        } else {
                                            var poId = create.ResponseMessage.substring(create.ResponseMessage.lastIndexOf('[') + 1 ,create.ResponseMessage.lastIndexOf(']'));
                                            $scope.cetakOPM(poId,create.ResponseCode);
                                            $scope.alertAfterSave(create);
                                            $scope.goBack();
                                            $scope.resetForm();
                                        }
                                        //$scope.alertAfterSave(create);
                                        // bsNotify.show(
                                        //   {
                                        //       title: "Purchase Order ",
                                        //       content: "Data berhasil disimpan",
                                        //       type: 'success'
                                        //   }
                                        // );
                                    },
                                    function(err) {
                                        console.log("err=>", err.data);
                                    }
                                );
                            }else{
                                return;
                            }
                        });
                    });

                    //console.log("Grid ", $scope.non_tam_sop_wo.data);
                    // // [START] Get Search Appointment
                })
            }
        }

        // Button yang terdapat di form
        $scope.SimpanDraft = function(mData) {
            // $scope.mData = mData;
            $scope.checkRefGINo = true;
            $scope.mData.PurchaseOrderStatusId = 1;
            var UserId = '';
            var DocNo = '';
            if(PurchaseOrder.GridData !== undefined){
                $scope.mData.GridDetail = PurchaseOrder.GridData;
            }
            console.log(" PurchaseOrder.GridData iniiii  , ",  PurchaseOrder.GridData, $scope.mData.GridDetail);
            console.log("SimpanDraft Persiapan , ", $scope.mData);

            // if($scope.mData.RefPOTypeId == 4 || $scope.mData.RefPOTypeId == '4' )
            // {
            //   console.log("NON SOP");
            //   $scope.mData.BackOrderFollowId = 0;
            // }
            // if($scope.mData.RefPOTypeId == 4 || $scope.mData.RefPOTypeId == '2' )
            // {
            //   console.log("SOP WO");
            //   $scope.mData.BackOrderFollowId = 0;
            // }
            // if($scope.mData.RefPOTypeId == 3 || $scope.mData.RefPOTypeId == '3' )
            // {
            //   console.log("SOP SO");
            //   $scope.mData.BackOrderFollowId = 0;
            // }
            // if(($scope.mData.RefPOTypeId == 1 || $scope.mData.RefPOTypeId == '1') && ($scope.mData.POTypeId == 5 || $scope.mData.POTypeId == '5'))
            // {
            //   console.log("SOP SO");
            //   $scope.mData.BackOrderFollowId = 0;
            // }
            if ($scope.isTAM == false) {
                $scope.mData.BackOrderFollowId = 1; //1:Kill 9:Fill
            }
            if ($scope.mData.RefPOTypeId == '4') { // && $scope.mData.MaterialTypeId == '2')  //)  ddg 12-12
                if ($scope.mData.MaterialTypeId == '2') {
                    $scope.mData.GridDetail = $scope.non_tam_non_sop.data; //non_tam_non_sop_bengkel
                } else {
                    $scope.mData.GridDetail = $scope.non_tam_non_sop_bengkel.data; //
                }
            }

            //if((('BackOrderFollowId' in $scope.mData) || ('VendorId' in $scope.mData) || ('PaymentMethodId' in $scope.mData) || ('PaymentPeriod' in $scope.mData)) && ('AppointmentDate' in $scope.mData))
            // if(($scope.mData.BackOrderFollowId == null || $scope.mData.BackOrderFollowId == 'undefined' || $scope.mData.BackOrderFollowId == "")){
            //   bsNotify.show({
            //       title: "Purchase Order",
            //       content: "BO TAM  kosong",
            //       type: 'warning'
            //   });
            // }
            // else
           

            if ($scope.mData.VendorId == null || $scope.mData.VendorId == 'undefined' || $scope.mData.VendorId == "") {
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Vendor kosong",
                    type: 'danger'
                });
                setTimeout(function(){
                    $scope.checkRefGINo = false;
                },1000);
            } else if ($scope.mData.PaymentMethodId == null || $scope.mData.PaymentMethodId == 'undefined' || $scope.mData.PaymentMethodId == "") {
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Metode Pembayaran kosong",
                    type: 'danger'
                });
                setTimeout(function(){
                    $scope.checkRefGINo = false;
                },1000);
                // } else if ($scope.mData.PaymentPeriod == null || $scope.mData.PaymentPeriod == 'undefined' || $scope.mData.PaymentPeriod == "") {
                //     console.log('periode', $scope.mData.PaymentPeriod);
                //     bsNotify.show({
                //         title: "Purchase Order",
                //         content: "Periode Pembayaran kosong",
                //         type: 'warning'
                //     });
            } else if ($scope.mData.MaterialTypeId == null || $scope.mData.MaterialTypeId == 'undefined' || $scope.mData.MaterialTypeId == "") {
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Tipe Material kosong",
                    type: 'warning'
                });
                setTimeout(function(){
                    $scope.checkRefGINo = false;
                },1000);

            } else if (($scope.mData.POTypeId == 1 || $scope.mData.POTypeId == "1" || $scope.mData.POTypeId == 2 || $scope.mData.POTypeId == "2" || $scope.mData.POTypeId == 3 || $scope.mData.POTypeId == "3") &&
                ($scope.mData.BackOrderFollowId == null || $scope.mData.BackOrderFollowId == 'undefined' || $scope.mData.BackOrderFollowId == "" || $scope.mData.BackOrderFollowId == 0 || $scope.mData.BackOrderFollowId == "0")) {
                bsNotify.show({
                    title: "Purchase Order",
                    content: "BO TAM Follow harus diisi",
                    type: 'danger'
                });
                setTimeout(function(){
                    $scope.checkRefGINo = false;
                },1000);
            }else if(($scope.mData.POTypeId == 6 ||  $scope.mData.POTypeId == "6") && ($scope.mData.TireOrderId == null || $scope.mData.TireOrderId == undefined || $scope.mData.TireOrderId == "")){
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Jenis Order Tire harus diisi",
                    type: 'danger'
                });
            }else if(($scope.mData.POTypeId == 6 ||  $scope.mData.POTypeId == "6") && 
                (!$scope.mData.NecessaryDate || $scope.mData.NecessaryTime == "NaN:NaN:00" || !$scope.mData.NecessaryTime )){
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Tanggal & Waktu Dibutuhkan harus diisi",
                    type: 'danger'
                });
            } else {
                //$scope.GenerateDocNum();
                console.log("Aloha ,");

                if($scope.mData.POTypeId == 6){ //Tire
                    var tempTime = new Date($scope.mData.NecessaryTime);
                    console.log(tempTime);
                    $scope.mData.NecessaryTime = tempTime.getHours() +':'+tempTime.getMinutes() + ':00';
                }
                $scope.mData.CampaignId = $scope.CampaignIdNow != null || $scope.CampaignIdNow != undefined ? $scope.CampaignIdNow : null; //Campaign

                $timeout(function() {
                    console.log("Aloha ,");

                    if (!('GridDetail' in $scope.mData))
                    // if(Object.keys($scope.filter).length==0)
                    {
                        bsNotify.show({
                            title: "Purchase Order",
                            content: "Detail masih kosong ,",
                            type: 'warning'
                        });
                        setTimeout(function(){
                            $scope.checkRefGINo = false;
                        },1000);
                    }
                   
                    if(PurchaseOrder.GridData !== undefined){
                        $scope.mData.GridDetail = PurchaseOrder.GridData;
                    }
                    for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                        $scope.mData.GridDetail[i].PurchaseOrderStatusId = 1; //for dratf
                        // append by faisal
                        if($scope.mData.GridDetail[i].DiscountPercent == null || $scope.mData.GridDetail[i].DiscountPercent == ''){
                            $scope.mData.GridDetail[i].DiscountPercent = 0;
                        }
                        // ==========
                        if ($scope.mData.GridDetail[i].QtyPO == null || $scope.mData.GridDetail[i].QtyPO == '') {
                            console.log("Masuk Sini -> QtyPo > ", $scope.mData.GridDetail[i].QtyPO);
            
                            bsNotify.show({
                                title: "Purchase Order",
                                content: "Qty Tidak Valid",
                                type: 'warning'
                            });
                            setTimeout(function(){
                                $scope.checkRefGINo = false;
                            },1000);
                     
                            return 0;
                        }

                        $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
                        $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;

                        var ETA = $filter('date')($scope.mData.GridDetail[0].ETA, 'yyyy-MM-dd HH:MM');
                        var ETD = $filter('date')($scope.mData.GridDetail[0].ETD, 'yyyy-MM-dd HH:MM');

                        $scope.mData.GridDetail[i].ETA = ETA;
                        $scope.mData.GridDetail[i].ETD = ETA;
                    }

                    console.log("Persiapan Create => ");
                    console.log("Head ", $scope.mData);
                    console.log("Tails ", $scope.mData.GridDetail);
                    // for (var i in $scope.mData.GridDetail){
                    //     var datainsert={OutletId : $scope.mData.GridDetail[i].OutletId ,Partid : $scope.mData.GridDetail[i].PartId,WarehouseId : $scope.mData.GridDetail[i].WarehouseId};
                    //     PurchaseOrder.InsertIntoStockPosition(datainsert).then(function(reso) {
                    //         console.log("TET TOT");
                    //     });
                    // }
                    // PurchaseOrder.create($scope.mData, $scope.mData.GridDetail).then(function(res) {
                    //         var create = res.data;
                    //         console.log(res);
                    //         console.log(res.data.Result);
                    //         // $scope.alertAfterSave(create);
                    //         $scope.alertAfterSaveDraft(create);
                    //         // bsNotify.show(
                    //         //   {
                    //         //       title: "Purchase Order ",
                    //         //       content: "Data berhasil disimpan",
                    //         //       type: 'success'
                    //         //   }
                    //         // );
                    //         $scope.goBack();
                    //         $scope.resetForm();
                    //     },
                    //     function(err) {
                    //         console.log("err=>", err.data);
                    $scope.GenerateDocNumWithPromise().then(function(resultPromise){
                        if(resultPromise){
                            console.log(resultPromise);
                            console.log("$scope.mData=>>",$scope.mData);
                            if ($scope.mData.PurchaseOrderNo == 0 || $scope.mData.PurchaseOrderNo == null  || $scope.mData.PurchaseOrderNo.includes('$$$')) {
                                console.log("Masuk Sini -> QtyPo > ", $scope.mData.PurchaseOrderNo);
                
                                bsNotify.show({
                                    title: "Purchase Order",
                                    // content: " Tolong refresh kembali karena Purchase Order No 0. Silahkan klik kembali dan input ulang data yg ingin dipakai",
                                    content: "Nomor PO belum terbentuk. Silahkan klik kembali tombol simpan",
                                    type: 'warning'
                                });
                                setTimeout(function(){
                                    $scope.checkRefGINo = false;
                                },1000);
                         
                                return 0;
                            }
                            console.log("after promise 1 ", $scope.mData);
                            console.log("after promise 2 ", $scope.mData.GridDetail);

                            for (var i in $scope.mData.GridDetail){
                                var datainsert={OutletId : $scope.mData.GridDetail[i].OutletId , Partid : $scope.mData.GridDetail[i].PartId, WarehouseId : $scope.mData.GridDetail[i].WarehouseId};
                                PurchaseOrder.InsertIntoStockPosition(datainsert).then(function(res) {
                                    console.log("TET TOT");
                                });
                                if ($scope.mData.POTypeId >= 1 && $scope.mData.POTypeId <= 3) {
                                    $scope.mData.GridDetail[i].StatusTPOS = 1;
                                }                
                            }
                            //Campaign
                            if($scope.mData.POTypeId == 7 && $scope.mData.tCampaign == 'C'){
                                $scope.mData.POTypeId = 7;
                            }else if($scope.mData.POTypeId == 7 && $scope.mData.tCampaign == '3'){
                                $scope.mData.POTypeId = 3;
                            }

                            PurchaseOrder.create($scope.mData, $scope.mData.GridDetail).then(function(res) {
                                        var create = res.data;
                                        console.log(res);
                                        console.log(res.data.Result);
                                        // $scope.alertAfterSave(create);
                                        $scope.alertAfterSaveDraft(create);
                                        // bsNotify.show(
                                        //   {
                                        //       title: "Purchase Order ",
                                        //       content: "Data berhasil disimpan",
                                        //       type: 'success'
                                        //   }
                                        // );
                                        $scope.goBack();
                                        $scope.resetForm();
                                    },
                                    function(err) {
                                        console.log("err=>", err.data);
                                    }
                                    );
                                }else{
                                    return;
                                }

                            });
                            
                        })
        
                    }
        
        
                }
                        
        $scope.SimpanData2 = function(mData) {
            $scope.mData = mData;
            var UserId = '';
            var DocNo = '';


            //$scope.GenerateDocNum();

            $timeout(function() {

                console.log("Aloha");

                for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                    $scope.mData.GridDetail[i].PurchaseOrderStatusId = 2; //for dratf
                    $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
                    $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;

                    var ETA = $filter('date')($scope.mData.GridDetail[0].ETA, 'yyyy-MM-dd HH:MM');
                    var ETD = $filter('date')($scope.mData.GridDetail[0].ETD, 'yyyy-MM-dd HH:MM');

                    $scope.mData.GridDetail[i].ETA = ETA;
                    $scope.mData.GridDetail[i].ETD = ETA;

                    if ($scope.mData.POTypeId >= 1 && $scope.mData.POTypeId <= 3) {
                        $scope.mData.GridDetail[i].StatusTPOS = 1;
                    }    
                }

                console.log("Persiapan Create => ");
                console.log("Head ", $scope.mData);
                console.log("Tails ", $scope.mData.GridDetail);

                // $scope.resetForm();


                PurchaseOrder.createApprove($scope.mData, $scope.mData.GridDetail).then(function(res) {
                        var create = res.data;
                        console.log(res.data.Result);
                        console.log(res);
                        $scope.alertAfterSave(create);
                        // bsNotify.show(
                        //   {
                        //       title: "Purchase Order ",
                        //       content: "Data berhasil disimpan",
                        //       type: 'success'
                        //   }
                        // );
                        $scope.resetForm();
                        $scope.goBack();
                    },
                    function(err) {
                        console.log("err=>", err.data);
                    }
                );



                //console.log("Grid ", $scope.non_tam_sop_wo.data);
                // // [START] Get Search Appointment
            })
        }

        // simpan drat
        // $scope.SimpanDraft = function(mData){
        //   // $scope.mData = mData;
        //   console.warn($scope.mData)
        //   var UserId = '';
        //   var DocNo = '';

        //   if($scope.mData.RefPOTypeId == '1')
        //   {
        //     $scope.mData.GridDetail = $scope.tam_sop_appointment.data;
        //   }
        //   if($scope.mData.RefPOTypeId == '4')
        //   {
        //     $scope.mData.GridDetail = $scope.non_tam_non_sop_bengkel.data;
        //   }
        //   if($scope.mData.RefPOTypeId == '4' && $scope.mData.MaterialTypeId == '2')
        //   {
        //     $scope.mData.GridDetail = $scope.non_tam_non_sop.data;
        //   }

        //   $scope.mData.PurchaseOrderStatusId = 1;


        //   //$scope.GenerateDocNum();

        //   $timeout(function(){

        //     console.log("Aloha");

        //     for(var i = 0; i < $scope.mData.GridDetail.length; i++)
        //     {
        //       $scope.mData.GridDetail[i].PurchaseOrderStatusId = 1; //for dratf
        //       $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
        //       $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;

        //       var ETA = $filter('date')($scope.mData.GridDetail[0].ETA, 'yyyy-MM-dd HH:MM');
        //       var ETD = $filter('date')($scope.mData.GridDetail[0].ETD, 'yyyy-MM-dd HH:MM');

        //       $scope.mData.GridDetail[i].ETA = ETA;
        //       $scope.mData.GridDetail[i].ETD = ETA;
        //     }

        //     console.log("Persiapan Create => ");
        //     console.log("Head ", $scope.mData);
        //     console.log("Tails ", $scope.mData.GridDetail);

        //     // $scope.resetForm();


        //     PurchaseOrder.createApprove($scope.mData, $scope.mData.GridDetail).then(function(res) {
        //           var create = res.data.Result;
        //           console.log(res.data.Result);
        //           bsNotify.show(
        //             {
        //                 title: "Purchase Order ",
        //                 content: "Data berhasil disimpan",
        //                 type: 'success'
        //             }
        //           );
        //           $scope.resetForm();
        //           $scope.goBack();
        //         },
        //         function(err) {
        //           console.log("err=>", err.data);
        //         }
        //       );



        //     //console.log("Grid ", $scope.non_tam_sop_wo.data);
        //       // // [START] Get Search Appointment
        //   })
        // }

        // Button yang terdapat di form
        $scope.UbahData = function(mData, flag) {
            // [START] Get Search Appointment
            $scope.checkRefGINo = true;
            console.log("mData ====",mData);
            //Tire
            if($scope.mData.POTypeId == 6){
                var tempTime = new Date($scope.mData.NecessaryTime);
                console.log(tempTime);
                $scope.mData.NecessaryTime = tempTime.getHours() +':'+tempTime.getMinutes() + ':00';
            }
            $scope.mData.CampaignId = $scope.CampaignIdNow != null || $scope.CampaignIdNow != undefined ? $scope.CampaignIdNow : null; //Campaign
            
            var update = 0; // 0 = update / 1 = order
            for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                var ETA = $filter('date')($scope.mData.GridDetail[0].ETA, 'yyyy-MM-dd HH:MM');
                var ETD = $filter('date')($scope.mData.GridDetail[0].ETD, 'yyyy-MM-dd HH:MM');
                
                setTimeout(function(){
                    $scope.checkRefGINo = false;
                },7000);

                $scope.mData.GridDetail[i].ETA = ETA;
                $scope.mData.GridDetail[i].ETD = ETD;
                $scope.mData.GridDetail[i].PurchaseOrderId = mData.PurchaseOrderId;
            }
            $scope.POStatusOld = $scope.mData.PurchaseOrderStatusId;
            // debugger;
            if(flag == undefined || flag == null){
                if (($scope.labelTAM == 'Simpan' || $scope.labelTAM == 'Simpan dan Order ke TAM') && mData.PurchaseOrderStatusId == 1 ){
                    mData.PurchaseOrderStatusId = 3;
                    mData.DocDate = $scope.localeDate(new(Date));
                    // mData.PurchaseOrderNo = mData.RefPONo;
                    update = 1;
                    console.log("mData.PurchaseOrderNo atas ====",mData.PurchaseOrderNo);
                }else{
                    //mData.PurchaseOrderNo = mData.RefPONo;
                    update = 0;
                    console.log("mData.PurchaseOrderNo bawah ====",mData.PurchaseOrderNo);
                }
            } 

            // if (PurchaseOrder.NeedApproval == 1) {
            //     mData.PurchaseOrderStatusId = 2;
            // } else {
            //     mData.PurchaseOrderStatusId = 3;
            // }
            console.log("$scope.mData.GridDetail",$scope.mData.GridDetail);
            console.log("$scope.mData ====",$scope.mData);
            console.log("mData ====",mData);
            console.log("POStatusOld ====",$scope.POStatusOld);
            // $scope.mData.CreatedUserId = $scope.user.UserId;
            // return

            var StatusPO = 5;
            for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                if (PurchaseOrder.NeedApproval == 0) {
                    $scope.mData.GridDetail[i].PurchaseOrderStatusId = 3; //for dratf
                } else {
                    $scope.mData.GridDetail[i].PurchaseOrderStatusId = 2; //for dratf
                }

                if ($scope.mData.GridDetail[i].QtyGR == $scope.mData.GridDetail[i].QtyPO) {
                    if (StatusPO == 5) { 
                        StatusPO = 5 
                    };
                } else {
                    if ($scope.mData.GridDetail[i].QtyGR > 0 ) {
                        StatusPO = 4; 
                    } else {
                        StatusPO = $scope.mData.PurchaseOrderStatusId;
                    }
                }

                if ($scope.mData.GridDetail[i].QtyPO == null || $scope.mData.GridDetail[i].QtyPO == '') {
                    bsNotify.show({
                        title: "Purchase Order",
                        content: "Qty Tidak Valid",
                        type: 'warning'
                    });
                    setTimeout(function(){
                        $scope.checkRefGINo = false;
                    },1000);
             
                    return 0;
                }

                if ($scope.mData.GridDetail[i].StandardCost == null || $scope.mData.GridDetail[i].StandardCost == '' || $scope.mData.GridDetail[i].StandardCost == 0) {
                    bsNotify.show({
                        title: "Purchase Order",
                        content: "Standard cost belum ada, Harap update terlebih dahulu",
                        type: 'warning'
                    });
                    setTimeout(function(){
                        $scope.checkRefGINo = false;
                    },1000);
             
                    return 0;
                }

                if ($scope.mData.PurchaseOrderNo == 0 || $scope.mData.PurchaseOrderNo == null ) {
                    bsNotify.show({
                        title: "Purchase Order",
                        content: " Tolong refresh kembali karena Purchase Order No 0. Silahkan klik kembali dan input ulang data yg ingin dipakai",
                        type: 'warning'
                    });
                    setTimeout(function(){
                        $scope.checkRefGINo = false;
                    },1000);
             
                    return 0;
                }

                $scope.mData.GridDetail[i].OutletId = $scope.mData.OutletId;
                $scope.mData.GridDetail[i].WarehouseId = $scope.mData.WarehouseId;


                console.log("$scope.mData.GridDetail[0].ETA1=>",$scope.mData.GridDetail[0].ETA);
                if($scope.mData.GridDetail[0].ETA == "-") {
                    $scope.mData.GridDetail[0].ETA = null
                }
                if($scope.mData.GridDetail[0].ETD == "-") {
                    $scope.mData.GridDetail[0].ETD = null
                }
                console.log("$scope.mData.GridDetail[0].ETA2=>",$scope.mData.GridDetail[0].ETA);

                var ETA = $filter('date')($scope.mData.GridDetail[0].ETA, 'yyyy-MM-dd HH:MM');
                var ETD = $filter('date')($scope.mData.GridDetail[0].ETD, 'yyyy-MM-dd HH:MM');

                $scope.mData.GridDetail[i].ETA = ETA;
                $scope.mData.GridDetail[i].ETD = ETA;
            }

            $scope.mData.PurchaseOrderStatusId = StatusPO;

            for (var i in $scope.mData.GridDetail){
                if($scope.mData.GridDetail[i].PurchaseOrderItemId === 0 || $scope.mData.GridDetail[i].PurchaseOrderItemId === null ){
                    var datainsert={OutletId : $scope.mData.GridDetail[i].OutletId , Partid : $scope.mData.GridDetail[i].PartId, WarehouseId : $scope.mData.GridDetail[i].WarehouseId};
                    PurchaseOrder.InsertIntoStockPosition(datainsert).then(function(res) {
                        console.log("TET TOT");
                    });
                }
            }
            PurchaseOrder.update(mData, $scope.mData.GridDetail, update, $scope.POStatusOld).then(function(res) {
                    var create = res.data;
                    var noPo = res.data.Response?res.data.Response:res.data;

                    if (res.data.ResponseCode == 23) {
                        bsNotify.show({
                            title: "Purchase Order ",
                            content: "Data berhasil diubah",
                            type: 'success'
                        });
                    }else if(noPo.ResponseCode == 404){
                        bsAlert.alert({
                            title: noPo.ResponseMessage,
                            text:'',
                            type: "error",
                            showCancelButton: false
                        },
                        function() {
                            $scope.getData();
                        }
                        )
                    }else if(flag == null){
                        $scope.alertAfterSave(noPo);
                    } else {
                        bsNotify.show({
                            title: "Purchase Order ",
                            content: "Data gagal diubah",
                            type: 'danger'
                        });
                    }
                    $scope.goBack();
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

            // PurchaseOrder.updateDetail($scope.mData.GridDetail).then(function(res) {
            //     var create = res.data.Result;
            //     console.log(res.data.Result);
            //     bsNotify.show(
            //       {
            //           title: "Purchase Order ",
            //           content: "Data berhasil diubah",
            //           type: 'success'
            //       }
            //     );
            //   },
            //   function(err) {
            //     console.log("err=>", err);
            //   }
            // );
        }

        // Button yang terdapat di form
        // $scope.BatalData = function(mData){
        //   // [START] Get Search Appointment
        //   PurchaseOrder.delete(mData).then(function(res) {
        //       var create = res.data.Result;
        //       console.log(res.data.Result);
        //       bsNotify.show(
        //         {
        //             title: "Purchase Order ",
        //             content: "Data berhasil dibatalkan",
        //             type: 'success'
        //         }
        //       );
        //     },
        //     function(err) {
        //       console.log("err=>", err);
        //     }
        //   );
        // }

        $scope.columnNonTAM = [
            {
                field: 'PartsCode',
                displayName: 'No. Material',
                // enableCellEdit: true,
                cellEditableCondition: function(scope){
                    console.log(scope.row.entity.PurchaseOrderStatusId,'Di grid nyaaa');
                    // return scope.rowRenderIndex%2;
                    if(scope.row.entity.PurchaseOrderStatusId > 3 && scope.row.entity.QtyGR > 0){
                        return false;
                    }else if(scope.row.entity.PurchaseOrderStatusId > 3 && scope.row.entity.QtyGR == 0){
                        return true;
                    }else{
                        return true;
                    }
                },
                width: '12%',
                cellTemplate: '<div>\
                    <div ng-show="row.entity.PurchaseOrderStatusId > 3 && row.entity.QtyGR > 0" class="ui-grid-cell-contents"><span>{{row.entity.PartsCode}}</span></div>\
                    <div class="input-group" ng-hide="row.entity.PurchaseOrderStatusId > 3 && row.entity.QtyGR > 0">\
                        <input type="text" style="height:30px;" class="form-control" ng-model="MODEL_COL_FIELD" ng-value="row.entity.PartsCode" required>\
                        <label class="input-group-btn">\
                            <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
                                <i class="fa fa-search fa-1"></i>\
                            </span>\
                        </label>\
                    </div>\
                </div>'
            },
            { name: 'Nama Material', sort: { priority: 0 }, width: '20%', field:"PartsName", cellTemplate: '<div class="ui-grid-cell-contents"><span>{{row.entity.PartsName}}</span></div>', enableCellEdit: false },
            { name: 'ETD', field: 'ETD', displayName: 'ETD', width: '8%', enableCellEdit: false, cellFilter: 'date:\'yyyy-MM-dd\'' },
            { name: 'ETA', field: 'ETA', displayName: 'ETA', width: '8%', enableCellEdit: false, cellFilter: 'date:\'yyyy-MM-dd\'' },
            { name: 'Qty PO', field: 'QtyPO', displayName: 'Qty PO', width: '6%', cellEditableCondition: function(scope){
                    if($scope.stateModeView){
                        return false;
                    } else {
                        if (scope.row.entity.PurchaseOrderStatusId <= 3 ) {
                            return true;
                        } else if(scope.row.entity.PurchaseOrderStatusId == 4 && scope.row.entity.QtyGR <=  scope.row.entity.QtyPO ){
                            return true;
                        } else if(scope.row.entity.PurchaseOrderStatusId === undefined){
                            return true
                        } else {
                            return false;
                        }
                    }
                }
            },
            { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', width: '6%', enableCellEdit: false, cellFilter: 'number' },
            { name: 'Satuan', field: 'Satuan', width: '5%', enableCellEdit: false },
            { name: 'Standard Cost', field: 'UnitPrice', displayName: 'Standard Cost', cellFilter: 'number', enableCellEdit: false },
            { name: 'Diskon %', field: 'DiscountPercent', displayName: '%', width: '4%', enableCellEdit: true, headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:252%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon</td></tr></tbody></table></div>%</div>" },
            { name: 'Diskon Jumlah', field: 'DiscountAmount', displayName: 'Jumlah', width: '6%', enableCellEdit: false, cellFilter: 'number', headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>", },
            { name: 'PPN', field: 'VATAmount', displayName: 'PPN', cellFilter: 'number', enableCellEdit: false },
            { name: 'Harga Total', field: 'TotalAmount', enableCellEdit: false, cellFilter: 'number', },
            { name: 'Action', cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>', visible : false },
            { name: 'UomId', field: 'UomId', visible: false },
            { name: 'PartsClassId4', field: 'PartsClassId4', visible: false },
        ]

        $scope.columnTAM = [
            {
                field: 'PartsCode',
                displayName: 'No. Material',
                // enableCellEdit: true,
                cellEditableCondition: function(scope){
                    console.log(scope.row.entity.PurchaseOrderStatusId,'Di grid nyaaa');
                    // return scope.rowRenderIndex%2;
                    if(scope.row.entity.PurchaseOrderStatusId > 3 && scope.row.entity.QtyGR > 0){
                        return false;
                    }else if(scope.row.entity.PurchaseOrderStatusId > 3 && scope.row.entity.QtyGR == 0){
                        return true;
                    }else{
                        return true;
                    }
                },
                width: '10%',
                cellTemplate: '<div>\
                    <div ng-show="row.entity.PurchaseOrderStatusId > 3 && row.entity.QtyGR > 0" class="ui-grid-cell-contents"><span>{{row.entity.PartsCode}}</span></div>\
                    <div class="input-group" ng-hide="row.entity.PurchaseOrderStatusId > 3 && row.entity.QtyGR > 0">\
                        <input type="text" style="height:30px;" class="form-control" ng-model="MODEL_COL_FIELD" ng-value="row.entity.PartsCode" required>\
                        <label class="input-group-btn">\
                            <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
                                <i class="fa fa-search fa-1"></i>\
                            </span>\
                        </label>\
                    </div>\
                </div>'
            },
            { name: 'Nama Material', sort: { priority: 0 }, width: '15%', field:"PartsName", cellTemplate: '<div class="ui-grid-cell-contents"><span>{{row.entity.PartsName}}</span></div>', enableCellEdit: false },
            { name: 'ETD', displayName: 'ETD', field: 'ETD', width: '9%', enableCellEdit: false, cellFilter: 'date:\'yyyy-MM-dd\'' },
            { name: 'ETA', displayName: 'ETA', field: 'ETA', width: '9%', enableCellEdit: false, cellFilter: 'date:\'yyyy-MM-dd\'' },
            { name: 'Qty PO', field: 'QtyPO', displayName: 'Qty PO', width: '6%', cellEditableCondition: function(scope){
                    if($scope.stateModeView){
                        return false;
                    } else {
                        if (scope.row.entity.PurchaseOrderStatusId <= 3 ) {
                            return true;
                        } else if(scope.row.entity.PurchaseOrderStatusId == 4 && scope.row.entity.QtyGR <=  scope.row.entity.QtyPO ){
                            return true;
                        } else if(scope.row.entity.PurchaseOrderStatusId === undefined){
                            return true
                        } else {
                            return false;
                        }
                    }
                }
            },
            { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', width: '6%', enableCellEdit: false, cellFilter: 'number' },
            { name: 'Satuan', field: 'Satuan', width: '5%', enableCellEdit: false },
            { name: 'Standard Cost', field: 'UnitPrice', displayName: 'Standard Cost', cellFilter: 'number', enableCellEdit: false },
            { name: 'Diskon %', displayName: '%', field: 'DiscountPercent', width: '3%', enableCellEdit: true, headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:270%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon</td></tr></tbody></table></div>%</div>" },
            { name: 'Diskon Jumlah', displayName: 'Jumlah', field: 'DiscountAmount', width: '5%', enableCellEdit: false, cellFilter: 'number', headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>", },
            { name: 'PPN', displayName: 'PPN', field: 'VATAmount', cellFilter: 'number', enableCellEdit: false },
            { name: 'Harga Total', field: 'TotalAmount', enableCellEdit: false, cellFilter: 'number', },
            { name: 'Action', cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>', visible : false },
            { name: 'UomId', field: 'UomId', visible: false },
            { name: 'PartsClassId4', field: 'PartsClassId4', visible: false },
            { name: 'Status TPOS', field: 'StatusTPOS', displayName: 'Status TPOS', enableCellEdit: false, cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.StatusTPOS">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>', },
            { name: 'Alasan Reject', field: 'ReasonTPOS', enableCellEdit: true, cellEditableCondition: true, allowCellFocus: true, visible: $scope.flagStatusTPOS }
        ]

        $scope.columnTAMCampaign = [
            { field: 'PartsCode', displayName: 'No. Material', width: '10%', enableCellEdit: false, },
            { name: 'Nama Material', sort: { priority: 0 }, width: '15%', field:"PartsName", cellTemplate: '<div class="ui-grid-cell-contents"><span>{{row.entity.PartsName}}</span></div>', enableCellEdit: false },
            { name: 'ETD', displayName: 'ETD', field: 'ETD', width: '9%', enableCellEdit: false, cellFilter: 'date:\'yyyy-MM-dd\'' },
            { name: 'ETA', displayName: 'ETA', field: 'ETA', width: '9%', enableCellEdit: false, cellFilter: 'date:\'yyyy-MM-dd\'' },
            { name: 'QuotaQtyMin', displayName: 'Quota Qty Min', field: 'QuotaQtyMin', width: '5%', enableCellEdit: false}, //Type C
            { name: 'QuotaQtyMax', displayName: 'Quota Qty Max', field: 'QuotaQtyMax', width: '5%', enableCellEdit: false}, //Type C
            { name: 'Qty PO', field: 'QtyPO', displayName: 'Qty PO', width: '6%', cellEditableCondition: function(scope){
                    if($scope.stateModeView){
                        return false;
                    } else {
                        if (scope.isBundle ) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                },                    
                editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">'
            },
            { name: 'RemainQuota', displayName: 'Remains Quota', field: 'RemainQuota', width: '5%', enableCellEdit: false}, //Type C
            { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', width: '6%', enableCellEdit: false, cellFilter: 'number' },
            { name: 'Satuan', field: 'Satuan', width: '5%', enableCellEdit: false },
            { name: 'UnitPrice', field: 'UnitPrice', displayName: 'Standard Cost', cellFilter: 'number', enableCellEdit: false },
            { name: 'Diskon %', displayName: '%', field: 'DiscountPercent', width: '3%', enableCellEdit: true, headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:270%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon</td></tr></tbody></table></div>%</div>" },
            { name: 'Diskon Jumlah', displayName: 'Jumlah', field: 'DiscountAmount', width: '5%', enableCellEdit: false, cellFilter: 'number', headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>", },
            { name: 'PPN', displayName: 'PPN', field: 'VATAmount', cellFilter: 'number', enableCellEdit: false },
            { name: 'Harga Total', field: 'TotalAmount', enableCellEdit: false, cellFilter: 'number', },
            { name: 'Action', cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>', visible : false },
            { name: 'UomId', field: 'UomId', visible: false },
            { name: 'PartsClassId4', field: 'PartsClassId4', visible: false },
            { name: 'Status TPOS', field: 'StatusTPOS', displayName: 'Status TPOS', enableCellEdit: false, cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.StatusTPOS">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>', },
            { name: 'Alasan Reject', field: 'ReasonTPOS', enableCellEdit: true, cellEditableCondition: true, allowCellFocus: true, visible: $scope.flagStatusTPOS }
        ]
        //----------------------------------
        // Grid Setup
        //----------------------------------
        //['PO TAM Tipe Order 1', 'PO TAM Tipe Order 2', 'PO TAM Tipe Order 3', 'PO NON TAM', 'PO SUBLET'];
        //['SOP - Appointment', 'SOP-WO', 'SOP-Sales Order', 'NON SOP'];
        $scope.ToNameRefPo = function(RefPoTypeID) {
            var GiName;
            switch (parseInt(RefPoTypeID)) {
                case 1:
                    GiName = "SOP - Appointment";
                    break;
                case 2:
                    GiName = "SOP-WO";
                    break;
                case 3:
                    GiName = "SOP-Sales Order";
                    break;
                case 4:
                    GiName = "NON SOP";
                    break;
            }

            return GiName;
        }
        $scope.ToNameTypePo = function(POTypeId,idCampaign) {
            var GiName;
            idCampaign = idCampaign == undefined ? null : idCampaign;

            if($scope.user.RoleId == 1122 || $scope.user.RoleId == 1123){
                if(parseInt(POTypeId) == 1){ //Type 1
                    GiName = "PO TAM Tipe Order 1";
                }else if (parseInt(POTypeId) == 2){ //Type 2
                    GiName = "PO TAM Tipe Order 2";
                }else if (parseInt(POTypeId) == 3 && idCampaign == null){ //Type 3
                    GiName = "PO TAM Tipe Order 3";
                }else if(parseInt(POTypeId) == 3 && idCampaign != null){ //Type 3 menjadi Campaign dengan sub type 3 karena ada campaignid
                    GiName = "PO TAM Campaign 3 & C";
                }else if (parseInt(POTypeId) == 6){ //Type 6
                    GiName = "PO TAM Tipe Order T";
                }else if (parseInt(POTypeId) == 7 && idCampaign == null){ //Type Campaign dengan sub type kosong
                    GiName = "PO TAM Campaign 3 & C";
                }else if (parseInt(POTypeId) == 7 && idCampaign != null){ //Type Campaign dengan sub type C
                    GiName = "PO TAM Campaign 3 & C";
                }else if(parseInt(POTypeId) == 4){ //Type PO NON TAM
                    GiName = "PO NON TAM";
                }
            }else{
                if(parseInt(POTypeId) == 1){ //Type 1
                    GiName = "PO TAM Tipe Order 1";
                }else if (parseInt(POTypeId) == 2){ //Type 2
                    GiName = "PO TAM Tipe Order 2";
                }else if (parseInt(POTypeId) == 3 && idCampaign == null){ //Type 3
                    GiName = "PO TAM Tipe Order 3";
                }else if(parseInt(POTypeId) == 3 && idCampaign != null){  //Type 3 menjadi Campaign dengan sub type 3 karena ada campaignid
                    GiName = "PO TAM Campaign 3 & C";
                }else if (parseInt(POTypeId) == 6){ //Type 6
                    GiName = "PO TAM Tipe Order T";
                }else if (parseInt(POTypeId) == 7 && idCampaign == null){ //Type Campaign dengan sub type kosong
                    GiName = "PO TAM Campaign 3 & C";
                }else if (parseInt(POTypeId) == 7 && idCampaign != null){ //Type Campaign dengan sub type C
                    GiName = "PO TAM Campaign 3 & C";
                }else if(parseInt(POTypeId) == 4){ //Type PO NON TAM
                    GiName = "PO NON TAM";
                }else if(parseInt(POTypeId) == 5){ //Type PO SUBLET
                    GiName = "PO SUBLET";
                }
            }
            
            return GiName;
        }

        $scope.ToNameStatus = function(PurchaseOrderId) {
            var statusName;
            switch (PurchaseOrderId) {
                case 1:
                    statusName = "Draft";
                    break;
                case 2:
                    statusName = "Request Approval";
                    break;
                case 3:
                    statusName = "Order";
                    break;
                case 4:
                    statusName = "Partial";
                    break;
                case 5:
                    statusName = "Completed";
                    break;
                case 6:
                    statusName = "Cancelled";
                    break;
                case 7:
                    statusName = "Closed";
                    break;
            }
            return statusName;
        }
        $scope.ToNameStatusTPOS = function(poTypeId,statusId) {
            var statusName;
            if ((poTypeId >=1 && poTypeId <= 3) || (poTypeId == 6)) {
                switch (statusId) {
                    case 1:
                        statusName = "On Process";
                        break;
                    case 2:
                        statusName = "Partial";
                        break;
                    case 3:
                        statusName = "Completed";
                        break;
                    case 4:
                        statusName = "Rejected";
                        break;
                }
            }
            return statusName;
        }

        $scope.ToNameMaterial = function(MaterialTypeId) {
            var materialName;
            switch (MaterialTypeId) {
                case 1:
                    materialName = "Parts";
                    break;
                case 2:
                    materialName = "Bahan";
                    break;
            }
            return materialName;
        }

        $scope.ToNamePackage = function(UomPOId) {
            var materialName;
            switch (UomPOId) {
                case 4:
                    materialName = "Pieces";
                    break;
                case 1:
                    materialName = "Box";
                    break;
            }
            return materialName;
        }

        $scope.ToNameBoTam = function(BackOrderFollowId) {
            var materialName;
            switch (BackOrderFollowId) {
                case 9:
                    materialName = "Fill";
                    break;
                case 1:
                    materialName = "Kill";
                    break;
                //Dibuat karena sudah banyak data yg salah karena dulunya hardcode ke 0 harusnya 1 kill 
                case 0:
                    materialName = "Kill";
                    break;
            }
            return materialName;
        }

        $scope.ToPaymentMethodId = function(PaymentMethodId) {
            var namePaymentMethodId;
            switch (PaymentMethodId) {
                case 1:
                    namePaymentMethodId = "Tagihan";
                    break;
                case 2:
                    namePaymentMethodId = "Tunai";
                    break;
            }
            return namePaymentMethodId;
        }

        $scope.ToNameStatusTPOSdetail = function(StatusTPOS) {
            var StatusTPOSName;
            switch (StatusTPOS) {
                case 1:
                    StatusTPOSName = "On Process";
                    break;
                case 2:
                    StatusTPOSName = "Accepted";
                    break;
                case 3:
                    StatusTPOSName = "Rejected";
                    break;
            }
            return StatusTPOSName;
        }

        $scope.ToReturnHour = function(datetime) {
            var time = $filter('date')(datetime, 'hh:mm');;

            return time;
        }

        $scope.gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
            // '<a href="#" uib-tooltip="Buat" tooltip-placement="bottom" ng-click="grid.appScope.actView(row.entity)" style="">Appointment</a>'+
            '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Lihat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Edit" ng-hide="row.entity.HasilSOQ == true || grid.appScope.$parent.user.RoleId==1128 || row.entity.PurchaseOrderStatusId == 5 || row.entity.PurchaseOrderStatusId == 6"  tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // {
                //     name: 'Id',
                //     field: 'PurchaseOrderId',
                //     width: '7%',
                //     visible: false
                // },
                // {
                //     name: 'Tipe Material',
                //     displayName: 'Tipe Material',
                //     field: 'MaterialTypeId',
                //     width: '7%',
                //     visible: false
                // },
                {
                    name: 'Tipe Material',
                    displayName: 'Tipe Material',
                    field: 'xMaterialTypeId',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameMaterial(row.entity.MaterialTypeId)}}</div>',
                    width: '5%'
                },
                // {
                //     displayName: 'Tipe PO',
                //     name: 'Tipe PO',
                //     field: 'POTypeId',
                //     width: '1%',
                //     visible: false
                // },
                {
                    name: 'Tipe PO',
                    displayName: 'Tipe PO',
                    field: 'xPOTypeId',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameTypePo(row.entity.POTypeId,row.entity.CampaignId)}}</div>',
                    width: '11%'
                },
                // {
                //     displayName: 'Referensi PO',
                //     name: 'Referensi PO',
                //     field: 'RefPOTypeId',
                //     width: '13%',
                //     visible: false
                // },
                {
                    name: 'Referensi PO',
                    displayName: 'Referensi PO',
                    field: 'xRefPOTypeId',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameRefPo(row.entity.RefPOTypeId)}}</div>',
                    width: '9%'
                },
                {
                    displayName: 'No. Referensi',
                    name: 'No. Referensi',
                    field: 'RefPONo',
                    width: '12%'
                },
                {
                    displayName: 'No. Polisi',
                    name: 'No. Polisi',
                    field: 'LicensePlate',
                    width: '7%'
                },
                {
                    name: 'Hasil SOQ',
                    displayName: 'Hasil SOQ',
                    field: 'HasilSOQ',
                    cellTemplate: '<input type="checkbox" ng-model="row.entity.HasilSOQ" disabled>',
                    width: '4%'
                },
                {
                    displayName: 'No PO',
                    name: 'No PO',
                    field: 'PurchaseOrderNo',
                    width: '7%'
                },
                {
                    displayName: 'Tanggal PO',
                    name: 'Tanggal PO',
                    field: 'DocDate',
                    cellFilter: 'date:\'yyyy-MM-dd\'',
                    width: '7%'
                },
                {
                    displayName: 'Vendor',
                    name: 'Vendor',
                    field: 'VendorName',
                    width: '12%'
                },
                // {
                //     displayName: 'BO TAM Follow',
                //     field: 'BackOrderFollowId',
                //     width: '13%',
                //     visible: false
                // },
                {
                    name: 'BO TAM Follow',
                    displayName: 'BO TAM Follow',
                    field: 'xBackOrderFollowId',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameBoTam(row.entity.BackOrderFollowId)}}</div>',
                    width: '5%'
                },
                // {
                //     name: 'Status',
                //     field: 'PurchaseOrderStatusId',
                //     width: '10%',
                //     visible: false
                // },
                {
                    name: 'Status',
                    displayName: 'Status',
                    field: 'xPurchaseOrderStatusId',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatus(row.entity.PurchaseOrderStatusId)}}</div>',
                    width: '6%'
                },
                {
                    name: 'StatusTPOS',
                    displayName: 'Status TPOS',
                    field: 'xStatusTPOS',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatusTPOS(row.entity.POTypeId,row.entity.StatusTPOS)}}</div>',
                    width: '6%'
                },
                // {
                //     name: 'Action',
                //     // width: '8%',
                //     width: 99,
                //     pinnedRight: true,
                //     cellTemplate: $scope.gridActionButtonTemplate
                // }
            ]
        };

        $scope.DiscountAmountResult = function(Qty, UnitPrice, DiscountPercent) {
            var result = 0;
            console.log("ini ada isinya?", Qty, UnitPrice, DiscountPercent);
            if (Qty == null || UnitPrice == null || DiscountPercent == null) {
                return 0;
            } else {
                result = Qty * UnitPrice * (DiscountPercent / 100);
                PurchaseOrder.GridDetail[PurchaseOrder.getETD()].DiscountAmount = result;
                PurchaseOrder.GridDetail[PurchaseOrder.getETD()].DiscountPercent = DiscountPercent;
            }
            return result;
        }

        // $scope.SubletDiscountAmountResult = function(Qty, UnitPrice, DiscountPercent, row)
        // {
        //   var result = 0;
        //   if(Qty == null || UnitPrice == null || DiscountPercent == null)
        //   {
        //     return 0;
        //   }
        //   else
        //   {
        //     result = Qty * UnitPrice * (DiscountPercent / 100);
        //     //PurchaseOrder.GridDetail[$scope.sublet_sop_wo.data.length - 1].DiscountAmount = result;
        //     //PurchaseOrder.GridDetail[$scope.sublet_sop_wo.data.length - 1].DiscountPercent = DiscountPercent;

        //     if($scope.State == 1)
        //       {
        //         $scope.mData.GridDetail[$scope.sublet_sop_wo.data.indexOf(row.entity)].DiscountAmount = result;
        //         $scope.mData.GridDetail[$scope.sublet_sop_wo.data.indexOf(row.entity)].DiscountPercent = DiscountPercent;

        //       }
        //       else if ($scope.State == 2)
        //       {
        //         $scope.mData.GridDetail[$scope.sublet_sop_wo.data.indexOf(row.entity)].DiscountAmount = result;
        //         $scope.mData.GridDetail[$scope.sublet_sop_wo.data.indexOf(row.entity)].DiscountAmount = result;

        //         $scope.mData.GridDetail[$scope.sublet_sop_wo.data.indexOf(row.entity)].DiscountPercent = DiscountPercent;
        //       }
        //   }
        //   return result;
        // }

        $scope.PPNResult = function(Qty, UnitPrice, DiscountPercent) {
            var result = 0;
            if (Qty == null || UnitPrice == null || DiscountPercent == null) {
                return 0;
            } else {
                // result = Qty * UnitPrice * (DiscountPercent / 100) * 0.1;
                result = Qty * UnitPrice * (DiscountPercent / 100) * (PPNPerc/100);

                PurchaseOrder.GridDetail[PurchaseOrder.getETD()].VATAmount = result;
            }
            return result;
        }

        $scope.SubletPPNResult = function(Qty, UnitPrice, DiscountPercent, row) {
            var result = 0;
            if (Qty == null || UnitPrice == null || DiscountPercent == null) {
                return 0;
            } else {
                // result = Qty * UnitPrice * (DiscountPercent / 100) * 0.1;
                result = Qty * UnitPrice * (DiscountPercent / 100) * (PPNPerc/100);

                $scope.mData.GridDetail[$scope.sublet_sop_wo.data.indexOf(row.entity)].VATAmount = result;
            }
            return result;
        }

        $scope.TotalAmount = function(QtyPO, StandardCost, row) {
            //console.log($scope.mData.GridDetail[$scope.tam_sop_appointment.data.indexOf(row.entity)]);
            if (QtyPO == null || StandardCost == null) {
                return 0;
            } else {
                // console.log("TotalAmount Appointment");
                var totalAmount = QtyPO * StandardCost;
                // console.log("QtyPO = ",QtyPO);
                // console.log("StandardCost = ",StandardCost);
                // console.log("Index = ", $scope.tam_sop_appointment.data.indexOf(row.entity));
                // console.log("$scope.formState ", $scope.formState);
                if ($scope.State == 1) {
                    $scope.mData.GridDetail[$scope.tam_sop_appointment.data.indexOf(row.entity)].TotalAmount = totalAmount;
                } else if ($scope.State == 2) {
                    $scope.mData.GridDetail[$scope.tam_sop_appointment_ubah.data.indexOf(row.entity)].TotalAmount = totalAmount;
                    $scope.mData.GridDetail[$scope.tam_sop_appointment.data.indexOf(row.entity)].TotalAmount = totalAmount;
                }
                return totalAmount;
            }

        }

        // $scope.SubletTotalAmount = function(QtyPO, StandardCost, DiscAmount, VATAmount, row){
        //   //console.log($scope.mData.GridDetail[$scope.tam_sop_appointment.data.indexOf(row.entity)]);
        //   if(QtyPO == null || StandardCost == null)
        //   {
        //     return 0;
        //   }
        //   else
        //   {
        //       //console.log("SubletTotalAmount");
        //       var totalAmount = ((QtyPO * StandardCost) - DiscAmount) + VATAmount ;
        //       //console.log("TotalAmount = ", totalAmount, QtyPO, StandardCost, DiscAmount, VATAmount, $scope.sublet_sop_wo.data.indexOf(row.entity));
        //       if($scope.State == 1)
        //       {
        //         $scope.mData.GridDetail[$scope.sublet_sop_wo.data.indexOf(row.entity)].TotalAmount = totalAmount;
        //       }
        //       else if ($scope.State == 2)
        //       {
        //         $scope.mData.GridDetail[$scope.sublet_sop_wo.data.indexOf(row.entity)].TotalAmount = totalAmount;
        //         $scope.mData.GridDetail[$scope.sublet_sop_wo_lihat.data.indexOf(row.entity)].TotalAmount = totalAmount;
        //       }
        //     return totalAmount;
        //   }

        // }

        $scope.NONTAMTotalAmount = function(QtyPO, StandardCost, row) {
            if (QtyPO == null || StandardCost == null) {
                return 0;
            } else {
                var totalAmount = QtyPO * StandardCost;
                // console.log("QtyPO = ",QtyPO);
                // console.log("StandardCost = ",StandardCost);
                // console.log(totalAmount);
                // console.log("State = ",$scope.State);
                if ($scope.State == 1) {
                    if (($scope.crudState == '142') || ($scope.crudState== '341')) {
                        $scope.mData.GridDetail[$scope.non_tam_sop_so.data.indexOf(row.entity)].TotalAmount = totalAmount;
                        console.log("$scope.mData.GridDetail[$scope.non_tam_sop_so.data.indexOf(row.entity)].TotalAmount ", $scope.mData.GridDetail[$scope.non_tam_sop_so.data.indexOf(row.entity)].TotalAmount);

                    } else if (($scope.crudState == '1441') || ($scope.crudState === '114') || ($scope.crudState === '124') || ($scope.crudState === '134')) {
                        $scope.non_tam_non_sop_bengkel.data[PurchaseOrder.gridDetailLength - 1].TotalAmount = totalAmount;
                        console.log("HEY YOU THERE",$scope.non_tam_non_sop_bengkel.data);
                        $scope.mData.GridDetail = $scope.non_tam_non_sop_bengkel.data;
                    }
                } else if ($scope.State == 2) {
                    if ($scope.crudState == '142') {
                        $scope.mData.GridDetail[$scope.non_tam_sop_wo.data.indexOf(row.entity)].TotalAmount = totalAmount;
                    } else if (($scope.crudState == '1441') || ($scope.crudState === '114') || ($scope.crudState === '124') || ($scope.crudState === '134')) {
                        $scope.non_tam_non_sop_bengkel.data[PurchaseOrder.gridDetailLength - 1].TotalAmount = totalAmount;
                        console.log("HEY YOU THERE",$scope.non_tam_non_sop_bengkel.data);
                        $scope.mData.GridDetail = $scope.non_tam_non_sop_bengkel.data;
                    } else if ($scope.crudState == '1442') {
                        $scope.non_tam_non_sop.data[PurchaseOrder.gridDetailLength - 1].TotalAmount = totalAmount;
                        $scope.mData.GridDetail = $scope.non_tam_non_sop.data;
                    }

                }
                return totalAmount;
            }
        }

        $scope.SumTotalAmount = function() {

                var totalAmount = 0;

                if ($scope.mData.GridDetail == null) {
    
    
                } else {
                    //$scope.mData.TotalAmount = $scope.mData.GridDetail[i].TotalAmount;
                    for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
                        //console.log("$scope.mData.GridDetail.length = ", $scope.mData.GridDetail.length);
                        //console.log("$scope.mData.GridDetail[i].TotalAmount = ", $scope.mData.GridDetail[i].TotalAmount);
    
                        totalAmount = totalAmount + $scope.mData.GridDetail[i].TotalAmount
                    }
    
                    $scope.mData.TotalAmount = totalAmount;

                    if ($scope.crudState != '152' && $scope.crudState != '151'){
                        // ------------------------ pake picing engine dong ------------------------------------- start 
                        $scope.mData.TotalAmount =  ASPricingEngine.calculate({
                                                        InputPrice: $scope.mData.TotalAmount,
                                                        Discount: 0,
                                                        Qty: 0,
                                                        Tipe: 25, 
                                                        PPNPercentage: PPNPerc,
                                                    });
                        // ------------------------ pake picing engine dong ------------------------------------- end 
                    }
    
                }
    
                console.log("totalAmountX ", totalAmount)

                return totalAmount;
            

        }



        $scope.Add = function() {

            console.log("ADD...");
            if ($scope.crudState === '142') {
                var idx = 0;
                if ($scope.non_tam_sop_so.data.length > 0) {
                    var lengthGrid = $scope.non_tam_sop_so.data.length - 1;
                    idx = $scope.non_tam_sop_so.data[lengthGrid].idx + 1;
                }
                var n = $scope.non_tam_sop_so.data.length + 1;
                $scope.non_tam_sop_so.data.push({
                    //yap : tambah purchase order item id
                    "PurchaseOrderItemId": 0,
                    "NoMaterial": "",
                    "PartsName": "",
                    "ETA": "",
                    "ETD": "",
                    "QtyPO": 0,
                    "QtyBase": 0,
                    "UomPOId": 0,
                    "UnitPrice": 0,
                    "DiscountPercent": 0,
                    "DiscountAmount": 0,
                    "VATAmount": 0,
                    "TotalAmount": 0,
                    "CreatedDate": $scope.localeDate(new(Date)),
                    "CreatedUserId": $scope.UserId, 
                    "idx":idx
                });

                PurchaseOrder.gridDetailLength = n;
            } else if(($scope.crudState === '124') || ($scope.crudState === '134')) {
                var n = $scope.non_tam_non_sop_bengkel.data.length + 1;
                var idx = 0;
                if ($scope.non_tam_non_sop_bengkel.data.length > 0) {
                    var lengthGrid = $scope.non_tam_non_sop_bengkel.data.length - 1;
                    idx = $scope.non_tam_non_sop_bengkel.data[lengthGrid].idx + 1;
                }
                $scope.non_tam_non_sop_bengkel.data.push({
                    //yap : tambah purchase order item id
                    "PurchaseOrderItemId": 0,
                    "NoMaterial": "",
                    "PartsName": "",
                    "ETA": "",
                    "ETD": "",
                    "QtyPO": 0,
                    "QtyBase": 0,
                    "UomPOId": 0,
                    "UnitPrice": 0,
                    "DiscountPercent": 0,
                    "DiscountAmount": 0,
                    "VATAmount": 0,
                    "TotalAmount": 0,
                    "PartsClassId4": 0,
                    "CreatedDate": $scope.localeDate(new(Date)),
                    "CreatedUserId": $scope.UserId, 
                    "idx":idx
                });

                PurchaseOrder.gridDetailLength = n;
            } else if(($scope.crudState === '114')) {
                var n = $scope.non_tam_non_sop_bengkel.data.length + 1;
                var idx = 0;
                if ($scope.non_tam_non_sop_bengkel.data.length > 0) {
                    var lengthGrid = $scope.non_tam_non_sop_bengkel.data.length - 1;
                    idx = $scope.non_tam_non_sop_bengkel.data[lengthGrid].idx + 1;
                }
                $scope.non_tam_non_sop_bengkel.data.push({
                    //yap : tambah purchase order item id
                    "PurchaseOrderItemId": 0,
                    "NoMaterial": "",
                    "PartsName": "",
                    "ETA": "",
                    "ETD": "",
                    "QtyPO": 0,
                    "QtyBase": 0,
                    "UomPOId": 0,
                    "UnitPrice": 0,
                    "DiscountPercent": 0,
                    "DiscountAmount": 0,
                    "VATAmount": 0,
                    "TotalAmount": 0,
                    "PartsClassId4": 0,
                    "CreatedDate": $scope.localeDate(new(Date)),
                    "CreatedUserId": $scope.UserId, 
                    "idx":idx
                });

                PurchaseOrder.gridDetailLength = n;
            } else if(($scope.crudState === '313')) {
                var n = $scope.non_tam_sop_so_ubah.data.length + 1;
                var idx = 0;
                if ($scope.non_tam_sop_so_ubah.data.length > 0) {
                    var lengthGrid = $scope.non_tam_sop_so_ubah.data.length - 1;
                    idx = $scope.non_tam_sop_so_ubah.data[lengthGrid].idx + 1;
                }
                $scope.non_tam_sop_so_ubah.data.push({
                    //yap : tambah purchase order item id
                    "PurchaseOrderItemId": 0,
                    "NoMaterial": "",
                    "PartsName": "",
                    "ETA": "",
                    "ETD": "",
                    "QtyPO": 0,
                    "QtyBase": 0,
                    "UomPOId": 0,
                    "UnitPrice": 0,
                    "DiscountPercent": 0,
                    "DiscountAmount": 0,
                    "VATAmount": 0,
                    "TotalAmount": 0,
                    "PartsClassId4": 0,
                    "CreatedDate": $scope.localeDate(new(Date)),
                    "CreatedUserId": $scope.UserId, 
                    "idx":idx
                });

                PurchaseOrder.gridDetailLength = n;
            } else if(($scope.crudState === '314')) {
                var n = $scope.non_tam_non_sop_bengkel.data.length + 1;
                var idx = 0;
                if ($scope.non_tam_non_sop_bengkel.data.length > 0) {
                    var lengthGrid = $scope.non_tam_non_sop_bengkel.data.length - 1;
                    idx = $scope.non_tam_non_sop_bengkel.data[lengthGrid].idx + 1;
                }
                $scope.non_tam_non_sop_bengkel.data.push({
                    //yap : tambah purchase order item id
                    "PurchaseOrderItemId": 0,
                    "NoMaterial": "",
                    "PartsName": "",
                    "ETA": "",
                    "ETD": "",
                    "QtyPO": 0,
                    "QtyBase": 0,
                    "UomId": "",
                    "UnitPrice": 0,
                    "DiscountPercent": 0,
                    "DiscountAmount": 0,
                    "VATAmount": 0,
                    "TotalAmount": 0,
                    "PartsClassId4": 0,
                    "CreatedDate": $scope.localeDate(new(Date)),
                    "CreatedUserId": $scope.UserId, 
                    "idx":idx
                });

                PurchaseOrder.gridDetailLength = n;
            } else if(($scope.crudState === '312')) {
                var n = $scope.tam_sop_wo_ubah.data.length + 1;
                var idx = 0;
                if ($scope.tam_sop_wo_ubah.data.length > 0) {
                    var lengthGrid = $scope.tam_sop_wo_ubah.data.length - 1;
                    idx = $scope.tam_sop_wo_ubah.data[lengthGrid].idx + 1;
                }
                $scope.tam_sop_wo_ubah.data.push({
                    //yap : tambah purchase order item id
                    "PurchaseOrderItemId": 0,
                    "NoMaterial": "",
                    "PartsName": "",
                    "ETA": "",
                    "ETD": "",
                    "QtyPO": 0,
                    "QtyBase": 0,
                    "UomId": "",
                    "UnitPrice": 0,
                    "DiscountPercent": 0,
                    "DiscountAmount": 0,
                    "VATAmount": 0,
                    "TotalAmount": 0,
                    "PartsClassId4": 0,
                    "CreatedDate": $scope.localeDate(new(Date)),
                    "CreatedUserId": $scope.UserId, 
                    "idx":idx
                });

                PurchaseOrder.gridDetailLength = n;
            } else if(($scope.crudState === '311')) {
                var n = $scope.tam_sop_appointment_ubah.data.length + 1;
                var idx = 0;
                if ($scope.tam_sop_appointment_ubah.data.length > 0) {
                    var lengthGrid = $scope.tam_sop_appointment_ubah.data.length - 1;
                    idx = $scope.tam_sop_appointment_ubah.data[lengthGrid].idx + 1;
                }
                $scope.tam_sop_appointment_ubah.data.push({
                    //yap : tambah purchase order item id
                    "PurchaseOrderItemId": 0,
                    "NoMaterial": "",
                    "PartsName": "",
                    "ETA": "",
                    "ETD": "",
                    "QtyPO": 0,
                    "QtyBase": 0,
                    "UomId": "",
                    "UnitPrice": 0,
                    "DiscountPercent": 0,
                    "DiscountAmount": 0,
                    "VATAmount": 0,
                    "TotalAmount": 0,
                    "PartsClassId4": 0,
                    "CreatedDate": $scope.localeDate(new(Date)),
                    "CreatedUserId": $scope.UserId, 
                    "idx":idx
                });

                PurchaseOrder.gridDetailLength = n;
            } else if ($scope.crudState === '1441') {
                var n = $scope.non_tam_non_sop_bengkel.data.length + 1;
                var idx = 0;
                if ($scope.non_tam_non_sop_bengkel.data.length > 0) {
                    var lengthGrid = $scope.non_tam_non_sop_bengkel.data.length - 1;
                    idx = $scope.non_tam_non_sop_bengkel.data[lengthGrid].idx + 1;
                }
                $scope.non_tam_non_sop_bengkel.data.push({
                    //yap : tambah purchase order item id
                    "PurchaseOrderItemId": 0,
                    "NoMaterial": "",
                    "PartsName": "",
                    "ETA": "",
                    "ETD": "",
                    "QtyPO": 0,
                    "QtyBase": 0,
                    "UomPOId": 0,
                    "UnitPrice": 0,
                    "DiscountPercent": 0,
                    "DiscountAmount": 0,
                    "VATAmount": 0,
                    "TotalAmount": 0,
                    "PartsClassId4": 0,
                    "CreatedDate": $scope.localeDate(new(Date)),
                    "CreatedUserId": $scope.UserId, 
                    "idx": idx
                });

                PurchaseOrder.gridDetailLength = n;
            } else if ($scope.crudState === '1442') {
                var n = $scope.non_tam_non_sop.data.length + 1;
                $scope.non_tam_non_sop.data.push({
                    //yap : tambah purchase order item id
                    "PurchaseOrderItemId": 0,
                    "NoMaterial": "",
                    "PartsName": "",
                    "ETA": "",
                    "ETD": "",
                    "QtyPO": 0,
                    "QtyBase": 0,
                    "UomPOId": 0,
                    "UnitPrice": 0,
                    "DiscountPercent": 0,
                    "DiscountAmount": 0,
                    "VATAmount": 0,
                    "TotalAmount": 0,
                    "PartsClassId4": 0,
                    "CreatedDate": $scope.localeDate(new(Date)),
                    "CreatedUserId": $scope.UserId, 
                });

                PurchaseOrder.gridDetailLength = n;
            } else if(($scope.crudState === '3441' || $scope.crudState === '3421')) { // condition edit po non tam - non sop
                var n = $scope.non_tam_non_sop_bengkel.data.length + 1;
                var idx = 0;
                if ($scope.non_tam_non_sop_bengkel.data.length > 0) {
                    var lengthGrid = $scope.non_tam_non_sop_bengkel.data.length - 1;
                    idx = $scope.non_tam_non_sop_bengkel.data[lengthGrid].idx + 1;
                }
                $scope.non_tam_non_sop_bengkel.data.push({
                    //yap : tambah purchase order item id
                    "PurchaseOrderItemId": 0,
                    "NoMaterial": "",
                    "PartsName": "",
                    "ETA": "",
                    "ETD": "",
                    "QtyPO": 0,
                    "QtyBase": 0,
                    "UomId": "",
                    "UnitPrice": 0,
                    "DiscountPercent": 0,
                    "DiscountAmount": 0,
                    "VATAmount": 0,
                    "TotalAmount": 0,
                    "PartsClassId4": 0,
                    "CreatedDate": $scope.localeDate(new(Date)),
                    "CreatedUserId": $scope.UserId, 
                    "idx":idx
                });

                PurchaseOrder.gridDetailLength = n;
            } else if ($scope.crudState === '3442') {
                var n = $scope.non_tam_non_sop.data.length + 1;
                $scope.non_tam_non_sop.data.push({
                    //yap : tambah purchase order item id
                    "PurchaseOrderItemId": 0,
                    "NoMaterial": "",
                    "PartsName": "",
                    "ETA": "",
                    "ETD": "",
                    "QtyPO": 0,
                    "QtyBase": 0,
                    "UomPOId": 0,
                    "UnitPrice": 0,
                    "DiscountPercent": 0,
                    "DiscountAmount": 0,
                    "VATAmount": 0,
                    "TotalAmount": 0,
                    "PartsClassId4": 0,
                    "CreatedDate": $scope.localeDate(new(Date)),
                    "CreatedUserId": $scope.UserId, 
                });

                PurchaseOrder.gridDetailLength = n;
            } else {
                console.log("sadasdadasdsa");
            }
        }

        $scope.ToDate = function(data) {
            var endDate = $filter('date')(data, 'yyyy-MM-dd');
            return endDate
        }

        //Perubahan Kolom
        

        $scope.SearchMaterial = function(NoMaterial) {
        //   console.log('SearchMaterial',$scope.mData, NoMaterial, $scope.NoDeadStock,$scope.PartsName);
        console.log('SearchMaterial | mData =============>',$scope.mData);
        console.log('SearchMaterial | NoMaterial ========>',NoMaterial);
        console.log('SearchMaterial | NoDeadStock =======>',$scope.NoDeadStock);
        console.log('SearchMaterial | namavendorid ======>',$scope.namavendorid);
        console.log('SearchMaterial | PartsName =========>',$scope.PartsName);
        console.log("SearchMaterial | disabledTambah ====>",$scope.disabledTambah);
        console.log("SearchMaterial | POTypeId ==========>",$scope.mData.POTypeId);
        console.log("SearchMaterial | RefPOTypeId =======>",$scope.mData.RefPOTypeId);


        if($scope.disabledTambah == false){
              if ($scope.namavendorid != undefined || $scope.namavendorid != null) {
                console.log("masuk sini");
                $scope.mData.VendorId = $scope.namavendorid;
                $scope.mData.PaymentMethodId = $scope.PaymentMethodId;
              }
                console.log("$scope.mData.VendorId==>",$scope.mData.VendorId);
                if (NoMaterial === "" || NoMaterial == null) {
                    bsNotify.show({
                        title: "Purchase Order ",
                        content: "No Material tidak boleh kosong",
                        type: 'danger'
                    });
                } else {
                    if ($scope.mData.POTypeId == "4" && $scope.mData.RefPOTypeId == "4") {

                        console.log('SearchMaterial | masuk if ========>',$scope.mData.POTypeId, $scope.mData.RefPOTypeId);


                        console.log("$scope.namavendorid",$scope.namavendorid);
                      if ($scope.mData.VendorId === undefined && $scope.namavendorid === undefined) {
                        bsNotify.show({
                            title: "Purchase Order ",
                            content: "Mohon Pilih Vendor Terlebih Dahulu",
                            type: 'danger'
                        });
                      }else {
                        if ($scope.mData.VendorId != null || $scope.mData.VendorId != undefined)
                            {
                                if ( $scope.NoDeadStock != null || $scope.NoDeadStock != undefined){
                                    var dataFilter = {PartsCode : NoMaterial , VendorId: $scope.mData.VendorId, NoDeadStock : $scope.NoDeadStock, WarehouseId : $scope.mData.WarehouseId};
                                } else{
                                    var dataFilter = {PartsCode : NoMaterial , VendorId: $scope.mData.VendorId, NoDeadStock : null, WarehouseId : $scope.mData.WarehouseId};
                                }}
                        else{
                            if ( $scope.NoDeadStock != null || $scope.NoDeadStock != undefined){
                                var dataFilter = {PartsCode : NoMaterial , VendorId: $scope.namavendorid , NoDeadStock : $scope.NoDeadStock, WarehouseId : $scope.mData.WarehouseId};
                            } else{
                                var dataFilter = {PartsCode : NoMaterial , VendorId: $scope.namavendorid , NoDeadStock : null, WarehouseId : $scope.mData.WarehouseId};
                            }
                        }
                        //var dataFilter = {PartsCode : NoMaterial , VendorId: $scope.mData.VendorId };
                        console.log ("dataFilter",dataFilter);
                        PurchaseOrder.getDetailByMaterialNoVendor(dataFilter).then(function(res) {

                                var gridData = res.data.Result;
                                console.log("gridData: ",gridData);

                                if (typeof gridData[0] === undefined || gridData[0] == null) {
                                    bsNotify.show({
                                        title: "Purchase Order",
                                        content: "Nomor Material tidak ditemukan",
                                        type: 'danger'
                                    });
                                // }else if (typeof gridData[0].RetailPrice === 0 || gridData[0].RetailPrice == 0) {
                                //     bsNotify.show({
                                //         title: "Purchase Order",
                                //         content: "Harga Material tidak ditemukan",
                                //         type: 'danger'
                                //     });
                                } else {
                                    if(gridData[0].StandardCost === 0){
                                        bsAlert.warning("Standard cost belum ada.","Harap update terlebih dahulu");
                                        return false;
                                    }
                                    $scope.checkRefGINo = false;
                                    var getDataParameter = [];
                                    var tempParameter = [5];
                                    var todayDate = new Date();
                                    var checkHours = todayDate.getHours(),
                                        hour;
                                    //condition to set currentTime
                                    if (checkHours < 10) {
                                        hour = '0' + todayDate.getHours();
                                    } else {
                                        hour = todayDate.getHours();
                                    }
                                    var currentTime = hour + ":" + todayDate.getMinutes() + ":" + todayDate.getSeconds();
                                    var ValidThru = '';
                                    var EffectiveDate = '';
                                    var OrderTypeId = 0;
                                    var CutOffTime = '';
                                    var ETDDayPlus = '';
                                    var ETDHour = '';
                                    var LeadTimeArrival = '';
                                    var ETDTemp = '',
                                        ETD = '',
                                        ETDFix = '';
                                    var ETATemp = '',
                                        ETATempHour = '',
                                        ETAFix = '',
                                        ETAETO = '';

                                    //parameter order: ValidThru, OrderTypeId, CutOffTime, ETDDayPlus, ETDHour, LeadTimeArrival
                                    OrderLeadTime.getData().then(
                                        function(res) {
                                            getDataParameter = res.data.Result;
                                            var indexOfValidThru = 0;
                                            ValidThru = new Date(getDataParameter[0].ValidThru);
                                            EffectiveDate = new Date(getDataParameter[0].EffectiveDate);
                                            console.log("getDataParameter: ", getDataParameter);
                                            console.log("currentTime: ", currentTime);
                                            console.log("todayDate: ", todayDate);
                                            console.log('initype', $scope.mData.POTypeId);
                                            if ($scope.mData.POTypeId == 1) {
                                                if (ValidThru >= todayDate && EffectiveDate <= todayDate) {
                                                    var i = 0;
                                                    while (getDataParameter.length > 0 && i < getDataParameter.length) {
                                                        console.log(i, " lll ", getDataParameter[i].OrderTypeId);
                                                        if (getDataParameter[i].OrderTypeId == 1 && $scope.mData.POTypeId == 1) {
                                                            // if (getDataParameter[i].OrderTypeId==1||getDataParameter[i].OrderTypeId==2||getDataParameter[i].OrderTypeId==3){

                                                            //not today ETD
                                                            if (currentTime >= getDataParameter[i].CutOffTime) {
                                                                ETDTemp = new Date(todayDate);
                                                                ETDTemp.setDate(todayDate.getDate() + 1 + getDataParameter[i].ETDDayPlus);
                                                                console.log("b");
                                                            } else {
                                                                ETDTemp = todayDate;
                                                                console.log("test hari");
                                                            }
                                                            ETD = $scope.changeFormatDate(ETDTemp);
                                                            ETDFix = ETD + ' ' + getDataParameter[i].ETDHour;
                                                            var ETAtanggal = new Date(ETDFix);
                                                            ETATempHour = ETAtanggal.getHours();
                                                            ETATemp = ETATempHour + getDataParameter[i].LeadTimeArrival;
                                                            // if (ETATemp>24){
                                                            //   ETAETO = ETATemp-24;
                                                            //   if(ETAETO<10){
                                                            //     ETAETO='0'+ETAETO;
                                                            //   }
                                                            //   else{
                                                            //     ETAETO=ETAETO;
                                                            //   }
                                                            //   ETAtanggal = ETAtanggal.setDate(ETD.getDate()+1);
                                                            //   console.log('ETA tanggal', ETAtanggal);
                                                            // }
                                                           var ETAminit = ETAtanggal.getMinutes();
                                                            if (ETAminit < 10) {
                                                                ETAminit = '0' + ETAminit;
                                                            } else {
                                                                ETAminit = ETAminit;
                                                            }
                                                            var ETAdetik = ETAtanggal.getSeconds();
                                                            if (ETAdetik < 10) {
                                                                ETAdetik = '0' + ETAdetik;
                                                            } else {
                                                                ETAdetik = ETAdetik;
                                                            }
                                                            ETAFix = ETD + ' ' + ETATemp + ':' + ETAminit + ':' + ETAdetik;

                                                            console.log(i, "ETAFiwx", ETAFix);
                                                            console.log(i, "CutOffTime", getDataParameter[i].CutOffTime);
                                                            console.log(i, "ETDTemp", ETDTemp);
                                                            console.log(i, "ETD", ETD + getDataParameter[i].ETDHour);
                                                            console.log(i, "ETDDayPlus", getDataParameter[i].ETDDayPlus);
                                                            break;
                                                        } else {
                                                            console.log("data percobaan 1");
                                                        }
                                                        i++;
                                                    }
                                                } else {
                                                    console.log("not valid");
                                                }
                                            } else if ($scope.mData.POTypeId == 2) {
                                                //   console.log('ini yang 2');
                                                //
                                                if (ValidThru >= todayDate && EffectiveDate <= todayDate) {
                                                    var i = 0;
                                                    while (getDataParameter.length > 0 && i < getDataParameter.length) {
                                                        console.log(i, " lll ", getDataParameter[i].OrderTypeId);
                                                        if (getDataParameter[i].OrderTypeId == 2 && $scope.mData.POTypeId == 2) {
                                                            // if (getDataParameter[i].OrderTypeId==1||getDataParameter[i].OrderTypeId==2||getDataParameter[i].OrderTypeId==3){

                                                            //not today ETD
                                                            if (currentTime >= getDataParameter[i].CutOffTime) {
                                                                ETDTemp = new Date(todayDate);
                                                                ETDTemp.setDate(todayDate.getDate() + 1 + getDataParameter[i].ETDDayPlus);
                                                                console.log("b");
                                                            } else {
                                                                ETDTemp = todayDate;
                                                                console.log("test hari");
                                                            }
                                                            ETD = $scope.changeFormatDate(ETDTemp);
                                                            ETDFix = ETD + ' ' + getDataParameter[i].ETDHour;
                                                            var ETAtanggal = new Date(ETDFix);
                                                            ETATempHour = ETAtanggal.getHours();
                                                            ETATemp = ETATempHour + getDataParameter[i].LeadTimeArrival;
                                                            // if (ETATemp>24){
                                                            //   ETAETO = ETATemp-24;
                                                            //   if(ETAETO<10){
                                                            //     ETAETO='0'+ETAETO;
                                                            //   }
                                                            //   else{
                                                            //     ETAETO=ETAETO;
                                                            //   }
                                                            //   ETAtanggal = ETAtanggal.setDate(ETD.getDate()+1);
                                                            //   console.log('ETA tanggal', ETAtanggal);
                                                            // }
                                                            var ETAminit = ETAtanggal.getMinutes();
                                                            if (ETAminit < 10) {
                                                                ETAminit = '0' + ETAminit;
                                                            } else {
                                                                ETAminit = ETAminit;
                                                            }
                                                            var ETAdetik = ETAtanggal.getSeconds();
                                                            if (ETAdetik < 10) {
                                                                ETAdetik = '0' + ETAdetik;
                                                            } else {
                                                                ETAdetik = ETAdetik;
                                                            }
                                                            ETAFix = ETD + ' ' + ETATemp + ':' + ETAminit + ':' + ETAdetik;

                                                            console.log(i, "ETAFiex", ETAFix);
                                                            console.log(i, "CutOffTime", getDataParameter[i].CutOffTime);
                                                            console.log(i, "ETDTemp", ETDTemp);
                                                            console.log(i, "ETD", ETD + getDataParameter[i].ETDHour);
                                                            console.log(i, "ETDDayPlus", getDataParameter[i].ETDDayPlus);
                                                            break;
                                                        } else {
                                                            console.log("data percobaan 2");
                                                        }
                                                        i++;
                                                    }
                                                } else {
                                                    console.log("not valid");
                                                }
                                            } else if ($scope.mData.POTypeId == 3 || $scope.mData.POTypeId == 6) {
                                                //   console.log('ini yang 2');
                                                //
                                                if (ValidThru >= todayDate && EffectiveDate <= todayDate) {
                                                    var i = 0;
                                                    while (getDataParameter.length > 0 && i < getDataParameter.length) {
                                                        console.log(i, " lll ", getDataParameter[i].OrderTypeId);
                                                        if ((getDataParameter[i].OrderTypeId == 3 && $scope.mData.POTypeId == 3) || (getDataParameter[i].OrderTypeId == 6 && $scope.mData.POTypeId == 6)) {
                                                            // if (getDataParameter[i].OrderTypeId==1||getDataParameter[i].OrderTypeId==2||getDataParameter[i].OrderTypeId==3){

                                                            //not today ETD
                                                            if (currentTime >= getDataParameter[i].CutOffTime) {
                                                                ETDTemp = new Date(todayDate);
                                                                ETDTemp.setDate(todayDate.getDate() + 1 + getDataParameter[i].ETDDayPlus);
                                                                console.log("b");
                                                            } else {
                                                                ETDTemp = todayDate;
                                                                console.log("test hari");
                                                            }
                                                            ETD = $scope.changeFormatDate(ETDTemp);
                                                            ETDFix = ETD + ' ' + getDataParameter[i].ETDHour;
                                                            var ETAtanggal = new Date(ETDFix);
                                                            ETATempHour = ETAtanggal.getHours();
                                                            ETATemp = ETATempHour + getDataParameter[i].LeadTimeArrival;
                                                            // if (ETATemp>24){
                                                            //   ETAETO = ETATemp-24;
                                                            //   if(ETAETO<10){
                                                            //     ETAETO='0'+ETAETO;
                                                            //   }
                                                            //   else{
                                                            //     ETAETO=ETAETO;
                                                            //   }
                                                            //   ETAtanggal = ETAtanggal.setDate(ETD.getDate()+1);
                                                            //   console.log('ETA tanggal', ETAtanggal);
                                                            // }
                                                            var ETAminit = ETAtanggal.getMinutes();
                                                            if (ETAminit < 10) {
                                                                ETAminit = '0' + ETAminit;
                                                            } else {
                                                                ETAminit = ETAminit;
                                                            }
                                                            var ETAdetik = ETAtanggal.getSeconds();
                                                            if (ETAdetik < 10) {
                                                                ETAdetik = '0' + ETAdetik;
                                                            } else {
                                                                ETAdetik = ETAdetik;
                                                            }
                                                            ETAFix = ETD + ' ' + ETATemp + ':' + ETAminit + ':' + ETAdetik;

                                                            console.log(i, "ETAFiox", ETAFix);
                                                            console.log(i, "CutOffTime", getDataParameter[i].CutOffTime);
                                                            console.log(i, "ETDTemp", ETDTemp);
                                                            console.log(i, "ETD", ETD + getDataParameter[i].ETDHour);
                                                            console.log(i, "ETDDayPlus", getDataParameter[i].ETDDayPlus);
                                                            break;
                                                        } else {
                                                            console.log("data percobaan 3");
                                                        }
                                                        i++;
                                                    }
                                                } else {
                                                    console.log("not valid");
                                                }
                                            }

                                            console.log("crudState:" + $scope.crudState);
                                            PurchaseOrder.crudState = $scope.crudState;
                                            if ($scope.crudState === '142') {
                                                var cekInput = '';
                                                var cekRes = angular.copy(gridData[0].PartsName)
                                                var posisi = 0;
                                                for (var i=0; i<$scope.non_tam_sop_so.data.length; i++){
                                                    cekInput = angular.copy($scope.non_tam_sop_so.data[i].PartsName);
                                                    if (cekInput != undefined && cekInput != null){
                                                        if (cekInput.toUpperCase() == cekRes.toUpperCase()){
                                                            posisi = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                                $scope.non_tam_sop_so.data[posisi].PartsName = gridData[0].PartsName;
                                                $scope.non_tam_sop_so.data[posisi].UomId = gridData[0].UomId;
                                                $scope.non_tam_sop_so.data[posisi].UomPOId = gridData[0].UomId;
                                                $scope.non_tam_sop_so.data[posisi].Name = gridData[0].Name;
                                                $scope.non_tam_sop_so.data[posisi].PartId = gridData[0].PartsId;
                                                $scope.non_tam_sop_so.data[posisi].QtyBase = 0;
                                                $scope.non_tam_sop_so.data[posisi].UomBaseId = gridData[0].UomId;

                                                // =================
												//$scope.non_tam_sop_so.data[posisi].DiscountAmount = Math.round((($scope.non_tam_sop_so.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice));
												// CR5 #10
												console.log('$scope.mData.IsNONPKP at non_tam_sop_so ($scope.crudState === 142) stv: ', $scope.mData.IsNONPKP);
												if($scope.mData.IsNONPKP == false){
													//PKP
													// $scope.non_tam_sop_so.data[posisi].UnitPrice = $scope.non_tam_sop_so.data[posisi].StandardCost / 1.1;
													$scope.non_tam_sop_so.data[posisi].UnitPrice = $scope.non_tam_sop_so.data[posisi].StandardCost / (1+(PPNPerc/100));

													$scope.non_tam_sop_so.data[posisi].DiscountAmount = Math.round((($scope.non_tam_sop_so.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice));

													// $scope.non_tam_sop_so.data[posisi].VATAmount = Math.round((($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice) - ($scope.non_tam_sop_so.data[posisi].DiscountAmount)) * 0.1);
													$scope.non_tam_sop_so.data[posisi].VATAmount = Math.round((($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice) - ($scope.non_tam_sop_so.data[posisi].DiscountAmount)) * (PPNPerc/100));

												}else if($scope.mData.IsNONPKP == true){
													//Non PKP
													$scope.non_tam_sop_so.data[posisi].VATAmount = 0;
													$scope.non_tam_sop_so.data[posisi].UnitPrice = $scope.non_tam_sop_so.data[posisi].StandardCost;
													$scope.non_tam_sop_so.data[posisi].DiscountAmount = Math.round((($scope.non_tam_sop_so.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice));
												}
												// CR5 #10
												//$scope.non_tam_sop_so.data[posisi].VATAmount = Math.round((($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice) - ($scope.non_tam_sop_so.data[posisi].DiscountAmount)) * 0.1);
												//Total  = (Qty * HargaDasar) - Diskon + VAT;
                                                if($scope.non_tam_sop_so.data[posisi].QtyPO){
                                                    $scope.non_tam_sop_so.data[posisi].TotalAmount = ($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice) - $scope.non_tam_sop_so.data[posisi].DiscountAmount + $scope.non_tam_sop_so.data[posisi].VATAmount;
                                                }
                                                // =============
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].PartsName = gridData[0].PartsName;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].UomId = gridData[0].UomId;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].UomPOId = gridData[0].UomId;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].Name = gridData[0].Name;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].PartId = gridData[0].PartsId;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].QtyBase = 0;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].UomBaseId = gridData[0].UomId;

                                                $scope.mData.GridDetail = $scope.non_tam_sop_so.data;
                                            } else if (($scope.crudState === '114') || ($scope.crudState === '124') || ($scope.crudState === '134')) {
                                                var cekInput = '';
                                                var cekRes = angular.copy(gridData[0].PartsCode)
                                                var posisi = 0;
                                                for (var i=0; i<$scope.non_tam_non_sop_bengkel.data.length; i++){
                                                    cekInput = angular.copy($scope.non_tam_non_sop_bengkel.data[i].PartsCode);
                                                    if (cekInput != undefined && cekInput != null){
                                                        if (cekInput.toUpperCase() == cekRes.toUpperCase()){
                                                            posisi = i;
                                                            break;
                                                        }
                                                    }
                                                }

                                                // ===================replace galang==================
                                                //  var UnitPrice = gridData[0].StandardCost / (1.1);
                                                //  $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = Math.round(UnitPrice);
                                                //  $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost = Math.round(UnitPrice);
                                                //  var HargaSatuan    = $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice;
                                                //  var DiscountSatuan = ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100) * HargaSatuan;
                                                //  var VATSatuan      = (HargaSatuan - DiscountSatuan) * 0.1;

                                                //  $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan;
                                                //  $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = Math.floor($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * VATSatuan);
                                                //  $scope.non_tam_non_sop_bengkel.data[posisi].TotalAmount = Math.round(($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount);
                                                // ===================replace galang==================

                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsCode = gridData[0].PartsCode;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsName = gridData[0].PartsName;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UomId = gridData[0].UomId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UomPOId = gridData[0].UomId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].Name = gridData[0].Satuan;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].ETD = ETDFix;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].ETA = ETAFix;
                                                // $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = gridData[0].StandardCost;
                                                //  commented 07/10/2020
                                                // $scope.non_tam_non_sop_bengkel.data[posisi].xUnitPrice = gridData[0].StandardCost; //default price with ppn
                                                
												// $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1.1));
												$scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1+(PPNPerc/100)));

												
                                                $scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartId = gridData[0].PartId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].OutletId = gridData[0].OutletId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].WarehouseId = gridData[0].WarehouseId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsClassId4 = gridData[0].PartsClassId4;
                                                //anita tambah
                                                $scope.non_tam_non_sop_bengkel.data[posisi].maxCost = gridData[0].maxCost;
                                                // =================
												// CR5 #10
												console.log('$scope.mData.IsNONPKP at non_tam_non_sop_bengkel ($scope.crudState === 114) || ($scope.crudState === 124) || ($scope.crudState === 134) stv: ', $scope.mData.IsNONPKP);
												if($scope.mData.IsNONPKP == false){
													//PPn
													$scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
													$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice));

													// $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount)) * 0.1);
													$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount)) * (PPNPerc/100));

												}else if($scope.mData.IsNONPKP == true){
													//Non PPn
													$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = Math.round(0);
													$scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
													$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice));
												}
												// CR5 #10
												
												//$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice));
												//$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount)) * 0.1);
												//Total  = (Qty * HargaDasar) - Diskon + VAT;
                                                if($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO){
                                                    $scope.non_tam_non_sop_bengkel.data[posisi].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount;
                                                }
                                                // =============
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartsCode = gridData[0].PartsCode;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartsName = gridData[0].PartsName;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].UomId = gridData[0].UomId;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].Name = gridData[0].Satuan;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].ETD = ETDFix;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].ETA = ETAFix;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].UnitPrice = gridData[0].StandardCost;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartId = gridData[0].PartId;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].OutletId = gridData[0].OutletId;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].WarehouseId = gridData[0].WarehouseId;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartsClassId4 = gridData[0].PartsClassId4;
                                                // //anita tambah
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].maxCost = gridData[0].maxCost;
                                                console.log('cek',$scope.crudState);

                                                PurchaseOrder.GridData = $scope.non_tam_non_sop_bengkel.data;
                                            } else if ($scope.crudState === '1441') {
                                                //SINI PAK ARI
                                                var cekInput = '';
                                                var cekRes = angular.copy(gridData[0].PartsCode)
                                                var posisi = 0;
                                                for (var i=0; i<$scope.non_tam_non_sop_bengkel.data.length; i++){
                                                    cekInput = angular.copy($scope.non_tam_non_sop_bengkel.data[i].PartsCode);
                                                    if (cekInput != undefined && cekInput != null){
                                                        if (cekInput.toUpperCase() == cekRes.toUpperCase()){
                                                            posisi = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                                    
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsCode = gridData[0].PartsCode;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsName = gridData[0].PartsName;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UomId = gridData[0].UomId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UomPOId = gridData[0].UomId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].Name = gridData[0].Satuan;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].ETD = '';
                                                $scope.non_tam_non_sop_bengkel.data[posisi].ETA = '';
                                                // $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = gridData[0].StandardCost;
                                                // commented 07/10/2020
                                                // $scope.non_tam_non_sop_bengkel.data[posisi].xUnitPrice = gridData[0].RetailPrice; //default price with ppn
                                                // $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = Math.round(gridData[0].RetailPrice / (1.1));

                                                // $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1.1));
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1+(PPNPerc/100)));

                                                $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost = gridData[0].StandardCost;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartId = gridData[0].PartId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].OutletId = gridData[0].OutletId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].WarehouseId = gridData[0].WarehouseId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].Satuan = gridData[0].Satuan;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsClassId4 = gridData[0].PartsClassId4;

                                                 // =================
												// $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount =Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice));
												// $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount)) * 0.1);
												// //Total  = (Qty * HargaDasar) - Diskon + VAT;
												// $scope.non_tam_non_sop_bengkel.data[posisi].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount;
												//var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1.1);
												//var DiscountSatuan = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * HargaSatuan);
												//$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan;
												// CR5 #10
												console.log('$scope.mData.IsNONPKP at non_tam_non_sop_bengkel ($scope.crudState === 1441) stv: ', $scope.mData.IsNONPKP);
												if($scope.mData.IsNONPKP == false){
													//PKP

													// var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1.1);
													var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1+(PPNPerc/100));

													var DiscountSatuan = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * HargaSatuan);
													$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan;

													// $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * 0.1);
													$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * (PPNPerc/100));

													// $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / 1.1;
													$scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1+(PPNPerc/100));

												}else if($scope.mData.IsNONPKP == true){
													//Non PKP
													var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
													var DiscountSatuan = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * HargaSatuan);
													$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan;
													$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = 0;
													$scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
												}
												// CR5 #10
												//$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * 0.1);
                                                if($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO){
                                                    $scope.non_tam_non_sop_bengkel.data[posisi].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount;
                                                }
                                                // =============
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartsCode = gridData[0].PartsCode;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartsName = gridData[0].PartsName;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].UomId = gridData[0].UomId;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].UomPOId = gridData[0].UomId;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].Name = gridData[0].Satuan;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].ETD = '';
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].ETA = '';
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].UnitPrice = gridData[0].StandardCost;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartId = gridData[0].PartId;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].OutletId = gridData[0].OutletId;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].WarehouseId = gridData[0].WarehouseId;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].Satuan = gridData[0].Satuan;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartsClassId4 = gridData[0].PartsClassId4;
                                                console.log("HEY YOU THER2E", $scope.non_tam_non_sop_bengkel.data);

                                                $scope.non_tam_sop_so.data = $scope.non_tam_non_sop_bengkel.data;
                                                PurchaseOrder.GridData = $scope.non_tam_non_sop_bengkel.data;
                                            }else if ($scope.crudState === '1442') {
                                                console.log("$scope.non_tam_non_sop.data",$scope.non_tam_non_sop.data);
                                                var cekInput = '';
                                                var cekRes = angular.copy(gridData[0].PartsCode)
                                                var posisi = 0;
                                                for (var i=0; i<$scope.non_tam_non_sop.data.length; i++){
                                                    cekInput = angular.copy($scope.non_tam_non_sop.data[i].PartsCode);
                                                    if (cekInput != undefined && cekInput != null){
                                                        if (cekInput.trim().toUpperCase() == cekRes.trim().toUpperCase()){
                                                            posisi = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                                // var idx = _.findIndex($scope.non_tam_sop_so.data, {PartsCode: dataFilter.PartsCode})
                                                // if(idx != -1){
                                                //     posisi = idx;
                                                // }

                                                console.log("posiiiiiisi", posisi, $scope.non_tam_non_sop.data.length);
                                                $scope.non_tam_non_sop.data[posisi].PartsCode = gridData[0].PartsCode;
                                                $scope.non_tam_non_sop.data[posisi].PartsName = gridData[0].PartsName;
                                                $scope.non_tam_non_sop.data[posisi].UomId = gridData[0].UomId;
                                                $scope.non_tam_non_sop.data[posisi].UomPOId = gridData[0].UomId;
                                                $scope.non_tam_non_sop.data[posisi].Name = gridData[0].Satuan;
                                                $scope.non_tam_non_sop.data[posisi].ETD = '-';
                                                $scope.non_tam_non_sop.data[posisi].ETA = '-';
                                                // $scope.non_tam_sop_so.data[posisi].UnitPrice = gridData[0].StandardCost;
                                                // == commented 07/10/2020
                                                // $scope.non_tam_non_sop.data[posisi].xUnitPrice = gridData[0].StandardCost; //default price with ppn
                                                // $scope.non_tam_non_sop.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1.1));

                                                // var UnitPrice = gridData[0].StandardCost / (1.1);
                                                var UnitPrice = gridData[0].StandardCost / (1+(PPNPerc/100));

                                                $scope.non_tam_non_sop.data[posisi].UnitPrice = parseFloat(UnitPrice.toFixed($scope.roundTo)); //2 digit dibelakang koma
                                                // $scope.non_tam_non_sop.data[posisi].UnitPrice = Math.round(UnitPrice);
                                                // $scope.non_tam_non_sop.data[posisi].StandardCost = Math.round(UnitPrice);
                                                $scope.non_tam_non_sop.data[posisi].StandardCost = gridData[0].StandardCost;
                                                $scope.non_tam_non_sop.data[posisi].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                $scope.non_tam_non_sop.data[posisi].PartId = gridData[0].PartId;
                                                $scope.non_tam_non_sop.data[posisi].OutletId = gridData[0].OutletId;
                                                $scope.non_tam_non_sop.data[posisi].WarehouseId = gridData[0].WarehouseId;
                                                $scope.non_tam_non_sop.data[posisi].Satuan = gridData[0].Satuan;
                                                $scope.non_tam_non_sop.data[posisi].PartsClassId4 = gridData[0].PartsClassId4;
 
                                                 // ============ Perubahan Galang =====
												// $scope.non_tam_sop_so.data[posisi].DiscountAmount =Math.round((($scope.non_tam_sop_so.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice));
												// $scope.non_tam_sop_so.data[posisi].VATAmount = Math.round((($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice) - ($scope.non_tam_sop_so.data[posisi].DiscountAmount)) * 0.1);
												// //Total  = (Qty * HargaDasar) - Diskon + VAT;
												// $scope.non_tam_sop_so.data[posisi].TotalAmount = ($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice) - $scope.non_tam_sop_so.data[posisi].DiscountAmount + $scope.non_tam_sop_so.data[posisi].VATAmount;
												// var HargaSatuan = $scope.non_tam_sop_so.data[posisi].UnitPrice;
												//var HargaSatuan = $scope.non_tam_sop_so.data[posisi].StandardCost / (1.1);
												//var DiscountSatuan = ($scope.non_tam_sop_so.data[posisi].DiscountPercent / 100) * HargaSatuan;
												//$scope.non_tam_sop_so.data[posisi].DiscountAmount = $scope.non_tam_sop_so.data[posisi].QtyPO * DiscountSatuan.toFixed($scope.roundTo);
												// $scope.non_tam_sop_so.data[posisi].DiscountAmount = $scope.non_tam_sop_so.data[posisi].QtyPO * DiscountSatuan;
												//var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
												// $scope.non_tam_sop_so.data[posisi].VATAmount = Math.floor($scope.non_tam_sop_so.data[posisi].QtyPO * VATSatuan);
												// CR5 #10
												console.log('$scope.mData.IsNONPKP at non_tam_sop_so ($scope.crudState === 1442) stv: ', $scope.mData.IsNONPKP);
												if($scope.mData.IsNONPKP == false){
													//PKP

													// var HargaSatuan = $scope.non_tam_non_sop.data[posisi].StandardCost / (1.1);
													var HargaSatuan = $scope.non_tam_non_sop.data[posisi].StandardCost / (1+(PPNPerc/100));

													
                                                    console.log("HargaSatuan=>",HargaSatuan);
                                                    
													var DiscountSatuan = ($scope.non_tam_non_sop.data[posisi].DiscountPercent / 100) * HargaSatuan;
                                                    console.log("DiscountSatuan=>",DiscountSatuan);
													$scope.non_tam_non_sop.data[posisi].DiscountAmount = $scope.non_tam_non_sop.data[posisi].QtyPO * DiscountSatuan.toFixed($scope.roundTo);
													// $scope.non_tam_non_sop.data[posisi].DiscountAmount = $scope.non_tam_non_sop.data[posisi].QtyPO * DiscountSatuan;

												
													// var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
													var VATSatuan = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);
                                                    
                                                    console.log("VATSatuan=>",VATSatuan);
													$scope.non_tam_non_sop.data[posisi].VATAmount = $scope.non_tam_non_sop.data[posisi].QtyPO * VATSatuan.toFixed($scope.roundTo);
													// $scope.non_tam_non_sop.data[posisi].UnitPrice = Math.round($scope.non_tam_non_sop.data[posisi].StandardCost / 1.1);
                                                    $scope.non_tam_non_sop.data[posisi].UnitPrice = parseFloat(HargaSatuan.toFixed($scope.roundTo)); //2 digit dibelakang koma
												}else if($scope.mData.IsNONPKP == true){
													//Non PKP
													var HargaSatuan = $scope.non_tam_non_sop.data[posisi].StandardCost;
													var DiscountSatuan = ($scope.non_tam_non_sop.data[posisi].DiscountPercent / 100) * HargaSatuan;
													$scope.non_tam_non_sop.data[posisi].DiscountAmount = $scope.non_tam_non_sop.data[posisi].QtyPO * DiscountSatuan.toFixed($scope.roundTo);
													// $scope.non_tam_non_sop.data[posisi].DiscountAmount = $scope.non_tam_non_sop.data[posisi].QtyPO * DiscountSatuan;

													// var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
													var VATSatuan = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);

													$scope.non_tam_non_sop.data[posisi].VATAmount = 0;
													$scope.non_tam_non_sop.data[posisi].UnitPrice = $scope.non_tam_non_sop.data[posisi].StandardCost;
												}
												// CR5 #10
												//$scope.non_tam_non_sop.data[posisi].VATAmount = $scope.non_tam_non_sop.data[posisi].QtyPO * VATSatuan.toFixed($scope.roundTo);
                                                if($scope.non_tam_non_sop.data[posisi].QtyPO){
                                                    $scope.non_tam_non_sop.data[posisi].TotalAmount = Math.round(($scope.non_tam_non_sop.data[posisi].QtyPO * $scope.non_tam_non_sop.data[posisi].UnitPrice) - $scope.non_tam_non_sop.data[posisi].DiscountAmount + $scope.non_tam_non_sop.data[posisi].VATAmount);
                                                }
                                                // =============
                                                // $scope.non_tam_non_sop.data[$scope.non_tam_non_sop.data.length - 1].PartsCode = gridData[0].PartsCode;
                                                // $scope.non_tam_non_sop.data[$scope.non_tam_non_sop.data.length - 1].PartsName = gridData[0].PartsName;
                                                // $scope.non_tam_non_sop.data[$scope.non_tam_non_sop.data.length - 1].UomId = gridData[0].UomId;
                                                // $scope.non_tam_non_sop.data[$scope.non_tam_non_sop.data.length - 1].UomPOId = gridData[0].UomId;
                                                // $scope.non_tam_non_sop.data[$scope.non_tam_non_sop.data.length - 1].Name = gridData[0].Satuan;
                                                // $scope.non_tam_non_sop.data[$scope.non_tam_non_sop.data.length - 1].ETD = '';
                                                // $scope.non_tam_non_sop.data[$scope.non_tam_non_sop.data.length - 1].ETA = '';
                                                // $scope.non_tam_non_sop.data[$scope.non_tam_non_sop.data.length - 1].UnitPrice = gridData[0].StandardCost;
                                                // $scope.non_tam_non_sop.data[$scope.non_tam_non_sop.data.length - 1].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                // $scope.non_tam_non_sop.data[$scope.non_tam_non_sop.data.length - 1].PartId = gridData[0].PartId;
                                                // $scope.non_tam_non_sop.data[$scope.non_tam_non_sop.data.length - 1].OutletId = gridData[0].OutletId;
                                                // $scope.non_tam_non_sop.data[$scope.non_tam_non_sop.data.length - 1].WarehouseId = gridData[0].WarehouseId;
                                                // $scope.non_tam_non_sop.data[$scope.non_tam_non_sop.data.length - 1].Satuan = gridData[0].Satuan;
                                                // $scope.non_tam_non_sop.data[$scope.non_tam_non_sop.data.length - 1].PartsClassId4 = gridData[0].PartsClassId4;

                                                $scope.non_tam_sop_so.data = $scope.non_tam_non_sop.data;
                                                PurchaseOrder.GridData = $scope.non_tam_non_sop.data;
                                            } else if ($scope.crudState === '3441') {
                                                var cekInput = '';
                                                var cekRes = angular.copy(gridData[0].PartsCode)
                                                var posisi = 0;
                                                for (var i=0; i<$scope.non_tam_non_sop_bengkel.data.length; i++){
                                                    cekInput = angular.copy($scope.non_tam_non_sop_bengkel.data[i].PartsCode);
                                                    if (cekInput != undefined && cekInput != null){
                                                        if (cekInput.toUpperCase() == cekRes.toUpperCase()){
                                                            posisi = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsCode = gridData[0].PartsCode;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsName = gridData[0].PartsName;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UomId = gridData[0].UomId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UomPOId = gridData[0].UomId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].Name = gridData[0].Satuan;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].Satuan = gridData[0].Satuan;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].ETD = ETDFix;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].ETA = ETAFix;
                                                // $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = gridData[0].StandardCost;
                                                // ---- commented 07/10/2020
                                                // $scope.non_tam_non_sop_bengkel.data[posisi].xUnitPrice = gridData[0].StandardCost; //default price with ppn

                                                // $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1.1));
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1+(PPNPerc/100)));

                                                $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost = gridData[0].StandardCost;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartId = gridData[0].PartId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].OutletId = gridData[0].OutletId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].WarehouseId = gridData[0].WarehouseId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsClassId4 = gridData[0].PartsClassId4;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].maxCost = gridData[0].maxCost;
                                                
                                                // =================
												// $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount =Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice));
												// $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount)) * 0.1);
												// //Total  = (Qty * HargaDasar) - Diskon + VAT;
												// $scope.non_tam_non_sop_bengkel.data[posisi].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice) - $scope.non_tam_sop_so.data[posisi].DiscountAmount + $scope.non_tam_sop_so.data[posisi].VATAmount;
												//var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1.1);
												//var DiscountSatuan = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * HargaSatuan);
												//$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan;

												// CR5 #10
												console.log('$scope.mData.IsNONPKP at non_tam_non_sop_bengkel ($scope.crudState === 3441) stv: ', $scope.mData.IsNONPKP);
												if($scope.mData.IsNONPKP == false){
													//PPn
													// var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1.1);
													var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1+(PPNPerc/100));

													var DiscountSatuan = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * HargaSatuan);
													$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan;

													// $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * 0.1);
													$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * (PPNPerc/100));

													// $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / 1.1;
													$scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1+(PPNPerc/100));

												}else if($scope.mData.IsNONPKP == true){
													//Non PPn
													var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
													var DiscountSatuan = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * HargaSatuan);
													$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan;
													$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = 0;
													$scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
												}
												// CR5 #10

												//$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * 0.1);
                                                if($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO){
                                                    $scope.non_tam_non_sop_bengkel.data[posisi].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount;
                                                }
                                                // =============

                                                PurchaseOrder.GridData = $scope.non_tam_non_sop_bengkel.data;
                                                //$scope.mData.GridDetail = $scope.non_tam_non_sop_bengkel.data;
                                            } else if ($scope.crudState === '3442') {
                                                var cekInput = '';
                                                var cekRes = angular.copy(gridData[0].PartsCode)
                                                var posisi = 0;
                                                for (var i=0; i<$scope.non_tam_non_sop.data.length; i++){
                                                    cekInput = angular.copy($scope.non_tam_non_sop.data[i].PartsCode);
                                                    if (cekInput != undefined && cekInput != null){
                                                        if (cekInput.trim().toUpperCase() == cekRes.trim().toUpperCase()){
                                                            posisi = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                                $scope.non_tam_non_sop.data[posisi].PartsCode = gridData[0].PartsCode;
                                                $scope.non_tam_non_sop.data[posisi].PartsName = gridData[0].PartsName;
                                                $scope.non_tam_non_sop.data[posisi].UomId = gridData[0].UomId;
                                                $scope.non_tam_non_sop.data[posisi].UomPOId = gridData[0].UomId;
                                                $scope.non_tam_non_sop.data[posisi].Name = gridData[0].Satuan;
                                                $scope.non_tam_non_sop.data[posisi].Satuan = gridData[0].Satuan;
                                                $scope.non_tam_non_sop.data[posisi].ETD = ETDFix;
                                                $scope.non_tam_non_sop.data[posisi].ETA = ETAFix;
                                                // $scope.non_tam_non_sop.data[posisi].UnitPrice = gridData[0].StandardCost;
                                                // ---- commented 07/10/2020
                                                // $scope.non_tam_non_sop.data[posisi].xUnitPrice = gridData[0].StandardCost; //default price with ppn

                                                // $scope.non_tam_non_sop.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1.1));
                                                $scope.non_tam_non_sop.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1+(PPNPerc/100)));

                                                $scope.non_tam_non_sop.data[posisi].StandardCost = gridData[0].StandardCost;
                                                $scope.non_tam_non_sop.data[posisi].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                $scope.non_tam_non_sop.data[posisi].PartId = gridData[0].PartId;
                                                $scope.non_tam_non_sop.data[posisi].OutletId = gridData[0].OutletId;
                                                $scope.non_tam_non_sop.data[posisi].WarehouseId = gridData[0].WarehouseId;
                                                $scope.non_tam_non_sop.data[posisi].PartsClassId4 = gridData[0].PartsClassId4;
                                                $scope.non_tam_non_sop.data[posisi].maxCost = gridData[0].maxCost;
                                                
                                                // =================
                                                if($scope.non_tam_non_sop.data[posisi].QtyPO){
                                                    // $scope.non_tam_non_sop.data[posisi].DiscountAmount =Math.round((($scope.non_tam_non_sop.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_non_sop.data[posisi].QtyPO * $scope.non_tam_non_sop.data[posisi].UnitPrice));
                                                    // $scope.non_tam_non_sop.data[posisi].VATAmount = Math.round((($scope.non_tam_non_sop.data[posisi].QtyPO * $scope.non_tam_non_sop.data[posisi].UnitPrice) - ($scope.non_tam_non_sop.data[posisi].DiscountAmount)) * 0.1);
                                                    // //Total  = (Qty * HargaDasar) - Diskon + VAT;
                                                    // $scope.non_tam_non_sop.data[posisi].TotalAmount = ($scope.non_tam_non_sop.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice) - $scope.non_tam_sop_so.data[posisi].DiscountAmount + $scope.non_tam_sop_so.data[posisi].VATAmount;

                                                    // var HargaSatuan = $scope.non_tam_non_sop.data[posisi].StandardCost / (1.1);
                                                    var HargaSatuan = $scope.non_tam_non_sop.data[posisi].StandardCost / (1+(PPNPerc/100));

                                                    var DiscountSatuan = Math.round((($scope.non_tam_non_sop.data[posisi].DiscountPercent / 100)) * HargaSatuan);
                                                    $scope.non_tam_non_sop.data[posisi].DiscountAmount = $scope.non_tam_non_sop.data[posisi].QtyPO * DiscountSatuan;

                                                    // $scope.non_tam_non_sop.data[posisi].VATAmount = $scope.non_tam_non_sop.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * 0.1);
                                                    $scope.non_tam_non_sop.data[posisi].VATAmount = $scope.non_tam_non_sop.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * (PPNPerc/100));

                                                    $scope.non_tam_non_sop.data[posisi].TotalAmount = ($scope.non_tam_non_sop.data[posisi].QtyPO * $scope.non_tam_non_sop.data[posisi].UnitPrice) - $scope.non_tam_non_sop.data[posisi].DiscountAmount + $scope.non_tam_non_sop.data[posisi].VATAmount;
                                                }
                                                // =============

                                                console.log("$scope.non_tam_non_sop.data==>",$scope.non_tam_non_sop.data);
                                                PurchaseOrder.GridData = $scope.non_tam_non_sop.data;
                                                //$scope.mData.GridDetail = $scope.non_tam_non_sop.data;
                                            } 
                                            else if ($scope.crudState === '314') {
                                                var cekInput = '';
                                                var cekRes = angular.copy(gridData[0].PartsCode)
                                                var posisi = 0;
                                                for (var i=0; i<$scope.tam_sop_appointment.data.length; i++){
                                                    cekInput = angular.copy($scope.tam_sop_appointment.data[i].PartsCode);
                                                    if (cekInput != undefined && cekInput != null){
                                                        if (cekInput.toUpperCase() == cekRes.toUpperCase()){
                                                            posisi = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                                $scope.tam_sop_appointment.data[posisi].PartsCode = gridData[0].PartsCode;
                                                $scope.tam_sop_appointment.data[posisi].PartsName = gridData[0].PartsName;
                                                $scope.tam_sop_appointment.data[posisi].UomId = gridData[0].UomId;
                                                $scope.tam_sop_appointment.data[posisi].UomPOId = gridData[0].UomId;
                                                $scope.tam_sop_appointment.data[posisi].Name = gridData[0].Satuan;
                                                $scope.tam_sop_appointment.data[posisi].ETD = ETDFix;
                                                $scope.tam_sop_appointment.data[posisi].ETA = ETAFix;
                                                // $scope.tam_sop_appointment.data[posisi].UnitPrice = gridData[0].StandardCost;
                                                // $scope.tam_sop_appointment.data[posisi].xUnitPrice = gridData[0].StandardCost; //default price with ppn

                                                // $scope.tam_sop_appointment.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1.1));
                                                $scope.tam_sop_appointment.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1+(PPNPerc/100)));

                                                $scope.tam_sop_appointment.data[posisi].StandardCost = gridData[0].StandardCost;
                                                $scope.tam_sop_appointment.data[posisi].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                $scope.tam_sop_appointment.data[posisi].PartId = gridData[0].PartId;
                                                $scope.tam_sop_appointment.data[posisi].OutletId = gridData[0].OutletId;
                                                $scope.tam_sop_appointment.data[posisi].WarehouseId = gridData[0].WarehouseId;
                                                $scope.tam_sop_appointment.data[posisi].PartsClassId4 = gridData[0].PartsClassId4;
                                                $scope.tam_sop_appointment.data[posisi].maxCost = gridData[0].maxCost;
                                               
                                                if($scope.tam_sop_appointment.data[posisi].QtyPO){
                                                    // $scope.tam_sop_appointment.data[posisi].VATAmount = Math.round((($scope.tam_sop_appointment.data[posisi].QtyPO * $scope.tam_sop_appointment.data[posisi].UnitPrice)) * 0.1);
                                                    $scope.tam_sop_appointment.data[posisi].VATAmount = Math.round((($scope.tam_sop_appointment.data[posisi].QtyPO * $scope.tam_sop_appointment.data[posisi].UnitPrice)) * (PPNPerc/100));

                                                    //Total  = (Qty * HargaDasar) - Diskon + VAT;
                                                    $scope.tam_sop_appointment.data[posisi].TotalAmount = ($scope.tam_sop_appointment.data[posisi].QtyPO * $scope.tam_sop_appointment.data[posisi].UnitPrice) + $scope.tam_sop_appointment.data[posisi].VATAmount;
                                                }
                                                // if($scope.tam_sop_appointment.data[posisi].QtyPO){
                                                    // $scope.tam_sop_appointment.data[posisi].TotalAmount = ($scope.tam_sop_appointment.data[posisi].QtyPO * gridData[0].StandardCost);
                                                // }

                                                PurchaseOrder.GridData = $scope.tam_sop_appointment.data;
                                                //$scope.mData.GridDetail = $scope.tam_sop_appointment.data;
                                            }    
                                            
                                        });
                                        
                                } 

                            },
                            function(err) {
                                console.log("err=>", err);
                                confirm("Nomor Material Tidak Ditemukan.");
                            }
                        );
                      }

                      console.log('HOOOOREEE',$scope.mData.VendorId);
                    } else {
                        console.log('SearchMaterial | masuk else ========>',$scope.mData.POTypeId, $scope.mData.RefPOTypeId);
                        console.log("res: " , NoMaterial);
                          PurchaseOrder.getDetailByMaterialNo(NoMaterial,$scope.mData.WarehouseId,$scope.mData.POTypeId).then(function(res) {
                            console.log("getDetailByMaterialNo res: " , res.data.Result[0]);
                                  var gridData = res.data.Result;

                                if($scope.mData.POTypeId == '7'){ //campaign
                                    if (typeof gridData[0] === undefined || gridData[0] == null) {
                                        bsNotify.show({
                                            title: "Purchase Order",
                                            content: "Campaign tidak ditemukan",
                                            type: 'danger'
                                        });
                                        if($scope.isBundle){
                                            $scope.nextCampaign = false;
                                            $scope.actionclearModalTambahListCampaign();
                                        }
                                        return false;
                                    }
                                    var tempArr = [];
                                    if($scope.non_tam_non_sop_bengkel.data.length > 0){
                                        for(var x in $scope.non_tam_non_sop_bengkel.data){
                                            if($scope.non_tam_non_sop_bengkel.data[x].PartsCode == res.data.Result[0].PartsCode){
                                                tempArr.push(true);
                                            }else{
                                                tempArr.push(false);
                                            }
                                        }
                        
                                        if(tempArr.includes(true)){
                                            return 0;
                                        }else{
                                            $scope.non_tam_non_sop_bengkel.data.push(res.data.Result[0]);
                                        }
                                    }else{
                                        $scope.non_tam_non_sop_bengkel.data.push(res.data.Result[0]);
                                    }
                                  }
                                  console.log("gridData: " , gridData[0]);
                                  console.log("res data " , res.data);
                                  console.log("res: " , res);

                                  if ((typeof gridData[0] === undefined || gridData[0] == null) && $scope.mData.POTypeId != '7') {
                                      bsNotify.show({
                                          title: "Purchase Order",
                                          content: "Nomor Material tidak ditemukan",
                                          type: 'danger'
                                      });
                                    }else if((gridData[0].WarehouseId == null || gridData[0].WarehouseId == 0) || (gridData[0].ShelfId == null || gridData[0].ShelfId == 0) || (gridData[0].OutletId == null || gridData[0].OutletId == 0 )){
                                    bsNotify.show({
                                        title: "Purchase Order",
                                        content: "Lokasi Material ini belum terdaftar, Daftarkan terlebih dahulu",
                                        type: 'danger'
                                    });
                                  } else {
                                    if(gridData[0].StandardCost === 0){
                                        bsAlert.warning("Standard cost belum ada.","Harap update terlebih dahulu");
                                        return false;
                                    }
                                    $scope.checkRefGINo = false;
                                      var getDataParameter = [];
                                      var tempParameter = [5];
                                      var todayDate = new Date();
                                      var checkHours = todayDate.getHours(),
                                          hour;
                                      //condition to set currentTime
                                      if (checkHours < 10) {
                                          hour = '0' + todayDate.getHours();
                                      } else {
                                          hour = todayDate.getHours();
                                      }
                                      var currentTime = hour + ":" + todayDate.getMinutes() + ":" + todayDate.getSeconds();
                                      var ValidThru = '';
                                      var EffectiveDate = '';
                                      var OrderTypeId = 0;
                                      var CutOffTime = '';
                                      var ETDDayPlus = '';
                                      var ETDHour = '';
                                      var LeadTimeArrival = '';
                                      var ETDTemp = '',
                                          ETD = '',
                                          ETDFix = '';
                                      var ETATemp = '',
                                          ETATempHour = '',
                                          ETAFix = '',
                                          ETAETO = '';

                                      //parameter order: ValidThru, OrderTypeId, CutOffTime, ETDDayPlus, ETDHour, LeadTimeArrival
                                      OrderLeadTime.getData().then(
                                          function(res) {
                                              getDataParameter = res.data.Result;

                                              if($scope.mData.POTypeId == '7' && !$scope.nextCampaign && $scope.isBundle){ //validation campaign
                                                    $scope.actionclearModalTambahListCampaign();
                                                    return false;
                                                }

                                              var indexOfValidThru = 0;
                                              ValidThru = new Date(getDataParameter[0].ValidThru);
                                              EffectiveDate = new Date(getDataParameter[0].EffectiveDate);
                                              console.log("getDataParameter: ", getDataParameter);
                                              console.log("currentTime: ", currentTime);
                                              console.log("todayDate: ", todayDate);
                                              console.log('initype', $scope.mData.POTypeId);
                                              if ($scope.mData.POTypeId == 1) {
                                                  if (ValidThru >= todayDate && EffectiveDate <= todayDate) {
                                                      var i = 0;
                                                      while (getDataParameter.length > 0 && i < getDataParameter.length) {
                                                          console.log(i, " lll ", getDataParameter[i].OrderTypeId);
                                                          if (getDataParameter[i].OrderTypeId == 1 && $scope.mData.POTypeId == 1) {
                                                              // if (getDataParameter[i].OrderTypeId==1||getDataParameter[i].OrderTypeId==2||getDataParameter[i].OrderTypeId==3){

                                                              //not today ETD
                                                              if (currentTime >= getDataParameter[i].CutOffTime) {
                                                                  ETDTemp = new Date(todayDate);
                                                                //   ETDTemp.setDate(todayDate.getDate() + 1 + getDataParameter[i].ETDDayPlus);
                                                                  ETDTemp.setDate(todayDate.getDate()  + getDataParameter[i].ETDDayPlus);
                                                                  console.log("b",ETDTemp);
                                                              } else {
                                                                break;
                                                                ETDTemp = new Date(todayDate);
                                                                ETDTemp.setDate(todayDate.getDate() + getDataParameter[i].ETDDayPlus);
                                                                  console.log("test hari", ETDTemp);
                                                              }

                                                              ETD = $scope.changeFormatDate(ETDTemp);
                                                              ETDFix = ETD + ' ' + getDataParameter[i].ETDHour;
                                                              var ETAtanggal = new Date(ETDFix);
                                                              ETATempHour = ETAtanggal.getHours();
                                                              ETATemp = ETATempHour + getDataParameter[i].LeadTimeArrival;
                                                              if (ETATemp>24){
                                                        
                                                                var ETAETO = ETATemp - 24 ;
                                                                if(ETAETO<10){
                                                                  ETAETO='0'+ETAETO;
                                                                }
                                                                else{
                                                                  ETAETO=ETAETO;
                                                                }
                                                                ETATemp = ETAETO
                                                                ETAtanggal = new Date(ETAtanggal.setDate(new Date(ETD).getDate()+1));
                                                                ETD = $scope.changeFormatDate(ETAtanggal);
                                                                console.log('ETA tanggal', ETAtanggal);
                                                              }
                                                              var ETAminit = ETAtanggal.getMinutes();
                                                              if (ETAminit < 10) {
                                                                  ETAminit = '0' + ETAminit;
                                                              } else {
                                                                  ETAminit = ETAminit;
                                                              }
                                                              var ETAdetik = ETAtanggal.getSeconds();
                                                              if (ETAdetik < 10) {
                                                                  ETAdetik = '0' + ETAdetik;
                                                              } else {
                                                                  ETAdetik = ETAdetik;
                                                              }
                                                              ETAFix = ETD + ' ' + ETATemp + ':' + ETAminit + ':' + ETAdetik;

                                                              console.log(i, "ETAFixp", ETAFix);
                                                              console.log(i, "CutOffTime", getDataParameter[i].CutOffTime);
                                                              console.log(i, "ETDTemp", ETDTemp);
                                                              console.log(i, "ETD", ETD + getDataParameter[i].ETDHour);
                                                              console.log(i, "ETDDayPlus", getDataParameter[i].ETDDayPlus);
                                                            //   break;
                                                          } else {
                                                              console.log("data percobaan 1");
                                                          }
                                                          i++;
                                                      }
                                                  } else {
                                                      console.log("not valid");
                                                  }
                                              } else if ($scope.mData.POTypeId == 2) {
                                                  //   console.log('ini yang 2');
                                                  //
                                                  if (ValidThru >= todayDate && EffectiveDate <= todayDate) {
                                                      var i = 0;
                                                      while (getDataParameter.length > 0 && i < getDataParameter.length) {
                                                          console.log(i, " lll ", getDataParameter[i].OrderTypeId);
                                                          if (getDataParameter[i].OrderTypeId == 2 && $scope.mData.POTypeId == 2) {
                                                              // if (getDataParameter[i].OrderTypeId==1||getDataParameter[i].OrderTypeId==2||getDataParameter[i].OrderTypeId==3){

                                                              //not today ETD
                                                              if (currentTime >= getDataParameter[i].CutOffTime) {
                                                                  ETDTemp = new Date(todayDate);
                                                                  ETDTemp.setDate(todayDate.getDate() + 1 + getDataParameter[i].ETDDayPlus);
                                                                  console.log("b");
                                                              } else {
                                                                break;
                                                                ETDTemp = new Date(todayDate);
                                                                ETDTemp.setDate(todayDate.getDate() + getDataParameter[i].ETDDayPlus);
                                                                  console.log("test hari");
                                                              }
                                                              ETD = $scope.changeFormatDate(ETDTemp);
                                                              ETDFix = ETD + ' ' + getDataParameter[i].ETDHour;
                                                              var ETAtanggal = new Date(ETDFix);
                                                              ETATempHour = ETAtanggal.getHours();
                                                              ETATemp = ETATempHour + getDataParameter[i].LeadTimeArrival;
                                                             ///
                                                              // if (ETATemp>24){
                                                              //   ETAETO = ETATemp-24;
                                                              //   if(ETAETO<10){
                                                              //     ETAETO='0'+ETAETO;
                                                              //   }
                                                              //   else{
                                                              //     ETAETO=ETAETO;
                                                              //   }
                                                              //   ETAtanggal = ETAtanggal.setDate(ETD.getDate()+1);
                                                              //   console.log('ETA tanggal', ETAtanggal);
                                                              // }
                                                              var ETAminit = ETAtanggal.getMinutes();
                                                              if (ETAminit < 10) {
                                                                  ETAminit = '0' + ETAminit;
                                                              } else {
                                                                  ETAminit = ETAminit;
                                                              }
                                                              var ETAdetik = ETAtanggal.getSeconds();
                                                              if (ETAdetik < 10) {
                                                                  ETAdetik = '0' + ETAdetik;
                                                              } else {
                                                                  ETAdetik = ETAdetik;
                                                              }
                                                              ETAFix = ETD + ' ' + ETATemp + ':' + ETAminit + ':' + ETAdetik;

                                                              console.log(i, "ETAFixps", ETAFix);
                                                              console.log(i, "CutOffTime", getDataParameter[i].CutOffTime);
                                                              console.log(i, "ETDTemp", ETDTemp);
                                                              console.log(i, "ETD", ETD + getDataParameter[i].ETDHour);
                                                              console.log(i, "ETDDayPlus", getDataParameter[i].ETDDayPlus);
                                                            //   break;
                                                          } else {
                                                              console.log("data percobaan 2");
                                                          }
                                                          i++;
                                                      }
                                                  } else {
                                                      console.log("not valid");
                                                  }
                                              } else if ($scope.mData.POTypeId == 3 || $scope.mData.POTypeId == 6 || $scope.mData.POTypeId == 7) {
                                                  //   console.log('ini yang 2');
                                                  //
                                                  if (ValidThru >= todayDate && EffectiveDate <= todayDate) {
                                                      var i = 0;
                                                      while (getDataParameter.length > 0 && i < getDataParameter.length) {
                                                          console.log(i, " lll ", getDataParameter[i].OrderTypeId);
                                                          if ((getDataParameter[i].OrderTypeId == 3 && $scope.mData.POTypeId == 3) || (getDataParameter[i].OrderTypeId == 6 && $scope.mData.POTypeId == 6)
                                                          || (getDataParameter[i].OrderTypeId == 7 && $scope.mData.POTypeId == 7)) {
                                                            // if (getDataParameter[i].OrderTypeId==1||getDataParameter[i].OrderTypeId==2||getDataParameter[i].OrderTypeId==3){

                                                              //not today ETD
                                                              if (currentTime >= getDataParameter[i].CutOffTime) {
                                                                  ETDTemp = new Date(todayDate);
                                                                  ETDTemp.setDate(todayDate.getDate() + 1 + getDataParameter[i].ETDDayPlus);
                                                                  console.log("b");
                                                              } else {
                                                                ETDTemp = new Date(todayDate);
                                                                ETDTemp.setDate(todayDate.getDate() + getDataParameter[i].ETDDayPlus);
                                                                  console.log("test hari", ETDTemp);
                                                              }
                                                              ETD = $scope.changeFormatDate(ETDTemp);
                                                              ETDFix = ETD + ' ' + getDataParameter[i].ETDHour;
                                                              var ETAtanggal = new Date(ETDFix);
                                                              ETATempHour = ETAtanggal.getHours();
                                                              ETATemp = ETATempHour + getDataParameter[i].LeadTimeArrival;

                                                              if(ETATemp > 24){
                                                                ETATemp = ETATemp - 24;
                                                                var yyyy = ETAtanggal.getFullYear().toString();
                                                                var mm = (ETAtanggal.getMonth() + 1).toString(); // getMonth() is zero-based
                                                                var dd = ETAtanggal.getDate().toString();
                                                                var finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                                                                console.log("changeFormatDate finalDate", finalDate);
                                                                //sini maa
                                                                ETDTemp = finalDate;
                                                              }
                                                              else {
                                                                ETATemp = ETATemp;
                                                              }

                                                              // if (ETATemp>24){
                                                              //   ETAETO = ETATemp-24;
                                                              //   if(ETAETO<10){
                                                              //     ETAETO='0'+ETAETO;
                                                              //   }
                                                              //   else{
                                                              //     ETAETO=ETAETO;
                                                              //   }
                                                              //   ETAtanggal = ETAtanggal.setDate(ETD.getDate()+1);
                                                              //   console.log('ETA tanggal', ETAtanggal);
                                                              // }
                                                              var ETAminit = ETAtanggal.getMinutes();
                                                              if (ETAminit < 10) {
                                                                  ETAminit = '0' + ETAminit;
                                                              } else {
                                                                  ETAminit = ETAminit;
                                                              }
                                                              var ETAdetik = ETAtanggal.getSeconds();
                                                              if (ETAdetik < 10) {
                                                                  ETAdetik = '0' + ETAdetik;
                                                              } else {
                                                                  ETAdetik = ETAdetik;
                                                              }
                                                              ETAFix = ETD + ' ' + ETATemp + ':' + ETAminit + ':' + ETAdetik;

                                                              console.log(i, "ETAFix", ETAFix);
                                                              console.log(i, "CutOffTime", getDataParameter[i].CutOffTime);
                                                              console.log(i, "ETDTemp", ETDTemp);
                                                              console.log(i, "ETD", ETD + getDataParameter[i].ETDHour);
                                                              console.log(i, "ETDDayPlus", getDataParameter[i].ETDDayPlus);
                                                              break;
                                                          } else {
                                                              console.log("data percobaan 3");
                                                          }
                                                          i++;
                                                      }
                                                  } else {
                                                      console.log("not valid");
                                                  }
                                              }


                                              console.log("crudState:" , $scope.crudState);
                                              console.log("gridData[0]:" , gridData[0]);
                                              PurchaseOrder.crudState = $scope.crudState;

                                              if ($scope.crudState === '142') {
                                                    var cekInput = '';
                                                    var cekRes = angular.copy(gridData[0].PartsName)
                                                    var posisi = 0;
                                                    for (var i=0; i<$scope.non_tam_sop_so.data.length; i++){
                                                        cekInput = angular.copy($scope.non_tam_sop_so.data[i].PartsName);
                                                        if (cekInput != undefined && cekInput != null){
                                                            if (cekInput.toUpperCase() == cekRes.toUpperCase()){
                                                                posisi = i;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                  $scope.non_tam_sop_so.data[posisi].PartsName = gridData[0].PartsName;
                                                  $scope.non_tam_sop_so.data[posisi].UomId = gridData[0].UomId;
                                                  $scope.non_tam_sop_so.data[posisi].UomPOId = gridData[0].UomId;
                                                  $scope.non_tam_sop_so.data[posisi].Name = gridData[0].Name;
                                                  $scope.non_tam_sop_so.data[posisi].PartId = gridData[0].PartsId;
                                                  $scope.non_tam_sop_so.data[posisi].QtyBase = 0;
                                                  $scope.non_tam_sop_so.data[posisi].UomBaseId = gridData[0].UomId;
 
												// $scope.non_tam_sop_so.data[posisi].DiscountAmount =Math.round((($scope.non_tam_sop_so.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice));
												// $scope.non_tam_sop_so.data[posisi].VATAmount = Math.round((($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice) - ($scope.non_tam_sop_so.data[posisi].DiscountAmount)) * 0.1);
												// //Total  = (Qty * HargaDasar) - Diskon + VAT;
												// $scope.non_tam_sop_so.data[posisi].TotalAmount = ($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice) - $scope.non_tam_sop_so.data[posisi].DiscountAmount + $scope.non_tam_sop_so.data[posisi].VATAmount;
												//var HargaSatuan = $scope.non_tam_sop_so.data[posisi].StandardCost / (1.1);
												//var DiscountSatuan = ($scope.non_tam_sop_so.data[posisi].DiscountPercent / 100) * HargaSatuan;
												//$scope.non_tam_sop_so.data[posisi].DiscountAmount = $scope.non_tam_sop_so.data[posisi].QtyPO * DiscountSatuan.toFixed($scope.roundTo);
												// CR5 #10
												console.log('$scope.mData.IsNONPKP at non_tam_sop_so ($scope.crudState === 142) stv: ', $scope.mData.IsNONPKP);
												if($scope.mData.IsNONPKP == false){
													//PKP
													// var HargaSatuan = $scope.non_tam_sop_so.data[posisi].StandardCost / (1.1);
													var HargaSatuan = $scope.non_tam_sop_so.data[posisi].StandardCost / (1+(PPNPerc/100));

													var DiscountSatuan = ($scope.non_tam_sop_so.data[posisi].DiscountPercent / 100) * HargaSatuan;
													$scope.non_tam_sop_so.data[posisi].DiscountAmount = $scope.non_tam_sop_so.data[posisi].QtyPO * DiscountSatuan.toFixed($scope.roundTo);

													// var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
													var VATSatuan = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);

													$scope.non_tam_sop_so.data[posisi].VATAmount = $scope.non_tam_sop_so.data[posisi].QtyPO * VATSatuan.toFixed($scope.roundTo);

													// $scope.non_tam_sop_so.data[posisi].UnitPrice = $scope.non_tam_sop_so.data[posisi].StandardCost / 1.1;
													$scope.non_tam_sop_so.data[posisi].UnitPrice = $scope.non_tam_sop_so.data[posisi].StandardCost / (1+(PPNPerc/100));

												}else if($scope.mData.IsNONPKP == true){
													//Non PKP
													var HargaSatuan = $scope.non_tam_sop_so.data[posisi].StandardCost;
													var DiscountSatuan = ($scope.non_tam_sop_so.data[posisi].DiscountPercent / 100) * HargaSatuan;
													$scope.non_tam_sop_so.data[posisi].DiscountAmount = $scope.non_tam_sop_so.data[posisi].QtyPO * DiscountSatuan.toFixed($scope.roundTo);
													$scope.non_tam_sop_so.data[posisi].VATAmount = 0;
													$scope.non_tam_sop_so.data[posisi].UnitPrice = $scope.non_tam_sop_so.data[posisi].StandardCost;
												}
												// CR5 #10
												//var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
												//$scope.non_tam_sop_so.data[posisi].VATAmount = $scope.non_tam_sop_so.data[posisi].QtyPO * VATSatuan.toFixed($scope.roundTo);
                                                if($scope.non_tam_sop_so.data[posisi].QtyPO){
													$scope.non_tam_sop_so.data[posisi].TotalAmount = Math.round(($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice) - $scope.non_tam_sop_so.data[posisi].DiscountAmount + $scope.non_tam_sop_so.data[posisi].VATAmount);
												}
                                                //   $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].PartsName = gridData[0].PartsName;
                                                //   $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].UomId = gridData[0].UomId;
                                                //   $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].Name = gridData[0].Name;
                                                //   $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].PartId = gridData[0].PartsId;
                                                //   $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].QtyBase = 0;
                                                //   $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].UomBaseId = gridData[0].UomId;

                                                  $scope.mData.GridDetail = $scope.non_tam_sop_so.data;
                                                  console.log("ice cream man");
                                              } else if ($scope.crudState === '152' || $scope.crudState === '151') {
                                                console.log("i can't get enough:" + $scope.crudState);
                                                    var cekInput = '';
                                                    var cekRes = angular.copy(gridData[0].PartsCode)
                                                    var posisi = 0;
                                                    for (var i=0; i<$scope.sublet_sop_wo.data.length; i++){
                                                        cekInput = angular.copy($scope.sublet_sop_wo.data[i].PartsCode);
                                                        if (cekInput != undefined && cekInput != null){
                                                            if (cekInput.trim().toUpperCase() == cekRes.trim().toUpperCase()){
                                                                posisi = i;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                $scope.sublet_sop_wo.data[posisi].PartsCode = gridData[0].PartsCode;
                                                $scope.sublet_sop_wo.data[posisi].PartsName = gridData[0].PartsName;
                                                $scope.sublet_sop_wo.data[posisi].UomId = gridData[0].UomId;
                                                $scope.sublet_sop_wo.data[posisi].UomPOId = gridData[0].UomId;
                                                $scope.sublet_sop_wo.data[posisi].Name = gridData[0].Satuan;
                                                $scope.sublet_sop_wo.data[posisi].ETD = ETDFix;
                                                $scope.sublet_sop_wo.data[posisi].ETA = ETAFix;
                                                // $scope.sublet_sop_wo.data[posisi].UnitPrice = gridData[0].StandardCost;
                                                // == commented 07/10/2020
                                                // $scope.sublet_sop_wo.data[posisi].xUnitPrice = gridData[0].StandardCost; //default price with ppn

                                                // var UnitPrice = gridData[0].StandardCost / (1.1);
                                                var UnitPrice = gridData[0].StandardCost / (1+(PPNPerc/100));

                                                $scope.sublet_sop_wo.data[posisi].UnitPrice = parseFloat(UnitPrice.toFixed($scope.roundTo));
                                                $scope.sublet_sop_wo.data[posisi].StandardCost = gridData[0].StandardCost;
                                                $scope.sublet_sop_wo.data[posisi].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                $scope.sublet_sop_wo.data[posisi].PartId = gridData[0].PartId;
                                                $scope.sublet_sop_wo.data[posisi].OutletId = gridData[0].OutletId;
                                                $scope.sublet_sop_wo.data[posisi].WarehouseId = gridData[0].WarehouseId;
                                                $scope.sublet_sop_wo.data[posisi].PartsClassId4 = gridData[0].PartsClassId4;
                                                $scope.sublet_sop_wo.data[posisi].maxCost = gridData[0].maxCost;
                                                    
                                                // $scope.sublet_sop_wo.data[$scope.sublet_sop_wo.data.length - 1].PartsCode = gridData[0].PartsCode;
                                                // $scope.sublet_sop_wo.data[$scope.sublet_sop_wo.data.length - 1].PartsName = gridData[0].PartsName;
                                                // $scope.sublet_sop_wo.data[$scope.sublet_sop_wo.data.length - 1].UomId = gridData[0].UomId;
                                                // $scope.sublet_sop_wo.data[$scope.sublet_sop_wo.data.length - 1].Name = gridData[0].Satuan;
                                                // $scope.sublet_sop_wo.data[$scope.sublet_sop_wo.data.length - 1].ETD = ETDFix;
                                                // $scope.sublet_sop_wo.data[$scope.sublet_sop_wo.data.length - 1].ETA = ETAFix;
                                                // $scope.sublet_sop_wo.data[$scope.sublet_sop_wo.data.length - 1].UnitPrice = gridData[0].StandardCost;
                                                // $scope.sublet_sop_wo.data[$scope.sublet_sop_wo.data.length - 1].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                // $scope.sublet_sop_wo.data[$scope.sublet_sop_wo.data.length - 1].PartId = gridData[0].PartId;
                                                // $scope.sublet_sop_wo.data[$scope.sublet_sop_wo.data.length - 1].OutletId = gridData[0].OutletId;
                                                // $scope.sublet_sop_wo.data[$scope.sublet_sop_wo.data.length - 1].WarehouseId = gridData[0].WarehouseId;
                                                // $scope.sublet_sop_wo.data[$scope.sublet_sop_wo.data.length - 1].PartsClassId4 = gridData[0].PartsClassId4;
                                                // $scope.sublet_sop_wo.data[$scope.sublet_sop_wo.data.length - 1].maxCost = gridData[0].maxCost;
												
												// $scope.sublet_sop_wo.data[posisi].DiscountAmount =Math.round((($scope.sublet_sop_wo.data[posisi].DiscountPercent / 100)) * ($scope.sublet_sop_wo.data[posisi].QtyPO * $scope.sublet_sop_wo.data[posisi].UnitPrice));
                                                    // $scope.sublet_sop_wo.data[posisi].VATAmount = Math.round((($scope.sublet_sop_wo.data[posisi].QtyPO * $scope.sublet_sop_wo.data[posisi].UnitPrice) - ($scope.sublet_sop_wo.data[posisi].DiscountAmount)) * 0.1);
                                                    // //Total  = (Qty * HargaDasar) - Diskon + VAT;
                                                    // $scope.sublet_sop_wo.data[posisi].TotalAmount = ($scope.sublet_sop_wo.data[posisi].QtyPO * $scope.sublet_sop_wo.data[posisi].UnitPrice) - $scope.sublet_sop_wo.data[posisi].DiscountAmount + $scope.sublet_sop_wo.data[posisi].VATAmount;
                                                    //var HargaSatuan = $scope.sublet_sop_wo.data[posisi].StandardCost / (1.1);
                                                    //var DiscountSatuan = ($scope.sublet_sop_wo.data[posisi].DiscountPercent / 100) * HargaSatuan;
                                                    //$scope.sublet_sop_wo.data[posisi].DiscountAmount = $scope.sublet_sop_wo.data[posisi].QtyPO * DiscountSatuan.toFixed($scope.roundTo);
													// CR5 #10
													console.log('$scope.mData.IsNONPKP at sublet_sop_wo ($scope.crudState === 152 || $scope.crudState === 151) stv: ', $scope.mData.IsNONPKP);
													console.log("masuk kebawah sini loh");
                                                    if($scope.mData.IsNONPKP == false){
														//PKP
														var HargaSatuan = $scope.sublet_sop_wo.data[posisi].StandardCost;
														var DiscountSatuan = ($scope.sublet_sop_wo.data[posisi].DiscountPercent / 100) * HargaSatuan;
														$scope.sublet_sop_wo.data[posisi].DiscountAmount = $scope.sublet_sop_wo.data[posisi].QtyPO * DiscountSatuan.toFixed($scope.roundTo);

														// var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
														var VATSatuan = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);

														$scope.sublet_sop_wo.data[posisi].VATAmount = $scope.sublet_sop_wo.data[posisi].QtyPO * VATSatuan.toFixed($scope.roundTo);
														$scope.sublet_sop_wo.data[posisi].UnitPrice = $scope.sublet_sop_wo.data[posisi].StandardCost;
													}else if($scope.mData.IsNONPKP == true){
														//Non PKP
														var HargaSatuan = $scope.sublet_sop_wo.data[posisi].StandardCost;
														var DiscountSatuan = ($scope.sublet_sop_wo.data[posisi].DiscountPercent / 100) * HargaSatuan;
														$scope.sublet_sop_wo.data[posisi].DiscountAmount = $scope.sublet_sop_wo.data[posisi].QtyPO * DiscountSatuan.toFixed($scope.roundTo);
														$scope.sublet_sop_wo.data[posisi].VATAmount = 0;
														$scope.sublet_sop_wo.data[posisi].UnitPrice = $scope.sublet_sop_wo.data[posisi].StandardCost;
													}
													// CR5 #10
                                                    //var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
													
                                                    //$scope.sublet_sop_wo.data[posisi].VATAmount = $scope.sublet_sop_wo.data[posisi].QtyPO * VATSatuan.toFixed($scope.roundTo);
                                                if($scope.sublet_sop_wo.data[posisi].QtyPO){
                                                    $scope.sublet_sop_wo.data[posisi].TotalAmount = Math.round(($scope.sublet_sop_wo.data[posisi].QtyPO * $scope.sublet_sop_wo.data[posisi].UnitPrice) - $scope.sublet_sop_wo.data[posisi].DiscountAmount + $scope.sublet_sop_wo.data[posisi].VATAmount);
												}

                                                $scope.mData.GridDetail =  $scope.sublet_sop_wo.data;
                                              }

                                              else if ($scope.crudState === '114') {
                                                var cekInput = '';
                                                var cekRes = angular.copy(gridData[0].PartsCode)
                                                var posisi = 0;
                                                for (var i=0; i<$scope.non_tam_non_sop_bengkel.data.length; i++){
                                                    cekInput = angular.copy($scope.non_tam_non_sop_bengkel.data[i].PartsCode);
                                                    if (cekInput != undefined && cekInput != null){
                                                        if (cekInput.toUpperCase() == cekRes.toUpperCase()){
                                                            posisi = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsCode = gridData[0].PartsCode;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsName = gridData[0].PartsName;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UomId = gridData[0].UomId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UomPOId = gridData[0].UomId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].Name = gridData[0].Satuan;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].Satuan = gridData[0].Satuan;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].ETD = ETDFix;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].ETA = ETAFix;
                                                // $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = gridData[0].StandardCost;
                                                // --- commented 07/09/2020
                                                // $scope.non_tam_non_sop_bengkel.data[posisi].xUnitPrice = gridData[0].StandardCost; //default price with ppn
                                                // $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1.1));
                                                // $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost = gridData[0].StandardCost;

                                                

                                                // var UnitPrice = gridData[0].StandardCost / (1.1);
                                                var UnitPrice = gridData[0].StandardCost / (1+(PPNPerc/100));

                                                $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = Math.round(UnitPrice);
                                                $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost = Math.round(UnitPrice);
                                                //campaign
                                                if($scope.mData.POTypeId == '7'){
                                                    $scope.non_tam_non_sop_bengkel.data[posisi].StockingPolicy = undefined;
                                                }

                                                $scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartId = gridData[0].PartId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].OutletId = gridData[0].OutletId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].WarehouseId = gridData[0].WarehouseId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsClassId4 = gridData[0].PartsClassId4;
                                                //anita tambah
                                                $scope.non_tam_non_sop_bengkel.data[posisi].maxCost = gridData[0].maxCost;

												// $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount =Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice));
												// $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount)) * 0.1);
												// //Total  = (Qty * HargaDasar) - Diskon + VAT;
												// $scope.non_tam_non_sop_bengkel.data[posisi].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount;


												// ===================replace galang awalnya yg bawah==================
												//var HargaSatuan    = $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice;
												//var DiscountSatuan = ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100) * HargaSatuan;
												//var VATSatuan      = (HargaSatuan - DiscountSatuan) * 0.1;
												// CR5 #10
                                                console.log("wew");
												console.log('$scope.mData.IsNONPKP at non_tam_non_sop_bengkel.data ($scope.crudState === 114) stv: ', $scope.mData.IsNONPKP);
												if($scope.mData.IsNONPKP == false){
													//PKP
												

													var HargaSatuan    = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
													var DiscountSatuan = ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100) * HargaSatuan;

													// var VATSatuan      = (HargaSatuan - DiscountSatuan) * 0.1;
													var VATSatuan      = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);

													$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = Math.floor($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * VATSatuan);
													$scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
												}else if($scope.mData.IsNONPKP == true){
													//Non PKP
													var HargaSatuan    = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
													var DiscountSatuan = ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100) * HargaSatuan;
													$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = 0;
													$scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
												}
												// CR5 #10

												$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = Math.round($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan);
												//$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = Math.floor($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * VATSatuan);
                                                if($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO){
                                                 $scope.non_tam_non_sop_bengkel.data[posisi].TotalAmount = Math.round(($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount);
                                                 // ===================replace galang awalnya yg bawah==================
                                                    
                                                    // var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1.1);
                                                    // var DiscountSatuan = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * HargaSatuan);
                                                    // $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan;
                                                    // $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * 0.1);
                                                    // $scope.non_tam_non_sop_bengkel.data[posisi].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount;
                                                }
                                                
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartsCode = gridData[0].PartsCode;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartsName = gridData[0].PartsName;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].UomId = gridData[0].UomId;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].Name = gridData[0].Satuan;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].ETD = ETDFix;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].ETA = ETAFix;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].UnitPrice = gridData[0].StandardCost;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartId = gridData[0].PartId;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].OutletId = gridData[0].OutletId;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].WarehouseId = gridData[0].WarehouseId;
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartsClassId4 = gridData[0].PartsClassId4;
                                                // //anita tambah
                                                // $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].maxCost = gridData[0].maxCost;
                                                console.log('UTAMI RIZMA');
                                                PurchaseOrder.GridData = $scope.non_tam_non_sop_bengkel.data;                                            
                                                $scope.mData.GridDetail = $scope.non_tam_non_sop_bengkel.data;
                                               }else if (($scope.crudState === '124') || ($scope.crudState === '134')) {
                                                    var cekInput = '';
                                                    var cekRes = angular.copy(gridData[0].PartsCode)
                                                    var posisi = 0;
                                                    for (var i=0; i<$scope.non_tam_non_sop_bengkel.data.length; i++){
                                                        cekInput = angular.copy($scope.non_tam_non_sop_bengkel.data[i].PartsCode);
                                                        if (cekInput != undefined && cekInput != null){
                                                            if (cekInput.toUpperCase() == cekRes.toUpperCase()){
                                                                posisi = i;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                  $scope.non_tam_non_sop_bengkel.data[posisi].PartsCode = gridData[0].PartsCode;
                                                  $scope.non_tam_non_sop_bengkel.data[posisi].PartsName = gridData[0].PartsName;
                                                  $scope.non_tam_non_sop_bengkel.data[posisi].UomId = gridData[0].UomId;
                                                  $scope.non_tam_non_sop_bengkel.data[posisi].UomPOId = gridData[0].UomId;
                                                  $scope.non_tam_non_sop_bengkel.data[posisi].Name = gridData[0].Satuan;
                                                  $scope.non_tam_non_sop_bengkel.data[posisi].ETD = ETDFix;
                                                  $scope.non_tam_non_sop_bengkel.data[posisi].ETA = ETAFix;
                                                //   $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = gridData[0].StandardCost;
                                                //   commented 07/10/2020
                                                    //$scope.non_tam_non_sop_bengkel.data[posisi].xUnitPrice = gridData[0].StandardCost; //default price with ppn

                                                //   var UnitPrice = gridData[0].StandardCost / (1.1);
                                                  var UnitPrice = gridData[0].StandardCost / (1+(PPNPerc/100));

                                                  $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = parseFloat(UnitPrice.toFixed($scope.roundTo));
                                                  $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost = gridData[0].StandardCost;
                                                  $scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                  $scope.non_tam_non_sop_bengkel.data[posisi].PartId = gridData[0].PartId;
                                                  $scope.non_tam_non_sop_bengkel.data[posisi].OutletId = gridData[0].OutletId;
                                                  $scope.non_tam_non_sop_bengkel.data[posisi].WarehouseId = gridData[0].WarehouseId;
                                                  $scope.non_tam_non_sop_bengkel.data[posisi].PartsClassId4 = gridData[0].PartsClassId4;
                                                  //anita tambah
                                                  $scope.non_tam_non_sop_bengkel.data[posisi].maxCost = gridData[0].maxCost;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartsCode = gridData[0].PartsCode;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartsName = gridData[0].PartsName;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].UomId = gridData[0].UomId;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].Name = gridData[0].Satuan;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].ETD = ETDFix;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].ETA = ETAFix;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].UnitPrice = gridData[0].StandardCost;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartId = gridData[0].PartId;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].OutletId = gridData[0].OutletId;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].WarehouseId = gridData[0].WarehouseId;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartsClassId4 = gridData[0].PartsClassId4;
                                                //   //anita tambah
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].maxCost = gridData[0].maxCost;
                                                // =================
												// $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount =Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice));
												// $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount)) * 0.1);
												// //Total  = (Qty * HargaDasar) - Diskon + VAT;
												// $scope.non_tam_non_sop_bengkel.data[posisi].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount;
												//var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1.1);
												//var DiscountSatuan = ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100) * HargaSatuan;
												//$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan.toFixed($scope.roundTo);
												// CR5 #10
												console.log('$scope.mData.IsNONPKP at non_tam_non_sop_bengkel ($scope.crudState === 124) || ($scope.crudState === 134) stv: ', $scope.mData.IsNONPKP);
												if($scope.mData.IsNONPKP == false){
													//PKP
													// var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1.1);
													var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1+(PPNPerc/100));

													var DiscountSatuan = ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100) * HargaSatuan;
													$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan.toFixed($scope.roundTo);

													// var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
													var VATSatuan = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);

													$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * VATSatuan.toFixed($scope.roundTo);

													// $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / 1.1;
													$scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1+(PPNPerc/100));

												}else if($scope.mData.IsNONPKP == true){
													//Non PKP
													var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
													var DiscountSatuan = ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100) * HargaSatuan;
													$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan.toFixed($scope.roundTo);
													$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = 0;
													$scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
												}
												// CR5 #10
												//var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
												//$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * VATSatuan.toFixed($scope.roundTo);
                                                if($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO){
                                                    $scope.non_tam_non_sop_bengkel.data[posisi].TotalAmount = Math.round(($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount);
                                                }
                                                // =============
                                                  PurchaseOrder.GridData = $scope.non_tam_non_sop_bengkel.data;
                                                  $scope.mData.GridDetail = $scope.non_tam_non_sop_bengkel.data;
                                              } else if ($scope.crudState === '1441') {
                                                var cekInput = '';
                                                var cekRes = angular.copy(gridData[0].PartsCode)
                                                var posisi = 0;
                                                for (var i=0; i<$scope.non_tam_non_sop_bengkel.data.length; i++){
                                                    cekInput = angular.copy($scope.non_tam_non_sop_bengkel.data[i].PartsCode);
                                                    if (cekInput != undefined && cekInput != null){
                                                        if (cekInput.toUpperCase() == cekRes.toUpperCase()){
                                                            posisi = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsCode = gridData[0].PartsCode;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsName = gridData[0].PartsName;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UomId = gridData[0].UomId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UomPOId = gridData[0].UomId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].Name = gridData[0].Satuan;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].ETD = '';
                                                $scope.non_tam_non_sop_bengkel.data[posisi].ETA = '';
                                                // $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = gridData[0].StandardCost;
                                                // commented 07/10/2020
                                                // $scope.non_tam_non_sop_bengkel.data[posisi].xUnitPrice = gridData[0].StandardCost; //default price with ppn

                                                // $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1.1));
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1+(PPNPerc/100)));

                                                $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost = gridData[0].StandardCost;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartId = gridData[0].PartId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].OutletId = gridData[0].OutletId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].WarehouseId = gridData[0].WarehouseId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].Satuan = gridData[0].Satuan;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsClassId4 = gridData[0].PartsClassId4;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartsCode = gridData[0].PartsCode;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartsName = gridData[0].PartsName;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].UomId = gridData[0].UomId;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].Name = gridData[0].Satuan;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].ETD = '';
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].ETA = '';
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].UnitPrice = gridData[0].StandardCost;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartId = gridData[0].PartId;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].OutletId = gridData[0].OutletId;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].WarehouseId = gridData[0].WarehouseId;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].Satuan = gridData[0].Satuan;
                                                //   $scope.non_tam_non_sop_bengkel.data[$scope.non_tam_non_sop_bengkel.data.length - 1].PartsClassId4 = gridData[0].PartsClassId4;
                                                // =================
												// $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount =Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice));
												// $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount)) * 0.1);
												// //Total  = (Qty * HargaDasar) - Diskon + VAT;
												// $scope.non_tam_non_sop_bengkel.data[posisi].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount;
												//var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1.1);
												//var DiscountSatuan = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * HargaSatuan);
												//$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan;
												// CR5 #10
												console.log('$scope.mData.IsNONPKP at non_tam_non_sop_bengkel ($scope.crudState === 1441) stv: ', $scope.mData.IsNONPKP);
												if($scope.mData.IsNONPKP == false){
													//PKP

													// var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1.1);
													var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1+(PPNPerc/100));

													var DiscountSatuan = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * HargaSatuan);
													$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan;

													// $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * 0.1);
													$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * (PPNPerc/100));

													// $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / 1.1;
													$scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1+(PPNPerc/100));

												}else if($scope.mData.IsNONPKP == true){
													//Non PKP
													var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
													var DiscountSatuan = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * HargaSatuan);
													$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan;
													$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = 0;
													$scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
												}
												// CR5 #10
												//$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * 0.1);
                                                if($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO){
                                                    $scope.non_tam_non_sop_bengkel.data[posisi].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount;
                                                }
                                                // =============
                                                  console.log("HEY YOU THERE3",$scope.non_tam_non_sop_bengkel.data);
                                                  PurchaseOrder.GridData = $scope.non_tam_non_sop_bengkel.data;
                                                  $scope.mData.GridDetail = $scope.non_tam_non_sop_bengkel.data;
                                              } else if ($scope.crudState === '1442') {
                                                var cekInput = '';
                                                var cekRes = angular.copy(gridData[0].PartsCode)
                                                var posisi = 0;
                                                for (var i=0; i<$scope.non_tam_non_sop.data.length; i++){
                                                    cekInput = angular.copy($scope.non_tam_non_sop.data[i].PartsCode);
                                                    if (cekInput != undefined && cekInput != null){
                                                        if (cekInput.toUpperCase() == cekRes.toUpperCase()){
                                                            posisi = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                                $scope.non_tam_non_sop.data[posisi].PartsCode = gridData[0].PartsCode;
                                                $scope.non_tam_non_sop.data[posisi].PartsName = gridData[0].PartsName;
                                                $scope.non_tam_non_sop.data[posisi].UomId = gridData[0].UomId;
                                                $scope.non_tam_non_sop.data[posisi].UomPOId = gridData[0].UomId;
                                                $scope.non_tam_non_sop.data[posisi].Name = gridData[0].Satuan;
                                                $scope.non_tam_non_sop.data[posisi].ETD = '';
                                                $scope.non_tam_non_sop.data[posisi].ETA = '';
                                                // $scope.non_tam_non_sop.data[posisi].UnitPrice = gridData[0].StandardCost;
                                                // commented 07/10/2020
                                                // $scope.non_tam_non_sop.data[posisi].xUnitPrice = gridData[0].StandardCost; //default price with ppn

                                                // var UnitPrice = gridData[0].StandardCost / (1.1);
                                                var UnitPrice = gridData[0].StandardCost / (1+(PPNPerc/100));

                                                $scope.non_tam_sop_so.data[posisi].UnitPrice = parseFloat(UnitPrice.toFixed($scope.roundTo)); //2 digit dibelakang koma
                                                $scope.non_tam_sop_so.data[posisi].StandardCost = gridData[0].StandardCost;
                                                $scope.non_tam_sop_so.data[posisi].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                $scope.non_tam_sop_so.data[posisi].PartId = gridData[0].PartId;
                                                $scope.non_tam_sop_so.data[posisi].OutletId = gridData[0].OutletId;
                                                $scope.non_tam_sop_so.data[posisi].WarehouseId = gridData[0].WarehouseId;
                                                $scope.non_tam_sop_so.data[posisi].Satuan = gridData[0].Satuan;
                                                $scope.non_tam_sop_so.data[posisi].PartsClassId4 = gridData[0].PartsClassId4;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].PartsCode = gridData[0].PartsCode;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].PartsName = gridData[0].PartsName;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].UomId = gridData[0].UomId;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].Name = gridData[0].Satuan;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].ETD = '';
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].ETA = '';
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].UnitPrice = gridData[0].StandardCost;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].PartId = gridData[0].PartId;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].OutletId = gridData[0].OutletId;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].WarehouseId = gridData[0].WarehouseId;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].Satuan = gridData[0].Satuan;
                                                // $scope.non_tam_sop_so.data[$scope.non_tam_sop_so.data.length - 1].PartsClassId4 = gridData[0].PartsClassId4;
												
												// $scope.non_tam_sop_so.data[posisi].DiscountAmount =Math.round((($scope.non_tam_sop_so.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice));
												// $scope.non_tam_sop_so.data[posisi].VATAmount = Math.round((($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice) - ($scope.non_tam_sop_so.data[posisi].DiscountAmount)) * 0.1);
												// //Total  = (Qty * HargaDasar) - Diskon + VAT;
												// $scope.non_tam_sop_so.data[posisi].TotalAmount = ($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice) - $scope.non_tam_sop_so.data[posisi].DiscountAmount + $scope.non_tam_sop_so.data[posisi].VATAmount;
												//var HargaSatuan = $scope.non_tam_sop_so.data[posisi].StandardCost / (1.1);
												//var DiscountSatuan = ($scope.non_tam_sop_so.data[posisi].DiscountPercent / 100) * HargaSatuan;
												//$scope.non_tam_sop_so.data[posisi].DiscountAmount = $scope.non_tam_sop_so.data[posisi].QtyPO * DiscountSatuan.toFixed($scope.roundTo);
												// CR5 #10
												console.log('$scope.mData.IsNONPKP at non_tam_sop_so ($scope.crudState === 1442) stv: ', $scope.mData.IsNONPKP);
												if($scope.mData.IsNONPKP == false){
													//PKP
													// var HargaSatuan = $scope.non_tam_sop_so.data[posisi].StandardCost / (1.1);
													var HargaSatuan = $scope.non_tam_sop_so.data[posisi].StandardCost / (1+(PPNPerc/100));

													var DiscountSatuan = ($scope.non_tam_sop_so.data[posisi].DiscountPercent / 100) * HargaSatuan;
													$scope.non_tam_sop_so.data[posisi].DiscountAmount = $scope.non_tam_sop_so.data[posisi].QtyPO * DiscountSatuan.toFixed($scope.roundTo);

													// var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
													var VATSatuan = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);

													$scope.non_tam_sop_so.data[posisi].VATAmount = $scope.non_tam_sop_so.data[posisi].QtyPO * VATSatuan.toFixed($scope.roundTo);

													// $scope.non_tam_sop_so.data[posisi].UnitPrice = Math.round($scope.non_tam_sop_so.data[posisi].StandardCost / 1.1);
													$scope.non_tam_sop_so.data[posisi].UnitPrice = Math.round($scope.non_tam_sop_so.data[posisi].StandardCost / (1+(PPNPerc/100)));

												}else if($scope.mData.IsNONPKP == true){
													//Non PKP
													var HargaSatuan = $scope.non_tam_sop_so.data[posisi].StandardCost;
													var DiscountSatuan = ($scope.non_tam_sop_so.data[posisi].DiscountPercent / 100) * HargaSatuan;
													$scope.non_tam_sop_so.data[posisi].DiscountAmount = $scope.non_tam_sop_so.data[posisi].QtyPO * DiscountSatuan.toFixed($scope.roundTo);
													$scope.non_tam_sop_so.data[posisi].VATAmount = 0;
													$scope.non_tam_sop_so.data[posisi].UnitPrice = Math.round($scope.non_tam_sop_so.data[posisi].StandardCost);
												}
												// CR5 #10
												//var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
												//$scope.non_tam_sop_so.data[posisi].VATAmount = $scope.non_tam_sop_so.data[posisi].QtyPO * VATSatuan.toFixed($scope.roundTo);
                                                if($scope.non_tam_sop_so.data[posisi].QtyPO){
                                                    $scope.non_tam_sop_so.data[posisi].TotalAmount = Math.round(($scope.non_tam_sop_so.data[posisi].QtyPO * $scope.non_tam_sop_so.data[posisi].UnitPrice) - $scope.non_tam_sop_so.data[posisi].DiscountAmount + $scope.non_tam_sop_so.data[posisi].VATAmount);
                                                }
                                                PurchaseOrder.GridData = $scope.non_tam_non_sop.data;
                                                $scope.mData.GridDetail = $scope.non_tam_non_sop.data;
                                            }
                                            else if ($scope.crudState === '314') {
                                                var cekInput = '';
                                                var cekRes = angular.copy(gridData[0].PartsCode)
                                                var posisi = 0;
                                                for (var i=0; i<$scope.non_tam_non_sop_bengkel.data.length; i++){
                                                    cekInput = angular.copy($scope.non_tam_non_sop_bengkel.data[i].PartsCode);
                                                    if (cekInput != undefined && cekInput != null){
                                                        if (cekInput.toUpperCase() == cekRes.toUpperCase()){
                                                            posisi = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsCode = gridData[0].PartsCode;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsName = gridData[0].PartsName;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UomId = gridData[0].UomId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UomPOId = gridData[0].UomId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].Name = gridData[0].Satuan;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].ETD = ETDFix;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].ETA = ETAFix;
                                                // $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = gridData[0].StandardCost;
                                                // commented 07/10/2020
                                                // $scope.non_tam_non_sop_bengkel.data[posisi].xUnitPrice = gridData[0].StandardCost; //default price with ppn

                                                // $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1.1));
                                                $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1+(PPNPerc/100)));

                                                $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost = gridData[0].StandardCost;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartId = gridData[0].PartId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].OutletId = gridData[0].OutletId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].WarehouseId = gridData[0].WarehouseId;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].PartsClassId4 = gridData[0].PartsClassId4;
                                                $scope.non_tam_non_sop_bengkel.data[posisi].maxCost = gridData[0].maxCost;
                                                
                                                // =================
												// $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount =Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice));
												// $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - ($scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount)) * 0.1);
												// //Total  = (Qty * HargaDasar) - Diskon + VAT;
												// $scope.non_tam_non_sop_bengkel.data[posisi].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount;
												//var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1.1);
												//var DiscountSatuan = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * HargaSatuan);
												//console.log("314 HargaSatuan:" , HargaSatuan);
												//console.log("314 DiscountSatuan:" , DiscountSatuan);
												//$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan;
												// CR5 #10
												console.log('$scope.mData.IsNONPKP at non_tam_non_sop_bengkel ($scope.crudState === 314) stv : ', $scope.mData.IsNONPKP);
												if($scope.mData.IsNONPKP == false){
													//PKP
													// var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1.1);
													var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1+(PPNPerc/100));

													var DiscountSatuan = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * HargaSatuan);
													console.log("314 HargaSatuan:" , HargaSatuan);
													console.log("314 DiscountSatuan:" , DiscountSatuan);
													$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan;

													// $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * 0.1);
													$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * (PPNPerc/100));

													// $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / 1.1;
													$scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost / (1+(PPNPerc/100));

												}else if($scope.mData.IsNONPKP == true){
													//Non PKP
													var HargaSatuan = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
													var DiscountSatuan = Math.round((($scope.non_tam_non_sop_bengkel.data[posisi].DiscountPercent / 100)) * HargaSatuan);
													console.log("314 HargaSatuan:" , HargaSatuan);
													console.log("314 DiscountSatuan:" , DiscountSatuan);
													$scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * DiscountSatuan;
													$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = 0;
													$scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice = $scope.non_tam_non_sop_bengkel.data[posisi].StandardCost;
												}
												// CR5 #10
												//$scope.non_tam_non_sop_bengkel.data[posisi].VATAmount = $scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * 0.1);
                                                if($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO){
                                                    $scope.non_tam_non_sop_bengkel.data[posisi].TotalAmount = ($scope.non_tam_non_sop_bengkel.data[posisi].QtyPO * $scope.non_tam_non_sop_bengkel.data[posisi].UnitPrice) - $scope.non_tam_non_sop_bengkel.data[posisi].DiscountAmount + $scope.non_tam_non_sop_bengkel.data[posisi].VATAmount;
                                                }
                                                // =============

                                                PurchaseOrder.GridData = $scope.non_tam_non_sop_bengkel.data;
                                                $scope.mData.GridDetail = $scope.non_tam_non_sop_bengkel.data;
                                                console.log("314 gridData[0]:" , PurchaseOrder.GridData);
                                            } 
                                            else if ($scope.crudState === '311') {
                                                var cekInput = '';
                                                var cekRes = angular.copy(gridData[0].PartsCode)
                                                var posisi = 0;
                                                for (var i=0; i < $scope.tam_sop_appointment_ubah.data.length; i++){
                                                    cekInput = angular.copy($scope.tam_sop_appointment_ubah.data[i].PartsCode);
                                                    if (cekInput != undefined && cekInput != null){
                                                        if (cekInput.toUpperCase() == cekRes.toUpperCase()){
                                                            posisi = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                                console.log("posisi =>",posisi);
                                                console.log("pikon =>",gridData[0].PartsCode,$scope.tam_sop_appointment_ubah.data);
                                                $scope.tam_sop_appointment_ubah.data[posisi].PartsCode = gridData[0].PartsCode;
                                                $scope.tam_sop_appointment_ubah.data[posisi].PartsName = gridData[0].PartsName;
                                                $scope.tam_sop_appointment_ubah.data[posisi].UomId = gridData[0].UomId;
                                                $scope.tam_sop_appointment_ubah.data[posisi].UomPOId = gridData[0].UomId;
                                                $scope.tam_sop_appointment_ubah.data[posisi].Name = gridData[0].Satuan;
                                                $scope.tam_sop_appointment_ubah.data[posisi].ETD = ETDFix;
                                                $scope.tam_sop_appointment_ubah.data[posisi].ETA = ETAFix;

                                                // $scope.tam_sop_appointment_ubah.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1.1));
                                                $scope.tam_sop_appointment_ubah.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1+(PPNPerc/100)));

                                                $scope.tam_sop_appointment_ubah.data[posisi].StandardCost = gridData[0].StandardCost;
                                                $scope.tam_sop_appointment_ubah.data[posisi].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                $scope.tam_sop_appointment_ubah.data[posisi].PartId = gridData[0].PartId;
                                                $scope.tam_sop_appointment_ubah.data[posisi].OutletId = gridData[0].OutletId;
                                                $scope.tam_sop_appointment_ubah.data[posisi].WarehouseId = gridData[0].WarehouseId;
                                                $scope.tam_sop_appointment_ubah.data[posisi].PartsClassId4 = gridData[0].PartsClassId4;
                                                $scope.tam_sop_appointment_ubah.data[posisi].StatusTPOS = gridData[0].StatusTPOS;
                                                $scope.tam_sop_appointment_ubah.data[posisi].ReasonTPOS = gridData[0].ReasonTPOS;
                                                // =================
                                                if($scope.tam_sop_appointment_ubah.data[posisi].QtyPO){
                                                    // $scope.tam_sop_appointment_ubah.data[posisi].VATAmount = Math.round((($scope.tam_sop_appointment_ubah.data[posisi].QtyPO * $scope.tam_sop_appointment_ubah.data[posisi].UnitPrice) ) * 0.1);
                                                    // //Total  = (Qty * HargaDasar) - Diskon + VAT;
                                                    // $scope.tam_sop_appointment_ubah.data[posisi].TotalAmount = ($scope.tam_sop_appointment_ubah.data[posisi].QtyPO * $scope.tam_sop_appointment_ubah.data[posisi].UnitPrice) + $scope.tam_sop_appointment_ubah.data[posisi].VATAmount;

                                                    // var HargaSatuan = $scope.tam_sop_wo_ubah.data[posisi].StandardCost / (1.1);
                                                    var HargaSatuan = $scope.tam_sop_wo_ubah.data[posisi].StandardCost / (1+(PPNPerc/100));

                                                    var DiscountSatuan = Math.round((($scope.tam_sop_wo_ubah.data[posisi].DiscountPercent / 100)) * HargaSatuan);
                                                    $scope.tam_sop_appointment_ubah.data[posisi].DiscountAmount = $scope.tam_sop_appointment_ubah.data[posisi].QtyPO * DiscountSatuan;

                                                    // $scope.tam_sop_appointment_ubah.data[posisi].VATAmount = $scope.tam_sop_appointment_ubah.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * 0.1);
                                                    $scope.tam_sop_appointment_ubah.data[posisi].VATAmount = $scope.tam_sop_appointment_ubah.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * (PPNPerc/100));

                                                    $scope.tam_sop_appointment_ubah.data[posisi].TotalAmount = ($scope.tam_sop_appointment_ubah.data[posisi].QtyPO * $scope.tam_sop_appointment_ubah.data[posisi].UnitPrice) - $scope.tam_sop_appointment_ubah.data[posisi].DiscountAmount + $scope.tam_sop_appointment_ubah.data[posisi].VATAmount;
                                                }
                                                // =============
                                                // if($scope.tam_sop_appointment_ubah.data[posisi].QtyPO){
                                                //     $scope.tam_sop_appointment_ubah.data[posisi].TotalAmount = ($scope.tam_sop_appointment_ubah.data[posisi].QtyPO * $scope.tam_sop_appointment_ubah.data[posisi].UnitPrice);
                                                // }
                                                $scope.tam_sop_appointment_ubah.data[posisi].maxCost = gridData[0].maxCost;
                                                PurchaseOrder.GridData = $scope.tam_sop_appointment_ubah.data;
                                                $scope.mData.GridDetail = $scope.tam_sop_appointment_ubah.data;
                                            }else if ($scope.crudState === '312') {
                                                var cekInput = '';
                                                var cekRes = angular.copy(gridData[0].PartsCode)
                                                var posisi = 0;
                                                for (var i=0; i < $scope.tam_sop_wo_ubah.data.length; i++){
                                                    cekInput = angular.copy($scope.tam_sop_wo_ubah.data[i].PartsCode);
                                                    if (cekInput != undefined && cekInput != null){
                                                        if (cekInput.toUpperCase() == cekRes.toUpperCase()){
                                                            posisi = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                                console.log("posisi =>",posisi);
                                                console.log("pikon =>",gridData[0].PartsCode,$scope.tam_sop_wo_ubah.data);
                                                $scope.tam_sop_wo_ubah.data[posisi].PartsCode = gridData[0].PartsCode;
                                                $scope.tam_sop_wo_ubah.data[posisi].PartsName = gridData[0].PartsName;
                                                $scope.tam_sop_wo_ubah.data[posisi].UomId = gridData[0].UomId;
                                                $scope.tam_sop_wo_ubah.data[posisi].UomPOId = gridData[0].UomId;
                                                $scope.tam_sop_wo_ubah.data[posisi].Name = gridData[0].Satuan;
                                                $scope.tam_sop_wo_ubah.data[posisi].ETD = ETDFix;
                                                $scope.tam_sop_wo_ubah.data[posisi].ETA = ETAFix;

                                                // $scope.tam_sop_wo_ubah.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1.1));
                                                $scope.tam_sop_wo_ubah.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1+(PPNPerc/100)));

                                                $scope.tam_sop_wo_ubah.data[posisi].StandardCost = gridData[0].StandardCost;
                                                // $scope.tam_sop_wo_ubah.data[posisi].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                $scope.tam_sop_wo_ubah.data[posisi].PartId = gridData[0].PartId;
                                                $scope.tam_sop_wo_ubah.data[posisi].OutletId = gridData[0].OutletId;
                                                $scope.tam_sop_wo_ubah.data[posisi].WarehouseId = gridData[0].WarehouseId;
                                                $scope.tam_sop_wo_ubah.data[posisi].PartsClassId4 = gridData[0].PartsClassId4;
                                                $scope.tam_sop_wo_ubah.data[posisi].StatusTPOS = gridData[0].StatusTPOS;
                                                $scope.tam_sop_wo_ubah.data[posisi].ReasonTPOS = gridData[0].ReasonTPOS;
                                                
                                                if($scope.tam_sop_wo_ubah.data[posisi].QtyPO){
													// $scope.tam_sop_wo_ubah.data[posisi].VATAmount = Math.round((($scope.tam_sop_wo_ubah.data[posisi].QtyPO * $scope.tam_sop_wo_ubah.data[posisi].UnitPrice) ) * 0.1);
													// $scope.tam_sop_wo_ubah.data[posisi].TotalAmount = ($scope.tam_sop_wo_ubah.data[posisi].QtyPO * gridData[0].StandardCost);

													// var HargaSatuan = $scope.tam_sop_wo_ubah.data[posisi].StandardCost / (1.1);
													var HargaSatuan = $scope.tam_sop_wo_ubah.data[posisi].StandardCost / (1+(PPNPerc/100));

													var DiscountSatuan = Math.round((($scope.tam_sop_wo_ubah.data[posisi].DiscountPercent / 100)) * HargaSatuan);
													$scope.tam_sop_wo_ubah.data[posisi].DiscountAmount = $scope.tam_sop_wo_ubah.data[posisi].QtyPO * DiscountSatuan;
													// CR5 #10
													//console.log('$scope.mData.IsNONPKP at tam_sop_wo_ubah ($scope.crudState === 312) stv : ', $scope.mData.IsNONPKP);
													//if($scope.mData.IsNONPKP == false){
														//PKP
														//$scope.tam_sop_wo_ubah.data[posisi].VATAmount = $scope.tam_sop_wo_ubah.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * 0.1);
													//}else if($scope.mData.IsNONPKP == true){
														//Non PKP
														//$scope.tam_sop_wo_ubah.data[posisi].VATAmount = 0;
													//}
													// CR5 #10
													// $scope.tam_sop_wo_ubah.data[posisi].VATAmount = $scope.tam_sop_wo_ubah.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * 0.1);
													$scope.tam_sop_wo_ubah.data[posisi].VATAmount = $scope.tam_sop_wo_ubah.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * (PPNPerc/100));

                                                    $scope.tam_sop_wo_ubah.data[posisi].TotalAmount = ($scope.tam_sop_wo_ubah.data[posisi].QtyPO * $scope.tam_sop_wo_ubah.data[posisi].UnitPrice) - $scope.tam_sop_wo_ubah.data[posisi].DiscountAmount + $scope.tam_sop_wo_ubah.data[posisi].VATAmount;
                                                }
                                                PurchaseOrder.GridData = $scope.tam_sop_wo_ubah.data;
                                                $scope.mData.GridDetail = $scope.tam_sop_wo_ubah.data;
                                            }else if ($scope.crudState === '313') {
                                                var cekInput = '';
                                                var cekRes = angular.copy(gridData[0].PartsCode)
                                                var posisi = 0;
                                                for (var i=0; i < $scope.non_tam_sop_so_ubah.data.length; i++){
                                                    cekInput = angular.copy($scope.non_tam_sop_so_ubah.data[i].PartsCode);
                                                    if (cekInput != undefined && cekInput != null){
                                                        if (cekInput.toUpperCase() == cekRes.toUpperCase()){
                                                            posisi = i;
                                                            break;
                                                        }
                                                    }
                                                }
                                                console.log("posisi 313=>",posisi);
                                                console.log("pikon 313=>",gridData[0].PartsCode,$scope.non_tam_sop_so_ubah.data);
                                                $scope.non_tam_sop_so_ubah.data[posisi].PartsCode = gridData[0].PartsCode;
                                                $scope.non_tam_sop_so_ubah.data[posisi].PartsName = gridData[0].PartsName;
                                                $scope.non_tam_sop_so_ubah.data[posisi].UomId = gridData[0].UomId;
                                                $scope.non_tam_sop_so_ubah.data[posisi].UomPOId = gridData[0].UomId;
                                                $scope.non_tam_sop_so_ubah.data[posisi].Name = gridData[0].Satuan;
                                                $scope.non_tam_sop_so_ubah.data[posisi].ETD = ETDFix;
                                                $scope.non_tam_sop_so_ubah.data[posisi].ETA = ETAFix;

                                                // $scope.non_tam_sop_so_ubah.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1.1));
                                                $scope.non_tam_sop_so_ubah.data[posisi].UnitPrice = Math.round(gridData[0].StandardCost / (1+(PPNPerc/100)));

                                                $scope.non_tam_sop_so_ubah.data[posisi].StandardCost = gridData[0].StandardCost;
                                                // $scope.non_tam_sop_so_ubah.data[posisi].DiscountPercent = gridData[0].DiscountPerMaterial;
                                                $scope.non_tam_sop_so_ubah.data[posisi].PartId = gridData[0].PartId;
                                                $scope.non_tam_sop_so_ubah.data[posisi].OutletId = gridData[0].OutletId;
                                                $scope.non_tam_sop_so_ubah.data[posisi].WarehouseId = gridData[0].WarehouseId;
                                                $scope.non_tam_sop_so_ubah.data[posisi].PartsClassId4 = gridData[0].PartsClassId4;
                                                $scope.non_tam_sop_so_ubah.data[posisi].maxCost = gridData[0].maxCost;
                                                $scope.non_tam_sop_so_ubah.data[posisi].StatusTPOS = gridData[0].StatusTPOS;
                                                $scope.non_tam_sop_so_ubah.data[posisi].ReasonTPOS = gridData[0].ReasonTPOS;
                                                
												// $scope.non_tam_sop_so_ubah.data[posisi].VATAmount = Math.round((($scope.non_tam_sop_so_ubah.data[posisi].QtyPO * $scope.non_tam_sop_so_ubah.data[posisi].UnitPrice) ) * 0.1);
												// $scope.non_tam_sop_so_ubah.data[posisi].TotalAmount = ($scope.non_tam_sop_so_ubah.data[posisi].QtyPO * gridData[0].StandardCost);
												//var HargaSatuan = $scope.non_tam_sop_so_ubah.data[posisi].StandardCost / (1.1);
												//var DiscountSatuan = Math.round((($scope.non_tam_sop_so_ubah.data[posisi].DiscountPercent / 100)) * HargaSatuan);
												//$scope.non_tam_sop_so_ubah.data[posisi].DiscountAmount = $scope.non_tam_sop_so_ubah.data[posisi].QtyPO * DiscountSatuan;
												// CR5 #10
												console.log('$scope.mData.IsNONPKP at non_tam_sop_so_ubah ($scope.crudState === 313) stv: ', $scope.mData.IsNONPKP);
												if($scope.mData.IsNONPKP == false){
													//PKP
													// var HargaSatuan = $scope.non_tam_sop_so_ubah.data[posisi].StandardCost / (1.1);
													var HargaSatuan = $scope.non_tam_sop_so_ubah.data[posisi].StandardCost / (1+(PPNPerc/100));

													var DiscountSatuan = Math.round((($scope.non_tam_sop_so_ubah.data[posisi].DiscountPercent / 100)) * HargaSatuan);
													$scope.non_tam_sop_so_ubah.data[posisi].DiscountAmount = $scope.non_tam_sop_so_ubah.data[posisi].QtyPO * DiscountSatuan;

													// $scope.non_tam_sop_so_ubah.data[posisi].VATAmount = $scope.non_tam_sop_so_ubah.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * 0.1);
													$scope.non_tam_sop_so_ubah.data[posisi].VATAmount = $scope.non_tam_sop_so_ubah.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * (PPNPerc/100));

													// $scope.non_tam_sop_so_ubah.data[posisi].UnitPrice = $scope.non_tam_sop_so_ubah.data[posisi].StandardCost / 1.1;
													$scope.non_tam_sop_so_ubah.data[posisi].UnitPrice = $scope.non_tam_sop_so_ubah.data[posisi].StandardCost / (1+(PPNPerc/100));

												}else if($scope.mData.IsNONPKP == true){
													//Non PKP
													var HargaSatuan = $scope.non_tam_sop_so_ubah.data[posisi].StandardCost;
													var DiscountSatuan = Math.round((($scope.non_tam_sop_so_ubah.data[posisi].DiscountPercent / 100)) * HargaSatuan);
													$scope.non_tam_sop_so_ubah.data[posisi].DiscountAmount = $scope.non_tam_sop_so_ubah.data[posisi].QtyPO * DiscountSatuan;
													$scope.non_tam_sop_so_ubah.data[posisi].VATAmount = 0;
													$scope.non_tam_sop_so_ubah.data[posisi].UnitPrice = $scope.non_tam_sop_so_ubah.data[posisi].StandardCost;
												}
												// CR5 #10
												//$scope.non_tam_sop_so_ubah.data[posisi].VATAmount = $scope.non_tam_sop_so_ubah.data[posisi].QtyPO * Math.round((HargaSatuan - DiscountSatuan) * 0.1);
                                                if($scope.non_tam_sop_so_ubah.data[posisi].QtyPO){
                                                    $scope.non_tam_sop_so_ubah.data[posisi].TotalAmount = ($scope.non_tam_sop_so_ubah.data[posisi].QtyPO * $scope.non_tam_sop_so_ubah.data[posisi].UnitPrice) - $scope.non_tam_sop_so_ubah.data[posisi].DiscountAmount + $scope.non_tam_sop_so_ubah.data[posisi].VATAmount;
                                                }
                                                PurchaseOrder.GridData = $scope.non_tam_sop_so_ubah.data;
                                                $scope.mData.GridDetail = $scope.non_tam_sop_so_ubah.data;
                                            }    

                                          });
                                  }
                              },
                              function(err) {
                                  console.log("err=>", err);
                                  confirm("Nomor Material Tidak Ditemukan.");
                              }
                          );
                    }
                }
            }
        }

        $scope.changeFormatDate = function(item) {
            var parDate = item;
            console.log("changeFormatDate item", item);
            parDate = new Date(parDate);
            var finalDate
            var yyyy = parDate.getFullYear().toString();
            var mm = (parDate.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = parDate.getDate().toString();
            finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
            console.log("changeFormatDate finalDate", finalDate);
            return finalDate;
        }

        $scope.Delete = function(row) {
            console.log("Delete row",row);
            console.log("Delete row.entity",row.entity);
            console.log("Delete ",$scope.sublet_sop_wo);
            //INI BUAT 
            if ($scope.crudState === '142' || $scope.crudState === '114'  || $scope.crudState == '121'|| $scope.crudState == '141'|| $scope.crudState == '132' || $scope.crudState === '113'|| $scope.crudState == '123'|| $scope.crudState == '134'|| $scope.crudState == '143') {
                console.log("non_tam_sop_wo");
                console.log("nrow.entity",row.entity);
                console.log("$scope.non_tam_sop_so.data",$scope.non_tam_sop_so.data);
                console.log("$scope.non_tam_sop_so.data.indexOf(row.entity)",$scope.non_tam_sop_so.data.indexOf(row.entity));
                var index = $scope.non_tam_sop_so.data.indexOf(row.entity);
                $scope.non_tam_sop_so.data.splice(index, 1);
                console.log($scope.non_tam_sop_so.data); 
            } else if ($scope.crudState === '112' || $scope.crudState == '122' || $scope.crudState == '132'|| $scope.crudState == '111'|| $scope.crudState == '114') {
                console.log("tam_sop_appointment");
                var index = $scope.tam_sop_appointment.data.indexOf(row.entity);
                $scope.tam_sop_appointment.data.splice(index, 1);
                //console.log($scope.gridSO.data);
            } else if ($scope.crudState === '152' || $scope.crudState === '151' ) {
                console.log("Sublet");
                // idx = _.findIndex($scope.non_tam_sop_so.data , { idx: $scope.selectedRows[x].idx });
                // $scope.non_tam_sop_so.data.splice(idx,1);
                var index = $scope.sublet_sop_wo.data.indexOf(row.entity);
                $scope.sublet_sop_wo.data.splice(index, 1);
                //console.log($scope.sublet_sop_wo.data);
            } else if ($scope.crudState === '1441') {
                console.log("1441");
                var index = $scope.non_tam_non_sop_bengkel.data.indexOf(row.entity);
                $scope.non_tam_non_sop_bengkel.data.splice(index, 1);
                //console.log($scope.sublet_sop_wo.data);
            } else if ($scope.crudState === '1442') {
                console.log("1442");
                var index = $scope.non_tam_non_sop.data.indexOf(row.entity);
                $scope.non_tam_non_sop.data.splice(index, 1);
                //console.log($scope.sublet_sop_wo.data);
            } 
            //INI EDIT
            else if ($scope.crudState === '311') {
                console.log("tam_sop_appointment_ubah");
                var index = $scope.tam_sop_appointment_ubah.data.indexOf(row.entity);
                $scope.tam_sop_appointment_ubah.data.splice(index, 1);
                //console.log($scope.gridSO.data);
            } else if ($scope.crudState === '312') {
                console.log("tam_sop_wo_lihat");
                var index = $scope.tam_sop_wo_lihat.data.indexOf(row.entity);
                $scope.tam_sop_wo_lihat.data.splice(index, 1);
                //console.log($scope.sublet_sop_wo.data);
            } else if ($scope.crudState === '313' || $scope.crudState === '3431') {
                console.log("non_tam_sop_so");
                var index = $scope.non_tam_sop_so.data.indexOf(row.entity);
                $scope.non_tam_sop_so.data.splice(index, 1);
                //console.log($scope.sublet_sop_wo.data);
            } else if ($scope.crudState === '344') {
                console.log("tam_soq_lihat");
                var index = $scope.tam_soq_lihat.data.indexOf(row.entity);
                $scope.tam_soq_lihat.data.splice(index, 1);
                //console.log($scope.sublet_sop_wo.data);
            } else if ($scope.crudState === ''||$scope.crudState === '341' ||$scope.crudState === '3432' ) {
                console.log("non_tam_sop_so");
                var index = $scope.non_tam_sop_so.data.indexOf(row.entity);
                $scope.non_tam_sop_so.data.splice(index, 1);
                //console.log($scope.sublet_sop_wo.data);
            }  else if ($scope.crudState === '314'||$scope.crudState === '324' || $scope.crudState === '334' || $scope.crudState === '354' || $scope.crudState === '342' || $scope.crudState === '3421' || $scope.crudState === '114') {
                console.log("non_tam_non_sop_bengkel");
                var index = $scope.non_tam_non_sop_bengkel.data.indexOf(row.entity);
                $scope.non_tam_non_sop_bengkel.data.splice(index, 1);
                //console.log($scope.sublet_sop_wo.data);
            } else if ($scope.crudState === '352' || $scope.crudState === '351') {
                console.log("sublet_sop_wo");
                var index = $scope.sublet_sop_wo.data.indexOf(row.entity);
                $scope.sublet_sop_wo.data.splice(index, 1);
                //console.log($scope.sublet_sop_wo.data);
            } else if ($scope.crudState === '112') {
                console.log("sublet_sop_wo");
                var index = $scope.non_tam_sop_so.data.indexOf(row.entity);
                $scope.non_tam_sop_so.data.splice(index, 1);
                //console.log($scope.sublet_sop_wo.data);
            } 

            console.log(index);
        };

        $scope.showMe = function() {
            var result = confirm('Apakah Anda yakin menghapus data ini?');
            if (result) {
                console.log(result);
                console.log($scope.crudState);
                console.log($scope.gridApi.selection.getSelectedRows());
                angular.forEach($scope.gridApi.selection.getSelectedRows(), function(data, index) {
                    $scope.sublet_sop_wo.data.splice($scope.sublet_sop_wo.data.lastIndexOf(data), 1);
                });
            }
        };

        $scope.ETDDialog = function(row) {
            console.log($scope.non_tam_sop_wo.data);
            if (($scope.crudState === '114') || ($scope.crudState === '124') || ($scope.crudState === '134')) {
                PurchaseOrder.GridDetail = $scope.non_tam_non_sop_bengkel.data;
                var n = $scope.non_tam_non_sop_bengkel.data.indexOf(row.entity)
                console.log("Index = ", n);
                PurchaseOrder.setETD(n);
            } else if (($scope.crudState === '113') || ($scope.crudState === '123') || ($scope.crudState === '133') || ($scope.crudState === '143')) {
                PurchaseOrder.GridDetail = $scope.non_tam_sop_so.data;
                var n = $scope.non_tam_sop_so.data.indexOf(row.entity)
                console.log("Index = ", n);
                PurchaseOrder.setETD(n);
            } else if (($scope.crudState === '1441')) {
                PurchaseOrder.GridDetail = $scope.non_tam_non_sop_bengkel.data;
                var n = $scope.non_tam_non_sop_bengkel.data.indexOf(row.entity)
                console.log("Index = ", n);
                PurchaseOrder.setETD(n);
            } else if (($scope.crudState === '111')) {
                PurchaseOrder.GridDetail = $scope.tam_sop_appointment.data;
                var n = $scope.tam_sop_appointment.data.indexOf(row.entity)
                console.log("Index = ", n);
                PurchaseOrder.setETD(n);
            } else {
                PurchaseOrder.GridDetail = $scope.non_tam_sop_wo.data;
                var n = $scope.non_tam_sop_wo.data.indexOf(row.entity)
                console.log("Index = ", n);
                PurchaseOrder.setETD(n);
            }

            try {
                ngDialog.openConfirm({
                    template: '\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Date Time Picker</b></p>\
                     <div class="ngdialog-buttons" align="center">\
                        <sddatetime \
                          hour-step="hourStep" \
                          minute-step="minuteStep"\
                          ng-model="date" \
                          show-meridian="showMeridian" \
                          date-format="{{format}}" \
                          date-options="dateOptions"\
                          date-disabled="disabled(date, mode)" \
                          show-spinners="true"\
                          date-opened="dateOpened"\
                          min-date="minDate"\
                          max-date="maxDate"\
                        >\
                      </sddatetime>\
                      <pre>{{DateTimeFilter(date)}}</pre>\
                      <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                      <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="GetDateTime(date, 1)">Ya</button>\
                     </div>\
                     </div>',
                    plain: true,
                    controller: 'PurchaseOrderController',
                });
            } catch (err) {
                //console.log("");
            }

        }

        $scope.ETADialog = function(row) {
            if (($scope.crudState === '1441') || ($scope.crudState === '114') || ($scope.crudState === '124') || ($scope.crudState === '134')) {
                PurchaseOrder.GridDetail = $scope.non_tam_non_sop_bengkel.data;
                var n = $scope.non_tam_non_sop_bengkel.data.indexOf(row.entity)
                console.log("Index = ", n);
                PurchaseOrder.setETA(n);
            } else if (($scope.crudState === '113') || ($scope.crudState === '123') || ($scope.crudState === '133') || ($scope.crudState === '143')) {
                PurchaseOrder.GridDetail = $scope.non_tam_sop_so.data;
                var n = $scope.non_tam_sop_so.data.indexOf(row.entity)
                console.log("Index = ", n);
                PurchaseOrder.setETD(n);
            } else if (($scope.crudState === '111')) {
                PurchaseOrder.GridDetail = $scope.tam_sop_appointment.data;
                var n = $scope.tam_sop_appointment.data.indexOf(row.entity)
                console.log("Index = ", n);
                PurchaseOrder.setETD(n);
            } else {
                PurchaseOrder.GridDetail = $scope.non_tam_sop_wo.data;
                var n = $scope.non_tam_sop_wo.data.indexOf(row.entity)
                console.log("Index = ", n);
                PurchaseOrder.setETA(n);
            }
            // var n = $scope.non_tam_sop_wo.data.indexOf(row.entity)
            // console.log("Index = ", n);
            // PurchaseOrder.setETA(n);
            try {
                ngDialog.openConfirm({
                    template: '\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Date Time Picker</b></p>\
                     <div class="ngdialog-buttons" align="center">\
                        <sddatetime \
                          hour-step="hourStep" \
                          minute-step="minuteStep"\
                          ng-model="date" \
                          show-meridian="showMeridian" \
                          date-format="{{format}}" \
                          date-options="dateOptions"\
                          date-disabled="disabled(date, mode)" \
                          show-spinners="true"\
                          date-opened="dateOpened"\
                          min-date="minDate"\
                          max-date="maxDate"\
                        >\
                      </sddatetime>\
                      <pre>{{DateTimeFilter(date)}}</pre>\
                      <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                      <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="GetDateTime(date, 2)">Ya</button>\
                     </div>\
                     </div>',
                    plain: true,
                    controller: 'PurchaseOrderController',
                });
            } catch (err) {
                //console.log("");
            }

        }
        $scope.disabledTambah = false;
        $scope.searchEst = function(){
            console.log("search Est1",$scope.mData);
            console.log("search Est2",$scope.mData.NoEstimasi);
            if($scope.mData.NoEstimasi == "" || $scope.mData.NoEstimasi == null || $scope.mData.NoEstimasi == undefined){
                $scope.non_tam_sop_so.data = [];

                $scope.disabledTambah = false;
                console.log("data estimasinya kosong");
            }else{
                PurchaseOrder.getEstimation($scope.mData.NoEstimasi).then(
                function(res) {
                    var dataEst = res.data.Result;
                    $scope.disabledTambah = true;
                    console.log("dataEst",dataEst);
                    $scope.mData.Requester = dataEst[0].Requester;
                    $scope.mData.RefPONo = $scope.mData.NoEstimasi;
                    $scope.mData.DocDate = $scope.localeDate(new(Date));
                    $scope.mData.RequiredDate = $scope.localeDate(new(Date));
                    console.log("$scope.mData==>",$scope.mData);

                    $scope.non_tam_sop_so.data = [] //kosongin grid ketika klik search biar data di grid ga ketimpa sama data yg udah ada sebelumnya
                    for(var a in dataEst){

                        if($scope.user.RoleName == "Partsman BP"){

                            if(dataEst[a].MaterialType == "Parts"){
                                // dataEst[a].UnitPrice = dataEst[a].RetailPrice;
                                if(dataEst[a].MaterialTypeWarehouse == 1){

									/*$scope.non_tam_sop_so.data.push(dataEst[a]);
                                    console.log('masuk parts')*/
									
                                    /*if(dataEst[a].QtyOnHand == 0){ //penjagaan ini dilepas karna  bentrok dengan skenatio AS ISSUE 261
										$scope.non_tam_sop_so.data.push(dataEst[a]);
										console.log('masuk parts')
                                    }*/
									
									if(dataEst[a].QtyOnHand + dataEst[a].QtyFree == 0){
										$scope.non_tam_sop_so.data.push(dataEst[a]);
										console.log('masuk parts')
                                    }
                                }
                            }
                        }else{
                            if(dataEst[a].MaterialType == "Bahan"){
                                // dataEst[a].UnitPrice = dataEst[a].RetailPrice;

                                if(dataEst[a].MaterialTypeWarehouse == 2){
                                    
                                    if($scope.mData.POTypeId == 5 && $scope.mData.RefPOTypeId == 4 || $scope.mData.POTypeId == 4 && $scope.mData.RefPOTypeId == 4){
                                        if(dataEst[a].isOPB != 1){
                                            /*$scope.non_tam_sop_so.data.push(dataEst[a]);
                                            console.log('masuk bahan bukan OPB')*/
											
											if(dataEst[a].QtyOnHand + dataEst[a].QtyFree == 0){
												$scope.non_tam_sop_so.data.push(dataEst[a]);
												console.log('masuk bahan bukan OPB stv')
											}
                                        }
                                        else{
                                            console.log('masuk bahan OPB ===>', a)
                                        }
                                    }else{
										/*$scope.non_tam_sop_so.data.push(dataEst[a]);
                                        console.log('masuk bahan bukan OPB')*/
										
										/*if(dataEst[a].QtyOnHand == 0){
											$scope.non_tam_sop_so.data.push(dataEst[a]);
											console.log('masuk bahan bukan OPB')
										}*/
										
										if(dataEst[a].QtyOnHand + dataEst[a].QtyFree == 0){
											$scope.non_tam_sop_so.data.push(dataEst[a]);
											console.log('masuk bahan bukan OPB st')
										}
                                        
                                    }
                                }

                            }
                        }
                    }
                    
                    var getDataParameter = [];
                    var tempParameter = [5];
                    var todayDate = new Date();
                    var checkHours = todayDate.getHours(),
                        hour;
                    //condition to set currentTime
                    if (checkHours < 10) {
                        hour = '0' + todayDate.getHours();
                    } else {
                        hour = todayDate.getHours();
                    }
                    var currentTime = hour + ":" + todayDate.getMinutes() + ":" + todayDate.getSeconds();
                    var ValidThru = '';
                    var EffectiveDate = '';
                    var OrderTypeId = 0;
                    var CutOffTime = '';
                    var ETDDayPlus = '';
                    var ETDHour = '';
                    var LeadTimeArrival = '';
                    var ETDTemp = '',
                        ETD = '',
                        ETDFix = '';
                    var ETATemp = '',
                        ETATempHour = '',
                        ETAFix = '',
                        ETAETO = '';

                     OrderLeadTime.getData().then(
                         function(res) {

                               var getDataParameter = res.data.Result;
                               
                               console.log('OrderLeadTime ==>', getDataParameter)
                               var indexOfValidThru = 0;
                               ValidThru = new Date(getDataParameter[0].ValidThru);
                               EffectiveDate = new Date(getDataParameter[0].EffectiveDate);
                               console.log("getDataParameter: ", getDataParameter);
                               console.log("currentTime: ", currentTime);
                               console.log("todayDate: ", todayDate);
                               console.log('initype', $scope.mData.POTypeId);

                               if (ValidThru >= todayDate && EffectiveDate <= todayDate) {
                                   for(var i in getDataParameter){
                                       console.log(i, " lll ", getDataParameter[i].OrderTypeId);
                                       if (getDataParameter[i].OrderTypeId == 3 && $scope.mData.POTypeId == 3) {
                                           // if (getDataParameter[i].OrderTypeId==1||getDataParameter[i].OrderTypeId==2||getDataParameter[i].OrderTypeId==3){

                                           //not today ETD
                                           if (currentTime >= getDataParameter[i].CutOffTime) {
                                               ETDTemp = new Date(todayDate);
                                               ETDTemp.setDate(todayDate.getDate() + 1 + getDataParameter[i].ETDDayPlus);
                                               console.log("b");
                                           } else {
                                               ETDTemp = todayDate;
                                               console.log("test hari");
                                           }
                                           ETD = $scope.changeFormatDate(ETDTemp);
                                           var ETDHour = getDataParameter[i].ETDHour;
                                           if (ETDHour>24){
                                               ETDHour = ETDHour-24;
                                               if(ETDHour<10){
                                                   ETDHour='0'+ETDHour;
                                               }
                                           }
                                           else{
                                               ETDHour=ETDHour;
                                           }
                                           getDataParameter[i].ETDHour = ETDHour;
                                           ETDFix = ETD + ' ' + getDataParameter[i].ETDHour;
                                           var ETAtanggal = new Date(ETDFix);
                                           ETATempHour = ETAtanggal.getHours();
                                           ETATemp = ETATempHour + getDataParameter[i].LeadTimeArrival;
                                           if (ETATemp>24){
                                               ETATemp = ETATemp-24;
                                             if(ETATemp<10){
                                               ETATemp='0'+ETATemp;
                                             }
                                             else{
                                               ETATemp=ETATemp;
                                             }
                                           }
                                           var ETAminit = ETAtanggal.getMinutes();
                                           if (ETAminit < 10) {
                                               ETAminit = '0' + ETAminit;
                                           } else {
                                               ETAminit = ETAminit;
                                           }
                                           var ETAdetik = ETAtanggal.getSeconds();
                                           if (ETAdetik < 10) {
                                               ETAdetik = '0' + ETAdetik;
                                           } else {
                                               ETAdetik = ETAdetik;
                                           }
                                           ETAFix = ETD + ' ' + ETATemp + ':' + ETAminit + ':' + ETAdetik;

                                           console.log(i, "ETAFixq", ETAFix);
                                           console.log(i, "CutOffTime", getDataParameter[i].CutOffTime);
                                           console.log(i, "ETDTemp", ETDTemp);
                                           console.log(i, "ETD", ETD + getDataParameter[i].ETDHour);
                                           console.log(i, "ETDDayPlus", getDataParameter[i].ETDDayPlus);
                                           break;
                                       } else {
                                           console.log("data percobaan 3");
                                       }
                                   }

                               } else {
                                   console.log("not valid");
                               }

                               $scope.mData.TotalAmount = 0;
                               for(var a in $scope.non_tam_sop_so.data){
                                   for(var b in dataEst){
                                       if($scope.non_tam_sop_so.data[a].PartId == dataEst[b].PartId){
                                            $scope.non_tam_sop_so.data[a].ETD = ETDFix;
                                            $scope.non_tam_sop_so.data[a].ETA = ETAFix;
                                            $scope.non_tam_sop_so.data[a].QtyPO = $scope.non_tam_sop_so.data[a].QtyEstimationBP;
                                            $scope.non_tam_sop_so.data[a].Name = $scope.non_tam_sop_so.data[a].Satuan;
                                            // $scope.non_tam_sop_so.data[a].UnitPrice = $scope.non_tam_sop_so.data[a].AmountAestimationBP;
                                            // $scope.non_tam_sop_so.data[a].StandardCost = $scope.non_tam_sop_so.data[a].AmountAestimationBP;
                                            // $scope.non_tam_sop_so.data[a].TotalAmount = $scope.non_tam_sop_so.data[a].AmountAestimationBP * $scope.non_tam_sop_so.data[a].QtyEstimationBP;

                                            // var UnitPrice = Math.round($scope.non_tam_sop_so.data[a].StandardCost / (1.1));
                                            var UnitPrice = Math.round($scope.non_tam_sop_so.data[a].StandardCost / (1+(PPNPerc/100)));

                                            $scope.non_tam_sop_so.data[a].UnitPrice = parseFloat(UnitPrice.toFixed($scope.roundTo)); //2 digit dibelakang koma
                                            $scope.non_tam_sop_so.data[a].StandardCost = $scope.non_tam_sop_so.data[a].StandardCost;
                                            $scope.non_tam_sop_so.data[a].TotalAmount = $scope.non_tam_sop_so.data[a].StandardCost * $scope.non_tam_sop_so.data[a].QtyEstimationBP;

                                            // $scope.non_tam_sop_so.data[a].VATAmount = Math.round(($scope.non_tam_sop_so.data[a].TotalAmount / 1.1)*0.1);
                                            $scope.non_tam_sop_so.data[a].VATAmount = Math.round(($scope.non_tam_sop_so.data[a].TotalAmount / (1+(PPNPerc/100)))*(PPNPerc/100));

                                            $scope.non_tam_sop_so.data[a].UomPOId = $scope.non_tam_sop_so.data[a].UomId;

                                            $scope.mData.TotalAmount += $scope.non_tam_sop_so.data[a].TotalAmount;
                                           
                                       }
                                   }
                               }
                               $scope.checkRefGINo = false;
                               PurchaseOrder.GridData = $scope.non_tam_sop_so.data;
                     });


                    // $scope.mData.TotalAmount = 0;
                    // for(var a in $scope.non_tam_sop_so.data){
                    //     $scope.mData.TotalAmount += $scope.non_tam_sop_so.data[a].TotalAmount;
                    // }


                    console.log("$scope.non_tam_sop_so.data =>",$scope.non_tam_sop_so.data);
                    console.log("$scope.mData ===>",$scope.mData);
                    console.log("$scope.mData.TotalAmount ===>",$scope.mData.TotalAmount);
                });
            }
        }

        $scope.searchEstOPB = function(){
            $scope.isEstimasiBP = true;
            console.log("searchEstOPB Est1",$scope.mData);
            console.log("searchEstOPB Est2",$scope.mData.NoEstimasi);
            console.log("searchEstOPB Est3=>",$scope.isEstimasiBP);
            if($scope.mData.NoEstimasi == "" || $scope.mData.NoEstimasi == null || $scope.mData.NoEstimasi == undefined){
                $scope.non_tam_sop_so.data = [];

                $scope.disabledTambah = false;
                console.log("data estimasinya kosong");
            }else{
                PurchaseOrder.getMaterialByEstimate($scope.mData.NoEstimasi).then(
                function(res) {
                    var dataEst = res.data.Result;
                    $scope.disabledTambah = true;
                    console.log("dataEst",dataEst);
                    $scope.mData.Requester = dataEst[0].Requester;
                    $scope.mData.RefPONo = $scope.mData.NoEstimasi;
                    console.log("$scope.mData==>",$scope.mData);

                    $scope.non_tam_sop_so.data = [] //kosongin grid ketika klik search biar data di grid ga ketimpa sama data yg udah ada sebelumnya
                    for(var a in dataEst){

                        if($scope.user.RoleName == "Partsman BP"){

                            if(dataEst[a].MaterialType == "Parts"){
                                // dataEst[a].UnitPrice = dataEst[a].RetailPrice;
                                if(dataEst[a].MaterialTypeWarehouse == 1){
									/*$scope.non_tam_sop_so.data.push(dataEst[a]);
                                    console.log('masuk parts')*/
									
                                    /*if(dataEst[a].QtyOnHand == 0){
										$scope.non_tam_sop_so.data.push(dataEst[a]);
										console.log('masuk parts')
									}*/
									
									if(dataEst[a].QtyOnHand + dataEst[a].QtyFree == 0){
										$scope.non_tam_sop_so.data.push(dataEst[a]);
										console.log('masuk parts')
									}
                                }
                            }
                        }else{
                            if(dataEst[a].MaterialType == "Bahan"){
                                // dataEst[a].UnitPrice = dataEst[a].RetailPrice;

                                if(dataEst[a].MaterialTypeWarehouse == 2){
                                    
                                    if(($scope.mData.POTypeId == 5 && $scope.mData.RefPOTypeId == 4) || $scope.mData.POTypeId == 4 && $scope.mData.RefPOTypeId == 4){
                                        if(dataEst[a].isOPB == 1){
                                            /*$scope.non_tam_sop_so.data.push(dataEst[a]);
                                            console.log('masuk bahan OPB')*/
											
											if(dataEst[a].QtyOnHand + dataEst[a].QtyFree == 0){
												$scope.non_tam_sop_so.data.push(dataEst[a]);
												console.log('masuk bahan OPB stv')
											}
                                        }else{
                                            console.log('ga ada yg masuk bahan OPB ===>', a)
                                        }
                                    }else{
										/*$scope.non_tam_sop_so.data.push(dataEst[a]);
                                        console.log('masuk bahan bukan OPB')*/
										
										/*if(dataEst[a].QtyOnHand == 0){
											$scope.non_tam_sop_so.data.push(dataEst[a]);
											console.log('masuk bahan bukan OPB')
										}*/
										
										if(dataEst[a].QtyOnHand + dataEst[a].QtyFree == 0){
											$scope.non_tam_sop_so.data.push(dataEst[a]);
											console.log('masuk bahan bukan OPB st')
										}
                                    }
                                }

                            }
                        }
                    }
                    
                    var getDataParameter = [];
                    var tempParameter = [5];
                    var todayDate = new Date();
                    var checkHours = todayDate.getHours(),
                        hour;
                    //condition to set currentTime
                    if (checkHours < 10) {
                        hour = '0' + todayDate.getHours();
                    } else {
                        hour = todayDate.getHours();
                    }
                    var currentTime = hour + ":" + todayDate.getMinutes() + ":" + todayDate.getSeconds();
                    var ValidThru = '';
                    var EffectiveDate = '';
                    var OrderTypeId = 0;
                    var CutOffTime = '';
                    var ETDDayPlus = '';
                    var ETDHour = '';
                    var LeadTimeArrival = '';
                    var ETDTemp = '',
                        ETD = '',
                        ETDFix = '';
                    var ETATemp = '',
                        ETATempHour = '',
                        ETAFix = '',
                        ETAETO = '';

                     OrderLeadTime.getData().then(
                         function(res) {

                               var getDataParameter = res.data.Result;
                               
                               console.log('OrderLeadTime ==>', getDataParameter)
                               var indexOfValidThru = 0;
                               ValidThru = new Date(getDataParameter[0].ValidThru);
                               EffectiveDate = new Date(getDataParameter[0].EffectiveDate);
                               console.log("getDataParameter: ", getDataParameter);
                               console.log("currentTime: ", currentTime);
                               console.log("todayDate: ", todayDate);
                               console.log('initype', $scope.mData.POTypeId);

                               if (ValidThru >= todayDate && EffectiveDate <= todayDate) {
                                   for(var i in getDataParameter){
                                       console.log(i, " lll ", getDataParameter[i].OrderTypeId);
                                       if (getDataParameter[i].OrderTypeId == 3 && $scope.mData.POTypeId == 3) {
                                           // if (getDataParameter[i].OrderTypeId==1||getDataParameter[i].OrderTypeId==2||getDataParameter[i].OrderTypeId==3){

                                           //not today ETD
                                           if (currentTime >= getDataParameter[i].CutOffTime) {
                                               ETDTemp = new Date(todayDate);
                                               ETDTemp.setDate(todayDate.getDate() + 1 + getDataParameter[i].ETDDayPlus);
                                               console.log("b");
                                           } else {
                                               ETDTemp = todayDate;
                                               console.log("test hari");
                                           }
                                           ETD = $scope.changeFormatDate(ETDTemp);
                                           var ETDHour = getDataParameter[i].ETDHour;
                                           if (ETDHour>24){
                                               ETDHour = ETDHour-24;
                                               if(ETDHour<10){
                                                   ETDHour='0'+ETDHour;
                                               }
                                           }
                                           else{
                                               ETDHour=ETDHour;
                                           }
                                           getDataParameter[i].ETDHour = ETDHour;
                                           ETDFix = ETD + ' ' + getDataParameter[i].ETDHour;
                                           var ETAtanggal = new Date(ETDFix);
                                           ETATempHour = ETAtanggal.getHours();
                                           ETATemp = ETATempHour + getDataParameter[i].LeadTimeArrival;
                                           if (ETATemp>24){
                                               ETATemp = ETATemp-24;
                                             if(ETATemp<10){
                                               ETATemp='0'+ETATemp;
                                             }
                                             else{
                                               ETATemp=ETATemp;
                                             }
                                           }
                                           var ETAminit = ETAtanggal.getMinutes();
                                           if (ETAminit < 10) {
                                               ETAminit = '0' + ETAminit;
                                           } else {
                                               ETAminit = ETAminit;
                                           }
                                           var ETAdetik = ETAtanggal.getSeconds();
                                           if (ETAdetik < 10) {
                                               ETAdetik = '0' + ETAdetik;
                                           } else {
                                               ETAdetik = ETAdetik;
                                           }
                                           ETAFix = ETD + ' ' + ETATemp + ':' + ETAminit + ':' + ETAdetik;

                                           console.log(i, "ETAFixq", ETAFix);
                                           console.log(i, "CutOffTime", getDataParameter[i].CutOffTime);
                                           console.log(i, "ETDTemp", ETDTemp);
                                           console.log(i, "ETD", ETD + getDataParameter[i].ETDHour);
                                           console.log(i, "ETDDayPlus", getDataParameter[i].ETDDayPlus);
                                           break;
                                       } else {
                                           console.log("data percobaan 3");
                                       }
                                   }

                               } else {
                                   console.log("not valid");
                               }

                               $scope.mData.TotalAmount = 0;
                               for(var a in $scope.non_tam_sop_so.data){
                                   for(var b in dataEst){
                                       if($scope.non_tam_sop_so.data[a].PartId == dataEst[b].PartId){
                                           $scope.non_tam_sop_so.data[a].ETD = ETDFix;
                                           $scope.non_tam_sop_so.data[a].ETA = ETAFix;
                                           $scope.non_tam_sop_so.data[a].QtyPO = $scope.non_tam_sop_so.data[a].QtyEstimationBP;
                                           $scope.non_tam_sop_so.data[a].Name = $scope.non_tam_sop_so.data[a].Satuan;
                                            //$scope.non_tam_sop_so.data[a].UnitPrice = $scope.non_tam_sop_so.data[a].AmountAestimationBP;
                                            //$scope.non_tam_sop_so.data[a].TotalAmount = $scope.non_tam_sop_so.data[a].AmountAestimationBP * $scope.non_tam_sop_so.data[a].QtyEstimationBP;
                                           $scope.non_tam_sop_so.data[a].UnitPrice = $scope.non_tam_sop_so.data[a].StandardCost;
                                        //    if($scope.non_tam_sop_so.data[a].AmountAestimationBP == undefined){

                                        //    }
                                            if($scope.non_tam_sop_so.data[a].AmountAestimationBP == undefined){
                                                // $scope.non_tam_sop_so.data[a].VATAmount = Math.round(($scope.non_tam_sop_so.data[a].StandardCost * $scope.non_tam_sop_so.data[a].QtyEstimationBP)*0.1);
                                                $scope.non_tam_sop_so.data[a].VATAmount = Math.round(($scope.non_tam_sop_so.data[a].StandardCost * $scope.non_tam_sop_so.data[a].QtyEstimationBP)*(PPNPerc/100));

                                            }else{
                                                var HargaSatuan    = $scope.non_tam_sop_so.data[a].StandardCost;
						                        // :1.1 di hapus karna sudah di bagi di BE oleh rade;
						                        var DiscountSatuan = ($scope.non_tam_sop_so.data[a].DiscountPercent / 100) * HargaSatuan;
                                                var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
                                                var tmpVataAmount = $scope.non_tam_sop_so.data[a].QtyEstimationBP * VATSatuan;
                                                console.log("tmpVataAmount",tmpVataAmount);

                                                // $scope.non_tam_sop_so.data[a].VATAmount = Math.round((($scope.non_tam_sop_so.data[a].AmountAestimationBP/1.1) * $scope.non_tam_sop_so.data[a].QtyEstimationBP)*0.1);
                                                $scope.non_tam_sop_so.data[a].VATAmount = Math.round((($scope.non_tam_sop_so.data[a].AmountAestimationBP/(1+(PPNPerc/100))) * $scope.non_tam_sop_so.data[a].QtyEstimationBP)*(PPNPerc/100));


                                                // $scope.non_tam_sop_so.data[a].VATAmount = Math.floor((($scope.non_tam_sop_so.data[a].AmountAestimationBP/1.1) * $scope.non_tam_sop_so.data[a].QtyEstimationBP)*0.1);
                                            }
                                           
                                           console.log("eky"); 
                                           if($scope.tmpPO == 5){
                                            console.log("Math.round($scope.non_tam_sop_so.data[a].StandardCost * $scope.non_tam_sop_so.data[a].QtyEstimationBP)",Math.round($scope.non_tam_sop_so.data[a].StandardCost * $scope.non_tam_sop_so.data[a].QtyEstimationBP));

                                            // $scope.non_tam_sop_so.data[a].TotalAmount = Math.round(($scope.non_tam_sop_so.data[a].AmountAestimationBP/1.1)* $scope.non_tam_sop_so.data[a].QtyEstimationBP)  + $scope.non_tam_sop_so.data[a].VATAmount;
                                            $scope.non_tam_sop_so.data[a].TotalAmount = Math.round(($scope.non_tam_sop_so.data[a].AmountAestimationBP/(1+(PPNPerc/100)))* $scope.non_tam_sop_so.data[a].QtyEstimationBP)  + $scope.non_tam_sop_so.data[a].VATAmount;

                                           }else{
                                            $scope.non_tam_sop_so.data[a].TotalAmount = ($scope.non_tam_sop_so.data[a].StandardCost * $scope.non_tam_sop_so.data[a].QtyEstimationBP)  + $scope.non_tam_sop_so.data[a].VATAmount;
                                           }
                                           $scope.non_tam_sop_so.data[a].UomPOId = $scope.non_tam_sop_so.data[a].UomId;
                                        }
                                    }
                                    $scope.mData.TotalAmount += $scope.non_tam_sop_so.data[a].TotalAmount;
                               }
                               $scope.checkRefGINo = false;
                               PurchaseOrder.GridData = $scope.non_tam_sop_so.data;
                     });


                    // $scope.mData.TotalAmount = 0;
                    // for(var a in $scope.non_tam_sop_so.data){
                    //     $scope.mData.TotalAmount += $scope.non_tam_sop_so.data[a].TotalAmount;
                    // }


                    console.log("$scope.non_tam_sop_so.data =>",$scope.non_tam_sop_so.data);
                    console.log("$scope.mData ===>",$scope.mData);
                    console.log("$scope.mData.TotalAmount ===>",$scope.mData.TotalAmount);
                });
            }
        }

        $scope.GetDateTime = function(date, param) {
            if (param == 1) {
                //console.log("ETD ", $scope.DateTimeFilter(date));
                try {
                    PurchaseOrder.GridDetail[PurchaseOrder.getETD()].ETD = $scope.DateTimeFilter(date);
                    $scope.non_tam_sop_wo.data = PurchaseOrder.GridDetail;
                } catch (err) {
                    if ((PurchaseOrder.crudState == '1441') || ($scope.crudState === '114') || ($scope.crudState === '124') || ($scope.crudState === '134')) {
                        console.log("Panjang grid ", PurchaseOrder.gridDetailLength);
                        $scope.non_tam_non_sop_bengkel.data = PurchaseOrder.GridData;
                        console.log("isi grid ", $scope.non_tam_non_sop_bengkel.data);
                        $scope.non_tam_non_sop_bengkel.data[PurchaseOrder.gridDetailLength - 1].ETD = $scope.DateTimeFilter(date);
                        console.log(date);
                    } else if (PurchaseOrder.crudState == '1442') {
                        console.log("Panjang grid ", PurchaseOrder.gridDetailLength);
                        $scope.non_tam_non_sop.data = PurchaseOrder.GridData;
                        console.log("isi grid ", $scope.non_tam_non_sop.data);
                        $scope.non_tam_non_sop.data[PurchaseOrder.gridDetailLength - 1].ETD = $scope.DateTimeFilter(date);
                        console.log(date);
                    } else {
                        //SO
                        $scope.non_tam_sop_so.data = PurchaseOrder.GridData;
                        $scope.non_tam_sop_so.data[PurchaseOrder.gridDetailLength - 1].ETD = $scope.DateTimeFilter(date);
                        console.log(date);

                        //
                    }

                }

            } else if (param == 2) {
                try {
                    PurchaseOrder.GridDetail[PurchaseOrder.getETA()].ETA = $scope.DateTimeFilter(date);
                    $scope.non_tam_sop_wo.data = PurchaseOrder.GridDetail;
                } catch (err) {
                    if ((PurchaseOrder.crudState == '1441') || ($scope.crudState === '114') || ($scope.crudState === '124') || ($scope.crudState === '134')) {
                        console.log("Panjang grid ", PurchaseOrder.gridDetailLength);
                        $scope.non_tam_non_sop_bengkel.data = PurchaseOrder.GridData;
                        console.log("isi grid ", $scope.non_tam_non_sop_bengkel.data);
                        $scope.non_tam_non_sop_bengkel.data[PurchaseOrder.gridDetailLength - 1].ETA = $scope.DateTimeFilter(date);
                        console.log(date);
                    } else if (PurchaseOrder.crudState == '1442') {
                        console.log("Panjang grid ", PurchaseOrder.gridDetailLength);
                        $scope.non_tam_non_sop.data = PurchaseOrder.GridData;
                        console.log("isi grid ", $scope.non_tam_non_sop.data);
                        $scope.non_tam_non_sop.data[PurchaseOrder.gridDetailLength - 1].ETA = $scope.DateTimeFilter(date);
                        console.log(date);
                    } else {
                        //SO
                        $scope.non_tam_sop_so.data = PurchaseOrder.GridData;
                        $scope.non_tam_sop_so.data[PurchaseOrder.gridDetailLength - 1].ETD = $scope.DateTimeFilter(date);
                        console.log(date);

                        //
                    }
                }

            }
            $scope.ngDialog.close();
        }



        //----------------------------------
        // Dummy tam_sop_appointment
        //----------------------------------
        $scope.tam_sop_appointment = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {

                    console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);

                    if ($scope.isEdit == true) {
                        rowEntity.LastModifiedDate = $scope.localeDate(new Date());
                        rowEntity.LastModifiedUserId = $scope.UserId;
                    }
                    // if(typeof row === undefined)
                    // {
                    //   rowEntity.DiscountPercent = 0;
                    //   rowEntity.DiscountAmount = 0;
                    // }


                    // // rowEntity.DiscountAmount = (rowEntity.DiscountPercent / 100) * (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // // rowEntity.VATAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) * 0.1;
                    // rowEntity.TotalAmount = (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // //console.log(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);
                    // //alert(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);

                    // this is new formula 07/10/2020 =================================
                    // rowEntity.VATAmount = Math.round(((rowEntity.QtyPO * rowEntity.UnitPrice)) * 0.1);
                    //Total  = (Qty * HargaDasar)  + VAT;
                    // rowEntity.TotalAmount = (rowEntity.QtyPO * rowEntity.UnitPrice) + rowEntity.VATAmount;

                    // imron 27okt2020
                    // var HargaSatuan = rowEntity.StandardCost / (1.1);
                    var HargaSatuan = rowEntity.StandardCost / (1+(PPNPerc/100));

                    var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
                    rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo);

                    // var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
                    var VATSatuan = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);

                    rowEntity.VATAmount = rowEntity.QtyPO * VATSatuan.toFixed($scope.roundTo);
                    rowEntity.TotalAmount =Math.round((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount + rowEntity.VATAmount);

                    console.log('DiscountAmount App', rowEntity.DiscountAmount);
                    console.log('VATAmount App', rowEntity.VATAmount);
                    console.log('TotalAmount App', rowEntity.TotalAmount);

                    var vTotalAmount = rowEntity.TotalAmount;
                    for (var i = 0; i < $scope.tam_sop_appointment.data.length; i++) {
                        vTotalAmount = vTotalAmount + $scope.tam_sop_appointment.data[i].TotalAmount;
                        $scope.mData.TotalAmount = vTotalAmount;
                    }


                });
            },
            enableSelectAll: true,
            columnDefs: [
                { name: 'No Material', field: 'PartsCode', enableCellEdit: false },
                { name: 'Nama Material', field: 'PartsName', sort: { direction: uiGridConstants.ASC, priority: 0 },enableCellEdit: false},
                 { displayName: 'ETD', name: 'ETD', field: 'ETD', cellFilter: 'date:\'yyyy-MM-dd\'',enableCellEdit: false },
                { displayName: 'ETA', name: 'ETA', field: 'ETA', cellFilter: 'date:\'yyyy-MM-dd\'',enableCellEdit: false },
                { name: 'Qty', field: 'QtyPO',enableCellEdit: false},
                { name: 'Satuan', field: 'Name',enableCellEdit: false},
                // { name: 'Unit Price App', field: 'UnitPrice', cellFilter: 'number', enableCellEdit: false},
                // { name: 'VAT Amount App', field: 'VATAmount', cellFilter: 'number', enableCellEdit: false},
                // { name: 'Disc Amount App', field: 'DiscountAmount', cellFilter: 'number', enableCellEdit: false},
                { name: 'Harga Landed Cost', field: 'StandardCost', cellFilter: 'number', enableCellEdit: false},
                { name: 'PartsClassId4', field: 'PartsClassId4', visible: false },
                {
                    name: 'Harga Total',
                    field: 'TotalAmount',
                    cellFilter: 'number',
                    enableCellEdit: false
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.TotalAmount(row.entity.QtyPO, row.entity.UnitPrice, row)}}</div>',
                },
                { 
                    name: 'Status TPOS', 
                    field: 'StatusTPOS', 
                    displayName: 'Status TPOS', 
                    cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.StatusTPOS">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    enableCellEdit: false,
            
                },
                { name: 'Alasan Reject', field: 'ReasonTPOS',enableCellEdit: false },
                {
                    name: 'Action',
                    cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>',
                    visible: true
                }
            ]
        };

        $scope.tam_sop_appointment_lihat = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {

                    console.log('rowEntity sini dib', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);
                    
                    if ($scope.isEdit == true) {
                        rowEntity.LastModifiedDate = $scope.localeDate(new Date());
                        rowEntity.LastModifiedUserId = $scope.UserId;
                    }
                
                    // if(typeof row === undefined)
                    // {
                    //   rowEntity.DiscountPercent = 0;
                    //   rowEntity.DiscountAmount = 0;
                    // }


                    // rowEntity.DiscountAmount = (rowEntity.DiscountPercent / 100) * (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // rowEntity.VATAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) * 0.1;
                    // rowEntity.TotalAmount = (rowEntity.QtyPO * rowEntity.UnitPrice);
                    //console.log(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);
                    //alert(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);

                    // imron 27okt2020
                    // var HargaSatuan = rowEntity.StandardCost / (1.1);
                    var HargaSatuan = rowEntity.StandardCost / (1+(PPNPerc/100));

                    var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
                    rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo);

                    // var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
                    var VATSatuan = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);

                    rowEntity.VATAmount = rowEntity.QtyPO * VATSatuan.toFixed($scope.roundTo);
                    rowEntity.TotalAmount =Math.round((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount + rowEntity.VATAmount);

                    var vTotalAmount = rowEntity.TotalAmount;
                    for (var i = 0; i < $scope.tam_sop_appointment_lihat.data.length; i++) {
                        vTotalAmount = vTotalAmount + $scope.tam_sop_appointment_lihat.data[i].TotalAmount;
                        $scope.mData.TotalAmount = vTotalAmount;
                    }


                });
            },
            enableSelectAll: true,
            columnDefs: [
                { name: 'No Material', field: 'PartsCode' },
                { name: 'Nama Material', field: 'PartsName', sort: { direction: uiGridConstants.ASC, priority: 0 } },
                { displayName: 'ETD', name: 'ETD', field: 'ETD', cellFilter: 'date:\'yyyy-MM-dd\'',enableCellEdit: false },
                { displayName: 'ETA', name: 'ETA', field: 'ETA', cellFilter: 'date:\'yyyy-MM-dd\'',enableCellEdit: false },
                { name: 'Qty', field: 'QtyPO', cellEditableCondition: function(scope){
                    if($scope.stateModeView){
                        return false;
                    }else{
                        return true
                    }
                },  editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">'},
                { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', enableCellEdit: false, editableCellTemplate: '<input type="number" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">',enableCellEdit: false },
                { name: 'Satuan', field: 'Name' },
                // { name: 'Unit Price AppL', field: 'UnitPrice', cellFilter: 'number', enableCellEdit: false},
                // { name: 'VAT Amount AppL', field: 'VATAmount', cellFilter: 'number', enableCellEdit: false},
                { name: 'Harga Landed Cost', field: 'StandardCost', cellFilter: 'number' },
                { name: 'PartsClassId4', field: 'PartsClassId4', visible: false },
                {
                    name: 'Harga Total',
                    field: 'TotalAmount',
                    cellFilter: 'number'
                        // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.TotalAmount(row.entity.QtyPO, row.entity.UnitPrice, row)}}</div>',
                },
                { 
                    name: 'Status TPOS', 
                    field: 'StatusTPOS', 
                    displayName: 'Status TPOS', 
                    cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.StatusTPOS">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    enableCellEdit: false,
            
                },
                { name: 'Alasan Reject', field: 'ReasonTPOS',enableCellEdit: false}
            ]
        };

        $scope.tam_sop_appointment_ubah = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {

                    console.log('rowEntity sokin', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);

                    if ($scope.isEdit == true) {
                        rowEntity.LastModifiedDate = $scope.localeDate(new Date());
                        rowEntity.LastModifiedUserId = $scope.UserId;
                    }

                    // if(typeof row === undefined)
                    // {
                    //   rowEntity.DiscountPercent = 0;
                    //   rowEntity.DiscountAmount = 0;
                    // }


                    // rowEntity.DiscountAmount = (rowEntity.DiscountPercent / 100) * (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // rowEntity.VATAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) * 0.1;
                    // rowEntity.TotalAmount = (rowEntity.QtyPO * rowEntity.UnitPrice);
                    //console.log(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);
                    //alert(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);

                    // imron 27okt2020
                    // var HargaSatuan = rowEntity.StandardCost / (1.1);
                    var HargaSatuan = rowEntity.StandardCost / (1+(PPNPerc/100));

                    var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
                    rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo);

                    // var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
                    var VATSatuan = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);

                    rowEntity.VATAmount = rowEntity.QtyPO * VATSatuan.toFixed($scope.roundTo);
                    rowEntity.TotalAmount =Math.round((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount + rowEntity.VATAmount);

                    var vTotalAmount = rowEntity.TotalAmount;
                    for (var i = 0; i < $scope.tam_sop_appointment_ubah.data.length; i++) {
                        vTotalAmount = vTotalAmount + $scope.tam_sop_appointment_ubah.data[i].TotalAmount;
                        $scope.mData.TotalAmount = vTotalAmount;
                    }


                });
            },
            enableSelectAll: true,
            cellEditableCondition: function($scope) {
                console.log('QtyGR ==> ',$scope.row.entity.QtyGR);
                return ($scope.row.entity.QtyGR == 0 || $scope.row.entity.QtyGR == null);
            },
            columnDefs: [{ field: 'PartsCode',
                displayName: 'No. Material',
                // enableCellEdit:true,
                cellEditableCondition: function(scope){
                    if($scope.stateModeView){
                        return false;
                    }else{
                        return true
                    }
                },
                //field: 'NoMaterial',
                width: '11%',
                cellTemplate: '<div><div class="input-group">\
                  <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.PartsCode" style="height:30px;" ng-disabled="grid.appScope.stateModeView" required>\
                  <label class="input-group-btn" ng-hide="grid.appScope.stateModeView">\
                      <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
                          <i class="fa fa-search fa-1"></i>\
                      </span>\
                  </label>\
                  </div></div>' },
                {name: 'Nama Material', sort: { direction: uiGridConstants.ASC, priority: 0 }, width: '15%', field:"PartsName", cellTemplate: '<div class="ui-grid-cell-contents"><span>{{row.entity.PartsName}}</span></div>', enableCellEdit: false },
                { displayName: 'ETD', name: 'ETD', field: 'ETD', cellFilter: 'date:\'yyyy-MM-dd\'' },
                { displayName: 'ETA', name: 'ETA', field: 'ETA', cellFilter: 'date:\'yyyy-MM-dd\''},
                { name: 'Qty PO', field: 'QtyPO', displayName: 'Qty PO', cellEditableCondition: function(scope){
                    if($scope.stateModeView){
                        return false;
                    }else{
                        return true
                    }
                }, editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">' },
                { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', enableCellEdit: false, editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">' },
                { name: 'Satuan', field: 'Name', enableCellEdit: false },
                // { name: 'Unit Price AppU', field: 'UnitPrice', cellFilter: 'number', enableCellEdit: false},
                // { name: 'VAT Amount AppU', field: 'VATAmount', cellFilter: 'number', enableCellEdit: false},
                { name: 'Harga Landed Cost', field: 'StandardCost', cellFilter: 'number',enableCellEdit: false },
                { name: 'PartsClassId4', field: 'PartsClassId4', visible: false },
                {
                    name: 'Harga Total',
                    field: 'TotalAmount',
                    cellFilter: 'number',
                    enableCellEdit: false
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.TotalAmount(row.entity.QtyPO, row.entity.UnitPrice, row)}}</div>',
                },
                { 
                    name: 'Status TPOS', 
                    field: 'StatusTPOS', 
                    displayName: 'Status TPOS', 
                    cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.StatusTPOS">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    enableCellEdit: false,
            
                },
                { name: 'Alasan Reject', field: 'ReasonTPOS', enableCellEdit: false},
                {
                    name: 'Action',
                    cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>',
                    visible: true
                }
            ]
        };


        //----------------------------------
        // Dummy non_tam_sop_wo
        //----------------------------------

        $scope.non_tam_sop_wo = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableFiltering: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
            },
            //selectedItems: console.log($scope.mySelections),
            enableSelectAll: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 5,
            rowHeight: 40
        };

        $scope.non_tam_sop_wo.columnDefs = [{
                field: 'PartsCode',
                displayName: 'No. Material',
                cellEditableCondition: function(scope){
                    if($scope.stateModeView){
                        return false;
                    }else{
                        return true;
                    }
                },
                //field: 'NoMaterial',
                width: '11%',
                cellTemplate: '<div><div class="input-group">\
                      <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.PartsCode" style="height:30px;" ng-disabled="grid.appScope.stateModeView" required>\
                      <label class="input-group-btn" ng-hide="grid.appScope.stateModeView">\
                          <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
                              <i class="fa fa-search fa-1"></i>\
                          </span>\
                      </label>\
                  </div></div>'
            },
            { name: 'Nama Material', width: '10%', cellTemplate: '<span>{{row.entity.PartsName}}</span>', sort: { priority: 0 }},
            {
                displayName: 'ETD',
                name: 'ETD',
                field: 'ETD',
                width: '10%',
                cellFilter: 'date:\'yyyy-MM-dd\'',
                enableCellEdit: false,
                cellTemplate: '<div><div class="input-group">\
                      <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.ETD" disabled>\
                      \
                  </div></div>'
            },
            {
                displayName: 'ETA',
                name: 'ETA',
                field: 'ETA',
                width: '10%',
                cellFilter: 'date:\"dd-MM-yyyy\"',
                enableCellEdit: false,
                cellTemplate: '<div><div class="input-group">\
                      <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.ETA" disabled>\
                      \
                  </div></div>'
            },
            { name: 'Qty', field: 'QtyPO',  editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">' },
            { name: 'Satuan', field: 'Name' },
            { name: 'Standart Cost', field: 'UnitPrice', cellFilter: 'number', enableCellEdit: false },
            { name: 'PartsClassId4', field: 'PartsClassId4', visible: false },
            {
                name: 'Diskon %',
                displayName: '%',
                field: 'DiscountPercent',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:202%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon %</td></tr></tbody></table></div></div>"
            },
            {
                name: 'Diskon Jumlah',
                displayName: 'Jumlah',
                field: 'DiscountAmount',
				cellFilter: 'number',
                headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>",
    
                cellFilter: 'number'
                //cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.DiscountAmountResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
            },
            {
                displayName: 'PPN',
                name: 'PPN',
                field: 'VATAmount',
                cellFilter: 'number'
                //cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.PPNResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
            },
            {
                name: 'Harga Total',
                field: 'TotalAmount',
                cellFilter: 'number',
                //cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.NONTAMTotalAmount(row.entity.QtyPO, row.entity.UnitPrice, row)}}</div>',
            },
            {
                name: 'Action',
                cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>',
                visible: true
            },
            { name: 'UomId', field: 'UomId', visible: false },
        ];



        //----------------------------------
        // Dummy tam_sop_wo
        //----------------------------------
        $scope.tam_sop_wo = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 5,
            enableSelectAll: true,
            columnDefs: [
                { name: 'No Material', field: 'no_material', enableCellEdit: false },
                { name: 'Nama Material', field: 'PartsName', enableCellEdit: false },
                { displayName: 'ETD', name: 'ETD', field: 'etd', cellFilter: 'date:\'yyyy-MM-dd\'', enableCellEdit: false },
                { displayName: 'ETA', name: 'ETA', field: 'eta', cellFilter: 'date:\'yyyy-MM-dd\'', enableCellEdit: false },
                { name: 'Qty', field: 'qty' },
                { name: 'Satuan', field: 'Name', enableCellEdit: false },
                { name: 'Harga Landed Cost', field: 'StandardCost', enableCellEdit: false },
                { name: 'PartsClassId4', field: 'PartsClassId4', enableCellEdit: false, visible: false },
                { name: 'Harga Total', field: 'harga_total', enableCellEdit: false },
                { 
                    name: 'Status TPOS', 
                    field: 'StatusTPOS', 
                    displayName: 'Status TPOS', 
                    cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.StatusTPOS">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    enableCellEdit: false,
            
                },
                { name: 'Alasan Reject', field: 'ReasonTPOS',enableCellEdit: false},
                {
                    name: 'Action',
                    cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.showMe()"><u>Hapus</u></a></center>',
                    visible: true,
                    enableCellEdit: false
                }
            ]
        };

        //----------------------------------
        // Dummy tam_sop_wo_lihat
        //----------------------------------
        $scope.tam_sop_wo_ubah = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope,function(row) {
                    $scope.selectedRows = gridApi.selection.getSelectedRows();
                    // console.log("selected=>",$scope.selectedRows);
                    if($scope.onSelectRows){
                        $scope.onSelectRows($scope.selectedRows);
                    }
                });
                gridApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
                        $scope.selectedRows = gridApi.selection.getSelectedRows();
                        if($scope.onSelectRows){
                            $scope.onSelectRows($scope.selectedRows);
                        }
                });
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {

                    console.log('rowEntity sokin', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);

                    if ($scope.isEdit == true) {
                        rowEntity.LastModifiedDate = $scope.localeDate(new Date());
                        rowEntity.LastModifiedUserId = $scope.UserId;
                    }
        
                    // if(typeof row === undefined)
                    // {
                    //   rowEntity.DiscountPercent = 0;
                    //   rowEntity.DiscountAmount = 0;
                    // }


                    // rowEntity.DiscountAmount = (rowEntity.DiscountPercent / 100) * (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // rowEntity.VATAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) * 0.1;
                    // rowEntity.TotalAmount = (rowEntity.QtyPO * rowEntity.UnitPrice);
                    //console.log(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);
                    //alert(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);

                    // imron 27okt2020
                    // var HargaSatuan = rowEntity.StandardCost / (1.1);
                    var HargaSatuan = rowEntity.StandardCost / (1+(PPNPerc/100));

                    var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
                    rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo);

                    // var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
                    var VATSatuan = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);

                    rowEntity.VATAmount = rowEntity.QtyPO * VATSatuan.toFixed($scope.roundTo);
                    rowEntity.TotalAmount =Math.round((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount + rowEntity.VATAmount);

                    var vTotalAmount = rowEntity.TotalAmount;
                    for (var i = 0; i < $scope.tam_sop_wo_ubah.data.length; i++) {
                        vTotalAmount = vTotalAmount + $scope.tam_sop_wo_ubah.data[i].TotalAmount;
                        $scope.mData.TotalAmount = vTotalAmount;
                    }
                    // QTYPO Partial GI ===
                    if(colDef.field == 'QtyPO'){
                        if(rowEntity.QtyPO < rowEntity.QtyGR){
                            rowEntity.QtyPO = oldValue;
                            bsAlert.warning('Quantity po tidak boleh kurang dari quantity GR','Silahkan Cek kembali');
                        }
                    }
                });
            },
            enableSelectAll: true,
            cellEditableCondition: function($scope) {
                console.log('QtyGR ==> ',$scope.row.entity.QtyGR);
                return ($scope.row.entity.QtyGR == 0 || $scope.row.entity.QtyGR == null);
            },
            columnDefs: [{ field: 'PartsCode',
                displayName: 'No. Material',
                // enableCellEdit: true,
                cellEditableCondition: function(scope){
                        console.log(scope.row.entity.PurchaseOrderStatusId,'Di grid nyaaa');
                        // return scope.rowRenderIndex%2;
                        if(scope.row.entity.PurchaseOrderStatusId > 3 && scope.row.entity.QtyGR > 0){
                            return false;
                        }else if(scope.row.entity.PurchaseOrderStatusId > 3 && scope.row.entity.QtyGR == 0){
                            return true;
                        }else{
                            return true;
                        }
                    },
                width: '11%',
                cellTemplate: '<div>\
                        <div ng-show="row.entity.PurchaseOrderStatusId > 3 && row.entity.QtyGR > 0" class="ui-grid-cell-contents"><span>{{row.entity.PartsCode}}</span></div>\
                        <div class="input-group" ng-hide="row.entity.PurchaseOrderStatusId > 3 && row.entity.QtyGR > 0">\
                            <input type="text" style="height:30px;" class="form-control" ng-model="MODEL_COL_FIELD" ng-value="row.entity.PartsCode" required>\
                            <label class="input-group-btn">\
                                <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
                                    <i class="fa fa-search fa-1"></i>\
                                </span>\
                            </label>\
                        </div>\
                    </div>' 
                },
                {name: 'Nama Material', width: '15%', cellTemplate: '<div class="ui-grid-cell-contents"><span>{{row.entity.PartsName}}</span></div>',field:"PartsName", enableCellEdit: false,sort: { direction: uiGridConstants.ASC, priority: 0 } },
                { displayName: 'ETD', name: 'ETD', field: 'ETD', enableCellEdit: false, cellFilter: 'date:\'yyyy-MM-dd\'', enableCellEdit: false, },
                { displayName: 'ETA', name: 'ETA', field: 'ETA', enableCellEdit: false, cellFilter: 'date:\'yyyy-MM-dd\'', enableCellEdit: false, },
                { name: 'Qty PO', field: 'QtyPO', displayName: 'Qty PO',
                    cellEditableCondition: function(scope){
                        // return scope.rowRenderIndex%2;
                        if($scope.stateModeView){
                           return false; 
                        }else{
                            if (scope.row.entity.PurchaseOrderStatusId <= 3 ) {
                                return true;
                            } else if(scope.row.entity.PurchaseOrderStatusId == 4 && scope.row.entity.QtyGR <=  scope.row.entity.QtyPO ){
                                return true;
                            } else if(scope.row.entity.PurchaseOrderStatusId === undefined){
                                return true
                            } else {
                                return false;
                            }
                        }
                    },
                    editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">'},
                { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', enableCellEdit: false, editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">',enableCellEdit: false },
                { name: 'Satuan', field: 'Name', enableCellEdit: false },
                // { name: 'Unit Price WOu', field: 'UnitPrice', enableCellEdit: false, cellFilter: 'number' },
                // { name: 'VAT Amount WOu', field: 'VATAmount', enableCellEdit: false, cellFilter: 'number' },
                { name: 'Harga Landed Cost', field: 'StandardCost', enableCellEdit: false, cellFilter: 'number' },
                { name: 'PartsClassId4', field: 'PartsClassId4', enableCellEdit: false, visible: false },
                { 
                    name: 'Status TPOS', 
                    field: 'StatusTPOS', 
                    displayName: 'Status TPOS', 
                    cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.StatusTPOS">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    enableCellEdit: false,
            
                },
                { name: 'Alasan Reject', field: 'ReasonTPOS',enableCellEdit: false},
                { name: 'Harga Total', field: 'TotalAmount', enableCellEdit: false, cellFilter: 'number' },
                {
                    name: 'Action',
                    cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>',
                    visible: false
                }
            ]
        };
        $scope.tam_sop_wo_lihat = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 5,
            enableSelectAll: true,
            columnDefs: [
                { name: 'No Material', field: 'PartsCode', enableCellEdit: false },
                { name: 'Nama Material', field: 'PartsName', enableCellEdit: false, sort: { priority: 0 } },
                { displayName: 'ETD', name: 'ETD', field: 'ETD', enableCellEdit: false, cellFilter: 'date:\'yyyy-MM-dd\'', enableCellEdit: false, },
                { displayName: 'ETA', name: 'ETA', field: 'ETA', enableCellEdit: false, cellFilter: 'date:\'yyyy-MM-dd\'', enableCellEdit: false, },
                { name: 'Qty', field: 'QtyPO', enableCellEdit:false,  editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">' },
                { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', enableCellEdit: false, editableCellTemplate: '<input type="number" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">',enableCellEdit: false },
                { name: 'Satuan', field: 'Name', enableCellEdit: false },
                // { name: 'Unit Price WOl', field: 'UnitPrice', enableCellEdit: false, cellFilter: 'number' },
                // { name: 'VAT Amount WOl', field: 'VATAmount', enableCellEdit: false, cellFilter: 'number' },
                { name: 'Harga Landed Cost', field: 'StandardCost', enableCellEdit: false, cellFilter: 'number' },
                { name: 'PartsClassId4', field: 'PartsClassId4', enableCellEdit: false, visible: false },
                { name: 'Harga Total', field: 'TotalAmount', enableCellEdit: false, cellFilter: 'number' },
                { 
                    name: 'Status TPOS', 
                    field: 'StatusTPOS', 
                    displayName: 'Status TPOS', 
                    cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.StatusTPOS">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    enableCellEdit: false,
            
                },
                { name: 'Alasan Reject', field: 'ReasonTPOS',enableCellEdit: false}
            ]
        };

        $scope.tam_soq_lihat = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 5,
            enableSelectAll: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {

                    console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);

                    if ($scope.isEdit == true) {
                        rowEntity.LastModifiedDate = $scope.localeDate(new Date());
                        rowEntity.LastModifiedUserId = $scope.UserId;
                    }

                    // if(typeof row === undefined)
                    // {
                    //   rowEntity.DiscountPercent = 0;
                    //   rowEntity.DiscountAmount = 0;
                    // }


                    // rowEntity.DiscountAmount = (rowEntity.DiscountPercent / 100) * (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // rowEntity.VATAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) * 0.1;
                    rowEntity.TotalAmount = (rowEntity.QtyPO * rowEntity.UnitPrice);
                    //console.log(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);
                    //alert(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);

                    var vTotalAmount = rowEntity.TotalAmount;
                    for (var i = 0; i < $scope.non_tam_sop_so.data.length; i++) {
                        vTotalAmount = vTotalAmount + $scope.non_tam_sop_so.data[i].TotalAmount;
                        $scope.mData.TotalAmount = vTotalAmount;
                    }


                });
            },
            columnDefs: [
                { name: 'No Material', field: 'PartsCode', enableCellEdit: false },
                { name: 'Nama Material', field: 'PartsName', enableCellEdit: false, sort: { priority: 0 } },
                { displayName: 'ETD', name: 'ETD', field: 'ETD', enableCellEdit: false, cellFilter: 'date:\'yyyy-MM-dd\'', enableCellEdit: false, },
                { displayName: 'ETA', name: 'ETA', field: 'ETA', enableCellEdit: false, cellFilter: 'date:\'yyyy-MM-dd\'', enableCellEdit: false, },
                { name: 'Qty', field: 'QtyPO', enableCellEdit:false,  editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">'},
                { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', enableCellEdit: false, editableCellTemplate: '<input type="number" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">',enableCellEdit: false },
                { name: 'Satuan', field: 'Name', enableCellEdit: false },
                { name: 'Standart Cost', field: 'UnitPrice', enableCellEdit: false, cellFilter: 'number' },
                { name: 'Harga Total', field: 'TotalAmount', enableCellEdit: false, cellFilter: 'number' }
            ]
        };

        //----------------------------------
        // Dummy tam_soq
        //----------------------------------
        $scope.tam_soq = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            columnDefs: [
                { name: 'No Material', field: 'no_material', enableCellEdit: false },
                { name: 'Nama Material', field: 'nama_material', enableCellEdit: false, sort: { priority: 0 } },
                { displayName: 'ETD', name: 'ETD', field: 'etd', enableCellEdit: false, enableCellEdit: false },
                { displayName: 'ETA', name: 'ETA', field: 'eta', enableCellEdit: false, enableCellEdit: false },
                { name: 'Qty', field: 'qty' },
                { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', enableCellEdit: false, cellFilter: 'number' },
                { name: 'Satuan', field: 'satuan', enableCellEdit: false },
                { name: 'Standart Cost', field: 'harga_satuan', enableCellEdit: false },
                { name: 'Harga Total', field: 'harga_total', enableCellEdit: false },
                {
                    name: 'Action',
                    cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.showMe()"><u>Hapus</u></a></center>',
                    visible: true,
                    enableCellEdit: false
                }
            ],
            data: [{
                    "no_material": "35677-33996",
                    "nama_material": "EXPANDER",
                    "etd": "3",
                    "eta": "3",
                    "qty": "3",
                    "satuan": "Pieces",
                    "harga_satuan": "1000",
                    "harga_total": "450.000"
                },
                {
                    "no_material": "35677-33296",
                    "nama_material": "ABSORBER",
                    "etd": "3",
                    "eta": "3",
                    "qty": "3",
                    "satuan": "Pieces",
                    "harga_satuan": "1000",
                    "harga_total": "450.000"
                }
            ]
        };


        //----------------------------------
        // Dummy po_sublet_sop_wo
        //----------------------------------
        $scope.sublet_sop_wo = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 5,
            enableSelectAll: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                $scope.edit = 0;
                gridApi.selection.on.rowSelectionChanged($scope,function(row) {
                    $scope.selectedRows = gridApi.selection.getSelectedRows();
                    // console.log("selected=>",$scope.selectedRows);
                    if($scope.onSelectRows){
                        $scope.onSelectRows($scope.selectedRows);
                    }
                });
                gridApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
                        $scope.selectedRows = gridApi.selection.getSelectedRows();
                        if($scope.onSelectRows){
                            $scope.onSelectRows($scope.selectedRows);
                        }
                });
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {

                    console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);
                    console.log('$scope.mData.GridDetai', $scope.mData.GridDetail);

                    if ($scope.isEdit == true) {
                        rowEntity.LastModifiedDate = $scope.localeDate(new Date());
                        rowEntity.LastModifiedUserId = $scope.UserId;
                        console.log("rowEntity.LastModifiedDate1=>",rowEntity.LastModifiedDate);
                    }

                    if (colDef.field == 'QtyPO') {
                        if ((rowEntity.QtyPO > rowEntity.QtyBase) || (rowEntity.QtyPO <= 0)) {
                            rowEntity.QtyPO = angular.copy(rowEntity.QtyBase);
                        }
                        console.log('rowEntity QtyPO', rowEntity);
                    }
                
                    if (colDef.field == 'UnitPrice') {
                        rowEntity.newUnitPrice = angular.copy(newValue);
                        console.log('rowEntity', rowEntity);
                    }
                
                    // if (typeof row === undefined) {
                    //     rowEntity.DiscountPercent = rowEntity.DiscountPercent;
                    //     rowEntity.DiscountAmount = rowEntity.DiscountAmount;
                    // }
                    // rowEntity.DiscountAmount = Math.round((rowEntity.DiscountPercent / 100)) * (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // if(rowEntity.xUnitPrice == undefined || rowEntity.xUnitPrice == null){
                    //     rowEntity.VATAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) * 0.1;
                    // }else{
                    //     rowEntity.VATAmount =((rowEntity.QtyPO * rowEntity.xUnitPrice) - rowEntity.DiscountAmount) - ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount);
                    // }
                    // rowEntity.TotalAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) + rowEntity.VATAmount;


                    // // rowEntity.DiscountAmount = (rowEntity.DiscountPercent / 100) * (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // // rowEntity.VATAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) * 0.1;
                    // // rowEntity.TotalAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) + rowEntity.VATAmount;
                    // // =====
                    // // rowEntity.DiscountAmount = Math.round((rowEntity.DiscountPercent / 100)) * (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // // rowEntity.VATAmount =((rowEntity.QtyPO * rowEntity.xUnitPrice) - rowEntity.DiscountAmount) - ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount);
                    // // rowEntity.TotalAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) + rowEntity.VATAmount;
                    // console.log('ROWNYA',rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);
                    
                    // this is new formula 07/10/2020 =================================
                    if (typeof row === undefined) {
                        rowEntity.DiscountPercent = rowEntity.DiscountPercent;
                        rowEntity.DiscountAmount = rowEntity.DiscountAmount;
                    }
                    //rowEntity.DiscountAmount = Math.round(((rowEntity.DiscountPercent / 100)) * (rowEntity.QtyPO * rowEntity.UnitPrice));
					// CR5 #10
					console.log('$scope.mData.IsNONPKP at $scope.sublet_sop_wo stv: ', $scope.mData.IsNONPKP);
					if($scope.tmpPO == 5){
                        var VATSatuanSV3;

                        // var HargaSatuanSV3 = rowEntity.StandardCost/1.1;
                        var HargaSatuanSV3 = rowEntity.StandardCost/ (1+(PPNPerc/100));

                        var DiscountSatuanSV3 = (rowEntity.DiscountPercent / 100) * HargaSatuanSV3;
                        if(rowEntity.newUnitPrice == undefined){
                            // rowEntity.DiscountAmount =  Math.round((((rowEntity.StandardCost/1.1)) * rowEntity.QtyPO) * rowEntity.DiscountPercent /100);
                            rowEntity.DiscountAmount = (Math.round(HargaSatuanSV3*rowEntity.DiscountPercent / 100))*rowEntity.QtyPO;
                        }else{
                            rowEntity.DiscountAmount =  Math.round((rowEntity.newUnitPrice * rowEntity.QtyPO) * rowEntity.DiscountPercent /100);
                        }
                        
                        if($scope.mData.IsNONPKP == false){
                            //PKP
                            // VATSatuanSV3 =  Math.round((HargaSatuanSV3 - DiscountSatuanSV3) * 0.1);
                            VATSatuanSV3 =  Math.round((HargaSatuanSV3 - DiscountSatuanSV3) * (PPNPerc/100));

                            // rowEntity.VATAmount = Math.round(rowEntity.QtyPO * (rowEntity.StandardCost-(rowEntity.StandardCost/1.1)));
                            if(rowEntity.newUnitPrice == undefined){
                                // rowEntity.VATAmount = Math.round(((rowEntity.QtyPO * (rowEntity.StandardCost/1.1))-rowEntity.DiscountAmount)*0.1);

                                // rowEntity.VATAmount = (Math.round((HargaSatuanSV3 - rowEntity.DiscountAmount)*0.1))*rowEntity.QtyPO;
                                //rowEntity.VATAmount = (Math.round((HargaSatuanSV3 - rowEntity.DiscountAmount)*(PPNPerc/100)))*rowEntity.QtyPO;
                                rowEntity.VATAmount = (Math.round((HargaSatuanSV3 - (Math.round(HargaSatuanSV3*rowEntity.DiscountPercent / 100)))*(PPNPerc/100)))*rowEntity.QtyPO;
                            }else{
                                // rowEntity.VATAmount = Math.round(((rowEntity.QtyPO * rowEntity.newUnitPrice)-rowEntity.DiscountAmount)*0.1);
                                rowEntity.VATAmount = Math.round(((rowEntity.QtyPO * rowEntity.newUnitPrice)-rowEntity.DiscountAmount)*(PPNPerc/100));

                            }
                        }else{
                            VATSatuanSV3 = 0;
                            rowEntity.VATAmount = 0;
                        }
                        

                        console.log("VATSatuanSV3",VATSatuanSV3);
                    
                        
                        console.log("VATSatuanSV3.toFixed($scope.roundTo)",VATSatuanSV3.toFixed($scope.roundTo));
                        if($scope.mData.IsNONPKP == false){
                            if(rowEntity.newUnitPrice == undefined){
                                rowEntity.TotalAmount =  ((rowEntity.UnitPrice * rowEntity.QtyPO) - rowEntity.DiscountAmount)+rowEntity.VATAmount;
                                // rowEntity.TotalAmount = Math.round(((rowEntity.StandardCost/1.1)* rowEntity.QtyPO) - rowEntity.DiscountAmount + ( rowEntity.QtyPO * ((HargaSatuanSV3 - DiscountSatuanSV3) * 0.1)));
                                // rowEntity.TotalAmount = Math.round((rowEntity.QtyPO * (rowEntity.StandardCost/1.1)) - rowEntity.DiscountAmount + rowEntity.VATAmount);
                            }else{
                                rowEntity.TotalAmount = Math.round((rowEntity.QtyPO * rowEntity.newUnitPrice) - rowEntity.DiscountAmount + rowEntity.VATAmount);
                            }
                            
                           
                        }else{
                            console.log("(rowEntity.QtyPO * (rowEntity.StandardCost/1.1)",(rowEntity.QtyPO * (rowEntity.StandardCost/(1+(PPNPerc/100)))));
                            if(rowEntity.newUnitPrice == undefined){
                                rowEntity.TotalAmount =  ((rowEntity.UnitPrice * rowEntity.QtyPO) - rowEntity.DiscountAmount)+rowEntity.VATAmount;
                                // rowEntity.TotalAmount = Math.round(((rowEntity.StandardCost/1.1)* rowEntity.QtyPO) - (rowEntity.QtyPO *((rowEntity.DiscountPercent / 100) * HargaSatuanSV3)) + rowEntity.VATAmount);
                                // rowEntity.TotalAmount = Math.round((rowEntity.QtyPO * (rowEntity.StandardCost/1.1)) - rowEntity.DiscountAmount + VATSatuanSV3);
                            }else{
                                rowEntity.TotalAmount = Math.round((rowEntity.QtyPO * rowEntity.newUnitPrice) - rowEntity.DiscountAmount + VATSatuanSV3);
                            }
                        }
                    }else{
                        
                        if($scope.mData.IsNONPKP == false){
                            //PKP
                            rowEntity.UnitPrice = rowEntity.StandardCost;

                            // rowEntity.VATAmount = Math.round(((rowEntity.QtyPO * rowEntity.UnitPrice) - (rowEntity.DiscountAmount)) * 0.1);
                            rowEntity.VATAmount = Math.round(((rowEntity.QtyPO * rowEntity.UnitPrice) - (rowEntity.DiscountAmount)) * (PPNPerc/100));

                            rowEntity.DiscountAmount = Math.round(((rowEntity.DiscountPercent / 100)) * (rowEntity.QtyPO * rowEntity.UnitPrice));
                        }else if($scope.mData.IsNONPKP == true){
                            //Non PKP
                            rowEntity.VATAmount = 0;
                            rowEntity.UnitPrice = rowEntity.StandardCost;
                            rowEntity.DiscountAmount = Math.round(((rowEntity.DiscountPercent / 100)) * (rowEntity.QtyPO * rowEntity.UnitPrice));
                        }
                        // CR5 #10
                        //rowEntity.VATAmount = Math.round(((rowEntity.QtyPO * rowEntity.UnitPrice) - (rowEntity.DiscountAmount)) * 0.1);
                        //Total  = (Qty * HargaDasar) - Diskon + VAT;
                        rowEntity.TotalAmount = (rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount + rowEntity.VATAmount;

                    }
                    // alert(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);

                    // var vTotalAmount = rowEntity.TotalAmount;
                    // for (var i = 0; i < $scope.mData.GridDetail.data.length; i++) {
                    //     vTotalAmount = vTotalAmount + $scope.mData.GridDetail[i].TotalAmount;
                    //     $scope.mData.TotalAmount = vTotalAmount;
                    // }

                    $scope.mData.TotalAmount = parseFloat($scope.mData.TotalAmount);
                    var vTotalAmount = 0;
                    vTotalAmount = parseFloat(vTotalAmount);
                    for (var i = 0; i < $scope.sublet_sop_wo.data.length; i++) {
                        vTotalAmount = vTotalAmount + $scope.sublet_sop_wo.data[i].TotalAmount;
                        $scope.mData.TotalAmount = vTotalAmount;
                    }


                });
                //   gridApi.cellNav.on.navigate($scope,function(newRowCol, oldRowCol){
                //     $log.log('navigation event');
                //     if($scope.edit == 0)
                //     {
                //       rowEntity.DiscountPercent = 0;
                //       rowEntity.DiscountAmount = 0;
                //     }

                //     rowEntity.DiscountAmount = (rowEntity.DiscountPercent / 100) * (rowEntity.QtyPO * rowEntity.UnitPrice);
                //     rowEntity.VATAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) * 0.1;
                //     rowEntity.TotalAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) + rowEntity.VATAmount;
                //     //console.log(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);
                //     // alert(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);

                //     var vTotalAmount = rowEntity.TotalAmount;
                //     for (var i = 0; i < $scope.non_tam_sop_so.data.length; i++) {
                //       vTotalAmount = vTotalAmount + $scope.non_tam_sop_so.data[i].TotalAmount;
                //       $scope.mData.TotalAmount = vTotalAmount;
                //     }
                //     $scope.edit = $scope.edit + 1;
                // });
            },
            columnDefs: [{
                field: 'PartsCode',
                displayName: 'No. Material',
                enableCellEdit: false,
                //field: 'NoMaterial',
                width: '12%',
                //     cellTemplate: '<div><div class="input-group">\
                //       <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.PartsCode" style="height:30px;" ng-disabled="grid.appScope.stateModeView" required>\
                //       <label class="input-group-btn">\
                //           <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
                //               <i class="fa fa-search fa-1"></i>\
                //           </span>\
                //       </label>\
                //   </div></div>'
                },
                // { name: 'Nama Material', sort: { direction: uiGridConstants.ASC, priority: 0 }, width: '24%', field: 'PartsName',enableCellEdit: false, visible: false }, // cellTemplate: '<span>{{row.entity.PartName}} {{row.entity.PartsName}}</span>'},
                { name: 'Nama Material', width: '24%', field: 'PartsName',enableCellEdit: false }, 
                { name: 'Qty', field: 'QtyPO', cellEditableCondition: function(scope){
                        if($scope.stateModeView){
                            return false;
                        }else{
                            return true;
                        }
                    }, width: '8%', editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">' },
                { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', width: '8%', enableCellEdit: false, cellFilter: 'number' },
                { name: 'Satuan', field: 'Name', width: '7%', enableCellEdit: false },
                { name: 'Standard Cost', field: 'UnitPrice', displayName: 'Standard Cost', width: '8%', enableCellEdit: true, type: 'number', cellFilter: 'number' },
                { name: 'PartsClassId4', field: 'PartsClassId4', visible: false },
                {
                    name: 'Diskon %',
                    displayName: '%',
                    field: 'DiscountPercent',
                    width: '5%', 
                    enableCellEdit: true,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:262%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'Diskon Jumlah',
                    displayName: 'Jumlah',
                    field: 'DiscountAmount',
                    width: '8%', 
                    enableCellEdit: false,
                    cellFilter: 'number',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>",
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.SubletDiscountAmountResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent, row)}}</div>',
                },
                {
                    displayName: 'PPN',
                    name: 'PPN',
                    field: 'VATAmount',
                    width: '8%', 
                    enableCellEdit: false,
                    cellFilter: 'number'
                        // cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.VATAmount">{{grid.appScope.$parent.SubletPPNResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent, row)}}</div>',
                },
                {
                    name: 'Harga Total',
                    field: 'TotalAmount',
                    width: '10%', 
                    enableCellEdit: false,
                    cellFilter: 'number',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.SubletTotalAmount(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountAmount, row.entity.VATAmount, row)}}</div>',
                },
                // {
                //     name: 'Action',
                //     cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>',
                //     visible: true
                // },
                { name: 'UomId', field: 'UomId', visible: false },
                // {
                //   name:'ETD',
                //   field: 'ETD',
                //   cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.ETD">{{grid.appScope.$parent.DateTimeFilter("0000-12-31T17:00:00.000Z")}}<div>',
                //   visible: false
                // },
                // {
                //   name:'ETA',
                //   field: 'ETA',
                //   cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.ETA">{{grid.appScope.$parent.DateTimeFilter("0000-12-31T17:00:00.000Z")}}<div>',
                //   visible: false
                // },
            ]
        };

        $scope.sublet_sop_wo_lihat = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 5,
            enableSelectAll: true,
            enableCellEdit: false,
            columnDefs: [{
                    field: 'PartsCode',
                    displayName: 'No. Material',
                    enableCellEdit: false,
                    //field: 'NoMaterial',
                    width: '12%',
                    //     cellTemplate: '<div><div class="input-group">\
                    //       <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.PartsCode" style="height:30px;" ng-disabled="grid.appScope.stateModeView" required>\
                    //       <label class="input-group-btn">\
                    //           <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
                    //               <i class="fa fa-search fa-1"></i>\
                    //           </span>\
                    //       </label>\
                    //   </div></div>'
                },
                { name: 'Nama Material', sort: { direction: uiGridConstants.ASC, priority: 0 }, width: '24%', cellTemplate: '<span>{{row.entity.PartsName}}</span>' },
                { name: 'Qty', field: 'QtyPO', cellEditableCondition: function(scope){
                        if($scope.stateModeView){
                            return false;
                        }else{
                            return true;
                        }
                    }, width: '8%', editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">' },
                { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', width: '8%', enableCellEdit: false, editableCellTemplate: '<input type="number" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">' },
                { name: 'Satuan', field: 'Name', width: '7%', enableCellEdit: false },
                { name: 'Standard Cost', field: 'UnitPrice', displayname: 'Standard Cost', width: '8%', enableCellEdit: false, cellFilter: 'number' },
                { name: 'PartsClassId4', field: 'PartsClassId4', visible: false },
                {
                    name: 'Diskon %',
                    displayName: '%',
                    field: 'DiscountPercent',
                    width: '5%', 
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:262%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'Diskon Jumlah',
                    displayName: 'Jumlah',
                    field: 'DiscountAmount',
					cellFilter: 'number',
                    width: '8%', 
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>",
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.DiscountAmountResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
                },
                {
                    name: 'PPN',
                    displayName: 'PPN',
                    field: 'VATAmount',
                    width: '8%', 
                    cellFilter: 'number'
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.PPNResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
                },
                {
                    name: 'Harga Total',
                    displayName: 'Harga Total',
                    field: 'TotalAmount',
                    width: '10%', 
                    cellFilter: 'number'
                        // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.NONTAMTotalAmount(row.entity.QtyPO, row.entity.UnitPrice, row)}}</div>',
                },
                // { name: 'Action', cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>', visible : true
                // },
                { name: 'UomId', field: 'UomId', visible: false },
            ]
        };

        // Non Tam Sop SO
        //
        //Ini SO

        $scope.non_tam_sop_so_lihat = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 5,
            enableSelectAll: true,
            columnDefs: [{
                    field: 'PartsCode',
                    displayName: 'No. Material',
                    enableCellEdit: false,
                    //field: 'NoMaterial',
                    width: '11%',
                    cellTemplate: '<div><div class="input-group">\
                      <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.PartsCode" style="height:30px;" ng-disabled="grid.appScope.stateModeView" required>\
                      <label class="input-group-btn" ng-hide="grid.appScope.stateModeView">\
                          <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
                              <i class="fa fa-search fa-1"></i>\
                          </span>\
                      </label>\
                  </div></div>'
                },
                { name: 'Nama Material', sort: { priority: 0 }, width: '20%',field:"PartsName", enableCellEdit: false, cellTemplate: '<div class="ui-grid-cell-contents"><span>{{row.entity.PartsName}}</span></div>' },
                {
                    displayName: 'ETD',
                    name: 'ETD',
                    field: 'ETD',
                    width: '10%',
                    enableCellEdit: false,
                    cellFilter: 'date:\'yyyy-MM-dd\''
                //     cellTemplate: '<div><div class="input-group">\
                //       <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.ETD" disabled>\
                //       \
                //   </div></div>'
                },
                {
                    displayName: 'ETA',
                    name: 'ETA',
                    field: 'ETA',
                    width: '10%',
                    enableCellEdit: false,
                    cellFilter: 'date:\'yyyy-MM-dd\''
                //     cellTemplate: '<div><div class="input-group">\
                //       <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.ETA" disabled>\
                //       \
                //   </div></div>'
                },
                { name: 'Qty PO', field: 'QtyPO', displayName: 'Qty PO', enableCellEdit: false, editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">' },
                { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', enableCellEdit: false, editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">' },
                { name: 'Satuan', field: 'Name', enableCellEdit: false },
                { name: 'Standart Cost', field: 'UnitPrice', cellFilter: 'number', enableCellEdit: false },
                {
                    name: 'Diskon %',
                    displayName: '%',
                    field: 'DiscountPercent',
                    enableCellEdit: false, 
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:202%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon %</td></tr></tbody></table></div></div>"
                },
                {
                    name: 'Diskon Jumlah',
                    displayName: 'Jumlah',
                    field: 'DiscountAmount',
                    enableCellEdit: false, 
					cellFilter: 'number',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>",
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.DiscountAmountResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
                },
                {
                    displayName: 'PPN',
                    name: 'PPN',
                    field: 'VATAmount',
                    cellFilter: 'number',
                    enableCellEdit: false, 
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.PPNResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
                },
                {
                    name: 'Harga Total',
                    field: 'TotalAmount',
                    cellFilter: 'number',
                    enableCellEdit: false, 
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.NONTAMTotalAmount(row.entity.QtyPO, row.entity.UnitPrice, row)}}</div>',
                },
                // { name: 'Action', cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>', visible : true
                // },
                { name: 'UomId', field: 'UomId', visible: false },
                { name: 'PartsClassId4', field: 'PartsClassId4', visible: false },
                { 
                    name: 'Status TPOS', 
                    field: 'StatusTPOS', 
                    displayName: 'Status TPOS', 
                    cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.StatusTPOS">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    enableCellEdit: false,
            
                },
                { name: 'Alasan Reject', field: 'ReasonTPOS', enableCellEdit: false}
            ]
        };
        
        $scope.selectedRows = [];
        $scope.delete = function(param){
            var data = $scope.selectedRows;
            console.log("param==>",param);
            console.log("$scope.selectedRows==>",$scope.selectedRows);
            console.log("data==>",data);
            var deleteItem = function(data,x){
                if (x <= data.length-1){
                    
                    console.log("data x==>",x);
                    if(param){
                        idx = _.findIndex($scope[param].data , { idx: $scope.selectedRows[x].idx });
                        console.log("idx==>",idx);
                        console.log("$scope[param].data[idx]==>",$scope[param].data[idx]);
                        if($scope[param].data[idx].PurchaseOrderStatusId <= 3 || $scope[param].data[idx].PurchaseOrderStatusId == undefined || ($scope[param].data[idx].PurchaseOrderStatusId == 4 && $scope[param].data[idx].QtyGR == 0)){
                            $scope[param].data.splice(idx,1);
                        }else if($scope[param].data[idx].PurchaseOrderStatusId == 4 && $scope[param].data[idx].QtyGR < $scope[param].data[idx].QtyPO && $scope[param].data[idx].QtyGR > 0 ){
                            var nameofpart = $scope[param].data[idx].PartsCode;
                            bsAlert.warning("Parts dengan kode "+ nameofpart +" sudah di GR .","Silahkan batalkan terlebih dahulu");
                        }else if($scope[param].data[idx].PurchaseOrderStatusId == 5 || ($scope[param].data[idx].PurchaseOrderStatusId == 4  && $scope[param].data[idx].QtyGR == $scope[param].data[idx].QtyPO )){
                            var nameofpart = $scope[param].data[idx].PartsCode;
                            bsAlert.warning("Parts dengan kode "+ nameofpart +" sudah di GR .","Silahkan batalkan terlebih dahulu");
                        }
                    }else{
                        console.log("$scope.non_tam_sop_so.data=>",$scope.non_tam_sop_so.data);
                        console.log("$scope.sublet_sop_wo.data=>",$scope.sublet_sop_wo.data);
                        //console.log("$scope.gridApi=>",$scope.gridApi);
                        if($scope.crudState == '151' || $scope.crudState == '152'){
                            idx = _.findIndex($scope.sublet_sop_wo.data , { idx: $scope.selectedRows[x].idx });
                            console.log("idx 151 152==>",idx);
                            $scope.sublet_sop_wo.data.splice(idx,1);
                        } else {
                            idx = _.findIndex($scope.non_tam_non_sop_bengkel.data , { idx: $scope.selectedRows[x].idx });
                            console.log("idx==>",idx);
                            $scope.non_tam_non_sop_bengkel.data.splice(idx,1);
                        }
                    }
                    if(x < data.length){
                        deleteItem(data,x+1);
                    }else{
                        $scope.selectedRows = [];
                        return
                    }
                } else {
                    $scope.selectedRows = [];
                    return
                }
            }
            deleteItem(data, 0)
        }
        
        $scope.non_tam_sop_so = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 5,
            enableSelectAll: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                $scope.edit = 0;
                gridApi.selection.on.rowSelectionChanged($scope,function(row) {
                        $scope.selectedRows = gridApi.selection.getSelectedRows();
                        // console.log("selected=>",$scope.selectedRows);
                        if($scope.onSelectRows){
                            $scope.onSelectRows($scope.selectedRows);
                        }
                });
                gridApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
                        $scope.selectedRows = gridApi.selection.getSelectedRows();
                        if($scope.onSelectRows){
                            $scope.onSelectRows($scope.selectedRows);
                        }
                });
                // gridApi.edit.on.beginCellEdit($scope, function (rowEntity, colDef, event) {
                //     if (colDef.field == 'ReasonTPOS') {
                //         rowEntity.ReasonTPOS.selection;
                //         console.log('rowEntity', rowEntity);
                //     }
                // });
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {

                    console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);

                    if (colDef.field == 'ReasonTPOS') {
                        rowEntity.ReasonTPOS = oldValue;
                        console.log('rowEntity', rowEntity);
                    }

                    if (colDef.field == 'UnitPrice') {
                        rowEntity.newUnitPrice = angular.copy(newValue);
                        console.log('rowEntity', rowEntity);
                    }

                    if ($scope.isEdit == true) {
                        rowEntity.LastModifiedDate = $scope.localeDate(new Date());
                        rowEntity.LastModifiedUserId = $scope.UserId;
                    }
                      
                    // if (typeof row === undefined) {
                    //     rowEntity.DiscountPercent = rowEntity.DiscountPercent;
                    //     rowEntity.DiscountAmount = rowEntity.DiscountAmount;
                    // }
                    // //Diskon = (Qty * HargaDasar) * (PersenDiskon / 100);
                    // rowEntity.DiscountAmount =Math.round(((rowEntity.DiscountPercent / 100)) * (rowEntity.QtyPO * rowEntity.UnitPrice));
                    // console.log("Sini DIskon",rowEntity.DiscountPercent)
                    // console.log("Sini DIskon",rowEntity.DiscountAmount)
                    // if(rowEntity.xUnitPrice == undefined || rowEntity.xUnitPrice == null){
                    //     rowEntity.VATAmount = (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // }else{
                    //     // before
                    //     // rowEntity.VATAmount =((rowEntity.QtyPO * rowEntity.xUnitPrice) - rowEntity.DiscountAmount) - ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount);
                        
                    //     //VAT = ((Qty * HargaDasar) - Diskon) * (PersenPPN / 100);
                    //     rowEntity.VATAmount = Math.round(((rowEntity.QtyPO * rowEntity.UnitPrice) - (rowEntity.DiscountAmount)) * 0.1);
                    //     // rowEntity.VATAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - (rowEntity.DiscountAmount)) * 0.1;
                    // }
                    // //Total  = (Qty * HargaDasar) - Diskon + VAT;
                    // rowEntity.TotalAmount = (rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount + rowEntity.VATAmount;
                    

                    // // rowEntity.DiscountAmount = (rowEntity.DiscountPercent / 100) * (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // // rowEntity.VATAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount)*0.1;
                    // // rowEntity.TotalAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) + rowEntity.VATAmount;

                    // // ====
                    // // rowEntity.DiscountAmount = Math.round((rowEntity.DiscountPercent / 100)) * (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // // // rowEntity.VATAmount = Math.round(((rowEntity.QtyPO * rowEntity.xUnitPrice) - rowEntity.DiscountAmount)*0.1);
                    // // rowEntity.VATAmount =((rowEntity.QtyPO * rowEntity.xUnitPrice) - rowEntity.DiscountAmount) - ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount);
                    // // rowEntity.TotalAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) + rowEntity.VATAmount;

                    // //console.log(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);
                    // // alert(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);

                    // // add by rizki
                    // $scope.mData.TotalAmount = parseFloat($scope.mData.TotalAmount);
                    // var vTotalAmount = 0;
                    // vTotalAmount = parseFloat(vTotalAmount);
                    // console.log('rowEntity.TotalAmount', rowEntity.TotalAmount);
                    // //var vTotalAmount = rowEntity.TotalAmount; // dicomment tgl 8 Januari 2018 by rizki, terkait perbaikan total amount

                    // for (var i = 0; i < $scope.non_tam_sop_so.data.length; i++) {
                    //     vTotalAmount = vTotalAmount + $scope.non_tam_sop_so.data[i].TotalAmount;
                    //     $scope.mData.TotalAmount = vTotalAmount;
                    //     console.log('TotalAmount[i]', $scope.non_tam_sop_so.data[i].TotalAmount);
                    // }
                    // console.log('vTotalAmount', vTotalAmount, $scope.non_tam_sop_so.data);

                    // this is new formula 07/10/2020 =================================
                    if (typeof row === undefined) {
                        rowEntity.DiscountPercent = rowEntity.DiscountPercent;
                        rowEntity.DiscountAmount = rowEntity.DiscountAmount;
                    }
                    // rowEntity.DiscountAmount = Math.round(((rowEntity.DiscountPercent / 100)) * (rowEntity.QtyPO * rowEntity.UnitPrice));
                    // rowEntity.VATAmount = Math.round(((rowEntity.QtyPO * rowEntity.UnitPrice) - (rowEntity.DiscountAmount)) * 0.1);
                    // //Total  = (Qty * HargaDasar) - Diskon + VAT;
                    // rowEntity.TotalAmount = (rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount + rowEntity.VATAmount;


                    // if(colDef.field == "UnitPrice"){
                    //     var HargaSatuan = parseInt(rowEntity.UnitPrice)
                    //     rowEntity.UnitPrice = parseInt(rowEntity.UnitPrice)
                    //     rowEntity.StandardCost = parseInt(rowEntity.UnitPrice)
                    // }else if(colDef.field == "QtyPO"){
                    //     var HargaSatuan = rowEntity.StandardCost
                    //     rowEntity.StandardCost = parseInt(rowEntity.UnitPrice)
                    // }else{
                    //     var HargaSatuan = rowEntity.StandardCost
                    // }
                    
                    // imron 27okt2020
                    // var HargaSatuan = rowEntity.StandardCost / (1.1);
                    
					//var HargaSatuan    = rowEntity.UnitPrice;
                     // :1.1 di hapus karna sudah di bagi di BE oleh rade;
                    //var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
                    //rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo);
					
					// CR5 #10
                    console.log("$scope.tmpPO",$scope.tmpPO);
					console.log('$scope.mData.IsNONPKP at $scope.non_tam_sop_so stv: ', $scope.mData.IsNONPKP);
					
                    if($scope.user.RoleId == 1124 || $scope.user.RoleId == 1125){
                        if($scope.tmpPO == 5){
                            // ini buat sublet
                        }else{
                            // ini selain sublet
                            
                            if($scope.mData.IsNONPKP == false){
                                //PKP
                                if(rowEntity.newUnitPrice == undefined){
                                    
                                    // var HargaSatuan    = Math.round(rowEntity.StandardCost/1.1);
                                    var HargaSatuan    = Math.round(rowEntity.StandardCost/ (1+(PPNPerc/100)));

                                    // rowEntity.UnitPrice = Math.round(rowEntity.StandardCost/1.1);
                                    rowEntity.UnitPrice = Math.round(rowEntity.StandardCost/ (1+(PPNPerc/100)));

                                }else{
                                    
                                    var HargaSatuan    = rowEntity.newUnitPrice;
                                    rowEntity.UnitPrice = rowEntity.newUnitPrice;
                                }
                                // var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
                                // rowEntity.DiscountAmount = parseFloat(rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo)).toFixed();
                                rowEntity.DiscountAmount = (Math.round(rowEntity.UnitPrice*rowEntity.DiscountPercent / 100))*rowEntity.QtyPO;
                                // var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
                                // var tmpVataAmount = rowEntity.QtyPO * VATSatuan;
                                console.log("Math.round((rowEntity.UnitPrice - rowEntity.DiscountAmount)*0.1)",Math.round((rowEntity.UnitPrice - rowEntity.DiscountAmount)*(PPNPerc/100)));

                                // rowEntity.VATAmount = (Math.round((rowEntity.UnitPrice - rowEntity.DiscountAmount)*0.1))*rowEntity.QtyPO;
                                rowEntity.VATAmount = (Math.round((rowEntity.UnitPrice - rowEntity.DiscountAmount)*(PPNPerc/100)))*rowEntity.QtyPO;

                                
                                // var HargaSatuanSV9 = (rowEntity.StandardCost / (1.1)).toFixed();
                                // var DiscountSatuanSV9 = Math.round(((rowEntity.DiscountPercent / 100)) * HargaSatuanSV9);
                                // rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuanSV9;
                                // rowEntity.VATAmount =rowEntity.QtyPO *((HargaSatuanSV9 - DiscountSatuanSV9) * 0.1).toFixed();
                                // rowEntity.UnitPrice = Math.round(rowEntity.StandardCost / 1.1);
                            }else if($scope.mData.IsNONPKP == true){
                                //Non PKP
                                if(rowEntity.newUnitPrice == undefined){
                                    
                                    var HargaSatuanSV9 = rowEntity.StandardCost;
                                    rowEntity.UnitPrice = rowEntity.StandardCost;
                                }else{
                                    
                                    var HargaSatuanSV9 = rowEntity.newUnitPrice;
                                    rowEntity.UnitPrice = rowEntity.newUnitPrice;
                                }
                                
                                // var DiscountSatuanSV9 = Math.round(((rowEntity.DiscountPercent / 100)) * HargaSatuanSV9);
                                rowEntity.DiscountAmount = Math.round(rowEntity.UnitPrice*rowEntity.DiscountPercent / 100)*rowEntity.QtyPO;
                                rowEntity.VATAmount = 0;
                                
                            }
                            if($scope.mData.IsNONPKP == false){
                                if(rowEntity.newUnitPrice == undefined){
                                    rowEntity.TotalAmount =  ((rowEntity.UnitPrice * rowEntity.QtyPO) - rowEntity.DiscountAmount)+rowEntity.VATAmount;
                                }else{
                                    rowEntity.TotalAmount =  Math.round((rowEntity.QtyPO * rowEntity.newUnitPrice) - rowEntity.DiscountAmount + rowEntity.VATAmount);
                                }
                            }else{
                                // if(rowEntity.newUnitPrice == undefined){
                                    rowEntity.TotalAmount = (rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount + rowEntity.VATAmount;
                                // }else{
                                    
                                // }
                            }
                        }
                    }else{ 
                        // imron 27okt2020
                        // var HargaSatuan = rowEntity.StandardCost / (1.1);
                        var HargaSatuan    = rowEntity.UnitPrice;
                        // :1.1 di hapus karna sudah di bagi di BE oleh rade;
                        var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
                        rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo);

                        // var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
                        var VATSatuan = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);

                        rowEntity.VATAmount = rowEntity.QtyPO * VATSatuan.toFixed($scope.roundTo);
                        // rowEntity.VATAmount = Math.floor(rowEntity.QtyPO * VATSatuan);
                        rowEntity.TotalAmount = Math.round((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount + rowEntity.VATAmount);

                    }
					// CR5 #10
                    //var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
                    //rowEntity.VATAmount = rowEntity.QtyPO * VATSatuan.toFixed($scope.roundTo);
                    // rowEntity.VATAmount = Math.floor(rowEntity.QtyPO * VATSatuan);
                    

                    // add by rizki
                    $scope.mData.TotalAmount = parseFloat($scope.mData.TotalAmount);
                    var vTotalAmount = 0;
                    vTotalAmount = parseFloat(vTotalAmount);
                    for (var i = 0; i < $scope.non_tam_sop_so.data.length; i++) {
                        vTotalAmount = vTotalAmount + $scope.non_tam_sop_so.data[i].TotalAmount;
                        $scope.mData.TotalAmount = vTotalAmount;
                    }
                });
            },
            cellEditableCondition: function(scope){
                if($scope.stateModeView){
                    return false;
                }else{
                    return true;
                }
            },
            columnDefs: [{
                    field: 'PartsCode',
                    displayName: 'No. Material',
                    //enableCellEdit : false,
                    cellEditableCondition: function(scope){
                        if($scope.stateModeView || ($scope.mData.RefPOTypeId == '4' && $scope.mData.POTypeId == '5')){
                            return false;
                        }else{
                            return true;
                        }
                    },
                    //field: 'NoMaterial',
                    width: '10%',
                    cellTemplate: '<div><div class="input-group">\
                      <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.PartsCode" style="height:30px;" ng-disabled="grid.appScope.stateModeView" required>\
                      <label class="input-group-btn" ng-hide="grid.appScope.stateModeView">\
                          <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
                              <i class="fa fa-search fa-1"></i>\
                          </span>\
                      </label>\
                  </div></div>'
                },
                { name: 'Nama Material', width: '11%', field:'PartsName', sort: { priority: 0 },field:"PartsName", enableCellEdit: false, cellTemplate: '<div class="ui-grid-cell-contents"><span>{{row.entity.PartsName}}</span></div>' },
                {
                    displayName: 'ETD',
                    name: 'ETD',
                    field: 'ETD',
                    width: '9%',
                    enableCellEdit: false,
                    cellFilter: 'date:\'yyyy-MM-dd\''
                //     cellTemplate: '<div><div class="input-group">\
                //       <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.ETD" disabled>\
                //       \
                //   </div></div>'
                },
                {
                    displayName: 'ETA',
                    name: 'ETA',
                    field: 'ETA',
                    width: '9%',
                    enableCellEdit: false,
                    cellFilter: 'date:\'yyyy-MM-dd\''
                //     cellTemplate: '<div><div class="input-group">\
                //       <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.ETA" disabled>\
                //       \
                //   </div></div>'
                },
                { name: 'Qty PO', field: 'QtyPO', displayName: 'Qty PO', cellEditableCondition: function(scope){
                        if($scope.stateModeView){
                            return false;
                        }else{
                            return true;
                        }
                    }, width: '6%', editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">' },
                { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', width: '6%', enableCellEdit: false, cellFilter: 'number' },
                { name: 'Satuan', field: 'Name', width: '5%', enableCellEdit: false},
                {
                    displayName: 'Standart Cost', 
                    name: 'Standart Cost', 
                    field: 'UnitPrice', 
                    cellFilter: 'number', 
                    enableCellEdit: false,
                    // enableCellEdit udah di enable di function onOverlayMode
                    
                },
                {
                    name: 'Diskon %',
                    displayName: '%',
                    field: 'DiscountPercent',
                    width: '4%', 
                    cellFilter: 'number',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:252%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'Diskon Jumlah',
                    displayName: 'Jumlah',
                    field: 'DiscountAmount',
                    width: '6%', 
                    cellFilter: 'number',
                    enableCellEdit: false,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>",
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.DiscountAmountResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
                },
                {
                    displayName: 'PPN',
                    name: 'PPN',
                    field: 'VATAmount',
                    cellFilter: 'number',
                    enableCellEdit: false,
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.PPNResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
                },
                {
                    name: 'Harga Total',
                    field: 'TotalAmount',
                    cellFilter: 'number',
                    enableCellEdit: false,
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.NONTAMTotalAmount(row.entity.QtyPO, row.entity.UnitPrice, row)}}</div>',
                },
                // { name: 'Action', cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>', visible : true
                // },
                { name: 'UomId', field: 'UomId', visible: false },
                { name: 'PartsClassId4', field: 'PartsClassId4', visible: false },
                { 
                    name: 'Status TPOS', 
                    field: 'StatusTPOS', 
                    displayName: 'Status TPOS', 
                    cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.StatusTPOS">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    enableCellEdit: false
                },
                { name: 'Alasan Reject', field: 'ReasonTPOS', enableCellEdit: true, cellEditableCondition: true, allowCellFocus: true},
                {
                    name: 'Action',
                    cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>',
                    visible: true
                }
            ]
        };

        $scope.non_tam_sop_so_ubah = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                $scope.edit = 0;
                gridApi.selection.on.rowSelectionChanged($scope,function(row) {
                        $scope.selectedRows = gridApi.selection.getSelectedRows();
                        // console.log("selected=>",$scope.selectedRows);
                        if($scope.onSelectRows){
                            $scope.onSelectRows($scope.selectedRows);
                        }
                });
                gridApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
                        $scope.selectedRows = gridApi.selection.getSelectedRows();
                        if($scope.onSelectRows){
                            $scope.onSelectRows($scope.selectedRows);
                        }
                });
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {

                    console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);

                    if ($scope.isEdit == true) {
                        rowEntity.LastModifiedDate = $scope.localeDate(new Date());
                        rowEntity.LastModifiedUserId = $scope.UserId;
                    }
        
                    // this is new formula 07/10/2020 =================================
                    if (typeof row === undefined) {
                        rowEntity.DiscountPercent = rowEntity.DiscountPercent;
                        rowEntity.DiscountAmount = rowEntity.DiscountAmount;
                    }
                    
                    // imron 27okt2020
                    //var HargaSatuan = rowEntity.StandardCost / (1.1);
                    //var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
                    //rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo);
					// CR5 #10
					console.log('$scope.mData.IsNONPKP at $scope.non_tam_sop_so_ubah stv: ', $scope.mData.IsNONPKP);
					if($scope.mData.IsNONPKP == false){
						//PKP
						// var HargaSatuan = rowEntity.StandardCost / (1.1);
						var HargaSatuan = rowEntity.StandardCost / (1+(PPNPerc/100));

						var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
						rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo);

						// var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
						var VATSatuan = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);

						rowEntity.VATAmount = rowEntity.QtyPO * VATSatuan.toFixed($scope.roundTo);

						// rowEntity.UnitPrice = rowEntity.StandardCost / 1.1;
						rowEntity.UnitPrice = rowEntity.StandardCost / (1+(PPNPerc/100));

					}else if($scope.mData.IsNONPKP == true){
						//Non PKP
						var HargaSatuan = rowEntity.StandardCost;
						var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
						rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo);
						rowEntity.VATAmount = 0;
						rowEntity.UnitPrice = rowEntity.StandardCost;
					}
					// CR5 #10
                    //var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
                    //rowEntity.VATAmount = rowEntity.QtyPO * VATSatuan.toFixed($scope.roundTo);
                    rowEntity.TotalAmount =Math.round((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount + rowEntity.VATAmount);

                    // add by rizki
                    $scope.mData.TotalAmount = parseFloat($scope.mData.TotalAmount);
                    var vTotalAmount = 0;
                    vTotalAmount = parseFloat(vTotalAmount);
                    for (var i = 0; i < $scope.non_tam_sop_so.data.length; i++) {
                        vTotalAmount = vTotalAmount + $scope.non_tam_sop_so.data[i].TotalAmount;
                        $scope.mData.TotalAmount = vTotalAmount;
                    }
                    
                    // QTYPO Partial GI ===
                    if(colDef.field == 'QtyPO'){
                        if(rowEntity.QtyPO < rowEntity.QtyGR){
                            rowEntity.QtyPO = oldValue;
                            bsAlert.warning('Quantity po tidak boleh kurang dari quantity GR','Silahkan Cek kembali');
                        }
                    }
                });
            },
            cellEditableCondition: function(scope){
                if($scope.stateModeView){
                    return false;
                }else{
                    return ($scope.row.entity.QtyGR == 0 || $scope.row.entity.QtyGR == null);
                }
            },
            columnDefs: [{
                    field: 'PartsCode',
                    displayName: 'No. Material',
                    cellEditableCondition: function(scope){
                        if($scope.stateModeView){
                            return false;
                        }else{
                            return true;
                        }
                    },
                    //field: 'NoMaterial',
                    width: '10%',
                    cellTemplate: '<div><div class="input-group">\
                      <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.PartsCode" style="height:30px;" ng-disabled="grid.appScope.stateModeView" required>\
                      <label class="input-group-btn" ng-hide="grid.appScope.stateModeView">\
                          <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
                              <i class="fa fa-search fa-1"></i>\
                          </span>\
                      </label>\
                  </div></div>'
                },
                { name: 'Nama Material', width: '10%', field:'PartsName', sort: { priority: 0 },field:"PartsName", enableCellEdit: false, cellTemplate: '<div class="ui-grid-cell-contents"><span>{{row.entity.PartsName}}</span></div>' },
                {
                    displayName: 'ETD',
                    name: 'ETD',
                    field: 'ETD',
                    width: '10%',
                    enableCellEdit: false,
                    cellFilter: 'date:\'yyyy-MM-dd\''
                //     cellTemplate: '<div><div class="input-group">\
                //       <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.ETD" disabled>\
                //       \
                //   </div></div>'
                },
                {
                    displayName: 'ETA',
                    name: 'ETA',
                    field: 'ETA',
                    width: '10%',
                    enableCellEdit: false,
                    cellFilter: 'date:\'yyyy-MM-dd\''
                //     cellTemplate: '<div><div class="input-group">\
                //       <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.ETA" disabled>\
                //       \
                //   </div></div>'
                },
                // { name: 'Qty PO', field: 'QtyPO', displayName: 'Qty PO', enableCellEdit: true, editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">' },
                { name: 'Qty PO', field: 'QtyPO', displayName: 'Qty PO',
                    cellEditableCondition: function(scope){
                        // return scope.rowRenderIndex%2;
                        if($scope.stateModeView){
                            return false;
                        }else{
                            if (scope.row.entity.PurchaseOrderStatusId <= 3 ) {
                                return true;
                            } else if(scope.row.entity.PurchaseOrderStatusId == 4 && scope.row.entity.QtyGR <=  scope.row.entity.QtyPO ){
                                return true;
                            } else if(scope.row.entity.PurchaseOrderStatusId === undefined){
                                return true
                            } else {
                                return false;
                            }
                        }
                    },
                    editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">'},
                { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', enableCellEdit: false, editableCellTemplate: '<input type="number" max="9999999" ui-grid-editor ng-model="MODEL_COL_FIELD" step="1">' },
                { name: 'Satuan', field: 'Name', enableCellEdit: false},
                { name: 'Standart Cost', field: 'UnitPrice', cellFilter: 'number', enableCellEdit: false },
                {
                    name: 'Diskon %',
                    displayName: '%',
                    field: 'DiscountPercent',
                    cellFilter: 'number',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:202%'>Diskon %</div></div>"
                },
                {
                    name: 'Diskon Jumlah',
                    displayName: 'Jumlah',
                    field: 'DiscountAmount',
                    cellFilter: 'number',
                    enableCellEdit: false,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>",
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.DiscountAmountResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
                },
                {
                    displayName: 'PPN',
                    name: 'PPN',
                    field: 'VATAmount',
                    cellFilter: 'number',
                    enableCellEdit: false,
                        // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.PPNResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
                },
                {
                    name: 'Harga Total',
                    field: 'TotalAmount',
                    cellFilter: 'number',
                    enableCellEdit: false,
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.NONTAMTotalAmount(row.entity.QtyPO, row.entity.UnitPrice, row)}}</div>',
                },
                // { name: 'Action', cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>', visible : true
                // },
                { name: 'UomId', field: 'UomId', visible: false },
                { name: 'PartsClassId4', field: 'PartsClassId4', visible: false },
                { 
                    name: 'Status TPOS', 
                    field: 'StatusTPOS', 
                    displayName: 'Status TPOS', 
                    cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.StatusTPOS">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    // cellTemplate: '<div class="ui-grid-cell-contents"><span>{{row.entity.PartsName}}</span></div>',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
                    enableCellEdit: false,
            
                },
                { name: 'Alasan Reject', field: 'ReasonTPOS', enableCellEdit: false}
            ]
        };

        $scope.non_tam_non_sop_bengkel = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 5,
            enableSelectAll: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope,function(row) {
                    $scope.selectedRows = gridApi.selection.getSelectedRows();
                    // console.log("selected=>",$scope.selectedRows);
                    if($scope.onSelectRows){
                        $scope.onSelectRows($scope.selectedRows);
                    }
                });
                gridApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
                        $scope.selectedRows = gridApi.selection.getSelectedRows();
                        if($scope.onSelectRows){
                            $scope.onSelectRows($scope.selectedRows);
                        }
                });
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {

                    console.log('rowEntity ntnsb', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);

                    console.log("$scope.isEditb==>",$scope.isEdit);
                    if ($scope.isEdit == true) {
                        rowEntity.LastModifiedDate = $scope.localeDate(new Date());
                        rowEntity.LastModifiedUserId = $scope.UserId;
                    }
                    
                    //Campaign Single validasi
                    if($scope.mData.POTypeId == 7){
                        if(rowEntity.QtyPO > rowEntity.RemainQuota){
                            bsNotify.show({
                                title: "Purchase Order",
                                content: "Quantity PO lebih besar dari Quantity sisa Quota.",
                                type: 'danger'
                            });
                            if($scope.isBundle){
                                for(var i in $scope.non_tam_non_sop_bengkel.data){
                                    $scope.non_tam_non_sop_bengkel.data[i].QtyPO = 0;
                                }
                            }else{
                                rowEntity.QtyPO = 0;
                            }
                            
                        }else if(rowEntity.QtyPO < rowEntity.QuotaQtyMin){
                            bsNotify.show({
                                title: "Purchase Order",
                                content: "Quantity PO tidak boleh lebih kecil dari Quantity minimal.",
                                type: 'danger'
                            });
                            if($scope.isBundle){
                                for(var i in $scope.non_tam_non_sop_bengkel.data){
                                    $scope.non_tam_non_sop_bengkel.data[i].QtyPO = 0;
                                }
                            }else{
                                rowEntity.QtyPO = 0;
                            }
                        }
                    }
                
                    // rowEntity.DiscountAmount = Math.round((rowEntity.DiscountPercent / 100)) * (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // if(rowEntity.xUnitPrice == undefined || rowEntity.xUnitPrice == null){
                    //     rowEntity.VATAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) * 0.1;
                    // }else{
                    //     rowEntity.VATAmount =((rowEntity.QtyPO * rowEntity.xUnitPrice) - rowEntity.DiscountAmount) - ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount);
                    // }
                    // rowEntity.TotalAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) + rowEntity.VATAmount;
                    // // rowEntity.DiscountAmount = (rowEntity.DiscountPercent / 100) * (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // // rowEntity.VATAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) * 0.1;
                    // // rowEntity.TotalAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) + rowEntity.VATAmount;
                    // // ====
                    // // rowEntity.DiscountAmount = Math.round((rowEntity.DiscountPercent / 100)) * (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // // rowEntity.VATAmount =((rowEntity.QtyPO * rowEntity.xUnitPrice) - rowEntity.DiscountAmount) - ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount);
                    // // rowEntity.TotalAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) + rowEntity.VATAmount;
                    
                    // this is new formula 07/10/2020 =================================
                    if (typeof row === undefined) {
                        rowEntity.DiscountPercent = rowEntity.DiscountPercent;
                        rowEntity.DiscountAmount = rowEntity.DiscountAmount;
                    }
                    // rowEntity.DiscountAmount = Math.round(((rowEntity.DiscountPercent / 100)) * (rowEntity.QtyPO * rowEntity.UnitPrice));
                    // rowEntity.VATAmount = Math.round(((rowEntity.QtyPO * rowEntity.UnitPrice) - (rowEntity.DiscountAmount)) * 0.1);
                    // //Total  = (Qty * HargaDasar) - Diskon + VAT;
                    // rowEntity.TotalAmount = (rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount + rowEntity.VATAmount;
                    
                    // imron 27okt2020
                    //var HargaSatuan = rowEntity.StandardCost / (1.1);
                    //var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
                    //rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo);
					// CR5 #10
					console.log('$scope.mData.IsNONPKP at $scope.non_tam_non_sop_bengkel stv: ', $scope.mData.IsNONPKP);
					if($scope.mData.IsNONPKP == false){
						//PKP
						var HargaSatuan = rowEntity.StandardCost;
						var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
						rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo);

						// var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
						var VATSatuan = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);

                        console.log("VATSatuan=>",VATSatuan);

						rowEntity.VATAmount = rowEntity.QtyPO * VATSatuan.toFixed($scope.roundTo);
						rowEntity.UnitPrice = rowEntity.StandardCost;
					}else if($scope.mData.IsNONPKP == true){
						//Non PKP
						var HargaSatuan = rowEntity.StandardCost;
						var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
						rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo);
						rowEntity.VATAmount = 0;
						rowEntity.UnitPrice = rowEntity.StandardCost;
					}
					// CR5 #10
                    //var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
                    //rowEntity.VATAmount = rowEntity.QtyPO * VATSatuan.toFixed($scope.roundTo);
                    rowEntity.TotalAmount =Math.round((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount + rowEntity.VATAmount);
                    
                    console.log(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);

                    var vTotalAmount = 0;
                    for (var i = 0; i < $scope.non_tam_non_sop_bengkel.data.length; i++) {
                        vTotalAmount = vTotalAmount + $scope.non_tam_non_sop_bengkel.data[i].TotalAmount;
                        $scope.mData.TotalAmount = vTotalAmount;
                    }

                    // QTYPO Partial GI ===
                    if(colDef.field == 'QtyPO'){
                        if(rowEntity.QtyPO < rowEntity.QtyGR){
                            rowEntity.QtyPO = oldValue;
                            bsAlert.warning('Quantity po tidak boleh kurang dari quantity GR','Silahkan Cek kembali');
                        }
                    }
                });
            },
            cellEditableCondition: function($scope) {
                console.log('QtyGR ==> ',$scope.row.entity.QtyGR);
                if($scope.stateModeView){
                    return false;
                }else{
                    return ($scope.row.entity.QtyGR == 0 || $scope.row.entity.QtyGR == null);
                }
            },
            columnDefs: $scope.columnNonTAM, 
            //     [{
            //         field: 'PartsCode',
            //         displayName: 'No. Material',
            //         // enableCellEdit: true,
            //         cellEditableCondition: function(scope){
            //             console.log(scope.row.entity.PurchaseOrderStatusId,'Di grid nyaaa');
            //             // return scope.rowRenderIndex%2;
            //             if(scope.row.entity.PurchaseOrderStatusId > 3 && scope.row.entity.QtyGR > 0){
            //                 return false;
            //             }else if(scope.row.entity.PurchaseOrderStatusId > 3 && scope.row.entity.QtyGR == 0){
            //                 return true;
            //             }else{
            //                 return true;
            //             }
            //         },
            //         // allowCellFocus: true,
            //         // enableCellEditOnFocus: false,
            //         //field: 'NoMaterial',
            //         width: '12%',
            //         cellTemplate: '<div>\
            //             <div ng-show="row.entity.PurchaseOrderStatusId > 3 && row.entity.QtyGR > 0" class="ui-grid-cell-contents"><span>{{row.entity.PartsCode}}</span></div>\
            //             <div class="input-group" ng-hide="row.entity.PurchaseOrderStatusId > 3 && row.entity.QtyGR > 0">\
            //                 <input type="text" style="height:30px;" class="form-control" ng-model="MODEL_COL_FIELD" ng-value="row.entity.PartsCode" required>\
            //                 <label class="input-group-btn">\
            //                     <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
            //                         <i class="fa fa-search fa-1"></i>\
            //                     </span>\
            //                 </label>\
            //             </div>\
            //         </div>'
            //     },
            //     { name: 'Nama Material', sort: { priority: 0 }, width: '20%', field:"PartsName", cellTemplate: '<div class="ui-grid-cell-contents"><span>{{row.entity.PartsName}}</span></div>', enableCellEdit: false },
            //     {
            //         displayName: 'ETD',
            //         name: 'ETD',
            //         field: 'ETD',
            //         // width: '8%',
            //         enableCellEdit: false,
            //         cellFilter: 'date:\'yyyy-MM-dd\''
            //     },
            //     {
            //         displayName: 'ETA',
            //         name: 'ETA',
            //         field: 'ETA',
            //         // width: '8%',
            //         enableCellEdit: false,
            //         cellFilter: 'date:\'yyyy-MM-dd\''
            //     },
            //     { 
            //         name: 'Qty PO', field: 'QtyPO', displayName: 'Qty PO', width: '6%',
            //         // enableCellEdit: true,
            //         cellEditableCondition: function(scope){
            //         // return scope.rowRenderIndex%2;
            //         if($scope.stateModeView){
            //             return false;
            //         } else {
            //             if (scope.row.entity.PurchaseOrderStatusId <= 3 ) {
            //                 return true;
            //             } else if(scope.row.entity.PurchaseOrderStatusId == 4 && scope.row.entity.QtyGR <=  scope.row.entity.QtyPO ){
            //                 return true;
            //             } else if(scope.row.entity.PurchaseOrderStatusId === undefined){
            //                 return true
            //             } else {
            //                 return false;
            //             }
            //         }
            //     }
            //     },
            //     { name: 'Qty GR', field: 'QtyGR', displayName: 'Qty GR', width: '6%', enableCellEdit: false, cellFilter: 'number' },
            //     { name: 'Satuan', field: 'Satuan', width: '5%', enableCellEdit: false },
            //     { name: 'Standard Cost', field: 'UnitPrice', displayName: 'Standard Cost', cellFilter: 'number', enableCellEdit: false },
            //     // { name: 'Standart Cost NTNSB', field: 'UnitPrice', cellFilter: 'number' },
            //     {
            //         name: 'Diskon %',
            //         displayName: '%',
            //         field: 'DiscountPercent',
            //         width: '4%',
            //         enableCellEdit: true,
            //         headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:252%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon</td></tr></tbody></table></div>%</div>"
            //     },
            //     {
            //         name: 'Diskon Jumlah',
            //         displayName: 'Jumlah',
            //         field: 'DiscountAmount',
            //         width: '6%',
            //         enableCellEdit: false,
            //         cellFilter: 'number',
            //         headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>",
            //         // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.DiscountAmountResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
            //     },
            //     {
            //         displayName: 'PPN',
            //         name: 'PPN',
            //         field: 'VATAmount',
            //         // width: '7%',
            //         cellFilter: 'number',
            //         enableCellEdit: false
            //         // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.PPNResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
            //     },
            //     {
            //         name: 'Harga Total',
            //         field: 'TotalAmount',
            //         // width: '8%',
            //         enableCellEdit: false,
            //         cellFilter: 'number',
            //         // aggregationType: uiGridConstants.aggregationTypes.sum,
            //         //cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.NONTAMTotalAmount(row.entity.QtyPO, row.entity.UnitPrice, row) || number}}</div>',
            //         // footerCellTemplate: '<div class="ui-grid-cell-contents">{{col.getAggregationValue() | number }}</div>'
            //     },
            //     // { name: 'Action', cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>', visible : true
            //     // },
            //     { name: 'UomId', field: 'UomId', visible: false },
            //     { name: 'PartsClassId4', field: 'PartsClassId4', visible: false },
            //     // { 
            //     //     name: 'Status TPOS', 
            //     //     field: 'StatusTPOS', 
            //     //     displayName: 'Status TPOS', 
            //     //     cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.StatusTPOS">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
            //     //     // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.ToNameStatusTPOSdetail(row.entity.StatusTPOS)}}</div>',
            //     //     enableCellEdit: false,
            //     //     visible: $scope.falgStatusTPOS,
            //     // },
            //     // { 
            //     //     name: 'Alasan Reject', 
            //     //     field: 'ReasonTPOS', 
            //     //     enableCellEdit: true, 
            //     //     cellEditableCondition: true, 
            //     //     allowCellFocus: true, 
            //     //     visible: $scope.flagStatusTPOS,
            //     // }
            // ]
        };

        $scope.non_tam_non_sop = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 5,
            enableSelectAll: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.selection.on.rowSelectionChanged($scope,function(row) {
                    $scope.selectedRows = gridApi.selection.getSelectedRows();
                    // console.log("selected=>",$scope.selectedRows);
                    if($scope.onSelectRows){
                        $scope.onSelectRows($scope.selectedRows);
                    }
                });
                gridApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
                        $scope.selectedRows = gridApi.selection.getSelectedRows();
                        if($scope.onSelectRows){
                            $scope.onSelectRows($scope.selectedRows);
                        }
                });
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {

                    console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);

                    if ($scope.isEdit == true) {
                        rowEntity.LastModifiedDate = $scope.localeDate(new Date());
                        rowEntity.LastModifiedUserId = $scope.UserId;
                    }

                    if (rowEntity.UomPOId == 1) {
                        // alert('Box');
                        rowEntity.QtyBase = rowEntity.QtyPO * 10;
                    } else {
                        rowEntity.QtyBase = rowEntity.QtyPO;
                    }

                    // rowEntity.DiscountAmount = (rowEntity.DiscountPercent / 100) * (rowEntity.QtyPO * rowEntity.UnitPrice);
                    // rowEntity.VATAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) * 0.1;
                    // rowEntity.TotalAmount = ((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount) + rowEntity.VATAmount;

                    // imron 27okt2020
                    //var HargaSatuan = rowEntity.StandardCost / (1.1);
                    //var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
                    //rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo);
					// CR5 #10
					console.log('$scope.mData.IsNONPKP at $scope.non_tam_non_sop : ', $scope.mData.IsNONPKP);
					if($scope.mData.IsNONPKP == false){
						//PKP
						// var HargaSatuan = rowEntity.StandardCost / (1.1);
						var HargaSatuan = rowEntity.StandardCost / (1+(PPNPerc/100));

						var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
						rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo);


						// var VATSatuan = Math.round((HargaSatuan - DiscountSatuan) * 0.1);
						// var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
						var VATSatuan = (HargaSatuan - DiscountSatuan) * (PPNPerc/100);

						rowEntity.VATAmount = rowEntity.QtyPO * VATSatuan.toFixed($scope.roundTo);
						// rowEntity.UnitPrice = Math.round(rowEntity.StandardCost / 1.1);
						rowEntity.UnitPrice = HargaSatuan.toFixed($scope.roundTo);
					}else if($scope.mData.IsNONPKP == true){
						//Non PKP
						var HargaSatuan = rowEntity.StandardCost;
						var DiscountSatuan = (rowEntity.DiscountPercent / 100) * HargaSatuan;
						rowEntity.DiscountAmount = rowEntity.QtyPO * DiscountSatuan.toFixed($scope.roundTo);
						rowEntity.VATAmount = 0;
						rowEntity.UnitPrice = rowEntity.StandardCost;
					}
					// CR5 #10
                    //var VATSatuan = (HargaSatuan - DiscountSatuan) * 0.1;
                    //rowEntity.VATAmount = rowEntity.QtyPO * VATSatuan.toFixed($scope.roundTo);
                    rowEntity.TotalAmount =Math.round((rowEntity.QtyPO * rowEntity.UnitPrice) - rowEntity.DiscountAmount + rowEntity.VATAmount);

                    console.log(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);

                    var vTotalAmount = 0;
                    for (var i = 0; i < $scope.non_tam_non_sop.data.length; i++) {
                        vTotalAmount = vTotalAmount + $scope.non_tam_non_sop.data[i].TotalAmount;
                        $scope.mData.TotalAmount = vTotalAmount;
                    }


                });
            },
            cellEditableCondition: function(scope){
                if($scope.stateModeView){
                    return false;
                }else{
                    return true;
                }
            },
            columnDefs: 
            [{
                    field: 'PartsCode',
                    displayName: 'No. Material',
                    enableCellEdit: true,
                    width: '12%',
                    cellEditableCondition: function(scope){
                        if($scope.stateModeView){
                            return false;
                        }else{
                            return true;
                        }
                    },
                    cellTemplate: '<div><div class="input-group">\
                      <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.PartsCode" style="height:30px;" ng-disabled="grid.appScope.stateModeView" required>\
                      <label class="input-group-btn" ng-hide="grid.appScope.stateModeView">\
                          <span class="btn wbtn" ng-click="grid.appScope.SearchMaterial(row.entity.PartsCode)">\
                              <i class="fa fa-search fa-1"></i>\
                          </span>\
                      </label>\
                  </div></div>'
                },
                { name: 'Nama Material', sort: { priority: 0 }, field:"PartsName", width: '20%', enableCellEdit: false, cellTemplate: '<div class="ui-grid-cell-contents"><span>{{row.entity.PartsName}}</span></div>' },
                {
                    displayName: 'ETD',
                    name: 'ETD',
                    field: 'ETD',
                    width: '10%',
                    enableCellEdit: false,
                    cellTemplate: '<div><div class="input-group">\
                      <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.ETD" disabled>\
                     \
                  </div></div>'
                },
                {
                    displayName: 'ETA',
                    name: 'ETA',
                    field: 'ETA',
                    width: '10%',
                    enableCellEdit: false,
                    cellTemplate: '<div><div class="input-group">\
                      <input type="text" ng-model="MODEL_COL_FIELD" ng-value="row.entity.ETA" disabled>\
                     \
                  </div></div>'
                },
                {
                    name: 'Packaging Qty',
                    displayName: 'Qty',
                    field: 'QtyPO',
                    visible: false,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:202%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Packaging</td></tr></tbody></table></div>Qty</div>"
                },
                {
                    name: 'Packaging Satuan',
                    displayName: 'Satuan',
                    field: 'UomPOId',
                    visible: false,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Satuan</div>",
                    enableCellEdit: true,
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    cellFilter: 'mapAdj',
                    editDropdownValueLabel: 'adj',
                    editDropdownOptionsArray: [{ id: 1, adj: 'Box' }, { id: 4, adj: 'Pieces' }],
                    cellTemplate: '<div class="ui-grid-cell-contents" ng-model="row.entity.UomPOId">{{grid.appScope.$parent.ToNamePackage(row.entity.UomPOId)}}</div>',

                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.DiscountAmountResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
                },
                {
                    //NOTE : QTY TIDAK BOLEH LEBIH DARI QTY ORDER
                    name: 'Dasar Qty',
                    displayName: 'Qty',
                    field: 'QtyPO',
                    cellEditableCondition: function(scope){
                        if($scope.stateModeView){
                            return false;
                        }else{
                            return true;
                        }
                    },
                    width: '6%',
                    // headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:202%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Dasar</td></tr></tbody></table></div>Qty</div>"
                },
                {
                    name: 'Dasar Satuan',
                    displayName: 'Satuan',
                    field: 'Satuan',
                    width: '6%',
                    enableCellEdit: false,
                    // headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Satuan</div>",
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.DiscountAmountResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
                },
                { 
                    name: 'Standart Cost Packaging', 
                    displayName: 'Standart Cost',
                    field: 'UnitPrice', 
                    width: '8%',
                    enableCellEdit: false,
                    cellFilter: 'number' 
                },
                {
                    name: 'Diskon %',
                    displayName: '%',
                    width: '4%',
                    field: 'DiscountPercent',
                    enableCellEdit: true,
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'><div class='ui-grid-split-merge-header' style='width:277%'><table class='ui-grid-header-table'><tbody><tr><td colspan='2'>Diskon</td></tr></tbody></table></div>%</div>"
                },
                {
                    name: 'Diskon Jumlah',
                    displayName: 'Jumlah',
                    field: 'DiscountAmount',
                    width: '7%',
                    enableCellEdit: false,
                    cellFilter: 'number',
                    headerCellTemplate: "<div class='ui-grid-spilt-header-main'>Jumlah</div>",
                        // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.DiscountAmountResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
                },
                {
                    name: 'PPN',
                    displayName: 'PPN',
                    field: 'VATAmount',
                    width: '7%',
                    enableCellEdit: false,
                    cellFilter: 'number',
                        // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.PPNResult(row.entity.QtyPO, row.entity.UnitPrice, row.entity.DiscountPercent)}}</div>',
                },
                {
                    name: 'Harga Total',
                    displayname: 'Harga Total',
                    field: 'TotalAmount',
                    width: '8%',
                    enableCellEdit: false,
                    cellFilter: 'number',
                        // cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.NONTAMTotalAmount(row.entity.QtyPO, row.entity.UnitPrice, row)}}</div>',
                },
                // { name: 'Action', cellTemplate: '<center><a href="" ng-click="grid.appScope.$parent.Delete(row)"><u>Hapus</u></a></center>', visible : true
                // },
                { name: 'UomId', field: 'UomId', enableCellEdit: true, visible: false },
                { name: 'PartsClassId4', enableCellEdit: true, field: 'PartsClassId4', visible: false },
            ]
        };


        //---------------------------------
        //GRID SET UP
        //----------------------------------
        $scope.ToPOTypeId = function(POTypeId) {
            var reasonName;
            switch (POTypeId) {
                case 1:
                    reasonName = "PO TAM Tipe Order 1";
                    break;
                case 2:
                    reasonName = "PO TAM Tipe Order 2";
                    break;
                case 3:
                    reasonName = "PO TAM Tipe Order 3";
                    break;
                case 4:
                    reasonName = "PO NON TAM";
                    break;
                case 5:
                    reasonName = "PO SUBLET";
                    break;
            }
            return reasonName;
        }


        //----------------------------------
        // SDDN ETA ETD Date Time Picker Handler
        //----------------------------------
        $scope.dateTimeNow = function() {
            $scope.date = new Date();
        };

        $scope.dateTimeNow();

        $scope.toggleMinDate = function() {
            var minDate = new Date();
            var maxDate = new Date();
            // set to yesterday
            minDate.setDate(minDate.getDate() - 1);
            maxDate.setDate(maxDate.getDate() + 3);
            $scope.dateOptions.minDate = $scope.dateOptions.minDate ? null : minDate;
            //    $scope.dateOptions.maxDate = $scope.dateOptions.maxDate ? null : maxDate;
        };

        $scope.dateOptions = {
            showWeeks: false,
            startingDay: 0
        };

        $scope.toggleMinDate();

        // Disable weekend selection
        $scope.disabled = function(calendarDate, mode) {
            return mode === 'day' && (calendarDate.getDay() === 0 || calendarDate.getDay() === 6);
        };

        $scope.open = function($event, opened) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.dateOpened = true;
        };

        $scope.dateOpened = false;
        $scope.hourStep = 1;
        $scope.format = "dd-MMM-yyyy";
        $scope.minuteStep = 1;
        // add min-time="minTime" to datetimepicker to use this value
        $scope.minTime = new Date(0, 0, 0, Math.max(1, $scope.date.getHours() - 2), 0, 0, 0);

        $scope.timeOptions = {
            hourStep: [1, 2, 3],
            minuteStep: [1, 5, 10, 15, 25, 30]
        };

        $scope.showMeridian = true;
        $scope.timeToggleMode = function() {
            $scope.showMeridian = !$scope.showMeridian;
        };

        $scope.$watch("date", function(date) {
            // read date value
        }, true);

        $scope.resetHours = function() {
            $scope.date.setHours(1);
        };

        $scope.DateTimeFilter = function(datetime) {
            var newdatetime = $filter('date')(datetime, 'yyyy-MM-dd HH:mm', '+0700');
            return newdatetime;
        };

        $scope.onNoAppointmentResult = function() {
            console.log('Gk nemu nomor appointment na');
        };

        $scope.onNewSearchRefPONo = function($item, $model, $label) {
            console.log("onNewSearchRefPONo =>", $item);
            console.log("onNewSearchRefPONo =>", $model);
            console.log("onNewSearchRefPONo =>", $label);
            $scope.onSearchRefPONo($model);
        };
		
		$scope.resetGrid = function(){
			$scope.sublet_sop_wo.data = [];
			$scope.non_tam_sop_so.data = [];
			$scope.non_tam_sop_so_ubah.data = [];
			$scope.non_tam_non_sop.data = [];
		};
		
		$scope.recalcGrid = function(x){
		};

        $scope.getNoAppointment = function(key) {
            console.log('getNoAppointment', key);
            var val = PurchaseOrder.getListAppointmetNo(key).then(function(resu) {
                var newList = [];
                newList = resu.data.Result;
                console.log('getNoAppointment newList', newList);
                return newList;
            });
            return val;
        };

        $scope.onGotResult = function() {
            console.log("onGotResult=>");
            // $scope.disableDP = false;
        }
    
        $scope.resetRefPo = function(POTypeId){
            if(POTypeId == 6){
                $scope.Filter.RefPOTypeId = null;
            }
        }

        $scope.necessaryDateChange = function(){
            var dateNow = new Date();
                dateNow.setHours(0,0,0);

            var dateNecessaryDate = angular.copy($scope.mData.NecessaryDate)
                dateNecessaryDate.setHours(0,0,0);
                
            if(dateNecessaryDate > dateNow){
                $scope.mData.TireOrderId = 2 //booking
            }else{
                $scope.mData.TireOrderId = 1 //today
            }
        }

        $scope.ShowModalListCampaign = function(){
            $scope.dCampaign = {};
            $scope.showModalTambahListCampaign();
            $scope.actionSearchPartsCampaign();
            $scope.ModalTambahListCampaign_UIGrid.data = [];
        }

        $scope.ModalTambahListCampaign_UIGrid = {
            paginationPageSizes: [10, 50, 100],
			paginationPageSize: 10,
			useCustomPagination: false,
			useExternalPagination: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			canSelectRows: false,
			enableFiltering: true,
			enableSelectAll: false,
			multiSelect: false,
			enableFullRowSelection: false,
			columnDefs: [
                { name: "CampaignId", displayName: "CampaignId", field: "CampaignId", enableCellEdit: false, enableHiding: false, visible: false },
                { name: "ApplyBy", displayName: "ApplyBy", field: "ApplyBy", enableCellEdit: false, enableHiding: false, visible: false },
                { name: "QuotaType", displayName: "QuotaType", field: "QuotaType", enableCellEdit: false, enableHiding: false, visible: false },
                { name: "OutletId", displayName: "OutletId", field: "OutletId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "CampaignCode", displayName: "Campaign Code", field: "CampaignCode", enableCellEdit: false},
				{ name: "CampaignName", displayName: "Campaign Name", field: "CampaignName", enableCellEdit: false },
                { name: "CampaignType", displayName: "Campaign Type", field: "CampaignType", enableCellEdit: false },
                { name: "StartDate", displayName: "Start Date", field: "StartDate", enableCellEdit: false, cellFilter: 'date:\'yyyy-MM-dd\'' },
                { name: "EndDate", displayName: "End Date", field: "EndDate", enableCellEdit: false, cellFilter: 'date:\'yyyy-MM-dd\'' },
                { name: "DateStatus", displayName: "Date Status", field: "DateStatus", enableCellEdit: false },
                {
					name: "Action", displayName: "Action", field: "Action", enableCellEdit: false, visible: true,
					cellTemplate: '<div style="margin-top: 5px;padding-left: 5px;"><a ng-click="grid.appScope.DetailGrid_Campaign(row)">Detail</a></div>'
				}
			],

			onRegisterApi: function (gridApi) {
				$scope.ModalTambahListCampaign_UIGrid_gridAPI = gridApi;
			}
        }

        $scope.ModalTambahListPartsCampaign_UIGrid = {
            paginationPageSizes: [10, 50, 100],
			paginationPageSize: 10,
			useCustomPagination: false,
			useExternalPagination: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			canSelectRows: false,
			enableFiltering: true,
			enableSelectAll: false,
			multiSelect: false,
			enableFullRowSelection: false,
			columnDefs: [
				{ name: "CampaignId", displayName: "CampaignId", field: "CampaignId", enableCellEdit: false, enableHiding: false, visible: false },
                { name: "PartsId", displayName: "PartsId", field: "PartsId", enableCellEdit: false, enableHiding: false, visible: false },
				{ name: "PartsCode", displayName: "Parts Code", field: "PartsCode", enableCellEdit: false},
				{ name: "PartsName", displayName: "Parts Name", field: "PartsName", enableCellEdit: false },
                { name: "StatusQuota", displayName: "Status Qouta", field: "StatusQuota", enableCellEdit: false },
                { name: "OrderTypeId", displayName: "OrderTypeId", field: "OrderTypeId", enableCellEdit: false, visible: false },
                {
					name: "Action", displayName: "Action", field: "Action", enableCellEdit: false, visible: true,
					cellTemplate: '<div style="margin-top: 5px;padding-left: 5px;"><a ng-hide="grid.appScope.isBundle || row.entity.Actions == 0"  ng-click="grid.appScope.ActionPilihGrid_Campaign(row)">Pilih</a></div>'
				}
			],

			onRegisterApi: function (gridApi) {
				$scope.ModalTambahListPartsCampaign_gridAPI = gridApi;
			}
        }

        $scope.ModalCheckPartsCampaign_UIGrid = {
            paginationPageSizes: [10, 50, 100],
			paginationPageSize: 10,
			useCustomPagination: false,
			useExternalPagination: false,
			displaySelectionCheckbox: false,
			enableColumnResizing: true,
			canSelectRows: false,
			enableFiltering: true,
			enableSelectAll: false,
			multiSelect: false,
			enableFullRowSelection: false,
			columnDefs: [
				{ name: "CampaignCode", displayName: "Campaign Code", field: "CampaignCode", enableCellEdit: false},
				{ name: "CampaignName", displayName: "Campaign Name", field: "CampaignName", enableCellEdit: false },
                { name: "PeriodeCampaign", displayName: "Periode Campaign", field: "PeriodeCampaign", enableCellEdit: false },
				{ name: "PartsCodeOri", displayName: "Parts Code Ori", field: "PartsCodeOri", enableCellEdit: false },
			],

			onRegisterApi: function (gridApi) {
				$scope.ModalCheckPartsCampaign_gridAPI = gridApi;
			}
        }


        var defaultDataFilter;
        $scope.isBundle = false;
        $scope.DetailGrid_Campaign = function(row){
            $scope.nextCampaign = true;
            $scope.ngDialog.close();
            $scope.dtlCampaign = {};
            $scope.isBundle = row.entity.CampaignType.toLowerCase().includes('bundle') ? true : false;
            $scope.ModalTambahListPartsCampaign_UIGrid.data = [];
            PurchaseOrder.getListParstCampaign(row.entity.CampaignId).then(function(res) {
                $scope.ModalTambahListPartsCampaign_UIGrid.data = res.data;
                $scope.dtlCampaign = row.entity;
                defaultDataFilter = angular.copy($scope.ModalTambahListPartsCampaign_UIGrid.data);
                $scope.showModalTambahListPartsCampaign();
            },
            function(err) {
                console.log("err=>", err);
            });
        }

        $scope.actionclearModalTambahListCampaign = function(){
            $scope.non_tam_non_sop_bengkel.data = [];
            $scope.mData.QuotaType = "";
            $scope.mData.CampaignCode = "";
            $scope.mData.CampaignName = "";
            $scope.mData.ApplyBy = "";
            $scope.mData.CampaignType = "";
            $scope.CampaignIdNow = "";
            $scope.mData.tCampaign = "";
            $scope.checkRefGINo = true;
        }
        $scope.actionCloseModalTambahListCampaign = function(){
            $scope.ngDialog.close();
        }

        $scope.actionKembaliModalTambahListPartsCampaign = function(){
            $scope.ngDialog.close();
            $scope.showModalTambahListCampaign();
        }

        $scope.actionCloseModalTambahListPartsCampaign = function(){
            $scope.ngDialog.close();
        }

        $scope.CampaignIdNow = null;
        $scope.ActionPilihGrid_Campaign = function(data,orderType){
            $scope.mData.qtyGenerate = null;
            var tmpCampaginId = $scope.isBundle ? data : data.entity.CampaignId;

            if($scope.non_tam_non_sop_bengkel.data.length > 0){
                if(tmpCampaginId != $scope.CampaignIdNow){
                    $scope.ngDialog.close();
                    bsAlert.alert({
                        title: "Sudah terinput campaign "+$scope.mData.CampaignName,
                        text: "apakah akan mengahapus part campaign yang sebelumnya?",
                        type: "question",
                        showCancelButton: true
                    },
                    function () {
                        $scope.actionclearModalTambahListCampaign();
                        $scope.addCampaignToGrid(data,orderType);
                        $scope.showModalTambahListCampaign();
                    })
                }else{
                    $scope.addCampaignToGrid(data);
                }
            }else{
                $scope.non_tam_non_sop_bengkel.columnDefs[6].enableCellEdit = $scope.isBundle == true ? false : true;
                $scope.addCampaignToGrid(data);
            }
        }

        $scope.nextCampaign = true;
        $scope.addCampaignToGrid = function(data,orderType){
            $scope.non_tam_non_sop_bengkel.columnDefs[6].enableCellEdit = $scope.isBundle == true ? false : true;

            if($scope.isBundle){
                $scope.CampaignIdNow = data;
                $scope.mData.tCampaign = orderType == 7 ? 'C' : '3';
                for(var i in $scope.ModalTambahListPartsCampaign_UIGrid.data){
                    var data = {
                        PartsCode : $scope.ModalTambahListPartsCampaign_UIGrid.data[i].PartsCode,
                        CampaignId : $scope.ModalTambahListPartsCampaign_UIGrid.data[i].CampaignId,
                        isBundle : $scope.isBundle,
                        VendorId: $scope.mData.VendorId,
                        WarehouseId: $scope.mData.WarehouseId
                    }
                    if($scope.nextCampaign){
                        $scope.SearchMaterial(data);
                    }
                }
            }else{
                $scope.CampaignIdNow = data.entity.CampaignId;
                $scope.mData.tCampaign = data.entity.OrderTypeId == 7 ? 'C' : '3';
                var data = {
                    PartsCode : data.entity.PartsCode,
                    CampaignId : data.entity.CampaignId,
                    VendorId: $scope.mData.VendorId,
                    WarehouseId: $scope.mData.WarehouseId
                }
                $scope.SearchMaterial(data);
            }
            $scope.mData.QuotaType = $scope.dtlCampaign.QuotaType;
            $scope.mData.CampaignName = $scope.dtlCampaign.CampaignName;
            $scope.mData.CampaignCode = $scope.dtlCampaign.CampaignCode;
            $scope.mData.ApplyBy = $scope.dtlCampaign.ApplyBy;
            $scope.mData.CampaignType = $scope.dtlCampaign.CampaignType;
        }

        $scope.actionCheckPartsCampaignModal = function(){
            $scope.ngDialog.close();
            PurchaseOrder.getListCampaignUnknown().then(function(res) {
                $scope.showModalCheckPartsCampaign();
                $scope.ModalCheckPartsCampaign_UIGrid.data = res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
            );

        }
        $scope.actionCloseModalCheckPartsCampaign = function(){
            $scope.ngDialog.close();
            $scope.showModalTambahListCampaign();
        }

        $scope.dCampaign = {};

        $scope.actionSearchPartsCampaign = function(){
            
            PurchaseOrder.getCampaignOrder($scope.dCampaign).then(function(res) {
                $scope.ModalTambahListCampaign_UIGrid.data = res.data;
            },
            function(err) {
                    console.log("err=>", err);
            });
        }

        $scope.actionClearPartsCampaign = function(){
            $scope.dCampaign = {};
        }

        $scope.FilterDetailChange = function(data){
            $scope.ModalTambahListPartsCampaign_UIGrid.data = [];
            var tempType = '';
            if((data.filterType == '' || data.filterType == null || data.filterType == undefined) 
                || (data.filter == '' || data.filter == null || data.filter == undefined)){
                    $scope.ModalTambahListPartsCampaign_UIGrid.data = defaultDataFilter;
            }else{
                //var filterType = data.filterType;
                for(var i in defaultDataFilter){
                    if(data.filterType == 'PartsCode'){tempType = defaultDataFilter[i].PartsCode}
                    else if(data.filterType == 'PartsName'){tempType = defaultDataFilter[i].PartsName}
                    else if(data.filterType == 'StatusQuota'){tempType = defaultDataFilter[i].StatusQuota}

                    if(tempType.toLowerCase().includes(data.filter.toLowerCase())){
                        $scope.ModalTambahListPartsCampaign_UIGrid.data.push(defaultDataFilter[i]);
                    }
                }
            }
        }

        $scope.actionPilihSemuaItemCampaign = function(){
            $scope.mData.qtyGenerate = null;
            $scope.nextCampaign = true;
            if($scope.isBundle){
                for( var x in $scope.ModalTambahListPartsCampaign_UIGrid.data){
                    if($scope.ModalTambahListPartsCampaign_UIGrid.data[x].PartsId == null){
                        bsNotify.show({
                            title: "Purchase Order",
                            content: "Terdapat part yang belum termapping, mohon update part terlebih dahulu.",
                            type: 'danger'
                        });
                        break;
                    }
                }
                $scope.ActionPilihGrid_Campaign($scope.ModalTambahListPartsCampaign_UIGrid.data[0].CampaignId,$scope.ModalTambahListPartsCampaign_UIGrid.data[0].OrderTypeId);
                // $scope.mData.tCampaign = $scope.ModalTambahListPartsCampaign_UIGrid.data[0].OrderTypeId == 7 ? 'C' : '3';
            }
        }

        $scope.actionGenerateQtyBundle = function(qty){
            if(isNaN(qty) || qty <= 0){
                bsNotify.show({
                    title: "Purchase Order",
                    content: "Qty Bundle Tidak Valid",
                    type: 'warning'
                });
                return 0;
            }
            console.log('QTY', qty)
            for(var i in $scope.non_tam_non_sop_bengkel.data){
                $scope.non_tam_non_sop_bengkel.data[i].QtyPO = $scope.non_tam_non_sop_bengkel.data[i].QuotaQtyMin * qty;

                if($scope.non_tam_non_sop_bengkel.data[i].QtyPO > $scope.non_tam_non_sop_bengkel.data[i].QuotaQtyMax){
                    for(var i in $scope.non_tam_non_sop_bengkel.data){
                        $scope.non_tam_non_sop_bengkel.data[i].QtyPO = 0;
                    }
                    bsNotify.show({
                        title: "Purchase Order",
                        content: "Quantity PO lebih besar dari Quantity sisa Quota.",
                        type: 'danger'
                    });
                    return false;
                }
                $scope.gridApi.edit.raise.afterCellEdit($scope.non_tam_non_sop_bengkel.data[i],$scope.non_tam_non_sop_bengkel.columnDefs[6],$scope.non_tam_non_sop_bengkel.data[i].QtyPO,undefined,undefined);
            }
        }

        $scope.showModalTambahListCampaign = function(){
            ngDialog.openConfirm({
                template: '<div ng-include=\"\'app/parts/purchaseorder/non_tam_non_sop_bengkel/view_list_campaign.html\'\"></div>',
                plain: true,
                width: 800,
                scope: $scope,
                className: 'ngdialog-theme-plain custom-width',
            });
        }

        $scope.showModalTambahListPartsCampaign = function(){
            ngDialog.openConfirm({
                template: '<div ng-include=\"\'app/parts/purchaseorder/non_tam_non_sop_bengkel/view_list_parts_campaign.html\'\"></div>',
                plain: true,
                width: 700,
                scope: $scope,
                className: 'ngdialog-theme-plain custom-width',
            });
        }

        $scope.showModalCheckPartsCampaign = function(){
            ngDialog.openConfirm({
                template: '<div ng-include=\"\'app/parts/purchaseorder/non_tam_non_sop_bengkel/view_check_parts_campaign.html\'\"></div>',
                plain: true,
                width: 600,
                scope: $scope
            });
        }

        $scope.qtyGenerateChange = function(){

        }

    })

app.filter('getById', function() {
    return function(input, id) {
        var i = 0,
            len = input.length;
        for (; i < len; i++) {
            if (+input[i].id == +id) {
                return input[i];
            }
        }
        return null;
    }
});
