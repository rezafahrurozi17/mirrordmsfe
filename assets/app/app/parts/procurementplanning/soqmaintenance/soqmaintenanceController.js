angular.module('app')
    .controller('SOQMaintenanceController', function($scope, $http, CurrentUser, SOQMaintenance, ngDialog,
      PartsGlobal, PartsCurrentUser, $timeout, bsNotify, PurchaseOrder, PrintRpt, bsAlert) {
    //----------------------------------
    // Start-Up
    //----------------------------------

    $scope.ApproveData = SOQMaintenance.getApprovalData();

    $scope.$on('$viewContentLoaded', function() {
        $scope.loading = false;
        $scope.getParamater();
        var gridData;
        //$scope.gridData=[];
        $timeout(function(){
        var parts = $scope.checkRights(2);
        console.log("edit ?= ", parts);
        console.log('user=',$scope.user);
        console.log("Rights : ", $scope.myRights);
        // $scope.mSOQMaintenance['MaterialTypeId'] = $scope.materialtypes; //1; //$scope.getRightMaterialType();
        // $scope.mSOQMaintenance.ServiceTypeId = $scope.servicetypes; //1; //$scope.getRightServiceType();
        // console.log("M T : ", $scope.mSOQMaintenance.MaterialTypeId);
        // console.log("S T : ", $scope.mSOQMaintenance.ServiceTypeId);
        if (PartsGlobal.myWarehouse = {}) {
          // if ($scope.mSOQMaintenance.MaterialTypeId > 0 && $scope.mSOQMaintenance.ServiceTypeId > 0) {
          PartsCurrentUser.getWarehouseIdi().then(function(res) {
            console.log("res getWarehouseIdi==>>",res);
            console.log("res.data getWarehouseIdi==>>",res.data);
            $scope.mSOQMaintenance.MaterialTypeId = res.data[0].MaterialType;
            $scope.mSOQMaintenance.ServiceTypeId = res.data[0].Servicetype;
            console.log("M T : ", $scope.mSOQMaintenance.MaterialTypeId);
            console.log("S T : ", $scope.mSOQMaintenance.ServiceTypeId);
    
            // PartsGlobal.getMyWarehouse({ //getWarehouse
            PartsGlobal.getWarehouse({
              OutletId : $scope.user.OrgId, MaterialTypeId : 1, //$scope.mData.MaterialTypeId,
              ServiceTypeId : $scope.mSOQMaintenance.ServiceTypeId}).then(function(res) {
                var gridData = res.data.Result;
                console.log("warehouse ", gridData[0]);
                PartsGlobal.myWarehouses = gridData;
                PartsGlobal.myWarehouse = gridData[0];
                $scope.mSOQMaintenance.WarehouseId = gridData[0].WarehouseId;
                $scope.mSOQMaintenance.WarehouseName = gridData[0].WarehouseName;
                if ($scope.mSOQMaintenance.WarehouseId != null) {
                  console.log("Masuk sini");
                  // $scope.grid.data = $scope.getData(null);
                  $scope.getData(null);
                  console.log("Data Grid : ", $scope.grid.data);
                }
              },
              function(err) {
                console.log("err=>", err);
              });
          },
          function(err) {
            console.log("err=>", err);
          });
        } else {
          $scope.mSOQMaintenance.WarehouseId = PartsGlobal.myWarehouse.WarehouseId;
          $scope.mSOQMaintenance.WarehouseName = PartsGlobal.myWarehouse.WarehouseName;
        }
      });
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.ApprovalProcessId = 8130;
    $scope.StatusDesc = "";
    $scope.user = CurrentUser.user();
    $scope.checkRights=function(bit){
      console.log("Rights : ", $scope.myRights);
        var p= $scope.myRights & Math.pow(2,bit);
        var res=(p==Math.pow(2,bit));
        console.log("myRights => ", $scope.myRights);
        return res;
    }
    $scope.dataICC = [];
    $scope.dataSCC = [];

    $scope.mData = {}; //Model
    $scope.mData.GridDetail = {};
    console.log('$scope.mData: ',$scope.mData);

    $scope.getParamater = function(){
      SOQMaintenance.getDataICC().then(function(res){
        $scope.dataICC = res.data.Result;
        console.log("getDataICC===>",res.data.Result);
      });
      SOQMaintenance.getDataSCC().then(function(res){
        $scope.dataSCC = res.data.Result;
        console.log("getDataSCC===>",res.data.Result);

      });
    }
    $scope.getRightX= function(a, b){
      var i = 0;
      if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
      else if ($scope.checkRights(b)) i= 2;
      else if ($scope.checkRights(a)) i= 1;
      return i;
    }
    $scope.getRightMaterialType= function(){
      var i = $scope.getRightX(8, 9);
      return i;
    }
    $scope.getRightServiceType= function(){
      var i = $scope.getRightX(10, 11);
      return i;
    }
    $scope.getServiceAndMaterial= function(){
      PartsCurrentUser.GetServiceAndMaterial().then(function(res) {
          console.log("res==>>",res);
            $scope.materialtypes = res.data[0].MaterialType;
            $scope.servicetypes = res.data[0].Servicetype;
            console.log("$scope.materialtypes fn==>>",$scope.materialtypes);
            console.log("$scope.servicetypes fn==>>",$scope.servicetypes);
      });
    }
    $scope.Lastprocessed="";
    $scope.DateMIP="";
    // $scope.vLastprocessed= null;
    $scope.mSOQMaintenance ={SOQId: 2, PartsCode:"", PartsName: "", ICCid: "", SCCid: ""}; //Model
    // $scope.mMIPMaintenance = {MIPId: 2, PartsCode:"", PartsName: ""};//null; //Model
    $scope.xSOQaintenance = {
      selected: []
    };
    $scope.gridHideActionColumn=true;
    $scope.selectedRows={};
    $scope.itemids="";

    //$scope.xMIPMaintenance.selected=[];
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.hapusFilter= function() {
      $scope.mSOQMaintenance.ICCid=null;
      $scope.mSOQMaintenance.PartsCode= null;
      $scope.mSOQMaintenance.PartsName=null;
      $scope.mSOQMaintenance.SCCid=null;
      $scope.grid.data = $scope.getData(null);
    }
    //var gridData = [];

    $scope.CekStatusSOQ = false;
    $scope.CekStatusPO = false;

    $scope.getData = function(mSOQMaintenance) {
      var getWarehouse = true;
      console.log("PARAM GETDATA ", mSOQMaintenance);

      SOQMaintenance.getDataMIP($scope.user.OrgId,$scope.mSOQMaintenance.ServiceTypeId).then(function(res) {
        dataMIP = res.data.Result[0];
        console.log("dataMIP==>",dataMIP);
        if(dataMIP.MIPStatusId < 3) {
          bsAlert.alert({
            title: "MIP belum di-approve",
            text: "Harap selesaikan proses MIP terlebih dahulu, SOQ dapat dilakukan sehari setelah MIP di setujui",
            type: "info",
            showCancelButton: false
          },
          function() {

          });
        } else if (dataMIP.SOQId == 0) {
          bsAlert.alert({
            title: "SOQ belum di-proses",
            text: "Harap tunggu proses pembuatan SOQ melalui scheduler, atau proses SOQ secara manual",
            type: "info",
            showCancelButton: false
          },
          function() {

          });
        } else {

          // if (!!mMIPMaintenance) {mMIPMaintenance.MIPId = 1}  .
          if(mSOQMaintenance != null && ($scope.mSOQMaintenance.PartsCode == null || $scope.mSOQMaintenance.PartsCode =="") &&
            ($scope.mSOQMaintenance.PartsName == null || $scope.mSOQMaintenance.PartsName =="") &&
            ($scope.mSOQMaintenance.ICCid == null || $scope.mSOQMaintenance.ICCid =="") &&
            ($scope.mSOQMaintenance.SCCid == null || $scope.mSOQMaintenance.SCCid =="")
            )
            {
              PartsGlobal.showAlert(
                  {
                    title: "SOQ Maintenance",
                    content: "Filter tidak boleh kosong",
                    type: 'danger'
                  }
                  );
              return 0;
            } else {
                if (mSOQMaintenance != null && $scope.mSOQMaintenance.PartsCode != null) {
                  if ($scope.mSOQMaintenance.ICCid == null) $scope.mSOQMaintenance.ICCid = "" ;
                  if ($scope.mSOQMaintenance.SCCid == null) $scope.mSOQMaintenance.SCCid = "" ;
                  if ($scope.mSOQMaintenance.PartsName == null) $scope.mSOQMaintenance.PartsName = "" ;
                }
                // console.log("Get here param : ", mSOQMaintenance );
            }

          SOQMaintenance.getData(mSOQMaintenance).then(function(res) {
              gridData = res.data.Result;
              console.log(gridData);
              console.log("Testing");
              $scope.vdata = res.data.Result;  //gridData;
              console.log("Data vData :", $scope.vdata) ;
              if (gridData.length==0 ) {
              // if (res==[]) {
                $scope.grid.data = [];
              }else if (!gridData[0].PartsCode) {
                $scope.grid.data = [];
                $scope.Lastprocessed= gridData[0].DateProcessed;
                $scope.DateMIP= gridData[0].MIPDate;
                $scope.Status= gridData[0].SOQStatusId;
                if ($scope.Status == 1) { $scope.CekStatusSOQ = false} else { $scope.CekStatusSOQ = true };
                if ($scope.Status == 3) { $scope.CekStatusPO = false} else { $scope.CekStatusPO = true };
                console.log("$scope.Status==>",$scope.Status);
                console.log("$scope.CekStatusSOQ==>",$scope.CekStatusSOQ);
                console.log("$scope.CekStatusPO==>",$scope.CekStatusPO);
                console.log("$scope.DateMIP==>",$scope.DateMIP);

                // $scope.vLastprocessed = PartsGlobal.toDate($scope.Lastprocessed);
                //lkp_MIPStatusDocName: function(id) { //PartsLookup
                // $scope.Total_SAOriginal= gridData[0].Total_SAOriginal;
                // $scope.Total_SAAdjustment= gridData[0].Total_SAAdjustment;
                // $scope.MIPStatus= PartsLookup.lkp_MIPStatusDocName(gridData[0].MIPStatusId);
              } else {
                $scope.grid.data = gridData;
                $scope.Lastprocessed= gridData[0].DateProcessed;
                $scope.DateMIP= gridData[0].MIPDate;
                $scope.StatusDesc =  $scope.StatusSOQ(gridData[0].SOQStatusId);
                $scope.Status= gridData[0].SOQStatusId;
                if ($scope.Status == 1) { $scope.CekStatusSOQ = false} else { $scope.CekStatusSOQ = true };
                if ($scope.Status == 3) { $scope.CekStatusPO = false} else { $scope.CekStatusPO = true };
                console.log("$scope.Status2==>",$scope.Status);
                console.log("$scope.CekStatusSOQ2==>",$scope.CekStatusSOQ);
                console.log("$scope.CekStatusPO2==>",$scope.CekStatusPO);
                // $scope.vLastprocessed = PartsGlobal.toDate($scope.Lastprocessed);
                // $scope.Total_SAOriginal= gridData[0].Total_SAOriginal;
                // $scope.Total_SAAdjustment= gridData[0].Total_SAAdjustment;
                // $scope.MIPStatus= PartsLookup.lkp_MIPStatusDocName(gridData[0].MIPStatusId);
              }
              $scope.loading = false;
            },
            function(err) {
              console.log("err=>", err);
            }
          );
        }
      });
    }

     $scope.StatusSOQ = function(statusId){
       var StatusDesc;
        switch (statusId)
            {
               case 1:
               StatusDesc = "Draft";
               break;

               case 2:
               StatusDesc = "Request Approval";;
               break;

               case 3:
               StatusDesc = "Completed";;
               break;
            }
          return StatusDesc;
     }
    $scope.simpanDraft = function(){
      if ($scope.grid.data.length == 0) {
          PartsGlobal.showAlert(
              {
                title: "SOQ Maintenance",
                content: "Tidak ada data untuk disimpan",
                type: 'danger'
              }
              );
          return 0;
        }
      SOQMaintenance.updateData($scope.grid.data);

    }

    $scope.onBulkApproveSOQ = function(mData) {
        console.log("mData : ", mData);
        // mData[0].dataId = mData[0].MaterialRequestId;
        if (mData[0].SOQStatusId != 2) {
            PartsGlobal.showAlert({
                title: "SOQ Maintenance",
                content: "Status data bukan 'Request Approval'",
                type: 'danger'
            })
            return 0
        } else {
          $scope.ApproveSOQ(mData);
          SOQMaintenance.sendNotifToStaff().then(
            function(res) {
              console.log('terkirim ke staff partsman')
              console.log("data SOQ", data.dataSOQ)
            })
        }
    }

    $scope.ApproveSOQ = function(data){
      console.log('ApproveTS data = ', data);
      $scope.ApproveData = data;
      ngDialog.openConfirm ({
        template:'\
                     <div align="center" class="ngdialog-buttons">\
                     <p><b>Konfirmasi</b></p>\
                     <p>Approve SOQ ini?</p>\
                     <div class="ngdialog-buttons" align="center">\
                       <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                       <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinApprove(ApproveData)">Ya</button>\
                     </div>\
                     </div>',
        plain: true,
        // controller: 'SOQMaintenanceController',
        scope : $scope
       });
       console.log('$scope.ApproveData bawah = ', $scope.ApproveData);
    };

    $scope.yakinApprove = function(data){
      console.log('yakinApprove => ', data);
      $scope.ngDialog.close();
      for (var i = 0; i < data.length; i++) {
        data[i].SOQStatusId = 3;
      }
      SOQMaintenance.updateDataAdjusment(data).then(function(res) {
        console.log('cek log', res);
        if (res.data.ResponseCode == 23) {
          bsNotify.show(
            {
                title: "SOQ Maintenance",
                content: "Perhitungan SOQ Telah di approve",
                type: 'success'
            }
          );
        }else {
          bsNotify.show(
            {
                title: "SOQ Maintenance",
                content: "Perhitungan SOQ Gagal dikirim silahkan hubungi administrator",
                type: 'danger'
            }
          );
        }

      },
      function(err) {
        console.log("err=>", err);
      }
    );
    }
    // ======== tombol  simpan =========
        $scope.SimpanDanApprove = function() {


          console.log('ini data lagi masuk saveapprove', $scope.grid.data);
          console.log('ini ada ApproveData',$scope.ApproveData );
          $scope.ApproveData.dataSOQ = [];
          if ($scope.grid.data.length == 0) {
              PartsGlobal.showAlert({
                  title: "SOQ Maintenance",
                  content: "Tidak ada data untuk disimpan",
                  type: 'danger'
              });
              return 0;
          } else {
            if ($scope.grid.data[0].SOQStatusId == 1) {
              NeedApproval = 0;
              for (var i = 0; i < $scope.grid.data.length; i++) {
                console.log('masuk for',$scope.grid.data[i]);
                if ($scope.grid.data[i].SOQStatusId == 1) {
                  if ($scope.grid.data[i].QtySOQOriginal != $scope.grid.data[i].QtySOQAdjustment) {
                    NeedApproval = 1;
                    $scope.grid.data[i]['SOQStatusId'] = 2;
                    $scope.grid.data[i]['QtySOQAdjustment'] = parseInt($scope.grid.data[i]['QtySOQAdjustment']);
                    $scope.dataSOQ = $scope.grid.data[i];
                    $scope.ApproveData.dataSOQ.push($scope.dataSOQ);
                    console.log('$scope.ApproveData.dataSOQ', $scope.ApproveData.dataSOQ);    
                  }
                } else {
                  PartsGlobal.showAlert({
                      title: "SOQ Maintenance",
                      content: "Perhitungan SOQ sedang dalam proses request approval",
                      type: 'danger'
                  });
                }
              }

              if (NeedApproval == 1) {
                // $scope.grid.data[i]['SOQStatusId'] = 2;
                // $scope.grid.data[i]['QtySOQAdjustment'] = parseInt($scope.grid.data[i]['QtySOQAdjustment']);
                // $scope.dataSOQ = $scope.grid.data[i];
                // $scope.ApproveData.dataSOQ.push($scope.dataSOQ);
                // console.log('$scope.ApproveData.dataSOQ', $scope.ApproveData.dataSOQ);

                PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res) {
                  console.log("Updated : 04-01-2018 16:20",res);
                  $scope.ApproveData.ApprovalName = res.data.Result[0].Approvers;
                  $scope.ApproveData.DocDate = new Date();
                  $scope.ApproveData.pesan = 'Mohon untuk approve list dari item Perhitungan SOQ berikut.';
                  if ($scope.ApproveData.ApprovalName == null) {
                      bsNotify.show({
                          title: "SOQ Maintenance",
                          content: "Belum ada informasi pemberi Approval, mohon tambahkan terlebih dahulu",
                          type: 'danger'
                      });

                      return 0;
                  }

                  ngDialog.openConfirm ({
                    template:'<div ng-include=\"\'app/parts/procurementplanning/soqmaintenance/helper/approval.html\'\"></div>',
                    plain: true,
                    //controller: 'SOQMaintenanceController',
                    scope: $scope
                  });
                });
              } else {
                $scope.grid.data[0]['SOQStatusId'] = 3;
                // $scope.dataSOQ = $scope.grid.data[i];
                $scope.dataSOQ = $scope.grid.data;
                //$scope.ApproveData.dataSOQ.push($scope.dataSOQ);
                // SOQMaintenance.updateData($scope.dataSOQ);
                // SOQMaintenance.updateDataAdjusment($scope.dataSOQ);

                SOQMaintenance.updateDataAdjusment($scope.dataSOQ).then(function(res) {
                  console.log('cek log', res);
                  if (res.data.ResponseCode == 23) {
                    bsNotify.show(
                      {
                          title: "SOQ Maintenance",
                          content: "Perhitungan SOQ completed",
                          type: 'success'
                      }
                    );
                    $scope.getData();
                  }else {
                    bsNotify.show(
                      {
                          title: "SOQ Maintenance",
                          content: "Perhitungan SOQ Gagal disimpan silahkan hubungi administrator",
                          type: 'danger'
                      }
                    );
                  }
                });      
                      
              }

            } else {
              PartsGlobal.showAlert({
                  title: "SOQ Maintenance",
                  content: "Perhitungan SOQ sedang dalam proses request approval",
                  type: 'danger'
              });
            }
          }
        };


        $scope.SimpanApprovalSOQ = function(data)
        {
          $scope.grid.data = SOQMaintenance.selectedRows;
          console.log("$scope.grid.data untuk Approval ", $scope.grid.data);
          console.log("Data untuk Approval ", data);
          //SOQMaintenance.updateDataAdjusment(data.dataSOQ).then(function(res) {
          SOQMaintenance.updateDataAdjusment($scope.grid.data).then(function(res) {
            console.log('cek log', res);
            if (res.data.ResponseCode == 23) {
              bsNotify.show(
                {
                    title: "SOQ Maintenance",
                    content: "Perhitungan SOQ berhasil dikirim dan menunggu proses Approve Kabeng",
                    type: 'success'
                },
              SOQMaintenance.sendNotif().then(
                function(res) {
                  console.log('terkirim ke KABENG login 35')
                  console.log("data SOQ", data.dataSOQ)
                })
              );
              $scope.getData();
            }else {
              bsNotify.show(
                {
                    title: "SOQ Maintenance",
                    content: "Perhitungan SOQ Gagal dikirim silahkan hubungi administrator",
                    type: 'danger'
                }
              );
            }

          },
          function(err) {
            console.log("err=>", err);
          }
        );

          $scope.ngDialog.close();

        }


      $scope.RequestApprovalNext = function() {
            console.log("PartsGlobal.RequestApprovalSent : ", PartsGlobal.RequestApprovalSent);
            if (PartsGlobal.RequestApprovalSent) {
                // console.log("Current Header b4 b4: ", $scope.grid.data[$scope.CurrRowIndex]);
                // $scope.MIPStatus= PartsLookup.lkp_MIPStatusDocName(2);
                $scope.setNewStatus(2);
            }
        }

     $scope.setNewStatus = function(newStatus) {
                $scope.MIPStatus = PartsLookup.lkp_MIPStatusDocName(newStatus);
            }

    $scope.buatPO = function(){
      // if ($scope.selectedRows == {}) {
      var proses = true;
      console.log("$scope.grid.data[0]==>",$scope.grid.data[0]);
      if ($scope.grid.data[0].SOQStatusId < 3) {
        console.log("$scope.Status",$scope.Status);
        PartsGlobal.showAlert({
            title:'Status SOQ',
            content:'Status SOQ harus completed',
            type:'danger'
        })
      } else if (!$scope.selectedRows[0]) {
        console.log("No Generate 1");
        PartsGlobal.showAlert({
            title:'Buat PO',
            content:'Item harus dipilih dulu ',
            type:'danger'
        })
      } else {
        console.log("Last Processed : ", $scope.Lastprocessed);
        console.log("getCurrDate : ", PartsGlobal.getCurrDate());
        if ($scope.Lastprocessed != PartsGlobal.getCurrDate()) {
          PartsGlobal.showAlert({
            title:'Buat PO',
            content:'Tanggal Proses SOQ bukan Tanggal Hari ini',
            type:'info'
          })
        }
        console.log("$scope.selectedRows: ",$scope.selectedRows);
        for (var i=0; i<$scope.selectedRows.length; i++) {
          console.log("$scope.selectedRows["+i+"].QtySOQAdjustment: ",$scope.selectedRows[i].QtySOQAdjustment);
          if ($scope.selectedRows[i].QtySOQAdjustment == 0) {
            proses = false;
            PartsGlobal.showAlert({
              title:'Buat PO',
              content: $scope.selectedRows[i].PartsCode+" "+$scope.selectedRows[i].PartsName+
                "<br>SOQ Adjustment = "+$scope.selectedRows[i].QtySOQAdjustment+"<br>Proses tidak dapat dilanjutkan",
              type:'info'
            })  
            break;
          } 
        }
        if (proses == true ) {
          console.log("Generate");
          SOQMaintenance.selectedRows = $scope.selectedRows;//[0]; // dari oki
          ngDialog.openConfirm ({
          template:'\
                      <div class="ngdialog-buttons">\
                      <p><b>Konfirmasi</b></p>\
                      <p>Anda yakin ingin generate PO</p>\
                        <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                        <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="generatePO()">Ya</button>\
                      </div>',
          //templateUrl: '/app/parts/pomaintenance/alasan.html',
          plain: true,
          controller: 'SOQMaintenanceController',
          }); 
        }
      }
    };
    $scope.getSelectedItemIds= function(item) {
      var res = ';';
      for (var i=0; i<item.length; i++) {
        res += item[i].SOQItemId+';'; //.toString;
      }
      return res;
    };
    /*
    $scope.gridOptions.data = someData;
    $scope.gridApi.grid.modifyRows($scope.gridOptions.data);
    $scope.gridApi.selection.selectRow($scope.gridOptions.data[0]);
    */

    $scope.cetakOPM = function(PurchaseOrderId, OutletId) {
      console.log('PurchaseOrderId =', PurchaseOrderId);
      console.log('OutletId =', OutletId);
      //console.log('OutletId =', $scope.user.OrgId);
      //var data = $scope.mData;
      $scope.printOPM = 'as/PrintOrderPembelianMaterial/' + PurchaseOrderId + '/' + $scope.user.OrgId;
      $scope.cetakan($scope.printOPM);
    };

    $scope.cetakan = function(data) {
      var pdfFile = null;

      PrintRpt.print(data).success(function(res) {
          var file = new Blob([res], { type: 'application/pdf' });
          var fileURL = URL.createObjectURL(file);

          console.log("pdf", fileURL);
          //$scope.content = $sce.trustAsResourceUrl(fileURL);
          pdfFile = fileURL;

          if (pdfFile != null) {
              //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame
              var ua = navigator.userAgent;
              if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                  var link = document.createElement('a');
                  link.href = fileURL;
                  //link.download="erlangga_file.pdf";
                  link.click();
              }
              else {
                  printJS(pdfFile);
              }
          }
          else
              console.log("error cetakan", pdfFile);
      }).error(function(res) {
          console.log("error cetakan", pdfFile);
      });
    };

    $scope.alertTPOSResponse = function(item) {
      bsAlert.alert({
          title: "Response From TPOS Application",
          text: "DATA TIDAK DITEMUKAN / TIDAK DISIMPAN DI TPOS",
          type: "warning",
          showCancelButton: false
      },
      function() {
          $scope.goBack();
          $scope.resetForm();
      });
    }

    $scope.SumTotalAmount = function() {
      var totalAmount = 0;
      if ($scope.mData.GridDetail == null) {
      } else {
          //$scope.mData.TotalAmount = $scope.mData.GridDetail[i].TotalAmount;
          for (var i = 0; i < $scope.mData.GridDetail.length; i++) {
              //console.log("$scope.mData.GridDetail.length = ", $scope.mData.GridDetail.length);
              //console.log("$scope.mData.GridDetail[i].TotalAmount = ", $scope.mData.GridDetail[i].TotalAmount);
              totalAmount = totalAmount + $scope.mData.GridDetail[i].TotalAmount;
          }
          $scope.mData.TotalAmount = totalAmount;
      }
      console.log("totalAmountX ", totalAmount)
      return totalAmount;
    }

    $scope.alertAfterSavePO = function(item) {
      var nosb = item;
      var nosb = item.ResponseMessage;
      nosb = nosb.substring(nosb.lastIndexOf('#') + 1);
      bsAlert.alert({
              title: "Data tersimpan dengan Nomor Purchase Order",
              text: nosb,
              type: "success",
              showCancelButton: false
          },
          function() {

          })
    };

    $scope.generatePO = function(){
        $scope.ngDialog.close();
        // $scope.grid.data[0]['SOQItemIds'] = $scope.itemids;
        var vdata= [];
        $scope.grid.data = SOQMaintenance.selectedRows; // dari oki
        console.log("data [0] :", $scope.grid.data);
        console.log("data vData 2 :", $scope.vdata);
        if ($scope.grid.data == undefined) {
          // vdata = $scope.vdata;
          console.log("ada ? : ", $scope.vdata);
        }

        //check approval
        SOQMaintenance.checkApproval($scope.grid.data[0]).then(function(res){
          var result = res.data.Result;
          var flag = result[0].Flag_Approval;

          SOQMaintenance.setApprovalData(result[0],$scope.grid.data[0]);

          if(flag == 1){
            ngDialog.openConfirm ({
              template:'<div ng-include=\"\'app/parts/procurementplanning/soqmaintenance/helper/approvalPO.html\'\"></div>',
              //templateUrl: '/app/parts/pomaintenance/alasan.html',
              plain: true,
              controller: 'SOQMaintenanceController',
            });
          }else{
              // karena ini bukakn approval, po numbernya di null di handle di sp
              $scope.ApproveData.PONumber = "n/a";
              // SOQMaintenance.createPO($scope.grid.data[0],$scope.ApproveData.PONumber).then(function(res) {
              //     var gridData = res.data.Result;
              //     console.log("No PO : ", gridData);
              //     PartsGlobal.showAlert({
              //       title:'Buat PO',
              //       content:'Generate PO telah dilakukan, No PO : '+ gridData[0].Results,
              //       type:'info'
              //   })
              // });

              //HeaderPO
              SOQMaintenance.GetDataPO($scope.grid.data[0],0).then(function(res) {
                var Datares = res.data.Result;
                console.log("POHeader : ", Datares[0]);
                $scope.mData = Datares[0];
                console.log("$scope.mData : ", $scope.mData);

                //ItemPO
                SOQMaintenance.GetDataPO($scope.grid.data[0],1).then(function(res) {
                  var gridData = res.data.Result;
                  console.log("PODetail : ", gridData);
                  $scope.mData.GridDetail = gridData;
                  console.log("$scope.mData : ", $scope.mData);
                  $scope.SumTotalAmount();

                  PurchaseOrder.create($scope.mData, $scope.mData.GridDetail).then(function(res) {
                    var create = res.data.Response?res.data.Response:res.data;
                    console.log('RESSSS :', res.data);
                    if (create.ResponseCode == 19) {
                        $scope.alertTPOSResponse(create);
                    }else if(create.ResponseCode == 404){
                        bsAlert.alert({
                            title: create.ResponseMessage,
                            text:'',
                            type: "error",
                            showCancelButton: false
                        },
                        function() {
                           
                        }
                        )
                    } else {
                        var poId = create.ResponseMessage.substring(create.ResponseMessage.lastIndexOf('[') + 1 ,create.ResponseMessage.lastIndexOf(']'));
                        $scope.cetakOPM(poId,create.ResponseCode);
                        $scope.alertAfterSavePO(create);
                        // $scope.goBack();
                        // $scope.resetForm();
                    }
                    //$scope.alertAfterSave(create);
                    // bsNotify.show(
                    //   {
                    //       title: "Purchase Order ",
                    //       content: "Data berhasil disimpan",
                    //       type: 'success'
                    //   }
                    // );
                  },
                  function(err) {
                      console.log("err=>", err.data);
                  });
                });
              });
          }
        });
    }

    $scope.SimpanApproval = function(ApproveData){

      var gridSelected = ApproveData.gridSelected;

      SOQMaintenance.createPO(gridSelected,ApproveData.PONumber).then(function(res) {
          var gridData = res.data.Result;
          console.log("No PO : ", gridData);
          PartsGlobal.showAlert({
            title:'Buat PO',
            content:'Generate PO telah dilakukan, No PO : '+ gridData[0].Results,
            type:'info'
        })

        $scope.ngDialog.close();
       });

    }

    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
        $scope.selectedRows = rows;
        console.log("onSelectRows 2 =>", $scope.selectedRows);
        $scope.itemids= $scope.getSelectedItemIds($scope.selectedRows);
        console.log("Itemids : ", $scope.itemids);
        console.log(" Items ", $scope.selectedRows);
        SOQMaintenance.selectedRows = $scope.selectedRows;
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    var csTemplate = ' <input type="checkbox" ng-model="row.entity.CriticalStock > 0">';
    // var csTemplate = ' <input type="checkbox" value="true">';
    var cellEditable = function($scope) {
      if($scope.row.entity.SOQStatusId == 1) {
        return true;
      } else {
        return false;
      }
    }

    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        // onRegisterApi: function (gridApi) {
        //   $scope.gridApi = gridApi;
        //   gridApi.selection.on.rowSelectionChanged($scope,function(rows){
        //       $scope.mySelections = gridApi.selection.getSelectedRows();
        //       console.log("Item(s) : ", $scope.mySelections); // ...
        //       //build aray string
        //       $scope.itemids= $scope.getSelectedItemIds($scope.mySelections);
        //       console.log("item ids : ", $scope.itemids);
        //   });
        // },
        //showTreeExpandNoChildren: true,
        paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100,200],
        paginationPageSize: 20,

        columnDefs: [
            { name:'SOQId', width:100, field:'SOQId', visible: false }, //
            { name:'WarehouseId', width:100, field:'WarehouseId', visible: false }, //
            { name:'SOQItemId', width:100, field:'SOQItemId', visible: false },
            { name:'Critical Stock', width:60, displayName:'Critical Stock', field:'CriticalStock', type: 'boolean',
            cellTemplate: csTemplate, enableCellEdit: false},
            { name:'NO. Parts', width:100, displayName:'No. Parts', field:'PartsCode'},
            { name:'nama Parts', width:170, displayName:'Nama Parts', field:'PartsName'},
            { name:'Stock', width:80, displayName:'Stock', field:'PartsClassId4Name'},
            { name:'icc', width:100, displayName:'ICC', field:'ICC'},
            { name:'scc', width:120, displayName:'SCC', field:'SCC'},
            { name:'harga Retail', width:100, displayName:'Harga Retail', field:'RetailPrice', cellFilter : 'number'},
            { name:'landed Cost', width:100, displayName:'Landed Cost', field:'LandedCost', cellFilter : 'number'},
            { name:'dad', width:100, displayName:'DAD', field:'QtyDAD'},
            { name:'order Circle', width:100, displayName:'Order Circle', field:'QtyParamOrderCycle'},
            { name:'lead Time', width:100, displayName:'Lead Time', field:'QtyParamLeadTime'},
            { name:'ss Lead Time', width:100, displayName:'SS Lead Time', field:'QtyParamLeadTimeSS'},
            { name:'ss Demand', width:100, displayName:'SS Demand', field:'QtyParamDemandSS'},
            { name:'total Parameter', width:100, displayName:'Total Parameter', field:'QtyParamTotal'},
            { name:'mip', width:100, displayName:'MIP', field:'QtyMIP'},
            // { name:'qty Order', width:100, displayName:'Qty Order', field:'QtyOrder'},
            { name:'qty Free', width:100, displayName:'Qty Free', field:'QtyFree'},
            { name:'total OH', width:100, displayName:'Total OH', field:'QtyOnHand'},
            // { name:'qty On Order', width:100, displayName:'Qty On Order', field:'QtyOnOrder'},
            { name:'qty On Order', width:100, displayName:'Qty On Order (PO Non SOP)', field:'QtyOnOrder'},
            { name:'soq_original', width:100, displayName:'SOQ Original', field:'QtySOQOriginal'},
            { name:'QtyMaxPO', width:100, displayName:'QtyMaxPO', field:'QtyMaxPO', visible: false},
            // { name:'soq_adjustment', width:100, displayName:'SOQ Adjustment', field:'QtySOQAdjustment', enableCellEdit: true},
            { name:'soq_adjustment', width:100, displayName:'SOQ Adjustment', field:'QtySOQAdjustment', enableCellEdit: true, cellEditableCondition: cellEditable},
            { name:'satuan', width:100, field:'Satuan'}
        ]
        //- ,
        //- data:  $scope.getData(null)
    };

    $scope.msg = {};

    $scope.afterRegisterGridApi = function(gridApi){
      //console.log('gridApi = ', gridApi);
      $scope.gridApi = gridApi;

      $scope.gridApi.edit.on.afterCellEdit($scope,function(rowEntity, colDef, newValue, oldValue){
        console.log('rowEntity', rowEntity);
        console.log('colDef', colDef);
        console.log('newValue', newValue);
        console.log('oldValue', oldValue);
          //if(newValue != oldValue) rowEntity.State = "changed";
          if(colDef.field = "QtySOQAdjustment"){
            if(newValue != oldValue){
              if(rowEntity.QtyMaxPO !== null && rowEntity.QtyMaxPO !== 0){
                if(newValue > rowEntity.QtyMaxPO){
                  console.log('<newValue>', newValue);
                  console.log('rowEntity.QtyMaxPO', rowEntity.QtyMaxPO);
                  PartsGlobal.showAlert({
                      title:'Alert',
                      content:'Qty Adjustment Melebihi Qty PO',
                      type:'info'
                  });
                }
              } else {
                console.log('BetulKah', (rowEntity.QtyMaxPO !== null && rowEntity.QtyMaxPO !== 0));
                //console.log('not 0', rowEntity.QtyMaxPO !== 0);
              }
            };
          }; // end of if
      }); // end of afterCellEdit
    } // end of afterRegisterGridApi
})
.filter('mapBOTAM', function() {
    var BOTAMHash = {
        1: 'Fill',
        2: 'Kill'
    };

  return function(input) {
    if (!input){
      return '';
    } else {
      return BOTAMHash[input];
    }
  };
})
;
