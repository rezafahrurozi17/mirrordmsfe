angular.module('app')
  .factory('SOQMaintenance', function ($http, CurrentUser, PartsLookup, PartsGlobal) {
    var currentUser = CurrentUser.user;
    var elm;
    var approvalData = {};

    this.selectedRows = {};
    return {
      // getData: function() {
      //   // var res=$http.get('countries.json');
      //   // return res;
      // }
      getDataICC: function () {
        // icc
        var res = $http.get('/api/as/PartsClasses/GetPartsClassesFromType?ClassTypeId=2');
        // console.log('resnya employee=>',res);
        return res;
      },
      getDataSCC: function () {
        // icc
        var res = $http.get('/api/as/PartsClasses/GetPartsClassesFromType?ClassTypeId=3');
        // console.log('resnya employee=>',res);
        return res;
      },
      getData: function (data) {
        console.log("[Factory SOQ]");
        console.log(data);
        if (data == null) {
          var viccid = "";
          var vsccid = "";
          var whid = PartsGlobal.myWarehouse.WarehouseId; //1;
        } else {
          var viccid = PartsLookup.lkp_ICCId(data.ICCid);
          var vsccid = PartsLookup.lkp_SCCId(data.SCCid);
          var vcs = PartsLookup.lkp_truefalse(data.CriticalStock);
          console.log("CS ", data.CriticalStock, vcs);
        }
        // var viccid = "";
        var res = $http.get('/api/as/PartSOQ', {
          params: {
            soqid: (data == null ? "1" : data.SOQId),
            partscode: (data == null ? "" : data.PartsCode),
            partsname: (data == null ? "" : data.PartsName),
            iccid: (data == null ? "" : viccid), //data.ICCid),
            sccid: (data == null ? "" : vsccid), //data.SCCid)
            cs: (data == null ? 0 : vcs),
            whid: (data == null ? whid : data.WarehouseId)
          }
        });
        console.log('hasil=>', res);
        //res.data.Result = null;
        return res;
      },
      getApprovalData: function () {
        return approvalData;
      },
      setApprovalData: function (data, gridSelected) {
        approvalData = data;

        if (gridSelected)
          approvalData.gridSelected = gridSelected;
        //approvalData.DocDate = '2018-01-09 20:04:21.783';
        //approvalData.ApprovalName = 'Kepala Bengkel GR';
        //approvalData.PONumber ='214/POG/1801-000105';
      },
      sendNotif: function () {
        // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
        return $http.post('/api/as/SendNotification', [{
          Message: 'Ada Perhitungan SOQ mohon butuh di approve',
          RecepientId: 11600,  //USER ID KEBENG UNTUK DI KIRIM NOTIF
          Param: 6
        }]);
      },
      sendNotifToStaff: function () {
        // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
        return $http.post('/api/as/SendNotification', [{
          Message: 'Perhitungan SOQ telah disetujui oleh KABENG',
          RecepientId: 11590,  //USER ID Staf UNTUK DI KIRIM NOTIF
          Param: 6
        }]);
      },
      updateData: function (data) {
        console.log(data);
        var res = $http.put('/api/as/PartSOQ', [data]);
        return res;
      },
      updateDataAdjusment: function (data) {
        console.log(data);
        var res = $http.put('/api/as/PartSOQ', data);
        return res;
      },
      createPO: function (data, noPo) {
        // data['SOQItemIds'] = ';8;9;10;';
        console.log("data :", data); //api/as/PartSOQ/BuatPO
        console.log("item rows, :", this.selectedRows); //api/as/PartSOQ/BuatPO
        var itemids = ';';
        for (var i = 0; i < this.selectedRows.length; i++) {
          itemids += this.selectedRows[i].SOQItemId + ';'; //.toString;
        }
        console.log("items2 :", itemids);
        var res = $http.get('/api/as/PartSOQ/BuatPO', {
          params: {
            soqid: (data == null ? "1" : data.SOQId),
            soqitemids: itemids,
            pono: noPo || null
          }
        });
        return res;
      },
      GetDataPO: function (data, mode) {
        // data['SOQItemIds'] = ';8;9;10;';
        console.log("data :", data); //api/as/PartSOQ/BuatPO
        console.log("item rows, :", this.selectedRows); //api/as/PartSOQ/BuatPO
        var itemids = ';';
        for (var i = 0; i < this.selectedRows.length; i++) {
          itemids += this.selectedRows[i].SOQItemId + ';'; //.toString;
        }
        console.log("items2 :", itemids);
        var res = $http.get('/api/as/PartSOQ/GetDataPO', {
          params: {
            soqid: (data == null ? "1" : data.SOQId),
            soqitemids: itemids,
            mode: mode
          }
        });
        return res;
      },
      checkApproval: function (data) {

        var itemids = ';';
        for (var i = 0; i < this.selectedRows.length; i++) {
          itemids += this.selectedRows[i].SOQItemId + ';'; //.toString;
        }

        var res = $http.get('/api/as/PartSOQ/CheckForApproval', {
          params: {
            soqid: (data == null ? "1" : data.SOQId),
            soqitemids: itemids
          }
        }
        );
        return res;
      },
      getDataMIP: function (outletId, serviceTypeId) {
        var res = $http.get('/api/as/PartSOQ/GetDataMIP', {
          params: {
            OutletId: outletId,
            ServiceTypeId: serviceTypeId,
          }
        });
        return res;
      }
    }
  });
