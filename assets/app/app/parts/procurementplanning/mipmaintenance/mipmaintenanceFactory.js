angular.module('app')
    .factory('MIPMaintenance', function($http, CurrentUser, PartsGlobal) {
        var currentUser = CurrentUser.user();
        var elm;
        this.formApi = {};
        return {
            // getData: function() {
            //   // var res=$http.get('countries.json');
            //   // return res;
            // }
            getData: function(data) {
                console.log("[Factory MIP]");
                console.log(data);
                var tmpMIP = 1;
                var tmpData = null;
                // var whid =PartsGlobal.myServiceTypeId ;
                if (data == null) {
                    // var whid = PartsGlobal.myWarehouse.WarehouseId; //1;
                    var whid = PartsGlobal.myServiceTypeId;
                } else {
                    console.log("[PartsGlobal.myServiceTypeId]", PartsGlobal.myServiceTypeId);
                    // if (data != null) {
                    if (data.PartsCode == null) data.PartsCode = "";
                    if (data.PartsName == null) data.PartsName = "";
                    if (data.WarehouseId > 2) data.WarehouseId = PartsGlobal.myServiceTypeId;
                };

                console.log('curent user', currentUser);
                if (data == null) {
                    if (currentUser.RoleId == 1128 || currentUser.RoleId == 1129) {
                        tmpMIP = 1;                        
                    } else {
                        tmpMIP = 1
                    };

                } else {
                    tmpMIP = data.MIPId
                };
                if (currentUser.RoleId == 1123 || currentUser.RoleId == 1129) { //Partsman BP
                    tmpData = 0;
                } else if (currentUser.RoleId == 1122 || currentUser.RoleId == 1128) { //Partsman GR
                    tmpData = 1;
                };
                // if (data.WarehouseId > 2) data.WarehouseId = PartsGlobal.myServiceTypeId ;
                var res = $http.get('/api/as/PartMIP', {
                    params: {
                        mipid: (tmpMIP),
                        partscode: (data == null ? "" : data.PartsCode),
                        partsname: (data == null ? "" : data.PartsName),
                        whid: tmpData
                            // whid: (data == null ? whid : tmpData)
                            // whid: data== whid
                    }
                });
                console.log('hasil=>', res);
                //res.data.Result = null;
                return res;
            },
            updateData: function(data) {
                console.log(data);
                var res = $http.put('/api/as/PartMIP', data);
                return res;
            },
            actApproval: function(data) {
                console.log("Data approval : ", data);
                var res = $http.get('/api/as/PartGlobal/ActApproval', {
                    params: {
                        ProcessId: data.ProcessId,
                        DataId: data.DataId,
                        ApprovalStatus: data.ApprovalStatus,
                        tblName: "APart_ProcurementPlan_MIP_TT",
                        dataIdName: "MIPID",
                        colUpdate: data.colUpdate
                    }
                });
                return res;
            },
            sendNotifToStaff: function () {
                return $http.post('/api/as/SendNotification', [{
                  Message: 'Perhitungan MIP telah disetujui oleh KABENG',
                  RecepientId: 11590,  //USER ID Staf UNTUK DI KIRIM NOTIF
                  Param: 6
                }]);
            },        
        }
    });