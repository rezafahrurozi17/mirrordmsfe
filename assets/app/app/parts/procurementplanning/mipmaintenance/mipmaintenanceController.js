angular.module('app')
    .controller('MIPMaintenanceController', function($scope, $http, CurrentUser, MIPMaintenance, ngDialog, $timeout, PartsGlobal, PartsCurrentUser, PartsLookup, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            //$scope.gridData=[];
            // from here
            PartsGlobal.partVersion = "18.01.04.016.25";
            $timeout(function() {
                var parts = $scope.checkRights(2);
                console.log("edit ?= ", parts);
                console.log('user=', $scope.user);
                console.log("Rights : ", $scope.myRights);
                $scope.myRights = 1329;
                // $scope.mMIPMaintenance['MaterialTypeId'] = 1; //$scope.getRightMaterialType();
                // $scope.mMIPMaintenance.ServiceTypeId = $scope.getRightServiceType();
                // console.log("M T : ", $scope.mMIPMaintenance.MaterialTypeId);
                // console.log("S T : ", $scope.mMIPMaintenance.ServiceTypeId);
                console.log("FE Part Version : ", PartsGlobal.partVersion); //getPartVersion
                PartsGlobal.getPartVersion().then(function(res) {
                    // body...
                    var pVer = res.data.Result;
                    console.log("BE Part Version : ", pVer);
                });
                if (PartsGlobal.myWarehouse = {}) {
                    // if ($scope.mMIPMaintenance.MaterialTypeId > 0 && $scope.mMIPMaintenance.ServiceTypeId > 0) {
                    PartsCurrentUser.getWarehouseIdi().then(function(res) {
                        console.log("res getWarehouseIdi==>>",res);
                        console.log("res.data getWarehouseIdi==>>",res.data);
                        $scope.mMIPMaintenance.MaterialTypeId = res.data[0].MaterialType;
                        $scope.mMIPMaintenance.ServiceTypeId = res.data[0].Servicetype;
                        console.log("M T : ", $scope.mMIPMaintenance.MaterialTypeId);
                        console.log("S T : ", $scope.mMIPMaintenance.ServiceTypeId);
                
                        PartsGlobal.myServiceTypeId = $scope.mMIPMaintenance.ServiceTypeId;
                        PartsGlobal.getWarehouse({
                            OutletId: $scope.user.OrgId,
                            MaterialTypeId: 1, //$scope.mData.MaterialTypeId,
                            ServiceTypeId: $scope.mMIPMaintenance.ServiceTypeId
                        }).then(function(res) {
                                var gridData = res.data.Result;
                                console.log("warehouse ", gridData[0]);
                                PartsGlobal.myWarehouses = gridData;
                                PartsGlobal.myWarehouse = gridData[0];
                                $scope.mMIPMaintenance.WarehouseId = gridData[0].WarehouseId;
                                $scope.mMIPMaintenance.WarehouseName = gridData[0].WarehouseName;
                                if ($scope.mMIPMaintenance.WarehouseId != null) {
                                    $scope.grid.data = $scope.getData(null);
                                    console.log("Data Grid : ", $scope.grid.data);
                                }
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    },
                    function(err) {
                      console.log("err=>", err);
                    });       
                } else {
                    $scope.mMIPMaintenance.WarehouseId = PartsGlobal.myWarehouse.WarehouseId;
                    $scope.mMIPMaintenance.WarehouseName = PartsGlobal.myWarehouse.WarehouseName;
                }
            });
            // to here
        });
        //$scope.mData = {};
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.checkRights = function(bit) {
            console.log("Rights : ", $scope.myRights);
            var p = $scope.myRights & Math.pow(2, bit);
            var res = (p == Math.pow(2, bit));
            console.log("myRights => ", $scope.myRights);
            return res;
        }
        $scope.getRightX = function(a, b) {
            var i = 0;
            if (($scope.checkRights(a) && $scope.checkRights(b))) i = 3;
            else if ($scope.checkRights(b)) i = 2;
            else if ($scope.checkRights(a)) i = 1;
            return i;
        }
        $scope.getRightMaterialType = function() {
            var i = $scope.getRightX(8, 9);
            return i;
        }
        $scope.getRightServiceType = function() {
            var i = $scope.getRightX(10, 11);
            console.log('getRightServiceType', i);
            return i;
        }
        $scope.localeDate = function(data) {
            var tmpDate = new Date(data);
            var resDate = new Date(tmpDate.toISOString().replace("Z","-0700")).toISOString(); 
            return resDate;
        }
        $scope.Lastprocessed = "";
        $scope.mMIPMaintenance = { MIPId: 2, PartsCode: "", PartsName: "" }; //null; //Model
        $scope.MIPStatus = "";
        $scope.CekStatusMIP = false;
        $scope.xMIPMaintenance = {
            selected: []
        };
        $scope.gridHideActionColumn = true;
        $scope.selectedRows = {};
        $scope.itemids = "";
        $scope.stockPolicy = PartsLookup.lkp_StockPolicy();
        $scope.ApprovalProcessId = 8030;
        $scope.hapusFilter = function() {
            // $scope.mDataFilter.RefMRTypeId='';
            $scope.mMIPMaintenance.PartsCode = null;
            $scope.mMIPMaintenance.PartsName = null;
            $scope.grid.data = $scope.getData(null);
            // alert("Hapus filter");
        }
        $scope.onBulkApproveMIP = function(mData) {
            console.log("mData : ", mData);
            // mData[0].dataId = mData[0].MaterialRequestId;
            if (mData[0].MIPStatusId != 2) {
                PartsGlobal.showAlert({
                    title: "MIP Maintenance",
                    content: "Status data bukan 'Request Approval'",
                    type: 'danger'
                })
                return 0
            } 
            else {
                $scope.ApproveMIP(mData);
                MIPMaintenance.sendNotifToStaff().then(
                function(res) {
                    console.log('terkirim ke staff partsman')
                    // console.log("data MIP", data.dataSOQ)
                })
            } 

            // alert("Approve donk");
            // // PartsGlobal.actApproval({
            // MIPMaintenance.actApproval({
            //     ProcessId: $scope.ApprovalProcessId,
            //     DataId: mData[0].MIPId,
            //     ApprovalStatus: 1,
            //     colUpdate: ", MIPStatusId=3"
            // }).then(function(res) {
            //     $scope.setNewStatus(3);
            // })
        }
        $scope.ApproveMIP = function(data){
            console.log('ApproveTS data = ', data);
            $scope.ApproveData = data;
            ngDialog.openConfirm ({
              template:'\
                           <div align="center" class="ngdialog-buttons">\
                           <p><b>Konfirmasi</b></p>\
                           <p>Approve MIP ini?</p>\
                           <div class="ngdialog-buttons" align="center">\
                             <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                             <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="yakinApprove(ApproveData)">Ya</button>\
                           </div>\
                           </div>',
              plain: true,
              // controller: 'SOQMaintenanceController',
              scope : $scope
             });
             console.log('$scope.ApproveData bawah = ', $scope.ApproveData);
        };      
        $scope.yakinApprove = function(data){
            console.log('yakinApprove => ', data);
            $scope.ngDialog.close();
            MIPMaintenance.actApproval({
                ProcessId: $scope.ApprovalProcessId,
                DataId: data[0].MIPId,
                ApprovalStatus: 1,
                colUpdate: ", MIPStatusId=3"
            }).then(function(res) {
                $scope.setNewStatus(3);
            })
        }      
        $scope.onBulkRejectMIP = function(mData) {
                if (mData[0].MIPStatusId != 2) {
                    PartsGlobal.showAlert({
                        title: "MIP Maintenance",
                        content: "Status data bukan 'Request Approval'",
                        type: 'danger'
                    })
                    return 0
                }
                PartsGlobal.showAlert({
                    title: "MIP Maintenance",
                    content: "Data MIP ditolak, dan direset semula",
                    type: 'info'
                });
                MIPMaintenance.actApproval({
                    ProcessId: $scope.ApprovalProcessId,
                    DataId: mData[0].MIPId,
                    ApprovalStatus: 2,
                    colUpdate: ", MIPStatusId=1"
                }).then(function(res) {
                    $scope.setNewStatus(1);
                })
            }
            //$scope.xMIPMaintenance.selected=[];
            //----------------------------------
            // Get Data
            //----------------------------------
            //var gridData = [];
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
            $scope.selectedRows = rows;
            console.log("onSelectRows 2 =>", $scope.selectedRows);
            // $scope.itemids= $scope.getSelectedItemIds($scope.selectedRows);
            console.log("Itemids : ", $scope.itemids);
            console.log(" Items ", $scope.selectedRows);
            // SOQMaintenance.selectedRows = $scope.selectedRows;
        }
        $scope.setNewStatus = function(newStatus) {
                $scope.MIPStatus = PartsLookup.lkp_MIPStatusDocName(newStatus);
                console.log('setNewStatus',$scope.MIPStatus);
                $scope.getData();
            }
            //yap
            //ngubah string jadi currency
        $scope.formatIntoCurrencyString = function(str) {
            str = str.split(".");

            var tempManipulate = "";
            for (var i = str[0].length - 1; i >= 0; i--) tempManipulate += str[0][i]

            str[0] = "";

            for (i = 0; i < tempManipulate.length; i++) {

                if (i % 3 == 0 && i != 0)
                    str[0] += ",";

                str[0] += tempManipulate[i];

            }

            tempManipulate = "";
            for (var i = str[0].length - 1; i >= 0; i--) tempManipulate += str[0][i]

            str[0] = tempManipulate;

            // str = str.length != 1 ? str[0] + "." +str[1] :str[0];
            str = str[0]; // katanya koma belakangnya mau di ilangin aja...klo ga mau di ilangin uncomment yang atas ganti sama ini


            // str = str.replace(".",",");
            return str;
        }
        $scope.getData = function(mMIPMaintenance) {
            console.log("PARAM GETDATA ", mMIPMaintenance);
            // if (!!mMIPMaintenance) {mMIPMaintenance.MIPId = 1}  .
            if (mMIPMaintenance != null && ($scope.mMIPMaintenance.PartsCode == null || $scope.mMIPMaintenance.PartsCode == "") &&
                ($scope.mMIPMaintenance.PartsName == null || $scope.mMIPMaintenance.PartsName == "")) {
                PartsGlobal.showAlert({
                    title: "MIP Maintenance",
                    content: "Filter tidak boleh kosong",
                    type: 'danger'
                });
                return 0;
            };

            MIPMaintenance.getData(mMIPMaintenance).then(function(res) {
                    var gridData = res.data.Result;
                    console.log("cek grid list", gridData);
                    console.log("Testing MIP");
                    // if (res.length==0 ) {

                    if (gridData.length == 0) {
                        // if (res==[]) {
                        $scope.grid.data = [];
                    } else if (!gridData[0].PartsCode) {
                        $scope.grid.data = [];
                        // $scope.Lastprocessed= gridData[0].DateProcessed;
                        $scope.Lastprocessed = gridData[0].CreatedDate;
                        // $scope.Lastprocessed = $scope.localeDate(gridData[0].CreatedDate);
                        gridData[0].DateProcessed = $scope.localeDate(gridData[0].DateProcessed);
                        gridData[0].CreatedDate = $scope.localeDate(gridData[0].CreatedDate);

                        $scope.Total_SAOriginal = gridData[0].Total_SAOriginal;
                        $scope.Total_SAOriginal = $scope.formatIntoCurrencyString($scope.Total_SAOriginal.toString());

                        $scope.Total_SAAdjustment = gridData[0].Total_SAAdjustment;
                        $scope.Total_SAAdjustment = $scope.formatIntoCurrencyString($scope.Total_SAAdjustment.toString());

                        $scope.MIPStatus = PartsLookup.lkp_MIPStatusDocName(gridData[0].MIPStatusId);
                        if(gridData[0].MIPStatusId == 1) { $scope.CekStatusMIP = false } else { $scope.CekStatusMIP = true };

                    } else {
                        $scope.grid.data = gridData;
                        // $scope.Lastprocessed= gridData[0].DateProcessed;
                        $scope.Lastprocessed = gridData[0].CreatedDate;
                        // $scope.Lastprocessed = $scope.localeDate(gridData[0].CreatedDate);
                        gridData[0].DateProcessed = $scope.localeDate(gridData[0].DateProcessed);
                        gridData[0].CreatedDate = $scope.localeDate(gridData[0].CreatedDate);

                        $scope.Total_SAOriginal = gridData[0].Total_SAOriginal;
                        $scope.Total_SAOriginal = $scope.formatIntoCurrencyString($scope.Total_SAOriginal.toString());

                        $scope.Total_SAAdjustment = gridData[0].Total_SAAdjustment;
                        $scope.Total_SAAdjustment = $scope.formatIntoCurrencyString($scope.Total_SAAdjustment.toString());

                        $scope.MIPStatus = PartsLookup.lkp_MIPStatusDocName(gridData[0].MIPStatusId);
                        if(gridData[0].MIPStatusId == 1) { $scope.CekStatusMIP = false } else { $scope.CekStatusMIP = true };

                        _.map(gridData, function(val) {

                            if (val.StockingPolicyAdjustmentId == "15") {
                                val.StockingPolicyAdjustmentIdDesc = "Stock"
                            } else {
                                val.StockingPolicyAdjustmentIdDesc = "Non Stock"
                            }
                            // var dad = val.QtyDAD.toString();
                            // val.QtyDAD = dad[0];
                            // console.log("ini data", gridData);

                        })

                    }
                    $scope.loading = false;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            // $('#layoutContainer_MIPMaintenanceForm > .ui-layout-column').css("overflow-y","hidden");
            $('#layoutContainer_MIPMaintenanceForm ').css("min-height",$('.ui-grid-contents-wrapper').height()+50);
            $('#layoutContainer_MIPMaintenanceForm ').css("height","auto");
            // $('#layoutContainer_MIPMaintenanceForm').height(30*20);
        }
        $scope.simpan = function() {
            // ngDialog.openConfirm ({
            //   template:'<div class="panel panel-danger" style="border-color:#D53337;"><div class="panel-heading" style="color:white; background-color:#D53337;"><h3 class="panel-title">Permohonan Approval</h3></div> <div class="panel-body"><form method="post"><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nama Kegiatan</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">MIP Maintenance</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Nomor Request</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">-</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Tanggal dan Jam</label></div></div><div class="col-md-7"><div class="form-group"><label class="control-label">-</label></div></div></div> <div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Approve(s)</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4"></textarea></div></div> <br><div class="row"><div class="col-md-5"><div class="form-group"><label class="control-label">Pesan</label></div></div><div class="col-md-7"><textarea class="form-control" rows="4">Mohon untuk approve list dari item Perhitungan MIP berikut. Terima kasih.</textarea></div></div> <br><div class="row"><div class="col-md-12"><div class="form-group"><div> <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Kembali</button> <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="hello()" style="color:white; background-color:#D53337">OK</button> </div></div></div></div></form></div></div>',
            //   plain: true,
            //   controller: 'MIPMaintenanceController',
            //  });
            var str0 = 'MIP Maintenance '; //Procurement Planning'; //MIP Maintenance ini membutuhkan Approval
            if ($scope.grid.data.length == 0) {
                PartsGlobal.showAlert({
                    title: "MIP Maintenance",
                    content: "Tidak ada data untuk disimpan",
                    type: 'danger'
                });
                return 0;
            }
            if ($scope.grid.data[0]['MIPStatusId'] == 2) {
                PartsGlobal.showAlert({
                    title: "MIP Maintenance",
                    content: "Data sedang menunggu Approval",
                    type: 'danger'
                });
                return 0;
            }
            if ($scope.grid.data[0]['MIPStatusId'] == 3) {
                PartsGlobal.showAlert({
                    title: "MIP Maintenance",
                    content: "Status Data Completed",
                    type: 'danger'
                });
                return 0;
            }
            PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res) {
                console.log("Updated : 04-01-2018 16:20");
                $scope.Approver = res.data.Result[0].Approvers;
                if ($scope.Approver == null) {
                    bsNotify.show({
                        title: "MIP Maintenance",
                        content: "Belum ada informasi pemberi Approval, mohon tambahkan terlebih dahulu",
                        type: 'danger'
                    });

                    return 0;
                }
                // if ($scope.Approver == null)
                // {$scope.Approver = 'Approver X'} // data master belum diset
                $scope.grid.data[0]['MIPStatusId'] = 2;
                PartsGlobal.RequestApprovalSent = false;
                PartsGlobal.setReqAppSentPG(false);
                PartsGlobal.setdoProcAfterRequestApprovalSentPG(false);
                // if (PartsGlobal.showApproval({ // baris ini menggantikan kode di atas
                $scope.mMIPMaintenance['ApprovalStatus'] = -1;
                _.map($scope.grid.data, function(a) {
                    if (a.StockingPolicyAdjustmentIdDesc === 'Stock') {
                        a.StockingPolicyAdjustmentId = 15
                    }else {
                        a.StockingPolicyAdjustmentId = 47
                    }

                });
                PartsGlobal.showApproval01({
                    subtitle: str0 + ' ini',
                    kegiatan: str0,
                    // noreq: '-',
                    noreq: $scope.grid.data[0].MIPId.toString(),
                    // approver: 'Factory Judit, Factory Tini',
                    approver: $scope.Approver,
                    pesan: 'Mohon untuk approve list dari item Perhitungan MIP berikut. ',
                    controller: 'MIPMaintenanceController',
                    ProcessId: $scope.ApprovalProcessId,
                    userid: 0,
                    dataId: $scope.grid.data[0].MIPId,
                    headerData: $scope.grid.data[0],
                    detailData: $scope.grid.data,
                    procUpdateData: MIPMaintenance.updateData,
                    procNext: $scope.RequestApprovalNext
                });

            });

        };
        $scope.RequestApprovalNext = function() {
            console.log("PartsGlobal.RequestApprovalSent : ", PartsGlobal.RequestApprovalSent);
            if (PartsGlobal.RequestApprovalSent) {
                // console.log("Current Header b4 b4: ", $scope.grid.data[$scope.CurrRowIndex]);
                // $scope.MIPStatus= PartsLookup.lkp_MIPStatusDocName(2);
                $scope.setNewStatus(2);
            }

        }
        $scope.simpanDraft = function() {
            // alert('Hello World!');
            if ($scope.grid.data.length == 0) {
                PartsGlobal.showAlert({
                    title: "MIP Maintenance",
                    content: "Tidak ada data untuk disimpan",
                    type: 'danger'
                });
                return 0;
            }
            if ($scope.grid.data[0]['MIPStatusId'] == 2) {
                PartsGlobal.showAlert({
                    title: "MIP Maintenance",
                    content: "Data sedang menunggu Approval",
                    type: 'danger'
                });
                return 0;
            }
            // if ($scope.grid.data[0]['MIPStatusId'] == 3) {
            //   PartsGlobal.showAlert(
            //       {
            //         title: "MIP Maintenance",
            //         content: "Status Data Completed",
            //         type: 'danger'
            //       }
            //       );
            //   return 0;
            // }
            $scope.grid.data[0]['MIPStatusId'] = 1;
            MIPMaintenance.updateData($scope.grid.data);
            // if (!$scope.grid.data.sp_adjustment) {
            //     $scope.grid.data.StockingPolicyAdjustmentId = $scope.grid.data.sp_adjustment;
            // }
            // console.log($scope.grid.data.Result);
            console.log($scope.grid.data);
        }
        $scope.SPANonStock = function(data) {
            // console.log("SPA : ", data.StockingPolicyAdjustmentId);
            var res = '';
            if (data.StockingPolicyAdjustmentId == 4) {
                var index = $scope.grid.data.indexOf(data);
                $scope.grid.data[index]['QtyParamOrderCycle'] = 0;
                $scope.grid.data[index]['QtyParamLeadTime'] = 0;
                $scope.grid.data[index]['QtyParamLeadTimeSS'] = 0;
                $scope.grid.data[index]['QtyParamDemandSS'] = 0;
                $scope.grid.data[index]['QtyParamTotal'] = 0;
                $scope.grid.data[index]['QtyMIPOriginal'] = 0;
                $scope.grid.data[index]['QtyMIPAdjustment'] = 0;
                $scope.grid.data[index]['Satuan'] = "-";
                $scope.grid.data[index]['LandedCost'] = 0;
                res = "Non Stock";
            } else res = "Stock";
            return res;
        }
        $scope.SPANonStockEffect = function(data, val) {
            // console.log("SPA : ", data.StockingPolicyAdjustmentId);
            var result = val;
            if (data.StockingPolicyAdjustmentId == 4) {
                var index = $scope.grid.data.indexOf(data);
                result = 0;
                $scope.grid.data[index]['QtyParamOrderCycle'] = 0;
                $scope.grid.data[index]['QtyParamLeadTime'] = 0;
                $scope.grid.data[index]['QtyParamLeadTimeSS'] = 0;
                $scope.grid.data[index]['QtyParamDemandSS'] = 0;
                $scope.grid.data[index]['QtyParamTotal'] = 0;
                $scope.grid.data[index]['QtyMIPOriginal'] = 0;
                $scope.grid.data[index]['QtyMIPAdjustment'] = 0;
                $scope.grid.data[index]['Satuan'] = "";
                $scope.grid.data[index]['LandedCost'] = 0;
            }
            return result;
        }
        $scope.grid = {};

        //----------------------------------
        // Grid Setup
        //----------------------------------
        // var cellspa = '<div> <select class="form-control" ng-model="mData.sp_adjustment" ng-options="ref.name for ref in stockPolicy.name"> </select> </div>';
        //var cellspa = '<select class="form-control" ng-model="mData.sp_adjustment" ng-options="ref.name for ref in grid.appScope.$parent.stockPolicy.name"> </select>';
        // var cellSPA = '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.SPANonStock(row.entity)}}</div>';
        // var cellParOrderCycle = '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.SPANonStockEffect(row.entity, row.entity.QtyParamOrderCycle)}}</div>';

        var cellEditable = function($scope) {
            if($scope.row.entity.MIPStatusId == 1) {
              return true;
            } else {
              return false;
            }
        }
      
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            // enableVerticalScrollbar: 1, //0:Never 1:Always 2:WhenNeeded
            // showTreeExpandNoChildren: true,
            paginationPageSizes: [5, 10, 20, 50, 60, 70, 80, 90, 100, 200],
            paginationPageSize: 10,
            rowHeight: 55, 

            columnDefs: [
                    { name: 'MIPItemId', field: 'MIPItemId', visible: false },
                    { name: 'MIPId', field: 'MIPId', visible: false },
                    { name: 'No. Parts', width: 100, displayName: 'No. Parts', field: 'PartsCode', enableCellEdit: false },
                    { name: 'Nama Parts', width: 170, displayName: 'Nama Parts', field: 'PartsName', enableCellEdit: false },
                    { name: 'icc', displayName: 'ICC', width: 100, field: 'ICC', enableCellEdit: false },
                    { name: 'scc', displayName: 'SCC', width: 100, field: 'SCC', enableCellEdit: false },
                    { name: 'Stock Policy Original', width: 100, displayName: 'Stock Policy Original', field: 'sp_original', enableCellEdit: false },
                    {
                        name: 'Stock Policy Adjustment1',
                        width: 100,
                        displayName: 'Stock Policy Adjustment1',
                        field: 'sp_adjustment',
                        enableCellEdit: true,
                        visible: false //cellTemplate : cellspa
                            //;editableCellTemplate: 'ui-grid/dropdownEditor'//, //cellFilter: 'mapAdj',
                            //editDropdownValueLabel: 'adj',
                            // enableCellEdit: true, editableCellTemplate: 'ui-grid/dropdownEditor', cellFilter: 'mapAdj',
                            //editDropdownOptionsArray: ['Stock', 'Non Stock']
                            // editDropdownOptionsArray: [{ id: 3, adj: 'Stock' }, { id: 4, adj: 'Non Stock' }]
                    },
                    {
                        name: 'Stock Policy Adjustment',
                        field: 'StockingPolicyAdjustmentIdDesc', //visible: true,
                        width: 100,
                        displayName: 'Stock Policy Adjustment',
                        enableCellEdit: true, 
                        cellEditableCondition: cellEditable,
                        editableCellTemplate: 'ui-grid/dropdownEditor',
                        editDropdownValueLabel: 'adj',
                        editDropdownOptionsArray: [{ id: 'Stock', adj: 'Stock' }, { id: 'Non Stock', adj: 'Non Stock' }]
                        // ,
                        // cellTemplate: cellSPA
                    },
                    { name: 'On Hand', width: 100, displayName: 'On Hand', field: 'QtyOnHand', enableCellEdit: false },
                    { name: 'dad', width: 100, displayName: 'DAD', field: 'QtyDAD', enableCellEdit: false },
                    {
                        name: 'Order Circle',
                        width: 100,
                        displayName: 'Order Circle',
                        field: 'QtyParamOrderCycle',
                        enableCellEdit: false
                    }, //cellTemplate: cellParOrderCycle},

                    { name: 'Lead Time', width: 100, displayName: 'Lead Time', field: 'QtyParamLeadTime', enableCellEdit: false },
                    { name: 'SS Lead Time', width: 100, displayName: 'SS Lead Time', field: 'QtyParamLeadTimeSS', enableCellEdit: false },
                    { name: 'SS Demand', width: 100, displayName: 'SS Demand', field: 'QtyParamDemandSS', enableCellEdit: false },
                    { name: 'total', width: 100, field: 'QtyParamTotal', enableCellEdit: false },
                    { name: 'MIP Original', width: 100, displayName: 'MIP Original', field: 'QtyMIPOriginal', enableCellEdit: false },
                    { name: 'MIP Adjustment', width: 100, displayName: 'MIP Adjustment', field: 'QtyMIPAdjustment', enableCellEdit: true, cellEditableCondition: cellEditable },
                    { name: 'satuan', width: 100, field: 'Satuan', enableCellEdit: false },
                    {
                        name: 'landedCost',
                        width: 100,
                        displayName: 'Landed Cost',
                        field: 'LandedCost',
                        cellFilter: 'number',
                        cellClass: 'text-right',
                        enableCellEdit: false
                    },
                    {
                        name: 'Stock Day Original',
                        width: 100,
                        displayName: 'Stock Day Original',
                        field: 'QtyStockDayOriginal',
                        cellFilter: 'number',
                        cellClass: 'text-right',
                        enableCellEdit: false
                    },
                    {
                        name: 'Stock Day Adjustment',
                        width: 100,
                        displayName: 'Stock Day Adjustment',
                        field: 'QtyStockDayAdjustment',
                        cellFilter: 'number',
                        cellClass: 'text-right'
                    },
                    {
                        name: 'Stock Amount Original',
                        width: 100,
                        displayName: 'Stock MIP Amount Original',
                        field: 'QtyStockAmountOriginal',
                        cellFilter: 'number',
                        cellClass: 'text-right',
                        enableCellEdit: false
                    },
                    {
                        name: 'Stock Amount Adjustment',
                        width: 100,
                        displayName: 'Stock MIP Amount Adjustment',
                        field: 'QtyStockAmountAdjustment',
                        cellFilter: 'number',
                        cellClass: 'text-right'
                    },
                    {
                        name: 'Stock Amount',
                        width: 100,
                        displayName: 'Stock Amount',
                        field: 'StockAmount',
                        cellFilter: 'number',
                        cellClass: 'text-right',
                        enableCellEdit: false
                    }
                ]
                //-,
                //-data: $scope.getData(null)
        };
        $scope.msg = {};
        $scope.afterRegisterGridApi = function(gridApi) {
                //console.log('gridApi = ', gridApi);
                $scope.gridApi = gridApi;
                // ==================
                // gridApi.pagination.on.paginationChanged($scope, function(newPage, pageSize) {
                //     $scope.pageSize = pageSize+10;
                //     console.log("newPage==>",newPage);
                //     console.log("pageSize==>",pageSize);
                //     $('#layoutContainer_MIPMaintenanceForm').height($scope.pageSize*20);
                //     // $scope.totalPage = Math.ceil($scope.gridOptions.totalItems / $scope.pageSize);
                // });
                // =================
                $scope.gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                    console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    //if(newValue != oldValue) rowEntity.State = "changed";
                    if (colDef.field == "StockingPolicyAdjustmentId") { // if.
                        if (newValue != oldValue) {
                            if (newValue == 47) {
                                console.log('<newValue>', newValue);
                                rowEntity.QtyParamOrderCycle = 0;
                                rowEntity.QtyParamLeadTime = 0;
                                rowEntity.QtyParamLeadTimeSS = 0;
                                rowEntity.QtyParamDemandSS = 0;
                                rowEntity.QtyParamTotal = 0;
                                rowEntity.QtyMIPOriginal = 0;
                                rowEntity.QtyMIPAdjustment = 0;
                                rowEntity.Satuan = "";
                                rowEntity.LandedCost = 0;
                                rowEntity.QtyStockDayOriginal = 0;
                                rowEntity.QtyStockDayAdjustment = 0;
                                rowEntity.QtyStockAmountOriginal = 0;
                                rowEntity.QtyStockAmountAdjustment = 0;
                                rowEntity.StockAmount = 0;
                            }
                        };
                    } // end of if.
                    else if (colDef.field == "QtyMIPAdjustment") {
                        // @QtyStockDayAdjustment = ROUND(@QtyStockDayOriginal + ((@QtyMIPAdjustment - @QtyMIPOriginal)/@QTYDAD), 0)
                        if (rowEntity.QtyDAD > 0) {
                            rowEntity.QtyStockDayAdjustment = //111;
                                Math.ceil(rowEntity.QtyStockDayOriginal + ((rowEntity.QtyMIPAdjustment - rowEntity.QtyMIPOriginal) / rowEntity.QtyDAD));
                        }
                        // @QtyStockDayAdjustment * @LandedCost
                        rowEntity.QtyStockAmountAdjustment = rowEntity.QtyStockDayAdjustment * rowEntity.LandedCost;
                    }
                }); // end of afterCellEdit
            } // end of afterRegisterGridApi


    })
    .filter('mapAdj', function() {
        var adjHash = {
            15: 'Stock',
            47: 'NonStock'
        };

        return function(input) {
            if (!input) {
                return '';
            } else {
                return adjHash[input];
            }
        };
    });
