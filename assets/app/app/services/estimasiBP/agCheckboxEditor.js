function agCheckboxCellEditor() {}


function getCharCodeFromEvent(event) {
    event = event || window.event;
    return (typeof event.which == "undefined") ? event.keyCode : event.which;
}

function isCharNumeric(charStr) {
    return !!/\d/.test(charStr);
}

function isKeyPressedNumeric(event) {
    var charCode = getCharCodeFromEvent(event);
    var charStr = String.fromCharCode(charCode);
    return isCharNumeric(charStr);
}

// gets called once before the renderer is used
agCheckboxCellEditor.prototype.init = function (params) {
    // create the cell
    var value = params.value;

    this.eInput = document.createElement('input');
    this.eInput.type = 'checkbox';
    this.eInput.style.border = "none";
    this.eInput.style.margin = '0 20px';
    this.eInput.style.width = '100%!important';
    this.eInput.style.height = '100%';
    this.eInput.tabIndex = "0";
    // this.eInput.style.width = params.actualWidth;
    this.eInput.checked = value;
    this.eInput.value = value;

    // var that = this;
    // this.eInput.addEventListener('keypress', function (event) {
    //     if (!isKeyPressedNumeric(event)) {
    //         // console.log("that.eInput=>",that.eInput.children);
    //         // that.eInput.children[0].focus();
    //         that.eInput.focus();
    //         if (event.preventDefault) event.preventDefault();
    //     } else if (that.isKeyPressedNavigation(event)){
    //         event.stopPropagation();
    //     }
    // });

    // only start edit if key pressed is a number, not a letter
    // var charPressIsNotANumber = params.charPress && ('1234567890'.indexOf(params.charPress) < 0);
    // this.cancelBeforeStart = charPressIsNotANumber;
};

agCheckboxCellEditor.prototype.isKeyPressedNavigation = function (event){
    return event.keyCode===39
        || event.keyCode===37;
};


// gets called once when grid ready to insert the element
agCheckboxCellEditor.prototype.getGui = function () {
    return this.eInput;
};

// focus and select can be done after the gui is attached
agCheckboxCellEditor.prototype.afterGuiAttached = function () {
    this.eInput.focus();
    this.eInput.select();
};

// returns the new value after editing
agCheckboxCellEditor.prototype.isCancelBeforeStart = function () {
    return this.cancelBeforeStart;
};

// example - will reject the number if it contains the value 007
// - not very practical, but demonstrates the method.
agCheckboxCellEditor.prototype.isCancelAfterEnd = function () {
    // var value = this.getValue();
    // return value.indexOf('007') >= 0;
    return false;
};

// returns the new value after editing
agCheckboxCellEditor.prototype.getValue = function () {
    return this.eInput.checked;
};


agCheckboxCellEditor.prototype.toggleValue = function(){
  var value = this.getValue();
  this.eInput.value = !value;
  this.eInput.checked = !value;
}

// any cleanup we need to be done here
agCheckboxCellEditor.prototype.destroy = function () {
    // but this example is simple, no cleanup, we could  even leave this method out as it's optional
};

// if true, then this editor will appear in a popup
agCheckboxCellEditor.prototype.isPopup = function () {
    // and we could leave this method out also, false is the default
    return false;
};
