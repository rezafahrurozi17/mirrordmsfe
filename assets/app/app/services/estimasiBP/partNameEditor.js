var peScope,peCompile;
function initPartNameEditor($scope,$compile){
    // console.log("init partNameEditor=>");
    peScope = $scope;
    peCompile = $compile;
}

function PartNameEditor() {};
        PartNameEditor.prototype.init = function (params) {
            this.eGui = document.createElement('div');
            this.eGui.style.display = "inline-block";
            this.eGui.style.minWidth = "190px";
            this.eGui.style.width = "250px";
            this.eGui.style.border = "none";
            this.eGui.tabIndex = "0";
            // this.eGui.innerHTML = '<input style="width: 150px; float: left" type="text" class="form-control" '+
            //                       ' uib-datepicker-popup="{{format}}" ng-model="dt" is-open="popup.opened" datepicker-options="dateOptions" ' +
            //                       ' ng-required="true" close-text="Close"/>' +
            //                       '<button style="float: left" type="button" class="btn btn-default" ng-click="open()"><i class="glyphicon glyphicon-calendar"></i></button>';
            this.eGui.innerHTML =
                   '<bstypeahead class="form=control" style="float:left;min-width:190px;width:250px;" '+
                   '    ng-model="pcode" get-data="taPartCode_getPartCode" item-text="PartsName" '+
                   '    ta-minlength="4" ng-maxlength="250"      '+
                   '    placeholder="No. Material" name="namaMaterial" icon="fa-id-card-o" '+
                   '    loading="loadingUsers" selected="selected" '+
                   '    noresult="taPartCode_noResults" on-select="taPartCode_onSelectPartCode" on-noresult="taPartCode_onNoPartResult" on-gotresult="taPartCode_onGotResult" '+
                   '    template-url="customTemplate.html" '+
                   '> '+
                   '</bstypeahead> ';
            // this.eGui.innerHTML =
            //         '<bstypeahead placeholder="No. Material" name="noMaterial" ng-model="ldModel.PartsCode" get-data="getParts" item-text="PartsCode" ng-maxlength="250" disallow-space loading="loadingUsers" noresult="noResults" selected="selected" '+
            //             'on-select="onSelectParts"'+
            //             'on-noresult="onNoPartResult" on-gotresult="onGotResult" ta-minlength="4"'+ 
            //             // 'template-url="customTemplate.html"'+ 
            //             'icon="fa-id-card-o">'+
            //         '</bstypeahead>';

            // create and compile AngularJS scope for this component
            this.$scope = peScope.$new();
            peCompile(this.eGui)(this.$scope);
            this.$scope.params = params;

            var that = this;
            if (params.value) {
                this.$scope.pcode = params.value
            } else {
                this.$scope.pcode = null
            }
            // this.$scope.open = function () {
            //     that.$scope.popup.opened = true;
            // };
            // this.$scope.popup = {
            //     opened: false
            // };
            this.$scope.$watch('popup.opened', function (newVal, oldVal) {
                if (!newVal && oldVal) {
                    window.setTimeout(function () {
                        return that.$scope.params.stopEditing();
                    }, 0)
                }
            });
            this.eGui.addEventListener('keypress', function (event) {
                console.log("keypress event=>",event,that.eGui);
                    that.eGui.children[0].children[1].focus();
                    // if (event.preventDefault) event.preventDefault();
                    if (that.isKeyPressedNavigation(event)){
                        event.stopPropagation();
                    }
            });
            // this.eGui.addEventListener('keydown', function (event) {
            //     that.onKeyDown(event)
            // });

            // in case we are running outside of angular (ie in an ag-grid started VM turn)
            // we call $apply. we put in timeout in case we are inside apply already.
            window.setTimeout(this.$scope.$apply.bind(this.$scope), 0);
        };
        PartNameEditor.prototype.onKeyDown = function (event) {
            console.log("onKeydown event=>",event);
            var key = event.which || event.keyCode;
            if (key == 37 ||  // left
                key == 39) {  // right
                event.stopPropagation();
            }
        };
        PartNameEditor.prototype.isKeyPressedNavigation = function (event){
            return event.keyCode===39
                || event.keyCode===37;
        };
        PartNameEditor.prototype.afterGuiAttached = function () {
            console.log("children[0]=>",this.eGui.children[0].children);
            this.eGui.children[0].children[1].focus();
        };
        PartNameEditor.prototype.getGui = function () {
            return this.eGui;
        };
        PartNameEditor.prototype.destroy = function () {
            this.$scope.$destroy();
        };
        PartNameEditor.prototype.isPopup = function () {
            // and we could leave this method out also, false is the default
            return true;
        };
        PartNameEditor.prototype.getValue = function () {
            if (!this.$scope.pcode) {
                return null;
            }else{
                // this.$scope.SearchMaterialAg(this.$scope.pcode);
            }
            return this.$scope.pcode;
        };
