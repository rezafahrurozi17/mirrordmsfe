angular.module('app')
.controller('estimasiBPController', function ($scope, $http, $filter, CurrentUser, MasterDiscountFactory,AppointmentBpService,estimasiBPFactory, WOBP, $compile, GeneralMaster, $timeout, bsNotify, ASPricingEngine, bsAlert, $q, uiGridConstants, AppointmentGrService, PrintRpt, MrsList, WO) {
    //----------------------------------
        // Start-Up
        //----------------------------------
        var PPNPerc = 0;

        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = true;
            $scope.gridData = [];
            initPartEditor($scope, $compile);
            initPartNameEditor($scope, $compile);
            // initnoteCellEditor($scope, $compile);
            $scope.removeModal('ModalOP'); //CR4: ==> tambah ini
            $scope.getDataInsuranceCB();
            $scope.getVehicModeldariWO();
            // $scope.GetDataSPK();
            // $scope.ngakalinCalculate();

            $scope.getPPN()

        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user                    = CurrentUser.user();
        console.log('$scope.user', $scope.user);
        $scope.xRole                   = { selected  : [] };
        $scope.formApi                 = {};
        $scope.OutletGR                = 280;
        $scope.OutletBP                = 2000280;
        $scope.mEstimationBP           = {};
        $scope.disabledNo              = true;
        $scope.PriceAvailable          = true;
        $scope.filter                  = { AttendDate: null };
        $scope.filter.vType            = null;
        $scope.filter.vTypeVC          = null;
        $scope.PPN                     = 0;
        $scope.totalMaterialDiscounted = 0;
        $scope.totalWorkDiscounted     = 0;
        $scope.totalEstimasi           = 0;


        $scope.model_info_km = {}
        $scope.model_info_km.show = false;
        $scope.viewButtoninfokm = {save:{text:'Ok'}};

        $scope.Saveinfokm = function(){
            $scope.model_info_km.show = false;
        }
        $scope.Cancelinfokm = function(){
            $scope.model_info_km.show = false;
        }
        $scope.show_km_info = function() {
            $scope.model_info_km.show = true;
        }
        //----------------------------------
        // Get Data test
        //----------------------------------

        //function lifa
        $scope.gridActionTemplate = '<div class="ui-grid-cell-contents">' +
                // '<a href="#" ng-click="grid.appScope.$parent.cetakBiling(row.entity)" uib-tooltip="Cetak Billing" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-print fa-lg" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
                // '<a href="#" ng-show="row.entity.Job.Status == 18"  tooltip-placement="bottom"><i class="" style="padding:5px 8px 8px 0px;margin-left:40px;"></i></a>' +
                '<a href="#" ng-click="grid.appScope.actView(row.entity)" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:5px 8px 8px 0px;margin-left:30px;"></i></a>' +
                '<a href="#" ng-click="grid.appScope.actEdit(row.entity)" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:5px 8px 8px 0px;margin-left:10px;"></i></a>' +
                '<a href="#" ng-click="grid.appScope.$parent.printEstimationBtn(row.entity)" ng-hide="row.entity.Status == 2" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-print fa-lg" style="padding:5px 8px 8px 0px;margin-left:10px;"></i></a>' +

                // '<a href="#" ng-click="grid.appScope.$parent.rejectBilling(row.entity)" uib-tooltip="Reject Billing" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-id-card-o fa-lg" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
                '</div>';

        $scope.diskonData = [{
                MasterId: -1,
                Name: "Tidak Ada Diskon",
        }, {
                MasterId: 0,
                Name: "Custom Diskon",
        }];

        $scope.dataGoodWork = [];

        function compare(a, b) {
            // Use toUpperCase() to ignore character casing
            const nameA = a.Name.toUpperCase();
            const nameB = b.Name.toUpperCase();
          
            var comparison = 0;
            if (nameA > nameB) {
              comparison = 1;
            } else if (nameA < nameB) {
              comparison = -1;
            }
            return comparison;
        }

        var listSatuan = [];
        AppointmentGrService.getUnitMeasurement().then(function(res) {
            $scope.unitData = res.data.Result.sort(compare);
            console.log("$scope.unitData ",$scope.unitData );
            for(var i =0 ; i<$scope.unitData.length; i++){
                listSatuan.push($scope.unitData[i].Name);
            }
            console.log('listSatuan ===>',listSatuan);
        });


        $scope.getPPN = function (noNpwp) {
            if (noNpwp !== undefined && noNpwp !== null && noNpwp !== ''){
                noNpwp = noNpwp.toString();
                if (noNpwp.length === 20){
                    if (noNpwp !== '00.000.000.0-000.000'){
                        ASPricingEngine.getPPN(1, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    } else {
                        ASPricingEngine.getPPN(0, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    }
                } else {
                    ASPricingEngine.getPPN(0, 3).then(function (res) {
                        PPNPerc = res.data
                    });
                }

            } else {
                // param 1 adalah isnpwp, param 2 tipe ppn
                ASPricingEngine.getPPN(0, 3).then(function (res) {
                    PPNPerc = res.data
                });
            }
            
        };

        estimasiBPFactory.getT1().then(function(res) {
            $scope.TFirst = res.data.Result;
            //console.log("T1", $scope.T1);
        }),


        
          

        //CR4: ==> Tambah ini CR4
        $scope.removeModal = function(modalName){
        var el = angular.element('.ui.modal.'+ modalName);
        var len = el.length;
        console.log("modal Len=>",len,el);
        if (len > 0) {
            for(var i=0;i<len;i++){
            el[i].remove();
            }
        }
        }


        var ListPaymentName = [];
        AppointmentGrService.getPayment().then(function(res) {
            // pembayaran free service mau di ilangin di bp kata BU nya
            for (var j=0; j<res.data.Result.length; j++){
                if (res.data.Result[j].MasterId == 2277){
                    res.data.Result.splice(j,1);
                }
            }
            $scope.paymentData = res.data.Result;
            console.log("$scope.paymentData",$scope.paymentData);

            for(var i =0 ; i<$scope.paymentData.length; i++){
                ListPaymentName.push($scope.paymentData[i].Name);
            }
            console.log('ListPaymentName ===>',ListPaymentName);

        });

        //pas di grid depan
        $scope.printEstimationBtn = function(data){
            console.log("printEstimationBtn print",data);
            $scope.EstimationNo = "";
            //if( (data.Status == 1 || data.Status == 0) && data.JobId !== null && data.WONo !== null){
            if( data.StatusWO >= 4 && data.JobId !== null && data.WONo !== null){
                console.log('PrintMasterEstimationBPBaruClose');
                $scope.EstimationNo = data.EstimationNo +"_"+ data.PoliceNumber;
                // $scope.printAppoinmentBP = 'as/PrintMasterEstimationBPBaruClose/'+$scope.user.OutletId+'/'+data.EstimationBPId; //kalo dia edit & tambah job sttsnya close 
                $scope.printAppoinmentBP = 'as/PrintMasterEstimationBPBaruCloseSPK/'+$scope.user.OutletId+'/'+data.EstimationBPId; //kalo dia edit & tambah job sttsnya close 
            }else if(data.JobId !== null && data.WONo == null && data.IsMasterEstimation == 0){
                console.log('PrintMasterReceptionBpEstimasi');
                $scope.printAppoinmentBP = 'as/PrintMasterReceptionBpEstimasi/'+data.JobId+'/'+$scope.user.OutletId+'/0'; // tidak lanjut WO
            //}else if(data.Status == 0 && data.IsMasterEstimation == 1){ //statusnya open
            }else if( (data.StatusWO <= 3 || data.StatusWO == null) && data.IsMasterEstimation == 1){ //statusnya open
                console.log('PrintMasterEstimationBPBaru');
                $scope.EstimationNo = data.EstimationNo +"_"+ data.PoliceNumber;
                $scope.printAppoinmentBP = 'as/PrintMasterEstimationBPBaru/'+$scope.user.OutletId+'/'+data.EstimationBPId; // waktu create awal"
                console.log("wdjs",data);
            }
            $scope.cetakan($scope.printAppoinmentBP,data);
        }


        //pas klik btn Simpan
        $scope.EstimationNo = "";
        $scope.printEstimation = function(data){
            $scope.EstimationNo = "";
            console.log("printEstimation print",data);
            // if( (data.Status == 1 || data.Status == 0) && data.JobId !== null && data.WONo !== null){
            if( data.StatusWO >= 4 && data.JobId !== null && data.WONo !== null){
                console.log('PrintMasterEstimationBPBaruClose');
                $scope.EstimationNo = data.EstimationNo +"_"+ data.PoliceNumber;
                // $scope.printAppoinmentBP = 'as/PrintMasterEstimationBPBaruClose/'+$scope.user.OutletId+'/'+data.EstimationBPId; //kalo dia edit & tambah job sttsnya close 
                $scope.printAppoinmentBP = 'as/PrintMasterEstimationBPBaruCloseSPK/'+$scope.user.OutletId+'/'+data.EstimationBPId; //kalo dia edit & tambah job sttsnya close 
            }else if(data.JobId !== null && data.WONo == null && data.IsMasterEstimation == 0){
                console.log('PrintMasterReceptionBpEstimasi');
                $scope.printAppoinmentBP = 'as/PrintMasterReceptionBpEstimasi/'+data.JobId+'/'+$scope.user.OutletId+'/0'; // tidak lanjut WO
            // }else if(data.Status == 0 && data.IsMasterEstimation == 1){ //statusnya open
            }else if( (data.StatusWO <= 3 || data.StatusWO == null) && data.IsMasterEstimation == 1){ //statusnya open
                console.log('PrintMasterEstimationBPBaru');
                $scope.EstimationNo = data.EstimationNo +"_"+ data.PoliceNumber;
                $scope.printAppoinmentBP = 'as/PrintMasterEstimationBPBaru/'+$scope.user.OutletId+'/'+data.EstimationBPId; // waktu create awal"
            }
            $scope.cetakan($scope.printAppoinmentBP,data);
        }
        

        $scope.cetakan = function(data,mData) {
            var cetakanName = ""
            var EstimationNo = $scope.EstimationNo.replace(/\//g, '-');
            cetakanName = EstimationNo;

            console.log('cetakan | $scope.EstimationNo',$scope.EstimationNo);
            console.log('cetakan | cetakanName',cetakanName);
            console.log('cetakan | Param Data  ====>',data);
            console.log('cetakan | Param mData  ====>',mData);
            console.log('cetakan | Param Status ===>',mData.status);
            
            var pdfFile = null;
            //console.log('cetakan ====>',data.EstimationNo)
            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download= $scope.EstimationNo + ".pdf";

                        if(mData.status == 1 || mData.JobId != null){
                            link.download = "RevisiEstimasiBP_" + cetakanName + ".pdf";
                        }else{
                            link.download = "EstimasiBP_" + cetakanName + ".pdf";
                        }
                        link.click();
                    } else {
                        //printJS(pdfFile);
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download= $scope.EstimationNo + ".pdf";
                        if(mData.status == 1 || mData.JobId != null){
                            link.download = "RevisiEstimasiBP_" + cetakanName + ".pdf";
                        }else{
                            link.download = "EstimasiBP_" + cetakanName + ".pdf";
                        }                        link.click();
                        console.log("aaa", cetakanName)
                    }
                } else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };
        

        var customTaskName = null;
        $scope.getWork = function(key) {
            console.log("$scope.mDataCrm", $scope.mEstimationBP);
            // console.log("$scope.mEstimationBP.VehicleTypeId", $scope.mEstimationBP.VehicleTypeId);
            var VehicleType = $scope.mEstimationBP.VehicleTypeId;
            var VehicleModelId = 0;

            if ($scope.mEstimationBP.ModelCode != null && $scope.mEstimationBP.ModelCode != undefined) {
                VehicleModelId = $scope.mEstimationBP.ModelCode
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Vehicle Model Terlebih Dahulu",
                });
            }

            // if ($scope.mEstimationBP.VehicleTypeId.VehicleTypeId !== null && $scope.mEstimationBP.VehicleTypeId.VehicleTypeId !== undefined) {
            //     VehicleType = $scope.mEstimationBP.VehicleTypeId.VehicleTypeId;
            // } else {
            //     VehicleType = $scope.mEstimationBP.VehicleTypeId.VehicleTypeId;
            // }
            console.log("key",key);
            customTaskName = key;
            console.log("VehicleType", VehicleType);

            var iswarranty = undefined
            if ($scope.jobwarranty == true) {
                iswarranty = 1
            }

            if (VehicleType != null) {
                var ress = AppointmentBpService.getDataTask(key, VehicleType, VehicleModelId, iswarranty).then(function(resTask) {
                    return resTask.data.Result;
                });
                console.log("ress typing task", ress);
                return ress
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Tipe Kendaraan Terlebih Dahulu",
                });
            }
        };

        $scope.showSPK = true;
        // var disPartsDude = true;
        var modalFromPlus = null;
        $scope.plushOrder = function(){
            gridPartsToStart('#gridParts .ag-body-horizontal-scroll-viewport',500);
            console.log('$scope.mEstimationBP.isCash ===>',$scope.mEstimationBP.isCash);
            console.log('$scope.mEstimationBP.InsuranceId ===>',$scope.mEstimationBP.InsuranceId);

            $scope.jobwarranty = false
            $scope.mode_popup_job = 'new';

            if($scope.mEstimationBP.isCash != undefined){
                $scope.mEstimationBP.isCash = $scope.mEstimationBP.isCash.toString();
            }
            
            if($scope.mEstimationBP.isCash == undefined || $scope.mEstimationBP.isCash == null || $scope.mEstimationBP.isCash ==""){
                console.log('$scope.mEstimationBP.isCash dalem===>',$scope.mEstimationBP.isCash);
                console.log('$scope.mEstimationBP.InsuranceId dalem===>',$scope.mEstimationBP.InsuranceId);
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Tipe pembayaran Terlebih Dahulu",
                });
            }else{
                if(($scope.mEstimationBP.isCash == 0 || $scope.mEstimationBP.isCash == '0') && ($scope.mEstimationBP.InsuranceId == undefined || $scope.mEstimationBP.InsuranceId == null || $scope.mEstimationBP.InsuranceId == "")){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Mohon Input Asuransi Terlebih Dahulu",
                    });
                }else{
                    if($scope.mEstimationBP.KatashikiCode == undefined || $scope.mEstimationBP.KatashikiCode == null || $scope.mEstimationBP.KatashikiCode ==""){
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Mohon Input Tipe Kendaraan Terlebih Dahulu",
                        });
                    }else{
                        $scope.mEstimationBP.DP = globalDP;
                        $scope.masterParts = [];
                        $scope.mEstimationBP.TipeDisc = 0;
                        $scope.mEstimationBP.TaskName = null;
                        $scope.mEstimationBP.ProcessId = null;
                        $scope.mEstimationBP.NoSpk = null;
                        $scope.mEstimationBP.Fare = null;
                        $scope.mEstimationBP.TFirst1 = null;
                        $scope.mEstimationBP.FlatRate = null;

                        $scope.FlatRateAvailable_new = true

                        dataTask = {};
                        $scope.GridPartsApi.setRowData($scope.masterParts);
        
                        
                        modalFromPlus = true;
                        console.log('modalFromPlus ==>',modalFromPlus);
        
                        $scope.disJob = false;
                        flagUpdate = 0;
                        var numLength = angular.element('.ui.modal.ModalOP').length;
                        setTimeout(function () {
                            angular.element('.ui.modal.ModalOP').modal('refresh');
                        }, 0);
        
                        if (numLength > 1) {
                            angular.element('.ui.modal.ModalOP').not(':first').remove();
                        }
        
                        angular.element(".ui.modal.ModalOP").modal("show");
                        angular.element(".ui.modal.ModalOP").modal({
                            backdrop: 'static',
                            // keyboard: false
                            });
        
                        console.log('options SPK list | plushOrder ===>',$scope.optionsSpkList);
        
        
                        // $scope.cekJob = null;
                        // $scope.isRelease = false;
                        // $scope.FlatRateAvailable = false;
                        $scope.PriceAvailable = false;
                        // $scope.partsAvailable = false;
                        // $scope.whenGridReady();
                        console.log("$scope.mEstimationBP.isCash",$scope.mEstimationBP.isCash);
                        console.log('selectSatuan ===>',$scope.mEstimationBP.TipeDisc)
                        console.log('selectTipeDisc ===>',$scope.mEstimationBP.TipeDisc)
                        console.log('PaidById ===>',$scope.mEstimationBP.PaidById)
                
                        if($scope.mEstimationBP.isCash == 0 || $scope.mEstimationBP.isCash == '0'){
                            $scope.showSPK = true;
                            var isurance = 29;
                            $scope.selectTypeCust(isurance.toString());
                            // var DiscountTypeId = -1;
                            // $scope.selectDiscount(DiscountTypeId.toString());  
                            var satuan = 2428;
                            $scope.selectSatuan(satuan.toString());
                            $scope.mEstimationBP.DiskonPersen = selectedInsurance.DiscountJasa;
                        }else{
                            $scope.showSPK = false;;
                            var cash = 28;
                            $scope.selectTypeCust(cash.toString());  
                            var DiscountTypeId = -1;
                            $scope.selectDiscount(DiscountTypeId.toString());  
                            var satuan = 2428;
                            $scope.selectSatuan(satuan.toString());
                            $scope.mEstimationBP.DiskonPersen = 0;
                        }
        
                        // =======dari select work=======
                        var cuk = -1;
                        var cash = 28
                        var isurance = 29
                        if($scope.mEstimationBP.isCash == 1){
                            $scope.selectDiscount(cuk.toString());
                            $scope.selectTypeCust(cash.toString());
                        }else{
                            $scope.selectTypeCust(isurance.toString());
                        }
        


                        // kl tambah pekerjaan dengan kategori pekerjaan twc / pwc ----------------- start
                        if ($scope.mEstimationBP.CategoryId == 2653 || $scope.mEstimationBP.CategoryId == 2451) { // 2653 twc , 2451 pwc
                            var no_discount = -1
                            var default_pembayaran = 30
                            $scope.selectDiscount(no_discount.toString());
                            $scope.selectTypeCust(default_pembayaran.toString());
                            $scope.mEstimationBP.TFirst1 = null;

                        } 
                        // kl tambah pekerjaan dengan kategori pekerjaan twc / pwc ----------------- end


                        var satuan = null;
                        for (var i = 0; i < $scope.unitData.length; i++) {
                            if ($scope.unitData[i].Name == $item.UOM) {
                                $scope.mEstimationBP.ProcessId  = $scope.unitData[i].MasterId;
                                satuan = $scope.unitData[i].MasterId;
                                $scope.selectSatuan($scope.unitData[i].MasterId.toString());
                            }
                        }
                        console.log("satuan",satuan);
                        // =======dari select work=======
        
        
        
                        // console.log("disPartsDude",disPartsDude);
                        console.log("$scope.showSPK",$scope.showSPK);
                        
                        // $scope.mEstimationBP.TipeDisc = -1;
                        $scope.disJob = false;
                        console.log("masterParts",$scope.masterParts);
                        console.log("$scope.disJob",$scope.disJob);
                        // $scope.GridPartsApi.updateRowData();
                        
                        //error pas di prod
                        // dataTaskSave = [];


                        

                    }
                }
            }
            
           

        }

        $scope.closeModal = function(){

            if(tmpDeleteParts.length > 0){
                for(var x in tmpDeleteParts){
                    if(tmpDeleteParts[x].isCancel == 1){
                        tmpDeleteParts.splice(x,1);
                    }
                }
            }
            console.log('tmpDeleteParts', tmpDeleteParts);

            angular.element(".ui.modal.ModalOP").modal("hide");

            $scope.masterParts = [];
            $scope.mEstimationBP.TaskName = '';
            $scope.mEstimationBP.ProcessId = '';
            $scope.mEstimationBP.NoSpk = '';
            $scope.mEstimationBP.Fare = '';
            $scope.mEstimationBP.PaidById = '';
            $scope.temp_paidJob = angular.copy($scope.mEstimationBP.PaidById)
            $scope.mEstimationBP.TipeDisc = '';
            // $scope.mEstimationBP.DiskonPersen = '';
        }

        $scope.allowPatternFilter = function(event) {
            console.log("event allowPatternFilter", event);
            if (event.key == "Escape") {
                $scope.closeModal();
            }
            // var patternRegex
            // if (type == 1) {
            //     patternRegex = /\d/i; //NUMERIC ONLY
            // } else if (type == 2) {
            //     patternRegex = /\d|[a-z]|[*]/i; //ALPHANUMERIC ONLY
            //     if (item.includes("*")) {
            //         event.preventDefault();
            //         return false;
            //     }
            // } else if (type == 3) {
            //     patternRegex = /\d|[a-z]/i;
            // }
            // var keyCode = event.which || event.keyCode;
            // var keyCodeChar = String.fromCharCode(keyCode);
            // if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
            //     event.preventDefault();
            //     return false;
            // }
        };


        $scope.onGotResult = function() {
            console.log("onGotResult=>");
        }

        

        $scope.onNoResult = function() {
            console.log("gak", $scope.lmModel);
            var jmlPart = 0;
            console.log("$scope.cekJob",$scope.cekJob);
            if ($scope.cekJob != null && $scope.cekJob != undefined){
                AppointmentBpService.getDataPartsByTaskId($scope.cekJob).then(function(res) {
                    console.log("resabis ambil parts", res.data.Result);
                    jmlPart = res.data.Result.length;
                });
            }
            if (jmlPart > 0){
                $scope.listApi.clearDetail();
            }        

            // ga kepake ini ga pake bslist
            // var row = $scope.lmModel;
            // row.FlatRate = 1;
            // row.Fare = "";
            // row.Summary = "";
            // row.TaskId = null;
            // row.tmpTaskId = null;
            // // added by sss on 2017-09-19
            // selectedTaskBP = null;
            // //
            

            if ($scope.mEstimationBP.PaidById == 30) {
                $scope.masterParts = [];
                $scope.mEstimationBP.TipeDisc = 0;
                $scope.mEstimationBP.TaskName = null;
                // $scope.mEstimationBP.ProcessId = null;
                $scope.mEstimationBP.NoSpk = null;
                $scope.mEstimationBP.Fare = null;
                $scope.mEstimationBP.TFirst1 = null;
                $scope.mEstimationBP.FlatRate = null;

                $scope.mEstimationBP.BodyEstimationMinute = 0
                $scope.mEstimationBP.PutyEstimationMinute = 0
                $scope.mEstimationBP.SurfacerEstimationMinute = 0
                $scope.mEstimationBP.SprayingEstimationMinute = 0
                $scope.mEstimationBP.PolishingEstimationMinute = 0
                $scope.mEstimationBP.ReassemblyEstimationMinute = 0
                $scope.mEstimationBP.FIEstimationMinute = 0

                taskBPIdFromMaster = null

                $scope.mEstimationBP.PaidById = '29'
                $scope.temp_paidJob = angular.copy($scope.mEstimationBP.PaidById)

            }


            $scope.listApi.addDetail(false, row);
            console.log("uunnnnchhh", $scope.lmModel);
            $scope.partsAvailable = false;
            $scope.jobAvailable = false;
            $scope.PriceAvailable = false;
            $scope.FlatRateAvailable = false;
            $scope.FlatRateAvailable_new = true;
        }
        var dataTask;
        var taskBPIdFromMaster = null;
        var taskNameFromMaster = null;
        $scope.onSelectWork = function($item, $model, $label) {
            console.log("onSelectWork=>", $item);
            console.log("onSelectWork=>", $model);
            console.log("onSelectWork=>", $label);
            $item.PutyEstimationMinute = $item.PuttyEstimationMinute;
            dataTask = $item;

            taskBPIdFromMaster = $item.TaskListBPId;
            taskNameFromMaster =  $item.TaskName;

            console.log("taskBPIdFromMaster ====>", taskBPIdFromMaster);
            console.log("taskNameFromMaster ====>", taskNameFromMaster);
            
            var cuk = -1;
            var cash = 28
            var isurance = 29
            if ($scope.mEstimationBP.PaidById != 30) {
                if($scope.mEstimationBP.isCash == 1){
                    $scope.selectDiscount(cuk.toString());
                    $scope.selectTypeCust(cash.toString());
                }else{
                    $scope.selectTypeCust(isurance.toString());
                }
            } 
            

            console.log("dataTask",dataTask)
            if ($item.ServiceRate != null) {
                $item.Fare = $item.ServiceRate;
                $scope.PriceAvailable = true;
                // $item.Fare = $item.Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                // $item.FareMask = $item.Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');

                var satuan = null;
                for (var i = 0; i < $scope.unitData.length; i++) {
                    if ($scope.unitData[i].Name == $item.UOM) {
                        $scope.mEstimationBP.ProcessId  = $scope.unitData[i].MasterId;
                        satuan = $scope.unitData[i].MasterId;
                        $scope.selectSatuan($scope.unitData[i].MasterId.toString());
                    }
                }
                console.log("satuan",satuan);
                // $item.Unit = satuan;
                // $item.ProcessId = satuan;
                // $scope.mEstimationBP.ProcessId = toString(satuan);
                // $item.UnitBy = $item.UOM;
                $scope.mEstimationBP.Fare = $item.Fare;
                console.log("masuk atas oi");

            } else {

                if ($item.WarrantyRate != null) {
                    $item.Fare = $item.WarrantyRate
                    $scope.PriceAvailable = true;
                    $scope.mEstimationBP.Fare = $item.Fare;
                    $scope.mEstimationBP.FlatRate = $item.FlatRate;

                    if ($item.FlatRate == 0) {
                        $scope.FlatRateAvailable_new = false;
                    } else {
                        $scope.FlatRateAvailable_new = true;
                    }
                    $item.Fare_w = $item.WarrantyRate * $item.FlatRate;
                    $scope.mEstimationBP.Fare_w = $item.Fare_w

                } else {
                    $item.Fare = 0;
                    $scope.PriceAvailable = false;
                    $scope.mEstimationBP.Fare = $item.Fare;
                    console.log("masuk bawah oi");
                }
                
            }
            console.log("$item",$item);
            $scope.mEstimationBP.BodyEstimationMinute = $item.BodyEstimationMinute;
            $scope.mEstimationBP.PolishingEstimationMinute = $item.PolishingEstimationMinute;
            $scope.mEstimationBP.PutyEstimationMinute = $item.PuttyEstimationMinute; //ini emng double tt dari backend
            $scope.mEstimationBP.ReassemblyEstimationMinute = $item.ReassemblyEstimationMinute;
            $scope.mEstimationBP.SprayingEstimationMinute = $item.SprayingEstimationMinute;
            $scope.mEstimationBP.SurfacerEstimationMinute = $item.SurfacerEstimationMinute;
            $scope.mEstimationBP.FIEstimationMinute = $item.FIEstimationMinute;
            // selectedTaskBP = $item;
            // $scope.listApiJob.addDetail(false, $item);
            // $scope.sendWork($label, $item);
            console.log("modelnya", $scope.mEstimationBP);
        }

        $scope.checkServiceRate = function (data_fr, data, from_check) {
            console.log($scope.mEstimationBP)
            $scope.mEstimationBP.Fare_w = data_fr * $scope.mEstimationBP.Fare

        }

        $scope.sendWork = function(key, data) {
            var taskandParts = [];
            var taskList = {};
            AppointmentBpService.getDataPartsByTaskId(data.TaskListBPId).then(function(res) {
                console.log("resabis ambil parts", res.data.Result);
                taskandParts = res.data.Result;
                // added by sss on 2017-09-15
                // get fare bukan task list..
                // AppointmentBpService.getDataTaskListByKatashiki($scope.mDataDetail.KatashikiCode).then(function(restask){
                //     taskList=restask.data.Result;
                //     console.log("taskList",taskList);
                //     $scope.addDetail(data,taskandParts,taskList,$scope.lmModel)
                // });
                //
            });
        }
        // order pekerjaan

        $scope.disabledNo = true;
        $scope.PriceAvailable = true;
        // bs list reqpekerjaan
        $scope.listRequestSelectedRows = [];
        $scope.JobRequest = [];
        $scope.lmRequest = {};
        $scope.listApiRequest = {};
        $scope.listButtonSettings = { new: { enable: true, icon: "fa fa-fw fa-car" } };

        $scope.listApiJob = {};
        $scope.JobRequest = [];
        $scope.onBeforeSaveJobRequest = function (data, mode) {
            console.log("dataaa on save", data);
            console.log("mode on save", mode);
            console.log('lreq', $scope.lmRequest)
            console.log('litem', $scope.lmItem)
            
            // $scope.JobRequest.push(data);
            console.log('$scope.JobRequest', $scope.JobRequest);
        }

        $scope.onListSelectRows = function (rows) {
            console.log("form controller=>", rows);
        };
        // bs list grid work
        // $scope.gridWork = [];



        // $scope.onBeforeSave = function (data, mode) {
        //     console.log("onBeforeSave data", data);
        //     console.log("onBeforeSave mode", mode);
        //     var tmpData = angular.copy(data);
        //     var tmpGridWork = $scope.gridWork;
        //     var lengthGrid = tmpGridWork.length - 1;
        //     // if ($scope.tmpCus !== undefined) {
        //     //     tmpData.PaidBy = $scope.tmpCus.Name;
        //     // }
        //     _.map($scope.paymentData, function (val) {
        //         if (val.MasterId == tmpData.PaidById) {
        //             tmpData.PaidBy = val.Name;
        //         }
        //     });
        //     tmpData.Summary = Math.round(tmpData.Fare * tmpData.FlatRate);

        //     // tmpData.PaidBy = $scope.tmpCus.Name;
        //     if (mode == 'new') {
        //         if (tmpData.tmpTaskId == null) {
        //             if (tmpGridWork.length > 0) {
        //                 tmpData.tmpTaskId = tmpGridWork[lengthGrid].tmpTaskId + 1;
        //             } else {
        //                 tmpData.tmpTaskId = 1;
        //             }
        //         }
        //         if (data.child.length > 0) {
        //             var tmpDataChild = angular.copy(data.child);
        //             for (var i = 0; i < tmpDataChild.length; i++) {
        //                 if (tmpDataChild[i].PaidById == 2277 || tmpDataChild[i].PaidById == 30 || tmpDataChild[i].PaidById == 31) {
        //                     tmpDataChild[i].minimalDp = 0;
        //                 }
        //                 tmpDataChild[i].DPRequest = Math.round(tmpDataChild[i].DownPayment);
        //                 delete data.child[i];
        //             }
        //             $scope.listApi.addDetail(tmpDataChild, tmpData);
        //         } else {
        //             $scope.listApi.addDetail(false, tmpData);
        //         };
        //     } else {
        //         if (data.child.length > 0) {
        //             var tmpDataChild = angular.copy(data.child);
        //             for (var i = 0; i < tmpDataChild.length; i++) {
        //                 if (tmpDataChild[i].PaidById == 2277 || tmpDataChild[i].PaidById == 30 || tmpDataChild[i].PaidById == 31) {
        //                     tmpDataChild[i].minimalDp = 0;
        //                 }
        //                 tmpDataChild[i].DPRequest = Math.round(tmpDataChild[i].DownPayment);
        //                 delete data.child[i];
        //             }
        //             $scope.listApi.addDetail(tmpDataChild, tmpData);
        //         } else {
        //             $scope.listApi.addDetail(false, tmpData);
        //         };
        //     }
        // }


        // $scope.onAfterDelete = function () {
        //     var totalW = 0;
        //     for (var i = 0; i < $scope.gridWork.length; i++) {
        //         if ($scope.gridWork[i].Summary !== undefined) {
        //             if ($scope.gridWork[i].Summary == "") {
        //                 $scope.gridWork[i].Summary = $scope.gridWork[i].Fare;
        //             }
        //             totalW += $scope.gridWork[i].Summary;
        //         }
        //     };
        //     $scope.totalWork = totalW / (1.1);  //request pak dodi dibagi 1.1 13/02/2019
        //     // ============
        //     var totalM = 0;
        //     for (var i = 0; i < $scope.gridWork.length; i++) {
        //         if ($scope.gridWork[i].child !== undefined) {
        //             for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
        //                 if ($scope.gridWork[i].child[j].subTotal !== undefined) {
        //                     totalM += $scope.gridWork[i].child[j].subTotal;
        //                     console.log("$scope.gridWork[i].child.Price", $scope.gridWork[i].child[j].subTotal);
        //                 }
        //             }
        //         }
        //     }
        //     $scope.totalMaterial = totalM / (1.1);  //request pak dodi dibagi 1.1 13/02/2019
        //     // ============
        //     var totalDp = 0;
        //     for (var i = 0; i < $scope.gridWork.length; i++) {
        //         if ($scope.gridWork[i].child !== undefined) {
        //             for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
        //                 if ($scope.gridWork[i].child[j].DownPayment !== undefined) {
        //                     totalDp += $scope.gridWork[i].child[j].DownPayment;
        //                     console.log("$scope.gridWork[i].child.Price", $scope.gridWork[i].child[j].DownPayment);
        //                 }
        //             }
        //         }
        //     }
        //     $scope.totalDp = totalDp;
        //     var totalDefaultDp = 0;
        //     for (var i = 0; i < $scope.gridWork.length; i++) {
        //         if ($scope.gridWork[i].child !== undefined) {
        //             for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
        //                 if ($scope.gridWork[i].child[j].minimalDp !== undefined) {
        //                     totalDefaultDp += $scope.gridWork[i].child[j].minimalDp;
        //                     console.log("$scope.gridWork[i].child.Price", $scope.gridWork[i].child[j].minimalDp);
        //                 }
        //             }
        //         }
        //     }
        //     $scope.totalDpDefault = totalDefaultDp;
        //     // =======================================================
        //     $scope.summaryJoblist = Math.round(($scope.totalWork + ($scope.totalMaterial - ($scope.totalMaterial * $scope.DiscountParts / 100))) + ((($scope.totalWork + ($scope.totalMaterial - ($scope.totalMaterial * $scope.DiscountParts / 100))) * 10) / 100));
        //     if ($scope.summaryJoblist >= 1000000) {
        //         $scope.costMaterial = 6000;
        //     } else if ($scope.summaryJoblist > 250000) {
        //         $scope.costMaterial = 3000;
        //     } else {
        //         $scope.costMaterial = 0;
        //     }
        //     // =======================================================
        //     $scope.tmpActRateChange = angular.copy($scope.tmpActRate);
        //     var totalW = 0;
        //     for (var i = 0; i < $scope.gridWork.length; i++) {
        //         if ($scope.gridWork[i].ActualRate !== undefined) {
        //             totalW += $scope.gridWork[i].ActualRate;
        //         }
        //     }
        //     $scope.tmpActRate = totalW;
        //     if ($scope.tmpActRate !== $scope.tmpActRateChange) {
        //         $scope.mData.StallId = 0;
        //         $scope.mData.FullDate = null;
        //         $scope.mData.PlanStart = null;
        //         $scope.mData.PlanFinish = null;
        //         $scope.mData.PlanDateStart = null;
        //         //$scope.mData.AppointmentDate = null; 
        //         delete $scope.mData.AppointmentDate;
        //         //$scope.mData.AppointmentTime = null;
        //         delete $scope.mData.AppointmentTime;
        //         $scope.mData.TargetDateAppointment = null;
        //         $scope.mData.FixedDeliveryTime1 = null;
        //         $scope.mData.PlanDateFinish = null;
        //         $scope.mData.StatusPreDiagnose = 0;
        //         delete $scope.mData.PrediagScheduledTime;
        //         delete $scope.mData.PrediagStallId;
        //         $scope.asb.NewChips = 1;
        //     }
        // };


        // $scope.onBeforeEditJob = function (row) {

        // }


        // var dateFilter = 'date:"dd/MM/yyyy"';

        // $scope.gridPartsDetail = {

        //     columnDefs: [
        //         { name: "No. Material", field: "PartsCode", width: 150 },
        //         { name: "Nama Material", field: "PartsName", width: 200 },
        //         { name: "Qty", field: "Qty", width: 60 },
        //         { name: "Satuan", field: "satuanName", width: 80 },
        //         // { name: "Satuan", field: "SatuanId", cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.unitDataGrid[row.entity.SatuanId]}}</div>' },
        //         { name: "Ketersediaan", field: "Availbility", width: 120 },
        //         { name: "Tipe", field: "Type", width: 60 },
        //         { name: "ETA", displayName: "ETA", field: "ETA", cellFilter: dateFilter, width: 110 },
        //         { name: "Harga", field: "RetailPrice", width: 100, cellFilter: "rupiahC" },
        //         { name: "Discount", field: "Discount", width: 100 },
        //         { name: "SubTotal", field: "subTotal", width: 100, cellFilter: "rupiahC" },
        //         { name: "Nilai Dp", displayName: "Nilai DP", field: "DownPayment", width: 100 },
        //         { name: "Status DP", displayName: "Status DP", field: "StatusDp", cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>', width: 100 },
        //         { name: "Pembayaran", field: "PaidBy", width: 110 },
        //     ]
        // };

        // $scope.onListSelectRows = function (rows) {
        //     console.log("form controller=>", rows);
        //     $scope.chooseAprove = rows;
        //     console.log("$scope.chooseAprove", $scope.chooseAprove);
        // };




        $scope.agGridWorkViewApi;
        // ag gridworkview
        function onFirstDataRendered(params){
            params.api.sizeColumnsToFit();
            setTimeout(function(){ params.api.getDisplayedRowAtIndex(1).setExpanded(true) }, 0);
        }

        $scope.master = [];
        var DataChoosedDelete = [];
        // $scope.gridWorkView.data = $scope.master;
        $scope.gridWorkView = {
            //DEFAULT AG-GRID OPTIONS
            // default ColDef, gets applied to every column
            defaultColDef: {
                width: 150,
                editable: false,
                filter: false,
                sortable: true,
                resizable: true
            },
            rowSelection: 'multiple',
            suppressRowClickSelection:true,
            angularCompileRows: true,
            toolPanelSuppressSideButtons:true,
            masterDetail:true,
            detailCellRendererParams:{
                detailGridOptions : {
                    columnDefs : [
                        // {field : 'idChild'},
                        {headerName: 'No.',field : 'No', suppressSizeToFit:true},
                        {headerName: 'No. Material',field : 'PartsCode', suppressSizeToFit:true},
                        {headerName: 'Nama Material',field : 'PartsName', suppressSizeToFit:true},
                        {headerName: 'Qty',field : 'qty', suppressSizeToFit:true},
                        {headerName: 'Satuan',field : 'Name', suppressSizeToFit:true},
                        {headerName: 'Harga',field : 'Price', suppressSizeToFit:true , cellStyle: {textAlign: "right"},
                            cellRenderer: function(params){ 
                                var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
                                var notOk = '<span style="float:left">Rp.</span>'+ 0;
                                if(params.value ==  null || params.value ==  undefined || params.value == "" ){
                                    return notOk;
                                } else{
                                    return isOke;
                                }
                            }
                        },
                        {headerName: 'Pembayaran',field : 'PaidBy', suppressSizeToFit:true},
                        {headerName: 'Ketersediaan',field : 'Availbility', suppressSizeToFit:true},
                        {headerName: 'Tipe',field : 'TipeMaterial', suppressSizeToFit:true},
                        {headerName: 'ETA',field : 'ETA',width: 150 , cellRenderer: customDate},
                        // {headerName: 'DP (%)',field : 'DownPayment', suppressSizeToFit:true},
                        // {headerName: 'Qty',field : 'Reserve'},
                    ],
                    onFirstDataRendered: function(params){
                        // params.api.sizeColumnsToFit();
                        var allColumnIds = [];
                        params.columnApi.getAllColumns().forEach(function(column) {
                            allColumnIds.push(column.colId);
                        });
                    
                        params.columnApi.autoSizeColumns(allColumnIds, false);
                    },

                   


                },
                getDetailRowData: function(params){
                    console.log("params",params)
                    params.successCallback(params.data.AEstimationBP_PartList);        
                },

            },


            onFirstDataRendered: onFirstDataRendered,
            // components: {
            //     partEditor: PartEditor,
            //     numericEditor: NumericCellEditor,
            //     numericEditorPct: NumericCellEditorPct
            // },
            onGridReady: function(params) {
                // params.api.sizeColumnsToFit();
                $scope.agGridWorkViewApi = params.api;

                
            },
            rowData: $scope.master,
            onCellEditingStopped: function(e) {
                console.log('parts cellEditingStopped=>',e.rowIndex,e.colDef.field,e.value,e.node,e);
                // if(e.colDef.field == 'PartCode'){
                    // $scope.onAfterCellEditAg(e.rowIndex,e.colDef.field,e.value,e.node);
                // }
            },
            getDetailRowData: function(params){
                   
            },
            
            autoSizeAll : function () {
                var allColumnIds = [];
                gridWorkView.columnApi.getAllColumns().forEach(function(column) {
                    allColumnIds.push(column.colId);
                });
            
                gridWorkView.columnApi.autoSizeColumns(allColumnIds, false);
            },
            
            isRowSelectable: function(rowNode) {
                if(rowNode.data.IsUse == null || rowNode.data.IsUse == 0  || typeof rowNode.data.IsUse == undefined){
                    return true
                }else{
                    return false;
                }
                // return rowNode.data.IsUse == null ? true : false;
            },
            
            onRowSelected: function(event) {
                console.log("event",event);
                console.log(" selected = " + event.node.selected);
                if(event.node.selected == true){
                    DataChoosedDelete.push(event.data);
                }else{
                    var filtered = DataChoosedDelete.filter(function(item) { 
                       return item.FlagID !== event.data.FlagID;  
                    });
                    console.log("filtered",filtered);
                    DataChoosedDelete = filtered;
                }
                console.log("DataChoosedDelete",DataChoosedDelete);
            },
            columnDefs: [
              
                { headerName: 'No.',field: 'No',width: 100, cellRenderer:'agGroupCellRenderer', headerCheckboxSelection: true, checkboxSelection: true},
                { headerName: 'Pekerjaan',field: 'TaskName',width: 300,},
                { headerName: 'Satuan',field: 'satuan',width: 120},

                // { headerName: 'Harga',field: 'Fare',width: 150, cellStyle: {textAlign: "right"},
                { headerName: 'Harga',field: 'SubTotalTask',width: 150, cellStyle: {textAlign: "right"},

                    cellRenderer: function(params){ 
                        var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
                        var notOk = '<span style="float:left">Rp.</span>'+ 0;
                        if(params.value ==  null || params.value ==  undefined || params.value == "" ){
                            return notOk;
                        } else{
                            return isOke;
                        }
                    }
                },

                // { headerName: 'No. Estimasi',field: 'EstimationNo',width: 200},
                // { headerName: 'Kategori SPK',field: 'KetSPK',width: 200},
                { headerName: 'Pembayaran',field: 'PaidBy',width: 150},
                { headerName: 'Total Material',field: 'totalParts',width: 150},
                { headerName: 'Action',width: 100, minWidth:100,maxWidth:100, suppressFilter: true, suppressMenu:true, pinned:'right', cellRenderer: actionRendererTask}
            ]
        };
        
        $scope.deleteAllDataGridViewWork = function(){
            console.log("$scope.master.length",$scope.master.length);
            console.log("DataChoosedDelete.length",DataChoosedDelete);
            console.log("$scope.dataGoodWork",JSON.stringify($scope.dataGoodWork));
            $scope.mEstimationBP.DP = 0;
            globalDP = 0;
            // if($scope.master.length == DataChoosedDelete.length){
            //     console.log("masuk atas");
            //     $scope.master = [];
            //     DataChoosedDelete = [];
            // }else{

                console.log("masuk bawah",$scope.master);
                console.log("$scope.gridEstimation",$scope.gridEstimation.data);
                if(DataChoosedDelete.length > 0){

                        var find = _.find($scope.dataGoodWork, DataChoosedDelete);
                        console.log('find data ====>',find);

                        for(var a = 0; a < DataChoosedDelete.length; a++){
                            console.log("aye aye", DataChoosedDelete[a]);
                            for(var c = 0; c < $scope.dataGoodWork.length; c++){
                                console.log("losal",$scope.dataGoodWork[c]);
                                console.log("wew",$scope.dataGoodWork[c].TaskListBPId);



                                if($scope.dataGoodWork[c].FlagID == DataChoosedDelete[a].FlagID){

                                    if($scope.dataGoodWork[c].AEstimationBP_PartList.length>0){
                                        for(var cuk in $scope.dataGoodWork[c].AEstimationBP_PartList){
                                            $scope.dataGoodWork[c].AEstimationBP_PartList[cuk].isDeleted = 1;
                                        }
                                    }
                                    if($scope.dataGoodWork[c].EstTaskBPId == null || typeof  $scope.dataGoodWork[c].EstTaskBPId == undefined){
                                        $scope.dataGoodWork.splice(c,1);
                                        console.log('ini langsung splice karna blm ada id nya')
                                    }else{
                                        $scope.dataGoodWork[c].isDeleted = 1;
                                        console.log('ini cuma ganti status is Deleted nyaa')

                                    }
                                }
                                
                                
                            }
                        }


                        


                        for(var a = 0; a < $scope.master.length; a++){
                            for(var b = 0; b < DataChoosedDelete.length; b++){
                                if($scope.master[a].FlagID !== undefined){

                                    if($scope.master[a].FlagID == DataChoosedDelete[b].FlagID){

                                        console.log("bgstr ahg",DataChoosedDelete[b]);
                                        $scope.master.splice(a,1);
                                    }
                                }
                            }
                        }


                        for(var a = 0; a < $scope.gridEstimation.data.length; a++){
                            for(var b = 0; b < DataChoosedDelete.length; b++){
                                if($scope.gridEstimation.data[a].Flag == DataChoosedDelete[b].FlagID){

                                    console.log("bgstr ahg",DataChoosedDelete[b]);
                                    $scope.gridEstimation.data.splice(a,1);
                                }
                            }
                        }
                                    // console.log("bgstr ahg kampret",DataChoosedDelete);
                        // $scope.master = DataChoosedDelete;
                    // }


                    for(var a = 0; a < $scope.master.length; a++){
                        $scope.master[a].No = a + 1;
                        for(var b = 0; b < $scope.master[a].AEstimationBP_PartList.length; b++){
                               $scope.master[a].AEstimationBP_PartList[b].No = $scope.master[a].No +"."+ (b+1);
                        }

                        // _.map($scope.dataGoodWork, function(res) {
                        //     console.log("biji",res);
                        //     if (res.FlagID == $scope.master[a].FlagID) {
                        //         console.log("masuk lag biji",item);
                        //     }
                        // });
                         // for(var c = 0; c < $scope.dataGoodWork.length; c++){
                         //     if($scope.dataGoodWork[c].FlagID == $scope.master[a].FlagID){
                         //         $scope.dataGoodWork[c].isDeleted = 1;
                         //     }
                         // }
                        // $scope.dataGoodWork.filter(function(item) {
                        //     console.log("item.",item); 
                        //     if(item.FlagID == $scope.master[a].FlagID){
                        //         console.log("masuk lag biji",item);
                        //         return item.isDeleted = 1;
                        //     }
                        // });

                    }


                    console.log("$scope.dataGoodWork",$scope.dataGoodWork);
                    console.log("$scope.gridEstimation.data after",$scope.gridEstimation.data);
                    console.log("$scope.master after",$scope.master);
                    if($scope.master.length == 0){
                        $scope.disTask = 0;
                        $scope.disParts = 0;
                    }
                    var dups = [];
                    var arr = $scope.master.filter(function(el) {
                        if(el.Disount !== null || el.Disount !== undefined){
                          dups.push(el.Discount);    
                        }
                    });
                    console.log("dis task beda",arr,dups);
                    var filtered = $scope.master.filter(function(item) { 
                       return item.Discount !== dups[0];
                    });
                    console.log("dis task beda real",filtered);
                    if(filtered.length == 0){
                        

                             $scope.disTask = dups[0];   
                    }else{
                         $scope.disTask = "";
                    }


                    var dupsParts = [];
                    for(var i in $scope.master){
                        var arrParts = $scope.master[i].AEstimationBP_PartList.filter(function(el) {
                            if(el.Disount !== null || el.Disount !== undefined){
                              dupsParts.push(el.Discount);    
                            }
                        });
                    }
                    console.log("dupsParts",dupsParts);
                    var flagDisc = 0;
                    for(var i in $scope.master){
                        if($scope.master[i].AEstimationBP_PartList.length == 0){
                            flagDisc++
                        }else{

                             $scope.master[i].AEstimationBP_PartList.filter(function(item) { 
                                if(item.Discount !== dupsParts[0]){
                                    flagDisc++
                                } 
                             });   
                        }
                    }
                    console.log("flagDisc",flagDisc);
                    if(flagDisc == 0){

                             $scope.disParts = dupsParts[0];   
                    }else{
                        $scope.disParts = ""; 
                    }
                    $scope.agGridWorkViewApi.setRowData($scope.master);
                    DataChoosedDelete = [];

                    // for(var i = 0; i < $scope.master.length; i++){

                    //     $scope.getDataGirdWorkView($scope.master[i]);
                    // }
                    console.log("DataChoosedDelete after",DataChoosedDelete);
                    $scope.coutnData();
                    $scope.calculateforest();
                }else{

                    bsNotify.show({
                      size: 'small',
                      type: 'notif',
                      title: "Pilih data",
                    });
                }

        }
        // $scope.optionsSpkList


        function actionRendererTask(){
            var actionHtml =' <button class="ui icon inverted grey button"\
                                style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px"\
                                onclick="this.blur()"\
                                ng-click="gridClickViewDetailHandler(data,1)">\
                                <i class="fa fa-fw fa-lg fa-list-alt"></i>\
                            </button>\
                            <button class="ui icon inverted grey button" ng-if="data.IsUse !== 1"\
                                style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px"\
                                onclick="this.blur()"\
                                ng-click="gridClickEditHandler(data,2)">\
                                <i class="fa fa-fw fa-lg fa-pencil"></i>\
                            </button>';
            return actionHtml;
        }


        function validateEmail(email){
            const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            console.log('valid emial ===>',re.test(email));
            if(email == ""){
                return true
            }else{
                return re.test(email);
            }
        }

        $scope.isValidEmail = true;
        $scope.emailLength = 0;
        $scope.onTypeEmail = function(typing){
            console.log('onTypeEmail ===>',typing,typing.length);


            if(typing == undefined){
                $scope.emailLength = 55;
            }else{
                $scope.emailLength = typing.length;
            }


            $scope.isValidEmail = validateEmail(typing)
        }

        $scope.disJob = false;
        // $scope.disFlag =
        $scope.gridClickViewDetailHandler = function(data,flag){
            gridPartsToStart('#gridParts .ag-body-horizontal-scroll-viewport',500);
            console.log("data view",data,flag);
            modalFromPlus = false;
            console.log('modalFromPlus ==>',modalFromPlus);


            // $scope.mEstimationBP.PaidById = parseInt(data.PaidById);
            // var findPembayaran = _.find($scope.paymentData,{"MasterId":parseInt(data.PaidById)});
            // console.log('findPembayaran ===>',findPembayaran);
            // $scope.mEstimationBP.PaidById = findPembayaran.MasterId;
            // $scope.selectTypeCustFromPlus(findPembayaran);
            $scope.selectTypeCust(parseInt(data.PaidById));

            var numLength = angular.element('.ui.modal.ModalOP').length;
            setTimeout(function () {
                angular.element('.ui.modal.ModalOP').modal('refresh');
            }, 0);

            if (numLength > 1) {
                angular.element('.ui.modal.ModalOP').not(':first').remove();
            }

            angular.element(".ui.modal.ModalOP").modal("show");
            angular.element(".ui.modal.ModalOP").modal({
                backdrop: 'static',
                keyboard: false
                });

            console.log('options SPK list | gridClickViewDetailHandler ===>',$scope.optionsSpkList);

            // $scope.cekJob = null;
            // $scope.isRelease = false;
            // $scope.FlatRateAvailable = false;
            $scope.PriceAvailable = true;
            // $scope.partsAvailable = false;
            console.log("$scope.mEstimationBP.isCash",$scope.mEstimationBP.isCash);
            // if($scope.mEstimationBP.isCash == 0){

                $scope.showSPK = true;
            // }else{
            //     $scope.showSPK = false;
            // }

            console.log("typeof data.PaidById",typeof data.PaidById);
            if(typeof data.PaidById == 'number'){
                data.PaidById = data.PaidById.toString();
            }
            
            if(typeof data.ProcessId == 'number'){
                data.ProcessId = data.ProcessId.toString();
            }

            if(data.DiscountTypeId != undefined){
               $scope.mEstimationBP.TipeDisc = data.DiscountTypeId;

            }else{

               $scope.mEstimationBP.TipeDisc = data.DiscountType; 
            }

            if(typeof $scope.mEstimationBP.TipeDisc == 'number'){
                $scope.mEstimationBP.TipeDisc = $scope.mEstimationBP.TipeDisc.toString();
            }

            $scope.disJob = true;
            $scope.mEstimationBP.TaskName = data.TaskName;
            $scope.mEstimationBP.NoSpk = data.EstSPKListId;
            $scope.mEstimationBP.ProcessId = data.ProcessId;
            $scope.mEstimationBP.PaidById = data.PaidById;
            $scope.temp_paidJob = angular.copy($scope.mEstimationBP.PaidById)
            $scope.mEstimationBP.Fare = data.Fare;
            $scope.mEstimationBP.DiskonPersen = data.Discount;
            $scope.mEstimationBP.DP = data.DP;

            if (data.PaidById == 30) {
                $scope.mEstimationBP.FlatRate = data.FlatRate;
                $scope.mEstimationBP.TFirst1 = data.TFirst1;
            }


            console.log("$scope.mEstimationBP",$scope.mEstimationBP);

           
            for(var x in data.AEstimationBP_PartList)
            {
                console.log('indexx',data.AEstimationBP_PartList.length)
                if(data.AEstimationBP_PartList[x].Availbility == 'Tidak Tersedia'){
                    // if(data.AEstimationBP_PartList[x].DPRequest > 100.00){
                    //     $scope.mEstimationBP.DP = data.AEstimationBP_PartList[x].DownPayment;
                    // }else{
                    //     $scope.mEstimationBP.DP = data.AEstimationBP_PartList[x].DPRequest;

                    // }
                    AppointmentBpService.getAvailableParts(data.AEstimationBP_PartList[x].PartsId, 0, data.AEstimationBP_PartList[x].Qty).then(function(res) { 
                        $scope.mEstimationBP.DP = res.data.Result[0].PercentDP
                    })
                    
                }else{
                    $scope.mEstimationBP.DP = 0;
                }
            }


            for(var i = 0; i < data.AEstimationBP_PartList.length;i++){

                
                if(data.AEstimationBP_PartList[i].IsSaveFin == 1){
                    data.AEstimationBP_PartList[i].No = i + 1;
                    $scope.masterParts.push(data.AEstimationBP_PartList[i]);
                }
            }
            console.log("$scope.masterParts",$scope.masterParts);
            $scope.GridPartsApi.setRowData($scope.masterParts);

        }
        var flagUpdate = 0;
        var dataEidt;
        $scope.gridClickEditHandler = function(data,flag){
            gridPartsToStart('#gridParts .ag-body-horizontal-scroll-viewport',500);
            console.log('gridClickEditHandler',data,flag);

            $scope.mode_popup_job = 'edit'
           
            dataTask = data;
            taskBPIdFromEdit = data.TaskBPId;
            customTaskName = data.TaskName;
            taskBPIdFromMaster = data.TaskBPId;
            taskNameFromMaster = null;

            for(var x in data.AEstimationBP_PartList)
            {
                console.log('indexx',data.AEstimationBP_PartList.length)
                if(data.AEstimationBP_PartList[x].Availbility == 'Tidak Tersedia')
                {
                    // $scope.mEstimationBP.DP = data.AEstimationBP_PartList[x].DPRequest;
                }
            }


            console.log('taskBPIdFromMaster ========>',taskBPIdFromMaster);
            console.log('TaskName yang sudah ada ===>',customTaskName);


            
            modalFromPlus = false;
            console.log('modalFromPlus ==>',modalFromPlus);

            flagUpdate = 1;
            dataEidt = data;

            if(flag == 2){
                var numLength = angular.element('.ui.modal.ModalOP').length;
                setTimeout(function () {
                    angular.element('.ui.modal.ModalOP').modal('refresh');
                }, 0);

                if (numLength > 1) {
                    angular.element('.ui.modal.ModalOP').not(':first').remove();
                }

                angular.element(".ui.modal.ModalOP").modal("show");
                angular.element(".ui.modal.ModalOP").modal({ backdrop: 'static', keyboard: false });
            }

            


            

            console.log('options SPK list ===> | gridClickEditHandler',$scope.optionsSpkList);
            console.log("typeof data.PaidById",typeof data.PaidById);
            if(typeof data.PaidById == 'number'){
                data.PaidById = data.PaidById.toString();
            }
            
            if(typeof data.ProcessId == 'number'){
                data.ProcessId = data.ProcessId.toString();
            }

            if(data.DiscountTypeId != undefined){
               $scope.mEstimationBP.TipeDisc = data.DiscountTypeId;

            }else{
               
               $scope.mEstimationBP.TipeDisc = data.DiscountType; 
            }
            console.log("typeof $scope.mEstimationBP.TipeDisc",typeof $scope.mEstimationBP.TipeDisc);
            if(typeof $scope.mEstimationBP.TipeDisc == 'number'){
                $scope.mEstimationBP.TipeDisc = $scope.mEstimationBP.TipeDisc.toString();
            }
            
            // $scope.cekJob = null;
            // $scope.isRelease = false;
            // $scope.FlatRateAvailable = false;
            $scope.PriceAvailable = false;
            // $scope.partsAvailable = false;
            console.log("$scope.mEstimationBP.isCash",$scope.mEstimationBP.isCash);
            if($scope.mEstimationBP.isCash == 0){

                $scope.showSPK = true;
            }else{
                $scope.showSPK = false;
            }
            
            console.log("data edit",data,flag);
            if(data.DiscountTypeId == 0){
                $scope.disabledDisc = false;
            }else if(data.DiscountTypeId == -1){
                $scope.disabledDisc = true;
            }


           
            


            $scope.disJob = false;
            $scope.mEstimationBP.TaskName = data.TaskName;
            $scope.mEstimationBP.NoSpk = data.EstSPKListId;
            $scope.mEstimationBP.ProcessId = data.ProcessId;
            $scope.mEstimationBP.PaidById = data.PaidById;
            $scope.temp_paidJob = angular.copy($scope.mEstimationBP.PaidById)
            $scope.mEstimationBP.Fare = data.Fare;
            $scope.mEstimationBP.DiskonPersen = data.Discount; 
            $scope.mEstimationBP.DP = 0;

            if (data.PaidById == 30) {
                $scope.mEstimationBP.FlatRate = data.FlatRate;
                $scope.mEstimationBP.TFirst1 = data.TFirst1;

                if ($scope.mEstimationBP.FlatRate == 0) {
                    $scope.FlatRateAvailable_new = false;
                } else {
                    $scope.FlatRateAvailable_new = true;
                }

                if ($scope.mEstimationBP.Fare_w == null || $scope.mEstimationBP.Fare_w == undefined) {
                    $scope.mEstimationBP.Fare_w = $scope.mEstimationBP.FlatRate * $scope.mEstimationBP.Fare
                }
            }
            

            // $scope.mEstimationBP.TipeDisc = data.DiscountType;
            console.log("$scope.dataGoodWork",$scope.dataGoodWork);
            console.log("$scope.mEstimationBP",$scope.mEstimationBP);
            console.log("data.estimation",data.AEstimationBP_PartList);


            for(var x in data.AEstimationBP_PartList)
            {
                console.log('indexx',data.AEstimationBP_PartList.length)
                if(data.AEstimationBP_PartList[x].Availbility == 'Tidak Tersedia'){
                    // if(data.AEstimationBP_PartList[x].DPRequest > 100.00){
                    //     $scope.mEstimationBP.DP = data.AEstimationBP_PartList[x].DownPayment;
                    // }else{
                    //     $scope.mEstimationBP.DP = data.AEstimationBP_PartList[x].DPRequest;

                    // }

                    AppointmentBpService.getAvailableParts(data.AEstimationBP_PartList[x].PartsId, 0, data.AEstimationBP_PartList[x].Qty).then(function(res) { 
                        $scope.mEstimationBP.DP = res.data.Result[0].PercentDP
                    })
                }else{
                    $scope.mEstimationBP.DP = 0;
                }
            }



            // if(data.AEstimationBP_PartList.length > 0){
            //     for(var a = 0;a<data.AEstimationBP_PartList;a++){
            //         if(data.AEstimationBP_PartList[a].DownPayment != 0){
            //             $scope.mEstimationBP.DP = data.AEstimationBP_PartList[a].DPRequest;
            //         }
            //     }
            // }else{
            //     $scope.mEstimationBP.DP = 0;
            // }



            for(var i = 0; i < data.AEstimationBP_PartList.length;i++){
                if(data.AEstimationBP_PartList[i].IsSaveFin == 1){

                    data.AEstimationBP_PartList[i].Qty = data.AEstimationBP_PartList[i].qty;
                    data.AEstimationBP_PartList[i].No = i + 1;

                    
                    $scope.masterParts.push(data.AEstimationBP_PartList[i]);
                }
            }
            // $scope.masterParts = data.AEstimationBP_PartList;
            console.log("$scope.masterParts",$scope.masterParts);
           
            $scope.GridPartsApi.setRowData($scope.masterParts);

        }
        // ag gridworkview

        // $scope.gridTimeEst.data = [];
        $scope.gridEstimation = {
            enableCellEditOnFocus: true,
            showColumnFooter: true,

            columnDefs: [{
                'name': 'Order Pekerjaan',
                'field': 'TaskName',
                enableCellEdit: false,
                footerCellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;">Time Per Stall</div>'
            }, {
                'name': 'Body',
                'field': 'BodyEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Putty',
                'field': 'PutyEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Surfacer',
                'field': 'SurfacerEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Painting',
                'field': 'SprayingEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Polishing',
                'field': 'PolishingEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Re-Assembly',
                displayName: "Re-Assembly",
                'field': 'ReassemblyEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Final Inspection',
                'field': 'FIEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Total',
                'field': 'Total',
                enableCellEdit: false,
                cellTemplate: '<div class="ui-grid-cell-contents" ng-model="MODEL_COL_FIELD =(row.entity.BodyEstimationMinute + row.entity.PutyEstimationMinute + row.entity.SurfacerEstimationMinute + row.entity.SprayingEstimationMinute +  row.entity.PolishingEstimationMinute + row.entity.ReassemblyEstimationMinute + row.entity.FIEstimationMinute )">{{row.entity.BodyEstimationMinute + row.entity.PutyEstimationMinute + row.entity.SurfacerEstimationMinute + row.entity.SprayingEstimationMinute +  row.entity.PolishingEstimationMinute + row.entity.ReassemblyEstimationMinute + row.entity.FIEstimationMinute }}</div>',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                footerCellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;">{{grid.appScope.gridEstimationApi.grid.columns[8].getAggregationValue()}}</div>',
                aggregationHideLabel: true

            }],
            onRegisterApi: function(gridApi) {
                $scope.gridEstimationApi = gridApi;
                $scope.gridEstimationApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                        console.log("afterCellEdit rowEntity", rowEntity);
                        console.log("afterCellEdit colDef", colDef);
                        console.log("afterCellEdit newValue", newValue);
                        console.log("afterCellEdit oldValue", oldValue);

                        if (newValue == null || newValue == undefined) {
                            newValue = 0;
                            return newValue;
                            // $scope.calculateforwork();
                        }
                        for(var i = 0;i < $scope.master.length; i++){
                            if($scope.master[i].FlagID == rowEntity.Flag){
                                $scope.master[i].BodyEstimationMinute = rowEntity.BodyEstimationMinute;
                                $scope.master[i].FIEstimationMinute = rowEntity.FIEstimationMinute;
                                $scope.master[i].PolishingEstimationMinute = rowEntity.PolishingEstimationMinute;
                                $scope.master[i].PutyEstimationMinute = rowEntity.PutyEstimationMinute;
                                $scope.master[i].ReassemblyEstimationMinute = rowEntity.ReassemblyEstimationMinute;
                                $scope.master[i].SprayingEstimationMinute = rowEntity.SprayingEstimationMinute;
                                $scope.master[i].SurfacerEstimationMinute = rowEntity.SurfacerEstimationMinute;
                            }
                        }

                        for(var i in $scope.dataGoodWork){
                            if($scope.dataGoodWork[i].FlagID == rowEntity.Flag){
                                $scope.dataGoodWork[i].BodyEstimationMinute = rowEntity.BodyEstimationMinute;
                                $scope.dataGoodWork[i].FIEstimationMinute = rowEntity.FIEstimationMinute;
                                $scope.dataGoodWork[i].PolishingEstimationMinute = rowEntity.PolishingEstimationMinute;
                                $scope.dataGoodWork[i].PutyEstimationMinute = rowEntity.PutyEstimationMinute;
                                $scope.dataGoodWork[i].ReassemblyEstimationMinute = rowEntity.ReassemblyEstimationMinute;
                                $scope.dataGoodWork[i].SprayingEstimationMinute = rowEntity.SprayingEstimationMinute;
                                $scope.dataGoodWork[i].SurfacerEstimationMinute = rowEntity.SurfacerEstimationMinute;
                            }
                        }
                        console.log("$scope.master coy",$scope.master);
                        console.log("$scope.dataGoodWork coy",$scope.dataGoodWork);
                        // $scope.calculateforwork();
                    })
                    // $scope.gridEstimationApi.selection.on.rowSelectionChanged($scope, function (row) {
                    //     $scope.selectedRow = row.entity;
                    // });
                    // if ($scope.gridEstimationApi.selection.selectRow) {
                    //     $scope.gridEstimationApi.selection.selectRow($scope.gridEstimation.data[0]);
                    // }
            }
        };


        $scope.onTypeDP = function (DPtyping) {
            console.log('typing dp value ===>', DPtyping);
            console.log('DPtyping | typeof',DPtyping,typeof(DPtyping))
            var nocomma = DPtyping;
            var rounded = parseFloat(nocomma).toFixed(2);
            var final = parseFloat((+rounded).toLocaleString('en-us', {useGrouping: true}))
            $scope.mEstimationBP.DP = final;
            console.log('final dp value ===>', final);
        }

        $scope.onTypeDiskonPersen = function (DiscountTyping) {
            console.log('typing dp value ===>', DiscountTyping);
            console.log('DiscountTyping | typeof',DiscountTyping,typeof(DiscountTyping))
            var nocomma = DiscountTyping;
            var rounded = parseFloat(nocomma).toFixed(2);
            var final = parseFloat((+rounded).toLocaleString('en-us', {useGrouping: true}))
            $scope.mEstimationBP.DiskonPersen = final;
            console.log('final onTypeDiskonPersen value ===>', final);
        }



        $scope.calculateforwork = function() {
            $scope.getCalculatedTimeWork = [];
            $scope.arrWorkTime = [];
            var newDate = new Date($scope.mData.AppointmentDate);
            console.log("newDate 1", newDate);
            // newDate.getHours(newDate.setHours() + 7);
            newDate.setSeconds(0);

            var total = 0;
            for (var i = 0; i < $scope.gridEstimationApi.grid.columns.length; i++) {
                var a = $scope.gridEstimationApi.grid.columns[i].getAggregationValue();
                // $scope.arrWorkTime.push(a + ' minutes');
                if (a != null || a !== undefined) {
                    $scope.arrWorkTime.push(a + ' minutes');
                    $scope.getCalculatedTimeWork.push(a);
                    total = total + a;
                };
            };

            function roundToTwo(num) {
                return +(Math.round(num + "e+2") + "e-2");
            };

            var days = Math.floor(Math.abs(total / (8 * 60)));
            var hours = Math.abs((total % (8 * 60)) / 60);
            newDate.setDate(newDate.getDate() + days);
            newDate.setHours(newDate.getHours() + roundToTwo(hours));
            // console.log("hours", hours);
            $scope.estHoursWork = roundToTwo(hours);
            $scope.estDaysWork = days;
            console.log("newDate 2", newDate);

            var hh = newDate.getHours().toString();
            var mm = newDate.getMinutes().toString();
            var ss = "00";
            var finalDate = (hh[1] ? hh : "0" + hh[0]) + ':' + (mm[1] ? mm : "0" + mm[0]) + ":" + ss;
            var finalHour = (hh[1] ? hh : "0" + hh[0]);
            var finalMinute = (mm[1] ? mm : "0" + mm[0]);
            if (!isNaN(finalHour) && !isNaN(finalMinute)) {
                $scope.mData.TargetPlanFinish = finalDate;
                $scope.mData.TargetPlanDateFinish = newDate;
                $scope.mData.AdjusmentDate = newDate;
            }
            console.log("finalDate with Hours", finalDate);
            console.log("$scope.getCalculatedTimeWork", $scope.getCalculatedTimeWork);
            console.log("$scope.estHoursWork", $scope.estHoursWork);
            console.log("$scope.estDaysWork", $scope.estDaysWork);
            console.log("newDate", newDate);
        };
        $scope.estHours = null;
        $scope.estDays = null;
        $scope.getCalculatedTimeEst = [];
        $scope.estimateTime = 3;
        $scope.countZeroTime = 3;
        $scope.arrEstTime = [];
        $scope.calculateforest = function() {
            $scope.getCalculatedTimeEst = [];
            $scope.arrEstTime = [];
            console.log("calculateforest | $scope.gridEstimation", $scope.gridEstimation);
            console.log("calculateforest | dataGoodWork", $scope.dataGoodWork);
            console.log('calculateforest | $scope.gridEstimation', $scope.gridEstimation.data);



            var total = 0;
            for (var i = 0; i < $scope.gridEstimationApi.grid.columns.length; i++) {
                var a = $scope.gridEstimationApi.grid.columns[i].getAggregationValue();
                if (a != null || a !== undefined) {
                    $scope.arrEstTime.push(a + ' minutes');
                    $scope.getCalculatedTimeEst.push(a);
                    total = total + a;
                };
            };

            total = total / 2;
            console.log('total waktu estmasi ===>',total)


            function roundToTwo(num) {
                return +(Math.round(num + "e+2") + "e-2");
            };

            var days2 = Math.abs(total / (8 * 60));
            var hours2 = Math.abs(total / 60);

            var daysString = days2.toString();
            var daysXX = '';
            if (daysString.includes('.')) {
                var daysSplit = daysString.split('.');
                var daysOne = daysSplit[0];
                var daysTwo = daysSplit[1];
                var daysOneX = parseInt(daysOne);
                var daysTwoX = daysTwo.substr(0, 1);
                daysTwoX = parseInt(daysTwoX);

                if (daysTwoX <= 5) {
                    daysTwoX = 5;
                    daysXX = daysOneX.toString() + '.' + daysTwoX.toString();
                } else {
                    daysOneX = daysOneX + 1;
                    daysXX = daysOneX.toString();
                }
            } else {
                daysXX = daysString;
            }

            days2 = parseFloat(daysXX);
            console.log('nih days', days2);



            // console.log("hours", hours);
            $scope.estHours = roundToTwo(hours2);
            $scope.estDays = days2;



            $scope.countZeroTime = 0;
            for (var i = 0; i < $scope.gridEstimation.data.length; i++) {
                var a = $scope.gridEstimation.data[i];
                console.log("a",a);
                // $scope.arrEstTime.push(a + ' minutes');
                if (a != null || a !== undefined) {

                    a.FIEstimationMinute         = a.FIEstimationMinute         == null || undefined?  0 : a.FIEstimationMinute;
                    a.BodyEstimationMinute       = a.BodyEstimationMinute       == null || undefined?  0 : a.BodyEstimationMinute;
                    a.PutyEstimationMinute       = a.PutyEstimationMinute       == null || undefined?  0 : a.PutyEstimationMinute;
                    a.ReassemblyEstimationMinute = a.ReassemblyEstimationMinute == null || undefined?  0 : a.ReassemblyEstimationMinute;
                    a.SprayingEstimationMinute   = a.SprayingEstimationMinute   == null || undefined?  0 : a.SprayingEstimationMinute;
                    a.SurfacerEstimationMinute   = a.SurfacerEstimationMinute   == null || undefined?  0 : a.SurfacerEstimationMinute;
                    a.PolishingEstimationMinute  = a.PolishingEstimationMinute  == null || undefined?  0 : a.PolishingEstimationMinute ;

                    if( a.FIEstimationMinute == 0){
                        $scope.countZeroTime ++;
                        if(a.BodyEstimationMinute == 0 ||  a.PolishingEstimationMinute == 0 ||  a.PutyEstimationMinute == 0 ||  a.ReassemblyEstimationMinute == 0 ||  a.SprayingEstimationMinute == 0 ||  a.SurfacerEstimationMinute == 0){
                            $scope.countZeroTime ++;
                        }
                    }else if(a.SurfacerEstimationMinute   == 0 &&  a.PolishingEstimationMinute == 0 &&  a.PutyEstimationMinute == 0 &&  a.ReassemblyEstimationMinute == 0 &&  a.SprayingEstimationMinute == 0 &&  a.BodyEstimationMinute == 0){
                        $scope.countZeroTime ++;
                    }
            
                    $scope.arrEstTime.push(a + ' minutes');
                    $scope.getCalculatedTimeEst.push(a);
                    total = total + a.BodyEstimationMinute + a.FIEstimationMinute + a.PolishingEstimationMinute + a.PutyEstimationMinute + a.ReassemblyEstimationMinute + a.SprayingEstimationMinute + a.SurfacerEstimationMinute;
                };
            };



            $scope.estimateTime = 0;
            for (var i = 0; i < $scope.gridEstimation.data.length; i++) {
                var a = $scope.gridEstimation.data[i];
                if (a != null || a !== undefined) {

                    if( a.FIEstimationMinute != 0 && a.FIEstimationMinute <= 4){
                        $scope.estimateTime ++;
                    }else if(a.BodyEstimationMinute != 0 && a.BodyEstimationMinute <= 4){
                        $scope.estimateTime ++;
                    }else if(a.PutyEstimationMinute != 0 && a.PutyEstimationMinute <= 4){
                        $scope.estimateTime ++;
                    }else if(a.ReassemblyEstimationMinute != 0 && a.ReassemblyEstimationMinute <= 4){
                        $scope.estimateTime ++;
                    }else if(a.SprayingEstimationMinute != 0 &&  a.SprayingEstimationMinute <= 4){
                        $scope.estimateTime ++;
                    }else if(a.SurfacerEstimationMinute != 0 &&  a.SurfacerEstimationMinute <= 4){
                        $scope.estimateTime ++;
                    }else if(a.PolishingEstimationMinute != 0 &&  a.PolishingEstimationMinute <= 4){
                        $scope.estimateTime ++;
                    }


                };
            };

            console.log('calculateforest | $scope.estimateTime ====>',$scope.estimateTime);
            console.log('calculateforest | $scope.countZeroTime ===>',$scope.countZeroTime);
            console.log('calculateforest | $scope.estDays =========>',$scope.estDays);
            console.log('calculateforest | $scope.estHours ========>',$scope.estHours);



            if($scope.estimateTime > 0){
                bsNotify.show({
                    size: 'big',
                    type: 'warning',
                    title: "Minimal Waktu Estimasi adalah 5 Menit",
                });
            }


            if($scope.countZeroTime > 0){
                $scope.bsAlertConfirmDelete("Terdapat estimasi waktu perbaikan yang masih 0 menit di luar proses FI.","Dalam setiap pekerjaan minimal ada 2 proses").then(function (res) {
                    if (res) {
                        console.log('ini kalo klik ok | Estimasi waktu perbaikan ',res);
                        return true
                    }else{
                        console.log("ini kalo klik cancle | Estimasi waktu perbaikan ", res);
                        return false
                    }
                });
            }

        };

        // ag grid parts
        
        ChoosedMaterial = 1;

        $scope.GridPartsApi;
        $scope.masterParts = [];
        var lastDiscountParts = null;
        // $scope.NO = 0;

        $scope.plushParts = function(){

            $scope.GridPartsApi.stopEditing(); // stop edit cell di agGrid, jika tidak pencet enter / pindah kolom

            gridPartsToStart('#gridParts .ag-body-horizontal-scroll-viewport',500);
            console.log("selectedInsurance",selectedInsurance);
            console.log("$scope.masterParts",$scope.masterParts);
            
            var DisParts = 0;
            dataParts = {};



            //===================new===============
            if(lastDiscountParts !== null ){
                DisParts = lastDiscountParts;
                console.log('daper diskondisini 1',DisParts)
            }
            var PaidBy = null;
            var PaidById = null;
            if( $scope.mEstimationBP.isCash == 1 || $scope.mEstimationBP.isCash == '1'){//cash
                PaidById = 28;
                var findPaidBy = _.find($scope.paymentData, {'MasterId':28});
                PaidBy = findPaidBy.Name;
                // if($scope.mEstimationBP.TipeDisc == 0 || $scope.mEstimationBP.TipeDisc == '0'){
                //     DisParts = $scope.mEstimationBP.DiskonPersen;
                //     console.log('daper diskondisini 2',DisParts)
                // }

                console.log("$scope.masterParts",$scope.masterParts);
                if($scope.masterParts.length > 0){
                    DisParts = $scope.masterParts[0].Discount;
                }else{
                    DisParts = 0;
                }

            }else{//asuransi
                PaidById = 29;
                var findPaidBy = _.find($scope.paymentData, {'MasterId':29});
                PaidBy = findPaidBy.Name;
                if(selectedInsurance != undefined){
                    if(selectedInsurance.DiscountPart != undefined){
                        DisParts = selectedInsurance.DiscountPart;
                        console.log('daper diskondisini 3',DisParts)
                    }
                }
            }

            if ($scope.mEstimationBP.PaidById == 30) {
                PaidById = 30
                var findPaidBy = _.find($scope.paymentData, {'MasterId':30});
                PaidBy = findPaidBy.Name;
                DisParts = 0;
            }


            // ===================new==============


            // ===================old=================
            // if( $scope.mEstimationBP.isCash == 1){//cash
            //     if($scope.mEstimationBP.TipeDisc == 0){
            //         DisParts = $scope.mEstimationBP.DiskonPersen;
            //     }
            // }else{//asuransi
            //     if(selectedInsurance != undefined){
            //         if(selectedInsurance.DiscountPart != undefined){
            //             DisParts = selectedInsurance.DiscountPart;
            //         }
            //     }
            // }
            

            // if(lastDiscountParts !== null){
            //     DisParts = lastDiscountParts;
            // }
            // ==================old==================

        


            console.log('$scope.mEstimationBP.PaidById ===>',$scope.mEstimationBP.PaidById);
            console.log('datatambahpart',dataTaskSave);




            console.log('$scope.mEstimationBP.TipeDisc ===>',$scope.mEstimationBP.TipeDisc);
            console.log('$scope.mEstimationBP.isCash   ===>',$scope.mEstimationBP.isCash)
            console.log('$scope.mEstimationBP.PaidById   ===>',$scope.mEstimationBP.PaidById);
            console.log('$scope.mEstimationBP.DP   ===>',$scope.mEstimationBP.DP);
            // $scope.GridPartsApi.setRowData($scope.masterParts);
            console.log("$scope.GridPartsApi",$scope.GridPartsApi);
            console.log("lastDiscountParts",DisParts);
            console.log("final diskon",DisParts);
            // for(var a = 0; a < $scope.masterParts.length; a++){
            //     $scope.NO++;
            // }
            for (var a = 0; a< dataTaskSave.length; a++){
                for (var b in dataTaskSave[a].AEstimationBP_PartList){
                    // if(dataTaskSave[a].AEstimationBP_PartList[b].DownPayment != 0){
                    //     $scope.mEstimationBP.DP = dataTaskSave[a].AEstimationBP_PartList[b].DownPayment;
                    //     console.log('chibichobo1',$scope.mEstimationBP.DP)
                    // }
                    // else if(dataTaskSave[a].AEstimationBP_PartList[b].DownPayment == 0 || dataTaskSave[a].AEstimationBP_PartList[b].DownPayment == null || dataTaskSave[a].AEstimationBP_PartList[b].DownPayment == ""){
                    //     $scope.mEstimationBP = globalDP;
                    //     console.log('chibichobo2',$scope.mEstimationBP.DP)
                    // }
                }
            }


            // if($scope.mEstimationBP.DP == 0 || $scope.mEstimationBP.DP == null || $scope.mEstimationBP.DP == ""){
            //     $scope.mEstimationBP.DP = globalDP;
            // }

            
            
            ChoosedMaterial = 1;


            var IdFlagParts =  Math.floor(Math.random() * 10000) + 1;
            var newData = {
                "No" : $scope.masterParts.length + 1,
                "PartsCode": "",
                "PartsName": "",
                "TipeMaterial": "Spare Parts",
                "PartsId" : "",
                "OutletId" : "",
                "DefaultETA": "",
                "StandardCost": "",
                "RetailPrice" : "",
                "QtyFree" : "",
                "PriceDP" : "",
                "Availbility": "",
                "PaidBy":PaidBy,
                "PaidById":PaidById,
                "OrderType":"",
                "ETA" : null,
                "SatuanId" : 4, //defaultya pcs
                "Name" : "",
                "IsOPB" : 0,
                "Discount" : parseFloat(DisParts),
                "IdFlagParts" : IdFlagParts
            };
            
            console.log("newData",newData);
            $scope.masterParts.push(newData);
            // $scope.GridParts.api.setRowData($scope.GridParts.rowData);
            var res = $scope.GridPartsApi.updateRowData({add: [newData]});
            console.log("res",res);
            res.add.forEach(function(rowNode) {
                console.log("Added Row Node", rowNode);
                $scope.GridPartsApi.setFocusedCell(rowNode.rowIndex,"PartCode");
            });
            // console.log("$scope.masterParts",$scope.masterParts);
        }
        
        $scope.taPartCode_getPartCode = function(key) {
            console.log('taGetPartCode=>',key);
            console.log("ChoosedMaterial",ChoosedMaterial);
            $scope.loadingTAPartCode = true;
            var isGr = 0;
            var materialTypeId = undefined; //$scope.ldModel.MaterialTypeId; MaterialTypeId=1 => Parts , MaterialTypeId=2 => Bahan
            // if (kategori !== undefined) {
                var res = estimasiBPFactory.getDataPartsEstimasiBP(key, isGr, ChoosedMaterial).then(function(resparts) {
                    console.log("SalesOrder resparts", resparts);
                    var tempResult = [];
                    if (resparts.data.Result[0] !== undefined) {
                        // tempResult.push(resparts.data.Result[0]);
                        for (var i = 0; i < resparts.data.Result.length; i++) {
                            tempResult.push(resparts.data.Result[i]);
                        }
                    }
                    console.log('SalesOrder tempResult =>', tempResult);
                    return tempResult;
                });
                return res
            // };
        };
        var dataParts;
        $scope.taPartCode_onSelectPartCode = function($item, $model, $label) {
            console.log("onSelectPartCode item=>", $item);
            console.log("onSelectPartCode model=>", $model);
            console.log("onSelectPartCode label=>", $label);
            dataParts = $item;
            console.log("dataParts",dataParts);
        };

        $scope.taPartCode_onNoPartResult = function($item, $model, $label){

            console.log("onSelectPartCode item=>", $item);
            console.log("onSelectPartCode model=>", $model);
            console.log("onSelectPartCode label=>", $label);
            dataParts = {};
            console.log("dataParts",dataParts);
        }

        $scope.taPartCode_onNoPartResultCode = function($item, $model, $label){

            console.log("onSelectPartCode item=>", $item);
            console.log("onSelectPartCode model=>", $model);
            console.log("onSelectPartCode label=>", $label);
            // bsNotify.show({
            //   size: 'small',
            //   type: 'notif',
            //   title: "Kode Material Tidak Ditemukan",
            // });
            // defer.resolve({msg:"Kode Material Tidak Ditemukan"}); 
            dataParts = {};
            console.log("dataParts",dataParts);

        }


        function gridPartsToEnd(targetElement, speed) {
            console.log('gridPartsToEnd ===>',targetElement,speed)
            var scrollWidth = $(targetElement).get(0).scrollWidth;
            var clientWidth = $(targetElement).get(0).clientWidth;
            $(targetElement).animate({ scrollLeft: scrollWidth - clientWidth },
            {
                duration: speed,
                easing :'swing'
                
            });
            
        };

        function gridPartsToStart(targetElement, speed) {
            console.log('gridPartsToStart ===>',targetElement,speed)
            var scrollWidth = $(targetElement).get(0).scrollWidth;
            var clientWidth = $(targetElement).get(0).clientWidth;
            $(targetElement).animate({ scrollLeft: scrollWidth - scrollWidth },
            {
                duration: speed,
                easing :'swing'
                
            });
        };

        $scope.taPartCode_onGotResult = function() {
            console.log("taPartCode_OnGotResult =>");
        };

        var ChoosedMaterial;
        ChoosedMaterial = 1;
        var TipMaterial;
        var dataForGrid = {};
        $scope.onAfterCellEditAg = function(idx,field,val,node) {    
            console.log("$scope.disJob",$scope.disJob);
            if($scope.disJob == false){

                console.log("onAfterCellEditAg=>",idx,field,val,node);

                console.log("idx",idx);
                console.log("field",field);
                console.log("val",val);
                console.log('type val ', typeof val);
                console.log("node",node);
                var dt = node.data;
                console.log("dt",dt);
                console.log("node.data",node.data);

                if(field == 'IsOPB'){
                    console.log('ini edit opb ===>',val);

                }

                if(field == 'RetailPrice'){
                    
                    dataForGrid.SatuanId = node.data.UomId;
                    dataForGrid.Name =  node.data.Name;

                    if(node.data.RetailPrice == NaN){
                        console.log('node.data.RetailPrice tadinya NAN ===>',node.data.Qty);
                        node.data.RetailPrice = 0;
                    }
                    // RetailPriceInputan = parseInt(val);
                    node.data.RetailPrice = parseInt(node.data.RetailPrice);
                    
                    
                    // if(
                    //     (node.data.PartsName == "" || node.data.PartsName == undefined || node.data.PartsName == null) &&
                    //     (node.data.PartsCode == "" || node.data.PartsCode == undefined || node.data.PartsCode == null)
                    //   ){ 
                        node.data.SubTotal =  (node.data.RetailPrice * node.data.Qty) - ((node.data.RetailPrice * node.data.Qty) * node.data.Discount/100);
                        dataForGrid = node.data;
                        

                  

                        node.setData(dataForGrid);
                    // }else{
                    //     AppointmentBpService.getAvailableParts(node.data.PartsId, 0, val).then(function(res) { 
                    //         console.log("data yagn didapat | Discount",res.data.Result);
                    //         dataForGrid.RetailPrice = res.data.Result[0].RetailPrice;
                    //         dataForGrid.PriceDP = res.data.Result[0].PriceDP;
                    //         $scope.GridPartsApi.forEachNode( function(node,index){
                    //             console.log("node to Update oi oi=>",node,node.data,index);
                    //             console.log("res.data.Result[0].PartsCode",res.data.Result[0].PartsCode);
                    //             if(node.data.PartsCode == res.data.Result[0].PartsCode){
                    //                 console.log("node to Update=>",node.data,index);
                    //                 if($scope.mEstimationBP.TipeDisc == -1 && node.data.Discount > 0){
    
                    //                     dataForGrid.Discount = 0;
    
                    //                     bsNotify.show({
                    //                         size: 'big',
                    //                         type: 'danger',
                    //                         title: "Diskon Tidak Boleh Melebihi 0 Karena Tipe Discount Tidak Ada Discount",
                    //                     }); 
                    //                 }else{
                    //                     console.log("masuk bawah dong");
                    //                     if($scope.mEstimationBP.isCash == 0 && $scope.mEstimationBP.TipeDisc == "" || $scope.mEstimationBP.TipeDisc == null || $scope.mEstimationBP.TipeDisc == undefined){
                    //                         dataForGrid.Discount = $scope.disParts;
                    //                         bsNotify.show({
                    //                             size: 'big',
                    //                             type: 'danger',
                    //                             title: "Diskon Berdasarkan Asuransi Yang Dipilih",
                    //                         }); 
    
                    //                     }
                    //                 }
                    //                 node.data.SubTotal = (node.data.RetailPrice * node.data.Qty) - ((node.data.RetailPrice * node.data.Qty)*dataForGrid.Discount/100);
                    //                 var finData = dataForGrid;
                    //                 node.setData(finData);
    
                    //             }
                    //         });
                    //         console.log("dataForGrid",dataForGrid);
                    //     },function(err) {
                    //             //console.log("err=>", err);
                    //     });
                    // }
                }

                if(field == 'TipeMaterial'){

                    dataForGrid = node.data
                    if(val == 'Bahan'){
                        ChoosedMaterial = 2;
                        TipMaterial = 'Bahan';
                        // dataForGrid.IsOPB = null;
                    }else{
                        ChoosedMaterial = 1;
                        TipMaterial = 'Spare Parts';
                        // dataForGrid.IsOPB = null;
                    }

                    // OPBinputFunc2(node);
                    node.setData(dataForGrid);
                }

                if(field == 'Qty'){
                    if(val.length > 3){
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Jumlah Qty Melebihi Batas Maksimal ( 3 Digit )",
                        }); 
                        node.data.Qty = 0;
                        dataForGrid = node.data;
                        node.setData(dataForGrid);
                    }else{
                        gridPartsToEnd('#gridParts .ag-body-horizontal-scroll-viewport',500);
                        console.log("on Qty change ===>",node.data);
                        dataForGrid.SatuanId = node.data.UomId;
                        dataForGrid.Name =  node.data.Name;

                        if(node.data.Qty == NaN || node.data.Qty == undefined || node.data.Qty == ""){
                            console.log('node.data.Qty tadinya NAN ===>',node.data.Qty);
                            node.data.Qty = 0;
                            node.data.qty = 0;
                        }

                        if(node.data.SatuanId == undefined){
                            node.data.Name = "Pcs";
                            node.data.SatuanId = 4;
                        }

                        node.data.Qty = parseInt(node.data.Qty);
                        node.data.qty = parseInt(node.data.qty);




                        // if(lastDiscountParts !== null || lastDiscountParts !== undefined || lastDiscountParts !== "" || lastDiscountParts !== 0){
                        //     node.data.Discount = lastDiscountParts;
                        // }else{
                        //     node.data.Discount = $scope.DiscountParts;
                        // }

                        if(node.data.PartsId == ""){
                            if(node.data.RetailPrice !== ""){
                                // if(node.data.TipeMaterial == "Bahan" && node.data.PaidBy == 28){//customer
                                //     node.data.Discount = 0;
                                //     node.data.SubTotal = (res.data.Result[0].RetailPrice * node.data.qty) - ((res.data.Result[0].RetailPrice * node.data.qty));
                                // }else{
                                    node.data.SubTotal = (res.data.Result[0].RetailPrice * node.data.qty) - ((res.data.Result[0].RetailPrice * node.data.qty) * (node.data.Discount/100));
                                // }
                                dataForGrid = node.data;
                                node.setData(dataForGrid);
                            }
                        }else{
                            AppointmentBpService.getAvailableParts(node.data.PartsId, 0, val).then(function(res) { 

                                // ====== validasi cek kl retailprice nya null ato 0 ga blh di pake ==================================================== start
                                if ((res.data.Result[0].RetailPrice === 0 || res.data.Result[0].RetailPrice === null || res.data.Result[0].RetailPrice === undefined) && node.data.TipeMaterial == "Spare Parts"){

                                    bsAlert.warning('Harga Retail Belum Tersedia. Harap hubungi Partsman/Petugas Gudang Bahan.');
                                    return
                                }
                                // ====== validasi cek kl retailprice nya null ato 0 ga blh di pake ==================================================== end
                                
                                for(var x = 0; x <3 ;x++){ // di looping karna kalo 1x subtotalnya jd NaN,
                                    console.log('looping Qty biar dapet subtotal ===>',x);
                                    console.log("data yagn didapat",res.data.Result);
                                    $scope.GridPartsApi.forEachNode( function(node,index){
                                        console.log("node to Update oi oi=>",node,node.data,index);
                                        console.log("res.data.Result[0].PartsCode",res.data.Result[0].PartsCode);
                                        // if($scope.mEstimationBP.TipeDisc == -1){
            
                                        //     node.data.Discount = 0;
                                            
                                        // }else{
            
                                            node.data.Discount = parseFloat(node.data.Discount);   
                                        // }
                                        if(node.data.PartsCode == res.data.Result[0].PartsCode){
            
                                            console.log("node.data.RetailPrice",res.data.Result[0].RetailPrice,node.data.qty,node.data.Discount);
                                            console.log('DPnya',res.data.Result[0].PercentDP)
                                            if(res.data.Result[0].isAvailable == 0 && node.data.PaidBy == 'Customer'){
                                                $scope.mEstimationBP.DP = res.data.Result[0].PercentDP;
                                            }
                                            console.log('DP MAster',$scope.mEstimationBP.DP)

                                            // if(node.data.TipeMaterial == "Bahan" && node.data.PaidBy == 28){//customer
                                            //     node.data.Discount = 0;
                                            //     node.data.SubTotal = (res.data.Result[0].RetailPrice * parseInt(node.data.qty)) - ((res.data.Result[0].RetailPrice * parseInt(node.data.qty)));
                                            // }else{
                                                node.data.SubTotal = (res.data.Result[0].RetailPrice * parseInt(node.data.qty)) - ((res.data.Result[0].RetailPrice * parseInt(node.data.qty)) * (node.data.Discount/100));
                                            // }
                                            console.log("node to Update=>",node.data,index);
                                            dataForGrid = node.data;
                                            _.map($scope.paymentData,function(val){
                                                if(val.MasterId == $scope.mEstimationBP.PaidById ){
                                                    dataForGrid.PaidBy = val.Name;
                                                }
                                            })
                                            switch (res.data.Result[0].isAvailable) {
                                                case 0:
                                                    dataForGrid.Availbility = "Tidak Tersedia";
                                                    if (res.data.Result[0].DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                                        dataForGrid.ETA =  $filter('date')(res.data.Result[0].DefaultETA, 'yyyy-MM-dd');;
            
                                                    } else {
            
                                                        dataForGrid.ETA = $filter('date')(res.data.Result[0].DefaultETA, 'yyyy-MM-dd');
                                                    }
                                                    dataForGrid.OrderType = 3;
                                                    break;
                                                case 1:
                                                    dataForGrid.Availbility = "Tersedia";
                                                    break;
                                                case 2:
                                                    dataForGrid.Availbility = "Tersedia Sebagian";
                                                    break;
                                            }

                                            
            
                                            dataForGrid.RetailPrice = res.data.Result[0].RetailPrice;
                                            dataForGrid.Qty = parseInt(val);
                                            dataForGrid.qty = parseInt(val);
                                            dataForGrid.PriceDP = res.data.Result[0].PriceDP;
                                            dataForGrid.DownPayment = res.data.Result[0].TotalDP;
                                            dataForGrid.DPRequest = res.data.Result[0].TotalDP;

                                            var finData = dataForGrid;
                                            node.setData(finData);
            
                                        }
                                    });
                                    console.log("dataForGrid",dataForGrid);
                                }
                            },function(err) {
                                    //console.log("err=>", err);
                            });
                        }
                    }
                }
            
                if(field == 'PartsCode'){
                    console.log('dt.PartsCode ===>',dt.PartsCode);
                    $scope.searchMaterialAg(dt.PartsCode,1).then(function(res){
                        console.log("searchMaterialAg resA=>",res);
                        console.log('data',data);
                        if(!res.msg){

                            if(res.PartsClassId1 == 1){
                                res.TipeMaterial = "Spare Parts";
                                ChoosedMaterial = 1
                            }else{
                                res.TipeMaterial = "Bahan";
                                ChoosedMaterial = 2
                            }

                            res.SatuanId = res.UomId;

                            console.log("masuk A");
                            console.log('res  ===>',res);
                            console.log('node ===>',node);
                            $scope.recalcRow(res,node); 
                            $scope.calcTotal();
                        }else{
                            // node.updateData({remove: [res]})
                            var filtered = $scope.masterParts.filter(function(item) {
                                console.log('filtered ====>',filtered)
                            // if(data.PartsId != ""){
                            //     console.log('masuk 1')
                            //     return item.PartsId !== data.PartsId;
                            // }else if(data.PartsName != ""){ 
                            //     console.log('masuk 2')
                            //     return item.PartsName !== data.PartsName;
                            // }else if(data.PartsCode != ""){
                            //     console.log('masuk 3')
                            //     return item.PartsCode !== data.PartsCode;
                            // }
                            return item.PartsId !== "";  
                            });
                            console.log("membersihkan data yang tidak ada",filtered);
                            console.log('masternya',$scope.masterParts)
                            console.log("filtered.length",filtered.length);
                            var partcodess = _.findLastIndex($scope.masterParts, { "PartsCode": dt.PartsCode });
                            console.log('indes',partcodess)
                            $scope.masterParts.splice(partcodess,1);
                            filtered.splice(partcodess,1);
                            for(var f = 0; f < filtered.length; f++){
                                console.log("index",f);
                                filtered[f].No = f + 1;
                            }
                            $scope.masterParts = filtered;
                            $scope.GridPartsApi.updateRowData({remove: [dt]});
                            $scope.GridPartsApi.setRowData($scope.masterParts);
                            //$scope.agGridUbahApi.updateRowData({remove: [dt]});
                        }
                    });
                }

                if(field == 'PartsName'){
                    $scope.searchMaterialAg(dt.PartsName,0).then(function(res){
                        console.log("searchMaterialAg resA=>",res);
                        if(!res.msg){

                            if(res.PartsClassId1 == 1){
                                res.TipeMaterial = "Spare Parts";
                                ChoosedMaterial = 1
                            }else{
                                res.TipeMaterial = "Bahan";
                                ChoosedMaterial = 2
                            }


                            // if(ChoosedMaterial == 2){
                            //     res.TipeMaterial = "Bahan";
                            // }else if(ChoosedMaterial == 1){
                            //     res.TipeMaterial = "Spare Parts";
                            // }
                            res.SatuanId = res.UomId;
                            console.log("masuk A Name");
                            $scope.recalcRow(res,node);
                            $scope.calcTotal();
                        }else{
                            // node.updateData({remove: [res]})
                            var filtered = $scope.masterParts.filter(function(item) { 
                               return item.PartsId !== "";  
                            });
                            console.log("membersihkan data yang tidak ada name",filtered);
                            //$scope.masterParts = filtered;
                            $scope.GridPartsApi.updateRowData({remove: [dt]});
                            //$scope.agGridUbahApi.updateRowData({remove: [dt]});
                        }
                    });
                }
 
                if(field == 'Discount'){
                    console.log("mEstimationBP.isCash",$scope.mEstimationBP.isCash);
                    dataForGrid = node.data; //isi dataforgrid selalu dari data terakhir yang di input. isian dataforgrid dan node.data tidak selalu sama, jadi di samain disini
                    node.data.Discount = parseFloat(val);
                    dataForGrid.Discount = parseFloat(val);
                    
                    console.log('node.data.Discount ===>',node.data.Discount)

                    if($scope.mEstimationBP.isCash == 0){
                        dataForGrid = node.data;
                        dataForGrid.Discount = $scope.disParts;
                        node.setData(dataForGrid);
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Diskon Berdasarkan Asuransi Yang Dipilih",
                        }); 

                    }else{
                        if(node.data.Discount >= 0){
                            if(node.data.Discount > 100.00){
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Diskon Tidak Boleh Melebihi 100",
                                }); 
                                node.data.Discount = 0;
                                dataForGrid = node.data;
                                node.setData(dataForGrid);
                            }else{
                                lastDiscountParts = node.data.Discount;
                                dataForGrid.SatuanId = node.data.UomId;
                                dataForGrid.Name =  node.data.Name;
                                if(node.data.PartsId == ""){
                                    if(node.data.TipeMaterial == "Bahan" && node.data.PaidBy == 28){//customer
                                        node.data.Discount = 0;
                                        node.data.SubTotal =  (node.data.RetailPrice * node.data.Qty) - ((node.data.RetailPrice * node.data.Qty));
                                    }else{
                                        node.data.SubTotal =  (node.data.RsetailPrice * node.data.Qty) - ((node.data.RetailPrice * node.data.Qty) * node.data.Discount/100);
                                    }
        
                                    dataForGrid = node.data;
                                    node.setData(dataForGrid);
                                }else{
                                    
                                    var qty_x = 0
                                    if (node.data.Qty != null && node.data.Qty != undefined) {
                                        qty_x = node.data.Qty
                                    } else {
                                        qty_x = node.data.qty
                                    }
                                    AppointmentBpService.getAvailableParts(node.data.PartsId, 0, qty_x).then(function(res) { 
                                        console.log("data yagn didapat | Discount",res.data.Result);
            
                                        dataForGrid.RetailPrice = res.data.Result[0].RetailPrice;
                                        dataForGrid.PriceDP = res.data.Result[0].PriceDP;
                                        dataForGrid.DownPayment = res.data.Result[0].TotalDP;
                                        dataForGrid.DPRequest = res.data.Result[0].TotalDP;
                                        $scope.GridPartsApi.forEachNode( function(node,index){
                                            console.log("node to Update oi oi=>",node,node.data,index);
                                            console.log("res.data.Result[0].PartsCode",res.data.Result[0].PartsCode);
                                            if(node.data.PartsCode == res.data.Result[0].PartsCode){
                                                console.log("node to Update=>",node.data,index);
                                                // if($scope.mEstimationBP.TipeDisc == -1 && node.data.Discount > 0){
            
                                                //     dataForGrid.Discount = 0;
            
                                                //     bsNotify.show({
                                                //         size: 'big',
                                                //         type: 'danger',
                                                //         title: "Diskon Tidak Boleh Melebihi 0 Karena Tipe Discount Tidak Ada Discount",
                                                //     }); 
                                                // }else
    
    
                                                // if(node.data.TipeMaterial == "Bahan" && (node.data.PaidBy == 28 || node.data.PaidBy == 'Customer')){
                                                //     dataForGrid.Discount = 0;
                                                //     bsNotify.show({
                                                //         size: 'big',
                                                //         type: 'danger',
                                                //         title: "Tipe material Bahan dengan pembayaran customer tidak mendapatkan diskon",
                                                //     }); 
                                                // }else{
                                                    console.log("masuk bawah dong");
                                                    // if($scope.mEstimationBP.isCash == 0 && $scope.mEstimationBP.TipeDisc == "" || $scope.mEstimationBP.TipeDisc == null || $scope.mEstimationBP.TipeDisc == undefined){
                                                    if($scope.mEstimationBP.isCash == 0){
                                                            dataForGrid.Discount = $scope.disParts;
                                                        bsNotify.show({
                                                            size: 'big',
                                                            type: 'danger',
                                                            title: "Diskon Berdasarkan Asuransi Yang Dipilih",
                                                        }); 
            
                                                    }
                                                // }
        
                                                // if(node.data.TipeMaterial == "Bahan" && (node.data.PaidBy == 28 || node.data.PaidBy == 'Customer')){//customer
                                                //     dataForGrid.Discount = 0;
                                                //     node.data.Discount = 0;
                                                //     node.data.SubTotal = (node.data.RetailPrice * parseInt(node.data.Qty)) - ((node.data.RetailPrice * parseInt(node.data.Qty)));
                                                // }else{
                                                    node.data.SubTotal = (node.data.RetailPrice * parseInt(node.data.Qty)) - ((node.data.RetailPrice * parseInt(node.data.Qty))*dataForGrid.Discount/100);
                                                // }z
                                                var finData = dataForGrid;
                                                node.setData(finData);
            
                                            }
                                        });
    
                                        console.log("dataForGrid",dataForGrid);
                                    },function(err) {
                                            //console.log("err=>", err);
                                    });
                                }
                            } 
                        }else{
                            console.log('ini diskonnya ga di isi ===>',node.data.Discount);
                            node.data.Discount = 0;
                            dataForGrid = node.data;
                            node.setData(dataForGrid);
                        }
                    }

                    
                    
                }

                if(field == 'PaidBy'){
                    dataForGrid.PaidBy = null;
                    console.log("all data when paymen changed ===>",idx,field,val,node);
                    console.log('dataForGrid ===>',dataForGrid);
                    console.log('selected payment method ===>', val)

                    var findPaid = _.find($scope.paymentData, {"Name":val});
                    dataForGrid.PaidBy = findPaid.Name;
                    dataForGrid.PaidById = findPaid.MasterId;

                    // for(var z in $scope.paymentData){
                    //     if($scope.paymentData[z].Name == val){
                    //         dataForGrid.PaidBy = val;
                    //         dataForGrid.PaidById = $scope.paymentData[z].MasterId;
                    //         console.log('catch the value payment method ===>',dataForGrid.PaidBy,dataForGrid.PaidById);
                    //     }
                    // }
                    // var satuanPartsYangDiedit = dataForGrid;
                    // $scope.masterParts.splice(idx,1,satuanPartsYangDiedit);
                }

                if(field == 'Name'){
                    dataForGrid.Name = null;
                    console.log('dataForGrid | Name ===>',dataForGrid);
                    var findSatuan = _.find($scope.unitData, {"Name":val});
                    console.log('findSatuan ===>',findSatuan);
                    dataForGrid.Name = findSatuan.Name;
                    dataForGrid.SatuanId = findSatuan.MasterId;
                }




            }
        }; // end - onAfterCellEditAg

        $scope.recalcRow = function(dt,node){
            console.log("recalcRow=>",dt,node);
            // dt.PartsName = ;
            // dt.QtySO = parseInt(dt.QtySO);
            // dt.DiscountMaterialAmount = (dt.DiscountMaterialPercent * (dt.QtySO * dt.RetailPrice)) / 100; // Diskon Material Amount
            // dt.DiscountSpecialAmount = (dt.DiscountSpecialPercent * (dt.QtySO * dt.RetailPrice)) / 100; // Diskon Spesial
            // dt.DiscountGroupMaterialAmount = (dt.DiscountGroupMaterialPercent * (dt.QtySO * dt.RetailPrice)) / 100; // Diskon Grup Material
            // var totalDiskon = 0 + dt.DiscountMaterialAmount + dt.DiscountSpecialAmount + dt.DiscountGroupMaterialAmount;
            // console.log("DMA",dt.DiscountMaterialAmount);
            // console.log("DSA",dt.DiscountSpecialAmount);
            // console.log("DGMA",dt.DiscountGroupMaterialAmount);
            // console.log("TD",totalDiskon);

            // dt.PartId = '';          
            // dt.PartsId = '';         
            // dt.PartsName = '';            
            // dt.TotalAmount = ((dt.QtySO * dt.RetailPrice) - totalDiskon) + dt.VATAmount;
            node.updateData(dt);
            // $scope.masterParts.push(dt);
            console.log("$scope.mData.masterParts",$scope.masterParts);
            console.log("$scope.GridParts.rowData on recalc",$scope.GridParts.rowData);
            // console.log("TC",dt.TotalCost);
            // console.log("VA",dt.VATAmount);
            // console.log("TA",dt.TotalAmount);
        }

        $scope.calcTotal = function(){
            //Calc Total
            // var vTotalAmount = 0;
            // var vDPAmount = 0;

            // $scope.agGridApi.forEachNode( function(node,index){
            //     vTotalAmount += node.data.TotalAmount;
            //     vDPAmount += node.data.DPAmount;
            // })
            // console.log('vTotalAmount,vDPAmount', vTotalAmount,vDPAmount);
            // $scope.mData.TotalAmount = vTotalAmount;
            // $scope.mData.DPAmount = vDPAmount;
            // if ($scope.mData.TotalAmount == null || $scope.mData.TotalAmount === undefined || $scope.mData.TotalAmount === 'NaN') { $scope.mData.TotalAmount = 0 };
            // console.log('$scope.mData.TotalAmount =>>', $scope.mData.TotalAmount);
            // console.log('$scope.mData.DPAmount =>>', $scope.mData.DPAmount);
        }
        // var HitungParts = [];
         // $scope.mData.GridDetail = {};
        $scope.searchMaterialAg = function (PartsCode,flag) {
            var defer = $q.defer();
            // added by JH on 2019-07-25
            console.log("flag",flag);
            console.log("ChoosedMaterial",ChoosedMaterial);
            console.log("PartsCode",PartsCode);
            // console.log("PartsId",dataParts.PartsId);
            console.log("$scope.gridOptionsLangsungAg",$scope.GridParts);
            var flagPartCode = 0;
            $scope.GridPartsApi.forEachNode( function(node,index){
                if(node.data.PartsCode == PartsCode || node.data.PartsName == PartsCode){
                    flagPartCode ++;
                }
            })
            // $scope.agGridUbahApi.forEachNode(function(node){
            //     if(node.data.PartCode == PartCode){
            //         flagPartCode ++;
            //     }
            // })
            console.log("flagPartCode=>",flagPartCode);
            if(dataParts.PartsId == undefined){
                dataParts.PartsId = 9099999;//kode asal nih
            }
            console.log("dataParts",dataParts);
            // var gridData = {};
            if(flagPartCode <= 1){
                    estimasiBPFactory.getDataPartsDetailEstimasiBP(dataParts.PartsId).then(function (res) {
                           // if(res.data.Result.length > 0){

                             var gridData = res.data.Result[0];

                            // }else{

                            //     gridData.FlagPartsId= Math.floor(Math.random() * 10000) + 1;
                            //     gridData.PartId='';
                            //     gridData.PartsId='';
                            //     // gridData.TipeMaterial:
                            // }
                            console.log("gridData murni",gridData);
                               // var gridData = {

                               // };
                           // }
                            if(gridData != undefined){

                                gridData.TipeMaterial = TipMaterial;
                                gridData.FlagPartsId =  Math.floor(Math.random() * 10000) + 1;
                            }
                            console.log("gridData",gridData);
                            console.log("masterpatrs awal",$scope.masterParts);
                            $scope.GridPartsApi.forEachNode( function(node,index){
                                console.log("node to Update=>",node,node.data,index,PartsCode);
                                if(flag == 1){
                                    // if(gridData != undefined){

                                        if(node.data.PartsCode == PartsCode){
                                            console.log("node to Update wakwaw=>",node.data,index,gridData);
                                            if(gridData == undefined){
                                                // node.data.FlagPartsId =  Math.floor(Math.random() * 10000) + 1;
                                                node.data.PartId = '';
                                                node.data.PartsId = '';
                                                node.data.PartsName = '';

                                                data = node.data;
                                                dataForGrid = node.data;

                                                
                                                node.setData(data);

                                                for(var i = 0; i < $scope.masterParts.length; i++){
                                                        if($scope.masterParts[i].PartsCode == PartsCode){
                                                            node.data.No = $scope.masterParts[i].No;
                                                            node.data.Discount = $scope.masterParts[i].Discount;
                                                            $scope.masterParts[i] = node.data;
                                                            console.log("masterParts",$scope.masterParts);
                                                        }
                                                }
                                            }else{

                                                 data = gridData;
                                                 dataForGrid = gridData; 

                                                 
                                                 node.setData(data);

                                                 for(var i = 0; i < $scope.masterParts.length; i++){
                                                         if($scope.masterParts[i].PartsCode == PartsCode){
                                                             gridData.No = $scope.masterParts[i].No;
                                                             gridData.Discount = $scope.masterParts[i].Discount;
                                                             $scope.masterParts[i] = gridData;
                                                             console.log("masterParts",$scope.masterParts);
                                                         }
                                                 }
                                            }

                                        }
                                    // }else{

                                    //     bsNotify.show({
                                    //       size: 'small',
                                    //       type: 'notif',
                                    //       title: "Kode Material Tidak Ditemukan",
                                    //     });
                                    //     // defer.resolve({msg:"Kode Material Tidak Ditemukan"}); 
                                    // }
                                }else{

                                     if(node.data.PartsName == PartsCode){
                                         console.log("node to Update wakwaw=>",node.data,index,gridData);
                                         // var data = node.data;

                                         // gridData.No = $scope.masterParts.length;
                                         if(gridData == undefined){
                                             // node.data.FlagPartsId =  Math.floor(Math.random() * 10000) + 1;
                                             node.data.PartId = '';
                                             node.data.PartsId = '';
                                             node.data.PartsCode = '';

                                             data = node.data;
                                             dataForGrid = node.data;

                                             node.setData(data);

                                             for(var i = 0; i < $scope.masterParts.length; i++){
                                                     if($scope.masterParts[i].PartsCode == PartsCode){
                                                         node.data.No = $scope.masterParts[i].No;
                                                         node.data.Discount = $scope.masterParts[i].Discount;
                                                         $scope.masterParts[i] = node.data;
                                                         console.log("masterParts",$scope.masterParts);
                                                     }
                                             }
                                         }else{

                                             data = gridData;
                                             dataForGrid = gridData;

                                             node.setData(data);
                                             
                                             for(var i = 0; i < $scope.masterParts.length; i++){
                                                     if($scope.masterParts[i].PartsName == PartsCode){
                                                         gridData.No = $scope.masterParts[i].No;
                                                         gridData.Discount = $scope.masterParts[i].Discount;
                                                         $scope.masterParts[i] = gridData;
                                                         console.log("masterParts by name",$scope.masterParts);
                                                     }
                                             }
                                         }
                                     }   
                                }
                            });

                            // $scope.gridOptionsLangsungAg.rowData[$scope.gridOptionsLangsungAg.rowData.length - 1] = gridData[0];
                            console.log("$scope.GridParts.rowData",$scope.GridParts.rowData);
                            // $scope.mData.GridDetail = $scope.GridParts.rowData;
                            // console.log("$scope.mData.GridDetail",$scope.mData.GridDetail);
                            // HitungParts.push(dataParts);
                            // $scope.masterParts.push({

                            //     "No" :  $scope.masterParts.length + 1,
                            //     "PartsCode": dataParts.PartsCode,
                            //     "PartsName": dataParts.PartsName,
                            // });
                            console.log(" $scope.masterParts", $scope.masterParts);
                            console.log('gridDataC = ', gridData);
                            defer.resolve(gridData);
                        },
                        function(err) {
                                //console.log("err=>", err);
                        }
                    );
                
                
            }else{
              if(flag == 1){
                  bsNotify.show({
                    size: 'small',
                    type: 'notif',
                    title: "Kode Material '"+PartsCode+"' Sudah Ada",
                  });
                  defer.resolve({msg:"Kode Material '"+PartsCode+"' Sudah Ada"})
              }else{
                  bsNotify.show({
                    size: 'small',
                    type: 'notif',
                    title: "Nama Material '"+PartsCode+"' Sudah Ada",
                  });
                  defer.resolve({msg:"Nama Material '"+PartsCode+"' Sudah Ada"});        
              }
            }
            return defer.promise;
        } // end - $scope.SearchMaterialAg

        // var disPartsDude = false;
        // $scope.whenGridReady = function(){
        //     if($scope.mEstimationBP.isCash == 0 || $scope.mEstimationBP.isCash == '0'){
        //        disPartsDude = false;
        //     }else{
        //       disPartsDude = true;
        //     }
        //     console.log("disPartsDude",disPartsDude);
        //     $scope.GridPartsApi.updateRowData({add: []});
        // }
        // ag SpkListGrid
        // $scope.SpkListGrid.data = $scope.master;

        $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];

        function formatNumber(number) {
            return Math.floor(number)
            .toString()
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }
        
        $scope.GridParts = {
            //DEFAULT AG-GRID OPTIONS
            // default ColDef, gets applied to every column
            defaultColDef: {
                width: 150,
                editable: false,
                filter: false,
                sortable: true,
                resizable: true
            },
            rowSelection: 'multiple',
            suppressRowClickSelection: true,
            suppressCellSelection: true,
            angularCompileRows: true,
            components: {
                partEditor: PartEditor,
                PartNameEditor : PartNameEditor,
                numericCellEditor : NumericCellEditor,
                checkboxEditor: agCheckboxCellEditor,
                noteCellEditor : noteCellEditor

                
            },
            onGridReady: function (params) {
                // params.api.sizeColumnsToFit();
                $scope.GridPartsApi = params.api;

                // var allColumnIds = [];
                // $scope.GridParts.columnApi.getAllColumns().forEach(function(column) {
                //     allColumnIds.push(column.colId);
                // });

                // $scope.GridParts.columnApi.autoSizeColumns(allColumnIds, false);

            },
            

            rowData: $scope.masterParts,
            onCellEditingStopped: function (e) {
                console.log('parts cellEditingStopped=>', e.rowIndex, e.colDef.field, e.value, e.node, e);
                // if(e.colDef.field == 'PartCode'){
                if($scope.disJob == false){
                    $scope.onAfterCellEditAg(e.rowIndex,e.colDef.field,e.value,e.node);
                }
                // }
            },

            sideBar: {
                    toolPanels: ['columns'],
                    hiddenByDefault: false
            },
            toolPanelSuppressSideButtons:true,
        
            
            columnDefs: [

                { headerName: 'No.', field: 'No', width: 60},
                { headerName: 'Tipe Material', field: 'TipeMaterial', width: 150,  editable: disOPB,cellEditor: 'agRichSelectCellEditor',cellEditorParams: {values: ['Spare Parts','Bahan']}},
                { headerName: 'Kode Material', field: 'PartsCode', width: 150, editable: disOPB, cellEditor: 'partEditor'},
                { headerName: 'Nama Material', field: 'PartsName', width: 200, editable: true, cellEditor: 'noteCellEditor'},
                { headerName: 'Qty', field: 'Qty', width: 60, editable: true, cellEditor: 'numericCellEditor'},
                { headerName: 'Satuan', field: 'Name', width: 150, cellEditor: 'agRichSelectCellEditor',editable: true, cellEditorParams: {values: listSatuan}},
                { headerName: 'Ketersediaan', field: 'Availbility', width: 150},
                { headerName: 'ETA', field: 'ETA', width: 200, cellRenderer: customDate},
                { headerName: 'Harga', field: 'RetailPrice', width: 150, editable: gridPartsIsOPB,  cellEditor: 'numericCellEditor', cellStyle: {textAlign: "right"},
                    cellRenderer: function(params){ 
                        var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
                        var notOk = '<span style="float:left">Rp.</span>'+ 0;
                        if(params.value ==  null || params.value ==  undefined || params.value == "" ){
                            return notOk;
                        } else{
                            return isOke;
                        }
                    }
                },
                //{ headerName: 'Diskon (%)', field: 'Discount', width: 100, editable: true, cellEditor: 'numericCellEditor'},
                { headerName: 'Diskon (%)', field: 'Discount', width: 100, editable: true, cellEditor: 'numericCellEditor'},
                // { headerName: 'Diskon (%)', field: 'Discount', width: 100, editable: true, cellEditor: 'numericCellEditor', editable: isDisableDiscount},
                { headerName: 'Sub Total', field: 'SubTotal', width: 150, cellStyle: {textAlign: "right"},
                    cellRenderer: function(params){ 
                        var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
                        var notOk = '<span style="float:left">Rp.</span>'+ 0;
                        if(params.value ==  null || params.value ==  undefined || params.value == "" ){
                            return notOk;
                        } else{
                            return isOke;
                        }
                    }
                },
                // { headerName: 'OPB', field: 'IsOPB', width: 70, cellRenderer: OPBinputFunc },
                // { headerName: 'OPB', field: 'IsOPB', width: 70, cellRenderer: OPBinputFunc2, editable: true },
                { headerName: 'OPB', field: 'IsOPB', width: 70, cellEditor: 'checkboxEditor', cellRenderer: agCheckboxCellRenderer, editable: isEditOPB },
                // { headerName: 'Pembayaran', field: 'PaidBy', width: 150, cellEditor: 'agRichSelectCellEditor',editable: true, cellEditorParams: {values: ListPaymentName}},
                { headerName: 'Pembayaran', field: 'PaidBy', width: 150, cellEditor: 'agRichSelectCellEditor',editable: isEditPembayaranPart, cellEditorParams: {values: ListPaymentName}},

                // { headerName: 'Pembayaran', field: 'PaidBy', width: 200},
                // { headerName: 'DP (%)', field: 'DownPayment', width: 150},           
                { headerName: 'Action', width: 100, minWidth: 100, maxWidth: 100, suppressFilter: true, suppressMenu: true, pinned: 'right', cellRenderer: actionRenderer2}
            ]
        };

        // ----------------------- note editor -------------------------------------------------------------------------- start

				function noteCellEditor() {
				}
				noteCellEditor.prototype.init = function (params) {
					// create the cell

					var maxL = 200
					if (params.column.colId == 'PartsCode') {
						maxL = 32
					} else if (params.column.colId == 'PartsName'){
                        maxL = 100
                    }

					this.eInput = document.createElement('input')
					this.eInput.setAttribute('maxLength',maxL);
					console.log('DATAAA1 =>',params.charPress)
					console.log('DATAAA2 =>',params.value)
					
				
				};

				noteCellEditor.prototype.isKeyPressedNavigation = function (event){
					
				};

				// gets called once when grid ready to insert the element
				noteCellEditor.prototype.getGui = function () {
					return this.eInput;
				};

				// focus and select can be done after the gui is attached
				noteCellEditor.prototype.afterGuiAttached = function () {
					this.eInput.focus();
				};

				// returns the new value after editing
				noteCellEditor.prototype.isCancelBeforeStart = function () {
					return this.cancelBeforeStart;
				};

				// returns the new value after editing
				noteCellEditor.prototype.getValue = function () {
					return this.eInput.value;
				};

				// any cleanup we need to be done here
				noteCellEditor.prototype.destroy = function () {
					// but this example is simple, no cleanup, we could  even leave this method out as it's optional
				};

				// if true, then this editor will appear in a popup 
				noteCellEditor.prototype.isPopup = function () {
					// and we could leave this method out also, false is the default
					return false;
				};

				// ----------------------- note editor -------------------------------------------------------------------------- end

        function currencyFormatter(params) {
            console.log('params currencyFormatter ===>',params.value);

            if(params.value == "" || params.value == undefined){
                return '0';
            }else{
                return formatNumber(params.value);
            }
        }
        
        function gridPartsIsOPB(params){
            console.log("gridPartsIsOPB => ",params.data.IsOPB,params);
            if( (params.data.PartsName == "" &&  params.data.PartsCode != "") || (params.data.PartsName != "" &&  params.data.PartsCode == "") || params.data.IsOPB == true ){
                return true;
            }else{
                return false
            }
            
        }

        function disOPB(params){
            console.log("gridPartsIsOPB => ",params.data.IsOPB,params);
            if(params.data.IsOPB == true){
                return false;
            }else{
                return true;
            }
        }

        function customDate(param){
            console.log('param customDate ===>',param.value);
            console.log('param customDate ===>',typeof param.value);


            if(param.value == null){
                return '-';
            }else{
                if(param.value !== ""){
                    if(typeof param.value == 'object'){
                        var date = new Date(param.value);
                        console.log('date parsin ===>',date);
                        var thn = date.getFullYear();
                        var bln = ('0' + (date.getMonth()+1)).slice(-2);
                        var tgl = ('0' + date.getDate()).slice(-2);
                        var result = '' + tgl +'-'+  bln +'-'+ thn +'';
                        console.log('thn --->',thn);
                        console.log('bln --->',bln);
                        console.log('tgl --->',tgl);
                        console.log('result parsing --->',result)
                        return result
                    }else if (typeof param.value == 'string'){
                        console.log('date normal ===>',date);
                        var date = param.value.split('-');
                        var thn = date[0];
                        var bln = date[1];
                        var tgl = date[2];
                        var result = '' + tgl +'-'+  bln +'-'+ thn +'';
                        console.log('thn --->',thn);
                        console.log('bln --->',bln);
                        console.log('tgl --->',tgl);
                        console.log('result normal--->',result)
                        return result
                    }else{
                        return  '-';
                    }
                }else{
                    return '-';
                }
            }

            
        }

        $scope.diskonEdit = true;
        function isDisableDiscount(){
            console.log('$scope.diskonEdit ===>',$scope.diskonEdit);
            if($scope.diskonEdit == true){
                return true;
            }else{
                return false;
            }
        }


        

        function actionRenderer2() {
            var actionHtml = ' <button ng-disabled="disJob" skip-enable class="ui icon inverted grey button"\
                                style="font-size:15px;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px"\
                                ng-click="deleteDataParts(data)">\
                                <i class="fa fa-trash"></i>\
                            </button>';
            return actionHtml;
        }

        var partsOPB = null;
        // function OPBinputFunc  (params) {
        //     console.log('params | OPBinputFunc ===> ',params    );
        //     return '<bscheckbox style="clear:both !important" ng-model="partsOPB"  ng-change="OPBchecks(data, partsOPB)" ng-true-value="1" ng-false-value="null"></bscheckbox>';
        // }

        // $scope.OPBchecks =  function (data, partsOPB){
        //     console.log('partsOPB ===>',partsOPB);
        //     console.log('data ===>',data);
        // }

        function OPBinputFunc2  (params) {
            console.log('params | OPBinputFunc ===> ',params);
            if(params.data.TipeMaterial == "Bahan"){//parts
                return '<input type="checkbox" disabled="true" checked></input>';
            }else{
                return '<input type="checkbox" disabled="true"></input>';
            }
            
        }

        function isEditOPB(params){
            console.log('isEditOPB | params =========> ',params.data);
            console.log('isEditOPB | PartsId ========> ',params.data.PartsId, typeof params.data.PartsId);
            console.log('isEditOPB | TipeMaterial ===> ',params.data.TipeMaterial);
            
            if(params.data.TipeMaterial == "Bahan"){
                if(typeof params.data.PartsId == "number" ){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Kode Material Sudah Terisi, Mohon Input Bahan Tanpa Kode Material",
                    });
                    return false
                }else{
                    return true
                }
            }else{
                return false
            }
        }

        function isEditPembayaranPart(params) {
            if (params.data.PaidById == 30) {
                return false
            } else {
                true
            }
        }


       
        var tmpDeleteParts = [];
        $scope.deleteDataParts = function(data){
            console.log("data parts delete",data);
            console.log("$scope.dataGoodWork",$scope.dataGoodWork);
            if(data.PartsId == "" && data.PartsName == "" && data.PartsCode == "" ){
                console.log("isi dulu bgst");

                for(var x in $scope.masterParts ){
                    console.log('masuk sini',$scope.masterParts)
                    console.log('masuk sini 2',data)
                    //console.log('masuk 3',item)
                    if($scope.masterParts[x].IdFlagParts == data.IdFlagParts){
                        console.log('masuk sini1')
                        $scope.masterParts.splice(x,1);
                    } 
                }

                var filtered = $scope.masterParts.filter(function(item) { 
                    console.log('filtered ====>',filtered)
                  return item.PartId =="";

                });

                console.log("harusnua data after delete",filtered);
                console.log("filtered.length",filtered.length);
                for(var f = 0; f < $scope.masterParts.length; f++){
                    console.log("index",f);
                    $scope.masterParts[f].No = f + 1;
                }
                console.log("filter after",filtered);

                $scope.GridPartsApi.setRowData($scope.masterParts); 
                console.log('masterpartss ===>',$scope.masterParts);


            }else{
                 // if(data.isDeleted != undefined){

                     data.isDeleted = 1;
                     data.isCancel = 1;
                     if(data.EstPartsId == undefined){
                        console.log('data ga masuk ke tmpDeleteParts ====>',data);
                    }else{
                        tmpDeleteParts.push(data);
                        console.log('data masuk ke tmpDeleteParts ====>',data);
                    }

                     // if(data.EstPartsId == undefined){

                     //     for(var a = 0; a<$scope.dataGoodWork.length; a++){
                     //         for(var c = 0; c < $scope.dataGoodWork[a].AEstimationBP_PartList.length; c++){
                     //             $scope.dataGoodWork[a].AEstimationBP_PartList.splice(c,1)
                     //         }
                     //     }  
                     // }else{

                     //      for(var a = 0; a<$scope.dataGoodWork.length; a++){
                     //          for(var c = 0; c < $scope.dataGoodWork[a].AEstimationBP_PartList.length; c++){
                     //              if($scope.dataGoodWork[a].AEstimationBP_PartList[c].EstPartsId == data.EstPartsId){
                     //                  $scope.dataGoodWork[a].AEstimationBP_PartList[c].isDeleted = 1;
                     //              }
                     //          }
                     //      }   
                     // }
                 // }
                 console.log("$scope.dataGoodWork after cuy",$scope.dataGoodWork);
                 console.log("masterpart",$scope.masterParts);
                 // console.log("data goodwork after delete",$scope.dataGoodWork);
                 
                var filtered = $scope.masterParts.filter(function(item) { 
                     console.log('filtered ====>',filtered)
                    if(data.PartsId != ""){
                        return item.PartsId !== data.PartsId;
                    }else if(data.PartsName != ""){ 
                        return item.PartsName !== data.PartsName;
                    }else if(data.PartsCode != ""){
                        return item.PartsCode !== data.PartsCode;
                    }
                });

                // var filtered = _.remove($scope.masterParts, function(n){
                //    return n.IdFlagParts != data.IdFlagParts
                // })
                    
                    console.log("harusnua data after delete",filtered);
                    console.log("filtered.length",filtered.length);
                    for(var f = 0; f < filtered.length; f++){
                        console.log("index",f);
                        filtered[f].No = f + 1;
                    }
                    console.log("filter after",filtered);
                    $scope.masterParts = filtered;
                    var partcodess = _.findIndex($scope.masterParts, { "Availbility": "Tidak Tersedia" });
                    console.log("masterpart",partcodess);
                   if(partcodess != -1){
                       $scope.mEstimationBP.DP = $scope.mEstimationBP.DP;
                   }else{
                       $scope.mEstimationBP.DP = 0;
                   }

                 $scope.GridPartsApi.setRowData($scope.masterParts);   
            }
        }

        var dataTask = {TaskListBPId:undefined};
        var globalDP = 0;
        $scope.saveFin = function(param){

            $scope.GridPartsApi.stopEditing(); // stop edit cell di agGrid, jika tidak pencet enter / pindah kolom
            
            var Id =  Math.floor(Math.random() * 10000) + 1;
            console.log('saveFin | param ===========>',param)
            console.log("saveFin | tmpDeleteParts ==>",tmpDeleteParts);
            console.log("saveFin | masterParts =====>",$scope.masterParts);
            console.log("saveFin | mEstimationBP ===>",$scope.mEstimationBP);
            console.log("saveFin | dataTaskSave ====>",dataTaskSave);
            var PartsSave = [];
            var TmpPartsAgain = [];
            var TipeMaterial = null;
            // console.log("viera",data);

            for (var i=0; i<$scope.masterParts.length; i++) {
                if ($scope.masterParts[i].PartsCode != '' && $scope.masterParts[i].PartsCode != null && $scope.masterParts[i].PartsCode != undefined) {
                    if (($scope.masterParts[i].RetailPrice == '' || $scope.masterParts[i].RetailPrice == 0 || $scope.masterParts[i].RetailPrice == null || $scope.masterParts[i].RetailPrice == undefined) && $scope.masterParts[i].TipeMaterial != 'Bahan') {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Terdapat parts dengan harga retail yang belum tersedia. Harap hubungi Partsman/Petugas Gudang Bahan.",
                        });
                        return
                    }
                }
            }
            

            // console.log('dataTask update ===>',dataTask);
            // if(dataTask.TaskListBPId == undefined || typeof dataTask.TaskListBPId ==undefined){
            //     console.log("TaskListBPId nya kosong");
            //     dataTask.TaskListBPId = null;
            // }
            for(var x in $scope.mEstimationBP.AEstimationBP_TaskList_TT)
            {
                for(var y in $scope.mEstimationBP.AEstimationBP_TaskList_TT[x].AEstimationBP_PartList)
                {
                    console.log('indexxx part',$scope.mEstimationBP.AEstimationBP_TaskList_TT[x].AEstimationBP_PartList)
                    if($scope.mEstimationBP.AEstimationBP_TaskList_TT[x].AEstimationBP_PartList[y].Availbility == 'Tidak Tersedia')
                    {
                        $scope.mEstimationBP.DP = $scope.mEstimationBP.DP;
                    }else if($scope.mEstimationBP.AEstimationBP_TaskList_TT[x].AEstimationBP_PartList[y].length <=0)
                    {
                        $scope.mEstimationBP.DP = 0;
                    }
                }
            }

            var filtered  = [];
            if($scope.optionsSpkList != undefined){
                filtered = $scope.optionsSpkList.filter(function(item) { 
                   return item.EstSPKListId == $scope.mEstimationBP.NoSpk;  
                });
                console.log('filtered ===>', filtered);
                console.log("filtered[0] SPK",filtered[0]);
            }
            

            var satuan = null;
            for (var i = 0; i < $scope.unitData.length; i++) {
                if ($scope.unitData[i].MasterId == $scope.mEstimationBP.ProcessId) {
                    satuan = $scope.unitData[i].Name;
                }
            }

            var KetSPK = null;
            var SPKNO = null;
            if(filtered.length > 0){
                SPKNO = filtered[0].SpkNo;
                if(filtered[0].EstSPKListIdParent == null){
                    KetSPK = "Utama";
                }else{
                    KetSPK = "Tambahan";
                }
            }


            if($scope.masterParts.length > 0){
                console.log('DP PArts',$scope.mEstimationBP.DP);
                // DPnya diminta selalu 0 oleh via 2-26-2020
                if($scope.mEstimationBP.DP == null || $scope.mEstimationBP.DP == ''){
                    //globalDP = 0;
                }else{
                    //globalDP = $scope.mEstimationBP.DP;
                }
                console.log('set global DP untuk yg ada parts===>',$scope.mEstimationBP.DP);
                //$scope.mEstimationBP.DP = globalDP;


                // buat set data satuan part jika di ganti ganti saat create estimasi ============================================================ start
                for (var i=0; i<$scope.masterParts.length; i++){
                    for (var j = 0; j < $scope.unitData.length; j++) {
                        if ($scope.masterParts[i].Name !== null && $scope.masterParts[i].Name !== undefined && $scope.masterParts[i].Name !== ''){
                            if ($scope.unitData[j].Name == $scope.masterParts[i].Name) {
                                $scope.masterParts[i].SatuanId = $scope.unitData[j].MasterId;
                            }
                        }
                    }
                }
                // buat set data satuan part jika di ganti ganti saat create estimasi ============================================================ end

            }else{
                //$scope.mEstimationBP.DP = globalDP;
                console.log('tidak set global DP untuk task kosong ===>',$scope.mEstimationBP.DP);
            }


            for (var i = 0; i < $scope.master.length; i++) {
                if ($scope.master[i].AEstimationBP_PartList !== undefined) {
                    for (var j = 0; j < $scope.master[i].AEstimationBP_PartList.length; j++) {
                    }
                }
            }

            var tempTotal = 0;
            var tmpDp = 0;
            var isAmanBanget = true;
            var wrongData = false;
            console.log("flagUpdate",flagUpdate);
            if(flagUpdate == 0){
                
                console.log('taskBPIdFromMaster ===>',taskBPIdFromMaster);
                if(taskBPIdFromMaster == null){
                    console.log("customTaskName ===>",customTaskName)
                }
        
                console.log('its Add new Task ===>',flagUpdate);
                for(var i = 0;i<$scope.masterParts.length; i++){
                    if($scope.masterParts[i].TipeMaterial == "Spare Parts"){
                        TipeMaterial = 1; 
                    }else{
                        TipeMaterial = 2;
                    }


                    if($scope.masterParts[i].IsOPB == true){
                        $scope.masterParts[i].IsOPB = 1;
                    }else if($scope.masterParts[i].IsOPB == false){
                        $scope.masterParts[i].IsOPB = 0;
                    }
                

                    if($scope.masterParts[i].Qty == "" || $scope.masterParts[i].Qty == undefined || $scope.masterParts[i].Qty == null){
                        if(param == false){
                            if(TipeMaterial = 1){
                                wrongData = true;
                                isAmanBanget = false;
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Qty pada data Sparepart harus di isi",
                                });
                            }else{
                                wrongData = true;
                                isAmanBanget = false;
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Qty pada data Bahan harus di isi",
                                });
                            }
                        }



                    } 

                    console.log('$scope.masterParts[i].TipeMaterial ===>',$scope.masterParts[i].TipeMaterial);
                    if($scope.masterParts[i].PaidBy == "" || $scope.masterParts[i].PaidBy == undefined || $scope.masterParts[i].PaidBy == null){
                        if(param == false){
                            if(TipeMaterial = 1){
                                wrongData = true;
                                isAmanBanget = false;
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Mohon Isi Tipe Pembayaran pada Sparepart yang anda masukan",
                                });
                            }else{
                                wrongData = true;
                                isAmanBanget = false;
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Mohon Isi Tipe Pembayaran pada Bahan yang anda masukan",
                                });
                            }
                        }
                    } 


                    if($scope.masterParts[i].PartsId == ""){ //manual parts
                        
                    }else{
                        if($scope.masterParts[i].RetailPrice == "" || $scope.masterParts[i].RetailPrice == undefined || $scope.masterParts[i].RetailPrice == null || $scope.masterParts[i].RetailPrice == 0){
                            if(param == false){
                                if(TipeMaterial = 1){
                                    wrongData = true;
                                    isAmanBanget = false;
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Mohon Isi Harga Sparepart yang anda masukan",
                                    });
                                }else{
                                    wrongData = true;
                                    isAmanBanget = false;
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Mohon Isi Harga Bahan yang anda masukan",
                                    });
                                }
                            }
                        }
                    }


                    // if(($scope.masterParts[i].Qty.toString()).length > 3){
                    //     wrongData = true;
                    //     isAmanBanget = false;
                    //     bsNotify.show({
                    //         size: 'big',
                    //         type: 'danger',
                    //         title: "Jumlah Qty Melebihi Batas Maksimal ( 3 Digit )",
                    //     }); 
                    // }
                    
                    



                    console.log('$scope.masterParts[i].TipeMaterial ====>',$scope.masterParts[i].TipeMaterial);
                    if((
                        $scope.masterParts[i].PartsName == "" || 
                        $scope.masterParts[i].PartsName == undefined || 
                        $scope.masterParts[i].PartsName == null) &&
                        
                        ($scope.masterParts[i].PartsCode == "" || 
                        $scope.masterParts[i].PartsCode == undefined || 
                        $scope.masterParts[i].PartsCode == null)){

                        wrongData = true;
                        isAmanBanget = false;
                        // bsNotify.show({
                        //     size: 'big',
                        //     type: 'danger',
                        //     title: "Nama atau Kode Sparepart pada data Sparepart atau Bahan harus di isi",
                        // });

                        if(param == false){
                            if(TipeMaterial = 1){
                                wrongData = true;
                                isAmanBanget = false;
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Nama atau Kode Sparepart pada data Sparepart harus di isi",
                                });
                            }else{
                                wrongData = true;
                                isAmanBanget = false;
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Nama atau Kode Bahan pada data Bahan harus di isi",
                                });
                            }
                        }


                    }


                    console.log('wrongData ====> add',wrongData);
                    console.log('isAmanBanget ====>add',isAmanBanget);
                    
                    if(wrongData == false){
                        PartsSave.push({
                            "PartsId":$scope.masterParts[i].PartsId,
                            "PartsName":$scope.masterParts[i].PartsName,
                            "Price":$scope.masterParts[i].RetailPrice,
                            "qty":$scope.masterParts[i].Qty,
                            "ETA":$scope.masterParts[i].ETA,
                            // "DownPayment":$scope.masterParts[i].PriceDP,
                            "PaidById":$scope.mEstimationBP.PaidById,
                          //   "PaidById":$scope.masterParts[i].PaidById,
                            "EstPartsId":0,
                            "SatuanId": $scope.masterParts[i].SatuanId,
                            "OrderType":$scope.masterParts[i].OrderType,
                            "QtyOrder":$scope.masterParts[i].Qty,
                            "Qty":$scope.masterParts[i].Qty,
                            "MaterialTypeId":TipeMaterial,
                            "isDeleted":0,
                            "LandedCost":$scope.masterParts[i].RetailPrice,
                            "RetailPrice":$scope.masterParts[i].RetailPrice,
                            "minimalDp":0,
                            // "DPRequest": $scope.masterParts[i].Availbility == "Tersedia" ? 0 : $scope.mEstimationBP.DP ,
                            "DPRequest": $scope.masterParts[i].Availbility == "Tersedia" ? 0 : $scope.masterParts[i].DPRequest ,

                            "PartsCode": $scope.masterParts[i].PartsCode,
                            "TipeMaterial": $scope.masterParts[i].TipeMaterial,
                            "Availbility": $scope.masterParts[i].Availbility,
                            "PaidBy":$scope.masterParts[i].PaidBy,
                            "Name" : $scope.masterParts[i].Name,
                            "SubTotal" : $scope.masterParts[i].SubTotal,
                            "No" : i + 1,
                            // "DownPayment" : $scope.masterParts[i].Availbility == "Tersedia" ? 0 : $scope.mEstimationBP.DP ,
                            "DownPayment" : $scope.masterParts[i].Availbility == "Tersedia" ? 0 : $scope.masterParts[i].DownPayment ,

                            "Discount" : $scope.masterParts[i].Discount,
                            "IsOPB" : $scope.masterParts[i].IsOPB,
  
  
                            "DiscountType" : $scope.mEstimationBP.TipeDisc,
                            "DiscountTypeId" : $scope.mEstimationBP.TipeDisc,
                            "IsSaveFin" : 1
                      });
                    }
                    
                }


               

                if(isAmanBanget == true){
                    console.log("PartsSave",PartsSave);

                    if ($scope.mEstimationBP.FlatRate == null || $scope.mEstimationBP.FlatRate == undefined) {
                        $scope.mEstimationBP.FlatRate = 1
                    } else {
                        $scope.mEstimationBP.FlatRate = $scope.mEstimationBP.FlatRate
                    }

                    dataTaskSave.push({
                        "EstSPKListId":$scope.mEstimationBP.NoSpk,
                        "AEstimationBP_PartList":PartsSave,
                        "TaskId":null,
                        "TaskBPId":taskBPIdFromMaster,
                        "TaskName": taskBPIdFromMaster == null? customTaskName : $scope.mEstimationBP.TaskName,
                        "Fare":$scope.mEstimationBP.Fare,
                        "PaidById":$scope.mEstimationBP.PaidById,
                        "ProcessId":$scope.mEstimationBP.ProcessId,
                        "totalParts" : $scope.masterParts.length,
                        "FlatRate" : $scope.mEstimationBP.FlatRate == null ? 1 : $scope.mEstimationBP.FlatRate,
                        "TFirst1" : $scope.mEstimationBP.TFirst1,
                        "SubTotalTask" : $scope.mEstimationBP.FlatRate * $scope.mEstimationBP.Fare,
                        
                        "BodyEstimationMinute" : $scope.mEstimationBP.BodyEstimationMinute,
                        "FIEstimationMinute" : $scope.mEstimationBP.FIEstimationMinute,
                        "PolishingEstimationMinute" : $scope.mEstimationBP.PolishingEstimationMinute,
                        "PutyEstimationMinute" : $scope.mEstimationBP.PutyEstimationMinute,
                        "ReassemblyEstimationMinute" : $scope.mEstimationBP.ReassemblyEstimationMinute,
                        "SprayingEstimationMinute" : $scope.mEstimationBP.SprayingEstimationMinute,
                        "SurfacerEstimationMinute" : $scope.mEstimationBP.SurfacerEstimationMinute,

                        "isDeleted":0,
                        "SPKNO" : SPKNO,
                        "KetSPK" : KetSPK,
                        "satuan" : satuan,
                        "FlagID" : Id,
                        "DiscountType" : $scope.mEstimationBP.TipeDisc,
                        "DiscountTypeId" : $scope.mEstimationBP.TipeDisc,
                        "Discount" : $scope.mEstimationBP.DiskonPersen,
                        "finalDP" : tmpDp
                    });
                    console.log("dataTaskSave",dataTaskSave);
                    $scope.dataGoodWork.push({
                        "EstSPKListId":$scope.mEstimationBP.NoSpk,
                        "AEstimationBP_PartList":PartsSave,
                        "TaskId":null,
                        "TaskBPId":taskBPIdFromMaster,
                        "TaskName": taskBPIdFromMaster == null? customTaskName : $scope.mEstimationBP.TaskName,
                        "Fare":$scope.mEstimationBP.Fare,
                        "PaidById":$scope.mEstimationBP.PaidById,
                        "ProcessId":$scope.mEstimationBP.ProcessId,
                        "totalParts" : $scope.masterParts.length,
                        "FlatRate" : $scope.mEstimationBP.FlatRate == null ? 1 : $scope.mEstimationBP.FlatRate,
                        "TFirst1" : $scope.mEstimationBP.TFirst1,
                        "SubTotalTask" : $scope.mEstimationBP.FlatRate * $scope.mEstimationBP.Fare,
                        
                        "BodyEstimationMinute" : $scope.mEstimationBP.BodyEstimationMinute,
                        "FIEstimationMinute" : $scope.mEstimationBP.FIEstimationMinute,
                        "PolishingEstimationMinute" : $scope.mEstimationBP.PolishingEstimationMinute,
                        "PutyEstimationMinute" : $scope.mEstimationBP.PutyEstimationMinute,
                        "ReassemblyEstimationMinute" : $scope.mEstimationBP.ReassemblyEstimationMinute,
                        "SprayingEstimationMinute" : $scope.mEstimationBP.SprayingEstimationMinute,
                        "SurfacerEstimationMinute" : $scope.mEstimationBP.SurfacerEstimationMinute,

                        "isDeleted":0,
                        "SPKNO" : SPKNO,
                        "KetSPK" : KetSPK,
                        "satuan" : satuan,
                        "FlagID" : Id,
                        "DiscountType" : $scope.mEstimationBP.TipeDisc,
                        "DiscountTypeId" : $scope.mEstimationBP.TipeDisc,
                        "Discount" : $scope.mEstimationBP.DiskonPersen,
                        "finalDP" : tmpDp
                    });
                }

                // if($scope.masterParts.length > 0){
                //     //update semua DP mengikuti data terbaru
                //     for(var i = 0;i < dataTaskSave.length; i++){
        
                //         for(var z in dataTaskSave[i].AEstimationBP_PartList){
                //             // if(dataTaskSave[i].AEstimationBP_PartList[z].DownPayment == 0){
                //             // }
                //             //dataTaskSave[i].AEstimationBP_PartList[z].DownPayment = $scope.mEstimationBP.DP;
                //         }
                //     }
                //     for(var i = 0;i < $scope.dataGoodWork.length; i++){
                //         for(var z in $scope.dataGoodWork[i].AEstimationBP_PartList){
                //             // if(dataTaskSave[i].AEstimationBP_PartList[z].DownPayment == 0){
                //             // }
                //             //dataTaskSave[i].AEstimationBP_PartList[z].DownPayment = $scope.mEstimationBP.DP;
                //         }
                //     }
                //     console.log('Addnew --- ganti DP karna ada partsnya');
                //     //update semua DP mengikuti data terbaru
                // }else{
                //     console.log('Addnew --- ga usah ganti DP karna ga ada partsnya');
                // }


            }else{
                console.log('its Save Edit Task ===>',flagUpdate);
                console.log("dataEidt",dataEidt);
                console.log("$scope.masterParts",$scope.masterParts);
                for(var i = 0;i<$scope.masterParts.length; i++){
                    
                    if($scope.masterParts[i].TipeMaterial == "Spare Parts"){
                        TipeMaterial = 1; 
                    }else{
                        TipeMaterial = 2;
                    }

                    if($scope.masterParts[i].IsOPB == true){
                        $scope.masterParts[i].IsOPB = 1;
                    }else if($scope.masterParts[i].IsOPB == false){
                        $scope.masterParts[i].IsOPB = 0;
                    }
                    
                    
                    if($scope.masterParts[i].Qty == "" || $scope.masterParts[i].Qty == undefined || $scope.masterParts[i].Qty == null){

                        if(param == false){
                            if(TipeMaterial = 1){
                                wrongData = true;
                                isAmanBanget = false;
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Qty pada data Sparepart harus di isi",
                                });
                            }else{
                                wrongData = true;
                                isAmanBanget = false;
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Qty pada data Bahan harus di isi",
                                });
                            }
                        }

                        



                    } 

                    console.log('$scope.masterParts[i].TipeMaterial ===>',$scope.masterParts[i].TipeMaterial);
                    if($scope.masterParts[i].PaidBy == "" || $scope.masterParts[i].PaidBy == undefined || $scope.masterParts[i].PaidBy == null){

                        if(param == false){
                            if(TipeMaterial = 1){
                                wrongData = true;
                                isAmanBanget = false;
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Mohon Isi Tipe Pembayaran pada Sparepart yang anda masukan",
                                });
                            }else{
                                wrongData = true;
                                isAmanBanget = false;
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Mohon Isi Tipe Pembayaran pada Bahan yang anda masukan",
                                });
                            }
                        }

                        
                    } 
                    
                    if($scope.masterParts[i].PartsId == ""){ //manual parts
                        
                    }else{
                        if($scope.masterParts[i].RetailPrice == "" || $scope.masterParts[i].RetailPrice == undefined || $scope.masterParts[i].RetailPrice == null || $scope.masterParts[i].RetailPrice == 0){
                            
                            
                            if(param == false){
                                if(TipeMaterial = 1){
                                    wrongData = true;
                                    isAmanBanget = false;
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Mohon Isi Harga Sparepart yang anda masukan",
                                    });
                                }else{
                                    wrongData = true;
                                    isAmanBanget = false;
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Mohon Isi Harga Bahan yang anda masukan",
                                    });
                                }
                            }

                            
                        }
                    }

                    // if(($scope.masterParts[i].Qty.toString()).length > 3){
                    //     wrongData = true;
                    //     isAmanBanget = false;
                    //     bsNotify.show({
                    //         size: 'big',
                    //         type: 'danger',
                    //         title: "Jumlah Qty Melebihi Batas Maksimal ( 3 Digit )",
                    //     }); 
                    // }



                    console.log('$scope.masterParts[i].TipeMaterial ====>',$scope.masterParts[i].TipeMaterial);
                    if((
                        $scope.masterParts[i].PartsName == "" || 
                        $scope.masterParts[i].PartsName == undefined || 
                        $scope.masterParts[i].PartsName == null) &&
                        
                        ($scope.masterParts[i].PartsCode == "" || 
                        $scope.masterParts[i].PartsCode == undefined || 
                        $scope.masterParts[i].PartsCode == null)){

                        wrongData = true;
                        isAmanBanget = false;
                        // bsNotify.show({
                        //     size: 'big',
                        //     type: 'danger',
                        //     title: "Nama atau Kode Sparepart pada data Sparepart atau Bahan harus di isi",
                        // });

                        if(param == false){
                            if(TipeMaterial = 1){
                                wrongData = true;
                                isAmanBanget = false;
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Nama atau Kode Sparepart pada data Sparepart harus di isi",
                                });
                            }else{
                                wrongData = true;
                                isAmanBanget = false;
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Nama atau Kode Bahan pada data Bahan harus di isi",
                                });
                            }
                        }

                        


                    }



                    console.log('wrongData ====>edit',wrongData);
                    console.log('isAmanBanget ====>edit',isAmanBanget);


                    if(wrongData == false){
                        PartsSave.push({
                            "PartsId":$scope.masterParts[i].PartsId,
                            "EstPartsId":$scope.masterParts[i].EstPartsId == undefined? 0: $scope.masterParts[i].EstPartsId,
                            "PartsName":$scope.masterParts[i].PartsName,
                            "Price":$scope.masterParts[i].RetailPrice,
                            "qty":$scope.masterParts[i].Qty,
                            "ETA":$scope.masterParts[i].ETA,
                            // "DownPayment": $scope.masterParts[i].Availbility == "Tersedia" ? 0 : $scope.mEstimationBP.DP ,
                            "DownPayment": $scope.masterParts[i].Availbility == "Tersedia" ? 0 : $scope.masterParts[i].DownPayment ,

                            "PaidById":$scope.mEstimationBP.PaidById,
                            "SatuanId": $scope.masterParts[i].SatuanId,
                            "OrderType":$scope.masterParts[i].OrderType,
                            "QtyOrder":$scope.masterParts[i].Qty,
                            "Qty":$scope.masterParts[i].Qty,
                            "MaterialTypeId":TipeMaterial,
                            "isDeleted":0,
                            "LandedCost":$scope.masterParts[i].RetailPrice,
                            "RetailPrice":$scope.masterParts[i].RetailPrice,
                            "minimalDp":0,
                            // "DPRequest": $scope.masterParts[i].Availbility == "Tersedia" ? 0 : $scope.mEstimationBP.DP ,
                            "DPRequest": $scope.masterParts[i].Availbility == "Tersedia" ? 0 : $scope.masterParts[i].DPRequest ,

                            "PartsCode": $scope.masterParts[i].PartsCode,
                            "TipeMaterial": $scope.masterParts[i].TipeMaterial,
                            "Availbility": $scope.masterParts[i].Availbility,
                            "PaidBy":$scope.masterParts[i].PaidBy,
                            "Name" : $scope.masterParts[i].Name,
                            "SubTotal" : $scope.masterParts[i].SubTotal,
                            "No" : i + 1,
                            "Discount" : $scope.masterParts[i].Discount,
                            "IsOPB" : $scope.masterParts[i].IsOPB,
                            "DiscountType" : $scope.mEstimationBP.TipeDisc,
                            "DiscountTypeId" : $scope.mEstimationBP.TipeDisc,
                            "IdFlagParts" : $scope.masterParts[i].IdFlagParts,
                            "IsSaveFin" : 1
                        });
                    }
                    
                }

                console.log('$scope.master ===>',$scope.master);
                console.log('dataTaskSave ===>', dataTaskSave)
                console.log('$scope.master ===>', $scope.master)    


              


                for(var i = 0;i < dataTaskSave.length; i++){

                    for(var z in dataTaskSave[i].AEstimationBP_PartList){
                        //dataTaskSave[i].AEstimationBP_PartList[z].DownPayment = $scope.mEstimationBP.DP;
                        console.log('chibichobo',dataTaskSave[i].AEstimationBP_PartList[z].DownPayment)
                    }

                    if ($scope.mEstimationBP.FlatRate == null || $scope.mEstimationBP.FlatRate == undefined) {
                        $scope.mEstimationBP.FlatRate = 1
                    } else {
                        $scope.mEstimationBP.FlatRate = $scope.mEstimationBP.FlatRate
                    }


                    if(dataTaskSave[i].FlagID == dataEidt.FlagID){
                        // dataTaskSave.push({
                        dataTaskSave[i].EstSPKListId = $scope.mEstimationBP.NoSpk;
                        dataTaskSave[i].AEstimationBP_PartList = PartsSave;
                        // TaskId = 1;
                        dataTaskSave[i].totalParts = $scope.masterParts.length;
                        dataTaskSave[i].Discount = $scope.mEstimationBP.DiskonPersen;
                        dataTaskSave[i].TaskName = $scope.mEstimationBP.TaskName;
                        dataTaskSave[i].Fare = $scope.mEstimationBP.Fare;
                        dataTaskSave[i].PaidById = $scope.mEstimationBP.PaidById;
                        dataTaskSave[i].ProcessId = $scope.mEstimationBP.ProcessId;
                        dataTaskSave[i].DiscountTypeId = $scope.mEstimationBP.TipeDisc;
                        dataTaskSave[i].DiscountType = $scope.mEstimationBP.TipeDisc;
                        dataTaskSave[i].SubTotalTask = $scope.mEstimationBP.Fare * $scope.mEstimationBP.FlatRate;

                        for(var a = 0; a < $scope.master.length; a++){
                            if($scope.dataGoodWork[i].FlagID == $scope.master[a].FlagID){

                                
                                dataTaskSave[i].BodyEstimationMinute       = $scope.master[a].BodyEstimationMinute;
                                dataTaskSave[i].FIEstimationMinute         = $scope.master[a].FIEstimationMinute;
                                dataTaskSave[i].PolishingEstimationMinute  = $scope.master[a].PolishingEstimationMinute;
                                dataTaskSave[i].PutyEstimationMinute       = $scope.master[a].PutyEstimationMinute;
                                dataTaskSave[i].ReassemblyEstimationMinute = $scope.master[a].ReassemblyEstimationMinute;
                                dataTaskSave[i].SprayingEstimationMinute   = $scope.master[a].SprayingEstimationMinute;
                                dataTaskSave[i].SurfacerEstimationMinute   = $scope.master[a].SurfacerEstimationMinute;
                            }
                        }

                        // isDeleted = 0;
                        dataTaskSave[i].TaskBPId = taskBPIdFromMaster == null? taskBPIdFromEdit : taskBPIdFromMaster;
                        dataTaskSave[i].SPKNO  =  SPKNO;
                        dataTaskSave[i].KetSPK  =  KetSPK;
                        dataTaskSave[i].satuan  =  satuan;
                        dataTaskSave[i].finalDP  =  tmpDp;
                           // "FlagID" : Math.floor(Math.random() * 10000) + 1;
                        // });
                    }

                }

                for(var x in tmpDeleteParts){
                    if(tmpDeleteParts[x].isCancel == 1){
                        tmpDeleteParts[x].isCancel = 0;
                    }
                }

                // TmpPartsAgain = angular.copy(PartsSave);
                $scope.dataGoodWork = angular.copy(dataTaskSave);
                console.log("PartsSave",PartsSave);
                console.log("$scope.dataGoodWork[i].AEstimationBP_PartList",$scope.dataGoodWork);

                console.log("mEstimationBP",$scope.mEstimationBP);
                // var dataFinCuy = [];
                for(var i = 0;i < $scope.dataGoodWork.length; i++){

                    // for(var z in $scope.dataGoodWork[i].AEstimationBP_PartList){
                    //     $scope.dataGoodWork[i].AEstimationBP_PartList[z].DownPayment = $scope.mEstimationBP.DP;
                    // }


                    if($scope.dataGoodWork[i].FlagID == dataEidt.FlagID){
                        $scope.dataGoodWork[i].EstSPKListId = $scope.mEstimationBP.NoSpk;
                        console.log("tmpDeleteParts",tmpDeleteParts);
                        console.log("$scope.dataGoodWork[i].AEstimationBP_PartList",$scope.dataGoodWork[i].AEstimationBP_PartList);
                        // console.log("dataFinCuy",dataFinCuy);

                        // for(var a = 0; a < dataFinCuy.length; a++){
                        //     console.log("push dataFinCuy",dataFinCuy[a]);
                        //     dataFinCuy[a].isSave = 1;   
                        //     $scope.dataGoodWork[i].AEstimationBP_PartList.push(dataFinCuy[a]);
                        // }
                             
                        $scope.dataGoodWork[i].totalParts = $scope.masterParts.length;
                        $scope.dataGoodWork[i].TaskName = $scope.mEstimationBP.TaskName;
                        $scope.dataGoodWork[i].Fare = $scope.mEstimationBP.Fare;
                        $scope.dataGoodWork[i].PaidById = $scope.mEstimationBP.PaidById;
                        $scope.dataGoodWork[i].ProcessId = $scope.mEstimationBP.ProcessId;
                        $scope.dataGoodWork[i].DiscountTypeId = $scope.mEstimationBP.TipeDisc;
                        $scope.dataGoodWork[i].DiscountType = $scope.mEstimationBP.TipeDisc;
                        $scope.dataGoodWork[i].Discount = $scope.mEstimationBP.DiskonPersen;
                        for(var a = 0; a < $scope.master.length; a++){
                            if($scope.dataGoodWork[i].FlagID == $scope.master[a].FlagID){
                                console.log("$scope.master[a]",$scope.master[a]);
                                
                                
                                $scope.dataGoodWork[i].BodyEstimationMinute  =  $scope.master[a].BodyEstimationMinute;
                                $scope.dataGoodWork[i].FIEstimationMinute  =  $scope.master[a].FIEstimationMinute;
                                $scope.dataGoodWork[i].PolishingEstimationMinute  =  $scope.master[a].PolishingEstimationMinute;
                                $scope.dataGoodWork[i].PutyEstimationMinute  =  $scope.master[a].PutyEstimationMinute;
                                $scope.dataGoodWork[i].ReassemblyEstimationMinute  =  $scope.master[a].ReassemblyEstimationMinute;
                                $scope.dataGoodWork[i].SprayingEstimationMinute  =  $scope.master[a].SprayingEstimationMinute;
                                $scope.dataGoodWork[i].SurfacerEstimationMinute  =  $scope.master[a].SurfacerEstimationMinute;

                                $scope.dataGoodWork[i].TaskBPId = taskBPIdFromMaster == null? taskBPIdFromEdit : taskBPIdFromMaster;
                                $scope.dataGoodWork[i].SPKNO  =  SPKNO;
                                $scope.dataGoodWork[i].KetSPK  =  KetSPK;
                                $scope.dataGoodWork[i].satuan  =  satuan;
                                $scope.dataGoodWork[i].finalDP  =  tmpDp;
                            }
                        }
                       
                    }

                }
                console.log("$scope.dataGoodWork in final",$scope.dataGoodWork);

                console.log('master parts ===>',$scope.masterParts)
                // if($scope.masterParts.length > 0){
                //     //update semua DP mengikuti data terbaru
                //     for(var i = 0;i < dataTaskSave.length; i++){
        
                //         for(var z in dataTaskSave[i].AEstimationBP_PartList){
                //             dataTaskSave[i].AEstimationBP_PartList[z].DownPayment = $scope.mEstimationBP.DP;
                //         }
                //     }
                //     for(var i = 0;i < $scope.dataGoodWork.length; i++){
                //         for(var z in $scope.dataGoodWork[i].AEstimationBP_PartList){
                //             $scope.dataGoodWork[i].AEstimationBP_PartList[z].DownPayment = $scope.mEstimationBP.DP;
                //         }
                //     }
                //     console.log('ganti DP karna ada partsnya',globalDP);
                //     //update semua DP mengikuti data terbaru
                // }else{
                //     console.log('ga usah ganti DP karna ga ada partsnya',globalDP);
                // }
    

            }


            console.log('savefin tmpDp ===>',tmpDp);


            

            if(isAmanBanget == true){
                console.log('dataTask ==>',dataTask);

                dataTask.TaskName = $scope.mEstimationBP.TaskName;

                if(flagUpdate == 0){
                    $scope.getDataGirdWorkView(dataTask,Id);
                    $scope.countDP();
                }else{
                    $scope.getDataGirdWorkView(dataTask,dataEidt.FlagID);
                    $scope.countDP();
                }

                taskBPIdFromMaster = null;
                customTaskName     = null;
                taskNameFromMaster = null;

                angular.element(".ui.modal.ModalOP").modal("hide");
                $scope.masterParts = [];
                $scope.mEstimationBP.TaskName = '';
                $scope.mEstimationBP.ProcessId = '';
                $scope.mEstimationBP.NoSpk = '';
                $scope.mEstimationBP.Fare = '';
                $scope.mEstimationBP.PaidById = '';
                $scope.temp_paidJob = angular.copy($scope.mEstimationBP.PaidById)
            }else{
                // for(var x in $scope.dataTaskSave){
                //     if($scope.dataTaskSave[x].FlagID == Id){
                //         $scope.dataTaskSave[x].splice(x,1);
                //     }
                // }

                // for(var x in $scope.dataGoodWork){
                //     if($scope.dataGoodWork[x].FlagID == Id){
                //         $scope.dataGoodWork[x].splice(x,1);
                //     }
                // }
            }

            
            
            // // if(dataTaskSave[0].DiscountType == 0 || dataTaskSave[0].DiscountType == '0'){ lifa
            //     console.log("dataTaskSave alah sia",dataTaskSave);
            //     // for(var i in dataTaskSave){
            //         var dups = [];
            //         var arr = dataTaskSave.filter(function(el) {
            //             if(el.Disount !== null || el.Disount !== undefined){
            //               dups.push(el.Discount);    
            //             }
            //         });
            //         console.log("dis task beda",arr,dups);
            //         var filtered = dataTaskSave.filter(function(item) { 
            //            return item.Discount !== dups[0];
            //         });
            //         console.log("dis task beda real",filtered);
            //         if(filtered.length == 0){

            //             $scope.disTask = dups[0];
            //         }else{
            //             $scope.disTask = '';
            //         }
            //     // }
            //     // $scope.removeDuplicateParts = function(data){
            //     //             var dups = [];
            //     //             var arr = data.filter(function(el) {
            //     //                 if (dups.indexOf(el.PartsId) == -1) {
            //     //                   dups.push(el.PartsId);              
            //     //                   return true;
            //     //                 }else{
            //     //                   return false;
                              
            //     //                 }
            //     //               });
            //     //             return arr
            //     //         }
            //     var dupsParts = [];
            //     for(var i in dataTaskSave){
            //         var arrParts = dataTaskSave[i].AEstimationBP_PartList.filter(function(el) {
            //             if(el.Disount !== null || el.Disount !== undefined){
            //               dupsParts.push(el.Discount);    
            //             }
            //         });
            //     }
            //     console.log("dupsParts",dupsParts);
            //     var flagDisc = 0;
            //     for(var i in dataTaskSave){
            //         console.log("dataTaskSave[i].AEstimationBP_PartList.length",dataTaskSave[i].AEstimationBP_PartList.length);
            //         if(dataTaskSave[i].AEstimationBP_PartList.length == 0){
            //             flagDisc++
            //         }else{

            //             dataTaskSave[i].AEstimationBP_PartList.filter(function(item) { 
            //                if(item.Discount !== dupsParts[0]){
            //                    flagDisc++
            //                } 
            //             });
            //         }
            //     }
            //     console.log("flagDisc",flagDisc);
            //     if(flagDisc == 0){

            //         $scope.disParts = dupsParts[0];
            //     }else{
            //         $scope.disParts = '';
            //     }
            // // }lifa

            // // this for set DP all parts
            // for(var i in $scope.master){
            //     for(var j in $scope.master[i].AEstimationBP_PartList){
                    
            //         // $scope.master[i].AEstimationBP_PartList[j].DownPayment = globalDP; 
            //         // tempTotal += ($scope.masterParts[i].SubTotal - ($scope.masterParts[i].SubTotal * 10/100));
            //         // console.log("tempTotal",tempTotal);
            //         // tmpDp = (tempTotal * globalDP/100);
            //         $scope.master[i].finalDP = tempTotal;
            //     }
            // }
            // // this for set DP all parts
            // console.log("$scope.disTask",$scope.disTask);

            // console.log("dataTask",dataTask);lifa

            


            

            

            
            // console.log("masterParts",$scope.masterParts);


        }

        var selectedTaskBP = null;
        var jejeranDiskonTask = [];
        var jejeranDiskonParts = [];
        $scope.displayDiscountAlgorithm = function(){
            console.log('jejeranDiskonTask ==>',jejeranDiskonTask);
            console.log('jejeranDiskonParts ==>',jejeranDiskonParts);
            if(jejeranDiskonTask.length > 1){
                var countTaskDiscount = 0;
                getSingleDiskonTask = [];
                for(var i in jejeranDiskonTask){
                    if(jejeranDiskonTask[i] == 0){
                        countTaskDiscount++;
                    }else{
                        getSingleDiskonTask.push(jejeranDiskonTask[i]);
                    }
                }
                if(countTaskDiscount == jejeranDiskonTask.length - 1 && getSingleDiskonTask.length == 1){
                    // $scope.DisplayDisTask = getSingleDiskonTask[0]; // [0,0,5] get 5 for Discount
                    $scope.DisplayDisTask = ' '; // [0,0,5] kasih blank
                    console.log('jejeranDiskonTask masuk Kondisi 1 | [0,0,5] | getSingleDiskonTask ===>', getSingleDiskonTask);
                }else{
                    if(countTaskDiscount == jejeranDiskonTask.length){
                        $scope.DisplayDisTask = jejeranDiskonTask[0]; //[0,0,0] get 0 for Discount
                        $scope.DisplayDisTask = 0; //[0,0,0] kasih nol
                        console.log('jejeranDiskonTask masuk Kondisi 2 | [0,0,0] | jejeranDiskonTask[0] ===>', getSingleDiskonTask);
                    }else{
                        var countSameDiscount = 0;
                        for(var x in getSingleDiskonTask){
                            if(getSingleDiskonTask[0] == getSingleDiskonTask[x]){
                                countSameDiscount++;
                            }
                        }
                        if(countSameDiscount == getSingleDiskonTask.length){
                            $scope.DisplayDisTask = getSingleDiskonTask[0]; //[0,3,3] get 3 for Discount
                            // $scope.DisplayDisTask = ' '; //[0,3,3] kasih blank
                            console.log('jejeranDiskonTask masuk Kondisi 3 | [0,3,3] | kasih blank ===>', getSingleDiskonTask);
                        }else{
                            $scope.DisplayDisTask = ' '; // [4,3,5] set blank For Discount
                            console.log('jejeranDiskonTask masuk Kondisi 4 | [4,3,5] | blank jejeranDiskonTask  ===>', jejeranDiskonTask);
                        }
                    }
                }
            }else{
                $scope.DisplayDisTask = jejeranDiskonTask[0]; //[x] get x for Discount
                console.log('jejeranDiskonTask masuk Kondisi 5 | [x] | single diskon jejeranDiskonTask  ===>', jejeranDiskonTask);
            }


            if(jejeranDiskonParts.length > 1){
                var countPartsDiscount = 0;
                getSingleDiskonParts = [];
                for(var i in jejeranDiskonParts){


                    if(jejeranDiskonParts[i] == 0){
                        countPartsDiscount++;
                    }else{
                        getSingleDiskonParts.push(jejeranDiskonParts[i]);
                    }
                }
                if(countPartsDiscount == jejeranDiskonParts.length - 1 && getSingleDiskonParts.length == 1){
                    $scope.DisplayDisParts = getSingleDiskonParts[0]; // [0,0,5] get 5 for Discount
                    $scope.DisplayDisParts =' '; // [0,0,5] kasih blank
                    console.log('jejeranDiskonParts masuk Kondisi 1 | [0,0,5] | jejeranDiskonParts ===>', jejeranDiskonParts);
                }else{
                    if(countPartsDiscount == jejeranDiskonParts.length){
                        $scope.DisplayDisParts = jejeranDiskonParts[0]; //[0,0,0] get 0 for Discount
                        $scope.DisplayDisParts = 0; //[0,0,0] kasih nol
                        console.log('jejeranDiskonParts masuk Kondisi 2 | [0,0,0] kasih nol | jejeranDiskonParts[0] ===>', jejeranDiskonParts);
                    }else{
                        var countSameDiscount = 0;
                        for(var x in getSingleDiskonParts){
                            if(getSingleDiskonParts[0] == getSingleDiskonParts[x]){
                                countSameDiscount++;
                            }
                        }
                        if(countSameDiscount == getSingleDiskonParts.length){
                            $scope.DisplayDisParts = getSingleDiskonParts[0]; //[0,3,3] get 3 for discount
                            // $scope.DisplayDisParts =' '; //[0,3,3] kasih blank
                            console.log('jejeranDiskonParts masuk Kondisi 3 | [0,3,3] | jejeranDiskonParts[0] ===>', jejeranDiskonParts);
                        }else{
                            $scope.DisplayDisParts = ' '; // [4,3,5] set blank For Discount
                            console.log('jejeranDiskonParts masuk Kondisi 4 | [4,3,5] | blank jejeranDiskonParts  ===>', jejeranDiskonParts);
                        }
                    }
                }
            }else{
                $scope.DisplayDisParts = jejeranDiskonParts[0];// get x for Discount
                console.log('jejeranDiskonParts masuk Kondisi 5 | [x] | single diskon jejeranDiskonParts  ===>', jejeranDiskonParts);
            }

        }

        $scope.getDataGirdWorkView = function(item,id){


            console.log("item",item);
            console.log("id",id);
            $scope.master = dataTaskSave;

            console.log("$scope.master2",$scope.master);
            for(var i = 0; i < $scope.master.length; i++){
                $scope.master[i].No = i + 1;
                for(var a = 0; a < $scope.master[i].AEstimationBP_PartList.length; a++){
                    $scope.master[i].AEstimationBP_PartList[a].No = $scope.master[i].No +"."+ (a+1);
                    $scope.master[i].AEstimationBP_PartList[a].test = $scope.master[i].AEstimationBP_PartList[a].Availbility; 
                }
            }

            
            for(var zz = 0; zz < $scope.paymentData.length; zz++){
                for(var d = 0; d < $scope.master.length; d++){
                    if ($scope.paymentData[zz].MasterId == $scope.master[d].PaidById) {
                        $scope.master[d].PaidBy = $scope.paymentData[zz].Name;
                    }
                }
            }
            console.log("$scope.dataGoodWork",$scope.dataGoodWork);
            $scope.agGridWorkViewApi.setRowData($scope.master);
            console.log("$scope.gridWorkView",$scope.gridWorkView.rowData);
            //$scope.gridEstimation.data=[];
            //$scope.gridEstimation.data = $scope.master;
            console.log("$scope.gridEstimation.data",$scope.gridEstimation.data);
            totalEstimation = parseInt(item.BodyEstimationMinute) +
                parseInt(item.PutyEstimationMinute) +
                parseInt(item.SurfacerEstimationMinute) +
                parseInt(item.SprayingEstimationMinute) +
                parseInt(item.PolishingEstimationMinute) +
                parseInt(item.ReassemblyEstimationMinute) +
                parseInt(item.FIEstimationMinute);
            

            console.log('item.isUpdateEstimate ===>',item.isUpdateEstimate);
            console.log('item.TaskName ===>',item.TaskName);
            console.log('item.TaskBPId ===>',item.TaskBPId);

            if(item.isUpdateEstimate == true){
                $scope.mEstimationBP.TaskName = item.TaskName;
            }else{
                taskBPIdFromMaster = item.TaskBPId;
            }

            // if(onEditTaskName == taskNameFromMaster){
            //     console.log('tidak ada perubahan')
            // }else{
            //     if(taskNameFromMaster == null){
            //         console.log('ini manual name',customTaskName);
            //     }else{
            //         console.log('ini name dari master', taskNameFromMaster);
            //     }
            // }


            
            

            if($scope.gridEstimation.data.length > 0){
                console.log('masuk kondisi 1')

                var filtered = $scope.gridEstimation.data.filter(function(item) { 
                   return item.Flag == id;  
                });
                console.log("estimate data push or not",filtered);
                if(filtered.length == 0){
                    console.log('masuk kondisi 2')
                    if(item.TaskListBPId == null){ //ini task manual
                        $scope.gridEstimation.data.push({
                            "TaskBPId": item.TaskListBPId,
                            "TaskName": item.TaskName,
                            // "TaskName": $scope.mEstimationBP.TaskName,

                            // "BodyEstimationMinute": 0,
                            // "PutyEstimationMinute": 0,
                            // "SurfacerEstimationMinute":  0,
                            // "SprayingEstimationMinute":  0,
                            // "PolishingEstimationMinute":  0,
                            // "ReassemblyEstimationMinute": 0,
                            // "FIEstimationMinute": 0,

                            "BodyEstimationMinute": item.BodyEstimationMinute ? item.BodyEstimationMinute : 0,
                            "PutyEstimationMinute": item.PutyEstimationMinute ? item.PutyEstimationMinute : 0,
                            "SurfacerEstimationMinute": item.SurfacerEstimationMinute ? item.SurfacerEstimationMinute : 0,
                            "SprayingEstimationMinute": item.SprayingEstimationMinute ? item.SprayingEstimationMinute : 0,
                            "PolishingEstimationMinute": item.PolishingEstimationMinute ? item.PolishingEstimationMinute : 0,
                            "ReassemblyEstimationMinute": item.ReassemblyEstimationMinute ? item.ReassemblyEstimationMinute : 0,
                            "FIEstimationMinute": item.FIEstimationMinute ? item.FIEstimationMinute : 0,

                            "Total": totalEstimation,
                            "Flag" : id,
                            "isFromVc": item.isFromVc == 1 ? item.isFromVc : 0
                        });
                        
                        
                    }else{ // ini task dari master.
                        //var tmpIdxGridTimeEst = -1 ;
                        var tmpIdxGridTimeEst = _.findIndex($scope.master, { "TaskBPId": item.TaskListBPId });
                        console.log('indexxx',tmpIdxGridTimeEst);
                        if(tmpIdxGridTimeEst != -1){
                            $scope.gridEstimation.data.splice(tmpIdxGridTimeEst,1);
                        }
                        console.log('push master1',$scope.gridEstimation.data)
                        $scope.gridEstimation.data.push({
                            "TaskBPId": item.TaskListBPId,
                            "TaskName": item.TaskName,
                            // "TaskName": $scope.mEstimationBP.TaskName,
                            "BodyEstimationMinute": item.BodyEstimationMinute ? item.BodyEstimationMinute : 0,
                            "PutyEstimationMinute": item.PutyEstimationMinute ? item.PutyEstimationMinute : 0,
                            "SurfacerEstimationMinute": item.SurfacerEstimationMinute ? item.SurfacerEstimationMinute : 0,
                            "SprayingEstimationMinute": item.SprayingEstimationMinute ? item.SprayingEstimationMinute : 0,
                            "PolishingEstimationMinute": item.PolishingEstimationMinute ? item.PolishingEstimationMinute : 0,
                            "ReassemblyEstimationMinute": item.ReassemblyEstimationMinute ? item.ReassemblyEstimationMinute : 0,
                            "FIEstimationMinute": item.FIEstimationMinute ? item.FIEstimationMinute : 0,
                            "Total": totalEstimation,
                            "Flag" : id,
                            "isFromVc": item.isFromVc == 1 ? item.isFromVc : 0
                        });
                        console.log('push master',$scope.gridEstimation.data)
                        tmpIdxGridTimeEst = -1 ;
                    }
                }else{
                    console.log('masuk kondisi 3')
                    for(var c = 0; c < $scope.gridEstimation.data.length; c++){
                        if($scope.gridEstimation.data[c].FlagID == id){
                            console.log("estimasti grid",$scope.gridEstimation.data)
                            
                            $scope.gridEstimation.data[c].TaskBPId = item.TaskListBPId;
                            // $scope.gridEstimation.data[c].TaskName = $scope.mEstimationBP.TaskName;
                            $scope.gridEstimation.data[c].TaskName = item.TaskName;
                            
                            $scope.gridEstimation.data[c].BodyEstimationMinute = item.BodyEstimationMinute ? item.BodyEstimationMinute : 0;
                            $scope.gridEstimation.data[c].PutyEstimationMinute = item.PutyEstimationMinute? item.PutyEstimationMinute : 0;
                            $scope.gridEstimation.data[c].SurfacerEstimationMinute = item.SurfacerEstimationMinute ? item.SurfacerEstimationMinute : 0; 
                            $scope.gridEstimation.data[c].SprayingEstimationMinute = item.SprayingEstimationMinute ? item.SprayingEstimationMinute : 0;
                            $scope.gridEstimation.data[c].PolishingEstimationMinute = item.PolishingEstimationMinute ? item.PolishingEstimationMinute : 0;
                            $scope.gridEstimation.data[c].ReassemblyEstimationMinute = item.ReassemblyEstimationMinute ? item.ReassemblyEstimationMinute : 0;
                            $scope.gridEstimation.data[c].FIEstimationMinute =  item.FIEstimationMinute ? item.FIEstimationMinute : 0;
                            $scope.gridEstimation.data[c].isFromVc = $scope.gridEstimation.data[c].isFromVc == 1? $scope.gridEstimation.data[c].isFromVc : 0
                        }
                    }
                }
            }else{
                console.log('masuk kondisi 4')
                $scope.gridEstimation.data.push({
                    "TaskBPId": item.TaskListBPId,
                    "TaskName": item.TaskName,
                    // "TaskName":  $scope.mEstimationBP.TaskName,
                    "BodyEstimationMinute": item.BodyEstimationMinute ? item.BodyEstimationMinute : 0,
                    "PutyEstimationMinute": item.PutyEstimationMinute ? item.PutyEstimationMinute : 0,
                    "SurfacerEstimationMinute": item.SurfacerEstimationMinute ? item.SurfacerEstimationMinute : 0,
                    "SprayingEstimationMinute": item.SprayingEstimationMinute ? item.SprayingEstimationMinute : 0,
                    "PolishingEstimationMinute": item.PolishingEstimationMinute ? item.PolishingEstimationMinute : 0,
                    "ReassemblyEstimationMinute": item.ReassemblyEstimationMinute ? item.ReassemblyEstimationMinute : 0,
                    "FIEstimationMinute": item.FIEstimationMinute ? item.FIEstimationMinute : 0,
                    "Total": totalEstimation,
                    "Flag" : id,
                    "isFromVc": item.isFromVc == 1 ? item.isFromVc : 0

                }); 
            }

            // =======================
            var totalW = 0;
            for (var i = 0; i < $scope.master.length; i++) {
                if($scope.master[i].Fare !== undefined){
                    if ($scope.master[i].PaidById == 30) {
                        totalW += 0
                    } else {
                        // totalW += Math.round($scope.master[i].Fare/1.1);
                        totalW += ASPricingEngine.calculate({
                                        InputPrice: $scope.master[i].Fare,
                                        // Discount: 5,
                                        // Qty: 2,
                                        Tipe: 2, 
                                        PPNPercentage: PPNPerc,
                                    });
                    }
                    
                }
            };
            console.log("totalW",totalW);
            $scope.totalWork = totalW;  //request pak dodi dibagi 1.1 13/02/2019
            // =======================
            // var totalM = 0;
            // for (var i = 0; i < $scope.master.length; i++) {
            //     if ($scope.master[i].AEstimationBP_PartList !== undefined) {
            //         for (var j = 0; j < $scope.master[i].AEstimationBP_PartList.length; j++) {
            //             if ($scope.master[i].AEstimationBP_PartList[j].SubTotal !== undefined) {
            //                 totalM += $scope.master[i].AEstimationBP_PartList[j].RetailPrice * $scope.master[i].AEstimationBP_PartList[j].qty;
            //                 console.log("$scope.master[i].AEstimationBP_PartList.Price", $scope.master[i].AEstimationBP_PartList[j].SubTotal);
            //             }
            //         }
            //     }
            // }
            // console.log("totalM",totalM);
            // $scope.totalMaterial = totalM / (1.1);  //request pak dodi dibagi 1.1 13/02/2019
            // =======================
            
            
            // =======================
            var totalM = 0;
            for (var i = 0; i < $scope.master.length; i++) {
                if ($scope.master[i].AEstimationBP_PartList !== undefined) {
                    for (var j = 0; j < $scope.master[i].AEstimationBP_PartList.length; j++) {
                        if ($scope.master[i].AEstimationBP_PartList[j].SubTotal !== undefined) {

                            if ($scope.master[i].AEstimationBP_PartList[j].PaidById == 30) {
                                totalM += 0
                            } else {
                                // totalM += Math.round(($scope.master[i].AEstimationBP_PartList[j].RetailPrice - ($scope.master[i].AEstimationBP_PartList[j].Discount/100))/1.1) * $scope.master[i].AEstimationBP_PartList[j].qty;
                                // totalM +=  Math.round($scope.master[i].AEstimationBP_PartList[j].RetailPrice / 1.1 ) * $scope.master[i].AEstimationBP_PartList[j].qty;
                                totalM +=  ASPricingEngine.calculate({
                                                InputPrice: $scope.master[i].AEstimationBP_PartList[j].RetailPrice,
                                                // Discount: 5,
                                                Qty: $scope.master[i].AEstimationBP_PartList[j].qty,
                                                Tipe: 102, 
                                                PPNPercentage: PPNPerc,
                                            }); 
                            }

                            

                        }
                    }
                }
            }
            $scope.totalMaterial = totalM;
            // =======================
            var totalMD = 0;
            for (var i = 0; i < $scope.master.length; i++) {
                if ($scope.master[i].AEstimationBP_PartList !== undefined) {
                    for (var j = 0; j < $scope.master[i].AEstimationBP_PartList.length; j++) {
                        if ($scope.master[i].AEstimationBP_PartList[j].SubTotal !== undefined) {

                            if ($scope.master[i].AEstimationBP_PartList[j].PaidById == 30) {
                                totalMD += 0
                            } else {
                                // totalMD +=  Math.round(Math.round($scope.master[i].AEstimationBP_PartList[j].RetailPrice /1.1 ) * ($scope.master[i].AEstimationBP_PartList[j].Discount/100)) * $scope.master[i].AEstimationBP_PartList[j].qty; 
                                totalMD += ASPricingEngine.calculate({
                                                InputPrice: $scope.master[i].AEstimationBP_PartList[j].RetailPrice,
                                                Discount: $scope.master[i].AEstimationBP_PartList[j].Discount,
                                                Qty: $scope.master[i].AEstimationBP_PartList[j].qty,
                                                Tipe: 112, 
                                                PPNPercentage: PPNPerc,
                                            }); 
                            }

                            

                        }
                    }
                }
            }
            $scope.totalMaterialDiscounted = totalMD;
            // =======================
            var totalWD = 0;
            console.log("RHfare",$scope.master);
            for (var i = 0; i < $scope.master.length; i++) {
                if($scope.master[i].Fare !== undefined){
                    if ($scope.master[i].PaidById == 30) {
                        totalWD += 0
                    } else {
                        // totalWD += Math.round(Math.round($scope.master[i].Fare /1.1 ) * ($scope.master[i].Discount/100));
                        totalWD += ASPricingEngine.calculate({
                                        InputPrice: $scope.master[i].Fare,
                                        Discount: $scope.master[i].Discount,
                                        // Qty: 2,
                                        Tipe: 12, 
                                        PPNPercentage: PPNPerc,
                                    }); 
                    }
                    
                }
            };

            console.log("TWD",$scope.totalWorkDiscounted);
            $scope.totalWorkDiscounted = totalWD;
            // =========================
            //$scope.countDP();
            $scope.totalFinalTask = $scope.totalWork - $scope.totalWorkDiscounted;
            $scope.totalFinalParts = $scope.totalMaterial - $scope.totalMaterialDiscounted;
            // $scope.PPN = Math.floor(($scope.totalFinalTask + $scope.totalFinalParts) * 0.1);
            $scope.PPN = ASPricingEngine.calculate({
                                InputPrice: (($scope.totalFinalTask + $scope.totalFinalParts) * (1+PPNPerc/100)),
                                // Discount: 0,
                                // Qty: 0,
                                Tipe: 33, 
                                PPNPercentage: PPNPerc,
                            });  
            $scope.totalEstimasi =  $scope.totalFinalTask + $scope.totalFinalParts + $scope.PPN ;
            console.log("$scope.PPN",$scope.PPN);

            //nentuin diskon task & parts
            jejeranDiskonTask = [];
            jejeranDiskonParts = [];
            for(var i in $scope.master){
                jejeranDiskonTask.push($scope.master[i].Discount);
                if($scope.master[i].AEstimationBP_PartList.length > 0 ){
                    for(var j in $scope.master[i].AEstimationBP_PartList){
                        jejeranDiskonParts.push($scope.master[i].AEstimationBP_PartList[j].Discount);
                    }
                }
            }
            //nentuin diskon task & parts

            taskBPIdFromMaster = null;
            customTaskName = null;

            $scope.displayDiscountAlgorithm();
         
        }

        $scope.countDP = function() {
            $scope.master = dataTaskSave;

            var totalDpCount = 0;
            for (var i = 0; i < $scope.master.length; i++) {
                console.log("dataRH",$scope.master[i].AEstimationBP_PartList);
                for(var j in $scope.master[i].AEstimationBP_PartList){
                    // var subtotal = $scope.master[i].AEstimationBP_PartList[j].qty;

                    //var subtotal = ($scope.master[i].AEstimationBP_PartList[j].RetailPrice * $scope.master[i].AEstimationBP_PartList[j].qty) - ($scope.master[i].AEstimationBP_PartList[j].RetailPrice * $scope.master[i].AEstimationBP_PartList[j].qty)*(($scope.master[i].AEstimationBP_PartList[j].Discount)/100);
                    var subtotal = ($scope.master[i].AEstimationBP_PartList[j].RetailPrice * $scope.master[i].AEstimationBP_PartList[j].qty);
                    console.log("subtotal",subtotal)
                    if($scope.master[i].AEstimationBP_PartList[j].Availbility != undefined){
                        console.log("RhPartlist",$scope.master[i].AEstimationBP_PartList[j]);
                    console.log("RhAvail",$scope.master[i].AEstimationBP_PartList[j].Availbility);
                    console.log("RhPBI",$scope.master[i].AEstimationBP_PartList[j].PaidById);
                    console.log("test",$scope.master[i].AEstimationBP_PartList[j].test);
                    }


                    // code ini agak aneh, di komen dl aja di ganti pake yg bwh nya
                    // if($scope.master[i].AEstimationBP_PartList[j].DPRequest > 100.00 && $scope.master[i].AEstimationBP_PartList[j].Availbility == 'Tidak Tersedia' && $scope.master[i].AEstimationBP_PartList[j].PaidBy == 'Customer' && $scope.master[i].AEstimationBP_PartList[j].OrderType == 3){
                    //     console.log("masuk 1");
                    //     // totalDpCount +=  $scope.master[i].AEstimationBP_PartList[j].qty
                    //     console.log("didalem 1",totalDpCount);
                    //     totalDpCount += subtotal * (($scope.master[i].AEstimationBP_PartList[j].DownPayment)/100);
                    // } else{
                    //     console.log("masuk 2");
                    //     totalDpCount += subtotal * (($scope.master[i].AEstimationBP_PartList[j].DPRequest)/100);
                    // }

                    if ($scope.master[i].AEstimationBP_PartList[j].DownPayment != null && $scope.master[i].AEstimationBP_PartList[j].DownPayment != undefined) {
                        if ($scope.master[i].AEstimationBP_PartList[j].PaidById == 28 || $scope.master[i].AEstimationBP_PartList[j].PaidById == 32)
                        totalDpCount += $scope.master[i].AEstimationBP_PartList[j].DownPayment
                    }
                }   
            }
            $scope.totalDp = totalDpCount;
            console.log('totalDp',$scope.totalDp)
        }

        // $scope.getDataGirdWorkView = function(item,id){


        //     console.log("item",item);
        //     $scope.master = dataTaskSave;
        
        //     console.log("$scope.master2",$scope.master);
        //     for(var i = 0; i < $scope.master.length; i++){
        //         $scope.master[i].No = i + 1;
        //         for(var a = 0; a < $scope.master[i].AEstimationBP_PartList.length; a++){
        //             $scope.master[i].AEstimationBP_PartList[a].No = $scope.master[i].No +"."+ (a+1);
        //         }
        //     }
        
            
        //     for(var zz = 0; zz < $scope.paymentData.length; zz++){
        //         for(var d = 0; d < $scope.master.length; d++){
        //             if ($scope.paymentData[zz].MasterId == $scope.master[d].PaidById) {
        //                 $scope.master[d].PaidBy = $scope.paymentData[zz].Name;
        //             }
        //         }
        //     }
        //     console.log("$scope.dataGoodWork",$scope.dataGoodWork);
        //     $scope.agGridWorkViewApi.setRowData($scope.master);
        //     console.log("$scope.gridWorkView",$scope.gridWorkView.rowData);
            
        //     // $scope.gridEstimation.data = $scope.master;
        //     console.log("$scope.gridEstimation.data",$scope.gridEstimation.data);
        //     totalEstimation = parseInt(item.BodyEstimationMinute) +
        //         parseInt(item.PutyEstimationMinute) +
        //         parseInt(item.SurfacerEstimationMinute) +
        //         parseInt(item.SprayingEstimationMinute) +
        //         parseInt(item.PolishingEstimationMinute) +
        //         parseInt(item.ReassemblyEstimationMinute) +
        //         parseInt(item.FIEstimationMinute);
            
        
        //     console.log('item.isUpdateEstimate ===>',item.isUpdateEstimate);
        //     console.log('item.TaskName ===>',item.TaskName);
        //     console.log('item.TaskBPId ===>',item.TaskBPId);
        
        //     if(item.isUpdateEstimate == true){
        //         $scope.mEstimationBP.TaskName = item.TaskName;
        //     }else{
        //         taskBPIdFromMaster = item.TaskBPId;
        //     }
        
        //     // if(onEditTaskName == taskNameFromMaster){
        //     //     console.log('tidak ada perubahan')
        //     // }else{
        //     //     if(taskNameFromMaster == null){
        //     //         console.log('ini manual name',customTaskName);
        //     //     }else{
        //     //         console.log('ini name dari master', taskNameFromMaster);
        //     //     }
        //     // }
        
        
            
        
        
        //     if($scope.gridEstimation.data.length > 0){
        //         console.log('masuk kondisi 1')
        
        //         var filtered = $scope.gridEstimation.data.filter(function(item) { 
        //            return item.Flag == id;  
        //         });
        
        //         console.log("estimate data push or not",filtered);
        //         if(filtered.length == 0){
        //             console.log('masuk kondisi 2')
        //             if(item.TaskListBPId == null){ //ini task manual
        //                 $scope.gridEstimation.data.push({
        //                     "TaskBPId": item.TaskListBPId,
        //                     "TaskName": item.TaskName,
        //                     // "TaskName": $scope.mEstimationBP.TaskName,
        //                     "BodyEstimationMinute": 0,
        //                     "PutyEstimationMinute": 0,
        //                     "SurfacerEstimationMinute":  0,
        //                     "SprayingEstimationMinute":  0,
        //                     "PolishingEstimationMinute":  0,
        //                     "ReassemblyEstimationMinute": 0,
        //                     "FIEstimationMinute": 0,
        //                     "Total": totalEstimation,
        //                     "Flag" : id,
        //                     "isFromVc": item.isFromVc == 1 ? item.isFromVc : 0
        //                 });
        //             }else{ // ini task dari master
        //                 $scope.gridEstimation.data.push({
        //                     "TaskBPId": item.TaskListBPId,
        //                     "TaskName": item.TaskName,
        //                     // "TaskName": $scope.mEstimationBP.TaskName,
        //                     "BodyEstimationMinute": item.BodyEstimationMinute ? item.BodyEstimationMinute : 0,
        //                     "PutyEstimationMinute": item.PutyEstimationMinute !== null ? item.PutyEstimationMinute : 0,
        //                     "SurfacerEstimationMinute": item.SurfacerEstimationMinute ? item.SurfacerEstimationMinute : 0,
        //                     "SprayingEstimationMinute": item.SprayingEstimationMinute ? item.SprayingEstimationMinute : 0,
        //                     "PolishingEstimationMinute": item.PolishingEstimationMinute ? item.PolishingEstimationMinute : 0,
        //                     "ReassemblyEstimationMinute": item.ReassemblyEstimationMinute ? item.ReassemblyEstimationMinute : 0,
        //                     "FIEstimationMinute": item.FIEstimationMinute ? item.FIEstimationMinute : 0,
        //                     "Total": totalEstimation,
        //                     "Flag" : id,
        //                     "isFromVc": item.isFromVc == 1 ? item.isFromVc : 0
        //                 });
        //             }
        //         }else{
        //             console.log('masuk kondisi 3')
        //             for(var c = 0; c < $scope.gridEstimation.data.length; c++){
        //                 if($scope.gridEstimation.data[c].Flag == id){
                            
        //                     $scope.gridEstimation.data[c].TaskBPId = item.TaskListBPId;
        //                     // $scope.gridEstimation.data[c].TaskName = $scope.mEstimationBP.TaskName;
        //                     $scope.gridEstimation.data[c].TaskName = item.TaskName;
                            
        //                     $scope.gridEstimation.data[c].BodyEstimationMinute = item.BodyEstimationMinute ? item.BodyEstimationMinute : 0;
        //                     $scope.gridEstimation.data[c].PutyEstimationMinute = item.PutyEstimationMinute? item.PutyEstimationMinute : 0;
        //                     $scope.gridEstimation.data[c].SurfacerEstimationMinute = item.SurfacerEstimationMinute ? item.SurfacerEstimationMinute : 0; 
        //                     $scope.gridEstimation.data[c].SprayingEstimationMinute = item.SprayingEstimationMinute ? item.SprayingEstimationMinute : 0;
        //                     $scope.gridEstimation.data[c].PolishingEstimationMinute = item.PolishingEstimationMinute ? item.PolishingEstimationMinute : 0;
        //                     $scope.gridEstimation.data[c].ReassemblyEstimationMinute = item.ReassemblyEstimationMinute ? item.ReassemblyEstimationMinute : 0;
        //                     $scope.gridEstimation.data[c].FIEstimationMinute =  item.FIEstimationMinute ? item.FIEstimationMinute : 0;
        //                     $scope.gridEstimation.data[c].isFromVc = $scope.gridEstimation.data[c].isFromVc == 1? $scope.gridEstimation.data[c].isFromVc : 0
        //                 }
        //             }
        //         }
        //     }else{
        //         console.log('masuk kondisi 4')
        //         $scope.gridEstimation.data.push({
        //             "TaskBPId": item.TaskListBPId,
        //             "TaskName": item.TaskName,
        //             // "TaskName":  $scope.mEstimationBP.TaskName,
        //             "BodyEstimationMinute": item.BodyEstimationMinute ? item.BodyEstimationMinute : 0,
        //             "PutyEstimationMinute": item.PutyEstimationMinute ? item.PutyEstimationMinute : 0,
        //             "SurfacerEstimationMinute": item.SurfacerEstimationMinute ? item.SurfacerEstimationMinute : 0,
        //             "SprayingEstimationMinute": item.SprayingEstimationMinute ? item.SprayingEstimationMinute : 0,
        //             "PolishingEstimationMinute": item.PolishingEstimationMinute ? item.PolishingEstimationMinute : 0,
        //             "ReassemblyEstimationMinute": item.ReassemblyEstimationMinute ? item.ReassemblyEstimationMinute : 0,
        //             "FIEstimationMinute": item.FIEstimationMinute ? item.FIEstimationMinute : 0,
        //             "Total": totalEstimation,
        //             "Flag" : id,
        //             "isFromVc": item.isFromVc == 1 ? item.isFromVc : 0
        
        //         }); 
        //     }
        
        //     // =======================
        //     var totalW = 0;
        //     for (var i = 0; i < $scope.master.length; i++) {
        //         if($scope.master[i].Fare !== undefined){
        //             totalW += Math.round($scope.master[i].Fare/1.1);
        //         }
        //     };
        //     console.log("totalW",totalW);
        //     $scope.totalWork = totalW;  //request pak dodi dibagi 1.1 13/02/2019
        //     // =======================
        //     // var totalM = 0;
        //     // for (var i = 0; i < $scope.master.length; i++) {
        //     //     if ($scope.master[i].AEstimationBP_PartList !== undefined) {
        //     //         for (var j = 0; j < $scope.master[i].AEstimationBP_PartList.length; j++) {
        //     //             if ($scope.master[i].AEstimationBP_PartList[j].SubTotal !== undefined) {
        //     //                 totalM += $scope.master[i].AEstimationBP_PartList[j].RetailPrice * $scope.master[i].AEstimationBP_PartList[j].qty;
        //     //                 console.log("$scope.master[i].AEstimationBP_PartList.Price", $scope.master[i].AEstimationBP_PartList[j].SubTotal);
        //     //             }
        //     //         }
        //     //     }
        //     // }
        //     // console.log("totalM",totalM);
        //     // $scope.totalMaterial = totalM / (1.1);  //request pak dodi dibagi 1.1 13/02/2019
        //     // =======================
            
        //     for (var i = 0; i < $scope.master.length; i++) {
        //         console.log("dataRH",$scope.master[i].AEstimationBP_PartList);
        //         // for(var j in $scope.master[i].AEstimationBP_PartList){
        //             for(var j = 0; j <  $scope.master[i].AEstimationBP_PartList.length; j++){
        
        //             // --------------------------------  coba cek available
        
        //             AppointmentBpService.getAvailableParts($scope.master[i].AEstimationBP_PartList[j].PartsId, 0, $scope.master[i].AEstimationBP_PartList[j].qty).then(function(res) { 
                        
        //                 var totalDpCount = 0;
        
        //                 console.log("data yagn didapat",res.data.Result);
        //                 var keter;
        //                 switch (res.data.Result[0].isAvailable) {
        //                     case 0:
        //                         keter = "Tidak Tersedia";
        //                         break;
        //                     case 1:
        //                         keter = "Tersedia";
        //                         break;
        //                     case 2:
        //                         keter = "Tersedia Sebagian";
        //                         break;
        //                 }
                        
        //             var subtotal = ($scope.master[i].AEstimationBP_PartList[j].RetailPrice * $scope.master[i].AEstimationBP_PartList[j].qty) - ($scope.master[i].AEstimationBP_PartList[j].RetailPrice * $scope.master[i].AEstimationBP_PartList[j].qty)*(($scope.master[i].AEstimationBP_PartList[j].Discount)/100);
        //             console.log("RhPartlist",$scope.master[i].AEstimationBP_PartList[j]);
        //             console.log("RhAvail",$scope.master[i].AEstimationBP_PartList[j].Availbility);
        //             console.log("RHketer",keter);
        //             console.log("rhpaidby",$scope.master[i].AEstimationBP_PartList[j].PaidById);
        //             if($scope.master[i].AEstimationBP_PartList[j].DownPayment > 100.00){
        //                 totalDpCount += $scope.master[i].AEstimationBP_PartList[j].DownPayment ;
        //             }else if($scope.master[i].AEstimationBP_PartList[j].DownPayment < 100 && $scope.master[i].AEstimationBP_PartList[j].Availbility == "Tidak Tersedia" && $scope.master[i].AEstimationBP_PartList[j].PaidBy == "Customer"){
        //                 console.log("masuk 1");
        //                 totalDpCount += subtotal * (($scope.master[i].AEstimationBP_PartList[j].DownPayment)/100);
        //             }else if($scope.master[i].AEstimationBP_PartList[j].DownPayment < 100.00 && $scope.master[i].AEstimationBP_PartList[j].OrderType == "3" && $scope.master[i].AEstimationBP_PartList[j].PaidBy == "Customer"){
        //                 console.log("masuk 2");
        //                 totalDpCount += subtotal * (($scope.master[i].AEstimationBP_PartList[j].DownPayment)/100);
        //             }
        
        //             $scope.totalDp = totalDpCount;
        //             console.log('totalDp',$scope.totalDp)
        
                        
        //             },function(err) {
        //                     //console.log("err=>", err);
        //             });
        
        //         }   
        //     }
        //     // =======================
        //     var totalM = 0;
        //     for (var i = 0; i < $scope.master.length; i++) {
        //         if ($scope.master[i].AEstimationBP_PartList !== undefined) {
        //             for (var j = 0; j < $scope.master[i].AEstimationBP_PartList.length; j++) {
        //                 if ($scope.master[i].AEstimationBP_PartList[j].SubTotal !== undefined) {
        //                     // totalM += Math.round(($scope.master[i].AEstimationBP_PartList[j].RetailPrice - ($scope.master[i].AEstimationBP_PartList[j].Discount/100))/1.1) * $scope.master[i].AEstimationBP_PartList[j].qty;
        //                     totalM +=  Math.round($scope.master[i].AEstimationBP_PartList[j].RetailPrice / 1.1 ) * $scope.master[i].AEstimationBP_PartList[j].qty;
        
        //                 }
        //             }
        //         }
        //     }
        //     $scope.totalMaterial = totalM;
        //     // =======================
        //     var totalMD = 0;
        //     for (var i = 0; i < $scope.master.length; i++) {
        //         if ($scope.master[i].AEstimationBP_PartList !== undefined) {
        //             for (var j = 0; j < $scope.master[i].AEstimationBP_PartList.length; j++) {
        //                 if ($scope.master[i].AEstimationBP_PartList[j].SubTotal !== undefined) {
        //                     totalMD +=  Math.round(Math.round($scope.master[i].AEstimationBP_PartList[j].RetailPrice /1.1 ) * ($scope.master[i].AEstimationBP_PartList[j].Discount/100)) * $scope.master[i].AEstimationBP_PartList[j].qty; 
        //                 }
        //             }
        //         }
        //     }
        //     $scope.totalMaterialDiscounted = totalMD;
        //     // =======================
        //     var totalWD = 0;
        //     console.log("RHfare",$scope.master);
        //     for (var i = 0; i < $scope.master.length; i++) {
        //         if($scope.master[i].Fare !== undefined){
        //             totalWD += Math.round(Math.round($scope.master[i].Fare /1.1 ) * ($scope.master[i].Discount/100));
        //         }
        //     };
        
        //     console.log("TWD",$scope.totalWorkDiscounted);
        //     $scope.totalWorkDiscounted = totalWD;
        //     // =========================
        //     $scope.totalFinalTask = $scope.totalWork - $scope.totalWorkDiscounted;
        //     $scope.totalFinalParts = $scope.totalMaterial - $scope.totalMaterialDiscounted;
        //     $scope.PPN = Math.floor(($scope.totalFinalTask + $scope.totalFinalParts) * 0.1);
        //     $scope.totalEstimasi =  $scope.totalFinalTask + $scope.totalFinalParts + $scope.PPN ;
        //     console.log("$scope.PPN",$scope.PPN);
        
        //     //nentuin diskon task & parts
        //     jejeranDiskonTask = [];
        //     jejeranDiskonParts = [];
        //     for(var i in $scope.master){
        //         jejeranDiskonTask.push($scope.master[i].Discount);
        //         if($scope.master[i].AEstimationBP_PartList.length > 0 ){
        //             for(var j in $scope.master[i].AEstimationBP_PartList){
        //                 jejeranDiskonParts.push($scope.master[i].AEstimationBP_PartList[j].Discount);
        //             }
        //         }
        //     }
        //     //nentuin diskon task & parts
        
        //     taskBPIdFromMaster = null;
        //     customTaskName = null;
        
        //     $scope.displayDiscountAlgorithm();
         
        // }

        $scope.coutnData = function(){
            console.log('$scope.master | coutnData ==>',$scope.master);
            var totalW = 0;
            for (var i = 0; i < $scope.master.length; i++) {
                if($scope.master[i].Fare !== undefined){
                    if ($scope.master[i].PaidById == 30) {
                        totalW += 0
                    } else {
                        // totalW += $scope.master[i].Fare;
                        totalW += ASPricingEngine.calculate({
                                        InputPrice: $scope.master[i].Fare,
                                        // Discount: 5,
                                        // Qty: 2,
                                        Tipe: 2, 
                                        PPNPercentage: PPNPerc,
                                    });
                    }
                    
                }
            };
            console.log("totalW",totalW);
            // $scope.totalWork = totalW / (1.1);  //request pak dodi dibagi 1.1 13/02/2019
            $scope.totalWork = totalW 
            // ========================
            // var totalM = 0;
            // for (var i = 0; i < $scope.master.length; i++) {
            //     if ($scope.master[i].AEstimationBP_PartList !== undefined) {
            //         for (var j = 0; j < $scope.master[i].AEstimationBP_PartList.length; j++) {
            //             if ($scope.master[i].AEstimationBP_PartList[j].SubTotal !== undefined) {
            //                 totalM +=  $scope.master[i].AEstimationBP_PartList[j].RetailPrice * $scope.master[i].AEstimationBP_PartList[j].qty;
            //                 console.log("$scope.master[i].AEstimationBP_PartList.Price", $scope.master[i].AEstimationBP_PartList[j].SubTotal);
            //             }
            //         }
            //     }
            // }
            // console.log("totalM",totalM);
            // $scope.totalMaterial = totalM / (1.1);  //request pak dodi dibagi 1.1 13/02/2019
            // ========================
        //    var totalDpCount = 0;
        //     for (var i = 0; i < $scope.master.length; i++) {
        //         for(var j in $scope.master[i].AEstimationBP_PartList){
        //             console.log('jjjj',$scope.master[i].AEstimationBP_PartList)
        //             var subtotal = ($scope.master[i].AEstimationBP_PartList[j].RetailPrice * $scope.master[i].AEstimationBP_PartList[j].qty) - ($scope.master[i].AEstimationBP_PartList[j].RetailPrice * $scope.master[i].AEstimationBP_PartList[j].qty)*(($scope.master[i].AEstimationBP_PartList[j].Discount)/100);
        //             if($scope.master[i].AEstimationBP_PartList[j].DownPayment > 100.00){
        //                 totalDpCount += $scope.master[i].AEstimationBP_PartList[j].DownPayment 
        //             }else if($scope.master[i].AEstimationBP_PartList[j].DownPayment < 100.00 && $scope.master[i].AEstimationBP_PartList[j].Availbility == 'Tidak Tersedia' && $scope.master[i].AEstimationBP_PartList[j].PaidById == 28){
        //                 totalDpCount += subtotal * (($scope.master[i].AEstimationBP_PartList[j].DownPayment)/100);
        //             }
        //         }   
        //     }
        //     $scope.totalDp = totalDpCount;
        //     console.log('totalDp',$scope.totalDp)
            $scope.countDP();
            // =========================
            var totalM = 0;
            for (var i = 0; i < $scope.master.length; i++) {
                if ($scope.master[i].AEstimationBP_PartList !== undefined) {
                    for (var j = 0; j < $scope.master[i].AEstimationBP_PartList.length; j++) {
                        if ($scope.master[i].AEstimationBP_PartList[j].SubTotal !== undefined) {

                            if ($scope.master[i].AEstimationBP_PartList[j].PaidById == 30) {
                                totalM += 0
                            } else {
                                // totalM += Math.round(($scope.master[i].AEstimationBP_PartList[j].RetailPrice - ($scope.master[i].AEstimationBP_PartList[j].Discount/100))/1.1) * $scope.master[i].AEstimationBP_PartList[j].qty;
                                totalM += ASPricingEngine.calculate({
                                                InputPrice: $scope.master[i].AEstimationBP_PartList[j].RetailPrice,
                                                // Discount: 5,
                                                Qty: $scope.master[i].AEstimationBP_PartList[j].qty,
                                                Tipe: 102, 
                                                PPNPercentage: PPNPerc,
                                            }); 
                            }
                            
                        }
                    }
                }
            }
            $scope.totalMaterial = totalM;
            // =======================
            var totalMD = 0;
            for (var i = 0; i < $scope.master.length; i++) {
                if ($scope.master[i].AEstimationBP_PartList !== undefined) {
                    for (var j = 0; j < $scope.master[i].AEstimationBP_PartList.length; j++) {
                        if ($scope.master[i].AEstimationBP_PartList[j].SubTotal !== undefined) {

                            if ($scope.master[i].AEstimationBP_PartList[j].PaidById == 30) {
                                totalMD += 0
                            } else {
                                // totalMD +=  Math.round(Math.round($scope.master[i].AEstimationBP_PartList[j].RetailPrice /1.1 ) * ($scope.master[i].AEstimationBP_PartList[j].Discount/100)) *  $scope.master[i].AEstimationBP_PartList[j].qty; 
                                totalMD +=  ASPricingEngine.calculate({
                                                InputPrice: $scope.master[i].AEstimationBP_PartList[j].RetailPrice,
                                                Discount: $scope.master[i].AEstimationBP_PartList[j].Discount,
                                                Qty: $scope.master[i].AEstimationBP_PartList[j].qty,
                                                Tipe: 112, 
                                                PPNPercentage: PPNPerc,
                                            }); 
                            }
                            
                        }
                    }
                }
            }
            $scope.totalMaterialDiscounted = totalMD;
            // =======================
            var totalWD = 0;
            for (var i = 0; i < $scope.master.length; i++) {
                console.log("RHfare2",$scope.master[i].Fare);
                if($scope.master[i].Fare !== undefined){

                    if ($scope.master[i].PaidById == 30) {
                        totalWD += 0
                    } else {
                        // totalWD +=  Math.round(Math.round($scope.master[i].Fare /1.1 ) * ($scope.master[i].Discount/100)) 
                        totalWD +=  ASPricingEngine.calculate({
                                        InputPrice: $scope.master[i].Fare,
                                        Discount: $scope.master[i].Discount,
                                        // Qty: 2,
                                        Tipe: 12, 
                                        PPNPercentage: PPNPerc,
                                    }); 
                    }
                    

                }
            };
            $scope.totalWorkDiscounted = totalWD;
            // =========================
            $scope.totalFinalTask = $scope.totalWork - $scope.totalWorkDiscounted;
            $scope.totalFinalParts = $scope.totalMaterial - $scope.totalMaterialDiscounted;
            // $scope.PPN = Math.floor(($scope.totalFinalTask + $scope.totalFinalParts) * 0.1);
            $scope.PPN = ASPricingEngine.calculate({
                                InputPrice: (($scope.totalFinalTask + $scope.totalFinalParts) * (1+PPNPerc/100)),
                                // Discount: 0,
                                // Qty: 0,
                                Tipe: 33, 
                                PPNPercentage: PPNPerc,
                            });  
            $scope.totalEstimasi =  $scope.totalFinalTask + $scope.totalFinalParts + $scope.PPN ;
            console.log("$scope.PPN",$scope.PPN);
        }
        // end ag grid parts
        //end function lifa



        
        // function Galang
        
       

        $scope.selectedColor = function(item) {
            console.log('selectedColor ===>', item);
            $scope.mEstimationBP.ColorId = item.ColorId;
            $scope.mEstimationBP.ColorCode = item.ColorCode;
            $scope.mEstimationBP.ColorName = item.ColorName;
        };

        


        $scope.DiskonPartsEndDate = null;
        $scope.DiskonTaskEndDate = null;
        $scope.choosePembayaran = function(data){
            $scope.mEstimationBP.isCash = data;
            console.log("data",data);



            if(data == 1){
                $scope.DiscountParts = 0;
                $scope.disParts = 0;
                $scope.DiscountTask = 0;
                $scope.disTask = 0;
                $scope.InsuranceName = null;
                $scope.DisplayDisTask = 0;
                $scope.DisplayDisParts = 0;



                if($scope.dataGoodWork.length > 0){
                    for(var i in $scope.dataGoodWork){

                        if ($scope.dataGoodWork[i].PaidById != 30) {
                            var cash = 28;
                            $scope.selectTypeCust(cash.toString());
    
                            $scope.selectDiscount($scope.dataGoodWork[i].DiscountTypeId);
    
                           
                            $scope.dataGoodWork[i].PaidById = 28; //customerId
                            $scope.dataGoodWork[i].PaidBy = "Customer"
                            $scope.dataGoodWork[i].DiscountTypeId = -1;
    
                            $scope.mEstimationBP.DiskonPersen = 0;
                            $scope.dataGoodWork[i].Discount = 0;
                            if($scope.dataGoodWork[i].AEstimationBP_PartList.length > 0){
                                for(var x in $scope.dataGoodWork[i].AEstimationBP_PartList){
                                    $scope.dataGoodWork[i].AEstimationBP_PartList[x].Discount = 0;
                                    $scope.dataGoodWork[i].AEstimationBP_PartList[x].PaidById = 28;
                                    $scope.dataGoodWork[i].AEstimationBP_PartList[x].PaidBy = "Customer";
                                    
                                }
                            }
                            $scope.gridClickEditHandler($scope.dataGoodWork[i],3);
                            $scope.saveFin(false);
                        }
                        
                        
                    }
                }
                // if(jejeranDiskonParts.length == 0 ){
                //     $scope.DisplayDisParts = 0;
                // }
                // if(jejeranDiskonTask.length == 0){
                //     $scope.DisplayDisTask = 0;
                // }
                $scope.mEstimationBP.InsuranceId = "";


                
            }else{
                console.log('ini pembayaran insurance ===>',data);
                
                $scope.DiscountParts = 0;
                $scope.disParts = 0;
                $scope.DiscountTask = 0;
                $scope.disTask = 0;
                if(jejeranDiskonParts.length == 0 ){
                    $scope.DisplayDisParts = 0;
                }
                if(jejeranDiskonTask.length == 0){
                    $scope.DisplayDisTask = 0;
                }
                



                if($scope.isFromHistory == true){
                    if($scope.dataHistory.length>0){
                        $scope.mEstimationBP.NoPolis             = $scope.dataHistory[0].NoPolis;
                        $scope.mEstimationBP.PolisDateTo         = $scope.dataHistory[0].PolisDateTo;
                        $scope.mEstimationBP.PolisDateFrom       = $scope.dataHistory[0].PolisDateFrom;
                        if($scope.dataHistory[0].isCash == 0){
                            for (i in $scope.optionsLihatInformasi_NamaInsurance_Search){
                                if($scope.optionsLihatInformasi_NamaInsurance_Search[i].InsuranceId == $scope.dataHistory[0].InsuranceId){
                                    $scope.mEstimationBP.InsuranceName = $scope.optionsLihatInformasi_NamaInsurance_Search[i].InsuranceName;
                                    $scope.mEstimationBP.InsuranceId = $scope.optionsLihatInformasi_NamaInsurance_Search[i].InsuranceId;
                                    $scope.onInsuranceSelected($scope.optionsLihatInformasi_NamaInsurance_Search[i]);
                                }
                            }
                        }
                    }
                }else{
                    $scope.InsuranceName = null;
                    $scope.mEstimationBP.InsuranceId = "";
                    selectedInsurance = {};
                }


            }
        }

        $scope.dateOptions = {
            startingDay: 1,
            format: 'dd-MM-yyyy',
        };



        $scope.mEstimationBP.VehicleTypeId = undefined;
        $scope.mVehicleCondition = {};
        $scope.areasArrayVC = [];
        $scope.imageSelVC = null;
        $scope.mData = {};
        $scope.copyTypeVehicle = null;
        $scope.copySelectedTypeVehicle = null;
        $scope.chooseVTypeVC = function (selected) {
            if($scope.mEstimationBP.isCash != undefined){
                $scope.mEstimationBP.isCash = $scope.mEstimationBP.isCash.toString();
            }
            if(selected == undefined){
                $scope.mVehicleCondition.Points = [];
                $('.choosen-area').removeClass('choosen-area');
                $scope.gridVC.data = [];
                dotsDataVC[mstIdVC] = [];
            }else{
                if($scope.mEstimationBP.isCash == undefined || $scope.mEstimationBP.isCash == null ||$scope.mEstimationBP.isCash ==""){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Mohon Input Tipe pembayaran Terlebih Dahulu",
                    });
                }else 
                    if(($scope.mEstimationBP.isCash == 0 || $scope.mEstimationBP.isCash == '0') &&  ($scope.mEstimationBP.InsuranceId == undefined || $scope.mEstimationBP.InsuranceId == null || $scope.mEstimationBP.InsuranceId == "") ){
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Mohon Input Asuransi Terlebih Dahulu",
                        });
                    }else{
                        if($scope.mEstimationBP.KatashikiCode == undefined || $scope.mEstimationBP.KatashikiCode == null || $scope.mEstimationBP.KatashikiCode ==""){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Mohon Input Tipe Kendaraan Terlebih Dahulu",
                            });
                        }else{
                            console.log('selected type on estimasi', selected);
                            console.log('scope model type on estimasi', $scope.filter.vTypeVC)


                            $scope.mEstimationBP.TypeForVC = selected;
                            $scope.mVehicleCondition.TypeId = selected; //ini untuk kirim WAC galang
                            $scope.mData.VehicleConditionTypeId = selected;
                            $scope.copyTypeVehicle = angular.copy($scope.filter.vTypeVC);
                            $scope.copySelectedTypeVehicle = angular.copy(selected);
                            mstIdVC = selected;
                            // $scope.gridVC.data = angular.copy(dotsDataVC[mstIdVC]);
                            // if (!$scope.isTablet) {
                            //     $scope.customHeight = { height: '500px' };
                            // } else {
                            $scope.customHeight = { height: '500px' };
                            // }
            
                            $scope.areasArrayVC = getImgAndPointsVC(mstIdVC);
                            console.log('$scope.areasArrayVC |chooseVTypeVC ====>',$scope.areasArrayVC);
            
                            $scope.gridVC.data =[];
                            $scope.mVehicleCondition.Points = [];
                        }
                    }
                } 

        
            
        };

        var dotsData = { 2221: [], 2222: [], 2223: [], 2224: [], 2225: [], 2226: [], 2227: [] };
        var mstId = null;

        // added by sss on 2017-10-11
        var dotsDataVC = { 2221: [], 2222: [], 2223: [], 2224: [], 2225: [], 2226: [], 2227: [] };
        var mstIdVC = null;
        var temp = {};

        function getImgAndPointsVC(mstIdVC) {
            console.log("mstIdVC : ", mstIdVC);
            switch (mstIdVC) {
                case 2221:
                    $scope.imageSelVC = "../images/wacImages/Type A.png";
                    break;
                case 2222:
                    $scope.imageSelVC = "../images/wacImages/Type B.png";
                    break;
                case 2223:
                    $scope.imageSelVC = "../images/wacImages/Type C.png";
                    break;
                case 2224:
                    $scope.imageSelVC = "../images/wacImages/Type D.png";
                    break;
                case 2225:
                    $scope.imageSelVC = "../images/wacImages/Type E.png";
                    break;
                case 2226:
                    $scope.imageSelVC = "../images/wacImages/Type F.png";
                    break;
                case 2227:
                    $scope.imageSelVC = "../images/wacImages/Type G.png";
                    break;
                case 5555:
                    $scope.imageSelVC = undefined;
                    break;
            }
            var temp = _.find($scope.imageWACArr, function (o) {
                return o.TypeId == mstIdVC;
            });
            console.log("temp : ", temp);
            return JSON.parse(temp.Points);
        };

        function setDataGrid(data) {
            console.log('data setDataGrid', data);
            $scope.grid.data = angular.copy(data);
        }

        // added by sss on 2017-10-11
        function setDataGridVC(data) {
            console.log('data setDataGrid VC ======> galang liat', data);
            $scope.gridVC.data = data;
            // $scope.gridApiVC.core.refresh();
            // $scope.gridApiVC.core.refreshRows();
            $scope.$apply();
        }

        GeneralMaster.getData(2030).then(
            function (res) {
                $scope.VTypeData = res.data.Result;
                console.log("$scope.VTypeData !!!!!!!!!!!!!!!!!!!!!!!!!!!!", $scope.VTypeData);
                return res.data;
            }
        );

        WOBP.getStatusVehicle().then(function (res) {
            $scope.StatusVehicle = res.data.Result;
        })

        WOBP.getPointVehicle().then(function (res) {
            $scope.PointsVehicle = res.data.Result;
        })

        $scope.onChangeAreas = function (ev, boxId, areas, area) {
            console.log('onChangeAreas ev|boxId|areas|area ===>', ev +"|" + boxId +"|"+ areas+"|"+area);
            $scope.logDots = JSON.stringify(areas);
            $scope.$apply();
        };

        $scope.onAddArea = function (ev, boxId, areas, area) {
            console.log('onAddArea ev|boxId|areas|area ===>', ev +"|" + boxId +"|"+ areas+"|"+area);
            $scope.logDots = JSON.stringify(areas);
            $scope.$apply();
        };

        $scope.onRemoveArea = function (ev, boxId, areas, area) {
            console.log('onRemoveArea ev|boxId|areas|area ===>', ev +"|" + boxId +"|"+ areas+"|"+area);
            $scope.logDots = JSON.stringify(areas);
            $scope.$apply();
        };


        $scope.PointsVehicle = [];
        $scope.StatusVehicle = [];
        $scope.DamageVehicle = [];
        $scope.chgChkVC = function () {
            // console.log("$scope.disableWACExtVC", $scope.disableWACExtVC);
            if ($scope.disableWACExtVC == true) {
                console.log(' ini uncheck delete all');
                WOBP.getStatusVehicle().then(function (res) {
                    $scope.StatusVehicle = res.data.Result;
                });
                WOBP.getPointVehicle().then(function (res) {
                    $scope.PointsVehicle = res.data.Result;
                });
                $scope.disableWACExtVC == false;
            }
            if (!$scope.disableWACExtVC) {
                console.log(' ini check delete all');
                $scope.mVehicleCondition.Points = [];
                console.log('$scope.mVehicleCondition pas klik clear ===>', $scope.mVehicleCondition);

                // console.log("false check..");
                // console.log("mstId : ",mstId);
                // var chAreas = document.getElementsByClassName('choosen-area');
                // for (var i in chAreas) {
                //     console.log(chAreas[i]);
                //     if (typeof chAreas[i].classList !== 'undefined') {
                //         chAreas[i].classList.remove('choosen-area');
                //     }
                // }
                $('.choosen-area').removeClass('choosen-area');
                $scope.gridVC.data = [];
                dotsDataVC[mstIdVC] = [];
                var tmpIdxsWork = [];
                var tmpIdxsTime = [];

                for (var i in $scope.master) {
                    console.log("$scope.master",$scope.master);
                    // var tmpIdx = _.findIndex($scope.master[i], function (val) {
                    //     console.log('val gridWorkEstimate ====>', val);
                    //     if (val.isFromVc == 1) {
                    //         console.log('val gridWorkEstimate true ====>', val);
                    //         return val
                    //     }
                    // });
                    if($scope.master[i].isFromVc == 1){
                        tmpIdxsWork.push($scope.master[i]);
                    }else{
                        $scope.master[i].isFromVc = 0;
                    }
                    // console.log("=========> master", tmpIdx);
                }
                console.log("tmpIdxsWork",tmpIdxsWork);


                for (var j in $scope.gridEstimation.data) {
                    // var tmpIdx = _.findIndex($scope.gridEstimation.data[j], function (val) {
                    //     console.log('val gridEstimation ====>', val);
                    //     if (val.isFromVc == 1) {
                    //         return val
                    //     }
                    // });
                    if($scope.gridEstimation.data[j].isFromVc == 1){
                        tmpIdxsTime.push($scope.gridEstimation.data[j]);
                    }else{
                        $scope.gridEstimation.data[j].isFromVc = 0;
                    }
                    // console.log("=========> waktu", tmpIdx);
                }

                $scope.master = _.remove($scope.master, function(n){
                    return n.isFromVc !== 1;
                })

                // for (var x in tmpIdxsWork) {
                //     $scope.master.splice(tmpIdxsWork[x], 1);
                // }

                // untuk set job data
                dataTaskSave = $scope.master;

                for (var i=0; i<dataTaskSave.length; i++) {
                    if (dataTaskSave[i].FlatRate == null || dataTaskSave[i].FlatRate == undefined) {
                        dataTaskSave[i].FlatRate = 1
                    } else {
                        dataTaskSave[i].FlatRate = dataTaskSave[i].FlatRate
                    }

                    dataTaskSave[i].SubTotalTask = dataTaskSave[i].FlatRate * dataTaskSave[i].Fare
                }

                $scope.dataGoodWork = angular.copy(dataTaskSave);
                $scope.agGridWorkViewApi.setRowData($scope.master);
                // untuk set job data
                
                for (var y in tmpIdxsTime) {
                    $scope.gridEstimation.data.splice(tmpIdxsTime[y], 1);
                }
                $scope.coutnData();
                $scope.disableWACExtVC == true;
                // $scope.calculateforest();
            }
            $scope.disableWACExtVC = !$scope.disableWACExtVC;
        }

        $scope.checkTaskByVehicleCondtion = function (data, type, param) {
            console.log("checkTaskByVehicleCondtion", data, type, param);
            console.log("mEstimationBP",$scope.mEstimationBP);

            var tmpData = _.find($scope.StatusVehicle, { Name: 'Repair' });
            console.log("tmpdAta", tmpData);
            _.map(data, function (val) {
                if (val.StatusVehicleId !== tmpData.MasterId) {
                    val.PointId = 0;
                    val.typeName = 0;
                    val.showPoints = 0;
                    
                } else {
                    val.typeName = 1;
                    val.showPoints = 1;
                }
            });
            _.map(data, function (value) {
                console.log("value",value);
                if (value.PointId !== null && value.StatusVehicleId !== null && tmpData.MasterId && ($scope.mEstimationBP.VehicleTypeId !== null && $scope.mEstimationBP.VehicleTypeId !== undefined)
                ) {
                    // $scope.isNextTab.priceEstimation = false;
                    if (value.StatusVehicleId == tmpData.MasterId && (value.PointId == 0 || value.PointId == null)) {
                        console.log("No Push cuz validation is not completed", value);

                    } else {
                        var checkIdx = _.findIndex($scope.gridWorkEstimate, { "AreaId": value.ItemId });
                        var check = _.find($scope.gridWorkEstimate, { "AreaId": value.ItemId });
                        if (check !== undefined) {
                            var checkIdxTime = _.findIndex($scope.gridTimeEst.data, { "TaskBPId": check.TaskListBPId });
                            if (checkIdxTime > -1) {
                                $scope.gridTimeEst.data.splice(checkIdxTime, 1);
                            }
                        }
                        if (checkIdx > -1) {
                            $scope.gridWorkEstimate.splice(checkIdx, 1);
                        }
                        estimasiBPFactory.getVehicleConditionTask(value, $scope.mEstimationBP.VehicleTypeId).then(function(res) {
                            var tmpRes = res.data.Result;
                            var flagDude = Math.floor(Math.random() * 10000) + 1;
                            if (tmpRes.length > 0) {
                                _.map(tmpRes, function(val) {
                                    val.PutyEstimationMinute = val.PuttyEstimationMinute
                                    console.log('getVehicleConditionTask | val ====>',val);
                                    var tmpDiscountedTask = val.ServiceRate - ((val.ServiceRate * 0) / 100);
                                    val.DiscountedPrice = tmpDiscountedTask;
                                    val.Fare = val.ServiceRate;
                                    var fare = val.Fare
                                        // fare = fare.toString();
                                        // val.NormalPrice = fare.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                                    val.NormalPrice = fare;
                                    if($scope.mEstimationBP.isCash == 0){

                                        val.PaidBy = "Insurance";
                                        val.PaidById = 29;
                                    }else if($scope.mEstimationBP.isCash == 1){

                                        val.PaidBy = "Customer";  
                                        val.PaidById = 28;
                                        val.DiscountTypeId = -1;
                                    }
                                    val.ProcessId = $scope.checkTypeProcess(val.UOM);
                                    val.UnitBy = val.UOM;
                                    val.Discount = $scope.DiscountTask;
                                    val.isCustomDiscount = 0;
                                    val.isFromVc = 1;
                                    val.TaskBPId = val.TaskListBPId;
                                    val.idForUi = value.idForUi;
                                    val.index = "$$" + $scope.master.length + 1;
                                    val.valueTmp = value.ItemName;
                                    // ================ Validation ==
                                    var flagUomTime = _.findIndex($scope.gridEstimation.data, { valueTmp: tmpRes[0].valueTmp });
                                    console.log("flagUomTime",flagUomTime);
                                    var tmpIdxGridTimeEst = _.findIndex($scope.gridEstimation.data, { "TaskBPId": tmpRes[0].TaskListBPId });
                                    var tmpDataForGridTimeEstimate = _.find($scope.gridEstimation.data, { 'TaskName': tmpRes[0].TaskName });
                                    console.log("tmpIdxGridTimeEst", tmpIdxGridTimeEst, $scope.gridEstimation.data);
                                    if (tmpIdxGridTimeEst > -1) {
                                        console.log("gak usah di push datanya udh ada yg sama");
                                    } else {
                                        console.log("masuk sini jugaaaa??");
                                        // $scope.master.push(tmpRes[0]);
                                        if (tmpDataForGridTimeEstimate == undefined) {
                                            if(flagUomTime > -1){
                                                for(var i in $scope.gridEstimation.data){
                                                    if($scope.gridEstimation.data[i].valueTmp == val.valueTmp){
                                                        $scope.gridEstimation.data[i].TaskBPId = val.TaskListBPId;
                                                        $scope.gridEstimation.data[i].TaskName = val.TaskName;
                                                        $scope.gridEstimation.data[i].BodyEstimationMinute = val.BodyEstimationMinute ? val.BodyEstimationMinute : 0;
                                                        $scope.gridEstimation.data[i].PutyEstimationMinute = val.PutyEstimationMinute ? val.PutyEstimationMinute : 0;
                                                        $scope.gridEstimation.data[i].SurfacerEstimationMinute = val.SurfacerEstimationMinute ? val.SurfacerEstimationMinute : 0;
                                                        $scope.gridEstimation.data[i].SprayingEstimationMinute = val.SprayingEstimationMinute ? val.SprayingEstimationMinute : 0;
                                                        $scope.gridEstimation.data[i].PolishingEstimationMinute = val.PolishingEstimationMinute ? val.PolishingEstimationMinute : 0;
                                                        $scope.gridEstimation.data[i].ReassemblyEstimationMinute = val.ReassemblyEstimationMinute ? val.ReassemblyEstimationMinute : 0;
                                                        $scope.gridEstimation.data[i].FIEstimationMinute = val.FIEstimationMinute ? val.FIEstimationMinute : 0;
                                                    }
                                                }
                                            }else{
                                                $scope.gridEstimation.data.push({
                                                    "TaskBPId": val.TaskListBPId,
                                                    "TaskName": val.TaskName,
                                                    "BodyEstimationMinute": val.BodyEstimationMinute ? val.BodyEstimationMinute : 0,
                                                    "PutyEstimationMinute": val.PutyEstimationMinute ? val.PutyEstimationMinute : 0,
                                                    "SurfacerEstimationMinute": val.SurfacerEstimationMinute ? val.SurfacerEstimationMinute : 0,
                                                    // "Painting": val.PaintingEstimationMinute ? val.PaintingEstimationMinute : 0, //yg ada itu sebenarnya spraying, bukan painting, hoho
                                                    "SprayingEstimationMinute": val.SprayingEstimationMinute ? val.SprayingEstimationMinute : 0,
                                                    "PolishingEstimationMinute": val.PolishingEstimationMinute ? val.PolishingEstimationMinute : 0,
                                                    "ReassemblyEstimationMinute": val.ReassemblyEstimationMinute ? val.ReassemblyEstimationMinute : 0,
                                                    "FIEstimationMinute": val.FIEstimationMinute ? val.FIEstimationMinute : 0,
                                                    "isFromVc": 1,
                                                    "Flag" : flagDude,
                                                    "valueTmp" : val.valueTmp
                                                });
                                            }
                                        } else {
                                            var y = _.findIndex($scope.gridEstimation.data, { 'TaskBPId': tmpDataForGridTimeEstimate.TaskBPId });
                                            $scope.gridEstimation.data[y].TaskListBPId2 = val.TaskListBPId;
                                        }
                                    }
                                });
                                console.log("tmpRes[0]",tmpRes[0]);
                                var flagUom = _.findIndex($scope.master, { valueTmp: tmpRes[0].valueTmp });
                                console.log("flagUom",flagUom);
                                var tmpIdxGridWorkEstimate = _.findIndex($scope.master, { TaskListBPId: tmpRes[0].TaskListBPId });
                                var tmpDataForGridEstimate = _.find($scope.master, { TaskName: tmpRes[0].TaskName });
                                console.log("tmpDataForGridEstimate ===== >", tmpDataForGridEstimate,tmpIdxGridWorkEstimate);
                                if (tmpIdxGridWorkEstimate > -1) {
                                    console.log("gak usah di push datanya udh ada yg sama bawah");
                                } else {
                                    if (tmpDataForGridEstimate == undefined) {
                                        // tmpRes[0].Name = tmpRes[0].UOM;

                                        // tmpRes[0].No = i + 1;  
                                        tmpRes[0].satuan = tmpRes[0].UOM;
                                        tmpRes[0].isDeleted = 0;
                                        tmpRes[0].FlagID = flagDude; 
                                        tmpRes[0].totalParts = 0;
                                        // tmpRes[0].valueTmp = 
                                        tmpRes[0].AEstimationBP_PartList = [];
                                        if(flagUom > -1){
                                            for(var i in $scope.master){
                                                if($scope.master[i].valueTmp == tmpRes[0].valueTmp){
                                                    $scope.master[i] = tmpRes[0];
                                                }
                                            }
                                        }else{

                                             $scope.master.push(tmpRes[0]);   
                                        }
                                        // dataTaskSave.push(tmpRes[0]);
                                    } else {
                                        var x = _.findIndex($scope.master, { TaskListBPId: tmpDataForGridEstimate.TaskListBPId });
                                        $scope.master[x].idForUi2 = tmpRes[0].idForUi;
                                    }

                                }

                                if($scope.master.length > 0){

                                    for(var i = 0; i < $scope.master.length; i++){
                                        $scope.master[i].No = i + 1;  
                                    } 
                                } 
                                dataTaskSave = $scope.master;
                                for (var i=0; i<dataTaskSave.length; i++) {
                                    if (dataTaskSave[i].FlatRate == null || dataTaskSave[i].FlatRate == undefined) {
                                        dataTaskSave[i].FlatRate = 1
                                    } else {
                                        dataTaskSave[i].FlatRate = dataTaskSave[i].FlatRate
                                    }
                
                                    dataTaskSave[i].SubTotalTask = dataTaskSave[i].FlatRate * dataTaskSave[i].Fare
                                }
                                $scope.dataGoodWork = angular.copy(dataTaskSave);
                                if(dataTaskSave.length > 0){
                                    for(var i = 0; i < dataTaskSave.length; i++){
                                        dataTaskSave[i].No = i + 1;  
                                        // dataTaskSave[i].satuan = tmpRes[0].UOM; 
                                    } 
                                }
                                console.log("$scope.master",$scope.master);
                                $scope.agGridWorkViewApi.setRowData($scope.master);
                                console.log("resssss getVehicleConditionTask ====>>", tmpRes);
                            }
                            // _.map($scope.master,function(val){
                            //     if(val.isFromVc !== undefined && val.isFromVc == 1){

                            //     }
                            // })
                            $scope.coutnData();
                        })
                    }
                } 
            })
            $scope.gridVC.data = data;

            $scope.mVehicleCondition.Points = [];
            for (var i = 0; i < data.length; i++) {
                    console.log('ini masuk kalo dalemnya ada pointsnya');
                    // data[i].areaid   = data[i].Points.areaid;
                    // data[i].cssClass = data[i].Points.cssClass;
                    // data[i].height   = data[i].Points.height;
                    // data[i].name     = data[i].Points.name;
                    // data[i].width    = data[i].Points.width;
                    // data[i].x        = data[i].Points.x;
                    // data[i].y        = data[i].Points.y;
                    // data[i].z        = data[i].Points.z;     
                data[i].Points.status = data[i].StatusVehicleId;
                data[i].Points.PointId = data[i].PointId;
                $scope.mVehicleCondition.Points.push(data[i].Points);
            }


            console.log('$scope.mVehicleCondition [mVehicleCondition] ===>',angular.copy($scope.mVehicleCondition));
            $scope.mVehicleCondition.Points = JSON.stringify($scope.mVehicleCondition.Points);

            console.log('ini grid bawah cuy ===>', data);

            // $scope.sumAllPrice();
        };


        // ini gatau nih kepake atau engga
        $scope.dataForVc = function () {
            if ($scope.gridWorkEstimate.length > 0 && $scope.mData.IsEstimation == 1) {
                console.log("$scope.areasArrayVC", $scope.areasArrayVC);
                console.log("dotsDataVC[mstIdVC]", dotsDataVC);
                console.log("$scope.gridWorkEstimate", $scope.gridWorkEstimate);
                _.map($scope.VTypeData, function (val) {
                    if ($scope.mData.VehicleConditionTypeId == val.MasterId) {
                        $scope.filter.vTypeVC = val;
                    }
                });
                $scope.chooseVTypeVC($scope.mData.VehicleConditionTypeId);
                mstIdVC = $scope.mData.VehicleConditionTypeId;
                $timeout(function () {
                    for (var i in $scope.gridWorkEstimate) {
                        var addtemp = _.find($scope.areasArrayVC, { "areaid": $scope.gridWorkEstimate[i].AreaId });
                        var addtempx = _.find($scope.areasArrayVC, { "name": addtemp.name, "x": addtemp.x, "y": addtemp.y });
                        var tmpData = _.find($scope.StatusVehicle, { Name: 'Repair' });
                        var tmpTypeName = "";
                        console.log("tmpdAta", tmpData);
                        if ($scope.gridWorkEstimate[i].StatusVehicleId !== tmpData.MasterId) {
                            tmpTypeName = 0
                        } else {
                            tmpTypeName = 1;
                        }
                        dotsDataVC[mstIdVC].push({
                            "ItemId": addtempx.areaid,
                            "idForUi": addtemp.areaid,
                            "ItemName": addtempx.name,
                            "Points": addtempx,
                            "StatusVehicleId": $scope.gridWorkEstimate[i].StatusVehicleId,
                            "PointId": $scope.gridWorkEstimate[i].PointId,
                            "typeName": tmpTypeName
                        });
                        _.map($scope.areasArrayVC, function (val) {
                            console.log("areaaaa ==== >", val.areaid, "====", $scope.gridWorkEstimate[i].AreaId);
                            if (val.areaid == $scope.gridWorkEstimate[i].AreaId) {
                                console.log("areaaaa", val.areaid, val);
                                val.cssClass = "choosen-area";
                            }

                        });
                    }
                    setDataGridVC(dotsDataVC[mstIdVC]);

                }, 500);


            }
        }

        // $scope.areas = [];
        // $scope.areas.Points = [];
        WOBP.getAllTypePoints().then(
            function (res) {
                console.log('TypePoints ===>',res.data.Result);
                $scope.imageWACArr = res.data.Result;
                return res.data;
            },
            function (err) {
                console.log("err=>", err);
            }
        );


        $scope.doClickVC = function (ev, boxId, areas, area, stat, selection) {
            console.log("false");
            console.log("$scope.estimationMode",$scope.estimationMode);
            if($scope.estimationMode == "View"){
                console.log("ini viewe ga bisa diedit lah");
            }else{

                if (!$scope.disableWACExtVC) {
                    if (selection.hasClass("choosen-area")) {
                        selection.removeClass("choosen-area");
                        console.log("dotsDataVC[mstIdVC] =====>", dotsDataVC[mstIdVC]);
                        console.log("area.areaid =====>", area);
                        var tmpGridWorkEstimate = angular.copy(_.find($scope.master, { idForUi: area.areaid }));
                        var tmpReturn = true;
                        console.log('======= UNSELECTED ======');
                        console.log("ev : ", ev);
                        console.log("boxId : ", boxId);
                        console.log("areas : ", areas);
                        console.log("area : ", area);
                        console.log("stat : ", stat);
                        console.log("selection : ", selection);
                        console.log("dotsDataVC[mstIdVC] : ", dotsDataVC[mstIdVC]);
                        console.log("$scope.areasArrayVC : ", $scope.areasArrayVC);
                        
                        var tmp = _.find(dotsDataVC[mstIdVC], { "ItemId": area.areaid });
                        console.log("tmp", tmp, area, dotsDataVC[mstIdVC]);
                        if (_.find(dotsDataVC[mstIdVC], { "idForUi": area.areaid }) !== undefined) {
                            console.log("find false");
                            dotsDataVC[mstIdVC].splice(_.findIndex(dotsDataVC[mstIdVC], { "idForUi": area.areaid }), 1);
                        } else if (_.find(dotsDataVC[mstIdVC], { "ItemName": area.name }) !== undefined) {
                            dotsDataVC[mstIdVC].splice(_.findIndex(dotsDataVC[mstIdVC], { "ItemName": area.name }), 1);
                        }
                        console.log("tmpIdxGridWorkEstimate index di pilih", tmpGridWorkEstimate);
                        var filtered = $scope.master.filter(function(item) { 
                                   return item.FlagID !== tmpGridWorkEstimate.FlagID;  
                        });
                        console.log("data yang tersisa",filtered);
                        for(var a = 0; a < filtered.length; a++){
                            filtered[a].No = a + 1;
                        }
                        $scope.master = filtered;
                        dataTaskSave = $scope.master;
                        for (var i=0; i<dataTaskSave.length; i++) {
                            if (dataTaskSave[i].FlatRate == null || dataTaskSave[i].FlatRate == undefined) {
                                dataTaskSave[i].FlatRate = 1
                            } else {
                                dataTaskSave[i].FlatRate = dataTaskSave[i].FlatRate
                            }
        
                            dataTaskSave[i].SubTotalTask = dataTaskSave[i].FlatRate * dataTaskSave[i].Fare
                        }
                        $scope.dataGoodWork = angular.copy(dataTaskSave);
                        $scope.agGridWorkViewApi.setRowData($scope.master);
                        
                        var filteredGridTime = $scope.gridEstimation.data.filter(function(item) { 
                                   return item.Flag !== tmpGridWorkEstimate.FlagID;  
                        });
                        console.log("data time tersisa",filteredGridTime);
                        $scope.gridEstimation.data = filteredGridTime;
                        $scope.coutnData();
                        // $scope.isNextTab.priceEstimation = false;
                    } else {
                        selection.addClass("choosen-area");
                        console.log('======= SELECTED ======');
                        console.log("ev : ", ev);
                        console.log("boxId : ", boxId);
                        console.log("areas : ", areas);
                        console.log("area : ", area);
                        console.log("stat : ", stat);
                        console.log("selection : ", selection);
                        console.log("dotsDataVC[mstIdVC] : ", dotsDataVC[mstIdVC]);
                        console.log("$scope.areasArrayVC : ", $scope.areasArrayVC);
   
                        if (_.find(dotsDataVC[mstIdVC], { "idForUi": area.areaid }) == undefined) {
                            if (_.find(areas, { "areaid": area.areaid }) !== undefined) {
                                var addtemp = _.find(areas, { "areaid": area.areaid });
                                if (_.find($scope.areasArrayVC, { "name": addtemp.name, "x": addtemp.x, "y": addtemp.y }) !== undefined) {
                                    var addtempx = _.find($scope.areasArrayVC, { "name": addtemp.name, "x": addtemp.x, "y": addtemp.y });
   
                                    dotsDataVC[mstIdVC].push({
                                        "ItemId": addtempx.areaid,
                                        "idForUi": area.areaid,
                                        "ItemName": addtempx.name,
                                        "Points": addtempx,
                                        "StatusVehicleId": null,
                                        "PointId": null
                                    });
                                }
                            }
                        }
                    }
                    setDataGridVC(dotsDataVC[mstIdVC]);
   
   
                    console.log(' $scope.mVehicleCondition final do click ===>', $scope.mVehicleCondition.Selection)   
                }

                
            }
        }


        $scope.checkTypeProcess = function (data) {
            console.log("dataaaa", data);
            var tmpResult = _.find($scope.unitData, { 'Name': data });
            console.log("tmpResult", tmpResult);
            if (tmpResult !== undefined) {
                return tmpResult.MasterId;
            }
        };


        $scope.chgChk = function () {
            if (!$scope.disableWACExt) {
                $('.choosen-area').removeClass('choosen-area');
                $scope.grid.data = [];
                dotsData[mstId] = [];
            }
            $scope.disableWACExt = !$scope.disableWACExt;
        }


        $scope.cekEstimasi = function () {
            console.log('mEstimationBP ====>', $scope.mEstimationBP);
            console.log('mSPK ===>', $scope.mSPK); 


            var numLength = angular.element('.ui.modal.ModalEstimasiYangBelumDigunakan').length;
            setTimeout(function () {
                angular.element('.ui.modal.ModalEstimasiYangBelumDigunakan').modal('refresh');
            }, 0);

            if (numLength > 1) {
                angular.element('.ui.modal.ModalEstimasiYangBelumDigunakan').not(':first').remove();
            }

            angular.element(".ui.modal.ModalEstimasiYangBelumDigunakan").modal("show");
            //angular.element('.ui.modal.PengurusanJasaDokumenSO').modal('show');

        }

        $scope.btnBatalCekEstimasi = function () {
            angular.element('.ui.modal.ModalEstimasiYangBelumDigunakan').modal('hide');
        }

        $scope.btnInputSPK = function () {


            
              
            var numLength = angular.element('.ui.modal.ModalInputSPK').length;
            setTimeout(function () {
                angular.element('.ui.modal.ModalInputSPK').modal('refresh');
            }, 0);

            if (numLength > 1) {
                angular.element('.ui.modal.ModalInputSPK').not(':first').remove();
            }

            angular.element(".ui.modal.ModalInputSPK").modal("show");
            //angular.element('.ui.modal.PengurusanJasaDokumenSO').modal('show');

        }

        $scope.btnBatalSPK = function () {
            angular.element('.ui.modal.ModalInputSPK').modal('hide');
        }

        $scope.mEstimationBP.IsSpk = 0;
        $scope.dariParent = {};
        $scope.onNomorSpkUtamaSelected = function (selected) {
            console.log('selected nomor spk utama ===>',selected);
            $scope.dariParent.EstSPKListId = selected.EstSPKListId;
            $scope.dariParent.NoPolis = selected.NoPolis;
            $scope.dariParent.NoLK = selected.NoLK;
            $scope.dariParent.PeriodePolis = selected.PeriodePolis;
            $scope.dariParent.OR = selected.OR;
            $scope.dariParent.SpkNo = selected.SpkNo;

            if ($scope.mSPK.SpkCategory == 1) { // spk tambahan
                console.log('ini copy dari SPK utama ===>',$scope.mSPK);
                $scope.mSPK.NoPolis      = selected.NoPolis;
                $scope.mSPK.NoLK         = selected.NoLK;
                $scope.mSPK.PeriodePolis = selected.PeriodePolis;
                $scope.mSPK.OR           = selected.OR;
            }
        }

        $scope.isInputForm = false;
        $scope.spkMode = "create";
        $scope.switchTambahSPK = function () {
            if ($scope.isInputForm == false) {
                $scope.isInputForm = true;
                $scope.mSPK = {};
                $scope.onTipeSpkChange(1);
            } else {
                $scope.isInputForm = false;
            }
        }

        $scope.gridSpkDeleteAction = function (params) {
            if (params.EstSPKListIdParent == null && params.isNew == true) {//delete spk utama buatan
                $scope.bsAlertConfirm('Hapus Data SPK Utama', 'Apakah anda yakin ingin menghapus data spk ini?').then(function (res) {
                    if (res) {
                        for (var i = 0; i < $scope.optionsSpkList.length; i++) {
                            if ($scope.optionsSpkList[i].EstSPKListId == params.EstSPKListId) {
                                $scope.optionsSpkList.splice(i, 1);
                                // $scope.optionsSpkList[i].StatusCode = 0;
                            }
                        }
                        console.log('ini spk utama buatan ===>', $scope.optionsSpkList);
                        $scope.setRowData_spkList($scope.optionsSpkList);
                    } else {
                        console.log("ini kalo klik cancle", res);
                    }
                });

            } else if (params.EstSPKListIdParent == null && params.OutletId != null) {//delete spk utama dari backend
                // $scope.displaySpkList = angular.copy($scope.optionsSpkList);
                $scope.bsAlertConfirm('Hapus Data SPK Utama', 'Apakah anda yakin ingin menghapus data spk ini?').then(function (res) {
                    if (res) {
                        for (var i = 0; i < $scope.optionsSpkList.length; i++) {
                            if ($scope.optionsSpkList[i].EstSPKListId == params.EstSPKListId) {
                                $scope.optionsSpkList[i].StatusCode = 0;
                            }
                        }
                        console.log('ini spk utama backend ===>', $scope.optionsSpkList);
                        $scope.setRowData_spkList($scope.optionsSpkList);

                    } else {
                        console.log("ini kalo klik cancle", res);
                    }
                });
            } else if (params.EstSPKListIdParent != null && params.isNew == true) {//delete spk tambahan buatan
                $scope.bsAlertConfirm('Hapus Data SPK Tambahan', 'Apakah anda yakin ingin menghapus data spk ini?').then(function (res) {
                    if (res) {
                        for (var i = 0; i < $scope.optionsSpkList.length; i++) {
                            if ($scope.optionsSpkList[i].EstSPKListId == params.EstSPKListIdParent) {

                                console.log('cari bapak nya anak anak ===>', $scope.optionsSpkList[i])
                                for (var x = 0; x < $scope.optionsSpkList[i].ListAEstimationSPKChild.length; x++) {
                                    if ($scope.optionsSpkList[i].ListAEstimationSPKChild[x].EstSPKListId == params.EstSPKListId) {
                                        console.log('ini anaknya bukan si? ===>', $scope.optionsSpkList[i].ListAEstimationSPKChild[x]);
                                        $scope.optionsSpkList[i].ListAEstimationSPKChild.splice(x, 1);
                                        // $scope.optionsSpkList[i].ListAEstimationSPKChild[x].StatusCode = 0
                                    }
                                }
                            }
                        }
                        console.log('ini spk tambahan buatan ===>', $scope.optionsSpkList);
                        $scope.setRowData_spkList($scope.optionsSpkList);
                    } else {
                        console.log("ini kalo klik cancle", res);
                    }
                });
            } else if (params.EstSPKListIdParent != null && params.OutletId != null) { //delete spk tambahan dari backend
                $scope.bsAlertConfirm('Hapus Data SPK Tambahan', 'Apakah anda yakin ingin menghapus data spk ini?').then(function (res) {
                    if (res) {
                        for (var i = 0; i < $scope.optionsSpkList.length; i++) {
                            if ($scope.optionsSpkList[i].EstSPKListId == params.EstSPKListIdParent) {
                                console.log('cari bapak nya anak anak ===>', $scope.optionsSpkList[i])
                                for (var x = 0; x < $scope.optionsSpkList[i].ListAEstimationSPKChild.length; x++) {
                                    if ($scope.optionsSpkList[i].ListAEstimationSPKChild[x].EstSPKListId == params.EstSPKListId) {
                                        console.log('ini anaknya bukan si? ===>', $scope.optionsSpkList[i].ListAEstimationSPKChild[x]);
                                        $scope.optionsSpkList[i].ListAEstimationSPKChild[x].StatusCode = 0;
                                    }
                                }
                            }
                        }
                        console.log('ini spk tamban backend ===>', $scope.optionsSpkList);
                        $scope.setRowData_spkList($scope.optionsSpkList);
                    } else {
                        console.log("ini kalo klik cancle", res);
                    }
                });
            }
        }



        

        $scope.gridSpkViewAction = function (params) {
            console.log('selected view ===>', params);
            $scope.isInputForm = true;
            $scope.mSPK = params;
            $scope.spkMode = "view";
            if (params.EstSPKListIdParent != null) {
                $scope.mSPK.SpkCategory = 1;
            }else{
                $scope.mSPK.SpkCategory = 0;
            }
        }

        $scope.GetDataSPK = function (){
            estimasiBPFactory.getDataSpk().then(
                function (res) {
    
                    console.log('data spk sebelum dirapihin ===> ', res.data.Result);
                    
                    $scope.optionsSpkList = res.data.Result;
    
                    for (var i = 0; i < $scope.optionsSpkList.length; i++) {
                        if ($scope.optionsSpkList[i].StatusCode == 1 || $scope.optionsSpkList[i].StatusCode == true){
                            $scope.optionsSpkList[i].StatusCode = 1;
                        }else{
                            $scope.optionsSpkList[i].StatusCode = 0;
                        }
    
                        if ($scope.optionsSpkList[i].EstSPKListIdParent != null) {
                            if ($scope.optionsSpkList[i].EstSPKListIdParent == $scope.optionsSpkList[i].EstSPKListId) {
                                $scope.optionsSpkList[i].ListAEstimationSPKChild.push($scope.optionsSpkList[i]);
                            }
                            $scope.optionsSpkList.splice(i,1);
                        }
                    }
                    $scope.agSpkListGalangApi.setRowData($scope.optionsSpkList);
                    console.log('spk dari backend optionsSpkList sudah dirapihin ===>', $scope.optionsSpkList);
                },
                function (err) {
                        console.log("gagal ambil data spk list by backend", err);
                    }
                );
    
            $scope.onSelectCategorySPK = function (selectedSPK) {
                console.log('selected SPK ===>', selectedSPK);
    
                if (selectedSPK.SpkCategory != 1 || selectedSPK.SpkCategory == undefined) {
                    $scope.isSpkExt = false;
                }else{
                    $scope.isSpkExt = true;
                }
            }
        }
        
        
        $scope.SpkCategory = 1;
        $scope.optionsSpkCategory = [
            {
                SpkCategory: 0,
                SpkCategoryName: "SPK Utama"
            },
            {
                SpkCategory: 1,
                SpkCategoryName: "SPK Tambahan"
            }
        ];



        $scope.setRowData_spkList = function (agGridSpkList) {
            $scope.optionsSpkList = angular.copy(agGridSpkList);
            console.log('ini yang mau masuk ke AG grid djanzukk ===>', $scope.optionsSpkList);
            for (var i = 0; i < agGridSpkList.length; i++) {
                if (agGridSpkList[i].ListAEstimationSPKChild != []) {
                    for (var x = 0; x < agGridSpkList[i].ListAEstimationSPKChild.length; x++) {
                        if (agGridSpkList[i].ListAEstimationSPKChild[x].StatusCode == 0 || agGridSpkList[i].ListAEstimationSPKChild[x].StatusCode == false){
                            agGridSpkList[i].ListAEstimationSPKChild.splice(x,1);
                            // agGridSpkList[i].ListAEstimationSPKChild[i].StatusCode = "jancukkk tambahan"
                        }
                    }
                }else{
                    $scope.agSpkListGalangApi.setRowData([]);
                }

                if (agGridSpkList[i].StatusCode == 0 || agGridSpkList[i].StatusCode == false) {
                    agGridSpkList.splice(i,1);
                    // agGridSpkList[i].StatusCode = "Jancukkk utama"
                }
            }
            $scope.agSpkListGalangApi.setRowData(agGridSpkList);
            console.log('ini grid yang ke filter ===>', agGridSpkList)
        }

        $scope.newSPK = function () {
            $scope.spkMode = "new";
        }

        $scope.gridSpkEditAction = function (params) {
            $scope.isInputForm = true;
            $scope.mSPK = params;
            $scope.spkMode = "edit";

            if (params.EstSPKListIdParent != null) {
                $scope.mSPK.SpkCategory = 1;
                $scope.jenisSPK = "spk_tambahan";
                console.log('spk_tambahan ===>', params);
            } else {
                $scope.mSPK.SpkCategory = 0;
                $scope.jenisSPK = "spk_utama";
                console.log('spk_utama ===>', params);

            }
        }



        $scope.spkListNewAdded =[];
        $scope.mSPK = {};
        $scope.onSpkSimpanClick = function () {
            console.log ('$scope.optionsSpkList ===>',$scope.optionsSpkList);

            if ($scope.spkMode == "new") { //buat SPK 
                var idBoongan = 990;
                if ($scope.mSPK.SpkCategory == 0) { //spk utama dari fe
                    $scope.mSPK.EstSPKListIdParent = null;
                    if ($scope.optionsSpkList.length > 0){
                        for (var i = 0; i < $scope.optionsSpkList.length; i++) {
                            idBoongan++;
                        }
                    }else{
                        idBoongan--;
                    }
                    $scope.mSPK.EstSPKListId = idBoongan;
                    $scope.mSPK.isNew = true;
                    $scope.mSPK.StatusCode = 1;
                    $scope.mSPK.PeriodePolis = $scope.fixDate($scope.mSPK.PeriodePolis);
                    $scope.mSPK.ListAEstimationSPKChild = [];
                    $scope.spkListNewAdded.push($scope.mSPK);
                    $scope.optionsSpkList.push($scope.mSPK);
                    $scope.setRowData_spkList($scope.optionsSpkList);
                    console.log('ini masuk ke SPK Utama ===>', $scope.spkListNewAdded);
                    $scope.switchTambahSPK();
                } else {// spk tambahan dari fe
                    for (var i = 0; i < $scope.optionsSpkList.length; i++) {
                        if ($scope.optionsSpkList[i].EstSPKListId == $scope.dariParent.EstSPKListId) {
                            $scope.mSPK.EstSPKListIdParent = $scope.dariParent.EstSPKListId;
                            $scope.mSPK.NoPolis = $scope.dariParent.NoPolis;
                            $scope.mSPK.StatusCode = 1;
                            $scope.mSPK.NoLK = $scope.dariParent.NoLK;
                            $scope.mSPK.PeriodePolis = $scope.dariParent.PeriodePolis;
                            $scope.mSPK.OR = $scope.dariParent.OR;
                            $scope.mSPK.isNew = true;
                            var id_anakan = 1;

                            if ($scope.optionsSpkList[i].ListAEstimationSPKChild != []) {
                                for (var x = 0; x < $scope.optionsSpkList[i].ListAEstimationSPKChild.length; x++) {
                                    id_anakan = $scope.optionsSpkList[i].ListAEstimationSPKChild.length;
                                    id_anakan++;
                                }
                            } else {
                                id_anakan++;
                            }
                            $scope.mSPK.EstSPKListId = parseInt($scope.dariParent.EstSPKListId.toString() + "27" + id_anakan);
                            $scope.optionsSpkList[i].ListAEstimationSPKChild.push($scope.mSPK);
                            $scope.setRowData_spkList($scope.optionsSpkList);

                            console.log('ini masuk ke SPK tmbahan ===>', $scope.spkListNewAdded);
                            $scope.switchTambahSPK();
                            
                        } else {
                            console.log('EstSPKListId tidak sama dengan SPK no yang dipilih ===>', $scope.dariParent.EstSPKListId);
                        }
                    }


                }
            }else{ //edit SPK
                if ($scope.jenisSPK == "spk_utama") {
                    console.log('edit spk utama ===>', $scope.mSPK);
                    for (var i = 0; i < $scope.optionsSpkList.length; i++) {
                        if ($scope.optionsSpkList[i].EstSPKListId == $scope.mSPK.EstSPKListId) {
                            // $scope.optionsSpkList[i] = $scope.mSPK;

                            $scope.mSPK.PeriodePolis = $scope.fixDate($scope.mSPK.PeriodePolis);
                            $scope.optionsSpkList[i] = $scope.mSPK;



                            if ($scope.optionsSpkList[i].ListAEstimationSPKChild != []) {
                                for (var x = 0; x < $scope.optionsSpkList[i].ListAEstimationSPKChild.length; x++) {
                                    $scope.optionsSpkList[i].ListAEstimationSPKChild[x].PeriodePolis = angular.copy($scope.mSPK.PeriodePolis);
                                    $scope.optionsSpkList[i].ListAEstimationSPKChild[x].NoPolis      = angular.copy($scope.mSPK.NoPolis);
                                    $scope.optionsSpkList[i].ListAEstimationSPKChild[x].NoLK         = angular.copy($scope.mSPK.NoLK);
                                    $scope.optionsSpkList[i].ListAEstimationSPKChild[x].OR           = angular.copy($scope.mSPK.OR);
                                }
                            }
                        }
                        
                    }
                    $scope.setRowData_spkList($scope.optionsSpkList);
                    $scope.switchTambahSPK();
                } else if ($scope.jenisSPK == "spk_tambahan"){
                    console.log('edit spk tambahan ===>', $scope.mSPK);
                    for (var i = 0; i < $scope.optionsSpkList.length; i++) {
                        for (var x = 0; x < $scope.optionsSpkList[i].ListAEstimationSPKChild.length; x++) {
                            if ($scope.optionsSpkList[i].ListAEstimationSPKChild[x].EstSPKListId == $scope.mSPK.EstSPKListId) {
                                // $scope.optionsSpkList[i].ListAEstimationSPKChild[x] == $scope.mSPK;

                                $scope.mSPK.PeriodePolis = $scope.fixDate($scope.mSPK.PeriodePolis);
                                $scope.optionsSpkList[i].ListAEstimationSPKChild[x] = $scope.mSPK;
                            }
                        }
                    }
                    $scope.setRowData_spkList($scope.optionsSpkList);
                    $scope.switchTambahSPK();
                    $scope.setRowData_spkList($scope.optionsSpkList);


                }
            }



        }



        $scope.onTipeSpkChange = function (selectedCategory) {
            console.log('apa ini ===>', selectedCategory);
        }


        $scope.getDataInsuranceCB = function () {
            AppointmentBpService.getInsurence().then(function(res) {
                console.log("res getInsurence", res.data);
                $scope.loading = false;
                $scope.optionsLihatInformasi_NamaInsurance_Search = res.data;
                console.log("$scope.optionsLihatInformasi_NamaInsurance_Search", $scope.optionsLihatInformasi_NamaInsurance_Search);
            });
            // estimasiBPFactory.getInsuranceComboBox()
            //     .then
            //     (
            //         function (res) {
            //             $scope.loading = false;
            //             $scope.optionsLihatInformasi_NamaInsurance_Search = res.data.Result;
            //             console.log('optionsLihatInformasi_NamaInsurance_Search ===>',$scope.optionsLihatInformasi_NamaInsurance_Search);
            //         },
            //         function (err) {
            //             console.log("err=>", err);
            //         }
            //     );
        };

        var selectedInsurance;
        $scope.onInsuranceSelected = function (data) {
            console.log('selectedInsurance ===>', data)
            if(data !== undefined){
                selectedInsurance = data;
                $scope.DiscountParts = data.DiscountPart;
                $scope.disParts = data.DiscountPart;
                $scope.DiscountTask = data.DiscountJasa;
                $scope.disTask = data.DiscountJasa;
                $scope.DisplayDisParts =  data.DiscountPart;
                $scope.DisplayDisTask  = data.DiscountJasa;



                if($scope.dataGoodWork.length > 0){
                    for(var i in $scope.dataGoodWork){
                        if ($scope.dataGoodWork[i].PaidById != 30) {
                            var isurance = 29;
                            $scope.selectTypeCust(isurance.toString());
                            $scope.selectDiscount($scope.dataGoodWork[i].DiscountTypeId);
                            $scope.dataGoodWork[i].PaidById = 29; //customerId
                            $scope.dataGoodWork[i].PaidBy = "Insurance"
                            delete $scope.dataGoodWork[i].DiscountTypeId;
    
                            $scope.mEstimationBP.DiskonPersen = $scope.DiscountTask;
                            $scope.dataGoodWork[i].Discount = $scope.DiscountTask;
                            if($scope.dataGoodWork[i].AEstimationBP_PartList.length > 0){
                                for(var x in $scope.dataGoodWork[i].AEstimationBP_PartList){
                                    $scope.dataGoodWork[i].AEstimationBP_PartList[x].Discount = $scope.DiscountParts;
                                    $scope.dataGoodWork[i].AEstimationBP_PartList[x].PaidById = 29;
                                    $scope.dataGoodWork[i].AEstimationBP_PartList[x].PaidBy = "Insurance"
                                }
                            }
                            $scope.gridClickEditHandler($scope.dataGoodWork[i],3);
                            $scope.saveFin(false);
                        }
                        
                    }
                }

                console.log('jejeran diskon parts | task ==>',$scope.DisplayDisParts ,$scope.DisplayDisTask)

                // if(jejeranDiskonParts.length == 0 ){
                //     $scope.DisplayDisParts = data.DiscountPart;
                // }
                // if(jejeranDiskonTask.length == 0){
                //     $scope.DisplayDisTask = data.DiscountJasa;
                // }

            }else{  
                console.log('data asuransi undefined');
                $scope.DiscountParts = 0;
                $scope.disParts = 0;
                $scope.DiscountTask = 0;
                $scope.disTask = 0;
                if(jejeranDiskonParts.length == 0 ){
                    $scope.DisplayDisParts = 0;
                }
                if(jejeranDiskonTask.length == 0){
                    $scope.DisplayDisTask = 0;
                }
            }
        }


        $scope.selectTypeCustFromPlus = function(selected){
            console.log('selectTypeCustFromPlus ===>',selected);
            $scope.mEstimationBP.PaidById = selected.MasterId;
            $scope.temp_paidJob = angular.copy($scope.mEstimationBP.PaidById)
        }

        $scope.selectTypeCust = function(id, action_from){
            console.log('selectTypeCust ===>',id);
            $scope.mEstimationBP.PaidById = id;

            if (action_from == 'manual') {
                if ($scope.mode_popup_job == 'new') {
                    if ($scope.temp_paidJob == 30 && id != 30) {
                        // merubah dr warranty ke bukan warranty
                        if ($scope.mEstimationBP.PaidById == 30) {
                            $scope.jobwarranty = true
                            $scope.selectDiscount("-1")
                        } else {
                            $scope.jobwarranty = false
                        }
            
                        $scope.temp_paidJob = angular.copy($scope.mEstimationBP.PaidById)
                        $scope.masterParts = [];
                        $scope.mEstimationBP.TipeDisc = 0;
                        $scope.mEstimationBP.TaskName = null;
                        // $scope.mEstimationBP.ProcessId = null;
                        $scope.mEstimationBP.NoSpk = null;
                        $scope.mEstimationBP.Fare = null;
                        $scope.mEstimationBP.TFirst1 = null;
                        $scope.mEstimationBP.FlatRate = null;

                        $scope.mEstimationBP.BodyEstimationMinute = 0
                        $scope.mEstimationBP.PutyEstimationMinute = 0
                        $scope.mEstimationBP.SurfacerEstimationMinute = 0
                        $scope.mEstimationBP.SprayingEstimationMinute = 0
                        $scope.mEstimationBP.PolishingEstimationMinute = 0
                        $scope.mEstimationBP.ReassemblyEstimationMinute = 0
                        $scope.mEstimationBP.FIEstimationMinute = 0

                        taskBPIdFromMaster = null
                        

                    } else  if ($scope.temp_paidJob != 30 && id == 30) {
                        // merubah dr bukan warranty ke warranty
                        if ($scope.mEstimationBP.PaidById == 30) {
                            $scope.jobwarranty = true
                            $scope.selectDiscount("-1")
                        } else {
                            $scope.jobwarranty = false
                        }
            
                        $scope.temp_paidJob = angular.copy($scope.mEstimationBP.PaidById)
                        $scope.masterParts = [];
                        $scope.mEstimationBP.TipeDisc = 0;
                        $scope.mEstimationBP.TaskName = null;
                        // $scope.mEstimationBP.ProcessId = null;
                        $scope.mEstimationBP.NoSpk = null;
                        $scope.mEstimationBP.Fare = null;
                        $scope.mEstimationBP.TFirst1 = null;
                        $scope.mEstimationBP.FlatRate = null;

                        $scope.mEstimationBP.BodyEstimationMinute = 0
                        $scope.mEstimationBP.PutyEstimationMinute = 0
                        $scope.mEstimationBP.SurfacerEstimationMinute = 0
                        $scope.mEstimationBP.SprayingEstimationMinute = 0
                        $scope.mEstimationBP.PolishingEstimationMinute = 0
                        $scope.mEstimationBP.ReassemblyEstimationMinute = 0
                        $scope.mEstimationBP.FIEstimationMinute = 0

                        taskBPIdFromMaster = null

                    }
    
                } else {
                    if ($scope.temp_paidJob == 30 && id != 30) {
                        // merubah dr warranty ke bukan warranty
                        $scope.mEstimationBP.PaidById = '30'
                        $scope.temp_paidJob = angular.copy($scope.mEstimationBP.PaidById)
                        bsAlert.warning('Tidak dapat mengganti pembayaran job warranty ke non-warranty, silahkan hapus & input baru.')
                        
                        
                        return
    
                    } else  if ($scope.temp_paidJob != 30 && id == 30) {
                        // merubah dr bukan warranty ke warranty
                        $scope.mEstimationBP.PaidById = angular.copy($scope.temp_paidJob.toString())
                        bsAlert.warning('Tidak dapat mengganti pembayaran job non-warranty ke warranty, silahkan hapus & input baru.')
                       
                        return
                    }
    
                }
            }
            


            if ($scope.mEstimationBP.PaidById == 30) {
                $scope.jobwarranty = true
                $scope.selectDiscount("-1")
            } else {
                $scope.jobwarranty = false
            }

            $scope.temp_paidJob = angular.copy($scope.mEstimationBP.PaidById)
        }

        $scope.selectDiscount = function(data){
            console.log("data",data);
            $scope.mEstimationBP.TipeDisc =data;
            $scope.mEstimationBP.DiskonPersen = 0;
            if(data == 0){
                $scope.disabledDisc = false;
            }else if(data == -1){
                $scope.disabledDisc = true;
                lastDiscountParts = 0;
            }
        }

        $scope.selectSatuan = function(data){
            $scope.mEstimationBP.ProcessId = data;
            console.log('selected Satuan  | mEstimationBP.ProcessId ===> ',$scope.mEstimationBP.ProcessId);
        }

        

        $scope.getVehicModeldariWO = function(){
            // WOBP.getVehicleMList().then(function (res) {
            //     $scope.optionsModel = res.data.Result;
            //     console.log('$scope.optionsModel ==>', $scope.optionsModel)
            //     return $scope.optionsModel;
            // });

            // MrsList.getDataModel().then(function(res) {
            estimasiBPFactory.getVehicleMList().then(function(res) {
                console.log("result : ", res.data.Result);
                $scope.optionsModel = res.data.Result;
            });
        }

        


        // var testdong = 0;
        $scope.dataGoodWork = [];
        $scope.onSelectModelChanged = function (ModelSelected) {
            console.log('on model change ===>', ModelSelected);
            if (ModelSelected == undefined || ModelSelected == null) {
                $scope.mEstimationBP.VehicleModelName = "";
                $scope.mEstimationBP.VehicleModelId   = "";
                $scope.mEstimationBP.VehicleTypeName  = "";
                $scope.mEstimationBP.VehicleTypeId    = "";
                $scope.mEstimationBP.KatashikiCode    = "";
                $scope.mEstimationBP.ModelType        = "";
                $scope.mEstimationBP.ModelCode        = "";
                $scope.optionsTipe                    = [];
                $scope.mEstimationBP.KatashikiCodex = '';
                $scope.optionsKatashiki = [];
                $scope.mEstimationBP.ColorName = "";




            } else {
                console.log('dataTaskSave ===>',dataTaskSave);
                console.log('$scope.master ===>',$scope.master);
                console.log('$scope.dataGoodWork ===>', $scope.dataGoodWork);
                
                if($scope.dataGoodWork.length > 0){
                    console.log('$scope.dataGoodWork[0].ModelCode ===>',$scope.dataGoodWork[0].ModelCode);
                    console.log('ModelSelected.VehicleModelId ===>',ModelSelected.VehicleModelId);
                    if(parseInt($scope.dataGoodWork[0].ModelCode) !== ModelSelected.VehicleModelId){
                        $scope.disableWACExtVC= false;
                        $scope.chgChkVC();
                        console.log('masukkk asooooo');
                    }else{
                        console.log('GA masukkk asooooo');
                    }
                }
                

                $scope.mEstimationBP.VehicleModelName    = ModelSelected.VehicleModelName;
                $scope.mEstimationBP.ModelCode           = ModelSelected.VehicleModelId;
                WO.getVehicleTypeById(ModelSelected.VehicleModelId).then(function(res) {
                    console.log('GetDataType ===>', res.data.Result);
                    $scope.optionsTipe                   = [];
                    $scope.optionsKatashiki = [];
                    $scope.mEstimationBP.KatashikiCodex = '';
                    $scope.mEstimationBP.VehicleTypeName = "";
                    $scope.mEstimationBP.VehicleTypeId   = "";
                    $scope.mEstimationBP.KatashikiCode   = "";
                    $scope.mEstimationBP.ColorName = "";
                    // $scope.optionsTipe                   = res.data.Result;
                    $scope.optionsKatashiki = res.data.Result;

                    for (var i=0; i<$scope.optionsKatashiki.length; i++){
                        $scope.optionsKatashiki[i].NewDescription = $scope.mEstimationBP.VehicleModelName + ' - ' + $scope.optionsKatashiki[i].KatashikiCode
                    }
                    // testdong = 1;
                }); 
            }
        }

        $scope.onSelectKatashikiChanged = function (KatashikiSelected) {
            console.log('on model change ===>', KatashikiSelected);
            if (KatashikiSelected == undefined || KatashikiSelected == null) {
                $scope.mEstimationBP.VehicleTypeName  = "";
                $scope.mEstimationBP.VehicleTypeId    = "";
                $scope.mEstimationBP.KatashikiCode    = "";
                $scope.optionsTipe                    = [];
                $scope.mEstimationBP.ColorName = "";



            } else {
                $scope.mEstimationBP.KatashikiCode    = KatashikiSelected.KatashikiCode;
                WO.GetCVehicleTypeListByKatashiki(KatashikiSelected.KatashikiCode).then(function(res) {
                    console.log('GetDataType ===>', res.data.Result);
                    $scope.optionsTipe                   = [];
                    $scope.mEstimationBP.VehicleTypeName = "";
                    $scope.mEstimationBP.VehicleTypeId   = "";
                    $scope.mEstimationBP.ColorName = "";

                    // $scope.optionsTipe                   = res.data.Result[0];
                    $scope.optionsTipe                   = res.data.Result;


                    for (var i=0; i<$scope.optionsTipe.length; i++){
                        $scope.optionsTipe[i].NewDescription = $scope.optionsTipe[i].Description + ' - ' + $scope.optionsTipe[i].KatashikiCode + ' - ' + $scope.optionsTipe[i].SuffixCode
                    }
                    // testdong = 1;
                }); 
            }
        }

        $scope.onSelectTipeChanged = function (TipeSelected, mode) {
            console.log('TipeSelected ===>', TipeSelected);
            if (TipeSelected == undefined || TipeSelected == null) {
                $scope.mEstimationBP.VehicleTypeName = "";
                $scope.mEstimationBP.VehicleTypeId   = "";
                $scope.mEstimationBP.KatashikiCode   = "";
                $scope.mEstimationBP.ModelType       = "";
                $scope.mEstimationBP.ColorName = "";

            } else {
                if (mode == 'bind') {

                } else {
                    $scope.mEstimationBP.ColorName = "";
                }

                $scope.mEstimationBP.VehicleTypeName = TipeSelected.Description;
                $scope.mEstimationBP.VehicleTypeId   = TipeSelected.VehicleTypeId;
                $scope.mEstimationBP.KatashikiCode   = TipeSelected.KatashikiCode;
                $scope.mEstimationBP.ModelType       = TipeSelected.VehicleTypeId;

                estimasiBPFactory.NewGetMVehicleTypeColorById(TipeSelected.VehicleTypeId).then(function(res) {
                    console.log('get color ===>',res);
                    $scope.VehicColor = res.data.Result;
                    _.map($scope.VehicColor, function(val3) {
                        console.log('val3', val3);
                        // $scope.selectedColor(val3.MColor);
                    });
                });
            }
        }

        estimasiBPFactory.getWoCategory().then(function (res) {
            console.log("Kategori Pekerjaan ===>", res.data.Result);
            $scope.woCategory = res.data.Result;
        });

        $scope.onSearchSPKFilter = function (textfilter) {
            $scope.setRowData_spkList($scope.optionsSpkList);                
        }


        
        $scope.doSaveDraft = function(){
            if($scope.mEstimationBP.isCash == 1){
                $scope.mEstimationBP.InsuranceId = null;
            }   

            var tmpKm = angular.copy($scope.mEstimationBP.Km).toString();
            var backupKm = angular.copy($scope.mEstimationBP.Km);
            if (tmpKm.includes(",") == true) {
                tmpKm = tmpKm.split(',');
                if (tmpKm.length > 1) {
                    tmpKm = tmpKm.join('');
                } else {
                    tmpKm = tmpKm[0];
                }
            }
            tmpKm = parseInt(tmpKm);
            $scope.mEstimationBP.Km = tmpKm;

            
            console.log('ini pas klik save draft',$scope.estimationMode);
                   console.log("$scope.mVehicleCondition",$scope.mVehicleCondition);
            if($scope.estimationMode == "Create"){
                $scope.AEstimationBP = [];
                $scope.AEstimationBP = angular.copy($scope.mEstimationBP);
                $scope.mEstimationBP.IsSpk = parseInt($scope.mEstimationBP.IsSpk);
                if ($scope.mEstimationBP.isCash == 1){ //kalo cash SPKnya kosong
                    $scope.AEstimationBP.AEstimationBP_SPKList = [];
                }else{
                    // $scope.AEstimationBP.AEstimationBP_SPKList = angular.copy($scope.optionsSpkList);
                    $scope.AEstimationBP.AEstimationBP_SPKList = [];

                    $scope.AEstimationBP.PolisDateFrom = $scope.fixDate($scope.AEstimationBP.PolisDateFrom);
                    $scope.AEstimationBP.PolisDateTo = $scope.fixDate($scope.AEstimationBP.PolisDateTo);
                }
                $scope.AEstimationBP.AEstimationBP_TaskList_TT = [];
                $scope.AEstimationBP.AEstimationBP_TaskList_TT = angular.copy($scope.master);
                $scope.AEstimationBP.AEstimationBP_PlanSummary = [];
                $scope.AEstimationBP.AEstimationBP_WAC_Ext = [];
                if($scope.mVehicleCondition.Points.length == 0){

                    console.log("waaw");
                }else{
                   console.log("$scope.mVehicleCondition",typeof $scope.mVehicleCondition);
                   $scope.AEstimationBP.AEstimationBP_WAC_Ext = angular.copy([$scope.mVehicleCondition]); 
                }
                $scope.AEstimationBP.AEstimationBP_Request = [];
                $scope.AEstimationBP.AEstimationBP_Request = angular.copy($scope.JobRequest);




                console.log('final to factory save draft===>', $scope.AEstimationBP);
                estimasiBPFactory.createDraft($scope.AEstimationBP).then(
                    function (res) {
                        console.log('balikan dari back end simpan succ===>', res);
                        bsNotify.show({
                            title: "Customer Data",
                            content: "Data berhasil di simpan",
                            type: 'success'
                        });
                        
                        $scope.estimationMode = "Grid";
                        $scope.formApi.setMode('grid');

                        var url = "&NoPolisi=" +  $scope.AEstimationBP.PoliceNumber + "&DateFrom=2004-01-01&DateTo=9999-09-09";                    
                        $scope.estimationBPFilter.NoPolisi = $scope.AEstimationBP.PoliceNumber;
                        estimasiBPFactory.getData(url).then(function (res) {
                            $scope.FilterModelTypeSebelumMasukGrid(res.data.Result);
                        });
                    },
                    function (err) {
                        console.log('balikan dari back end simpan err===>', err);
                        bsNotify.show({
                                type: 'danger',
                                title: "Data gagal disimpan!",
                                content: err.data.Message,
                        });
                    
                });
            }else if($scope.estimationMode == "Edit"){
                $scope.AEstimationBP = [];
                $scope.AEstimationBP = angular.copy($scope.mEstimationBP);
                // $scope.AEstimationBP = angular.copy($scope.mEstimationBP);
                $scope.mEstimationBP.IsSpk = parseInt($scope.mEstimationBP.IsSpk);
                if ($scope.mEstimationBP.isCash == 1){ //kalo cash SPKnya kosong
                    $scope.AEstimationBP.AEstimationBP_SPKList = [];
                }else{
                    // $scope.AEstimationBP.AEstimationBP_SPKList = angular.copy($scope.optionsSpkList);
                }
                $scope.AEstimationBP.AEstimationBP_TaskList_TT = [];
                $scope.AEstimationBP.AEstimationBP_TaskList_TT = angular.copy($scope.dataGoodWork);
                $scope.AEstimationBP.AEstimationBP_PlanSummary = [];
                $scope.AEstimationBP.AEstimationBP_WAC_Ext = [];
                // $scope.AEstimationBP.AEstimationBP_WAC_Ext = angular.copy([$scope.mVehicleCondition]);
                $scope.AEstimationBP.AEstimationBP_Request = [];
                $scope.AEstimationBP.AEstimationBP_Request = angular.copy($scope.JobRequest);                 
                 if($scope.mVehicleCondition.Points.length == 0){

                     console.log("waaw");
                 }else{

                      console.log("$scope.mVehicleCondition",typeof $scope.mVehicleCondition);
                      
                      $scope.mVehicleCondition.EstimationBPId = $scope.mEstimationBP.EstimationBPId;
                      $scope.AEstimationBP.AEstimationBP_WAC_Ext = angular.copy([$scope.mVehicleCondition]);        
                 }
                // $scope.AEstimationBP.AEstimationBP_WAC_Ext = angular.copy([$scope.mVehicleCondition]);




                console.log('final to factory edit draft===>', $scope.AEstimationBP);
                estimasiBPFactory.UpdateDraft($scope.AEstimationBP).then(
                    function (res) {
                        console.log('balikan dari back end simpan succ===>', res);
                        bsNotify.show({
                            title: "Customer Data",
                            content: "Data berhasil di simpan",
                            type: 'success'
                        });

                        $scope.estimationMode = "Grid";
                        $scope.formApi.setMode('grid');

                        //panggil data itu
                        var url = "&NoPolisi=" +  $scope.AEstimationBP.PoliceNumber + "&DateFrom=2004-01-01&DateTo=9999-09-09";                    
                        estimasiBPFactory.getData(url).then(function (res) {
                            $scope.FilterModelTypeSebelumMasukGrid(res.data.Result);
                            console.log('berhasil get after update draft');
                        });
                    },
                    function (err) {
                        console.log('balikan dari back end simpan err===>', err);
                        bsNotify.show({
                                type: 'danger',
                                title: "Data gagal disimpan!",
                                content: err.data.Message,
                        });
                    
                });
            }
        }

        $scope.AEstimationBP = [];
        $scope.doCustomSave = function () {


            var tmpKm = angular.copy($scope.mEstimationBP.Km).toString();
            var backupKm = angular.copy($scope.mEstimationBP.Km);
            if (tmpKm.includes(",") == true) {
                tmpKm = tmpKm.split(',');
                if (tmpKm.length > 1) {
                    tmpKm = tmpKm.join('');
                } else {
                    tmpKm = tmpKm[0];
                }
            }
            tmpKm = parseInt(tmpKm);
            $scope.mEstimationBP.Km = tmpKm;

            console.log("$scope.mVehicleCondition", Object.keys($scope.mVehicleCondition).length);
            console.log("$scope.mEstimationBP",$scope.mEstimationBP);
            if($scope.mEstimationBP.isCash == 1){
                $scope.mEstimationBP.InsuranceId = null;
            }
            console.log('ini pas klik save',$scope.estimationMode);
            if($scope.estimationMode == "Create"){
                $scope.AEstimationBP = [];
                $scope.AEstimationBP = angular.copy($scope.mEstimationBP);
                $scope.mEstimationBP.IsSpk = parseInt($scope.mEstimationBP.IsSpk);
                if ($scope.mEstimationBP.isCash == 1){ //kalo cash SPKnya kosong
                    $scope.AEstimationBP.AEstimationBP_SPKList = [];
                }else{
                    // $scope.AEstimationBP.AEstimationBP_SPKList = angular.copy($scope.optionsSpkList);
                    $scope.AEstimationBP.AEstimationBP_SPKList = [];

                    $scope.AEstimationBP.PolisDateFrom = $scope.fixDate($scope.AEstimationBP.PolisDateFrom);
                    $scope.AEstimationBP.PolisDateTo = $scope.fixDate($scope.AEstimationBP.PolisDateTo);
                }
                $scope.AEstimationBP.AEstimationBP_TaskList_TT = [];
                $scope.AEstimationBP.AEstimationBP_TaskList_TT = angular.copy($scope.master);
                $scope.AEstimationBP.AEstimationBP_PlanSummary = [];
                $scope.AEstimationBP.AEstimationBP_WAC_Ext = [];

                console.log("$scope.mVehicleCondition | new =====>", $scope.mVehicleCondition);
                if($scope.mVehicleCondition.Points.length > 0){
                   $scope.AEstimationBP.AEstimationBP_WAC_Ext = angular.copy([$scope.mVehicleCondition]); 
                }
                $scope.AEstimationBP.AEstimationBP_Request = [];
                 
                $scope.AEstimationBP.AEstimationBP_Request = angular.copy($scope.JobRequest);


                for(var a in $scope.AEstimationBP.AEstimationBP_TaskList_TT){
                    if($scope.AEstimationBP.AEstimationBP_TaskList_TT[a].AEstimationBP_PartList.length>0){
                        for(var b in $scope.AEstimationBP.AEstimationBP_TaskList_TT[a].AEstimationBP_PartList){
                            if($scope.AEstimationBP.AEstimationBP_TaskList_TT[a].AEstimationBP_PartList[b].IsOPB == false){
                                $scope.AEstimationBP.AEstimationBP_TaskList_TT[a].AEstimationBP_PartList[b].IsOPB = 0;
                            }else if($scope.AEstimationBP.AEstimationBP_TaskList_TT[a].AEstimationBP_PartList[b].IsOPB == true){
                                $scope.AEstimationBP.AEstimationBP_TaskList_TT[a].AEstimationBP_PartList[b].IsOPB = 1;
                            }
                        }
                    }
                }
                
                console.log('final to factory ===>', $scope.AEstimationBP);
                estimasiBPFactory.create($scope.AEstimationBP).then(
                    function (res) {
                        console.log('balikan dari back end simpan succ===>', res);
                        bsNotify.show({
                            title: "Customer Data",
                            content: "Data berhasil di simpan",
                            type: 'success'
                        });
                        $scope.estimationMode = "Grid";
                        $scope.formApi.setMode('grid');

                        var findNoEst = res.data.ResponseMessage.split('#');
                        $scope.alertAfterSave(findNoEst[2]);
                        // $scope.alertAfterSave(findNoEst.slice(-19));



                      

                        //panggil data itu
                        $scope.estimationBPFilter.DateFrom = $scope.fixDate(new Date());
                        $scope.estimationBPFilter.DateTo = $scope.fixDate(new Date());
                        
                      
                        
                           
                        var url = "&NoPolisi=" + $scope.mEstimationBP.PoliceNumber + "&DateFrom=2004-01-01&DateTo=9999-09-09";  
                        // var url = "&NoPolisi=" +  $scope.mEstimationBP.PoliceNumber + "&DateFrom=2004-01-01&DateTo=9999-09-09";                    
                        $scope.estimationBPFilter.NoPolisi = $scope.mEstimationBP.PoliceNumber;
                        estimasiBPFactory.getData(url).then(function (res) {
                            // $scope.printAppoinmentBP = 'as/PrintMasterEstimationBPBaru/'+$scope.user.OutletId+'/'+ res.data.Result[0].EstimationBPId; // waktu create awal"
                            // $scope.cetakan($scope.printAppoinmentBP);
                            console.log('berhasil get after save',res.data.Result);
                            var findSavedEstimatiom = _.find(res.data.Result, function(n){
                                return n.EstimationNo == findNoEst[2];
                            })
                            console.log('findSavedEstimatiom ===>',findSavedEstimatiom);

                              //panggil cetakan
                            $scope.EstimationNo = findSavedEstimatiom.EstimationNo +"_"+ findSavedEstimatiom.PoliceNumber;
                            $scope.printAppoinmentBP = 'as/PrintMasterEstimationBPBaru/'+$scope.user.OutletId+'/'+ findNoEst[1]; // waktu create awal"
                            $scope.cetakan($scope.printAppoinmentBP,findSavedEstimatiom);
                            //panggil cetakan




                            $scope.FilterModelTypeSebelumMasukGrid(res.data.Result);
                        });

                    },
                    function (err) {
                        console.log('balikan dari back end simpan err===>', err);
                        bsNotify.show({
                                type: 'danger',
                                title: "Data gagal disimpan!",
                                content: err.data.Message.split('-')[1],
                        });
                    
                });
            }else if($scope.estimationMode == "Edit"){
                $scope.AEstimationBP = [];
                $scope.AEstimationBP = angular.copy($scope.mEstimationBP);
                // $scope.AEstimationBP = angular.copy($scope.mEstimationBP);
                $scope.mEstimationBP.IsSpk = parseInt($scope.mEstimationBP.IsSpk);
                if ($scope.mEstimationBP.isCash == 1){ //kalo cash SPKnya kosong
                    $scope.AEstimationBP.AEstimationBP_SPKList = [];
                }else{
                    // $scope.AEstimationBP.AEstimationBP_SPKList = angular.copy($scope.optionsSpkList);
                }




                console.log('tmpDeleteParts ===>',tmpDeleteParts);
                if(tmpDeleteParts.length > 0){
                    for(var k in tmpDeleteParts){
                        console.log('tmpDeleteParts ke-',k);
                        for(var i in $scope.dataGoodWork){
                            console.log('dataGoodWork ke-',i)
                            console.log('dataGoodWork => ',$scope.dataGoodWork[i].EstTaskBPId+ ' | tmpDel =>' + tmpDeleteParts[k].EstTaskBPId);
                            if($scope.dataGoodWork[i].EstTaskBPId == tmpDeleteParts[k].EstTaskBPId){
                                console.log('masuk nih data deletenya yang ke -', k)
                                $scope.dataGoodWork[i].AEstimationBP_PartList.push(tmpDeleteParts[k]);
                            }
                        }   
                    }
                }


                

     


                
                console.log('$scope.dataGoodWork ===>',$scope.dataGoodWork);

                $scope.AEstimationBP.AEstimationBP_TaskList_TT = [];
                $scope.AEstimationBP.AEstimationBP_TaskList_TT = angular.copy($scope.dataGoodWork);
                $scope.AEstimationBP.AEstimationBP_PlanSummary = [];
                $scope.AEstimationBP.AEstimationBP_WAC_Ext = [];


                $scope.AEstimationBP.AEstimationBP_Request_TT = $scope.JobRequest;
                $scope.mVehicleCondition.EstimationBPId = $scope.mEstimationBP.EstimationBPId;
                $scope.AEstimationBP.AEstimationBP_WAC_Ext = angular.copy($scope.mVehicleCondition); 
                $scope.AEstimationBP.AEstimationBP_Request = [];
                $scope.AEstimationBP.AEstimationBP_Request = angular.copy($scope.JobRequest);

                for(var a in $scope.AEstimationBP.AEstimationBP_TaskList_TT){
                    if($scope.AEstimationBP.AEstimationBP_TaskList_TT[a].AEstimationBP_PartList.length>0){
                        for(var b in $scope.AEstimationBP.AEstimationBP_TaskList_TT[a].AEstimationBP_PartList){
                            if($scope.AEstimationBP.AEstimationBP_TaskList_TT[a].AEstimationBP_PartList[b].IsOPB == false){
                                $scope.AEstimationBP.AEstimationBP_TaskList_TT[a].AEstimationBP_PartList[b].IsOPB = 0;
                            }else if($scope.AEstimationBP.AEstimationBP_TaskList_TT[a].AEstimationBP_PartList[b].IsOPB == true){
                                $scope.AEstimationBP.AEstimationBP_TaskList_TT[a].AEstimationBP_PartList[b].IsOPB = 1;
                            }
                        }
                    }
                }




                console.log('final to factory ===>', $scope.AEstimationBP);
                estimasiBPFactory.update($scope.AEstimationBP).then(
                    function (res) {

                        // if($scope.AEstimationBP.JobId !== null && $scope.AEstimationBP.StatusCode == 1){
                            // var url = "&NoPolisi=" + $scope.mEstimationBP.PoliceNumber + "&DateFrom=" + $scope.estimationBPFilter.DateFrom + "&DateTo="+ $scope.estimationBPFilter.DateTo;
                            var url = "&NoPolisi=" +  $scope.mEstimationBP.PoliceNumber + "&DateFrom=2004-01-01&DateTo=9999-09-09";                    
                            $scope.estimationBPFilter.NoPolisi = $scope.mEstimationBP.PoliceNumber;
                            estimasiBPFactory.getData(url).then(function (res) {
                                var findSavedEstimatiom = _.find(res.data.Result, { 'EstimationBPId': $scope.AEstimationBP.EstimationBPId, 'JobId': $scope.AEstimationBP.JobId })
                                console.log('findSavedEstimatiom ===>',findSavedEstimatiom);
                                console.log('berhasil get after edit save');
                                $scope.printEstimation(findSavedEstimatiom);
                                $scope.FilterModelTypeSebelumMasukGrid(res.data.Result);
                            });
                        // }

                        console.log('balikan dari back end simpan succ===>', res);
                        bsNotify.show({
                            title: "Customer Data",
                            content: "Data berhasil di simpan",
                            type: 'success'
                        });

                        $scope.estimationMode = "Grid";
                        $scope.formApi.setMode('grid');
                    },
                    function (err) {
                        console.log('balikan dari back end simpan err===>', err);
                        bsNotify.show({
                                type: 'danger',
                                title: "Data gagal disimpan!",
                                content: err.data.Message.split('-')[1],
                        });
                    
                });
            }
           

        }

        $scope.MainGridEstimasi = true;
        $scope.FormEstimasi = false;
        
        $scope.btnTambahEstimasi = function(){
            $scope.MainGridEstimasi = false;
            $scope.FormEstimasi = true;
        }
        
        $scope.btnKembaliEstimasi = function(){
            $scope.MainGridEstimasi = true;
            $scope.FormEstimasi = false;
        }
        
        $scope.btnDraftEstimasi = function(){
            console.log('on click btnDraftEstimasi');
        }
        
        


        $scope.onFilterTextBoxChanged = function () {
            console.log('$scope.FilterSpkNo ===>', $scope.FilterSpkNo);
            $scope.agSpkListGalangApi.setQuickFilter($scope.FilterSpkNo);
        }



        $scope.alertAfterSave = function(item) {
            
            bsAlert.alert({
                    title: "Data tersimpan dengan Nomor Estimasi",
                    text: item,
                    type: "warning",
                    showCancelButton: false
                },
                function() {
                    $scope.formApi.setMode('grid');
                },
                function() {

                }
            )
        }
    

        
        $scope.agSpkListGalangApi;
        $scope.SpkListGalang = {
            //DEFAULT AG-GRID OPTIONS
            // default ColDef, gets applied to every column
            defaultColDef: {
                width: 150,
                editable: false,
                filter: true,
                sortable: true,
                resizable: true
            },
            rowSelection: 'multiple',
            suppressRowClickSelection: true,
            angularCompileRows: true,
            rowData: $scope.optionsSpkList,
            
            // components: {
            //     partEditor: PartEditor,
            //     numericEditor: NumericCellEditor,
            //     numericEditorPct: NumericCellEditorPct
            // },
            onGridReady: function (params) {
                // params.api.sizeColumnsToFit();
                $scope.agSpkListGalangApi = params.api;
            },

            
            // rowData: $scope.master,
            onCellEditingStopped: function (e) {
                console.log('parts cellEditingStopped=>', e.rowIndex, e.colDef.field, e.value, e.node, e);
                // if(e.colDef.field == 'PartCode'){
                $scope.onAfterCellEditAg(e.rowIndex, e.colDef.field, e.value, e.node);
                // }
            },
            getNodeChildDetails: function (row) {
                if (row.ListAEstimationSPKChild) {
                    return {
                        group: row.ListAEstimationSPKChild && row.ListAEstimationSPKChild.length > 0,
                        field: "SpkNo",
                        children: row.ListAEstimationSPKChild,
                        key: row.SpkNo,
                        expanded: row.open
                    };
                } else {
                    return null;
                }
            },
            columnDefs: [

                { headerName: 'SPK List ID', field: 'EstSPKListId', width: 150, editable: true, pinned: true, hide: true},
                { headerName: 'No. SPK', field: 'SpkNo', width: 190, editable: true, pinned: true, cellRenderer: 'group' },
                { headerName: 'No. Polis', field: 'NoPolis', width: 120, editable: true },
                { headerName: 'No. LK', field: 'NoLK', width: 120},
                { headerName: 'Periode Polis', field: 'PeriodePolis', width: 150},
                { headerName: 'Own Risk', field: 'OR', width: 120, editable: false},
                { headerName: 'Action', width: 150, suppressFilter: true, suppressMenu: true, pinned: 'right', cellRenderer: actionRenderer,
                
                }
            ]
        };

        function actionRenderer() {
            var actionHtml = ' <button class="ui icon inverted grey button"\
                                style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px"\
                                onclick="this.blur()"\
                                ng-click="gridSpkViewAction(data)">\
                                <i class="fa fa-fw fa-lg fa-list-alt"></i>\
                            </button>\
                            <button class="ui icon inverted grey button"\
                                style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px"\
                                onclick="this.blur()"\
                                ng-click="gridSpkEditAction(data)">\
                                <i class="fa fa-fw fa-lg fa-pencil"></i>\
                            </button>\
                            <button class="ui icon inverted grey button"\
                                style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px"\
                                onclick="this.blur()"\
                                ng-click="gridSpkDeleteAction(data)">\
                                <i class="fa fa-fw fa-lg fa-trash-o"></i>\
                            </button>';
            return actionHtml;
        }



        

        $scope.bsAlertConfirm = function (title, text) {
            var defer = $q.defer();
            bsAlert.alert({
                title: title || 'Estimasi BP',
                text: text || '',
                type: "question",
                showCancelButton: true
            },
                function () {
                    defer.resolve(true);
                },
                function () {
                    defer.resolve(false);
                }
            )
            return defer.promise;
        };


    
       
        


        // ag SpkListGalangs

        $scope.bsAlertConfirm = function (title, text) {
            var defer = $q.defer();
            bsAlert.alert({
                title: title || 'Appointment Service GR',
                text: text || '',
                type: "warning",
                showCancelButton: false
            },
                function () {
                    defer.resolve(true);
                },
                function () {
                    defer.resolve(false);
                }
            )
            return defer.promise;
        };


        var candicateDelete = [];
        $scope.onSelectRows = function (rows) {
            console.log('onSelectRows ===>', rows);
            candicateDelete = rows
            console.log('Current User ===>', $scope.user);
        }


        $scope.onValidateDelete = function(){

            console.log('prepare to delete ===>',candicateDelete);

            var countFail = 0;
            for(var i in candicateDelete){
                if(candicateDelete[i].EmployeeId == $scope.user.EmployeeId){
                    countFail ++;
                }
            }

            var countTidakBisaDeleteKarnaSudahWO = 0;
            for(var x in candicateDelete){
                if(candicateDelete[x].EstimationBPId == 0){
                    countTidakBisaDeleteKarnaSudahWO ++;
                }
            }


            if(countFail > 0 && countTidakBisaDeleteKarnaSudahWO == 0){
                $scope.bsAlertConfirmDelete("Data tidak dapat dihapus","Anda tidak memiliki hak untuk menghapus data ini").then(function (res) {
                    if (res) {
                        console.log('ini kalo klik ok',res);
                        return true
                    }else{
                        console.log("ini kalo klik cancle", res);
                        return false

                    }
                });
                return false;
            }else if(countTidakBisaDeleteKarnaSudahWO > 0){
                $scope.bsAlertConfirmDelete("Data tidak dapat dihapus","Anda tidak dapat menghapus data estimasi ini karna status data ini tidak lanjut WO ").then(function (res) {
                    if (res) {
                        console.log('ini kalo klik ok',res);
                        return true
                    }else{
                        console.log("ini kalo klik cancle", res);
                        return false
                    }
                });
                return false;
            }else{
                return true;
            }
        }



        $scope.bsAlertConfirmDelete = function (title, text) {
            var defer = $q.defer();
            bsAlert.alert({
                title: title || 'Delete Data Estimasi BP',
                text: text || '',
                type: "warning",
                showCancelButton: false
            },
                function () {
                    defer.resolve(true);
                },
                function () {
                    defer.resolve(false);
                }
            )
            return defer.promise;
        };

        $scope.mEstimationBP.DP = 0;
        $scope.onBeforeEdit = function (data) {
            $scope.formApi.setMode('view');
            $scope.formApi.setMode('edit');
            $scope.estimationMode = "Edit";
            console.log('ini on before edit',data);
            $scope.activeJustified = undefined;
            $scope.activeJustified = 0;
            // console.log('DP PArts',$scope.mEstimationBP.DP);


            $scope.restart_digit_odo = 0
            $scope.isKmHistory = 0
            $scope.nilaiKM = 0
            $scope.KMTerakhir = 0

            $scope.restart_digit_odo = 0;
            $scope.model_info_km.show = false;

            

            // jangan di hapus, di dobel karna kalo dipake 1x doang ga mempan
            if(data.EstimationNo !== null){ //kalo ada isinya
                console.log('MASUK SINI DONG KLO UDAH ADA NO ESTIMASI  1| disablePoliceNo', $scope.disablePoliceNo, data.EstimationNo);
                $scope.disablePoliceNo = true;
            }else{
                console.log('MASUK SINI DONG KLO BLM ADA NO ESTIMASI  2| disablePoliceNo', $scope.disablePoliceNo);
                $scope.disablePoliceNo = false;
            }


            // if($scope.flagNgakalinCalculate == 0){
            //     console.log('onBeforeView jalanin clearAll');
            //     $scope.clearAll();
            // }else{
            //     console.log('onBeforeView tidak jalanin clearAll');
            // }


          
            var kondisiestimasi = "";
            if(data.EstimationBPId !== 0){
                kondisiestimasi = data.EstimationBPId;
                console.log('ini Estimasi belum WO ===> EstimationBPId => ' ,data.EstimationBPId)
            }else{
                kondisiestimasi = data.JobId;
                console.log('ini Estimasi sudah WO ===> JobId => ',data.JobId)
            }
            estimasiBPFactory.getDetail(kondisiestimasi, data.PoliceNumber).then(function (res) {
            // estimasiBPFactory.getDetail(kondisiestimasi).then(function (res) {
                $scope.mEstimationBP = res.data.Result[0];
                console.log('$scope.mEstimationBP | onBeforeEdit',$scope.mEstimationBP);

                $scope.mEstimationBP.AEstimationBP_TaskList_TT = $scope.orderingData($scope.mEstimationBP.AEstimationBP_TaskList_TT, 'EstTaskBPId')

                for (var i=0; i< $scope.mEstimationBP.AEstimationBP_TaskList_TT.length; i++) {
                    $scope.mEstimationBP.AEstimationBP_TaskList_TT[i].AEstimationBP_PartList = $scope.orderingData($scope.mEstimationBP.AEstimationBP_TaskList_TT[i].AEstimationBP_PartList, 'EstPartsId')
                }
                

                $scope.mEstimationBP.Km = String($scope.mEstimationBP.Km);
                $scope.mEstimationBP.Km = $scope.mEstimationBP.Km.replace(/\B(?=(\d{3})+(?!\d))/g, ',');

                $scope.CekServiceHistory(null, $scope.mEstimationBP.VIN, null, null, 'edit')

                //SPK
                // if($scope.mEstimationBP.AEstimationBP_SPKList.length > 0){
                //     $scope.mEstimationBP.NoPolis      = $scope.mEstimationBP.AEstimationBP_SPKList[0].NoPolis;
                //     $scope.mEstimationBP.NoLK         = $scope.mEstimationBP.AEstimationBP_SPKList[0].NoLK;
                //     $scope.mEstimationBP.PeriodePolis = $scope.mEstimationBP.AEstimationBP_SPKList[0].PeriodePolis;
                // }

                var tampunganVehicleTypeId = res.data.Result[0].ModelType;
                // $scope.setRowData_spkList($scope.mEstimationBP.AEstimationBP_SPKList);
                for (var i = 0; i < $scope.optionsModel.length; i++) {
                    if ($scope.optionsModel[i].VehicleModelId == parseInt($scope.mEstimationBP.ModelCode)){
                        $scope.mEstimationBP.VehicleModelName    = $scope.optionsModel[i].VehicleModelName;
                        $scope.mEstimationBP.ModelCode           = $scope.optionsModel[i].VehicleModelId;
                        WO.getVehicleTypeById($scope.optionsModel[i].VehicleModelId).then(function(res) {
                            // $scope.optionsTipe = res.data.Result;

                            $scope.optionsKatashiki = res.data.Result;

                            for (var i=0; i<$scope.optionsKatashiki.length; i++){
                                $scope.optionsKatashiki[i].NewDescription = $scope.mEstimationBP.VehicleModelName + ' - ' + $scope.optionsKatashiki[i].KatashikiCode
                            }

                            for (var i=0; i < $scope.optionsKatashiki.length; i++) {
                                if ($scope.optionsKatashiki[i].KatashikiCode == $scope.mEstimationBP.KatashikiCode) {
                                    $scope.mEstimationBP.KatashikiCodex   = $scope.mEstimationBP.KatashikiCode;

                                }
                            }

                            if ($scope.mEstimationBP.KatashikiCodex != null && $scope.mEstimationBP.KatashikiCodex != undefined && $scope.mEstimationBP.KatashikiCodex != '') {

                                WO.GetCVehicleTypeListByKatashiki($scope.mEstimationBP.KatashikiCode).then(function(res) {
                                    // $scope.optionsTipe = res.data.Result[0];
                                    $scope.optionsTipe = res.data.Result;


                                    for (var i=0; i<$scope.optionsTipe.length; i++){
                                        $scope.optionsTipe[i].NewDescription = $scope.optionsTipe[i].Description + ' - ' + $scope.optionsTipe[i].KatashikiCode + ' - ' + $scope.optionsTipe[i].SuffixCode
                                    }

                                    for (var z = 0; z < $scope.optionsTipe.length; z++) {
                                        if($scope.optionsTipe[z].VehicleTypeId == tampunganVehicleTypeId){
                                            $scope.onSelectTipeChanged($scope.optionsTipe[z] , 'bind')
                                            console.log('Type Gue ===> ',tampunganVehicleTypeId +" == ModelOptions "+$scope.optionsTipe[z].VehicleTypeId);
                                        }
                                    }

                                })


                            }

                            
                        })
                    }
                }

                

                //ambil diskon dari tipe pembayaran
                if($scope.mEstimationBP.isCash == 1){
                    $scope.choosePembayaran(1);
                    
                    if($scope.mEstimationBP.AEstimationBP_TaskList_TT.length > 0){
                        $scope.disTask = $scope.mEstimationBP.AEstimationBP_TaskList_TT[0].Discount;
                        if($scope.mEstimationBP.AEstimationBP_TaskList_TT[0].AEstimationBP_PartList.length > 0){
                            $scope.disParts = $scope.mEstimationBP.AEstimationBP_TaskList_TT[0].AEstimationBP_PartList[0].Discount;
                        }
                    }
                    console.log('ini pembayaran cash');
                }else{
                    console.log('ini pembayaran Bukan cash');
                    
                    for (i in $scope.optionsLihatInformasi_NamaInsurance_Search){
                        if($scope.optionsLihatInformasi_NamaInsurance_Search[i].InsuranceId == $scope.mEstimationBP.InsuranceId){
                            $scope.mData.InsuranceName = $scope.optionsLihatInformasi_NamaInsurance_Search[i].InsuranceName;
                            $scope.onInsuranceSelected($scope.optionsLihatInformasi_NamaInsurance_Search[i]);
                        }
                    }
                }
                //ambil diskon dari tipe pembayaran
                
                // untuk request 
                $scope.JobRequest = $scope.mEstimationBP.AEstimationBP_Request_TT;
                // untuk request

                 /*untuk data job*/
                 $scope.gridEstimation.data = [];
                 dataTaskSave = $scope.mEstimationBP.AEstimationBP_TaskList_TT;
                 for (var i=0; i<dataTaskSave.length; i++) {
                    if (dataTaskSave[i].FlatRate == null || dataTaskSave[i].FlatRate == undefined) {
                        dataTaskSave[i].FlatRate = 1
                    } else {
                        dataTaskSave[i].FlatRate = dataTaskSave[i].FlatRate
                    }

                    dataTaskSave[i].SubTotalTask = dataTaskSave[i].FlatRate * dataTaskSave[i].Fare
                }
                 
                 console.log("dataTaskSave",dataTaskSave);

                 // if ($scope.mEstimationBP.DP !== undefined) {
                 // }
                 var tempTotal = 0;
                 var tmpDp = 0;
                 var tmpsat = 0;
                 var tmplagi = 0;
                 var dp = 0;
                 console.log('dataTaskSave buat ag grid ===>',dataTaskSave);
                 for(var a = 0; a < dataTaskSave.length; a++){
                    //$scope.getDataGirdWorkView(dataTaskSave[a],dataTaskSave[a].FlagID);
                    // var satuan = null;
                    console.log("masuk sini ga ?");
                    dataTaskSave[a].isDeleted = 0;
                    dataTaskSave[a].totalParts = dataTaskSave[a].AEstimationBP_PartList.length;
                    dataTaskSave[a].EstimationNo = $scope.mEstimationBP.EstimationNo;
                    for (var i = 0; i < $scope.unitData.length; i++) {
                        if ($scope.unitData[i].MasterId == dataTaskSave[a].ProcessId) {
                            dataTaskSave[a].satuan = $scope.unitData[i].Name;
                        }

                        for(var d = 0; d < dataTaskSave[a].AEstimationBP_PartList.length; d++){
                        dataTaskSave[a].AEstimationBP_PartList[d].Qty = dataTaskSave[a].AEstimationBP_PartList[d].qty
                        if ($scope.unitData[i].MasterId == dataTaskSave[a].AEstimationBP_PartList[d].SatuanId) {
                            dataTaskSave[a].AEstimationBP_PartList[d].Name = $scope.unitData[i].Name;
                            }

                            
                        // if (dataTaskSave[a].AEstimationBP_PartList[d].DPRequest != 0){
                        //     $scope.mEstimationBP.DP = dataTaskSave[a].AEstimationBP_PartList[d].DPRequest;
                        // }
                        // else{
                        //     $scope.mEstimationBP.DP = 0;
                        // }

                            // AppointmentBpService.getAvailableParts(dataTaskSave[a].AEstimationBP_PartList[d].PartsId, 0, dataTaskSave[a].AEstimationBP_PartList[d].Qty).then(function(res) { 
                            //     $scope.mEstimationBP.DP = res.data.Result[0].PercentDP
                            // })
                        }

                    }
                    // console.log("satuan",dataTaskSave[a].satuan);


                    for(var zz = 0; zz < $scope.paymentData.length; zz++){
                        for(var d = 0; d < dataTaskSave[a].AEstimationBP_PartList.length; d++){
                            if ($scope.paymentData[zz].MasterId == dataTaskSave[a].AEstimationBP_PartList[d].PaidById) {
                                dataTaskSave[a].AEstimationBP_PartList[d].PaidBy = $scope.paymentData[zz].Name;
                            }
                        }
                    }

                    for(var d = 0; d < dataTaskSave[a].AEstimationBP_PartList.length; d++){
                        
                        AppointmentBpService.getAvailableParts(dataTaskSave[a].AEstimationBP_PartList[d].PartsId, 0, dataTaskSave[a].AEstimationBP_PartList[d].qty).then(function(res) { 
                            console.log("data yagn didapat",res.data.Result);

                            $scope.mEstimationBP.DP = res.data.Result[0].PercentDP
                            
                            var keter;
                            switch (res.data.Result[0].isAvailable) {
                                case 0:
                                    
                                    keter = "Tidak Tersedia";
                                    break;
                                case 1:
                                    keter = "Tersedia";
                                    break;
                                case 2:
                                    keter = "Tersedia Sebagian";
                                    break;
                            }
                            console.log("keter",keter,dataTaskSave);
                            
                            for(var a = 0; a < dataTaskSave.length; a++){
                                
                                for(var d = 0; d < dataTaskSave[a].AEstimationBP_PartList.length; d++){

                                    console.log('estimationBP',$scope.mEstimationBP.DP);
                                    
                                    console.log("tmpsat xxz",tmpsat);
                                    console.log("tmpsat",tmplagi);
                                    if(tmpsat == tmplagi){
                                            //$scope.countDP();
                                        }else{
                                            //tmpsat++;
                                            //$scope.countDP();
                                            console.log("masih belom tunggu dulu");
                                        }
                                    if(dataTaskSave[a].AEstimationBP_PartList[d].PartsId == res.data.Result[0].PartsId){
                                        console.log("dataTaskSave[a].AEstimationBP_PartList[d].PartsId",dataTaskSave[a].AEstimationBP_PartList[d]);
                                        dataTaskSave[a].AEstimationBP_PartList[d].Availbility = keter;
                                        dataTaskSave[a].AEstimationBP_PartList[d].isAvailable = res.data.Result[0].isAvailable;
                                        
                                    }
                                }
                            }
                            
                        }
                        ,function(err) {
                                console.log("err=>", err);
                        }
                        );
                    if (dataTaskSave[a].AEstimationBP_PartList[d].MaterialTypeId == 1) {
                        dataTaskSave[a].AEstimationBP_PartList[d].TipeMaterial = "Spare Parts"; //jancooooooook
                    } else{
                        dataTaskSave[a].AEstimationBP_PartList[d].TipeMaterial = "Bahan";
                    }

                    if (dataTaskSave[a].AEstimationBP_PartList[d].RetailPrice == undefined ){
                        dataTaskSave[a].AEstimationBP_PartList[d].RetailPrice = dataTaskSave[a].AEstimationBP_PartList[d].Price;
                        console.log('ini ga ada retailpricenya');
                    }else{
                        console.log('ini ada retail pricenya');
                    }


                    dataTaskSave[a].AEstimationBP_PartList[d].IsSaveFin = 1;
                    dataTaskSave[a].AEstimationBP_PartList[d].SubTotal = dataTaskSave[a].AEstimationBP_PartList[d].Price * dataTaskSave[a].AEstimationBP_PartList[d].qty - ((dataTaskSave[a].AEstimationBP_PartList[d].Price * dataTaskSave[a].AEstimationBP_PartList[d].qty)* dataTaskSave[a].AEstimationBP_PartList[d].Discount  /100) ;
                    dataTaskSave[a].AEstimationBP_PartList[d].IdFlagParts = Math.floor(Math.random() * 10000) + 1;
                    dataTaskSave[a].AEstimationBP_PartList[d].isSave = 1;
                    if( dataTaskSave[a].EstTaskBPId == dataTaskSave[a].AEstimationBP_PartList[d].EstTaskBPId){

                        tempTotal += dataTaskSave[a].AEstimationBP_PartList[d].SubTotal;
                        console.log("tempTotal",tempTotal);
                        tmpDp = (tempTotal * dataTaskSave[a].AEstimationBP_PartList[0].DownPayment/100);
                        console.log("tmpDp",tmpDp); 
                        dataTaskSave[a].finalDP = tmpDp;
                        console.log("tmpDp2",tmpDp); 
                        //$scope.totalDp = tmpDp;
                    } 
                    }


                    dataTaskSave[a].FlagID = Math.floor(Math.random() * 10000) + 1;

                    dataTaskSave[a].TaskListBPId =  dataTaskSave[a].TaskBPId;
                    $scope.dataGoodWork = angular.copy(dataTaskSave);

                    dataTaskSave[a].isUpdateEstimate = true;

                    console.log('dataTaskSave[a]',dataTaskSave[a])
                    console.log('dataTaskSave[a] avail',dataTaskSave[a].AEstimationBP_PartList);
                    console.log("tmpsat",tmpsat); 
                    
                    console.log("tmplagi",tmplagi);
                    for(var dd in dataTaskSave[a].AEstimationBP_PartList){
                        tmplagi++
                        
                    }

                    $scope.getDataGirdWorkView(dataTaskSave[a],dataTaskSave[a].FlagID);
                    $scope.countDP();
                    
                }
                 // $scope.agGridWorkViewApi.setRowData(dataTaskSave);
                 /*untuk data job*/


                 if($scope.mEstimationBP.EstimationNo == null || $scope.mEstimationBP.EstimationNo == undefined || $scope.mEstimationBP.EstimationNo == ""){
                    $scope.chooseVTypeVC($scope.mEstimationBP.TypeForVC);
                    $scope.filter.vTypeVC = $scope.mEstimationBP.TypeForVC;
                }
         

                //Vehicle Condition 
                for (var i = 0; i < $scope.VTypeData.length; i++) {
                    if ($scope.mEstimationBP.AEstimationBP_WAC_Ext.length > 0) {
                        if ($scope.VTypeData[i].MasterId == $scope.mEstimationBP.AEstimationBP_WAC_Ext[0].TypeId) {
                            $scope.filter.vTypeVC = $scope.VTypeData[i];
                        }   

                        console.log('points ===>',JSON.parse($scope.mEstimationBP.AEstimationBP_WAC_Ext[0].Points));

                    }
                    
                }


                // $scope.chooseVTypeVC($scope.mEstimationBP.AEstimationBP_WAC_Ext[0].TypeId);
                $scope.areasArrayVC = [];
                $scope.customHeight = { height: '500px' };
                mstIdVC = $scope.mEstimationBP.AEstimationBP_WAC_Ext[0].TypeId;
                // $scope.filter.vTypeVC = mstIdVC;
                $scope.areasArrayVC = getImgAndPointsVC(mstIdVC);
                var resu = $scope.mEstimationBP.AEstimationBP_WAC_Ext;

                console.log('resuu ===>',resu);

                console.log("$scope.areasArrayVC 1 ====>", $scope.areasArrayVC);
                dotsDataVC[mstIdVC] = [];
                var tmpBiji;
                for (var i in resu) {
                    var getTemp = [];
                    getTemp =  JSON.parse(resu[i].Points);
                    var xtemp = {};


                    tmpBiji = angular.copy(getTemp);
                    console.log('gettemp', getTemp);
                    for (var j in getTemp) {
                        console.log('gettempJstat', getTemp[j]);
                        var xtemp = {};
                        xtemp = angular.copy(getTemp[j]);
                        delete xtemp['status'];
                        delete xtemp['PointId'];
                        xtemp.cssClass = ''; //jancok
                        console.log("xtemp for J : ", xtemp);
                        console.log("scope.areasArrayVC 2 : ", $scope.areasArrayVC);
                        if (typeof _.find($scope.areasArrayVC, xtemp) !== 'undefined') {
                            console.log("merah");
                            console.log("find..", xtemp);
                            console.log("scope.areasArrayVC 3 : ", $scope.areasArrayVC);
                            var idx = _.findIndex($scope.areasArrayVC, xtemp);
                            console.log('idx', idx);
                            xtemp.cssClass = "choosen-area";
                            $scope.areasArrayVC[idx] = xtemp;

                            dotsDataVC[mstIdVC].push({
                                "ItemId": xtemp.areaid,
                                "ItemName": xtemp.name,
                                "Points": xtemp,
                                "Status": getTemp[j].status
                            });
                        }else{
                            console.log('gamasuk kunyukkk');
                            console.log('$scope.areasArrayVC ====>',$scope.areasArrayVC);
                            console.log('xtemp ==================>',xtemp);
                        }
                    }    
                }
                for (var i = 0; i < dotsDataVC[mstIdVC].length; i++) {
                    dotsDataVC[mstIdVC][i].StatusVehicleId = dotsDataVC[mstIdVC][i].Status;
                }


                var data = [];
                data = dotsDataVC[mstIdVC];
                $scope.mVehicleCondition.Points = [];
                for (var i = 0; i < data.length; i++) {
                    data[i].Points.status = data[i].StatusVehicleId;
                    $scope.mVehicleCondition.Points.push(data[i].Points);
                }

                $scope.mVehicleCondition.TypeId = $scope.mEstimationBP.AEstimationBP_WAC_Ext[0].TypeId;
                console.log('ini yg mau disimpen ke backend [mVehicleCondition] ===>',$scope.mVehicleCondition);

                console.log('sebelum masuk grid ===>',dotsDataVC[mstIdVC]);
                for(var a in tmpBiji){
                    for(var b in dotsDataVC[mstIdVC]){
                        if(tmpBiji[a].name == dotsDataVC[mstIdVC][b].ItemName){
                            dotsDataVC[mstIdVC][b].PointId = tmpBiji[a].PointId;
                            dotsDataVC[mstIdVC][b].typeName = 1;
                            dotsDataVC[mstIdVC][b].showPoints = 1;  
                            if(dotsDataVC[mstIdVC][b].PointId == 0){
                                dotsDataVC[mstIdVC][b].showPoints = 0;
                            }
                        }else{
                            if(dotsDataVC[mstIdVC][b].PointId == 0){
                                dotsDataVC[mstIdVC][b].showPoints = 0;
                            }
                        }



                    }
                }


                $scope.disJob = false;
                console.log("tmpBiji",tmpBiji);
                $scope.gridVC.data = dotsDataVC[mstIdVC];

                $scope.mVehicleCondition = $scope.mEstimationBP.AEstimationBP_WAC_Ext[0];
                console.log('$scope.mVehicleCondition | onbeforeedit ====>',$scope.mVehicleCondition);



                $scope.onOrderPekerjaan();
                $scope.calculateforest();

                

            });


            


            console.log('$scope.imageSelVC ===>',$scope.imageSelVC);
            try{
                console.log('try  calculateforest error ');
                $scope.calculateforest();
            }catch(err){
                console.log('catch calculateforest error ===>',err);
                $scope.calculateforest();
            }

            if($scope.estHours == 0 || $scope.estHours){
                $scope.calculateforest();
            }

        }

        $scope.FilterModelTypeSebelumMasukGrid = function (data) {
            // console.log('data ===> ',data)
            // var dataTemp = data;
            // for (var x = 0; x < data.length; x++) {
            //     data[x].PoliceNumber = data[x].PoliceNumber.toUpperCase();
            //     for (var y = 0; y < $scope.optionsModel.length; y++) {
            //         if(parseInt(data[x].ModelCode) == $scope.optionsModel[y].VehicleModelId){
            //             console.log('model gue ===> ',parseInt(data[x].ModelCode) +" == ModelOptions "+ $scope.optionsModel[y].VehicleModelId );
            //             data[x].VehicleModelName = $scope.optionsModel[y].VehicleModelName;
            //             WO.getVehicleTypeById($scope.optionsModel[y].VehicleModelId).then(function(res) {
            //                 $scope.optionsTipe = res.data.Result
            //                 console.log('GetDataType ===>', res.data.Result);
            //                 for (var z = 0; z < $scope.optionsTipe.length; z++) {
            //                     for (var djancuk = 0; djancuk < data.length; djancuk++) {
            //                         console.log('dataTemp ====>',dataTemp);
            //                         console.log('datadoang ====>',data[djancuk]);
            //                         console.log('dataArray ===>',data[djancuk].VehicleTypeId+ " | dataTipe ===> "+ $scope.optionsTipe[z].VehicleTypeId);
            //                         if($scope.optionsTipe[z].VehicleTypeId == data[djancuk].VehicleTypeId){
            //                             $scope.onSelectTipeChanged($scope.optionsTipe[z])
            //                             console.log('Type Gue ===> ',data[djancuk].VehicleTypeId +" == ModelOptions "+$scope.optionsTipe[z].VehicleTypeId);
            //                             data[djancuk].VehicleTypeName = $scope.optionsTipe[z].Description;
            //                         }
            //                     }
            //                 }
            //             }); 
            //         }
            //     }        
            // }
            console.log('ini setelah di filter ===>',data);
            for(var i in data){
                if(data[i].Status == 0){
                    data[i].StatusEstimasi = 'Open'; 
                }else if(data[i].Status == 1){
                    data[i].StatusEstimasi = 'Close';
                }else if(data[i].Status == 2){
                    data[i].StatusEstimasi = 'Draft';
                }


                if(data[i].StatusParts == 0){
                    data[i].StatusPartsTmp = 'Complete'; 
                }else if(data[i].StatusParts == 1){
                    data[i].StatusPartsTmp = 'Menunggu Part Info - Part';
                }else if(data[i].StatusParts == 2){
                    data[i].StatusPartsTmp = 'Menunggu Part Info - Bahan';
                }else if(data[i].StatusParts == 3){
                    data[i].StatusPartsTmp = 'Menunggu Part Info - Part & Bahan';
                }else{
                    data[i].StatusPartsTmp = '-';
                }
            }
            $scope.grid.data = data;
            $scope.loading = false;
        }

        $scope.givePattern = function(item, event) {
            console.log("item", item);
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
                $scope.nilaiKM = item;
                if ($scope.nilaiKM.includes(",")) {
                    $scope.nilaiKM = $scope.nilaiKM.split(',');
                    if ($scope.nilaiKM.length > 1) {
                        $scope.nilaiKM = $scope.nilaiKM.join('');
                    } else {
                        $scope.nilaiKM = $scope.nilaiKM[0];
                    }
                }
                $scope.nilaiKM = parseInt($scope.nilaiKM);
                $scope.mEstimationBP.Km = $scope.mEstimationBP.Km.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                return;
            }
        };


        $scope.onBeforeView = function (data,mode) {
            console.log("mode",mode);
            $scope.masterParts = [];
            $scope.clearAll();

            $scope.restart_digit_odo = 0
            $scope.isKmHistory = 0
            $scope.nilaiKM = 0
            $scope.KMTerakhir = 0

            $scope.restart_digit_odo = 0;
            $scope.model_info_km.show = false;
            // if($scope.flagNgakalinCalculate == 0){
            //     console.log('onBeforeView jalanin clearAll');
            //     $scope.clearAll();
            // }else{
            //     console.log('onBeforeView tidak jalanin clearAll');
            // }
            console.log('$scope.dataGoodWork',$scope.dataGoodWork)
            $scope.activeJustified = undefined;
            $scope.activeJustified = 0;
            
            if(mode == 'view'){
                $scope.formApi.setMode('view');
                $scope.estimationMode = "View";
                console.log('ini on before View dude');
                // $scope.mEstimationBP = data;

                var kondisiestimasi = "";
                if(data.EstimationBPId !== 0){
                    kondisiestimasi = data.EstimationBPId;
                    console.log('ini Estimasi belum WO ===> EstimationBPId => ' ,data.EstimationBPId)
                }else{
                    kondisiestimasi = data.JobId;
                    console.log('ini Estimasi sudah WO ===> JobId => ',data.JobId)
                }
                estimasiBPFactory.getDetail(kondisiestimasi, data.PoliceNumber).then(function (res) {

                    $scope.mEstimationBP = res.data.Result[0];
                    $scope.mEstimationBP.Km = String($scope.mEstimationBP.Km);
                    $scope.mEstimationBP.Km = $scope.mEstimationBP.Km.replace(/\B(?=(\d{3})+(?!\d))/g, ',');


                    $scope.mEstimationBP.AEstimationBP_TaskList_TT = $scope.orderingData($scope.mEstimationBP.AEstimationBP_TaskList_TT, 'EstTaskBPId')

                    for (var i=0; i< $scope.mEstimationBP.AEstimationBP_TaskList_TT.length; i++) {
                        $scope.mEstimationBP.AEstimationBP_TaskList_TT[i].AEstimationBP_PartList = $scope.orderingData($scope.mEstimationBP.AEstimationBP_TaskList_TT[i].AEstimationBP_PartList, 'EstPartsId')
                    }


                    $scope.CekServiceHistory(null, $scope.mEstimationBP.VIN, null, null, 'edit')

                    //SPK
                    // if($scope.mEstimationBP.AEstimationBP_SPKList.length > 0){
                    //     $scope.mEstimationBP.NoPolis      = $scope.mEstimationBP.AEstimationBP_SPKList[0].NoPolis;
                    //     $scope.mEstimationBP.NoLK         = $scope.mEstimationBP.AEstimationBP_SPKList[0].NoLK;
                    //     $scope.mEstimationBP.PeriodePolis = $scope.mEstimationBP.AEstimationBP_SPKList[0].PeriodePolis;
                    // }

                    var tampunganVehicleTypeId = res.data.Result[0].ModelType;
                    // $scope.setRowData_spkList($scope.mEstimationBP.AEstimationBP_SPKList);
                    for (var i = 0; i < $scope.optionsModel.length; i++) {
                        if ($scope.optionsModel[i].VehicleModelId == parseInt($scope.mEstimationBP.ModelCode)){
                            $scope.mEstimationBP.VehicleModelName    = $scope.optionsModel[i].VehicleModelName;
                            $scope.mEstimationBP.ModelCode           = $scope.optionsModel[i].VehicleModelId;
                            WO.getVehicleTypeById($scope.optionsModel[i].VehicleModelId).then(function(res) {
                                // $scope.optionsTipe = res.data.Result;

                                $scope.optionsKatashiki = res.data.Result;

                                for (var i=0; i<$scope.optionsKatashiki.length; i++){
                                    $scope.optionsKatashiki[i].NewDescription = $scope.mEstimationBP.VehicleModelName + ' - ' + $scope.optionsKatashiki[i].KatashikiCode
                                }

                                for (var i=0; i < $scope.optionsKatashiki.length; i++) {
                                    if ($scope.optionsKatashiki[i].KatashikiCode == $scope.mEstimationBP.KatashikiCode) {
                                        $scope.mEstimationBP.KatashikiCodex   = $scope.mEstimationBP.KatashikiCode;

                                    }
                                }

                                if ($scope.mEstimationBP.KatashikiCodex != null && $scope.mEstimationBP.KatashikiCodex != undefined && $scope.mEstimationBP.KatashikiCodex != '') {

                                    WO.GetCVehicleTypeListByKatashiki($scope.mEstimationBP.KatashikiCode).then(function(res) {
                                        // $scope.optionsTipe = res.data.Result[0];
                                        $scope.optionsTipe = res.data.Result;


                                        for (var i=0; i<$scope.optionsTipe.length; i++){
                                            $scope.optionsTipe[i].NewDescription = $scope.optionsTipe[i].Description + ' - ' + $scope.optionsTipe[i].KatashikiCode + ' - ' + $scope.optionsTipe[i].SuffixCode
                                        }

                                        for (var z = 0; z < $scope.optionsTipe.length; z++) {
                                            if($scope.optionsTipe[z].VehicleTypeId == tampunganVehicleTypeId){
                                                $scope.onSelectTipeChanged($scope.optionsTipe[z] , 'bind')
                                                console.log('Type Gue ===> ',tampunganVehicleTypeId +" == ModelOptions "+$scope.optionsTipe[z].VehicleTypeId);
                                            }
                                        }

                                    })


                                }
                            })
                        }
                    }


                    if($scope.mEstimationBP.EstimationNo == null|| $scope.mEstimationBP.EstimationNo == undefined || $scope.mEstimationBP.EstimationNo == ""){
                        $scope.chooseVTypeVC($scope.mEstimationBP.TypeForVC);
                        $scope.filter.vTypeVC = $scope.mEstimationBP.TypeForVC;
                    }




              //ambil diskon dari tipe pembayaran
                if($scope.mEstimationBP.isCash == 1){
                    $scope.choosePembayaran(1);
                    
                    if($scope.mEstimationBP.AEstimationBP_TaskList_TT.length > 0){
                        $scope.disTask = $scope.mEstimationBP.AEstimationBP_TaskList_TT[0].Discount;
                        if($scope.mEstimationBP.AEstimationBP_TaskList_TT[0].AEstimationBP_PartList.length > 0){
                            $scope.disParts = $scope.mEstimationBP.AEstimationBP_TaskList_TT[0].AEstimationBP_PartList[0].Discount;
                        }
                    }
                    console.log('ini pembayaran cash');
                }else{
                    console.log('ini pembayaran Bukan cash');
                    
                    for (i in $scope.optionsLihatInformasi_NamaInsurance_Search){
                        if($scope.optionsLihatInformasi_NamaInsurance_Search[i].InsuranceId == $scope.mEstimationBP.InsuranceId){
                            $scope.mData.InsuranceName = $scope.optionsLihatInformasi_NamaInsurance_Search[i].InsuranceName;
                            $scope.onInsuranceSelected($scope.optionsLihatInformasi_NamaInsurance_Search[i]);
                        }
                    }
                }
                //ambil diskon dari tipe pembayaran
                // untuk request 
                $scope.JobRequest = $scope.mEstimationBP.AEstimationBP_Request_TT;
                // untuk request


                 /*untuk data job*/
                 $scope.gridEstimation.data = [];
                 dataTaskSave = $scope.mEstimationBP.AEstimationBP_TaskList_TT;
                 for (var i=0; i<dataTaskSave.length; i++) {
                    if (dataTaskSave[i].FlatRate == null || dataTaskSave[i].FlatRate == undefined) {
                        dataTaskSave[i].FlatRate = 1
                    } else {
                        dataTaskSave[i].FlatRate = dataTaskSave[i].FlatRate
                    }

                    dataTaskSave[i].SubTotalTask = dataTaskSave[i].FlatRate * dataTaskSave[i].Fare
                }
                 
                 console.log("dataTaskSave",dataTaskSave);

                 // if ($scope.mEstimationBP.DP !== undefined) {
                 // }
                 var tempTotal = 0;
                 var tmpDp = 0;
                 console.log('dataTaskSave buat ag grid ===>',dataTaskSave);
                 for(var a = 0; a < dataTaskSave.length; a++){
                    
                    // var satuan = null;
                    dataTaskSave[a].isDeleted = 0;
                    dataTaskSave[a].totalParts = dataTaskSave[a].AEstimationBP_PartList.length;
                    dataTaskSave[a].EstimationNo = $scope.mEstimationBP.EstimationNo;
                    for (var i = 0; i < $scope.unitData.length; i++) {
                        if ($scope.unitData[i].MasterId == dataTaskSave[a].ProcessId) {
                            dataTaskSave[a].satuan = $scope.unitData[i].Name;
                        }

                        for(var d = 0; d < dataTaskSave[a].AEstimationBP_PartList.length; d++){
                        dataTaskSave[a].AEstimationBP_PartList[d].Qty = dataTaskSave[a].AEstimationBP_PartList[d].qty
                        if ($scope.unitData[i].MasterId == dataTaskSave[a].AEstimationBP_PartList[d].SatuanId) {
                            dataTaskSave[a].AEstimationBP_PartList[d].Name = $scope.unitData[i].Name;
                        }
                        // if (dataTaskSave[a].AEstimationBP_PartList[d].DPRequest != 0){
                        //     $scope.mEstimationBP.DP = dataTaskSave[a].AEstimationBP_PartList[d].DPRequest;
                        // }
                        // else{
                        //     $scope.mEstimationBP.DP = 0;
                        // }

                            AppointmentBpService.getAvailableParts(dataTaskSave[a].AEstimationBP_PartList[d].PartsId, 0, dataTaskSave[a].AEstimationBP_PartList[d].Qty).then(function(res) { 
                                $scope.mEstimationBP.DP = res.data.Result[0].PercentDP
                            })
                        }

                    }
                    // console.log("satuan",dataTaskSave[a].satuan);


                    for(var zz = 0; zz < $scope.paymentData.length; zz++){
                        for(var d = 0; d < dataTaskSave[a].AEstimationBP_PartList.length; d++){
                            if ($scope.paymentData[zz].MasterId == dataTaskSave[a].AEstimationBP_PartList[d].PaidById) {
                                dataTaskSave[a].AEstimationBP_PartList[d].PaidBy = $scope.paymentData[zz].Name;
                            }
                        }
                    }
                    
                    for(var d = 0; d < dataTaskSave[a].AEstimationBP_PartList.length; d++){
                        
                        AppointmentBpService.getAvailableParts(dataTaskSave[a].AEstimationBP_PartList[d].PartsId, 0, dataTaskSave[a].AEstimationBP_PartList[d].qty).then(function(res) { 
                            console.log("data yagn didapat",res.data.Result);
                            var keter;
                            switch (res.data.Result[0].isAvailable) {
                                case 0:
                                    keter = "Tidak Tersedia";
                                    break;
                                case 1:
                                    keter = "Tersedia";
                                    break;
                                case 2:
                                    keter = "Tersedia Sebagian";
                                    break;
                            }
                            console.log("keter",keter,dataTaskSave[a]);

                             for(var a = 0; a < dataTaskSave.length; a++){
                                
                                for(var d = 0; d < dataTaskSave[a].AEstimationBP_PartList.length; d++){
                                    //$scope.countDP();

                                    if(dataTaskSave[a].AEstimationBP_PartList[d].PartsId == res.data.Result[0].PartsId){
                                        dataTaskSave[a].AEstimationBP_PartList[d].Availbility = keter;
                                    }
                                }
                            }
                         },function(err) {
                                 //console.log("err=>", err);
                         });

                        if (dataTaskSave[a].AEstimationBP_PartList[d].MaterialTypeId == 1) {
                            dataTaskSave[a].AEstimationBP_PartList[d].TipeMaterial = "Spare Parts";
                        } else{
                            dataTaskSave[a].AEstimationBP_PartList[d].TipeMaterial = "Bahan";
                        }

                        if (dataTaskSave[a].AEstimationBP_PartList[d].RetailPrice == undefined ){
                            dataTaskSave[a].AEstimationBP_PartList[d].RetailPrice = dataTaskSave[a].AEstimationBP_PartList[d].Price;
                            console.log('ini ga ada retailpricenya');
                        }else{
                            console.log('ini ada retail pricenya');
                        }


                        dataTaskSave[a].AEstimationBP_PartList[d].IsSaveFin = 1;
                        dataTaskSave[a].AEstimationBP_PartList[d].SubTotal = dataTaskSave[a].AEstimationBP_PartList[d].Price * dataTaskSave[a].AEstimationBP_PartList[d].qty - ((dataTaskSave[a].AEstimationBP_PartList[d].Price * dataTaskSave[a].AEstimationBP_PartList[d].qty)* dataTaskSave[a].AEstimationBP_PartList[d].Discount  /100) ;
                        dataTaskSave[a].AEstimationBP_PartList[d].IdFlagParts = Math.floor(Math.random() * 10000) + 1;
                        dataTaskSave[a].AEstimationBP_PartList[d].isSave = 1;
                        if( dataTaskSave[a].EstTaskBPId == dataTaskSave[a].AEstimationBP_PartList[d].EstTaskBPId){

                           tempTotal += dataTaskSave[a].AEstimationBP_PartList[d].SubTotal;
                           console.log("tempTotal",tempTotal);
                           tmpDp = (tempTotal * dataTaskSave[a].AEstimationBP_PartList[0].DownPayment/100);
                           console.log("tmpDp",tmpDp); 
                           dataTaskSave[a].finalDP = tmpDp;
                           console.log("tmpDp2",tmpDp);
                        } 
                     }
 
                     dataTaskSave[a].FlagID = Math.floor(Math.random() * 10000) + 1;

                     dataTaskSave[a].TaskListBPId =  dataTaskSave[a].TaskBPId;
                     $scope.dataGoodWork = angular.copy(dataTaskSave);

                     $scope.getDataGirdWorkView(dataTaskSave[a],dataTaskSave[a].FlagID);
                                $scope.countDP();
                     
                     
                 }
                 // $scope.agGridWorkViewApi.setRowData(dataTaskSave);
                 /*untuk data job*/


                 if($scope.mEstimationBP.EstimationNo == null || $scope.mEstimationBP.EstimationNo == undefined || $scope.mEstimationBP.EstimationNo == ""){
                    $scope.chooseVTypeVC($scope.mEstimationBP.TypeForVC);
                    $scope.filter.vTypeVC = $scope.mEstimationBP.TypeForVC;
                }
         

                    //Vehicle Condition 
                    for (var i = 0; i < $scope.VTypeData.length; i++) {
                        if ($scope.VTypeData[i].MasterId == $scope.mEstimationBP.AEstimationBP_WAC_Ext[0].TypeId) {
                            $scope.filter.vTypeVC = $scope.VTypeData[i];
                        }   
                    }

                    console.log('points ===>',JSON.parse($scope.mEstimationBP.AEstimationBP_WAC_Ext[0].Points));

                    // $scope.chooseVTypeVC($scope.mEstimationBP.AEstimationBP_WAC_Ext[0].TypeId);
                    $scope.areasArrayVC = [];
                    $scope.customHeight = { height: '500px' };
                    mstIdVC = $scope.mEstimationBP.AEstimationBP_WAC_Ext[0].TypeId;
                    // $scope.filter.vTypeVC = mstIdVC;
                    $scope.areasArrayVC = getImgAndPointsVC(mstIdVC);
                    var resu = $scope.mEstimationBP.AEstimationBP_WAC_Ext;

                    console.log('resuu ===>',resu);

                    console.log("$scope.areasArrayVC 1 ====>", $scope.areasArrayVC);
                    dotsDataVC[mstIdVC] = [];
                    var tmpBiji;
                    for (var i in resu) {
                        var getTemp = [];
                        getTemp =  JSON.parse(resu[i].Points);
                        var xtemp = {};


                        tmpBiji = angular.copy(getTemp);
                        console.log('gettemp', getTemp);
                        for (var j in getTemp) {
                            console.log('gettempJstat', getTemp[j]);
                            var xtemp = {};
                            xtemp = angular.copy(getTemp[j]);
                            delete xtemp['status'];
                            delete xtemp['PointId'];
                            xtemp.cssClass = ''; //jancok
                            console.log("xtemp for J : ", xtemp);
                            console.log("scope.areasArrayVC 2 : ", $scope.areasArrayVC);
                            if (typeof _.find($scope.areasArrayVC, xtemp) !== 'undefined') {
                                console.log("merah");
                                console.log("find..", xtemp);
                                console.log("scope.areasArrayVC 3 : ", $scope.areasArrayVC);
                                var idx = _.findIndex($scope.areasArrayVC, xtemp);
                                console.log('idx', idx);
                                xtemp.cssClass = "choosen-area";
                                $scope.areasArrayVC[idx] = xtemp;

                                dotsDataVC[mstIdVC].push({
                                    "ItemId": xtemp.areaid,
                                    "ItemName": xtemp.name,
                                    "Points": xtemp,
                                    "Status": getTemp[j].status
                                });
                            }else{
                                console.log('gamasuk kunyukkk');
                                console.log('$scope.areasArrayVC ====>',$scope.areasArrayVC);
                                console.log('xtemp ==================>',xtemp);
                            }
                        }    
                    }
                    for (var i = 0; i < dotsDataVC[mstIdVC].length; i++) {
                        dotsDataVC[mstIdVC][i].StatusVehicleId = dotsDataVC[mstIdVC][i].Status;
                    }


                    var data = [];
                    data = dotsDataVC[mstIdVC];
                    $scope.mVehicleCondition.Points = [];
                    for (var i = 0; i < data.length; i++) {
                        data[i].Points.status = data[i].StatusVehicleId;
                        $scope.mVehicleCondition.Points.push(data[i].Points);
                    }

                    $scope.mVehicleCondition.TypeId = $scope.mEstimationBP.AEstimationBP_WAC_Ext[0].TypeId;
                    console.log('ini yg mau disimpen ke backend [mVehicleCondition] ===>',$scope.mVehicleCondition);

                    console.log('sebelum masuk grid ===>',dotsDataVC[mstIdVC]);
                    for(var a in tmpBiji){
                        for(var b in dotsDataVC[mstIdVC]){
                            if(tmpBiji[a].name == dotsDataVC[mstIdVC][b].ItemName){
                                dotsDataVC[mstIdVC][b].PointId = tmpBiji[a].PointId;
                                dotsDataVC[mstIdVC][b].typeName = 1;
                                dotsDataVC[mstIdVC][b].showPoints = 1;  
                                if(dotsDataVC[mstIdVC][b].PointId == 0){
                                    dotsDataVC[mstIdVC][b].showPoints = 0;
                                }
                            }else{
                                if(dotsDataVC[mstIdVC][b].PointId == 0){
                                    dotsDataVC[mstIdVC][b].showPoints = 0;
                                }
                            }



                        }
                    }


                    $scope.disJob = false;
                    console.log("tmpBiji",tmpBiji);
                    $scope.gridVC.data = dotsDataVC[mstIdVC];

                    $scope.mVehicleCondition = $scope.mEstimationBP.AEstimationBP_WAC_Ext[0];
                    console.log('$scope.mVehicleCondition | onbeforeedit ====>',$scope.mVehicleCondition);


                    

                });
            
                try{
                    console.log('try  calculateforest error ');
                    $scope.calculateforest();
                }catch(err){
                    console.log('catch calculateforest error ===>',err);
                    $scope.calculateforest();
                }
            

            }else if(mode == 'edit'){
                //$scope.onBeforeEdit(data);
            }
            
        }

        


        


        $scope.onOrderPekerjaan = function(){
            console.log('onOrderPekerjaan | $scope.gridVC.data===>',$scope.gridVC.data);
            var  msgMetodePerbaikan = "Mohon lengkapi Metode Perbaikan pada Vehicle Condition";
            var  msgTingkatKerusakan = "Mohon lengkapi Tingkat Kerusakan pada Vehicle Condition";

            var countMetodePerbaikan = 0;
            var countTingkatKerusakan = 0;
            if($scope.gridVC.data.length > 0){
                for(var x in $scope.gridVC.data){
                    if($scope.gridVC.data[x].StatusVehicleId == null){
                        countMetodePerbaikan++;
                    }else{
                        if($scope.gridVC.data[x].StatusVehicleId == 2442 && ($scope.gridVC.data[x].PointId == null ||$scope.gridVC.data[x].PointId == 0) ){
                            countTingkatKerusakan++;
                        }
                    }
                    
                }
    
                if(countMetodePerbaikan == 0 && countTingkatKerusakan == 0){
                    //ini aman
                }else{
                    if(countMetodePerbaikan > 0 && countTingkatKerusakan == 0){
                        bsNotify.show({
                            size: 'big',
                            type: 'warning',
                            title: msgMetodePerbaikan,
                        });
                    }else if(countMetodePerbaikan == 0 && countTingkatKerusakan > 0){
                        bsNotify.show({
                            size: 'big',
                            type: 'warning',
                            title: msgTingkatKerusakan,
                        });
                    }else{
                        bsNotify.show({
                            size: 'big',
                            type: 'warning',
                            title: msgMetodePerbaikan + '<br>'+ msgTingkatKerusakan,
                        });
                    }
                }
            }

        }

        $scope.doCustomCancel = function(){
            //globalDP = 0;
            $scope.isValidEmail = true;
            $scope.estimationMode = "Grid";
            $scope.formApi.setMode('grid');            
            console.log('ini on before cancel');
            $scope.imageSelVC = undefined;
            $scope.filter.vTypeVC = undefined;
            $scope.flagNgakalinCalculate = 0;
            $scope.isFromHistory = false;
            $scope.dataHistory = [];
        }

        $scope.onKategoriPekerjaanSelect = function(selected){
            console.log('onKategoriPekerjaanSelect ===>',selected);
        }

        $scope.onBeforeNew = function(){
            $scope.formApi.setMode('new');
            $scope.estimationMode = 'Create';
            $scope.activeJustified = undefined;
            $scope.activeJustified = 0;

            $scope.restart_digit_odo = 0
            $scope.isKmHistory = 0
            $scope.nilaiKM = 0
            $scope.KMTerakhir = 0

            $scope.restart_digit_odo = 0;
            $scope.model_info_km.show = false;


            if($scope.estimationBPFilter.NoPolisi !== null){
                $scope.getDataByNoPoliceHistory($scope.estimationBPFilter.NoPolisi);
            }
            $scope.EstimasiBPform.$setPristine();
            $scope.EstimasiBPform.$setUntouched();

            console.log('on before new',$scope.estimationMode);
            $scope.mEstimationBP.EstimationNo = null;
            console.log('$scope.mEstimationBP ===>',$scope.mEstimationBP);
            // if($scope.mEstimationBP.EstimationNo != null || $scope.mEstimationBP.EstimationNo != undefined || $scope.mEstimationBP.EstimationNo != ''){
            //     $scope.disablePoliceNo = true;
            // }else{
                $scope.disablePoliceNo = false;
            // }
            console.log('$scope.disablePoliceNo ====>',$scope.disablePoliceNo);
            $scope.clearAll();

            if($scope.mEstimationBP.CategoryId == null || $scope.mEstimationBP.CategoryId == undefined || $scope.mEstimationBP.CategoryId == ""){
                $scope.mEstimationBP.CategoryId = 2449;
            }
        }


        $scope.clearAll = function(){
            $scope.mEstimationBP = {};
            $scope.mVehicleCondition.Points = [];
            $('.choosen-area').removeClass('choosen-area');
            $scope.gridVC.data = [];
            dotsDataVC[mstIdVC] = [];

            // LC HERE DUDE!!!
            $scope.DisplayDisTask = 0;
            $scope.DisplayDisParts = 0;
            $scope.totalWorkDiscounted = 0;
            $scope.totalFinalTask = 0;
            $scope.PPN = 0;
            $scope.totalEstimasi = 0;
            $scope.totalDp = 0;
            $scope.totalFinalParts =0;
            $scope.totalMaterialDiscounted =0;
            $scope.totalMaterialDiscounted =0;
            $scope.estHours = 0;
            $scope.estDays = 0;
            $scope.totalWork = 0;
            $scope.disTask = 0;
            $scope.totalMaterial = 0;
            $scope.disParts = 0;
            $scope.master = [];
            $scope.dataGoodWork = [];
            dataTaskSave = [];
            $scope.JobRequest = [];
            $scope.disJob = false;
            console.log("master.length",$scope.master.length);
            $scope.agGridWorkViewApi.setRowData($scope.master);
            $scope.gridEstimation.data = [];
            $scope.imageSelVC = undefined;
            // LC HERE DUDE!!!
        }


        $scope.fixDate = function (date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + "-" +
                    ('0' + (date.getMonth() + 1)).slice(-2) + "-" +
                    ('0' + date.getDate()).slice(-2)
                return fix;
            } else {
                return null;
            }
        };


        $scope.ngakalinCalculate = function(){

            $scope.flagNgakalinCalculate = 1;
            var yesterday = new Date();
            var pastDate = yesterday.getDate() - 7;
            yesterday.setDate(pastDate);  
            var today = new Date();

            $scope.estimationBPFilter.DateFrom = $scope.fixDate(yesterday);
            $scope.estimationBPFilter.DateTo = $scope.fixDate(today);
            var tmpPolice;
            if($scope.estimationBPFilter.NoPolisi == null || $scope.estimationBPFilter.NoPolisi == ""){
                tmpPolice = '-';
            }else{
                tmpPolice = $scope.estimationBPFilter.NoPolisi;
            }
                
            var url = "&NoPolisi=" + tmpPolice + "&DateFrom=" + $scope.estimationBPFilter.DateFrom + "&DateTo="+ $scope.estimationBPFilter.DateTo;
            $scope.onSelectTipeChanged(undefined);
            
            estimasiBPFactory.getData(url).then(function (res) {

                if(res.data.Result.length == 0){
                    // $scope.bsAlertConfirm("Data Tidak Ditemukan").then(function (res) {
                    //     if (res) {
                    //         $scope.grid.data = [];
                    //         $scope.onBeforeNew();
                    //         $scope.mEstimationBP.PoliceNumber = $scope.estimationBPFilter.NoPolisi;
                    //         console.log('ini kalo klik ok',res);
                    //     }else{
                    //         console.log("ini kalo klik cancle", res);
                    //     }
                    // });
                }else{
                    $scope.FilterModelTypeSebelumMasukGrid(res.data.Result);
                    $scope.onBeforeEdit(res.data.Result[0]);
                    $scope.doCustomCancel();
                    $scope.grid.data = [];
                    $scope.estimationBPFilter = { NoPolisi: "", DateFrom: null, DateTo : null};

                }
            });
        }

        $scope.isFromHistory = false;
        $scope.dataHistory = [];
        $scope.isKmHistory = 0;
        $scope.KMTerakhir = 0;
        $scope.getDataByNoPoliceHistory = function(nopolisi){
            estimasiBPFactory.getHistoryData(nopolisi).then(
                function (res) {
                    console.log('getHistoryData success ===>',res.data.Result);

                    if(res.data.Result.length > 0){
                        $scope.dataHistory = res.data.Result;
                        $scope.isFromHistory = true;
                        $scope.mEstimationBP.Email               = res.data.Result[0].Email;
                        $scope.mEstimationBP.EngineNo            = res.data.Result[0].EngineNo;
                        $scope.mEstimationBP.ColorCode           = res.data.Result[0].ColorCode;
                        $scope.mEstimationBP.ColorId             = res.data.Result[0].ColorId;
                        $scope.mEstimationBP.ColorName           = res.data.Result[0].ColorName;
                        $scope.mEstimationBP.ContactPerson       = res.data.Result[0].ContactPerson;
                        $scope.mEstimationBP.KatashikiCode       = res.data.Result[0].KatashikiCode;
                        $scope.mEstimationBP.ModelType           = res.data.Result[0].ModelType;
                        $scope.mEstimationBP.PoliceNumber        = res.data.Result[0].PoliceNumber;
                        $scope.mEstimationBP.VIN                 = res.data.Result[0].VIN;
                        $scope.mEstimationBP.VehicleTypeId       = res.data.Result[0].VehicleTypeId;
                        $scope.mEstimationBP.Alamat              = res.data.Result[0].Alamat;
                        $scope.mEstimationBP.PhoneContactPerson1 = res.data.Result[0].PhoneContactPerson1;
                        $scope.mEstimationBP.ModelCode           = res.data.Result[0].ModelCode;
                        $scope.mEstimationBP.VehicleModelId      = res.data.Result[0].VehicleModelId;
                        

                        $scope.isKmHistory =1;
                        if ($scope.isKmHistory >= 1) {
                            $scope.KMTerakhir = res.data.Result[0].Km;
                        }


                        if(res.data.Result[0].isCash == 0){
                            $scope.mEstimationBP.NoPolis             = res.data.Result[0].NoPolis;
                            $scope.mEstimationBP.PolisDateTo         = res.data.Result[0].PolisDateTo;
                            $scope.mEstimationBP.PolisDateFrom       = res.data.Result[0].PolisDateFrom;

                            for (var i in $scope.optionsLihatInformasi_NamaInsurance_Search){
                                if($scope.optionsLihatInformasi_NamaInsurance_Search[i].InsuranceId == res.data.Result[0].InsuranceId){
                                    $scope.mEstimationBP.InsuranceName = $scope.optionsLihatInformasi_NamaInsurance_Search[i].InsuranceName;
                                    $scope.mEstimationBP.InsuranceId = $scope.optionsLihatInformasi_NamaInsurance_Search[i].InsuranceId;
                                    $scope.onInsuranceSelected($scope.optionsLihatInformasi_NamaInsurance_Search[i]);
                                }
                            }
                        }


                        

                        

                        var tampunganVehicleTypeId = res.data.Result[0].VehicleTypeId;
                        for (var i = 0; i < $scope.optionsModel.length; i++) {
                            if ($scope.optionsModel[i].VehicleModelId == parseInt($scope.mEstimationBP.ModelCode)){
                                $scope.mEstimationBP.VehicleModelName    = $scope.optionsModel[i].VehicleModelName;
                                $scope.mEstimationBP.ModelCode           = $scope.optionsModel[i].VehicleModelId;
                                WO.getVehicleTypeById($scope.optionsModel[i].VehicleModelId).then(function(res) {
                                    // $scope.optionsTipe = res.data.Result;

                                    $scope.optionsKatashiki = res.data.Result;

                                    for (var i=0; i<$scope.optionsKatashiki.length; i++){
                                        $scope.optionsKatashiki[i].NewDescription = $scope.mEstimationBP.VehicleModelName + ' - ' + $scope.optionsKatashiki[i].KatashikiCode
                                    }

                                    for (var i=0; i < $scope.optionsKatashiki.length; i++) {
                                        if ($scope.optionsKatashiki[i].KatashikiCode == $scope.mEstimationBP.KatashikiCode) {
                                            $scope.mEstimationBP.KatashikiCodex   = $scope.mEstimationBP.KatashikiCode;

                                        }
                                    }

                                    if ($scope.mEstimationBP.KatashikiCodex != null && $scope.mEstimationBP.KatashikiCodex != undefined && $scope.mEstimationBP.KatashikiCodex != '') {

                                        WO.GetCVehicleTypeListByKatashiki($scope.mEstimationBP.KatashikiCode).then(function(res) {
                                            // $scope.optionsTipe = res.data.Result[0];
                                            $scope.optionsTipe = res.data.Result;


                                            for (var i=0; i<$scope.optionsTipe.length; i++){
                                                $scope.optionsTipe[i].NewDescription = $scope.optionsTipe[i].Description + ' - ' + $scope.optionsTipe[i].KatashikiCode + ' - ' + $scope.optionsTipe[i].SuffixCode
                                            }

                                            for (var z = 0; z < $scope.optionsTipe.length; z++) {
                                                if($scope.optionsTipe[z].VehicleTypeId == tampunganVehicleTypeId){
                                                    $scope.onSelectTipeChanged($scope.optionsTipe[z] , 'bind')
                                                    console.log('Type Gue ===> ',tampunganVehicleTypeId +" == ModelOptions "+$scope.optionsTipe[z].VehicleTypeId);
                                                }
                                            }

                                        })


                                    }
                                })
                            }
                        }
                    }else{
                        $scope.isFromHistory = false;
                    }

                },
                function(err){
                    console.log('getHistoryData success ===>',err)
                }
            );
        }

        $scope.estimationBPFilter = { NoPolisi: "", DateFrom:null, DateTo :null};
        $scope.getData = function () { 
            if($scope.estimationBPFilter.NoPolisi == ""){$scope.estimationBPFilter.NoPolisi = null;}
            if($scope.estimationBPFilter.DateFrom == undefined){$scope.estimationBPFilter.DateFrom = null;}
            if($scope.estimationBPFilter.DateTo   == undefined){$scope.estimationBPFilter.DateTo = null;}
            if(typeof $scope.estimationBPFilter.DateFrom   != 'object'){$scope.estimationBPFilter.DateFrom = null;}
            if(typeof $scope.estimationBPFilter.DateTo   != 'object'){$scope.estimationBPFilter.DateTo = null;}
            console.log('estimationBPFilter ====>', $scope.estimationBPFilter);


           
            if($scope.estimationBPFilter.DateFrom == null || $scope.estimationBPFilter.DateFrom == undefined||  $scope.estimationBPFilter.DateTo == null || $scope.estimationBPFilter.DateTo == undefined ) { //kalo tglnya kosong

                console.log('kalo tanggalnya Kosong from ====>',$scope.estimationBPFilter.DateFrom);
                console.log('kalo tanggalnya Kosong  to  ====>',$scope.estimationBPFilter.DateTo);


                if($scope.estimationBPFilter.NoPolisi == null){ //nopol kosong
                   
                   $scope.grid.data = [];
                   bsNotify.show({
                       title: "Field Mandatory",
                       content: "Filter Tanggal Mulai Dan Tanggal Berakhir Harus diisi",
                       type: 'warning'
                   }); 
                }else{ //nopol ok

                    var url = "&NoPolisi=" + $scope.estimationBPFilter.NoPolisi + "&DateFrom=2004-01-01&DateTo=9999-09-09";                    

                    estimasiBPFactory.getData(url).then(function (res) {

                        if(res.data.Result.length == 0){
                            $scope.bsAlertConfirm("Data Tidak Ditemukan").then(function (res) {
                                if (res) {
                                    $scope.grid.data = [];
                                    $scope.onBeforeNew();
                                    $scope.mEstimationBP.PoliceNumber = $scope.estimationBPFilter.NoPolisi;
                                    console.log('ini kalo klik ok',res);
                                }else{
                                    console.log("ini kalo klik cancle", res);
                                }
                            });
                        }else{
                            $scope.FilterModelTypeSebelumMasukGrid(res.data.Result);
                        }
                    });
                }
        
            }else{//kalo tanggalnya ok

                console.log('kalo tanggalnya ok from ====>',$scope.estimationBPFilter.DateFrom);
                console.log('kalo tanggalnya ok  to  ====>',$scope.estimationBPFilter.DateTo);

                

                if(typeof $scope.estimationBPFilter.DateFrom == 'object'){
                    $scope.estimationBPFilter.DateFrom = $scope.fixDate($scope.estimationBPFilter.DateFrom);
                }
                if(typeof $scope.estimationBPFilter.DateTo == 'object' ){
                    $scope.estimationBPFilter.DateTo = $scope.fixDate($scope.estimationBPFilter.DateTo);
                }


                var tmpPolice;
                if($scope.estimationBPFilter.NoPolisi == null || $scope.estimationBPFilter.NoPolisi == ""){
                   tmpPolice = '-';
                }else{
                    tmpPolice = $scope.estimationBPFilter.NoPolisi;
                }
                   
                var url = "&NoPolisi=" + tmpPolice + "&DateFrom=" + $scope.estimationBPFilter.DateFrom + "&DateTo="+ $scope.estimationBPFilter.DateTo;
                
                estimasiBPFactory.getData(url).then(function (res) {

                    if(res.data.Result.length == 0){
                        $scope.bsAlertConfirm("Data Tidak Ditemukan").then(function (res) {
                            if (res) {
                                $scope.grid.data = [];
                                $scope.onBeforeNew();
                                $scope.mEstimationBP.PoliceNumber = $scope.estimationBPFilter.NoPolisi;
                                console.log('ini kalo klik ok',res);
                            }else{
                                console.log("ini kalo klik cancle", res);
                            }
                        });
                    }else{
                        $scope.FilterModelTypeSebelumMasukGrid(res.data.Result);
                    }
                });
            }
            // else{
            //     console.log("$scope.estimationBPFilter",$scope.estimationBPFilter);
            //     if($scope.estimationBPFilter.DateFrom ==  null || $scope.estimationBPFilter.DateFrom == "" ||$scope.estimationBPFilter.DateTo ==  null || $scope.estimationBPFilter.DateTo == ""){

            //         bsNotify.show({
            //             title: "Field Mandatory",
            //             content: "Filter Tanggal Mulan Dan Tanggal Berakhir Harus diisi",
            //             type: 'error'
            //         });
                    
            //     }else{
            //          if($scope.estimationBPFilter.NoPolisi == null || $scope.estimationBPFilter.NoPolisi == ""){
            //              $scope.estimationBPFilter.NoPolisi = '-';
            //          }
            //         var url = "&NoPolisi=" + $scope.estimationBPFilter.NoPolisi + "&DateFrom=2004-01-01&DateTo=9999-09-09";                    
            //         estimasiBPFactory.getData(url).then(function (res) {
            //             if(res.data.Result.length == 0){
            //                 $scope.bsAlertConfirm("Data Tidak Ditemukan").then(function (res) {
            //                     if (res) {
            //                         $scope.grid.data = [];
            //                         console.log('ini kalo klik ok',res);
            //                     }else{
            //                         console.log("ini kalo klik cancle", res);
            //                     }
            //                 });
            //             }else{
            //                 $scope.FilterModelTypeSebelumMasukGrid(res.data.Result);
            //             }
            //         });
            //     }
            // }
        }


        $scope.onIsCashTrue = function(isCash){
            if(isCash == 0){
                console.log('ini bayarnya asuransi ==>',isCash);
                $scope.mEstimationBP.NoPolis = null;
                $scope.mEstimationBP.PolisDateFrom = null;
                $scope.mEstimationBP.PolisDateTo = null;
                $scope.mEstimationBP.InsuranceId = null;
            }else{
                console.log('ini bayarnya cash ==>',isCash);
            }
        }


        //numeric editornya AG GRID
        // function to act as a class
        function NumericCellEditor() {
        }

        // gets called once before the renderer is used
        NumericCellEditor.prototype.init = function (params) {
            // create the cell
            var maxL = 15
            if (params.column.colId == 'RetailPrice') {
                maxL = 11
            } else if (params.column.colId == 'Discount') {
                maxL = 5
            } 
            this.eInput = document.createElement('input');
            this.eInput.setAttribute('maxLength',maxL);
            console.log('DATAAA1 =>',params.charPress)
            console.log('DATAAA2 =>',params.value)

            if (isCharNumeric(params.charPress)) {
                this.eInput.value = params.charPress;
                console.log('masuk ifff',params.charPress)
            } else {
                if (params.value !== undefined && params.value !== null) {
                    this.eInput.value = params.value;
                    console.log('masuk elseee',params.value)
                }
            }

            var that = this;
            this.eInput.addEventListener('keypress', function (event) {
                if (!isKeyPressedNumeric(event)) {
                    
                    if(event.charCode == 46){
                        console.log('ini titik jancokkkkk')
                    }else{
                        console.log('masuk ifff isKeyPressedNumeric',event)
                        that.eInput.focus();
                        if (event.preventDefault) event.preventDefault();
                    }
                } else if (that.isKeyPressedNavigation(event) ){
                    console.log('masuk elseee',event)

                    event.stopPropagation();
                }else if(that.isKeyPressedDot(event)){
                    console.log('ini titikkkkkkk')
                }else{
                    console.log('ini benerr',event);
                }
            });

            // only start edit if key pressed is a number, not a letter
            var charPressIsNotANumber = params.charPress && ('1234567890'.indexOf(params.charPress) < 0);
            this.cancelBeforeStart = charPressIsNotANumber;
        };

        NumericCellEditor.prototype.isKeyPressedNavigation = function (event){
            return event.keyCode===39
                || event.keyCode===37;
        };


        NumericCellEditor.prototype.isKeyPressedDot = function (event){
            return event.keyCode==46;
        };


        // gets called once when grid ready to insert the element
        NumericCellEditor.prototype.getGui = function () {
            console.log('getGui ===>',this.eInput)
            return this.eInput;
        };

        // focus and select can be done after the gui is attached
        NumericCellEditor.prototype.afterGuiAttached = function () {
            this.eInput.focus();
        };

        // returns the new value after editing
        NumericCellEditor.prototype.isCancelBeforeStart = function () {
            return this.cancelBeforeStart;
        };

        // example - will reject the number if it contains the value 007
        // - not very practical, but demonstrates the method.
        NumericCellEditor.prototype.isCancelAfterEnd = function () {
            var value = this.getValue();
            return value.indexOf('007') >= 0;
        };

        // returns the new value after editing
        NumericCellEditor.prototype.getValue = function () {
            return this.eInput.value;
        };

        // any cleanup we need to be done here
        NumericCellEditor.prototype.destroy = function () {
            // but this example is simple, no cleanup, we could  even leave this method out as it's optional
        };

        // if true, then this editor will appear in a popup 
        NumericCellEditor.prototype.isPopup = function () {
            // and we could leave this method out also, false is the default
            return false;
        };
        //numeric editornya AG GRID

        

        

        

        // function Galang end

        

        $scope.cekHistoryKM = function() {
            console.log($scope.mEstimationBP.VIN)
            console.log($scope.mEstimationBP.Km)
            console.log('woooo')

            // $scope.mEstimationBP.Km = null;

            var vin = $scope.mEstimationBP.VIN
            var category = 0; // KATEGORI BP
            var startDate = '2001-01-1';
            var endDate = new Date();

            var d = endDate.getDate();
            var m = endDate.getMonth() + 1;
            var y = endDate.getFullYear();

            endDate = y + '-' + m + '-' + d;
            console.log('endDate ==>', endDate);


            // var tmpKm = angular.copy($scope.mEstimationBP.Km).toString();
            // var backupKm = angular.copy($scope.mEstimationBP.Km);
            // if (tmpKm.includes(",") == true) {
            //     tmpKm = tmpKm.split(',');
            //     if (tmpKm.length > 1) {
            //         tmpKm = tmpKm.join('');
            //     } else {
            //         tmpKm = tmpKm[0];
            //     }
            // }
            // tmpKm = parseInt(tmpKm);
            // $scope.mEstimationBP.Km = tmpKm;

            $scope.CekServiceHistory(category, vin, startDate, endDate, 'new')

            

        }

        $scope.CekServiceHistory = function(category, vin, startDate, endDate, mode_from) {
            if (mode_from != 'new') {
                vin = $scope.mEstimationBP.VIN
                category = 0; // KATEGORI BP
                startDate = '2001-01-1';
                endDate = new Date();

                var d = endDate.getDate();
                var m = endDate.getMonth() + 1;
                var y = endDate.getFullYear();

                endDate = y + '-' + m + '-' + d;
                console.log('endDate ==>', endDate);

                var tmpKm = angular.copy($scope.mEstimationBP.Km).toString();
                var backupKm = angular.copy($scope.mEstimationBP.Km);
                if (tmpKm.includes(",") == true) {
                    tmpKm = tmpKm.split(',');
                    if (tmpKm.length > 1) {
                        tmpKm = tmpKm.join('');
                    } else {
                        tmpKm = tmpKm[0];
                    }
                }
                tmpKm = parseInt(tmpKm);

                $scope.nilaiKM = tmpKm
            }
            estimasiBPFactory.getDataHistoryService(category, vin, startDate, endDate).then(
                function(res) {

                    // for(var i in res.data){
                    //     var tmpArray = [];
                    //     for(var j = 0; j<res.data[i].VehicleJobService.length; j++){
                    //         if(res.data[i].VehicleJobService[j].KTPNo == null || res.data[i].VehicleJobService[j].KTPNo == undefined){
                    //             res.data[i].VehicleJobService[j].KTPNo = ''
                    //         }
                    //         if(!(res.data[i].VehicleJobService[j].KTPNo.includes('/'))){
                    //             if(res.data[i].VehicleJobService[j].EmployeeName !== null){
                    //                 tmpArray.push(res.data[i].VehicleJobService[j].EmployeeName);
                    //             }
                    //         }
                            
                    //     }
                    //     tmpArray = $scope.getUnique(tmpArray);
                    //     res.data[i].namaTeknisi = tmpArray.join(', ');
                    // }

                    console.log('DATA HISTORY ====>', res.data);
                    $scope.mDataVehicleHistory = res.data;
                    $scope.isKmHistory = $scope.mDataVehicleHistory.length;

                    if ($scope.isKmHistory >= 1) {

                        $scope.KMTerakhir = $scope.mDataVehicleHistory[0].Km;

                        $scope.restart_digit_odo = 0
                        estimasiBPFactory.GetResetCounterOdometer(vin).then(function(res) {
                            $scope.restart_digit_odo = res.data

                            if (mode_from == 'new') {

                                var tmpKm = angular.copy($scope.mEstimationBP.Km).toString();
                                var backupKm = angular.copy($scope.mEstimationBP.Km);
                                if (tmpKm.includes(",") == true) {
                                    tmpKm = tmpKm.split(',');
                                    if (tmpKm.length > 1) {
                                        tmpKm = tmpKm.join('');
                                    } else {
                                        tmpKm = tmpKm[0];
                                    }
                                }
                                tmpKm = parseInt(tmpKm);
                                // $scope.mEstimationBP.Km = tmpKm;

                                if (tmpKm < $scope.KMTerakhir && $scope.KMTerakhir < 999999) {
                                    $scope.nilaiKM = 0
                                    $scope.mEstimationBP.Km = null
                                }
                            }
                            
                        })

                    }

                    console.log('Data History', $scope.isKmHistory);
                    console.log("kesini ga?");

                    console.log("mDataVehicleHistory=> CEK SERVICE TERAKHIR ", $scope.mDataVehicleHistory);
                },
                function(err) {}
            );
        }


        // ------------------------------- ordering data ------------------------------------- start

        $scope.orderingData = function(data, orderby_what) {
            //orderby_what sebaiknya int value
            data = data.sort(function(a, b){
                var A=a[orderby_what], B=b[orderby_what]
                return A-B //sort by date ascending
                // return B-A //sort by date Descending

            })
            return data
        }
        // ------------------------------- ordering data ------------------------------------- end






        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { displayName: 'Estimasi BP Id', name: 'Estimasi BP Id', field: 'EstimationBPId', visible: false },
                { displayName: 'No. Estimasi', name: 'No. Estimasi', field: 'EstimationNo', width:'180'},
                { displayName: 'Pembuat', name: 'Pembuat', field: 'LastModifiedEmployeeName', width:'150' },
                { displayName: 'No. Polisi', name: 'No. Polisi', field: 'PoliceNumber', width:'100'},
                { displayName: 'Model', name: 'Model', field: 'VehicleModelName', width:'100'},
                { displayName: 'Tipe', name: 'Tipe', field: 'VehicleTypeName', width:'220' },
                { displayName: 'Tanggal Estimasi', name: 'Tanggal Estimasi', field: 'EstimationDate', cellFilter: 'date:\"dd-MM-yyyy\"' , width:'130'},        
                { displayName: 'Status Estimasi', name: 'Status Estimasi', field: 'StatusEstimasi', width:'130' },
                { displayName: 'Status Parts', name: 'Status Parts', field: 'StatusPartsTmp', width:'250' },
        
            ]
        };


        $scope.cellTemplatePointVehicleCondition = '<div ng-if="(row.entity.StatusVehicleId !== undefined && row.entity.StatusVehicleId !== null && row.entity.typeName == 1)" class="btn-group ui-grid-cell-contents" ng-repeat="item in grid.appScope.PointsVehicle">' +
            '<label>' +
            '<input ng-model="row.entity.PointId" type="radio" ng-change="grid.appScope.checkTaskByVehicleCondtion(grid.appScope.gridVC.data,0,row.entity.PointId)" ng-value={{item.MasterId}} style="width:20px">' +
            '{{item.Name}}</label>' +
            '</div>';

        $scope.cellTemplateStatusVehicleCondition = '<div class="btn-group ui-grid-cell-contents" ng-repeat="item in grid.appScope.StatusVehicle">' +
            '<label>' +
            '<input ng-model="row.entity.StatusVehicleId" type="radio" ng-change="grid.appScope.checkTaskByVehicleCondtion(grid.appScope.gridVC.data,1,row.entity.StatusVehicleId)" ng-value={{item.MasterId}} style="width:20px">' +
            '{{item.Name}}</label>' +
            '</div>';
        $scope.gridVC = {
            enableSorting: true,
            enableRowSelection: false,
            // added by sss on 2017-10-11
            // rowHeight: 30,
            //
            // multiSelect: true,
            // enableSelectAll: true,
            // enableCellEdit: true,
            // showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            onRegisterApi: function (gridApi) {
                $scope.gridApiVC = gridApi;
            },
            columnDefs: [
                { name: 'JobWACExtId', field: 'JobWACExtId', width: '7%', visible: false },
                { name: 'Nama Item', field: 'ItemName', width: '30%', enableCellEdit: false },
                {
                    name: 'Metode Perbaikan',
                    field: 'StatusVehicleId',
                    // width: 278,
                    width: '45%',
                    enableCellEdit: false,
                    cellTemplate: $scope.cellTemplateStatusVehicleCondition,
                },
                {
                    name: 'Tingkat Kerusakan',
                    field: 'PointId',
                    // width: 200,
                    width: '25%',
                    enableCellEdit: false,
                    cellTemplate: $scope.cellTemplatePointVehicleCondition
                },
                {
                    name: 'typeName',
                    field: 'typeName',
                    enableCellEdit: false,
                    visible: false,
                },

            ]
        };

        $scope.listEstimasiYangBelumDigunakan = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { displayName: 'Appointment Diskon Id', name: 'Appointment Diskon Id', field: 'AppointmentDiscountId', visible: false },
                { displayName: 'No. Estimasi', name: '', field: 'NoEstimasi', visible: true },
                { displayName: 'Tanggal Estimasi', name: 'Persentage Diskon', field: 'Discount', visible: true, },
                { displayName: 'No. SPK', name: 'Outlet ID', field: 'OutletId', visible: true, },

            ]
        };


    });


