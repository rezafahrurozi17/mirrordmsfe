angular.module('app')
    .factory('estimasiBPFactory', function ($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function (param) {
                if(param == undefined){
                    var res = $http.get('/api/as/AEstimationBP/Get/?start=1&limit=100000');
                }else{
                    var res = $http.get('/api/as/AEstimationBP/Get/?start=1&limit=100000' + param);
                }
                return res;
            },
            getDetail: function (EstId,PoliceNumber) {
            // getDetail: function (EstId,PoliceNumber) {
                var res = $http.get('/api/as/AEstimationBP/GetDetail/?EstId='+ EstId+ '&PoliceNumber=' + PoliceNumber+'&Ref=0');
                // var res = $http.get('/api/as/AEstimationBP/GetDetail/?EstId='+ EstId+'&PoliceNumber='+PoliceNumber);
                return res;
                
            },


            

            getHistoryData: function(noPolisi) {
                var url = '/api/as/AEstimationBP/GetNopolAfterWO?NoPolisi=' + noPolisi
                var res = $http.get(url);
                return res;
            },

            getVehicleTypeColorById: function(id) {
                var url = '/api/crm/GetMVehicleTypeColorById/' + id
                var res = $http.get(url);
                return res;
            },
            NewGetMVehicleTypeColorById: function(id) {
                var url = '/api/crm/NewGetMVehicleTypeColorById/' + id
                var res = $http.get(url);
                return res;
            },
            getDataSpk: function () {
                var res = $http.get('/api/as/AEstimationBP/GetListSPK');
                return res;
            },
            create: function (data) {
                console.log('ini factory post data | create ===>', [data])
                return $http.post('/api/as/AEstimationBP/SubmitData', [data]);
            },

            createDraft: function (data) {
                console.log('ini factory post data ===>', [data])
                return $http.post('/api/as/AEstimationBP/SubmitDataDraft', [data]);
                // return $http.post('/api/as/MasterDiskon', [data]);
            },

            UpdateDraft: function (data) {
                console.log('ini factory post data ===>', [data])
                return $http.put('/api/as/AEstimationBP/UpdateDataDraft', [data]);
                // return $http.post('/api/as/MasterDiskon', [data]);
            },


            update: function (data) {
                console.log("ini factory update data | update ===>", data);
                return $http.put('/api/as/AEstimationBP/UpdateData', [data]);
            },
            getInsuranceComboBox: function () {
                console.log("factory.getInsuranceComboBox");
                var url = '/api/fe/Insurance/SelectData?start=0&limit=0&FilterData=0';
                var res = $http.get(url);
                return res;
            },
            getWoCategory: function () {
                var catId = 2039;
                // var catId = 1012;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId + '&Flag=1');
                console.log('resnya pause=>', res);
                return res;
            },
            getDataPartsEstimasiBP: function(Key, servicetype, type) {
                console.log("keyyyyyy", Key);
                // api/as/AEstimationBPCheckParts/GetListParts?PartsCode=0888&ServiceTypeId=1&PartsClassId=1
                var res = $http.get('/api/as/AEstimationBPCheckParts/GetListParts?PartsCode=' + Key + '&ServiceTypeId=' + servicetype + '&PartsClassId=' + type);
                // console.log("/api/as/TaskLists?KatashikiCode="+Katashiki+"&Name="+Key);
                // console.log("resnya",res);
                return res;
            },
            getDataPartsDetailEstimasiBP: function(Key) {
                console.log("keyyyyyy GetDetailParts", Key);
                // api/as/AEstimationBPCheckParts/GetListParts?PartsCode=0888&ServiceTypeId=1&PartsClassId=1
                var res = $http.get('/api/as/AEstimationBPCheckParts/GetDetailParts?PartsId=' + Key);
                // console.log("/api/as/TaskLists?KatashikiCode="+Katashiki+"&Name="+Key);
                // console.log("resnya",res);
                return res;
            },
            cetakEstimation: function(OutletId,EstimationBPId) {
                console.log("EstimationBPId", EstimationBPId);
                return $http.get('/api/as/PrintMasterEstimationBPBaru/'+OutletId+'/'+EstimationBPId);
            },
            
            getVehicleConditionTask: function(data, tipeMobil) {
                console.log('ini di factory | getVehicleConditionTask | data ===>',data, tipeMobil);
                return $http.get('/api/as/TaskListBpFilter/' + data.ItemId + '/' + data.PointId + '/' + data.StatusVehicleId + '/' + tipeMobil);
                // return $http.get('/api/as/TaskListBpFilter/11/2441/2443');
            },

            delete: function(id){
                var arrayIdDeleted = [];
                for(var i in id){
                    arrayIdDeleted.push({EstimationBPId : id[i]})
                }
                console.log('id in factory ==>',id)
                console.log('arrayIdDeleted in factory ==>',arrayIdDeleted)
                return $http.put('/api/as/AEstimationBP/DeleteData', arrayIdDeleted);
            },
            getVehicleMList: function() {
                return $http.get('/api/crm/GetCVehicleModel/');
            },
            GetResetCounterOdometer: function(vin) {
                // api/ct/GetResetCounterOdometer/{VIN}
                return $http.get('/api/ct/GetResetCounterOdometer/' + vin)
            },
            getDataHistoryService: function(category, vin, StartDate1, StartDate2) {
                // var res=$http.get('/api/as/Jobs/'+category+'/'+vehicleid);
                // var res=$http.get('/api/ct/GetVehicleService/'+category+'/'+vehicleid+'');
                var res = $http.get('/api/ct/GetVehicleService/' + category + '/' + vin + '/' + StartDate1 + '/' + StartDate2);

                console.log('res dari wo history crm=>', res);
                return res;
            },

            getT1: function() {
                // var catId = 2023;
                var res = $http.get('/api/as/GetT1');
                // console.log('resnya pause=>',res);
                return res;
            },

        }
    });
