function NumericCellEditorPct() {
}


function getCharCodeFromEvent(event) {
    event = event || window.event;
    return (typeof event.which == "undefined") ? event.keyCode : event.which;
}

function isCharNumeric(charStr) {
    return !!/\d/.test(charStr);
}

function isKeyPressedNumeric(event) {
    var charCode = getCharCodeFromEvent(event);
    var charStr = String.fromCharCode(charCode);
    return isCharNumeric(charStr);
}

// gets called once before the renderer is used
NumericCellEditorPct.prototype.init = function (params) {
    // create the cell
    this.eInput = document.createElement('input');
    this.eInput.type = 'number';
    this.eInput.style.border = "none";
    this.eInput.tabIndex = "0";
    this.eInput.max = '0';
    this.eInput.max = '100';
    this.eInput.style.width = params.actualWidth;
    // this.eInput.innerHTML = '<input type="text" style="height:100px;">'

    if (isCharNumeric(params.charPress)) {
        this.eInput.value = params.charPress;
    } else {
        if (params.value !== undefined && params.value !== null) {
            this.eInput.value = params.value;
        }
    }

    var that = this;
    this.eInput.addEventListener('keypress', function (event) {
        if (!isKeyPressedNumeric(event)) {
            // console.log("that.eInput=>",that.eInput.children);
            // that.eInput.children[0].focus();
            that.eInput.focus();
            if (event.preventDefault) event.preventDefault();
        } else if (that.isKeyPressedNavigation(event)){
            event.stopPropagation();
        }
    });

    // only start edit if key pressed is a number, not a letter
    var charPressIsNotANumber = params.charPress && ('1234567890'.indexOf(params.charPress) < 0);
    this.cancelBeforeStart = charPressIsNotANumber;
};

NumericCellEditorPct.prototype.isKeyPressedNavigation = function (event){
    return event.keyCode===39
        || event.keyCode===37;
};


// gets called once when grid ready to insert the element
NumericCellEditorPct.prototype.getGui = function () {
    return this.eInput;
};

// focus and select can be done after the gui is attached
NumericCellEditorPct.prototype.afterGuiAttached = function () {
    this.eInput.focus();
};

// returns the new value after editing
NumericCellEditorPct.prototype.isCancelBeforeStart = function () {
    return this.cancelBeforeStart;
};

// example - will reject the number if it contains the value 007
// - not very practical, but demonstrates the method.
NumericCellEditorPct.prototype.isCancelAfterEnd = function () {
    // var value = this.getValue();
    // return value.indexOf('007') >= 0;
    return false;
};

// returns the new value after editing
NumericCellEditorPct.prototype.getValue = function(){
    var val = parseInt(this.eInput.value);
    val = ( val>100 ? 100 : (val<0 ? 0 : val) );
    return val;
};

// any cleanup we need to be done here
NumericCellEditorPct.prototype.destroy = function () {
    // but this example is simple, no cleanup, we could  even leave this method out as it's optional
};

// if true, then this editor will appear in a popup 
NumericCellEditorPct.prototype.isPopup = function () {
    // and we could leave this method out also, false is the default
    return false;
};