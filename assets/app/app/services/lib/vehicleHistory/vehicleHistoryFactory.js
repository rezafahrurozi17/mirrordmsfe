angular.module('app')
    .factory('VehicleHistory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(category, vin, StartDate1, StartDate2) {
                // var res=$http.get('/api/as/Jobs/'+category+'/'+vehicleid);
                // var res=$http.get('/api/ct/GetVehicleService/'+category+'/'+vehicleid+'');
                var res = $http.get('/api/ct/GetVehicleService/' + category + '/' + vin + '/' + StartDate1 + '/' + StartDate2);

                console.log('res dari wo history crm=>', res);
                return res;
            },
        }
    });