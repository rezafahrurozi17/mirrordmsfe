angular.module('app').directive('vehicleHistory', function(VehicleHistory, bsNotify) {
    return {
        restrict: 'E',
        templateUrl: 'app/services/lib/vehicleHistory/vehicleHistory.html',
        scope: {
            vin: '=',
            category: '=?',
            startDate: '=?',
            endDate: '=?',
            value: '=?'
        },
        link: function(scope) {

            // scope.category = "2";
            scope.dateOptions = {
                startingDay: 1,
                format: 'dd/MM/yyyy',

                // disableWeekend: 1
            };
            scope.change = function() {
                if (_.isEmpty(scope.category) || _.isNull(scope.category) || _.isUndefined(scope.category)) {
                    return bsNotify.show({
                        size: 'big',
                        title: 'Mohon Isi Kategori Pekerjaan',
                        text: 'I will close in 2 seconds.',
                        timeout: 5000,
                        type: 'danger'
                    });
                }

                if (_.isUndefined(scope.startDate) || scope.startDate == "" || _.isNull(scope.startDate) || scope.startDate == "Invalid Date") {
                    scope.startDate = "-";

                    scope.mDataVehicleHistory = [];
                    scope.value = angular.copy(scope.mDataVehicleHistory);

                    return bsNotify.show({
                        size: 'big',
                        title: 'Data Riwayat Service Tidak Ditemukan',
                        text: 'I will close in 2 seconds.',
                        timeout: 5000,
                        type: 'danger'
                    });
                }
                var tmpDate = scope.startDate;
                var tmpMonth = scope.startDate;
                console.log("tmpMonth", tmpMonth);
                var tmpTime = scope.startDate;
                // if (tmpDate.getMonth() < 10) {
                // tmpMonth = "0" + (tmpDate.getMonth() + 1);
                // } else {
                tmpMonth = (tmpDate.getMonth() + 1).toString();
                // }
                var ini = tmpDate.getFullYear() + "-" + (tmpMonth[1] ? tmpMonth : "0" + tmpMonth[0]) + "-" + tmpDate.getDate();
                var d = new Date(ini);
                console.log('data yang diinginkan', ini);
                scope.startDate = ini;

                if (_.isUndefined(scope.endDate) || scope.endDate == "" || _.isNull(scope.endDate) || scope.endDate == "Invalid Date") {
                    scope.endDate = "-";

                    scope.mDataVehicleHistory = [];
                    scope.value = angular.copy(scope.mDataVehicleHistory);

                    return bsNotify.show({
                        size: 'big',
                        title: 'Data Riwayat Service Tidak Ditemukan',
                        text: 'I will close in 2 seconds.',
                        timeout: 5000,
                        type: 'danger'
                    });
                }

                // var finalDate
                // var yyyy = tmpDate.getFullYear().toString();
                // var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                // var dd = tmpDate.getDate().toString();
                // finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + " " + tmpTime;

                var tmpDate2 = scope.endDate;
                var tmpMonth2 = scope.endDate;
                console.log("tmpMonth2", tmpMonth2);
                var tmpTime2 = scope.endDate;

                // if (tmpDate2.getMonth() < 10) {
                //     tmpMonth2 = "0" + (tmpDate2.getMonth() + 1);
                // } else {
                tmpMonth2 = (tmpDate2.getMonth() + 1).toString();
                // }
                var ini2 = tmpDate2.getFullYear() + "-" + (tmpMonth2[1] ? tmpMonth2 : "0" + tmpMonth2[0]) + "-" + tmpDate2.getDate();
                var d2 = new Date(ini2);
                console.log('data yang diinginkan 2', ini2);
                scope.endDate = ini2;

                VehicleHistory.getData(scope.category, scope.vin, scope.startDate, scope.endDate).then(
                    function(res) {
                        if (res.data.length == 0) {
                            bsNotify.show({

                                size: 'big',
                                title: 'Data Riwayat Service Tidak Ditemukan',
                                text: 'I will close in 2 seconds.',
                                timeout: 5000,
                                type: 'danger'
                            });
                            scope.mDataVehicleHistory = [];
                            scope.value = angular.copy(scope.mDataVehicleHistory);
                        } else {
                            scope.mDataVehicleHistory = res.data;
                            scope.value = angular.copy(scope.mDataVehicleHistory);
                        }

                        console.log("mDataVehicleHistory=>", scope.mDataVehicleHistory);
                    },
                    function(err) {}
                );
            };
        }
    };
});