angular.module('app').directive('chipSamainmenu', function() {
    return {
        restrict: 'E',
        templateUrl: 'app/services/lib/chipSaMainMenu/chipSaMainMenu.html',
        scope: {
            policenumber: '=',
            timeStart: '=',
            modelname: '=',
            timeFinish: '=',
            status: '=',
            place: '=',
            employeesa: '=',
            midcolor: '=',
            iswaiting: '=?',
            ismultijob:"=?",
            isdatasatelite:"=?",
            ispaid:"=?",
            isokbilling:"=?",
            nocallcust:"=?",
            isloginonsatelite:"=?",
            isbelumlunas:"=?"
        },
        link: function(scope) {
            // console.log('midcolor', scope.midcolor);
            scope.bgColor = {
                // string
                'background': typeof scope.midcolor === 'string' ? scope.midcolor : "lightgray"
            };
            // console.log('scope chipsamainmenu 2', scope.status);
            if (scope.isloginonsatelite == 1){
                if (scope.status == 15 && scope.isokbilling == 1) {
                    scope.showPhone = true;
                } else {
                    scope.showPhone = false;
                };
            } else {
                if (scope.status == 15) {
                    scope.showPhone = true;
                } else {
                    scope.showPhone = false;
                };
            }
            
            if (scope.status == 16) {
                scope.showCheck = true;
            } else {
                scope.showCheck = false;
            };
            if (scope.iswaiting == 1) {
                scope.showUser = true;
            } else {
                scope.showUser = false;
            };
            if (scope.ismultijob == 1){
                scope.showMulti = true;
            } else {
                scope.showMulti = false;
            }
            if (scope.isdatasatelite == 1){
                scope.dataSatelite = true;
            } else {
                scope.dataSatelite = false;
            }
            
            if (scope.status == 17 && scope.ispaid == 1){
                scope.ReadyGateout = true
            } else {
                scope.ReadyGateout = false
            }

            if (scope.nocallcust == 1){
                scope.readycallcust = false
            } else {
                scope.readycallcust = true
            }

            if (scope.status == 17 && scope.isbelumlunas == 1) {
                scope.showDollar = true
            } else {
                scope.showDollar = false
            }
        },
    };
});