angular.module('app').directive('step', function() {
    return {
        restrict: "A",
        require: "ngModel",
        link: function(scope, element, attributes, ngModel) {
            ngModel.$validators.step = function() {
                console.log("masuk testing directive");
                return element[0].validity.stepMismatch === false;
            };
        }
    };
});