angular.module('app')
  .factory('translator', function() {
    return {
      arraySortforBSFormTree: function(arr, stringId, stringParentId) {
        // console.log(arr);
        var result = [];

        do {
          //comment at 2018-05-09 
          // arr.push(arr.shift());
        } while (arr[0][stringParentId] != 0);

        arr.forEach(function(item) {
          // console.log(item);
          if (item[stringParentId] == 0) {
            item.$$treeLevel = 0;
            result.push(item);
          } else {
            var target = result.map(function(e) {
              return e[stringId];
            }).indexOf(item[stringParentId]);

            if (target >= 0) {
              item.$$treeLevel = result[target].$$treeLevel + 1;

              result.splice(target + 1, 0, item);
            }
          }
        });

        return result;
      },
    }
  });
