angular.module('app').directive('chipTechnicianmainmenu', function() {
  return {
    restrict: 'E',
    templateUrl: 'app/services/lib/chipTechnicianMainMenu/chipTechnicianMainMenu.html',
    scope: {
      policenumber: '=',
      date: '=',
      modelname: '=',
      time: '=',
      status: '=',
      jobtype: '=',
      planfinish: '=',
      timeplanfinish: '=',
      employeesa : '=',
      midcolor : '=',
      iswaiting : '=',
      jmltask : '=',
      returnjob : '=',
      jobtwc : '=',
      jobpwc : '=',
      waitingpart : '=',
      listtech : '='

    },
    link: function(scope) {

      
      scope.bgColor = {
        'background': typeof scope.midcolor === 'string' ? scope.midcolor : "lightgray"
      };
      if(scope.status == 5 || scope.status == 7 || scope.status == 9 || scope.status == 11){
        scope.iconPlay = true;
      } else {
        scope.iconPlay = false;
      };

      if(scope.status == 6){
        scope.iconPause = true;
      } else {
        scope.iconPause = false;
      };

      if(scope.status == 8){
        scope.iconOff = true;
      } else {
        scope.iconOff = false;
      };

      if(scope.status >=5 ){
        scope.iconDispatch = true;
      } else {
        scope.iconDispatch = false;
      };

      if(scope.iswaiting == 1){
        scope.iconCustomer = true;
      } else {
        scope.iconCustomer = false;
      };

      if(scope.jmltask > 1){
        scope.iconMulti = true;
      } else {
        scope.iconMulti = false;
      };

      if(scope.returnjob > 0){
        scope.iconReturnJob = true;
      } else {
        scope.iconReturnJob = false;
      };

      if(scope.jobtwc > 0 || scope.jobpwc > 0){
        scope.iconWarranty = true;
      } else {
        scope.iconWarranty = false;
      };

      if(scope.waitingpart > 0){
        scope.iconWaitingPart = true;
      } else {
        scope.iconWaitingPart = false;
      }

      


    }
  };
});
