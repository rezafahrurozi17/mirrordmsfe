angular.module('app')
    .controller('KatalogController', function($scope, $http, CurrentUser, KatalogFactory, $timeout,bsAlert) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $timeout( function(){ $('body').addClass('hidden-menu'); $scope.loading = true;},500)
        });
        $scope.$on("$destroy", function() {
            $timeout( function(){ $('body').removeClass('hidden-menu');},200);
        });

    });