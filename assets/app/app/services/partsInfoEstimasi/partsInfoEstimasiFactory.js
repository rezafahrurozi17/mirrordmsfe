angular.module('app')
  .factory('PartsInfoEstimasiFactory', function($http,bsNotify,bsAlert) {
    var MRHeader = {};
    return {
      showAlert: function(msg) {
        bsNotify.show(msg);
      },
      setMRHeader: function(data) {
        for (var k in data) MRHeader[k] = data[k];
      },
      getMRHeader: function() {
          return MRHeader;
      },
      headerState: function(data) {
        console.log("Data Param For Get HeaderState : ", data);
        var res = $http.get('/api/as/PartMR/HeaderState', { params: { mrid: data.MaterialRequestId } });
        console.log('res. headerState =>', res);
        return res;
      },

      update: function (data) {
        console.log("ini factory update data ===>", data);
        // var dataTmp = data;
        // console.log("dataTmp",dataTmp);
        // return $http.put('/api/as/MasterDiskon', [data]); // sekarang update sama di delete disatuin pake post
        

        return $http.put('/api/as/PartMREstimationBP', [data]);
        // return $http.put('/api/as/AEstimationBP/UpdateData', [data]);
      },

      getDetailMaterialOneWarehouse: function(part) {
        console.log("P code : ", part);
        var res = $http.get('/api/as/PartGlobal', {
            params: {
                PartsCode: part.pcode,
                WHId: part.WarehouseId
            }
        });
        console.log('hasil=.>', res);
        return res;
    },

    
      getCategories: function() {
        var res=$http.get('/api/as/GlobalMaster/Categories');
        return res;
      },
      getData: function() {
        // var res = $http.get('/api/as/AEstimationBP/Get/?start=1&limit=100000');
        var res = $http.get('/api/as/AEstimationBP/GetWaitingPartsEstimationBP?start=1&limit=1000'); 
        return res;
      },
      getDetail: function (EstBpId, MatTypeId) {
        // var res = $http.get('/api/as/AEstimationBP/GetDetail/?EstId='+ EstBpId);
        var res = $http.get('/api/as/AEstimationBP/GetWaitingPartsEstimationBPDetail?start=1&limit=1000&EstBpId='+ EstBpId + '&MatTypeId=' + MatTypeId);
        return res;
      },
      getDetailitem: function (EstBpId, MatTypeId) {
        // var res = $http.get('/api/as/AEstimationBP/GetDetail/?EstId='+ EstBpId);
        var res = $http.get('/api/as/AEstimationBP/GetWaitingPartsEstimationBPDetailItem?start=1&limit=1000&EstBpId='+ EstBpId + '&MatTypeId=' + MatTypeId);
        return res;
      },
      create: function(obj) {
        console.log(obj);
        return $http.post('/api/as/GlobalMaster', [obj]);
      },
      
      delete: function(arrId) {
        return $http.delete('/api/as/GlobalMaster/DeleteSubCategory', {data:arrId,headers: {'Content-Type': 'application/json'}});
      },
    }
  });