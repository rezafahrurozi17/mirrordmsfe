angular.module('app')
    .controller('partsInfoEstimasiController', function($scope, $http, $filter, CurrentUser, $compile, PartsInfoEstimasiFactory, $timeout,WOBP, bsNotify, PartsGlobal, AppointmentGrService,AppointmentBpService) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = true;
            $scope.gridData = [];
            $scope.getVehicModeldariWO();
            $scope.getData();
            $scope.selectFilter($scope.searchParam[1]);
        });

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        console.log('$scope.user ====>',$scope.user)
        $scope.mGeneralMaster = {}; //Model
        $scope.xGeneralMaster = {};
        $scope.formApi        = {};
        $scope.displayMode = 'Grid';
        $scope.xGeneralMaster.selected = [];
        $scope.masterCategories = [];
        $scope.filter = { MasterCategoryId: 0 };
        $scope.textFilterPartsInfoEstimasi = {};
        var tempCat = 0;
        //----------------------------------
        // Get Data
        //----------------------------------


        $scope.textFilterPartInfoEstimasi = "";
        $scope.onHapusFilterClick = function () {
            console.log('kategory',$scope.filter.category);
            console.log('value',$scope.filter.value);
            var x = undefined
            $scope.selectFilter(x);
            $scope.filter={category:"",value:""}
        }

        $scope.filter={category:"",value:""}

        // $scope.GeneralMasterForm.$setPristine();
        // $scope.GeneralMasterForm.$setUntouched();

        AppointmentGrService.getUnitMeasurement().then(function(res) {
            $scope.unitData = res.data.Result;
            console.log("$scope.unitData ",$scope.unitData );
            }
        );
    





        $scope.selectedFilterModel = {};
        $scope.selectFilter = function(selected){
            console.log('selectFilter ===>',selected);
            $scope.selectedFilterModel = selected;

        }
        
        $scope.actionButtonSettingsDetail = [
            {
                title: 'Simpan',
                icon: 'fa fa-fw fa-save',
                func: function(row, formScope) {
                    console.log('$scope.gridPartsEstimasi.data ====>',$scope.gridPartsEstimasi.data);
                    $scope.mPartInfoEstimasi.ApartEstimationBP_MaterialRequestItemT = [];
                    $scope.mPartInfoEstimasi.ApartEstimationBP_MaterialRequestItemT = $scope.gridPartsEstimasi.data;

                    for(var i in $scope.mPartInfoEstimasi.ApartEstimationBP_MaterialRequestItemT){
                        if($scope.mPartInfoEstimasi.ApartEstimationBP_MaterialRequestItemT[i].isAvailable == 1 || $scope.mPartInfoEstimasi.ApartEstimationBP_MaterialRequestItemT[i].isAvailable == 2){
                            $scope.mPartInfoEstimasi.ApartEstimationBP_MaterialRequestItemT[i].QtyOrder = 0;
                            console.log('parts ini tersedia, seharusnya QtyOrder == ', $scope.mPartInfoEstimasi.ApartEstimationBP_MaterialRequestItemT[i].QtyRequest+' eh malah '+$scope.mPartInfoEstimasi.ApartEstimationBP_MaterialRequestItemT[i].QtyOrder)
                        }else{
                            $scope.mPartInfoEstimasi.ApartEstimationBP_MaterialRequestItemT[i].QtyOrder = $scope.mPartInfoEstimasi.ApartEstimationBP_MaterialRequestItemT[i].QtyRequest;
                            console.log('parts ini tidak tersedia, seharusnya QtyOrder == 0 | ', $scope.mPartInfoEstimasi.ApartEstimationBP_MaterialRequestItemT[i].QtyOrder)
                        }
                    }


                    console.log('ini do custom save ===>',$scope.mPartInfoEstimasi);
                    PartsInfoEstimasiFactory.update($scope.mPartInfoEstimasi).then(
                        function (res) {
                            console.log('balikan dari back end simpan succ===>', res);
                            bsNotify.show({
                                title: "Customer Data",
                                content: "Data berhasil di simpan",
                                type: 'success'
                            });

                            $scope.getData();        
                            $scope.estimationMode = "Grid";
                            $scope.formApi.setMode('grid');
                        },
                        function (err) {
                            console.log('balikan dari back end simpan err===>', err);
                            bsNotify.show({
                                    type: 'danger',
                                    title: "Data gagal disimpan!",
                                    content: err.data.Message.split('-')[1],
                            });
                    });
                },
            },
            {
                // actionType: 'back', //Use 'Back Action' of bsForm
                title: 'Kembali',

                func: function(row, formScope) {
                    console.log(' ini klik kembali ===>');
                    $scope.mPartInfoEstimasi = {};
                    $scope.statusMR = "";
                    $scope.GrandTotal = null;
                    $scope.formApi.setMode('grid');
                }
                //icon: 'fa fa-fw fa-edit',
            },
        ]

        $scope.FilterGridPartsInfoEstimasi = function (category,value) {
            var inputfilter = value;
            console.log('value ===>', value);
    
            var tempGrid = angular.copy($scope.PartsInfoEstimasi_grid.dataTemp);
            console.log("tempGrid",tempGrid);
            var objct = '{"' + category + '":"' + value + '"}'
            console.log("objct",objct);
            if (inputfilter == "" || inputfilter == null) {
                $scope.PartsInfoEstimasi_grid.data = $scope.PartsInfoEstimasi_grid.dataTemp;
                console.log('$scope.PartsInfoEstimasi_grid.data if ===>', $scope.PartsInfoEstimasi_grid.data)
    
            } else {
                $scope.PartsInfoEstimasi_grid.data = $filter('filter')(tempGrid, JSON.parse(objct));
                console.log('tempGrid else ===>', tempGrid)
            }
        }
       

        $scope.selectCategory = function (selected) {
            console.log('selected Categpry ===>',selected);
        }



        $scope.jumlahData = 0;
        $scope.getData = function() {
            PartsInfoEstimasiFactory.getData().then(
                function(res){
                    // $scope.grid.data       = res.data.Result;
                    // $scope.grid.totalItems = res.data.Total;
                    // $scope.grid.dataTemp   = angular.copy($scope.grid.data);

                    res.data.Result = _.remove(res.data.Result, function(data){
                        if($scope.user.RoleId == 1123 || $scope.user.RoleName == "Partsman BP"){
                            return data.MaterialTypeId == 1;
                        }else if($scope.user.RoleId == 1124 || $scope.user.RoleName == "Petugas Gudang Bahan B"){
                            return data.MaterialTypeId == 2;
                        }
                    })

                    var resultData = res.data.Result;




                    $scope.jumlahData = resultData.length;
                    $scope.PartsInfoEstimasi_grid.data         = resultData;  
					$scope.PartsInfoEstimasi_grid.totalItems   = res.data.Total;
					$scope.PartsInfoEstimasi_grid.dataTemp     = angular.copy(resultData);
                    $scope.loading         = false;
                    console.log('getData ===>', $scope.PartsInfoEstimasi_grid.data );

                },
                function(err){
                    bsNotify.show(
                        {
                            title: "gagal",
                            content: "Data tidak ditemukan.",
                            type: 'danger'
                        }
                    );
                }
            );
        }
        $scope.onTypeFilter = function (textFilter) {
            console.log('textFilter ===>',textFilter);
        }

      

        $scope.onSelectModelChanged = function (ModelSelected) {
            console.log('on model change ===>', ModelSelected);
            if (ModelSelected == undefined || ModelSelected == null) {
                $scope.mPartInfoEstimasi.VehicleModelName = "";
                $scope.mPartInfoEstimasi.VehicleModelId   = "";
                $scope.mPartInfoEstimasi.VehicleTypeName  = "";
                $scope.mPartInfoEstimasi.VehicleTypeId    = "";
                $scope.mPartInfoEstimasi.KatashikiCode    = "";
                $scope.mPartInfoEstimasi.ModelType        = "";
                $scope.mPartInfoEstimasi.ModelCode        = "";
                $scope.optionsTipe                        = [];



            } else {
                $scope.optionsTipe                        = [];
                $scope.mPartInfoEstimasi.VehicleModelName = ModelSelected.VehicleModelName;
                $scope.mPartInfoEstimasi.ModelCode        = ModelSelected.VehicleModelId;
                $scope.mPartInfoEstimasi.VehicleTypeName  = "";
                $scope.mPartInfoEstimasi.VehicleTypeId    = "";
                $scope.mPartInfoEstimasi.KatashikiCode    = "";
                $scope.mPartInfoEstimasi.ModelType        = "";
                $scope.optionsTipe                        = ModelSelected.MVehicleType;
                
            }

        }

        $scope.onSelectTipeChanged = function (TipeSelected) {
            console.log('TipeSelected ===>', TipeSelected);
            if (TipeSelected == undefined || TipeSelected == null) {
                $scope.mPartInfoEstimasi.VehicleTypeName = "";
                $scope.mPartInfoEstimasi.VehicleTypeId   = "";
                $scope.mPartInfoEstimasi.KatashikiCode   = "";
                $scope.mPartInfoEstimasi.ModelType       = "";
            } else {
                $scope.mPartInfoEstimasi.VehicleTypeName = TipeSelected.Description;
                $scope.mPartInfoEstimasi.VehicleTypeId   = TipeSelected.VehicleTypeId;
                $scope.mPartInfoEstimasi.KatashikiCode   = TipeSelected.KatashikiCode;
                $scope.mPartInfoEstimasi.ModelType       = TipeSelected.VehicleTypeId;
            }
        }
        
        $scope.onSelectRows = function (rows) {
            console.log('onSelectRows ===>', rows);
        }

        $scope.getVehicModeldariWO = function(){
            WOBP.getVehicleMList().then(function (res) {
                $scope.optionsModel = res.data.Result;
                console.log('$scope.optionsModel ==>', $scope.optionsModel)
                return $scope.optionsModel;
            });
        }


        
        $scope.gridClickHandler = function(row){
            $scope.displayMode = 'Edit';
            // $scope.formApi.setMode('edit');
            // $scope.formApi.setMode('form');
            $scope.formApi.setMode('detail');
            console.log('pas klik datanya ===>',row);
            // $scope.CurrRowIndex = $scope.grid.data.indexOf(row);
            $scope.CurrRowIndex = $scope.PartsInfoEstimasi_grid.data.indexOf(row);
            $scope.CurrRowIndex = $scope.gridPartsEstimasi.data.indexOf(row);
            $scope.IsBahan = row.MaterialTypeId == "2";
            $scope.MRHeader = PartsInfoEstimasiFactory.getMRHeader();
            console.log("index data : ", $scope.CurrRowIndex);

            $scope.GrandTotal = 0;


            PartsInfoEstimasiFactory.getDetail(row.EstimationBPId, row.MaterialTypeId).then(function (res) {
                $scope.mPartInfoEstimasi = res.data.Result[0];
                console.log('getDetail | $scope.mPartInfoEstimasi ====>',$scope.mPartInfoEstimasi);

                // [Start] - Part Passing
                var dataPart = [];
                $scope.GrandTotal = 0;
                for (i in $scope.mPartInfoEstimasi.AEstimationBP_TaskList_TT[0].AEstimationBP_PartList){
                    dataPart.push($scope.mPartInfoEstimasi.AEstimationBP_TaskList_TT[0].AEstimationBP_PartList[i])
                    dataPart[i].TaskName = $scope.mPartInfoEstimasi.AEstimationBP_TaskList_TT[0].TaskName;
                    dataPart[i].QtyRequest = $scope.mPartInfoEstimasi.AEstimationBP_TaskList_TT[0].AEstimationBP_PartList[i].qty;
                    dataPart[i].HargaTotal = $scope.mPartInfoEstimasi.AEstimationBP_TaskList_TT[0].AEstimationBP_PartList[i].Price *
                                             $scope.mPartInfoEstimasi.AEstimationBP_TaskList_TT[0].AEstimationBP_PartList[i].qty;

                    // cek keseluruhan status material request, bila data ada 1 atau lebih dari 1 dan salah satunya ada yg menunggu parts info, maka stts parts infonya MENUNGGU PARTS INFO
                    if($scope.mPartInfoEstimasi.AEstimationBP_TaskList_TT[0].AEstimationBP_PartList[i].StatusId == 1 ||$scope.mPartInfoEstimasi.AEstimationBP_TaskList_TT[0].AEstimationBP_PartList[i].Status == "Menunggu Parts Info" ){
                        $scope.statusMR = "Menunggu Parts Info"
                    }else{
                        $scope.statusMR = "Completed Request"
                    }

                    // Masukin Satuan ke grid partlist
                    for ( j in $scope.unitData){
                        if($scope.mPartInfoEstimasi.AEstimationBP_TaskList_TT[0].AEstimationBP_PartList[i].SatuanId == $scope.unitData[j].MasterId){
                            dataPart[i].Satuan = $scope.unitData[j].Name;
                            dataPart[i].SatuanId = $scope.unitData[j].MasterId;
                        }
                    }

                    // Hitung Grand Total
                    $scope.GrandTotal +=  $scope.mPartInfoEstimasi.AEstimationBP_TaskList_TT[0].AEstimationBP_PartList[i].HargaTotal;


                }


                console.log('datapart ===>', dataPart);
                $scope.gridPartsEstimasi.data = dataPart;
                 // [End] - Part Passing



                 

                // [Start] - Vehicle pass
                var tampunganVehicleTypeId = $scope.mPartInfoEstimasi.VehicleTypeId;
                for (var i = 0; i < $scope.optionsModel.length; i++) {
                    if ($scope.optionsModel[i].VehicleModelId == parseInt($scope.mPartInfoEstimasi.ModelCode)){
                        $scope.onSelectModelChanged($scope.optionsModel[i]);
                        for (var x = 0; x < $scope.optionsModel[i].MVehicleType.length; x++) {
                            if($scope.optionsModel[i].MVehicleType[x].VehicleTypeId == tampunganVehicleTypeId){
                                $scope.onSelectTipeChanged($scope.optionsModel[i].MVehicleType[x]);
                            }
                        }
                    }
                }// [End] - Vehicle pass
            });


            PartsInfoEstimasiFactory.getDetailitem(row.EstimationBPId, row.MaterialTypeId).then(function(res) {
                console.log('getDetailitem | res.data.Result ===>',res.data.Result);
                    var gridData = res.data.Result;
                    $scope.GrandTotal = 0;
                    var i, Total = 0;
                    console.log('asu',gridData);
                    // $scope.PartsInfoEstimasi_grid.data         = res.data.Result;  
					// $scope.PartsInfoEstimasi_grid.totalItems   = res.data.Total;
                    $scope.gridPartsEstimasi.data = gridData;
                    console.log('aaa',gridData,$scope.gridPartsEstimasi.data)
                    $scope.itemOldData = angular.copy(gridData);
                    $scope.itemOldData[0].MaterialRequestNo = $scope.IniMaterialRequestNo;
                    if (res.length == 0) { //gridDetail
                        $scope.gridDetail.data = [];
                    } else {
                        // $scope.gridOptions.data = gridData;
                        $scope.gridDetail = gridData;
                        // if (mode=='edit') {$scope.gridDetail.data.push($scope.dataNewItem[0]);} didisable 06062017..
                        // $scope.showBuatPO = $scope.anyorder(gridData);
                        // $scope.showPrePicking = $scope.anyPP(gridData);
                        // $scope.showBuatGI = $scope.anyGI(gridData);
                        for (i = 0; i < gridData.length; i++) {
                            Total += gridData[i].HargaTotal;
                        }
                        $scope.GrandTotal = Total;
                    }
                    console.log("GrandTotal : ", $scope.GrandTotal);
                });
             
            
        }

        $scope.setNewDataItem = function() {
            $scope.dataNewItem = [{
                MaterialRequestId: "",
                MaterialRequestItemId: 0,
                PartNo: "",
                Price: 0,
                QtyRequest: 0,
                QtyGI: 0,
                QtyReserved: 0,
                QtyGIReserved: 0,
                QtyPrepicking: 0,
                QtyFree: 0,
                QtyOrder: 0,
                UomId: 0
            }];
        }



        $scope.addNewDataItem = function(caller) {
            $scope.setNewDataItem();
            $scope.dataNewItem[0].MaterialRequestId = $scope.MRHeader.MaterialRequestId;
            $scope.dataNewItem[0].OutletId = $scope.MRHeader.OutletId;
            $scope.dataNewItem[0].WarehouseId = $scope.MRHeader.WarehouseId;
            $scope.gridPartsEstimasi.data.push($scope.dataNewItem[0]);
        }

        function checkDuplicateInObject(propertyName, inputArray) {
            var seenDuplicate = false,
                testObject = {};

            inputArray.map(function(item) {
                console.log("itemmmm",item);
                var itemPropertyName = item[propertyName];
                    if (itemPropertyName != null){
                    if (itemPropertyName in testObject) {
                    testObject[itemPropertyName].duplicate = true;
                    item.duplicate = true;
                    seenDuplicate = true;
                    }
                    else {
                    testObject[itemPropertyName] = item;
                    delete item.duplicate;
                    }
                }
            });

            return seenDuplicate;
        }


        $scope.SearchMaterial = function (data) {
            console.log('ini pas klik search grid ===>',data);
            console.log('$scope.mData.WarehouseId ===>',$scope.mPartInfoEstimasi.WarehouseId);  
            console.log('$scope.gridPartsEstimasi.data ===>',$scope.gridPartsEstimasi.data)
            var part = { pcode : data.PartNo };
            if ($scope.mPartInfoEstimasi.WarehouseId != null) {
                part['WarehouseId'] = $scope.mPartInfoEstimasi.WarehouseId
            };

            console.log("Part Param : ", part);
            $scope.cekdatadouble = $scope.gridPartsEstimasi.data;
            var tst = checkDuplicateInObject('PartNo',$scope.cekdatadouble);
            console.log("tasdfasdfasdf =====>",tst);
            if (tst == false){
                console.log("Masuk? sini");
            }
            else if (tst == true){
                console.log("Masuk?");
                // PartsGlobal.showAlert({
                //     title: "Material Request",
                //     content: "No Material Tidak Boleh Sama",
                //     type: 'danger'

                // }); return 0;
            }

            var index = $scope.gridPartsEstimasi.data.indexOf(data);


            PartsInfoEstimasiFactory.getDetailMaterialOneWarehouse(part).then(function(res) {
                console.log('result part ===>', res.data.Result);
                var flagParts = 0;
                var flagBahan = 0;
                if(res.data.Result.length > 0){
                        if($scope.user.RoleId == 1123 || $scope.user.RoleName == "Partsman BP"){

                            for(var a in res.data.Result){
                                if(res.data.Result[a].MaterialType == "Bahan"){
                                    flagParts++
                                    res.data.Result.splice(a,1);
                                }
                            }
                            console.log("res.data.Result",res.data.Result);
                            var gridData = res.data.Result;
                        }else if($scope.user.RoleId == 1124 || $scope.user.RoleName == "Petugas Gudang Bahan B"){
                            
                            for(var a in res.data.Result){
                                if(res.data.Result[a].MaterialType == "Parts"){
                                    
                                    flagBahan++
                                   res.data.Result.splice(a,1);
                                }
                            }

                            console.log("res.data.Result",res.data.Result);
                            var gridData = res.data.Result;
                        }


                        AppointmentBpService.getAvailableParts(res.data.Result[0].PartsId, 0, data.QtyRequest).then(function(resu) {
                            // jancok
                            console.log('gridData ===>',gridData);
                            console.log('check ketersediaan ===>',res.data.Result);
        
                            gridData[0].QtyFree = resu.data.Result[0].QtyFree;
                            $scope.gridPartsEstimasi.data[index].QtyFree = resu.data.Result[0].QtyFree
                            if(resu.data.Result[0].isAvailable == 0){
                                $scope.gridPartsEstimasi.data[index].Availbility = "Tidak Tersedia";
                                $scope.gridPartsEstimasi.data[index].isAvailable = resu.data.Result[0].isAvailable;
                                $scope.gridPartsEstimasi.data[index].OrderType = 3;
                                
                            }else if(res.data.Result[0].isAvailable == 1){
                                $scope.gridPartsEstimasi.data[index].Availbility = "Tersedia";
                                $scope.gridPartsEstimasi.data[index].isAvailable = resu.data.Result[0].isAvailable;
                                $scope.gridPartsEstimasi.data[index].OrderType = "";
                            }else{
                                $scope.gridPartsEstimasi.data[index].Availbility = "Tersedia Sebagian";
                                $scope.gridPartsEstimasi.data[index].isAvailable = resu.data.Result[0].isAvailable;
                                $scope.gridPartsEstimasi.data[index].OrderType = "";
                            }
    
                           
                        });
                }
                
                if(flagBahan > 0){

                    PartsInfoEstimasiFactory.showAlert({
                        title: "Data Tidak Ditemukan",
                        content: "Hanya dapat mecari bahan",
                        type: 'danger'
                    });
                    return 0; 
                }else if(flagParts > 0){

                    PartsInfoEstimasiFactory.showAlert({
                        title: "Data Tidak Ditemukan",
                        content: "Hanya dapat mecari part",
                        type: 'danger'
                    });
                    return 0; 
                }

                if (typeof gridData == 'undefined') return 0;
                if(gridData[0] == undefined || gridData[0] == null){
                    PartsInfoEstimasiFactory.showAlert({
                        title: "Material Request",
                        content: "Harap Isi No Material dengan Benar",
                        type: 'danger'
                    });
                    return 0;
                }
                else{
                    gridData[0]['PartName'] = gridData[0]['PartsName'];
                }

                for ( j in $scope.unitData){
                    if(gridData[0]['Uomid'] == $scope.unitData[j].MasterId){
                        dataPart[i].Satuan = $scope.unitData[j].Name;
                        dataPart[i].SatuanId = $scope.unitData[j].MasterId;
                    }
                }

       
                if(flagBahan > 0 || flagParts > 0){

                   gridData[0]['PartsCode'] = "";
                }else{

                  gridData[0]['PartsCode'] = data.PartsCode;
                }
                gridData[0]['Price'] =  gridData[0]['RetailPrice'];
                // gridData[0]['SatuanId'] =  gridData[0]['Uomid'];
                gridData[0]['TaskName'] =  $scope.gridPartsEstimasi.data[0].TaskName;
                gridData[0]['PartName'] = gridData[0]['PartsName'];
                gridData[0]['HargaTotal'] = gridData[0]['QtyRequest'] * gridData[0]['Price'];


                if (data.MaterialRequestStatusId == 1 || data.MaterialRequestStatusId == 75 ||
                    data.MaterialRequestStatusId == 5  || data.MaterialRequestStatusId == null ) { // Khusus data yg "Menunggu Parts Info", harga ambil dari master
                    gridData[0]['UnitPrice'] = gridData[0]['RetailPrice'];
                    gridData[0]['HargaTotal'] = gridData[0]['RetailPrice'] * data.QtyRequest;
                    console.log("20-12-2017 20:42  : UnitPrice from master ", gridData[0]['UnitPrice']);

                }

               
                // $scope.gridPartsEstimasi.data[$scope.gridPartsEstimasi.data.length - 1] = gridData[0];
                // $scope.gridPartsEstimasi.data[index] = gridData[0];
                for (var k in gridData[0]) $scope.gridPartsEstimasi.data[index][k] = gridData[0][k];
                var Total = 0;


                console.log('cek DG', $scope.gridPartsEstimasi.data);
                for (i = 0; i < $scope.gridPartsEstimasi.data.length; i++) {
                  console.log('detail ke', i , $scope.gridPartsEstimasi.data[i]);
                    Total += $scope.gridPartsEstimasi.data[i].HargaTotal;
                    console.log('Total', Total);
                }
                

                $scope.GrandTotal = 0;
                for( i in $scope.gridPartsEstimasi.data){
                    $scope.GrandTotal +=  $scope.gridPartsEstimasi.data[i].HargaTotal;
                }

                console.log("GrandTotal", $scope.GrandTotal);
                console.log('ini isi tasklis ===>',$scope.gridPartsEstimasi);

           
            },
            function(err) {
                console.log("err=>", err);
                // confirm("Nomor Material Tidak Ditemukan.");
                confirm("Ada masalah, search Material gagal ");
            }


        )
    }


     


        
    


        //----------------------------------
        // Grid Setup
        //--------------------------------- -
        $scope.canEdit = function() {
            return true;
        }

        $scope.ToNameStatus = function(MaterialRequestStatusId) {
            var statusName;
            switch (MaterialRequestStatusId) {
                case 0:
                    statusName = "Complete";
                    break;
                case 1:
                    statusName = "Menunggu Parts Info";
                    break;
               
            }
            return statusName;
        }

        var cellPartNo = '<div><div class="input-group" >\
                      <span style="text-transform:uppercase;">{{row.entity.PartNo}}</span>\
                      <label class="input-group-btn" >\
                          <span class="btn wbtn"  ng-click="grid.appScope.SearchMaterial(row.entity)">\
                              <i class="fa fa-search fa-1"></i>\
                          </span>\
                      </label>\
                  </div></div>';

        $scope.gridPartsEstimasi = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue, row) {

                    console.log('rowEntity', rowEntity);
                    console.log('colDef', colDef);
                    console.log('newValue', newValue);
                    console.log('oldValue', oldValue);
                    console.log('Position', row);

                    rowEntity.HargaTotal = (rowEntity.QtyRequest * rowEntity.UnitPrice);
                    //console.log(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);
                    //alert(rowEntity.TotalAmount, rowEntity.VATAmount, rowEntity.QtyPO, rowEntity.UnitPrice);

                });
                gridApi.selection.on.rowSelectionChanged($scope, function(rows) {
                    $scope.mySelections = gridApi.selection.getSelectedRows();
                    console.log("Item(s) : ", $scope.mySelections); // ...
                    //build aray string
                    $scope.itemids = $scope.getSelectedItemIds($scope.mySelections);
                    console.log("item ids : ", $scope.itemids);
                });
            },
            columnDefs: [
                { name: 'PartsId', field: 'PartsId', visible: false },
                { name: 'Pekerjaan', field: 'TaskName', width: '15%',},
                {
                    name: 'No. Material',
                    field: 'PartNo',
                    enableCellEdit: true,                    
                    width: '15%',
                    cellTemplate: cellPartNo
                },
                { name: 'Nama Material', field: 'PartName', enableCellEdit: true},
                { name: 'Qty Free', field: 'QtyFree',  enableCellEdit: false,},
                { name: 'Qty Request', field: 'QtyRequest',  enableCellEdit: false,},
                { name: 'Qty Order', field: 'QtyOrder',  enableCellEdit: false,},
                { name: 'Qty Reserved', field: 'QtyReserved',  enableCellEdit: false,},
                { name: 'Harga Satuan', field: 'UnitPrice', cellFilter: 'number'},
                { name: 'Satuan', field: 'Satuan'},
                { name: 'Harga Total', field: 'HargaTotal', cellFilter: 'number'},
                { name: 'Status', field: 'MaterialRequestStatusId', cellFilter: 'filterStatusMatReq'},

            ]
        };


        $scope.searchParam = [
            { name: '', field: ''},
            { name: 'Countdown', field: 'Countdown'},
            { displayName: 'Tanggal Dibutuhkan', name: 'Tanggal Dibutuhkan', field: 'RequiredDate',cellFilter: 'date:\"dd-MM-yyyy\"' },
            { displayName: 'No. Referensi', name: 'No. Referensi', field: 'RefNo'  },
            { displayName: 'No. Polisi', name: 'No. Polisi', field: 'LicensePlate'  },
            { displayName: 'Request dari', name: 'Request dari', field: 'RequestBy'  }     
        ]




        console.log('$scope.searchParam ===>',$scope.searchParam);

        $scope.PartsInfoEstimasi_grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            columnDefs: [
                { name: 'Countdown', field: 'Countdown'},
                { displayName: 'Tanggal Dibutuhkan', name: 'Tanggal Dibutuhkan', field: 'RequiredDate',cellFilter: 'date:\"dd-MM-yyyy\"' },
                { displayName: 'No. Referensi', name: 'No. Referensi', field: 'RefNo'  },
                { displayName: 'No. Polisi', name: 'No. Polisi', field: 'LicensePlate'  },
                { displayName: 'Request dari', name: 'Request dari', field: 'RequestBy'  },
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: 250,
                    enableCellEdit: false,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    // cellTemplate: '<span style="color:blue"><p style="padding:5px 0 0 5px;cursor: pointer;"><u ng-click="grid.appScope.$parent.gridClickHandler(row.entity)">Isi No Parts</u></p></span>'
                    cellTemplate: '<span style="color:blue"><p style="padding:5px 0 0 5px;cursor: pointer;"><u ng-click="grid.appScope.gridClickHandler(row.entity)">Isi No Parts</u></p></span>'
                }
            ]
        };

    }).filter('filterStatusMatReq', function() {
        var xtipe = {
            '0': 'Complete',
            '1': 'Menunggu Parts Info',
           
        };
        return function(input) {
            if (!input) {
                return '';
            } else {
                return xtipe[input];
            }
        };
    });