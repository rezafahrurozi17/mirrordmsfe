angular.module('app')
  .factory('Attendance', function($http, CurrentUser) {
    var currentUser = CurrentUser.user();
    return {
      getData: function(date) {
        console.log('currentUser', currentUser);
        return res = $http.get('/api/as/GetAttendanceList/' + date + '/' + currentUser.RoleId);
        console.log("res==>", res);
      },
      update: function(arr){
        console.log("arr",arr);
        return $http.put('/api/as/PostAttendance',[arr]);
      },
      updatecuti: function(arr){
        console.log("arr",arr);
        return $http.post('/api/as/PostAttendanceLeave',[arr]);
      },
      getReason : function(){
        var catId = 2027;
        var res=$http.get('/api/as/GlobalMaster?CategoryId='+catId);
        console.log('resnya pause=>',res);
        return res;
      },
    }
  });