angular.module('app')
  .controller('AttendanceController', function($scope, $http, CurrentUser, Attendance, WorkingShift, $timeout,bsNotify,$q) {

    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mAttendance = []; //Model
    $scope.shifts = [];

    $scope.filter = {
      AttendDate: new Date()
    };

    //----------------------------------
    // Function
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
      $scope.loading=false;
      $scope.gridData = [];
      $scope.getData();
    });
    $scope.show_modal = {
        show: false
    };
    $scope.modal_model = {};
    $scope.modalMode = 'new';
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    $scope.ismeridian = false;
    // $scope.mytime = new Date();
    $scope.hstep = 1;
    $scope.mstep = 1;
    $scope.toggleMode = function() {
        $scope.ismeridian = ! $scope.ismeridian;
    };
    $scope.DateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    }
    $scope.fFrom = function(){
    var d = new Date($scope.modal_model.TimeThru);
    d.getHours();
    d.getMinutes();
    // $scope.maxFrom = d;
    if($scope.modal_model.TimeFrom > $scope.modal_model.TimeThru){
            $scope.modal_model.TimeFrom =  $scope.modal_model.TimeThru;
            // $scope.maxFrom =;
            
        }
    }
    $scope.fThrough =function(){
        var z = new Date($scope.modal_model.TimeFrom);
        z.getHours();
        z.getMinutes();
        // $scope.minThru = z;
        if($scope.modal_model.TimeThru < $scope.modal_model.TimeFrom){
            $scope.modal_model.TimeThru = $scope.modal_model.TimeFrom;
            
        }
    }
  //   $scope.fAdjusment =function(){
  //     var x = new Date($scope.modal_model.Adjusment);
  //     x.getHours();
  //     x.getMinutes();
  //     console.log(x.getHours(), ':', x.getMinutes());
  //     $scope.modal_model.Available = 0;
  //     $scope.dText = false;
  //     // $scope.chkBoxChange2($scope.modal_model.Available);
      
  // }

    $scope.getData = function() {
      console.log("$scope.filter.AttendDate",$scope.filter.AttendDate);
      if($scope.filter.AttendDate !== null){
        var a = new Date($scope.filter.AttendDate);
        var yearFirst = a.getFullYear();
        var monthFirst = a.getMonth() + 1;
        var dayFirst = a.getDate();
        var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
        // var firstDate = $scope.filter.AttendDate;

        //console.log("firstDate", firstDate);
        $scope.loading=true;
        Attendance.getData(firstDate)
          .then(function(res) {
            console.log("res.data.Result",res.data.Result);
            var gridData = res.data.Result;
            $scope.grid.data = gridData;
            $scope.loading=false;
                Attendance.getReason().then(function(res){
                  console.log("res.data.Result dataReason",res.data.Result);
                  $scope.dataReason = res.data.Result;
                },function(err){

                })
            },
            function(err) {
              console.log("err=>", err);
            }
          );
      }else{
        $scope.loading = false;
        bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "Mohon Input Filter",
                // content: error.join('<br>'),
                // number: error.length
            }),
            $scope.grid.data = [];
        return $q.resolve()
      }
    }
    var gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
        '<a href="#" ng-click="grid.appScope.$parent.actViewAttendent(row.entity)" uib-tooltip="Lihat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
        '<a href="#" ng-click="grid.appScope.$parent.actEditAttendent(row.entity)" uib-tooltip="Edit" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
        '<a href="#" ng-click="grid.appScope.$parent.actCutiAttendent(row.entity)" uib-tooltip="Cuti" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-calendar" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
        '</div>';

    $scope.actViewAttendent = function(item){
      $scope.modalMode = 'view';
      var tmpItem = angular.copy(item);
      console.log("tmpItem view", item);
      // $scope.dAdjusment = true;      
      $scope.dThru = true;
      $scope.dFrom = true;
      var tFrom = new Date("Wed Jan 18 2017 "+tmpItem.TimeFrom+" GMT+0700 (SE Asia Standard Time)");
      var tThru = new Date("Wed Jan 18 2017 "+tmpItem.TimeThru+" GMT+0700 (SE Asia Standard Time)");
      tmpItem.TimeFrom = tFrom;
      tmpItem.TimeThru = tThru;
      $scope.modal_model= angular.copy(tmpItem);
      $scope.show_modal = {
          show: true
      };
    }
    $scope.actEditAttendent = function(item){
      $scope.modalMode = 'edit';
      var tmpItem = angular.copy(item);
      console.log("tmpItem edit", item);
      
      // $scope.dAdjusment = false;  
      if (tmpItem.Available == 0) {
        $scope.dThru = false;
        $scope.dFrom = false;
        // $scope.dText = true;
      } else {
        $scope.dThru = false;
        $scope.dFrom = false;
        // $scope.dText = false;
        $scope.dText = true;      
        
      }
      var tFrom = new Date("Wed Jan 18 2017 "+tmpItem.TimeFrom+" GMT+0700 (SE Asia Standard Time)");
      var tThru = new Date("Wed Jan 18 2017 "+tmpItem.TimeThru+" GMT+0700 (SE Asia Standard Time)");
      tmpItem.TimeFrom = tFrom;
      tmpItem.TimeThru = tThru;

      // set default value
      // tmpItem.Attend = true;
      // tmpItem.Available = true;
      console.log("tmpItem tmpItem", tmpItem);
      
      $scope.modal_model= angular.copy(tmpItem);
      $scope.tmpJT = angular.copy(tmpItem.JTAdjustment)
      // set checkbox true on load
      // $scope.chkBoxChange1($scope.modal_model);

      $scope.show_modal = {
          show: true
      };
    }
    $scope.actCutiAttendent = function(item){
      $scope.modalMode = 'edit';
      var tmpItem = angular.copy(item);
      
      $scope.modal_model= angular.copy(tmpItem);
      $scope.show_modalCuti = {
          show: true
      };
    }
    $scope.chkBoxChange1 = function(item){
      console.log('klik terooos', $scope.modal_model.JTAdjustment)
      if (item == 1){
        $scope.modal_model.Available = 0;
        $scope.modal_model.JTAdjustment = 0;
        $scope.dThru = false;
        $scope.dFrom = false;
        $scope.dText = false;
      } else {
        $scope.modal_model.JTAdjustment = $scope.tmpJT;
        $scope.modal_model.Attend = 1;
        $scope.modal_model.Available = 1;
        $scope.dThru = false;
        $scope.dFrom = false;
        $scope.dText = true;
      }
    }
    $scope.chkBoxChange2 = function(item){
      if (item == 0) {
        $scope.dThru = false;
        $scope.dFrom = false;
        $scope.dText = true;
      } else {
        $scope.dThru = false;
        $scope.dFrom = false;
        $scope.dText = false;
      }
    }
    $scope.actCancel = function(){
      $scope.show_modal = {
          show: false
      };
    }
    $scope.actCancelCuti = function(){
      $scope.show_modalCuti = {
          show: false
      };
    }
    $scope.doSave = function(data){
      var tmpData = angular.copy(data);
      console.log("tmpData===>",tmpData);
      var dt = new Date('"'+tmpData.TimeFrom+'"');
      var dtt = new Date('"'+tmpData.TimeThru+'"');
      dt.setSeconds(0);
      dtt.setSeconds(0);
      var dtFrom = dt.toTimeString();
      var dtThru = dtt.toTimeString();
      dtFrom = dtFrom.split(' ');
      dtThru = dtThru.split(' ');
      data.TimeFrom = dtFrom[0];
      data.TimeThru = dtThru[0];
      console.log("data====>",data);
      // ==================================
      var tmpWorkCall = tmpData.WorkCall;
      tmpWorkCall = new Date(tmpWorkCall);
      var m = tmpWorkCall.getMonth() + 1;
      var day = tmpWorkCall.getDate();
      var year = tmpWorkCall.getFullYear();
      var n = year + "/" + m + "/" + day;
      data.WorkCall = n;
      // ==================================
      // if(data.Available == 1 && data.Attend == 1){
      //     $scope.show_modal = {
      //         show: false
      //     };
      // }else{
        Attendance.update(data).then(
          function(res) {
            console.log("data",res);
            console.log('json',data);
            $scope.show_modal = {
                show: false
            };
            $scope.getData();
        },
          function(err) {
            console.log("err=>", err);
        }
        );
      // }
    }




    $scope.doSaveCuti = function(data){
      var tmpData = angular.copy(data);
      console.log("tmpData===>",tmpData);
      // var dt = new Date('"'+tmpData.TimeFrom+'"');
      // var dtt = new Date('"'+tmpData.TimeThru+'"');
      // dt.setSeconds(0);
      // dtt.setSeconds(0);
      // var dtFrom = dt.toTimeString();
      // var dtThru = dtt.toTimeString();
      // dtFrom = dtFrom.split(' ');
      // dtThru = dtThru.split(' ');
      // data.TimeFrom = dtFrom[0];
      // data.TimeThru = dtThru[0];
      console.log("data====>",data);
      // ==================================
      var StartCuti = tmpData.StartCuti;
      StartCuti = new Date(StartCuti);
      var m = StartCuti.getMonth() + 1;
      var day = StartCuti.getDate();
      var year = StartCuti.getFullYear();
      var n = year + "/" + m + "/" + day;
      data.StartCuti = n;

      var EndCuti = tmpData.EndCuti;
      EndCuti = new Date(EndCuti);
      var m1 = EndCuti.getMonth() + 1;
      var day1 = EndCuti.getDate();
      var year1 = EndCuti.getFullYear();
      var n1 = year1 + "/" + m1 + "/" + day1;
      data.EndCuti = n1;
      // ==================================
      // if(data.Available == 1 && data.Attend == 1){
      //     $scope.show_modal = {
      //         show: false
      //     };
      // }else{

      data.Attend = 0;
      data.Available = 0;

        Attendance.updatecuti(data).then(
          function(res) {
            console.log("data",res);
            console.log('json',data);
            $scope.show_modalCuti = {
                show: false
            };
            $scope.getData();
        },
          function(err) {
            console.log("err=>", err);
        }
        );
      // }
    }


    $scope.grid = {
      enableSorting: true,
      paginationPageSize: 500,
      enableRowSelection: false,
      // showGridFooter: true,
      // showColumnFooter: true, 
      multiSelect: false,
      enableCellEdit: false,
      enableSelectAll: false,
      enableCellEditOnFocus: false,
      columnDefs: [{
          name: 'CalId',
          field: 'CalId',
          visible: false
        },
        {
          name: 'NIP',
          field: 'EmployeeId',
          width : 90,
        },
        {
          name: 'Initial',          
          // field: 'EmployeeCode',
          field: 'Initial',
          width : 150,
        },
        {
          name: 'Nama',
          field: 'EmployeeName',
          width : 200,
        },
        {
          name: 'Jabatan',
          field: 'LastManPowerPositionTypeName',
          width : 80,
        },
        {
          name: 'Posisi',
          field: 'Position',
          width : 120,
        },
        {
          name: 'Group',
          width:50,
          visible:false,
          field: 'Group'
        },
        {
          name: 'Tanggal',
          field: 'WorkCall',
          width : 150,
          cellFilter: dateFilter
        },
        {
          name: 'Hadir',
          width:80,
          field: 'Attend',
          cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-checked="MODEL_COL_FIELD > 0"></div>'
        },
        {
          name: 'Availability',
          width:100,
          field: 'Available',
          cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-checked="MODEL_COL_FIELD > 0"></div>'
        },
        {
          name: 'Jam Datang',
          field: 'TimeFrom',
          width : 150,
        },
        {
          name: 'Jam Pulang',
          field: 'TimeThru',
          width : 150,
        },
        {
          name: 'JT Adjustment',
          field: 'JTAdjustment',
          width : 150,
        },
        {
          name: 'Shift',
          field: 'Position',
          width : 100,
        },
        {
          name: 'Alasan',
          field: 'Reason',
          width : 200,
        },
        {
          name: 'Catatan',
          field: 'Note',
          width : 250,
        },
        {
            name: 'Action',
            width: 150,
            pinnedRight: true,
            cellTemplate: gridActionButtonTemplate
        }
      ]
    };

  });
