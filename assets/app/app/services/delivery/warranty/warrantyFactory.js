angular.module("app")
    .factory("warranty", function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {

            getData: function(StartDate, StopDate, Filter) {
                console.log("filter", Filter);
                // var res = $http.get('/api/as/Jobs/getListTowasApprove/'+StartDate+'/'+StopDate);
                return $http.patch('/api/as/Jobs/getListTowasApprove', [{
                    NoWo: Filter.WoNo,
                    typeWaranty1: Filter.typeWarranty,
                    typeWaranty2: Filter.typeWarranty,
                    TglAwal: StartDate,
                    TglAkhir: StopDate,
                    filterStatus: Filter.status
                }]);
                // console.log('res=>',res);
                // return res;
            },
            getPayment: function() {
                var catId = 1008;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getDataDetail: function(ClaimNo, AppStatus) {
                var res = $http.get('/api/as/Towass/getCreateClaim/' + ClaimNo + '/' + AppStatus);
                return res;
            },
            getStatusApproval: function() {
                var res = $http.get('/api/as/GetStatusApprovalTowass');
                return res;
            },
            CekIsPurchaseOrderSOP: function(jobid) {
                var res = $http.get('/api/as/CekIsPurchaseOrderSOP/' + jobid);
                return res;
            },
            // ApproveTowas: function(data, mData, user, status, date) {
            //     //debugger;
            //     return $http.put('/api/as/ASubmited_ApprovalTOWASS_TR', [{
            //         OutletId: user.OrgId,
            //         // OutletId:280,
            //         TransactionId: data[0].TransactionId,
            //         JobTaskId: data[0].JobTaskId,
            //         StatusApprovalId: status,
            //         RejectReason: mData.RejectReason,
            //         ApproveRejectDate: date
            //     }]);
            // },
            ApproveTowas: function(data, mData, user, status, date) {
                //debugger;
                return $http.put('/api/as/ApproveTowas', [{
                    OutletId: user.OrgId,
                    // OutletId:280,
                    NoClaim: data[0].ClaimNo,
                    TransactionId: data[0].TransactionId,
                    JobTaskId: data[0].JobTaskId,
                    StatusApprovalId: status,
                    RejectReason: mData.RejectReason,
                    ApproveRejectDate: date
                }]);
            },

            update: function(data) {
                //debugger;
                console.log("data.tmpUploadFile",data.tmpUploadFile);
                console.log("data.tmpUploadFile.length",data.tmpUploadFile.length);
                if(data.tmpUploadFile.length > 0){
                    console.log("data.tmpUploadFile[0].UploadDataDocument.FileName",data.tmpUploadFile[0].UploadDataDocument.FileName);
                    data.fileName1 = data.tmpUploadFile[0].UploadDataDocument.FileName;
                    var streetaddress1 = data.tmpUploadFile[0].UploadDataDocument.UpDocObj.substr(0, data.tmpUploadFile[0].UploadDataDocument.UpDocObj.indexOf(';'));
                    console.log("streetaddress",streetaddress1);
                    data.mimeType1 = streetaddress1;
                    var replaceValue1 = (data.tmpUploadFile[0].UploadDataDocument.UpDocObj.split(',')[0]);
                    console.log("replaceValue",replaceValue1);
                    var base64String1 = data.tmpUploadFile[0].UploadDataDocument.UpDocObj.replace(replaceValue1+ ",", "");
                    console.log("base64String",base64String1);
                    data.base641 = base64String1;
                }
                
                if(data.tmpUploadFile.length > 1){
                    console.log("data.tmpUploadFile[1].UploadDataDocument.FileName",data.tmpUploadFile[1].UploadDataDocument.FileName);
                    data.fileName2 = data.tmpUploadFile[1].UploadDataDocument.FileName;
                    var streetaddress2 = data.tmpUploadFile[1].UploadDataDocument.UpDocObj.substr(0, data.tmpUploadFile[1].UploadDataDocument.UpDocObj.indexOf(';'));
                    console.log("streetaddress",streetaddress2);
                    data.mimeType2 = streetaddress2;
                    var replaceValue2 = (data.tmpUploadFile[1].UploadDataDocument.UpDocObj.split(',')[0]);
                    console.log("replaceValue",replaceValue2);
                    var base64String2 = data.tmpUploadFile[1].UploadDataDocument.UpDocObj.replace(replaceValue2+ ",", "");
                    console.log("base64String",base64String2);
                    data.base642 = base64String2;
                }
                
                if(data.tmpUploadFile.length > 2){
                    console.log("data.tmpUploadFile[2].UploadDataDocument.FileName",data.tmpUploadFile[2].UploadDataDocument.FileName);
                    data.fileName3 = data.tmpUploadFile[2].UploadDataDocument.FileName;
                    var streetaddress3 = data.tmpUploadFile[2].UploadDataDocument.UpDocObj.substr(0, data.tmpUploadFile[2].UploadDataDocument.UpDocObj.indexOf(';'));
                    console.log("streetaddress",streetaddress3);
                    data.mimeType3 = streetaddress3;
                    var replaceValue3 = (data.tmpUploadFile[2].UploadDataDocument.UpDocObj.split(',')[0]);
                    console.log("replaceValue",replaceValue3);
                    var base64String3 = data.tmpUploadFile[2].UploadDataDocument.UpDocObj.replace(replaceValue3+ ",", "");
                    console.log("base64String",base64String3);
                    data.base643 = base64String3;
                }
                
                if(data.tmpUploadFile.length  > 3){
                    console.log("data.tmpUploadFile[3].UploadDataDocument.FileName",data.tmpUploadFile[3].UploadDataDocument.FileName);
                    data.fileName4 = data.tmpUploadFile[3].UploadDataDocument.FileName;
                    var streetaddress4 = data.tmpUploadFile[3].UploadDataDocument.UpDocObj.substr(0, data.tmpUploadFile[3].UploadDataDocument.UpDocObj.indexOf(';'));
                    console.log("streetaddress",streetaddress4);
                    data.mimeType4 = streetaddress4;
                    var replaceValue4 = (data.tmpUploadFile[3].UploadDataDocument.UpDocObj.split(',')[0]);
                    console.log("replaceValue",replaceValue4);
                    var base64String4 = data.tmpUploadFile[3].UploadDataDocument.UpDocObj.replace(replaceValue4+ ",", "");
                    console.log("base64String",base64String4);
                    data.base644 = base64String4;
                }
                
                if(data.tmpUploadFile.length > 4){
                    console.log("data.tmpUploadFile[4].UploadDataDocument.FileName",data.tmpUploadFile[4].UploadDataDocument.FileName);
                    data.fileName5 = data.tmpUploadFile[4].UploadDataDocument.FileName;
                    var streetaddress5 = data.tmpUploadFile[4].UploadDataDocument.UpDocObj.substr(0, data.tmpUploadFile[4].UploadDataDocument.UpDocObj.indexOf(';'));
                    console.log("streetaddress",streetaddress5);
                    data.mimeType5 = streetaddress5;
                    var replaceValue5 = (data.tmpUploadFile[4].UploadDataDocument.UpDocObj.split(',')[0]);
                    console.log("replaceValue",replaceValue5);
                    var base64String5 = data.tmpUploadFile[4].UploadDataDocument.UpDocObj.replace(replaceValue5+ ",", "");
                    console.log("base64String",base64String5);
                    data.base645 = base64String5;
                }
                console.log("data in factory",data)

                console.log('$scope.mData.SpecialClaimFlag factory', data.SpecialClaimFlag);
                var checkSpesialClaim = 0;
                if (data.SpecialClaimFlag == true) {
                    checkSpesialClaim = 1;
                }else{
                    checkSpesialClaim = 0;
                }

                return $http.put('/api/as/ReCalculateRPP', [{
                    ClaimNo: data.ClaimNo,
                    WoNo: data.wono,
                    PRR: data.dataPRR,
                    Task: data.dataTask,
                    SpecialClaimFlag: checkSpesialClaim,
                    // OFP: data.OFP,
                    // T1_Cd: data.T1_Cd,
                    // T2_Cd: data.T2_Cd,
                    Document: {
                        Base641 : data.base641,
                        mimeType1 : data.mimeType1,
                        fileName1 : data.fileName1,
                        Base642 : data.base642,
                        mimeType2 : data.mimeType2,
                        fileName2 : data.fileName2,
                        Base643 : data.base643,
                        mimeType3 : data.mimeType3,
                        fileName3 : data.fileName3,
                        Base644 : data.base644,
                        mimeType4 : data.mimeType4,
                        fileName4 : data.fileName4,
                        Base645 : data.base645,
                        mimeType5 : data.mimeType5,
                        fileName5 : data.fileName5,
                    }
             
                }]);
            },

           
            getWarrantyType: function(catId) {
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya warranty type=>', res);
                return res;
            },
            getT1: function() {
                var res = $http.get('/api/as/GetT1');
                return res;
            },

            getT2: function() {
                var res = $http.get('/api/as/GetT2');
                return res;
            },
            getDataTaskOpe: function(Key, VIN, catg, vehicleModelId) {
                var res = $http.get('/api/as/TaskListsWarranty?VIN=' + VIN + '&Name=' + Key + '&TaskCategory=' + catg + '&VehicleModelId=' + vehicleModelId);
                
                return res;
            },
            ReSubmitWarranty: function(TransactionId) {
                // api/as/ReSubmitWarranty/{TransactionId}
                var res = $http.put('/api/as/ReSubmitWarranty/' + TransactionId);
                
                return res;
            },
            getDataDocument: function () {
                var res = $http.get('/api/sales/PCategoryDocument');
                return res;
            },

            getDataUploadFile: function(ClaimNo){
                var res = $http.get('/api/as/Jobs/getListDocument/' + ClaimNo);
                return res;
            }

        };
    });