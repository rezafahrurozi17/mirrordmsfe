angular.module('app')
    .controller('WarrantyController', function ($scope, $http, CurrentUser, warranty, $timeout, bsNotify, bsAlert, ASPricingEngine) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.formApi = {};
        $scope.formGridApi = {};
        var PPNPerc = 0;
        $scope.CheckSpesialClaim = false;
        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = false;
            $scope.gridData = {};
            $scope.modalmainopeno = {}
            $scope.modalmainopeno.show = false;
            $scope.getPPN()

            // console.log("nih", $scope.VM );            
            warranty.getPayment()
                .then(

                    function (res) {

                        var typeWarrantytest = res.data.Result;
                        $scope.typeWarranty = typeWarrantytest;
                        // console.log("test",res.data.Result);
                    },
                    function (err) {
                        console.log("err=>", err);
                    }
                );

            warranty.getStatusApproval().then(function (res) {
                $scope.filterStatus = res.data.Result;
                console.log("status Approval", $scope.filterStatus);
            },
                function (err) {
                    console.log("err=>", err);
                }
            );

            warranty.getT1().then(function(res) {
                $scope.T1 = res.data.Result;
                // T1Final = res.data.Result;
                for (var i = 0; i < res.data.Result.length; i++) {
                    $scope.T1[i].xName = $scope.T1[i].T1Code + ' - ' + $scope.T1[i].Name;
                    // T1Final[i].xName = T1Final[i].T1Code + ' - ' + T1Final[i].Name;
                }
            });

            warranty.getT2().then(function(res) {
                $scope.T2 = res.data.Result;
                for (var i = 0; i < res.data.Result.length; i++) {
                    $scope.T2[i].T2Code = $scope.T2[i].T2Code.trim();
                    $scope.T2[i].xName = $scope.T2[i].T2Code + ' - ' + $scope.T2[i].Name;
                }
            });

            $timeout(function () {
                console.log("rightEnableByte", $scope.rightEnableByte);
                checkBytes($scope.rightEnableByte);
                console.log("RIGHST =====>", $scope.RightsEnableForMenu);
            }, 500)

        });
    
        // $scope.setUpload = function (file){
        //     if (file && file.$error != null){
        //         $scope.cedulaSizeError=true;
        //         console.log('upload eror')
        //     }
        //     else {
        //         $scope.file=file;
        //         console.log('upload berhasill')
        //         $scope.reload=true;
        //     }
        // };

        $scope.uploader = function(dataUpload){
            console.log('data uploader', dataUpload);
            
        }

        $scope.onBeforeEdit = function(data){
            console.log('data upload mamang', data)
            $scope.mData.tmpUploadFile = [];
            warranty.getDataUploadFile(data.ClaimNo).then(function(res){
                console.log('data upload file yeuu', res);
                var getUploadFile = res.data.Result[0];
                
                if(getUploadFile.base641 != null && getUploadFile.fileName1 != null && getUploadFile.mimeType1 != null){
                        $scope.mData.tmpUploadFile.push({
                            base64 : getUploadFile.base641,
                            fileName : getUploadFile.fileName1,
                            mimeType : getUploadFile.mimeType1,
                            UploadDataDocument:{
                                UpDocObj: getUploadFile.mimeType1+";base64,"+getUploadFile.base641,
                                FileName: getUploadFile.fileName1
                            }
                        })
                        
                }

                if(getUploadFile.base642 != null && getUploadFile.fileName2 != null && getUploadFile.mimeType2 != null){
                    $scope.mData.tmpUploadFile.push({
                        base64 : getUploadFile.base642,
                        fileName : getUploadFile.fileName2,
                        mimeType : getUploadFile.mimeType2,
                        UploadDataDocument:{
                            UpDocObj: getUploadFile.mimeType2+";base64,"+getUploadFile.base642,
                            FileName: getUploadFile.fileName2
                        }
                    })
                }

                if(getUploadFile.base643 != null && getUploadFile.fileName3 != null && getUploadFile.mimeType3 != null){
                    $scope.mData.tmpUploadFile.push({
                        base64 : getUploadFile.base643,
                        fileName : getUploadFile.fileName3,
                        mimeType : getUploadFile.mimeType3,
                        UploadDataDocument:{
                            UpDocObj: getUploadFile.mimeType3+";base64,"+getUploadFile.base643,
                            FileName: getUploadFile.fileName3
                        }
                    })
                }

                if(getUploadFile.base644 != null && getUploadFile.fileName4 != null && getUploadFile.mimeType4 != null){
                    $scope.mData.tmpUploadFile.push({
                        base64 : getUploadFile.base644,
                        fileName : getUploadFile.fileName4,
                        mimeType : getUploadFile.mimeType4,
                        UploadDataDocument:{
                            UpDocObj: getUploadFile.mimeType4+";base64,"+getUploadFile.base644,
                            FileName: getUploadFile.fileName4
                        }
                    })
                }

                if(getUploadFile.base645 != null && getUploadFile.fileName5 != null && getUploadFile.mimeType5 != null){
                    $scope.mData.tmpUploadFile.push({
                        base64 : getUploadFile.base645,
                        fileName : getUploadFile.fileName5,
                        mimeType : getUploadFile.mimeType5,
                        UploadDataDocument:{
                            UpDocObj: getUploadFile.mimeType5+";base64,"+getUploadFile.base645,
                            FileName: getUploadFile.fileName5
                        }
                    })
                }
            });
            console.log("$scope.mData.tmpUploadFile",$scope.mData.tmpUploadFile);
            console.log('$scope.mData.SpecialClaimFlag', $scope.mData.SpecialClaimFlag);
            if ($scope.mData.SpecialClaimFlag == 1) {
                $scope.CheckSpesialClaim = true;
            }else{
                $scope.CheckSpesialClaim = false;
            }
            $scope.mData.SpecialClaimFlag = $scope.CheckSpesialClaim;
        };

        $scope.keluarDokumen = function () {
            angular.element('.ui.modal.dokumenSPKViewMobilesDraft').modal('hide');
        }

        $scope.gambar = {
            "max-width": $scope.lebar - 40,
            "max-height": $scope.tinggi - 40
        }

        $scope.viewImage = function (docList) {
            console.log("docList",docList);
            // $scope.image = { documentname: null, viewdocument: null, viewdocumentName: null, typeFile: null };
            // $scope.image = {};
           
            var numItems = angular.element('.ui.modal.dokumenSPKViewMobilesDraft').length;
            setTimeout(function () {
                angular.element('.ui.modal.dokumenSPKViewMobilesDraft').modal('refresh');
            }, 0);
            
            console.log('view data upload file', $scope.mData.tmpUploadFile)
            console.log('docList.UploadDataDocument', docList.UploadDataDocument)
            console.log("$scope.mData.tmpUploadFile.UploadDataDocument.FileName",docList.UploadDataDocument.FileName);
            docList.fileName = docList.UploadDataDocument.FileName;
            var streetaddress = docList.UploadDataDocument.UpDocObj.substr(0, docList.UploadDataDocument.UpDocObj.indexOf(';'));
            console.log("streetaddress",streetaddress);
            docList.mimeType = streetaddress;
            var replaceValue = (docList.UploadDataDocument.UpDocObj.split(',')[0]);
            console.log("replaceValue",replaceValue);
            var base64String = docList.UploadDataDocument.UpDocObj.replace(replaceValue+ ",", "");
            console.log("base64String",base64String);
            docList.base64 = base64String;
            var dataUploadImage = {
                documentname: docList.UploadDataDocument.FileName, 
                viewdocument: docList.UploadDataDocument.UpDocObj, 
                viewdocumentName: docList.UploadDataDocument.FileName, 
                typeFile: docList.mimeType
            };
            console.log('data upload image documentName', dataUploadImage.documentname)
            console.log('data upload image viewdocument', dataUploadImage.viewdocument)
            console.log('data upload image viewdocumentName', dataUploadImage.viewdocumentName)
            console.log('data upload image typeFile', dataUploadImage.typeFile)
            $scope.image = dataUploadImage;
            console.log('scope image', $scope.image)
            // $scope.image = { documentname: $scope.mData.tmpUploadFile.UploadDataDocument.FileName, viewdocument: $scope.mData.tmpUploadFile.UploadDataDocument.UpDocObj, viewdocumentName: $scope.mData.tmpUploadFile.UploadDataDocument.FileName, typeFile: $scope.mData.tmpUploadFile.mimeType };
            if (numItems > 1) {
                angular.element('.ui.modal.dokumenSPKViewMobilesDraft').not(':first').remove();
            }

            angular.element('.ui.modal.dokumenSPKViewMobilesDraft').modal('show');
        }

        $scope.selectCheckBox = function(){
            // if ($scope.mData.SpecialClaimFlag == true) {
            //     $scope.mData.SpecialClaimFlag = 1
            // }else{
            //     $scope.mData.SpecialClaimFlag = 0
            // }
            console.log('checklist imam', $scope.mData.SpecialClaimFlag)
        }

        // $scope.testFile = function(data){
        //     console.log("data upload",data);
        // }

        // $scope.cekUpload = function(data){
        //     console.log('cek list imam', data)
        //     console.log('warrant imam', $scope.dataUpload)
        //     console.log("$scope.mData.tmpUploadFile",$scope.mData.tmpUploadFile);
        // }

        // $scope.countTambah = 0;
        // $scope.loadingdoc == false;
        $scope.openDokumenlist = function () {
            // $scope.loadingdoc = true;
            // $scope.countTambah++;
            if($scope.mData.tmpUploadFile.length < 5){
                $scope.mData.tmpUploadFile.push({
                    base64 : "",
                    fileName : "",
                    mimeType : "",
                })
            }
            console.log("$scope.mData.tmpUploadFile",$scope.mData.tmpUploadFile);
            // $scope.dataUpload.UploadDataDocument = [];
            // if ($scope.dataUpload.UploadDataDocument.length == 0) {
            //     $scope.countTambah.push($scope.countTambah++);
            // } else {
            //     for (var i in $scope.dataUpload.UploadDataDocument) {
            //         for (var j in $scope.countTambah) {
            //             if ($scope.countTambah[j] == $scope.dataUpload.UploadDataDocument[i]) {
            //                 $scope.countTambah.splice(j, 1);
            //             }
            //         }
            //     }
            //     for (var i in $scope.countTambah) {
            //         $scope.dataUpload.UploadDataDocument.push($scope.countTambah[i]);
            //     }
            // }
            console.log('upload imam', $scope.dataUpload)
        }

        $scope.RejectData = function () {
            for (var i = 0; i < $scope.selectedItems.length; i++) {
                $scope.selectedItems[i].ApprovalFlag = 0;
                delete $scope.selectedItems[i].checked;
            }

            $scope.selectedItems = [];
            angular.element('.ui.modal.dokumentList').modal('hide');
        };

        $scope.removeUploader = function(index){
            console.log('remove index', index)
            console.log('mData mamang yeu', $scope.mData)
            // if ($scope.mData.tmpUploadFile[index].base64 == $scope.mData.base641) {
                $scope.mData.base641 = null
                $scope.mData.fileName1 = null
                $scope.mData.mimeType1 = null
            // }
            // if ($scope.mData.tmpUploadFile[index].base64 == $scope.mData.base642) {
                $scope.mData.base642 = null
                $scope.mData.fileName2 = null
                $scope.mData.mimeType2 = null
            // }
            // if ($scope.mData.tmpUploadFile[index].base64 == $scope.mData.base643) {
                $scope.mData.base643 = null
                $scope.mData.fileName3 = null
                $scope.mData.mimeType3 = null
            // }
            // if ($scope.mData.tmpUploadFile[index].base64 == $scope.mData.base644) {
                $scope.mData.base644 = null
                $scope.mData.fileName4 = null
                $scope.mData.mimeType4 = null
            // }
            // if ($scope.mData.tmpUploadFile[index].base64 == $scope.mData.base645) {
                $scope.mData.base645 = null
                $scope.mData.fileName5 = null
                $scope.mData.mimeType5 = null
            // }
            $scope.mData.tmpUploadFile.splice(index, 1);
        }

        $scope.removeImage = function (list, index) {
            list.UploadDataDocument.UpDocObj = null;
            list.UploadDataDocument.FileName = null;
            list.base64 = null;
            list.fileName = null;
            list.mimeType = null;
        }

        $scope.getPPN = function (noNpwp) {
            if (noNpwp !== undefined && noNpwp !== null && noNpwp !== ''){
                noNpwp = noNpwp.toString();
                if (noNpwp.length === 20){
                    if (noNpwp !== '00.000.000.0-000.000'){
                        ASPricingEngine.getPPN(1, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    } else {
                        ASPricingEngine.getPPN(0, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    }
                } else {
                    ASPricingEngine.getPPN(0, 3).then(function (res) {
                        PPNPerc = res.data
                    });
                }

            } else {
                // param 1 adalah isnpwp, param 2 tipe ppn
                ASPricingEngine.getPPN(0, 3).then(function (res) {
                    PPNPerc = res.data
                });
            }
            
        };

        function getMod(a, b) {
            return (a & Math.pow(2, b)) == Math.pow(2, b)
        }
        $scope.RightsEnableForMenu = {};
        function checkBytes(u) {
            $scope.RightsEnableForMenu.allowNew = getMod(u, 1);
            $scope.RightsEnableForMenu.allowEdit = getMod(u, 2);
            $scope.RightsEnableForMenu.allowDelete = getMod(u, 3);
            $scope.RightsEnableForMenu.allowApprove = getMod(u, 4);
            $scope.RightsEnableForMenu.allowReject = getMod(u, 5);
            $scope.RightsEnableForMenu.allowReview = getMod(u, 6);
            $scope.RightsEnableForMenu.allowPrint = getMod(u, 7);
            $scope.RightsEnableForMenu.allowPart = getMod(u, 8);
            $scope.RightsEnableForMenu.allowConsumeable = getMod(u, 9);
            $scope.RightsEnableForMenu.allowGR = getMod(u, 10);
            $scope.RightsEnableForMenu.allowBP = getMod(u, 11);
        }

        $scope.dataPRRType = [
            {PRRTypeId : '1', PRRTypeName : '1'},
            {PRRTypeId : '2', PRRTypeName : '2'},
            {PRRTypeId : '3', PRRTypeName : '3'},
            {PRRTypeId : '4', PRRTypeName : '4'},
            {PRRTypeId : '5', PRRTypeName : '5'},
            {PRRTypeId : '6', PRRTypeName : '6'},
        ]
        $scope.getTableHeight = function () {
            var rowHeight = 30; // your row height
            var headerHeight = 50; // your header height
            var filterHeight = 40; // your filter height
            var pageSize = $scope.grid.paginationPageSize;
            if ($scope.grid.paginationPageSize > $scope.grid.data.length) {
                pageSize = $scope.grid.data.length;
            }
            if (pageSize < 4) {
                pageSize = 3;
            }
            var height = (pageSize * rowHeight + headerHeight) + 50;
            $('div[ui-grid="grid"]').height(height);

            return {
                height: (pageSize * rowHeight + headerHeight) + 50 + "px"
            };
        };
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        console.log("user", $scope.user);
        $scope.mData = {}; //Model
        $scope.mFilter = {}; //Model FIlter
        $scope.xRole = { selected: [] };
        // $scope.mData.Approver = $scope.user;
        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.ismeridian = false;
        $scope.hstep = 1;
        $scope.mstep = 1;

        $scope.DateOptions = { // ini huruf gede kecil nya ngefek.. yg atas ga dihapus takut kepake
            startingDay: 1,
            format: 'dd/MM/yyyy',
            //disableWeekend: 1
        };
        // var showData =[{
        //      Id:1,
        //      Name:"New"
        // },
        // {
        //      Id:2,
        //      Name:"Old"
        // }];
        // $scope.filterStatus = showData;

        var typeWarranty = [
            {
                typeWaranty1: 0,
                // typeWaranty1: 2400,
                NameWarranty: "ALL"
            },
            {
                typeWaranty1: 59,
                // typeWaranty1: 2207,
                NameWarranty: "TWC"
            },
            {
                typeWaranty1: 60,
                // typeWaranty1: 2400,
                NameWarranty: "PWC"
            },
            {
                typeWaranty1: 2675,
                // typeWaranty1: 2400,
                NameWarranty: "FPC - Free Parts Claim"
            },
            {
                typeWaranty1: 2677,
                // typeWaranty1: 2207,
                NameWarranty: "FLC - Free Labor Claim"
            },
            {
                typeWaranty1: 2678,
                // typeWaranty1: 2400,
                NameWarranty: "PLC - Parts Labor Claim"
            }
        ];

        var typeWarrantyBP = [
            {
                typeWaranty1: 0,
                // typeWaranty1: 2400,
                NameWarranty: "ALL"
            },
            {
                // typeWaranty1: 2447,
                typeWaranty1: 2653,
                NameWarranty: "TWC"
            },
            {
                // typeWaranty1: 2448,
                typeWaranty1: 2451,
                NameWarranty: "PWC"
            }
        ];

        //Jika BP maka menggunakan warranty type BP
        if ($scope.user.RoleId == 1129 || $scope.user.RoleId == 1029) {
            $scope.filterTypeWar = typeWarrantyBP;
        }
        else {
            $scope.filterTypeWar = typeWarranty;
        }

        $scope.onBeforeNewMode = function (row, mode) {
            $scope.mData.EntryTime = new Date();
        }

        $scope.FreeClaimPRR = false;
        $scope.onShowDetail = function (row, mode) {
            console.log('row upload', row)
            console.log('mode upload', mode)
            $scope.modalmainopeno.show = false;
            
            if (row.JobTypeId == 2675 || row.JobTypeId == 2677 || row.JobTypeId == 2678) {
                $scope.FreeClaimPRR = true;
            }else{
                $scope.FreeClaimPRR = false;
            }
            
            if (mode == 'view'){
                $scope.modeUpload = 'view'
                $scope.disablePRR = true
                $scope.statusW = false
                $scope.canEditope = false

            } else {
                $scope.modeUpload = 'edit'
                warranty.CekIsPurchaseOrderSOP(row.JobId).then(function (resul){
                    $scope.disablePRR = true;
                    if (resul.data == 666){
                        $scope.disablePRR = true;
                    } else if (resul.data == 0){
                        $scope.disablePRR = false;
                    } else {
                        $scope.disablePRR = true;
                    }

                    if (row.StatusApprovalId == 2){
                        $scope.statusW = false
                    } else {
                        $scope.statusW = true;
                        $scope.disablePRR = true;
                        bsAlert.alert({
                            title: "Data Sudah di Approve / Reject",
                            text: "Tidak Bisa Edit",
                            type: "warning",
                            showCancelButton: false
                        });
                    }
                })

                if (row.IsReSubmitWarranty === 1){
                    $scope.canEditope = true
                } else {
                    $scope.canEditope = false
                }
            }
            console.log("row", row);
            $scope.dataFromgrid = row;
            console.log("data from grid 123", $scope.dataFromgrid);
            console.log("$scope.mData.tmpUploadFile detail", $scope.mData.tmpUploadFile);
            warranty.getDataDetail(row.ClaimNo, row.StatusApprovalId)
                .then(
                    function (res) {
                        $scope.dataWarranty = res.data.Result;
                        console.log('$scope.dataWarranty detail', $scope.dataWarranty)
                        $scope.dataBaruT1 = null;
                        $scope.dataBaruT2 = null;
                        for (var i=0; i<$scope.dataWarranty.length; i++){
                            $scope.dataWarranty[i].dataArrayKe = i;
                            $scope.dataWarranty[i].T2_Cd = $scope.dataWarranty[i].T2_Cd.trim();

                            if ($scope.dataWarranty[i].Caused !== null && $scope.dataWarranty[i].Caused !== ''){
                                var causedTemp = $scope.dataWarranty[i].Caused.replace("-", "@");
                                

                                var t2_splitCaused = causedTemp.split('@');
                                var t2_splitCaused_code = t2_splitCaused[0].trim();
                                var t2_splitCaused_name = t2_splitCaused[1].trim();
                                $scope.dataWarranty[i].T2_xName = t2_splitCaused_code + ' - ' + t2_splitCaused_name
                            } else {
                                $scope.dataWarranty[i].T2_xName = ''
                            }
                            
                        }
                        // $scope.dataWarranty[0].PartNo = '43512-0K020'
                        // $scope.dataWarranty[1].PartNo = '43512-0K020'
                        // $scope.dataWarranty[2].PartNo = '08880-83221'
                        // $scope.dataWarranty[3].PartNo = '08880-83221'
                        //$scope.dataWarrantyJob = _.uniq($scope.dataWarranty, 'JobTaskId');
                        //======================== di bawah ini punya pa wildan ================ start
                        // $scope.dataWarrantyJob =_.uniq($scope.dataWarranty,function(v){
                        //     if (v.JobSeqNo == null || v.JobSeqNo == undefined)
                        //     {
                        //         return v = [v.claimNo, v.JobId].join();
                        //     }
                        //     return v = [v.claimNo, v.JobSeqNo].join();
                        // });
                        // $scope.dataWarrantyJobTask =_.uniq($scope.dataWarranty,function(v,k){
                        //     if (v.JobSeqNo == null || v.JobSeqNo == undefined)
                        //     {
                        //         return v = [v.claimNo, v.JobId].join();
                        //     }
                        //     return v = [v.claimNo, v.JobSeqNo].join();
                        // });
                        // $scope.dataWarrantyParts =_.uniq($scope.dataWarranty,function(v){
                        //     return v = [v.claimNo, v.PartNo].join();
                        // });
                        // //$scope.dataWarrantySub = _.uniq($scope.dataWarranty, 'JobClaimId');
                        // $scope.dataWarrantySub =_.uniq($scope.dataWarranty,function(v){
                        //     return v = [v.claimNo, v.InvoiceNo].join();
                        // });
                        //======================== di bawah ini punya pa wildan ================ end
                        var getUnique = function (array){
                            var uniqArray = []
                            for (var i=0; i<array.length; i++){
                                if (uniqArray.length === 0){
                                    uniqArray.push(array[i])
                                } else {
                                    var adaSama = 0
                                    for (var j=0; j<uniqArray.length; j++){
                                        if (array[i].JobTaskId == uniqArray[j].JobTaskId){
                                            adaSama++
                                        }
                                    }
                                    if (adaSama === 0){
                                        uniqArray.push(array[i])
                                    }
                                }
                                
                            }
                            return uniqArray
                            
                        }

                        var uniqjob = getUnique($scope.dataWarranty)

                        // ------------------ hitung harga labor, sublet, total claim, tax ---------------------------------start

                        for (var q=0; q<uniqjob.length; q++){
                            uniqjob[q].LaborTotClaim_without_ppn = ASPricingEngine.calculate({
                                                                        InputPrice: uniqjob[q].LaborTotClaim,
                                                                        // Discount: 5,
                                                                        // Qty: 2,
                                                                        Tipe: 2, 
                                                                        PPNPercentage: PPNPerc,
                                                                    });

                            uniqjob[q].SubletTotClaim_without_ppn = ASPricingEngine.calculate({
                                                                        InputPrice: uniqjob[q].SubletTotClaim,
                                                                        // Discount: 5,
                                                                        // Qty: 2,
                                                                        Tipe: 2, 
                                                                        PPNPercentage: PPNPerc,
                                                                    });

                            // WarrType: PL (Part Labor), FL(Free Labor), FP(Free Part)
                            if (uniqjob[q].WarrType == 'PL' || uniqjob[q].WarrType == 'FP' || uniqjob[q].WarrType == 'FL') {
                                // var PartTotClaim = Math.floor(uniqjob[q].PartTotClaim / 1.11); 
                                uniqjob[q].PartTotClaim = ASPricingEngine.calculate({
                                                            InputPrice: uniqjob[q].PartTotClaim,
                                                            // Discount: 5,
                                                            // Qty: 2,
                                                            Tipe: 2, 
                                                            PPNPercentage: PPNPerc,
                                                        }); 
                            }                                      
                            // console.log('PartTotClaim', PartTotClaim); 
                            // uniqjob[q].PartTotClaim = PartTotClaim;

                            uniqjob[q].ClaimTotAmount_without_ppn = uniqjob[q].PartTotClaim + uniqjob[q].LaborTotClaim_without_ppn + uniqjob[q].SubletTotClaim_without_ppn

                            uniqjob[q].TaxTot_without_ppn = ASPricingEngine.calculate({
                                                                InputPrice: (uniqjob[q].ClaimTotAmount_without_ppn * (1+PPNPerc/100)),
                                                                // Discount: 5,
                                                                // Qty: 2,
                                                                Tipe: 33, 
                                                                PPNPercentage: PPNPerc,
                                                            });


                        }

                        // ------------------ hitung harga labor, sublet, total claim, tax ---------------------------------



                        // ----------------------------------- data warrantty sub uniq -------------------------------------- start
                        var getUniqueSub = function (array){
                            var uniqArray = []
                            for (var i=0; i<array.length; i++){
                                if (uniqArray.length === 0){
                                    uniqArray.push(array[i])
                                } else {
                                    var adaSama = 0
                                    for (var j=0; j<uniqArray.length; j++){
                                        if (array[i].JobClaimOPLId == uniqArray[j].JobClaimOPLId){
                                            adaSama++
                                        }
                                    }
                                    if (adaSama === 0){
                                        uniqArray.push(array[i])
                                    }
                                }
                                
                            }
                            return uniqArray
                            
                        }

                        var uniqSub = getUniqueSub($scope.dataWarranty)

                        // ----------------------------------- data warrantty sub uniq -------------------------------------- end


                        $scope.dataWarrantyJob = angular.copy(uniqjob);
                        $scope.dataWarrantyJobTask = angular.copy(uniqjob);
                        $scope.dataWarrantyParts = angular.copy($scope.dataWarranty);
                        // $scope.dataWarrantySub = angular.copy($scope.dataWarranty);
                        $scope.dataWarrantySub = angular.copy(uniqSub);


                        // ============================== try uniq dari BE, semoga ini ga di pake lagi ================================================================================================================== start
                        // var data1Splice = [];
                        // for (var i = 0; i < $scope.dataWarrantyJob.length; i++) {
                        //     for (var j = 0; j < $scope.dataWarrantyJob.length; j++) {
                        //         if ($scope.dataWarrantyJob[i].JobSeqNo == null || $scope.dataWarrantyJob[i].JobSeqNo == undefined) {
                        //             if ($scope.dataWarrantyJob[i].claimNo == $scope.dataWarrantyJob[j].claimNo && $scope.dataWarrantyJob[i].JobId == $scope.dataWarrantyJob[j].JobId && i != j) {
                        //                 data1Splice.push(j);
                        //             }
                        //         } else {
                        //             if ($scope.dataWarrantyJob[i].claimNo == $scope.dataWarrantyJob[j].claimNo && $scope.dataWarrantyJob[i].JobSeqNo == $scope.dataWarrantyJob[j].JobSeqNo && i != j) {
                        //                 data1Splice.push(j);
                        //             }
                        //         }
                        //     }
                        // }
                        // for (var k = 0; k < $scope.dataWarrantyJob.length; k++) {
                        //     for (var l = 0; l < data1Splice.length; l++) {
                        //         if (k == data1Splice[l]) {
                        //             $scope.dataWarrantyJob.splice(k, 1);
                        //         }
                        //     }
                        // }

                        // var data2Splice = [];
                        // for (var i = 0; i < $scope.dataWarrantyJobTask.length; i++) {
                        //     for (var j = 0; j < $scope.dataWarrantyJobTask.length; j++) {
                        //         if ($scope.dataWarrantyJobTask[i].JobSeqNo == null || $scope.dataWarrantyJobTask[i].JobSeqNo == undefined) {
                        //             if ($scope.dataWarrantyJobTask[i].claimNo == $scope.dataWarrantyJobTask[j].claimNo && $scope.dataWarrantyJobTask[i].JobId == $scope.dataWarrantyJobTask[j].JobId && i != j) {
                        //                 data2Splice.push(j);
                        //             }
                        //         } else {
                        //             if ($scope.dataWarrantyJobTask[i].claimNo == $scope.dataWarrantyJobTask[j].claimNo && $scope.dataWarrantyJobTask[i].JobSeqNo == $scope.dataWarrantyJobTask[j].JobSeqNo && i != j) {
                        //                 data2Splice.push(j);
                        //             }
                        //         }
                        //     }
                        // }
                        // for (var k = 0; k < $scope.dataWarrantyJobTask.length; k++) {
                        //     for (var l = 0; l < data2Splice.length; l++) {
                        //         if (k == data2Splice[l]) {
                        //             $scope.dataWarrantyJobTask.splice(k, 1);
                        //         }
                        //     }
                        // }

                        // var data3Splice = [];
                        // for (var i = 0; i < $scope.dataWarrantyParts.length; i++) {
                        //     for (var j = 0; j < $scope.dataWarrantyParts.length; j++) {
                        //         // if ($scope.dataWarrantyParts[i].JobSeqNo == null || $scope.dataWarrantyParts[i].JobSeqNo == undefined){
                        //         //     if ($scope.dataWarrantyParts[i].claimNo == $scope.dataWarrantyParts[j].claimNo && $scope.dataWarrantyParts[i].JobId == $scope.dataWarrantyParts[j].JobId && i != j){
                        //         //         data3Splice.push(j);
                        //         //     }
                        //         // }
                        //         if ($scope.dataWarrantyParts[i].claimNo == $scope.dataWarrantyParts[j].claimNo && $scope.dataWarrantyParts[i].PartNo == $scope.dataWarrantyParts[j].PartNo && i != j) {
                        //             data3Splice.push(j);
                        //         }
                        //     }
                        // }
                        // // var ar3 = []
                        // for (var k = 0; k < $scope.dataWarrantyParts.length; k++) {
                        //     var ketemuSama = 0;
                        //     for (var l = 0; l < data3Splice.length; l++) {
                        //         if (k == data3Splice[l]) {
                        //             $scope.dataWarrantyParts.splice(k, 1);
                        //             // ketemuSama++;
                        //         }
                        //     }
                        //     // if (ketemuSama == 0){
                        //     //     ar3.push($scope.dataWarrantyParts[k])
                        //     // }
                        // }
                        // // $scope.dataWarrantyParts = ar3;

                        // var data4Splice = [];
                        // for (var i = 0; i < $scope.dataWarrantySub.length; i++) {
                        //     for (var j = 0; j < $scope.dataWarrantySub.length; j++) {
                        //         // if ($scope.dataWarrantySub[i].JobSeqNo == null || $scope.dataWarrantySub[i].JobSeqNo == undefined){
                        //         //     if ($scope.dataWarrantySub[i].claimNo == $scope.dataWarrantySub[j].claimNo && $scope.dataWarrantySub[i].JobId == $scope.dataWarrantySub[j].JobId && i != j){
                        //         //         data3Splice.push(j);
                        //         //     }
                        //         // }
                        //         if ($scope.dataWarrantySub[i].claimNo == $scope.dataWarrantySub[j].claimNo && $scope.dataWarrantySub[i].InvoiceNo == $scope.dataWarrantySub[j].InvoiceNo
                        //             && $scope.dataWarrantySub[i].SubletType == $scope.dataWarrantySub[j].SubletType && $scope.dataWarrantySub[i].SubletAmount == $scope.dataWarrantySub[j].SubletAmount && $scope.dataWarrantySub[i].SubletDesc == $scope.dataWarrantySub[j].SubletDesc && i != j) {
                        //             data4Splice.push(j);
                        //         }
                        //     }
                        // }
                        // for (var k = 0; k < $scope.dataWarrantySub.length; k++) {
                        //     for (var l = 0; l < data4Splice.length; l++) {
                        //         if (k == data4Splice[l]) {
                        //             $scope.dataWarrantySub.splice(k, 1);
                        //         }
                        //     }
                        // }
                        // ============================== try uniq dari BE, semoga ini ga di pake lagi ================================================================================================================== end


                        for (var g = 0; g < $scope.dataWarrantySub.length; g++) {
                            $scope.dataWarrantySub[g].SeqNox = g + 1;
                            $scope.dataWarrantySub[g].SeqNox = $scope.dataWarrantySub[g].SeqNox.toString();
                        }

                        //ubah type prrtype jadi string
                        for (var q = 0; q<$scope.dataWarrantyParts.length; q++){
                            $scope.dataWarrantyParts[q].PRRType = $scope.dataWarrantyParts[q].PRRType.toString()
                        }
                        console.log("data detail 1", $scope.dataWarrantyJob);
                        console.log("data detail 2", $scope.dataWarrantyJobTask);
                        console.log("data detail 3", $scope.dataWarrantyParts);
                        console.log("data detail 4", $scope.dataWarrantySub);

                        if (mode == 'view'){
                            $scope.disablePRR = true
                            $scope.statusW = false
                            $scope.canEditope = false
                            console.log('cek data row yeu', row)
                            $scope.dataUpload = row;
                            $scope.dataUpload.UploadDataDocument = [];      
                            $scope.mData.tmpUploadFile = [];
                            warranty.getDataUploadFile(row.ClaimNo).then(function(res){
                                console.log('data upload file yeuu', res);
                                var getUploadFile = res.data.Result[0];
                                if(getUploadFile.base641 != null && getUploadFile.fileName1 != null && getUploadFile.mimeType1 != null){
                                    $scope.mData.tmpUploadFile.push({
                                        base64 : getUploadFile.base641,
                                        fileName : getUploadFile.fileName1,
                                        mimeType : getUploadFile.mimeType1,
                                        UploadDataDocument:{
                                            UpDocObj: getUploadFile.mimeType1+";base64,"+getUploadFile.base641,
                                            FileName: getUploadFile.fileName1
                                        }
                                    })
                                    
                                }
            
                                if(getUploadFile.base642 != null && getUploadFile.fileName2 != null && getUploadFile.mimeType2 != null){
                                    $scope.mData.tmpUploadFile.push({
                                        base64 : getUploadFile.base642,
                                        fileName : getUploadFile.fileName2,
                                        mimeType : getUploadFile.mimeType2,
                                        UploadDataDocument:{
                                            UpDocObj: getUploadFile.mimeType2+";base64,"+getUploadFile.base642,
                                            FileName: getUploadFile.fileName2
                                        }
                                    })
                                }
                
                                if(getUploadFile.base643 != null && getUploadFile.fileName3 != null && getUploadFile.mimeType3 != null){
                                    $scope.mData.tmpUploadFile.push({
                                        base64 : getUploadFile.base643,
                                        fileName : getUploadFile.fileName3,
                                        mimeType : getUploadFile.mimeType3,
                                        UploadDataDocument:{
                                            UpDocObj: getUploadFile.mimeType3+";base64,"+getUploadFile.base643,
                                            FileName: getUploadFile.fileName3
                                        }
                                    })
                                }
                
                                if(getUploadFile.base644 != null && getUploadFile.fileName4 != null && getUploadFile.mimeType4 != null){
                                    $scope.mData.tmpUploadFile.push({
                                        base64 : getUploadFile.base644,
                                        fileName : getUploadFile.fileName4,
                                        mimeType : getUploadFile.mimeType4,
                                        UploadDataDocument:{
                                            UpDocObj: getUploadFile.mimeType4+";base64,"+getUploadFile.base644,
                                            FileName: getUploadFile.fileName4
                                        }
                                    })
                                }
                
                                if(getUploadFile.base645 != null && getUploadFile.fileName5 != null && getUploadFile.mimeType5 != null){
                                    $scope.mData.tmpUploadFile.push({
                                        base64 : getUploadFile.base645,
                                        fileName : getUploadFile.fileName5,
                                        mimeType : getUploadFile.mimeType5,
                                        UploadDataDocument:{
                                            UpDocObj: getUploadFile.mimeType5+";base64,"+getUploadFile.base645,
                                            FileName: getUploadFile.fileName5
                                        }
                                    })
                                }
                            });
                            

                            console.log('$scope.mData.tmpUploadFile mamang', $scope.mData.tmpUploadFile)
                            
                            if ($scope.mData.SpecialClaimFlag == 1) {
                                $scope.CheckSpesialClaim = true;
                            }else{
                                $scope.CheckSpesialClaim = false;
                            }
            
                        }

                    },
                    function (err) {
                        console.log("err=>", err);
                    }
                );
        }

        $scope.selectT1 = function (data){
            console.log('data t1', data)
            $scope.dataBaruT1 = data
        }
        $scope.selectT2 = function (data){
            console.log('data t2', data)
            $scope.dataBaruT2 = data
        }

        $scope.onValidateSave = function () {
            console.log('$scope.mData.tmpUploadFile mamang', $scope.mData.tmpUploadFile)
            var dt = new Date('"' + $scope.mData.EntryTime + '"');
            dt.setSeconds(0);
            var dtFrom = dt.toTimeString();
            dtFrom = dtFrom.split(' ');
            $scope.mData.EntryTime = dtFrom[0];
            $scope.mData.dataWarrantyJob = $scope.dataWarrantyJob
            $scope.mData.dataWarrantyJobTask = $scope.dataWarrantyJobTask
            $scope.mData.dataWarrantyParts = $scope.dataWarrantyParts
            $scope.mData.dataWarrantySub = $scope.dataWarrantySub
            console.log('$scope.dataWarrantyJob 123', $scope.dataWarrantyJob)
            var dataPRR = []
            var dataTask = []
            for(var i=0; i<$scope.dataWarrantyParts.length; i++){
                var obj = {}
                obj.PRRType = $scope.dataWarrantyParts[i].PRRType
                obj.PartsNo = $scope.dataWarrantyParts[i].PartNo
                dataPRR.push(obj);                
            }
            for(var j=0; j<$scope.dataWarrantyJob.length; j++){
                var obj = {}
                obj.JobClaimId = $scope.dataWarrantyJob[j].JobClaimId
                // obj.JobClaimPartId = $scope.dataWarrantyJob[j].JobClaimPartId
                obj.JobTaskId = $scope.dataWarrantyJob[j].JobTaskId
                obj.T1_Cd = $scope.dataWarrantyJob[j].T1_Cd
                // obj.T2_Cd = $scope.dataWarrantyJob[j].T2_Cd
                obj.OFP = $scope.dataWarrantyJob[j].OFP

                if ($scope.dataWarrantyJob[j].T2_xName == undefined) {
                    obj.T2_Cd = $scope.dataWarrantyJob[j].T2_Cd
                }else{
                    var tempT2Cd = $scope.dataWarrantyJob[j].T2_xName.split('-')
                    obj.T2_Cd = tempT2Cd[0].trim();
                }

                obj.Caused = $scope.dataWarrantyJob[j].T2_xName

                // for (var k=0; k<$scope.T2.length; k++){
                //     // if (obj.T2_Cd == $scope.T2[k].T2Code){
                //     //     obj.Caused = $scope.T2[k].xName
                //     // }
                //     if ($scope.dataBaruT2 === null){
                //         // berarti data t2 ga di rubah
                //         if ($scope.dataBaruT2 !== undefined){
                //             // berarti dropdown na ga kosng jg
                //             obj.Caused = $scope.dataWarrantyJob[j].Caused
                //         }
                //     }

                //     if ($scope.dataBaruT2 !== null && $scope.dataBaruT2 !== undefined){
                //         // berarti di rubah dan isi nya tidak kosong
                //         if ($scope.dataBaruT2.T2Id === $scope.T2[k].T2Id){
                //             obj.Caused = $scope.T2[k].xName
                //         }
                //     }
                // }

                for (var l=0; l<$scope.T1.length; l++){
                    // if (obj.T1_Cd == $scope.T1[l].T1Code){
                    //     obj.Condition = $scope.T1[l].xName
                    // }
                    if ($scope.dataBaruT1 === null){
                        // berarti data t1 ga di rubah
                        if ($scope.dataBaruT1 !== undefined){
                            // berarti dropdown na ga kosng jg
                            obj.Condition = $scope.dataWarrantyJob[j].Condition
                        }
                    }

                    if ($scope.dataBaruT1 !== null && $scope.dataBaruT1 !== undefined){
                        // berarti di rubah dan isi nya tidak kosong
                        if ($scope.dataBaruT1.T1Id === $scope.T1[l].T1Id){
                            obj.Condition = $scope.T1[l].xName
                        }
                    }
                }

                dataTask.push(obj);                
            }

            for (var i=0; i<$scope.dataWarrantyJobTask.length; i++){
                for (var j=0; j<dataTask.length; j++){
                    if ($scope.dataWarrantyJobTask[i].JobTaskId == dataTask[j].JobTaskId){
                        if ($scope.dataWarrantyJobTask[i].TaskId == '' || $scope.dataWarrantyJobTask[i].TaskId == null || $scope.dataWarrantyJobTask[i].TaskId == undefined){
                            dataTask[j].TaskId = 0
                        } else {
                            dataTask[j].TaskId = $scope.dataWarrantyJobTask[i].TaskId
                        }
                        dataTask[j].MainOpeno = $scope.dataWarrantyJobTask[i].MainOpeno
                    }
                }
            }


            $scope.mData.dataPRR = dataPRR
            $scope.mData.dataTask = dataTask
            

            return true;
        }

        var gridData = [];
        $scope.getData = function () {

            // console.log("nih", $scope.VM );
            if ($scope.mFilter.StartDate == undefined || $scope.mFilter.StartDate == "" || $scope.mFilter.StartDate == null
                || $scope.mFilter.StartDate == "Invalid Date" || $scope.mFilter.StopDate == undefined
                || $scope.mFilter.StopDate == "" || $scope.mFilter.StopDate == null || $scope.mFilter.StopDate == "Invalid Date"
                || $scope.mFilter.typeWarranty == null || $scope.mFilter.typeWarranty == undefined) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Isi Tanggal Mulai ,Akhir, Dan Type Warranty"
                    // ,
                    // content: error.join('<br>'),
                    // number: error.length
                });
            } else {
                var tmpDate = $scope.mFilter.StartDate;
                var tmpMonth = $scope.mFilter.StartDate;
                var tmpTime = $scope.mFilter.StartDate;
                if ((tmpDate.getMonth() + 1) < 10) {
                    tmpMonth = "0" + (tmpDate.getMonth() + 1);
                } else {
                    tmpMonth = tmpDate.getMonth() + 1;
                }
                var ini = tmpDate.getFullYear() + "-" + tmpMonth + "-" + tmpDate.getDate();
                var d = new Date(ini);
                console.log('data yang diinginkan', ini);
                $scope.mFilter.StartDate = ini;

                var tmpDate2 = $scope.mFilter.StopDate;
                console.log("var mounth", tmpDate2.getMonth() + 1);
                var tmpMonth2 = $scope.mFilter.StopDate;
                var tmpTime2 = $scope.mFilter.StopDate;
                if ((tmpDate2.getMonth() + 1) < 10) {
                    tmpMonth2 = "0" + (tmpDate2.getMonth() + 1);
                } else {
                    tmpMonth2 = tmpDate2.getMonth() + 1;
                }
                var ini2 = tmpDate2.getFullYear() + "-" + tmpMonth2 + "-" + tmpDate2.getDate();
                var d2 = new Date(ini2);
                console.log('data yang diinginkan 2', ini2);
                $scope.mFilter.StopDate = ini2

                if ($scope.mFilter.WoNo == "" || $scope.mFilter.WoNo == null || $scope.mFilter.WoNo == undefined) {
                    $scope.mFilter.WoNo = "-";
                } else {
                    $scope.mFilter.WoNo = $scope.mFilter.WoNo;
                }

                if ($scope.mFilter.status == "" || $scope.mFilter.status == null || $scope.mFilter.status == undefined) {
                    $scope.mFilter.status = "-";
                } else {
                    $scope.mFilter.status = $scope.mFilter.status;
                }
                console.log("mFilter", $scope.mFilter.WoNo);


                warranty.getData($scope.mFilter.StartDate, $scope.mFilter.StopDate, $scope.mFilter).then(function (res) {
                    // var inidata = res.data.Result;
                    // console.log("ini data", inidata);
                    gridData = [];

                    // for (var i = 0; i < res.data.Result.length; i++) {
                    //     if (res.data.Result[i].StatusApprovalId == 1) {
                    //         res.data.Result[i].xStatusApprovalId = "Approved";
                    //     } else if (res.data.Result[i].StatusApprovalId == 2) {
                    //         res.data.Result[i].xStatusApprovalId = "Reject";
                    //     } else if (res.data.Result[i].StatusApprovalId == 3) {
                    //         res.data.Result[i].xStatusApprovalId = "Request";
                    //     } else {
                    //         res.data.Result[i].xStatusApprovalId = " - ";
                    //     }
                    // };


                    _.map(res.data.Result, function (a) {
                        if (a.ApproveRejectDate == '0001-01-01T00:00:00') {
                            a.ApproveRejectDate = '-';
                            a.SendToTowasDate = '-';
                        }
                    });

                    //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
                    $scope.grid.data = res.data.Result;
                    // $scope.selected.data = res.data.Result
                    // console.log("role=>", res.data.Result);
                    //console.log("grid data=>",$scope.grid.data);
                    //$scope.roleData = res.data;
                    $scope.loading = false;
                    console.log("test", res.data.Result);
                    $scope.getTableHeight();
                    console.log("====as", $scope.formGridApi);
                },
                    function (err) {
                        console.log("err=>", err);
                    }
                );

            }
        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }
        var dataApprove = [];
        $scope.selectRole = function (rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function () { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function (rows) {
            console.log("onSelectRows=>", rows);
            dataApprove = rows;
        }

        $scope.show_modalDtl = { show: false };
        $scope.show_cantApprove = { show: false };

        $scope.modal_modelDtl = [];
        $scope.modalMode = 'new';
        $scope.dateApp = '';

        $scope.customApprove = function () {
            //debugger;
            $scope.clickAppWarranty = 0;
            console.log("ini approve", dataApprove);
            if (dataApprove[0].StatusApprovalId == 3) {
                bsAlert.alert({
                    title: "Claim Sudah Reject Kabeng, Harap Submit Ulang ",
                    text: "",
                    type: "error",
                    showCancelButton: false
                });
            } else if (dataApprove[0].StatusApprovalId == 5) {
                bsAlert.alert({
                    title: "Tidak Dapat Approve, Claim Sudah Reject Towass",
                    text: "",
                    type: "error",
                    showCancelButton: false
                });
            } else if (dataApprove[0].StatusApprovalId == 4 || dataApprove[0].StatusApprovalId == 6) {
                bsAlert.alert({
                    title: "Tidak Dapat Approve Claim, Karena Sudah Dalam Proses Towass",
                    text: "",
                    type: "error",
                    showCancelButton: false
                });
            } else if (dataApprove[0].ClaimAmount >= 1000000000) {
                bsAlert.alert({
                    title: "Claim hanya bisa diproses dengan total nominal < Rp. 1.000.000.000. Silahkan dicek kembali data yg akan diproses",
                    text: "",
                    type: "error",
                    showCancelButton: false
                });
            }else if(dataApprove[0].JobTypeId == 2675 || dataApprove[0].JobTypeId == 2677 || dataApprove[0].JobTypeId == 2678 || dataApprove[0].SpecialClaimFlag == 1){
                if (dataApprove[0].fileName1 == null || dataApprove[0].base641 == null) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Claim Tidak dapat disubmit dikarenakan upload file belum tersedia"
                    });
                }else{
                    var hariIni = new Date();
                    $scope.dateApp = hariIni.getFullYear() + '-' + (hariIni.getMonth() + 1) + '-' + hariIni.getDate();
                    $scope.dApprove = dataApprove;
                    $scope.show_modalDtl = { show: true };
                }
             } else {
                if (dataApprove[0].StatusTowass == 5 && dataApprove[0].StatusApprovalId == 2){
                    $scope.show_cantApprove.show = true;
                    return;
                }
                var hariIni = new Date();
                $scope.dateApp = hariIni.getFullYear() + '-' + (hariIni.getMonth() + 1) + '-' + hariIni.getDate();
                // $scope.dateApp = dataApprove[0].ApproveRejectDate.getFullYear() + '-' + (dataApprove[0].ApproveRejectDate.getMonth() + 1) + '-' + dataApprove[0].ApproveRejectDate.getDate();
                $scope.dApprove = dataApprove;
                console.log("date fix", $scope.dateApp);
                console.log("date aprv", $scope.dApprove);
                $scope.show_modalDtl = { show: true };
            }
        }

        $scope.doApprove = function () {
            $scope.clickAppWarranty++;
            if ($scope.clickAppWarranty == 1){
                console.log("ini approve gue", dataApprove);
                console.log("diapprove");
                console.log("aproveeeeee", dataApprove, $scope.mData, $scope.user, 6, $scope.dateApp);
                //debugger;
                for (var i = 0; i < dataApprove.length; i++) {
                    if (dataApprove[i].StatusTowass == 2) {
                        warranty.ApproveTowas(dataApprove, $scope.mData, $scope.user, 4, $scope.dateApp)
                            .then(function (res) {
                                console.log("resresres", res.data);
                                // WO.updateTeco(dataApprove[0].JobId).then(function(resu) {
    
                                // });

                                if (res.data == 666) {
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "TOWASS Under Maintenance, Mohon kembali beberapa saat lagi atau hubungi PIC TOWASS (TAM) untuk mengetahui masalah ini."
                                    });
                                    $scope.mData.RejectReason = "";
                                    $scope.show_modalDtl = { show: false };
                                    $scope.getData();
                                } else {
                                    $scope.mData.RejectReason = "";
                                    $scope.show_modalDtl = { show: false };
        
                                    var errMsg = angular.copy(res.data);
                                    var showMsg = "";
                                    if (errMsg.detailMessages != null && errMsg.detailMessages != undefined) {
                                        if (errMsg.detailMessages.length > 0) {
                                            for (var i = 0; i < errMsg.detailMessages.length; i++) {
                                                showMsg = showMsg + "<br>" + errMsg.detailMessages[i];
                                            };
                                        }
            
                                        if (errMsg.status == false) {
                                            bsNotify.show({
                                                size: 'small',
                                                type: 'danger',
                                                title: "Peringatan",
                                                text: 'I will close in 5 seconds.',
                                                timeout: 5000,
                                                content: errMsg.message + showMsg
                                            });
                                        }
                                        else {
                                            bsNotify.show({
                                                size: 'small',
                                                type: 'success',
                                                title: "Info",
                                                text: 'I will close in 5 seconds.',
                                                timeout: 5000,
                                                content: errMsg.message + showMsg
                                            });
                                        }
                                    }
                                    $scope.getData();
                                }

                                
    
                            },
                                function (err) {
                                    console.log("err=>", err);
                                });
                    }
                    if (dataApprove[i].StatusTowass == 4) {
                        warranty.ApproveTowas(dataApprove, $scope.mData, $scope.user, 6, $scope.dateApp)
                            .then(function (res) {
                                console.log("resresres", res.data);
                                // WO.updateTeco(dataApprove[0].JobId).then(function(resu) {
    
                                // });

                                if (res.data == 666) {
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "TOWASS Under Maintenance, Mohon kembali beberapa saat lagi atau hubungi PIC TOWASS (TAM) untuk mengetahui masalah ini."
                                    });
                                    $scope.mData.RejectReason = "";
                                    $scope.show_modalDtl = { show: false };
                                    $scope.getData();
                                } else {
                                    $scope.mData.RejectReason = "";
                                    $scope.show_modalDtl = { show: false };
        
                                    var errMsg = angular.copy(res.data);
                                    var showMsg = "";
        
                                    if (errMsg.detailMessages != null && errMsg.detailMessages != undefined) {
                                        if (errMsg.detailMessages.length > 0) {
                                            for (var i = 0; i < errMsg.detailMessages.length; i++) {
                                                showMsg = showMsg + "<br>" + errMsg.detailMessages[i];
                                            };
                                        }
        
                                        if (errMsg.status == false) {
                                            bsNotify.show({
                                                size: 'small',
                                                type: 'danger',
                                                title: "Peringatan",
                                                text: 'I will close in 5 seconds.',
                                                timeout: 5000,
                                                content: errMsg.message + showMsg
                                            });
                                        }
                                        else {
                                            bsNotify.show({
                                                size: 'small',
                                                type: 'success',
                                                title: "Info",
                                                text: 'I will close in 5 seconds.',
                                                timeout: 5000,
                                                content: errMsg.message + showMsg
                                            });
                                        }
                                    }
                                    $scope.getData();
                                }

                                
    
                            },
                                function (err) {
                                    console.log("err=>", err);
                                });
                    }
                    else {
                        // $scope.show_modalDtl = { show: false };

                        // $timeout(function () {
                        //     $scope.show_cantApprove.show = true;
                        // }, 100);
                        
    
                    }
                }
            }
            
        }

        $scope.cancelApprove = function () {
            $scope.show_modalDtl = { show: false };
        }

        $scope.show_modalReject = { show: false };
        $scope.modal_modelReject = [];
        $scope.modalModeReject = 'new';

        $scope.customReject = function () {
            $scope.clickRejWarranty = 0;
            console.log("ini Reject", dataApprove);
            if (dataApprove[0].StatusApprovalId == 4 || dataApprove[0].StatusApprovalId == 6) {
                bsAlert.alert({
                    title: "Tidak Dapat Reject Claim, Karena Sudah Dalam Proses Towass",
                    text: "",
                    type: "error",
                    showCancelButton: false
                });
            } else {
                $scope.dReject = dataApprove;
                var DateReject = new Date();
                $scope.dateApp = DateReject.getFullYear() + '-' + (DateReject.getMonth() + 1) + '-' + DateReject.getDate();
                console.log("date fix", $scope.dateApp);
                $scope.show_modalReject = { show: true };
            }
        }

        $scope.doReject = function () {
            $scope.clickRejWarranty++;
            if ($scope.clickRejWarranty == 1){
                $scope.show_modalReject = { show: false };

                warranty.ApproveTowas(dataApprove, $scope.mData, $scope.user, 3, $scope.dateApp)
                    .then(function (res) {
                        if (res.data == 666) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "TOWASS Under Maintenance, Mohon kembali beberapa saat lagi atau hubungi PIC TOWASS (TAM) untuk mengetahui masalah ini."
                            });
                        } 
                        $scope.mData.RejectReason = "";
                        $scope.show_modalReject = { show: false };
                        $scope.getData();
                    },
                        function (err) {
                            console.log("err=>", err);
                        });
            }
        }

        $scope.cancelReject = function () {
            $scope.show_modalReject = { show: false };
        }

        $scope.modalMainOpeNo = 'new'
        $scope.EditMainOpeNo = function (JobTaskId, MainOpeno, dataLabor) {
            $scope.dataModal = {}
            $scope.dataModal.MainOpenoLama = angular.copy(MainOpeno)
            $scope.dataModal.HourLama = angular.copy(dataLabor.Hour)
            $scope.dataModal.JobTaskId = angular.copy(JobTaskId)
            $scope.modalmainopeno.show = true;

        }

        $scope.doCancelEditMainOpeNo = function() {
            $scope.dataModal = {}
        }

        $scope.doSaveEditMainOpeNo = function() {
            console.log('data modal', $scope.dataModal)
            console.log('data lama', $scope.dataWarrantyJobTask)

            for (var i=0; i<$scope.dataWarrantyJobTask.length; i++){
                if ($scope.dataWarrantyJobTask[i].JobTaskId == $scope.dataModal.JobTaskId){
                    $scope.dataWarrantyJobTask[i].MainOpeno = angular.copy($scope.dataModal.MainOpenoBaru)
                    $scope.dataWarrantyJobTask[i].TaskId = angular.copy($scope.dataModal.TaskId)
                }
            }
            $scope.modalmainopeno.show = false;

        }

        $scope.getMainOpeno = function(key) {
           
            if ($scope.dataWarrantyJobTask[0].VIN != null) {

                var VIN = $scope.dataWarrantyJobTask[0].VIN
                var catg = 59
                var vehModelId = 0
                if (VIN != null && catg != null) {
                    var ress = warranty.getDataTaskOpe(key, VIN, catg, vehModelId).then(function(resTask) {
                        return resTask.data.Result;
                    });
                    console.log("ress", ress);
                    return ress;
                };
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "No Rangka Kendaraan Kosong",
                });
            };
        
        };

        $scope.onSelectWork = function($item, $model, $label) {
            console.log("onSelectWork=>", $item);
            console.log("onSelectWork=>", $model);
            console.log("onSelectWork=>", $label);

            $scope.dataModal.TaskId = $item.TaskId

            var splitmodel = $model.split('|')

            $scope.dataModal.MainOpenoBaru = splitmodel[0].trim()
            $scope.dataModal.HourBaru = splitmodel[1].trim()
            $scope.dataModal.MainOpenameBaru = splitmodel[2].trim()
           
        };

        $scope.onNoResult = function() {
            console.log('no result nih')
            $scope.dataModal.MainOpenoBaru = ''
            $scope.dataModal.HourBaru = ''
            $scope.dataModal.MainOpenameBaru = ''
        }

        $scope.onGotResult = function() {
            console.log("onGotResult=>");
        };

        $scope.actResubmit = function(row) {
            warranty.ReSubmitWarranty(row.TransactionId).then(function(result){
                bsAlert.alert({
                    title: "Proses Resubmit Warranty Berhasil",
                    text: "",
                    type: "success",
                    showCancelButton: false
                });
                $scope.getData();
            });
        }


        $scope.docantApprove = function () {
            $scope.show_cantApprove = { show: false };
        }

        $scope.cancelcantApprove = function () {
            $scope.show_cantApprove = { show: false };
        }

        // 1	Open
        // 2	Waiting Approval Kabeng
        // 3	Rejected By Kabeng
        // 4	Waiting Approval TOWASS (Approved By Kabeng)
        // 5	Rejected By TOWASS
        // 6	Approved Claim

        $scope.gridActionTemplate = '<div class="ui-grid-cell-contents"> \
                <a href="#"  ng-click="grid.appScope.actView(row.entity)" uib-tooltip="View" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-list-alt fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
                <a href="#" ng-hide="!grid.appScope.$parent.RightsEnableForMenu.allowEdit"  ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Edit" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-pencil fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
                <a href="#" ng-hide="row.entity.StatusApprovalId != 5" ng-click="grid.appScope.$parent.actResubmit(row.entity)" uib-tooltip="Re-Submit" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-undo" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
                </div>';


        //----------------------------------
        // Grid Setup
        //----------------------------------

        $scope.gridDataDokumen = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //data: 'ListMasterDokumen',
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                { name: 'Tipe Dokumen', field: 'DocumentId', visible: false },
                { name: 'Tipe Dokumen', field: 'DocumentType', width: '20%' },
                { name: 'Dokumen', field: 'DocumentName' }
            ]
        };

        var dateFilter = 'date:"dd/MM/yyyy"';
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
            enablePaginationControls: true,
            // data : contohData,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // { name:'Warranty Id',    field:'warrantyId', width:'7%', visible:false },
                { name: 'No LT', displayName: 'Claim No.', field: 'LTNo', width: 100 },
                { name: 'No WO', displayName: 'WO No.', width: 180, field: 'wono', allowCellFocus: false },
                { name: 'Grand Total Amount', displayName: 'Claim Amount (DMS)', width: 120, field: 'ClaimAmount', cellFilter: 'currency:"":0' },
                // { name: 'Status', displayName: 'Status', width: '8%', field: 'StatusTowass' },
                { name: 'Type Warranty', displayName: 'Claim Type', width: 120, field: 'nameWaranty' },
                { name: 'No Warranty TAM', displayName: 'No. Warranty TAM', width: 140, field: 'ClaimNo', headerAlignment: 'center' },
                { name: 'Acceptable Amount', displayName: 'Grand Total (TOWASS)', width: 100, field: 'ClaimGrandTotal', cellFilter: 'currency:"":0' },
                { name: 'Error Code', displayName: 'Error Code', width: 100, field: 'ErrorCode' },
                { name: 'Status ToWass', displayName: 'Status (TOWASS)', width: 300, field: 'xStatusTowass' },
                { name: 'Status Approval', displayName: 'Approval Status', width: 300, field: 'xStatusApproval' },
                { name: 'Approval Date', displayName: 'Approval Date (DMS)', width: 120, field: 'ApproveRejectDate', cellFilter: dateFilter },
                // xStatusApprovalId
            ],
            onRegisterApi: function (gridApi) {
                $scope.formGridApi = gridApi;
                gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    console.log("kacauuuuu");
                    $timeout(function () {
                        $scope.getTableHeight();
                    }, 100);
                });
            }
        };
        $scope.grid.data = [];
        $timeout(function () {
            $scope.formGridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                console.log("kacauuuuu");
                $timeout(function () {
                    $scope.getTableHeight();
                }, 100);
            });
        }, 5000);
    });