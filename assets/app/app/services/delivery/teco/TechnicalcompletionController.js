angular.module('app')
    .controller('TechnicalcompletionController', function($scope, $http, CurrentUser, Role, Technicalcompletion, $timeout, ngDialog, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mData = null; //Model
        $scope.filter = {};
        $scope.JobRequest = [];
        $scope.JobComplaint = [];
        $scope.lmComplaint = {};
        $scope.lmRequest = {};
        $scope.lmModel = {};
        $scope.ldModel ={}
        $scope.listApi = {};
        $scope.gridWork = [];
        $scope.listButtonSettings={new:{enable:false,icon:"fa fa-fw fa-car"}};
        $scope.listCustomButtonSettings={enable:false,
                                      icon:"fa fa-fw fa-car",
                                      func:function(row){
                                            console.log("customButton",row);
                                      }
                                    };
        $scope.ComplaintCategory=[
            {ComplaintCatg:'Body'},
            {ComplaintCatg:'Body Electrical'},
            {ComplaintCatg:'Brake'},
            {ComplaintCatg:'Drive Train'},
            {ComplaintCatg:'Engine'},
            {ComplaintCatg:'Heater System & AC'},
            {ComplaintCatg:'Restraint'},
            {ComplaintCatg:'Steering'},
            {ComplaintCatg:'Suspension and Axle'},
            {ComplaintCatg:'Transmission'},
        ]
        $scope.listSelectedRows=[];
        $scope.gridPartsDetail = {

          columnDefs: [
            { name: "NoMaterial",field: "Parts.PartsCode"},
            {name: "NamaMaterial",field: "Parts.PartsName"},
            {name: "Qty",field: "Qty"},
            {name: "Satuan"},
            {name: "Ketersedian"},
            {name: "Tipe"},
            {name: "ETA"},
            {name: "Harga",field: "Parts.RetailPrice"},
            {name: "Disc"},
            {name: "SubTotal"},
            {name: "NilaiDp"},
            {name: "StatusDp"},
            {name: "Pembayaran"},
            {name: "OPB"}
          ]
        }
    });