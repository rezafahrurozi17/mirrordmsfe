angular.module('app')
    .factory('Technicalcompletion', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res = $http.get('/api/as/Washings/Available/');
                //console.log('res=>',res);
                return res;
            },
            update: function(data) {
                    return $http.put('/api/as/Washings/', [{
                        Id: data.Id,
                        JobId: data.JobId,
                        TimeStart: data.TimeStart,
                        TimeFinish: data.TimeFinish,
                        TimeThru: data.TimeThru,
                        StatusWashing: data.StatusWashing
                    }])
                }
                // create: function(role) {
                //   return $http.post('/api/fw/Role', [{
                //                                       AppId: 1,
                //                                       ParentId: 0,
                //                                       Name: role.Name,
                //                                       Description: role.Description}]);
                // },
                // update: function(role){
                //   return $http.put('/api/fw/Role', [{
                //                                       Id: role.Id,
                //                                       //pid: role.pid,
                //                                       Name: role.Name,
                //                                       Description: role.Description}]);
                // },
                // delete: function(id) {
                //   return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
                // },
        }
    });