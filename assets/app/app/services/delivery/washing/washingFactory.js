angular.module('app')
    .factory('Washing', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function() {
                var res = $http.get('/api/as/Washings/Available/');
                //console.log('res=>',res);
                return res;
            },
            update: function(data) {
                return $http.put('/api/as/Washings/', [{
                    Id: data.Id,
                    JobId: data.JobId,
                    TimeStart: data.TimeStart,
                    TimeFinish: data.TimeFinish,
                    TimeThru: data.TimeThru,
                    StatusWashing: data.StatusWashing,
                    EstimatedTimeFinish: data.EstimatedTimeFinish,
                    StallId: parseInt(data.StallId),
                    WasherEmployeeId: parseInt(data.WasherEmployeeId)
                }])
            },
            petugasWashing: function() {
                return $http.get('/api/as/Mprofile_Employee/PW');
            },
            getStall: function(typeId) {
                return $http.get('/api/as/Washings/Stall/' + typeId);
            },
            getCar: function(PoliceNumber) {
                console.log("PoliceNumber", PoliceNumber);
                return $http.get('/api/as/WashingFilter/' + PoliceNumber);
            },
            SimpanWashing: function(dataJob, dataWashing, param) {
                console.log("SimpanWashing ", dataJob);
                console.log("SimpanWashing ", dataWashing);
                console.log("SimpanWashing ", param);
                var item = {};
                if(dataJob.length > 0){
                    item = dataJob[0];
                }else{
                    item.JobId = null;
                    item.UserIdSa = null;
                    item.PoliceNumber = null
                }
                if (param == 1) {
                    return $http.post('/api/as/PostWashing/', [{
                        "DataWashing": [{
                            "JobId": item.JobId,
                            "SAEmployeeId":item.UserIdSa,
                            "TimeInQueue": null,
                            "TimeFinish": null,
                            "TimeStart": null,
                            "TimeThru": null,
                            "PoliceNumber": item.PoliceNumber,
                            "StallId": null,
                            "WasherEmployeeId": null,
                            "StatusWashing": 0,
                            "Reason": dataWashing.Reason
                        }],
                        "DataJob": dataJob
                    }])
                } else {
                    return $http.post('/api/as/PostWashing/', [{
                        "DataWashing": [{
                            "JobId": null,
                            "SAEmployeeId": item.UserIdSa,
                            "TimeInQueue": null,
                            "TimeFinish": null,
                            "TimeStart": null,
                            "TimeThru": null,
                            "PoliceNumber": dataWashing.NoPolisi,
                            "StallId": null,
                            "WasherEmployeeId": null,
                            "StatusWashing": 0,
                            "Reason": dataWashing.Reason
                        }],
                        "DataJob": []
                    }])
                }
            },
            getCountWashing: function() {
                var res = $http.get('/api/as/WashingCount/');
                //console.log('res=>',res);
                return res;
            },

            // create: function(role) {
            //   return $http.post('/api/fw/Role', [{
            //                                       AppId: 1,
            //                                       ParentId: 0,
            //                                       Name: role.Name,
            //                                       Description: role.Description}]);
            // },
            // update: function(role){
            //   return $http.put('/api/fw/Role', [{
            //                                       Id: role.Id,
            //                                       //pid: role.pid,
            //                                       Name: role.Name,
            //                                       Description: role.Description}]);
            // },
            // delete: function(id) {
            //   return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
            // },
        }
    });