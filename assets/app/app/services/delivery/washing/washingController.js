angular.module('app')
    .controller('WashingController', function($scope, $http, $filter, CurrentUser, Role, Washing, $timeout, ngDialog, bsNotify, bsAlert, $q, $interval) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.Role = $scope.user.RoleId;
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            console.log("Tampil");
            $scope.getData();
            // $scope.startTime();    
            console.log("Role =>", $scope.Role)
        });
        $scope.adaData = true;
        //----------------------------------
        // Initialization
        //----------------------------------

        $scope.mData = null; //Model
        $scope.filter = {};
        $scope.xRole = { selected: [] };
        $scope.hideNewButton = false;
        $scope.today = new Date;
        $scope.clock = "loading clock..."; // initialise the time variable
        $scope.tickInterval = 1000 //ms
        $scope.hourminute = '';
        var timeFilter = 'date:"HH:mm:ss"';

        // $scope.mode = false;

        var d = new Date();
        var weekday = new Array(7);
        weekday[0] = "Minggu";
        weekday[1] = "Senin";
        weekday[2] = "Selasa";
        weekday[3] = "Rabu";
        weekday[4] = "Kamis";
        weekday[5] = "Jumat";
        weekday[6] = "Sabtu";

        var n = weekday[d.getDay()];
        console.log("hari", n);
        $scope.day = n;

        var onTimer = function() {
            var vhmin = ($scope.clock, 'hh:mm');
            $scope.hourminute = vhmin;
        }

        var tick = function() {
            $scope.clock = Date.now() // get the current time
            onTimer();
            $timeout(tick, $scope.tickInterval); // reset the timer
        }

        // Start the timer
        $timeout(tick, $scope.tickInterval);

        //----------------------------------
        // Get Data
        //----------------------------------
        var res = [{
            NoPolisi: 'F1421GH',
            WaktuPenyerahan: '12:00',
            StatusCustomer: 'EM',
            SA: 'Andi',
            // PetugasWashing : 'Petugas 1',
            // Stall : '3',
            mode: false
        }];
        var gridData = [];
        $scope.tmpCountWashing = 0;
        $scope.tmpCountWashingToday = 0;
        $scope.getData = function() {
            return $q.resolve(
                Washing.getData().then(function(res) {
                    console.log("Get Data", res.data);
                    gridData = [];
                    $scope.loading = false;

                    var tmpCount = 0;
                    _.forEach(res.data, function(e) {
                        var obj = {};
                        if (e.StatusWashing == 0) {
                            tmpCount++
                        }
                        if (e.WasherEmployeeId != null) {
                            e.PetugasWashing = e.WasherEmployeeId;
                            e.Stall = e.StallId;
                        };

                        if ($scope.Role == 1044) {
                            e.StatusWashing = 1;
                            e.WasherEmployeeId = null;
                            e.StallId == null;
                        }
                        // console.log('e >', e.Job);
                        // console.log('e.StatusWashing',tmpCount);
                        $scope.tmpCountWashing = tmpCount;

                        if (e.Status == 0){
                            if (e.EstimatedTimeFinish == null && e.SaInitial == null){
                                e.Statusx = ''
                            } else {
                                e.Statusx = 'Appointment Open'
                            }
                        } else if (e.Status == 1) {
                            e.Statusx = 'Appointment Cancel'
                        } else if (e.Status == 2) {
                            e.Statusx = 'Appointment No Show'
                        } else if (e.Status == 3) {
                            e.Statusx = 'Gate In'
                        } else if (e.Status == 4) {
                            e.Statusx = 'WO Created'
                        } else if (e.Status == 5) {
                            e.Statusx = 'Repair Process (Clock On)'

                        } else if (e.Status == 6) {
                            e.Statusx = 'Repair Process (Pause)'

                        } else if (e.Status == 7) {
                            e.Statusx = 'Repair Process (Clock On)'

                        } else if (e.Status == 8) {
                            e.Statusx = 'Repair Process (Clock Off)'

                        } else if (e.Status == 9) {
                            e.Statusx = 'Repair Process (QC Start)'

                        } else if (e.Status == 10) {
                            e.Statusx = 'Repair Process (QC Finish)'

                        } else if (e.Status == 11) {
                            e.Statusx = 'Final Inspection (Start)'

                        } else if (e.Status == 12) {
                            e.Statusx = 'Final Inspection (Finish)'

                        } else if (e.Status == 13) {
                            e.Statusx = 'Car Wash'

                        } else if (e.Status == 14) {
                            e.Statusx = 'Teco'

                        } else if (e.Status == 15) {
                            e.Statusx = 'Billing'

                        } else if (e.Status == 16) {
                            e.Statusx = 'Customer Call'

                        } else if (e.Status == 17) {
                            e.Statusx = 'Service Explanation'

                        } else if (e.Status == 18) {
                            e.Statusx = 'Close WO'

                        } else if (e.Status == 20) {
                            e.Statusx = 'Cancel WO'

                        } else if (e.Status == 21) {
                            e.Statusx = 'BSTK'

                        } else if (e.Status == 22) {
                            e.Statusx = 'Job Dispatch'

                        } else if (e.Status == 25) {
                            e.Statusx = 'Rawat Jalan'

                        } else {
                            e.Statusx = ''
                        }

                    });
                    res.data.sort(function(a, b) {
                        // Turn your strings into dates, and then subtract them
                        // to get a value that is either negative, positive, or zero.
                        return new Date(b.TimeStart) - new Date(a.TimeStart);
                    });
                    // var FixDelivery=null;
                    // _.map(res.data, function(val) {
                    //         if(val.Job.FixedDeliveryTime1 === val.Job.FixedDeliveryTime2)
                    //         {
                    //             FixDelivery = val.Job.FixedDeliveryTime1;
                    //         }
                    //         else
                    //         {
                    //             FixDelivery = val.Job.FixedDeliveryTime2                            
                    //         }
                        
                        
                    // })

                    // res.data.Job.FixedDeliveryTime1 = FixDelivery;
                    $scope.grid.data = res.data;
                    console.log('$scope.grid.data >', $scope.grid.data);
                    console.log('$scope.grid.data2 >', $scope.grid);

                }),
                Washing.getStall(100).then(function(ress) {
                    console.log('stall washing', ress.data.Result);
                    var dataStall = ress.data.Result;
                    // _.map(dataStall, function(val) {

                    // });
                    _.map($scope.grid.columnDefs, function(x) {
                        if (x.name === "Stall") {
                            x.editDropdownOptionsArray = dataStall;
                        };
                    });
                    $scope.arrStallWashing = dataStall;
                }),

                Washing.petugasWashing().then(function(resu) {
                    console.log('petugas washing', resu.data.Result);
                    console.log('colomn def', $scope.grid.columnDefs);
                    var dataPetugas = resu.data.Result;
                    // _.map(dataPetugas, function(val) {

                    // });
                    _.map($scope.grid.columnDefs, function(x) {
                        if (x.name === "Petugas Washing") {
                            x.editDropdownOptionsArray = dataPetugas;
                        };
                    });
                    $scope.arrPetugasWashing = dataPetugas;
                }),

                Washing.getCountWashing().then(function(resa) {
                    $scope.tmpCountWashingToday = resa.data;
                    console.log('Jumlah cuci ', resa.data);
                })
            );
        };
        // ========== Append By Faisal =====
        $scope.TimeForClock = "";
        $scope.checkTime = function(i) {
            if (i < 10) { i = "0" + i }; // add zero in front of numbers < 10
            return i;
        }
        $interval(function() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            h = $scope.checkTime(h);
            m = $scope.checkTime(m);
            s = $scope.checkTime(s);
            // $scope.TimeForClock = h + ":" + m + ":" + s;
            $scope.TimeForClock = h + ":" + m;
            $scope.checkTimeFromGrid();
        }, 1000);

        $scope.checkTimeFromGrid = function() {
                var tmpGridData = angular.copy($scope.grid.data);
                var tmpDateforGridNow = new Date();


                for (var i in tmpGridData) {
                    // console.log("true",tmpGridData[i].EstimatedTimeFinish);
                    if (tmpGridData[i].TimeStart !== null) {
                        var tmp1 = new Date(tmpGridData[i].TimeStart);
                        var tmp2 = new Date();
                        var h1 = tmp1.getHours();
                        var m1 = tmp1.getMinutes();
                        var s1 = tmp1.getSeconds();
                        var h2 = tmp2.getHours();
                        var m2 = tmp2.getMinutes();
                        var s2 = tmp2.getSeconds();
                        h1 = $scope.checkTime(h1);
                        m1 = $scope.checkTime(m1);
                        s1 = $scope.checkTime(s1);
                        h2 = $scope.checkTime(h2);
                        m2 = $scope.checkTime(m2);
                        s2 = $scope.checkTime(s2);
                        var jadi1 = h1 + ":" + m1 + ":" + s1;
                        var a1 = jadi1.split(':');
                        var seconds1 = (+a1[0]) * 60 * 60 + (+a1[1]) * 60 + (+a1[2]);
                        var jadi2 = h2 + ":" + m2 + ":" + s2;
                        var a2 = jadi2.split(':');
                        var seconds2 = (+a2[0]) * 60 * 60 + (+a2[1]) * 60 + (+a2[2]);
                        var totalSeconds = seconds2 - seconds1;
                        // console.log("totalSeconds",totalSeconds,"(",seconds2,seconds1,")");
                        if (totalSeconds == 601) {
                            // if(new Date(tmpGridData[i].EstimatedTimeFinish).getTime() < tmpDateforGridNow.getTime()){
                            $scope.NeedTime(tmpGridData[i]);
                        }
                    }
                }
            }
            // ==================================

        $scope.getDataSearch = function() {
            if ($scope.filter.NoPolisiDepan == null || $scope.filter.NoPolisiDepan == '' || $scope.filter.NoPolisiDepan == undefined) {
                $scope.getData();
            } else {
                $scope.loading = false
                var gridData = angular.copy($scope.grid.data)
                var newGridData = _.remove(gridData, function(resu) {
                    var NoPol = $scope.filter.NoPolisiDepan.toUpperCase();
                    var hasil;
                    if (resu.PoliceNumber != null) {
                        hasil = resu.PoliceNumber.toUpperCase().includes(NoPol);
                    }
                    console.log('nopol', NoPol);
                    console.log('resu', resu.PoliceNumber);
                    // if (resu.PoliceNumber == $scope.filter.NoPolisiDepan) {
                    if (hasil) {
                        console.log("tesssss", resu);
                        return resu;
                    };
                });
                if (newGridData.length == 0) {
                    bsAlert.alert({
                        title: "Data Kendaraan Tidak Ditemukan",
                        text: "",
                        type: "warning",
                        showCancelButton: true
                    });
                    $scope.getData();
                };
                $scope.grid.data = newGridData;
                return $q.resolve(

                );
            }
        };

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                };
            };
            return gridData;
        };
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        };
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        };

        var test = {};
        // $scope.data = [];
        $scope.clockOn = function(index, data) {
            var newData = $scope.$new();
            newData = data;
            console.log("clockOn idx", index);
            console.log("clockOn data", data);
            test = angular.copy(data);
            var tmpClock = $scope.TimeForClock;
            var tmpClocknya = "";
            tmpClock = tmpClock.split(":");
            tmpClocknya = tmpClock[0] + ":" + (parseInt(tmpClock[1]) + 10) + ":" + tmpClock[2];
            console.log("Copy data >>>", tmpClocknya);
            // test.TimeStart = new Date();
            // $scope.data.start = test;
            // console.log('$scope.data.start', $scope.data.start);
            // ngDialog.openConfirm({
            //     template: '\
            //       <p>Anda yakin untuk memulai proses pencucian ?</p>\
            //       <div class="ngdialog-buttons">\
            //         <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
            //         <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm()">Yes</button>\
            //       </div>',
            //     plain: true,
            //     controller: 'WashingController',
            // }).then(function() {
            //     $scope.cleanAlertThenClockOn(1, test);
            // });
            // $scope.btnClockOnGreen = true;

            bsAlert.alert({
                title: "Anda yakin untuk memulai proses pencucian ?",
                text: "",
                type: "question",
                showCancelButton: true,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
            },
                function () {
                    $scope.cleanAlertThenClockOn(1, test);                    
                },
                function () { }
            )
        };
        $scope.cleanAlertThenClockOn = function(param, data) {
            console.log('$scope.data >>', $scope.data);
            console.log('data >>', data);
            var trigger = true;
            // var data = {};
            if (param == 1) {
                // data = $scope.data.start;
                var tmpClock = $scope.TimeForClock;
                var tmpClocknya = "";
                tmpClock = tmpClock.split(":");
                tmpClocknya = tmpClock[0] + ":" + $scope.checkTime((parseInt(tmpClock[1]) + 10)) + ":" + tmpClock[2];
                var finalDate
                var starDate
                var tmpDate = new Date();
                var yyyy = tmpDate.getFullYear().toString();
                var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                var dd = tmpDate.getDate().toString();
                finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + " " + tmpClocknya;
                starDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + " " + $scope.TimeForClock;
                // var d = new Date(finalDate);
                // var s = new Date(starDate);
                data.TimeStart = starDate;
                data.EstimatedTimeFinish = null;
                // data.TimeStart = new Date();
                data.StatusWashing = param;
                $scope.btnColor = 'Green';
            } else if (param == 2) {
                data.TimeFinish = new Date();
                trigger == false;
                data.StatusWashing = param;
                data.TimeStart = $filter('date')(data.TimeStart, 'yyyy-MM-dd HH:mm:ss')
                data.TimeFinish = $filter('date')(data.TimeFinish, 'yyyy-MM-dd HH:mm:ss')
            } else if (param == 3) {
                data.TimeThru = new Date();
                trigger == false;
                data.StatusWashing = param;
                data.TimeStart = $filter('date')(data.TimeStart, 'yyyy-MM-dd HH:mm:ss')
                data.TimeFinish = $filter('date')(data.TimeFinish, 'yyyy-MM-dd HH:mm:ss')
                data.TimeThru = $filter('date')(data.TimeThru, 'yyyy-MM-dd HH:mm:ss')
            };

            console.log("Copy data", data);

            // if ((data.PetugasWashing == null || data.PetugasWashing === undefined) && (data.Stall == null || data.Stall === undefined)) {
            // alert("Pilih Petugas Washing dan Stall Terlebih Dahulu");
            // bsAlert.alert({
            //         title: "Pilih Petugas Washing dan Stall Terlebih Dahulu",
            //         text: "",
            //         type: "warning"
            //             // showCancelButton:true
            //     },
            //     function() {

            //     },
            //     function() {

            //     }
            // )
            // } else {
            Washing.update(data).then(function(resu) {
                console.log("update data", data);
                $scope.getData();
            });

            ngDialog.closeAll();
            // if (trigger == true) {
            //     $timeout(function() {
            //         ngDialog.openConfirm({
            //             template: '\
            //       <p>Batas Waktu Penyelesaian Proses Cuci</p>\
            //        <p>Anda sudah habis (10 Menit)!</p>\
            //         <p>Apakah anda Ingin menambah Waktu </p>\
            //         <p>Untuk Proses Cuci?</p>\
            //       <div class="ngdialog-buttons">\
            //         <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
            //         <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="NeedTime()">Yes</button>\
            //       </div>',
            //             plain: true,
            //             controller: 'WashingController',
            //         });
            //     }, 6000);
            // } else {
            //     $timeout(tick, $scope.tickInterval);
            // };
            // };
            // 600000
        };

        // $scope.NeedTime = function() {
        //     ngDialog.closeAll();
        //     $timeout(function() {
        //         ngDialog.openConfirm({
        //             template: '\
        //               <p>Batas Waktu Penyelesaian Proses Cuci</p>\
        //                <p>Anda sudah habis (10 Menit)!</p>\
        //                 <p>Apakah anda Ingin menambah Waktu </p>\
        //                 <p>Untuk Proses Cuci?</p>\
        //               <div class="ngdialog-buttons">\
        //                 <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
        //                 <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="NeedTime()">Yes</button>\
        //               </div>',
        //             plain: true,
        //             controller: 'WashingController',
        //         });
        //     }, 6000);
        //     // 600000
        // };
        $scope.NeedTime = function(data) {
            var newData = $scope.$new();
            newData = data;
            // console.log("clockOn idx", index);
            console.log("clockOn data", data);
            $scope.test = angular.copy(data);
            ngDialog.closeAll();
            ngDialog.openConfirm({
                template: '\
                      <p>Nomor Polisi : {{test.PoliceNumber}}</p>\
                      <p>Batas Waktu Penyelesaian Proses Cuci</p>\
                       <p>Anda sudah habis (10 Menit)!</p>\
                        <p>Apakah anda Ingin menambah Waktu </p>\
                        <p>Untuk Proses Cuci?</p>\
                      <div class="ngdialog-buttons">\
                        <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="clockOff(1,test)">No</button>\
                        <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="cleanAlertThenClockOn(1,test)">Yes</button>\
                      </div>',
                plain: true,
                scope: $scope,
                controller: 'WashingController',
            });
            // 600000
        };

        $scope.clockOff = function(index, data) {
            test = angular.copy(data);
            console.log("Clock off idx", index);
            console.log("Clock off data", data);
            // $scope.data.stop = test;
            // ngDialog.openConfirm({
            //     template: '\
            //       <p>Anda yakin untuk menyudahi proses pencucian ?</p>\
            //       <div class="ngdialog-buttons">\
            //         <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
            //         <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm()">Yes</button>\
            //       </div>',
            //     plain: true,
            //     controller: 'WashingController',
            // }).then(function() {
            //     $scope.cleanAlertThenClockOn(2, test);
            // });
            // $scope.btnClockOnGreen = true; 

            bsAlert.alert({
                title: "Apakah proses cuci telah selesai ?",
                text: "",
                type: "question",
                showCancelButton: true,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
            },
                function () {
                    $scope.cleanAlertThenClockOn(2, test);
                },
                function () { }
            )
        };
        $scope.skip = function(index, data) {
            test = angular.copy(data);
            // $scope.data.skip = test;
            console.log("skip idx", index);
            console.log("skip data", data);
            // ngDialog.openConfirm({
            //     template: '\
            //       <p>Anda yakin untuk membatalkan proses pencucian ?</p>\
            //       <div class="ngdialog-buttons">\
            //         <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
            //         <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm()">Yes</button>\
            //       </div>',
            //     plain: true,
            //     controller: 'WashingController',
            // }).then(function() {
            //     $scope.cleanAlertThenClockOn(3, test);
            // });

            bsAlert.alert({
                title: "Anda yakin untuk membatalkan proses pencucian ?",
                text: "",
                type: "question",
                showCancelButton: true,
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
            },
                function () {
                    $scope.cleanAlertThenClockOn(3, test);
                },
                function () { }
            )
        };
        //----------------------------------
        // Grid Setup
        //----------------------------------
        // ng-click ="grid.appScope.$parent.clockOn(grid.renderContainers.body.visibleRowCache.indexOf(row),row.entity)"
        // ng-hide = "row.entity.mode"
        //ng-click = 'grid.appScope.$paren.NumaFuntion'
        // $scope.gridActionButtonTemplate = '<a class="glyphicon glyphicon-play" title="Clock On"  style="font-size: 15px;" ng-click ="grid.appScope.$parent.clockOn(grid.renderContainers.body.visibleRowCache.indexOf(row),row.entity)" >' +
        //     // '<a class="glyphicon glyphicon-play" title="Clock On"  ng-show = "row.entity.mode" style="font-size: 15px;color:green;">'+

        //     '<a class="glyphicon glyphicon-stop" title="Clock Off" style="font-size: 15px;" ng-click ="grid.appScope.$parent.clockOff(grid.renderContainers.body.visibleRowCache.indexOf(row),row.entity)"></a>' +
        //     // '<a class="glyphicon glyphicon-stop" title="Clock Off" ng-hide = "row.entity.mode" style="font-size: 15px;color:grey;"></a>'+

        //     '<a class="glyphicon glyphicon-fast-forward" title="Skip" style="font-size: 15px;margin-left:2px;" ng-click ="grid.appScope.$parent.skip(grid.renderContainers.body.visibleRowCache.indexOf(row),row.entity)"></a>'
        // '<a class="glyphicon glyphicon-fast-forward" title="Skip" ng-show="row.entity.mode" style="font-size: 15px;margin-left:2px;color:grey"></a>'+

        // var actDetail = '<div class="ui-grid-cell-contents">'+
        //                     '<a class="glyphicon glyphicon-play" title="Clock On" ng-hide = "row.entity.mode" style="font-size: 15px;" ng-click ="grid.appScope.clockOn(grid.renderContainers.body.visibleRowCache.indexOf(row),row.entity)">'+
        //                     '<a class="glyphicon glyphicon-play" title="Clock On"  ng-show = "row.entity.mode" style="font-size: 15px;color:green;">'+

        //                     '<a class="glyphicon glyphicon-stop" title="Clock Off" ng-show = "row.entity.mode" style="font-size: 15px;" ng-click ="grid.appScope.clockOff(grid.renderContainers.body.visibleRowCache.indexOf(row),row.entity)"></a>'+
        //                     '<a class="glyphicon glyphicon-stop" title="Clock Off" ng-hide = "row.entity.mode" style="font-size: 15px;color:grey;"></a>'+

        //                     '<a class="glyphicon glyphicon-fast-forward" title="Skip" ng-hide="row.entity.mode" style="font-size: 15px;margin-left:2px;" ng-click ="grid.appScope.skip(grid.renderContainers.body.visibleRowCache.indexOf(row),row.entity)"></a>'+
        //                     '<a class="glyphicon glyphicon-fast-forward" title="Skip" ng-show="row.entity.mode" style="font-size: 15px;margin-left:2px;color:grey"></a>'+
        //                 '</div>'


        $scope.findCar = function() {
            if ($scope.filter.NoPolisi == "" || $scope.filter.NoPolisi == null || $scope.filter.NoPolisi == undefined) {

                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "No. Polisi Tidak Boleh Kosong",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            } else {

                Washing.getCar($scope.filter.NoPolisi).then(function(res) {
                    console.log('get Car', res.data.Result);
                    $scope.DataWashing = res.data.Result;
                    if ($scope.DataWashing.length == 0) {
                        // bsAlert.alert({
                        //     title: "Data Kendaraan Tidak Ditemukan",
                        //     text: "",
                        //     type: "warning",
                        //     showCancelButton: true
                        // });
                        $scope.adaData = false;
                    };
                });
            };
        };

        $scope.gridWashing = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: false,
            enableRowSelection: true,
            enableSelectAll: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            data: 'DataWashing',
            showGridFooter: true,
            columnDefs: [
                { name: 'No. Polisi', field: 'PoliceNumber' },
                { name: 'No. WO', field: 'WoNo' },
                { name: 'Model Kendaraan', field: 'ModelType' }
            ],
            onRegisterApi: function(gridApi) {
                // set gridApi on $scope
                $scope.gridApi = gridApi;
                $scope.gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                    // console.log("selected=>",$scope.selectedRows);
                    if ($scope.onSelectRows) {
                        $scope.onSelectRows($scope.selectedRows);
                    };
                });
            }
        };

        $scope.formApi = {};
        // $scope.actionButtonSettings = [{
        //     actionType: 'new',
        //     title: 'Tambah',
        //     func: function(row, formScope) {
        //         console.log('row', row);
        //         console.log('formScope', formScope);
        //         $scope.formApi.setMode('detail');
        //     }
        // }];


        // $scope.filter = [];

        // $scope.onValidateSave = function() {
        //     console.log("onValidateSave", DataSaveWash);

        //     Washing.SimpanWashing(DataSaveWash).then(
        //         function(res) {
        //             // $scope.choseData(JobId);
        //             $scope.findCar(DataSaveWash[0].PoliceNumber);
        //             console.log("simpan suksess");
        //         },
        //         function(err) {
        //             console.log("err=>", err);
        //         }
        //     );
        // };
        // var DataSaveWash = {};
        // $scope.onSelectRows = function(rows) {
        //     console.log("form onSelectRows=>", rows);
        //     DataSaveWash = rows;
        // };



        $scope.onValidateSave = function(row) {
            console.log('rowwwww', row);
        }
        $scope.actionButtonSettingsDetail = [{
            actionType: 'back',
            title: 'Kembali'
        }, {
            title: "Simpan",
            func: function(row, formScope) {
                // $scope.formApi.setMode('grid');
                console.log("onValidateSave", row, row);

                Washing.getCar(row.NoPolisi).then(function(res) {
                    console.log("res", res.data);
                    if (res.data.Result.length == 0) {
                        bsAlert.alert({
                                title: "No. Polisi " + row.NoPolisi + " Tidak Ditemukan",
                                text: "Apakah tetap akan dicuci?",
                                type: "question",
                                showCancelButton: true
                            },
                            function() {
                                Washing.SimpanWashing(res.data.Result, row, 0).then(
                                    function(res) {
                                        // $scope.choseData(JobId);
                                        // $scope.findCar(DataSaveWash[0].PoliceNumber);
                                        console.log("simpan suksess");
                                        $scope.getData();
                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                );
                                $scope.formApi.setMode('grid');
                            },
                            function() {

                            }
                        )

                    } else {
                        Washing.SimpanWashing(res.data.Result, row, 1).then(
                            function(res) {
                                // $scope.choseData(JobId);
                                // $scope.findCar(DataSaveWash[0].PoliceNumber);
                                $scope.formApi.setMode('grid');
                                console.log("simpan suksess");
                                $scope.getData();
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    };
                });
            }
        }];
        $scope.disableButton = {
            stop: true,
            skip: false
        };
        $scope.actionButtonSettings = [{
            title: 'Tambah Washing',
            icon: 'fa fa-fw fa-car',
            func: function(row, formScope) {
                console.log("func view=>", $scope, row);
                console.log("func formScope=>", formScope);
                console.log("api=>", $scope.formApi);
                // if (formScope) {
                $scope.filter.NoPolisi = "";
                $scope.gridWashing.data = [];
                $scope.mData = {};
                // $scope.mDataDetail = {};
                // };
                $scope.adaData = true;
                $scope.formApi.setMode('detail');
            },
            type: 'custom' //for link
        }];

        $scope.gridActionTemplate = '<div class="ui-grid-cell-contents" style="margin-left:3px;">\
                                    <button class="ui icon inverted button" style="box-shadow:none!important;color:#3276b1;" data-toggle="tooltip" data-placement="bottom" title="Clock On" ng-disabled="row.entity.StatusWashing == 1 || (row.entity.WasherEmployeeId == null || row.entity.StallId == null)" ng-click="grid.appScope.clockOn(grid.renderContainers.body.visibleRowCache.indexOf(row),row.entity)"><i class="fa fa-fw fa-lg fa-play" ng-if="(row.entity.StatusWashing == 0)" style="font-size: 25px;" ></i> <i class="fa fa-fw fa-lg fa-play" ng-if="(row.entity.StatusWashing == 1)" style="font-size: 25px; color : Green;" ></i></button>&nbsp;&nbsp;\
                                    <button class="ui icon inverted button" style="box-shadow:none!important;color:#3276b1;" data-toggle="tooltip" data-placement="bottom" title="Clock Off" ng-click="grid.appScope.clockOff(grid.renderContainers.body.visibleRowCache.indexOf(row),row.entity)" ng-disabled="row.entity.StatusWashing == 0  || (row.entity.WasherEmployeeId == null || row.entity.StallId == null)"><i class="fa fa-fw fa-lg fa-stop"  ng-if="row.entity.StatusWashing == 0" style="font-size: 25px;" ></i><i class="fa fa-fw fa-lg fa-stop" style="font-size: 25px; color : red;" ng-if="row.entity.StatusWashing == 1" ></i></button>&nbsp;&nbsp;\
                                    <button class="ui icon inverted button" style="box-shadow:none!important;color:#3276b1;" data-toggle="tooltip" data-placement="bottom" title="Cancel Washing" ng-click="grid.appScope.skip(grid.renderContainers.body.visibleRowCache.indexOf(row),row.entity)" ng-disabled="row.entity.StatusWashing == 1"><i class="fa fa-fw fa-lg fa-forward" style="font-size: 25px;"  ></i></button></div>';


        var petugasWashing = $scope.arrPetugasWashing;
        var stallWashing = $scope.arrStallWashing;
        var gridActionTemplate = $scope.gridActionTemplate;
        $scope.gridTableHeight = function() {
            var rowHeight = 40; // your row height
            var headerHeight = 50; // your header height
            var filterHeight = 40; // your filter height
            var pageSize = $scope.grid.paginationPageSize;

            if ($scope.grid.paginationPageSize > $scope.grid.data.length) {
                pageSize = $scope.grid.data.length;
            };
            if (pageSize < 4) {
                pageSize = 3;
            };

            return {
                height: (pageSize * rowHeight + headerHeight) + 50 + "px"
            };

        };


        var customCellTemplate = '<div><select ui-grid-edit-dropdown style="height:40px;" ng-model=\"MODEL_COL_FIELD\" ng-change="grid.appScope.onDropdownChange(row.entity)" ng-options=\"field[editDropdownIdLabel] as field[editDropdownValueLabel] CUSTOM_FILTERS for field in editDropdownOptionsArray\"></select></div>';


        $scope.filter
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            enableCellEditOnFocus: true,
            rowHeight: 40,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100, 200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'No. Polisi', field: 'PoliceNumber', enableCellEdit: false, },
                { name: 'Waktu Penyerahan', field: 'EstimatedTimeFinish', cellFilter: timeFilter, enableCellEdit: false, },
                // { name: 'Status Customer', field: 'StatusCustomer' },
                { name: 'SA', displayName: 'SA', field: 'SaInitial', enableCellEdit: false, },
                { name: 'Status WO', displayName: 'Status WO', field: 'Statusx', enableCellEdit: false, },
                // { name: 'Stall' }
                {
                    name: 'Petugas Washing',
                    field: 'WasherEmployeeId',
                    enableCellEdit: true,
                    // editableCellTemplate: 'ui-grid/dropdownEditor',
                    editableCellTemplate: customCellTemplate,
                    // width: '20%',
                    editDropdownValueLabel: 'EmployeeName',
                    editDropdownIdLabel: 'EmployeeId',
                    // editDropdownOptionsArray: [
                    //     { PetugasWashing: 'Petugas 1' },
                    //     { PetugasWashing: 'Petugas 2' },
                    //     { PetugasWashing: 'Petugas 3' },
                    // ]
                    editDropdownOptionsArray: petugasWashing,
                    cellFilter: "gridPetugasWashing:this",
                    cellClass: 'yourClass'

                },
                {
                    name: 'Stall',
                    field: 'StallId',
                    enableCellEdit: true,
                    // editableCellTemplate: 'ui-grid/dropdownEditor',
                    editableCellTemplate: customCellTemplate,
                    // width: '10%',
                    editDropdownValueLabel: 'Name',
                    editDropdownIdLabel: 'StallId',
                    editDropdownOptionsArray: stallWashing,
                    cellFilter: "gridStall:this",
                    cellClass: 'yourClass'

                },
                {
                    name: 'Action',
                    field: 'Action',
                    cellTemplate: gridActionTemplate,
                    width: 250,
                    pinnedRight: true,
                    enableCellEdit: false
                }
                // { name:'Action', field:'Action',cellEditableCondition:false,
                //                                    enableEdit:false,
                //                                    enableColumnMenu:false,
                //                                    enableSorting: false,
                //                                    cellTemplate:actDetail,
                //                                    enableColumnResizing: false, },
            ]
        };
    }).filter('gridPetugasWashing', function() {
        return function(input, xthis) {
            console.log("filter..Petugas");
            console.log('input', input);
            console.log('xthis', xthis);
            if (xthis !== undefined) {
                if (xthis.col !== undefined) {
                    var map = xthis.col.colDef.editDropdownOptionsArray;
                    var idField = xthis.col.colDef.editDropdownIdLabel;
                    var valueField = xthis.col.colDef.editDropdownValueLabel;
                    for (var i = 0; i < map.length; i++) {
                        if (map[i][idField] == input) {
                            return map[i][valueField];
                        };
                    };
                };
            };
            return '';
        };
    }).filter('gridStall', function() {
        return function(input, xthis) {
            console.log("filter..Stall");
            console.log(input);
            console.log(xthis);
            if (xthis !== undefined) {
                if (xthis.col !== undefined) {
                    var map = xthis.col.colDef.editDropdownOptionsArray;
                    var idField = xthis.col.colDef.editDropdownIdLabel;
                    var valueField = xthis.col.colDef.editDropdownValueLabel;
                    for (var i = 0; i < map.length; i++) {
                        if (map[i][idField] == input) {
                            return map[i][valueField];
                        };
                    };
                };
            };
            return '';
        };


    });