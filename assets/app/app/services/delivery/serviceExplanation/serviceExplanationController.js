angular.module('app')
    .controller('serviceExplanationController', function($scope,AppointmentGrService, $http, CurrentUser, Role, ServiceExplanation,$q, $timeout, ngDialog, bsNotify,RepairProcessGR) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mData = null; //Model
        $scope.filter = {};
        $scope.JobRequest = [];
        $scope.JobComplaint = [];
        $scope.lmComplaint = {};
        $scope.lmRequest = {};
        $scope.lmModel = {};
        $scope.ldModel ={}
        $scope.listApi = {};
        $scope.gridWork = [];
        $scope.formApi = {};
        $scope.hideNewButton = true;
        $scope.listButtonSettings={new:{enable:false,icon:"fa fa-fw fa-car"}};
        $scope.listCustomButtonSettings={enable:false,
                                      icon:"fa fa-fw fa-car",
                                      func:function(row){
                                            console.log("customButton",row);
                                      }
                                    };
        $scope.ComplaintCategory=[
            {ComplaintCatg:'Body'},
            {ComplaintCatg:'Body Electrical'},
            {ComplaintCatg:'Brake'},
            {ComplaintCatg:'Drive Train'},
            {ComplaintCatg:'Engine'},
            {ComplaintCatg:'Heater System & AC'},
            {ComplaintCatg:'Restraint'},
            {ComplaintCatg:'Steering'},
            {ComplaintCatg:'Suspension and Axle'},
            {ComplaintCatg:'Transmission'},
        ]
        $scope.listSelectedRows=[];
        $scope.optionInspection=[{Id:1,Name:"Ok"},{Id:2,Name:"Not Ok"}]
        $scope.gridPartsDetail = {

          columnDefs: [
            { name: "NoMaterial",field: "Parts.PartsCode"},
            {name: "NamaMaterial",field: "Parts.PartsName"},
            {name: "Qty",field: "Qty"},
            {name: "Satuan"},
            {name: "Ketersedian"},
            {name: "Tipe"},
            {name: "ETA"},
            {name: "Harga",field: "Parts.RetailPrice"},
            {name: "Disc"},
            {name: "SubTotal"},
            {name: "NilaiDp"},
            {name: "StatusDp"},
            {name: "Pembayaran"},
            {name: "OPB"}
          ]
        }
        $scope.isWashing = 1;
        $scope.isWaiting = 0;
        $scope.isParts = 0;
        $scope.listApi={};
        $scope.listSelectedRows = [];
        $scope.lmModel = {};
        $scope.ldModel = {};
        $scope.ComplaintCategory=[
                                    {ComplaintCatg:'Body'},
                                    {ComplaintCatg:'Body Electrical'},
                                    {ComplaintCatg:'Brake'},
                                    {ComplaintCatg:'Drive Train'},
                                    {ComplaintCatg:'Engine'},
                                    {ComplaintCatg:'Heater System & AC'},
                                    {ComplaintCatg:'Restraint'},
                                    {ComplaintCatg:'Steering'},
                                    {ComplaintCatg:'Suspension and Axle'},
                                    {ComplaintCatg:'Transmission'},
                                  ]
        $scope.typeSearch = [{
            Id: 1,
            Name: 'No. Polisi'
        },{
            Id: 2,
            Name: 'No. WO'
        }];
        $scope.listView = {
            new: { enable: false },
            view: { enable: false },
            delete: { enable: false },
            select: { enable: false },
            selectall: { enable: false },
            edit: { enable: false },
        }
        var Parts = [];
        var gridTemp = [];
        $scope.onSelectRows = function(rows) {
          console.log("onSelectRows=>", rows);
        }

        var getAllData = function(){
            
        }
        $scope.selectCategory = function(selected) {
            if (selected == 1) {
                $scope.filter.noWo = null;
            }else{
                $scope.filter.PoliceNumber = null;
            }
        };
        $scope.getData = function() {

            if($scope.filter.typeCategory == 1 && $scope.filter.PoliceNumber !== null && $scope.filter.PoliceNumber !== undefined){
                $scope.loading = true;
                ServiceExplanation.getData($scope.filter.typeCategory,$scope.filter.PoliceNumber).then(function(res){
                    $scope.loading = false;
                    // var tmpRes = res.data;
                    // var dataGrid = [];
                    // for(var i=0;i<tmpRes.length;i++){
                    //     if(tmpRes[i].Status == 16){
                    //         dataGrid.push(tmpRes[i]);
                    //     }
                    // }
                    $scope.grid.data = res.data;
                    console.log("grid", res.data);
                });
            }else if($scope.filter.typeCategory == 2 && $scope.filter.noWo !== null && $scope.filter.noWo !== undefined){
                $scope.loading = true;
                ServiceExplanation.getData($scope.filter.typeCategory,$scope.filter.noWo).then(function(res){
                    $scope.loading = false;
                    // var tmpRes = res.data;
                    // var dataGrid = [];
                    // for(var i=0;i<tmpRes.length;i++){
                    //     if(tmpRes[i].Status == 16){
                    //         dataGrid.push(tmpRes[i]);
                    //     }
                    // }
                    $scope.grid.data = res.data;
                    console.log("grid", res.data);
                });
            }else{
                bsNotify.show({
                  size: 'big',
                  type: 'danger',
                  title: "Mohon Input Filter",
                  // content: error.join('<br>'),
                  // number: error.length
                });
                $scope.grid.data = [];
                $scope.loading=false;
            }
        }
        var dataRow = {};
        $scope.onBeforeEdit = function(row, mode){
          console.log("row",row);
          dataRow = row;
          console.log("mode",mode);
          ServiceExplanation.getDataPreDiagnose(row.JobId).then(function(res){  
            $scope.dataPrediagnose=res.data.Result;
            $scope.mData.firstDate = $scope.dataPrediagnose[0].ScheduledTime;
            $scope.mData.lastDate = $scope.dataPrediagnose[0].LastModifiedDate;
            $scope.PlanFinish = row.PlanFinish;
            $scope.PlanStart = row.PlanStart;
            if(row.MProfileForeman !== null){
                $scope.mData.foreMan = row.MProfileForeman.EmployeeName;
            }
            $scope.dataForBslist(row);
            $scope.dataCrm(row);
            console.log("coooy", $scope.dataCrm(row));

            RepairProcessGR.getFinalInspectionById(row.JobId).then(
                function(res) {
                    $scope.dataSaveFi = res.data;
                    $scope.filter.Type = $scope.dataSaveFi.Status;
                    $scope.mData.waktuInspection = $scope.dataSaveFi.LastModifiedDate;
                    console.log("data FI",$scope.dataSaveFi);
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

            RepairProcessGR.getJobSuggest(row.JobId).then(function(res){

                $scope.GetDataJobSuggest = res.data.Result;
                console.log("Data JObSUggest", $scope.GetDataJobSuggest);
                $scope.mData.firstDate = row.SuggestionDate;
                $scope.mData.SuggestionCatg = $scope.GetDataJobSuggest[0].SuggestionCatg;
               
                var co = ["CategoryJobSuggestKeamanan","CategoryJobSuggestPeraturan","CategoryJobSuggestKenyamanan","CategoryJobSuggestEfisiensi"];

                var total = $scope.mData.SuggestionCatg;
                var arrBin = [1,2,4,8];
                for (var i in arrBin) {
                   if (total&arrBin[i]) {
                       var xc = co[i];
                        $scope.mData[xc] = arrBin[i];
                   }    
                }
                
                console.log("total", total);
                console.log("$scope.GetDataJobSuggest[0].Suggestion",$scope.GetDataJobSuggest[0].Suggestion);
                console.log("$scope.GetDataJobSuggest[0].SuggestionCatg",$scope.GetDataJobSuggest[0].SuggestionCatg);
                console.log("$scope.GetDataJobSuggest[0].SuggestionDate",$scope.GetDataJobSuggest[0].SuggestionDate);
            });
            
            console.log("$scope.dataPrediagnose",$scope.dataPrediagnose);
          });
        }
        $scope.tmptoyotaid = "";
        $scope.tmpCustomerName = "";
        $scope.dataCrm = function(row){
            console.log("rownyaboiii",row);
            AppointmentGrService.getDataVehicle(row.VehicleId).then(function(ress) {
                if( ress.data.Result.length >0){
                    $scope.tmptoyotaid = ress.data.Result[0].ToyotaId;
                    $scope.tmpCustomerName = ress.data.Result[0].CustomerName;
                }
            })
        }
        $scope.phoneType = [
            {
                Id : 1,
                Name : 'Rumah'
            },
            {
                Id : 2,
                Name : 'Kantor'
            },
            {
                Id : 3,
                Name : 'Handphone'
            }
        ];
        $scope.StatusFUTime = [
            {
                Id : 1,
                Name : 'Pagi'
            },
            {
                Id : 2,
                Name : 'Siang'
            },
            {
                Id : 3,
                Name : 'Sore'
            }
        ];
        $scope.formApi = {};
        $scope.onValidateSave = function(data){
            console.log("data",data);
            // console.log("test aja",dataRow);

            // ServiceExplanation.update(dataRow.JobId,$scope.mData).then(
            //     function(res) {
            //         console.log("Susess");
            //         $scope.formApi.setMode('grid');
            //         // return true
            //     },
            //     function(err) {
            //         console.log("err=>", err);
            //     }
            // );
            return data
        }
        
        $scope.CountShift = function(){;
            var FinalCount = 
               ($scope.mData.FinalCheck1==undefined?0:$scope.mData.FinalCheck1)
             + ($scope.mData.FinalCheck2==undefined?0:$scope.mData.FinalCheck2)
             + ($scope.mData.FinalCheck3==undefined?0:$scope.mData.FinalCheck3)
             + ($scope.mData.FinalCheck4==undefined?0:$scope.mData.FinalCheck4)
             + ($scope.mData.FinalCheck5==undefined?0:$scope.mData.FinalCheck5);
             console.log("Hasil Count",FinalCount); 
          // // $scope.mData.SuggestionCatg = $scope.mData.CategoryJobSuggestKeamanan + $scope.mData.CategoryJobSuggestPeraturan + $scope.mData.CategoryJobSuggestKenyamanan + $scope.mData.CategoryJobSuggestEfisiensi;
        }

        $scope.dataForBslist = function(data) {
            gridTemp = []
            Parts.splice();
            Parts = [];
            if (data.JobTask.length > 0) {
                var tmpJob = data.JobTask;
                for (var i = 0; i < tmpJob.length; i++) {
                    console.log("tmpJob", tmpJob);
                    // 
                    if (tmpJob[i].Fare !== undefined) {
                        var sum = tmpJob[i].Fare;
                        var summ = tmpJob[i].FlatRate;
                        var summaryy = sum * summ;
                        summaryy = parseInt(summaryy);
                    }
                    for (var j = 0; j < tmpJob[i].JobParts.length; j++) {
                        console.log("tmpJob[i].JobParts[j]",tmpJob[i].JobParts[j]);
                        if(tmpJob[i].JobParts[j].PaidBy !== null){
                            tmpJob[i].JobParts[j].paidName = tmpJob[i].JobParts[j].PaidBy.Name;
                            delete tmpJob[i].JobParts[j].PaidBy;
                        }
                        if(tmpJob[i].JobParts[j].Satuan !== null){
                            tmpJob[i].JobParts[j].satuanName = tmpJob[i].JobParts[j].Satuan.Name;
                            delete tmpJob[i].JobParts[j].Satuan;
                        }
                        tmpJob[i].subTotal = tmpJob[i].Qty * tmpJob[i].RetailPrice;
                        $scope.getAvailablePartsServiceManual(tmpJob[i].JobParts[j]);
                        
                        // Parts.push(tmpJob[i].JobParts[j]);
                    }
                    gridTemp.push({
                        ActualRate: tmpJob[i].ActualRate,
                        JobTaskId: tmpJob[i].JobTaskId,
                        JobTypeId: tmpJob[i].JobTypeId,
                        // catName: tmpJob[i].JobType.Name,
                        Fare: tmpJob[i].Fare,
                        Summary: summaryy,
                        PaidById: tmpJob[i].PaidById,
                        JobId: tmpJob[i].JobId,
                        TaskId: tmpJob[i].JobTaskId,
                        TaskName: tmpJob[i].TaskName,
                        FlatRate: tmpJob[i].FlatRate,
                        ProcessId: tmpJob[i].ProcessId,
                        TFirst1: tmpJob[i].TFirst1,
                        // PaidBy: tmpJob[i].PaidBy.Name,
                        child: Parts
                    });
                }
                $scope.gridWork = gridTemp;
                // $scope.onAfterSave();
                console.log("totalw", $scope.totalWork);
            }
            $scope.sumAllPrice();
            console.log("$scope.gridWork", $scope.gridWork);
            console.log("$scope.JobRequest", $scope.JobRequest);
            console.log("$scope.JobComplaint", $scope.JobComplaint);
        };
        $scope.getAvailablePartsServiceManual = function(item) {
            AppointmentGrService.getAvailableParts(item.PartsId).then(function(res) {
                var tmpRes = res.data.Result[0];
                tmpParts = tmpRes;
                if(tmpParts !== null){
                    console.log("have data RetailPrice");
                    item.RetailPrice = tmpParts.RetailPrice;
                    item.priceDp = tmpParts.PriceDP;
                    item.ETA = tmpParts.DefaultETA;
                    item.subTotal = item.Qty * item.RetailPrice;
                    item.Type = 3;
                    if (tmpParts.isAvailable == 0) {
                        item.Availbility = "Tidak Tersedia";
                    } else {
                        item.Availbility = "Tersedia"
                    }
                }else{
                    console.log("haven't data RetailPrice");
                }
                Parts.push(item);
                $scope.sumAllPrice();
                return item;
            });
        };
        $scope.CetakToyotaID = function(printSectionId) {
            var innerContents = document.getElementById(printSectionId).innerHTML;
            var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWinindow.document.open();
            popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
            popupWinindow.document.close();
          }

        // $scope.CetakToyotaID = function(item) {
        //     console.log("item", item);
        //     bsAlert.alert({
        //             title: "Apakah Anda yakin akan melakukan Cetak Toyota ID?",
        //             text: "",
        //             type: "question",
        //             showCancelButton: true
        //         },
        //         function() {
        //             // if (item.JobId > 0) {
        //             //     WO.updateStatusForWO(item.JobId, 20).then(function(res) {
        //             //         $scope.actionButtonSettingsDetail[0].visible = true;
        //             //         $scope.pageMode = 'view'
        //             //         $scope.formApi.setMode('grid');
        //             //         $scope.getData();
        //             //     });
        //             // } else {
        //             //     WO.CancelWOWalkin(item).then(function(res) {
        //             //         $scope.actionButtonSettingsDetail[0].visible = true;
        //             //         $scope.pageMode = 'view'
        //             //         $scope.formApi.setMode('grid');
        //             //         $scope.getData();
        //             //     });
        //             // }
        //         },
        //         function() {}
        //     )
        // };

        $scope.sumAllPrice = function() {
            var totalW = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].Summary !== undefined) {
                    if (gridTemp[i].Summary == "") {
                        gridTemp[i].Summary = gridTemp[i].Fare;
                    }
                    totalW += gridTemp[i].Summary;
                }
            }
            $scope.totalWork = totalW;
            var totalM = 0;
            for (var i = 0; i < Parts.length; i++) {
                if (Parts[i].subTotal !== undefined) {
                    totalM += Parts[i].subTotal;
                }
            }
            $scope.totalMaterial = totalM;
            var totalDp = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].DownPayment !== undefined) {
                            totalDp += gridTemp[i].child[j].DownPayment;
                        }
                    }
                }
            }
            $scope.totalDp = totalDp;
        };
        $scope.grid = {
          enableSorting: true,
          enableRowSelection: true,
          multiSelect: true,
          enableSelectAll: true,
          //showTreeExpandNoChildren: true,
          // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
          // paginationPageSize: 15,
          columnDefs: [{
              name: 'Id. Estimate',
              field: 'IdEstimate',
              width: '7%',
              visible: false
            },
            {
              name:'No. Working Order',
              field:'WoNo'
            },
            {
              name: 'No. Polisi',
              field: 'PoliceNumber'
            },
            {
              name: 'Model',
              field: 'ModelType'
            }
          ]
        };   

    });