angular.module('app')
    .factory('ServiceExplanation', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(type,filter) {
                var param = {};
                param.isGr = type;
                if(type == 1){
                    param.policeNo = filter;
                    param.woNo = '';
                    // var res = $http.get('/api/as/jobs/1/WoNo/-/policeNo/'+filter);
                    // api/as/serviceexplanation/1/WoNo/-/policeNo/F1234AN
                    // var res = $http.get('/api/as/serviceexplanation/1/WoNo/-/policeNo/'+filter);
                    //console.log('res=>',res);
                    // return res;
                }else{
                    param.woNo = filter;
                    param.policeNo = '';
                    // var res = $http.get('/api/as/serviceexplanation/1/WoNo/'+filter+'/policeNo/-');
                    //console.log('res=>',res);
                    // return res;
                }
                return $http.put('/api/as/serviceexplanation/',[param]);
                
            },
            getDataPreDiagnose: function(JobID) {
                    var res = $http.get('/api/as/ServiceExplanations/getByJobId/'+JobID);
                    //console.log('res=>',res);
                    return res;
                
            },
            update: function(data) {
                
                // console.log("jobid",JobID);
                // console.log("mData",mData);

                return $http.put('/api/as/ServiceExplanations/Update/'+data.JobId+'',
                [
                    [{
                        InsideCleaness:data.InsideCleaness,
                        OutsideCleaness:data.OutsideCleaness,
                        CompletenessVehicle:data.CompletenessVehicle,
                        UsedPart:data.UsedPart,
                        DriverCarpetPosition:data.DriverCarpetPosition,
                        IsJobResultExplainOk:data.IsJobResultExplainOk,
                        JobResultExplainReason:data.JobResultExplainReason,
                        IsSparePartExplainOk:data.IsSparePartExplainOk,
                        SparePartExplainReason:data.SparePartExplainReason,
                        IsConfirmPriceOk:data.IsConfirmPriceOk,
                        ConfirmPriceReason:data.ConfirmPriceReason,
                        IsJobSuggestExplainOk:data.IsJobSuggestExplainOk,
                        JobSuggestExplainReason:data.JobSuggestExplainReason,
                        IsConfirmVehicleCheckOk:data.IsConfirmVehicleCheckOk,
                        ConfirmVehicleCheckReason:data.ConfirmVehicleCheckReason
                    }],    
                    [{
                        FollowUpDate:data.FollowUpDate,
                        PhoneType:data.phoneType,
                        PhoneNumber:data.PhoneNumber,
                        StatusFUTime:data.StatusFUTime
                    }]
                ]);

                    // return $http.put('/api/as/Washings/', [{
                    //     Id: data.Id,
                    //     JobId: data.JobId,
                    //     TimeStart: data.TimeStart,
                    //     TimeFinish: data.TimeFinish,
                    //     TimeThru: data.TimeThru,
                    //     StatusWashing: data.StatusWashing
                    // }])
            }
                // create: function(role) {
                //   return $http.post('/api/fw/Role', [{
                //                                       AppId: 1,
                //                                       ParentId: 0,
                //                                       Name: role.Name,
                //                                       Description: role.Description}]);
                // },
                // update: function(role){
                //   return $http.put('/api/fw/Role', [{
                //                                       Id: role.Id,
                //                                       //pid: role.pid,
                //                                       Name: role.Name,
                //                                       Description: role.Description}]);
                // },
                // delete: function(id) {
                //   return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
                // },
        }
    });