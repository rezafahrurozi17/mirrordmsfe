angular.module('app')
    .factory('BillingFactory', function($http, CurrentUser, ASPricingEngine) {
        var currentUser = CurrentUser.user();
        console.log(currentUser);
        return {
            getData: function(DateStart, DateFInish) {
                // /api/as/JobTasks/WOListForBilling/0/'+DateStart+'/'+DateFInish+'

                var res = $http.get('/api/as/jobs/WOListForBilling/' + DateStart + '/' + DateFInish + '');
                return res;
            },
            create: function(mBilling, DateNow, JobTask, JobParts, Opl, Total, TotalDiscount, Tax, TotalFinal, DPnya, listSelectedRowsOpb, dataByJobId, PPNPerc) {
                console.log("opl diFactory", Opl);
                console.log("mBilling", mBilling);
                console.log("DateNow", DateNow);
                console.log("JobTask", JobTask);
                console.log("JobParts", JobParts);
                console.log("total in factory", Total);
                console.log("TotalDiscount", TotalDiscount);

                var tmpBillingParts = [];
                for (var i = 0; i < JobParts.length; i++) {
                    // Case ticket #54889
                    // var subTotalParts = ((JobParts[i].Price * JobParts[i].Qty) / 1.1); // Exclude PPN
                    // var totalDiscParts = (subTotalParts * JobParts[i].Discount / 100);
                    // var totalParts = (subTotalParts - totalDiscParts);
                    // totalParts = (totalParts * 1.1);
                    // var subTotalParts = ( Math.round(JobParts[i].Price / 1.1) * JobParts[i].Qty); // Exclude PPN
                    var subTotalParts = ASPricingEngine.calculate({
                                            InputPrice: JobParts[i].Price,
                                            // Discount: 5,
                                            Qty: JobParts[i].Qty,
                                            Tipe: 102, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                    // var totalDiscParts = ( Math.round(Math.round(JobParts[i].Price / 1.1) * (JobParts[i].Discount / 100)) * JobParts[i].Qty );
                    var totalDiscParts = ASPricingEngine.calculate({
                                            InputPrice: JobParts[i].Price,
                                            Discount: JobParts[i].Discount,
                                            Qty: JobParts[i].Qty,
                                            Tipe: 112, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                    var totalParts = (subTotalParts - totalDiscParts);
                    // totalParts = (totalParts * 1.1);
                    totalParts = (totalParts * (1+PPNPerc/100));
                    var subtotalTanpaDiskon = (subTotalParts * (1+PPNPerc/100));

                    if (JobParts[i].PaidById == 30){
                        // ganti semua hitungan na kl warranty harga awal na dah ga ada ppn
                        subTotalParts = JobParts[i].Price * JobParts[i].Qty
                        // totalDiscParts = ( Math.round(JobParts[i].Price * (JobParts[i].Discount / 100)) * JobParts[i].Qty ); // karena warranty ga ada diskon ini bs di ilangin
                        // totalParts = (subTotalParts - totalDiscParts); // karena ga ada diskon ini di ganti aja
                        totalParts = (subTotalParts);
                        // totalParts = (totalParts * 1.1);
                        for(var x in dataByJobId){
                            for(var j in dataByJobId[x].JobTask){
                                var getJobTypeId = dataByJobId[x].JobTask[j].JobTypeId
                                if (getJobTypeId == 2675 || getJobTypeId == 2678) {   
                                    totalParts = (subTotalParts);
                                }else{
                                    totalParts = (totalParts * (1+PPNPerc/100));
                                }
                            }
                        }
                    }
                    if (JobParts[i].PaidById == 30){
                        // permintaan pa dodi, warranty itu, total = subtotal = price
                        tmpBillingParts.push({
                            Id: 0,
                            BillingId: 0,
                            JobPartsId: JobParts[i].JobPartsId,
                            Qty: JobParts[i].Qty,
                            Price: JobParts[i].Price,
                            Subtotal: totalParts,
                            Disc: JobParts[i].Discount,
                            Total: totalParts,
                            IsBilling: 1
                        });
                    } else {
                        tmpBillingParts.push({
                            Id: 0,
                            BillingId: 0,
                            JobPartsId: JobParts[i].JobPartsId,
                            Qty: JobParts[i].Qty,
                            Price: JobParts[i].Price,
                            Subtotal: subtotalTanpaDiskon,
                            Disc: JobParts[i].Discount,
                            Total: totalParts,
                            IsBilling: 1
                        });
                    }
                    
                }

                for (var i = 0; i < listSelectedRowsOpb.length; i++) {
                    // Case ticket #54889
                    // var subTotalOpb = ((listSelectedRowsOpb[i].Price * listSelectedRowsOpb[i].Qty) / 1.1); // Exclude PPN
                    // var totalDiscOpb = (subTotalOpb * listSelectedRowsOpb[i].Discount / 100);
                    // var totalOpb = (subTotalOpb - totalDiscOpb);
                    // totalOpb = (totalOpb * 1.1);
                    // var subTotalOpb = ( Math.round(listSelectedRowsOpb[i].Price / 1.1) * listSelectedRowsOpb[i].Qty ); // Exclude PPN
                    var subTotalOpb = ASPricingEngine.calculate({
                                            InputPrice: listSelectedRowsOpb[i].Price,
                                            // Discount: 5,
                                            Qty: listSelectedRowsOpb[i].Qty,
                                            Tipe: 102, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                    // var totalDiscOpb = ( Math.round(Math.round(listSelectedRowsOpb[i].Price / 1.1) * listSelectedRowsOpb[i].Discount / 100) * listSelectedRowsOpb[i].Qty);
                    var totalDiscOpb = ASPricingEngine.calculate({
                                            InputPrice: listSelectedRowsOpb[i].Price,
                                            Discount: listSelectedRowsOpb[i].Discount,
                                            Qty: listSelectedRowsOpb[i].Qty,
                                            Tipe: 112, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                    var totalOpb = (subTotalOpb - totalDiscOpb);
                    // totalOpb = (totalOpb * 1.1);
                    totalOpb = (totalOpb * (1+PPNPerc/100));


                    tmpBillingParts.push({
                        Id: 0,
                        BillingId: 0,
                        JobPartsId: listSelectedRowsOpb[i].JobPartsId,
                        Qty: listSelectedRowsOpb[i].Qty,
                        Price: listSelectedRowsOpb[i].Price,
                        Subtotal: (listSelectedRowsOpb[i].Price * listSelectedRowsOpb[i].Qty),
                        Disc: listSelectedRowsOpb[i].Discount,
                        Total: totalOpb,
                        IsBilling: 1
                    });
                }
                console.log("billparts lagi", tmpBillingParts);

                var routePause = '/api/as/Billings/' + mBilling.isGr;

                var tmpBillingJob = [];
                var FareBP = 1;
                for (var i = 0; i < JobTask.length; i++) {

                    if (JobTask[i].FlatRate == null || JobTask[i].FlatRate == undefined) {
                        FareBP = 1;
                    } else {
                        FareBP = JobTask[i].FlatRate;
                    }

                    // Case ticket #54889
                    // var subTotalJob = ((JobTask[i].Fare * FareBP) / 1.1); // Exclude PPN
                    // var totalDiscJob = (subTotalJob * JobTask[i].Discount / 100);
                    // var totalJob = (subTotalJob - totalDiscJob);
                    // totalJob = (totalJob * 1.1);
                    // var subTotalJob = ( Math.round(Math.round(JobTask[i].Fare * FareBP) / 1.1)); // Exclude PPN
                    var subTotalJob = ASPricingEngine.calculate({
                                            InputPrice: Math.round(JobTask[i].Fare * FareBP),
                                            // Discount: 5,
                                            // Qty: 2,
                                            Tipe: 2, 
                                            PPNPercentage: PPNPerc,
                                        });
                    // var totalDiscJob = Math.round(subTotalJob * JobTask[i].Discount / 100);
                    var totalDiscJob = ASPricingEngine.calculate({
                                            InputPrice: Math.round(JobTask[i].Fare * FareBP),
                                            Discount: JobTask[i].Discount,
                                            // Qty: 2,
                                            Tipe: 12, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                    var totalJob = (subTotalJob - totalDiscJob);
                    // totalJob = (totalJob * 1.1);
                    totalJob = (totalJob * (1+PPNPerc/100));


                    tmpBillingJob.push({
                        Id: 0,
                        BillingId: 0,
                        JobTaskId: JobTask[i].JobTaskId,
                        Qty: 1,
                        Price: Math.round(JobTask[i].Fare * FareBP),
                        Subtotal: Math.round(JobTask[i].Fare * FareBP),
                        Disc: JobTask[i].Discount,
                        Total: totalJob,
                        IsBilling: 1
                    });
                }
                console.log("billjob lagi", tmpBillingJob);

                var tmpOpl = [];
                for (var o = 0; o < Opl.length; o++) {

                    var oplpurchaseprice = 0;
                    oplpurchaseprice = Opl[o].Price;
                    if (Opl[o].PaidById == 30 ){
                        oplpurchaseprice = Opl[o].OPLPurchasePrice;
                    }

                    // var subTotalOPL = ( Math.round(oplpurchaseprice / 1.1) * Opl[o].QtyPekerjaan ); // Exclude PPN
                    var subTotalOPL = ASPricingEngine.calculate({
                                            InputPrice: oplpurchaseprice,
                                            // Discount: 5,
                                            Qty: Opl[o].QtyPekerjaan,
                                            Tipe: 102, 
                                            PPNPercentage: PPNPerc,
                                        }); 

                    // var totalDiscOPL = ( Math.round(Math.round(oplpurchaseprice / 1.1) * (Opl[o].Discount / 100)) * Opl[o].QtyPekerjaan );
                    var totalDiscOPL = ASPricingEngine.calculate({
                                            InputPrice: oplpurchaseprice,
                                            Discount: Opl[o].Discount,
                                            Qty: Opl[o].QtyPekerjaan,
                                            Tipe: 112, 
                                            PPNPercentage: PPNPerc,
                                        }); 

                    var totalOPL = (subTotalOPL - totalDiscOPL);
                    // totalOPL = (totalOPL * 1.1);
                    totalOPL = (totalOPL * (1+PPNPerc/100));

                    
                    tmpOpl.push({
                        Id: 0,
                        BillingId: 0,
                        JobTaskOplId: Opl[o].JobTaskOplId,
                        Qty: Opl[o].QtyPekerjaan,
                        Price: oplpurchaseprice,
                        Subtotal: (oplpurchaseprice * Opl[o].QtyPekerjaan),
                        Disc: Opl[o].Discount,
                        // Total: (oplpurchaseprice * Opl[o].QtyPekerjaan) - (oplpurchaseprice * Opl[o].QtyPekerjaan * Opl[o].Discount / 100),
                        Total: totalOPL,
                        IsBilling: 1
                            // ,
                            // "LastModifiedUserId": 0,
                            // "LastModifiedDate": "2017-04-19T00:00:00"
                    });
                }
                console.log("tmpOpl", tmpOpl);

                var totalDP = 0;
                if (mBilling.IsPaidOR == true && mBilling.dataPayment == 29) { // Insurance
                    // totalDP = mBilling.ORPaidAmount + DPnya;
                    totalDP = mBilling.ORPaidAmount;

                } else if (mBilling.dataPayment == 28) { // Customer
                    totalDP = dataByJobId[0].DPPaidAmount
                    if (totalDP == null || totalDP == undefined){
                        totalDP = 0
                    }
                    // totalDP = DPnya;
                }

                var ispaidd = 0;
                // if (mBilling.dataPayment == 2458) { // PKS
                //     ispaidd = 1;
                // } else {
                //     ispaidd = 0;
                // }
				console.log('mBilling.TIN at factory : ', mBilling.TIN);
				
				if (mBilling.dataPayment == 2277) { 
					// free service
					mBilling.KTPKITAS = '';
                }
				
				console.log('mBilling.KTPKITAS at factory : ', mBilling.KTPKITAS);
				
				if(mBilling.isNPWP == 0){
					mBilling.KTPKITAS = '';
				}else if(mBilling.isNPWP == 1){
					mBilling.TIN = '';
				}

				if(mBilling.TIN == null || mBilling.TIN == "" || mBilling.TIN == "00.000.000.0-000.000"){
					mBilling.TIN = "00.000.000.0-000.000";
				}

                return $http.post(routePause, [{
                    BillingId: 0,
                    BillingNo: "",
                    BillingDate: DateNow,
                    JobId: mBilling.JobId,
                    ChargeTo: mBilling.dataPayment,
                    SubTotal: Total,
                    isPaid: ispaidd,
                    Disc: TotalDiscount,
                    Total: TotalFinal,
                    StatusBilling: 1,
                    isPrintedBilling: 0,
                    isPrintedCancel: 0,
                    CancelBillingNo: "",
                    // BillTo: mBilling.BillTo,
                    BillTo: this.checkIsCharacter(mBilling.BillTo),
                    BillAddress: mBilling.BillAddress,
                    BillPhoneNumber: mBilling.BillPhoneNumber,
                    TIN: mBilling.TIN,
                    TaxName: mBilling.TaxName,
                    TaxAddress: mBilling.TaxAddress,
                    TaxCode: mBilling.IdCodeTransaction,
                    BillingParts: tmpBillingParts,
                    BillingJob: tmpBillingJob,
                    BillingOpls: tmpOpl,
                    taxAmount: Tax,
                    BillingPaidAmount: totalDP,
                    StartBillingDate: mBilling.TimeStartBilling,
                    FinishBillingDate: mBilling.TimeFinishBilling,
                    IsWNA: mBilling.isWNA,
					KTPKITAS: mBilling.KTPKITAS,
					IsKTP:mBilling.isNPWP,
					GridDetailCustomer: [
							{
								IsWNA: mBilling.isWNA,
								TIN: mBilling.TIN,
								KTPKITAS: mBilling.KTPKITAS,
								CustomerId: mBilling.CustomerId,
                                IsNPWP:mBilling.isNPWP,
                                BillTo : this.checkIsCharacter(mBilling.BillTo),
                                dataCustOLD : mBilling.dataCustOLD,
                                ToyotaId:mBilling.ToyotaId
							}
						],

					//KTPKITAS: mBilling.KTPKITAS, //stv
					//IsWNA : mBilling.isWNA //stv
                    // data.BillingParts,
                    // BillingJob: data.BillingJob
                }]);
            },
            // update: function(data){
            //   return $http.put('/api/as/WorkingShift', [{
            //                                       Id: data.Id,
            //                                       ShiftByte:data.ShiftByte,
            //                                       Name:data.Name,
            //                                       TimeFrom: data.TimeFrom,
            //                                       TimeThru: data.TimeThru}]);
            // },
            delete: function(id) {
                return $http.delete('/api/as/WorkingShift', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
            getDataAfterCreate: function(Status, DateStart, DateFInish, FlagIsGr) {
                var res = $http.get('/api/as/BillingJobs/ListSudahBilling/' + Status + '/' + DateStart + '/' + DateFInish + '/' + FlagIsGr);
                return res;
            },
            getDataEmployeeSa: function(JobId) {
                //var res = $http.get('/api/as/Jobs/' + JobId + '');
                var res = $http.get('/api/as/BillingJobs/getJobData/' + JobId + '');
                return res;
            },
            getDataJobTask: function(JobTaskId) {
                var res = $http.get('/api/as/JobTasks/' + JobTaskId + '');
                return res;
            },
            getDataCRM: function(VehicleId) {
                var res = $http.get('/api/crm/GetListDataVehicleforBilling/' + VehicleId + '');

                console.log("rest di factory crm GetListDataVehicleforBilling", res);

                return res;
            },
            getDataCancel: function(BillingId) {
                var res = $http.get('/api/as/Billings/' + BillingId + '');

                return res;

            },
            ReqCancelBilling: function(data, BillingId) {
                return $http.post('/api/as/Billings/RequestCancelBilling', [{
                    BillingId: BillingId,
                    CancelType: data.CancelType,
                    ApproverCancelBy: 1,
                    CancelMessage: data.CancelMessage,
                    RejectMessage: ""
                }]);
            },
            PostApproveCancelBilling: function(BillingId, BillingNo, FakturNo) {
                return $http.post('/api/as/PostApprovalCancelBilling', [{
                    "BillingId": BillingId,
                    "BillingNo": BillingNo,
                    "FakturNo": FakturNo
                }]);
            },
            // RejectCancelBilling: function(data, BillingId, CancelType, CancelMessage) {

            //     return $http.put('/api/as/Billings/RejectCancelBilling', [{
            //         BillingId: BillingId,
            //         CancelType: CancelType,
            //         ApproverCancelBy: 1,
            //         CancelMessage: CancelMessage,
            //         RejectMessage: data.RejectMessage
            //     }]);
            // }, dari pak deddy
            RejectCancelBilling: function(data, dataAprove, user) {
                return $http.put('/api/as/Billing/UpdateApproval', [
                    [{
                        "OutletId": user.OrgId,
                        "TransactionId": dataAprove.ApprovalBilling[0].TransactionId,
                        "BillingId": dataAprove.ApprovalBilling[0].BillingId,
                        "BillingNo": dataAprove.ApprovalBilling[0].BillingNo,
                        "TaxInvoiceNumber": dataAprove.ApprovalBilling[0].TaxInvoiceNumber,
                        "ApprovalCategoryId": 44,
                        "seq": 1,
                        "ApproverId": null,
                        "RequesterId": null,
                        "ApproverRoleId": user.RoleId,
                        "RequestorRoleId": dataAprove.ApprovalBilling[0].RequestorRoleId,
                        "StatusApprovalId": 2,
                        "Reason": data.RejectMessage,
                        "ApproveRejectDate": new Date(),
                        "StatusCode": 1
                    }],
                    [{
                        "CancelType": data.CancelType,
                        "CancelMessage": data.RejectMessage
                    }]
                ]);
            },
            ApproveCancelBilling: function(data, dataAprove, user) {
                if (dataAprove.ApprovalBilling[0] != undefined || dataAprove.ApprovalBilling[0] != null) {
                    return $http.put('/api/as/Billing/UpdateApproval', [
                        [{
                            "OutletId": user.OrgId,
                            "TransactionId": dataAprove.ApprovalBilling[0].TransactionId,
                            "BillingId": dataAprove.ApprovalBilling[0].BillingId,
                            "BillingNo": dataAprove.ApprovalBilling[0].BillingNo,
                            "TaxInvoiceNumber": dataAprove.ApprovalBilling[0].TaxInvoiceNumber,
                            "ApprovalCategoryId": 44,
                            "seq": 1,
                            "ApproverId": null,
                            "RequesterId": null,
                            "ApproverRoleId": user.RoleId,
                            "RequestorRoleId": dataAprove.ApprovalBilling[0].RequestorRoleId,
                            "StatusApprovalId": 1,
                            "Reason": data.RejectMessage,
                            "ApproveRejectDate": new Date(),
                            "StatusCode": 1
                        }],
                        [{
                            "CancelType": data.CancelType,
                            "CancelMessage": data.RejectMessage
                        }]
                    ]);
                } else {
                    return $http.put('/api/as/Billing/UpdateApproval', [
                        [{
                            "OutletId": user.OrgId,
                            "TransactionId": dataAprove.ApprovalBilling.TransactionId,
                            "BillingId": dataAprove.ApprovalBilling.BillingId,
                            "BillingNo": dataAprove.ApprovalBilling.BillingNo,
                            "TaxInvoiceNumber": dataAprove.ApprovalBilling.TaxInvoiceNumber,
                            "ApprovalCategoryId": 44,
                            "seq": 1,
                            "ApproverId": null,
                            "RequesterId": null,
                            "ApproverRoleId": user.RoleId,
                            "RequestorRoleId": dataAprove.ApprovalBilling.RequestorRoleId,
                            "StatusApprovalId": 1,
                            "Reason": data.RejectMessage,
                            "ApproveRejectDate": new Date(),
                            "StatusCode": 1
                        }],
                        [{
                            "CancelType": data.CancelType,
                            "CancelMessage": data.RejectMessage
                        }]
                    ]);
                }
            },
            getPayment: function() {
                var catId = 1008;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getKabeng: function(KabengCode) {
                console.log("KabengCode", KabengCode);
                var res = $http.get('/api/as/GetEmployeeName/' + KabengCode);
                // console.log('resnya pause=>', res);
                return res;
            },
            createInvoice: function(data) {
                console.log('createInvoice', data);
                
                for(var i in data[0].Billing){
                    data[0].Billing[i].ChargeTo = data[0].Billing[i].ChargeToId
                }
                console.log('createInvoice billing==>', data[0].Billing);



                return $http.post('/api/as/Billings/Invoice', data)
            },
            GetDataInvoice: function(Data) {
                console.log("Data Ramah", Data);
                var res = $http.put('/api/as/Billings/GetInvoceInfo', [{
                    "ArrBilling": Data
                }]);
                return res;
            },
            ReqApprovalReprintBilling: function(mBilling, date, billingid) {
                for (var i = 0; i < billingid.length; i++) {

                    if (billingid[i].Job.isGr == 1) {
                        // if(billingid[i].isGr == 1){

                        var res = $http.post('/api/as/ApprovalReprintBilling', [{
                            "BillingId": billingid[i].BillingId,
                            "ApprovalCategoryId": 52,
                            "ApproverRoleId": 1128,
                            // gr 1128 bp 1129
                            "RequestorRoleId": 1044,
                            "StatusApprovalId": 3,
                            "RequestReason": mBilling.CancelMessage,
                            "RequestDate": date,
                            "IsGR": 1
                        }]);
                    } else {

                        var res = $http.post('/api/as/ApprovalReprintBilling', [{
                            "BillingId": billingid[i].BillingId,
                            "ApprovalCategoryId": 52,
                            "ApproverRoleId": 1129,
                            // sg 1128 bp 1129
                            "RequestorRoleId": 1044,
                            "StatusApprovalId": 3,
                            "RequestReason": mBilling.CancelMessage,
                            "RequestDate": date,
                            "IsGR": 0
                        }]);
                    }

                }

                return res;
            },
            getDataReprintBilling: function(item) {
                var res = $http.put('/api/as/GetListReprintBilling', [{
                    Name: item
                }]);
                // console.log('resnya pause=>', res);
                return res;
            },

            putCountPrintedBilling: function(BillingId) {
                var res = $http.put('/api/as/Billings/printed/' + BillingId);
                // console.log('resnya pause=>', res);
                return res;
            },

            getDataReprintBillingByDate: function(StartDate, EndDate) {
                var res = $http.put('/api/as/GetListReprintBilling', [{
                    StartDate: StartDate,
                    EndDate: EndDate
                }]);
                // console.log('resnya pause=>', res);
                return res;
            },
            getCustName: function(key) {
                // api/crm/GetCustName?name=
                return $http.get("/api/crm/GetCustName?name=" + key);
            },
            getVendorThirdParty: function(key) {
                return $http.get("/api/crm/GetCustNameThirdParty?name=" + key);
            },
            getListOutlet: function(key, param) {
                if (key == null || typeof key == 'undefined' || key == "") {
                    key = '-';
                }
                return $http.get('/api/as/GetOutletName/' + param + '?name=' + key);
            },
            getCOutletById: function(key, param) {
                console.log("currentUser", currentUser);
                return $http.get('/api/ct/GetCOutletById/' + currentUser.OrgId);
            },
            getCOutletById2: function(key, param) {
                console.log("currentUser", currentUser);
                // api/ct/GetCOutletByIdSP/{outletId}
                return $http.get('/api/ct/GetCOutletByIdSP/' + currentUser.OrgId);
            },
            putReprintBilling: function(mBilling, date, billingid, statusapprovalid) {
                for (var i = 0; i < billingid.length; i++) {

                    return $http.put('/api/as/UpdateApprovalReprintBilling', [{
                        "OutletId": billingid[i].OutletId,
                        "TransactionId": billingid[i].TransactionId,
                        "BillingId": billingid[i].BillingId,
                        "IsGR": billingid[i].IsGR,
                        "GroupNo": billingid[i].GroupNo,
                        "ApproverRoleId": billingid[i].ApproverRoleId,
                        "RequestorRoleId": billingid[i].RequestorRoleId,
                        "StatusApprovalId": statusapprovalid,
                        "Reason": mBilling.CancelMessage,
                        "ApproveRejectDate": date
                            // "OutletId": 280,
                            // "TransactionId": 6,
                            // "BillingId": 78,
                            // "IsGR": 1,
                            // "GroupNo": null,
                            // "ApproverRoleId": 1028,
                            // "RequestorRoleId": 1044,
                            // "StatusApprovalId": 1,
                            // "Reason": "Boleh",
                            // "ApproveRejectDate": "2018-04-06"

                    }]);
                }
            },
            cancelWithoutApproval: function(data) {
                console.log("anita2", data);
                return $http.post('/api/as/Billings/CancelWithoutApproval', [{ "BillingId": data.BillingId, "CancelType": data.CancelType, "CancelMessage": data.CancelMessage }]);
            },
            CheckTowassApproval: function(key) {
                return $http.get('/api/as/BillingJobs/Checktowas/' + key)
            },
            getDataPKS: function(PoliceNumber) {
                return $http.get('/api/as/PKS/GetByLicensePlate/' + PoliceNumber);
            },
            getDataPKSByOutletId: function(PoliceNumber) {
                return $http.get('/api/as/PKS/GetByLicensePlatePKS/' + PoliceNumber + '/' + currentUser.OrgId);
            },
            getClaim: function(jobId) {
                return $http.get('/api/as/SPKClaim/' + jobId);
            },
            getInsuranceProfile: function(insuranceId) {
                return $http.get('/api/as/Mprofile_InsuranceById/' + insuranceId);
            },
            sendNotif: function(data, recepient, Param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotification', [{
                    Message: data,
                    RecepientId: recepient,
                    Param: Param
                }]);
            },
            sendNotifRole: function(data, recepient, param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param: param
                }]);
            },
            InserToVehicTR: function(VIN, ToyotaId) {
                return $http.post('/api/crm/CRM_InsertVehicleTRtoCTO_SP_ShortVer', [{
                    VIN: VIN,
                    ToyotaId: ToyotaId
                }]);
            },
            getDataBilling: function(BillingId) {
                return $http.get('/api/as/Billings/' + BillingId);
            },
            getTransactionCode: function() {
                // var catId = 2028;
                // var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                // console.log('resnya getTransactionCode=>', res);
                // return res;

                return $http.get('/api/as/Billings/GetDataInvoiceTaxList');
            },
            CheckBillingIncoming: function(BillingId) {
                // api/as/Billing/CheckBillingIncoming/{billingId}
                return $http.get('/api/as/Billing/CheckBillingIncoming/' + BillingId);
            },
            CheckJobInsurance: function(jobid,insuranceId) {
                // api/as/Billing/CheckBillingIncoming/{billingId}
                return $http.get('/api/as/Billings/CheckJobInsurance/' + jobid + '/' + insuranceId);
            },

            CheckStatusGate: function(PoliceNo,BillingId) {
                return $http.get('/api/as/Billings/CheckStatusGate/' + PoliceNo +'/'+ BillingId);
            },

            CheckValdationBillingCancelApproval: function(BillingId) {
                // api/as/CheckValdationBillingCancelApproval/{BillingId}
                return $http.put('/api/as/CheckValdationBillingCancelApproval/', BillingId); // kiriman billingid na bentuk array karena bs sekaligus print banyak
            },

            RecalculateOPLTeknisi: function (JobId) {
                // api/as/RecalculateOPLTeknisi/{JobId}
                return $http.put('/api/as/RecalculateOPLTeknisi/' + JobId );
            },

            CleansingMaterialRequestCompleteGI: function (JobId) {
                // api/as/RecalculateOPLTeknisi/{JobId}
                return $http.put('/api/as/GoodsIssue/CleansingMaterialRequestCompleteGI/?JobId=' + JobId );
            },

            cekDataAlreadyBilling: function(mBilling, DateNow, JobTask, JobParts, Opl, Total, TotalDiscount, Tax, TotalFinal, DPnya, listSelectedRowsOpb, dataByJobId, PPNPerc) {
                console.log("opl diFactory", Opl);
                console.log("mBilling", mBilling);
                console.log("DateNow", DateNow);
                console.log("JobTask", JobTask);
                console.log("JobParts", JobParts);
                console.log("total in factory", Total);
                console.log("TotalDiscount", TotalDiscount);

                var tmpBillingParts = [];
                for (var i = 0; i < JobParts.length; i++) {
                    // Case ticket #54889
                    // var subTotalParts = ((JobParts[i].Price * JobParts[i].Qty) / 1.1); // Exclude PPN
                    // var totalDiscParts = (subTotalParts * JobParts[i].Discount / 100);
                    // var totalParts = (subTotalParts - totalDiscParts);
                    // totalParts = (totalParts * 1.1);
                    // var subTotalParts = ( Math.round(JobParts[i].Price / 1.1) * JobParts[i].Qty); // Exclude PPN
                    var subTotalParts = ASPricingEngine.calculate({
                                            InputPrice: JobParts[i].Price,
                                            // Discount: 5,
                                            Qty: JobParts[i].Qty,
                                            Tipe: 102, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                    // var totalDiscParts = ( Math.round(Math.round(JobParts[i].Price / 1.1) * (JobParts[i].Discount / 100)) * JobParts[i].Qty );
                    var totalDiscParts = ASPricingEngine.calculate({
                                            InputPrice: JobParts[i].Price,
                                            Discount: JobParts[i].Discount,
                                            Qty: JobParts[i].Qty,
                                            Tipe: 112, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                    var totalParts = (subTotalParts - totalDiscParts);
                    // totalParts = (totalParts * 1.1);
                    totalParts = (totalParts * (1+PPNPerc/100));
                    var subtotalTanpaDiskon = (subTotalParts * (1+PPNPerc/100));


                    if (JobParts[i].PaidById == 30){
                        // ganti semua hitungan na kl warranty harga awal na dah ga ada ppn
                        subTotalParts = JobParts[i].Price * JobParts[i].Qty
                        // totalDiscParts = ( Math.round(JobParts[i].Price * (JobParts[i].Discount / 100)) * JobParts[i].Qty ); // karena warranty ga ada diskon ini bs di ilangin
                        // totalParts = (subTotalParts - totalDiscParts); // karena ga ada diskon ini di ganti aja
                        totalParts = (subTotalParts);
                        // totalParts = (totalParts * 1.1);
                        totalParts = (totalParts * (1+PPNPerc/100));
                    }

                    if (JobParts[i].PaidById == 30){
                        // permintaan pa dodi, warranty itu, total = subtotal = price
                        tmpBillingParts.push({
                            Id: 0,
                            BillingId: 0,
                            JobPartsId: JobParts[i].JobPartsId,
                            Qty: JobParts[i].Qty,
                            Price: JobParts[i].Price,
                            Subtotal: totalParts,
                            Disc: JobParts[i].Discount,
                            Total: totalParts,
                            IsBilling: 1
                        });
                    } else {
                        tmpBillingParts.push({
                            Id: 0,
                            BillingId: 0,
                            JobPartsId: JobParts[i].JobPartsId,
                            Qty: JobParts[i].Qty,
                            Price: JobParts[i].Price,
                            Subtotal: subtotalTanpaDiskon,
                            Disc: JobParts[i].Discount,
                            Total: totalParts,
                            IsBilling: 1
                        });
                    }
                }

                for (var i = 0; i < listSelectedRowsOpb.length; i++) {
                    // Case ticket #54889
                    // var subTotalOpb = ((listSelectedRowsOpb[i].Price * listSelectedRowsOpb[i].Qty) / 1.1); // Exclude PPN
                    // var totalDiscOpb = (subTotalOpb * listSelectedRowsOpb[i].Discount / 100);
                    // var totalOpb = (subTotalOpb - totalDiscOpb);
                    // totalOpb = (totalOpb * 1.1);
                    // var subTotalOpb = ( Math.round(listSelectedRowsOpb[i].Price / 1.1) * listSelectedRowsOpb[i].Qty ); // Exclude PPN
                    var subTotalOpb = ASPricingEngine.calculate({
                                            InputPrice: listSelectedRowsOpb[i].Price,
                                            // Discount: 5,
                                            Qty: listSelectedRowsOpb[i].Qty,
                                            Tipe: 102, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                    // var totalDiscOpb = ( Math.round(Math.round(listSelectedRowsOpb[i].Price / 1.1) * listSelectedRowsOpb[i].Discount / 100) * listSelectedRowsOpb[i].Qty);
                    var totalDiscOpb = ASPricingEngine.calculate({
                                            InputPrice: listSelectedRowsOpb[i].Price,
                                            Discount: listSelectedRowsOpb[i].Discount,
                                            Qty: listSelectedRowsOpb[i].Qty,
                                            Tipe: 112, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                    var totalOpb = (subTotalOpb - totalDiscOpb);
                    // totalOpb = (totalOpb * 1.1);
                    totalOpb = (totalOpb * (1+PPNPerc/100));


                    tmpBillingParts.push({
                        Id: 0,
                        BillingId: 0,
                        JobPartsId: listSelectedRowsOpb[i].JobPartsId,
                        Qty: listSelectedRowsOpb[i].Qty,
                        Price: listSelectedRowsOpb[i].Price,
                        Subtotal: (listSelectedRowsOpb[i].Price * listSelectedRowsOpb[i].Qty),
                        Disc: listSelectedRowsOpb[i].Discount,
                        Total: totalOpb,
                        IsBilling: 1
                    });
                }
                console.log("billparts lagi", tmpBillingParts);

                var routePause = '/api/as/Billing/CheckIsJobAlreadyBilling/';

                var tmpBillingJob = [];
                var FareBP = 1;
                for (var i = 0; i < JobTask.length; i++) {

                    if (JobTask[i].FlatRate == null || JobTask[i].FlatRate == undefined) {
                        FareBP = 1;
                    } else {
                        FareBP = JobTask[i].FlatRate;
                    }

                    // Case ticket #54889
                    // var subTotalJob = ((JobTask[i].Fare * FareBP) / 1.1); // Exclude PPN
                    // var totalDiscJob = (subTotalJob * JobTask[i].Discount / 100);
                    // var totalJob = (subTotalJob - totalDiscJob);
                    // totalJob = (totalJob * 1.1);
                    // var subTotalJob = ( Math.round(Math.round(JobTask[i].Fare * FareBP) / 1.1)); // Exclude PPN
                    var subTotalJob = ASPricingEngine.calculate({
                                            InputPrice: Math.round(JobTask[i].Fare * FareBP),
                                            // Discount: 5,
                                            // Qty: 2,
                                            Tipe: 2, 
                                            PPNPercentage: PPNPerc,
                                        });
                    // var totalDiscJob = Math.round(subTotalJob * JobTask[i].Discount / 100);
                    var totalDiscJob = ASPricingEngine.calculate({
                                            InputPrice: Math.round(JobTask[i].Fare * FareBP),
                                            Discount: JobTask[i].Discount,
                                            // Qty: 2,
                                            Tipe: 12, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                    var totalJob = (subTotalJob - totalDiscJob);
                    // totalJob = (totalJob * 1.1);
                    totalJob = (totalJob * (1+PPNPerc/100));


                    tmpBillingJob.push({
                        Id: 0,
                        BillingId: 0,
                        JobTaskId: JobTask[i].JobTaskId,
                        Qty: 1,
                        // Price: (JobTask[i].Fare * FareBP),
                        // Subtotal: (JobTask[i].Fare * FareBP),
                        Price: Math.round(JobTask[i].Fare * FareBP),
                        Subtotal: Math.round(JobTask[i].Fare * FareBP),
                        Disc: JobTask[i].Discount,
                        Total: totalJob,
                        IsBilling: 1
                    });
                }
                console.log("billjob lagi", tmpBillingJob);

                var tmpOpl = [];
                for (var o = 0; o < Opl.length; o++) {

                    var oplpurchaseprice = 0;
                    oplpurchaseprice = Opl[o].Price;
                    if (Opl[o].PaidById == 30 ){
                        oplpurchaseprice = Opl[o].OPLPurchasePrice;
                    }

                    // var subTotalOPL = ( Math.round(oplpurchaseprice / 1.1) * Opl[o].QtyPekerjaan ); // Exclude PPN
                    var subTotalOPL = ASPricingEngine.calculate({
                                            InputPrice: oplpurchaseprice,
                                            // Discount: 5,
                                            Qty: Opl[o].QtyPekerjaan,
                                            Tipe: 102, 
                                            PPNPercentage: PPNPerc,
                                        }); 

                    // var totalDiscOPL = ( Math.round(Math.round(oplpurchaseprice / 1.1) * (Opl[o].Discount / 100)) * Opl[o].QtyPekerjaan );
                    var totalDiscOPL = ASPricingEngine.calculate({
                                            InputPrice: oplpurchaseprice,
                                            Discount: Opl[o].Discount,
                                            Qty: Opl[o].QtyPekerjaan,
                                            Tipe: 112, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                    var totalOPL = (subTotalOPL - totalDiscOPL);
                    // totalOPL = (totalOPL * 1.1);
                    totalOPL = (totalOPL * (1+PPNPerc/100));

                    
                    tmpOpl.push({
                        Id: 0,
                        BillingId: 0,
                        JobTaskOplId: Opl[o].JobTaskOplId,
                        Qty: Opl[o].QtyPekerjaan,
                        Price: oplpurchaseprice,
                        Subtotal: (oplpurchaseprice * Opl[o].QtyPekerjaan),
                        Disc: Opl[o].Discount,
                        // Total: (oplpurchaseprice * Opl[o].QtyPekerjaan) - (oplpurchaseprice * Opl[o].QtyPekerjaan * Opl[o].Discount / 100),
                        Total: totalOPL,
                        IsBilling: 1
                            // ,
                            // "LastModifiedUserId": 0,
                            // "LastModifiedDate": "2017-04-19T00:00:00"
                    });
                }
                console.log("tmpOpl", tmpOpl);

                var totalDP = 0;
                if (mBilling.IsPaidOR == true && mBilling.dataPayment == 29) { // Insurance
                    // totalDP = mBilling.ORPaidAmount + DPnya;
                    totalDP = mBilling.ORPaidAmount;

                } else if (mBilling.dataPayment == 28) { // Customer
                    totalDP = dataByJobId[0].DPPaidAmount
                    if (totalDP == null || totalDP == undefined){
                        totalDP = 0
                    }
                    // totalDP = DPnya;
                }

                var ispaidd = 0;
                // if (mBilling.dataPayment == 2458) { // PKS
                //     ispaidd = 1;
                // } else {
                //     ispaidd = 0;
                // }

                return $http.put(routePause, [{
                    BillingId: 0,
                    BillingNo: "",
                    BillingDate: DateNow,
                    JobId: mBilling.JobId,
                    ChargeTo: mBilling.dataPayment,
                    SubTotal: Total,
                    isPaid: ispaidd,
                    Disc: TotalDiscount,
                    Total: TotalFinal,
                    StatusBilling: 1,
                    isPrintedBilling: 0,
                    isPrintedCancel: 0,
                    CancelBillingNo: "",
                    // BillTo: mBilling.BillTo,
                    BillTo: this.checkIsCharacter(mBilling.BillTo),
                    BillAddress: mBilling.BillAddress,
                    BillPhoneNumber: mBilling.BillPhoneNumber,
                    TIN: mBilling.TIN,
                    TaxName: mBilling.TaxName,
                    TaxAddress: mBilling.TaxAddress,
                    TaxCode: mBilling.IdCodeTransaction,
                    BillingParts: tmpBillingParts,
                    BillingJob: tmpBillingJob,
                    BillingOpls: tmpOpl,
                    taxAmount: Tax,
                    BillingPaidAmount: totalDP
                    // data.BillingParts,
                    // BillingJob: data.BillingJob
                }]);
            },

            checkIsCharacter: function(data){
                if(typeof data == 'string'){
                    data = data.replace(/[^\x20-\x7f\xA\xD]/g, '');
                }
                return data;
            },

            CekStatusBilling: function(data) {
                return $http.put('/api/as/CekStatusBilling/', data);
            },
        }
    });