angular.module('app')
    .controller('ReprintBillingController', function($scope, $http, CurrentUser, ReprintBillingFactory,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    console.log("User",$scope.user);
    $scope.mData = null; //Model
    $scope.show_modal={show:false};
    $scope.modal_model = [];
    $scope.modalMode='new';

    $scope.show_modalCancel={show:false};
    $scope.modal_modelCancel = [];
    $scope.modalModeCancel='new';
    // $scope.xStaff = {};
    // $scope.xStaff.selected=[];
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.dataFilter = [
        {
            Id:1,
            Name:'Tanggal Billing'
        },
        {
            Id:2,
            Name:'Tanggal Wo'
        }
    ];

    var gridData = [];
    var res = [
        {
            NoBilling : '12345',
            NoWo : '56778',
            NoPolisi : 'B 1234 FG',
            TanggalBilling : '2017-04-27',
            TanggalWo : '2017-03-27',
            Model : 'Avanza',
            Status:'Approved',
            Printed : 1
        }
    ];
    $scope.getData = function() {
        gridData = []
        $scope.loading = false;
        // $scope.grid.data = res;
        // StaffManagement.getData()
        // .then(
        //     function(res){
        //         gridData = [];
        //         //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
                $scope.grid.data = res;
        //         //console.log("role=>",res);
        //         //console.log("grid data=>",$scope.grid.data);
        //         //$scope.roleData = res.data;
        //         $scope.loading=false;
        //     },
        //     function(err){
        //         // console.log("err=>",err);
        //     }
        // );
    }
   
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }

    $scope.onBeforeNewMode = function(){
        $scope.getDataShift();
        console.log("berhasil");
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.reqSave = function(){
        console.log("save");
    }

    $scope.reqCancel = function(){
        console.log("cancel");
    }

    $scope.actReq = function(data){
        console.log("Batal Biling",data);
        $scope.show_modal={show:true};
        // BillingFactory.getDataCancel(data.BillingId)
        // .then(
        //     function(res){
        //         // $scope.DataPemeriksaanAwal = res.data.Result;
        //         $scope.dataCancel = res.data.Result;
        //         $scope.show_modal={show:true};
        //         console.log("data cancel",$scope.dataCancel);
        //         // console.log("First Check", $scope.DataPemeriksaanAwal);
        //         // $scope.loading=false;
        //     },
        //     function(err){
        //         console.log("err=>",err);
        //     }
        // );

    }

    $scope.actRePrint = function(){
        console.log("Reprint");
    }

    $scope.actRejectReq = function(){
        $scope.show_modalCancel={show:true};
    }

    $scope.reqSaveCancel = function(){
        console.log("save");
    }

    $scope.reqCancelCancel = function(){
        console.log("cancel");
    }


    $scope.gridActionTemplate = '<div class="ui-grid-cell-contents">\
    <a href="#" ng-click="grid.appScope.$parent.actReq(row.entity)" uib-tooltip="Buat Permohonan Reprint" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-print fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>\
    <a href="#" ng-click="grid.appScope.$parent.actRePrint(row.entity)" uib-tooltip="Reprint Billing" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-print fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>\
    <a href="#" ng-click="grid.appScope.$parent.actRejectReq(row.entity)" uib-tooltip="Reject Reprint Billing" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-times-circle-o fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>\
    </div>';

    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'People Id', field:'People.PeopleId', width:'7%', visible:false},
            { name:'No Billing',    field:'NoBilling'},
            { name:'No Wo', field:'NoWo' },
            { name:'No Polisi', field:'NoPolisi' },
            { name:'Tanggal Billing', field:'TanggalBilling'},
            { name:'Tanggal Wo', field:'TanggalWo' },
            { name:'Model', field:'Model' },
            { name:'Status Re-Print', field:'Status' },
            { name:'Printed', field:'Printed' }
        ]
    };
});
