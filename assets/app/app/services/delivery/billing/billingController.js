angular.module('app')
    .controller('billingController', function($scope, $http, $filter, AppointmentGrService, CurrentUser, BillingFactory, WO, $timeout, bsNotify, ASPricingEngine, PrintRpt, bsAlert, PartsGlobal) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            $scope.getPPN()

            $scope.changingFilterButton();
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.getBillTo = function(key) {
            console.log("getBillTo", key);
            console.log(" mBilling.dataPayment", $scope.mBilling.dataPayment);
            var callServ = [];
            if ($scope.mBilling.dataPayment == 2277) { //Free Service

                callServ = BillingFactory.getListOutlet(key, 2).then(function(resu) {
                    var listName = angular.copy(resu.data.Result);
                    return listName;
                });
            } else if ($scope.mBilling.dataPayment == 31) { //Internal
                callServ = BillingFactory.getListOutlet(key, 0).then(function(resu) {
                    var listName = angular.copy(resu.data.Result);
                    return listName;
                });
            } else if ($scope.mBilling.dataPayment == 30) { //Warranty
                callServ = BillingFactory.getListOutlet(key, 4).then(function(resu) {
                    var listName = angular.copy(resu.data.Result);
                    return listName;
                });
            } else if ($scope.mBilling.dataPayment == 32) { //third party
                callServ = BillingFactory.getVendorThirdParty(key).then(function(resu) {
                    var listName = angular.copy(resu.data.Result);
                    return listName;
                });
            } else {
                callServ = BillingFactory.getCustName(key).then(function(resu) {
                    var listName = angular.copy(resu.data.Result);
                    return listName;
                });

                
            }

            return callServ;
        };

        var PPNPerc = 0;
		
		// CR5 #27
		$scope.nationalityTypeData = [{ Id: 0, Name: 'WNI' }, { Id: 1, Name: 'WNA' }];
		
		$scope.showNPWP = true;
		//$scope.showKTPKITAS = false;
		$scope.showPassport = false;
		$scope.showKTP = false;
		// CR5 #27

        $scope.getPPN = function (noNpwp) {
            if (noNpwp !== undefined && noNpwp !== null && noNpwp !== ''){
                noNpwp = noNpwp.toString();
                if (noNpwp.length === 20){
                    if (noNpwp !== '00.000.000.0-000.000'){
                        ASPricingEngine.getPPN(1, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    } else {
                        ASPricingEngine.getPPN(0, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    }
                } else {
                    ASPricingEngine.getPPN(0, 3).then(function (res) {
                        PPNPerc = res.data
                    });
                }

            } else {
                // param 1 adalah isnpwp, param 2 tipe ppn
                ASPricingEngine.getPPN(0, 3).then(function (res) {
                    PPNPerc = res.data
                });
            }
            
        };

        $scope.onSelectName = function(data) {
            console.log('on-Select-Name', data);

            $scope.mBilling.BillTo = data.Name;
            $scope.mBilling.BillAddress = data.Address;
            $scope.mBilling.ToyotaId = data.ToyotaId != null ? data.ToyotaId : '-';
            // $scope.mBilling.TIN = data.Npwp;
            $scope.mBilling.TIN = _.isUndefined(data.NPWP) ? data.Npwp : data.NPWP;
            $scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
            $scope.mBilling.TaxName = data.Name;
            $scope.mBilling.TaxAddress = data.Address;
            $scope.mBilling.BillPhoneNumber = data.PhoneNumber;
            if ($scope.mBilling.BillPhoneNumber == null || $scope.mBilling.BillPhoneNumber == undefined){
                $scope.mBilling.BillPhoneNumber = data.HP;
            }
            $scope.getPPN($scope.mBilling.TIN)
        };

        $scope.onNoResult = function() {
            console.log('gk ketenu apa2');
        };

        $scope.noResults = true;

        $scope.onGotResult = function() {
            console.log("onGotResult=>");
        };
        $scope.user = CurrentUser.user();
        console.log("user", $scope.user);
        var RId = {};
        if ($scope.user.RoleId == 1129) {
            RId = 1129; //login teknisi BP
        } else if ($scope.user.RoleId == 1128 || $scope.user.RoleId == 1174) {
            RId = 1128; //Login teknisi GR
        } else if ($scope.user.RoleId == 1021) {
            RId = 1021; //Login ADH
        } else {
            RId = 0;
        }
        console.log("RId", RId);

        $scope.mBilling = []; //Model
        $scope.xBilling = {};
        $scope.xBilling.selected = [];
        $scope.filter = {};
        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        $scope.ismeridian = false;
        $scope.DateWo = true;
        $scope.DateJob = true;
        $scope.hstep = 1;
        $scope.mstep = 1;
        $scope.formApi = {};
        $scope.formGridApi = {};
        $scope.KabengCodeNotif = "";

        $scope.ApprovalProcessId = 8902;

        $scope.customButtonReprint = {
            save: {
                text: "Kirim Permohonan"
            }
        };

        // $scope.dataJobTaskId = {};
        var fJobParts = [];

        $scope.show_modal = { show: false };
        $scope.modal_model = [];

        $scope.show_modalReprint = { show: false };
        $scope.modal_modelReprint = [];

        $scope.show_modalReprintApprove = { show: false };
        $scope.modal_modelReprintApprove = [];

        $scope.show_modalReprintReject = { show: false };
        $scope.modal_modelReprintReject = [];

        $scope.show_modalReject = { show: false };
        $scope.modal_modelReject = [];

        $scope.show_modalApprove = { show: false };
        $scope.modal_modelApprove = [];

        $scope.show_modalDtl = { show: false };
        $scope.modal_modelDtl = [];
        $scope.date = new Date();

        $scope.modalMode = 'new';

        $scope.show_modalCancelWithoutApprove = { show: false };
        $scope.buttonCancelWithoutApproval = {
            save: {
                text: "Yes"
            },
            cancel: {
                text: "No"
            }
        };
        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        var showData = [];

        if ($scope.user.RoleId == 1129) {
            //login teknisi BP
            showData.push({
                Id: 2,
                Name: "Daftar WO Yang  Sudah Di Billing"
            }, {
                Id: 3,
                Name: "List Data Reprint Billing"
            });
        } else if ($scope.user.RoleId == 1128 || $scope.user.RoleId == 1174) {
            showData.push({
                Id: 2,
                Name: "Daftar WO Yang  Sudah Di Billing"
            }, {
                Id: 3,
                Name: "List Data Reprint Billing"
            }); //Login teknisi GR
        } else {
            showData.push({
                Id: 1,
                Name: "Daftar WO Yang Belum Billing"
            }, {
                Id: 2,
                Name: "Daftar WO Yang  Sudah Di Billing"
            });
        }

        $scope.option = showData;
        var ShowData = '';
        $scope.showw = null;
        $scope.getData = function() {
            console.log("======>>", $scope.showw);
            $scope.grid.data = [];

            BillingFactory.getTransactionCode().then(function(resu) {
                $scope.codeTransaction = resu.data;
                _.map(resu.data, function(val){
                    val.code = parseInt(val.Code);
                });
                console.log('codeTransaction', $scope.codeTransaction);
            });

            // BillingFactory.getListOutlet().then(function(resu) {
            //     $scope.listOutlet = resu.data.Result;
            //     console.log('listOutlet', $scope.listOutlet);
            // });

            // var DateStart = "-";
            // var DateFinish = "-";

            // console.log("First date", firstDate);

            // if (firstDate == undefined || firstDate == "" ||firstDate == null) {
            //   DateStart="-"
            // }else{
            //   DateStart=firstDate
            // };

            // if (lastDate == undefined || lastDate == "" ||lastDate == null) {
            //   DateFinish="-"
            // }else{
            //   DateFinish=lastDate
            // };

            if ($scope.showw != null) {

                var a = new Date($scope.filter.DateStart);
                var yearFirst = a.getFullYear().toString();
                var monthFirst = (a.getMonth() + 1).toString();
                var dayFirst = a.getDate().toString();
                var firstDate = yearFirst + '-' + (monthFirst[1] ? monthFirst : "0" + monthFirst[0]) + '-' + (dayFirst[1] ? dayFirst : "0" + dayFirst[0]);
                console.log("First date", firstDate);

                var b = new Date($scope.filter.DateFinish);
                var yearLast = b.getFullYear().toString();
                var monthLast = (b.getMonth() + 1).toString();
                var dayLast = b.getDate().toString();
                var lastDate = yearLast + '-' + (monthLast[1] ? monthLast : "0" + monthLast[0]) + '-' + (dayLast[1] ? dayLast : "0" + dayLast[0]);

                console.log("last date", lastDate);



                var c = new Date($scope.filter.DateStartWo);
                var yearFirst = c.getFullYear().toString();
                var monthFirst = (c.getMonth() + 1).toString();
                var dayFirst = c.getDate().toString();
                var firstDateWo = yearFirst + '-' + (monthFirst[1] ? monthFirst : "0" + monthFirst[0]) + '-' + (dayFirst[1] ? dayFirst : "0" + dayFirst[0]);

                // console.log("First date", firstDate);


                var d = new Date($scope.filter.DateFinishWo);
                var yearFirst = d.getFullYear().toString();
                var monthFirst = (d.getMonth() + 1).toString();
                var dayFirst = d.getDate().toString();
                var lastDateWo = yearFirst + '-' + (monthFirst[1] ? monthFirst : "0" + monthFirst[0]) + '-' + (dayFirst[1] ? dayFirst : "0" + dayFirst[0]);


                var f = new Date($scope.filter.DateStartAfterBill);
                var yearFirst = f.getFullYear().toString();
                var monthFirst = (f.getMonth() + 1).toString();
                var dayFirst = f.getDate().toString();
                var firstDateAfterBill = yearFirst + '-' + (monthFirst[1] ? monthFirst : "0" + monthFirst[0]) + '-' + (dayFirst[1] ? dayFirst : "0" + dayFirst[0]);

                // console.log("First date", firstDate);

                var g = new Date($scope.filter.DateFinishAfterBill);
                var yearLast = g.getFullYear().toString();
                var monthLast = (g.getMonth() + 1).toString();
                var dayLast = g.getDate().toString();
                var lastDateAfterBill = yearLast + '-' + (monthLast[1] ? monthLast : "0" + monthLast[0]) + '-' + (dayLast[1] ? dayLast : "0" + dayLast[0]);

                // console.log("last date", lastDate);

                if ($scope.showw == 1) {
                    if ($scope.filter.DateStart != null && $scope.filter.DateFinish != null) {

                        BillingFactory.getData(firstDate, lastDate)
                            .then(
                                function(res) {

                                    console.log('BillingFactory.getData ===>',res.data.Result);
                                    $scope.grid.data = [];
                                    // $scope.DataPemeriksaanAwal = res.data.Result;
                                    if(res.data.Result.length > 0){
                                        for (var i = 0; i < res.data.Result.length; i++) {

                                            if(typeof res.data.Result[i].ChargeTo == 'number'){
                                                if(res.data.Result[i].ChargeTo == 2277){
                                                    res.data.Result[i].ChargeToId = 2277
                                                    res.data.Result[i].ChargeToNew = 'Free Service - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 28){
                                                    res.data.Result[i].ChargeToId = 28
                                                    res.data.Result[i].ChargeToNew = 'Customer - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 29){
                                                    res.data.Result[i].ChargeToId = 29
                                                    res.data.Result[i].ChargeToNew = 'Insurance - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 32){
                                                    res.data.Result[i].ChargeToId = 32
                                                    res.data.Result[i].ChargeToNew = 'Third Party - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 30){
                                                    res.data.Result[i].ChargeToId = 30
                                                    // res.data.Result[i].ChargeToNew = 'Warranty - '+res.data.Result[i].BillTo;
                                                    res.data.Result[i].ChargeToNew = 'Warranty - Toyota Astra Motor';
                                                }else if(res.data.Result[i].ChargeTo == 31){
                                                    res.data.Result[i].ChargeToId = 31
                                                    res.data.Result[i].ChargeToNew = 'Internal - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 2458){
                                                    res.data.Result[i].ChargeToId = 2458
                                                    res.data.Result[i].ChargeToNew = 'PKS - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 29){
                                                    res.data.Result[i].ChargeToId = 29
                                                    res.data.Result[i].ChargeToNew = 'Insurance - '+res.data.Result[i].BillTo;
                                                }
                                            }
                                            else{
                                                
                                                // var findWarranty;
                                                // if(res.data.Result[i].ChargeTo == null){
                                                //     findWarranty = " - ";
                                                // }else{
                                                //     findWarranty = res.data.Result[i].ChargeTo.substring(0, 8);
                                                // }
                                                // console.log('is warranty? ===>',findWarranty);
                                                // if(findWarranty == 'Warranty'){
                                                //     res.data.Result[i].ChargeTo= 'Warranty - Toyota Astra Motor';
                                                // }
                                                res.data.Result[i].ChargeToNew = res.data.Result[i].ChargeTo;
                                            }
                                            res.data.Result[i].Job = {};
                                            res.data.Result[i].Job.TaskName = res.data.Result[i].TaskName
    
    
                                            if (res.data.Result[i].IsAllPulledBilling != 1) {
                                                var resF = res.data.Result[i];
                                                resF.xWoCreatedDate = $scope.ubahDateToString(resF.WoCreatedDate)
                                                console.log("data benar 111", resF);
                                                $scope.grid.data.push(resF);
                                            } else {
                                                console.log("masuk else grid depan");
                                            }
                                            $scope.loading = false;
                                            // console.log(res.data);
                                            // var temp = [];
                                            // _.forEach(res.data, function(e) {
                                            //     var obj = {};
                                            //     if (e.JobType == 0) {
                                            //         e.JobType = 'Teco';
                                            //     } else if (e.JobType == 15) {
                                            //         e.JobType = 'Billing';
                                            //     };
                                            //     temp.push(e);
                                            // });
                                            // $scope.grid.data = temp;
                                        }
                                    }
                                    // console.log("First Check", $scope.DataPemeriksaanAwal);
                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
                    } else if ($scope.filter.DateStart == null || $scope.filter.DateFinish == null || $scope.filter.DateStart == undefined || $scope.filter.DateFinish == undefined || $scope.filter.DateStart == '' || $scope.filter.DateFinish == '') {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Mohon isi filter tanggal terlebih dahulu"
                        });
                    } else {

                        // var Tmp = new Date();
                        // var yearLastTmp = Tmp.getFullYear();
                        // var monthLastTmp = Tmp.getMonth() + 1;
                        // var dayLastTmp = Tmp.getDate();
                        // var lastTemp = yearLastTmp + '-' + monthLastTmp + '-' + dayLastTmp;

                        // ======================= Dicomment biar gak muncul data kalo gak isi filter tanggal ======================= start
                        // BillingFactory.getData('-', '-') //'1945-1-1', lastTemp
                        //     .then(
                        //         function(res) {
                        //             $scope.grid.data = [];
                        //             // $scope.DataPemeriksaanAwal = res.data.Result;
                        //             for (var i = 0; i < res.data.Result.length; i++) {
                        //                 if (res.data.Result[i].IsAllPulledBilling != 1) {
                        //                     var resF = res.data.Result[i];
                        //                     resF.xWoCreatedDate = $scope.ubahDateToString(resF.WoCreatedDate)
                        //                     console.log("data benar", resF);
                        //                     $scope.grid.data.push(resF);
                        //                 } else {
                        //                     console.log("masuk else grid depan");
                        //                 }
                        //             }
                        //             // console.log("First Check", $scope.DataPemeriksaanAwal);
                        //             $scope.loading = false;
                        //         },
                        //         function(err) {
                        //             console.log("err=>", err);
                        //         }
                        //     );
                        // ====================== Dicomment biar gak muncul data kalo gak isi filter tanggal ======================= end

                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Mohon isi filter tanggal terlebih dahulu"
                        });

                        // bsNotify.show({
                        //     size: 'big',
                        //     type: 'danger',
                        //     title: "Mohon Input Tanggal Billing",
                        //     // content: error.join('<br>'),
                        //     // number: error.length
                        // });
                    }
                } else if ($scope.showw == 2) {
                    if ($scope.filter.TipeFilter != null) {
                        if ($scope.filter.TipeFilter == 2) {
                            if ($scope.filter.DateStartWo != null && $scope.filter.DateFinishWo != null) {

                                BillingFactory.getDataAfterCreate(0, firstDateWo, lastDateWo, RId)
                                    .then(
                                        function(res) {
                                            console.log('getDataAfterCreate TipeFilter == 2 ====>',res.data.Result)

                                            // for (var i = 0; i < res.data.Result.length; i++) {
                                            //     // if (res.data.Result[i].Billing.StatusBilling == 1) {
                                            //     if (res.data.Result[i].StatusBilling == 1) {
                                            //         res.data.Result[i].xStatusBilling = "Created";
                                            //     } else if (res.data.Result[i].StatusBilling == 2) {
                                            //         res.data.Result[i].xStatusBilling = "Request Cancel Billing";
                                            //     } else if (res.data.Result[i].StatusBilling == 3) {
                                            //         res.data.Result[i].xStatusBilling = "Reject Request Cancel approval";
                                            //     } else if (res.data.Result[i].StatusBilling == 4) {
                                            //         res.data.Result[i].xStatusBilling = "Approve Request Cancel Billing";
                                            //     } else if (res.data.Result[i].StatusBilling == 5) {
                                            //         res.data.Result[i].xStatusBilling = "Printed";
                                            //     } else if (res.data.Result[i].StatusBilling == 6) {
                                            //         res.data.Result[i].xStatusBilling = "Request Reprint Billing";
                                            //     } else if (res.data.Result[i].StatusBilling == 7) {
                                            //         res.data.Result[i].xStatusBilling = "Reject Request Reprint Billing";
                                            //     } else if (res.data.Result[i].StatusBilling == 8) {
                                            //         res.data.Result[i].xStatusBilling = "Approve Request Reprint Billing";
                                            //     } else if (res.data.Result[i].StatusBilling == 0) {
                                            //         res.data.Result[i].xStatusBilling = "Cancel Without Approval";
                                            //     } else {
                                            //         res.data.Result[i].xStatusBilling = " - ";
                                            //     }
                                            // }

                                            // for (var i = 0; i < res.data.Result.length; i++) {
                                            //     if (res.data.Result[i].CancelType == 1) {
                                            //         res.data.Result[i].xCancelType = "Cancel Faktur Pajak";
                                            //     } else if (res.data.Result[i].CancelType == 2) {
                                            //         res.data.Result[i].xCancelType = "Perubahan Harga";
                                            //     } else if (res.data.Result[i].CancelType == 3) {
                                            //         res.data.Result[i].xCancelType = "Perubahan item jasa dan atau item part";
                                            //     } else {
                                            //         res.data.Result[i].xCancelType = " - ";
                                            //     }
                                            // }

                                            // for (var i = 0; i < res.data.Result.length; i++) {
                                            //     if (res.data.Result[i].ChargeTo == 28) {
                                            //         res.data.Result[i].xChargeTo = "Customer";
                                            //     } else if (res.data.Result[i].ChargeTo == 29) {
                                            //         res.data.Result[i].xChargeTo = "Insurance";
                                            //     } else if (res.data.Result[i].ChargeTo == 30) {
                                            //         res.data.Result[i].xChargeTo = "Waranty";
                                            //     } else if (res.data.Result[i].ChargeTo == 31) {
                                            //         res.data.Result[i].xChargeTo = "Internal";
                                            //     } else if (res.data.Result[i].ChargeTo == 32) {
                                            //         res.data.Result[i].xChargeTo = "Third Party";
                                            //     } else if (res.data.Result[i].ChargeTo == 2277) {
                                            //         res.data.Result[i].xChargeTo = "Free Service";
                                            //     } else if (res.data.Result[i].ChargeTo == 2458) {
                                            //         res.data.Result[i].xChargeTo = "PKS";
                                            //     }

                                            //     // for (var a = 0; a < res.data.Result[i].ApprovalBilling.length; a++) {
                                            //     //     if (res.data.Result[i].ApprovalBilling[a].StatusApprovalId == 2) {
                                            //     //         res.data.Result[i].xStatusApprovalId = "Reject";
                                            //     //     } else if (res.data.Result[i].ApprovalBilling[a].StatusApprovalId == 1) {
                                            //     //         res.data.Result[i].xStatusApprovalId = "Approved";
                                            //     //     } else if (res.data.Result[i].ApprovalBilling[a].StatusApprovalId == 3) {
                                            //     //         res.data.Result[i].xStatusApprovalId = "Request Approval";
                                            //     //     } else {
                                            //     //         res.data.Result[i].xStatusApprovalId = " - ";
                                            //     //     }
                                            //     //     // console.log("trah",res.data.Result[i]);
                                            //     // }

                                            // }
                                            // $scope.DataPemeriksaanAwal = res.data.Result;
                                            console.log("anitaaa", res.data.Result);
                                            for (var i = 0; i < res.data.Result.length; i++) {
                                                if(res.data.Result[i].ChargeTo == 2277){
                                                    res.data.Result[i].ChargeToId = 2277;
                                                    res.data.Result[i].ChargeToNew = 'Free Service - '+res.data.Result[i].BillTo;
                                                    res.data.Result[i].ChargeTo = 'Free - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 28){
                                                    res.data.Result[i].ChargeToId = 28;
                                                    res.data.Result[i].ChargeToNew = 'Customer - '+res.data.Result[i].BillTo;
                                                    res.data.Result[i].ChargeTo = 'Customer - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 29){
                                                    res.data.Result[i].ChargeToId = 29
                                                    res.data.Result[i].ChargeToNew = 'Insurance - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 32){
                                                    res.data.Result[i].ChargeToId = 32
                                                    res.data.Result[i].ChargeToNew = 'Third Party - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 30){
                                                    res.data.Result[i].ChargeToId = 30;
                                                    // res.data.Result[i].ChargeToNew = 'Warranty - '+res.data.Result[i].BillTo;
                                                    res.data.Result[i].ChargeToNew = 'Warranty - Toyota Astra Motor';
                                                    res.data.Result[i].ChargeTo = 'Warranty - Toyota Astra Motor';
                                                    // res.data.Result[i].ChargeTo = 'Warranty - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 31){
                                                    res.data.Result[i].ChargeToId = 31;
                                                    res.data.Result[i].ChargeToNew = 'Internal - '+res.data.Result[i].BillTo;
                                                    res.data.Result[i].ChargeTo = 'Internal - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 2458){
                                                    res.data.Result[i].ChargeToId = 2458;
                                                    res.data.Result[i].ChargeToNew = 'PKS - '+res.data.Result[i].BillTo;
                                                    res.data.Result[i].ChargeTo = 'PKS - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 29){
                                                    res.data.Result[i].ChargeToId = 29;
                                                    res.data.Result[i].ChargeToNew = 'Insurance - '+res.data.Result[i].BillTo;
                                                    res.data.Result[i].ChargeTo = 'Insurance - '+res.data.Result[i].BillTo;
                                                }
                                                res.data.Result[i].TaskName = res.data.Result[i].Job.TaskName

                                                if(res.data.Result[i].isPaid == 1){
                                                    res.data.Result[i].isPaidText = 'Lunas';
                                                }else{
                                                    res.data.Result[i].isPaidText = 'Belum Lunas';
                                                }  

                                                res.data.Result[i].xWoCreatedDate = $scope.ubahDateToString(res.data.Result[i].WoCreatedDate)
                                                res.data.Result[i].xBillingDate = $scope.ubahDateToString(res.data.Result[i].BillingDate)

                                                if (RId == 1021) { //RId (approval hanya di lakukan oleh ADH) [anita ubah code RId - 20190102]
                                                    console.log("res.data.Result.ApprovalBilling.length", res.data.Result[i].ApprovalBilling.length);

                                                    if (res.data.Result[i].ApprovalBilling.length !== 0) {

                                                        for (var a = 0; a < res.data.Result[i].ApprovalBilling.length; a++) {
                                                            if (res.data.Result[i].ApprovalBilling[a].StatusApprovalId == 3) {
                                                                console.log("hahaha", res.data.Result[i]);

                                                                $scope.grid.data.push(res.data.Result[i]);
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    $scope.grid.data.push(res.data.Result[i]);
                                                }
                                            }

                                            // console.log("First Check", $scope.DataPemeriksaanAwal);
                                            $scope.loading = false;
                                            $scope.formGridApi.core.refresh();
                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                        }
                                    );
                            } else {
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Mohon Input Tanggal WO",
                                    // content: error.join('<br>'),
                                    // number: error.length
                                });
                            }
                        } else {
                            if ($scope.filter.DateStartAfterBill != null && $scope.filter.DateFinishAfterBill != null) {
                                console.log("after billing", firstDateAfterBill);
                                console.log("after biling terakhir", lastDateAfterBill);
                                BillingFactory.getDataAfterCreate(1, firstDateAfterBill, lastDateAfterBill, RId)
                                    .then(
                                        function(res) {
                                            console.log('getDataAfterCreate TipeFilter == !2 ====>',res.data.Result)

                                            // for (var i = 0; i < res.data.Result.length; i++) {
                                            //     if (res.data.Result[i].StatusBilling == 1) {
                                            //         res.data.Result[i].xStatusBilling = "Created";
                                            //     } else if (res.data.Result[i].StatusBilling == 2) {
                                            //         res.data.Result[i].xStatusBilling = "Request Cancel Billing";
                                            //     } else if (res.data.Result[i].StatusBilling == 3) {
                                            //         res.data.Result[i].xStatusBilling = "Reject Request Cancel approval";
                                            //     } else if (res.data.Result[i].StatusBilling == 4) {
                                            //         res.data.Result[i].xStatusBilling = "Approve Request Cancel Billing";
                                            //     } else if (res.data.Result[i].StatusBilling == 5) {
                                            //         res.data.Result[i].xStatusBilling = "Printed";
                                            //     } else if (res.data.Result[i].StatusBilling == 6) {
                                            //         res.data.Result[i].xStatusBilling = "Request Reprint Billing";
                                            //     } else if (res.data.Result[i].StatusBilling == 7) {
                                            //         res.data.Result[i].xStatusBilling = "Reject Request Reprint Billing";
                                            //     } else if (res.data.Result[i].StatusBilling == 8) {
                                            //         res.data.Result[i].xStatusBilling = "Approve Request Reprint Billing";
                                            //     } else if (res.data.Result[i].StatusBilling == 0) {
                                            //         res.data.Result[i].xStatusBilling = "Cancel Without Approval";
                                            //     } else {
                                            //         res.data.Result[i].xStatusBilling = " - ";
                                            //     }
                                            // }
                                            // for (var i = 0; i < res.data.Result.length; i++) {
                                            //     // if (res.data.Result[i].CancelType == 1) {
                                            //     //     res.data.Result[i].xCancelType = "Cancel Faktur Pajak";
                                            //     // } else if (res.data.Result[i].CancelType == 2) {
                                            //     //     res.data.Result[i].xCancelType = "Perubahan Harga";
                                            //     // } else if (res.data.Result[i].CancelType == 3) {
                                            //     //     res.data.Result[i].xCancelType = "Perubahan item jasa dan atau item part";
                                            //     // } else {
                                            //     //     res.data.Result[i].xCancelType = " - ";
                                            //     // }

                                            //     for (var a = 0; a < res.data.Result[i].ApprovalBilling.length; a++) {
                                            //         if (res.data.Result[i].ApprovalBilling[a].StatusApprovalId == 2) {
                                            //             res.data.Result[i].xStatusApprovalId = "Reject";
                                            //         } else if (res.data.Result[i].ApprovalBilling[a].StatusApprovalId == 1) {
                                            //             res.data.Result[i].xStatusApprovalId = "Approved";
                                            //         } else if (res.data.Result[i].ApprovalBilling[a].StatusApprovalId == 3) {
                                            //             res.data.Result[i].xStatusApprovalId = "Request Approval";
                                            //         } else {
                                            //             res.data.Result[i].xStatusApprovalId = " - ";
                                            //         }
                                            //         // console.log("trah",res.data.Result[i]);
                                            //     }
                                            // }

                                            // for (var i = 0; i < res.data.Result.length; i++) {
                                            //     if (res.data.Result[i].ChargeTo == 28) {
                                            //         res.data.Result[i].xChargeTo = "Customer";
                                            //     } else if (res.data.Result[i].ChargeTo == 29) {
                                            //         res.data.Result[i].xChargeTo = "Insurance";
                                            //     } else if (res.data.Result[i].ChargeTo == 30) {
                                            //         res.data.Result[i].xChargeTo = "Waranty";
                                            //     } else if (res.data.Result[i].ChargeTo == 31) {
                                            //         res.data.Result[i].xChargeTo = "Internal";
                                            //     } else if (res.data.Result[i].ChargeTo == 32) {
                                            //         res.data.Result[i].xChargeTo = "Third Party";
                                            //     } else if (res.data.Result[i].ChargeTo == 2277) {
                                            //         res.data.Result[i].xChargeTo = "Free Service";
                                            //     } else if (res.data.Result[i].ChargeTo == 2458) {
                                            //         res.data.Result[i].xChargeTo = "PKS";
                                            //     }

                                            // }
                                            for (var i = 0; i < res.data.Result.length; i++) {
                                                if(res.data.Result[i].ChargeTo == 2277){
                                                    res.data.Result[i].ChargeToId = 2277;
                                                    res.data.Result[i].ChargeToNew = 'Free Service - '+res.data.Result[i].BillTo;
                                                    res.data.Result[i].ChargeTo = 'Free - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 28){
                                                    res.data.Result[i].ChargeToId = 28;
                                                    res.data.Result[i].ChargeToNew = 'Customer - '+res.data.Result[i].BillTo;
                                                    res.data.Result[i].ChargeTo = 'Customer - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 29){
                                                    res.data.Result[i].ChargeToId = 29
                                                    res.data.Result[i].ChargeToNew = 'Insurance - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 32){
                                                    res.data.Result[i].ChargeToId = 32
                                                    res.data.Result[i].ChargeToNew = 'Third Party - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 30){
                                                    res.data.Result[i].ChargeToId = 30;
                                                    // res.data.Result[i].ChargeToNew = 'Warranty - '+res.data.Result[i].BillTo;
                                                    res.data.Result[i].ChargeToNew = 'Warranty - Toyota Astra Motor';
                                                    res.data.Result[i].ChargeTo = 'Warranty - Toyota Astra Motor';
                                                    // res.data.Result[i].ChargeTo = 'Warranty - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 31){
                                                    res.data.Result[i].ChargeToId = 31;
                                                    res.data.Result[i].ChargeToNew = 'Internal - '+res.data.Result[i].BillTo;
                                                    res.data.Result[i].ChargeTo = 'Internal - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 2458){
                                                    res.data.Result[i].ChargeToId = 2458;
                                                    res.data.Result[i].ChargeToNew = 'PKS - '+res.data.Result[i].BillTo;
                                                    res.data.Result[i].ChargeTo = 'PKS - '+res.data.Result[i].BillTo;
                                                }else if(res.data.Result[i].ChargeTo == 29){
                                                    res.data.Result[i].ChargeToId = 29;
                                                    res.data.Result[i].ChargeToNew = 'Insurance - '+res.data.Result[i].BillTo;
                                                    res.data.Result[i].ChargeTo = 'Insurance - '+res.data.Result[i].BillTo;
                                                }
                                                res.data.Result[i].TaskName = res.data.Result[i].Job.TaskName

                                                if(res.data.Result[i].isPaid == 1){
                                                    res.data.Result[i].isPaidText = 'Lunas';
                                                }else{
                                                    res.data.Result[i].isPaidText = 'Belum Lunas';
                                                }  

                                                res.data.Result[i].xWoCreatedDate = $scope.ubahDateToString(res.data.Result[i].WoCreatedDate)
                                                res.data.Result[i].xBillingDate = $scope.ubahDateToString(res.data.Result[i].BillingDate)
                                                if (RId == 1021) { //RId (approval hanya di lakukan oleh ADH) [anita ubah code RId - 20190102]
                                                    console.log("res.data.Result.ApprovalBilling.length", res.data.Result[i].ApprovalBilling.length);
                                                    if (res.data.Result[i].ApprovalBilling.length !== 0) {

                                                        for (var a = 0; a < res.data.Result[i].ApprovalBilling.length; a++) {
                                                            if (res.data.Result[i].ApprovalBilling[a].StatusApprovalId == 3) {
                                                                console.log("hahaha", res.data.Result[i]);

                                                                $scope.grid.data.push(res.data.Result[i]);
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    $scope.grid.data.push(res.data.Result[i]);
                                                }
                                            }
                                            // $scope.DataPemeriksaanAwal = res.data.Result;
                                            // $scope.grid.data = res.data.Result;
                                            // console.log("First Check", $scope.DataPemeriksaanAwal);
                                            $scope.loading = false;
                                            $scope.formGridApi.core.refresh();
                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                        }
                                    );
                            } else {
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Mohon Input Tanggal Billing",
                                    // content: error.join('<br>'),
                                    // number: error.length
                                });
                            }
                        }
                    } else {

                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Mohon Input Filter",
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                    }
                } else {

                    if ($scope.filter.ShowData == 3) {

                        if ($scope.filter.TipeFilterReprint == 1) {

                            if (($scope.filter.DateStartReprintBill != undefined) && ($scope.filter.DateFinishReprintBill != undefined)) {
                                var DS = $scope.filter.DateStartReprintBill;
                                var d = new Date(DS),
                                    month = '' + (d.getMonth() + 1),
                                    day = '' + d.getDate(),
                                    year = d.getFullYear();
                                if (month.length < 2) month = '0' + month;
                                if (day.length < 2) day = '0' + day;
                                var DateStartReprintBill = [year, month, day].join('-');

                                var DE = $scope.filter.DateFinishReprintBill;
                                var d2 = new Date(DE),
                                    month2 = '' + (d2.getMonth() + 1),
                                    day2 = '' + d2.getDate(),
                                    year2 = d2.getFullYear();
                                if (month2.length < 2) month2 = '0' + month2;
                                if (day2.length < 2) day2 = '0' + day2;
                                var DateFinishReprintBill = [year2, month2, day2].join('-');

                                console.log('tgl awal format', DateStartReprintBill);
                                console.log('tgl akhir format', DateFinishReprintBill);

                                BillingFactory.getDataReprintBillingByDate(DateStartReprintBill, DateFinishReprintBill)
                                    .then(
                                        function(res) {
                                            $scope.DataPemeriksaanAwal = res.data.Result;
                                            for (var i = 0; i < res.data.Result.length; i++) {
                                                if (res.data.Result[i].IsAllPulledBilling != 1) {
                                                    var resF = res.data.Result[i];
                                                    // if (res.data.Result[i].Billing.StatusBilling == 1) {
                                                    //     res.data.Result[i].xStatusBilling = "Created";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 2) {
                                                    //     res.data.Result[i].xStatusBilling = "Request Cancel Billing";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 3) {
                                                    //     res.data.Result[i].xStatusBilling = "Reject Request Cancel approval";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 4) {
                                                    //     res.data.Result[i].xStatusBilling = "Approve Request Cancel Billing";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 5) {
                                                    //     res.data.Result[i].xStatusBilling = "Printed";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 6) {
                                                    //     res.data.Result[i].xStatusBilling = "Request Reprint Billing";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 7) {
                                                    //     res.data.Result[i].xStatusBilling = "Reject Request Reprint Billing";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 8) {
                                                    //     res.data.Result[i].xStatusBilling = "Approve Request Reprint Billing";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 0) {
                                                    //     res.data.Result[i].xStatusBilling = "Cancel Without Approval";
                                                    // } else {
                                                    //     res.data.Result[i].xStatusBilling = " - ";
                                                    // }
                                                    if (resF.StatusApprovalId === 1){
                                                        resF.StatusBillingName = 'Approved'
                                                    } else if (resF.StatusApprovalId === 2){
                                                        resF.StatusBillingName = 'Rejected'
                                                    } else if (resF.StatusApprovalId === 3){
                                                        resF.StatusBillingName = 'Request'
                                                    }
                                                    $scope.grid.data.push(resF);
                                                } else {
                                                    console.log("masuk else grid depan");
                                                }
                                            }
                                            console.log("First Check", $scope.DataPemeriksaanAwal);
                                            $scope.loading = false;
                                            $scope.formGridApi.core.refresh();
                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                        }
                                    );

                            } else {
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Mohon Input Filter Tanggal",
                                    // content: error.join('<br>'),
                                    // number: error.length
                                });
                            }

                        } else {

                            if ($scope.filter.NamaFilter !== null && $scope.filter.NamaFilter !== undefined) {
                                BillingFactory.getDataReprintBilling($scope.filter.NamaFilter)
                                    .then(
                                        function(res) {
                                            // $scope.DataPemeriksaanAwal = res.data.Result;
                                            for (var i = 0; i < res.data.Result.length; i++) {
                                                if (res.data.Result[i].IsAllPulledBilling != 1) {
                                                    var resF = res.data.Result[i];
                                                    // if (res.data.Result[i].Billing.StatusBilling == 1) {
                                                    //     res.data.Result[i].xStatusBilling = "Created";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 2) {
                                                    //     res.data.Result[i].xStatusBilling = "Request Cancel Billing";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 3) {
                                                    //     res.data.Result[i].xStatusBilling = "Reject Request Cancel approval";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 4) {
                                                    //     res.data.Result[i].xStatusBilling = "Approve Request Cancel Billing";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 5) {
                                                    //     res.data.Result[i].xStatusBilling = "Printed";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 6) {
                                                    //     res.data.Result[i].xStatusBilling = "Request Reprint Billing";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 7) {
                                                    //     res.data.Result[i].xStatusBilling = "Reject Request Reprint Billing";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 8) {
                                                    //     res.data.Result[i].xStatusBilling = "Approve Request Reprint Billing";
                                                    // } else if (res.data.Result[i].Billing.StatusBilling == 0) {
                                                    //     res.data.Result[i].xStatusBilling = "Cancel Without Approval";
                                                    // } else {
                                                    //     res.data.Result[i].xStatusBilling = " - ";
                                                    // }
                                                    if (resF.StatusApprovalId === 1){
                                                        resF.StatusBillingName = 'Approved'
                                                    } else if (resF.StatusApprovalId === 2){
                                                        resF.StatusBillingName = 'Rejected'
                                                    } else if (resF.StatusApprovalId === 3){
                                                        resF.StatusBillingName = 'Request'
                                                    }
                                                    $scope.grid.data.push(resF);
                                                } else {
                                                    console.log("masuk else grid depan");
                                                }
                                            }
                                            console.log("First Check2", $scope.grid.data);
                                            $scope.loading = false;
                                            $scope.formGridApi.core.refresh();
                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                        }
                                    );
                            } else {

                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Mohon Input Filter Nama",
                                    // content: error.join('<br>'),
                                    // number: error.length
                                });
                            }
                        }
                    }
                }
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Filter",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            }
        };
        
        $scope.DateOptionsStart = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };

        $scope.DateOptionsEnd = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        
        $scope.DateOptionsStart2 = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };

        $scope.DateOptionsEnd2 = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };


        $scope.changeStartDate = function (tgl) {
            var today = new Date();
            if ($scope.filter.ShowData == 1) {
                var dayOne = new Date($scope.filter.DateStart);
        
                $scope.DateOptionsEnd.minDate = new Date(dayOne.getFullYear(), dayOne.getMonth(), dayOne.getDate());
                if (tgl == null || tgl == undefined) {
                    $scope.filter.DateFinish = null;
                } else {
                    if ($scope.filter.DateStart < $scope.filter.DateFinish) {
        
                    } else {
                        $scope.filter.DateFinish = $scope.filter.DateStart;
                    }
                }
            } else if ($scope.filter.ShowData == 2 && $scope.filter.TipeFilter == 1) {
                var dayOne = new Date($scope.filter.DateStartAfterBill);
        
                $scope.DateOptionsEnd2.minDate = new Date(dayOne.getFullYear(), dayOne.getMonth(), dayOne.getDate());
                if (tgl == null || tgl == undefined) {
                    $scope.filter.DateFinishAfterBill = null;
                } else {
                    if ($scope.filter.DateStartAfterBill < $scope.filter.DateFinishAfterBill) {
        
                    } else {
                        $scope.filter.DateFinishAfterBill = $scope.filter.DateStartAfterBill;
                    }
                }
            } else if ($scope.filter.ShowData == 2 && $scope.filter.TipeFilter == 2) {
                var dayOne = new Date($scope.filter.DateStartWo);
        
                $scope.DateOptionsEnd2.minDate = new Date(dayOne.getFullYear(), dayOne.getMonth(), dayOne.getDate());
                if (tgl == null || tgl == undefined) {
                    $scope.filter.DateFinishWo = null;
                } else {
                    if ($scope.filter.DateStartWo < $scope.filter.DateFinishWo) {
        
                    } else {
                        $scope.filter.DateFinishWo = $scope.filter.DateStartWo;
                    }
                }
            }
        
        };

        var selectrowsLc = [];
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
            selectrowsLc = rows;
        }

        $scope.onBeforeNewMode = function() {
            console.log("Before New");
        }

        $scope.onBeforeSave = function() {
            console.log("Before Save");
        }

        $scope.onCheckBeforeSave = function(DataTask, DataParts, DataOPL, DataOPB) {

            var flag = 0;
            if (DataOPL.length > 0) {
                var countStatusOPL = 0;
                _.map(DataOPL, function(val, key) {
                    if (val.Status == 2) {
                        countStatusOPL++;
                    };
                    console.log("countStatusOPL", countStatusOPL);

                    if ((DataOPL.length - 1) == key) {
                        if (countStatusOPL == 0) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "OPL Belum Selesai Dikerjakan"
                            });
                            flag = 1;
                            // return false;
                        }
                        // else {
                        //     console.log("Masuk SINI OPL BOI, BOLEH NGESAVE OPL 1", DataOPL);
                        //     // $scope.rejectSaveBillTO();
                        // };
                    };
                });
            };

            // if (DataTask.length > 0) {
            //     var countStatusTask = 0;
            //     _.map(DataTask, function(val, key) {
            //         if (val.isOpe == 0) {
            //             countStatusTask++;
            //         };
            //         console.log("countStatusTask", countStatusTask);

            //         if ((DataTask.length - 1) == key) {
            //             if (countStatusTask == 0) {
            //                 bsNotify.show({
            //                     size: 'big',
            //                     type: 'danger',
            //                     title: "TASK Belum Selesai Dikerjakan"
            //                 });
            //                 flag = 1;
            //                 // return false;
            //             }
            //             // else {
            //             //     console.log("Masuk SINI OPL BOI, BOLEH NGESAVE TASK 1", DataTask);
            //             //     // $scope.rejectSaveBillTO();
            //             // };
            //         };
            //     });
            // };

            if (flag == 1) {
                return false;
            } else {
                return true;
            };

        }

        var BillingPartsH = [];
        var BillingJob = [];
        $scope.countCreateBilling = 0;
        $scope.onValidateSave = function(data) {
			
			//CR5 #27 stv
			/*console.log("$scope.mBilling.Nationality at onValidateSave: ", $scope.mBilling.Nationality);
			if($scope.mBilling.dataPayment == 28 || $scope.mBilling.dataPayment == 32){
				//Customer / Third Party
				if($scope.mBilling.Nationality.Id == 1){
					//WNI
					if($scope.mBilling.isNPWP == 0){
						//WNI -> NPWP
						if($scope.mBilling.TIN.length == 0 || $scope.mBilling.TIN.length < 15 || $scope.mBilling.TIN == "000000000000000"){
							bsAlert.alert({
								title: "Data NPWP tidak boleh 00.0000.0000.0-000.000 / kosong",
								text: "",
								type: "warning"
							})
						}
						
						if($scope.mBilling.KTPRaw == null || $scope.mBilling.KTPRaw == undefined){
							$scope.mBilling.KTPKITAS = "0000000000000000";
						}else{
							$scope.mBilling.KTPKITAS = $scope.mBilling.KTPRaw;
						}
					}else if($scope.mBilling.isNPWP == 1){
						//WNI -> Tidak punya NPWP
						if($scope.mBilling.KTP.length == 0 || $scope.mBilling.KTP.length < 16 || $scope.mBilling.KTP == "0000000000000000"){
							bsAlert.alert({
								title: "Mohon mengisi nomor KTP dari " & $scope.mBilling.BillTo & " pada kolom NPWP",
								text: "",
								type: "warning"
							})
						}

						$scope.mBilling.TIN = "000000000000000";
						$scope.mBilling.KTPKITAS = $scope.mBilling.KTP;
					}
				}else if($scope.mBilling.Nationality.Id == 2){
					//WNA -> Bebas boleh ga isi?
					$scope.mBilling.TIN = "000000000000000";
					$scope.mBilling.KTPKITAS = $scope.mBilling.passport;
				}

				console.log('masuk');
				if($scope.mBilling.Nationality.Id == 1){
					//WNI
					$scope.mBilling.isWNA = 0;
					console.log('masuk0');
				}else if($scope.mBilling.Nationality.Id == 2){
					//WNA
					console.log('masuk1');
					$scope.mBilling.isWNA = 1;
				}
			}else{
				console.log('masuk2');
				$scope.mBilling.isWNA = null;

				if($scope.mBilling.IsWNARaw == null){
					$scope.mBilling.KTPKITAS = $scope.mBilling.KTPRaw;
				}else if($scope.mBilling.IsWNARaw == 0){
					if($scope.mBilling.KTPRaw == null || $scope.mBilling.KTPRaw == undefined){
						$scope.mBilling.KTPKITAS = "0000000000000000";
					}else{
						$scope.mBilling.KTPKITAS = $scope.mBilling.KTPRaw;
					}
				}else if($scope.mBilling.IsWNARaw == 1){
					if($scope.mBilling.passportRaw == null || $scope.mBilling.passportRaw == undefined){
						$scope.mBilling.KTPKITAS = "0000000000000000";
					}else{
						$scope.mBilling.KTPKITAS = $scope.mBilling.passportRaw;
					}
				}
			}

			console.log("$scope.mBilling.TIN at onValidateSave: ", $scope.mBilling.TIN);
			console.log("$scope.mBilling.KTPKITAS at onValidateSave: ", $scope.mBilling.KTPKITAS);
			console.log("$scope.mBilling.isWNA at onValidateSave: ", $scope.mBilling.isWNA);*/
			//CR5 #27
			
            if ($scope.countCreateBilling != 1){
                $scope.countCreateBilling = 1;
                // BillingParts.push(fJobParts);
                if ($scope.listSelectedRows.length != 0 || $scope.listSelectedRowsParts.length != 0 || $scope.listSelectedRowsOPL.length != 0 || $scope.listSelectedRowsOpb.length != 0) {
                    if ($scope.mBilling.dataPayment == 2458){
                        BillingFactory.getDataPKSByOutletId(data.PoliceNumber).then(function(res) {
                            $scope.dataPKS = res.data.Result;
                            if ($scope.dataPKS.length !== 0) {
                                if ($scope.dataPKS[0].PaymentMethod.toUpperCase() == 'TOP') {
                                    if ($scope.dataPKS[0].OnPeriode == 0) {
                                        // bsNotify.show({
                                        //     size: 'big',
                                        //     type: 'danger',
                                        //     title: "PKS TOP Sudah Lewat Batas Tanggal Periode"
                                        // });

                                        bsAlert.warning("Tagihan ini tidak bisa dibebankan ke PKS dikarenakan masa berlaku PKS untuk kendaraan dengan No. Polisi : " + data.PoliceNumber + " sudah berakhir.", "Silakan hubungi admin service untuk memperpanjang masa berlaku PKS.");
                                        $scope.countCreateBilling = 0;
                                        return false;
                                    } else {
                                        $scope.onValidateSave2(data);
                                    }
                                } else {
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Payment Method harus TOP, silahkan update Master PKS"                                       
                                    });
                                    $scope.countCreateBilling = 0;
                                    return false;
                                }
                            } else {
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Data tidak ditemukan di Master PKS"                                       
                                });
                                $scope.countCreateBilling = 0;
                                return false;
                            }
                        })
                    } else if ($scope.mBilling.dataPayment == 30){
                        // cek kalau ada yg bukan warranty maka ga blh di simpan ======================================================================= start
                        var NotWarranty = 0;
                        for (var i=0; i<$scope.listSelectedRows.length; i++){
                            if ($scope.listSelectedRows[i].PaidById != 30){
                                NotWarranty++;
                            }
                        }
                        for (var i=0; i<$scope.listSelectedRowsOPL.length; i++){
                            if ($scope.listSelectedRowsOPL[i].PaidById != 30){
                                NotWarranty++;
                            }
                        }
                        for (var i=0; i<$scope.listSelectedRowsOpb.length; i++){
                            if ($scope.listSelectedRowsOpb[i].PaidById != 30){
                                NotWarranty++;
                            }
                        }
                        for (var i=0; i<$scope.listSelectedRowsParts.length; i++){
                            if ($scope.listSelectedRowsParts[i].PaidById != 30){
                                NotWarranty++;
                            }
                        }

                        if (NotWarranty === 0){
                            $scope.onValidateSave2(data);
                        } else {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Tidak dapat membuat billing Warranty untuk data yang pembayarannya bukan Warranty"                                       
                            });
                            $scope.countCreateBilling = 0;
                            return false;
                        }

                        // cek kalau ada yg bukan warranty maka ga blh di simpan ======================================================================= end
                    } else {
                        $scope.onValidateSave2(data);
                    }
                    

                } else {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Mohon lengkapi data yang ingin dibuat billing",
                        // content: error.join('<br>'),
                        // number: error.length
                    });
                    $scope.countCreateBilling = 0;

                }
            }


        };

        $scope.onValidateSave2 = function(data) {
            if($scope.cekAsuransiFromMainGrid != null && $scope.cekAsuransiFromJob == 0){
                BillingFactory.CheckJobInsurance(data.JobId, $scope.cekAsuransiFromMainGrid).then(function(resuxz){
                    if (resuxz.data == 1){
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Nama Asuransi Berbeda Dengan WO, Silahkan Refresh Data",
                        });
                        $scope.countCreateBilling = 0;

                    } else {
                        WO.CekStatusJobBilling(data.JobId, 2).then(function(resxx) {
                            if (resxx.data == 1) {
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Tidak bisa Billing karena ada perubahan status. Silahkan Refresh",
                                });
                                $scope.countCreateBilling = 0;

                            } else {
                                var testValidation = $scope.onCheckBeforeSave($scope.listSelectedRows, $scope.listSelectedRowsParts, $scope.listSelectedRowsOPL, $scope.listSelectedRowsOpb);

                                console.log('testValidation', testValidation);

                                if (testValidation) {
                                    $scope.rejectSaveBillTO();
                                    BillingJob = data;
                                    console.log("billing job", BillingJob);
                                    $scope.loading = false;

                                    //kirim notifikasi
                                    var DataNotif = {};
                                    var messagetemp = "";
                                    messagetemp = "No. Polisi : " + BillingJob.PoliceNumber +
                                        " <br> No. WO : " + BillingJob.WoNo;



                                    DataNotif.Message = messagetemp;

                                    console.log('DataNotif ', DataNotif);

                                    if (BillingJob.isGr === 1) {

                                        BillingFactory.sendNotifRole(DataNotif, 1028, 42).then( // SA GR
                                            function(res) {

                                            },
                                            function(err) {
                                                //console.log("err=>", err);
                                            });

                                    } else {

                                        BillingFactory.sendNotifRole(DataNotif, 1029, 42).then( // SA BP
                                            function(res) {

                                            },
                                            function(err) {
                                                //console.log("err=>", err);
                                            });
                                    }


                                } else {
                                    $scope.getData();
                                    $scope.countCreateBilling = 0;
                                    return false;
                                }

                                // if ($scope.listSelectedRowsOPL.length > 0) {
                                //     var countStatusOPL = 0;
                                //     _.map($scope.listSelectedRowsOPL, function(val, key) {
                                //         if (val.Status == 2) {
                                //             countStatusOPL++
                                //         };
                                //         console.log("countStatusOPL", countStatusOPL);

                                //         if (($scope.listSelectedRowsOPL.length - 1) == key) {
                                //             if (countStatusOPL == 0) {
                                //                 bsNotify.show({
                                //                     size: 'big',
                                //                     type: 'danger',
                                //                     title: "OPL Belum Selesai Dikerjakan"
                                //                 });
                                //                 return false;
                                //             } else {
                                //                 console.log("Masuk SINI OPL BOI, BOLEH NGESAVE 1", $scope.listSelectedRowsOPL);
                                //                 // $scope.rejectSaveBillTO();
                                //             };
                                //         };
                                //     });

                                // } else {
                                //     console.log("Masuk SINI OPL BOI, BOLEH NGESAVE 2", $scope.listSelectedRowsOPL);
                                //     // $scope.rejectSaveBillTO();
                                // };

                                // $scope.show_modalDtl = { show: true };
                                // for(var i=0; i < $scope.dataJobTaskId.length; i++){
                                //     var JobTask = {};
                                //     JobTask = $scope.dataJobTaskId[i];
                                //     BillingJob.push(JobTask);
                                //     for(var j = 0; j < $scope.dataJobTaskId[i].JobParts.length; j++){
                                //         var JbPrts = {};
                                //         JbPrts = $scope.dataJobTaskId[i].JobParts[j];
                                //         BillingPartsH.push(JbPrts);
                                //     }
                                // }
                                // BillingJob = data;

                                // // console.log("billing Parts",BillingPartsH);
                                // console.log("billing job", BillingJob);
                                // // console.log("Data Yang Dimpan",data);
                                // $scope.loading = false;

                                // mBilling.BillingParts.push(fJobParts);
                                // console.log("$scope.dataJobTaskId",$scope.dataJobTaskId);
                                // return true
                            }
                        })

                    }
                })
            } else {
                WO.CekStatusJobBilling(data.JobId, 2).then(function(resxx) {
                    if (resxx.data == 1) {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Tidak bisa Billing karena ada perubahan status. Silahkan Refresh",
                        });
                        $scope.countCreateBilling = 0;

                    } else {
                        var testValidation = $scope.onCheckBeforeSave($scope.listSelectedRows, $scope.listSelectedRowsParts, $scope.listSelectedRowsOPL, $scope.listSelectedRowsOpb);

                        console.log('testValidation', testValidation);

                        if (testValidation) {
                            $scope.rejectSaveBillTO();
                            BillingJob = data;
                            console.log("billing job", BillingJob);
                            $scope.loading = false;

                            //kirim notifikasi
                            var DataNotif = {};
                            var messagetemp = "";
                            messagetemp = "No. Polisi : " + BillingJob.PoliceNumber +
                                " <br> No. WO : " + BillingJob.WoNo;



                            DataNotif.Message = messagetemp;

                            console.log('DataNotif ', DataNotif);

                            if (BillingJob.isGr === 1) {

                                BillingFactory.sendNotifRole(DataNotif, 1028, 42).then( // SA GR
                                    function(res) {

                                    },
                                    function(err) {
                                        //console.log("err=>", err);
                                    });

                            } else {

                                BillingFactory.sendNotifRole(DataNotif, 1029, 42).then( // SA BP
                                    function(res) {

                                    },
                                    function(err) {
                                        //console.log("err=>", err);
                                    });
                            }


                        } else {
                            $scope.getData();
                            $scope.countCreateBilling = 0;
                            return false;
                        }

                        // if ($scope.listSelectedRowsOPL.length > 0) {
                        //     var countStatusOPL = 0;
                        //     _.map($scope.listSelectedRowsOPL, function(val, key) {
                        //         if (val.Status == 2) {
                        //             countStatusOPL++
                        //         };
                        //         console.log("countStatusOPL", countStatusOPL);

                        //         if (($scope.listSelectedRowsOPL.length - 1) == key) {
                        //             if (countStatusOPL == 0) {
                        //                 bsNotify.show({
                        //                     size: 'big',
                        //                     type: 'danger',
                        //                     title: "OPL Belum Selesai Dikerjakan"
                        //                 });
                        //                 return false;
                        //             } else {
                        //                 console.log("Masuk SINI OPL BOI, BOLEH NGESAVE 1", $scope.listSelectedRowsOPL);
                        //                 // $scope.rejectSaveBillTO();
                        //             };
                        //         };
                        //     });

                        // } else {
                        //     console.log("Masuk SINI OPL BOI, BOLEH NGESAVE 2", $scope.listSelectedRowsOPL);
                        //     // $scope.rejectSaveBillTO();
                        // };

                        // $scope.show_modalDtl = { show: true };
                        // for(var i=0; i < $scope.dataJobTaskId.length; i++){
                        //     var JobTask = {};
                        //     JobTask = $scope.dataJobTaskId[i];
                        //     BillingJob.push(JobTask);
                        //     for(var j = 0; j < $scope.dataJobTaskId[i].JobParts.length; j++){
                        //         var JbPrts = {};
                        //         JbPrts = $scope.dataJobTaskId[i].JobParts[j];
                        //         BillingPartsH.push(JbPrts);
                        //     }
                        // }
                        // BillingJob = data;

                        // // console.log("billing Parts",BillingPartsH);
                        // console.log("billing job", BillingJob);
                        // // console.log("Data Yang Dimpan",data);
                        // $scope.loading = false;

                        // mBilling.BillingParts.push(fJobParts);
                        // console.log("$scope.dataJobTaskId",$scope.dataJobTaskId);
                        // return true
                    }
                })

            }
        }

        $scope.onPaidByContains = function(target, pattern) {
            var value = 0;
            pattern.forEach(function(word) {
                value = value + target.includes(word);
            });
            return (value === 1);
        }

        // Fungsi menyederhakan array object biar ga dipanggil berulang-ulang
        $scope.onPushGridWork = function(paramAction, isCustomDisc, approvalId, dataJobs, dataJobTask, dataJobSPK, dataBillingJob, dataBillingJobArr, normalPrice, discountedPrice, summaryy) {
            console.log('onPushGridWork | dataJobTask ======>',dataJobTask)
            console.log('onPushGridWork | dataJobSPK =======>',dataJobSPK)
            console.log('onPushGridWork | dataBillingJob ===>',dataBillingJob)
   

            var gridWorkBilling = [];
            if (paramAction == 0) { // Before Edit
                if (isCustomDisc == 0) {
                    $scope.gridWork.push({
                        ActualRate: dataJobTask.ActualRate,
                        AdditionalTaskId: dataJobTask.AdditionalTaskId,
                        isOpe: dataJobTask.isOpe,
                        Discount: dataJobTask.Discount,
                        JobTaskId: dataJobTask.JobTaskId,
                        JobTypeId: dataJobTask.JobTypeId,
                        JobTypeName: dataJobTask.JobType.Name,
                        DiscountedPrice: discountedPrice,
                        IsBilling: dataJobTask.IsBilling,
                        typeDiskon: dataJobTask.typeDiskon,
                        NormalPrice: normalPrice,
                        // catName: $scope.dataByJobId[i].JobTask[j].JobType.Name,
                        catName: dataJobTask.Process != undefined || dataJobTask.Process != null ? dataJobs.dataJobTask.Process.Name : "",
                        Fare: dataJobTask.Fare,
                        IsCustomDiscount: dataJobTask.IsCustomDiscount,
                        Summary: summaryy,
                        PaidById: dataJobTask.PaidById,
                        JobId: dataJobTask.JobId,
                        TaskId: dataJobTask.TaskId,
                        TaskName: dataJobTask.TaskName,
                        FlatRate: dataJobTask.FlatRate,
                        ProcessId: dataJobTask.ProcessId,
                        TFirst1: dataJobTask.TFirst1,
                        PaidBy: dataJobTask.PaidBy.Name,
                        DiscountJasa: dataJobs.Insurance != undefined || dataJobs.Insurance != null ? dataJobs.Insurance.DiscountJasa : 0,
                        SpkNo: dataJobSPK == undefined || dataJobSPK == null ? '' : dataJobSPK.SpkNo,
                        ORAmount: dataJobSPK == undefined || dataJobSPK == null ? 0 : dataJobSPK.ORAmount,
                        SpkHide: dataJobSPK == undefined || dataJobSPK == null ? true : false
                    });
                }

                if (isCustomDisc == 1) {
                    if (approvalId == -1) {
                        $scope.gridWork.push({
                            ActualRate: dataJobTask.ActualRate,
                            AdditionalTaskId: dataJobTask.AdditionalTaskId,
                            isOpe: dataJobTask.isOpe,
                            Discount: dataJobTask.Discount,
                            JobTaskId: dataJobTask.JobTaskId,
                            JobTypeId: dataJobTask.JobTypeId,
                            JobTypeName: dataJobTask.JobType.Name,
                            DiscountedPrice: discountedPrice,
                            IsBilling: dataJobTask.IsBilling,
                            typeDiskon: dataJobTask.typeDiskon,
                            NormalPrice: normalPrice,
                            // catName: $scope.dataByJobId[i].JobTask[j].JobType.Name,
                            catName: dataJobTask.Process != undefined || dataJobTask.Process != null ? dataJobs.dataJobTask.Process.Name : "",
                            Fare: dataJobTask.Fare,
                            IsCustomDiscount: dataJobTask.IsCustomDiscount,
                            Summary: summaryy,
                            PaidById: dataJobTask.PaidById,
                            JobId: dataJobTask.JobId,
                            TaskId: dataJobTask.TaskId,
                            TaskName: dataJobTask.TaskName,
                            FlatRate: dataJobTask.FlatRate,
                            ProcessId: dataJobTask.ProcessId,
                            TFirst1: dataJobTask.TFirst1,
                            PaidBy: dataJobTask.PaidBy.Name,
                            DiscountJasa: dataJobs.Insurance != undefined || dataJobs.Insurance != null ? dataJobs.Insurance.DiscountJasa : 0,
                            SpkNo: dataJobSPK == undefined || dataJobSPK == null ? '' : dataJobSPK.SpkNo,
                            ORAmount: dataJobSPK == undefined || dataJobSPK == null ? 0 : dataJobSPK.ORAmount,
                            SpkHide: dataJobSPK == undefined || dataJobSPK == null ? true : false
                        });
                    }

                    if (approvalId == 1 || approvalId == 2 || approvalId == 3) {
                        $scope.gridWork.push({
                            ActualRate: dataJobTask.ActualRate,
                            AdditionalTaskId: dataJobTask.AdditionalTaskId,
                            isOpe: dataJobTask.isOpe,
                            Discount: (dataJobTask.ApprovalDiscountTransTR[0].StatusApprovalId == 1 ? dataJobTask.Discount : 0),
                            JobTaskId: dataJobTask.JobTaskId,
                            JobTypeId: dataJobTask.JobTypeId,
                            JobTypeName: dataJobTask.JobType.Name,
                            DiscountedPrice: (dataJobTask.ApprovalDiscountTransTR[0].StatusApprovalId == 1 ? discountedPrice : 0),
                            IsBilling: dataJobTask.IsBilling,
                            typeDiskon: dataJobTask.typeDiskon,
                            NormalPrice: normalPrice,
                            // catName: $scope.dataByJobId[i].JobTask[j].JobType.Name,
                            catName: dataJobTask.Process != undefined || dataJobTask.Process != null ? dataJobs.dataJobTask.Process.Name : "",
                            Fare: dataJobTask.Fare,
                            IsCustomDiscount: dataJobTask.IsCustomDiscount,
                            Summary: summaryy,
                            PaidById: dataJobTask.PaidById,
                            JobId: dataJobTask.JobId,
                            TaskId: dataJobTask.TaskId,
                            TaskName: dataJobTask.TaskName,
                            FlatRate: dataJobTask.FlatRate,
                            ProcessId: dataJobTask.ProcessId,
                            TFirst1: dataJobTask.TFirst1,
                            PaidBy: dataJobTask.PaidBy.Name,
                            DiscountJasa: dataJobs.Insurance != undefined || dataJobs.Insurance != null ? dataJobs.Insurance.DiscountJasa : 0,
                            SpkNo: dataJobSPK == undefined || dataJobSPK == null ? '' : dataJobSPK.SpkNo,
                            ORAmount: dataJobSPK == undefined || dataJobSPK == null ? 0 : dataJobSPK.ORAmount,
                            SpkHide: dataJobSPK == undefined || dataJobSPK == null ? true : false
                        });
                    }
                }
            } else if (paramAction == 1) { // ShowDetail
                if (isCustomDisc == 0) {
                    $scope.gridWork.push({
                        ActualRate: dataJobTask.ActualRate,
                        AdditionalTaskId: dataJobTask.AdditionalTaskId,
                        isOpe: dataJobTask.isOpe,
                        Discount: dataJobTask.Discount,
                        JobTaskId: dataJobTask.JobTaskId,
                        JobTypeId: dataJobTask.JobTypeId,
                        JobTypeName: dataJobTask.JobType.Name,
                        DiscountedPrice: discountedPrice,
                        IsBilling: dataJobTask.IsBilling,
                        typeDiskon: dataJobTask.typeDiskon,
                        NormalPrice: normalPrice,
                        // catName: data.JobTask[j].JobType.Name,
                        catName: (dataBillingJob != null || dataBillingJob != undefined ? dataBillingJobArr.JobTask.satuanName : ""),
                        Fare: dataJobTask.Fare,
                        IsCustomDiscount: dataJobTask.IsCustomDiscount,
                        Summary: summaryy,
                        PaidById: dataJobTask.PaidById,
                        JobId: dataJobTask.JobId,
                        TaskId: dataJobTask.TaskId,
                        TaskName: dataJobTask.TaskName,
                        FlatRate: dataJobTask.FlatRate,
                        ProcessId: (dataBillingJob != null || dataBillingJob != undefined ? dataBillingJobArr.JobTask.ProcessId : dataJobTask.ProcessId),
                        TFirst1: dataJobTask.TFirst1,
                        PaidBy: dataJobTask.PaidBy.Name,
                        DiscountJasa: dataJobs.Insurance != undefined || dataJobs.Insurance != null ? dataJobs.Insurance.DiscountJasa : 0,
                        SpkNo: dataJobSPK == undefined || dataJobSPK == null ? '' : dataJobSPK.SpkNo,
                        ORAmount: dataJobSPK == undefined || dataJobSPK == null ? 0 : dataJobSPK.ORAmount,
                        SpkHide: dataJobSPK == undefined || dataJobSPK == null ? true : false
                    });
                }
                if (isCustomDisc == 1) {
                    if (approvalId == -1) {
                        $scope.gridWork.push({
                            ActualRate: dataJobTask.ActualRate,
                            AdditionalTaskId: dataJobTask.AdditionalTaskId,
                            isOpe: dataJobTask.isOpe,
                            Discount: dataJobTask.Discount,
                            JobTaskId: dataJobTask.JobTaskId,
                            JobTypeId: dataJobTask.JobTypeId,
                            JobTypeName: dataJobTask.JobType.Name,
                            DiscountedPrice: discountedPrice,
                            IsBilling: dataJobTask.IsBilling,
                            typeDiskon: dataJobTask.typeDiskon,
                            NormalPrice: normalPrice,
                            // catName: data.JobTask[j].JobType.Name,
                            catName: (dataBillingJob != null || dataBillingJob != undefined ? dataBillingJobArr.JobTask.satuanName : ""),
                            Fare: dataJobTask.Fare,
                            IsCustomDiscount: dataJobTask.IsCustomDiscount,
                            Summary: summaryy,
                            PaidById: dataJobTask.PaidById,
                            JobId: dataJobTask.JobId,
                            TaskId: dataJobTask.TaskId,
                            TaskName: dataJobTask.TaskName,
                            FlatRate: dataJobTask.FlatRate,
                            ProcessId: (dataBillingJob != null || dataBillingJob != undefined ? dataBillingJobArr.JobTask.ProcessId : dataJobTask.ProcessId),
                            TFirst1: dataJobTask.TFirst1,
                            PaidBy: dataJobTask.PaidBy.Name,
                            DiscountJasa: dataJobs.Insurance != undefined || dataJobs.Insurance != null ? dataJobs.Insurance.DiscountJasa : 0,
                            SpkNo: dataJobSPK == undefined || dataJobSPK == null ? '' : dataJobSPK.SpkNo,
                            ORAmount: dataJobSPK == undefined || dataJobSPK == null ? 0 : dataJobSPK.ORAmount,
                            SpkHide: dataJobSPK == undefined || dataJobSPK == null ? true : false
                        });
                    }
                    if (approvalId == 1 || approvalId == 2 || approvalId == 3) {
                        $scope.gridWork.push({
                            ActualRate: dataJobTask.ActualRate,
                            AdditionalTaskId: dataJobTask.AdditionalTaskId,
                            isOpe: dataJobTask.isOpe,
                            Discount: (dataJobTask.ApprovalDiscountTransTR[0].StatusApprovalId == 1 ? dataJobTask.Discount : 0),
                            JobTaskId: dataJobTask.JobTaskId,
                            JobTypeId: dataJobTask.JobTypeId,
                            JobTypeName: dataJobTask.JobType.Name,
                            DiscountedPrice: (dataJobTask.ApprovalDiscountTransTR[0].StatusApprovalId == 1 ? discountedPrice : 0),
                            IsBilling: dataJobTask.IsBilling,
                            typeDiskon: dataJobTask.typeDiskon,
                            NormalPrice: normalPrice,
                            // catName: data.JobTask[j].JobType.Name,
                            catName: (dataBillingJob != null || dataBillingJob != undefined ? dataBillingJob.JobTask.satuanName : ""),
                            Fare: dataJobTask.Fare,
                            IsCustomDiscount: dataJobTask.IsCustomDiscount,
                            Summary: summaryy,
                            PaidById: dataJobTask.PaidById,
                            JobId: dataJobTask.JobId,
                            TaskId: dataJobTask.TaskId,
                            TaskName: dataJobTask.TaskName,
                            FlatRate: dataJobTask.FlatRate,
                            ProcessId: (dataBillingJob != null || dataBillingJob != undefined ? dataBillingJobArr.JobTask.ProcessId : dataJobTask.ProcessId),
                            TFirst1: dataJobTask.TFirst1,
                            PaidBy: dataJobTask.PaidBy.Name,
                            DiscountJasa: dataJobs.Insurance != undefined || dataJobs.Insurance != null ? dataJobs.Insurance.DiscountJasa : 0,
                            SpkNo: dataJobSPK == undefined || dataJobSPK == null ? '' : dataJobSPK.SpkNo,
                            ORAmount: dataJobSPK == undefined || dataJobSPK == null ? 0 : dataJobSPK.ORAmount,
                            SpkHide: dataJobSPK == undefined || dataJobSPK == null ? true : false
                        });
                    }
                }
            }
        }

        // $scope.loading = false;
        $scope.gridWork = [];
        $scope.partsData = [];
        $scope.oplData = [];
        $scope.datalistSelectedRows = [];


        $scope.fixDate = function (date) {
            if (date != null || date != undefined) {
                var fix = 
                    ('0' + date.getDate()).slice(-2) + "/"  + 
                    ('0' + (date.getMonth() + 1)).slice(-2) + "/" +
                    date.getFullYear() ;
                return fix;
            } else {
                return null;
            }
        };

        // $scope.gridParts = [];
        $scope.onBeforeEdit = function(data) {
            console.log("onBeforeEdit | kucluk", data);


            console.log("onBeforeEdit | data", data);
            console.log("onBeforeEdit | mBilling", $scope.mBilling);

            tmp_mBillingDataPayment = $scope.mBilling.dataPayment;

            console.log("onBeforeEdit | VehicleId", data.VehicleId);
            console.log("onBeforeEdit | policeNumber", data.PoliceNumber);
            $scope.bilingId = data.BillingId;
            $scope.choseData = data.TypeJob;
            console.log('onBeforeEdit | chosedata--------', $scope.choseData);
            $scope.disMode = false;
            $scope.dataInsurance = [];
            $scope.getPPN()

            BillingFactory.RecalculateOPLTeknisi(data.JobId).then(function(result) {
                console.log("berhasil update OPLTeknisi");
            });
            
            // $scope.subTotalLabor = 0;
            // $scope.discountFinalTask = 0;
            // $scope.subTotalOpb = 0;
            // $scope.subTotalOpl = 0;
            // $scope.discountFinalParts = 0;
            // $scope.discountFinalPartsOpb = 0;
            // TotalOpl = 0;
            // TotalParts = 0;
            // TotalOpb = 0;
            // TotalFinal = 0;
            // TotalAfterDiscount = 0;

            $scope.TotalInvoice = 0;
            $scope.PPN = 0;
            $scope.TotalYangHarusDibayar = 0;

            $scope.subTotalLabor = 0;
            $scope.subTotalParts = 0;
            $scope.discountPercentFinalTask = 0;
            $scope.discountFinalTask = 0;
            $scope.subTotalOpb = 0;
            $scope.subTotalOpl = 0;
            $scope.discountPercentFinalParts = 0;
            $scope.discountFinalParts = 0;
            $scope.discountFinalPartsOpb = 0;
            $scope.discountPercentFinalOpb = 0;
            $scope.TotalAfterDiscountFinal = 0;
            $scope.discountPercentFinalOpl = 0;
            $scope.discountFinalOpl = 0;
            $scope.tax = 0;
            $scope.Materai = 0;
            $scope.DP = 0;
            $scope.DPPaid = 0;
            $scope.TotalFinal = 0;
            $scope.discountOPl = 0;
            $scope.listSelectedRows = [];
            $scope.listSelectedRowsParts = [];
            $scope.listSelectedRowsOPL = [];
            $scope.listSelectedRowsOpb = [];
            $scope.listApiJob.clearSelected();
            $scope.listApiParts.clearSelected();
            $scope.listApiOpl.clearSelected();
            $scope.listApiOpb.clearSelected();

            var TotalOpl = 0;
            // var TotalParts = 0;
            var TotalOpb = 0;
            var TotalFinal = 0;
            var TotalAfterDiscount = 0;

            var tempInsuranceId = 0;

            if (data.InsuranceId != undefined || data.InsuranceId != null) {
                tempInsuranceId = data.InsuranceId;
            }

            if (data.Job != undefined || data.Job != null) {
                tempInsuranceId = data.Job.InsuranceId;
            }

            console.log("onBeforeEdit | data Insurance before", tempInsuranceId);

            BillingFactory.getInsuranceProfile(tempInsuranceId).then(function(resu) {
                $scope.dataInsurance = resu.data.Result;
                console.log("onBeforeEdit | data Insurance after", $scope.dataInsurance);
            });

            BillingFactory.getCOutletById2().then(function(resu) {
                $scope.dataOutlet = resu.data.Result;
            });

            BillingFactory.getPayment()
                .then(
                    function(res) {
                        // $scope.DataPemeriksaanAwal = res.data.Result;
                        if (data.isGr == 1) {
                            var tmpPayment = [];
                            _.map(res.data.Result, function(val) {
                                if (val.Name !== 'Insurance') {
                                    tmpPayment.push(val);
                                }
                            })
                            $scope.dataPayment = tmpPayment;
                        } else {
                            $scope.dataPayment = res.data.Result;
                        }
                        // console.log("onBeforeEdit | data CRM", $scope.dataCRM);
                        // console.log("onBeforeEdit | First Check", $scope.DataPemeriksaanAwal);
                        // $scope.loading=false;
                    },
                    function(err) {
                        console.log("onBeforeEdit | err=>", err);
                    }
                );


            BillingFactory.CleansingMaterialRequestCompleteGI(data.JobId).then(function(result) {
                console.log("berhasil update CleansingMaterialRequestCompleteGI");

                BillingFactory.getDataCRM(data.VehicleId).then(function(res) {
                    
                        // $scope.DataPemeriksaanAwal = res.data.Result;
                        $scope.dataCRM = res.data.Result;
                        console.log("onBeforeEdit | data CRM", $scope.dataCRM);

                        // $scope.mBilling.BillTo = $scope.dataCRM[0].Name;
                        // $scope.mBilling.BillAddress = $scope.dataCRM[0].Address;
                        // $scope.mBilling.ToyotaId = $scope.dataCRM[0].ToyotaId != null ? $scope.dataCRM[0].ToyotaId : '-';
                        // $scope.mBilling.TIN = $scope.dataCRM[0].Npwp;
                        // $scope.mBilling.TaxName = $scope.dataCRM[0].Name;
                        // $scope.mBilling.TaxAddress = $scope.dataCRM[0].Address;
                        // $scope.mBilling.BillPhoneNumber = $scope.dataCRM[0].PhoneNumber;

                        if ($scope.dataCRM.length > 0) {
                            
                            // CR5 #27 CRM Edit
                            $scope.mBilling.CustomerId = $scope.dataCRM[0].CustomerId
                            $scope.mBilling.TINRaw = $scope.dataCRM[0].Npwp;

                            $scope.mBilling.IsWNARaw = $scope.dataCRM[0].IsWNA;

                            if($scope.dataCRM[0].IsWNA == null){
                                delete $scope.mBilling.Nationality;
                                console.log('$scope.mBilling.Nationality after delete at CRM: ', $scope.mBilling.Nationality);

                                $scope.mBilling.Nationality = undefined;
                                console.log('$scope.mBilling.Nationality after set at CRM: ', $scope.mBilling.Nationality);

                                $scope.mBilling.KTP = $scope.dataCRM[0].KTPKITAS;
                                $scope.mBilling.KTPRaw = $scope.dataCRM[0].KTPKITAS;

                                $scope.mBilling.passport = $scope.dataCRM[0].KTPKITAS;
                                $scope.mBilling.passportRaw = $scope.dataCRM[0].KTPKITAS;
                            }else if($scope.dataCRM[0].IsWNA == 0){
                                //WNI
                                $scope.mBilling.Nationality = { Id: 0, Name: 'WNI' };

                                $scope.mBilling.passport = '0000000000000000';
                                $scope.mBilling.passportRaw = '0000000000000000';

                                if($scope.dataCRM[0].Npwp == null || $scope.dataCRM[0].Npwp == '00.000.000.0-000.000' || $scope.dataCRM[0].Npwp == undefined){
                                    // WNI -> Tidak punya NPWP
                                    //$scope.mBilling.isNPWP = 1;

                                    $scope.mBilling.KTP = $scope.dataCRM[0].KTPKITAS;
                                    $scope.mBilling.KTPRaw = $scope.dataCRM[0].KTPKITAS;
                                }else{
                                    // WNI -> NPWP
                                    //$scope.mBilling.isNPWP = 0;

                                    if($scope.dataCRM[0].KTPKITAS == null || $scope.dataCRM[0].KTPKITAS == '0000000000000000' || $scope.dataCRM[0].KTPKITAS == undefined){
                                        $scope.mBilling.KTP = '0000000000000000';
                                        $scope.mBilling.KTPRaw = '0000000000000000';
                                    }else{
                                        $scope.mBilling.KTP = $scope.dataCRM[0].KTPKITAS;
                                        $scope.mBilling.KTPRaw = $scope.dataCRM[0].KTPKITAS;
                                    }
                                }
                            }else if($scope.dataCRM[0].IsWNA == 1){
                                //WNA
                                $scope.mBilling.Nationality = { Id: 1, Name: 'WNA' };

                                $scope.mBilling.KTP = '0000000000000000';
                                $scope.mBilling.KTPRaw = '0000000000000000';

                                //$scope.mBilling.isNPWP = 1;

                                $scope.mBilling.passport = $scope.dataCRM[0].KTPKITAS;
                                $scope.mBilling.passportRaw = $scope.dataCRM[0].KTPKITAS;							
                            }

                            console.log('$scope.mBilling.Nationality at onBeforeEdit', $scope.mBilling.Nationality);
                            console.log('$scope.mBilling.KTP at onBeforeEdit', $scope.mBilling.KTP);
                            console.log('$scope.mBilling.passport at onBeforeEdit', $scope.mBilling.passport);

                            console.log('$scope.mBilling.KTP Awal at onBeforeEdit', $scope.mBilling.KTPRaw);
                            console.log('$scope.mBilling.passport Awal at onBeforeEdit', $scope.mBilling.passportRaw);

                            console.log('$scope.mBilling.TIN Awal at onBeforeEdit', $scope.mBilling.TINRaw);
                            // CR5 #27 //
                        
                            $scope.mBilling.BillTo = $scope.dataCRM[0].Name;
                            $scope.mBilling.BillAddress = $scope.dataCRM[0].Address;
                            $scope.mBilling.ToyotaId = $scope.dataCRM[0].ToyotaId != null ? $scope.dataCRM[0].ToyotaId : '-';
                            $scope.mBilling.TIN = $scope.dataCRM[0].Npwp;
                            $scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
                            $scope.mBilling.TaxName = $scope.dataCRM[0].Name;
                            $scope.mBilling.TaxAddress = $scope.dataCRM[0].Address;
                            $scope.mBilling.BillPhoneNumber = $scope.dataCRM[0].PhoneNumber;

                            BillingFactory.getListOutlet(null, 2).then(function(resu) {
                                $scope.freeServiceData = resu.data.Result;
                            });
                            $scope.getPPN($scope.mBilling.TIN)
                        }
                        // console.log("onBeforeEdit | First Check", $scope.DataPemeriksaanAwal);
                        // $scope.loading=false;


                        BillingFactory.getDataPKSByOutletId(data.PoliceNumber).then(function(res) {
                                var dataPKS = res.data.Result;
                                console.log('onBeforeEdit | Data PKS ', dataPKS);

                                $scope.default_pks = []
                                if (dataPKS.length > 0) {
                                    $scope.default_pks = dataPKS
                                }

            
                                $scope.NoPKS = "";
                                $scope.NamaPKS = "";
                                $scope.TOP = "";
                                $scope.Outstanding = "";
            
                                if (dataPKS != undefined || dataPKS != null) {
                                    if (dataPKS.length > 0) {
                                        $scope.NoPKS = dataPKS[0].NoPKS;
                                        $scope.NamaPKS = dataPKS[0].CompanyName;
                                        $scope.TOP = dataPKS[0].CustomerPKSTop;
                                        $scope.Outstanding = dataPKS[0].AvailableBalanceBilling;
                                        //$scope.Outstanding = dataPKS[0].AvailableBalance;
                                    }
                                }
                            },
                            function(err) {
                                console.log("onBeforeEdit | err=>", err);
                            }
            
                        );
            
                        BillingFactory.getClaim(data.JobId).then(function(res) {
                                var dataAsuransi = res.data.Result;
                                console.log('onBeforeEdit | Data dataAsuransi ', dataAsuransi);
            
                                $scope.PolicyNo = "";
                                $scope.StartPeriod = "";
                                $scope.EndPeriod = "";
            
                                if (dataAsuransi != undefined || dataAsuransi != null) {
                                    if (dataAsuransi.length > 0) {
                                        $scope.PolicyNo = dataAsuransi[0].PolicyNo;
                                        $scope.StartPeriod = dataAsuransi[0].StartPeriod;
                                        $scope.EndPeriod = dataAsuransi[0].EndPeriod;
                                    }
                                }
            
                            },
                            function(err) {
                                console.log("onBeforeEdit | err=>", err);
                            }
                        );
                        // BillingFactory.getDataJobTask(data.JobTask.JobTaskId)
                        // .then(
                        //     function(res){
                        //         // $scope.DataPemeriksaanAwal = res.data.Result;
                        //         $scope.dataJobTaskId = res.data;
                        //         $scope.gridWork = $scope.dataJobTaskId;
                        //         console.log("onBeforeEdit | data by job task id", $scope.dataJobTaskId);
                        //         for(var i = 0; i < $scope.dataJobTaskId.length; i++){
                        //             // var JobParts = $scope.dataJobTaskId[i]
                        //             for(var j = 0; j < $scope.dataJobTaskId[i].JobParts.length; j++){
                        //                 fJobParts = $scope.dataJobTaskId[i].JobParts[j];
                        //             }
                        //         console.log("onBeforeEdit | Job Parts",fJobParts);
                        //         $scope.gridParts = fJobParts;
                        //         }
                        //         console.log("onBeforeEdit | grid parts", $scope.gridParts);
            
                        //         // console.log("onBeforeEdit | First Check", $scope.DataPemeriksaanAwal);
                        //         // $scope.loading=false;
                        //     },
                        //     function(err){
                        //         console.log("onBeforeEdit | err=>",err);
                        //     }
                        // );
            
                        BillingFactory.getDataEmployeeSa(data.JobId).then(function(res) {
                                $scope.cekAsuransiFromJob = angular.copy(res.data.Result[0].isCash);
                                $scope.temp_dataJobList = angular.copy(res.data.Result[0])
                                $scope.getTypeJobFreeClaim = 0;
                                
                                console.log("onBeforeEdit | datanya 2 =>", res);
                                $scope.gridWork = [];
                                $scope.partsData = [];
                                $scope.oplData = [];
                                $scope.opbData = [];
                                // $scope.DataPemeriksaanAwal = res.data.Result;
                                $scope.dataByJobId = res.data.Result;
            
                                $scope.checkJobBelumBilling = 0;
                                $scope.checkJobOPLBelumBilling = 0;
                                $scope.checkPartsBelumBilling = 0;
                                $scope.checkJobOPLBPBelumBilling = 0;
            
                                $scope.BukanInsurance = 0;
            
                                console.log('onBeforeEdit | AEstimationBPWOUsed ====>', $scope.dataByJobId[0].AEstimationBPWOUsed)
                                if($scope.dataByJobId[0].AEstimationBPWOUsed.length > 0){
                                    $scope.StartPeriod = $scope.fixDate($scope.dataByJobId[0].AEstimationBPWOUsed[0].PolisDateFrom);
                                    $scope.EndPeriod = $scope.fixDate($scope.dataByJobId[0].AEstimationBPWOUsed[0].PolisDateTo);
                                    $scope.PolicyNo = $scope.dataByJobId[0].AEstimationBPWOUsed[0].NoPolis;
                                    
                                }
            
                                // $scope.mBilling.dataPayment = $scope.dataByJobId[0].JobTask[0].PaidById;
            
                                // cek buat default pembayaran======================================================= start
                                if ($scope.dataByJobId[0].JobTask.length > 0){
                                    for (var i=0; i<$scope.dataByJobId[0].JobTask.length; i++){
                                        if ($scope.dataByJobId[0].JobTask[i].IsBilling != 1 && $scope.dataByJobId[0].JobTask[i].isDeleted != 1){
                                            $scope.dataJobBelumBilling = $scope.dataByJobId[0].JobTask[i].PaidById;
                                            $scope.checkJobBelumBilling = 1;
                                            break;
                                        }
                                    }
                                }
            
                                if ($scope.dataByJobId[0].JobTaskOpl.length > 0){
                                    for (var i=0; i<$scope.dataByJobId[0].JobTaskOpl.length; i++){
                                        if ($scope.dataByJobId[0].JobTaskOpl[i].IsBilling != 1 && $scope.dataByJobId[0].JobTaskOpl[i].isDeleted != 1){
                                            $scope.dataJobOPLBelumBilling = $scope.dataByJobId[0].JobTaskOpl[i].PaidById;
                                            $scope.checkJobOPLBelumBilling = 1;
                                            break;
                                        }
                                    }
                                }
            
                                if ($scope.dataByJobId[0].JobTask.length > 0){
                                    for (var i=0; i<$scope.dataByJobId[0].JobTask.length; i++){
                                        if ($scope.dataByJobId[0].JobTask[i].isDeleted != 1){
                                            if ($scope.dataByJobId[0].JobTask[i].JobParts.length > 0){
                                                for (var j=0; j<$scope.dataByJobId[0].JobTask[i].JobParts.length; j++){
                                                    if ($scope.dataByJobId[0].JobTask[i].JobParts[j].IsBilling != 1 && $scope.dataByJobId[0].JobTask[i].JobParts[j].isDeleted != 1){
                                                        $scope.dataPartsBelumBilling = $scope.dataByJobId[0].JobTask[i].JobParts[j].PaidById;
                                                        $scope.checkPartsBelumBilling = 1;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
            
                                if ($scope.dataByJobId[0].JobTaskOplBP.length > 0){
                                    for (var i=0; i<$scope.dataByJobId[0].JobTaskOplBP.length; i++){
                                        if ($scope.dataByJobId[0].JobTaskOplBP[i].IsBilling != 1 && $scope.dataByJobId[0].JobTaskOplBP[i].isDeleted != 1){
                                            $scope.dataJobOPLBPBelumBilling = $scope.dataByJobId[0].JobTaskOplBP[i].PaidById;
                                            $scope.checkJobOPLBPBelumBilling = 1;
                                            break;
                                        }
                                    }
                                }
                                // cek buat default pembayaran======================================================= end
            
                                // cek buat beda pembayaran======================================================= start MasterId: 29 insurance
                                if ($scope.dataByJobId[0].JobTask.length > 0){
                                    for (var i=0; i<$scope.dataByJobId[0].JobTask.length; i++){
                                        if ($scope.dataByJobId[0].JobTask[i].IsBilling != 1 && $scope.dataByJobId[0].JobTask[i].isDeleted != 1){
                                            if ($scope.dataByJobId[0].JobTask[i].PaidById != 29){
                                                $scope.BukanInsurance = 1;
                                                break;
                                            }
                                        }
                                    }
                                }
            
                                if ($scope.dataByJobId[0].JobTaskOpl.length > 0){
                                    for (var i=0; i<$scope.dataByJobId[0].JobTaskOpl.length; i++){
                                        if ($scope.dataByJobId[0].JobTaskOpl[i].IsBilling != 1 && $scope.dataByJobId[0].JobTaskOpl[i].isDeleted != 1){
                                            if ($scope.dataByJobId[0].JobTaskOpl[i].PaidById != 29){
                                                $scope.BukanInsurance = 1;
                                                break;
                                            }
                                        }
                                    }
                                }
            
                                if ($scope.dataByJobId[0].JobTask.length > 0){
                                    for (var i=0; i<$scope.dataByJobId[0].JobTask.length; i++){
                                        if ($scope.dataByJobId[0].JobTask[i].JobParts.length > 0){
                                            for (var j=0; j<$scope.dataByJobId[0].JobTask[i].JobParts.length; j++){
                                                if ($scope.dataByJobId[0].JobTask[i].JobParts[j].IsBilling != 1 && $scope.dataByJobId[0].JobTask[i].JobParts[j].isDeleted != 1){
                                                    if ($scope.dataByJobId[0].JobTask[i].JobParts[j].PaidById != 29){
                                                        $scope.BukanInsurance = 1;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
            
                                if ($scope.dataByJobId[0].JobTaskOplBP.length > 0){
                                    for (var i=0; i<$scope.dataByJobId[0].JobTaskOplBP.length; i++){
                                        if ($scope.dataByJobId[0].JobTaskOplBP[i].IsBilling != 1 && $scope.dataByJobId[0].JobTaskOplBP[i].isDeleted != 1){
                                            if ($scope.dataByJobId[0].JobTaskOplBP[i].PaidById != 29){
                                                $scope.BukanInsurance = 1;
                                                break;
                                            }
                                        }
                                    }
                                }
                                // cek buat beda pembayaran======================================================= end
            
            
                                
            
                                // Tgl. Billing
                                $scope.date = new Date();
            
                                for (i = 0; i < $scope.dataByJobId.length; i++) {
                                    console.log("onBeforeEdit | invoiceType", $scope.dataByJobId[i].invoiceType);
                                    console.log("onBeforeEdit | invoiceType", $scope.dataByJobId[i]);
            
                                    if ($scope.dataByJobId[i].invoiceType == 7 || $scope.dataByJobId[i].invoiceType == null) {
                                        $scope.tax = PPNPerc;
                                    } else {
                                        $scope.tax = PPNPerc;
                                    }
            
                                    for (z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++) {
                                        // console.log("onBeforeEdit | total opl", TotalOpl);
                                        //job task opl belum di cek ga ada data
                                        if ($scope.dataByJobId[i].JobTaskOpl[z].IsBilling < 1 && $scope.dataByJobId[i].JobTaskOpl[z].Status !== 4 && $scope.dataByJobId[i].JobTaskOpl[z].isDeleted != 1) {
                                            // ============================= cek kalau dr pembuatan wo nya dia pembayaran na insurance maka discount nya di timpa pake diskon dari master insurance nya ============================= start
                                            if ($scope.dataByJobId[i].JobTaskOpl[z].PaidById == 29){
                                                $scope.dataByJobId[i].JobTaskOpl[z].Discount = ($scope.dataByJobId[i].Insurance != undefined && $scope.dataByJobId[i].Insurance != null ? $scope.dataByJobId[i].Insurance.DiscountJasa : 0);
                                            }
                                            // ============================= cek kalau dr pembuatan wo nya dia pembayaran na insurance maka discount nya di timpa pake diskon dari master insurance nya ============================= end
                                            for (var vv=0; vv<$scope.dataPayment.length; vv++){
                                                if ($scope.dataByJobId[i].JobTaskOpl[z].PaidById == $scope.dataPayment[vv].MasterId){
                                                    $scope.dataByJobId[i].JobTaskOpl[z].Pembayaran = $scope.dataPayment[vv].Name
                                                }
                                            }
            
                                            if ($scope.dataByJobId[i].JobTaskOpl[z].PaidById == 30){
                                                //opl warranty cek dl ke towas claim result nya di jobtask
                                                var sudahAppTowas = 0;
                                                for (var n=0; n<$scope.dataByJobId[i].JobTask.length; n++){
                                                    //cek salah satu jobnya yg pembayaran warranty, list towas claim result nya
                                                    if ($scope.dataByJobId[i].JobTask[n].isDeleted == 0 && $scope.dataByJobId[i].JobTask[n].PaidById == 30){
                                                        if ($scope.dataByJobId[i].JobTask[n].JobTypeId != null && $scope.dataByJobId[i].JobTask[n].JobTypeId != undefined) {          
                                                            if ($scope.dataByJobId[i].JobTask[n].TOWASSClaimResult != null) {
                                                                if ($scope.dataByJobId[i].JobTask[n].TOWASSClaimResult.length > 0) {
                                                                    if ($scope.dataByJobId[i].JobTask[n].TOWASSClaimResult[0].ClaimStatus == 6) {
                                                                        sudahAppTowas = 1;
                                                                        break;
                                                                    }
                                                                }    
                                                            } 
                                                        }else{
                                                            if ($scope.dataByJobId[i].JobTask[n].TOWASSClaimResult != null) {
                                                                if ($scope.dataByJobId[i].JobTask[n].TOWASSClaimResult.length > 0) {
                                                                    if ($scope.dataByJobId[i].JobTask[n].TOWASSClaimResult[0].ClaimStatus == 6) {
                                                                        sudahAppTowas = 1;
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }else{
                                                        if ($scope.dataByJobId[i].JobTask[n].TOWASSClaimResult != null) {
                                                            if ($scope.dataByJobId[i].JobTask[n].TOWASSClaimResult.length > 0) {
                                                                if ($scope.dataByJobId[i].JobTask[n].TOWASSClaimResult[0].ClaimStatus == 6) {
                                                                    sudahAppTowas = 1;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                                if (sudahAppTowas == 1){
                                                    $scope.oplData.push($scope.dataByJobId[i].JobTaskOpl[z]);
                                                } else {
                                                    //ya blm di approve sama towas jobnya jg
                                                }
                                            } else {
                                                $scope.oplData.push($scope.dataByJobId[i].JobTaskOpl[z]);
                                            }
    
                                            console.log("$scope.oplData", $scope.oplData);
                                        } else {
                                            console.log("onBeforeEdit | Masuk else OplData");
                                        }
                                    }
            
                                    for (j = 0; j < $scope.dataByJobId[i].JobTask.length; j++) {
                                        console.log("onBeforeEdit | Test $scope.dataByJobId[i].JobTask[j].IsBilling =>", $scope.dataByJobId[i].JobTask[j].IsBilling);
                                        console.log("onBeforeEdit | Test $scope.dataByJobId[i].JobTask[j].PaidById =>", $scope.dataByJobId[i].JobTask[j].PaidById);
            
                                        // ============================= cek kalau dr pembuatan wo nya dia pembayaran na insurance maka discount nya di timpa pake diskon dari master insurance nya ============================= start
                                        if ($scope.dataByJobId[i].JobTask[j].PaidById == 29){
                                            $scope.dataByJobId[i].JobTask[j].Discount = ($scope.dataByJobId[i].Insurance != undefined && $scope.dataByJobId[i].Insurance != null ? $scope.dataByJobId[i].Insurance.DiscountJasa : 0);
                                            $scope.dataByJobId[i].JobTask[j].IsCustomDiscount = 0;
                                        }
                                        // ============================= cek kalau dr pembuatan wo nya dia pembayaran na insurance maka discount nya di timpa pake diskon dari master insurance nya ============================= end
            
                                        // Special case for IsCustomDiscount = 1 if Charge To Free Service
                                        if (($scope.dataByJobId[i].JobTask[j].PaidById == 2277 && $scope.dataByJobId[i].JobTask[j].IsCustomDiscount == 1) &&
                                            ($scope.dataByJobId[i].JobTask[j].Discount == null || $scope.dataByJobId[i].JobTask[j].Discount == 0)) {
                                            $scope.dataByJobId[i].JobTask[j].IsCustomDiscount = 0;
                                        }
            
                                        if (($scope.dataByJobId[i].JobTask[j].IsBilling < 1 || $scope.dataByJobId[i].JobTask[j].IsBilling == null) && ($scope.dataByJobId[i].JobTask[j].PaidById == 28 || $scope.dataByJobId[i].JobTask[j].PaidById == 2458 || $scope.dataByJobId[i].JobTask[j].PaidById == 29 || $scope.dataByJobId[i].JobTask[j].PaidById == 31 || $scope.dataByJobId[i].JobTask[j].PaidById == 32) &&
                                            $scope.dataByJobId[i].JobTask[j].IsCustomDiscount == 0 &&
                                            $scope.dataByJobId[i].JobTask[j].isDeleted == 0) {
            
                                            if ($scope.dataByJobId[i].JobTask[j].Fare !== undefined) {
                                                var sum = $scope.dataByJobId[i].JobTask[j].Fare;
                                                var summ = 0;
                                                if ($scope.dataByJobId[i].JobTask[j].FlatRate == null || $scope.dataByJobId[i].JobTask[j].FlatRate == undefined) {
                                                    summ = 1;
                                                } else {
                                                    summ = $scope.dataByJobId[i].JobTask[j].FlatRate;
                                                }
            
                                                var summaryy = Math.round(sum * summ);
                                                var discountedPrice = (summaryy * $scope.dataByJobId[i].JobTask[j].Discount) / 100;
                                                var normalPrice = summaryy
                                                summaryy = parseInt(summaryy);
                                            }
                                            if ($scope.dataByJobId[i].JobTask[j].JobType == null) {
                                                $scope.dataByJobId[i].JobTask[j].JobType = { Name: "" };
                                            }
                                            if ($scope.dataByJobId[i].JobTask[j].PaidBy == null) {
                                                $scope.dataByJobId[i].JobTask[j].PaidBy = { Name: "" }
                                            }
                                            if ($scope.dataByJobId[i].JobTask[j].AdditionalTaskId == null) {
                                                $scope.dataByJobId[i].JobTask[j].isOpe = 0;
                                            } else {
                                                $scope.dataByJobId[i].JobTask[j].isOpe = 1;
                                            }
                                            if ($scope.dataByJobId[i].JobTask[j].IsCustomDiscount == 0 && $scope.dataByJobId[i].JobTask[j].Discount == 0) {
                                                $scope.dataByJobId[i].JobTask[j].typeDiskon = -1;
                                                $scope.dataByJobId[i].JobTask[j].DiscountedPrice = normalPrice;
                                            } else if ($scope.dataByJobId[i].JobTask[j].IsCustomDiscount == 1 && $scope.dataByJobId[i].JobTask[j].Discount !== 0) {
                                                $scope.dataByJobId[i].JobTask[j].typeDiskon = 0;
                                                $scope.dataByJobId[i].JobTask[j].DiscountedPrice = (normalPrice * $scope.dataByJobId[i].JobTask[j].Discount) / 100;
                                            }
            
                                            //disini
                                            $scope.onPushGridWork(0, $scope.dataByJobId[i].JobTask[j].IsCustomDiscount, -1,
                                                $scope.dataByJobId[i],
                                                $scope.dataByJobId[i].JobTask[j],
                                                $scope.dataByJobId[i].AJobSPK[j],
                                                null,
                                                null,
                                                normalPrice,
                                                discountedPrice,
                                                summaryy);
                                        } else if (($scope.dataByJobId[i].JobTask[j].IsBilling < 1 || $scope.dataByJobId[i].JobTask[j].IsBilling == null) && ($scope.dataByJobId[i].JobTask[j].PaidById == 28 || $scope.dataByJobId[i].JobTask[j].PaidById == 2458 || $scope.dataByJobId[i].JobTask[j].PaidById == 29 || $scope.dataByJobId[i].JobTask[j].PaidById == 31 || $scope.dataByJobId[i].JobTask[j].PaidById == 32) &&
                                            $scope.dataByJobId[i].JobTask[j].IsCustomDiscount == 1 &&
                                            $scope.dataByJobId[i].JobTask[j].isDeleted == 0) {
                                            console.log('onBeforeEdit | bisa masuk mik kesini lagi', $scope.dataByJobId[i].JobTask[j]);
                                            console.log('onBeforeEdit | ApprovalDiscountTransApprovalDiscountTrans', $scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR);
            
                                            if ($scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR.length > 0 || $scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR != null) {
            
            
                                                if ($scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR[0].StatusApprovalId != 1 && $scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR[0].StatusApprovalId > 0){
                                                    $scope.dataByJobId[i].JobTask[j].Discount = 0;
                                                    // ini biar diskon ga muncul kl approval bertingkat... ga blh ada yg status req
                                                }
                                                // ini loop di komen karena dr be di sort descending by transactionid, jadi ambil array pertama aja buat cek di approve ato ngga
                                                // for (var jTR=0; jTR<$scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR.length; jTR++){
                                                //     // if ($scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR[jTR].StatusApprovalId == 3){
                                                //     if ($scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR[jTR].StatusApprovalId != 1 && $scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR[jTR].StatusApprovalId > 0){
                                                //         $scope.dataByJobId[i].JobTask[j].Discount = 0;
                                                //         // ini biar diskon ga muncul kl approval bertingkat... ga blh ada yg status req
                                                //     }
                                                // }
            
                                                if ($scope.dataByJobId[i].JobTask[j].Fare !== undefined) {
                                                    var sum = $scope.dataByJobId[i].JobTask[j].Fare;
                                                    var summ = 0;
                                                    if ($scope.dataByJobId[i].JobTask[j].FlatRate == null || $scope.dataByJobId[i].JobTask[j].FlatRate == undefined) {
                                                        summ = 1;
                                                    } else {
                                                        summ = $scope.dataByJobId[i].JobTask[j].FlatRate;
                                                    }
            
                                                    var summaryy = Math.round(sum * summ);
                                                    var discountedPrice = (summaryy * $scope.dataByJobId[i].JobTask[j].Discount) / 100;
                                                    var normalPrice = summaryy
                                                    summaryy = parseInt(summaryy);
                                                }
                                                if ($scope.dataByJobId[i].JobTask[j].JobType == null) {
                                                    $scope.dataByJobId[i].JobTask[j].JobType = { Name: "" };
                                                }
                                                if ($scope.dataByJobId[i].JobTask[j].PaidBy == null) {
                                                    $scope.dataByJobId[i].JobTask[j].PaidBy = { Name: "" }
                                                }
                                                if ($scope.dataByJobId[i].JobTask[j].AdditionalTaskId == null) {
                                                    $scope.dataByJobId[i].JobTask[j].isOpe = 0;
                                                } else {
                                                    $scope.dataByJobId[i].JobTask[j].isOpe = 1;
                                                }
                                                if ($scope.dataByJobId[i].JobTask[j].IsCustomDiscount == 0 && $scope.dataByJobId[i].JobTask[j].Discount == 0) {
                                                    $scope.dataByJobId[i].JobTask[j].typeDiskon = -1;
                                                    $scope.dataByJobId[i].JobTask[j].DiscountedPrice = normalPrice;
                                                } else if ($scope.dataByJobId[i].JobTask[j].IsCustomDiscount == 1 && $scope.dataByJobId[i].JobTask[j].Discount !== 0) {
                                                    $scope.dataByJobId[i].JobTask[j].typeDiskon = 0;
                                                    $scope.dataByJobId[i].JobTask[j].DiscountedPrice = (normalPrice * $scope.dataByJobId[i].JobTask[j].Discount) / 100;
                                                }
            
                                                // Checking for approval discount
                                                if ($scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR[0].StatusApprovalId == -1) {
                                                    // add to gridwork
                                                    $scope.onPushGridWork(0, $scope.dataByJobId[i].JobTask[j].IsCustomDiscount,
                                                        $scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR[0].StatusApprovalId,
                                                        $scope.dataByJobId[i],
                                                        $scope.dataByJobId[i].JobTask[j],
                                                        $scope.dataByJobId[i].AJobSPK[j],
                                                        null,
                                                        null,
                                                        normalPrice,
                                                        discountedPrice,
                                                        summaryy);
                                                }
            
                                                if ($scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR[0].StatusApprovalId == 1 || $scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR[0].StatusApprovalId == 2 || $scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR[0].StatusApprovalId == 3) {
                                                    // add to gridwork
                                                    $scope.onPushGridWork(0, $scope.dataByJobId[i].JobTask[j].IsCustomDiscount,
                                                        $scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR[0].StatusApprovalId,
                                                        $scope.dataByJobId[i],
                                                        $scope.dataByJobId[i].JobTask[j],
                                                        $scope.dataByJobId[i].AJobSPK[j],
                                                        null,
                                                        null,
                                                        normalPrice,
                                                        discountedPrice,
                                                        summaryy);
                                                }
                                            }
                                        } else if (($scope.dataByJobId[i].JobTask[j].IsBilling < 1 || $scope.dataByJobId[i].JobTask[j].IsBilling == null) &&
                                            $scope.dataByJobId[i].JobTask[j].PaidById == 30 &&
                                            $scope.dataByJobId[i].JobTask[j].isDeleted == 0) {
                                            // } else if (($scope.dataByJobId[i].JobTask[j].IsBilling < 1 || $scope.dataByJobId[i].JobTask[j].IsBilling == null) && $scope.dataByJobId[i].JobTask[j].PaidById == 30 && $scope.dataByJobId[i].JobTask[j].TOWASSClaimResult[0].ClaimStatus == 6) {
                                            // $scope.gridWork.push($scope.dataByJobId[i].JobTask[j]);
                                            // Added by Fyberz 2017-11-29  --------------------------
                                            console.log('onBeforeEdit | bisa masuk mik kesini', $scope.dataByJobId[i].JobTask[j]);
                                            if ($scope.dataByJobId[i].JobTask[j].TOWASSClaimResult != null) {
                                                if ($scope.dataByJobId[i].JobTask[j].TOWASSClaimResult.length > 0) {
                                                    // if ($scope.dataByJobId[i].JobTask[j].TOWASSClaimResult != null) {
                                                    console.log('onBeforeEdit | bisa masuk mik kesini yg towass na oke');
                                                    if ($scope.dataByJobId[i].JobTask[j].TOWASSClaimResult[0].ClaimStatus == 6) {
                                                        console.log('onBeforeEdit | bisa masuk mik kesini yg ada towass na');
                                                        if ($scope.dataByJobId[i].JobTask[j].Fare !== undefined) {
                                                            var sum = $scope.dataByJobId[i].JobTask[j].Fare;
                                                            var summ = 0;
                                                            if ($scope.dataByJobId[i].JobTask[j].FlatRate == null || $scope.dataByJobId[i].JobTask[j].FlatRate == undefined) {
                                                                summ = 1;
                                                            } else {
                                                                summ = $scope.dataByJobId[i].JobTask[j].FlatRate;
                                                            }
            
                                                            var summaryy = Math.round(sum * summ);
                                                            var discountedPrice = (summaryy * $scope.dataByJobId[i].JobTask[j].Discount) / 100;
                                                            var normalPrice = summaryy
                                                            summaryy = parseInt(summaryy);
                                                        }
                                                        if ($scope.dataByJobId[i].JobTask[j].JobType == null) {
                                                            $scope.dataByJobId[i].JobTask[j].JobType = { Name: "" };
                                                        }
                                                        if ($scope.dataByJobId[i].JobTask[j].PaidBy == null) {
                                                            $scope.dataByJobId[i].JobTask[j].PaidBy = { Name: "" }
                                                        }
                                                        if ($scope.dataByJobId[i].JobTask[j].AdditionalTaskId == null) {
                                                            $scope.dataByJobId[i].JobTask[j].isOpe = 0;
                                                        } else {
                                                            $scope.dataByJobId[i].JobTask[j].isOpe = 1;
                                                        }
                                                        if ($scope.dataByJobId[i].JobTask[j].IsCustomDiscount == 0 && $scope.dataByJobId[i].JobTask[j].Discount == 0) {
                                                            $scope.dataByJobId[i].JobTask[j].typeDiskon = -1;
                                                            $scope.dataByJobId[i].JobTask[j].DiscountedPrice = normalPrice;
                                                        } else if ($scope.dataByJobId[i].JobTask[j].IsCustomDiscount == 1 && $scope.dataByJobId[i].JobTask[j].Discount !== 0) {
                                                            $scope.dataByJobId[i].JobTask[j].typeDiskon = 0;
                                                            $scope.dataByJobId[i].JobTask[j].DiscountedPrice = (normalPrice * $scope.dataByJobId[i].JobTask[j].Discount) / 100;
                                                        }
                                                        $scope.gridWork.push({
                                                            ActualRate: $scope.dataByJobId[i].JobTask[j].ActualRate,
                                                            AdditionalTaskId: $scope.dataByJobId[i].JobTask[j].AdditionalTaskId,
                                                            isOpe: $scope.dataByJobId[i].JobTask[j].isOpe,
                                                            Discount: $scope.dataByJobId[i].JobTask[j].Discount,
                                                            JobTaskId: $scope.dataByJobId[i].JobTask[j].JobTaskId,
                                                            JobTypeId: $scope.dataByJobId[i].JobTask[j].JobTypeId,
                                                            JobTypeName: $scope.dataByJobId[i].JobTask[j].JobType.Name,
                                                            DiscountedPrice: discountedPrice,
                                                            IsBilling: $scope.dataByJobId[i].JobTask[j].IsBilling,
                                                            typeDiskon: $scope.dataByJobId[i].JobTask[j].typeDiskon,
                                                            NormalPrice: normalPrice,
                                                            // catName: $scope.dataByJobId[i].JobTask[j].JobType.Name,
                                                            catName: $scope.dataByJobId[i].Process != undefined || $scope.dataByJobId[i].Process != null ? $scope.dataByJobId[i].Process.Name : "",
                                                            Fare: $scope.dataByJobId[i].JobTask[j].Fare,
                                                            IsCustomDiscount: $scope.dataByJobId[i].JobTask[j].IsCustomDiscount,
                                                            Summary: summaryy,
                                                            PaidById: $scope.dataByJobId[i].JobTask[j].PaidById,
                                                            JobId: $scope.dataByJobId[i].JobTask[j].JobId,
                                                            TaskId: $scope.dataByJobId[i].JobTask[j].TaskId,
                                                            TaskName: $scope.dataByJobId[i].JobTask[j].TaskName,
                                                            FlatRate: $scope.dataByJobId[i].JobTask[j].FlatRate,
                                                            ProcessId: $scope.dataByJobId[i].JobTask[j].ProcessId,
                                                            TFirst1: $scope.dataByJobId[i].JobTask[j].TFirst1,
                                                            PaidBy: $scope.dataByJobId[i].JobTask[j].PaidBy.Name,
                                                            DiscountJasa: $scope.dataByJobId[i].Insurance != undefined || $scope.dataByJobId[i].Insurance != null ? $scope.dataByJobId[i].Insurance.DiscountJasa : 0,
                                                            SpkNo: $scope.dataByJobId[i].AJobSPK[j] == undefined || $scope.dataByJobId[i].AJobSPK[j] == null ? '' : $scope.dataByJobId[i].AJobSPK[j].SpkNo,
                                                            ORAmount: $scope.dataByJobId[i].AJobSPK[j] == undefined || $scope.dataByJobId[i].AJobSPK[j] == null ? 0 : $scope.dataByJobId[i].AJobSPK[j].ORAmount,
                                                            SpkHide: $scope.dataByJobId[i].AJobSPK[j] == undefined || $scope.dataByJobId[i].AJobSPK[j] == null ? true : false
                                                        });
                                                    };
                                                } else {
                                                    console.log('onBeforeEdit | bisa masuk mik kesini yg gk ada data towass na');
            
                                                }
                                            }
            
                                        } else if (($scope.dataByJobId[i].JobTask[j].IsBilling < 1 || $scope.dataByJobId[i].JobTask[j].IsBilling == null) && $scope.dataByJobId[i].JobTask[j].PaidById != 28 && $scope.dataByJobId[i].JobTask[j].PaidById != 30 &&
                                            $scope.dataByJobId[i].JobTask[j].IsCustomDiscount == 0 &&
                                            $scope.dataByJobId[i].JobTask[j].isDeleted == 0) {
                                            // $scope.gridWork.push($scope.dataByJobId[i].JobTask[j]);
                                            // Added by Fyberz 2017-11-29  --------------------------
            
                                            if ($scope.dataByJobId[i].JobTask[j].Fare !== undefined) {
                                                var sum = $scope.dataByJobId[i].JobTask[j].Fare;
                                                var summ = 0;
                                                if ($scope.dataByJobId[i].JobTask[j].FlatRate == null || $scope.dataByJobId[i].JobTask[j].FlatRate == undefined) {
                                                    summ = 1;
                                                } else {
                                                    summ = $scope.dataByJobId[i].JobTask[j].FlatRate;
                                                }
            
                                                var summaryy = Math.round(sum * summ);
                                                var discountedPrice = (summaryy * $scope.dataByJobId[i].JobTask[j].Discount) / 100;
                                                var normalPrice = summaryy
                                                summaryy = parseInt(summaryy);
                                            }
                                            if ($scope.dataByJobId[i].JobTask[j].JobType == null) {
                                                $scope.dataByJobId[i].JobTask[j].JobType = { Name: "" };
                                            }
                                            if ($scope.dataByJobId[i].JobTask[j].PaidBy == null) {
                                                $scope.dataByJobId[i].JobTask[j].PaidBy = { Name: "" }
                                            }
                                            if ($scope.dataByJobId[i].JobTask[j].AdditionalTaskId == null) {
                                                $scope.dataByJobId[i].JobTask[j].isOpe = 0;
                                            } else {
                                                $scope.dataByJobId[i].JobTask[j].isOpe = 1;
                                            }
                                            if ($scope.dataByJobId[i].JobTask[j].IsCustomDiscount == 0 && $scope.dataByJobId[i].JobTask[j].Discount == 0) {
                                                $scope.dataByJobId[i].JobTask[j].typeDiskon = -1;
                                                $scope.dataByJobId[i].JobTask[j].DiscountedPrice = normalPrice;
                                            } else if ($scope.dataByJobId[i].JobTask[j].IsCustomDiscount == 1 && $scope.dataByJobId[i].JobTask[j].Discount !== 0) {
                                                $scope.dataByJobId[i].JobTask[j].typeDiskon = 0;
                                                $scope.dataByJobId[i].JobTask[j].DiscountedPrice = (normalPrice * $scope.dataByJobId[i].JobTask[j].Discount) / 100;
                                            }
                                            $scope.gridWork.push({
                                                ActualRate: $scope.dataByJobId[i].JobTask[j].ActualRate,
                                                AdditionalTaskId: $scope.dataByJobId[i].JobTask[j].AdditionalTaskId,
                                                isOpe: $scope.dataByJobId[i].JobTask[j].isOpe,
                                                Discount: $scope.dataByJobId[i].JobTask[j].Discount,
                                                JobTaskId: $scope.dataByJobId[i].JobTask[j].JobTaskId,
                                                JobTypeId: $scope.dataByJobId[i].JobTask[j].JobTypeId,
                                                JobTypeName: $scope.dataByJobId[i].JobTask[j].JobType.Name,
                                                DiscountedPrice: discountedPrice,
                                                IsBilling: $scope.dataByJobId[i].JobTask[j].IsBilling,
                                                typeDiskon: $scope.dataByJobId[i].JobTask[j].typeDiskon,
                                                NormalPrice: normalPrice,
                                                // catName: $scope.dataByJobId[i].JobTask[j].JobType.Name,
                                                catName: $scope.dataByJobId[i].Process != undefined || $scope.dataByJobId[i].Process != null ? $scope.dataByJobId[i].Process.Name : "",
                                                Fare: $scope.dataByJobId[i].JobTask[j].Fare,
                                                IsCustomDiscount: $scope.dataByJobId[i].JobTask[j].IsCustomDiscount,
                                                Summary: summaryy,
                                                PaidById: $scope.dataByJobId[i].JobTask[j].PaidById,
                                                JobId: $scope.dataByJobId[i].JobTask[j].JobId,
                                                TaskId: $scope.dataByJobId[i].JobTask[j].TaskId,
                                                TaskName: $scope.dataByJobId[i].JobTask[j].TaskName,
                                                FlatRate: $scope.dataByJobId[i].JobTask[j].FlatRate,
                                                ProcessId: $scope.dataByJobId[i].JobTask[j].ProcessId,
                                                TFirst1: $scope.dataByJobId[i].JobTask[j].TFirst1,
                                                PaidBy: $scope.dataByJobId[i].JobTask[j].PaidBy.Name,
                                                DiscountJasa: $scope.dataByJobId[i].Insurance != undefined || $scope.dataByJobId[i].Insurance != null ? $scope.dataByJobId[i].Insurance.DiscountJasa : 0,
                                                SpkNo: $scope.dataByJobId[i].AJobSPK[j] == undefined || $scope.dataByJobId[i].AJobSPK[j] == null ? '' : $scope.dataByJobId[i].AJobSPK[j].SpkNo,
                                                ORAmount: $scope.dataByJobId[i].AJobSPK[j] == undefined || $scope.dataByJobId[i].AJobSPK[j] == null ? 0 : $scope.dataByJobId[i].AJobSPK[j].ORAmount,
                                                SpkHide: $scope.dataByJobId[i].AJobSPK[j] == undefined || $scope.dataByJobId[i].AJobSPK[j] == null ? true : false
                                            });
                                        } else {
                                            console.log("onBeforeEdit | masuk else JobTask sudah Billing");
                                        }
            
                                        for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
            
                                            // ============================= cek kalau dr pembuatan wo nya dia pembayaran na insurance maka discount nya di timpa pake diskon dari master insurance nya ============================= start
                                            if ($scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 29){
                                                $scope.dataByJobId[i].JobTask[j].JobParts[a].Discount = ($scope.dataByJobId[i].Insurance != undefined && $scope.dataByJobId[i].Insurance != null ? $scope.dataByJobId[i].Insurance.DiscountJasa : 0);
                                                $scope.dataByJobId[i].JobTask[j].JobParts[a].IsCustomDiscount = 0;
                                            }
                                            // ============================= cek kalau dr pembuatan wo nya dia pembayaran na insurance maka discount nya di timpa pake diskon dari master insurance nya ============================= end
            
                                            // Special case for IsCustomDiscount = 1 if Charge To Free Service
                                            if (($scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 2277 && $scope.dataByJobId[i].JobTask[j].JobParts[a].IsCustomDiscount == 1) &&
                                                ($scope.dataByJobId[i].JobTask[j].JobParts[a].Discount == null || $scope.dataByJobId[i].JobTask[j].JobParts[a].Discount == 0)) {
                                                $scope.dataByJobId[i].JobTask[j].JobParts[a].IsCustomDiscount = 0;
                                            }
            
                                            if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1 || $scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling == null &&
                                                $scope.dataByJobId[i].JobTask[j].JobParts[a].MaterialRequestStatusId == 80) {
                                                if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB != 1 ||  $scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB == null) {
                                                    if (($scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 28 || $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 2458 || $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 29 || $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 31 || $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 2663 || $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 32) &&
                                                        $scope.dataByJobId[i].JobTask[j].JobParts[a].IsCustomDiscount == 0 &&
                                                        $scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0) {
                                                        if ($scope.dataByJobId[i].JobTask[j].JobParts[a].Price == null || $scope.dataByJobId[i].JobTask[j].JobParts[a].Price == 0) {
                                                            $scope.dataByJobId[i].JobTask[j].JobParts[a].Price = $scope.dataByJobId[i].JobTask[j].JobParts[a].Part.RetailPrice;
                                                        }
                                                        // $scope.partsData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
                                                        $scope.partsData.push({
                                                            JobPartsId: $scope.dataByJobId[i].JobTask[j].JobParts[a].JobPartsId,
                                                            JobTaskId: $scope.dataByJobId[i].JobTask[j].JobParts[a].JobTaskId,
                                                            PartsId: $scope.dataByJobId[i].JobTask[j].JobParts[a].PartsId,
                                                            PartsName: $scope.dataByJobId[i].JobTask[j].JobParts[a].PartsName,
                                                            Price: $scope.dataByJobId[i].JobTask[j].JobParts[a].Price,
                                                            Status: $scope.dataByJobId[i].JobTask[j].JobParts[a].Status,
                                                            Qty: $scope.dataByJobId[i].JobTask[j].JobParts[a].Qty,
                                                            DownPayment: $scope.dataByJobId[i].JobTask[j].JobParts[a].DownPayment,
                                                            PaidById: $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById,
                                                            PaidBy: $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidBy.Name,
                                                            SatuanId: $scope.dataByJobId[i].JobTask[j].JobParts[a].SatuanId,
                                                            Satuan: $scope.dataByJobId[i].JobTask[j].JobParts[a].Satuan,
                                                            IsOfp: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsOfp,
                                                            MaterialRequestStatusId: $scope.dataByJobId[i].JobTask[j].JobParts[a].MaterialRequestStatusId,
                                                            QtyGI: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyGI,
                                                            QtyPrepicking: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyPrepicking,
                                                            QtyGIReserved: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyGIReserved,
                                                            QtyReserved: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyReserved,
                                                            IsOPB: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB,
                                                            SeqOPB: $scope.dataByJobId[i].JobTask[j].JobParts[a].SeqOPB,
                                                            OrderType: $scope.dataByJobId[i].JobTask[j].JobParts[a].OrderType,
                                                            QtyOrder: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyOrder,
                                                            MaterialTypeId: $scope.dataByJobId[i].JobTask[j].JobParts[a].MaterialTypeId,
                                                            IsDeleted: $scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted,
                                                            Discount: $scope.dataByJobId[i].JobTask[j].JobParts[a].Discount,
                                                            DiscountPart: $scope.dataByJobId[i].Insurance != undefined || $scope.dataByJobId[i].Insurance != null ? $scope.dataByJobId[i].Insurance.DiscountPart : 0,
                                                            IsCustomDiscount: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsCustomDiscount,
                                                            DiscountTypeId: $scope.dataByJobId[i].JobTask[j].JobParts[a].DiscountTypeId,
                                                            DiscountTypeListId: $scope.dataByJobId[i].JobTask[j].JobParts[a].DiscountTypeListId
                                                        });
                                                    } else if (($scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 28 || $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 2458 || $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 29 || $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 31 || $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 2663 || $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 32) &&
                                                        $scope.dataByJobId[i].JobTask[j].JobParts[a].IsCustomDiscount == 1 &&
                                                        $scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0) {
                                                        if ($scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR.length > 0 || $scope.dataByJobId[i].JobTask[j].ApprovalDiscountTransTR != null) {
                                                            if ($scope.dataByJobId[i].JobTask[j].JobParts[a].Price == null || $scope.dataByJobId[i].JobTask[j].JobParts[a].Price == 0) {
                                                                $scope.dataByJobId[i].JobTask[j].JobParts[a].Price = $scope.dataByJobId[i].JobTask[j].JobParts[a].Part.RetailPrice;
                                                            }
                                                            // Checking for approval discount
                                                            if ($scope.dataByJobId[i].JobTask[j].JobParts[a].ApprovalDiscountTransTR[0].StatusApprovalId == -1) {
                                                                // Add to grid parts
                                                                $scope.partsData.push({
                                                                    JobPartsId: $scope.dataByJobId[i].JobTask[j].JobParts[a].JobPartsId,
                                                                    JobTaskId: $scope.dataByJobId[i].JobTask[j].JobParts[a].JobTaskId,
                                                                    PartsId: $scope.dataByJobId[i].JobTask[j].JobParts[a].PartsId,
                                                                    PartsName: $scope.dataByJobId[i].JobTask[j].JobParts[a].PartsName,
                                                                    Price: $scope.dataByJobId[i].JobTask[j].JobParts[a].Price,
                                                                    Status: $scope.dataByJobId[i].JobTask[j].JobParts[a].Status,
                                                                    Qty: $scope.dataByJobId[i].JobTask[j].JobParts[a].Qty,
                                                                    DownPayment: $scope.dataByJobId[i].JobTask[j].JobParts[a].DownPayment,
                                                                    PaidById: $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById,
                                                                    PaidBy: $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidBy.Name,
                                                                    SatuanId: $scope.dataByJobId[i].JobTask[j].JobParts[a].SatuanId,
                                                                    Satuan: $scope.dataByJobId[i].JobTask[j].JobParts[a].Satuan,
                                                                    IsOfp: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsOfp,
                                                                    MaterialRequestStatusId: $scope.dataByJobId[i].JobTask[j].JobParts[a].MaterialRequestStatusId,
                                                                    QtyGI: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyGI,
                                                                    QtyPrepicking: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyPrepicking,
                                                                    QtyGIReserved: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyGIReserved,
                                                                    QtyReserved: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyReserved,
                                                                    IsOPB: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB,
                                                                    SeqOPB: $scope.dataByJobId[i].JobTask[j].JobParts[a].SeqOPB,
                                                                    OrderType: $scope.dataByJobId[i].JobTask[j].JobParts[a].OrderType,
                                                                    QtyOrder: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyOrder,
                                                                    MaterialTypeId: $scope.dataByJobId[i].JobTask[j].JobParts[a].MaterialTypeId,
                                                                    IsDeleted: $scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted,
                                                                    Discount: $scope.dataByJobId[i].JobTask[j].JobParts[a].Discount,
                                                                    DiscountPart: $scope.dataByJobId[i].Insurance != undefined || $scope.dataByJobId[i].Insurance != null ? $scope.dataByJobId[i].Insurance.DiscountPart : 0,
                                                                    IsCustomDiscount: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsCustomDiscount,
                                                                    DiscountTypeId: $scope.dataByJobId[i].JobTask[j].JobParts[a].DiscountTypeId,
                                                                    DiscountTypeListId: $scope.dataByJobId[i].JobTask[j].JobParts[a].DiscountTypeListId
                                                                });
                                                            }
                                                            if ($scope.dataByJobId[i].JobTask[j].JobParts[a].ApprovalDiscountTransTR[0].StatusApprovalId == 1 || $scope.dataByJobId[i].JobTask[j].JobParts[a].ApprovalDiscountTransTR[0].StatusApprovalId == 2 || $scope.dataByJobId[i].JobTask[j].JobParts[a].ApprovalDiscountTransTR[0].StatusApprovalId == 3) {
                                                                // Add to grid parts
                                                                $scope.partsData.push({
                                                                    JobPartsId: $scope.dataByJobId[i].JobTask[j].JobParts[a].JobPartsId,
                                                                    JobTaskId: $scope.dataByJobId[i].JobTask[j].JobParts[a].JobTaskId,
                                                                    PartsId: $scope.dataByJobId[i].JobTask[j].JobParts[a].PartsId,
                                                                    PartsName: $scope.dataByJobId[i].JobTask[j].JobParts[a].PartsName,
                                                                    Price: $scope.dataByJobId[i].JobTask[j].JobParts[a].Price,
                                                                    Status: $scope.dataByJobId[i].JobTask[j].JobParts[a].Status,
                                                                    Qty: $scope.dataByJobId[i].JobTask[j].JobParts[a].Qty,
                                                                    DownPayment: $scope.dataByJobId[i].JobTask[j].JobParts[a].DownPayment,
                                                                    PaidById: $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById,
                                                                    PaidBy: $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidBy.Name,
                                                                    SatuanId: $scope.dataByJobId[i].JobTask[j].JobParts[a].SatuanId,
                                                                    Satuan: $scope.dataByJobId[i].JobTask[j].JobParts[a].Satuan,
                                                                    IsOfp: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsOfp,
                                                                    MaterialRequestStatusId: $scope.dataByJobId[i].JobTask[j].JobParts[a].MaterialRequestStatusId,
                                                                    QtyGI: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyGI,
                                                                    QtyPrepicking: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyPrepicking,
                                                                    QtyGIReserved: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyGIReserved,
                                                                    QtyReserved: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyReserved,
                                                                    IsOPB: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB,
                                                                    SeqOPB: $scope.dataByJobId[i].JobTask[j].JobParts[a].SeqOPB,
                                                                    OrderType: $scope.dataByJobId[i].JobTask[j].JobParts[a].OrderType,
                                                                    QtyOrder: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyOrder,
                                                                    MaterialTypeId: $scope.dataByJobId[i].JobTask[j].JobParts[a].MaterialTypeId,
                                                                    IsDeleted: $scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted,
                                                                    Discount: ($scope.dataByJobId[i].JobTask[j].JobParts[a].ApprovalDiscountTransTR[0].StatusApprovalId == 1 ? $scope.dataByJobId[i].JobTask[j].JobParts[a].Discount : 0),
                                                                    DiscountPart: $scope.dataByJobId[i].Insurance != undefined || $scope.dataByJobId[i].Insurance != null ? $scope.dataByJobId[i].Insurance.DiscountPart : 0,
                                                                    IsCustomDiscount: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsCustomDiscount,
                                                                    DiscountTypeId: $scope.dataByJobId[i].JobTask[j].JobParts[a].DiscountTypeId,
                                                                    DiscountTypeListId: $scope.dataByJobId[i].JobTask[j].JobParts[a].DiscountTypeListId
                                                                });
                                                            }
                                                        }
                                                    } else if ($scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 30) {
                                                        if ($scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0) {
                                                            if ($scope.dataByJobId[i].JobTask[j].TOWASSClaimResult != null) {
                                                                if ($scope.dataByJobId[i].JobTask[j].TOWASSClaimResult.length > 0) {
                                                                    if ($scope.dataByJobId[i].JobTask[j].JobParts[a].Price == null || $scope.dataByJobId[i].JobTask[j].JobParts[a].Price == 0) {
                                                                        $scope.dataByJobId[i].JobTask[j].JobParts[a].Price = $scope.dataByJobId[i].JobTask[j].JobParts[a].Part.RetailPrice;
                                                                    }
                                                                    // $scope.partsData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
                                                                    $scope.partsData.push({
                                                                        JobPartsId: $scope.dataByJobId[i].JobTask[j].JobParts[a].JobPartsId,
                                                                        JobTaskId: $scope.dataByJobId[i].JobTask[j].JobParts[a].JobTaskId,
                                                                        PartsId: $scope.dataByJobId[i].JobTask[j].JobParts[a].PartsId,
                                                                        PartsName: $scope.dataByJobId[i].JobTask[j].JobParts[a].PartsName,
                                                                        Price: $scope.dataByJobId[i].JobTask[j].JobParts[a].Price,
                                                                        Status: $scope.dataByJobId[i].JobTask[j].JobParts[a].Status,
                                                                        Qty: $scope.dataByJobId[i].JobTask[j].JobParts[a].Qty,
                                                                        DownPayment: $scope.dataByJobId[i].JobTask[j].JobParts[a].DownPayment,
                                                                        PaidById: $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById,
                                                                        PaidBy: $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidBy.Name,
                                                                        SatuanId: $scope.dataByJobId[i].JobTask[j].JobParts[a].SatuanId,
                                                                        Satuan: $scope.dataByJobId[i].JobTask[j].JobParts[a].Satuan,
                                                                        IsOfp: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsOfp,
                                                                        MaterialRequestStatusId: $scope.dataByJobId[i].JobTask[j].JobParts[a].MaterialRequestStatusId,
                                                                        QtyGI: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyGI,
                                                                        QtyPrepicking: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyPrepicking,
                                                                        QtyGIReserved: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyGIReserved,
                                                                        QtyReserved: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyReserved,
                                                                        IsOPB: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB,
                                                                        SeqOPB: $scope.dataByJobId[i].JobTask[j].JobParts[a].SeqOPB,
                                                                        OrderType: $scope.dataByJobId[i].JobTask[j].JobParts[a].OrderType,
                                                                        QtyOrder: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyOrder,
                                                                        MaterialTypeId: $scope.dataByJobId[i].JobTask[j].JobParts[a].MaterialTypeId,
                                                                        IsDeleted: $scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted,
                                                                        Discount: $scope.dataByJobId[i].JobTask[j].JobParts[a].Discount,
                                                                        DiscountPart: $scope.dataByJobId[i].Insurance != undefined || $scope.dataByJobId[i].Insurance != null ? $scope.dataByJobId[i].Insurance.DiscountPart : 0,
                                                                        IsCustomDiscount: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsCustomDiscount,
                                                                        DiscountTypeId: $scope.dataByJobId[i].JobTask[j].JobParts[a].DiscountTypeId,
                                                                        DiscountTypeListId: $scope.dataByJobId[i].JobTask[j].JobParts[a].DiscountTypeListId
                                                                    });
                                                                }
                                                            }
            
                                                        } else {
                                                            console.log('onBeforeEdit | bisa masuk mik kesini yg gk ada data parts towass na');
                                                        }
                                                    } 
                                                    else if (($scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById != 28 || $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById != 30) &&
                                                        $scope.dataByJobId[i].JobTask[j].JobParts[a].IsCustomDiscount == 0 &&
                                                        $scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0) {
                                                        if ($scope.dataByJobId[i].JobTask[j].JobParts[a].Price == null || $scope.dataByJobId[i].JobTask[j].JobParts[a].Price == 0) {
                                                            $scope.dataByJobId[i].JobTask[j].JobParts[a].Price = $scope.dataByJobId[i].JobTask[j].JobParts[a].Part.RetailPrice;
                                                        }
            
                                                        
                                                        // $scope.partsData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
                                                        $scope.partsData.push({
                                                            JobPartsId: $scope.dataByJobId[i].JobTask[j].JobParts[a].JobPartsId,
                                                            JobTaskId: $scope.dataByJobId[i].JobTask[j].JobParts[a].JobTaskId,
                                                            PartsId: $scope.dataByJobId[i].JobTask[j].JobParts[a].PartsId,
                                                            PartsName: $scope.dataByJobId[i].JobTask[j].JobParts[a].PartsName,
                                                            Price: $scope.dataByJobId[i].JobTask[j].JobParts[a].Price,
                                                            Status: $scope.dataByJobId[i].JobTask[j].JobParts[a].Status,
                                                            Qty: $scope.dataByJobId[i].JobTask[j].JobParts[a].Qty,
                                                            DownPayment: $scope.dataByJobId[i].JobTask[j].JobParts[a].DownPayment,
                                                            PaidById: $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById,
                                                            PaidBy: $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidBy.Name,
                                                            SatuanId: $scope.dataByJobId[i].JobTask[j].JobParts[a].SatuanId,
                                                            Satuan: $scope.dataByJobId[i].JobTask[j].JobParts[a].Satuan,
                                                            IsOfp: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsOfp,
                                                            MaterialRequestStatusId: $scope.dataByJobId[i].JobTask[j].JobParts[a].MaterialRequestStatusId,
                                                            QtyGI: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyGI,
                                                            QtyPrepicking: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyPrepicking,
                                                            QtyGIReserved: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyGIReserved,
                                                            QtyReserved: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyReserved,
                                                            IsOPB: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB,
                                                            SeqOPB: $scope.dataByJobId[i].JobTask[j].JobParts[a].SeqOPB,
                                                            OrderType: $scope.dataByJobId[i].JobTask[j].JobParts[a].OrderType,
                                                            QtyOrder: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyOrder,
                                                            MaterialTypeId: $scope.dataByJobId[i].JobTask[j].JobParts[a].MaterialTypeId,
                                                            IsDeleted: $scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted,
                                                            Discount: $scope.dataByJobId[i].JobTask[j].JobParts[a].Discount,
                                                            DiscountPart: $scope.dataByJobId[i].Insurance != undefined || $scope.dataByJobId[i].Insurance != null ? $scope.dataByJobId[i].Insurance.DiscountPart : 0,
                                                            IsCustomDiscount: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsCustomDiscount,
                                                            DiscountTypeId: $scope.dataByJobId[i].JobTask[j].JobParts[a].DiscountTypeId,
                                                            DiscountTypeListId: $scope.dataByJobId[i].JobTask[j].JobParts[a].DiscountTypeListId
                                                        });
                                                    }
                                                } else {
                                                    console.log("onBeforeEdit | salah satu data ada yg opb");
                                                }
                                            } else {
                                                console.log("onBeforeEdit | mausk else Job Parts Sudah Ada Billing");
                                            }
                                        }
                                        // console.log("onBeforeEdit | TotalParts", TotalParts);
                                        for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
            
                                            // ============================= cek kalau dr pembuatan wo nya dia pembayaran na insurance maka discount nya di timpa pake diskon dari master insurance nya ============================= start
                                            if ($scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById == 29){
                                                $scope.dataByJobId[i].JobTask[j].JobParts[a].Discount = ($scope.dataByJobId[i].Insurance != undefined && $scope.dataByJobId[i].Insurance != null ? $scope.dataByJobId[i].Insurance.DiscountJasa : 0);
                                            }
                                            // ============================= cek kalau dr pembuatan wo nya dia pembayaran na insurance maka discount nya di timpa pake diskon dari master insurance nya ============================= end
            
                                            if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1 &&
                                                $scope.dataByJobId[i].JobTask[j].JobParts[a].MaterialRequestStatusId == 80) {
                                                if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB == 1 &&
                                                    $scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0) {
                                                    // $scope.opbData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
                                                    $scope.opbData.push({
                                                        JobPartsId: $scope.dataByJobId[i].JobTask[j].JobParts[a].JobPartsId,
                                                        JobTaskId: $scope.dataByJobId[i].JobTask[j].JobParts[a].JobTaskId,
                                                        PartsId: $scope.dataByJobId[i].JobTask[j].JobParts[a].PartsId,
                                                        PartsName: $scope.dataByJobId[i].JobTask[j].JobParts[a].PartsName,
                                                        Price: $scope.dataByJobId[i].JobTask[j].JobParts[a].Price,
                                                        Status: $scope.dataByJobId[i].JobTask[j].JobParts[a].Status,
                                                        Qty: $scope.dataByJobId[i].JobTask[j].JobParts[a].Qty,
                                                        DownPayment: $scope.dataByJobId[i].JobTask[j].JobParts[a].DownPayment,
                                                        PaidById: $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById,
                                                        PaidBy: $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidBy.Name,
                                                        SatuanId: $scope.dataByJobId[i].JobTask[j].JobParts[a].SatuanId,
                                                        Satuan: $scope.dataByJobId[i].JobTask[j].JobParts[a].Satuan,
                                                        IsOfp: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsOfp,
                                                        MaterialRequestStatusId: $scope.dataByJobId[i].JobTask[j].JobParts[a].MaterialRequestStatusId,
                                                        QtyGI: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyGI,
                                                        QtyPrepicking: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyPrepicking,
                                                        QtyGIReserved: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyGIReserved,
                                                        QtyReserved: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyReserved,
                                                        IsOPB: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB,
                                                        SeqOPB: $scope.dataByJobId[i].JobTask[j].JobParts[a].SeqOPB,
                                                        OrderType: $scope.dataByJobId[i].JobTask[j].JobParts[a].OrderType,
                                                        QtyOrder: $scope.dataByJobId[i].JobTask[j].JobParts[a].QtyOrder,
                                                        MaterialTypeId: $scope.dataByJobId[i].JobTask[j].JobParts[a].MaterialTypeId,
                                                        IsDeleted: $scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted,
                                                        Discount: $scope.dataByJobId[i].JobTask[j].JobParts[a].PaidById != 29 ? $scope.dataByJobId[i].JobTask[j].JobParts[a].Discount : ($scope.dataByJobId[i].Insurance != undefined || $scope.dataByJobId[i].Insurance != null ? $scope.dataByJobId[i].Insurance.DiscountPart : 0),
                                                        DiscountPart: $scope.dataByJobId[i].Insurance != undefined || $scope.dataByJobId[i].Insurance != null ? $scope.dataByJobId[i].Insurance.DiscountPart : 0,
                                                        IsCustomDiscount: $scope.dataByJobId[i].JobTask[j].JobParts[a].IsCustomDiscount,
                                                        DiscountTypeId: $scope.dataByJobId[i].JobTask[j].JobParts[a].DiscountTypeId,
                                                        DiscountTypeListId: $scope.dataByJobId[i].JobTask[j].JobParts[a].DiscountTypeListId
                                                    });
                                                } else {
                                                    console.log("onBeforeEdit | tidak ada data Opb");
                                                }
                                            } else {
                                                console.log("onBeforeEdit | mausk else Job Parts Sudah Ada Billing");
                                            }
                                        }
                                    }
                                }
            
            
            
                                if ($scope.checkJobBelumBilling == 1){
                                    $scope.mBilling.dataPayment = $scope.dataJobBelumBilling;
                                    console.log('onBeforeEdit | cek pembayaran 1 | checkJobBelumBilling ===>',$scope.dataJobBelumBilling)
                                } else if ($scope.checkJobOPLBelumBilling == 1){
                                    $scope.mBilling.dataPayment = $scope.dataJobOPLBelumBilling;
                                    // $scope.mBilling.dataPayment = $scope.oplData[0].PaidById;
                                    console.log('onBeforeEdit | cek pembayaran 2 | dataJobOPLBelumBilling ===>',$scope.dataJobOPLBelumBilling)
                                } else if ($scope.checkPartsBelumBilling == 1){
                                    $scope.mBilling.dataPayment = $scope.dataPartsBelumBilling;
                                    console.log('onBeforeEdit | cek pembayaran 3 | dataPartsBelumBilling ===>',$scope.dataPartsBelumBilling)
                                } else if ($scope.checkJobOPLBPBelumBilling == 1){
                                    $scope.mBilling.dataPayment = $scope.dataJobOPLBPBelumBilling;
                                    console.log('onBeforeEdit | cek pembayaran 4 | dataJobOPLBPBelumBilling ===>',$scope.dataJobOPLBPBelumBilling)
                                } else {
                                    $scope.mBilling.dataPayment = $scope.dataByJobId[0].JobTask[0].PaidById;
                                    console.log('onBeforeEdit | cek pembayaran 5 | $scope.dataByJobId[0].JobTask[0].PaidById ===>',$scope.$scope.dataByJobId[0].JobTask[0].PaidById)
                                }
            
                                $scope.SelectedPayment($scope.mBilling.dataPayment);
            
            
                                console.log("onBeforeEdit | dataPayment ===>", $scope.mBilling.dataPayment);
                                if($scope.mBilling.dataPayment == 30){//warranty
                                    var findTAM = [];
                                    BillingFactory.getListOutlet('toyota astra motor', 4).then(function(resu) {
                                    findTAM = resu.data.Result;
                                    console.log('onBeforeEdit | dataPayment Warranty findTAM ===>',findTAM);
                                        if(findTAM.length > 0){
                                            $scope.mBilling.BillTo = findTAM[0].Name;
                                            $scope.mBilling.BillAddress = findTAM[0].Address;
                                            $scope.mBilling.TIN = _.isUndefined(findTAM[0].NPWP) ? findTAM[0].Npwp : findTAM[0].NPWP;
                                            $scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
                                            $scope.mBilling.TaxName = findTAM[0].Name;
                                            $scope.mBilling.TaxAddress = findTAM[0].Address;
                                            $scope.mBilling.BillPhoneNumber = findTAM[0].PhoneNumber;
                                        }
            
                                    });
                                    
            
            
                                }
            
                                // $scope.subTotalOpb = TotalOpb;
            
                                // TotalFinal = $scope.subTotalOpb + $scope.subTotalParts + $scope.subTotalLabor + $scope.subTotalOpl;
                                // $scope.TotalFinal = TotalFinal;
            
                                console.log("onBeforeEdit | total akhir", TotalFinal);
            
                                console.log("onBeforeEdit | data Opl", $scope.oplData);
                                console.log("onBeforeEdit | gridWirk onBeforeEdit", $scope.gridWork);
                                console.log("onBeforeEdit | dataparts", $scope.partsData);
                                console.log("onBeforeEdit | data by job id", $scope.dataByJobId);
                                // console.log("onBeforeEdit | First Check", $scope.DataPemeriksaanAwal);
                                // $scope.loading=false;
            
                                // $scope.mBilling.TIN = $scope.dataByJobId[0].Npwp.Npwp;
                                // $scope.mBilling.TaxName = $scope.dataByJobId[0].Npwp.CustomerName;
                                // $scope.mBilling.TaxAddress = $scope.dataByJobId[0].Npwp.Address + ' RT. ' + $scope.dataByJobId[0].Npwp.RT + ' RW. ' + $scope.dataByJobId[0].Npwp.RW + ' Kel/Des. ' + $scope.dataByJobId[0].VillageName + ', Kec/Dis. ' + $scope.dataByJobId[0].DistrictName + ', Kab/Kota. ' + $scope.dataByJobId[0].CityRegencyName + ', Provinsi. ' + $scope.dataByJobId[0].ProvinceName + ', KodePos. ' + $scope.dataByJobId[0].PostalCode
                                $scope.mBilling.ORPaidAmount = $scope.dataByJobId[0].ORPaidAmount;
                                $scope.mBilling.IsPaidOR = $scope.dataByJobId[0].IsPaidOR;
                                $scope.mBilling.CodeTransaction = $scope.dataByJobId[0].invoiceType;
                                $scope.mBilling.IdCodeTransaction = $scope.findValueFromArray($scope.codeTransaction,'Code','CodeInvoiceTransactionTaxId',$scope.dataByJobId[0].invoiceType),
            
                                console.log('onBeforeEdit | kode', $scope.mBilling.CodeTransaction);
                                console.log('onBeforeEdit | kode', $scope.mBilling.IdCodeTransaction);
                                // $scope.getData(); //0149-SIT-033- DeliveryBilling
                            },
                            function(err) {
                                console.log("onBeforeEdit | err=>", err);
                            }
                        );



                    },
                    function(err) {
                        console.log("onBeforeEdit | err=>", err);
                    }
                );
            });


            

            
            // JobId
        };
        // arr = for array
        $scope.findValueFromArray = function(arr, key, value, param){
            for(var i in arr){
                console.log('arr ====>', arr[i][key] ,key, param);
                if(arr[i][key] == param ){
                    return arr[i][value]
                }
            }
        }
        $scope.SelectedOutlet = function(item) {
            console.log('item SelectedOutlet selected', item);

        };

        $scope.SelectedPayment = function(item, isManualChange) {
            console.log('item payment selected', item, $scope.dataCRM);


            // ----------------- cek masa berlaku pks dulu ------------------- start
            if (item == 2458 && isManualChange == 1) {
                if ($scope.default_pks.length > 0) {
                    if ($scope.default_pks[0].OnPeriode == 0) {
                        bsAlert.warning("Tagihan ini tidak bisa dibebankan ke PKS dikarenakan masa berlaku PKS untuk kendaraan dengan No. Polisi : " + $scope.mBilling.PoliceNumber + " sudah berakhir.", "Silakan hubungi admin service untuk memperpanjang masa berlaku PKS.");
                        $scope.mBilling.dataPayment = null
                        return;
                    }
                } else {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Data tidak ditemukan di Master PKS"                                       
                    });
                    $scope.mBilling.dataPayment = null
                    return;
                }
                
            }

            // ----------------- cek masa berlaku pks dulu ------------------- end


            // if(tmp_mBillingDataPayment == undefined){
               
                tmp_mBillingDataPayment = item;
            // }
			
			if($scope.mBilling.Nationality == undefined || $scope.mBilling.Nationality == null || $scope.mBilling.Nationality == ""){
				tmp_mBillingNatTypeData = 0;
			}else{
				tmp_mBillingNatTypeData = $scope.mBilling.Nationality.Id;
			}

			$scope.showNPWP = true;
			$scope.showPassport = false;
			$scope.showKTP = false;

			$scope.mBilling.isNPWP = 0;

            if (item == 28) // Payment To Customer
            {
                if ($scope.dataCRM != undefined && $scope.dataCRM != null) {
                    if ($scope.dataCRM.length > 0) {
                        $scope.mBilling.BillTo = $scope.dataCRM[0].Name;
                        $scope.mBilling.BillAddress = $scope.dataCRM[0].Address;
                        $scope.mBilling.TIN = $scope.dataCRM[0].Npwp;
                        $scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
                        $scope.mBilling.ToyotaId = $scope.dataCRM[0].ToyotaId != null ? $scope.dataCRM[0].ToyotaId : '-';
                        $scope.mBilling.TaxName = $scope.dataCRM[0].Name;
                        $scope.mBilling.TaxAddress = $scope.dataCRM[0].Address;
                        // $scope.mBilling.BillPhoneNumber = $scope.dataCRM[0].PhoneNumber;
                        $scope.mBilling.BillPhoneNumber = $scope.dataCRM[0].PhoneNumber?$scope.dataCRM[0].PhoneNumber:$scope.dataByJobId[0].PhoneContactPerson1;
                    }
                }
            } else if (item == 29) { // Payment To Insurance
                console.log('dataInsurance', $scope.dataInsurance);
				//$scope.mBilling.isNPWP = 0;
                if ($scope.dataInsurance.length > 0) {
                    $scope.mBilling.BillTo = $scope.dataInsurance[0].InsuranceName;
                    $scope.mBilling.BillAddress = $scope.dataInsurance[0].BillingAddress;
                    $scope.mBilling.TIN = $scope.dataInsurance[0].NPWP;
                    $scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
                    $scope.mBilling.TaxName = $scope.dataInsurance[0].InsuranceName;
                    $scope.mBilling.TaxAddress = $scope.dataInsurance[0].BillingAddress;
                    $scope.mBilling.ToyotaId = '-';
                    $scope.mBilling.BillPhoneNumber = $scope.dataInsurance[0].PhoneNumber;
                    // $scope.mBilling.BillPhoneNumber = $scope.dataInsurance[0].PhoneNumber?$scope.dataInsurance[0].PhoneNumber:$scope.dataByJobId[0].PhoneContactPerson1;
                } else {
                    $scope.mBilling.BillTo = '';
                    $scope.mBilling.BillAddress = '';
                    $scope.mBilling.TIN = '';
                    $scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
                    $scope.mBilling.TaxName = '';
                    $scope.mBilling.TaxAddress = '';
                    $scope.mBilling.ToyotaId = '-'
                    $scope.mBilling.BillPhoneNumber = "";
                }
            } else if (item == 2277) { // Payment To Free Service
                console.log('freeServiceData', $scope.freeServiceData);
				//$scope.mBilling.isNPWP = 0;
                if ($scope.freeServiceData != undefined && $scope.freeServiceData != null) {
                    if ($scope.freeServiceData.length > 0) {
                        var pos = 0;
                        for (var i=0; i<$scope.freeServiceData.length; i++){
                            if ($scope.user.OrgName == $scope.freeServiceData[i].Name){
                                pos = i;
                                break;
                            }
                        }
                        $scope.mBilling.BillTo = $scope.freeServiceData[pos].Name;
                        $scope.mBilling.BillAddress = $scope.freeServiceData[pos].Address;
                        $scope.mBilling.TIN = $scope.freeServiceData[pos].Npwp;
                        $scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
                        $scope.mBilling.TaxName = $scope.freeServiceData[pos].Name;
                        $scope.mBilling.TaxAddress = $scope.freeServiceData[pos].Address;
                        $scope.mBilling.ToyotaId = '-'
                        $scope.mBilling.BillPhoneNumber = $scope.freeServiceData[pos].PhoneNumber;
                        // $scope.mBilling.BillPhoneNumber = $scope.freeServiceData[0].PhoneNumber?$scope.freeServiceData[0].PhoneNumber:$scope.dataByJobId[0].PhoneContactPerson1;

                    } else {
                        $scope.mBilling.BillTo = '';
                        $scope.mBilling.BillAddress = '';
                        $scope.mBilling.TIN = '';
                        $scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
                        $scope.mBilling.TaxName = '';
                        $scope.mBilling.TaxAddress = '';
                        $scope.mBilling.ToyotaId = '-'
                        $scope.mBilling.BillPhoneNumber = "";
                    }
                }
            } else if (item == 31) { // Payment To Internal
                console.log('dataOutlet', $scope.dataOutlet);
				//$scope.mBilling.isNPWP = 0;
                if ($scope.dataOutlet.length > 0) {
                    $scope.mBilling.BillTo = $scope.dataOutlet[0].Name;
                    $scope.mBilling.BillAddress = $scope.dataOutlet[0].Address;
                    $scope.mBilling.TIN = $scope.dataOutlet[0].NPWP;
                    $scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
                    $scope.mBilling.TaxName = $scope.dataOutlet[0].Name;
                    $scope.mBilling.TaxAddress = $scope.dataOutlet[0].Address;
                    $scope.mBilling.ToyotaId = '-'
                    $scope.mBilling.BillPhoneNumber = $scope.dataOutlet[0].PhoneNumber;
                    // $scope.mBilling.BillPhoneNumber = $scope.dataOutlet[0].PhoneNumber?$scope.dataOutlet[0].PhoneNumber:$scope.dataByJobId[0].PhoneContactPerson1;

                }
            } else if (item == 30) { // Payment To Internal
                console.log('dataOutlet', $scope.warantyData);
				//$scope.mBilling.isNPWP = 0;
                if ($scope.warantyData.length > 0) {
                    $scope.mBilling.BillTo = $scope.warantyData[0].Name;
                    $scope.mBilling.BillAddress = $scope.warantyData[0].Address;
                    $scope.mBilling.TIN = $scope.warantyData[0].Npwp;
                    $scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
                    $scope.mBilling.TaxName = $scope.warantyData[0].Name;
                    $scope.mBilling.TaxAddress = $scope.warantyData[0].Address;
                    $scope.mBilling.ToyotaId = '-'
                    $scope.mBilling.BillPhoneNumber = $scope.warantyData[0].PhoneNumber;
                    // $scope.mBilling.BillPhoneNumber = $scope.dataOutlet[0].PhoneNumber?$scope.dataOutlet[0].PhoneNumber:$scope.dataByJobId[0].PhoneContactPerson1;
                } 
            } else { // Default
                if ($scope.dataCRM.length > 0) {
                    $scope.mBilling.BillTo = $scope.dataCRM[0].Name;
                    $scope.mBilling.BillAddress = $scope.dataCRM[0].Address;
                    $scope.mBilling.TIN = $scope.dataCRM[0].Npwp;
                    $scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
                    $scope.mBilling.ToyotaId = $scope.dataCRM[0].ToyotaId != null ? $scope.dataCRM[0].ToyotaId : '-';
                    $scope.mBilling.TaxName = $scope.dataCRM[0].Name;
                    $scope.mBilling.TaxAddress = $scope.dataCRM[0].Address;
                    $scope.mBilling.BillPhoneNumber = $scope.dataCRM[0].PhoneNumber;
                    // $scope.mBilling.BillPhoneNumber = $scope.dataCRM[0].PhoneNumber?$scope.dataCRM[0].PhoneNumber:$scope.dataByJobId[0].PhoneContactPerson1;

                }
            }
			
			if (item == 2458) // Payment To PKS
            {
				$scope.mBilling.isNPWP = 0;

                // set default pks --------------------------------------------- start
                $scope.mBilling.BillTo = $scope.default_pks[0].CompanyName;
                $scope.mBilling.BillAddress = $scope.default_pks[0].BillingAddress;
                $scope.mBilling.TIN = $scope.default_pks[0].NPWP;
                // set default pks --------------------------------------------- end



			}

            $scope.getPPN($scope.mBilling.TIN)

            // cek kl dia customer, set ulang DP dan DPPaid nya. selain customer 0 in kata pa dodi
            if (item == 28){
                //cek kl data part ato data opb nya ada, baru tampilin dp, tapi kl ga ada ya ga usah tampil dp nya
                if ($scope.partsData.length > 0) { 
                    $scope.onListSelectRowsParts($scope.listSelectedRowsParts); // di dlm sini ada set dp dan dppaid
                }
                if ($scope.opbData.length > 0) { 
                    $scope.onListSelectRowsOpb($scope.listSelectedRowsOpb); // di dlm sini ada set dp dan dppaid
                }

            } else {
                $scope.DP = 0; // req pa dodi 21 September 2020, jika biling bukan charge to customer, dp dan dppaid nya di 0 in
                $scope.DPPaid = 0; // req pa dodi 21 September 2020, jika biling bukan charge to customer, dp dan dppaid nya di 0 in
            }

            if (isManualChange === 1){
                $scope.onListSelectRows($scope.listSelectedRows)
                $scope.onListSelectRowsOPL($scope.listSelectedRowsOPL)
                $scope.onListSelectRowsOpb($scope.listSelectedRowsOpb)
                $scope.onListSelectRowsParts($scope.listSelectedRowsParts)
            }
			
			$scope.mBilling.TINState = $scope.mBilling.TIN;
			$scope.mBilling.KTPState = $scope.mBilling.KTP;
			$scope.mBilling.passportState = $scope.mBilling.passport;
			
			if(item == 28 || item == 32){
				$scope.isDisabledNat = false;

				$scope.mBilling.TIN = $scope.dataCRM[0].Npwp;
				$scope.mBilling.KTP = $scope.dataCRM[0].KTPKITAS;

				$scope.onChangeNat(tmp_mBillingNatTypeData);
			}else{
				$scope.isDisabledNat = true;

				delete $scope.mBilling.Nationality;

				$scope.mBilling.Nationality = undefined;

				$timeout(function () {
					$('.checkBoxKTP').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
				}, 100);
			}
			
			console.log('$scope.modeBil at selectedpayment stv : ' , $scope.modeBil);

			if(item == 32){
				$scope.mBilling.TIN = '00.000.000.0-000.000';
				$scope.mBilling.TINRaw = '00.000.000.0-000.000';
				$scope.mBilling.KTP = '';
				$scope.mBilling.KTPRaw = '';
				$scope.mBilling.passport = '';
				$scope.mBilling.passportRaw = '';
			}
        };

        AppointmentGrService.getTaskCategory().then(function(res) {
            $scope.unitData = res.data.Result;
        });

        var tmp_mBillingDataPayment = 0;
        $scope.onShowDetail = function(data, mode) {
            $scope.TimeStartBilling = new Date()
            $scope.getPPN()
            $scope.countCreateBilling = 0;
            console.log("Lihat Biling", data);
            console.log("Mode ==>", mode);
            $scope.modeBil = mode;
            $scope.cekAsuransiFromMainGrid = angular.copy(data.InsuranceId);
            $scope.disMode = true;
            console.log("onShowDetail | mBilling ====>", $scope.mBilling);
            // console.log("VehicleId", data.Job.VehicleId);
            // console.log("policeNumber", data.Job.PoliceNumber);
            $scope.bilingId = data.BillingId;
            $scope.choseData = (data.Job != undefined || data.Job != null ? (data.Job.Process != null ? data.Job.Process.Name : "") : "");
            console.log('choseData------view', $scope.choseData);
            $scope.dataInsurance = [];
            $scope.subTotalLabor = 0;
            $scope.subTotalParts = 0;
            $scope.discountPercentFinalTask = 0;
            $scope.discountFinalTask = 0;
            $scope.subTotalOpb = 0;
            $scope.subTotalOpl = 0;
            $scope.discountPercentFinalParts = 0;
            $scope.discountFinalParts = 0;
            $scope.discountFinalOpl = 0;
            $scope.discountPercentFinalOpl = 0;
            $scope.discountFinalPartsOpb = 0;
            $scope.TotalInvoice = 0;
            $scope.TotalYangHarusDibayar = 0;
            $scope.PPN = 0;
            $scope.discountPercentFinalOpb = 0;
            $scope.TotalAfterDiscountFinal = 0;
            $scope.tax = 0;
            $scope.Materai = 0;
            $scope.DP = 0;
            $scope.DPPaid = 0;
            $scope.TotalFinal = 0;
            $scope.discountOPl = 0;
            $scope.listApiJob.clearSelected();
            $scope.listApiParts.clearSelected();
            $scope.listApiOpl.clearSelected();
            $scope.listApiOpb.clearSelected();

            var TotalFinal = 0;
            var tempInsuranceId = 0;

            if (data.InsuranceId != undefined || data.InsuranceId != null) {
                tempInsuranceId = data.InsuranceId;
            }

            if (data.Job != undefined || data.Job != null) {
                tempInsuranceId = data.Job.InsuranceId != undefined ? data.Job.InsuranceId : data.InsuranceId;
            }
            BillingFactory.getInsuranceProfile(tempInsuranceId).then(function(resu) {
                $scope.dataInsurance = resu.data.Result;
                console.log("onShowDetail | data Insurance ===>", $scope.dataInsurance);
            });

            BillingFactory.getCOutletById2().then(function(resu) {
                $scope.dataOutlet = resu.data.Result;
                console.log("onShowDetail | data dataOutlet ===>", $scope.dataOutlet);
            });

            BillingFactory.getPayment()
                .then(
                    function(res) {
                        // $scope.DataPemeriksaanAwal = res.data.Result;
                        if (data.isGr || data.isGR == 1) {
                            var tmpPayment = [];
                            _.map(res.data.Result, function(val) {
                                if (val.Name !== 'Insurance') {
                                    tmpPayment.push(val);
                                }
                            })
                            $scope.dataPayment = tmpPayment;
                        } else {
                            $scope.dataPayment = res.data.Result;
                        }

                        console.log("PAYMENT ===>", $scope.dataPayment);
                        // console.log("data CRM", $scope.dataCRM);
                        // console.log("First Check", $scope.DataPemeriksaanAwal);
                        // $scope.loading=false;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

            var tmpVehicleId = data.VehicleId;
            BillingFactory.getDataCRM(tmpVehicleId).then(function(res) {
                    // $scope.DataPemeriksaanAwal = res.data.Result;
                    $scope.dataCRM = res.data.Result;
                    console.log("data CRM", $scope.dataCRM);
                    if ($scope.dataCRM.length > 0) {
						
						// CR5 #27 CRM View
                        $scope.mBilling.dataCustOLD = $scope.dataCRM[0].Name;
						$scope.mBilling.CustomerId = $scope.dataCRM[0].CustomerId
						$scope.mBilling.TINRaw = $scope.dataCRM[0].Npwp;

						$scope.mBilling.IsWNARaw = $scope.dataCRM[0].IsWNA;

						if($scope.dataCRM[0].IsWNA == null){
							delete $scope.mBilling.Nationality;
							console.log('$scope.mBilling.Nationality after delete at CRM: ', $scope.mBilling.Nationality);

							$scope.mBilling.Nationality = undefined;
							console.log('$scope.mBilling.Nationality after set at CRM: ', $scope.mBilling.Nationality);

							$scope.mBilling.KTP = $scope.dataCRM[0].KTPKITAS;
							$scope.mBilling.KTPRaw = $scope.dataCRM[0].KTPKITAS;

							$scope.mBilling.passport = $scope.dataCRM[0].KTPKITAS;
							$scope.mBilling.passportRaw = $scope.dataCRM[0].KTPKITAS;
						}else if($scope.dataCRM[0].IsWNA == 0){
							//WNI
							$scope.mBilling.Nationality = { Id: 0, Name: 'WNI' };

							$scope.mBilling.passport = '0000000000000000';
							$scope.mBilling.passportRaw = '0000000000000000';

							if($scope.dataCRM[0].Npwp == null || $scope.dataCRM[0].Npwp == "" || $scope.dataCRM[0].Npwp == '000000000000000' || $scope.dataCRM[0].Npwp == undefined){
								// WNI -> Tidak punya NPWP
								//$scope.mBilling.isNPWP = 1;

								$scope.mBilling.KTP = $scope.dataCRM[0].KTPKITAS;
								$scope.mBilling.KTPRaw = $scope.dataCRM[0].KTPKITAS;
							}else{
								// WNI -> NPWP
								//$scope.mBilling.isNPWP = 0;

								if($scope.dataCRM[0].KTPKITAS == null || $scope.dataCRM[0].KTPKITAS == "" || $scope.dataCRM[0].KTPKITAS == '0000000000000000' || $scope.dataCRM[0].KTPKITAS == undefined){
									$scope.mBilling.KTP = '0000000000000000';
									$scope.mBilling.KTPRaw = '0000000000000000';
								}else{
									$scope.mBilling.KTP = $scope.dataCRM[0].KTPKITAS;
									$scope.mBilling.KTPRaw = $scope.dataCRM[0].KTPKITAS;
								}
							}
						}else if($scope.dataCRM[0].IsWNA == 1){
							//WNA
							$scope.mBilling.Nationality = { Id: 1, Name: 'WNA' };

							$scope.mBilling.KTP = '0000000000000000';
							$scope.mBilling.KTPRaw = '0000000000000000';

							//$scope.mBilling.isNPWP = 1;

							$scope.mBilling.passport = $scope.dataCRM[0].KTPKITAS;
							$scope.mBilling.passportRaw = $scope.dataCRM[0].KTPKITAS;							
						}

						console.log('$scope.mBilling.Nationality at onShowDetail', $scope.mBilling.Nationality);
						console.log('$scope.mBilling.KTP at onShowDetail', $scope.mBilling.KTP);
						console.log('$scope.mBilling.passport at onShowDetail', $scope.mBilling.passport);

						console.log('$scope.mBilling.KTP Awal at onShowDetail', $scope.mBilling.KTPRaw);
						console.log('$scope.mBilling.passport Awal at onShowDetail', $scope.mBilling.passportRaw);

						console.log('$scope.mBilling.TIN Awal at onShowDetail', $scope.mBilling.TINRaw);

						$timeout(function () {
							$('.checkBoxKTP').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
						}, 100);
						
						if($scope.mBilling.Nationality == undefined || $scope.mBilling.Nationality == null || $scope.mBilling.Nationality == ""){
							tmp_mBillingNatTypeDataV = 0;
						}else{
							tmp_mBillingNatTypeDataV = $scope.mBilling.Nationality.Id;
						}

						$scope.showNPWP = true;
						$scope.showPassport = false;
						$scope.showKTP = false;

						$scope.mBilling.isNPWP = 0;
						// CR5 #27 //
						
                        BillingFactory.getListOutlet(null, 2).then(function(resu) {
                            $scope.freeServiceData = resu.data.Result;
                        });
                        BillingFactory.getListOutlet(null, 4).then(function(resu) {
                            $scope.warantyData = resu.data.Result;
                        });
                    }
                    $scope.mBilling.ToyotaId = ($scope.dataCRM[0] != null ? ($scope.dataCRM[0].ToyotaId != null ? $scope.dataCRM[0].ToyotaId : '-') : '-');

                    console.log(' onShowDetail | freeServiceData ====>',$scope.freeServiceData)
                    console.log(' onShowDetail | freeServiceData ====>',$scope.freeServiceData)
                    console.log(' onShowDetail | mBilling.ToyotaId===>',$scope.mBilling.ToyotaId)


                    // $scope.mBilling.BillTo = $scope.dataCRM[0].Name;
                    // $scope.mBilling.BillAddress = $scope.dataCRM[0].Address;

                    // $scope.mBilling.TIN = $scope.dataCRM[0].Npwp;
                    // $scope.mBilling.TaxName = $scope.dataCRM[0].Name;
                    // $scope.mBilling.TaxAddress = $scope.dataCRM[0].Address;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

            var tmpPoliceNumber = data.PoliceNumber;
            BillingFactory.getDataPKSByOutletId(tmpPoliceNumber).then(function(res) {
                    var dataPKS = res.data.Result;
                    console.log('Data PKS ', dataPKS);

                    $scope.NoPKS = "";
                    $scope.NamaPKS = "";
                    $scope.TOP = "";
                    $scope.Outstanding = "";

                    if (dataPKS != undefined || dataPKS != null) {
                        if (dataPKS.length > 0) {
                            $scope.NoPKS = dataPKS[0].NoPKS;
                            $scope.NamaPKS = dataPKS[0].CompanyName;
                            $scope.TOP = dataPKS[0].CustomerPKSTop;
                            $scope.Outstanding = dataPKS[0].AvailableBalanceBilling;
                            //$scope.Outstanding = dataPKS[0].AvailableBalance;
                        }
                    }
                },
                function(err) {
                    console.log("err=>", err);
                }

            );

            BillingFactory.getClaim(data.JobId).then(function(res) {
                    var dataAsuransi = res.data.Result;
                    console.log('Data dataAsuransi ', dataAsuransi);

                    $scope.PolicyNo = "";
                    $scope.StartPeriod = "";
                    $scope.EndPeriod = "";

                    if (dataAsuransi != undefined || dataAsuransi != null) {
                        if (dataAsuransi.length > 0) {
                            $scope.PolicyNo = dataAsuransi[0].PolicyNo;
                            $scope.StartPeriod = dataAsuransi[0].StartPeriod;
                            $scope.EndPeriod = dataAsuransi[0].EndPeriod;
                        }
                    }
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

            if (mode == 'view') {
                BillingFactory.getDataEmployeeSa(data.JobId).then(function(res) {
                        console.log("datanya 1 =>", res);
                        $scope.gridWork = [];
                        $scope.partsData = [];
                        $scope.oplData = [];
                        $scope.opbData = [];
                        // $scope.DataPemeriksaanAwal = res.data.Result;
                        // if(mode !== 'view'){
                        $scope.dataByJobId = res.data.Result;
                        $scope.temp_dataJobList = angular.copy(res.data.Result[0])
                        // if (res.data.Result[0].OutletId < 2000000) {
                        //     $scope.getTypeJobFreeClaim = 0;
                        //     for (var i = 0; i < res.data.Result[0].JobTask.length; i++) {
                        //         for (var j = 0; j < res.data.Result[0].JobTask[i].JobParts.length; j++) {
                        //             if (res.data.Result[0].JobTask[i].JobParts[j].isDeleted == 0) {
                        //                 var getdataIdClaimType = angular.copy(res.data.Result[0].JobTask[i].JobType.MasterId);
                        //                 // 2675 FPC - Free Parts Claim || 2677 FLC - Free Labor Clai || 2678 PLC - Parts Labor Claim
                        //                 if (getdataIdClaimType == 2675 || getdataIdClaimType == 2677 || getdataIdClaimType == 2678) {
                        //                     $scope.getTypeJobFreeClaim++;
                        //                 }else{
                        //                     $scope.getTypeJobFreeClaim = 0;
                        //                 }
                        //             }
                        //         }   
                        //     }
                        // }
                        // }
                        $scope.mBilling.dataPayment = $scope.dataByJobId[0].JobTask[0].PaidById;
                        tmp_mBillingDataPayment = angular.copy($scope.mBilling.dataPayment)
                        
						// CR5 #27
						if($scope.mBilling.ChargeToId == 28 || $scope.mBilling.ChargeToId == 32){
							console.log("onCRMView",$scope.dataCRM[0].Npwp);
                            if ($scope.dataCRM.length > 0) {
							    $scope.mBilling.TIN = $scope.dataCRM[0].Npwp;
                            }
							//if($scope.mBilling.dataPayment != 28){
								BillingFactory.getDataBilling($scope.bilingId).then(function(resBil) {
									console.log("DATA BILLINGS stv ===>", resBil.data.Result);
									var dataBil = resBil.data.Result[0];

                                    if (resBil.data.Result[0].taxAmount != undefined && resBil.data.Result[0].taxAmount != null && resBil.data.Result[0].taxAmount != 0) {
                                        PPNPerc = resBil.data.Result[0].taxAmount
                                        $scope.tax = PPNPerc;
                                    }

									if (dataBil !== undefined) {
										$scope.mBilling.TIN = dataBil.TIN;
										$scope.mBilling.TINRaw = dataBil.TIN;
										$scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN);
										$scope.mBilling.KTP = dataBil.KTPKITAS;
										$scope.mBilling.passport = dataBil.KTPKITAS;
										
										console.log('$scope.mBilling.KTP at onShowDetail bil', $scope.mBilling.KTP);
										console.log('$scope.mBilling.passport at onShowDetail bil', $scope.mBilling.passport);

										console.log('$scope.mBilling.KTP Awal at onShowDetail bil', $scope.mBilling.KTPRaw);
										console.log('$scope.mBilling.passport Awal at onShowDetail bil', $scope.mBilling.passportRaw);

										console.log('$scope.mBilling.TIN Awal at onShowDetail', $scope.mBilling.TINRaw);

										/*if(dataBil.TIN == undefined || dataBil.TIN == null || dataBil.TIN == "" || dataBil.TIN == '000000000000000'){
											$scope.mBilling.TIN = '00.000.000.0-000.000';
											$scope.mBilling.TINRaw = '00.000.000.0-000.000';
											$scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
										}else{
											$scope.mBilling.TIN = dataBil.TIN;
											$scope.mBilling.TINRaw = dataBil.TIN;
											$scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
										}*/ 

										if(dataBil.IsWNA == 0){
											$scope.mBilling.Nationality = { Id: 0, Name: 'WNI' };
											tmp_mBillingNatTypeDataV = 0;
										}else if(dataBil.IsWNA == 1){
											$scope.mBilling.Nationality = { Id: 1, Name: 'WNA' };
											tmp_mBillingNatTypeDataV = 1;
										}else{
											$scope.mBilling.Nationality = { Id: 1, Name: 'WNA' };
											tmp_mBillingNatTypeDataV = 1;
										}
                                        console.log("$scope.mBilling.Nationality",$scope.mBilling.Nationality);
										console.log('dataBil.isKTP : ', dataBil.isKTP);
										if(dataBil.IsKTP == 0 || dataBil.IsKTP == 1){
											$scope.isKTP = dataBil.IsKTP;
										}

										$scope.onChangeNatView(tmp_mBillingNatTypeDataV);
									}
									
								});
							//}
						}else{
							delete $scope.mBilling.Nationality;

							$scope.mBilling.Nationality = undefined;
							
							console.log("onCRMView A ",$scope.dataCRM[0].Npwp);
                            if ($scope.dataCRM.length > 0) {
							    $scope.mBilling.TIN = $scope.dataCRM[0].Npwp;
                            }
							//if($scope.mBilling.dataPayment != 28){
								BillingFactory.getDataBilling($scope.bilingId).then(function(resBil) {
									console.log("DATA BILLINGS stv ===>", resBil.data.Result);
									var dataBil = resBil.data.Result[0];

                                    if (resBil.data.Result[0].taxAmount != undefined && resBil.data.Result[0].taxAmount != null && resBil.data.Result[0].taxAmount != 0) {
                                        PPNPerc = resBil.data.Result[0].taxAmount
                                        $scope.tax = PPNPerc;
                                    }

									if (dataBil !== undefined) {
										$scope.mBilling.TIN = dataBil.TIN;
										$scope.mBilling.TINRaw = dataBil.TIN;
										$scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN);
										$scope.mBilling.KTP = dataBil.KTPKITAS;
										$scope.mBilling.passport = dataBil.KTPKITAS;
										
										console.log('$scope.mBilling.KTP at onShowDetail bil A ', $scope.mBilling.KTP);
										console.log('$scope.mBilling.passport at onShowDetail bil A ', $scope.mBilling.passport);

										console.log('$scope.mBilling.KTP Awal at onShowDetail bil A ', $scope.mBilling.KTPRaw);
										console.log('$scope.mBilling.passport Awal at onShowDetail bil A ', $scope.mBilling.passportRaw);

										console.log('$scope.mBilling.TIN Awal at onShowDetail A ', $scope.mBilling.TINRaw);

										/*if(dataBil.TIN == undefined || dataBil.TIN == null || dataBil.TIN == "" || dataBil.TIN == '000000000000000'){
											$scope.mBilling.TIN = '00.000.000.0-000.000';
											$scope.mBilling.TINRaw = '00.000.000.0-000.000';
											$scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
										}else{
											$scope.mBilling.TIN = dataBil.TIN;
											$scope.mBilling.TINRaw = dataBil.TIN;
											$scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
										}*/

										console.log('dataBil.isKTP A : ', dataBil.isKTP);
										if(dataBil.IsKTP == 0 || dataBil.IsKTP == 1){
											$scope.isKTP = dataBil.IsKTP;
										}
									}
									
								});
							//}
						}
						// CR5 #27
                        // $scope.SelectedPayment($scope.mBilling.dataPayment);

                        for (i = 0; i < $scope.dataByJobId.length; i++) {
                            console.log("invoiceType", $scope.dataByJobId[i].invoiceType);
                            // Tgl. Billing
                            for (var x=0; x<$scope.dataByJobId[i].Billing.length; x++){
                                if ($scope.dataByJobId[i].Billing[x].BillingNo == data.BillingNo){
                                    $scope.date = $scope.dataByJobId[i].Billing[x].BillingDate;
                                }
                            }

                            if ($scope.dataByJobId[i].invoiceType == 7 || $scope.dataByJobId[i].invoiceType == null) {
                                $scope.tax = PPNPerc;
                            } else {
                                $scope.tax = PPNPerc;
                            }
                            if ($scope.bilingId !== undefined && $scope.bilingId !== null) {
                                BillingFactory.getDataCancel($scope.bilingId).then(function(resBil) {
                                    console.log("DATA BILLING ===>", resBil.data.Result);
                                    var dataBil = resBil.data.Result[0];
                                    if (dataBil !== undefined) {
                                        $scope.mBilling.BillTo = dataBil.BillTo;
                                        $scope.mBilling.BillAddress = dataBil.BillAddress;

                                        // $scope.mBilling.CodeTransaction = dataBil.TaxCode; // di comment 20 januari 2020 karena drop down nya ga kluar wkt view
                                        $scope.mBilling.IdCodeTransaction = $scope.findValueFromArray($scope.codeTransaction,'Code','CodeInvoiceTransactionTaxId',dataBil.TaxCode),
                                        $scope.mBilling.TIN = dataBil.TIN;
                                        $scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
                                        $scope.mBilling.TaxName = dataBil.TaxName;
                                        $scope.mBilling.TaxAddress = dataBil.TaxAddress;
                                        $scope.mBilling.BillPhoneNumber = dataBil.BillPhoneNumber

                                        $scope.getPPN($scope.mBilling.TIN)

                                        if (resBil.data.Result[0].taxAmount != undefined && resBil.data.Result[0].taxAmount != null && resBil.data.Result[0].taxAmount != 0) {
                                            PPNPerc = resBil.data.Result[0].taxAmount
                                            $scope.tax = PPNPerc;
                                        }
                                        //============= NEW CODE && NEW ERA =====
                                        $scope.dataForBsList(dataBil, 1, mode);
                                        console.log('have Billing id', $scope.bilingId)
                                    }
                                });
                            } else {
                                $scope.dataForBsList($scope.dataByJobId[i], 0, mode);
                                console.log('have not Billing id', $scope.bilingId)
                            }
                        }
                        console.log("mode ====>", mode);
                        if (mode !== 'edit') {
                            console.log("total akhir", TotalFinal);

                            console.log("data Opl", $scope.oplData);
                            console.log("gridWirk onShowDetail", $scope.gridWork);
                            console.log("dataparts", $scope.partsData);
                            console.log("data by job id", $scope.dataByJobId);
                            $scope.onListSelectRows($scope.gridWork);
                            $scope.onListSelectRowsOPL($scope.oplData);
                            $scope.onListSelectRowsParts($scope.partsData);
                            $scope.onListSelectRowsOpb($scope.opbData);


                            $scope.mBilling.dataPayment = data.ChargeToId;
                            tmp_mBillingDataPayment = angular.copy($scope.mBilling.dataPayment)
                            // $scope.mBilling.dataPayment = data.ChargeTo;
                            $scope.mBilling.ORPaidAmount = $scope.dataByJobId[0].ORPaidAmount;
                            $scope.mBilling.IsPaidOR = $scope.dataByJobId[0].IsPaidOR;
                            $scope.mBilling.CodeTransaction = $scope.dataByJobId[0].invoiceType;
                            $scope.mBilling.IdCodeTransaction = $scope.findValueFromArray($scope.codeTransaction,'Code','CodeInvoiceTransactionTaxId',$scope.dataByJobId[0].invoiceType),


                            console.log('kode', $scope.mBilling.CodeTransaction);
                        }
                        // $scope.getData(); //0149-SIT-033- DeliveryBilling
						console.log('$scope.modeBil at onshowdetail stv : ' , $scope.modeBil);

						
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }
            $timeout(function() {
                $('.bstypeahead input').attr('ng-maxlength', '255');
            }, 1000);
        }

        $scope.dataForBsList = function(data, param, mode) {
            console.log("dataForBsList | DATA =======>", data);
            console.log("dataForBsList | PARAM =======>", param);
            console.log("dataForBsList | MODE =======>", mode);

            var findIsSPKjob = [];
            for(var i in data.Job.AJobSPK){
                for(var j in data.Job.AJobSPK[i].AJobSPKList){
                    for(var k in data.BillingJob){
                        if(data.Job.AJobSPK[i].AJobSPKList[j].TaskBPId == data.BillingJob[k].JobTask.TaskBPId){
                            findIsSPKjob.push(data.Job.AJobSPK[i].AJobSPKList[j]);
                        }                           
                    }
                }
            }

            console.log('dataForBsList | findIsSPKjob ===>',findIsSPKjob)



            var arrPaidBy = ["CUSTOMER", "INSURANCE", "WARRANTY", "INTERNAL", "FREE SERVICE", "PKS", "THIRD PARTY"];
            if (param == 0) {
                for (j = 0; j < data.JobTask.length; j++) {

                    console.log("Test data.JobTask[j].IsBilling =>", data.JobTask[j].IsBilling);
                    console.log("Test data.JobTask[j].PaidById =>", data.JobTask[j].PaidById);

                    // Special case for IsCustomDiscount = 1 if Charge To Free Service
                    if ((data.JobTask[j].PaidBy.Name.toUpperCase() == arrPaidBy[4] && data.JobTask[j].IsCustomDiscount == 0) &&
                        (data.JobTask[j].Discount == null || data.JobTask[j].Discount == 0)) {
                        data.JobTask[j].IsCustomDiscount == 0;
                    }

                    if ((data.JobTask[j].IsBilling >= 1 || data.JobTask[j].IsBilling != null) && (data.JobTask[j].PaidBy.Name.toUpperCase() == arrPaidBy[0] || data.JobTask[j].PaidBy.Name.toUpperCase() == arrPaidBy[5] || data.JobTask[j].PaidBy.Name.toUpperCase() == arrPaidBy[1] || data.JobTask[j].PaidBy.Name.toUpperCase() == arrPaidBy[3]) &&
                        data.JobTask[j].IsCustomDiscount == 0 && data.JobTask[j].isDeleted == 0) {

                        if (data.JobTask[j].Fare !== undefined) {
                            var sum = data.JobTask[j].Fare;
                            var summ = 0;
                            if (data.JobTask[j].FlatRate == null || data.JobTask[j].FlatRate == undefined) {
                                summ = 1;
                            } else {
                                summ = data.JobTask[j].FlatRate;
                            }

                            var summaryy = Math.round(sum * summ);
                            var discountedPrice = (summaryy * data.JobTask[j].Discount) / 100;
                            var normalPrice = summaryy
                            summaryy = parseInt(summaryy);
                        }
                        if (data.JobTask[j].JobType == null) {
                            data.JobTask[j].JobType = { Name: "" };
                        }
                        if (data.JobTask[j].PaidBy == null) {
                            data.JobTask[j].PaidBy = { Name: "" }
                        }
                        if (data.JobTask[j].AdditionalTaskId == null) {
                            data.JobTask[j].isOpe = 0;
                        } else {
                            data.JobTask[j].isOpe = 1;
                        }
                        if (data.JobTask[j].IsCustomDiscount == 0 && data.JobTask[j].Discount == 0) {
                            data.JobTask[j].typeDiskon = -1;
                            data.JobTask[j].DiscountedPrice = normalPrice;
                        } else if (data.JobTask[j].IsCustomDiscount == 1 && data.JobTask[j].Discount !== 0) {
                            data.JobTask[j].typeDiskon = 0;
                            data.JobTask[j].DiscountedPrice = (normalPrice * data.JobTask[j].Discount) / 100;
                        }
                        for (var i in $scope.unitData) {
                            if (data.BillingJob != null || data.BillingJob != undefined) {
                                if ($scope.unitData[i].MasterId == data.BillingJob[j].JobTask.ProcessId) {
                                    data.BillingJob[j].JobTask.satuanName = $scope.unitData[i].Name;
                                }
                            }
                        }
                        // add to gridwork
                        $scope.onPushGridWork(1, data.JobTask[j].IsCustomDiscount, -1,
                            data,
                            data.JobTask[j],
                            data.JobTask[j].AJobSPK[j],
                            data.BillingJob != undefined ? data.JobTask[j] : data.BillingJob,
                            data.BillingJob != undefined ? data.JobTask[j] : data.BillingJob,
                            normalPrice,
                            discountedPrice,
                            summaryy);

                    } else if ((data.JobTask[j].IsBilling >= 1 || data.JobTask[j].IsBilling != null) && (data.JobTask[j].PaidBy.Name.toUpperCase() == arrPaidBy[0] || data.JobTask[j].PaidBy.Name.toUpperCase() == arrPaidBy[5] || data.JobTask[j].PaidBy.Name.toUpperCase() == arrPaidBy[1] || data.JobTask[j].PaidBy.Name.toUpperCase() == arrPaidBy[3]) &&
                        data.JobTask[j].IsCustomDiscount == 1 && data.JobTask[j].isDeleted == 0) {
                        // Checking for approval discount
                        if (data.JobTask[j].ApprovalDiscountTransTR.length > 0 || data.JobTask[j].ApprovalDiscountTransTR != null) {
                            if (data.JobTask[j].Fare !== undefined) {
                                var sum = data.JobTask[j].Fare;
                                var summ = 0;
                                if (data.JobTask[j].FlatRate == null || data.JobTask[j].FlatRate == undefined) {
                                    summ = 1;
                                } else {
                                    summ = data.JobTask[j].FlatRate;
                                }

                                var summaryy = Math.round(sum * summ);
                                var discountedPrice = (summaryy * data.JobTask[j].Discount) / 100;
                                var normalPrice = summaryy
                                summaryy = parseInt(summaryy);
                            }
                            if (data.JobTask[j].JobType == null) {
                                data.JobTask[j].JobType = { Name: "" };
                            }
                            if (data.JobTask[j].PaidBy == null) {
                                data.JobTask[j].PaidBy = { Name: "" }
                            }
                            if (data.JobTask[j].AdditionalTaskId == null) {
                                data.JobTask[j].isOpe = 0;
                            } else {
                                data.JobTask[j].isOpe = 1;
                            }
                            if (data.JobTask[j].IsCustomDiscount == 0 && data.JobTask[j].Discount == 0) {
                                data.JobTask[j].typeDiskon = -1;
                                data.JobTask[j].DiscountedPrice = normalPrice;
                            } else if (data.JobTask[j].IsCustomDiscount == 1 && data.JobTask[j].Discount !== 0) {
                                data.JobTask[j].typeDiskon = 0;
                                data.JobTask[j].DiscountedPrice = (normalPrice * data.JobTask[j].Discount) / 100;
                            }
                            for (var i in $scope.unitData) {
                                if (data.BillingJob != undefined || data.BillingJob != null) {
                                    if ($scope.unitData[i].MasterId == data.BillingJob[j].JobTask.ProcessId) {
                                        data.BillingJob[j].JobTask.satuanName = $scope.unitData[i].Name;
                                    }
                                }
                            }

                            if (data.JobTask[j].ApprovalDiscountTransTR[0].StatusApprovalId == -1) {
                                // add to gridwork
                                $scope.onPushGridWork(1, data.JobTask[j].IsCustomDiscount,
                                    data.JobTask[j].ApprovalDiscountTransTR[0].StatusApprovalId,
                                    data,
                                    data.JobTask[j],
                                    data.JobTask[j].AJobSPK[j],
                                    data.BillingJob != undefined ? data.JobTask[j] : data.BillingJob,
                                    data.BillingJob != undefined ? data.JobTask[j] : data.BillingJob,
                                    normalPrice,
                                    discountedPrice,
                                    summaryy);
                            }

                            if (data.JobTask[j].ApprovalDiscountTransTR[0].StatusApprovalId == 1 || data.JobTask[j].ApprovalDiscountTransTR[0].StatusApprovalId == 2 || data.JobTask[j].ApprovalDiscountTransTR[0].StatusApprovalId == 3) {
                                // add to gridwork
                                $scope.onPushGridWork(1, data.JobTask[j].IsCustomDiscount,
                                    data.JobTask[j].ApprovalDiscountTransTR[0].StatusApprovalId,
                                    data,
                                    data.JobTask[j],
                                    data.JobTask[j].AJobSPK[j],
                                    data.BillingJob != undefined ? data.JobTask[j] : data.BillingJob,
                                    data.BillingJob != undefined ? data.JobTask[j] : data.BillingJob,
                                    normalPrice,
                                    discountedPrice,
                                    summaryy);
                            }
                        }

                    } else if ((data.JobTask[j].IsBilling >= 1 || data.JobTask[j].IsBilling != null) && (data.JobTask[j].PaidBy.Name.toUpperCase() === arrPaidBy[2]) &&
                        data.JobTask[j].isDeleted == 0) { // Warranty
                        // } else if ((data.JobTask[j].IsBilling < 1 || data.JobTask[j].IsBilling == null) && data.JobTask[j].PaidById == 30 && data.JobTask[j].TOWASSClaimResult[0].ClaimStatus == 6) {
                        // $scope.gridWork.push(data.JobTask[j]);
                        // Added by Fyberz 2017-11-29  --------------------------
                        console.log('bisa masuk mik kesini', data.JobTask[j]);
                        if (data.JobTask[j].TOWASSClaimResult != null) {
                            if (data.JobTask[j].TOWASSClaimResult.length > 0) {
                                // if (data.JobTask[j].TOWASSClaimResult != null) {
                                console.log('bisa masuk mik kesini yg towass na oke');
                                if (data.JobTask[j].TOWASSClaimResult[0].ClaimStatus == 6) {
                                    console.log('bisa masuk mik kesini yg ada towass na');
                                    if (data.JobTask[j].Fare !== undefined) {
                                        var sum = data.JobTask[j].Fare;
                                        var summ = 0;
                                        if (data.JobTask[j].FlatRate == null || data.JobTask[j].FlatRate == undefined) {
                                            summ = 1;
                                        } else {
                                            summ = data.JobTask[j].FlatRate;
                                        }

                                        var summaryy = Math.round(sum * summ);
                                        var discountedPrice = (summaryy * data.JobTask[j].Discount) / 100;
                                        var normalPrice = summaryy
                                        summaryy = parseInt(summaryy);
                                    }
                                    if (data.JobTask[j].JobType == null) {
                                        data.JobTask[j].JobType = { Name: "" };
                                    }
                                    if (data.JobTask[j].PaidBy == null) {
                                        data.JobTask[j].PaidBy = { Name: "" }
                                    }
                                    if (data.JobTask[j].AdditionalTaskId == null) {
                                        data.JobTask[j].isOpe = 0;
                                    } else {
                                        data.JobTask[j].isOpe = 1;
                                    }
                                    if (data.JobTask[j].IsCustomDiscount == 0 && data.JobTask[j].Discount == 0) {
                                        data.JobTask[j].typeDiskon = -1;
                                        data.JobTask[j].DiscountedPrice = normalPrice;
                                    } else if (data.JobTask[j].IsCustomDiscount == 1 && data.JobTask[j].Discount !== 0) {
                                        data.JobTask[j].typeDiskon = 0;
                                        data.JobTask[j].DiscountedPrice = (normalPrice * data.JobTask[j].Discount) / 100;
                                    }
                                    for (var i in $scope.unitData) {
                                        if (data.BillingJob != undefined || data.BillingJob != null) {
                                            if ($scope.unitData[i].MasterId == data.BillingJob[j].JobTask.ProcessId) {
                                                data.BillingJob[j].JobTask.satuanName = $scope.unitData[i].Name;
                                            }
                                        }
                                    }

                                    // ===============cek Task ini ada SPK nya nggak?=====================
                                    var isSPKHide = true;
                                    var discountTaskFromInsurance = 0;
                                    if(data.BillingJob[j].JobTask.PaidById == 29 ){
                                        if($scope.dataInsurance.length > 0){
                                            discountTaskFromInsurance = $scope.dataInsurance[0].DiscountJasa;
                                            var findSPKindex = _.findIndex(findIsSPKjob,function(o){
                                                return o.TaskBPId == data.BillingJob[j].JobTask.TaskBPId
                                            })
                                            var findSPKData = {};
                                            if(findSPKindex == -1){
                                                isSPKHide = true;
                                            }else{
                                                findSPKData = _.find(findIsSPKjob,function(o){
                                                    return o.TaskBPId == data.BillingJob[j].JobTask.TaskBPId
                                                })
                                                isSPKHide = false;
                                            }
                                            console.log('dataForBsList | findSPKindex | isSPKHide ===>',findSPKindex, isSPKHide);
                                        }
                                    }
                                    // ===============cek Task ini ada SPK nya nggak?=====================


                                    $scope.gridWork.push({
                                        ActualRate: data.JobTask[j].ActualRate,
                                        AdditionalTaskId: data.JobTask[j].AdditionalTaskId,
                                        isOpe: data.JobTask[j].isOpe,
                                        Discount: data.JobTask[j].Discount,
                                        JobTaskId: data.JobTask[j].JobTaskId,
                                        JobTypeId: data.JobTask[j].JobTypeId,
                                        JobTypeName: data.JobTask[j].JobType.Name,
                                        DiscountedPrice: discountedPrice,
                                        IsBilling: data.JobTask[j].IsBilling,
                                        typeDiskon: data.JobTask[j].typeDiskon,
                                        NormalPrice: normalPrice,
                                        // catName: data.JobTask[j].JobType.Name,
                                        catName: data.BillingJob[j].JobTask.satuanName,
                                        Fare: data.JobTask[j].Fare,
                                        IsCustomDiscount: data.JobTask[j].IsCustomDiscount,
                                        Summary: summaryy,
                                        PaidById: data.JobTask[j].PaidById,
                                        JobId: data.JobTask[j].JobId,
                                        TaskId: data.JobTask[j].TaskId,
                                        TaskName: data.JobTask[j].TaskName,
                                        FlatRate: data.JobTask[j].FlatRate,
                                        ProcessId: data.BillingJob[j].JobTask.ProcessId,
                                        TFirst1: data.JobTask[j].TFirst1,
                                        PaidBy: data.JobTask[j].PaidBy.Name,

                                        DiscountJasa: discountTaskFromInsurance,
                                        SpkHide: isSPKHide,
                                        SpkNo: isSPKHide == false? findSPKData.SpkNo : '',
                                        ORAmount: isSPKHide == false? findSPKData.ORAmount : '',
                                        // DiscountJasa: data.Insurance != undefined || data.Insurance != null ? data.Insurance.DiscountJasa : 0,
                                        // SpkNo: data.AJobSPK == undefined || data.AJobSPK == null ? '' : data.AJobSPK[j].SpkNo,
                                        // ORAmount: data.AJobSPK == undefined || data.AJobSPK == null ? 0 : data.AJobSPK[j].ORAmount,
                                        // SpkHide: data.AJobSPK == undefined || data.AJobSPK == null ? true : false 
                                    });
                                };
                            } else {
                                console.log('bisa masuk mik kesini yg gk ada data towass na');

                            }
                        }

                    } else if ((data.JobTask[j].IsBilling >= 1 || data.JobTask[j].IsBilling != null) && data.JobTask[j].PaidBy.Name.toUpperCase() !== arrPaidBy[0] && data.JobTask[j].PaidBy.Name.toUpperCase() != arrPaidBy[2] &&
                        data.JobTask[j].IsCustomDiscount == 0 &&
                        data.JobTask[j].isDeleted == 0) { // Customer, Warranty
                        // $scope.gridWork.push(data.JobTask[j]);
                        // Added by Fyberz 2017-11-29  --------------------------

                        if (data.JobTask[j].Fare !== undefined) {
                            var sum = data.JobTask[j].Fare;
                            var summ = 0;
                            if (data.JobTask[j].FlatRate == null || data.JobTask[j].FlatRate == undefined) {
                                summ = 1;
                            } else {
                                summ = data.JobTask[j].FlatRate;
                            }

                            var summaryy = Math.round(sum * summ);
                            var discountedPrice = (summaryy * data.JobTask[j].Discount) / 100;
                            var normalPrice = summaryy
                            summaryy = parseInt(summaryy);
                        }
                        if (data.JobTask[j].JobType == null) {
                            data.JobTask[j].JobType = { Name: "" };
                        }
                        if (data.JobTask[j].PaidBy == null) {
                            data.JobTask[j].PaidBy = { Name: "" }
                        }
                        if (data.JobTask[j].AdditionalTaskId == null) {
                            data.JobTask[j].isOpe = 0;
                        } else {
                            data.JobTask[j].isOpe = 1;
                        }
                        if (data.JobTask[j].IsCustomDiscount == 0 && data.JobTask[j].Discount == 0) {
                            data.JobTask[j].typeDiskon = -1;
                            data.JobTask[j].DiscountedPrice = normalPrice;
                        } else if (data.JobTask[j].IsCustomDiscount == 1 && data.JobTask[j].Discount !== 0) {
                            data.JobTask[j].typeDiskon = 0;
                            data.JobTask[j].DiscountedPrice = (normalPrice * data.JobTask[j].Discount) / 100;
                        }
                        for (var i in $scope.unitData) {
                            if (data.BillingJob != undefined || data.BillingJob != null) {
                                if ($scope.unitData[i].MasterId == data.BillingJob[j].JobTask.ProcessId) {
                                    data.BillingJob[j].JobTask.satuanName = $scope.unitData[i].Name;
                                }
                            }
                        }
                        $scope.gridWork.push({
                            ActualRate: data.JobTask[j].ActualRate,
                            AdditionalTaskId: data.JobTask[j].AdditionalTaskId,
                            isOpe: data.JobTask[j].isOpe,
                            Discount: data.JobTask[j].Discount,
                            JobTaskId: data.JobTask[j].JobTaskId,
                            JobTypeId: data.JobTask[j].JobTypeId,
                            JobTypeName: data.JobTask[j].JobType.Name,
                            DiscountedPrice: discountedPrice,
                            IsBilling: data.JobTask[j].IsBilling,
                            typeDiskon: data.JobTask[j].typeDiskon,
                            NormalPrice: normalPrice,
                            // catName: data.JobTask[j].JobType.Name,
                            catName: (data.BillingJob != null || data.BillingJob != undefined ? data.BillingJob[j].JobTask.satuanName : ""),
                            Fare: data.JobTask[j].Fare,
                            IsCustomDiscount: data.JobTask[j].IsCustomDiscount,
                            Summary: summaryy,
                            PaidById: data.JobTask[j].PaidById,
                            JobId: data.JobTask[j].JobId,
                            TaskId: data.JobTask[j].TaskId,
                            TaskName: data.JobTask[j].TaskName,
                            FlatRate: data.JobTask[j].FlatRate,
                            ProcessId: (data.BillingJob != null || data.BillingJob != undefined ? data.BillingJob[j].JobTask.ProcessId : data.JobTask[j].ProcessId),
                            TFirst1: data.JobTask[j].TFirst1,
                            PaidBy: data.JobTask[j].PaidBy.Name,
                            DiscountJasa: data.Insurance != undefined || data.Insurance != null ? data.Insurance.DiscountJasa : 0,
                            SpkNo: data.AJobSPK == undefined || data.AJobSPK == null ? '' : data.AJobSPK[j].SpkNo,
                            ORAmount: data.AJobSPK == undefined || data.AJobSPK == null ? 0 : data.AJobSPK[j].ORAmount,
                            SpkHide: data.AJobSPK == undefined || data.AJobSPK == null ? true : false
                        });
                    } else {
                        console.log("masuk else JobTask sudah Billing");
                    }

                    for (a = 0; a < data.JobTask[j].JobParts.length; a++) {

                        // Special case for IsCustomDiscount = 1 if Charge To Free Service
                        if ((data.JobTask[j].JobParts[a].PaidBy.Name.toUpperCase() == arrPaidBy[4] && data.JobTask[j].JobParts[a].IsCustomDiscount == 0) &&
                            (data.JobTask[j].JobParts[a].Discount == null || data.JobTask[j].JobParts[a].Discount == 0)) {
                            data.JobTask[j].JobParts[a].IsCustomDiscount == 0;
                        }

                        if (data.JobTask[j].JobParts[a].IsBilling < 1) {
                            if (data.JobTask[j].JobParts[a].IsOPB != 1) {
                                if ((data.JobTask[j].JobParts[a].PaidBy.Name.toUpperCase() == arrPaidBy[0] || data.JobTask[j].JobParts[a].PaidBy.Name.toUpperCase() == arrPaidBy[5] || data.JobTask[j].JobParts[a].PaidBy.Name.toUpperCase() == arrPaidBy[1] || data.JobTask[j].JobParts[a].PaidBy.Name.toUpperCase() == arrPaidBy[3]) &&
                                    data.JobTask[j].JobParts[a].IsCustomDiscount == 0 &&
                                    data.JobTask[j].JobParts[a].isDeleted == 0) {
                                    if ((data.JobTask[j].JobParts[a].Price == null || data.JobTask[j].JobParts[a].Price == 0)) {
                                        data.JobTask[j].JobParts[a].Price = data.JobTask[j].JobParts[a].Part.RetailPrice;
                                    }
                                    // $scope.partsData.push(data.JobTask[j].JobParts[a]);

                                    console.log('Masukin pars 1 ===>', $scope.partsData)
                                    $scope.partsData.push({
                                        JobPartsId: data.JobTask[j].JobParts[a].JobPartsId,
                                        JobTaskId: data.JobTask[j].JobParts[a].JobTaskId,
                                        PartsId: data.JobTask[j].JobParts[a].PartsId,
                                        PartsName: data.JobTask[j].JobParts[a].PartsName,
                                        Price: data.JobTask[j].JobParts[a].Price,
                                        Status: data.JobTask[j].JobParts[a].Status,
                                        Qty: data.JobTask[j].JobParts[a].Qty,
                                        DownPayment: data.JobTask[j].JobParts[a].DownPayment,
                                        PaidById: data.JobTask[j].JobParts[a].PaidById,
                                        PaidBy: data.JobTask[j].JobParts[a].PaidBy.Name,
                                        SatuanId: data.JobTask[j].JobParts[a].SatuanId,
                                        Satuan: data.JobTask[j].JobParts[a].Satuan,
                                        IsOfp: data.JobTask[j].JobParts[a].IsOfp,
                                        MaterialRequestStatusId: data.JobTask[j].JobParts[a].MaterialRequestStatusId,
                                        QtyGI: data.JobTask[j].JobParts[a].QtyGI,
                                        QtyPrepicking: data.JobTask[j].JobParts[a].QtyPrepicking,
                                        QtyGIReserved: data.JobTask[j].JobParts[a].QtyGIReserved,
                                        QtyReserved: data.JobTask[j].JobParts[a].QtyReserved,
                                        IsOPB: data.JobTask[j].JobParts[a].IsOPB,
                                        SeqOPB: data.JobTask[j].JobParts[a].SeqOPB,
                                        OrderType: data.JobTask[j].JobParts[a].OrderType,
                                        QtyOrder: data.JobTask[j].JobParts[a].QtyOrder,
                                        MaterialTypeId: data.JobTask[j].JobParts[a].MaterialTypeId,
                                        IsDeleted: data.JobTask[j].JobParts[a].isDeleted,
                                        Discount: data.JobTask[j].JobParts[a].Discount,
                                        DiscountPart: data.Insurance != undefined || data.Insurance != null ? data.Insurance.DiscountPart : 0,
                                        IsCustomDiscount: data.JobTask[j].JobParts[a].IsCustomDiscount,
                                        DiscountTypeId: data.JobTask[j].JobParts[a].DiscountTypeId,
                                        DiscountTypeListId: data.JobTask[j].JobParts[a].DiscountTypeListId
                                    });
                                } else if ((data.JobTask[j].JobParts[a].PaidBy.Name.toUpperCase() == arrPaidBy[0] || data.JobTask[j].JobParts[a].PaidBy.Name.toUpperCase() == arrPaidBy[5] || data.JobTask[j].JobParts[a].PaidBy.Name.toUpperCase() == arrPaidBy[1] || data.JobTask[j].JobParts[a].PaidBy.Name.toUpperCase() == arrPaidBy[3]) &&
                                    data.JobTask[j].JobParts[a].IsCustomDiscount == 1 &&
                                    data.JobTask[j].JobParts[a].isDeleted == 0) {
                                    // Checking for approval discount
                                    if (data.JobTask[j].JobParts[a].ApprovalDiscountTransTR.length > 0 || data.JobTask[j].JobParts[a].ApprovalDiscountTransTR != null) {
                                        if ((data.JobTask[j].JobParts[a].Price == null || data.JobTask[j].JobParts[a].Price == 0)) {
                                            data.JobTask[j].JobParts[a].Price = data.JobTask[j].JobParts[a].Part.RetailPrice;
                                        }

                                        if (data.JobTask[j].JobParts[a].ApprovalDiscountTransTR[0].StatusApprovalId == -1) {
                                            // $scope.partsData.push(data.JobTask[j].JobParts[a]);
                                            console.log('Masukin pars 2 ===>', $scope.partsData)
                                            $scope.partsData.push({
                                                JobPartsId: data.JobTask[j].JobParts[a].JobPartsId,
                                                JobTaskId: data.JobTask[j].JobParts[a].JobTaskId,
                                                PartsId: data.JobTask[j].JobParts[a].PartsId,
                                                PartsName: data.JobTask[j].JobParts[a].PartsName,
                                                Price: data.JobTask[j].JobParts[a].Price,
                                                Status: data.JobTask[j].JobParts[a].Status,
                                                Qty: data.JobTask[j].JobParts[a].Qty,
                                                DownPayment: data.JobTask[j].JobParts[a].DownPayment,
                                                PaidById: data.JobTask[j].JobParts[a].PaidById,
                                                PaidBy: data.JobTask[j].JobParts[a].PaidBy.Name,
                                                SatuanId: data.JobTask[j].JobParts[a].SatuanId,
                                                Satuan: data.JobTask[j].JobParts[a].Satuan,
                                                IsOfp: data.JobTask[j].JobParts[a].IsOfp,
                                                MaterialRequestStatusId: data.JobTask[j].JobParts[a].MaterialRequestStatusId,
                                                QtyGI: data.JobTask[j].JobParts[a].QtyGI,
                                                QtyPrepicking: data.JobTask[j].JobParts[a].QtyPrepicking,
                                                QtyGIReserved: data.JobTask[j].JobParts[a].QtyGIReserved,
                                                QtyReserved: data.JobTask[j].JobParts[a].QtyReserved,
                                                IsOPB: data.JobTask[j].JobParts[a].IsOPB,
                                                SeqOPB: data.JobTask[j].JobParts[a].SeqOPB,
                                                OrderType: data.JobTask[j].JobParts[a].OrderType,
                                                QtyOrder: data.JobTask[j].JobParts[a].QtyOrder,
                                                MaterialTypeId: data.JobTask[j].JobParts[a].MaterialTypeId,
                                                IsDeleted: data.JobTask[j].JobParts[a].isDeleted,
                                                Discount: data.JobTask[j].JobParts[a].Discount,
                                                DiscountPart: data.Insurance != undefined || data.Insurance != null ? data.Insurance.DiscountPart : 0,
                                                IsCustomDiscount: data.JobTask[j].JobParts[a].IsCustomDiscount,
                                                DiscountTypeId: data.JobTask[j].JobParts[a].DiscountTypeId,
                                                DiscountTypeListId: data.JobTask[j].JobParts[a].DiscountTypeListId
                                            });
                                        }

                                        if (data.JobTask[j].JobParts[a].ApprovalDiscountTransTR[0].StatusApprovalId == 1 || data.JobTask[j].JobParts[a].ApprovalDiscountTransTR[0].StatusApprovalId == 2 || data.JobTask[j].JobParts[a].ApprovalDiscountTransTR[0].StatusApprovalId == 3) {
                                            // $scope.partsData.push(data.JobTask[j].JobParts[a]);
                                            console.log('Masukin pars 3 ===>', $scope.partsData)
                                            $scope.partsData.push({
                                                JobPartsId: data.JobTask[j].JobParts[a].JobPartsId,
                                                JobTaskId: data.JobTask[j].JobParts[a].JobTaskId,
                                                PartsId: data.JobTask[j].JobParts[a].PartsId,
                                                PartsName: data.JobTask[j].JobParts[a].PartsName,
                                                Price: data.JobTask[j].JobParts[a].Price,
                                                Status: data.JobTask[j].JobParts[a].Status,
                                                Qty: data.JobTask[j].JobParts[a].Qty,
                                                DownPayment: data.JobTask[j].JobParts[a].DownPayment,
                                                PaidById: data.JobTask[j].JobParts[a].PaidById,
                                                PaidBy: data.JobTask[j].JobParts[a].PaidBy.Name,
                                                SatuanId: data.JobTask[j].JobParts[a].SatuanId,
                                                Satuan: data.JobTask[j].JobParts[a].Satuan,
                                                IsOfp: data.JobTask[j].JobParts[a].IsOfp,
                                                MaterialRequestStatusId: data.JobTask[j].JobParts[a].MaterialRequestStatusId,
                                                QtyGI: data.JobTask[j].JobParts[a].QtyGI,
                                                QtyPrepicking: data.JobTask[j].JobParts[a].QtyPrepicking,
                                                QtyGIReserved: data.JobTask[j].JobParts[a].QtyGIReserved,
                                                QtyReserved: data.JobTask[j].JobParts[a].QtyReserved,
                                                IsOPB: data.JobTask[j].JobParts[a].IsOPB,
                                                SeqOPB: data.JobTask[j].JobParts[a].SeqOPB,
                                                OrderType: data.JobTask[j].JobParts[a].OrderType,
                                                QtyOrder: data.JobTask[j].JobParts[a].QtyOrder,
                                                MaterialTypeId: data.JobTask[j].JobParts[a].MaterialTypeId,
                                                IsDeleted: data.JobTask[j].JobParts[a].isDeleted,
                                                Discount: (data.JobTask[j].JobParts[a].ApprovalDiscountTransTR.StatusApprovalId == 1 ? data.JobTask[j].JobParts[a].Discount : 0),
                                                DiscountPart: data.Insurance != undefined || data.Insurance != null ? data.Insurance.DiscountPart : 0,
                                                IsCustomDiscount: data.JobTask[j].JobParts[a].IsCustomDiscount,
                                                DiscountTypeId: data.JobTask[j].JobParts[a].DiscountTypeId,
                                                DiscountTypeListId: data.JobTask[j].JobParts[a].DiscountTypeListId
                                            });
                                        }

                                    }

                                } else if (data.JobTask[j].JobParts[a].PaidBy.Name.toUpperCase() == arrPaidBy[2] &&
                                    data.JobTask[j].JobParts[a].isDeleted == 0) { //parts warranty masih menunggu approval kabeng, sama seperti tasknya
                                    if (data.JobTask[j].TOWASSClaimResult != null) {
                                        if (data.JobTask[j].TOWASSClaimResult.length > 0) {
                                            if ((data.JobTask[j].JobParts[a].Price == null || data.JobTask[j].JobParts[a].Price == 0)) {
                                                data.JobTask[j].JobParts[a].Price = data.JobTask[j].JobParts[a].Part.RetailPrice;
                                            }
                                            // $scope.partsData.push(data.JobTask[j].JobParts[a]);
                                            console.log('Masukin pars 4 ===>', $scope.partsData)
                                            $scope.partsData.push({
                                                JobPartsId: data.JobTask[j].JobParts[a].JobPartsId,
                                                JobTaskId: data.JobTask[j].JobParts[a].JobTaskId,
                                                PartsId: data.JobTask[j].JobParts[a].PartsId,
                                                PartsName: data.JobTask[j].JobParts[a].PartsName,
                                                Price: data.JobTask[j].JobParts[a].Price,
                                                Status: data.JobTask[j].JobParts[a].Status,
                                                Qty: data.JobTask[j].JobParts[a].Qty,
                                                DownPayment: data.JobTask[j].JobParts[a].DownPayment,
                                                PaidById: data.JobTask[j].JobParts[a].PaidById,
                                                PaidBy: data.JobTask[j].JobParts[a].PaidBy.Name,
                                                SatuanId: data.JobTask[j].JobParts[a].SatuanId,
                                                Satuan: data.JobTask[j].JobParts[a].Satuan,
                                                IsOfp: data.JobTask[j].JobParts[a].IsOfp,
                                                MaterialRequestStatusId: data.JobTask[j].JobParts[a].MaterialRequestStatusId,
                                                QtyGI: data.JobTask[j].JobParts[a].QtyGI,
                                                QtyPrepicking: data.JobTask[j].JobParts[a].QtyPrepicking,
                                                QtyGIReserved: data.JobTask[j].JobParts[a].QtyGIReserved,
                                                QtyReserved: data.JobTask[j].JobParts[a].QtyReserved,
                                                IsOPB: data.JobTask[j].JobParts[a].IsOPB,
                                                SeqOPB: data.JobTask[j].JobParts[a].SeqOPB,
                                                OrderType: data.JobTask[j].JobParts[a].OrderType,
                                                QtyOrder: data.JobTask[j].JobParts[a].QtyOrder,
                                                MaterialTypeId: data.JobTask[j].JobParts[a].MaterialTypeId,
                                                IsDeleted: data.JobTask[j].JobParts[a].isDeleted,
                                                Discount: data.JobTask[j].JobParts[a].Discount,
                                                DiscountPart: data.Insurance != undefined || data.Insurance != null ? data.Insurance.DiscountPart : 0,
                                                IsCustomDiscount: data.JobTask[j].JobParts[a].IsCustomDiscount,
                                                DiscountTypeId: data.JobTask[j].JobParts[a].DiscountTypeId,
                                                DiscountTypeListId: data.JobTask[j].JobParts[a].DiscountTypeListId
                                            });
                                        } else {
                                            console.log('bisa masuk mik kesini yg gk ada data towass na');
                                        }
                                    }

                                } else if (data.JobTask[j].JobParts[a].PaidBy.Name.toUpperCase() != arrPaidBy[0] &&
                                    data.JobTask[j].IsCustomDiscount == 0 &&
                                    data.JobTask[j].JobParts[a].isDeleted == 0) {
                                    if ((data.JobTask[j].JobParts[a].Price == null || data.JobTask[j].JobParts[a].Price == 0)) {
                                        data.JobTask[j].JobParts[a].Price = data.JobTask[j].JobParts[a].Part.RetailPrice;
                                    }
                                    // $scope.partsData.push(data.JobTask[j].JobParts[a]);
                                    console.log('Masukin pars 5 ===>', $scope.partsData)
                                    $scope.partsData.push({
                                        JobPartsId: data.JobTask[j].JobParts[a].JobPartsId,
                                        JobTaskId: data.JobTask[j].JobParts[a].JobTaskId,
                                        PartsId: data.JobTask[j].JobParts[a].PartsId,
                                        PartsName: data.JobTask[j].JobParts[a].PartsName,
                                        Price: data.JobTask[j].JobParts[a].Price,
                                        Status: data.JobTask[j].JobParts[a].Status,
                                        Qty: data.JobTask[j].JobParts[a].Qty,
                                        DownPayment: data.JobTask[j].JobParts[a].DownPayment,
                                        PaidById: data.JobTask[j].JobParts[a].PaidById,
                                        PaidBy: data.JobTask[j].JobParts[a].PaidBy.Name,
                                        SatuanId: data.JobTask[j].JobParts[a].SatuanId,
                                        Satuan: data.JobTask[j].JobParts[a].Satuan,
                                        IsOfp: data.JobTask[j].JobParts[a].IsOfp,
                                        MaterialRequestStatusId: data.JobTask[j].JobParts[a].MaterialRequestStatusId,
                                        QtyGI: data.JobTask[j].JobParts[a].QtyGI,
                                        QtyPrepicking: data.JobTask[j].JobParts[a].QtyPrepicking,
                                        QtyGIReserved: data.JobTask[j].JobParts[a].QtyGIReserved,
                                        QtyReserved: data.JobTask[j].JobParts[a].QtyReserved,
                                        IsOPB: data.JobTask[j].JobParts[a].IsOPB,
                                        SeqOPB: data.JobTask[j].JobParts[a].SeqOPB,
                                        OrderType: data.JobTask[j].JobParts[a].OrderType,
                                        QtyOrder: data.JobTask[j].JobParts[a].QtyOrder,
                                        MaterialTypeId: data.JobTask[j].JobParts[a].MaterialTypeId,
                                        IsDeleted: data.JobTask[j].JobParts[a].isDeleted,
                                        Discount: data.JobTask[j].JobParts[a].Discount,
                                        DiscountPart: data.Insurance != undefined || data.Insurance != null ? data.Insurance.DiscountPart : 0,
                                        IsCustomDiscount: data.JobTask[j].JobParts[a].IsCustomDiscount,
                                        DiscountTypeId: data.JobTask[j].JobParts[a].DiscountTypeId,
                                        DiscountTypeListId: data.JobTask[j].JobParts[a].DiscountTypeListId
                                    });
                                }
                            } else {
                                console.log("salah satu data ada yg opb");
                            }
                        } else {
                            console.log("mausk else Job Parts Sudah Ada Billing");
                        }
                    }
                    // console.log("TotalParts", TotalParts);
                    for (a = 0; a < data.JobTask[j].JobParts.length; a++) {
                        if (data.JobTask[j].JobParts[a].IsBilling < 1) {
                            if (data.JobTask[j].JobParts[a].IsOPB == 1 && data.JobTask[j].JobParts[a].isDeleted == 0) {
                                $scope.opbData.push(data.JobTask[j].JobParts[a]);

                                //job task opb belum di cek ga ada data
                            } else {
                                console.log("tidak ada data Opb");
                            }
                        } else {
                            console.log("mausk else Job Parts Sudah Ada Billing");
                        }
                    }
                }
            } else {
                console.log("dataForBsList | PARAM 1 =======>", param);
                for (j = 0; j < data.BillingJob.length; j++) {
                    // 2452
                    for (var xx=0; xx<$scope.dataByJobId[0].JobTask.length; xx++){
                        if (data.BillingJob[j].JobTaskId == $scope.dataByJobId[0].JobTask[xx].JobTaskId){
                            data.BillingJob[j].JobTask.ApprovalDiscountTransTR = $scope.dataByJobId[0].JobTask[xx].ApprovalDiscountTransTR
                        }
                    }

                    // ========================== untuk view billing, value tidak hrs ambil dari data yg ada di billing (create), jadi beberapa value jobtask/jobpart/opb/opl hrs di timpa pakai yg ada di billing create ============================== start
                    //value yg perlu di timpa/di cek ulang antara lain : discount, paidby, paidbyid, DiscountedPrice, discountTaskFromInsurance
                    data.BillingJob[j].JobTask.Discount = data.BillingJob[j].Disc
                    data.BillingJob[j].JobTask.PaidBy = data.ChargeToBy
                    data.BillingJob[j].JobTask.PaidById = data.ChargeTo

                    if (data.BillingJob[j].JobTask.PaidBy === null || data.BillingJob[j].JobTask.PaidBy === undefined){
                        data.BillingJob[j].JobTask.PaidBy = {}
                        for (var vv=0; vv<$scope.dataPayment.length; vv++){
                            if (data.BillingJob[j].JobTask.PaidById == $scope.dataPayment[vv].MasterId){
                                data.BillingJob[j].JobTask.PaidBy.Name = $scope.dataPayment[vv].Name
                            }
                        }
                    }

                    //cek kalau pas create billing na charge to insurance custom diskon nya jadi 0, krn ada kemungkinan beda antara pembayran wo vs penagihan di billing
                    if (data.ChargeTo == 29){
                        data.BillingJob[j].JobTask.IsCustomDiscount = 0
                    }
                    
                    // ========================== untuk view billing, value tidak hrs ambil dari data yg ada di billing (create), jadi beberapa value jobtask/jobpart/opb/opl hrs di timpa pakai yg ada di billing create ============================== end
                    
                    if ((data.BillingJob[j].JobTask.IsBilling >= 1 || data.BillingJob[j].JobTask.IsBilling != null) && (data.BillingJob[j].JobTask.PaidById == 28 || data.BillingJob[j].JobTask.PaidById == 2458 || data.BillingJob[j].JobTask.PaidById == 29 || data.BillingJob[j].JobTask.PaidById == 31 || data.BillingJob[j].JobTask.PaidById == 32) &&
                        data.BillingJob[j].JobTask.IsCustomDiscount == 0 &&
                        data.BillingJob[j].JobTask.isDeleted == 0) {


                        console.log('dataForBsList |Kon 1| data.BillingJob[j].JobTask.IsBilling >= 1            | ===>',data.BillingJob[j].JobTask.IsBilling);
                        console.log('dataForBsList |Kon 1| data.BillingJob[j].JobTask.PaidById == 28,2458,29,31 | ===>',data.BillingJob[j].JobTask.PaidById);
                        console.log('dataForBsList |Kon 1| data.BillingJob[j].JobTask.IsCustomDiscount == 0     | ===>',data.BillingJob[j].JobTask.IsCustomDiscount);
                        console.log('dataForBsList |Kon 1| data.BillingJob[j].JobTask.isDeleted == 0            | ===>',data.BillingJob[j].JobTask.isDeleted);
                          

                        if (data.BillingJob[j].JobTask.Fare !== undefined) {
                            var sum = data.BillingJob[j].JobTask.Fare;
                            var summ = 0;
                            if (data.BillingJob[j].JobTask.FlatRate == null || data.BillingJob[j].JobTask.FlatRate == undefined) {
                                summ = 1;
                            } else {
                                summ = data.BillingJob[j].JobTask.FlatRate;
                            }

                            var summaryy = Math.round(sum * summ);
                            var discountedPrice = (summaryy * data.BillingJob[j].JobTask.Discount) / 100;
                            var normalPrice = summaryy
                            summaryy = parseInt(summaryy);
                        }
                        if (data.BillingJob[j].JobTask.JobType == null) {
                            data.BillingJob[j].JobTask.JobType = { Name: "" };
                        }
                        if (data.BillingJob[j].JobTask.PaidBy == null) {
                            data.BillingJob[j].JobTask.PaidBy = { Name: "" }
                        }
                        if (data.BillingJob[j].JobTask.AdditionalTaskId == null) {
                            data.BillingJob[j].JobTask.isOpe = 0;
                        } else {
                            data.BillingJob[j].JobTask.isOpe = 1;
                        }
                        if (data.BillingJob[j].JobTask.IsCustomDiscount == 0 && data.BillingJob[j].JobTask.Discount == 0) {
                            data.BillingJob[j].JobTask.typeDiskon = -1;
                            data.BillingJob[j].JobTask.DiscountedPrice = normalPrice;
                        } else if (data.BillingJob[j].JobTask.IsCustomDiscount == 1 && data.BillingJob[j].JobTask.Discount !== 0) {
                            data.BillingJob[j].JobTask.typeDiskon = 0;
                            data.BillingJob[j].JobTask.DiscountedPrice = (normalPrice * data.BillingJob[j].JobTask.Discount) / 100;
                        }
                        for (var i in $scope.unitData) {
                            if (data.BillingJob != undefined || data.BillingJob != null) {
                                if ($scope.unitData[i].MasterId == data.BillingJob[j].JobTask.ProcessId) {
                                    data.BillingJob[j].JobTask.satuanName = $scope.unitData[i].Name;
                                }
                                if ($scope.unitData[i].MasterId == data.BillingJob[j].JobTask.JobTypeId) {
                                    data.BillingJob[j].JobTask.JobType.Name = $scope.unitData[i].Name;
                                }
                            }
                        }




                        // ===============cek Task ini ada SPK nya nggak?=====================
                        var isSPKHide = true;
                        var discountTaskFromInsurance = 0;
                        if(data.BillingJob[j].JobTask.PaidById == 29 ){
                            if($scope.dataInsurance.length > 0){
                                // discountTaskFromInsurance = $scope.dataInsurance[0].DiscountJasa;
                                discountTaskFromInsurance = data.BillingJob[j].Disc // ambil nilai diskon saat billing di create, jng ambil dari master lagi
                                var findSPKindex = _.findIndex(findIsSPKjob,function(o){
                                    return o.TaskBPId == data.BillingJob[j].JobTask.TaskBPId
                                })
                                var findSPKData = {};
                                if(findSPKindex == -1){
                                    isSPKHide = true;
                                }else{
                                    findSPKData = _.find(findIsSPKjob,function(o){
                                        return o.TaskBPId == data.BillingJob[j].JobTask.TaskBPId
                                    })
                                    isSPKHide = false;
                                }
                                console.log('dataForBsList | findSPKindex | isSPKHide ===>',findSPKData,findSPKindex, isSPKHide);
                            }
                        }
                        // ===============cek Task ini ada SPK nya nggak?=====================





                        


                        $scope.gridWork.push({
                            ActualRate: data.BillingJob[j].JobTask.ActualRate,
                            AdditionalTaskId: data.BillingJob[j].JobTask.AdditionalTaskId,
                            isOpe: data.BillingJob[j].JobTask.isOpe,
                            Discount: data.BillingJob[j].JobTask.Discount,
                            JobTaskId: data.BillingJob[j].JobTask.JobTaskId,
                            JobTypeId: data.BillingJob[j].JobTask.JobTypeId,
                            JobTypeName: data.BillingJob[j].JobTask.JobType.Name,
                            DiscountedPrice: discountedPrice,
                            IsBilling: data.BillingJob[j].JobTask.IsBilling,
                            typeDiskon: data.BillingJob[j].JobTask.typeDiskon,
                            NormalPrice: normalPrice,
                            // catName: data.BillingJob[j].JobTask.JobType.Name,
                            catName: data.BillingJob[j].JobTask.satuanName,
                            Fare: data.BillingJob[j].JobTask.Fare,
                            IsCustomDiscount: data.BillingJob[j].JobTask.IsCustomDiscount,
                            Summary: summaryy,
                            PaidById: data.BillingJob[j].JobTask.PaidById,
                            JobId: data.BillingJob[j].JobTask.JobId,
                            TaskId: data.BillingJob[j].JobTask.TaskId,
                            TaskName: data.BillingJob[j].JobTask.TaskName,
                            FlatRate: data.BillingJob[j].JobTask.FlatRate,
                            ProcessId: data.BillingJob[j].JobTask.ProcessId,
                            TFirst1: data.BillingJob[j].JobTask.TFirst1,
                            PaidBy: data.BillingJob[j].JobTask.PaidBy.Name,
                            
                            DiscountJasa: discountTaskFromInsurance,
                            SpkHide: isSPKHide,
                            SpkNo: isSPKHide == false? findSPKData.SpkNo : '',
                            ORAmount: isSPKHide == false? findSPKData.ORAmount : '',

                            // DiscountJasa: data.Insurance != undefined || data.Insurance != null ? data.Insurance.DiscountJasa : 0,
                            // SpkNo: data.Job.AJobSPK == undefined || data.Job.AJobSPK == null ? '' : (data.Job.AJobSPK.length == 0 ? '' : data.Job.AJobSPK[j] == undefined ? '' : data.Job.AJobSPK[j].SpkNo),
                            // ORAmount: data.Job.AJobSPK == undefined || data.Job.AJobSPK == null ? 0 : (data.Job.AJobSPK.length == 0 ? 0 : data.Job.AJobSPK[j] == undefined ? 0 : data.Job.AJobSPK[j].ORAmount),
                            // SpkHide: data.Job.AJobSPK == undefined || data.Job.AJobSPK == null || data.Job.AJobSPK.length == 0 ? true : false
                        }); 
                    } else if ((data.BillingJob[j].JobTask.IsBilling >= 1 || data.BillingJob[j].JobTask.IsBilling != null) && (data.BillingJob[j].JobTask.PaidById == 28 || data.BillingJob[j].JobTask.PaidById == 2458 || data.BillingJob[j].JobTask.PaidById == 29 || data.BillingJob[j].JobTask.PaidById == 31 || data.BillingJob[j].JobTask.PaidById == 32) &&
                        data.BillingJob[j].JobTask.IsCustomDiscount == 1 &&
                        data.BillingJob[j].JobTask.isDeleted == 0) {


                        console.log('dataForBsList |Kon 2| data.BillingJob[j].JobTask.IsBilling >= 1            | ===>',data.BillingJob[j].JobTask.IsBilling);
                        console.log('dataForBsList |Kon 2| data.BillingJob[j].JobTask.PaidById == 28,2458,29,31 | ===>',data.BillingJob[j].JobTask.PaidById);
                        console.log('dataForBsList |Kon 2| data.BillingJob[j].JobTask.IsCustomDiscount == 1     | ===>',data.BillingJob[j].JobTask.IsCustomDiscount);
                        console.log('dataForBsList |Kon 2| data.BillingJob[j].JobTask.isDeleted == 0            | ===>',data.BillingJob[j].JobTask.isDeleted);


                        if (data.BillingJob[j].JobTask.ApprovalDiscountTransTR.length > 0 || data.BillingJob[j].JobTask.ApprovalDiscountTransTR != null) {

                            if (data.BillingJob[j].JobTask.ApprovalDiscountTransTR[0].StatusApprovalId != 1 && data.BillingJob[j].JobTask.ApprovalDiscountTransTR[0].StatusApprovalId > 0){ // berarti blm di approve
                                data.BillingJob[j].JobTask.Discount = 0;
                                // ini biar diskon ga muncul kl approval bertingkat... ga blh ada yg status req
                            }
                            // ini loop di komen karena dr be di sort descending by transactionid, jadi ambil array pertama aja buat cek di approve ato ngga
                            // for (var jTR=0; jTR<data.BillingJob[j].JobTask.ApprovalDiscountTransTR.length; jTR++){
                            //     if (data.BillingJob[j].JobTask.ApprovalDiscountTransTR[jTR].StatusApprovalId != 1 && data.BillingJob[j].JobTask.ApprovalDiscountTransTR[jTR].StatusApprovalId > 0){ // berarti blm di approve
                            //         data.BillingJob[j].JobTask.Discount = 0;
                            //         // ini biar diskon ga muncul kl approval bertingkat... ga blh ada yg status req
                            //     }
                            // }

                            if (data.BillingJob[j].JobTask.Fare !== undefined) {
                                var sum = data.BillingJob[j].JobTask.Fare;
                                var summ = 0;
                                if (data.BillingJob[j].JobTask.FlatRate == null || data.BillingJob[j].JobTask.FlatRate == undefined) {
                                    summ = 1;
                                } else {
                                    summ = data.BillingJob[j].JobTask.FlatRate;
                                }

                                var summaryy = Math.round(sum * summ);
                                var discountedPrice = (summaryy * data.BillingJob[j].JobTask.Discount) / 100;
                                var normalPrice = summaryy
                                summaryy = parseInt(summaryy);
                            }
                            if (data.BillingJob[j].JobTask.JobType == null) {
                                data.BillingJob[j].JobTask.JobType = { Name: "" };
                            }
                            if (data.BillingJob[j].JobTask.PaidBy == null) {
                                data.BillingJob[j].JobTask.PaidBy = { Name: "" }
                            }
                            if (data.BillingJob[j].JobTask.AdditionalTaskId == null) {
                                data.BillingJob[j].JobTask.isOpe = 0;
                            } else {
                                data.BillingJob[j].JobTask.isOpe = 1;
                            }
                            if (data.BillingJob[j].JobTask.IsCustomDiscount == 0 && data.BillingJob[j].JobTask.Discount == 0) {
                                data.BillingJob[j].JobTask.typeDiskon = -1;
                                data.BillingJob[j].JobTask.DiscountedPrice = normalPrice;
                            } else if (data.BillingJob[j].JobTask.IsCustomDiscount == 1 && data.BillingJob[j].JobTask.Discount !== 0) {
                                data.BillingJob[j].JobTask.typeDiskon = 0;
                                data.BillingJob[j].JobTask.DiscountedPrice = (normalPrice * data.BillingJob[j].JobTask.Discount) / 100;
                            }
                            for (var i in $scope.unitData) {
                                if (data.BillingJob != undefined || data.BillingJob != null) {
                                    if ($scope.unitData[i].MasterId == data.BillingJob[j].JobTask.ProcessId) {
                                        data.BillingJob[j].JobTask.satuanName = $scope.unitData[i].Name;
                                    }
                                    if ($scope.unitData[i].MasterId == data.BillingJob[j].JobTask.JobTypeId) {
                                        data.BillingJob[j].JobTask.JobType.Name = $scope.unitData[i].Name;
                                    }
                                }
                            }

                            // ===============cek Task ini ada SPK nya nggak?=====================
                            var isSPKHide = true;
                            var discountTaskFromInsurance = 0;
                            if(data.BillingJob[j].JobTask.PaidById == 29 ){
                                if($scope.dataInsurance.length > 0){
                                    // discountTaskFromInsurance = $scope.dataInsurance[0].DiscountJasa;
                                    discountTaskFromInsurance = data.BillingJob[j].Disc // ambil nilai diskon saat billing di create, jng ambil dari master lagi
                                    var findSPKindex = _.findIndex(findIsSPKjob,function(o){
                                        return o.TaskBPId == data.BillingJob[j].JobTask.TaskBPId
                                    })
                                    var findSPKData = {};
                                    if(findSPKindex == -1){
                                        isSPKHide = true;
                                    }else{
                                        findSPKData = _.find(findIsSPKjob,function(o){
                                            return o.TaskBPId == data.BillingJob[j].JobTask.TaskBPId
                                        })
                                        isSPKHide = false;
                                    }
                                    console.log('dataForBsList | findSPKindex | isSPKHide ===>',findSPKindex, isSPKHide);
                                }
                            }
                            // ===============cek Task ini ada SPK nya nggak?=====================


                            $scope.gridWork.push({
                                ActualRate: data.BillingJob[j].JobTask.ActualRate,
                                AdditionalTaskId: data.BillingJob[j].JobTask.AdditionalTaskId,
                                isOpe: data.BillingJob[j].JobTask.isOpe,
                                Discount: data.BillingJob[j].JobTask.Discount,
                                JobTaskId: data.BillingJob[j].JobTask.JobTaskId,
                                JobTypeId: data.BillingJob[j].JobTask.JobTypeId,
                                JobTypeName: data.BillingJob[j].JobTask.JobType.Name,
                                DiscountedPrice: discountedPrice,
                                IsBilling: data.BillingJob[j].JobTask.IsBilling,
                                typeDiskon: data.BillingJob[j].JobTask.typeDiskon,
                                NormalPrice: normalPrice,
                                // catName: data.BillingJob[j].JobTask.JobType.Name,
                                catName: data.BillingJob[j].JobTask.satuanName,
                                Fare: data.BillingJob[j].JobTask.Fare,
                                IsCustomDiscount: data.BillingJob[j].JobTask.IsCustomDiscount,
                                Summary: summaryy,
                                PaidById: data.BillingJob[j].JobTask.PaidById,
                                JobId: data.BillingJob[j].JobTask.JobId,
                                TaskId: data.BillingJob[j].JobTask.TaskId,
                                TaskName: data.BillingJob[j].JobTask.TaskName,
                                FlatRate: data.BillingJob[j].JobTask.FlatRate,
                                ProcessId: data.BillingJob[j].JobTask.ProcessId,
                                TFirst1: data.BillingJob[j].JobTask.TFirst1,
                                PaidBy: data.BillingJob[j].JobTask.PaidBy.Name,
                                DiscountJasa: discountTaskFromInsurance,
                                SpkHide: isSPKHide,
                                SpkNo: isSPKHide == false? findSPKData.SpkNo : '',
                                ORAmount: isSPKHide == false? findSPKData.ORAmount : '',
                                // DiscountJasa: data.Insurance != undefined || data.Insurance != null ? data.Insurance.DiscountJasa : 0,
                                // SpkNo: data.Job.AJobSPK == undefined || data.Job.AJobSPK == null ? '' : (data.Job.AJobSPK.length == 0 ? '' : data.Job.AJobSPK[j] == undefined ? '' : data.Job.AJobSPK[j].SpkNo),
                                // ORAmount: data.Job.AJobSPK == undefined || data.Job.AJobSPK == null ? 0 : (data.Job.AJobSPK.length == 0 ? 0 : data.Job.AJobSPK[j] == undefined ? 0 : data.Job.AJobSPK[j].ORAmount),
                                // SpkHide: data.Job.AJobSPK == undefined || data.Job.AJobSPK == null || data.Job.AJobSPK.length == 0 ? true : false
                            });
                        }

                    } else if ((data.BillingJob[j].JobTask.IsBilling >= 1 || data.BillingJob[j].JobTask.IsBilling != null) &&
                        data.BillingJob[j].JobTask.PaidById == 30 && data.BillingJob[j].JobTask.isDeleted == 0) {
                        // } else if ((data.BillingJob[j].JobTask.IsBilling < 1 || data.BillingJob[j].JobTask.IsBilling == null) && data.BillingJob[j].JobTask.PaidById == 30 && data.BillingJob[j].JobTask.TOWASSClaimResult[0].ClaimStatus == 6) {
                        // $scope.gridWork.push(data.BillingJob[j].JobTask);
                        // Added by Fyberz 2017-11-29  --------------------------

                        console.log('dataForBsList |Kon 3| data.BillingJob[j].JobTask.IsBilling >= 1 | ===>',data.BillingJob[j].JobTask.IsBilling);
                        console.log('dataForBsList |Kon 3| data.BillingJob[j].JobTask.PaidById == 30 | ===>',data.BillingJob[j].JobTask.PaidById);
                        console.log('dataForBsList |Kon 3| data.BillingJob[j].JobTask.isDeleted == 0 | ===>',data.BillingJob[j].JobTask.isDeleted);


                        console.log('bisa masuk mik kesini', data.BillingJob[j].JobTask);
                        if (data.BillingJob[j].JobTask.TOWASSClaimResult != undefined || data.BillingJob[j].JobTask.TOWASSClaimResult != null) {
                            if (data.BillingJob[j].JobTask.TOWASSClaimResult.length > 0) {
                                // if (data.BillingJob[j].JobTask.TOWASSClaimResult != null) {
                                console.log('bisa masuk mik kesini yg towass na oke');
                                if (data.BillingJob[j].JobTask.TOWASSClaimResult[0].ClaimStatus == 6) {
                                    console.log('bisa masuk mik kesini yg ada towass na');
                                    if (data.BillingJob[j].JobTask.Fare !== undefined) {
                                        var sum = data.BillingJob[j].JobTask.Fare;
                                        var summ = 0;
                                        if (data.BillingJob[j].JobTask.FlatRate == null || data.BillingJob[j].JobTask.FlatRate == undefined) {
                                            summ = 1;
                                        } else {
                                            summ = data.BillingJob[j].JobTask.FlatRate;
                                        }

                                        var summaryy = Math.round(sum * summ);
                                        var discountedPrice = (summaryy * data.BillingJob[j].JobTask.Discount) / 100;
                                        var normalPrice = summaryy
                                        summaryy = parseInt(summaryy);
                                    }
                                    if (data.BillingJob[j].JobTask.JobType == null) {
                                        data.BillingJob[j].JobTask.JobType = { Name: "" };
                                    }
                                    if (data.BillingJob[j].JobTask.PaidBy == null) {
                                        data.BillingJob[j].JobTask.PaidBy = { Name: "" }
                                    }
                                    if (data.BillingJob[j].JobTask.AdditionalTaskId == null) {
                                        data.BillingJob[j].JobTask.isOpe = 0;
                                    } else {
                                        data.BillingJob[j].JobTask.isOpe = 1;
                                    }
                                    if (data.BillingJob[j].JobTask.IsCustomDiscount == 0 && data.BillingJob[j].JobTask.Discount == 0) {
                                        data.BillingJob[j].JobTask.typeDiskon = -1;
                                        data.BillingJob[j].JobTask.DiscountedPrice = normalPrice;
                                    } else if (data.BillingJob[j].JobTask.IsCustomDiscount == 1 && data.BillingJob[j].JobTask.Discount !== 0) {
                                        data.BillingJob[j].JobTask.typeDiskon = 0;
                                        data.BillingJob[j].JobTask.DiscountedPrice = (normalPrice * data.BillingJob[j].JobTask.Discount) / 100;
                                    }
                                    for (var i in $scope.unitData) {
                                        if (data.BillingJob != undefined || data.BillingJob != null) {
                                            if ($scope.unitData[i].MasterId == data.BillingJob[j].JobTask.ProcessId) {
                                                data.BillingJob[j].JobTask.satuanName = $scope.unitData[i].Name;
                                            }
                                            if ($scope.unitData[i].MasterId == data.BillingJob[j].JobTask.JobTypeId) {
                                                data.BillingJob[j].JobTask.JobType.Name = $scope.unitData[i].Name;
                                            }
                                        }
                                    }
                                    $scope.gridWork.push({
                                        ActualRate: data.BillingJob[j].JobTask.ActualRate,
                                        AdditionalTaskId: data.BillingJob[j].JobTask.AdditionalTaskId,
                                        isOpe: data.BillingJob[j].JobTask.isOpe,
                                        Discount: data.BillingJob[j].JobTask.Discount,
                                        JobTaskId: data.BillingJob[j].JobTask.JobTaskId,
                                        JobTypeId: data.BillingJob[j].JobTask.JobTypeId,
                                        JobTypeName: data.BillingJob[j].JobTask.JobType.Name,
                                        DiscountedPrice: discountedPrice,
                                        IsBilling: data.BillingJob[j].JobTask.IsBilling,
                                        typeDiskon: data.BillingJob[j].JobTask.typeDiskon,
                                        NormalPrice: normalPrice,
                                        // catName: data.BillingJob[j].JobTask.JobType.Name,
                                        catName: data.BillingJob[j].JobTask.satuanName,
                                        Fare: data.BillingJob[j].JobTask.Fare,
                                        IsCustomDiscount: data.BillingJob[j].JobTask.IsCustomDiscount,
                                        Summary: summaryy,
                                        PaidById: data.BillingJob[j].JobTask.PaidById,
                                        JobId: data.BillingJob[j].JobTask.JobId,
                                        TaskId: data.BillingJob[j].JobTask.TaskId,
                                        TaskName: data.BillingJob[j].JobTask.TaskName,
                                        FlatRate: data.BillingJob[j].JobTask.FlatRate,
                                        ProcessId: data.BillingJob[j].JobTask.ProcessId,
                                        TFirst1: data.BillingJob[j].JobTask.TFirst1,
                                        PaidBy: data.BillingJob[j].JobTask.PaidBy.Name,
                                        // DiscountJasa: data.Insurance != undefined || data.Insurance != null ? data.Insurance.DiscountJasa : 0,
                                        DiscountJasa: data.BillingJob[j].Disc, // ambil nilai diskon saat billing di create, jng ambil dari master lagi
                                        SpkNo: data.Job.AJobSPK == undefined || data.Job.AJobSPK == null ? '' : (data.Job.AJobSPK.length == 0 ? '' : data.Job.AJobSPK[j] == undefined ? '' : data.Job.AJobSPK[j].SpkNo),
                                        ORAmount: data.Job.AJobSPK == undefined || data.Job.AJobSPK == null ? 0 : (data.Job.AJobSPK.length == 0 ? 0 : data.Job.AJobSPK[j] == undefined ? 0 : data.Job.AJobSPK[j].ORAmount),
                                        SpkHide: data.Job.AJobSPK == undefined || data.Job.AJobSPK == null || data.Job.AJobSPK.length == 0 ? true : false
                                    });
                                };
                            } else {
                                console.log('bisa masuk mik kesini yg gk ada data towass na');
                            }
                        } else {
                            console.log('bisa masuk mik kesini yg ada towass na');
                            if (data.BillingJob[j].JobTask.Fare !== undefined) {
                                var sum = data.BillingJob[j].JobTask.Fare;
                                var summ = 0;
                                if (data.BillingJob[j].JobTask.FlatRate == null || data.BillingJob[j].JobTask.FlatRate == undefined) {
                                    summ = 1;
                                } else {
                                    summ = data.BillingJob[j].JobTask.FlatRate;
                                }

                                var summaryy = Math.round(sum * summ);
                                var discountedPrice = (summaryy * data.BillingJob[j].JobTask.Discount) / 100;
                                var normalPrice = summaryy
                                summaryy = parseInt(summaryy);
                            }
                            if (data.BillingJob[j].JobTask.JobType == null) {
                                data.BillingJob[j].JobTask.JobType = { Name: "" };
                            }
                            if (data.BillingJob[j].JobTask.PaidBy == null) {
                                data.BillingJob[j].JobTask.PaidBy = { Name: "" }
                            }
                            if (data.BillingJob[j].JobTask.AdditionalTaskId == null) {
                                data.BillingJob[j].JobTask.isOpe = 0;
                            } else {
                                data.BillingJob[j].JobTask.isOpe = 1;
                            }
                            if (data.BillingJob[j].JobTask.IsCustomDiscount == 0 && data.BillingJob[j].JobTask.Discount == 0) {
                                data.BillingJob[j].JobTask.typeDiskon = -1;
                                data.BillingJob[j].JobTask.DiscountedPrice = normalPrice;
                            } else if (data.BillingJob[j].JobTask.IsCustomDiscount == 1 && data.BillingJob[j].JobTask.Discount !== 0) {
                                data.BillingJob[j].JobTask.typeDiskon = 0;
                                data.BillingJob[j].JobTask.DiscountedPrice = (normalPrice * data.BillingJob[j].JobTask.Discount) / 100;
                            }
                            for (var i in $scope.unitData) {
                                if (data.BillingJob != undefined || data.BillingJob != null) {
                                    if ($scope.unitData[i].MasterId == data.BillingJob[j].JobTask.ProcessId) {
                                        data.BillingJob[j].JobTask.satuanName = $scope.unitData[i].Name;
                                    }
                                    if ($scope.unitData[i].MasterId == data.BillingJob[j].JobTask.JobTypeId) {
                                        data.BillingJob[j].JobTask.JobType.Name = $scope.unitData[i].Name;
                                    }
                                }
                            }
                            $scope.gridWork.push({
                                ActualRate: data.BillingJob[j].JobTask.ActualRate,
                                AdditionalTaskId: data.BillingJob[j].JobTask.AdditionalTaskId,
                                isOpe: data.BillingJob[j].JobTask.isOpe,
                                Discount: data.BillingJob[j].JobTask.Discount,
                                JobTaskId: data.BillingJob[j].JobTask.JobTaskId,
                                JobTypeId: data.BillingJob[j].JobTask.JobTypeId,
                                JobTypeName: data.BillingJob[j].JobTask.JobType.Name,
                                DiscountedPrice: discountedPrice,
                                IsBilling: data.BillingJob[j].JobTask.IsBilling,
                                typeDiskon: data.BillingJob[j].JobTask.typeDiskon,
                                NormalPrice: normalPrice,
                                // catName: data.BillingJob[j].JobTask.JobType.Name,
                                catName: data.BillingJob[j].JobTask.satuanName,
                                Fare: data.BillingJob[j].JobTask.Fare,
                                IsCustomDiscount: data.BillingJob[j].JobTask.IsCustomDiscount,
                                Summary: summaryy,
                                PaidById: data.BillingJob[j].JobTask.PaidById,
                                JobId: data.BillingJob[j].JobTask.JobId,
                                TaskId: data.BillingJob[j].JobTask.TaskId,
                                TaskName: data.BillingJob[j].JobTask.TaskName,
                                FlatRate: data.BillingJob[j].JobTask.FlatRate,
                                ProcessId: data.BillingJob[j].JobTask.ProcessId,
                                TFirst1: data.BillingJob[j].JobTask.TFirst1,
                                PaidBy: data.BillingJob[j].JobTask.PaidBy.Name,
                                // DiscountJasa: data.Insurance != undefined || data.Insurance != null ? data.Insurance.DiscountJasa : 0,
                                DiscountJasa: data.BillingJob[j].Disc, // ambil nilai diskon saat billing di create, jng ambil dari master lagi
                                SpkNo: data.Job.AJobSPK == undefined || data.Job.AJobSPK == null ? '' : (data.Job.AJobSPK.length == 0 ? '' : data.Job.AJobSPK[j] == undefined ? '' : data.Job.AJobSPK[j].SpkNo),
                                ORAmount: data.Job.AJobSPK == undefined || data.Job.AJobSPK == null ? 0 : (data.Job.AJobSPK.length == 0 ? 0 : data.Job.AJobSPK[j] == undefined ? 0 : data.Job.AJobSPK[j].ORAmount),
                                SpkHide: data.Job.AJobSPK == undefined || data.Job.AJobSPK == null || data.Job.AJobSPK.length == 0 ? true : false
                            });
                        }
                    } else if ((data.BillingJob[j].JobTask.IsBilling >= 1 || data.BillingJob[j].JobTask.IsBilling != null) && data.BillingJob[j].JobTask.PaidById != 28 && data.BillingJob[j].JobTask.PaidById != 30 &&
                        data.BillingJob[j].JobTask.IsCustomDiscount == 0 &&
                        data.BillingJob[j].JobTask.isDeleted == 0) {
                        // $scope.gridWork.push(data.BillingJob[j].JobTask);
                        // Added by Fyberz 2017-11-29  --------------------------

                        console.log('dataForBsList |Kon 4| data.BillingJob[j].JobTask.IsBilling >= 1        | ===>',data.BillingJob[j].JobTask.IsBilling);
                        console.log('dataForBsList |Kon 4| data.BillingJob[j].JobTask.PaidById == !28,!30   | ===>',data.BillingJob[j].JobTask.PaidById);
                        console.log('dataForBsList |Kon 4| data.BillingJob[j].JobTask.IsCustomDiscount == 0 | ===>',data.BillingJob[j].JobTask.IsCustomDiscount);
                        console.log('dataForBsList |Kon 4| data.BillingJob[j].JobTask.isDeleted == 0        | ===>',data.BillingJob[j].JobTask.isDeleted);


                        if (data.BillingJob[j].JobTask.Fare !== undefined) {
                            var sum = data.BillingJob[j].JobTask.Fare;
                            var summ = 0;
                            if (data.BillingJob[j].JobTask.FlatRate == null || data.BillingJob[j].JobTask.FlatRate == undefined) {
                                summ = 1;
                            } else {
                                summ = data.BillingJob[j].JobTask.FlatRate;
                            }

                            var summaryy = Math.round(sum * summ);
                            var discountedPrice = (summaryy * data.BillingJob[j].JobTask.Discount) / 100;
                            var normalPrice = summaryy
                            summaryy = parseInt(summaryy);
                        }
                        if (data.BillingJob[j].JobTask.JobType == null) {
                            data.BillingJob[j].JobTask.JobType = { Name: "" };
                        }
                        if (data.BillingJob[j].JobTask.PaidBy == null) {
                            data.BillingJob[j].JobTask.PaidBy = { Name: "" }
                        }
                        if (data.BillingJob[j].JobTask.AdditionalTaskId == null) {
                            data.BillingJob[j].JobTask.isOpe = 0;
                        } else {
                            data.BillingJob[j].JobTask.isOpe = 1;
                        }
                        if (data.BillingJob[j].JobTask.IsCustomDiscount == 0 && data.BillingJob[j].JobTask.Discount == 0) {
                            data.BillingJob[j].JobTask.typeDiskon = -1;
                            data.BillingJob[j].JobTask.DiscountedPrice = normalPrice;
                        } else if (data.BillingJob[j].JobTask.IsCustomDiscount == 1 && data.BillingJob[j].JobTask.Discount !== 0) {
                            data.BillingJob[j].JobTask.typeDiskon = 0;
                            data.BillingJob[j].JobTask.DiscountedPrice = (normalPrice * data.BillingJob[j].JobTask.Discount) / 100;
                        }
                        for (var i in $scope.unitData) {
                            if (data.BillingJob != undefined || data.BillingJob != null) {
                                if ($scope.unitData[i].MasterId == data.BillingJob[j].JobTask.ProcessId) {
                                    data.BillingJob[j].JobTask.satuanName = $scope.unitData[i].Name;
                                }
                                if ($scope.unitData[i].MasterId == data.BillingJob[j].JobTask.JobTypeId) {
                                    data.BillingJob[j].JobTask.JobType.Name = $scope.unitData[i].Name;
                                }
                            }
                        }
                        $scope.gridWork.push({
                            ActualRate: data.BillingJob[j].JobTask.ActualRate,
                            AdditionalTaskId: data.BillingJob[j].JobTask.AdditionalTaskId,
                            isOpe: data.BillingJob[j].JobTask.isOpe,
                            Discount: data.BillingJob[j].JobTask.Discount,
                            JobTaskId: data.BillingJob[j].JobTask.JobTaskId,
                            JobTypeId: data.BillingJob[j].JobTask.JobTypeId,
                            JobTypeName: data.BillingJob[j].JobTask.JobType.Name,
                            DiscountedPrice: discountedPrice,
                            IsBilling: data.BillingJob[j].JobTask.IsBilling,
                            typeDiskon: data.BillingJob[j].JobTask.typeDiskon,
                            NormalPrice: normalPrice,
                            // catName: data.BillingJob[j].JobTask.JobType.Name,
                            catName: data.BillingJob[j].JobTask.satuanName,
                            Fare: data.BillingJob[j].JobTask.Fare,
                            IsCustomDiscount: data.BillingJob[j].JobTask.IsCustomDiscount,
                            Summary: summaryy,
                            PaidById: data.BillingJob[j].JobTask.PaidById,
                            JobId: data.BillingJob[j].JobTask.JobId,
                            TaskId: data.BillingJob[j].JobTask.TaskId,
                            TaskName: data.BillingJob[j].JobTask.TaskName,
                            FlatRate: data.BillingJob[j].JobTask.FlatRate,
                            ProcessId: data.BillingJob[j].JobTask.ProcessId,
                            TFirst1: data.BillingJob[j].JobTask.TFirst1,
                            PaidBy: data.BillingJob[j].JobTask.PaidBy.Name,
                            // DiscountJasa: data.Insurance != undefined || data.Insurance != null ? data.Insurance.DiscountJasa : 0,
                            DiscountJasa: data.BillingJob[j].Disc, // ambil nilai diskon saat billing di create, jng ambil dari master lagi
                            SpkNo: data.Job.AJobSPK == undefined || data.Job.AJobSPK == null ? '' : (data.Job.AJobSPK.length == 0 ? '' : data.Job.AJobSPK[j] == undefined ? '' : data.Job.AJobSPK[j].SpkNo),
                            ORAmount: data.Job.AJobSPK == undefined || data.Job.AJobSPK == null ? 0 : (data.Job.AJobSPK.length == 0 ? 0 : data.Job.AJobSPK[j] == undefined ? 0 : data.Job.AJobSPK[j].ORAmount),
                            SpkHide: data.Job.AJobSPK == undefined || data.Job.AJobSPK == null || data.Job.AJobSPK.length == 0 ? true : false
                        });
                    } else {
                        console.log("masuk else JobTask sudah Billing");
                        console.log('dataForBsList |Kon else| data.BillingJob[j].JobTask.IsBilling        | ===>',data.BillingJob[j].JobTask.IsBilling);
                        console.log('dataForBsList |Kon else| data.BillingJob[j].JobTask.PaidById         | ===>',data.BillingJob[j].JobTask.PaidById);
                        console.log('dataForBsList |Kon else| data.BillingJob[j].JobTask.IsCustomDiscount | ===>',data.BillingJob[j].JobTask.IsCustomDiscount);
                        console.log('dataForBsList |Kon else| data.BillingJob[j].JobTask.isDeleted        | ===>',data.BillingJob[j].JobTask.isDeleted);

                    }
                }
                // ========== PARTS
                console.log('billing parts ===>',data.BillingParts);
                for (a = 0; a < data.BillingParts.length; a++) {

                    angular.forEach($scope.dataByJobId[0].JobTask, function(v1, k1) {
                        angular.forEach(v1.JobParts, function(v2, k2) {
                            // if (v2.PartsId == data.BillingParts[a].JobParts.PartsId){JobPartsId
                            if (v2.JobPartsId == data.BillingParts[a].JobParts.JobPartsId){
                                data.BillingParts[a].JobParts.ApprovalDiscountTransTR = v2.ApprovalDiscountTransTR
                            }
                        })

                    })

                    // ========================== untuk view billing, value tidak hrs ambil dari data yg ada di billing (create), jadi beberapa value jobtask/jobpart/opb/opl hrs di timpa pakai yg ada di billing create ============================== start
                    //value yg perlu di timpa/di cek ulang antara lain : discount, paidby, paidbyid, DiscountedPrice, discountTaskFromInsurance
                    data.BillingParts[a].JobParts.Discount = data.BillingParts[a].Disc
                    data.BillingParts[a].JobParts.PaidBy = data.ChargeToBy
                    data.BillingParts[a].JobParts.PaidById = data.ChargeTo

                    if (data.BillingParts[a].JobParts.PaidBy === null || data.BillingParts[a].JobParts.PaidBy === undefined){
                        data.BillingParts[a].JobParts.PaidBy = {}
                        for (var vv=0; vv<$scope.dataPayment.length; vv++){
                            if (data.BillingParts[a].JobParts.PaidById == $scope.dataPayment[vv].MasterId){
                                data.BillingParts[a].JobParts.PaidBy.Name = $scope.dataPayment[vv].Name
                            }
                        }
                    }

                    //cek kalau pas create billing na charge to insurance custom diskon nya jadi 0, krn ada kemungkinan beda antara pembayran wo vs penagihan di billing
                    if (data.ChargeTo == 29){
                        data.BillingParts[a].JobParts.IsCustomDiscount = 0
                        data.BillingParts[a].JobParts.DiscountPart = data.BillingParts[a].Disc
                    }
                    
                    // ========================== untuk view billing, value tidak hrs ambil dari data yg ada di billing (create), jadi beberapa value jobtask/jobpart/opb/opl hrs di timpa pakai yg ada di billing create ============================== end
                    

                    if (data.BillingParts[a].JobParts.IsBilling >= 1 || data.BillingParts[a].JobParts.IsBilling !== null) {
                        if (data.BillingParts[a].JobParts.IsOPB != 1) {
                            if ((data.BillingParts[a].JobParts.PaidById == 28 || data.BillingParts[a].JobParts.PaidById == 2458 || data.BillingParts[a].JobParts.PaidById == 29 || data.BillingParts[a].JobParts.PaidById == 31 || data.BillingParts[a].JobParts.PaidById == 2663 || data.BillingParts[a].JobParts.PaidById == 32 ) &&
                                data.BillingParts[a].JobParts.IsCustomDiscount == 0 &&
                                data.BillingParts[a].JobParts.isDeleted == 0) {
                                // if (data.BillingParts[a].JobParts.Price == null || data.BillingParts[a].JobParts.Price == 0) {
                                    data.BillingParts[a].JobParts.Price = data.BillingParts[a].Price;
                                // }
                                data.BillingParts[a].JobParts.PaidBy = (data.BillingParts[a].JobParts.PaidBy != undefined || data.BillingParts[a].JobParts.PaidBy != null ? data.BillingParts[a].JobParts.PaidBy.Name : "");
                                $scope.partsData.push(data.BillingParts[a].JobParts);
                            } else if ((data.BillingParts[a].JobParts.PaidById == 28 || data.BillingParts[a].JobParts.PaidById == 2458 || data.BillingParts[a].JobParts.PaidById == 29 || data.BillingParts[a].JobParts.PaidById == 31 || data.BillingParts[a].JobParts.PaidById == 2663 || data.BillingParts[a].JobParts.PaidById == 32) && data.BillingParts[a].JobParts.IsCustomDiscount == 1 &&
                                        data.BillingParts[a].JobParts.isDeleted == 0) {

                                if (data.BillingParts[a].JobParts.ApprovalDiscountTransTR != null) {
                                    if (data.BillingParts[a].JobParts.ApprovalDiscountTransTR.length > 0){
                                        for (var x=0; x<data.BillingParts[a].JobParts.ApprovalDiscountTransTR.length; x++){
                                            if (data.BillingParts[a].JobParts.ApprovalDiscountTransTR[x].StatusApprovalId == 1 || data.BillingParts[a].JobParts.ApprovalDiscountTransTR[x].StatusApprovalId == -1) {
                                                // if (data.BillingParts[a].JobParts.Price == null || data.BillingParts[a].JobParts.Price == 0) {
                                                    // data.BillingParts[a].JobParts.Price = data.BillingParts[a].JobParts.Part.RetailPrice;
                                                    data.BillingParts[a].JobParts.Price = data.BillingParts[a].Price;
                                                // }
                                                data.BillingParts[a].JobParts.PaidBy = (data.BillingParts[a].JobParts.PaidBy != undefined || data.BillingParts[a].JobParts.PaidBy != null ? data.BillingParts[a].JobParts.PaidBy.Name : "");
                                                $scope.partsData.push(data.BillingParts[a].JobParts);
                                            } else {
                                                // kalau status ApprovalDiscountTransTR.StatusApprovalId tidak 1 maka diskon di 0 in
                                                // if (data.BillingParts[a].JobParts.Price == null || data.BillingParts[a].JobParts.Price == 0) {
                                                    // data.BillingParts[a].JobParts.Price = data.BillingParts[a].JobParts.Part.RetailPrice;
                                                    data.BillingParts[a].JobParts.Price = data.BillingParts[a].Price;
                                                // }
                                                data.BillingParts[a].JobParts.PaidBy = (data.BillingParts[a].JobParts.PaidBy != undefined || data.BillingParts[a].JobParts.PaidBy != null ? data.BillingParts[a].JobParts.PaidBy.Name : "");
                                                data.BillingParts[a].JobParts.Discount = 0;
                                                $scope.partsData.push(data.BillingParts[a].JobParts);
                                            }
                                        }
                                    }
                                    
                                } else {
                                    // if (data.BillingParts[a].JobParts.Price == null || data.BillingParts[a].JobParts.Price == 0) {
                                        // data.BillingParts[a].JobParts.Price = data.BillingParts[a].JobParts.Part.RetailPrice;
                                        data.BillingParts[a].JobParts.Price = data.BillingParts[a].Price;
                                    // }
                                    data.BillingParts[a].JobParts.PaidBy = (data.BillingParts[a].JobParts.PaidBy != undefined || data.BillingParts[a].JobParts.PaidBy != null ? data.BillingParts[a].JobParts.PaidBy.Name : "");
                                    $scope.partsData.push(data.BillingParts[a].JobParts);
                                    console.log('masuk sini boy');
                                }

                            } else if (data.BillingParts[a].JobParts.PaidById == 30) { //parts warranty masih menunggu approval kabeng, sama seperti tasknya
                                if (data.BillingParts[a].JobParts.isDeleted == 0) {
                                    // if (data.BillingParts[a].JobParts.Price == null || data.BillingParts[a].JobParts.Price == 0) {
                                        // data.BillingParts[a].JobParts.Price = data.BillingParts[a].JobParts.Part.RetailPrice;
                                        data.BillingParts[a].JobParts.Price = data.BillingParts[a].Price;
                                    // }
                                    data.BillingParts[a].JobParts.PaidBy = (data.BillingParts[a].JobParts.PaidBy != undefined || data.BillingParts[a].JobParts.PaidBy != null ? data.BillingParts[a].JobParts.PaidBy.Name : "");
                                    $scope.partsData.push(data.BillingParts[a].JobParts);
                                } else {
                                    console.log('bisa masuk mik kesini yg gk ada data parts towass na');
                                }
                            } else if (data.BillingParts[a].JobParts.PaidById != 28 &&
                                data.BillingParts[a].JobParts.Discount == 0 &&
                                data.BillingParts[a].JobParts.isDeleted == 0) {
                                // if (data.BillingParts[a].JobParts.Price == null || data.BillingParts[a].JobParts.Price == 0) {
                                    // data.BillingParts[a].JobParts.Price = data.BillingParts[a].JobParts.Part.RetailPrice;
                                    data.BillingParts[a].JobParts.Price = data.BillingParts[a].Price;
                                // }
                                data.BillingParts[a].JobParts.PaidBy = (data.BillingParts[a].JobParts.PaidBy != undefined || data.BillingParts[a].JobParts.PaidBy != null ? data.BillingParts[a].JobParts.PaidBy.Name : "");
                                $scope.partsData.push(data.BillingParts[a].JobParts);
                            }else{
                                // =============================================== CR4 Request Pak Eko =============================================
                                // ini harusnya pembayarannya free service, tp biar yakin di tambhin kondisi lg
                                if(data.BillingParts[a].JobParts.PaidById == 2277){
                                    console.log('ini partsnya masuk siniiii??', data.BillingParts[a].JobParts);
                                    data.BillingParts[a].JobParts.PaidBy = data.BillingParts[a].JobParts.PaidBy.Name
                                    $scope.partsData.push(data.BillingParts[a].JobParts);
                                }
                                // =============================================== CR4 Request Pak Eko =============================================

                            }
                        } else {
                            console.log("salah satu data ada yg opb");
                        }
                        console.log("data.BillingParts[a].JobParts => ", data.BillingParts[a].JobParts);
                    } else {
                        console.log("mausk else Job Parts Sudah Ada Billing");
                    }
                }
                // console.log("TotalParts", TotalParts);
                for (a = 0; a < data.BillingParts.length; a++) {

                    if (data.BillingParts[a].JobParts.IsBilling >= 1 || data.BillingParts[a].JobParts.IsBilling !== null) {
                        if (data.BillingParts[a].JobParts.IsOPB == 1 && data.BillingParts[a].JobParts.isDeleted == 0) {
                            // ========================== untuk view billing, value tidak hrs ambil dari data yg ada di billing (create), jadi beberapa value jobtask/jobpart/opb/opl hrs di timpa pakai yg ada di billing create ============================== start
                            //value yg perlu di timpa/di cek ulang antara lain : discount, paidby, paidbyid, DiscountedPrice, discountTaskFromInsurance
                            data.BillingParts[a].JobParts.Discount = data.BillingParts[a].Disc
                            data.BillingParts[a].JobParts.PaidBy = data.ChargeToBy
                            data.BillingParts[a].JobParts.PaidById = data.ChargeTo

                            if (data.BillingParts[a].JobParts.PaidBy === null || data.BillingParts[a].JobParts.PaidBy === undefined){
                                data.BillingParts[a].JobParts.PaidBy = {}
                                for (var vv=0; vv<$scope.dataPayment.length; vv++){
                                    if (data.BillingParts[a].JobParts.PaidById == $scope.dataPayment[vv].MasterId){
                                        data.BillingParts[a].JobParts.PaidBy.Name = $scope.dataPayment[vv].Name
                                    }
                                }
                            }      

                            //cek kalau pas create billing na charge to insurance custom diskon nya jadi 0, krn ada kemungkinan beda antara pembayran wo vs penagihan di billing
                            if (data.ChargeTo == 29){
                                data.BillingParts[a].JobParts.IsCustomDiscount = 0
                            }
                            
                            // ========================== untuk view billing, value tidak hrs ambil dari data yg ada di billing (create), jadi beberapa value jobtask/jobpart/opb/opl hrs di timpa pakai yg ada di billing create ============================== end

                            data.BillingParts[a].JobParts.PaidBy = (data.BillingParts[a].JobParts.PaidBy != undefined || data.BillingParts[a].JobParts.PaidBy != null ? data.BillingParts[a].JobParts.PaidBy.Name : "");

                            $scope.opbData.push(data.BillingParts[a].JobParts);

                            //job task opb belum di cek ga ada data
                        } else {
                            console.log("tidak ada data Opb");
                        }
                    } else {
                        console.log("mausk else Job Parts Sudah Ada Billing");
                    }
                }

                for (z = 0; z < data.BillingOpls.length; z++) {
                    // console.log("total opl", TotalOpl);
                    //job task opl belum di cek ga ada data
                    if ((data.BillingOpls[z].IsBilling == 1 || data.BillingOpls[z].IsBilling !== null) && data.BillingOpls[z].Status !== 4 && data.BillingOpls[z].isDeleted != 1) {
                        
                        // ========================== untuk view billing, value tidak hrs ambil dari data yg ada di billing (create), jadi beberapa value jobtask/jobpart/opb/opl hrs di timpa pakai yg ada di billing create ============================== start
                        //value yg perlu di timpa/di cek ulang antara lain : discount, paidby, paidbyid, DiscountedPrice, discountTaskFromInsurance
                        data.BillingOpls[z].JobTaskOpl.Discount = data.BillingOpls[z].Disc
                        data.BillingOpls[z].JobTaskOpl.PaidBy = data.ChargeToBy
                        data.BillingOpls[z].JobTaskOpl.PaidById = data.ChargeTo

                        //cek kalau pas create billing na charge to insurance custom diskon nya jadi 0, krn ada kemungkinan beda antara pembayran wo vs penagihan di billing
                        // if (data.ChargeTo == 29){
                        //     data.BillingOpls[z].JobTaskOpl.IsCustomDiscount = 0 // opl blm ada custom discount
                        // }
                        
                        // ========================== untuk view billing, value tidak hrs ambil dari data yg ada di billing (create), jadi beberapa value jobtask/jobpart/opb/opl hrs di timpa pakai yg ada di billing create ============================== end
                        // data.BillingOpls[z].JobTaskOpl.Pembayaran = data.BillingOpls[z].JobTaskOpl.PaidBy.Name
                        for (var vv=0; vv<$scope.dataPayment.length; vv++){
                            if (data.BillingOpls[z].JobTaskOpl.PaidById == $scope.dataPayment[vv].MasterId){
                                data.BillingOpls[z].JobTaskOpl.Pembayaran = $scope.dataPayment[vv].Name
                            }
                        }
                        $scope.oplData.push(data.BillingOpls[z].JobTaskOpl);
                        console.log("opl sudah billing", data.BillingOpls[z].JobTaskOpl);
                    } else {
                        console.log("Masuk else OplData");
                    }
                }
            }
            if (mode !== 'edit') {
                $scope.onListSelectRows($scope.gridWork);
                $scope.onListSelectRowsOPL($scope.oplData);
                $scope.onListSelectRowsParts($scope.partsData);
                $scope.onListSelectRowsOpb($scope.opbData);
            }




        }

        // { name:'Charge To', field:'PaidBy.Name' },
        $scope.reprintApproval = function() {
            console.log('reprintApproval', $scope.mBilling);
            var datenoew = new Date();
            console.log("datenoew", datenoew);
            console.log("selectrowsLc", selectrowsLc);
            $scope.show_modalReprint = { show: false };

            console.log('billingz',$scope.dataCekBillingz)

            BillingFactory.CekStatusBilling($scope.dataCekBillingz).then(function(resucek) {
                if (resucek.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Ada perubahan status billing, mohon refresh data terlebih dahulu",
                    });
                } else if (resucek.data == 888) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Billing tidak dapat di proses reprint, harap selesaikan approval cancel billing dahulu",
                    });
                } else {
                    BillingFactory.ReqApprovalReprintBilling($scope.mBilling, datenoew, selectrowsLc)
                    .then(
                        function(res) {
                            console.log("berhasil nih", res);
                            bsNotify.show({
                                size: 'small',
                                type: 'success',
                                title: "Request Approve Billing Success",
                            });
                            // $scope.show_modalReprint = { show : false };
                            // $scope.mBilling.ApproverCancelBy = res.data.Result[0].EmployeeName;
                            // $scope.show_modal = { show: true };
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
                }

            })

            

        };

        //update status re-print billing approved
        $scope.UpdateReprintApprove = function() {
            var d = new Date(),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            var datenoew = [year, month, day].join('-');

            console.log('UpdateReprint', $scope.mBilling);
            console.log("datenoew", datenoew);
            console.log("selectrowsLc", selectrowsLc);
            BillingFactory.putReprintBilling($scope.mBilling, datenoew, selectrowsLc, 1)
                .then(
                    function(res) {
                        console.log("berhasil nih");
                        $scope.show_modalReprintApprove = { show: false };
                        bsNotify.show({
                            size: 'small',
                            type: 'success',
                            title: "Data berhasil di approve"
                        });
                        $scope.mBilling.CancelMessage = "";
                        $scope.getData();
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        };

        //update status re-print billing rejected
        $scope.UpdateReprintReject = function() {
            var d = new Date(),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            var datenoew = [year, month, day].join('-');

            console.log('UpdateReprint', $scope.mBilling);
            console.log("datenoew", datenoew);
            console.log("selectrowsLc", selectrowsLc);
            BillingFactory.putReprintBilling($scope.mBilling, datenoew, selectrowsLc, 2)
                .then(
                    function(res) {
                        console.log("berhasil nih");
                        $scope.show_modalReprintReject = { show: false };
                        bsNotify.show({
                            size: 'small',
                            type: 'success',
                            title: "Data berhasil di tolak"
                        });
                        $scope.mBilling.CancelMessage = "";
                        $scope.getData();
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        };

        $scope.changeFormatDate = function(item) {
            var tmpAppointmentDate = item;
            console.log("changeFormatDate item", item);
            tmpAppointmentDate = new Date(tmpAppointmentDate);
            var finalDate
            var yyyy = tmpAppointmentDate.getFullYear().toString();
            var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = tmpAppointmentDate.getDate().toString();
            finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
            console.log("changeFormatDate finalDate", finalDate);
            return finalDate;
        }
        $scope.changeFormatDateStrip = function(item) {
            var tmpAppointmentDate = item;
            console.log("changeFormatDate item", item);
            tmpAppointmentDate = new Date(tmpAppointmentDate);
            var finalDate
            var yyyy = tmpAppointmentDate.getFullYear().toString();
            var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = tmpAppointmentDate.getDate().toString();
            finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
            console.log("changeFormatDate finalDate", finalDate);
            return finalDate;
        }
        $scope.changingFilterButton = function() {
            var tmpGridCols = [];
            var x = -1;
            for (var i = 0; i < $scope.grid.columnDefs.length; i++) {
                if ($scope.grid.columnDefs[i].visible == undefined && $scope.grid.columnDefs[i].name !== 'Action' && $scope.grid.columnDefs[i].name !== 'action') {
                    x++;

                    tmpGridCols.push($scope.grid.columnDefs[i]);
                    console.log('COLNAME==>', tmpGridCols[x].name[tmpGridCols[x].name.length - 1] * 0);
                    if (tmpGridCols[x].name[tmpGridCols[x].name.length - 1] * 0 != 0) {

                    } else {
                        tmpGridCols[x].name = tmpGridCols[x].name.slice(0, -1);

                    }
                    tmpGridCols[x].idx = i;
                }
            }
            $scope.formApi.changeDropdown(tmpGridCols);

         

            

        }
        $scope.reprintCancel = function() {};
        $scope.buttonSettingModal = { save: { text: 'Kirim Permohonan' } };
        $scope.changeFormatDataBilling = function(data) {
            var newTmpData = [];
            for (var z in data) {
                newTmpData.push({
                    JobId: data[z].JobId,
                    Job: {
                        JobId: data[z].JobId,
                        WoNo: data[z].WoNo,
                        AppointmentDate: data[z].AppointmentDate,
                        isGR: data[z].isGR,
                        MStatusJob: null,
                        Billing: null,
                        Process: null,
                        Vehicle: null,
                        JobTask: null
                    },
                    BillingId: data[z].BillingId,
                    BillingNo: data[z].BillingNo,
                    BillingDate: data[z].BillingDate,
                    BillTo: data[z].BillTo,
                    StatusBilling: data[z].StatusBilling,
                    FakturNo: data[z].FakturNo,
                    OutletId: data[z].OutletId,
                    ChargeTo: data[z].ChargeTo,
                    isPrintedBilling: data[z].isPrintedBilling,
                    isPrintedCancel: data[z].isPrintedCancel,
                    BillAddress: data[z].BillAddress,
                    BillPhoneNumber: data[z].BillPhoneNumber,
                    PrintedCount: data[z].PrintedCount
                })
            }
        }
        $scope.bulkPrint = function(data) {
            console.log('bulkprint', data);
            if ($scope.filter.ShowData == 1) {
                bsAlert.alert({
                        title: "Data WO belum di Billing",
                        text: "",
                        type: "warning"
                    },
                    function() {},
                    function() {}
                )
                return -1;
            }

      

            for (var n=0; n<data.length; n++){
               if (data[n].StatusBilling == 0) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Tidak dapat melakukan print karena ada data yang berstatus cancel",
                    });
                    return false
               }
            }

            var ArrIdBilling = []

            for (var x=0; x<data.length; x++){
                var idB = {}
                idB.BillingId = {}
                idB.BillingId = data[x].BillingId
                ArrIdBilling.push(idB)
            }

            var dataCekBilling = []
            $scope.dataCekBillingz = [];

            for (var y=0; y<data.length; y++){
                var idBilling = {
                    BillingId: null,
                    CurrentStatus: null
                }
                idBilling.BillingId = data[y].BillingId
                idBilling.CurrentStatus = data[y].StatusBilling
                dataCekBilling.push(idBilling)
            }

            $scope.dataCekBillingz = dataCekBilling

            BillingFactory.CekStatusBilling(dataCekBilling).then(function(resucek) {
                if (resucek.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Ada perubahan status billing, mohon refresh data terlebih dahulu",
                    });
                } else if (resucek.data == 888) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Billing tidak dapat di proses reprint, harap selesaikan approval cancel billing dahulu",
                    });
                } else {
                    BillingFactory.CheckValdationBillingCancelApproval(ArrIdBilling).then(function(resuv) {
                        var cekres = resuv.data.toString();
                        var dtCancel = ''
                        var haselRes = null;
                        if (cekres.includes('#')){
                            var seplit = cekres.split('#')
                            haselRes = seplit[0]
                            dtCancel = seplit[1]
                        } else {
                            haselRes = cekres
                        }
                        if (haselRes == '666'){
                            bsNotify.show({
                                size: 'big',
                                type: 'warning',
                                title: "Data tidak dapat dicetak.",
                                content: "Terdapat data billing [ " + dtCancel + " ] sedang dalam proses pengajuan cancel.",
                            });
                        } else if(haselRes == '0') {
                            for (i = 0; i < data.length; i++) {
                                if ($scope.changeFormatDate(data[i].AppointmentDate) == "1/01/01") {
                                    data[i].AppointmentDate = new Date('1900-01-01');
                                    data[i].Job.AppointmentDate = new Date('1900-01-01');
                                    //     // console.log("data[0].Billing[0]",data[0].Billing[0]);
                                    //     _.map(data[i].BillingJob, function(val) {
                                    //         if (val.JobTask != null && val.JobTask != undefined) {
                                    //             val.JobTask.Job.AppointmentDate = new Date('1900-01-01');
                                    //         }
                                    //     });
                                    //     if (data[i].BillingParts.length > 0) {
                                    //         _.map(data[i].BillingParts, function(val) {
                                    //             if (val.JobParts != null && val.JobParts != undefined) {
                                    //                 if (val.JobParts.JobTask != null && val.JobParts.JobTask != undefined) {
                                    //                     val.JobParts.JobTask.Job.AppointmentDate = '1900-01-01';
                                    //                 }
                                    //             }
                                    //         });
                                    //     }
                                    //     if (data[i].BillingOpls.length > 0) {
                                    //         _.map(data[i].BillingOpls, function(val) {
                                    //             if (val.JobTaskOpl != null && val.JobTaskOpl != undefined) {
                                    //                 val.JobTaskOpl.Job.AppointmentDate = '1900-01-01';
                                    //             }
                                    //         });
                                    //     }
                                };
                
                                data[i].Job.MStatusJob = null;
                                data[i].Job.Billing = null;
                                data[i].Job.Process = null;
                                data[i].Job.Vehicle = null;
                                data[i].Job.JobTask = null;
                                // data[0].Job.AppointmentDate = $scope.changeFormatDate(data[0].Job.AppointmentDate);
                            }
                            var needReprint = 0;
                            var total = 0;
                            var needApprove = 0;
                            _.map(data, function(val) {
                                total = total + val.Total;
                                if (val.isPrintedBilling == 1) {
                                    needReprint = 1;
                                };
                                needApprove = val.StatusBilling;
                            });
                            console.log('needApprove', needApprove);
                
                            if (needReprint == 1) {
                                var Bill = _.map(data, 'BillingId').join(', ');
                
                                if (needApprove == 6) {
                                    bsNotify.show({
                                        size: 'small',
                                        type: 'warning',
                                        title: "Data tidak dapat dicetak, data sedang dalam proses approval"
                                    });
                                    console.log('Status Request Reprint Billing');
                                }
                
                                if (needApprove == 2 || needApprove == 4 || needApprove == 0) {
                                    bsNotify.show({
                                        size: 'small',
                                        type: 'warning',
                                        title: "Data tidak dapat dicetak, data sudah di cancel"
                                    });
                                    console.log('Status Request Reprint Billing');
                                }
                
                                if (needApprove == 5 || needApprove == 7) {
                                    BillingFactory.GetDataInvoice(Bill).then(function(resu) {
                                        $scope.show_modalReprint = { show: true };
                                        $scope.getDataReprint = resu.data.Result;
                                        $scope.getSelectedRows = selectrowsLc[0];
                                        console.log("$scope.getDataReprint", $scope.getDataReprint);
                                    });
                                    console.log('ini mau request approval');
                                }
                
                                if (needApprove == 8) {
                                    BillingFactory.GetDataInvoice(Bill).then(function(resu) {
                                        $scope.getData();
                                        console.log('resu', resu);
                                        console.log('resu', resu.data.Result);
                                        console.log('resu', resu.data.Result[0].InvoiceId);
                                        var InvId = resu.data.Result[0].InvoiceId;
                                        BillingFactory.putCountPrintedBilling(Bill).then(function(rus) {
                                            // var newRush = rus.data.ResponseMessage;
                                            // newRush = newRush.split('#');
                                            console.log('anita cans =>', rus.data);
                                        });
                                        $scope.cetakBiling(InvId);
                                    });
                                }
                
                                if (needApprove == 1) {
                                    var arr = [];
                                    var obj = {};
                                    if (data.length == 1) {
                                        obj.Description = "Tagihan Pembayaran untuk Billing dengan No. " + data[0].BillingNo;
                                    } else if (data.length > 1) {
                                        var newBillNo = _.map(data, 'BillingNo').join(', ');
                                        // console.log('bulkprint newBillNo', newBillNo);
                                        obj.Description = "Tagihan Pembayaran untuk Billing dengan No. " + newBillNo;
                                    };
                                    obj.TotalAmount = total;
                                    obj.Billing = data;
                                    for (var i = 0; i < obj.Billing.length; i++) {
                                        obj.Billing[i].BillingDate = $filter('date')(obj.Billing[i].BillingDate, 'yyyy-MM-dd HH:mm');
                                    }
                                    arr.push(obj);
                                    BillingFactory.createInvoice(arr).then(function(resu) {
                                        $scope.getData();
                                        var newResu = resu.data.ResponseMessage;
                                        newResu = newResu.split('#');
                                        console.log('newResu', newResu);
                                        var InvId = newResu[1];
                                        BillingFactory.putCountPrintedBilling(Bill).then(function(rus) {
                                            // var newRush = rus.data.ResponseMessage;
                                            // newRush = newRush.split('#');
                                            console.log('anita cans =>', rus.data);
                                        });
                                        $scope.cetakBiling(InvId);
                                    });
                                    console.log('ini sudah approve');
                                    // bsNotify.show({
                                    //     size: 'small',
                                    //     type: 'success',
                                    //     title: "Billing Berhasil Dicetak",
                                    // });
                                }
                                console.log('bulkprint Need Approval Reprint', Bill);
                            } else {
                                console.log('bulkprint total', total);
                                var arr = [];
                                var obj = {};
                                if (data.length == 1) {
                                    obj.Description = "Tagihan Pembayaran untuk Billing dengan No. " + data[0].BillingNo;
                
                                    obj.TotalAmount = total;
                
                                    obj.Billing = data;
                                    // obj.Billing = data;
                                    for (var i = 0; i < obj.Billing.length; i++) {
                                        obj.Billing[i].BillingDate = $filter('date')(obj.Billing[i].BillingDate, 'yyyy-MM-dd HH:mm');
                                    }
                
                                    arr.push(obj);
                
                
                                    BillingFactory.createInvoice(arr).then(function(resu) {
                                        $scope.getData();
                                        var newResu = resu.data.ResponseMessage;
                                        newResu = newResu.split('#');
                                        console.log('newResu', newResu);
                                        var InvId = newResu[1];
                                        $scope.cetakBiling(InvId);
                
                                        BillingFactory.putCountPrintedBilling(data[0].BillingId).then(function(rus) {
                                            // var newRush = rus.data.ResponseMessage;
                                            // newRush = newRush.split('#');
                                            console.log('anita cansx =>', rus.data);
                                        });
                
                
                                    });
                                } else if (data.length > 1) {
                                    var countDiff = 0;
                                    _.map(data, function(val1) {
                                        _.map(data, function(val2) {
                                            if (val1.ChargeTo !== val2.ChargeTo) {
                                                countDiff++;
                                            }
                                            console.log('countDiff 1 ==>', countDiff);
                
                                        })
                                        console.log('countDiff 2 ==>', countDiff);
                
                                    });
                
                                    if (countDiff > 0) {
                                        console.log('GAK BISA PRINT ==>')
                                        bsAlert.alert({
                                                title: "ChargeTo lebih dari 1, Silahkan cek kembali",
                                                text: "",
                                                type: "warning"
                                            },
                                            function() {},
                                            function() {}
                                        )
                                    } else {
                                        console.log('BISA PRINT ==>');
                                        var newBillNo = _.map(data, 'BillingNo').join(', ');
                                        console.log('bulkprint newBillNo', newBillNo);
                                        obj.Description = "Tagihan Pembayaran untuk Billing dengan No. " + newBillNo;
                
                                        obj.TotalAmount = total;
                                        obj.Billing = data;
                                        // obj.Billing = data;
                                        for (var i = 0; i < obj.Billing.length; i++) {
                                            obj.Billing[i].BillingDate = $filter('date')(obj.Billing[i].BillingDate, 'yyyy-MM-dd HH:mm');
                                        }
                                        arr.push(obj);
                
                
                
                                        BillingFactory.createInvoice(arr).then(function(resu) {
                                            $scope.getData();
                                            var newResu = resu.data.ResponseMessage;
                                            newResu = newResu.split('#');
                                            console.log('newResu', newResu);
                                            var InvId = newResu[1];
                                            $scope.cetakBiling(InvId);
                
                                            BillingFactory.putCountPrintedBilling(data[0].BillingId).then(function(rus) {
                                                // var newRush = rus.data.ResponseMessage;
                                                // newRush = newRush.split('#');
                                                console.log('anita cansxx =>', rus.data);
                                            });
                
                                        });
                                    }
                
                                };
                                // obj.TotalAmount = total;
                                // obj.Billing = data;
                
                                // arr.push(obj);
                
                                // BillingFactory.putCountPrintedBilling(data[0].BillingId).then(function(rus) {
                                //     // var newRush = rus.data.ResponseMessage;
                                //     // newRush = newRush.split('#');
                                //     console.log('anita cans =>', rus.data);
                                // });
                
                                // BillingFactory.createInvoice(arr).then(function(resu) {
                                //     $scope.getData();
                                //     var newResu = resu.data.ResponseMessage;
                                //     newResu = newResu.split('#');
                                //     console.log('newResu', newResu);
                                //     var InvId = newResu[1];
                                //     $scope.cetakBiling(InvId);
                                // });
                            }
        
                        } else {
                            console.log('service ada eror')
                        }
        
                    })
                }
            })



            

        };

        $scope.cetakBiling = function(inVoiceId) {
            $scope.DataBilling = 'as/PrintDeliveryFakturService/' + inVoiceId + '/' + $scope.user.OrgId;
            $scope.cetakan($scope.DataBilling);
        };

        $scope.cetakBilingafterSave = function(tmpData, tmpAmount, msg) {
            var data = tmpData[0].Billing;
            var billData = [];
            console.log('bulkprint total', data);
            var arr = [];
            var obj = {};
            var needReprint = 0;
            var total = 0;
            var needApprove = 0;
            // _.map(data, function(val) {
            //     total = total + val.Total;
            //     if (val.isPrintedBilling == 1) {
            //         needReprint = 1;
            //     };
            //     needApprove = val.StatusBilling;
            // });

            var newBillNo = msg.split("#");
            _.map(data, function(val) {
                if (val.BillingNo == newBillNo[1]) {
                    billData.push(val);
                }
            });
            obj.Description = "Tagihan Pembayaran untuk Billing dengan No. " + newBillNo[1];
            obj.TotalAmount = tmpAmount;
            obj.Billing = billData;

            arr.push(obj);
            BillingFactory.createInvoice(arr).then(function(resu) {
                var newResu = resu.data.ResponseMessage;
                newResu = newResu.split('#');
                console.log('newResu', newResu);
                var InvId = newResu[1];

                //$scope.cetakBiling(InvId);
            });
        }


        $scope.cetakNota = function(Data) {
            console.log("cetak Nota", Data);

            $scope.DataNota = 'as/PrintDeliveryNotaPembatalan/' + Data.BillingId + '/' + Data.OutletId;
            $scope.cetakan($scope.DataNota);

        };

        $scope.cetakan = function(data) {
            var pdfFile = null;

            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    } else {
                        printJS(pdfFile);
                    }
                } else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };

        $scope.dataCancel = [];
        $scope.batalBiling = function(data) {
            BillingFactory.CheckStatusGate(data.PoliceNumber, data.BillingId).then(function(ressul) {
                if (ressul.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Tidak bisa cancel billing, sudah dilakukan pembayaran secara full"
                    });
                } else {
                    $scope.dataCancel = [];
                    console.log("Batal Biling", data);
                    if (data.ApprovalBilling.length > 0) {
                        // bsNotify.show({
                        //     size: 'big',
                        //     type: 'danger',
                        //     title: "Data sudah dalam proses approval"
                        // });

                        var trxid_temp = 0;
                        var statApproval = null;
                        for (var i=0; i<data.ApprovalBilling.length; i++) {
                            if (data.ApprovalBilling[i].TransactionId > trxid_temp) {
                                trxid_temp = data.ApprovalBilling[i].TransactionId
                                statApproval = data.ApprovalBilling[i].StatusApprovalId
                            }
                        }

                        if(statApproval == 1 ){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Pembatalan billing sudah di approve. Silahkan refresh"
                                // content: error.join('<br>'),
                                // number: error.length
                            });
                        } else if (statApproval == 3) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Data sudah dalam proses approval. Silahkan refresh"
                            });
                        } else{
                            $scope.batalBiling2(data)
                        }
                    } else {
                        $scope.batalBiling2(data)

                    }

                }
            });
            

        };

        $scope.batalBiling2 = function(data) {
            BillingFactory.CheckBillingIncoming(data.BillingId).then(function(resss) {
                if (resss.data.IncomingNo != "0") {
                    var titleAlert = resss.data.IsBackdate == false ? "Batalkan Incoming Terlebih Dahulu" : "Tolak Pengajuan Incoming Terlebih Dahulu";

                    bsAlert.alert({
                        title: titleAlert,
                        text: "No Incoming : " + resss.data.IncomingNo,
                        type: "warning",
                        showCancelButton: false

                    })
                } else {
                    BillingFactory.getDataCancel(data.BillingId)
                        .then(
                            function(res) {
                                $scope.dataCancel = res.data.Result;
                                var KabengCode;
                                // if (data.Job.isGr == 1) {
                                if (data.Job.isGR == 1) { //object na ganti jadi isGR
                                    KabengCode = 1128 // GR
                                } else {
                                    KabengCode = 1129 // BP
                                }
                                $scope.KabengCodeNotif = KabengCode;
                                if (res.data.Result[0] != undefined || res.data.Result[0] != null) {
                                    if (res.data.Result[0].FakturNo == null) {
                                        console.log("show_modalCancelWithoutApprove", $scope.dataCancel);
                                        $scope.show_modalCancelWithoutApprove = { show: true };
                                    } else {
                                        BillingFactory.getKabeng(KabengCode)
                                            .then(
                                                function(res) {
                                                    if (res.data.Result.length == 0 && KabengCode == 1128){
                                                        BillingFactory.getKabeng(1174).then(function(resus) { // role setara kabeng gr
                                                            console.log("data Kabeng", resus.data.Result);
                                                            $scope.mBilling.ApproverCancelBy = resus.data.Result[0].EmployeeName;
                                                            $scope.show_modal = { show: true };
                                                        })
                                                    } else {
                                                        console.log("data Kabeng", res.data.Result);
                                                        $scope.mBilling.ApproverCancelBy = res.data.Result[0].EmployeeName;
                                                        $scope.show_modal = { show: true };
                                                    }
                                                    
                                                },
                                                function(err) {
                                                    console.log("err=>", err);
                                                }
                                            );
                                    }
                                } else {
                                    if ($scope.dataCancel.FakturNo == null) {
                                        console.log("show_modalCancelWithoutApprove", $scope.dataCancel);
                                        $scope.show_modalCancelWithoutApprove = { show: true };
                                    } else {
                                        BillingFactory.getKabeng(KabengCode)
                                            .then(
                                                function(res) {
                                                    if (res.data.Result.length == 0 && KabengCode == 1128){
                                                        BillingFactory.getKabeng(1174).then(function(resus) { // role setara kabeng gr
                                                            console.log("data Kabeng", resus.data.Result);
                                                            $scope.mBilling.ApproverCancelBy = resus.data.Result[0].EmployeeName;
                                                            $scope.show_modal = { show: true };
                                                        })
                                                    } else {
                                                        console.log("data Kabeng", res.data.Result);
                                                        $scope.mBilling.ApproverCancelBy = res.data.Result[0].EmployeeName;
                                                        $scope.show_modal = { show: true };
                                                    }
                                                },
                                                function(err) {
                                                    console.log("err=>", err);
                                                }
                                            );
                                    }
                                }
                                console.log("data cancel", $scope.dataCancel);
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                }
            })
        }


        $scope.rescheduleCancel = function() {
            console.log('itemcancel');
            $scope.mBilling.CancelType = "";
            $scope.mBilling.CancelMessage = "";
        }

        $scope.CancelType = [{
                Id: 1,
                Name: "Cancel Faktur Pajak"
            },
            {
                Id: 2,
                Name: "Perubahan Harga"
            },
            {
                Id: 3,
                Name: "Perubahan Item Jasa atau Item Part"
            }
        ];
        $scope.doCancelWithoutApproval = function() {

            var dataCekBilling2 = []
            var dataBilling = {
                BillingId: null,
                CurrentStatus: null
            }
            dataBilling.BillingId = $scope.dataCancel[0].BillingId
            dataBilling.CurrentStatus = $scope.dataCancel[0].StatusBilling
            dataCekBilling2.push(dataBilling)

            BillingFactory.CekStatusBilling(dataCekBilling2).then(function(resucek) {
                if (resucek.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Ada perubahan status billing, mohon refresh data terlebih dahulu",
                    });
                } else if (resucek.data == 777) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Billing tidak dapat dibatalkan, harap selesaikan proses approval reprint billing dahulu",
                    });
                } else {
                    if ($scope.dataCancel[0] != undefined || $scope.dataCancel[0] != null) {
                        $scope.mBilling.BillingId = $scope.dataCancel[0].BillingId;
                        console.log("anita1", $scope.dataCancel);
                        console.log("anita3", $scope.mBilling);
                        BillingFactory.cancelWithoutApproval($scope.mBilling).then(
                            function(res) {
                                $scope.show_modalCancelWithoutApprove = { show: false };
                                $scope.getData();
                                console.log("cancelWithoutApproval=>", res);
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    } else {
                        $scope.mBilling.BillingId = $scope.dataCancel.BillingId;
                        console.log("anita1", $scope.dataCancel);
                        console.log("anita3", $scope.mBilling);
                        BillingFactory.cancelWithoutApproval($scope.mBilling).then(
                            function(res) {
                                $scope.show_modalCancelWithoutApprove = { show: false };
                                $scope.getData();
                                console.log("cancelWithoutApproval=>", res);
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    }
                }
            })   

            
        };
        $scope.doCloseModalCancelWithoutApproval = function() {
            $scope.show_modalCancelWithoutApprove = { show: false };
        };
        $scope.rescheduleSave = function() {
            console.log("mBiliing", $scope.mBilling, $scope.dataCancel);
            console.log("data", $scope.dataCancel[0] != undefined ? $scope.dataCancel[0].BillingId : $scope.dataCancel.BillingId);
            console.log("dataCancel", $scope.dataCancel);
            if ($scope.dataCancel[0] != undefined || $scope.dataCancel[0] != null) {
                if ($scope.dataCancel[0].StatusBilling == 1) {
                    if ($scope.dataCancel[0].FakturNo.length > 0) {
                        // BillingFactory.ReqCancelBilling($scope.mBilling, $scope.dataCancel[0].BillingId, $scope.dataCancel[0].CancelType, $scope.dataCancel[0].CancelMessage) // dikomen permintaan pa cien, pake yg PostApproveCancelBilling aja
                        BillingFactory.PostApproveCancelBilling($scope.dataCancel[0].BillingId, $scope.dataCancel[0].BillingNo, $scope.dataCancel[0].FakturNo)
                            .then(
                                function(res) {
                                    // $scope.DataPemeriksaanAwal = res.data.Result;
                                    // $scope.dataCancel = res.data.Result;
                                    $scope.show_modal = { show: false };
                                    console.log("berhasil simpan");
                                    $scope.getData();
                                    // console.log("First Check", $scope.DataPemeriksaanAwal);
                                    // $scope.loading=false;

                                    //notif setelah cancel biling
                                    //kirim notifikasi
                                    var DataNotif = {};
                                    var messagetemp = "";
                                    messagetemp = "Nama Permohonan : Batal Billing <br> " +
                                        "No. Billing : " + $scope.dataCancel[0].BillingNo + " <br> " +
                                        "No. WO : " + $scope.dataCancel[0].Job.WoNo + "<br> " +
                                        "Alasan : " + $scope.mBilling.CancelMessage;

                                    console.log("messagetemp", messagetemp);


                                    DataNotif.Message = messagetemp;
                                    BillingFactory.sendNotifRole(DataNotif, $scope.KabengCodeNotif, 32).then(
                                        function(res) {

                                        },
                                        function(err) {
                                            //console.log("err=>", err);
                                        });

                                    $scope.mBilling.CancelMessage = "";


                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
                    }
                } else if ($scope.dataCancel[0].StatusBilling >= 1 && ($scope.dataCancel[0].FakturNo != null)) { // anita ganti kondisi. sebelumnya : StatusBilling >= 1
                    BillingFactory.PostApproveCancelBilling($scope.dataCancel[0].BillingId, $scope.dataCancel[0].BillingNo, $scope.dataCancel[0].FakturNo)
                        .then(
                            function(res) {
                                // $scope.DataPemeriksaanAwal = res.data.Result;
                                // $scope.dataCancel = res.data.Result;
                                $scope.show_modal = { show: false };
                                console.log("berhasil simpan");
                                $scope.getData();
                                // console.log("First Check", $scope.DataPemeriksaanAwal);
                                // $scope.loading=false;

                                //notif setelah cancel biling
                                //kirim notifikasi
                                var DataNotif = {};
                                var messagetemp = "";
                                messagetemp = "Nama Permohonan : Batal Billing <br> " +
                                    "No. Billing : " + $scope.dataCancel[0].BillingNo + " <br> " +
                                    "No. WO : " + $scope.dataCancel[0].Job.WoNo + "<br> " +
                                    "Alasan : " + $scope.mBilling.CancelMessage;

                                console.log("messagetemp", messagetemp);


                                DataNotif.Message = messagetemp;
                                BillingFactory.sendNotifRole(DataNotif, $scope.KabengCodeNotif, 32).then(
                                    function(res) {

                                    },
                                    function(err) {
                                        //console.log("err=>", err);
                                    });

                                $scope.mBilling.CancelMessage = "";

                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                } else if ($scope.dataCancel[0].StatusBilling > 1 && $scope.dataCancel[0].StatusBilling < 5) {
                    bsAlert.alert({
                        title: "Data Sedang Dalam Proses Request Cancel Billing",
                        text: "",
                        type: "warning",
                        showCancelButton: true
                    })
                }
                // BillingFactory.ReqCancelBilling($scope.mBilling, $scope.dataCancel[0].BillingId, $scope.dataCancel[0].CancelType, $scope.dataCancel[0].CancelMessage)
                //     .then(
                //         function(res) {
                //             // $scope.DataPemeriksaanAwal = res.data.Result;
                //             // $scope.dataCancel = res.data.Result;
                //             $scope.show_modal = { show: false };
                //             $scope.mBilling.CancelMessage = "";
                //             console.log("berhasil simpan");
                //             $scope.getData();
                //             // console.log("First Check", $scope.DataPemeriksaanAwal);
                //             // $scope.loading=false;
                //         },
                //         function(err) {
                //             console.log("err=>", err);
                //         }
                //     );
            } else {
                if ($scope.dataCancel.StatusBilling == 1) {
                    if ($scope.dataCancel.FakturNo.length > 0) {
                        // BillingFactory.ReqCancelBilling($scope.mBilling, $scope.dataCancel.BillingId, $scope.dataCancel.CancelType, $scope.dataCancel.CancelMessage) // dikomen permintaan pa cien, pake yg PostApproveCancelBilling aja
                        BillingFactory.PostApproveCancelBilling($scope.dataCancel.BillingId, $scope.dataCancel.BillingNo, $scope.dataCancel.FakturNo)
                            .then(
                                function(res) {
                                    // $scope.DataPemeriksaanAwal = res.data.Result;
                                    // $scope.dataCancel = res.data.Result;
                                    $scope.show_modal = { show: false };
                                    console.log("berhasil simpan");
                                    $scope.getData();
                                    // console.log("First Check", $scope.DataPemeriksaanAwal);
                                    // $scope.loading=false;

                                    //notif setelah cancel biling
                                    //kirim notifikasi
                                    var DataNotif = {};
                                    var messagetemp = "";
                                    messagetemp = "Nama Permohonan : Batal Billing <br> " +
                                        "No. Billing : " + $scope.dataCancel.BillingNo + " <br> " +
                                        "No. WO : " + $scope.dataCancel.Job.WoNo + "<br> " +
                                        "Alasan : " + $scope.mBilling.CancelMessage;

                                    console.log("messagetemp", messagetemp);


                                    DataNotif.Message = messagetemp;
                                    BillingFactory.sendNotifRole(DataNotif, $scope.KabengCodeNotif, 32).then(
                                        function(res) {

                                        },
                                        function(err) {
                                            //console.log("err=>", err);
                                        });

                                    $scope.mBilling.CancelMessage = "";


                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
                    }
                } else if ($scope.dataCancel.StatusBilling >= 1 && ($scope.dataCancel.FakturNo != null)) { // anita ganti kondisi. sebelumnya : StatusBilling >= 1
                    BillingFactory.PostApproveCancelBilling($scope.dataCancel.BillingId, $scope.dataCancel.BillingNo, $scope.dataCancel.FakturNo)
                        .then(
                            function(res) {
                                // $scope.DataPemeriksaanAwal = res.data.Result;
                                // $scope.dataCancel = res.data.Result;
                                $scope.show_modal = { show: false };
                                console.log("berhasil simpan");
                                $scope.getData();
                                // console.log("First Check", $scope.DataPemeriksaanAwal);
                                // $scope.loading=false;

                                //notif setelah cancel biling
                                //kirim notifikasi
                                var DataNotif = {};
                                var messagetemp = "";
                                messagetemp = "Nama Permohonan : Batal Billing <br> " +
                                    "No. Billing : " + $scope.dataCancel.BillingNo + " <br> " +
                                    "No. WO : " + $scope.dataCancel.Job.WoNo + "<br> " +
                                    "Alasan : " + $scope.mBilling.CancelMessage;

                                console.log("messagetemp", messagetemp);


                                DataNotif.Message = messagetemp;
                                BillingFactory.sendNotifRole(DataNotif, $scope.KabengCodeNotif, 32).then(
                                    function(res) {

                                    },
                                    function(err) {
                                        //console.log("err=>", err);
                                    });

                                $scope.mBilling.CancelMessage = "";

                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                } else if ($scope.dataCancel.StatusBilling > 1 && $scope.dataCancel.StatusBilling < 5) {
                    bsAlert.alert({
                        title: "Data Sedang Dalam Proses Request Cancel Billing",
                        text: "",
                        type: "warning",
                        showCancelButton: true
                    })
                }
            }
        }

        $scope.dataReject = [];
        $scope.rejectBilling = function() {
            var adaYgBukanReq = 0
            for (var i=0; i<selectrowsLc.length; i++){
                if (selectrowsLc[i].StatusBilling == 3 || selectrowsLc[i].StatusBilling == 4){
                    adaYgBukanReq++;
                }
            }
            if (adaYgBukanReq != 0){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Status Billing Ada Yang Sudah di Approve / Reject",
                });
            } else {
                $scope.mBilling.CancelType = "";
                if (selectrowsLc.length !== 0) {
    
                    BillingFactory.getDataCancel(selectrowsLc[0].BillingId)
                        .then(
                            function(res) {
                                // $scope.DataPemeriksaanAwal = res.data.Result;
                                $scope.dataReject = res.data.Result;
                                if ($scope.showw === 2) {
                                    $scope.show_modalReject = { show: true };
                                } else if ($scope.showw === 3) {
                                    $scope.show_modalReprintReject = { show: true };
                                }
                                console.log("data cancel1", $scope.dataReject);
                                // console.log("First Check", $scope.DataPemeriksaanAwal);
                                // $scope.loading=false;
                                PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res) {
                                        var Approvers = res.data.Result[0].Approvers;
                                        console.log("getApprover res = ", Approvers);
                                        $scope.dataReject[0].Approvers = Approvers;
                                        //$scope.ApprovalData.Approvers = Approvers;
                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                );
    
                                switch ($scope.dataReject[0].ChargeTo) {
                                    case 28:
                                        $scope.dataReject[0].xChargeTo = "Customer";
                                        break;
                                    case 29:
                                        $scope.dataReject[0].xChargeTo = "Insurance";
                                        break;
                                    case 30:
                                        $scope.dataReject[0].xChargeTo = "Warranty";
                                        break;
                                    case 31:
                                        $scope.dataReject[0].xChargeTo = "Internal";
                                        break;
                                    case 32:
                                        $scope.dataReject[0].xChargeTo = "Third Party";
                                        break;
                                    case 2277:
                                        $scope.dataReject[0].xChargeTo = "Free Service";
                                        break;
                                    case 2458:
                                        $scope.dataReject[0].xChargeTo = "PKS";
                                        break;
                                }
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                } else {
    
    
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Mohon Pilih data",
                        // content: error.join('<br>'),
                        // number: error.length
                    });
                }
                // $scope.show_modalReject={show:true};
                // console.log("reject Billing");
            }
            
        }

        $scope.rejectCancel = function() {
            console.log('item reject');
        }

        $scope.rejectSave = function() {
            console.log("mBiliing", $scope.mBilling);
            console.log("data", $scope.dataReject[0].BillingId);
            BillingFactory.RejectCancelBilling($scope.mBilling, $scope.dataReject[0], $scope.user)
                .then(
                    function(res) {
                        bsNotify.show({
                            size: 'small',
                            type: 'success',
                            title: "Data Berhasil Disimpan",
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                        $scope.show_modalReject = { show: false };
                        $scope.mBilling.RejectMessage = "";
                        $scope.getData();
                        console.log("berhasil simpan");

                        //Kabeng sudah Reject
                        var DataNotif = {};
                        var messagetemp = "";
                        messagetemp = "Nama Permohonan : Permohonan Approval (Approved) <br> " +
                            "No. Billing : " + $scope.dataReject[0].BillingNo + " <br> " +
                            "No. WO : " + $scope.dataReject[0].Job.WoNo + "<br> " +
                            "Alasan : " + $scope.mBilling.RejectMessage;

                        console.log("messagetemp", messagetemp);


                        DataNotif.Message = messagetemp;
                        BillingFactory.sendNotifRole(DataNotif, 1044, 34).then(
                            function(res) {

                            },
                            function(err) {
                                //console.log("err=>", err);
                            });
                        $scope.mBilling.RejectMessage = "";
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        }


        $scope.dataReject = [];
        $scope.approveBilling = function() {
            console.log("selectrowsLc", selectrowsLc);
            var adaYgBukanReq = 0
            for (var i=0; i<selectrowsLc.length; i++){
                if (selectrowsLc[i].StatusBilling == 3 || selectrowsLc[i].StatusBilling == 4){
                    adaYgBukanReq++;
                }
            }
            if (adaYgBukanReq != 0){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Status Billing Ada Yang Sudah di Approve / Reject",
                });
            } else {
                var adaWithoutApp = 0;
                for (var i=0; i<selectrowsLc.length; i++){
                    if (selectrowsLc[i].StatusBillingName == 'Cancel Without Approval'){
                        adaWithoutApp++;
                    }
                }
                if (adaWithoutApp > 0){
                    if (adaWithoutApp == selectrowsLc.length){
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Status Billing Cancel Without Approval",
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                    } else {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Ada Billing Yang Tidak Perlu Approval",
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                    }
                    
                } else {
                    $scope.mBilling.CancelType = "";
                    if (selectrowsLc.length !== 0) {
                        BillingFactory.getDataCancel(selectrowsLc[0].BillingId)
                            .then(
                                function(res) {
                                    $scope.dataReject = res.data.Result;
                                    if ($scope.showw === 2) {
                                        $scope.show_modalApprove = { show: true };
                                    } else if ($scope.showw === 3) {
                                        $scope.show_modalReprintApprove = { show: true };
                                    }
                                    console.log("data approve", $scope.dataReject);
                                    $scope.dataReject111 = res.data.Result[0].ChargeTo;
                                    console.log('aaa', $scope.dataReject111, res.data.Result[0].ChargeTo)
        
                                    PartsGlobal.getApprover($scope.ApprovalProcessId).then(function(res) {
                                            var Approvers = res.data.Result[0].Approvers;
                                            console.log("getApprover res = ", Approvers);
                                            $scope.dataReject[0].Approvers = Approvers;
                                            //$scope.ApprovalData.Approvers = Approvers;
                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                        }
                                    );
                                    switch ($scope.dataReject[0].ChargeTo) {
                                        case 28:
                                            $scope.dataReject[0].xChargeTo = "Customer";
                                            break;
                                        case 29:
                                            $scope.dataReject[0].xChargeTo = "Insurance";
                                            break;
                                        case 30:
                                            $scope.dataReject[0].xChargeTo = "Waranty";
                                            break;
                                        case 31:
                                            $scope.dataReject[0].xChargeTo = "Internal";
                                            break;
                                        case 32:
                                            $scope.dataReject[0].xChargeTo = "Third Party";
                                            break;
                                        case 2277:
                                            $scope.dataReject[0].xChargeTo = "Free Service";
                                            break;
                                        case 2458:
                                            $scope.dataReject[0].xChargeTo = "PKS";
                                            break;
                                    }
        
                                    $timeout(function() {
                                        $('div.modal[show="show"]').css('margin-top', '-240px');
                                    }, 100)
                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
        
        
                    } else {
        
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Mohon Pilih data",
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                    }
                }

            }
            
            
        }

        $scope.approveSave = function() {
            console.log("mBiliing", $scope.mBilling);
            console.log("data", $scope.dataReject[0] != undefined ? $scope.dataReject[0] : $scope.dataReject);

            if ($scope.dataReject[0] != undefined || $scope.dataReject[0] != null) {
                BillingFactory.ApproveCancelBilling($scope.mBilling, $scope.dataReject[0], $scope.user)
                    .then(function(res) {
                        bsNotify.show({
                            size: 'small',
                            type: 'success',
                            title: "Data Berhasil Disimpan",
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                            $scope.show_modalApprove = { show: false };
                            $scope.getData();
                            console.log("berhasil simpan");
                            // BillingFactory.ReqCancelBilling($scope.mBilling, $scope.dataReject[0].BillingId, $scope.dataReject[0].CancelType, $scope.dataReject[0].CancelMessage)
                            //     .then(
                            //     function (res) {
                            //     },
                            //     function (err) {
                            //         console.log("err=>", err);
                            //     });


                            //Kabeng sudah approve
                            var DataNotif = {};
                            var messagetemp = "";
                            messagetemp = "Nama Permohonan : Permohonan Approval (Approved) <br> " +
                                "No. Billing : " + $scope.dataReject[0].BillingNo + " <br> " +
                                "No. WO : " + $scope.dataReject[0].Job.WoNo + "<br> " +
                                "Alasan : " + $scope.mBilling.RejectMessage;

                            console.log("messagetemp", messagetemp);


                            DataNotif.Message = messagetemp;
                            BillingFactory.sendNotifRole(DataNotif, 1044, 33).then(
                                function(res) {

                                },
                                function(err) {
                                    //console.log("err=>", err);
                                });
                            $scope.mBilling.RejectMessage = "";

                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
            } else {
                BillingFactory.ApproveCancelBilling($scope.mBilling, $scope.dataReject, $scope.user)
                    .then(function(res) {
                            $scope.show_modalApprove = { show: false };
                            $scope.getData();
                            console.log("berhasil simpan");
                            // BillingFactory.ReqCancelBilling($scope.mBilling, $scope.dataReject[0].BillingId, $scope.dataReject[0].CancelType, $scope.dataReject[0].CancelMessage)
                            //     .then(
                            //     function (res) {
                            //     },
                            //     function (err) {
                            //         console.log("err=>", err);
                            //     });


                            //Kabeng sudah approve
                            var DataNotif = {};
                            var messagetemp = "";
                            messagetemp = "Nama Permohonan : Permohonan Approval (Approved) <br> " +
                                "No. Billing : " + $scope.dataReject.BillingNo + " <br> " +
                                "No. WO : " + $scope.dataReject.Job.WoNo + "<br> " +
                                "Alasan : " + $scope.mBilling.RejectMessage;

                            console.log("messagetemp", messagetemp);


                            DataNotif.Message = messagetemp;
                            BillingFactory.sendNotifRole(DataNotif, 1044, 33).then(
                                function(res) {

                                },
                                function(err) {
                                    //console.log("err=>", err);
                                });
                            $scope.mBilling.RejectMessage = "";

                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
            }
        }
        $scope.approveCancel = function() {
            console.log('item approve');
            $scope.mBilling.CancelType = "";
            $scope.mBilling.RejectMessage = "";
        }

        // $scope.formApi = {};
        $scope.rejectSaveBillTO = function() {

            var b = new Date($scope.date);
            var yearLast = b.getFullYear();
            var monthLast = b.getMonth() + 1;
            var dayLast = b.getDate();
            var DateNow = yearLast + '-' + monthLast + '-' + dayLast;

            console.log('data kode faktur pajak', $scope.codeTransaction)
            console.log('$scope.mBilling.dataPayment', $scope.mBilling.dataPayment);
            console.log('$scope.mBilling.BillAddress', $scope.mBilling.BillAddress);
            console.log('$scope.mBilling.TIN', $scope.mBilling.TIN);
            console.log('$scope.mBilling.Insurance', $scope.mBilling.dataPayment);
            console.log('$scope.dataCRM >>', $scope.dataCRM);
            console.log('$scope.mBilling.CodeTransaction >>', $scope.mBilling.CodeTransaction);

            for (var i=0; i<$scope.codeTransaction.length; i++){
                if ($scope.mBilling.CodeTransaction == $scope.codeTransaction[i].code){
                    $scope.mBilling.IdCodeTransaction = $scope.codeTransaction[i].CodeInvoiceTransactionTaxId
                    // $scope.mBilling.IdCodeTransaction = $scope.codeTransaction[i].Code

                }
            }

            if ((($scope.mBilling.dataPayment != undefined) || ($scope.mBilling.dataPayment != null)) ||
                (($scope.mBilling.BillAddress != undefined) || ($scope.mBilling.BillAddress != null)) ||
                // (($scope.mBilling.CodeTransaction != undefined) || ($scope.mBilling.CodeTransaction != null)) ||
                (($scope.mBilling.TIN != undefined) || ($scope.mBilling.TIN != null)) ||
                (($scope.mBilling.Insurance != undefined) || ($scope.mBilling.Insurance != null))
            ) {

                // set discount kl beda pembayaran antara wo dan billing ======================================================================= start
                for (var i=0; i<$scope.listSelectedRows.length; i++){
                    if ($scope.mBilling.dataPayment == 29){
                        $scope.listSelectedRows[i].Discount = $scope.dataInsurance[0].DiscountJasa == null ? 0 : $scope.dataInsurance[0].DiscountJasa;
                    }
                }
                for (var i=0; i<$scope.listSelectedRowsOPL.length; i++){
                    if ($scope.mBilling.dataPayment == 29){
                        $scope.listSelectedRowsOPL[i].Discount = $scope.dataInsurance[0].DiscountJasa == null ? 0 : $scope.dataInsurance[0].DiscountJasa;
                    }
                }
                for (var i=0; i<$scope.listSelectedRowsOpb.length; i++){
                    if ($scope.mBilling.dataPayment == 29){
                        $scope.listSelectedRowsOpb[i].Discount = $scope.dataInsurance[0].DiscountPart == null ? 0 : $scope.dataInsurance[0].DiscountPart;
                    }
                }
                for (var i=0; i<$scope.listSelectedRowsParts.length; i++){
                    if ($scope.mBilling.dataPayment == 29){
                        $scope.listSelectedRowsParts[i].Discount = $scope.dataInsurance[0].DiscountPart == null ? 0 : $scope.dataInsurance[0].DiscountPart;
                    }
                }

                // set discount kl beda pembayaran antara wo dan billing ======================================================================= end

                // BillingFactory.cekDataAlreadyBilling($scope.mBilling, DateNow, $scope.listSelectedRows, $scope.listSelectedRowsParts, $scope.listSelectedRowsOPL, $scope.TotalFinal, $scope.finalAlldiscount, $scope.tax, $scope.TotalAfterDiscountFinal, $scope.DP, $scope.listSelectedRowsOpb, $scope.dataByJobId, PPNPerc)
                //     .then(function(resda){
                        // if (resda.data == 666){
                        //     bsNotify.show({
                        //         size: 'big',
                        //         type: 'danger',
                        //         title: "Ada Data Yang Sudah di Billing, Silahkan Refresh Data",
                        //     });
                        //     $scope.countCreateBilling = 0;
                        // } else {
                            // Aftifkan Simpan button
                            $(".ladda-button").attr('disabled', 'disabled');
                            $scope.TimeFinishBilling = new Date()
                            $scope.TimeStartBilling = $filter('date')($scope.TimeStartBilling, 'yyyy-MM-dd HH:mm:ss');
                            $scope.TimeFinishBilling = $filter('date')($scope.TimeFinishBilling, 'yyyy-MM-dd HH:mm:ss');
                            $scope.mBilling.TimeStartBilling = $scope.TimeStartBilling
                            $scope.mBilling.TimeFinishBilling = $scope.TimeFinishBilling

							//If(valid){BillingFactory.create} stv valid save
							
							// CR5 #27
							if($scope.mBilling.dataPayment == 28 || $scope.mBilling.dataPayment == 32){
								//Customer / Third Party
								
								console.log('masuk');
								if($scope.mBilling.Nationality.Id == 0){
									//WNI
									$scope.mBilling.isWNA = 0;
									console.log('masuk0');
								}else if($scope.mBilling.Nationality.Id == 1){
									//WNA
									console.log('masuk1');
									$scope.mBilling.isWNA = 1;
								}

								if($scope.mBilling.Nationality.Id == 0){
									//WNI
									if($scope.mBilling.isNPWP == 0){
										//WNI -> NPWP
										if($scope.mBilling.KTPRaw == null || $scope.mBilling.KTPRaw == "" || $scope.mBilling.KTPRaw == undefined){
											$scope.mBilling.KTPKITAS = "0000000000000000";
										}else{
											$scope.mBilling.KTPKITAS = $scope.mBilling.KTPRaw;
										}

										//if($scope.mBilling.TIN.length == 0 || $scope.mBilling.TIN == "00.000.000.0-000.000" ){
										console.log('$scope.mBilling.TIN at A : ', $scope.mBilling.TIN);
										if($scope.mBilling.TIN == null || $scope.mBilling.TIN == "" || $scope.mBilling.TIN == "00.000.000.0-000.000"){
											$(".ladda-button").removeAttr('disabled', '');
											bsAlert.alert({
												/*title: "Data NPWP tidak boleh 00.000.000.0-000.000 / kosong",
												text: "",
												type: "warning"*/
												title: "Data No. NPWP Pembayaran diisi 00.000.000.0-000.000 / kosong, Apakah Anda Akan Melanjutkan Transaksi?",
												text: "",
												type: "question",
												showCancelButton: true
											},
											function(){
												console.log("Simpan NPWP kosong A");
												
												BillingFactory.create($scope.mBilling, DateNow, $scope.listSelectedRows, $scope.listSelectedRowsParts, $scope.listSelectedRowsOPL, $scope.TotalFinal, $scope.finalAlldiscount, $scope.tax, $scope.TotalAfterDiscountFinal, $scope.DP, $scope.listSelectedRowsOpb, $scope.dataByJobId, PPNPerc)
													.then(function(resBilling) {

														// debugger;
														$scope.resBillingMSG = resBilling.data.ResponseMessage;
														var responseBill = angular.copy(resBilling.data);

														// Jika petugas billingnya memilih Charge To nya PKS
														if (responseBill.ResponseCode == 44) {
															bsNotify.show({
																size: 'big',
																type: 'danger',
																title: "Data kendaraan ini tidak terdaftar sebagai member PKS.",
																// content: error.join('<br>'),
																// number: error.length
															});
															return -1;
														}

                                                        if (responseBill.ResponseCode == 67 || responseBill.ResponseCode == 99) {
															bsNotify.show({
																size: 'big',
																type: 'danger',
																title: "Tidak bisa Billing karena ada perubahan status. Silahkan Refresh",
																// content: error.join('<br>'),
																// number: error.length
															});
															return -1;
														}

														if ($scope.dataCRM[0].ToyotaId == null) {
															BillingFactory.getDataCRM($scope.dataCRM[0].VehicleId).then(function(resx) {
																// $scope.DataPemeriksaanAwal = res.data.Result;
																var dataCRM = resx.data.Result[0];
																console.log("data CRM", dataCRM);

																// debugger;
																if (dataCRM.ToyotaId != null) {
																	WO.CustomerInsertTOCRMTAM(dataCRM.CustomerOwnerId).then(function(resu) {
																		console.log('resu >>', resu.data);
																		BillingFactory.InserToVehicTR(dataCRM.VIN, dataCRM.ToyotaId).then(function(resux) {
																			console.log('resux >>', resux.data);
																		});
																	});
																};

																// console.log("First Check", $scope.DataPemeriksaanAwal);
																// $scope.loading=false;
															});
														} else { //Insert ke CCustomer_Vehicle_TR di CRM_TAM.
															BillingFactory.InserToVehicTR($scope.dataCRM[0].VIN, $scope.dataCRM[0].ToyotaId).then(function(resux) {
																console.log('resux >>', resux.data);
															});
														};

														BillingFactory.getDataEmployeeSa($scope.mBilling.JobId)
															.then(
																function(res) {
																	$scope.mBilling.dataPayment = "";
																	$scope.mBilling.BillTo = "";
																	$scope.mBilling.BillAddress = "";
																	$scope.mBilling.TaxCode = 0;
																	$scope.mBilling.TIN = "";
																	$scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
																	$scope.mBilling.TaxName = "";
																	$scope.mBilling.TaxAddress = "";
																	$scope.listSelectedRows = [];
																	console.log("$scope.listSelectedRows", $scope.listSelectedRows);
																	// $scope.listSelectedRows.splice(0);
																	$scope.listSelectedRowsParts = [];
																	console.log("$scope.listSelectedRowsParts", $scope.listSelectedRowsParts);
																	$scope.listSelectedRowsOPL = [];
																	$scope.listSelectedRowsOpb = [];

																	// $scope.formApi.resetForm(true);

																	$scope.gridWork = [];
																	$scope.partsData = [];
																	$scope.oplData = [];
																	// $scope.DataPemeriksaanAwal = res.data.Result;
																	$scope.dataByJobId = res.data.Result;

																	for (i = 0; i < $scope.dataByJobId.length; i++) {
																		// for(z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++){
																		//     TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
																		//     $scope.subTotalOpl = TotalOpl;
																		// }

																		for (z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++) {

																			// TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
																			// $scope.subTotalOpl = TotalOpl;

																			if ($scope.dataByJobId[i].JobTaskOpl[z].IsBilling < 1 && $scope.dataByJobId[i].JobTaskOpl[z].isDeleted != 1) {
																				$scope.oplData.push($scope.dataByJobId[i].JobTaskOpl[z]);
																			} else {
																				console.log("Masuk else OplData");
																			}
																		}

																		for (j = 0; j < $scope.dataByJobId[i].JobTask.length; j++) {
																			if ($scope.dataByJobId[i].JobTask[j].IsBilling < 1) {
																				$scope.gridWork.push($scope.dataByJobId[i].JobTask[j]);
																			} else {
																				console.log("masuk else JobTask sudah Billing");
																			}

																			// $scope.subTotalLabor += $scope.dataByJobId[i].JobTask[j].Fare;

																			for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
																				if ($scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0){
																					if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
																						if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB != 1) {
																							if ($scope.dataByJobId[i].JobTask[j].JobParts[a].Price == null || $scope.dataByJobId[i].JobTask[j].JobParts[a].Price == 0) {
																								$scope.dataByJobId[i].JobTask[j].JobParts[a].Price = $scope.dataByJobId[i].JobTask[j].JobParts[a].Part.RetailPrice;
																							}
																							$scope.partsData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
																						} else {
																							console.log("ada data yang opb");
																						}
																					} else {
																						console.log("mausk else Job Parts Sudah Ada Billing");
																					}
																					// TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
																					// $scope.subTotalParts = TotalParts;
																				}
																				
																			}

																			for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
																				if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
																					if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB == 1 &&
																						$scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0) {
																						$scope.opbData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
																					} else {
																						console.log("tidak ada data Opb");
																					}
																				} else {
																					console.log("mausk else Job Parts Sudah Ada Billing");
																				}
																				// TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
																				// $scope.subTotalParts = TotalParts;
																			}
																		}
																	}
																	$scope.show_modalDtl = { show: false };
																	$scope.modal_modelDtl = [];
																	// $scope.cetakBilingafterSave($scope.dataByJobId,$scope.TotalFinal,$scope.resBillingMSG);
																	$scope.formApi.setMode('grid');

																	// $scope.listSelectedRowsParts = [];
																	$scope.subTotalLabor = 0;
																	$scope.discountPercentFinalTask = 0;
																	$scope.discountFinalTask = 0;
																	$scope.subTotalOpl = 0;
																	$scope.discountOPl = 0;
																	$scope.subTotalParts = 0;
																	$scope.discountPercentFinalParts = 0;
																	$scope.discountFinalParts = 0;
																	$scope.subTotalOpb = 0;
																	$scope.discountFinalPartsOpb = 0;
																	$scope.discountPercentFinalOpb = 0;
																	$scope.discountPercentFinalOpl = 0;
																	$scope.discountFinalOpl = 0;
																	$scope.TotalFinal = 0;
																	$scope.TotalAfterDiscountFinal = 0;
																	$scope.Materai = 0;
																	$scope.tax = 0;
																	$scope.DP = 0;
																	$scope.DPPaid = 0;

																	// Aftifkan Simpan button
																	setTimeout(function() { $(".ladda-button").removeAttr('disabled', '') }, 2000);

																	$scope.formApi.setMode('grid');
																},
																function(err) {
																	console.log("err=>", err);
																	$(".ladda-button").removeAttr('disabled', '');
																}
															);

														console.log("save");
														bsNotify.show({
															size: 'small',
															type: 'success',
															title: "Data Berhasil Disimpan",
															// content: error.join('<br>'),
															// number: error.length
														});
														$scope.getData();
														console.log("$scope.showw", $scope.showw, $scope.filter);

														// console.log("First Check", $scope.DataPemeriksaanAwal);
														// $scope.loading=false;

														//kirim notifikasi
														var DataNotif = {};
														var messagetemp = "";
														var pekerjaanNotif = "";
														console.log("data pekerjaan", $scope.listSelectedRows);
														if ($scope.listSelectedRows != undefined) {
															for (var i = 0; i < $scope.listSelectedRows.length; i++) {
																pekerjaanNotif = $scope.listSelectedRows[i].TaskName + ", " + pekerjaanNotif;
															}
														}
														messagetemp = "( " + $scope.mBilling.PoliceNumber + " / " + pekerjaanNotif + ")";

														DataNotif.Message = messagetemp;
														//1028 = SAGR
														//1029 = SABP

														var SAId = 0
														if ($scope.mBilling.TypeJob == "GR") {
															SAId = "1028";
														} else {
															SAId = "1029";
														}
														BillingFactory.sendNotifRole(DataNotif, SAId, 26).then(
															function(res) {

															},
															function(err) {
																//console.log("err=>", err);
																$(".ladda-button").removeAttr('disabled', '');
															});

														$scope.getData(); //0149-SIT-033- DeliveryBilling
													},
													function(err) {
														console.log("err=>", err);
														$(".ladda-button").removeAttr('disabled', '');
													}
												);

												//$scope.countCreateBilling = 0;
												//return false;

												// Aftifkan Simpan button
												// setTimeout(function() { $(".ladda-button").removeAttr('disabled', '') }, 200);

												// $(".ladda-button").attr('disabled', 'disabled');
											},
											function(){
												console.log("Tidak Simpan");
												$scope.countCreateBilling = 0;
												return false;
											})

										}else{
											//Save
											BillingFactory.create($scope.mBilling, DateNow, $scope.listSelectedRows, $scope.listSelectedRowsParts, $scope.listSelectedRowsOPL, $scope.TotalFinal, $scope.finalAlldiscount, $scope.tax, $scope.TotalAfterDiscountFinal, $scope.DP, $scope.listSelectedRowsOpb, $scope.dataByJobId, PPNPerc)
												.then(function(resBilling) {

													// debugger;
													$scope.resBillingMSG = resBilling.data.ResponseMessage;
													var responseBill = angular.copy(resBilling.data);

													// Jika petugas billingnya memilih Charge To nya PKS
													if (responseBill.ResponseCode == 44) {
														bsNotify.show({
															size: 'big',
															type: 'danger',
															title: "Data kendaraan ini tidak terdaftar sebagai member PKS.",
															// content: error.join('<br>'),
															// number: error.length
														});
														return -1;
													}

                                                    if (responseBill.ResponseCode == 67 || responseBill.ResponseCode == 99) {
                                                        bsNotify.show({
                                                            size: 'big',
                                                            type: 'danger',
                                                            title: "Tidak bisa Billing karena ada perubahan status. Silahkan Refresh",
                                                            // content: error.join('<br>'),
                                                            // number: error.length
                                                        });
                                                        return -1;
                                                    }

													if ($scope.dataCRM[0].ToyotaId == null) {
														BillingFactory.getDataCRM($scope.dataCRM[0].VehicleId).then(function(resx) {
															// $scope.DataPemeriksaanAwal = res.data.Result;
															var dataCRM = resx.data.Result[0];
															console.log("data CRM", dataCRM);

															// debugger;
															if (dataCRM.ToyotaId != null) {
																WO.CustomerInsertTOCRMTAM(dataCRM.CustomerOwnerId).then(function(resu) {
																	console.log('resu >>', resu.data);
																	BillingFactory.InserToVehicTR(dataCRM.VIN, dataCRM.ToyotaId).then(function(resux) {
																		console.log('resux >>', resux.data);
																	});
																});
															};

															// console.log("First Check", $scope.DataPemeriksaanAwal);
															// $scope.loading=false;
														});
													} else { //Insert ke CCustomer_Vehicle_TR di CRM_TAM.
														BillingFactory.InserToVehicTR($scope.dataCRM[0].VIN, $scope.dataCRM[0].ToyotaId).then(function(resux) {
															console.log('resux >>', resux.data);
														});
													};

													BillingFactory.getDataEmployeeSa($scope.mBilling.JobId)
														.then(
															function(res) {
																$scope.mBilling.dataPayment = "";
																$scope.mBilling.BillTo = "";
																$scope.mBilling.BillAddress = "";
																$scope.mBilling.TaxCode = 0;
																$scope.mBilling.TIN = "";
																$scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
																$scope.mBilling.TaxName = "";
																$scope.mBilling.TaxAddress = "";
																$scope.listSelectedRows = [];
																console.log("$scope.listSelectedRows", $scope.listSelectedRows);
																// $scope.listSelectedRows.splice(0);
																$scope.listSelectedRowsParts = [];
																console.log("$scope.listSelectedRowsParts", $scope.listSelectedRowsParts);
																$scope.listSelectedRowsOPL = [];
																$scope.listSelectedRowsOpb = [];

																// $scope.formApi.resetForm(true);

																$scope.gridWork = [];
																$scope.partsData = [];
																$scope.oplData = [];
																// $scope.DataPemeriksaanAwal = res.data.Result;
																$scope.dataByJobId = res.data.Result;

																for (i = 0; i < $scope.dataByJobId.length; i++) {
																	// for(z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++){
																	//     TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
																	//     $scope.subTotalOpl = TotalOpl;
																	// }

																	for (z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++) {

																		// TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
																		// $scope.subTotalOpl = TotalOpl;

																		if ($scope.dataByJobId[i].JobTaskOpl[z].IsBilling < 1 && $scope.dataByJobId[i].JobTaskOpl[z].isDeleted != 1) {
																			$scope.oplData.push($scope.dataByJobId[i].JobTaskOpl[z]);
																		} else {
																			console.log("Masuk else OplData");
																		}
																	}

																	for (j = 0; j < $scope.dataByJobId[i].JobTask.length; j++) {
																		if ($scope.dataByJobId[i].JobTask[j].IsBilling < 1) {
																			$scope.gridWork.push($scope.dataByJobId[i].JobTask[j]);
																		} else {
																			console.log("masuk else JobTask sudah Billing");
																		}

																		// $scope.subTotalLabor += $scope.dataByJobId[i].JobTask[j].Fare;

																		for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
																			if ($scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0){
																				if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
																					if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB != 1) {
																						if ($scope.dataByJobId[i].JobTask[j].JobParts[a].Price == null || $scope.dataByJobId[i].JobTask[j].JobParts[a].Price == 0) {
																							$scope.dataByJobId[i].JobTask[j].JobParts[a].Price = $scope.dataByJobId[i].JobTask[j].JobParts[a].Part.RetailPrice;
																						}
																						$scope.partsData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
																					} else {
																						console.log("ada data yang opb");
																					}
																				} else {
																					console.log("mausk else Job Parts Sudah Ada Billing");
																				}
																				// TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
																				// $scope.subTotalParts = TotalParts;
																			}
																			
																		}

																		for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
																			if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
																				if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB == 1 &&
																					$scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0) {
																					$scope.opbData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
																				} else {
																					console.log("tidak ada data Opb");
																				}
																			} else {
																				console.log("mausk else Job Parts Sudah Ada Billing");
																			}
																			// TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
																			// $scope.subTotalParts = TotalParts;
																		}
																	}
																}
																$scope.show_modalDtl = { show: false };
																$scope.modal_modelDtl = [];
																// $scope.cetakBilingafterSave($scope.dataByJobId,$scope.TotalFinal,$scope.resBillingMSG);
																$scope.formApi.setMode('grid');

																// $scope.listSelectedRowsParts = [];
																$scope.subTotalLabor = 0;
																$scope.discountPercentFinalTask = 0;
																$scope.discountFinalTask = 0;
																$scope.subTotalOpl = 0;
																$scope.discountOPl = 0;
																$scope.subTotalParts = 0;
																$scope.discountPercentFinalParts = 0;
																$scope.discountFinalParts = 0;
																$scope.subTotalOpb = 0;
																$scope.discountFinalPartsOpb = 0;
																$scope.discountPercentFinalOpb = 0;
																$scope.discountPercentFinalOpl = 0;
																$scope.discountFinalOpl = 0;
																$scope.TotalFinal = 0;
																$scope.TotalAfterDiscountFinal = 0;
																$scope.Materai = 0;
																$scope.tax = 0;
																$scope.DP = 0;
																$scope.DPPaid = 0;

																// Aftifkan Simpan button
																setTimeout(function() { $(".ladda-button").removeAttr('disabled', '') }, 2000);

																$scope.formApi.setMode('grid');
															},
															function(err) {
																console.log("err=>", err);
																$(".ladda-button").removeAttr('disabled', '');
															}
														);

													console.log("save");
													bsNotify.show({
														size: 'small',
														type: 'success',
														title: "Data Berhasil Disimpan",
														// content: error.join('<br>'),
														// number: error.length
													});
													$scope.getData();
													console.log("$scope.showw", $scope.showw, $scope.filter);

													// console.log("First Check", $scope.DataPemeriksaanAwal);
													// $scope.loading=false;

													//kirim notifikasi
													var DataNotif = {};
													var messagetemp = "";
													var pekerjaanNotif = "";
													console.log("data pekerjaan", $scope.listSelectedRows);
													if ($scope.listSelectedRows != undefined) {
														for (var i = 0; i < $scope.listSelectedRows.length; i++) {
															pekerjaanNotif = $scope.listSelectedRows[i].TaskName + ", " + pekerjaanNotif;
														}
													}
													messagetemp = "( " + $scope.mBilling.PoliceNumber + " / " + pekerjaanNotif + ")";

													DataNotif.Message = messagetemp;
													//1028 = SAGR
													//1029 = SABP

													var SAId = 0
													if ($scope.mBilling.TypeJob == "GR") {
														SAId = "1028";
													} else {
														SAId = "1029";
													}
													BillingFactory.sendNotifRole(DataNotif, SAId, 26).then(
														function(res) {

														},
														function(err) {
															//console.log("err=>", err);
															$(".ladda-button").removeAttr('disabled', '');
														});

													$scope.getData(); //0149-SIT-033- DeliveryBilling
												},
												function(err) {
													console.log("err=>", err);
													$(".ladda-button").removeAttr('disabled', '');
												}
											);
										}
									}else if($scope.mBilling.isNPWP == 1){
										//WNI -> Tidak punya NPWP
										$scope.mBilling.TIN = "00.000.000.0-000.000";
										$scope.mBilling.KTPKITAS = $scope.mBilling.KTP;

										console.log('$scope.mBilling.KTP at validate save : ',$scope.mBilling.KTP);
										//if($scope.mBilling.KTP.length == 0 || $scope.mBilling.KTP == "0000000000000000"){
										if($scope.mBilling.KTP == null || $scope.mBilling.KTP == "" || $scope.mBilling.KTP == "0000000000000000"){
											$(".ladda-button").removeAttr('disabled', '');
                                            /*bsAlert.alert({
												title: "Mohon mengisi nomor KTP dari " + $scope.mBilling.BillTo + " pada kolom NPWP",
												text: "",
												type: "warning"
											})*/
											bsAlert.alert({
												title: "Data No. KTP Pembayaran diisi 0000000000000000 / kosong, Apakah Anda Akan Melanjutkan Transaksi?",
												text: "",
												type: "question",
												showCancelButton: true
											},
											function(){
												console.log("Simpan KTP kosong");
												BillingFactory.create($scope.mBilling, DateNow, $scope.listSelectedRows, $scope.listSelectedRowsParts, $scope.listSelectedRowsOPL, $scope.TotalFinal, $scope.finalAlldiscount, $scope.tax, $scope.TotalAfterDiscountFinal, $scope.DP, $scope.listSelectedRowsOpb, $scope.dataByJobId, PPNPerc)
													.then(function(resBilling) {

														// debugger;
														$scope.resBillingMSG = resBilling.data.ResponseMessage;
														var responseBill = angular.copy(resBilling.data);

														// Jika petugas billingnya memilih Charge To nya PKS
														if (responseBill.ResponseCode == 44) {
															bsNotify.show({
																size: 'big',
																type: 'danger',
																title: "Data kendaraan ini tidak terdaftar sebagai member PKS.",
																// content: error.join('<br>'),
																// number: error.length
															});
															return -1;
														}

                                                        if (responseBill.ResponseCode == 67 || responseBill.ResponseCode == 99) {
															bsNotify.show({
																size: 'big',
																type: 'danger',
																title: "Tidak bisa Billing karena ada perubahan status. Silahkan Refresh",
																// content: error.join('<br>'),
																// number: error.length
															});
															return -1;
														}

														if ($scope.dataCRM[0].ToyotaId == null) {
															BillingFactory.getDataCRM($scope.dataCRM[0].VehicleId).then(function(resx) {
																// $scope.DataPemeriksaanAwal = res.data.Result;
																var dataCRM = resx.data.Result[0];
																console.log("data CRM", dataCRM);

																// debugger;
																if (dataCRM.ToyotaId != null) {
																	WO.CustomerInsertTOCRMTAM(dataCRM.CustomerOwnerId).then(function(resu) {
																		console.log('resu >>', resu.data);
																		BillingFactory.InserToVehicTR(dataCRM.VIN, dataCRM.ToyotaId).then(function(resux) {
																			console.log('resux >>', resux.data);
																		});
																	});
																};

																// console.log("First Check", $scope.DataPemeriksaanAwal);
																// $scope.loading=false;
															});
														} else { //Insert ke CCustomer_Vehicle_TR di CRM_TAM.
															BillingFactory.InserToVehicTR($scope.dataCRM[0].VIN, $scope.dataCRM[0].ToyotaId).then(function(resux) {
																console.log('resux >>', resux.data);
															});
														};

														BillingFactory.getDataEmployeeSa($scope.mBilling.JobId)
															.then(
																function(res) {
																	$scope.mBilling.dataPayment = "";
																	$scope.mBilling.BillTo = "";
																	$scope.mBilling.BillAddress = "";
																	$scope.mBilling.TaxCode = 0;
																	$scope.mBilling.TIN = "";
																	$scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
																	$scope.mBilling.TaxName = "";
																	$scope.mBilling.TaxAddress = "";
																	$scope.listSelectedRows = [];
																	console.log("$scope.listSelectedRows", $scope.listSelectedRows);
																	// $scope.listSelectedRows.splice(0);
																	$scope.listSelectedRowsParts = [];
																	console.log("$scope.listSelectedRowsParts", $scope.listSelectedRowsParts);
																	$scope.listSelectedRowsOPL = [];
																	$scope.listSelectedRowsOpb = [];

																	// $scope.formApi.resetForm(true);

																	$scope.gridWork = [];
																	$scope.partsData = [];
																	$scope.oplData = [];
																	// $scope.DataPemeriksaanAwal = res.data.Result;
																	$scope.dataByJobId = res.data.Result;

																	for (i = 0; i < $scope.dataByJobId.length; i++) {
																		// for(z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++){
																		//     TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
																		//     $scope.subTotalOpl = TotalOpl;
																		// }

																		for (z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++) {

																			// TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
																			// $scope.subTotalOpl = TotalOpl;

																			if ($scope.dataByJobId[i].JobTaskOpl[z].IsBilling < 1 && $scope.dataByJobId[i].JobTaskOpl[z].isDeleted != 1) {
																				$scope.oplData.push($scope.dataByJobId[i].JobTaskOpl[z]);
																			} else {
																				console.log("Masuk else OplData");
																			}
																		}

																		for (j = 0; j < $scope.dataByJobId[i].JobTask.length; j++) {
																			if ($scope.dataByJobId[i].JobTask[j].IsBilling < 1) {
																				$scope.gridWork.push($scope.dataByJobId[i].JobTask[j]);
																			} else {
																				console.log("masuk else JobTask sudah Billing");
																			}

																			// $scope.subTotalLabor += $scope.dataByJobId[i].JobTask[j].Fare;

																			for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
																				if ($scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0){
																					if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
																						if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB != 1) {
																							if ($scope.dataByJobId[i].JobTask[j].JobParts[a].Price == null || $scope.dataByJobId[i].JobTask[j].JobParts[a].Price == 0) {
																								$scope.dataByJobId[i].JobTask[j].JobParts[a].Price = $scope.dataByJobId[i].JobTask[j].JobParts[a].Part.RetailPrice;
																							}
																							$scope.partsData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
																						} else {
																							console.log("ada data yang opb");
																						}
																					} else {
																						console.log("mausk else Job Parts Sudah Ada Billing");
																					}
																					// TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
																					// $scope.subTotalParts = TotalParts;
																				}
																				
																			}

																			for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
																				if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
																					if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB == 1 &&
																						$scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0) {
																						$scope.opbData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
																					} else {
																						console.log("tidak ada data Opb");
																					}
																				} else {
																					console.log("mausk else Job Parts Sudah Ada Billing");
																				}
																				// TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
																				// $scope.subTotalParts = TotalParts;
																			}
																		}
																	}
																	$scope.show_modalDtl = { show: false };
																	$scope.modal_modelDtl = [];
																	// $scope.cetakBilingafterSave($scope.dataByJobId,$scope.TotalFinal,$scope.resBillingMSG);
																	$scope.formApi.setMode('grid');

																	// $scope.listSelectedRowsParts = [];
																	$scope.subTotalLabor = 0;
																	$scope.discountPercentFinalTask = 0;
																	$scope.discountFinalTask = 0;
																	$scope.subTotalOpl = 0;
																	$scope.discountOPl = 0;
																	$scope.subTotalParts = 0;
																	$scope.discountPercentFinalParts = 0;
																	$scope.discountFinalParts = 0;
																	$scope.subTotalOpb = 0;
																	$scope.discountFinalPartsOpb = 0;
																	$scope.discountPercentFinalOpb = 0;
																	$scope.discountPercentFinalOpl = 0;
																	$scope.discountFinalOpl = 0;
																	$scope.TotalFinal = 0;
																	$scope.TotalAfterDiscountFinal = 0;
																	$scope.Materai = 0;
																	$scope.tax = 0;
																	$scope.DP = 0;
																	$scope.DPPaid = 0;

																	// Aftifkan Simpan button
																	setTimeout(function() { $(".ladda-button").removeAttr('disabled', '') }, 2000);

																	$scope.formApi.setMode('grid');
																},
																function(err) {
																	console.log("err=>", err);
																	$(".ladda-button").removeAttr('disabled', '');
																}
															);

														console.log("save");
														bsNotify.show({
															size: 'small',
															type: 'success',
															title: "Data Berhasil Disimpan",
															// content: error.join('<br>'),
															// number: error.length
														});
														$scope.getData();
														console.log("$scope.showw", $scope.showw, $scope.filter);

														// console.log("First Check", $scope.DataPemeriksaanAwal);
														// $scope.loading=false;

														//kirim notifikasi
														var DataNotif = {};
														var messagetemp = "";
														var pekerjaanNotif = "";
														console.log("data pekerjaan", $scope.listSelectedRows);
														if ($scope.listSelectedRows != undefined) {
															for (var i = 0; i < $scope.listSelectedRows.length; i++) {
																pekerjaanNotif = $scope.listSelectedRows[i].TaskName + ", " + pekerjaanNotif;
															}
														}
														messagetemp = "( " + $scope.mBilling.PoliceNumber + " / " + pekerjaanNotif + ")";

														DataNotif.Message = messagetemp;
														//1028 = SAGR
														//1029 = SABP

														var SAId = 0
														if ($scope.mBilling.TypeJob == "GR") {
															SAId = "1028";
														} else {
															SAId = "1029";
														}
														BillingFactory.sendNotifRole(DataNotif, SAId, 26).then(
															function(res) {

															},
															function(err) {
																//console.log("err=>", err);
																$(".ladda-button").removeAttr('disabled', '');
															});

														$scope.getData(); //0149-SIT-033- DeliveryBilling
													},
													function(err) {
														console.log("err=>", err);
														$(".ladda-button").removeAttr('disabled', '');
													}
												);
											},
											function(){
												console.log("Tidak simpan");
												$scope.countCreateBilling = 0;
												return false;
											})
											//$scope.countCreateBilling = 0;
											//return false;
										}else{
											//Save
											BillingFactory.create($scope.mBilling, DateNow, $scope.listSelectedRows, $scope.listSelectedRowsParts, $scope.listSelectedRowsOPL, $scope.TotalFinal, $scope.finalAlldiscount, $scope.tax, $scope.TotalAfterDiscountFinal, $scope.DP, $scope.listSelectedRowsOpb, $scope.dataByJobId, PPNPerc)
												.then(function(resBilling) {

													// debugger;
													$scope.resBillingMSG = resBilling.data.ResponseMessage;
													var responseBill = angular.copy(resBilling.data);

													// Jika petugas billingnya memilih Charge To nya PKS
													if (responseBill.ResponseCode == 44) {
														bsNotify.show({
															size: 'big',
															type: 'danger',
															title: "Data kendaraan ini tidak terdaftar sebagai member PKS.",
															// content: error.join('<br>'),
															// number: error.length
														});
														return -1;
													}

                                                    if (responseBill.ResponseCode == 67 || responseBill.ResponseCode == 99) {
                                                        bsNotify.show({
                                                            size: 'big',
                                                            type: 'danger',
                                                            title: "Tidak bisa Billing karena ada perubahan status. Silahkan Refresh",
                                                            // content: error.join('<br>'),
                                                            // number: error.length
                                                        });
                                                        return -1;
                                                    }

													if ($scope.dataCRM[0].ToyotaId == null) {
														BillingFactory.getDataCRM($scope.dataCRM[0].VehicleId).then(function(resx) {
															// $scope.DataPemeriksaanAwal = res.data.Result;
															var dataCRM = resx.data.Result[0];
															console.log("data CRM", dataCRM);

															// debugger;
															if (dataCRM.ToyotaId != null) {
																WO.CustomerInsertTOCRMTAM(dataCRM.CustomerOwnerId).then(function(resu) {
																	console.log('resu >>', resu.data);
																	BillingFactory.InserToVehicTR(dataCRM.VIN, dataCRM.ToyotaId).then(function(resux) {
																		console.log('resux >>', resux.data);
																	});
																});
															};

															// console.log("First Check", $scope.DataPemeriksaanAwal);
															// $scope.loading=false;
														});
													} else { //Insert ke CCustomer_Vehicle_TR di CRM_TAM.
														BillingFactory.InserToVehicTR($scope.dataCRM[0].VIN, $scope.dataCRM[0].ToyotaId).then(function(resux) {
															console.log('resux >>', resux.data);
														});
													};

													BillingFactory.getDataEmployeeSa($scope.mBilling.JobId)
														.then(
															function(res) {
																$scope.mBilling.dataPayment = "";
																$scope.mBilling.BillTo = "";
																$scope.mBilling.BillAddress = "";
																$scope.mBilling.TaxCode = 0;
																$scope.mBilling.TIN = "";
																$scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
																$scope.mBilling.TaxName = "";
																$scope.mBilling.TaxAddress = "";
																$scope.listSelectedRows = [];
																console.log("$scope.listSelectedRows", $scope.listSelectedRows);
																// $scope.listSelectedRows.splice(0);
																$scope.listSelectedRowsParts = [];
																console.log("$scope.listSelectedRowsParts", $scope.listSelectedRowsParts);
																$scope.listSelectedRowsOPL = [];
																$scope.listSelectedRowsOpb = [];

																// $scope.formApi.resetForm(true);

																$scope.gridWork = [];
																$scope.partsData = [];
																$scope.oplData = [];
																// $scope.DataPemeriksaanAwal = res.data.Result;
																$scope.dataByJobId = res.data.Result;

																for (i = 0; i < $scope.dataByJobId.length; i++) {
																	// for(z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++){
																	//     TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
																	//     $scope.subTotalOpl = TotalOpl;
																	// }

																	for (z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++) {

																		// TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
																		// $scope.subTotalOpl = TotalOpl;

																		if ($scope.dataByJobId[i].JobTaskOpl[z].IsBilling < 1 && $scope.dataByJobId[i].JobTaskOpl[z].isDeleted != 1) {
																			$scope.oplData.push($scope.dataByJobId[i].JobTaskOpl[z]);
																		} else {
																			console.log("Masuk else OplData");
																		}
																	}

																	for (j = 0; j < $scope.dataByJobId[i].JobTask.length; j++) {
																		if ($scope.dataByJobId[i].JobTask[j].IsBilling < 1) {
																			$scope.gridWork.push($scope.dataByJobId[i].JobTask[j]);
																		} else {
																			console.log("masuk else JobTask sudah Billing");
																		}

																		// $scope.subTotalLabor += $scope.dataByJobId[i].JobTask[j].Fare;

																		for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
																			if ($scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0){
																				if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
																					if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB != 1) {
																						if ($scope.dataByJobId[i].JobTask[j].JobParts[a].Price == null || $scope.dataByJobId[i].JobTask[j].JobParts[a].Price == 0) {
																							$scope.dataByJobId[i].JobTask[j].JobParts[a].Price = $scope.dataByJobId[i].JobTask[j].JobParts[a].Part.RetailPrice;
																						}
																						$scope.partsData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
																					} else {
																						console.log("ada data yang opb");
																					}
																				} else {
																					console.log("mausk else Job Parts Sudah Ada Billing");
																				}
																				// TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
																				// $scope.subTotalParts = TotalParts;
																			}
																			
																		}

																		for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
																			if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
																				if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB == 1 &&
																					$scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0) {
																					$scope.opbData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
																				} else {
																					console.log("tidak ada data Opb");
																				}
																			} else {
																				console.log("mausk else Job Parts Sudah Ada Billing");
																			}
																			// TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
																			// $scope.subTotalParts = TotalParts;
																		}
																	}
																}
																$scope.show_modalDtl = { show: false };
																$scope.modal_modelDtl = [];
																// $scope.cetakBilingafterSave($scope.dataByJobId,$scope.TotalFinal,$scope.resBillingMSG);
																$scope.formApi.setMode('grid');

																// $scope.listSelectedRowsParts = [];
																$scope.subTotalLabor = 0;
																$scope.discountPercentFinalTask = 0;
																$scope.discountFinalTask = 0;
																$scope.subTotalOpl = 0;
																$scope.discountOPl = 0;
																$scope.subTotalParts = 0;
																$scope.discountPercentFinalParts = 0;
																$scope.discountFinalParts = 0;
																$scope.subTotalOpb = 0;
																$scope.discountFinalPartsOpb = 0;
																$scope.discountPercentFinalOpb = 0;
																$scope.discountPercentFinalOpl = 0;
																$scope.discountFinalOpl = 0;
																$scope.TotalFinal = 0;
																$scope.TotalAfterDiscountFinal = 0;
																$scope.Materai = 0;
																$scope.tax = 0;
																$scope.DP = 0;
																$scope.DPPaid = 0;

																// Aftifkan Simpan button
																setTimeout(function() { $(".ladda-button").removeAttr('disabled', '') }, 2000);

																$scope.formApi.setMode('grid');
															},
															function(err) {
																console.log("err=>", err);
																$(".ladda-button").removeAttr('disabled', '');
															}
														);

													console.log("save");
													bsNotify.show({
														size: 'small',
														type: 'success',
														title: "Data Berhasil Disimpan",
														// content: error.join('<br>'),
														// number: error.length
													});
													$scope.getData();
													console.log("$scope.showw", $scope.showw, $scope.filter);

													// console.log("First Check", $scope.DataPemeriksaanAwal);
													// $scope.loading=false;

													//kirim notifikasi
													var DataNotif = {};
													var messagetemp = "";
													var pekerjaanNotif = "";
													console.log("data pekerjaan", $scope.listSelectedRows);
													if ($scope.listSelectedRows != undefined) {
														for (var i = 0; i < $scope.listSelectedRows.length; i++) {
															pekerjaanNotif = $scope.listSelectedRows[i].TaskName + ", " + pekerjaanNotif;
														}
													}
													messagetemp = "( " + $scope.mBilling.PoliceNumber + " / " + pekerjaanNotif + ")";

													DataNotif.Message = messagetemp;
													//1028 = SAGR
													//1029 = SABP

													var SAId = 0
													if ($scope.mBilling.TypeJob == "GR") {
														SAId = "1028";
													} else {
														SAId = "1029";
													}
													BillingFactory.sendNotifRole(DataNotif, SAId, 26).then(
														function(res) {

														},
														function(err) {
															//console.log("err=>", err);
															$(".ladda-button").removeAttr('disabled', '');
														});

													$scope.getData(); //0149-SIT-033- DeliveryBilling
												},
												function(err) {
													console.log("err=>", err);
													$(".ladda-button").removeAttr('disabled', '');
												}
											);
										}										
									}
								}else if($scope.mBilling.Nationality.Id == 1){
									//WNA -> Bebas boleh ga isi?
									$scope.mBilling.TIN = "00.000.000.0-000.000";
									$scope.mBilling.KTPKITAS = $scope.mBilling.passport;

									console.log('$scope.mBilling.passport : ', $scope.mBilling.passport);
									if($scope.mBilling.passport == null || $scope.mBilling.passport == "" || $scope.mBilling.passport == undefined || $scope.mBilling.passport == "0000000000000000"){
										$(".ladda-button").removeAttr('disabled', '');
                                        bsAlert.alert({
											title: "Data No. Passport Pembayaran diisi kosong, Apakah Anda Akan Melanjutkan Transaksi?",
											text: "",
											type: "question",
											showCancelButton: true
										},
										function(){
											//save
											BillingFactory.create($scope.mBilling, DateNow, $scope.listSelectedRows, $scope.listSelectedRowsParts, $scope.listSelectedRowsOPL, $scope.TotalFinal, $scope.finalAlldiscount, $scope.tax, $scope.TotalAfterDiscountFinal, $scope.DP, $scope.listSelectedRowsOpb, $scope.dataByJobId, PPNPerc)
												.then(function(resBilling) {

													// debugger;
													$scope.resBillingMSG = resBilling.data.ResponseMessage;
													var responseBill = angular.copy(resBilling.data);

													// Jika petugas billingnya memilih Charge To nya PKS
													if (responseBill.ResponseCode == 44) {
														bsNotify.show({
															size: 'big',
															type: 'danger',
															title: "Data kendaraan ini tidak terdaftar sebagai member PKS.",
															// content: error.join('<br>'),
															// number: error.length
														});
														return -1;
													}

                                                    if (responseBill.ResponseCode == 67 || responseBill.ResponseCode == 99) {
                                                        bsNotify.show({
                                                            size: 'big',
                                                            type: 'danger',
                                                            title: "Tidak bisa Billing karena ada perubahan status. Silahkan Refresh",
                                                            // content: error.join('<br>'),
                                                            // number: error.length
                                                        });
                                                        return -1;
                                                    }

													if ($scope.dataCRM[0].ToyotaId == null) {
														BillingFactory.getDataCRM($scope.dataCRM[0].VehicleId).then(function(resx) {
															// $scope.DataPemeriksaanAwal = res.data.Result;
															var dataCRM = resx.data.Result[0];
															console.log("data CRM", dataCRM);

															// debugger;
															if (dataCRM.ToyotaId != null) {
																WO.CustomerInsertTOCRMTAM(dataCRM.CustomerOwnerId).then(function(resu) {
																	console.log('resu >>', resu.data);
																	BillingFactory.InserToVehicTR(dataCRM.VIN, dataCRM.ToyotaId).then(function(resux) {
																		console.log('resux >>', resux.data);
																	});
																});
															};

															// console.log("First Check", $scope.DataPemeriksaanAwal);
															// $scope.loading=false;
														});
													} else { //Insert ke CCustomer_Vehicle_TR di CRM_TAM.
														BillingFactory.InserToVehicTR($scope.dataCRM[0].VIN, $scope.dataCRM[0].ToyotaId).then(function(resux) {
															console.log('resux >>', resux.data);
														});
													};

													BillingFactory.getDataEmployeeSa($scope.mBilling.JobId)
														.then(
															function(res) {
																$scope.mBilling.dataPayment = "";
																$scope.mBilling.BillTo = "";
																$scope.mBilling.BillAddress = "";
																$scope.mBilling.TaxCode = 0;
																$scope.mBilling.TIN = "";
																$scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
																$scope.mBilling.TaxName = "";
																$scope.mBilling.TaxAddress = "";
																$scope.listSelectedRows = [];
																console.log("$scope.listSelectedRows", $scope.listSelectedRows);
																// $scope.listSelectedRows.splice(0);
																$scope.listSelectedRowsParts = [];
																console.log("$scope.listSelectedRowsParts", $scope.listSelectedRowsParts);
																$scope.listSelectedRowsOPL = [];
																$scope.listSelectedRowsOpb = [];

																// $scope.formApi.resetForm(true);

																$scope.gridWork = [];
																$scope.partsData = [];
																$scope.oplData = [];
																// $scope.DataPemeriksaanAwal = res.data.Result;
																$scope.dataByJobId = res.data.Result;

																for (i = 0; i < $scope.dataByJobId.length; i++) {
																	// for(z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++){
																	//     TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
																	//     $scope.subTotalOpl = TotalOpl;
																	// }

																	for (z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++) {

																		// TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
																		// $scope.subTotalOpl = TotalOpl;

																		if ($scope.dataByJobId[i].JobTaskOpl[z].IsBilling < 1 && $scope.dataByJobId[i].JobTaskOpl[z].isDeleted != 1) {
																			$scope.oplData.push($scope.dataByJobId[i].JobTaskOpl[z]);
																		} else {
																			console.log("Masuk else OplData");
																		}
																	}

																	for (j = 0; j < $scope.dataByJobId[i].JobTask.length; j++) {
																		if ($scope.dataByJobId[i].JobTask[j].IsBilling < 1) {
																			$scope.gridWork.push($scope.dataByJobId[i].JobTask[j]);
																		} else {
																			console.log("masuk else JobTask sudah Billing");
																		}

																		// $scope.subTotalLabor += $scope.dataByJobId[i].JobTask[j].Fare;

																		for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
																			if ($scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0){
																				if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
																					if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB != 1) {
																						if ($scope.dataByJobId[i].JobTask[j].JobParts[a].Price == null || $scope.dataByJobId[i].JobTask[j].JobParts[a].Price == 0) {
																							$scope.dataByJobId[i].JobTask[j].JobParts[a].Price = $scope.dataByJobId[i].JobTask[j].JobParts[a].Part.RetailPrice;
																						}
																						$scope.partsData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
																					} else {
																						console.log("ada data yang opb");
																					}
																				} else {
																					console.log("mausk else Job Parts Sudah Ada Billing");
																				}
																				// TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
																				// $scope.subTotalParts = TotalParts;
																			}
																			
																		}

																		for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
																			if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
																				if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB == 1 &&
																					$scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0) {
																					$scope.opbData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
																				} else {
																					console.log("tidak ada data Opb");
																				}
																			} else {
																				console.log("mausk else Job Parts Sudah Ada Billing");
																			}
																			// TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
																			// $scope.subTotalParts = TotalParts;
																		}
																	}
																}
																$scope.show_modalDtl = { show: false };
																$scope.modal_modelDtl = [];
																// $scope.cetakBilingafterSave($scope.dataByJobId,$scope.TotalFinal,$scope.resBillingMSG);
																$scope.formApi.setMode('grid');

																// $scope.listSelectedRowsParts = [];
																$scope.subTotalLabor = 0;
																$scope.discountPercentFinalTask = 0;
																$scope.discountFinalTask = 0;
																$scope.subTotalOpl = 0;
																$scope.discountOPl = 0;
																$scope.subTotalParts = 0;
																$scope.discountPercentFinalParts = 0;
																$scope.discountFinalParts = 0;
																$scope.subTotalOpb = 0;
																$scope.discountFinalPartsOpb = 0;
																$scope.discountPercentFinalOpb = 0;
																$scope.discountPercentFinalOpl = 0;
																$scope.discountFinalOpl = 0;
																$scope.TotalFinal = 0;
																$scope.TotalAfterDiscountFinal = 0;
																$scope.Materai = 0;
																$scope.tax = 0;
																$scope.DP = 0;
																$scope.DPPaid = 0;

																// Aftifkan Simpan button
																setTimeout(function() { $(".ladda-button").removeAttr('disabled', '') }, 2000);

																$scope.formApi.setMode('grid');
															},
															function(err) {
																console.log("err=>", err);
																$(".ladda-button").removeAttr('disabled', '');
															}
														);

													console.log("save");
													bsNotify.show({
														size: 'small',
														type: 'success',
														title: "Data Berhasil Disimpan",
														// content: error.join('<br>'),
														// number: error.length
													});
													$scope.getData();
													console.log("$scope.showw", $scope.showw, $scope.filter);

													// console.log("First Check", $scope.DataPemeriksaanAwal);
													// $scope.loading=false;

													//kirim notifikasi
													var DataNotif = {};
													var messagetemp = "";
													var pekerjaanNotif = "";
													console.log("data pekerjaan", $scope.listSelectedRows);
													if ($scope.listSelectedRows != undefined) {
														for (var i = 0; i < $scope.listSelectedRows.length; i++) {
															pekerjaanNotif = $scope.listSelectedRows[i].TaskName + ", " + pekerjaanNotif;
														}
													}
													messagetemp = "( " + $scope.mBilling.PoliceNumber + " / " + pekerjaanNotif + ")";

													DataNotif.Message = messagetemp;
													//1028 = SAGR
													//1029 = SABP

													var SAId = 0
													if ($scope.mBilling.TypeJob == "GR") {
														SAId = "1028";
													} else {
														SAId = "1029";
													}
													BillingFactory.sendNotifRole(DataNotif, SAId, 26).then(
														function(res) {

														},
														function(err) {
															//console.log("err=>", err);
															$(".ladda-button").removeAttr('disabled', '');
														});

													$scope.getData(); //0149-SIT-033- DeliveryBilling
												},
												function(err) {
													console.log("err=>", err);
													$(".ladda-button").removeAttr('disabled', '');
												}
											);
										},
										function(){
											//not save
											$scope.countCreateBilling = 0;
											return false;
										})
										//$scope.countCreateBilling = 0;
										//return false;
									}else{
										BillingFactory.create($scope.mBilling, DateNow, $scope.listSelectedRows, $scope.listSelectedRowsParts, $scope.listSelectedRowsOPL, $scope.TotalFinal, $scope.finalAlldiscount, $scope.tax, $scope.TotalAfterDiscountFinal, $scope.DP, $scope.listSelectedRowsOpb, $scope.dataByJobId, PPNPerc)
											.then(function(resBilling) {

												// debugger;
												$scope.resBillingMSG = resBilling.data.ResponseMessage;
												var responseBill = angular.copy(resBilling.data);

												// Jika petugas billingnya memilih Charge To nya PKS
												if (responseBill.ResponseCode == 44) {
													bsNotify.show({
														size: 'big',
														type: 'danger',
														title: "Data kendaraan ini tidak terdaftar sebagai member PKS.",
														// content: error.join('<br>'),
														// number: error.length
													});
													return -1;
												}

                                                if (responseBill.ResponseCode == 67 || responseBill.ResponseCode == 99) {
                                                    bsNotify.show({
                                                        size: 'big',
                                                        type: 'danger',
                                                        title: "Tidak bisa Billing karena ada perubahan status. Silahkan Refresh",
                                                        // content: error.join('<br>'),
                                                        // number: error.length
                                                    });
                                                    return -1;
                                                }

												if ($scope.dataCRM[0].ToyotaId == null) {
													BillingFactory.getDataCRM($scope.dataCRM[0].VehicleId).then(function(resx) {
														// $scope.DataPemeriksaanAwal = res.data.Result;
														var dataCRM = resx.data.Result[0];
														console.log("data CRM", dataCRM);

														// debugger;
														if (dataCRM.ToyotaId != null) {
															WO.CustomerInsertTOCRMTAM(dataCRM.CustomerOwnerId).then(function(resu) {
																console.log('resu >>', resu.data);
																BillingFactory.InserToVehicTR(dataCRM.VIN, dataCRM.ToyotaId).then(function(resux) {
																	console.log('resux >>', resux.data);
																});
															});
														};

														// console.log("First Check", $scope.DataPemeriksaanAwal);
														// $scope.loading=false;
													});
												} else { //Insert ke CCustomer_Vehicle_TR di CRM_TAM.
													BillingFactory.InserToVehicTR($scope.dataCRM[0].VIN, $scope.dataCRM[0].ToyotaId).then(function(resux) {
														console.log('resux >>', resux.data);
													});
												};

												BillingFactory.getDataEmployeeSa($scope.mBilling.JobId)
													.then(
														function(res) {
															$scope.mBilling.dataPayment = "";
															$scope.mBilling.BillTo = "";
															$scope.mBilling.BillAddress = "";
															$scope.mBilling.TaxCode = 0;
															$scope.mBilling.TIN = "";
															$scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
															$scope.mBilling.TaxName = "";
															$scope.mBilling.TaxAddress = "";
															$scope.listSelectedRows = [];
															console.log("$scope.listSelectedRows", $scope.listSelectedRows);
															// $scope.listSelectedRows.splice(0);
															$scope.listSelectedRowsParts = [];
															console.log("$scope.listSelectedRowsParts", $scope.listSelectedRowsParts);
															$scope.listSelectedRowsOPL = [];
															$scope.listSelectedRowsOpb = [];

															// $scope.formApi.resetForm(true);

															$scope.gridWork = [];
															$scope.partsData = [];
															$scope.oplData = [];
															// $scope.DataPemeriksaanAwal = res.data.Result;
															$scope.dataByJobId = res.data.Result;

															for (i = 0; i < $scope.dataByJobId.length; i++) {
																// for(z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++){
																//     TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
																//     $scope.subTotalOpl = TotalOpl;
																// }

																for (z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++) {

																	// TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
																	// $scope.subTotalOpl = TotalOpl;

																	if ($scope.dataByJobId[i].JobTaskOpl[z].IsBilling < 1 && $scope.dataByJobId[i].JobTaskOpl[z].isDeleted != 1) {
																		$scope.oplData.push($scope.dataByJobId[i].JobTaskOpl[z]);
																	} else {
																		console.log("Masuk else OplData");
																	}
																}

																for (j = 0; j < $scope.dataByJobId[i].JobTask.length; j++) {
																	if ($scope.dataByJobId[i].JobTask[j].IsBilling < 1) {
																		$scope.gridWork.push($scope.dataByJobId[i].JobTask[j]);
																	} else {
																		console.log("masuk else JobTask sudah Billing");
																	}

																	// $scope.subTotalLabor += $scope.dataByJobId[i].JobTask[j].Fare;

																	for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
																		if ($scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0){
																			if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
																				if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB != 1) {
																					if ($scope.dataByJobId[i].JobTask[j].JobParts[a].Price == null || $scope.dataByJobId[i].JobTask[j].JobParts[a].Price == 0) {
																						$scope.dataByJobId[i].JobTask[j].JobParts[a].Price = $scope.dataByJobId[i].JobTask[j].JobParts[a].Part.RetailPrice;
																					}
																					$scope.partsData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
																				} else {
																					console.log("ada data yang opb");
																				}
																			} else {
																				console.log("mausk else Job Parts Sudah Ada Billing");
																			}
																			// TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
																			// $scope.subTotalParts = TotalParts;
																		}
																		
																	}

																	for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
																		if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
																			if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB == 1 &&
																				$scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0) {
																				$scope.opbData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
																			} else {
																				console.log("tidak ada data Opb");
																			}
																		} else {
																			console.log("mausk else Job Parts Sudah Ada Billing");
																		}
																		// TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
																		// $scope.subTotalParts = TotalParts;
																	}
																}
															}
															$scope.show_modalDtl = { show: false };
															$scope.modal_modelDtl = [];
															// $scope.cetakBilingafterSave($scope.dataByJobId,$scope.TotalFinal,$scope.resBillingMSG);
															$scope.formApi.setMode('grid');

															// $scope.listSelectedRowsParts = [];
															$scope.subTotalLabor = 0;
															$scope.discountPercentFinalTask = 0;
															$scope.discountFinalTask = 0;
															$scope.subTotalOpl = 0;
															$scope.discountOPl = 0;
															$scope.subTotalParts = 0;
															$scope.discountPercentFinalParts = 0;
															$scope.discountFinalParts = 0;
															$scope.subTotalOpb = 0;
															$scope.discountFinalPartsOpb = 0;
															$scope.discountPercentFinalOpb = 0;
															$scope.discountPercentFinalOpl = 0;
															$scope.discountFinalOpl = 0;
															$scope.TotalFinal = 0;
															$scope.TotalAfterDiscountFinal = 0;
															$scope.Materai = 0;
															$scope.tax = 0;
															$scope.DP = 0;
															$scope.DPPaid = 0;

															// Aftifkan Simpan button
															setTimeout(function() { $(".ladda-button").removeAttr('disabled', '') }, 2000);

															$scope.formApi.setMode('grid');
														},
														function(err) {
															console.log("err=>", err);
															$(".ladda-button").removeAttr('disabled', '');
														}
													);

												console.log("save");
												bsNotify.show({
													size: 'small',
													type: 'success',
													title: "Data Berhasil Disimpan",
													// content: error.join('<br>'),
													// number: error.length
												});
												$scope.getData();
												console.log("$scope.showw", $scope.showw, $scope.filter);

												// console.log("First Check", $scope.DataPemeriksaanAwal);
												// $scope.loading=false;

												//kirim notifikasi
												var DataNotif = {};
												var messagetemp = "";
												var pekerjaanNotif = "";
												console.log("data pekerjaan", $scope.listSelectedRows);
												if ($scope.listSelectedRows != undefined) {
													for (var i = 0; i < $scope.listSelectedRows.length; i++) {
														pekerjaanNotif = $scope.listSelectedRows[i].TaskName + ", " + pekerjaanNotif;
													}
												}
												messagetemp = "( " + $scope.mBilling.PoliceNumber + " / " + pekerjaanNotif + ")";

												DataNotif.Message = messagetemp;
												//1028 = SAGR
												//1029 = SABP

												var SAId = 0
												if ($scope.mBilling.TypeJob == "GR") {
													SAId = "1028";
												} else {
													SAId = "1029";
												}
												BillingFactory.sendNotifRole(DataNotif, SAId, 26).then(
													function(res) {

													},
													function(err) {
														//console.log("err=>", err);
														$(".ladda-button").removeAttr('disabled', '');
													});

												$scope.getData(); //0149-SIT-033- DeliveryBilling
											},
											function(err) {
												console.log("err=>", err);
												$(".ladda-button").removeAttr('disabled', '');
											}
										);	
									}
								}

							}else{
								//if($scope.mBilling.TIN.length == 0 || $scope.mBilling.TIN == "00.000.000.0-000.000" ){
								console.log('$scope.mBilling.TIN at B : ', $scope.mBilling.TIN);
								if($scope.mBilling.TIN == null || $scope.mBilling.TIN == undefined || $scope.mBilling.TIN == "" || $scope.mBilling.TIN == "00.000.000.0-000.000"){
                                    $(".ladda-button").removeAttr('disabled', '');
                                    bsAlert.alert({
										/*title: "Data NPWP tidak boleh 00.000.000.0-000.000 / kosong",
										text: "",
										type: "warning"*/
										title: "Data No. NPWP Pembayaran diisi 00.000.000.0-000.000 / kosong, Apakah Anda Akan Melanjutkan Transaksi?",
										text: "",
										type: "question",
										showCancelButton: true
									},
									function(){
										console.log("Simpan NPWP kosong B");
										
										BillingFactory.create($scope.mBilling, DateNow, $scope.listSelectedRows, $scope.listSelectedRowsParts, $scope.listSelectedRowsOPL, $scope.TotalFinal, $scope.finalAlldiscount, $scope.tax, $scope.TotalAfterDiscountFinal, $scope.DP, $scope.listSelectedRowsOpb, $scope.dataByJobId, PPNPerc)
											.then(function(resBilling) {

												// debugger;
												$scope.resBillingMSG = resBilling.data.ResponseMessage;
												var responseBill = angular.copy(resBilling.data);

												// Jika petugas billingnya memilih Charge To nya PKS
												if (responseBill.ResponseCode == 44) {
													bsNotify.show({
														size: 'big',
														type: 'danger',
														title: "Data kendaraan ini tidak terdaftar sebagai member PKS.",
														// content: error.join('<br>'),
														// number: error.length
													});
													return -1;
												}

                                                if (responseBill.ResponseCode == 67 || responseBill.ResponseCode == 99) {
                                                    bsNotify.show({
                                                        size: 'big',
                                                        type: 'danger',
                                                        title: "Tidak bisa Billing karena ada perubahan status. Silahkan Refresh",
                                                        // content: error.join('<br>'),
                                                        // number: error.length
                                                    });
                                                    return -1;
                                                }

												if ($scope.dataCRM[0].ToyotaId == null) {
													BillingFactory.getDataCRM($scope.dataCRM[0].VehicleId).then(function(resx) {
														// $scope.DataPemeriksaanAwal = res.data.Result;
														var dataCRM = resx.data.Result[0];
														console.log("data CRM", dataCRM);

														// debugger;
														if (dataCRM.ToyotaId != null) {
															WO.CustomerInsertTOCRMTAM(dataCRM.CustomerOwnerId).then(function(resu) {
																console.log('resu >>', resu.data);
																BillingFactory.InserToVehicTR(dataCRM.VIN, dataCRM.ToyotaId).then(function(resux) {
																	console.log('resux >>', resux.data);
																});
															});
														};

														// console.log("First Check", $scope.DataPemeriksaanAwal);
														// $scope.loading=false;
													});
												} else { //Insert ke CCustomer_Vehicle_TR di CRM_TAM.
													BillingFactory.InserToVehicTR($scope.dataCRM[0].VIN, $scope.dataCRM[0].ToyotaId).then(function(resux) {
														console.log('resux >>', resux.data);
													});
												};

												BillingFactory.getDataEmployeeSa($scope.mBilling.JobId)
													.then(
														function(res) {
															$scope.mBilling.dataPayment = "";
															$scope.mBilling.BillTo = "";
															$scope.mBilling.BillAddress = "";
															$scope.mBilling.TaxCode = 0;
															$scope.mBilling.TIN = "";
															$scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
															$scope.mBilling.TaxName = "";
															$scope.mBilling.TaxAddress = "";
															$scope.listSelectedRows = [];
															console.log("$scope.listSelectedRows", $scope.listSelectedRows);
															// $scope.listSelectedRows.splice(0);
															$scope.listSelectedRowsParts = [];
															console.log("$scope.listSelectedRowsParts", $scope.listSelectedRowsParts);
															$scope.listSelectedRowsOPL = [];
															$scope.listSelectedRowsOpb = [];

															// $scope.formApi.resetForm(true);

															$scope.gridWork = [];
															$scope.partsData = [];
															$scope.oplData = [];
															// $scope.DataPemeriksaanAwal = res.data.Result;
															$scope.dataByJobId = res.data.Result;

															for (i = 0; i < $scope.dataByJobId.length; i++) {
																// for(z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++){
																//     TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
																//     $scope.subTotalOpl = TotalOpl;
																// }

																for (z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++) {

																	// TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
																	// $scope.subTotalOpl = TotalOpl;

																	if ($scope.dataByJobId[i].JobTaskOpl[z].IsBilling < 1 && $scope.dataByJobId[i].JobTaskOpl[z].isDeleted != 1) {
																		$scope.oplData.push($scope.dataByJobId[i].JobTaskOpl[z]);
																	} else {
																		console.log("Masuk else OplData");
																	}
																}

																for (j = 0; j < $scope.dataByJobId[i].JobTask.length; j++) {
																	if ($scope.dataByJobId[i].JobTask[j].IsBilling < 1) {
																		$scope.gridWork.push($scope.dataByJobId[i].JobTask[j]);
																	} else {
																		console.log("masuk else JobTask sudah Billing");
																	}

																	// $scope.subTotalLabor += $scope.dataByJobId[i].JobTask[j].Fare;

																	for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
																		if ($scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0){
																			if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
																				if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB != 1) {
																					if ($scope.dataByJobId[i].JobTask[j].JobParts[a].Price == null || $scope.dataByJobId[i].JobTask[j].JobParts[a].Price == 0) {
																						$scope.dataByJobId[i].JobTask[j].JobParts[a].Price = $scope.dataByJobId[i].JobTask[j].JobParts[a].Part.RetailPrice;
																					}
																					$scope.partsData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
																				} else {
																					console.log("ada data yang opb");
																				}
																			} else {
																				console.log("mausk else Job Parts Sudah Ada Billing");
																			}
																			// TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
																			// $scope.subTotalParts = TotalParts;
																		}
																		
																	}

																	for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
																		if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
																			if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB == 1 &&
																				$scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0) {
																				$scope.opbData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
																			} else {
																				console.log("tidak ada data Opb");
																			}
																		} else {
																			console.log("mausk else Job Parts Sudah Ada Billing");
																		}
																		// TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
																		// $scope.subTotalParts = TotalParts;
																	}
																}
															}
															$scope.show_modalDtl = { show: false };
															$scope.modal_modelDtl = [];
															// $scope.cetakBilingafterSave($scope.dataByJobId,$scope.TotalFinal,$scope.resBillingMSG);
															$scope.formApi.setMode('grid');

															// $scope.listSelectedRowsParts = [];
															$scope.subTotalLabor = 0;
															$scope.discountPercentFinalTask = 0;
															$scope.discountFinalTask = 0;
															$scope.subTotalOpl = 0;
															$scope.discountOPl = 0;
															$scope.subTotalParts = 0;
															$scope.discountPercentFinalParts = 0;
															$scope.discountFinalParts = 0;
															$scope.subTotalOpb = 0;
															$scope.discountFinalPartsOpb = 0;
															$scope.discountPercentFinalOpb = 0;
															$scope.discountPercentFinalOpl = 0;
															$scope.discountFinalOpl = 0;
															$scope.TotalFinal = 0;
															$scope.TotalAfterDiscountFinal = 0;
															$scope.Materai = 0;
															$scope.tax = 0;
															$scope.DP = 0;
															$scope.DPPaid = 0;

															// Aftifkan Simpan button
															setTimeout(function() { $(".ladda-button").removeAttr('disabled', '') }, 2000);

															$scope.formApi.setMode('grid');
														},
														function(err) {
															console.log("err=>", err);
															$(".ladda-button").removeAttr('disabled', '');
														}
													);

												console.log("save");
												bsNotify.show({
													size: 'small',
													type: 'success',
													title: "Data Berhasil Disimpan",
													// content: error.join('<br>'),
													// number: error.length
												});
												$scope.getData();
												console.log("$scope.showw", $scope.showw, $scope.filter);

												// console.log("First Check", $scope.DataPemeriksaanAwal);
												// $scope.loading=false;

												//kirim notifikasi
												var DataNotif = {};
												var messagetemp = "";
												var pekerjaanNotif = "";
												console.log("data pekerjaan", $scope.listSelectedRows);
												if ($scope.listSelectedRows != undefined) {
													for (var i = 0; i < $scope.listSelectedRows.length; i++) {
														pekerjaanNotif = $scope.listSelectedRows[i].TaskName + ", " + pekerjaanNotif;
													}
												}
												messagetemp = "( " + $scope.mBilling.PoliceNumber + " / " + pekerjaanNotif + ")";

												DataNotif.Message = messagetemp;
												//1028 = SAGR
												//1029 = SABP

												var SAId = 0
												if ($scope.mBilling.TypeJob == "GR") {
													SAId = "1028";
												} else {
													SAId = "1029";
												}
												BillingFactory.sendNotifRole(DataNotif, SAId, 26).then(
													function(res) {

													},
													function(err) {
														//console.log("err=>", err);
														$(".ladda-button").removeAttr('disabled', '');
													});

												$scope.getData(); //0149-SIT-033- DeliveryBilling
											},
											function(err) {
												console.log("err=>", err);
												$(".ladda-button").removeAttr('disabled', '');
											}
										);

										//$scope.countCreateBilling = 0;
										//return false;

										// Aftifkan Simpan button
										// setTimeout(function() { $(".ladda-button").removeAttr('disabled', '') }, 200);

										// $(".ladda-button").attr('disabled', 'disabled');
									},
									function(){
										console.log("Tidak Simpan");
										$scope.countCreateBilling = 0;
										return false;
									})
                                    //$scope.countCreateBilling = 0;
                                    //return false;
                                }else{
                                    console.log('masuk2');
                                    $scope.mBilling.isWNA = null;

                                    if($scope.mBilling.IsWNARaw == null){
                                        $scope.mBilling.KTPKITAS = $scope.mBilling.KTPRaw;
                                    }else if($scope.mBilling.IsWNARaw == 0){
                                        if($scope.mBilling.KTPRaw == null || $scope.mBilling.KTPRaw == undefined){
                                            $scope.mBilling.KTPKITAS = "0000000000000000";
                                        }else{
                                            $scope.mBilling.KTPKITAS = $scope.mBilling.KTPRaw;
                                        }
                                    }else if($scope.mBilling.IsWNARaw == 1){
                                        if($scope.mBilling.passportRaw == null || $scope.mBilling.passportRaw == undefined){
                                            $scope.mBilling.KTPKITAS = "0000000000000000";
                                        }else{
                                            $scope.mBilling.KTPKITAS = $scope.mBilling.passportRaw;
                                        }
                                    }
                                    //Save
                                    BillingFactory.create($scope.mBilling, DateNow, $scope.listSelectedRows, $scope.listSelectedRowsParts, $scope.listSelectedRowsOPL, $scope.TotalFinal, $scope.finalAlldiscount, $scope.tax, $scope.TotalAfterDiscountFinal, $scope.DP, $scope.listSelectedRowsOpb, $scope.dataByJobId, PPNPerc)
                                        .then(function(resBilling) {

                                            // debugger;
                                            $scope.resBillingMSG = resBilling.data.ResponseMessage;
                                            var responseBill = angular.copy(resBilling.data);

                                            // Jika petugas billingnya memilih Charge To nya PKS
                                            if (responseBill.ResponseCode == 44) {
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "Data kendaraan ini tidak terdaftar sebagai member PKS.",
                                                    // content: error.join('<br>'),
                                                    // number: error.length
                                                });
                                                return -1;
                                            }

                                            if (responseBill.ResponseCode == 67 || responseBill.ResponseCode == 99) {
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "Tidak bisa Billing karena ada perubahan status. Silahkan Refresh",
                                                    // content: error.join('<br>'),
                                                    // number: error.length
                                                });
                                                return -1;
                                            }

                                            if ($scope.dataCRM[0].ToyotaId == null) {
                                                BillingFactory.getDataCRM($scope.dataCRM[0].VehicleId).then(function(resx) {
                                                    // $scope.DataPemeriksaanAwal = res.data.Result;
                                                    var dataCRM = resx.data.Result[0];
                                                    console.log("data CRM", dataCRM);

                                                    // debugger;
                                                    if (dataCRM.ToyotaId != null) {
                                                        WO.CustomerInsertTOCRMTAM(dataCRM.CustomerOwnerId).then(function(resu) {
                                                            console.log('resu >>', resu.data);
                                                            BillingFactory.InserToVehicTR(dataCRM.VIN, dataCRM.ToyotaId).then(function(resux) {
                                                                console.log('resux >>', resux.data);
                                                            });
                                                        });
                                                    };

                                                    // console.log("First Check", $scope.DataPemeriksaanAwal);
                                                    // $scope.loading=false;
                                                });
                                            } else { //Insert ke CCustomer_Vehicle_TR di CRM_TAM.
                                                BillingFactory.InserToVehicTR($scope.dataCRM[0].VIN, $scope.dataCRM[0].ToyotaId).then(function(resux) {
                                                    console.log('resux >>', resux.data);
                                                });
                                            };

                                            BillingFactory.getDataEmployeeSa($scope.mBilling.JobId)
                                                .then(
                                                    function(res) {
                                                        $scope.mBilling.dataPayment = "";
                                                        $scope.mBilling.BillTo = "";
                                                        $scope.mBilling.BillAddress = "";
                                                        $scope.mBilling.TaxCode = 0;
                                                        $scope.mBilling.TIN = "";
                                                        $scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
                                                        $scope.mBilling.TaxName = "";
                                                        $scope.mBilling.TaxAddress = "";
                                                        $scope.listSelectedRows = [];
                                                        console.log("$scope.listSelectedRows", $scope.listSelectedRows);
                                                        // $scope.listSelectedRows.splice(0);
                                                        $scope.listSelectedRowsParts = [];
                                                        console.log("$scope.listSelectedRowsParts", $scope.listSelectedRowsParts);
                                                        $scope.listSelectedRowsOPL = [];
                                                        $scope.listSelectedRowsOpb = [];

                                                        // $scope.formApi.resetForm(true);

                                                        $scope.gridWork = [];
                                                        $scope.partsData = [];
                                                        $scope.oplData = [];
                                                        // $scope.DataPemeriksaanAwal = res.data.Result;
                                                        $scope.dataByJobId = res.data.Result;

                                                        for (i = 0; i < $scope.dataByJobId.length; i++) {
                                                            // for(z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++){
                                                            //     TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
                                                            //     $scope.subTotalOpl = TotalOpl;
                                                            // }

                                                            for (z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++) {

                                                                // TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
                                                                // $scope.subTotalOpl = TotalOpl;

                                                                if ($scope.dataByJobId[i].JobTaskOpl[z].IsBilling < 1 && $scope.dataByJobId[i].JobTaskOpl[z].isDeleted != 1) {
                                                                    $scope.oplData.push($scope.dataByJobId[i].JobTaskOpl[z]);
                                                                } else {
                                                                    console.log("Masuk else OplData");
                                                                }
                                                            }

                                                            for (j = 0; j < $scope.dataByJobId[i].JobTask.length; j++) {
                                                                if ($scope.dataByJobId[i].JobTask[j].IsBilling < 1) {
                                                                    $scope.gridWork.push($scope.dataByJobId[i].JobTask[j]);
                                                                } else {
                                                                    console.log("masuk else JobTask sudah Billing");
                                                                }

                                                                // $scope.subTotalLabor += $scope.dataByJobId[i].JobTask[j].Fare;

                                                                for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
                                                                    if ($scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0){
                                                                        if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
                                                                            if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB != 1) {
                                                                                if ($scope.dataByJobId[i].JobTask[j].JobParts[a].Price == null || $scope.dataByJobId[i].JobTask[j].JobParts[a].Price == 0) {
                                                                                    $scope.dataByJobId[i].JobTask[j].JobParts[a].Price = $scope.dataByJobId[i].JobTask[j].JobParts[a].Part.RetailPrice;
                                                                                }
                                                                                $scope.partsData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
                                                                            } else {
                                                                                console.log("ada data yang opb");
                                                                            }
                                                                        } else {
                                                                            console.log("mausk else Job Parts Sudah Ada Billing");
                                                                        }
                                                                        // TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
                                                                        // $scope.subTotalParts = TotalParts;
                                                                    }
                                                                    
                                                                }

                                                                for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
                                                                    if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
                                                                        if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB == 1 &&
                                                                            $scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0) {
                                                                            $scope.opbData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
                                                                        } else {
                                                                            console.log("tidak ada data Opb");
                                                                        }
                                                                    } else {
                                                                        console.log("mausk else Job Parts Sudah Ada Billing");
                                                                    }
                                                                    // TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
                                                                    // $scope.subTotalParts = TotalParts;
                                                                }
                                                            }
                                                        }
                                                        $scope.show_modalDtl = { show: false };
                                                        $scope.modal_modelDtl = [];
                                                        // $scope.cetakBilingafterSave($scope.dataByJobId,$scope.TotalFinal,$scope.resBillingMSG);
                                                        $scope.formApi.setMode('grid');

                                                        // $scope.listSelectedRowsParts = [];
                                                        $scope.subTotalLabor = 0;
                                                        $scope.discountPercentFinalTask = 0;
                                                        $scope.discountFinalTask = 0;
                                                        $scope.subTotalOpl = 0;
                                                        $scope.discountOPl = 0;
                                                        $scope.subTotalParts = 0;
                                                        $scope.discountPercentFinalParts = 0;
                                                        $scope.discountFinalParts = 0;
                                                        $scope.subTotalOpb = 0;
                                                        $scope.discountFinalPartsOpb = 0;
                                                        $scope.discountPercentFinalOpb = 0;
                                                        $scope.discountPercentFinalOpl = 0;
                                                        $scope.discountFinalOpl = 0;
                                                        $scope.TotalFinal = 0;
                                                        $scope.TotalAfterDiscountFinal = 0;
                                                        $scope.Materai = 0;
                                                        $scope.tax = 0;
                                                        $scope.DP = 0;
                                                        $scope.DPPaid = 0;

                                                        // Aftifkan Simpan button
                                                        setTimeout(function() { $(".ladda-button").removeAttr('disabled', '') }, 2000);

                                                        $scope.formApi.setMode('grid');
                                                    },
                                                    function(err) {
                                                        console.log("err=>", err);
                                                        $(".ladda-button").removeAttr('disabled', '');
                                                    }
                                                );

                                            console.log("save");
                                            bsNotify.show({
                                                size: 'small',
                                                type: 'success',
                                                title: "Data Berhasil Disimpan",
                                                // content: error.join('<br>'),
                                                // number: error.length
                                            });
                                            $scope.getData();
                                            console.log("$scope.showw", $scope.showw, $scope.filter);

                                            // console.log("First Check", $scope.DataPemeriksaanAwal);
                                            // $scope.loading=false;

                                            //kirim notifikasi
                                            var DataNotif = {};
                                            var messagetemp = "";
                                            var pekerjaanNotif = "";
                                            console.log("data pekerjaan", $scope.listSelectedRows);
                                            if ($scope.listSelectedRows != undefined) {
                                                for (var i = 0; i < $scope.listSelectedRows.length; i++) {
                                                    pekerjaanNotif = $scope.listSelectedRows[i].TaskName + ", " + pekerjaanNotif;
                                                }
                                            }
                                            messagetemp = "( " + $scope.mBilling.PoliceNumber + " / " + pekerjaanNotif + ")";

                                            DataNotif.Message = messagetemp;
                                            //1028 = SAGR
                                            //1029 = SABP

                                            var SAId = 0
                                            if ($scope.mBilling.TypeJob == "GR") {
                                                SAId = "1028";
                                            } else {
                                                SAId = "1029";
                                            }
                                            BillingFactory.sendNotifRole(DataNotif, SAId, 26).then(
                                                function(res) {

                                                },
                                                function(err) {
                                                    //console.log("err=>", err);
                                                    $(".ladda-button").removeAttr('disabled', '');
                                                });

                                            $scope.getData(); //0149-SIT-033- DeliveryBilling
                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                            $(".ladda-button").removeAttr('disabled', '');
                                        }
                                    );
                                }
							}

							console.log("$scope.mBilling.TIN at onValidateSave: ", $scope.mBilling.TIN);
							console.log("$scope.mBilling.KTPKITAS at onValidateSave: ", $scope.mBilling.KTPKITAS);
							console.log("$scope.mBilling.isWNA at onValidateSave: ", $scope.mBilling.isWNA);
							// CR5 #27
							
                            /*BillingFactory.create($scope.mBilling, DateNow, $scope.listSelectedRows, $scope.listSelectedRowsParts, $scope.listSelectedRowsOPL, $scope.TotalFinal, $scope.finalAlldiscount, $scope.tax, $scope.TotalAfterDiscountFinal, $scope.DP, $scope.listSelectedRowsOpb, $scope.dataByJobId, PPNPerc)
                                .then(function(resBilling) {

                                        if (resBilling.data == 666){
                                            bsNotify.show({
                                                size: 'big',
                                                type: 'danger',
                                                title: "Ada Data Yang Sudah di Billing, Silahkan Refresh Data",
                                            });
                                            $scope.countCreateBilling = 0;
                                        } else {
                                            // debugger;
                                            $scope.resBillingMSG = resBilling.data.ResponseMessage;
                                            var responseBill = angular.copy(resBilling.data);

                                            // Jika petugas billingnya memilih Charge To nya PKS
                                            if (responseBill.ResponseCode == 44) {
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "Data kendaraan ini tidak terdaftar sebagai member PKS.",
                                                    // content: error.join('<br>'),
                                                    // number: error.length
                                                });
                                                return -1;
                                            }

                                            if ($scope.dataCRM[0].ToyotaId == null) {
                                                BillingFactory.getDataCRM($scope.dataCRM[0].VehicleId).then(function(resx) {
                                                    // $scope.DataPemeriksaanAwal = res.data.Result;
                                                    var dataCRM = resx.data.Result[0];
                                                    console.log("data CRM", dataCRM);

                                                    // debugger;
                                                    if (dataCRM.ToyotaId != null) {
                                                        WO.CustomerInsertTOCRMTAM(dataCRM.CustomerOwnerId).then(function(resu) {
                                                            console.log('resu >>', resu.data);
                                                            BillingFactory.InserToVehicTR(dataCRM.VIN, dataCRM.ToyotaId).then(function(resux) {
                                                                console.log('resux >>', resux.data);
                                                            });
                                                        });
                                                    };

                                                    // console.log("First Check", $scope.DataPemeriksaanAwal);
                                                    // $scope.loading=false;
                                                });
                                            } else { //Insert ke CCustomer_Vehicle_TR di CRM_TAM.
                                                BillingFactory.InserToVehicTR($scope.dataCRM[0].VIN, $scope.dataCRM[0].ToyotaId).then(function(resux) {
                                                    console.log('resux >>', resux.data);
                                                });
                                            };

                                            BillingFactory.getDataEmployeeSa($scope.mBilling.JobId)
                                                .then(
                                                    function(res) {
                                                        $scope.mBilling.dataPayment = "";
                                                        $scope.mBilling.BillTo = "";
                                                        $scope.mBilling.BillAddress = "";
                                                        $scope.mBilling.TaxCode = 0;
                                                        $scope.mBilling.TIN = "";
                                                        $scope.mBilling.TINBackup = angular.copy($scope.mBilling.TIN)
                                                        $scope.mBilling.TaxName = "";
                                                        $scope.mBilling.TaxAddress = "";
                                                        $scope.listSelectedRows = [];
                                                        console.log("$scope.listSelectedRows", $scope.listSelectedRows);
                                                        // $scope.listSelectedRows.splice(0);
                                                        $scope.listSelectedRowsParts = [];
                                                        console.log("$scope.listSelectedRowsParts", $scope.listSelectedRowsParts);
                                                        $scope.listSelectedRowsOPL = [];
                                                        $scope.listSelectedRowsOpb = [];

                                                        // $scope.formApi.resetForm(true);

                                                        $scope.gridWork = [];
                                                        $scope.partsData = [];
                                                        $scope.oplData = [];
                                                        // $scope.DataPemeriksaanAwal = res.data.Result;
                                                        $scope.dataByJobId = res.data.Result;

                                                        for (i = 0; i < $scope.dataByJobId.length; i++) {
                                                            // for(z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++){
                                                            //     TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
                                                            //     $scope.subTotalOpl = TotalOpl;
                                                            // }

                                                            for (z = 0; z < $scope.dataByJobId[i].JobTaskOpl.length; z++) {

                                                                // TotalOpl += $scope.dataByJobId[i].JobTaskOpl[z].Price;
                                                                // $scope.subTotalOpl = TotalOpl;

                                                                if ($scope.dataByJobId[i].JobTaskOpl[z].IsBilling < 1 && $scope.dataByJobId[i].JobTaskOpl[z].isDeleted != 1) {
                                                                    $scope.oplData.push($scope.dataByJobId[i].JobTaskOpl[z]);
                                                                } else {
                                                                    console.log("Masuk else OplData");
                                                                }
                                                            }

                                                            for (j = 0; j < $scope.dataByJobId[i].JobTask.length; j++) {
                                                                if ($scope.dataByJobId[i].JobTask[j].IsBilling < 1) {
                                                                    $scope.gridWork.push($scope.dataByJobId[i].JobTask[j]);
                                                                } else {
                                                                    console.log("masuk else JobTask sudah Billing");
                                                                }

                                                                // $scope.subTotalLabor += $scope.dataByJobId[i].JobTask[j].Fare;

                                                                for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
                                                                    if ($scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0){
                                                                        if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
                                                                            if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB != 1) {
                                                                                if ($scope.dataByJobId[i].JobTask[j].JobParts[a].Price == null || $scope.dataByJobId[i].JobTask[j].JobParts[a].Price == 0) {
                                                                                    $scope.dataByJobId[i].JobTask[j].JobParts[a].Price = $scope.dataByJobId[i].JobTask[j].JobParts[a].Part.RetailPrice;
                                                                                }
                                                                                $scope.partsData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
                                                                            } else {
                                                                                console.log("ada data yang opb");
                                                                            }
                                                                        } else {
                                                                            console.log("mausk else Job Parts Sudah Ada Billing");
                                                                        }
                                                                        // TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
                                                                        // $scope.subTotalParts = TotalParts;
                                                                    }
                                                                    
                                                                }

                                                                for (a = 0; a < $scope.dataByJobId[i].JobTask[j].JobParts.length; a++) {
                                                                    if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsBilling < 1) {
                                                                        if ($scope.dataByJobId[i].JobTask[j].JobParts[a].IsOPB == 1 &&
                                                                            $scope.dataByJobId[i].JobTask[j].JobParts[a].isDeleted == 0) {
                                                                            $scope.opbData.push($scope.dataByJobId[i].JobTask[j].JobParts[a]);
                                                                        } else {
                                                                            console.log("tidak ada data Opb");
                                                                        }
                                                                    } else {
                                                                        console.log("mausk else Job Parts Sudah Ada Billing");
                                                                    }
                                                                    // TotalParts += $scope.dataByJobId[i].JobTask[j].JobParts[a].Price;
                                                                    // $scope.subTotalParts = TotalParts;
                                                                }
                                                            }
                                                        }
                                                        $scope.show_modalDtl = { show: false };
                                                        $scope.modal_modelDtl = [];
                                                        // $scope.cetakBilingafterSave($scope.dataByJobId,$scope.TotalFinal,$scope.resBillingMSG);
                                                        $scope.formApi.setMode('grid');

                                                        // $scope.listSelectedRowsParts = [];
                                                        $scope.subTotalLabor = 0;
                                                        $scope.discountPercentFinalTask = 0;
                                                        $scope.discountFinalTask = 0;
                                                        $scope.subTotalOpl = 0;
                                                        $scope.discountOPl = 0;
                                                        $scope.subTotalParts = 0;
                                                        $scope.discountPercentFinalParts = 0;
                                                        $scope.discountFinalParts = 0;
                                                        $scope.subTotalOpb = 0;
                                                        $scope.discountFinalPartsOpb = 0;
                                                        $scope.discountPercentFinalOpb = 0;
                                                        $scope.discountPercentFinalOpl = 0;
                                                        $scope.discountFinalOpl = 0;
                                                        $scope.TotalFinal = 0;
                                                        $scope.TotalAfterDiscountFinal = 0;
                                                        $scope.Materai = 0;
                                                        $scope.tax = 0;
                                                        $scope.DP = 0;
                                                        $scope.DPPaid = 0;

                                                        // Aftifkan Simpan button
                                                        setTimeout(function() { $(".ladda-button").removeAttr('disabled', '') }, 2000);

                                                        $scope.formApi.setMode('grid');
                                                    },
                                                    function(err) {
                                                        console.log("err=>", err);
                                                        $(".ladda-button").removeAttr('disabled', '');
                                                    }
                                                );

                                            console.log("save");
                                            bsNotify.show({
                                                size: 'small',
                                                type: 'success',
                                                title: "Data Berhasil Disimpan",
                                                // content: error.join('<br>'),
                                                // number: error.length
                                            });
                                            $scope.getData();
                                            console.log("$scope.showw", $scope.showw, $scope.filter);

                                            // console.log("First Check", $scope.DataPemeriksaanAwal);
                                            // $scope.loading=false;

                                            //kirim notifikasi
                                            var DataNotif = {};
                                            var messagetemp = "";
                                            var pekerjaanNotif = "";
                                            console.log("data pekerjaan", $scope.listSelectedRows);
                                            if ($scope.listSelectedRows != undefined) {
                                                for (var i = 0; i < $scope.listSelectedRows.length; i++) {
                                                    pekerjaanNotif = $scope.listSelectedRows[i].TaskName + ", " + pekerjaanNotif;
                                                }
                                            }
                                            messagetemp = "( " + $scope.mBilling.PoliceNumber + " / " + pekerjaanNotif + ")";

                                            DataNotif.Message = messagetemp;
                                            //1028 = SAGR
                                            //1029 = SABP

                                            var SAId = 0
                                            if ($scope.mBilling.TypeJob == "GR") {
                                                SAId = "1028";
                                            } else {
                                                SAId = "1029";
                                            }
                                            BillingFactory.sendNotifRole(DataNotif, SAId, 26).then(
                                                function(res) {

                                                },
                                                function(err) {
                                                    //console.log("err=>", err);
                                                    $(".ladda-button").removeAttr('disabled', '');
                                                });

                                            $scope.getData(); //0149-SIT-033- DeliveryBilling

                                        }
                                        
                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                        $(".ladda-button").removeAttr('disabled', '');
                                    }
                                );
                        // }
                    // })*/
                
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Data Bertanda Bintang (*)",
                    // content: error.join('<br>'),
                    // number: error.length
                });
                $(".ladda-button").removeAttr('disabled', '');
            }
        };


        $scope.rejectCancelBillTO = function() {
            console.log("cancel");
        };
        var actionAfterBilling = {};
        if (RId == 1128 || RId == 1129) {
            actionAfterBilling = '<div class="ui-grid-cell-contents">' +
                // '<a href="#" ng-click="grid.appScope.$parent.rejectBilling(row.entity)" uib-tooltip="Tolak" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-print fa-lg" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
                // '<a href="#" ng-click="grid.appScope.$parent.approveBilling(row.entity)" uib-tooltip="Setuju" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-id-card-o fa-lg" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
                // '<a href="#" ng-click="grid.appScope.$parent.rejectBilling(row.entity)" uib-tooltip="Reject Billing" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-id-card-o fa-lg" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
                '</div>';
        } else {
            actionAfterBilling = '<div class="ui-grid-cell-contents">' +
                // '<a href="#" ng-click="grid.appScope.$parent.cetakBiling(row.entity)" uib-tooltip="Cetak Billing" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-print fa-lg" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
                // '<a href="#" ng-show="row.entity.Job.Status == 18"  tooltip-placement="bottom"><i class="" style="padding:5px 8px 8px 0px;margin-left:40px;"></i></a>' +
                '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Lihat Biling" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-list-alt fa-lg" style="padding:5px 8px 8px 0px;margin-left:30px;"></i></a>' +
                '<a href="#" ng-click="grid.appScope.$parent.cetakNota(row.entity)" uib-tooltip="Cetak Nota Pembatalan" ng-hide="row.entity.StatusBilling != 0 && row.entity.StatusBilling != 4" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-id-card-o fa-lg" style="padding:5px 8px 8px 0px;margin-left:10px;"></i></a>' +
                '<a href="#" ng-click="grid.appScope.$parent.batalBiling(row.entity)" ng-hide="row.entity.Job.Status == 18" uib-tooltip="Batal Biling" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-times-circle fa-lg" style="padding:5px 8px 8px 0px;margin-left:10px;"></i></a>' +

                // '<a href="#" ng-click="grid.appScope.$parent.rejectBilling(row.entity)" uib-tooltip="Reject Billing" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-id-card-o fa-lg" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
                '</div>';
        };


        var afterBilling = [];
        var reprintBilling = [];
        if (RId == 1128 || RId == 1129) {

            afterBilling = [
                { name: 'id', field: 'JobId', width: '4%', visible: false },
                { name: 'no wo3', displayName: "No. WO", field: 'WoNo', allowCellFocus: false },
                { name: 'no billing3', displayName: "No. Billing", field: 'BillingNo', allowCellFocus: false },
                // { name:'No Pembatalan', field:'Billing.CancelBillingNo' },
                { name: 'no polisi3', displayName: "No. Polisi", field: 'PoliceNumber', allowCellFocus: false },
                { name: 'nama3', displayName: "Nama", field: 'BillTo', allowCellFocus: false },
                { name: 'tgl wo3', displayName: "Tgl. WO", field: 'WoCreatedDate', cellFilter: dateFilter, allowCellFocus: false },
                // { name: 'model Type3', displayName: "Model", field: 'Job.Vehicle.MVehicleTypeColor.MVehicleType.VehicleModel.VehicleModelName' },
                { name: 'model Type3', displayName: "Model", field: 'ModelType', allowCellFocus: false },
                { name: 'type job3', displayName: "Kategori WO", field: 'Process.ProcessName', allowCellFocus: false },


                { name: 'pekerjaan3', displayName: "Pekerjaan", width: 130, field: 'Job.TaskName', allowCellFocus: false },
                { name: 'charge to3', displayName: "Charge To", width: 130, field: 'ChargeToNew', allowCellFocus: false },


                { name: 'type invoice3', displayName: "Type Invoice", field: 'ChargeToName', allowCellFocus: false },
                { name: 'status billing3', displayName: "Status Billing", field: 'StatusBillingName', allowCellFocus: false },
                // { name: 'Status WO', field: 'xStatusApprovalId' },
                { name: 'status wo3', displayName: "Status WO", field: 'MStatusJob.JobStatusName', allowCellFocus: false },
                { name: 'nomor Faktur pajak3', displayName: "Nomor Faktur Pajak", field: 'FakturNo', allowCellFocus: false }
                // { name: 'Action', cellTemplate: actionAfterBilling, width: 180, pinnedRight: false, visible: false }
            ];

            reprintBilling = [
                { name: 'Nomor Billing', field: 'Billing.BillingNo', allowCellFocus: false },
                { name: 'Tanggal Billing', field: 'Billing.BillingDate', cellFilter: dateFilter, allowCellFocus: false },
                { name: 'Bill To', field: 'Billing.BillTo', allowCellFocus: false },
                { name: 'Bill Address', field: 'Billing.BillAddress', allowCellFocus: false },
                { name: 'Status Billing', field: 'StatusBillingName', allowCellFocus: false },
            ];

        } else {
            afterBilling = [
                { name: 'id', field: 'JobId', width: '4%', visible: false },
                { name: 'no wo2', displayName: "No. WO", field: 'WoNo', width: 160, allowCellFocus: false },
                // { name: 'tgl wo2', displayName: "Tgl. WO", field: 'WoCreatedDate', cellFilter: dateFilter, width: 120, allowCellFocus: false }, // biar bs di filter pake yg bwh
                { name: 'tgl wo2', displayName: "Tgl. WO", field: 'xWoCreatedDate', width: 120, allowCellFocus: false },
                { name: 'no faktur service2', displayName: "No. Faktur Service", field: 'BillingNo', width: 160, allowCellFocus: false },
                // { name: 'tgl billing2', displayName: "Tgl. Billing", field: 'BillingDate', cellFilter: dateFilter, width: 120, allowCellFocus: false }, // biar bs di filter pake yg bwh
                { name: 'tgl billing2', displayName: "Tgl. Billing", field: 'xBillingDate', width: 120, allowCellFocus: false },
                { name: 'no faktur pajak2', displayName: "No. Faktur Pajak", field: 'FakturNo', width: 140, allowCellFocus: false },

                // { name:'No Pembatalan', field:'Billing.CancelBillingNo' },
                { name: 'no polisi2', displayName: "No. Polisi", field: 'PoliceNumber', width: 120, allowCellFocus: false },
                { name: 'nama2', displayName: "Nama", field: 'BillTo', width: 160, allowCellFocus: false },

                // { name: 'model Type2', displayName: "Model", field: 'Job.Vehicle.MVehicleTypeColor.MVehicleType.VehicleModel.VehicleModelName', width: 120 },
                { name: 'model Type2', displayName: "Model", field: 'ModelType', width: 120, allowCellFocus: false },
                { name: 'type job2', displayName: "Kategori WO", field: 'Process.ProcessName', width: 120, allowCellFocus: false },

                { name: 'pekerjaan2', displayName: "Pekerjaan", width: 130, field: 'Job.TaskName', allowCellFocus: false },
                { name: 'charge to2', displayName: "Charge To", width: 130, field: 'ChargeToNew', allowCellFocus: false },

                { name: 'status wo2', displayName: "Status WO", field: 'MStatusJob.JobStatusName', width: 160, allowCellFocus: false },
                //{ name: 'TypeInvoice', field: 'xChargeTo'},
                { name: 'status billing2', displayName: "Status Billing", field: 'StatusBillingName', width: 160, allowCellFocus: false },
                // { name: 'StatusBatalBilling', field: 'xStatusApprovalId' },

                //{ name: 'Nomor Faktur Pajak', field: 'FakturNo', cellTemplate: '<div style="center">{{row.entity.FakturNo=null}} - </div>' }

                // { name: 'Action', cellTemplate: actionAfterBilling, width: 180, pinnedRight: false }
                { name: 'status pembayaran2', displayName: "Status Pembayaran", field: 'isPaidText', width: 160, allowCellFocus: false }

            ];
        }

        var actionBeforeBilling = '<div class="ui-grid-cell-contents">\
        <a href="#" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Buat Billing" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-plus-circle fa-lg" style="margin-left:35%;margin-top:3px"></i></a>\
        </div>';

        var beforeBilling = [
            { name: 'id', field: 'JobId', width: 180, visible: false },
            { name: 'no Wo1', displayName: "No. WO", width: 180, field: 'WoNo', allowCellFocus: false },
            { name: 'no Polisi1', displayName: "No. Polisi", width: 120, field: 'PoliceNumber', allowCellFocus: false },
            // { name: 'tgl WO1', displayName: "Tgl. WO", width: 130, field: 'WoCreatedDate', cellFilter: dateFilter, allowCellFocus: false }, // biar bs di filter pake yg bwh
            { name: 'tgl WO1', displayName: "Tgl. WO", width: 130, field: 'xWoCreatedDate', allowCellFocus: false },
            { name: 'model1', displayName: "Model", width: 130, field: 'ModelType', allowCellFocus: false },
            //{ name: 'Type Job', field: 'Process.Name' },
            { name: 'type Job1', displayName: "Kategori WO", width: 130, field: 'TypeJob', allowCellFocus: false },
            { name: 'pekerjaan1', displayName: "Pekerjaan", width: 130, field: 'TaskName', allowCellFocus: false },
            { name: 'charge to1', displayName: "Charge To", width: 130, field: 'ChargeToNew', allowCellFocus: false },
            { name: 'status Wo1', displayName: "Status WO", width: 230, field: 'StatusWo', allowCellFocus: false },
            //{ name: 'Status Wo',  field: 'Status', cellTemplate:'<div>{{row.entity.name="Completed"}}</div>' },
            { name: 'Action', cellTemplate: actionBeforeBilling, width: 85, pinnedRight: false }
        ];


        $scope.gridActionTemplate = actionAfterBilling;
        var test = {};
        // var filternih;
        $scope.select = function(data) {
            $scope.grid.data = [];
            $scope.showw = data;
            if (data == 1) {
                // test = beforeBilling;
                $scope.grid.columnDefs = new Array();
                $scope.grid.columnDefs = beforeBilling;
                $scope.showButton1 = true;
                $scope.changingFilterButton();
                $scope.formGridApi.core.refresh();
                console.log("==============>", $scope.formGridApi)
                    // filternih = beforeBilling;
            } else if (data == 2) {
                // test = afterBilling;
                $scope.grid.columnDefs = new Array();
                $scope.grid.columnDefs = afterBilling;
                $scope.showButton1 = false;
                $scope.changingFilterButton();
                $scope.formGridApi.core.refresh();
                // filternih = afterBilling;
            } else if (data == 3) {
                $scope.grid.columnDefs = new Array();
                $scope.grid.columnDefs = reprintBilling;
                $scope.showButton1 = false;
                $scope.changingFilterButton();
                $scope.formGridApi.core.refresh();
            } else {
                $scope.grid.columnDefs = [];
                // filternih = [];
            }

            var findindex = _.findIndex($scope.formGridApi.grid.columns, function(data){
                return (typeof data.filters[0].term == 'string' &&  typeof data.filters[0].term != undefined)
            })
            console.log('findindex ===>',findindex);
            $scope.formGridApi.grid.columns[findindex].filters[0] = {}
            


            console.log("inisiisi", data);
        }

        $scope.dataFilter = [{
                Id: 1,
                Name: "Tanggal Billing"
            },
            {
                Id: 2,
                Name: "Tanggal WO"
            }
        ];

        $scope.dataFilterReprint = [{
                Id: 1,
                Name: "Tanggal Billing"
            },
            {
                Id: 2,
                Name: "Nama"
            }
        ];

        $scope.disableDate = function(value) {
                if (value == 1) {
                    $scope.DateWo = false;
                    $scope.DateJob = true;
                } else {
                    $scope.DateJob = false;
                    $scope.DateWo = true;
                }
            }
            //----------------------------------
            // Grid Setup
            //----------------------------------
        var dateFilter = 'date:"dd/MM/yyyy"';

        //grid work

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            // enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: afterBilling
        };

        $scope.lmModel = {};
        $scope.ldModel = {};
        $scope.listApi = {};
        $scope.listApiJob = {};
        $scope.listApiParts = {};
        $scope.listApiOpl = {};
        $scope.listApiOpb = {};


        // $scope.gridPartsDetail = {

        //   columnDefs: [
        //     { name: "NoMaterial",field: "Parts.PartsCode"},
        //     {name: "NamaMaterial",field: "Parts.PartsName"},
        //     {name: "Qty",field: "Qty"},
        //     {name: "Satuan"},
        //     {name: "Ketersedian"},
        //     {name: "Tipe"},
        //     {name: "ETA"},
        //     {name: "Harga",field: "Parts.RetailPrice"},
        //     {name: "Disc"},
        //     {name: "SubTotal"},
        //     {name: "NilaiDp"},
        //     {name: "StatusDp"},
        //     {name: "Pembayaran"},
        //     {name: "OPB"}
        //   ]
        // }

        $scope.listSelectedRows = [];
        $scope.onListSelectRows = function(rows) {
            console.log("onListSelectRows | Jasa/Labor", rows);
            $scope.listSelectedRows = rows;

            if(tmp_mBillingDataPayment == undefined){

                tmp_mBillingDataPayment = $scope.mBilling.dataPayment;
            }
            

            console.log('onListSelectRows | listSelectedRows ==========>',$scope.listSelectedRows)
            console.log('onListSelectRows | mBilling.dataPayment ======>',$scope.mBilling.dataPayment)
            console.log('onListSelectRows | tmp_mBillingDataPayment ===>',tmp_mBillingDataPayment)
            $scope.subTotalLabor = 0;
            $scope.discountPercentFinalTask = 0;
            $scope.discountFinalTask = 0;
            var FlateRateF = 0;

            // for (var i = 0; i < $scope.listSelectedRows.length; i++) {
            //     if ($scope.listSelectedRows[i].FlatRate == null || $scope.listSelectedRows[i].FlatRate == undefined) {
            //         FlateRateF = 1;
            //     } else {
            //         FlateRateF = $scope.listSelectedRows[i].FlatRate;
            //     }

            //     console.log("FlateRateF", FlateRateF);
            //     var subTotalLaborBefore = $scope.listSelectedRows[i].Fare * FlateRateF;
            //     // To Avoid double PPN
            //     var subTotalLaborPpn = subTotalLaborBefore / (1.1);
            //     $scope.subTotalLabor += subTotalLaborPpn;

            //     // Jika Insurance get DiscountJasa from MProfile_Insurance
            //     if ($scope.listSelectedRows[i].PaidById == 29) { // 29: Insurance
            //         if ($scope.dataInsurance[0] != undefined || $scope.dataInsurance[0] != null) {
            //             $scope.listSelectedRows[i].Discount = $scope.dataInsurance[0].DiscountJasa == null ? 0 : $scope.dataInsurance[0].DiscountJasa;
            //             console.log("$scope.listSelectedRows[i].Discount: ", $scope.listSelectedRows[i].Discount);
            //             var discountTask = (subTotalLaborPpn * ($scope.listSelectedRows[i].Discount == null ? 0 : $scope.listSelectedRows[i].Discount)) / 100;
            //             $scope.discountFinalTask += discountTask;
            //             $scope.discountPercentFinalTask += $scope.listSelectedRows[i].Discount;
            //         }
            //     } else {
            //         if ($scope.listSelectedRows[i].Discount > 0) {
            //             var discountTask = (subTotalLaborPpn * ($scope.listSelectedRows[i].Discount == null ? 0 : $scope.listSelectedRows[i].Discount)) / 100;
            //             $scope.discountFinalTask += discountTask;
            //             $scope.discountPercentFinalTask += $scope.listSelectedRows[i].Discount;
            //         }
            //     }
            // }


            // ================================================================ Rounding CR4 ======================================================================            
            for (var i = 0; i < $scope.listSelectedRows.length; i++) {
                if ($scope.listSelectedRows[i].FlatRate == null || $scope.listSelectedRows[i].FlatRate == undefined) {
                    FlateRateF = 1;
                } else {
                    FlateRateF = $scope.listSelectedRows[i].FlatRate;
                }
                console.log("FlateRateF", FlateRateF);

                // var harga = $scope.listSelectedRows[i].Fare * FlateRateF; //karena di wo fare * flaterate nya di round.. ini ngikut aja dah selisih 1 mulu soal nya di biling
                var harga = Math.round($scope.listSelectedRows[i].Fare * FlateRateF);
               
                // $scope.subTotalLabor += (harga *10/11); //kalo bagi 1.1 sk aneh bos cek aja (1626240/1.1) 
                // $scope.subTotalLabor += Math.round(harga *10/11); // akhirnya di round lagi dong 
                $scope.subTotalLabor += ASPricingEngine.calculate({
                                            InputPrice: harga,
                                            // Discount: 5,
                                            // Qty: 2,
                                            Tipe: 2, 
                                            PPNPercentage: PPNPerc,
                                        });

                
                // validasi jika masuk view billing maka discount tidak perlu di timpa pake data insurance
                // kalau masuk create billing maka cek pembayaran di informasi penagihan, jika ke insurance, maka timpa discount nya
                var discJob = $scope.listSelectedRows[i].Discount
                if ($scope.modeBil == 'edit'){
                    if ($scope.mBilling.dataPayment == 29){
                        if ($scope.dataInsurance[0] != undefined || $scope.dataInsurance[0] != null) {
                            discJob = $scope.dataInsurance[0].DiscountJasa == null ? 0 : $scope.dataInsurance[0].DiscountJasa;
                        } else {
                            discJob = 0; // data insurance ga ada, tp milih pembayaran insurance
                        }

                    }
                }

                // Jika Insurance get DiscountJasa from MProfile_Insurance
                if ($scope.listSelectedRows[i].PaidById == 29) { // 29: Insurance
                    if ($scope.dataInsurance[0] != undefined || $scope.dataInsurance[0] != null) {
                        // $scope.listSelectedRows[i].Discount = $scope.dataInsurance[0].DiscountJasa == null ? 0 : $scope.dataInsurance[0].DiscountJasa;
                        
                        // $scope.discountFinalTask += Math.round(Math.round(harga / 1.1) * $scope.listSelectedRows[i].Discount /100);
                        $scope.discountFinalTask += ASPricingEngine.calculate({
                                                        InputPrice: harga,
                                                        Discount: discJob,
                                                        // Qty: 2,
                                                        Tipe: 12, 
                                                        PPNPercentage: PPNPerc,
                                                    }); 
                    }
                } else {
                    if (discJob > 0) {
                        // $scope.discountFinalTask += Math.round(Math.round(harga / 1.1) * $scope.listSelectedRows[i].Discount /100);
                        $scope.discountFinalTask += ASPricingEngine.calculate({
                                                        InputPrice: harga,
                                                        Discount: discJob,
                                                        // Qty: 2,
                                                        Tipe: 12, 
                                                        PPNPercentage: PPNPerc,
                                                    }); 
                    }
                }
            }
            // ================================================================ Rounding CR4 ======================================================================            




            var TotalFinal = $scope.subTotalOpb + $scope.subTotalParts + $scope.subTotalLabor + $scope.subTotalOpl;
            $scope.TotalFinal = TotalFinal;


            var TotalAfterDiscount = TotalFinal - ($scope.discountFinalTask + $scope.discountFinalParts + $scope.discountFinalPartsOpb + $scope.discountFinalOpl);
            $scope.TotalAfterDiscountFinal = TotalAfterDiscount;

            // $scope.PPN = Math.floor($scope.TotalAfterDiscountFinal * 0.1);
            $scope.PPN = ASPricingEngine.calculate({
                            InputPrice: (($scope.TotalAfterDiscountFinal) * (1+PPNPerc/100)),
                            // Discount: 0,
                            // Qty: 0,
                            Tipe: 33, 
                            PPNPercentage: PPNPerc,
                        });    
            $scope.TotalInvoice = $scope.TotalAfterDiscountFinal + $scope.PPN;
            // $scope.TotalYangHarusDibayar = $scope.TotalInvoice - $scope.DP; // di ubah req dari raflizal dan pa dodi 17September2020
            $scope.TotalYangHarusDibayar = $scope.TotalInvoice - $scope.DPPaid; // di ubah req dari raflizal dan pa dodi 17September2020

            $scope.finalAlldiscount = $scope.discountFinalTask + 0 + $scope.discountFinalParts + $scope.discountFinalPartsOpb + $scope.discountFinalOpl;

            if (TotalAfterDiscount <= 250000) {
                $scope.Materai = 0;
            } else if (TotalAfterDiscount > 250000 && TotalAfterDiscount <= 1000000) {
                $scope.Materai = 0;
            } else {
                $scope.Materai = 0;
            }

            var adaPembayaranBerbeda = 0;
            if ($scope.listSelectedRows != null && $scope.listSelectedRows != undefined && $scope.listSelectedRows.length > 0) {
                for (var i = 0; i < $scope.listSelectedRows.length; i++) {
                    if ($scope.listSelectedRows[i].PaidById != tmp_mBillingDataPayment) {
                        adaPembayaranBerbeda++;
                    }
                }
            }

            if (adaPembayaranBerbeda > 0) {
                if ($scope.dataByJobId[0].DPPaidAmount > 0) {
                    bsAlert.alert({
                            title: 'Terdapat perubahan tipe pembayaran atas parts yang memiliki DP (Down Payment) <br/> Jika proses Billing tetap dilanjutkan maka <span style="color:red">nominal DP (Down Payment) tidak akan terpotong</span>',
                            text: "",
                            type: "warning"
                        },
                        function() {},
                        function() {}
                    )
                } else {
                    bsAlert.alert({
                            title: "Terdapat Tipe Pembayaran Yang Berbeda",
                            text: "",
                            type: "warning"
                        },
                        function() {},
                        function() {}
                    )
                }
            }

            console.log("buka", $scope.listSelectedRows);
            console.log("form controller=>", rows);

            $timeout(function() {
                // ini di buat karena ada bentrok antara maskme dan bslist sehingga TIN suka berubah jadi null ketika click select all di bslist
                if ($scope.mBilling.TIN === null){
                    if ($scope.mBilling.TINBackup !== null && $scope.mBilling.TINBackup !== undefined){
                        $scope.mBilling.TIN = angular.copy($scope.mBilling.TINBackup)
                    }
                }
            }, 200);
            
        }

        $scope.listSelectedRowsOPL = [];
        $scope.onListSelectRowsOPL = function(rows) {
            console.log('onListSelectRowsOPL | OPL ====>',rows)
            $scope.listSelectedRowsOPL = rows;
            $scope.subTotalOpl = 0;
            $scope.discountPercentFinalOpl = 0;
            $scope.discountFinalOpl = 0;

            
            // for (var o = 0; o < $scope.listSelectedRowsOPL.length; o++) {
            //     var TotalOpl = $scope.listSelectedRowsOPL[o].Price * $scope.listSelectedRowsOPL[o].QtyPekerjaan;

            //     // To Avoid double PPN
            //     var subTotalOplBeforePpn = TotalOpl / (1.1);

            //     if ($scope.listSelectedRowsOPL[o].PaidById != 30) {
            //         $scope.subTotalOpl += subTotalOplBeforePpn;
            //     }

            //     var discountOplNonPpn = subTotalOplBeforePpn;
            //     var discountOpl = (discountOplNonPpn * ($scope.listSelectedRowsOPL[o].Discount == null ? 0 : $scope.listSelectedRowsOPL[o].Discount)) / 100;
            //     $scope.discountFinalOpl += discountOpl;
            //     $scope.discountPercentFinalOpl += $scope.listSelectedRowsOPL[o].Discount;

            //     if ($scope.listSelectedRowsOPL[o].PaidById == 30) {
            //         TotalOpl = $scope.listSelectedRowsOPL[o].OPLPurchasePrice * $scope.listSelectedRowsOPL[o].QtyPekerjaan;
            //         subTotalOplBeforePpn = TotalOpl / (1.1);
            //         discountOplNonPpn = subTotalOplBeforePpn;
            //         $scope.subTotalOpl += discountOplNonPpn;
            //         discountOpl = (discountOplNonPpn * ($scope.listSelectedRowsOPL[o].Discount == null ? 0 : $scope.listSelectedRowsOPL[o].Discount)) / 100;
            //         $scope.discountFinalOpl += discountOpl;
            //         $scope.discountPercentFinalOpl += $scope.listSelectedRowsOPL[o].Discount;
            //     }
            // }

            // ================================================================ Rounding CR4 OPL ======================================================================            
            for (var o = 0; o < $scope.listSelectedRowsOPL.length; o++) {
                if ($scope.listSelectedRowsOPL[o].PaidById == 30){
                    // kl opl warranty ambil total nya dari OPLPurchasePrice
                    // $scope.subTotalOpl += Math.round($scope.listSelectedRowsOPL[o].OPLPurchasePrice / 1.1) * $scope.listSelectedRowsOPL[o].QtyPekerjaan;
                    $scope.subTotalOpl += ASPricingEngine.calculate({
                                                InputPrice: $scope.listSelectedRowsOPL[o].OPLPurchasePrice,
                                                // Discount: 5,
                                                Qty: $scope.listSelectedRowsOPL[o].QtyPekerjaan,
                                                Tipe: 102, 
                                                PPNPercentage: PPNPerc,
                                            }); 
                    
                } else {
                // $scope.subTotalOpl += Math.round($scope.listSelectedRowsOPL[o].Price * 10/11) * $scope.listSelectedRowsOPL[o].QtyPekerjaan; // akhirnya di round lagi dong
                $scope.subTotalOpl += ASPricingEngine.calculate({
                                            InputPrice: $scope.listSelectedRowsOPL[o].Price,
                                            // Discount: 5,
                                            Qty: $scope.listSelectedRowsOPL[o].QtyPekerjaan,
                                            Tipe: 102, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                }


                // validasi jika masuk view billing maka discount tidak perlu di timpa pake data insurance
                // kalau masuk create billing maka cek pembayaran di informasi penagihan, jika ke insurance, maka timpa discount nya
                var discOpl = $scope.listSelectedRowsOPL[o].Discount == null ? 0 : $scope.listSelectedRowsOPL[o].Discount
                if ($scope.modeBil == 'edit'){
                    if ($scope.mBilling.dataPayment == 29){
                        if ($scope.dataInsurance[0] != undefined || $scope.dataInsurance[0] != null) {
                            discOpl = $scope.dataInsurance[0].DiscountJasa == null ? 0 : $scope.dataInsurance[0].DiscountJasa;
                        } else {
                            discOpl = 0; // data insurance ga ada, tp milih pembayaran insurance
                        }

                    }
                }

                if ($scope.listSelectedRowsOPL[o].PaidById == 30){
                    $scope.discountFinalOpl += 0
                } else {
                    // $scope.discountFinalOpl += Math.round((Math.round($scope.listSelectedRowsOPL[o].Price / 1.1))*(($scope.listSelectedRowsOPL[o].Discount == null ? 0 : $scope.listSelectedRowsOPL[o].Discount)/ 100))*$scope.listSelectedRowsOPL[o].QtyPekerjaan;
                    $scope.discountFinalOpl += ASPricingEngine.calculate({
                                                    InputPrice: $scope.listSelectedRowsOPL[o].Price,
                                                    Discount: discOpl,
                                                    Qty: $scope.listSelectedRowsOPL[o].QtyPekerjaan,
                                                    Tipe: 112, 
                                                    PPNPercentage: PPNPerc,
                                                });
                }
                 
            }
            // ================================================================ Rounding CR4 OPL ======================================================================            


            var TotalFinal = $scope.subTotalOpb + $scope.subTotalParts + $scope.subTotalLabor + $scope.subTotalOpl;
            $scope.TotalFinal = TotalFinal;


            var TotalAfterDiscount = TotalFinal - ($scope.discountFinalTask + $scope.discountFinalParts + $scope.discountFinalPartsOpb + $scope.discountFinalOpl);
            $scope.TotalAfterDiscountFinal = TotalAfterDiscount;

            // $scope.PPN = Math.floor($scope.TotalAfterDiscountFinal * 0.1);
            $scope.PPN = ASPricingEngine.calculate({
                            InputPrice: (($scope.TotalAfterDiscountFinal) * (1+PPNPerc/100)),
                            // Discount: 0,
                            // Qty: 0,
                            Tipe: 33, 
                            PPNPercentage: PPNPerc,
                        });   
            $scope.TotalInvoice = $scope.TotalAfterDiscountFinal + $scope.PPN;
            // $scope.TotalYangHarusDibayar = $scope.TotalInvoice - $scope.DP; // di ubah req dari raflizal dan pa dodi 17September2020
            $scope.TotalYangHarusDibayar = $scope.TotalInvoice - $scope.DPPaid; // di ubah req dari raflizal dan pa dodi 17September2020

            $scope.finalAlldiscount = $scope.discountFinalTask + 0 + $scope.discountFinalParts + $scope.discountFinalPartsOpb + $scope.discountFinalOpl;

            if (TotalAfterDiscount <= 250000) {
                $scope.Materai = 0;
            } else if (TotalAfterDiscount > 250000 && TotalAfterDiscount <= 1000000) {
                $scope.Materai = 0;
            } else {
                $scope.Materai = 0;
            }

            var adaPembayaranBerbeda = 0;
            if ($scope.listSelectedRowsOPL != null && $scope.listSelectedRowsOPL != undefined && $scope.listSelectedRowsOPL.length > 0) {
                for (var i = 0; i < $scope.listSelectedRowsOPL.length; i++) {
                    if ($scope.listSelectedRowsOPL[i].PaidById != tmp_mBillingDataPayment) {
                        adaPembayaranBerbeda++;
                    }
                }
            }
            if (adaPembayaranBerbeda > 0) {
                if ($scope.dataByJobId[0].DPPaidAmount > 0) {
                    bsAlert.alert({
                            title: 'Terdapat perubahan tipe pembayaran atas parts yang memiliki DP (Down Payment) <br/> Jika proses Billing tetap dilanjutkan maka <span style="color:red">nominal DP (Down Payment) tidak akan terpotong</span>',
                            text: "",
                            type: "warning"
                        },
                        function() {},
                        function() {}
                    )
                } else {
                    bsAlert.alert({
                            title: "Terdapat Tipe Pembayaran Yang Berbeda",
                            text: "",
                            type: "warning"
                        },
                        function() {},
                        function() {}
                    )
                }
            }

            console.log("form controller=>", rows);

            $timeout(function() {
                // ini di buat karena ada bentrok antara maskme dan bslist sehingga TIN suka berubah jadi null ketika click select all di bslist
                if ($scope.mBilling.TIN === null){
                    if ($scope.mBilling.TINBackup !== null && $scope.mBilling.TINBackup !== undefined){
                        $scope.mBilling.TIN = angular.copy($scope.mBilling.TINBackup)
                    }
                }
            }, 200);
        }

        $scope.listSelectedRowsOpb = [];
        $scope.onListSelectRowsOpb = function(rows) {
            console.log('onListSelectRowsOpb | OPB ====>',rows)
            $scope.listSelectedRowsOpb = rows;
            $scope.discountFinalPartsOpb = 0;
            $scope.subTotalOpb = 0;
            $scope.discountPercentFinalOpb = 0;
            // $scope.DP = 0;
            // for (var b = 0; b < $scope.listSelectedRowsOpb.length; b++) {
            //     var TotalOpb = $scope.listSelectedRowsOpb[b].Price * $scope.listSelectedRowsOpb[b].Qty;
            //     // To Avoid double PPN
            //     var subTotalOpbBeforePpn = TotalOpb / (1.1);
            //     $scope.subTotalOpb += subTotalOpbBeforePpn;

            //     // Jika Insurance get DiscountJasa from MProfile_Insurance
            //     if ($scope.listSelectedRowsOpb[b].PaidById == 29) { // 29: Insurance
            //         if ($scope.dataInsurance[0] != undefined || $scope.dataInsurance[0] != null) {
            //             $scope.listSelectedRowsOpb[b].Discount = $scope.dataInsurance[0].DiscountPart == null ? 0 : $scope.dataInsurance[0].DiscountPart;
            //             console.log("$scope.listSelectedRowsOpb[b].Discount: ", $scope.listSelectedRowsOpb[b].Discount);
            //             var discountPartsOpbNonPPn = subTotalOpbBeforePpn;
            //             var discountPartsOpb = (discountPartsOpbNonPPn * ($scope.listSelectedRowsOpb[b].Discount == null ? 0 : $scope.listSelectedRowsOpb[b].Discount)) / 100;
            //             $scope.discountFinalPartsOpb += discountPartsOpb;
            //             $scope.discountPercentFinalOpb += $scope.listSelectedRowsOpb[b].Discount;
            //         }
            //     } else {
            //         var discountPartsOpbNonPPn = subTotalOpbBeforePpn;
            //         var discountPartsOpb = (discountPartsOpbNonPPn * $scope.listSelectedRowsOpb[b].Discount) / 100;
            //         $scope.discountFinalPartsOpb += discountPartsOpb;
            //         $scope.discountPercentFinalOpb += $scope.listSelectedRowsOpb[b].Discount;
            //     }

            //     $scope.DP += $scope.listSelectedRowsOpb[b].DownPayment;
            // }


            // ================================================================ Rounding CR4 OPB ======================================================================            

            for (var b = 0; b < $scope.listSelectedRowsOpb.length; b++) {
               
               
                // $scope.subTotalOpb += Math.round($scope.listSelectedRowsOpb[b].Price * 10/11) * $scope.listSelectedRowsOpb[b].Qty; // akhirnya di round lagi dong
                $scope.subTotalOpb += ASPricingEngine.calculate({
                                            InputPrice: $scope.listSelectedRowsOpb[b].Price,
                                            // Discount: 5,
                                            Qty: $scope.listSelectedRowsOpb[b].Qty,
                                            Tipe: 102, 
                                            PPNPercentage: PPNPerc,
                                        }); 


                // validasi jika masuk view billing maka discount tidak perlu di timpa pake data insurance
                // kalau masuk create billing maka cek pembayaran di informasi penagihan, jika ke insurance, maka timpa discount nya
                var discOpb = $scope.listSelectedRowsOpb[b].Discount
                if ($scope.modeBil == 'edit'){
                    if ($scope.mBilling.dataPayment == 29){
                        if ($scope.dataInsurance[0] != undefined || $scope.dataInsurance[0] != null) {
                            discOpb = $scope.dataInsurance[0].DiscountPart == null ? 0 : $scope.dataInsurance[0].DiscountPart;
                        } else {
                            discOpb = 0; // data insurance ga ada, tp milih pembayaran insurance
                        }

                    }
                }

                // Jika Insurance get DiscountJasa from MProfile_Insurance
                if ($scope.listSelectedRowsOpb[b].PaidById == 29) { // 29: Insurance
                    if ($scope.dataInsurance[0] != undefined || $scope.dataInsurance[0] != null) {
                        // $scope.listSelectedRowsOpb[b].Discount = $scope.dataInsurance[0].DiscountPart == null ? 0 : $scope.dataInsurance[0].DiscountPart;
                        
                        // $scope.discountFinalPartsOpb += Math.round(Math.round($scope.listSelectedRowsOpb[b].Price / 1.1) * $scope.listSelectedRowsOpb[b].Discount/100)*$scope.listSelectedRowsOpb[b].Qty;
                        $scope.discountFinalPartsOpb += ASPricingEngine.calculate({
                                                            InputPrice: $scope.listSelectedRowsOpb[b].Price,
                                                            Discount: discOpb,
                                                            Qty: $scope.listSelectedRowsOpb[b].Qty,
                                                            Tipe: 112, 
                                                            PPNPercentage: PPNPerc,
                                                        }); 
                    }
                } else if ($scope.listSelectedRowsOpb[b].PaidById == 30) {
                    $scope.discountFinalPartsOpb += 0 // biar kl warranty jng ada diskon di summary billing na

                } else {
                    // $scope.discountFinalPartsOpb += Math.round(Math.round($scope.listSelectedRowsOpb[b].Price / 1.1) * $scope.listSelectedRowsOpb[b].Discount/100)*$scope.listSelectedRowsOpb[b].Qty;
                    $scope.discountFinalPartsOpb += ASPricingEngine.calculate({
                                                        InputPrice: $scope.listSelectedRowsOpb[b].Price,
                                                        Discount: discOpb,
                                                        Qty: $scope.listSelectedRowsOpb[b].Qty,
                                                        Tipe: 112, 
                                                        PPNPercentage: PPNPerc,
                                                    }); 
                }

                // $scope.DP += $scope.listSelectedRowsOpb[b].DownPayment;
                if ($scope.mBilling.dataPayment == 28) {
                    $scope.DP = $scope.dataByJobId[0].FinalDP; // di ubah req dari raflizal dan pa dodi 17September2020
                    $scope.DPPaid = $scope.dataByJobId[0].DPPaidAmount; // di ubah req dari raflizal dan pa dodi 17September2020
                } else {
                    $scope.DP = 0; // req pa dodi 21 September 2020, jika biling bukan charge to customer, dp dan dppaid nya di 0 in
                    $scope.DPPaid = 0; // req pa dodi 21 September 2020, jika biling bukan charge to customer, dp dan dppaid nya di 0 in
                }
                
            }
            // ================================================================ Rounding CR4 OPB ======================================================================            
            



            var TotalFinal = $scope.subTotalOpb + $scope.subTotalParts + $scope.subTotalLabor + $scope.subTotalOpl;
            $scope.TotalFinal = TotalFinal;


            var TotalAfterDiscount = TotalFinal - ($scope.discountFinalTask + $scope.discountFinalParts + $scope.discountFinalPartsOpb + $scope.discountFinalOpl);
            $scope.TotalAfterDiscountFinal = TotalAfterDiscount;

            // $scope.PPN = Math.floor($scope.TotalAfterDiscountFinal * 0.1);
            $scope.PPN = ASPricingEngine.calculate({
                            InputPrice: (($scope.TotalAfterDiscountFinal) * (1+PPNPerc/100)),
                            // Discount: 0,
                            // Qty: 0,
                            Tipe: 33, 
                            PPNPercentage: PPNPerc,
                        });   
            $scope.TotalInvoice = $scope.TotalAfterDiscountFinal + $scope.PPN;
            // $scope.TotalYangHarusDibayar = $scope.TotalInvoice - $scope.DP;
            $scope.TotalYangHarusDibayar = $scope.TotalInvoice - $scope.DPPaid; // di ubah req dari raflizal dan pa dodi 17September2020

            $scope.finalAlldiscount = $scope.discountFinalTask + 0 + $scope.discountFinalParts + $scope.discountFinalPartsOpb + $scope.discountFinalOpl;

            if (TotalAfterDiscount <= 250000) {
                $scope.Materai = 0;
            } else if (TotalAfterDiscount > 250000 && TotalAfterDiscount <= 1000000) {
                $scope.Materai = 0;
            } else {
                $scope.Materai = 0;
            }

            var adaPembayaranBerbeda = 0;
            if ($scope.listSelectedRowsOpb != null && $scope.listSelectedRowsOpb != undefined && $scope.listSelectedRowsOpb.length > 0) {
                for (var i = 0; i < $scope.listSelectedRowsOpb.length; i++) {
                    if ($scope.listSelectedRowsOpb[i].PaidById != tmp_mBillingDataPayment) {
                        adaPembayaranBerbeda++;
                    }
                }
            }

            if (adaPembayaranBerbeda > 0) {
                if ($scope.dataByJobId[0].DPPaidAmount > 0) {
                    bsAlert.alert({
                            title: 'Terdapat perubahan tipe pembayaran atas parts yang memiliki DP (Down Payment) <br/> Jika proses Billing tetap dilanjutkan maka <span style="color:red">nominal DP (Down Payment) tidak akan terpotong</span>',
                            text: "",
                            type: "warning"
                        },
                        function() {},
                        function() {}
                    )
                } else {
                    bsAlert.alert({
                            title: "Terdapat Tipe Pembayaran Yang Berbeda",
                            text: "",
                            type: "warning"
                        },
                        function() {},
                        function() {}
                    )
                }
            }

            $timeout(function() {
                // ini di buat karena ada bentrok antara maskme dan bslist sehingga TIN suka berubah jadi null ketika click select all di bslist
                if ($scope.mBilling.TIN === null){
                    if ($scope.mBilling.TINBackup !== null && $scope.mBilling.TINBackup !== undefined){
                        $scope.mBilling.TIN = angular.copy($scope.mBilling.TINBackup)
                    }
                }
            }, 200);

        }

        $scope.listSelectedRowsParts = [];
        $scope.onListSelectRowsParts = function(rows) {
            console.log('onListSelectRowsParts | Parts ====>',rows)
            $scope.listSelectedRowsParts = rows;
            $scope.subTotalParts = 0;
            $scope.discountPercentFinalParts = 0;
            $scope.discountFinalParts = 0;
            // $scope.DP = 0;
            // for (var a = 0; a < $scope.listSelectedRowsParts.length; a++) {
            //     var TotalParts = parseFloat(($scope.listSelectedRowsParts[a].Price / (1.1)).toFixed(2));
            //     var TotalPartsNoQty = $scope.listSelectedRowsParts[a].Price;
            //     var PartsQty = $scope.listSelectedRowsParts[a].Qty;
            //     TotalParts = TotalParts * $scope.listSelectedRowsParts[a].Qty;
            //     // To Avoid double PPN
            //     var subTotalPartsPpn = TotalParts;
            //     var subTotalPartsNoQty = TotalPartsNoQty;

            //     // Jika Insurance get DiscountJasa from MProfile_Insurance
            //     if ($scope.listSelectedRowsParts[a].PaidById == 29) { // 29: Insurance
            //         if ($scope.dataInsurance[0] != undefined || $scope.dataInsurance[0] != null) {
            //             $scope.listSelectedRowsParts[a].Discount = $scope.dataInsurance[0].DiscountPart == null ? 0 : $scope.dataInsurance[0].DiscountPart;
            //             console.log("$scope.listSelectedRowsParts[a].Discount: ", $scope.listSelectedRowsParts[a].Discount);
            //             var discountNonPpnParts = subTotalPartsNoQty / (1.1); // Exclude PPN
            //             var discountParts = parseFloat((discountNonPpnParts * ($scope.listSelectedRowsParts[a].Discount == null ? 0 : $scope.listSelectedRowsParts[a].Discount) / 100).toFixed(2));
            //             //var discountParts = ($scope.subTotalParts * ($scope.listSelectedRowsParts[a].Discount == null ? 0 : $scope.listSelectedRowsParts[a].Discount)) / 100;
            //             discountParts = discountParts * PartsQty;
            //             $scope.discountFinalParts += discountParts;
            //             $scope.discountPercentFinalParts += $scope.listSelectedRowsParts[a].Discount;
            //         }
            //     } else if ($scope.listSelectedRowsParts[a].PaidById == 30) { // 30: Warranty
            //         // To Avoid double PPN
            //         subTotalPartsPpn = subTotalPartsPpn * (1.1);
            //         $scope.subTotalParts += subTotalPartsPpn;
            //         var discountNonPpnParts = subTotalPartsPpn;
            //         $scope.discountFinalParts += 0;
            //         $scope.discountPercentFinalParts += 0;
            //     } else {
            //         if ($scope.listSelectedRowsParts[a].Discount > 0) {
            //             var discountNonPpnParts = subTotalPartsNoQty / (1.1); // Exclude PPN
            //             var discountParts = parseFloat((discountNonPpnParts * ($scope.listSelectedRowsParts[a].Discount == null ? 0 : $scope.listSelectedRowsParts[a].Discount) / 100).toFixed(2));
            //             discountParts = discountParts * PartsQty;
            //             $scope.discountFinalParts += discountParts;
            //             $scope.discountPercentFinalParts += $scope.listSelectedRowsParts[a].Discount;
            //         }
            //     }

            //     if ($scope.listSelectedRowsParts[a].PaidById != 30) { // 30: Warranty
            //       $scope.subTotalParts += subTotalPartsPpn;
            //     }

            //     $scope.DP += $scope.listSelectedRowsParts[a].DownPayment;
            //     console.log('$scope.DP', $scope.DP);
            // }

            // ================================================================ Rounding CR4 Parts ======================================================================    
            for (var a = 0; a < $scope.listSelectedRowsParts.length; a++) {
               
                $scope.getTypeJobFreeClaim = 0
                if ($scope.temp_dataJobList.OutletId < 2000000) {
                    for (var i = 0; i < $scope.temp_dataJobList.JobTask.length; i++) {

                        if ($scope.listSelectedRowsParts[a].JobTaskId == $scope.temp_dataJobList.JobTask[i].JobTaskId) {
                            var getdataIdClaimType = angular.copy($scope.temp_dataJobList.JobTask[i].JobType.MasterId);
                            if (getdataIdClaimType == 2675 || getdataIdClaimType == 2677 || getdataIdClaimType == 2678) {
                                $scope.getTypeJobFreeClaim++;
                            }
                        }

                        // for (var j = 0; j < $scope.temp_dataJobList.JobTask[i].JobParts.length; j++) {
                        //     if ($scope.temp_dataJobList.JobTask[i].JobParts[j].isDeleted == 0) {
                        //         var getdataIdClaimType = angular.copy($scope.temp_dataJobList.JobTask[i].JobType.MasterId);
                        //         // 2675 FPC - Free Parts Claim || 2677 FLC - Free Labor Clai || 2678 PLC - Parts Labor Claim
                        //         if (getdataIdClaimType == 2675 || getdataIdClaimType == 2677 || getdataIdClaimType == 2678) {
                        //             $scope.getTypeJobFreeClaim++;
                        //         }else{
                        //             $scope.getTypeJobFreeClaim = 0;
                        //         }
                        //     }
                        // }   
                    }
                }

                // validasi jika masuk view billing maka discount tidak perlu di timpa pake data insurance
                // kalau masuk create billing maka cek pembayaran di informasi penagihan, jika ke insurance, maka timpa discount nya
                var discParts = $scope.listSelectedRowsParts[a].Discount
                if ($scope.modeBil == 'edit'){
                    if ($scope.mBilling.dataPayment == 29){
                        if ($scope.dataInsurance[0] != undefined || $scope.dataInsurance[0] != null) {
                            discParts = $scope.dataInsurance[0].DiscountPart == null ? 0 : $scope.dataInsurance[0].DiscountPart;
                        } else {
                            discParts = 0; // data insurance ga ada, tp milih pembayaran insurance
                        }

                    }
                }
                
                // Jika Insurance get DiscountJasa from MProfile_Insurance
                if ($scope.listSelectedRowsParts[a].PaidById == 29) { // 29: Insurance
                    if ($scope.dataInsurance[0] != undefined || $scope.dataInsurance[0] != null) {
                        // $scope.listSelectedRowsParts[a].Discount = $scope.dataInsurance[0].DiscountPart == null ? 0 : $scope.dataInsurance[0].DiscountPart;
                        // $scope.subTotalParts += Math.round($scope.listSelectedRowsParts[a].Price / 1.1) * $scope.listSelectedRowsParts[a].Qty; // kata pa cien ganti rumusnya
                        // $scope.subTotalParts += ($scope.listSelectedRowsParts[a].Price * $scope.listSelectedRowsParts[a].Qty) / 1.1;
                        // $scope.subTotalParts += ($scope.listSelectedRowsParts[a].Price * $scope.listSelectedRowsParts[a].Qty) *10/11;
                     
                        
                        
                        // $scope.subTotalParts += Math.round($scope.listSelectedRowsParts[a].Price *10/11) * $scope.listSelectedRowsParts[a].Qty; // akhirnya di round lagi dong
                        $scope.subTotalParts += ASPricingEngine.calculate({
                                                    InputPrice: $scope.listSelectedRowsParts[a].Price,
                                                    // Discount: 5,
                                                    Qty: $scope.listSelectedRowsParts[a].Qty,
                                                    Tipe: 102, 
                                                    PPNPercentage: PPNPerc,
                                                }); 


                        // $scope.discountFinalParts +=  Math.round(Math.round($scope.listSelectedRowsParts[a].Price / 1.1) * $scope.listSelectedRowsParts[a].Discount/100)*$scope.listSelectedRowsParts[a].Qty;
                        $scope.discountFinalParts += ASPricingEngine.calculate({
                                                        InputPrice: $scope.listSelectedRowsParts[a].Price,
                                                        Discount: discParts,
                                                        Qty: $scope.listSelectedRowsParts[a].Qty,
                                                        Tipe: 112, 
                                                        PPNPercentage: PPNPerc,
                                                    }); 
                    }
                } else if ($scope.listSelectedRowsParts[a].PaidById == 30 ) { // 30: Warranty
                    if ($scope.getTypeJobFreeClaim > 0) {
                        if (discParts > 0) {
                            // $scope.discountFinalParts +=  Math.round(Math.round($scope.listSelectedRowsParts[a].Price / 1.1) * $scope.listSelectedRowsParts[a].Discount/100)*$scope.listSelectedRowsParts[a].Qty;
                            $scope.discountFinalParts += ASPricingEngine.calculate({
                                                            InputPrice: $scope.listSelectedRowsParts[a].Price,
                                                            Discount: discParts,
                                                            Qty: $scope.listSelectedRowsParts[a].Qty,
                                                            Tipe: 112, 
                                                            PPNPercentage: PPNPerc,
                                                        }); 
                        }
                       
                        // $scope.subTotalParts += Math.round($scope.listSelectedRowsParts[a].Price *10/11) * $scope.listSelectedRowsParts[a].Qty; // akhirnya di round lagi dong
                        $scope.subTotalParts += ASPricingEngine.calculate({
                                                    InputPrice: $scope.listSelectedRowsParts[a].Price,
                                                    // Discount: 5,
                                                    Qty: $scope.listSelectedRowsParts[a].Qty,
                                                    Tipe: 102, 
                                                    // Tipe: 124, 
                                                    PPNPercentage: PPNPerc,
                                                });
    
    
                    }else{  
                        // To Avoid double PPN
                        // $scope.subTotalParts += Math.round($scope.listSelectedRowsParts[a].Price / 1.1) * $scope.listSelectedRowsParts[a].Qty; // kata pa cien ganti rumusnya
                        // $scope.subTotalParts += ($scope.listSelectedRowsParts[a].Price * $scope.listSelectedRowsParts[a].Qty) / 1.1; // katanya biar sama kek towas ga di bagi 1.1
                        //untuk double ppn
                        $scope.subTotalParts += ($scope.listSelectedRowsParts[a].Price * $scope.listSelectedRowsParts[a].Qty);

                        $scope.discountFinalParts += 0;
                    }
                    
                    
                    
                } 
                // else if($scope.listSelectedRowsParts[a].PaidById == 2277){ //tambahan by pak eko
                //     $scope.subTotalParts += 0;
                //     $scope.discountFinalParts += 0;
                // }
                else {
                    if (discParts > 0) {
                        // $scope.discountFinalParts +=  Math.round(Math.round($scope.listSelectedRowsParts[a].Price / 1.1) * $scope.listSelectedRowsParts[a].Discount/100)*$scope.listSelectedRowsParts[a].Qty;
                        $scope.discountFinalParts += ASPricingEngine.calculate({
                                                        InputPrice: $scope.listSelectedRowsParts[a].Price,
                                                        Discount: discParts,
                                                        Qty: $scope.listSelectedRowsParts[a].Qty,
                                                        Tipe: 112, 
                                                        PPNPercentage: PPNPerc,
                                                    }); 
                    }
                   
                    // $scope.subTotalParts += Math.round($scope.listSelectedRowsParts[a].Price *10/11) * $scope.listSelectedRowsParts[a].Qty; // akhirnya di round lagi dong
                    $scope.subTotalParts += ASPricingEngine.calculate({
                                                InputPrice: $scope.listSelectedRowsParts[a].Price,
                                                // Discount: 5,
                                                Qty: $scope.listSelectedRowsParts[a].Qty,
                                                Tipe: 102, 
                                                PPNPercentage: PPNPerc,
                                            });


                }

                // if ($scope.listSelectedRowsParts[a].PaidById != 30) { // 30: Warranty
                //     $scope.subTotalParts += Math.round($scope.listSelectedRowsParts[a].Price / 1.1) * $scope.listSelectedRowsParts[a].Qty;
                // }

                // $scope.DP += $scope.listSelectedRowsParts[a].DownPayment;
                if ($scope.mBilling.dataPayment == 28) {
                    $scope.DP = $scope.dataByJobId[0].FinalDP; // di ubah req dari raflizal dan pa dodi 17September2020
                    $scope.DPPaid = $scope.dataByJobId[0].DPPaidAmount; // di ubah req dari raflizal dan pa dodi 17September2020
                } else {
                    $scope.DP = 0; // req pa dodi 21 September 2020, jika biling bukan charge to customer, dp dan dppaid nya di 0 in
                    $scope.DPPaid = 0; // req pa dodi 21 September 2020, jika biling bukan charge to customer, dp dan dppaid nya di 0 in
                }
                console.log('$scope.DP', $scope.DP);
            }

            
        
            // ================================================================ Rounding CR4 Parts ======================================================================            



            var TotalFinal = $scope.subTotalOpb + $scope.subTotalParts + $scope.subTotalLabor + $scope.subTotalOpl;
            $scope.TotalFinal = TotalFinal;


            var TotalAfterDiscount = TotalFinal - ($scope.discountFinalTask + $scope.discountFinalParts + $scope.discountFinalPartsOpb + $scope.discountFinalOpl);
            $scope.TotalAfterDiscountFinal = TotalAfterDiscount;

            // $scope.PPN = Math.floor($scope.TotalAfterDiscountFinal * 0.1);
            $scope.PPN = ASPricingEngine.calculate({
                            InputPrice: (($scope.TotalAfterDiscountFinal) * (1+PPNPerc/100)),
                            // Discount: 0,
                            // Qty: 0,
                            Tipe: 33, 
                            PPNPercentage: PPNPerc,
                        });  
            $scope.TotalInvoice = $scope.TotalAfterDiscountFinal + $scope.PPN;
            // $scope.TotalYangHarusDibayar = $scope.TotalInvoice - $scope.DP;
            $scope.TotalYangHarusDibayar = $scope.TotalInvoice - $scope.DPPaid; // di ubah req dari raflizal dan pa dodi 17September2020


            $scope.finalAlldiscount = $scope.discountFinalTask + 0 + $scope.discountFinalParts + $scope.discountFinalPartsOpb + $scope.discountFinalOpl;

            if (TotalAfterDiscount <= 250000) {
                $scope.Materai = 0;
            } else if (TotalAfterDiscount > 250000 && TotalAfterDiscount <= 1000000) {
                $scope.Materai = 0;
            } else {
                $scope.Materai = 0;
            }

            var adaPembayaranBerbeda = 0;
            if ($scope.listSelectedRowsParts != null && $scope.listSelectedRowsParts != undefined && $scope.listSelectedRowsParts.length > 0) {
                for (var i = 0; i < $scope.listSelectedRowsParts.length; i++) {
                    if ($scope.listSelectedRowsParts[i].PaidById != tmp_mBillingDataPayment) {
                        adaPembayaranBerbeda++;
                    }
                }
            }

            if (adaPembayaranBerbeda > 0) {

                if ($scope.dataByJobId[0].DPPaidAmount > 0) {
                    bsAlert.alert({
                            title: 'Terdapat perubahan tipe pembayaran atas parts yang memiliki DP (Down Payment) <br/> Jika proses Billing tetap dilanjutkan maka <span style="color:red">nominal DP (Down Payment) tidak akan terpotong</span>',
                            text: "",
                            type: "warning"
                        },
                        function() {},
                        function() {}
                    )
                } else {
                    bsAlert.alert({
                            title: "Terdapat Tipe Pembayaran Yang Berbeda",
                            text: "",
                            type: "warning"
                        },
                        function() {},
                        function() {}
                    )
                }
                
            }

            console.log("form controller=>", rows);

            $timeout(function() {
                // ini di buat karena ada bentrok antara maskme dan bslist sehingga TIN suka berubah jadi null ketika click select all di bslist
                if ($scope.mBilling.TIN === null){
                    if ($scope.mBilling.TINBackup !== null && $scope.mBilling.TINBackup !== undefined){
                        $scope.mBilling.TIN = angular.copy($scope.mBilling.TINBackup)
                    }
                }
            }, 200);
        }


        //grid opl

        $scope.listButtonSettings = {
            new: { enable: false, icon: "fa fa-fw fa-car" },
            delete: { enable: false },
            edit: { enable: false },
            view: { enable: false }
            // select: { enable: false },
            // selectall: { enable: false }
        };

        // var gridTemp=[
        //     {
        //         TaskId : 1,
        //         TaskName : 'Ganti Oli',
        //         FR : '1243',
        //         child:[{
        //             Parts :{
        //             PartsId : 1,
        //             TaskId : 1,
        //             PartsCode : 'ASDF-QWED',
        //             PartsName : 'Tempat Duduk'
        //             }
        //         }]
        //     }
        // ];

        // $scope.oplData = [
        //     {
        //         oplId : 1,
        //         OplName : 'jajaj',
        //         VendorName : 'WOW',
        //         RequiredDate : '2017-14-02',
        //         Price : '12000'
        //     }
        // ];
        $scope.lmModelOpl = {};


        // $scope.partsData = [
        //     {
        //         JobPartsId : 1,
        //         KodeParts : 'ABSCD-BFDG',
        //         NamaParts : 'Filter Oli',
        //         QTY : 2,
        //         HargaSatuan : 12000,
        //         SubTotal : 900000,
        //     }
        // ];
        $scope.lmModelParts = {};


        // $scope.opbData = [{
        //     opbId: 1,
        //     OplName: 'jajaj',
        //     VendorName: 'WOW',
        //     RequiredDate: '2017-14-02',
        //     Price: '12000'
        // }];
        $scope.lmModelOpb = {};

        $scope.backupTINx = function(data) {
            $scope.mBilling.TINBackup = angular.copy(data)
        }
		
		$scope.onChangeCB = function(data) {
			console.log("onChangeCB : ",data);

			if(data == 0){
				//unchecked to checked
				$scope.showNPWP = false;
				$scope.showPassport = false;
				$scope.showKTP = true;

				//only customer 28
				/*if($scope.mBilling.dataPayment == 28){
					$scope.mBilling.KTP = $scope.mBilling.KTPState;
				}*/
			}else{
				//checked to unchecked
				$scope.showNPWP = true;
				$scope.showPassport = false;
				$scope.showKTP = false;

				//only customer 28
				/*if($scope.mBilling.dataPayment == 28){
					$scope.mBilling.TIN = $scope.mBilling.TINState;
				}*/
			}			
		}
		
		$scope.onChangeNatView = function(data) {
			console.log("onChangeNatView : ", data);

			/*if(data == 0){
				delete $scope.mBilling.Nationality;

				$scope.mBilling.Nationality = undefined;
			}else*/ if(data == 0){
				$scope.mBilling.Nationality = { Id: 0, Name: 'WNI' };

				//if($scope.mBilling.TIN == "00.000.000.0-000.000"){
				if($scope.isKTP == 0){
					// WNI -> Show NPWP
					$scope.showNPWP = true;
					$scope.showPassport = false;
					$scope.showKTP = false;

					$scope.mBilling.isNPWP = 0;
					/*if($scope.mBilling.KTP == null || $scope.mBilling.KTP == "" || $scope.mBilling.KTP == undefined || $scope.mBilling.KTP == '0000000000000000'){
						//WNI -> Show NPWP
						$scope.showNPWP = true;
						$scope.showPassport = false;
						$scope.showKTP = false;

						$scope.mBilling.isNPWP = 0;
					}else{
						//WNI -> Show KTP
						$scope.showNPWP = false;
						$scope.showPassport = false;
						$scope.showKTP = true;

						$scope.mBilling.isNPWP = 1;
					}*/				
				}else if($scope.isKTP == 1){
					// WNI -> Show NPWP
					/*$scope.showNPWP = true;
					$scope.showPassport = false;
					$scope.showKTP = false;

					$scope.mBilling.isNPWP = 0;*/

					// WNI -> Show KTP
					$scope.showNPWP = false;
					$scope.showPassport = false;
					$scope.showKTP = true;

					$scope.mBilling.isNPWP = 1;
				}

				/*if($scope.mBilling.TIN == null || $scope.mBilling.TIN == "" || $scope.mBilling.TIN == '00.000.000.0-000.000' || $scope.mBilling.TIN == undefined){
					//WNI -> Tidak punya NPWP

					$scope.showNPWP = false;
					$scope.showPassport = false;
					$scope.showKTP = true;

					$scope.mBilling.isNPWP = 1;

					if($scope.mBilling.KTP == null || $scope.mBilling.KTP == "" || $scope.mBilling.KTP == '00.000.000.0-000.000' || $scope.mBilling.KTP == undefined){
						$scope.showNPWP = true;
						$scope.showPassport = false;
						$scope.showKTP = false;

						$scope.mBilling.isNPWP = 0;
					}
				}else{
					//WNI -> NPWP

					$scope.showNPWP = true;
					$scope.showPassport = false;
					$scope.showKTP = false;

					$scope.mBilling.isNPWP = 0;
				}*/ //Ver 1
			}else if(data == 1){
				$scope.mBilling.Nationality = { Id: 1, Name: 'WNA' };

				$scope.showNPWP = false;
				$scope.showPassport = true;
				$scope.showKTP = false;

				$scope.mBilling.isNPWP = 1;		
			}
		}
		
		$scope.onChangeNat = function(data) {
            console.log("onChangeNat : ", data);

			if(data == 0){
				//enable checkbox
				console.log("enable checkbox default");

				$timeout(function () {
					$('.checkBoxKTP').find('*').attr('ng-disabled', false).attr('skip-ensable', false).removeAttr('disabled', 'disabled');
					// $('#rowCustDisableCompleted').find('*').attr('skip-enable', true).attr('disabled', 'disabled').removeAttr('skip-disable');
				}, 100);

				$scope.mBilling.Nationality = { Id: 0, Name: 'WNI' };

				$scope.showNPWP = true;
				$scope.showPassport = false;
				$scope.showKTP = false;

				$scope.mBilling.isNPWP = 0;

				$scope.mBilling.TIN = $scope.mBilling.TINState;

				/*if($scope.mBilling.TIN == null || $scope.mBilling.TIN == '000000000000000' || $scope.mBilling.TIN == undefined){
					//WNI -> Tidak punya NPWP

					$scope.showNPWP = false;
					$scope.showPassport = false;
					$scope.showKTP = true;

					$scope.mBilling.isNPWP = 1;
				}else{
					//WNI -> NPWP

					$scope.showNPWP = true;
					$scope.showPassport = false;
					$scope.showKTP = false;

					$scope.mBilling.isNPWP = 0;
				}*/
			}else if(data == 1){
				//disable checkbox
				console.log("disable checkbox WNA");
				$scope.showNPWP = false;
				$scope.showPassport = true;
				$scope.showKTP = false;

				$scope.mBilling.isNPWP = 1;

				$scope.mBilling.Nationality = { Id: 1, Name: 'WNA' };
				$scope.mBilling.passport = $scope.mBilling.passportState;

                $timeout(function () {
                    $('.checkBoxKTP').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
                }, 100);
			}
        }
		
		$scope.allowPattern = function(event, type, item) {
            var patternRegex
            if (type == 1) {
                patternRegex = /\d/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /\d|[a-z]|[*]/i; //ALPHANUMERIC ONLY
            };
            console.log("event", event);
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            };
		};

        $scope.ubahDateToString = function(val) {
            var pecahData = val.toString().split(' ');
            // console.log('pecah data', pecahData);
            // Jan:0, Feb:0, Mar:0, Apr:0, May:0, Jun:0, Jul:0, Aug:0, Sep:0, Oct:0, Nov:0, Dec:0
            if (pecahData[1] == 'Jan'){
                pecahData[1] = '01'
            } else if (pecahData[1] == 'Feb'){
                pecahData[1] = '02'
            } else if (pecahData[1] == 'Mar'){
                pecahData[1] = '03'
            } else if (pecahData[1] == 'Apr'){
                pecahData[1] = '04'
            } else if (pecahData[1] == 'May'){
                pecahData[1] = '05'
            } else if (pecahData[1] == 'Jun'){
                pecahData[1] = '06'
            } else if (pecahData[1] == 'Jul'){
                pecahData[1] = '07'
            } else if (pecahData[1] == 'Aug'){
                pecahData[1] = '08'
            } else if (pecahData[1] == 'Sep'){
                pecahData[1] = '09'
            } else if (pecahData[1] == 'Oct'){
                pecahData[1] = '10'
            } else if (pecahData[1] == 'Nov'){
                pecahData[1] = '11'
            } else if (pecahData[1] == 'Dec'){
                pecahData[1] = '12'
            } else {
                pecahData[1] = '01'
            }

            var hasilFormat = pecahData[2] + '/' + pecahData[1] + '/' + pecahData[3]
            return hasilFormat;
        }

         //-----------KeyPress Start-------------
         $scope.PatternMessage = function(event) {
            console.log("event", event);
            // patternRegex = /\d|[a-z]|\s/i; //ALPHANUMERIC ONLY
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            // if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
            //     event.preventDefault();
            //     return false;
            // }
        };
        //-----------KeyPress End---------------

    });
