angular.module('app')
    .factory('BSTK', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getCategoryWO: function() {
                return $http.get('/api/as/GlobalMaster?CategoryId=1010')
            },
            getCategoryRepair: function() {
                return $http.get('/api/as/GlobalMaster?CategoryId=1012')
            },
            getDataWAC: function() {
                return $http.get('/api/as/GroupWACsOnly');
            },
            getItemWAC: function(groupId) {
                return $http.get('/api/as/ItemWACs/GetByGroupId/' + groupId);
            },
            getPayment: function() {
                var catId = 1008;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getUnitMeasurement: function() {
                var catId = 1;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getTaskCategory: function() {
                var catId = 1010;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getDetailCustomer: function(arr) {
                var res = $http.get('/api/as/MRS/' + arr);
                // api/as/MRS/{MrsId}/
                return res;
            },
            createWOBP: function(mData, mDataCrm, mDataDM, JobRequest, JobComplaint, JobList, gridEstTime) {
                return $http.post('/api/as/Jobs/updateDetil/2', [{
                    JobId: 0,
                    TechnicianAction: 1,
                    JobSuggest: 1,
                    // Status: 0,
                    JobTask: JobList,
                    // isGr: 1,
                    Km: mData.Km,
                    isCash: mData.isCash,
                    isSpk: mData.isSpk,
                    // IsEstimation: 0,
                    EstimationDate: mData.EstimateDeliveryTime,
                    JobDate: new Date(),
                    // KatashikiCode: 'ASV50R-JETGKD',
                    KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    JobComplaint: JobbComplaint,
                    JobRequest: JobRequest,
                    DecisionMaker: mDataDM.PK.Name,
                    PhoneDecisionMaker1: mDataDM.PK.HP1,
                    PhoneDecisionMaker2: mDataDM.PK.HP2,
                    ContactPerson: mDataDM.CP.Name,
                    PhoneContactPerson1: mDataDM.CP.HP1,
                    PhoneContactPerson2: mDataDM.CP.HP2,
                    UsedPart: mData.flag.parts,
                    IsWaiting: mData.flag.wait,
                    IsWash: mData.flag.wash
                }]);
            },
            releaseWOBP: function(mData, mDataCrm, mDataDM, JobRequest, JobComplaint, JobList, gridEstTime) {
                return $http.post('/api/as/Jobs/updateDetil/2?createNo=1', [{
                    JobId: 0,
                    TechnicianAction: 1,
                    JobSuggest: 1,
                    // Status: 0,
                    JobTask: JobList,
                    // isGr: 1,
                    Km: mData.Km,
                    isCash: mData.isCash,
                    isSpk: mData.isSpk,
                    // IsEstimation: 0,
                    EstimationDate: mData.EstimateDeliveryTime,
                    JobDate: new Date(),
                    // KatashikiCode: 'ASV50R-JETGKD',
                    KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    JobComplaint: JobbComplaint,
                    JobRequest: JobRequest,
                    DecisionMaker: mDataDM.PK.Name,
                    PhoneDecisionMaker1: mDataDM.PK.HP1,
                    PhoneDecisionMaker2: mDataDM.PK.HP2,
                    ContactPerson: mDataDM.CP.Name,
                    PhoneContactPerson1: mDataDM.CP.HP1,
                    PhoneContactPerson2: mDataDM.CP.HP2,
                    UsedPart: mData.flag.parts,
                    IsWaiting: mData.flag.wait,
                    IsWash: mData.flag.wash
                }])
            },
            createWACBPbyJobId: function(data) {
                // var WAC = function
                return $http.post('/api/as/JobWACBPs/NewList', data);
            },
            createWOBPEstimation: function(jobId, dataEst) {
                return $http.put('/api/as/jobs/BpEstimation/' + jobId, [{
                    JobTasks: [],
                    JobTaskBP: dataEst
                }])
            },
            getVehicleList: function(param1, param2) {
                return $http.get('/api/crm/GetVehicleListSpecific/' + param1 + '/' + param2);
            },
            getVehicleMList: function() {
                return $http.get('/api/crm/GetCVehicleModel/');
            },
            getVehicleTList: function() {
                return $http.get('/api/crm/GetCVehicleType/');
            },
            createNewVehicleList: function(data) {
                console.log('data createNewVehicleList', data);
                return $http.post('/api/crm/PostVehicleList/', [{
                    VIN: data.VIN,
                    LicensePlate: data.LicensePlate,
                    EngineNo: data.EngineNo,
                    AssemblyYear: data.AssemblyYear,
                    DECDate: data.DECDate,
                    VehicleTypeColorId: 1,
                    STNKName: data.STNKName,
                    STNKAddress: data.STNKAddress,
                    STNKDate: data.STNKDate
                        // "StatusCode": 1,

                }]);
            },
            updateVehicleList: function(data) {
                console.log('data updateVehicleList', data);
                return $http.put('/api/crm/PutVehicleList/', [{
                    VehicleId: data.VehicleId,
                    VIN: data.VIN,
                    LicensePlate: data.LicensePlate,
                    EngineNo: data.EngineNo,
                    AssemblyYear: data.AssemblyYear,
                    DECDate: data.DECDate,
                    VehicleTypeColorId: 1,
                    STNKName: data.STNKName,
                    STNKAddress: data.STNKAddress,
                    STNKDate: data.STNKDate
                        // "StatusCode": 1,

                }]);
            },
            getCustomerList: function(param) {
                return $http.get('/api/crm/GetCCustomerListSpecific/' + param + '/-/-/-/-/-/-');
            },
            getCustomerVehicleList: function(value) {
                var param1 = value.TID ? value.TID : '-';
                var param2 = '-';
                var param3 = '-';
                var param4 = '-';
                var param5 = '-';
                var param6 = '-';
                var param7 = '-';
                switch (value.flag) {
                    case 1:
                        param2 = value.filterValue ? value.filterValue : '-';
                        break;
                    case 2:
                        param3 = value.filterValue ? value.filterValue : '-';;
                        break;
                    case 3:
                        param4 = value.filterValue ? value.filterValue : '-';;
                        break;
                    case 4:
                        param5 = value.filterValue ? value.filterValue : '-';;
                        break;
                    case 5:
                        param6 = value.filterValue ? value.filterValue : '-';;
                        break;
                    case 6:
                        param7 = value.filterValue ? value.filterValue : '-';;

                };

                return $http.get('/api/crm/GetCustomerListFilter/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7);
            },
            createNewCustomerList: function(data) {
                return $http.post('/api/crm/PostCustomerList/', [{
                    // ToyotaId: data.Customer.ToyotaId, //Masi Pakai Dummy Math.Random
                    Npwp: data.Customer.Npwp,
                    CustomerTypeId: data.Customer.CustomerTypeId,
                    FleetId: 1, //Foreign Key, Masih Dummy
                    StatusCode: 1, //Status Create New
                }]);
            },
            createNewCustomerListPersonal: function(id, data) {
                return $http.post('/api/crm/PostCustomerListPersonal/', [{
                    CustomerId: id,
                    CustomerName: data.Customer.Name,
                    KTPKITAS: data.Customer.KTPKITAS,
                    // BirthPlace: 'Dummy', //Not Used in WOBP
                    BirthDate: data.Customer.BirthDate,
                    Handphone1: data.Customer.HandPhone1,
                    Handphone2: data.Customer.HandPhone2,
                    StatusCode: 1
                }]);
            },
            createNewCustomerListAddress: function(id, data) {
                return $http.post('/api/crm/PostCCustomerAddress/', data);
            },
            updateCustomerListAddress: function(Aid, Cid, data) {
                return $http.put('/api/crm/PutCCustomerAddress/', data);
            },
            updateCustomerList: function(id, cTypeId, data) {
                return $http.put('/api/crm/PutCustomerList/', [{
                    CustomerId: id,
                    // OutletId: 655,
                    ToyotaId: data.Customer.ToyotaId,
                    Npwp: data.Customer.Npwp,
                    CustomerTypeId: cTypeId,
                    // FleetId: 1,
                    StatusCode: 1
                }]);
            },
            
            updateCustomerListPersonal: function(Pid, Cid, data) {
                return $http.put('/api/crm/PutCListPersonal', [{
                    PersonalId: Pid,
                    CustomerId: Cid,
                    CustomerName: data.Customer.Name,
                    KTPKITAS: data.Customer.KTPKITAS,
                    // BirthPlace: 'Dummy', //Not Used in WOBP
                    BirthDate: data.Customer.BirthDate,
                    Handphone1: data.Customer.HandPhone1,
                    Handphone2: data.Customer.HandPhone2,
                    StatusCode: 1

                }]);
            },
            updateCustomerListUser: function(data) {
                return $http.put('/api/crm/PutVehicleUser/', [{
                    VehicleUserId: data.VehicleUserId,
                    CustomerVehicleId: data.CustomerVehicleId,
                    Name: data.Name,
                    Phone: data.Phone,
                    StatusCode: 1
                }]);
            },
            createNewCustomerListUser: function(data) {
                return $http.post('/api/crm/PostVehicleUser/', [{
                    CustomerVehicleId: data.CustomerVehicleId,
                    Name: data.Name,
                    Phone: data.Phone,
                    StatusCode: 1
                }]);
            },
            getOwnerUser: function() {
                return $http.get('');
            },
            getDataWOBP: function() {
                return $http.get('');
            },
            setBSTK: function(mData, mDataCrm, mDataDM, gridEstTime) {
                // return $http.post('/api/as/jobs/setBSTK', [{
                //     //     JobId: 0,
                //     //     TechnicianAction: 1,
                //     //     JobSuggest: 1,
                //     //     // Status: 0,
                //     //     // JobTask: JobList,
                //     //     // isGr: 1,
                //     //     Km: mData.Km,
                //     //     isCash: mData.isCash,
                //     //     isSpk: mData.isSpk,
                //     //     // IsEstimation: 0,
                //     //     EstimationDate: mData.EstimateDeliveryTime,
                //     //     JobDate: new Date(),
                //     //     // KatashikiCode: 'ASV50R-JETGKD',
                //     //     KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                //     //     // JobComplaint: JobbComplaint,
                //     //     // JobRequest: JobRequest,
                //     //     // DecisionMaker: mDataDM.PK.Name,
                //     //     // PhoneDecisionMaker1: mDataDM.PK.HP1,
                //     //     // PhoneDecisionMaker2: mDataDM.PK.HP2,
                //     //     // ContactPerson: mDataDM.CP.Name,
                //     //     // PhoneContactPerson1: mDataDM.CP.HP1,
                //     //     // PhoneContactPerson2: mDataDM.CP.HP2,
                //     //     UsedPart: mData.flag.parts,
                //     //     IsWaiting: mData.flag.wait,
                //     //     IsWash: mData.flag.wash
                //     // },
                //     // {
                //     JobId: 0,
                //     JobNo: 0,
                //     OutletId: 0,
                //     CalId: 1,
                //     StallId: 1,
                //     VehicleId: 4,
                //     // PlanStart: 00:00:00, 
                //     // PlanFinish: 00:00:00,
                //     TechnicianAction: 1,
                //     JobSuggest: 1,
                //     T1: 1,
                //     T2: 1,
                //     RSA: 1,
                //     AdditionalTime: 1,
                //     JobType: 0,
                //     Status: 0,
                //     // AppointmentDate: 2017-03-21T00:00:00,
                //     // AppointmentTime: 12:00:00,
                //     AppointmentNo: null,
                //     isAppointment: 0,
                //     AppointmentVia: 0,
                //     isGr: 0,
                //     Km: mData.Km,
                //     isCash: mData.isCash,
                //     isSpk: mData.isSpk,
                //     // SpkNo: -,
                //     // InsuranceName: -,
                //     isWOBase: 1,
                //     WoNo: 0,
                //     WoCategoryId: null,
                //     // IsEstimation: 0,
                //     EstimationDate: mData.EstimateDeliveryTime,
                //     JobDate: new Date(),
                //     // EstimationNo: ,
                //     // Suggestion: suges,
                //     PlanDateStart: 2017 - 04 - 01,
                //     PlanDateFinish: 2017 - 04 - 01
                // }])
            }
        };
    });