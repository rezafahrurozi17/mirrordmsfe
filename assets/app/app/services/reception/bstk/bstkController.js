angular.module('app')
    .directive('pitchCanvas', function() {

        function link(scope, element, attrs) {

            console.info('element: ', element);
            console.info('attrs: ', attrs);
            var context = element[0].getContext('2d');
            var background = new Image();
            background.src = "images/Vehicle_Body.jpg";
            console.info('context: ', element[0].getContext('2d'));

            background.onload = function() {
                context.drawImage(background, 0, 0);
            };
            console.log("scope=", scope);
            var drawNewValue = null;
            scope.$watch('data', function(newValue, oldValue) {
                console.log('newValue', newValue);
                console.log('oldValue', oldValue);
                drawNewValue = newValue;
            });
            var stats = null;
            scope.$watch('status', function(newValue, oldValue) {
                stats = newValue;
            });

            if (stats != null) {
                console.log('masuk sini');
                context.beginPath();
                context.arc(drawNewValue[0].x, drawNewValue[0].y, 25, 0, 2 * Math.PI, false);
                if (stats == 1) {
                    context.fillStyle = "#ccddff";
                } else if (stats == 2) {
                    context.fillStyle = "red";
                } else if (stats == 3) {
                    context.fillStyle = "black";
                }
                context.fill();
                context.stroke();
            }
            // function draw(data, param) {
            //     for (var i = 0; i < data.length; i++) {
            //         console.log('data draw', data[i]);
            //         drawDot(data[i], param);
            //     }
            // }

            // function drawDot(data, param) {
            //     console.log('data.x', data.x);
            //     console.log('data.y', data.y);
            //     context.beginPath();
            //     context.arc(data.x, data.y, 5, 0, 2 * Math.PI, false);
            //     if (param == 1) {
            //         context.fillStyle = "#ccddff";
            //     } else if (param == 2) {
            //         context.fillStyle = "red";
            //     } else if (param == 3) {
            //         context.fillStyle = "black";
            //     }
            //     // context.fillStyle = "#ccddff";
            //     context.fill();
            //     context.lineWidth = 1;
            //     context.strokeStyle = "#666666";
            //     context.stroke();
            // }
        };

        return {
            restrict: 'AE',
            replace: true,
            // scope: true,
            link: link,
            template: '<canvas id="myCanvas" width="885" height = "1075" style="border: 1px gray solid; float: center" ng-click="$parent.openPopup($event)"></canvas>',
            scope: {
                data: '=',
                status: '='
            }
        }
    })
    .controller('BSTKController', function($scope, $http, $q, $interval, PrintRpt, GeneralParameter, WoHistoryGR, $timeout, RepairProcessGR, AppointmentBpService, FollowupService, CurrentUser, CMaster, WOBP, bsNotify, WO, uiGridConstants, AppointmentGrService, AppointmentBpService, ngDialog, bsAlert, RepairSupportActivityGR, GeneralMaster, warranty, BSTK) {
        //On Page Loading------------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.getAllImpData();
            // $scope.getDetailCust();
            // $scope.wobpmenu();
            $scope.getVehicleMnT();
            // $scope.getAddressList();
        });

        $scope.wobpmenu = function() {
            $scope.wobpMode = 'new';
            $scope.disPng = false;
            $scope.disPnj = false;
        };

        $scope.mData = [];
        $scope.mData.US = [];
        // $scope.mData.US.name = [];
        // $scope.mData.US.phoneNumber1 = [];
        // $scope.mData.US.phoneNumber2 = [];
        $scope.mDataCrm = [];
        $scope.isWashing = '';
        $scope.isWaiting = '';
        $scope.isParts = '';
        $scope.reqModel = {};
        $scope.lmComplaint = {};
        $scope.lmRequest = {};
        $scope.lmModel = {};
        $scope.lmModelAddress = {};
        $scope.ldModel = {};
        $scope.modeQueue = 1;
        $scope.show_modal = { show: false };
        $scope.isCenter = true;
        $scope.listEstimation = { show: false };
        $scope.gridOther = [];
        $scope.JobId = null;
        $scope.isJobID = true;
        // $scope.gridOther.data = {
        //     JobWacItemId: 28,
        //     ItemStatus: ''
        // };
        // $scope.pageMode = 'view';
        $scope.VehicGas = [{
            VehicleGasTypeId: 1,
            VehicleGasTypeName: 'Bensin'
        }, {
            VehicleGasTypeId: 2,
            VehicleGasTypeName: 'Diesel'
        }, {
            VehicleGasTypeId: 3,
            VehicleGasTypeName: 'Hybrid'
        }];
        //----------------------------------------------------15
        //SA Main Menu----------------------------------------
        //----------------------------------------------------16
        $scope.actionButtonSettings = [{
            title: 'Queue Taking',
            icon: 'fa fa-fw fa-list',
            func: function(formScope) {
                console.log("func view=>", $scope, formScope);
                console.log("api=>", $scope.formApi);
                gridTemp = []
                $scope.JobComplaint = [];
                $scope.JobRequest = [];
                $scope.gridWork = gridTemp;
                $scope.modeQueue = 2;
                $scope.gridQueue();
                $scope.getCounter();
                $scope.formApi.setMode('detail');
            }
        }];
        $scope.actionButtonSettingsDetail = [{
            title: 'Back',
            icon: '',
            func: function() {
                $scope.modeQueue = 0;
                $scope.pageMode = 'view';
                // $scope.isCenter = false;
                $scope.formApi.setMode('grid');
            }
        }];
        $scope.dataChips = []
        var gridData = [];
        var arrWap = [];
        var arrIp = [];
        var arrWt = [];
        var arrDeliv = [];
        var dataCounter
        $scope.getDataWOBPSA = function() {
            WO.getDataWOSAList(0).then(function(resu) {
                console.log('data getdatawosalist', resu.data.Result);
                var result = angular.copy(resu.data.Result);

                _.forEach(result, function(e) {
                    // console.log('eeeee >>', e);
                    if (e.Status == 4) {
                        arrWap.push(e);
                    } else if (e.Status >= 5 && e.Status <= 12) {
                        arrIp.push(e);
                    } else if (e.Status == 13) {
                        arrWt.push(e);
                    } else if (e.Status >= 14 && e.Status <= 17) {
                        arrDeliv.push(e);
                    };
                });

                $scope.dataChips.wap = arrWap;
                $scope.dataChips.ip = arrIp;
                $scope.dataChips.wt = arrWt;
                $scope.dataChips.deliv = arrDeliv;
            });
        };

        $scope.getData = function() {
            $scope.dataChips = [];
            gridData = [];
            arrWap = [];
            arrIp = [];
            arrWt = [];
            arrDeliv = [];
            console.log("getData=>");
            $scope.loading = true;
            $scope.getDataWOBPSA();
            return $q.resolve(
                AppointmentGrService.getPayment().then(function(res) {
                    $scope.paymentData = res.data.Result;
                }),
                AppointmentGrService.getUnitMeasurement().then(function(res) {
                    $scope.unitData = res.data.Result;
                }),
                AppointmentGrService.getWoCategory().then(function(res) {
                    $scope.woCategory = res.data.Result;
                }),
                AppointmentGrService.getTaskCategory().then(function(res) {
                    $scope.taskCategory = res.data.Result;
                })

                // $scope.getDataWOSA()
            );
        };

        //----------------------------------------------------1
        //-----CHIPS Details----------------------------------

        $scope.detailChip = function(item) {
            console.log("itemyg dipilih", item);
            console.log("item Status", item.Status);
            AppointmentGrService.getWobyJobId(item.JobId).then(function(res) {
                var tmpData = res.data.Result[0];
                console.log("tmpData", tmpData);
                $scope.mData = tmpData
                WO.getDataJobInspection($scope.mData.JobId).then(function(res) {
                    console.log("ressWO", res);
                    $scope.partBekas = res.data.OldPartKeepPlace;
                });
                $scope.JobRequest = tmpData.JobRequest;
                $scope.JobComplaint = tmpData.JobComplaint;
                $scope.dataForBslist(tmpData);

            });
            if (item.Status == 13) {
                $scope.modeQueue = 13
            } else if (item.Status >= 14 && item.Status <= 17) {
                $scope.modeQueue = 16
            }
            AppointmentGrService.getWoCategory().then(function(res) {
                $scope.woCategory = res.data.Result;
            });
            $scope.formApi.setMode('detail');

        }
        $scope.callingCustomer = function(item) {
            $scope.show_modal.show = !$scope.show_modal.show;
            $scope.modal_model = angular.copy(item);
        }
        $scope.saveCustomer = function(item) {
            console.log('itemini', item);
            $scope.isService = true
            console.log("isService", $scope.isService);
            $scope.show_modal.show = !$scope.show_modal.show;
        }
        $scope.cancelCustomer = function(item) {
            console.log('itemcancel', item);
        }
        $scope.backService = function() {
            $scope.isService = false;
        }

        $scope.dataForBslist = function(data) {
            gridTemp = []
            if (data.JobTask.length > 0) {
                var tmpJob = data.JobTask;
                for (var i = 0; i < tmpJob.length; i++) {
                    console.log("tmpJob", tmpJob);
                    var Parts = []
                    for (var j = 0; j < tmpJob[i].JobParts.length; j++) {
                        Parts.push({
                            Parts: tmpJob[i].JobParts[j],
                            Qty: tmpJob[i].JobParts[j].Qty
                        });
                    }
                    gridTemp.push({
                        ActualRate: tmpJob[i].ActualRate,
                        JobTaskId: tmpJob[i].JobTaskId,
                        JobTypeId: tmpJob[i].JobTypeId,
                        Price: tmpJob[i].Price,
                        PaidById: tmpJob[i].PaidById,
                        JobId: tmpJob[i].JobId,
                        TaskId: tmpJob[i].TaskId,
                        TaskName: tmpJob[i].TaskName,
                        FR: tmpJob[i].FR,
                        Fare: tmpJob[i].Fare,
                        ProcessId: tmpJob[i].ProcessId,
                        child: Parts
                    })
                }
                $scope.gridWork = gridTemp;
                $scope.onAfterSave();
                console.log("totalw", $scope.totalWork);
            }
            console.log("$scope.gridWork", $scope.gridWork);
            console.log("$scope.JobRequest", $scope.JobRequest);
            console.log("$scope.JobComplaint", $scope.JobComplaint);
        };
        $scope.completeTeco = function(item) {
            WO.updateTeco(item.JobId).then(function(res) {
                $scope.pageMode = 'view'
                $scope.formApi.setMode('grid');
                $scope.getData();
            });
        };
        $scope.UncompleteTeco = function(item) {
            bsAlert.alert({
                    title: "Apakah Anda yakin akan melakukan UnTeco?",
                    text: item.WoNo,
                    type: "question",
                    showCancelButton: true
                },
                function() {
                    WO.updateStatusForWO(item.JobId, 13).then(function(res) {
                        $scope.actionButtonSettingsDetail[0].visible = true;
                        $scope.pageMode = 'view'
                        $scope.formApi.setMode('grid');
                        $scope.getData();
                    });
                },
                function() {}
            )
        };
        $scope.typeTime = [{ Name: "Pagi" }, { Name: "Siang" }, { Name: "Sore" }];
        $scope.saveService = function(data) {
            if (data.IsJobResultExplainOk != undefined && data.IsSparePartExplainOk != undefined && data.IsConfirmPriceOk !== undefined && data.IsJobSuggestExplainOk != undefined) {
                var a = new Date(data.FollowUpDate);
                var yearFirst = a.getFullYear();
                var monthFirst = a.getMonth() + 1;
                var dayFirst = a.getDate();
                data.FollowUpDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                WO.saveServiceExplanations(data).then(function(res) {
                    console.log("res", res);
                    $scope.isService = false;
                    $scope.pageMode = 'view'
                    $scope.formApi.setMode('grid');
                    $scope.getData();
                });
            } else {
                bsAlert.alert({
                        title: "Form Belum terisi dengan lengkap",
                        text: "Silahkan Cek kembali !",
                        type: "warning"
                    },
                    function() {},
                    function() {}
                )
            }
        };
        var actionCheckIn = '<div class="ui-grid-cell-contents">' +
            '<a href="#" ng-click="grid.appScope.manualCall(row.entity)" uib-tooltip="Call" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-phone" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.removeCall(row.entity)" uib-tooltip="Remove" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-trash-o" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';

        $scope.removeCall = function(item) {
            console.log("item", item);
            bsAlert.alert({
                    title: "Apakah anda yakin ?",
                    text: "",
                    type: "question",
                    showCancelButton: true
                },
                function() {
                    WO.removeCall(item).then(function(res) {
                        $scope.gridQueue();
                    });
                },
                function() {

                }
            )
        }
        $scope.manualCall = function(item) {
            console.log("item", item);
            bsAlert.alert({
                    title: "Apakah anda yakin ?",
                    text: "",
                    type: "question",
                    showCancelButton: true
                },
                function() {
                    WO.manualCall(item, dataCounter).then(function(res) {
                        var dataResult = res.data[0];
                        console.log('dataResult', dataResult);
                        $scope.manualCallCustomer(dataResult);
                        $scope.gridQueue();
                    });
                },
                function() {

                }
            )
        };
        // ==================================================
        $scope.manualCallCustomer = function(data) {
            console.log("manualCallCustomer dagta=>", data);
            $scope.dataAuto = data;
            $scope.mData = {};
            $scope.mDataCrm.Vehicle = {};
            $scope.mDataDM = {};
            $scope.mDataWAC = {};
            // $scope.mDataCrm.Vehicle.LicensePlate = null;
            gridTemp = [];
            gridTempEstimate = [];
            $scope.JobComplaint = [];
            $scope.JobRequest = [];
            $scope.gridWork = [];
            $scope.gridWorkEstimate = [];
            $scope.gridWork = gridTemp;
            $scope.gridWorkEstimate = gridTempEstimate;
            $scope.totalWork = 0;
            $scope.totalMaterial = 0;
            $scope.totalWorkEstimate = 0;
            $scope.totalMaterialEstimate = 0;
            $scope.gridTimeEst.data = [];
            $scope.gridExt.data = [];
            $scope.gridInt.data = [];
            $scope.gridElec.data = [];
            $scope.gridEquip.data = [];
            $scope.gridDoc.data = [];
            $scope.gridOther.data = [];
            window.scrollTo(0, 0);
            $scope.callTimer = 60;
            console.log('WALK-IN', data);
            $scope.mDataDetail = {};
            $scope.mData = data;
            $scope.mData.PoliceNumber = data.LicensePlate;
            $scope.mDataDetail.VehicleModelName = "";
            // var newDate = new Date();
            // newDate.setHours(0, 0, 0);
            // console.log('WALK-IN newDate', newDate);
            // $scope.mData.AppointmentDate = newDate;
            $scope.mDataCrm.Vehicle.LicensePlate = data.LicensePlate;
            var value = {};
            value.TID = "";
            value.filterValue = data.LicensePlate;
            value.flag = 1
            $scope.searchCustomer(99, value);
            console.log('WALK-IN scope.mData', $scope.mData);

            $scope.pageMode = 'call';
            intervalCallTimer = $interval(countInterval, 1000);
        };

        $scope.searchCustomer = function(flag, value) {
            $scope.getCustIdBySearch = true;
            $scope.mDataCrm = {};
            $scope.mDataCrm.Vehicle = {};
            $scope.mDataCrm.Customer = {};
            console.log('value search', value);
            console.log('flag search', flag);
            if (value.filterValue == "") {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon input filter terlebih dahulu",
                });
            } else {
                if (flag == 1 || flag == 99) {
                    WOBP.getCustomerVehicleList(value).then(function(resu) {
                        console.log('search cust >>>', resu);
                        console.log('resu.data customer>', resu.data.Result[0]);
                        var data = resu.data.Result[0];
                        if (data === undefined) {
                            if (flag == 1) {
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Data Tidak Ditemukan",
                                });
                            };
                        } else {
                            //Customer
                            var customer = data.CustomerList.CustomerListPersonal[0];
                            var institusi = data.CustomerList.CustomerListInstitution[0];
                            var address = data.CustomerList.CustomerAddress;
                            console.log('customer ==========', customer);
                            console.log('institusi ==========', institusi);
                            console.log('address ==========', address);
                            var objC_Personal = {};
                            var objC_Institusi = {};
                            var checkAddres = function() {
                                if (address.length != 0) {
                                    // $scope.custAddressId = data.CustomerAddress.CustomerAddressId;
                                    // var address = data.CustomerAddress.Address;
                                    // return address;
                                    var tempAddressData = [];
                                    _.map(address, function(val) {
                                        console.log('adddersss <><>', val);
                                        var obj = {};
                                        obj.CustomerId = val.CustomerId;
                                        obj.CustomerAddressId = val.CustomerAddressId;
                                        // obj.ProvinceId = val.MVillage.MDistrict.MCityRegency.MProvince.ProvinceId;
                                        // obj.CityRegencyId = val.MVillage.MDistrict.MCityRegency.CityRegencyId;
                                        // obj.VillageId = val.MVillage.VillageId;
                                        // obj.DistrictId = val.MVillage.MDistrict.DistrictId;
                                        obj.Address = val.Address;
                                        obj.AddressCategoryId = val.AddressCategoryId;
                                        obj.MainAddress = val.MainAddress;
                                        obj.Phone1 = val.Phone1;
                                        obj.Phone2 = val.Phone2;
                                        obj.RT = val.RT;
                                        obj.RW = val.RW;
                                        if (val.MVillage != null) {
                                            obj.ProvinceId = val.MVillage.MDistrict.MCityRegency.MProvince.ProvinceId;
                                            obj.CityRegencyId = val.MVillage.MDistrict.MCityRegency.CityRegencyId;
                                            obj.VillageId = val.MVillage.VillageId;
                                            obj.DistrictId = val.MVillage.MDistrict.DistrictId;
                                        };
                                        // obj.CustomerId =
                                        tempAddressData.push(obj);
                                    });
                                    // $scope.CustomerAddress = tempAddressData;

                                    return tempAddressData;
                                } else {
                                    return "No Data Available";
                                }
                            };

                            $scope.custTypeId = data.CustomerList.CustomerTypeId;
                            $scope.customerId = data.CustomerList.CustomerId;
                            $scope.CustomerVehicleId = data.CustomerVehicleId;
                            $scope.vehicleId = data.VehicleId;
                            $scope.getCPPK(data.VehicleId);
                            var addressTemporary = checkAddres();
                            $scope.mDataCustomer.address = addressTemporary[0].Address;
                            if (data.CustomerList.CustomerTypeId == 3) {
                                $scope.mDataCustomer.owner = customer.FrontTitle + '. ' + customer.CustomerName + ', ' + customer.EndTitle;

                                $scope.showFieldPersonal = true;
                                $scope.showFieldInst = false;
                                $scope.personalId = customer.PersonalId;
                                objC_Personal.BirthDate = customer.BirthDate;
                                objC_Personal.KTPKITAS = customer.KTPKITAS;
                                objC_Personal.HandPhone1 = customer.Handphone1;
                                objC_Personal.HandPhone2 = customer.Handphone2;
                                objC_Personal.Name = customer.CustomerName;
                                objC_Personal.FrontTitle = customer.FrontTitle;
                                objC_Personal.EndTitle = customer.EndTitle;
                                objC_Personal.ToyotaId = data.CustomerList.ToyotaId;
                                objC_Personal.Npwp = data.CustomerList.Npwp;
                                objC_Personal.CustomerId = data.CustomerList.CustomerId;
                                objC_Personal.FleetId = data.CustomerList.FleetId;
                                // objC_Personal.Address = checkAddres();
                                objC_Personal.CustomerVehicleId = data.CustomerVehicleId;
                                $scope.CustomerAddress = checkAddres();
                                $scope.mDataCrm.Customer = objC_Personal;
                                objC_Institusi = {};

                            } else {
                                $scope.mDataCustomer.owner = institusi.Name;

                                $scope.showFieldInst = true;
                                $scope.showFieldPersonal = false;
                                $scope.institusiId = institusi.InstitutionId;
                                objC_Institusi.ToyotaId = data.ToyotaId;
                                objC_Institusi.Name = institusi.Name;
                                objC_Institusi.PICName = institusi.PICName;
                                objC_Institusi.PICDepartment = institusi.PICDepartment;
                                objC_Institusi.PICHp = institusi.PICHp;
                                objC_Institusi.SIUP = institusi.PICKTPKITAS;
                                objC_Institusi.PICEmail = institusi.PICEmail;
                                // objC_Institusi.Address = checkAddres();
                                objC_Institusi.CustomerVehicleId = data.CustomerVehicleId;
                                $scope.CustomerAddress = checkAddres();
                                $scope.mDataCrm.Customer = objC_Institusi;
                                objC_Personal = {};
                            };

                            //Vehicle
                            if (data.VehicleList) {

                                var vehicle = data.VehicleList;
                                $scope.mDataCustomer.model = vehicle.MVehicleTypeColor.MVehicleType.Description;
                                console.log('vehicle ==============', vehicle);
                                var objV = {};
                                objV.VehicleId = vehicle.VehicleId;
                                objV.LicensePlate = vehicle.LicensePlate;
                                objV.GasTypeId = vehicle.GasTypeId;
                                objV.VIN = vehicle.VIN;
                                objV.ModelCode = vehicle.MVehicleTypeColor.MVehicleType.MVehicleModel.VehicleModelCode;
                                objV.AssemblyYear = vehicle.AssemblyYear;
                                // objV.Color = vehicle.MVehicleTypeColor.MColor.ColorName;
                                // objV.ColorCode = vehicle.MVehicleTypeColor.MColor.ColorCode;
                                objV.KatashikiCode = vehicle.MVehicleTypeColor.MVehicleType.KatashikiCode;
                                objV.DECDate = vehicle.DECDate;
                                objV.EngineNo = vehicle.EngineNo;
                                objV.SPK = vehicle.SPK ? vehicle.SPK : 'No Data Available YET';
                                objV.Insurance = vehicle.Insurance ? vehicle.Insurance : 'No Data Available YET';
                                objV.STNKName = vehicle.STNKName;
                                objV.STNKAddress = vehicle.STNKAddress;
                                objV.STNKDate = vehicle.STNKDate;
                                objV.Model = vehicle.MVehicleTypeColor.MVehicleType.VehicleModelId;
                                // objV.Type = vehicle.MVehicleTypeColor.MVehicleType.VehicleTypeId;
                                var testingType = null;
                                var testingColor = null;
                                _.map($scope.VehicM, function(val) {
                                    if (val.VehicleModelId == objV.Model) {
                                        $scope.selectedModel(val);
                                        _.map(val.MVehicleType, function(val2) {
                                            console.log('val2', val2);
                                            if (val2.VehicleTypeId == vehicle.MVehicleTypeColor.MVehicleType.VehicleTypeId) {
                                                testingType = val2;
                                                objV.Type = testingType.VehicleTypeId;
                                                $scope.selectedType(val2, 1);
                                                _.map($scope.VehicColor, function(val3) {
                                                    console.log('val3', val3);
                                                    if (val3.ColorId == vehicle.MVehicleTypeColor.ColorId) {
                                                        testingColor = val3;
                                                        objV.Color = testingColor.ColorId;
                                                        objV.ColorCode = testingColor.ColorCode;
                                                        $scope.selectedColor(testingColor);
                                                    }
                                                })
                                            };
                                        });
                                    };
                                });
                                // WO.getVehicleUser(vehicle.VehicleId).then(function(resu) {
                                //     console.log('list pengguna >>', resu.data.Result);
                                // });
                                if (data.CustomerVehicleId) {
                                    $scope.getUserList(data.CustomerVehicleId, 4);
                                }

                                $scope.mDataCrm.Vehicle = objV;
                                objV = {};
                                var objUS = {};
                                var arr1 = [];
                                var arr2 = [];
                                var arrusVUid = [];
                                var tempArr = [];
                                if (data.VehicleUser.length > 1) {
                                    _.map(data.CustomerVehicle.VehicleUser, function(e, k) {
                                        arr1.push(e.Name);
                                        arr2.push(e.Phone);
                                        arrusVUid.push(e.VehicleUserId);
                                        tempArr.push({});
                                    });
                                    $scope.mData.US.name = arr1;
                                    $scope.mData.US.phoneNumber1 = arr2;
                                    $scope.mData.US.VehicleUserId = arrusVUid;
                                    $scope.userForm = tempArr;
                                };
                            } else {
                                $scope.mDataCrm.Vehicle = {};
                                $scope.VehicT = [];
                                $scope.enableType = true;
                            };
                        }
                    });
                };
            };
        };
        $scope.getCPPK = function(vehicleId, flag, arrCP, arrPK) {
            console.log('masuk getCPPK', vehicleId, flag, arrCP, arrPK);
            WO.getListVehicleCPPK(vehicleId).then(function(resu) {
                console.log('resu cppk', resu.data);
                if (flag == 1) {
                    console.log('return 1 cppk', resu.data.Result);
                    console.log('return 1 arrCP', arrCP);
                    console.log('return 1 arrPK', arrPK);
                    // return resu.data.Result;
                    var data = resu.data.Result;
                    var newData = _.union(arrCP, arrPK, data, 'Name');
                    console.log('return 1 newData', newData);
                    $scope.cpList = newData;
                    $scope.pkList = newData;
                } else {
                    console.log('return undefined cppk', resu.data.Result);
                    _.map(resu.data.Result, function(o) {
                        o.HP1 = o.Handphone1;
                        o.HP2 = o.Handphone2;
                    });
                    $scope.cpList = resu.data.Result;
                    $scope.pkList = resu.data.Result;
                }
            });
        };
        $scope.getUserList = function(data) {
            WOBP.getUserLists(data).then(function(resu) {
                console.log('resu >>', resu.data.Result);
                var dataResu = resu.data.Result;
                if (dataResu.length != 0) {
                    var arr = [];
                    _.map(dataResu, function(val) {
                        var obj = {};
                        obj.customerVehicleId = data;
                        obj.name = val.Name;
                        obj.vehicleUserId = val.VehicleUserId;
                        obj.Relationship = val.RelationId;
                        obj.phoneNumber1 = val.Phone1;
                        obj.phoneNumber2 = val.Phone2;
                        arr.push(obj);
                    });
                    // $scope.pngList = data;
                    $scope.CVehicUId = data;
                    // console.log('$scope.CVehicUId', $scope.CVehicUId);
                    $scope.pngList = arr;
                    $scope.hiddenUserList = false;
                } else {
                    $scope.hiddenUserList = true;
                    $scope.CVehicUId = 0;
                }
                console.log('$scope.CVehicUI >>>000<<<<', $scope.CVehicUId);
            });
        };
        //----------------------------------------------------3
        //Queue Taking----------------------------------------
        //----------------------------------------------------4

        $scope.user = CurrentUser.user();
        //mode nya ada 3 view : untuk tampilan sebelum manggil customer, call : untuk tampilan panggil customer,
        //call orang jadi yang di batasi form call nya
        $scope.pageMode = 'view';
        $scope.callTimer = 60;
        var intervalCallTimer;

        $scope.gridQueue = function() {
            var emOnTime = [];
            var em = [];
            var walkIn = [];
            var bookingOnTime = [];
            var booking = [];
            var others = [];

            WOBP.getDataQueueList().then(function(res) {
                var data = res.data;
                var result = angular.copy(res.data);
                if (data.length > 0) {
                    _.forEach(result, function(e) {
                        // console.log('eeeee >>', e);
                        if (e.QueueCategory.Name == 'EM On Time' && e.ServiceType == 0) {
                            e.WaitingTime = $scope.getDateWait(e.ArrivalTime);
                            emOnTime.push(e);
                        } else if (e.QueueCategory.Name == 'EM' && e.ServiceType == 0) {
                            e.WaitingTime = $scope.getDateWait(e.ArrivalTime);
                            em.push(e);
                        } else if (e.QueueCategory.Name == 'Booking On Time' && e.ServiceType == 0) {
                            e.WaitingTime = $scope.getDateWait(e.ArrivalTime);
                            bookingOnTime.push(e);
                        } else if (e.QueueCategory.Name == 'Booking' && e.ServiceType == 0) {
                            e.WaitingTime = $scope.getDateWait(e.ArrivalTime);
                            booking.push(e);
                        } else if (e.QueueCategory.Name == 'Walk In' && e.ServiceType == 0) {
                            e.WaitingTime = $scope.getDateWait(e.ArrivalTime);
                            walkIn.push(e);
                        } else if (e.QueueCategory.Name == 'Others' && e.ServiceType == 0) {
                            e.WaitingTime = $scope.getDateWait(e.ArrivalTime);
                            others.push(e);
                        }
                    });
                    if ($scope.isCenter == false) {
                        $scope.gridReceptBook.data = emOnTime;
                        $scope.gridReceptWalkIn.data = em;
                        $scope.gridReceptSatelite.data = bookingOnTime;
                        $scope.gridSubmission.data = booking;
                    } else {
                        $scope.gridCRecept.data = walkIn;
                        $scope.gridCSubmission.data = others;
                    }
                }
            });
            // $scope.gridEMOnTime.data = res.data
            // $scope.gridEMOnTime.data = $scope.mDataEMOnTime;
            // $scope.gridEM.data = $scope.mDataEM;
            // $scope.gridBookingOnTime.data = $scope.mDataBookingOnTime;
            // $scope.gridBooking.data=$scope.mDataBooking;
            // $scope.gridWalkIn.data= $scope.mDataWalkIn;
            // $scope.gridOthers.data=$scope.mDataOthers;
        };
        $scope.getCounter = function() {
            WO.GetCounter().then(function(res) {
                console.log("res.data.Result", res.data);
                dataCounter = res.data[0];
            });
        };
        $scope.getDateWait = function(data) {
            var timeStart = new Date().getTime();
            var timeEnd = new Date(data).getTime();
            // ===============================
            var hourDiff = timeStart - timeEnd; //in ms
            var secDiff = hourDiff / 1000; //in s
            var minDiff = hourDiff / 60 / 1000; //in minutes
            var hDiff = hourDiff / 3600 / 1000; //in hours
            var humanReadable = {};
            humanReadable.hours = Math.floor(hDiff);
            humanReadable.minutes = Math.floor(minDiff - 60 * humanReadable.hours);
            var gabung = (humanReadable.hours < 10 ? '0' + humanReadable.hours : humanReadable.hours) + ':' + (humanReadable.minutes < 10 ? '0' + humanReadable.minutes : humanReadable.minutes);
            return gabung
        }

        $scope.gridReceptBook = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            enableFiltering: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApiReceptBook = gridApi;
            },
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'Customer',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.LicensePlate}} - {{row.entity.owner}}</div>',
                    width: '65%'
                },
                {
                    name: 'Janji',
                    field: 'meetTime',
                    width: '10%'
                },
                {
                    name: 'Waktu Tunggu',
                    field: 'WaitingTime',
                    // cellFilter:dateFilter,
                    // cellTemplate:datenya,
                    width: '10%'
                },
                { name: 'Action', field: 'Action', cellTemplate: actionCheckIn, width: 120, pinnedRight: false }
            ]
        };
        $scope.gridReceptWalkIn = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            enableFiltering: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApiReceptWalkIn = gridApi;
            },
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'Customer',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.LicensePlate}} - {{row.entity.owner}}</div>',
                    width: '65%'
                },
                {
                    name: 'Janji',
                    field: 'meetTime',
                    width: '10%'
                },
                {
                    name: 'Waktu Tunggu',
                    field: 'WaitingTime',
                    // cellFilter:dateFilter,
                    // cellTemplate:datenya,
                    width: '10%'
                },
                { name: 'Action', field: 'Action', cellTemplate: actionCheckIn, width: 120, pinnedRight: false }
            ]
        };
        $scope.gridReceptSatelite = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            enableFiltering: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApiReceptSatelite = gridApi;
            },
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'Customer',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.LicensePlate}} - {{row.entity.owner}}</div>',
                    width: '65%'
                },
                {
                    name: 'Janji',
                    field: 'meetTime',
                    width: '10%'
                },
                {
                    name: 'Waktu Tunggu',
                    field: 'WaitingTime',
                    // cellFilter:dateFilter,
                    // cellTemplate:datenya,
                    width: '10%'
                },
                { name: 'Action', field: 'Action', cellTemplate: actionCheckIn, width: 120, pinnedRight: false }
            ]
        };
        $scope.gridSubmission = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            enableFiltering: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApiSubmission = gridApi;
            },
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'Customer',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.LicensePlate}} - {{row.entity.owner}}</div>',
                    width: '65%'
                },
                {
                    name: 'Janji',
                    field: 'meetTime',
                    width: '10%'
                },
                {
                    name: 'Waktu Tunggu',
                    field: 'WaitingTime',
                    // cellFilter:dateFilter,
                    // cellTemplate:datenya,
                    width: '10%'
                },
                { name: 'Action', field: 'Action', cellTemplate: actionCheckIn, width: 120, pinnedRight: false }
            ]
        };
        $scope.gridCRecept = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            enableFiltering: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApiCRecept = gridApi;
            },
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'Customer',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.LicensePlate}} - {{row.entity.owner}}</div>',
                    width: '65%'
                },
                {
                    name: 'Janji',
                    field: 'meetTime',
                    width: '10%'
                },
                {
                    name: 'Waktu Tunggu',
                    field: 'WaitingTime',
                    // cellFilter:dateFilter,
                    // cellTemplate:datenya,
                    width: '10%'
                },
                { name: 'Action', field: 'Action', cellTemplate: actionCheckIn, width: 120, pinnedRight: false }
            ]
        };

        $scope.gridCSubmission = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            enableFiltering: true,
            onRegisterApi: function(gridApi) {
                $scope.gridApiCSubmission = gridApi;
            },
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'Customer',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.LicensePlate}} - {{row.entity.owner}}</div>',
                    width: '65%'
                },
                {
                    name: 'Janji',
                    field: 'meetTime',
                    width: '10%'
                },
                {
                    name: 'Waktu Tunggu',
                    field: 'WaitingTime',
                    // cellFilter:dateFilter,
                    // cellTemplate:datenya,
                    width: '10%'
                },
                { name: 'Action', field: 'Action', cellTemplate: actionCheckIn, width: 120, pinnedRight: false }
            ]
        };

        var cellTemplatePSFU = '<div class="ui-grid-cell-contents" ng-bind-html="COL_FIELD"></div>';
        var datenya = '<div class="ui-grid-cell-contents"><input type="text" ng-model="grid.appScope.inisih" /></div>'
        $scope.columnDefs = [{
                name: 'Customer',
                cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.LicensePlate}} - {{row.entity.owner}}</div>',
                width: '65%'
            },
            {
                name: 'Janji',
                field: 'meetTime',
                width: '10%'
            },
            {
                name: 'Waktu Tunggu',
                field: 'WaitingTime',
                // cellFilter:dateFilter,
                // cellTemplate:datenya,
                width: '10%'
            }
        ];


        $scope.isNew = true;
        // $scope.modeQueue = 1;
        // $scope.woList = false;
        // $scope.QueueTaking = function() {
        //     $scope.modeQueue = 2;
        //     $scope.getCounter();
        // };
        $scope.pickCustomer = function() {
            console.log('masuk sini');
            $scope.modeQueue = 4;
        };
        $scope.pickEstimates = function() {
            if ($scope.isJobID) {
                // Set JobId 
                //$scope.JobId = blablablabla
                $scope.modeQueue = 4;
            } else {
                if ($scope.isNew) {
                    ngDialog.openConfirm({
                        template: 'app/services/reception/wo_bp/queueTakingPickEstimatesDialog.html',
                        controller: 'WOBPController',
                        scope: $scope
                    }).then(function(item) {
                        console.log('item buat wo', item);
                        if (item == undefined) {
                            $scope.createWOBP();
                        } else {
                            console.log("berhasil");
                            $scope.modeQueue = 4;
                            $scope.mData = item;
                            $scope.JobComplaint = item.JobComplaint;
                            $scope.gridWork = item.JobTask;
                        }
                    })
                } else {
                    $scope.modeQueue = 4;
                }
            }
        };

        $scope.createWo = function() {
            console.log("tambah");
            $scope.lmEstimation = {};
            $scope.listEstimation.show = !$scope.listEstimation.show;
            $scope.bukaWo(1)

        };
        // $scope.bukaWo = function(item){
        // if(item == 1 && $scope.listEstimation.show == false){
        //     window.scrollTo(0, 0);
        //     $interval.cancel(intervalCallTimer);
        //     ngDialog.closeAll();
        //     $scope.modeQueue = 3;
        //     console.log('modequeue', $scope.modeQueue);
        //     $scope.savemode();
        //     $scope.clearData();
        // }
        // }
        $scope.cancelWo = function() {
            $scope.lmEstimation = {}
            $scope.listEstimation.show = !$scope.listEstimation.show;
        };
        $scope.savemode = function() {
            $scope.hiddenRow = true;
            $scope.disVehic = true;
        };
        $scope.createWOBP = function() {
            // console.log($scope);
            window.scrollTo(0, 0);
            $interval.cancel(intervalCallTimer);
            ngDialog.closeAll();
            $scope.modeQueue = 3;
            console.log('modequeue', $scope.modeQueue);
            $scope.savemode();
            $scope.clearData();
        };

        function countInterval() {
            if ($scope.callTimer > 0) {
                $scope.callTimer = $scope.callTimer - 1;
            } else {
                $scope.callCustomerAgain();
                // $interval.cancel(intervalCallTimer);
            }
        }
        $scope.callCustomer = function() {
            $scope.pageMode = 'call';
            window.scrollTo(0, 0);
            $scope.callTimer = 60;
            WO.autoCall(dataCounter.CounterId).then(function(res) {
                console.log("masuk sini", res.data);
                $scope.dataAuto = res.data[0];
            });
            intervalCallTimer = $interval(countInterval, 1000);
        }
        $scope.callCustomerAgain = function() {
            $interval.cancel(intervalCallTimer);
            WO.reCall(dataCounter, $scope.dataAuto).then(function(res) {
                console.log("Masuk sini abis reCall", res.data);
                $scope.callTimer = 60;
            });
            intervalCallTimer = $interval(countInterval, 1000);
        }
        $scope.callCancel = function() {
            $interval.cancel(intervalCallTimer);
            $scope.pageMode = 'view';
            $scope.gridQueue();
            window.scrollTo(0, 0);
        }
        $scope.skipCall = function() {
            $interval.cancel(intervalCallTimer);
            WO.skipCall(dataCounter, $scope.dataAuto).then(function(res) {
                console.log("Masuk sini abis skipp", res.data);
                $scope.dataAuto = res.data[0];
            });
            $scope.callTimer = 60;
            intervalCallTimer = $interval(countInterval, 1000);
        }

        //----------------------------------------------------5
        //Customer dataList-----------------------------------
        //----------------------------------------------------6

        $scope.getDetailCust = function() {
            WOBP.getDetailCustomer(1).then(function(resu) {
                console.log('resu get detail cust >>', resu.data);
                console.log('resu get detail cust', resu.data[0]);
                $scope.mDataCrm = resu.data[0];
            });
        };
        $scope.getAllDataWOBP = function() {
            //Not Available For Now
        };
        $scope.changeType = function(value) {
            console.log('value >>', value);
            $scope.CustomerAddress = [];
            if (value == 3) {
                $scope.showFieldPersonal = true;
                $scope.showFieldInst = false;
            } else {
                $scope.showFieldInst = true;
                $scope.showFieldPersonal = false;
            }
        };
        $scope.custTypeId = null;
        $scope.showFieldInst = false;
        $scope.showFieldPersonal = false;
        $scope.enableType = true;
        $scope.selectedModel = function(item) {
            console.log('selectedModel >>>', item);
            $scope.VehicT = [];
            $scope.mData.VehicleModelName = item.VehicleModelName;
            $scope.mDataCrm.Vehicle.Type = null;
            // var dVehic = item.MVehicleType;
            var dVehic = _.uniq(item.MVehicleType, 'KatashikiCode');
            _.map(dVehic, function(a) {
                a.NewDescription = a.Description + ' - ' + a.KatashikiCode;
                // a.NewDescription = a.Description + ' - ' + a.KatashikiCode + ' - ' + a.SuffixCode;
            });
            console.log('dVehic >>>', dVehic);
            $scope.VehicT = dVehic;
            $scope.enableType = false;
        };
        $scope.mDataCrm.Vehicle = {};
        $scope.selectedType = function(item, param) {
            console.log('selectedType >>>', item, param);
            $scope.mDataCrm.Vehicle.Color = null;
            $scope.VehicColor = [];
            var arrCol = [];
            _.map($scope.Color, function(val) {
                // console.log('val Color >>>', val);
                if (val.VehicleTypeId == item.VehicleTypeId) {
                    arrCol = val.ListColor;
                };
            });
            if (param == 1) {
                _.map(arrCol, function(a) {
                    // if(a.ColorId == item.)
                })
            };
            $scope.enableColor = false;
            $scope.VehicColor = arrCol;
            $scope.mDataCrm.Vehicle.VehicleTypeId = item.VehicleTypeId;
            console.log('$scope.mDataCrm.Vehicle selectedType >>>', $scope.mDataCrm.Vehicle);
            $scope.mDataCrm.Vehicle.KatashikiCode = item.KatashikiCode;

            // added by sss on 2017-10-12
            taskListObj = {};
            // var prmVTypeId = 4693;
            var prmVTypeId = angular.copy($scope.mDataCrm.Vehicle.VehicleTypeId);
            AppointmentBpService.getDataTask('-', prmVTypeId).then(function(resTask) {
                console.log("res of getDataTask : ", resTask.data.Result);
                for (var i in resTask.data.Result) {
                    // remark below if BE have been updated
                    // resTask.data.Result[i]["PointId"] = 1;
                    //
                    if (!taskListObj[resTask.data.Result[i]["PointId"]]) {
                        taskListObj[resTask.data.Result[i]["PointId"]] = [];
                    }
                    taskListObj[resTask.data.Result[i]["PointId"]].push(resTask.data.Result[i]);
                }
                console.log("taskListObj : ", taskListObj);
            });
            //
        };
        $scope.selectedColor = function(item) {
            console.log('selectedColor >>>', item);
            $scope.mDataCrm.Vehicle.ColorCode = item.ColorCode;
            console.log('$scope.mDataCrm >>>', $scope.mDataCrm);
        };
        $scope.selectedGas = function(item) {
            console.log('VehicGas >>>', item);
            $scope.VehicGas = item.VehicleGasTypeId;
            $scope.enableType = false;
        };

        var dateFormatFilter = 'dd-MM-yyyy';
        $scope.dateOptionsFilter = {
            startingDay: 1,
            format: dateFormatFilter,
            disableWeekend: 1
        };
        $scope.filterSearch = { flag: "", TID: "", filterValue: "", choice: false };
        $scope.filterField = [
            { key: "Nopol", desc: "Nomor Polisi", flag: 1, value: "" },
            { key: "NoRank", desc: "Nomor Rangka", flag: 2, value: "" },
            { key: "NoMesin", desc: "Nomor Mesin", flag: 3, value: "" },
            { key: "Phone", desc: "Nomor Handphone", flag: 4, value: "" },
            { key: "NoKTP", desc: "Nomor KTP/KITAS", flag: 5, value: "" },
            { key: "TTL", desc: "Tanggal Lahir", flag: 6, value: "" }
        ];

        $scope.filterFieldChange = function(item) {
            $scope.filterSearch.flag = item.flag;
            $scope.filterSearch.filterValue = "";
            $scope.filterFieldSelected = item.desc;
        };

        $timeout(function() {
            $scope.filterFieldChange($scope.filterField[0]);
        }, 0);

        $scope.filterSearchV = { flag: "", TID: "", filterValue: "", choice: false };
        $scope.filterFieldV = [
            { key: "Nopol", desc: "Nomor Polisi", flag: 1, value: "" },
            { key: "NoRank", desc: "Nomor Rangka", flag: 2, value: "" }
        ];

        $scope.filterFieldChangeV = function(item) {
            $scope.filterSearchV.flag = item.flag;
            $scope.filterSearchV.filterValue = "";
            $scope.filterFieldSelectedV = item.desc;
        };

        $timeout(function() {
            $scope.filterFieldChangeV($scope.filterField[0]);
        }, 0);

        $scope.hiddenUserList = true;
        $scope.CVehicUId = 0;
        $scope.getCustIdBySearch = false;
        $scope.searchCustomer = function(flag, value) {
            $scope.getCustIdBySearch = true;
            $scope.mDataCrm = [];
            console.log('value search', value);
            console.log('flag search', flag);
            if (value.filterValue == "") {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon input filter terlebih dahulu",
                });
            } else {
                if (flag == 1) {
                    WOBP.getCustomerVehicleList(value).then(function(resu) {
                        console.log('search cust >>>', resu);
                        console.log('resu.data customer>', resu.data.Result[0]);
                        var data = resu.data.Result[0];
                        if (data === undefined) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Data Tidak Ditemukan",
                            });
                        } else {
                            //Customer
                            var customer = data.CustomerList.CustomerListPersonal[0];
                            var institusi = data.CustomerList.CustomerListInstitution[0];
                            var address = data.CustomerList.CustomerAddress;
                            var objC_Personal = {};
                            var objC_Institusi = {};
                            var checkAddres = function() {
                                if (address.length != 0) {
                                    // $scope.custAddressId = data.CustomerAddress.CustomerAddressId;
                                    // var address = data.CustomerAddress.Address;
                                    // return address;
                                    var tempAddressData = [];
                                    _.map(address, function(val) {
                                        console.log('adddersss <><>', val);
                                        var obj = {};
                                        obj.CustomerId = val.CustomerId;
                                        obj.CustomerAddressId = val.CustomerAddressId;
                                        obj.ProvinceId = val.MVillage.MDistrict.MCityRegency.MProvince.ProvinceId;
                                        obj.CityRegencyId = val.MVillage.MDistrict.MCityRegency.CityRegencyId;
                                        obj.VillageId = val.MVillage.VillageId;
                                        obj.DistrictId = val.MVillage.MDistrict.DistrictId;
                                        obj.Address = val.Address;
                                        obj.AddressCategoryId = val.AddressCategoryId;
                                        obj.MainAddress = val.MainAddress;
                                        obj.Phone1 = val.Phone1;
                                        obj.Phone2 = val.Phone2;
                                        obj.RT = val.RT;
                                        obj.RW = val.RW;
                                        tempAddressData.push(obj);
                                    });
                                    // $scope.CustomerAddress = tempAddressData;

                                    return tempAddressData;
                                } else {
                                    return "No Data Available";
                                }
                            };

                            $scope.custTypeId = data.CustomerList.CustomerTypeId;
                            $scope.customerId = data.CustomerList.CustomerId;
                            $scope.CustomerVehicleId = data.CustomerVehicleId;
                            if (data.CustomerList.CustomerTypeId == 3) {
                                $scope.showFieldPersonal = true;
                                $scope.showFieldInst = false;
                                $scope.personalId = customer.PersonalId;
                                objC_Personal.BirthDate = customer.BirthDate;
                                objC_Personal.KTPKITAS = customer.KTPKITAS;
                                objC_Personal.HandPhone1 = customer.Handphone1;
                                objC_Personal.HandPhone2 = customer.Handphone2;
                                objC_Personal.Name = customer.CustomerName;
                                objC_Personal.FrontTitle = customer.FrontTitle;
                                objC_Personal.EndTitle = customer.EndTitle;
                                objC_Personal.ToyotaId = data.CustomerList.ToyotaId;
                                objC_Personal.Npwp = data.CustomerList.Npwp;
                                objC_Personal.CustomerId = data.CustomerList.CustomerId;
                                objC_Personal.FleetId = data.CustomerList.FleetId;
                                // objC_Personal.Address = checkAddres();
                                objC_Personal.CustomerVehicleId = data.CustomerVehicleId;
                                $scope.CustomerAddress = checkAddres();
                                $scope.mDataCrm.Customer = objC_Personal;
                                objC_Institusi = {};

                            } else {
                                $scope.showFieldInst = true;
                                $scope.showFieldPersonal = false;
                                $scope.institusiId = institusi.InstitutionId;
                                objC_Institusi.ToyotaId = data.ToyotaId;
                                objC_Institusi.Name = institusi.Name;
                                objC_Institusi.PICName = institusi.PICName;
                                objC_Institusi.PICDepartment = institusi.PICDepartment;
                                objC_Institusi.PICHp = institusi.PICHp;
                                objC_Institusi.PICKTPKITAS = institusi.PICKTPKITAS;
                                objC_Institusi.PICEmail = institusi.PICEmail;
                                // objC_Institusi.Address = checkAddres();
                                objC_Institusi.CustomerVehicleId = data.CustomerVehicleId;
                                $scope.CustomerAddress = checkAddres();
                                $scope.mDataCrm.Customer = objC_Institusi;
                                objC_Personal = {};
                            };

                            //Vehicle
                            if (data.VehicleList) {
                                var vehicle = data.VehicleList;
                                var objV = {};
                                objV.VehicleId = vehicle.VehicleId;
                                objV.LicensePlate = vehicle.LicensePlate;
                                objV.VIN = vehicle.VIN;
                                objV.ModelCode = vehicle.MVehicleTypeColor.MVehicleType.MVehicleModel.VehicleModelCode;
                                objV.AssemblyYear = vehicle.AssemblyYear;
                                objV.Color = vehicle.MVehicleTypeColor.MColor.ColorName;
                                objV.ColorCode = vehicle.MVehicleTypeColor.MColor.ColorCode;
                                objV.KatashikiCode = vehicle.MVehicleTypeColor.MVehicleType.KatashikiCode;
                                objV.DECDate = vehicle.DECDate;
                                objV.EngineNo = vehicle.EngineNo;
                                objV.SPK = vehicle.SPK ? vehicle.SPK : 'No Data Available YET';
                                objV.Insurance = vehicle.Insurance ? vehicle.Insurance : 'No Data Available YET';
                                objV.STNKName = vehicle.STNKName;
                                objV.STNKAddress = vehicle.STNKAddress;
                                objV.STNKDate = vehicle.STNKDate;
                                objV.Model = vehicle.MVehicleTypeColor.MVehicleType.VehicleModelId;
                                // objV.Type = vehicle.MVehicleTypeColor.MVehicleType.VehicleTypeId;
                                var testingType = null;
                                _.map($scope.VehicM, function(val) {
                                    if (val.VehicleModelId == objV.Model) {
                                        $scope.selectedModel(val);
                                        _.map(val.MVehicleType, function(val2) {
                                            if (val2.VehicleTypeId == vehicle.MVehicleTypeColor.MVehicleType.VehicleTypeId) {
                                                testingType = val2;
                                            };
                                        });
                                    };
                                });

                                objV.Type = testingType.VehicleTypeId;
                                $scope.mDataCrm.Vehicle = objV;
                                objV = {};
                                var objUS = {};
                                var arr1 = [];
                                var arr2 = [];
                                var arrusVUid = [];
                                var tempArr = [];
                                if (data.VehicleUser.length > 1) {
                                    _.map(data.CustomerVehicle.VehicleUser, function(e, k) {
                                        arr1.push(e.Name);
                                        arr2.push(e.Phone);
                                        arrusVUid.push(e.VehicleUserId);
                                        tempArr.push({});
                                    });
                                    $scope.mData.US.name = arr1;
                                    $scope.mData.US.phoneNumber1 = arr2;
                                    $scope.mData.US.VehicleUserId = arrusVUid;
                                    $scope.userForm = tempArr;
                                };
                            } else {
                                $scope.mDataCrm.Vehicle = {};
                                $scope.VehicT = [];
                                $scope.enableType = true;
                            };
                        }
                    });
                }
            }
        };

        $scope.getUserList = function(data) {
            WOBP.getUserLists(data).then(function(resu) {
                console.log('resu >>', resu.data.Result);
                var dataResu = resu.data.Result;
                if (dataResu.length != 0) {
                    var arr = [];
                    _.map(dataResu, function(val) {
                        var obj = {};
                        obj.customerVehicleId = data;
                        obj.name = val.Name;
                        obj.vehicleUserId = val.VehicleUserId;
                        obj.Relationship = val.RelationId;
                        obj.phoneNumber1 = val.Phone1;
                        obj.phoneNumber2 = val.Phone2;
                        arr.push(obj);
                    });
                    // $scope.pngList = data;
                    $scope.CVehicUId = data;
                    // console.log('$scope.CVehicUId', $scope.CVehicUId);
                    $scope.pngList = arr;
                    $scope.hiddenUserList = false;
                } else {
                    $scope.hiddenUserList = true;
                    $scope.CVehicUId = 0;
                }
            });
            console.log('$scope.CVehicUI >>>000<<<<', $scope.CVehicUI);
        };
        $scope.saveCustomerCrm = function(param, data, address) {
            console.log('param >>>000<<<<', param);
            console.log('data >>>000<<<<', data);
            console.log('address >>>000<<<<', address);
            if ((data.Customer.ToyotaIDFlag !== null && data.Customer.ToyotaIDFlag !== undefined) && address.length > 0) {
                $scope.viewmode = 'view';
                $scope.getCustIdBySearch = false;
                var date = new Date(data.Customer.BirthDate);
                date.setSeconds(0);
                var yearFirst = date.getFullYear();
                var monthFirst = date.getMonth() + 1;
                var dayFirst = date.getDate();
                var bDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                data.Customer.BirthDate = bDate;
                data.Customer.newBirthDate = bDate;
                console.log('bDate', bDate);
                console.log('data >>>000<<<< 2', data);

                // var dummyToyataId = Math.floor((Math.random() * 99999999) + 1);

                if (param == 2) {
                    // data.Customer.ToyotaId = dummyToyataId;
                    WOBP.createNewCustomerList(data).then(function(resu) {
                        console.log('resu customer post', resu);
                        var customerId = resu.data[0].CustomerId;
                        $scope.newCustomerId = customerId;
                        _.map(address, function(val) {
                            val.CustomerId = customerId;
                        });
                        if (data.Customer.CustomerTypeId == 3) {
                            WOBP.createNewCustomerListPersonal(customerId, data).then(function(resu2) {
                                console.log('resu customer post', resu2.data);
                                WOBP.createNewCustomerListAddress(customerId, address).then(function(resu3) {
                                    console.log('resu.data personal create address', resu3.data);
                                    $scope.clearEditCustomer();
                                });
                            });
                        } else {
                            console.log("DISINI SIMPAN ADDRESS & INSTITUSI");
                            //SIMPAN NEW CUSTOMER LIST INSTITUSI
                            WOBP.createNewCustomerListInstitusi(customerId, data).then(function(resu2) {
                                console.log('resu institusi post', resu2.data);
                                WOBP.createNewCustomerListAddress(customerId, address).then(function(resu3) {
                                    console.log('resu.data institusi create address', resu3.data);
                                    $scope.clearEditCustomer();
                                });
                            });
                        };
                    });
                } else if (param == 1) {
                    console.log("DISINI customerId, custTypeId", $scope.customerId, $scope.custTypeId);
                    WOBP.updateCustomerList($scope.customerId, $scope.custTypeId, data).then(function(resu) {
                        // console.log('resu.data update', resu.data);
                        console.log('$scope.custTypeId', $scope.custTypeId);
                        if ($scope.custTypeId == 3) {
                            WOBP.updateCustomerListPersonal($scope.personalId, $scope.customerId, data).then(function(resu2) {
                                console.log('resu.data update personal', resu2.data);
                                WOBP.updateCustomerListAddress($scope.custAddressId, $scope.customerId, address).then(function(resu3) {
                                    console.log('resu.data update personal address', resu3.data);
                                    $scope.clearEditCustomer();
                                });
                            });
                        } else {
                            console.log("DISINI UPDATE ADDRESS & INSTITUSI");
                            WOBP.updateCustomerListInstitusi($scope.institusiId, $scope.customerId, data).then(function(resu2) {
                                console.log('resu.data update INSTITUSI', resu2.data);
                                WOBP.updateCustomerListAddress($scope.custAddressId, $scope.customerId, address).then(function(resu3) {
                                    console.log('resu.data update institusi address', resu3.data);
                                    $scope.clearEditCustomer();
                                });
                            });
                        };
                    });
                };
                $scope.mode = 'view';
                $scope.clearEditCustomer();
                mode = 'view';
                console.log("modeeeeeeeeeeeeeeeeeee", mode);
                $scope.getGrouCustomerDiscount();
                return mode
            } else {
                bsAlert.warning("Form masih ada yang belum terisi", "silahkan cek kembali");
            }
        };

        $scope.editcus = function() {
            console.log("$scope.newCustomerId", $scope.newCustomerId);
            $scope.mode = 'search';
            if ($scope.newCustomerId == undefined) {
                $scope.showFieldInst = false;
                $scope.showFieldPersonal = false;
            } else {
                if ($scope.mDataCrm.Customer.CustomerTypeId == 3) {
                    $scope.showFieldPersonal = true;
                } else {
                    $scope.showFieldInst = true;
                }
            }
            // $scope.showFieldInst = false;
            // $scope.showFieldPersonal = false;
            // $scope.mode = 'search';
        };
        $scope.batalEditcus = function() {
            $scope.mode = 'view';
        }
        $scope.editenable = true;
        $scope.changeOwner = function() {
            if ($scope.action == 2) {
                $scope.mDataCrm.Customer = {};
                $scope.mode = 'form';
            } else {
                $scope.showFieldInst = false;
                $scope.showFieldPersonal = false;
                $scope.filterSearch.filterValue = "";
                $scope.mode = 'search';
            };
        };
        $scope.changeVehic = function(param) {
            console.log('paramV', param);
            if (param == 2) {
                console.log('masuk sini vehic 2', $scope.mDataCrm);
                if ($scope.mDataCrm.Vehicle.VehicleId != null) {
                    $scope.hideVehic = true;
                    // $scope.mDataCrm
                } else if ($scope.mDataCrm.Vehicle.Type != null) {
                    $scope.hideVehic = true;
                    // $scope.actionV = 2;
                } else if ($scope.newVehicleId != null) {
                    $scope.hideVehic = true;
                } else {
                    $scope.mDataCrm.Vehicle = {};
                    $scope.hideVehic = true;
                };
                $scope.mDataCrm.Vehicle.LicensePlate = $scope.mData.PoliceNumber;
                $scope.mDataVehicle = [];
                $scope.modeV = 'form';
            } else {
                $scope.mDataCrm.Vehicle = {};
                $scope.hideVehic = false;
                $scope.modeV = 'search';
            }
        };

        $scope.searchV = function() {
            console.log('actionV', $scope.actionV);
        };
        $scope.mode = 'view';
        $scope.modeV = 'view';
        var dateFormat = 'dd/MM/yyyy ';
        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.DateOptionsWithHour = {
            startingDay: 1,
            format: 'dd/MM/yyyy HH:mm:ss'
        };

        $scope.editenable = true;
        $scope.mode = 'view';
        $scope.modeV = 'view';
        var dateFormat = 'dd/MM/yyyy';
        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        //BS LIST ADDRESS Pelanggan------------------------------------
        $scope.CustomerAddress = [];
        $scope.getAddressList = function() {
            return $q.resolve(
                CMaster.GetCAddressCategory().then(function(res) {
                    console.log("List GetCAddressCategory====>", res.data.Result);
                    $scope.categoryContact = res.data.Result;
                }),
                CMaster.GetLocation().then(function(res) {
                    console.log("List Province====>", res.data.Result);
                    // ProvinceId == row.ProvinceId
                    $scope.provinsiData = res.data.Result;
                    // $scope.tempProvinsiData = angular.copy($scope.provinsiData);
                })
            );
        };

        $scope.selectProvince = function(row) {
            console.log('row selectProvince', row);
            $scope.kabupatenData = row.MCityRegency;
            $scope.selectedProvince = angular.copy(row);
        };
        $scope.selectRegency = function(row) {
            console.log('row selectRegency', row);
            $scope.kecamatanData = row.MDistrict;
            $scope.selectedRegency = angular.copy(row);
        };
        $scope.selectDistrict = function(row) {
            console.log('row selectDistrict', row);
            $scope.kelurahanData = row.MVillage;
            $scope.selectedDistrict = angular.copy(row);
        };
        $scope.selectVillage = function(row) {
            console.log('row selectVillage', row);
            $scope.selecedtVillage = angular.copy(row);
            $scope.lmModelAddress.PostalCode = row.PostalCode ? row.PostalCode : '-';
        }
        $scope.onBeforeSaveAddress = function(row) {
            console.log('masuk sini onbeforesaveaddress', row);
            console.log('masuk sini customerId', $scope.customerId);
            var AddressCategory = {};
            if (row.CustomerAddressId == null) {
                row.CustomerAddressId = 0;
                row.CustomerId = $scope.customerId ? $scope.customerId : 0;
            };
            _.map($scope.categoryContact, function(val) {
                if (val.AddressCategoryId == row.AddressCategoryId) {
                    AddressCategory = val;
                }
            });
            row.AddressCategory = AddressCategory;
            row.ProvinceData = $scope.selectedProvince;
            delete row.ProvinceData["MCityRegency"];
            row.CityRegencyData = $scope.selectedRegency;
            delete row.CityRegencyData["MDistrict"];
            row.DistrictData = $scope.selectedDistrict;
            delete row.DistrictData["MVillage"];
            row.VillageData = $scope.selecedtVillage;

            console.log('masuk sini onbeforesaveaddress NEWDATA', row);
        };

        $scope.Mlocation = [];

        $scope.onBeforeEditAddress = function(row) {
            // console.log("ediiit address==>", row);
            console.log('masuk sini onBeforeEditAddress', row);
            console.log('masuk sini onBeforeEditAddress $scope.CustomerAddress', $scope.CustomerAddress);

            // // Mlocation
            // // $scope.provinsiData = res.data.Result;
            // for (var i = 0; i < $scope.Mlocation.length; i++) {
            //     console.log($scope.Mlocation[i].ProvinceId, "==", row.MVillage.MDistrict.MCityRegency.ProvinceId);
            //     if ($scope.Mlocation[i].ProvinceId == row.MVillage.MDistrict.MCityRegency.ProvinceId) {
            //         $scope.kabupatenData = $scope.Mlocation[i].MCityRegency;
            //         for (var j = 0; j < $scope.Mlocation[i].MCityRegency.length; j++) {
            //             if ($scope.Mlocation[i].MCityRegency[j].CityRegencyId == row.MVillage.MDistrict.CityRegencyId) {
            //                 $scope.kecamatanData = $scope.Mlocation[i].MCityRegency[j].MDistrict;
            //                 for (var k = 0; k < $scope.Mlocation[i].MCityRegency[j].MDistrict.length; k++) {
            //                     if ($scope.Mlocation[i].MCityRegency[j].MDistrict[k].DistrictId == row.MVillage.DistrictId) {
            //                         $scope.kelurahanData = $scope.Mlocation[i].MCityRegency[j].MDistrict[k].MVillage;
            //                         break;
            //                     }
            //                 };
            //                 break;
            //             }
            //         };
            //         break;
            //     }}
        };

        $scope.onShowDetailAddress = function(row) {
            console.log('masuk sini onShowDetailAddress', row);
        };

        $scope.onBeforeDeleteAddress = function(row) {
            console.log('masuk sini onBeforeDeleteAddress', row);
            // var FD = [];
            // console.log("delete Address==>", row);
            // for (var i = 0; i < row.length; i++) {
            //     for (var j = 0; j < Fileaddress.length; j++) {
            //         if (row[i] == Fileaddress[j].CustomerAddressId) {
            //             var xc = angular.copy(Fileaddress[j]);
            //             xc.CustomerId = $scope.mCustData.acc1[0].CustomerId;
            //             xc.modul = ($state.current.data.title + ' - Address');
            //             FD.push(xc);

            //             break;
            //         }
            //     };
            // };
            // row.push(FD);
        };
        // $scope.onAfterSaveAddress = function() {
        //     $scope.onShowDetail(detailData, 'view');
        // }
        $scope.listButtonSettingsAddress = { view: { enable: false }, new: { enable: true, icon: "glyphicon glyphicon-home" } };
        $scope.listCustomButtonSettingsAddress = {
            enable: true,
            caption: "Jadikan Utama",
            icon: "glyphicon glyphicon-home",
            func: function(row) {
                console.log("customButton", row);

                CustData.updateMainAddress(row);
            }
        };

        //=================================
        $scope.disVehic = true;
        $scope.tempDataVehicle = [];
        $scope.editVehic = function() {
            $scope.disVehic = false;
            console.log('$scope.mDataCrm', $scope.mDataCrm);
            console.log('$scope.mDataCrm.Vehicle', $scope.mDataCrm.Vehicle);
            if ($scope.mDataCrm.Vehicle != undefined) {
                $scope.tempDataVehicle = angular.copy($scope.mDataCrm.Vehicle);
            };
            $scope.mDataCrm.Vehicle = [];
        };
        $scope.saveVehic = function(param, data) {
            $scope.disVehic = true;
            console.log('data >>><<<<', data);
            var date = new Date(data.Vehicle.DECDate);
            var date2 = new Date(data.Vehicle.STNKDate);
            date.setSeconds(0);
            date2.setSeconds(0);
            var yearFirst = date.getFullYear();
            var monthFirst = date.getMonth() + 1;
            var dayFirst = date.getDate();
            date2.setSeconds(0);
            var yearFirst2 = date2.getFullYear();
            var monthFirst2 = date2.getMonth() + 1;
            var dayFirst2 = date2.getDate();
            var DECDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
            var STNKDate = yearFirst2 + '-' + monthFirst2 + '-' + dayFirst2;
            data.Vehicle.DECDate = DECDate;
            data.Vehicle.STNKDate = STNKDate;
            // $scope.modeV = 'view';
            if (param == 2) {
                WOBP.createNewVehicleList(data.Vehicle).then(function(resu) {
                    console.log('$resu.createNewVehicleList', resu.data);
                });
            } else {
                WOBP.updateVehicleList(data.Vehicle).then(function(resu) {
                    console.log('$resu.updateVehicleList', resu.data);
                });
            }
        };
        $scope.cancelSearchV = function() {
            $scope.disVehic = true;
            if ($scope.tempDataVehicle != undefined) {
                $scope.mDataCrm.Vehicle = $scope.tempDataVehicle;
            }
        };
        //=================================
        $scope.disPnj = true;
        $scope.editPnj = function() {
            $scope.disPnj = false;
        };
        $scope.savePnj = function() {
            $scope.disPnj = true;
            console.log('$scope.mDataVehicle', $scope.mDataVehicle);
        };
        //=================================
        $scope.onChangePng = function(item) {
            console.log('onchangePng', item);
            // if ($scope.pngList != undefined) {
            //     _.map($scope.pngList, function(val) {
            //         console.log('resu >>', val);

            //         if (val.VehicleUserId == item.VehicleUserId) {
            //             var obj = {};
            //             obj.name = val.Name
            //         }
            //     })
            // }

            $scope.mData.US = item;

            // var arr = [];
            // if (item != undefined) {
            //     var obj = {};
            //     obj.name = item.Name;
            //     obj.phoneNumber1 = item.Phone1;
            //     obj.phoneNumber2 = item.Phone2;
            //     obj.Relationship = item.RelationId;
            //     arr.push(obj);
            //     $scope.mData.US = obj;
            // }
        };

        $scope.disPng = true;
        $scope.editPng = function() {
            $scope.disPng = false;
        };
        $scope.cancelPng = function() {
            $scope.disPng = true;
            $scope.mData.US = [];
        }
        $scope.savePng = function(data, CVehicUId) {
            var tempArrSavePng = [];
            var tempArrSavePngNoId = [];
            console.log('$scope.mData.US data >>', data, CVehicUId);

            if (data.customerVehicleId == undefined) {
                if (CVehicUId == 0) {
                    bsAlert.alert({
                            title: "Customer Vehicle ID Tidak Ditemukan !!",
                            text: "Silahkan Cek Data kembali !",
                            type: "warning"
                        },
                        function() {},
                        function() {}
                    );
                } else {
                    $scope.disPng = true;
                    console.log('createNewUserList data 1>>', data, CVehicUId);
                    WOBP.createNewUserList(data, CVehicUId).then(function(resu) {
                        console.log('createNewUserList resu >>', resu.data);
                        $scope.getUserList(CVehicUId);
                    });
                }
            } else {
                $scope.disPng = true;
                if (data.vehicleUserId != undefined) {
                    console.log('updateUserList data >>', data, CVehicUId);
                    WOBP.updateUserList(data, CVehicUId).then(function(resu) {
                        console.log('updateUserList resu>>', resu.data);
                        $scope.getUserList(CVehicUId);
                    });
                } else {
                    console.log('createNewUserList data 2>>', data, CVehicUId);
                }
            }
            // for (var i in data.US.name) {
            //     if (data.US.VehicleUserId[i] != null || data.US.VehicleUserId !== undefined) {
            //         tempArrSavePng.push({
            //             VehicleUserId: data.US.VehicleUserId[i],
            //             Name: data.US.VehicleUserId[i],
            //             Phone: data.US.phoneNumber1[i],
            //             CustomerVehicleId: $scope.CustomerVehicleId
            //         })
            //     } else {
            //         tempArrSavePngNoId.push({
            //             CustomerVehicleId: $scope.CustomerVehicleId,
            //             Name: data.US.VehicleUserId[i],
            //             Phone: data.US.phoneNumber1[i]
            //         })
            //     }
            // };
            // _.map(data, function(val) {
            //     console.log('data <><><>', val);
            // })
        };

        // $scope.userForm = [{}];
        // $scope.addPengguna = function() {
        //     $scope.userForm.push({});
        //     console.log('userForm', $scope.userForm);
        // };
        //=================================

        $scope.filter = { AttendDate: null };

        //Vehicle dataList------------------------------------

        $scope.getVehicleMnT = function() {
            WOBP.getVehicleMList().then(function(resuM) {
                console.log('getVehicleMList', resuM.data.Result);
                $scope.VehicM = resuM.data.Result;
            });
            // WOBP.getVehicleTList().then(function(resuT) {
            //     console.log('getVehicleTList', resuT.data.Result);
            //     $scope.VehicT = resuT.data.Result;
            // })
        };

        $scope.searchVehicle = function(flag, value) {
            console.log('searchVehicle', flag, value);
            var plat = '-';
            var vin = '-';
            if (value.flag == 1) {
                plat = value.filterValue;
                vin = '-';
            } else if (value.flag == 2) {
                vin = value.filterValue;
                plat = '-';
            };

            WOBP.getVehicleList(vin, plat).then(function(resu) {
                console.log('resu search vehic', resu.data.Result[0]);
                var data = resu.data.Result[0];
                var obj = {};
                obj.VehicleId = data.VehicleId;
                obj.LicensePlate = data.LicensePlate;
                obj.VIN = data.VIN;
                obj.ModelCode = data.MVehicleTypeColor.MVehicleType.MVehicleModel.VehicleModelCode;
                obj.AssemblyYear = data.AssemblyYear;
                obj.Color = data.MVehicleTypeColor.MColor.ColorName;
                obj.ColorCode = data.MVehicleTypeColor.MColor.ColorCode;
                obj.KatashikiCode = data.MVehicleTypeColor.MVehicleType.KatashikiCode;
                obj.DECDate = data.DECDate;
                obj.EngineNo = data.EngineNo;
                obj.SPK = data.SPK ? data.SPK : 'No Data Available';
                obj.Insurance = data.Insurance ? data.Insurance : 'No Data Available';
                obj.Model = data.MVehicleTypeColor.MVehicleType.VehicleModelId;
                obj.STNKName = data.STNKName;
                obj.STNKAddress = data.STNKAddress;
                obj.STNKDate = data.STNKDate;

                var testingType = null;
                _.map($scope.VehicM, function(val) {
                    if (val.VehicleModelId == obj.Model) {
                        $scope.selectedModel(val);
                        _.map(val.MVehicleType, function(val2) {
                            if (val2.VehicleTypeId == data.MVehicleTypeColor.MVehicleType.VehicleTypeId) {
                                testingType = val2;
                            };
                        });
                    };
                });

                obj.Type = testingType.VehicleTypeId;
                // obj.Type = data.MVehicleTypeColor.MVehicleType.VehicleTypeId;
                $scope.mDataCrm.Vehicle = obj;
                console.log('objobj', obj);
                console.log(' $scope.mDataCrm', $scope.mDataCrm);
            });

        };
        // $scope.mDataVehicle = {};
        // var data = {
        //     'No_Rangka': 'KAI22493X4568',
        //     'Model_Code': 'AMK22',
        //     'Tipe': '1',
        //     'Model': '1',
        //     'Warna': 'Red',
        //     'Kode_Warna': 'R03',
        //     'No_Polisi': 'A 4669 KZ',
        //     'Full_Model': 'FQK250-GKMDSX',
        //     'No_Mesin': '22X',
        //     'Asuransi': 'Astra',
        //     'No_SPK': '456789123',
        //     'Tanggal_DEC': '12 April 2017',
        //     'Tahun_Rakit': '2015'
        // };
        // $scope.mDataVehicle = data;

        //----------------------------------------------------7
        //------Kontak Person & Pengambil keputusan-----------
        //----------------------------------------------------8

        $scope.dataDummy = [{
            Id: 1,
            Name: 'Adi Daya Sundaya',
            HP1: '08588585220',
            HP2: '08154782120'
        }, {
            Id: 2,
            Name: 'Abi Daya Sundaya',
            HP1: '08588585222',
            HP2: '08154782122'
        }, {
            Id: 3,
            Name: 'Aci Daya Sundaya',
            HP1: '08588585225',
            HP2: '08154782125'
        }, {
            Id: 4,
            Name: 'Ari Daya Sundaya',
            HP1: '08588585278',
            HP2: '08154782178'
        }];
        $scope.mDataDM = [];
        $scope.cpList = [{
            Id: 1,
            Name: 'Adi Daya'
        }, {
            Id: 2,
            Name: 'Abi Daya'
        }, {
            Id: 3,
            Name: 'Aci Daya'
        }];

        $scope.onChangeCP = function(item) {
            console.log('selected CP', item);
            var arr = [];
            if (item != null) {
                _.map($scope.dataDummy, function(e) {
                    if (e.Id == item.Id) {
                        arr.push(e);
                    }
                });
                // console.log('arrCP', arr);
                $scope.mDataDM.CP = arr[0];
            }
        };

        $scope.pkList = [{
            Id: 1,
            Name: 'Adi Daya'
        }, {
            Id: 2,
            Name: 'Abi Daya'
        }, {
            Id: 4,
            Name: 'Ari Daya'
        }];

        $scope.onChangePK = function(item) {
            console.log('selected PJ', item);
            var arr = [];
            _.map($scope.dataDummy, function(e) {
                if (e.Id == item.Id) {
                    arr.push(e);
                }
            });
            $scope.mDataDM.PK = arr[0];
        };


        //----------------------------------------------------9
        //-------Estimation-----------------------------------
        //----------------------------------------------------10
        $scope.headerEst = [{
            'id': 1,
            'name': 'Vehicle Condition'
        }, {
            'id': 2,
            'name': 'Price Estimation'
        }, {
            'id': 3,
            'name': 'Time Estimation'
        }, {
            'id': 4,
            'name': 'Estimation Sheet'
        }];
        //----------------------------------------------------11
        //--------Estimation >> Time Estimation---------------
        //----------------------------------------------------12
        $scope.counterTimeEst = 0;
        $scope.getTimeEst = function(param) {
            console.log('$scope.counterTimeEst', $scope.counterTimeEst);
            // console.log()
            // var newData = null;
            if ($scope.gridTimeEst.data.length == 0) {
                $scope.counterTimeEst = 0;
            }
            if (param == 3) {
                // newData = angular.copy($scope.JobRequest);
                if ($scope.counterTimeEst == 0) {
                    console.log('gettimeest', $scope.gridWork);
                    var arr = [];
                    _.forEach($scope.gridWork, function(e) {
                        var obj = {};
                        obj.OrderName = e.TaskName;
                        obj.Body = '';
                        obj.Putty = '';
                        obj.Surfacer = '';
                        obj.Painting = '';
                        obj.Polishing = '';
                        obj.Reassembbly = '';
                        obj.Final_Inspection = '';

                        arr.push(obj);
                    });
                    $scope.gridTimeEst.data = arr;
                    $scope.counterTimeEst = 1;
                } else if ($scope.counterTimeEst == 1) {
                    if ($scope.gridTimeEst.data.length != $scope.gridWork.length) {
                        var arr = [];
                        _.forEach($scope.gridWork, function(e) {
                            var obj = {};
                            obj.OrderName = e.TaskName;
                            obj.Body = '';
                            obj.Putty = '';
                            obj.Surfacer = '';
                            obj.Painting = '';
                            obj.Polishing = '';
                            obj.Reassembbly = '';
                            obj.Final_Inspection = '';

                            arr.push(obj);
                        });
                        // newData = angular.copy()
                        $scope.gridTimeEst.data = arr;
                    } else {

                    }
                };
            }
        }

        $scope.gridTimeEst = {
            enableCellEditOnFocus: true,
            showColumnFooter: true,

            columnDefs: [{
                'name': 'Order Pekerjaan',
                'field': 'OrderName',
                footerCellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;">Time Per Stall</div>'
            }, {
                'name': 'Body',
                'field': 'Body',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Putty',
                'field': 'Putty',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Surfacer',
                'field': 'Surfacer',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Painting',
                'field': 'Painting',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Polishing',
                'field': 'Polishing',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Reassembbly',
                'field': 'Reassembbly',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Final Inspection',
                'field': 'Final_Inspection',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }],
            onRegisterApi: function(gridApi) {
                $scope.gridTimeEstApi = gridApi;
                $scope.gridTimeEstApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridTimeEstApi.selection.selectRow) {
                    $scope.gridTimeEstApi.selection.selectRow($scope.gridTimeEst.data[0]);
                }
            }
        };
        // var arrDataTimeEst = [{
        //     'Id': 1,
        //     'OrderName': 'Perbaikan Bumper Depan',
        //     'Body': '',
        //     'Putty': '',
        //     'Surfacer': '',
        //     'Painting': '',
        //     'Polishing': '',
        //     'Reassembbly': '',
        //     'Final_Inspection': ''
        // }, {
        //     'Id': 2,
        //     'OrderName': 'Perbaikan Fender',
        //     'Body': '',
        //     'Putty': '',
        //     'Surfacer': '',
        //     'Painting': '',
        //     'Polishing': '',
        //     'Reassembbly': '',
        //     'Final_Inspection': ''
        // }];
        // $scope.gridTimeEst.data = arrDataTimeEst;
        $scope.testTimeEst = function() {
            console.log('gridApi', $scope.gridTimeEstApi.grid.columns.length);
        };
        $scope.estHours = null;
        $scope.estDays = null;
        $scope.calculate = function() {
            $scope.getCalculatedTime = [];
            $scope.arrEstTime = [];
            // console.log("totoal", $scope.gridTimeEstApi.grid);
            var total = 0;
            for (var i = 0; i < $scope.gridTimeEstApi.grid.columns.length; i++) {
                var a = $scope.gridTimeEstApi.grid.columns[i].getAggregationValue();
                // $scope.arrEstTime.push(a + ' minutes');
                if (a != null || a !== undefined) {
                    $scope.arrEstTime.push(a + ' minutes');
                    $scope.getCalculatedTime.push(a);

                    total = total + a;
                };
            };

            function roundToTwo(num) {
                return +(Math.round(num + "e+2") + "e-2");
            };

            var days = Math.floor(Math.abs(total / (24 * 60)));
            var hours = Math.abs((total % (24 * 60)) / 60);
            $scope.estHours = roundToTwo(hours);
            $scope.estDays = days;
            // console.log("hours", hours);
            // console.log("arrEstTime", $scope.arrEstTime);



        };
        //----------------------------------------------------13
        //WAC LIST--------------------------------------------
        //----------------------------------------------------14

        $scope.slider = {
            value: 0,
            options: {
                floor: 0,
                ceil: 100,
                showSelectionBar: true,
                translate: function(value) {
                    return value + '%';
                }
            }
        };
        $scope.refreshSlider = function() {
            $timeout(function() {
                $scope.$broadcast('rzSliderForceRender');
            });
        };
        $scope.mDataWAC = [];

        $scope.tabClick = function(param) {
            $scope.getDataExt(param);
        };
        $scope.getAllImpData = function() {
            return $q.resolve(
                WOBP.getDataWAC().then(function(resu) {
                    console.log('cek data WAC', resu.data);
                    $scope.headerWAC = resu.data;
                }),
                WOBP.getCategoryWO().then(function(resu) {
                    console.log('cek data getCategoryWO', resu.data);
                    $scope.CatWO = resu.data.Result;
                }),
                WOBP.getCategoryRepair().then(function(resu) {
                    console.log('cek data getCategoryRepair', resu.data);
                    $scope.CatPerbaikan = resu.data.Result;
                }),
                WOBP.getRelationship().then(function(resu) {
                    $scope.dataRelationship = resu.data.Result;
                }),
                RepairSupportActivityGR.getT1().then(function(res) {
                    $scope.TFirst = res.data.Result;
                    //console.log("T1", $scope.T1);
                })
            )
        };
        // $scope.getRelationship = function() {
        //     WOBP.getRelationship().then(function(resu) {
        //         $scope.dataRelationship = resu.data.Result;
        //     });
        // }
        $scope.selectedCatPerbaikan = function(item) {
            console.log('selectedCatPerbaikan', item);
        };
        $scope.selectCatWO = function(item) {
            console.log('selectCatWO', item);
        };
        $scope.getDataExt = function(id) {
            console.log('id header', id)
            WOBP.getItemWAC(id).then(function(resu) {
                if (id == 2 && $scope.gridExt.data.length == 0) {
                    $scope.dataExt = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataExt, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = '';
                        obj.OtherDescription = null;

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Ext = arr;
                    $scope.gridExt.data = $scope.mDataWAC.Ext;
                    // console.log('$scope.mDataWAC.Ex', $scope.mDataWAC.Ext);
                } else if (id == 4 && $scope.gridInt.data.length == 0) {
                    $scope.dataInt = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataInt, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = '';
                        obj.OtherDescription = null;

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Int = arr;
                    $scope.gridInt.data = $scope.mDataWAC.Int;
                } else if (id == 1003 && $scope.gridElec.data.length == 0) {
                    $scope.dataElec = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataElec, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = '';
                        obj.OtherDescription = null;

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Elec = arr;
                    $scope.gridElec.data = $scope.mDataWAC.Elec;
                } else if (id == 1004 && $scope.gridEquip.data.length == 0) {
                    $scope.dataEquip = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataEquip, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = '';
                        obj.OtherDescription = null;

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Equip = arr;
                    $scope.gridEquip.data = $scope.mDataWAC.Equip;
                } else if (id == 1005 && $scope.gridDoc.data.length == 0) {
                    $scope.dataDoc = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataDoc, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = '';
                        obj.OtherDescription = null;

                        arr.push(obj);
                    });
                    console.log('grid dataDoc arr', arr);
                    $scope.mDataWAC.Doc = arr;
                    $scope.gridDoc.data = $scope.mDataWAC.Doc;
                } else if (id == 1006) {
                    $scope.dataOther = resu.data.Result;
                    console.log('grid other ', $scope.dataOther[0]);
                    var arr = [];
                    angular.forEach($scope.dataOther, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = null;
                        obj.OtherDescription = '';
                        arr.push(obj);
                    });
                    $scope.mDataWAC.Oth = arr;
                    $scope.gridOther.data = $scope.mDataWAC.Oth;
                    console.log('grid other arr', $scope.gridOther.data);
                }
            })
        };
        //WAC LIST > Body ------------------------------------
        //Canvas
        // $(function() {
        //     var canvas = angular.element('canvas');
        //     console.log('canvas', canvas);

        //     var angularCanvas = angular.element(document.getElementById('myCanvas'));
        //     console.log('angularCanvas', angularCanvas);
        //     var context = canvas.getContext('2d');
        //     console.log('context context', context);
        // });
        // var canvas = angular.element('pitch-canvas');
        // var angularCanvas = angular.element(document.getElementById('myCanvas'));
        // var canvas2 = document.getElementById('myCanvas');
        // console.log('canvas', canvas);
        // console.log('angularCanvas', angularCanvas);
        // // var context2 = canvas2.getContext('2d');
        // console.log('context context', context);
        // // var canvas = angular.element('canvas');
        // // var angularCanvas = angular.element(document.getElementById('myCanvas'));
        // // var canvas2 = document.getElementById('myCanvas')
        // console.log('canvas', canvas);
        // console.log('canvas2', canvas2);
        // console.log('angularCanvas', angularCanvas);
        // var context = canvas.context(';
        // var background = new Image();
        // background.src = "http://i.imgur.com/yf6d9SX.jpg";

        // background.onload = function() {
        //     context.drawImage(background, 0, 0);
        // };
        // $scope.data = [];
        // $scope.addClick = function(event) {
        //     var id = 0;
        //     if ($scope.data.length > 0) {
        //         id = $scope.data[$scope.data.length - 1].id + 1;
        //     }
        //     var p = { id: id, x: event.offsetX, y: event.offsetY, amount: 5 };
        //     $scope.data.push(p);
        //     $scope.x = '';
        //     $scope.y = '';
        //     $scope.amount = '';
        //     draw($scope.data);

        // };
        // $scope.addData = function() {
        //     var id = 0;
        //     if ($scope.data.length > 0) {
        //         id = $scope.data[$scope.data.length - 1].id + 1;
        //     }
        //     var p = { id: id, x: $scope.x, y: $scope.y, amount: $scope.amount };
        //     $scope.data.push(p);
        //     $scope.x = '';
        //     $scope.y = '';
        //     $scope.amount = '';
        //     draw($scope.data);
        // };

        // $scope.removePoint = function(point) {
        //     console.log(point);
        //     for (var i = 0; i < $scope.data.length; i++) {
        //         if ($scope.data[i].id === point.id) {
        //             console.log("removing item at position: " + i);
        //             $scope.data.splice(i, 1);
        //         }
        //     }

        //     context.clearRect(0, 0, 600, 400);
        //     draw($scope.data);
        //     console.log($scope.data);
        // }

        // function draw(data) {
        //     for (var i = 0; i < data.length; i++) {
        //         drawDot(data[i]);
        //         // if(i > 0) {
        //         //   drawLine(data[i], data[i-1]);
        //         //}
        //     }
        // }

        // function drawDot(data) {
        //     context.beginPath();
        //     context.arc(data.x, data.y, data.amount, 0, 2 * Math.PI, false);
        //     context.fillStyle = "#ccddff";
        //     context.fill();
        //     context.lineWidth = 1;
        //     context.strokeStyle = "#666666";
        //     context.stroke();
        // }

        // function drawLine(data1, data2) {
        //     context.beginPath();
        //     context.moveTo(data1.x, data1.y);
        //     context.lineTo(data2.x, data2.y);
        //     context.strokeStyle = "black";
        //     context.stroke();
        // }

        // // setup
        // canvas.width = 600;
        // canvas.height = 400;
        // context.globalAlpha = 1.0;
        // context.beginPath();
        // draw($scope.data);
        ///
        $scope.coorX = null;
        $scope.coorY = null;
        $scope.drawData = [];
        $scope.showPopup = false;
        $scope.openPopup = function(e) {
            ////
            $scope.showPopup = true;
            var left = e.offsetX;
            var top = e.offsetY;
            $scope.coorX = angular.copy(left) + 'px';
            $scope.coorY = angular.copy(top) + 'px';
            console.log('event X', $scope.coorX);
            console.log('event Y', $scope.coorY);
            $scope.popoverStyle = {
                'position': 'absolute',
                'background': '#fff',
                'border': '1px solid #999',
                'padding': '10px',
                'width': 'auto',
                'box-shadow': '0 0 10px rgba(0, 0, 0, .5)',
                'left': $scope.coorX,
                'top': $scope.coorY
            };
            $scope.drawData = [{
                x: $scope.coorX,
                y: $scope.coorY
            }];
            console.log('scope.drawData', $scope.drawData);
        };

        $scope.closePopover = function(param) {
            $scope.paramClosePopover = param;
            $scope.showPopup = false;
            console.log('scope.paramClosePopover', $scope.paramClosePopover);
        };


        //WAC LIST > Exterior --------------------------------
        $scope.ItemStatusTemplate = '<div class="btn-group ui-grid-cell-contents"><input ng-model="row.entity.ItemStatus" type="radio" ng-value="1" style="width:20px">&nbsp;Baik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input ng-model="row.entity.ItemStatus" type="radio" ng-value="2" style="width:20px">&nbsp;Rusak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input ng-model="row.entity.ItemStatus" type="radio" ng-value="3" style="width:20px">&nbsp;Tidak Ada</div>';

        $scope.ItemStatus = null;
        $scope.gridExt = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                field: 'ItemStatus',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridExtApi = gridApi;

                $scope.gridExtApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridExtApi.selection.selectRow) {
                    $scope.gridExtApi.selection.selectRow($scope.gridExt.data[0]);
                }
            }
        };
        //WAC LIST > Interior --------------------------------
        $scope.gridInt = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridIntApi = gridApi;

                $scope.gridIntApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridIntApi.selection.selectRow) {
                    $scope.gridIntApi.selection.selectRow($scope.gridInt.data[0]);
                }
            }
        };

        //WAC LIST > Electricity --------------------------------
        $scope.gridElec = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridElecApi = gridApi;

                $scope.gridElecApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridElecApi.selection.selectRow) {
                    $scope.gridElecApi.selection.selectRow($scope.gridElec.data[0]);
                }
            }
        };

        //WAC LIST > Equipment --------------------------------
        $scope.gridEquip = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridEquipApi = gridApi;

                $scope.gridEquipApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridEquipApi.selection.selectRow) {
                    $scope.gridEquipApi.selection.selectRow($scope.gridEquip.data[0]);
                }
            }
        };

        //WAC LIST > Document --------------------------------
        $scope.gridDoc = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridDocApi = gridApi;

                $scope.gridDocApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridDocApi.selection.selectRow) {
                    $scope.gridDocApi.selection.selectRow($scope.gridDoc.data[0]);
                }
            }
        };
        //WAC LIST > Other ----------------------------------
        $scope.summernoteOptions2 = {
            dialogsInBody: true,
            dialogsFade: false,
            height: 300
        };

        //---------------------------------------------------
        //Job List ------------------------------------------

        $scope.headerJB = [{
            'id': 1,
            'name': 'Job List'
        }, {
            'id': 2,
            'name': 'Job Plan'
        }];
        //---------------------------------------------------
        //Job List > Job List--------------------------------   
        AppointmentGrService.getPayment().then(function(res) {
            $scope.paymentData = res.data.Result;
        });
        AppointmentGrService.getUnitMeasurement().then(function(res) {
            $scope.unitData = res.data.Result;
        });
        AppointmentGrService.getTaskCategory().then(function(res) {
            $scope.taskCategory = res.data.Result;
        });
        AppointmentGrService.getWoCategory().then(function(res) {
            $scope.woCategory = res.data.Result;
        });
        var gridTemp = [];
        $scope.listApi = {};
        $scope.listSelectedRows = [];
        $scope.modalMode = 'new';
        $scope.totalWork = 0;
        $scope.totalMaterial = 0;
        $scope.totalAll = 0;

        $scope.ComplaintCategory = [
            { ComplaintCatg: 'Body' },
            { ComplaintCatg: 'Body Electrical' },
            { ComplaintCatg: 'Brake' },
            { ComplaintCatg: 'Drive Train' },
            { ComplaintCatg: 'Engine' },
            { ComplaintCatg: 'Heater System & AC' },
            { ComplaintCatg: 'Restraint' },
            { ComplaintCatg: 'Steering' },
            { ComplaintCatg: 'Suspension and Axle' },
            { ComplaintCatg: 'Transmission' },
        ];
        $scope.buttonSettingModal = { save: { text: 'Service Explanation' } };
        $scope.listEstimationButton = { save: { text: 'Buat WO' } };
        $scope.conditionData = [{ Id: 1, Name: "Ok" }, { Id: 2, Name: "Not Ok" }];
        $scope.typeMData = [{ Id: 1, Name: 1 }, { Id: 2, Name: 2 }, { Id: 3, Name: 3 }];
        $scope.jobAvailable = false;
        $scope.partsAvailable = false;
        $scope.FlatRateAvailable = false;
        $scope.PriceAvailable = false;
        $scope.JobRequest = [];
        $scope.JobComplaint = [];
        $scope.listView = {
            new: { enable: false },
            view: { enable: false },
            delete: { enable: false },
            select: { enable: false },
            selectall: { enable: false },
            edit: { enable: false },
        }
        $scope.listCustomer = {
            new: { enable: false },
            view: { enable: false },
            delete: { enable: false },
            select: { enable: false },
            selectall: { enable: false },
            edit: { enable: false },
        }
        $scope.buttonSummaryData = {
            new: { enable: false },
            view: { enable: false },
            delete: { enable: false },
            select: { enable: false },
            selectall: { enable: false },
            edit: { enable: false },
        };
        $scope.listButtonSettings = { new: { enable: true, icon: "fa fa-fw fa-car" } };
        $scope.listCustomButtonSettings = {
            enable: true,
            icon: "fa fa-fw fa-car",
            func: function(row) {
                console.log("customButton", row);
            }
        };
        $scope.listButtonFalse = {
            new: {
                enable: false
            }
        };

        // ============ Diskon AE =============
        $scope.getGrouCustomerDiscount = function() {
            console.log("$scope.mDataCrm.Customer", $scope.mDataCrm.Customer);
            WO.getDiscountMGroupCustomer($scope.mDataCrm.Customer).then(function(res) {
                var tmpdataDiskon = res.data.Result;
                if (tmpdataDiskon.length > 0) {
                    $scope.tmpDiskonListData = tmpdataDiskon[0];
                    $scope.diskonData.push({
                        MasterId: 2,
                        Name: "Group Customer Diskon"
                    });
                }
            });
        };
        // ==================================================

        $scope.materialCategory = [{
            MasterId: 1,
            Name: "SpareParts"
        }, {
            MasterId: 2,
            Name: "Bahan"
        }];
        $scope.listRequestSelectedRows = [];
        $scope.listComplaintSelectedRows = [];
        $scope.listJoblistSelectedRows = [];

        $scope.addDetail = function(data, item, itemPrice, row) {
            $scope.listApi.clearDetail();
            console.log("row", row);
            row.FlatRate = data.FlatRate;
            row.Faisal = 10100101;
            row.catName = $scope.tmpCatg.Name;
            // row.cusType=$scope.tmpCus.Name
            console.log("itemPrice", itemPrice);
            if (itemPrice.length > 0) {
                for (var i = 0; i < itemPrice.length; i++) {
                    if (itemPrice[i].AdjustmentServiceRate !== undefined) {
                        var harganya = itemPrice[i].AdjustmentServiceRate;
                        var harganyaa = data.FlatRate;
                        var summary = harganya * harganyaa;
                        var summaryy = parseInt(summary);
                        row.Fare = itemPrice[i].AdjustmentServiceRate;
                        row.Summary = summaryy;
                        // row.
                        $scope.PriceAvailable = true;
                    } else {
                        row.Fare = itemPrice[i].AdjustmentServiceRate;
                        row.Summary = row.Fare;
                    }
                }
            } else {
                $scope.PriceAvailable = false;
                row.Fare = "";
                row.Summary = "";
            }
            row.TaskId = data.TaskId;
            row.isOPB = data.isOPB ? data.isOPB : 0;
            var tmp = {}
                // $scope.listApi.addDetail(tmp,row)
            if (item.length > 0) {
                for (var i = 0; i < item.length; i++) {
                    tmp = item[i];
                    $scope.listApi.addDetail(tmp, row);
                    console.log('adddetail tmp', tmp);
                    console.log('adddetail row', row);
                }
            } else {
                $scope.listApi.addDetail('', row);
            }
        };
        $scope.onListSelectRows = function(rows) {
            console.log("form controller=>", rows);
        };
        $scope.gridPartsDetail = {

            columnDefs: [
                { name: "NoMaterial", field: "PartsCode" },
                { name: "NamaMaterial", field: "PartsName" },
                { name: "Qty", field: "Qty" },
                { name: "Satuan", field: "SatuanId" },
                { name: "Ketersedian" },
                { name: "Tipe", field: "Type" },
                { name: "ETA", field: "ETA" },
                { name: "Harga", field: "RetailPrice" },
                { name: "Disc", field: "Discount" },
                { name: "SubTotal", field: "subTotal" },
                { name: "NilaiDp", field: "Dp" },
                { name: "StatusDp" },
                { name: "Pembayaran", field: "PaidById" },
                { name: "OPB", field: "isOPB", cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>' }
            ]
        };
        $scope.gridWork = gridTemp;
        $scope.sendWork = function(key, data) {
            var taskandParts = [];
            console.log("SendWork Key", key);
            console.log("SendWork Data", data);
            console.log("SendWork $scope.lmModel", $scope.lmModel);
            var taskList = {};
            AppointmentGrService.getDataPartsByTaskId(data.TaskId).then(function(res) {
                console.log("resabis ambil parts", res.data.Result);
                taskandParts = res.data.Result;
                AppointmentGrService.getDataTaskListByKatashiki($scope.mDataCrm.Vehicle.KatashikiCode).then(function(restask) {
                    taskList = restask.data.Result;
                    console.log("taskList", taskList);
                    $scope.addDetail(data, taskandParts, taskList, $scope.lmModel)
                });
            });
        };
        $scope.sendParts = function(key, data) {
            console.log("a $scope.ldModel.Parts", $scope.ldModel.Parts);
            $scope.ldModel = data;
            var tmp = {};
            tmp = data;
            $scope.ldModel.PartsName = data.PartsName
                // $scope.listApi.addDetail(tmp);
            console.log("key Parts", key);
            console.log("data Parts", data);
            console.log("b $scope.ldModel.Parts", $scope.ldModel);
        };
        //----------------------------------
        // Type Ahead
        //----------------------------------

        $scope.getWork = function(key) {
            console.log("$scope.mDataCrm", $scope.mDataCrm);
            console.log("$scope.mDataCrm.vehicle.KatashikiCode", $scope.mDataCrm.Vehicle.KatashikiCode);

            if ($scope.mDataCrm.Vehicle.KatashikiCode != null) {
                var Katashiki = $scope.mDataCrm.Vehicle.KatashikiCode;
                var catg = $scope.tmpCatg.MasterId;
                console.log("$scope.mDataDetail.KatashikiCode", Katashiki);
                console.log("$scope.lmModel.JobType", catg);
                if (Katashiki != null && catg != null) {
                    var ress = AppointmentGrService.getDataTask(key, Katashiki, catg).then(function(resTask) {
                        return resTask.data.Result;
                    });
                    console.log("ress", ress);
                    return ress;
                }
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Katashiki Code Terlebih Dahulu",
                });
            }


        };
        $scope.getParts = function(key) {
            var ress = AppointmentGrService.getDataParts(key).then(function(resparts) {
                return resparts.data.Result;
            });
            console.log("ress", ress);
            return ress
                // }
        };
        $scope.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };
        $scope.noResults = false;
        $scope.onSelectWork = function($item, $model, $label) {
            // console.log("onSelectWork=>", id);
            // console.log("onSelectWork=>", $item);
            // console.log("onSelectWork=>", $model);
            // console.log("onSelectWork=>", $label);
            // $scope.mData.Work = $label;
            // console.log('materialArray', materialArray);
            // materialArray = [];
            // materialArray.push($item);

            // console.log("onSelectWork=>", idx);
            console.log("onSelectWork=>", $item);
            console.log("onSelectWork=>", $model);
            console.log("onSelectWork=>", $label);
            // $scope.mData.Work = $label;
            // console.log('materialArray', materialArray);
            // materialArray = [];
            // materialArray.push($item);
            if ($item.FlatRate != null) {
                console.log("FlatRate", $item.FlatRate);
                $scope.FlatRateAvailable = true;
            } else {
                $scope.FlatRateAvailable = false;
            }
            $scope.sendWork($label, $item);
            console.log("modelnya", $scope.lmModel);
            // console.log('materialArray',materialArray);
        };
        $scope.onSelectParts = function($item, $model, $label) {
            // console.log("onSelectWork=>", idx);
            console.log("onSelectParts=>", $item);
            console.log("onSelectParts=>", $model);
            console.log("onSelectParts=>", $label);
            // $scope.mData.Work = $label;
            // console.log('materialArray', materialArray);
            // materialArray = [];
            // materialArray.push($item);
            // if($item.PartsName != null){
            //   $scope.partsAvailable = true;
            // }else{
            //   $scope.partsAvailable= false;
            // }
            $scope.sendParts($label, $item);
            console.log("modelnya", $item);
            // console.log('materialArray',materialArray);
        };
        $scope.onNoResult = function() {
            console.log("gak", $scope.lmModel);
            $scope.listApi.clearDetail();
            var row = $scope.lmModel;
            row.FlatRate = "";
            row.Fare = "";
            row.Summary = "";
            row.catName = $scope.tmpCatg.Name;
            // row.cusType =$scope.tmpCus.Name;
            $scope.listApi.addDetail('', row);
            console.log("uunnnnchhh", $scope.lmModel)
            $scope.partsAvailable = false;
            $scope.jobAvailable = false;
            $scope.PriceAvailable = false;
            $scope.FlatRateAvailable = false;
        };
        $scope.onGotResult = function() {
            console.log("onGotResult=>");
        };
        $scope.selected = {};
        $scope.selectTypeWork = function(data) {
            console.log("data", data);
            $scope.tmpCatg = data;
            console.log("gridTemp", gridTemp);
            console.log("gridWork", $scope.lmModel);
        }
        $scope.selectTypeCust = function(data) {
            console.log("data", data);
            $scope.tmpCus = data;
            $scope.lmModel.cusType = data.Name;
        }
        $scope.onListSave = function(item) {
            console.log("save_modal=>", item);
        };
        $scope.onListCancel = function(item) {
            console.log("cancel_modal=>", item);
        };
        $scope.onAfterCancel = function() {
            $scope.jobAvailable = false;
            $scope.FlatRateAvailable = false;
            $scope.PriceAvailable = false;
        }
        $scope.onAfterSave = function() {
            var totalW = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].Price !== undefined) {
                    totalW += gridTemp[i].Price;
                };
            };
            $scope.totalWork = totalW;
            var totalM = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].RetailPrice !== undefined) {
                            totalM += gridTemp[i].child[j].RetailPrice;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].RetailPrice);
                        };
                    };
                };
            };
            $scope.totalMaterial = totalM;
        };
        $scope.onAfterDelete = function() {
            var totalW = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].Price !== undefined) {
                    totalW += gridTemp[i].Price;
                }
            };
            $scope.totalWork = totalW;
            console.log(totalW);
            // ============
            var totalM = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].RetailPrice !== undefined) {
                            totalM += gridTemp[i].child[j].RetailPrice;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].RetailPrice);
                        };
                    };
                };
            };
            $scope.totalMaterial = totalM;
        };
        //---------------------------------------------------
        $scope.releaseWOBP = function(isParts, isWashing, isWaiting) {
            $scope.mData.Fuel = $scope.slider.value;
            $scope.mData.flag = {
                parts: isParts,
                wash: isWashing,
                wait: isWaiting
            };
            _.forEach($scope.gridWork, function(e) {
                _.forEach($scope.gridTimeEst.data, function(f) {
                    if (e.TaskName === f.OrderName) {
                        e.BodyEstimationMinute = f.Body;
                        e.FIEstimationMinute = f.Final_Inspection;
                        e.SprayingEstimationMinute = f.Painting;
                        e.PolishingEstimationMinute = f.Polishing;
                        e.PutyEstimationMinute = f.Putty;
                        e.ReassemblyEstimationMinute = f.Reassembbly;
                        e.SurfacerEstimationMinute = f.Surfacer;
                    }
                })
            });
            $scope.counterTimeEst = 0;

            WOBP.releaseWOBP(
                $scope.mData,
                $scope.mDataCrm,
                $scope.mDataDM,
                $scope.JobRequest,
                $scope.JobComplaint,
                $scope.gridWork,
                $scope.gridTimeEst.data
            ).then(function(resu) {
                // console.log('resu.data', resu.data);
                var wobpData = resu.data.ResponseMessage.split('#').pop();
                var union = _.unionBy($scope.gridExt.data, $scope.gridInt.data, $scope.gridElec.data, $scope.gridEquip.data, $scope.gridDoc.data, 'JobWacItemId');
                _.forEach(union, function(e) {
                    e.JobId = parseInt(wobpData)
                });
                var getCalculatedTime = {
                    // JobId: 1000,
                    JobId: parseInt(wobpData),
                    Body: $scope.getCalculatedTime[0],
                    Putty: $scope.getCalculatedTime[1],
                    Surfacer: $scope.getCalculatedTime[2],
                    Spraying: $scope.getCalculatedTime[3],
                    Polishing: $scope.getCalculatedTime[4],
                    ReAssembly: $scope.getCalculatedTime[5],
                    FinalInspection: $scope.getCalculatedTime[6]
                };
                console.log('getCalculatedTime', getCalculatedTime);
                WOBP.createWACBPbyJobId(union).then(function(resu2) {
                    // console.log('res', resu2.data);
                    WOBP.createWOBPEstimation(wobpData, getCalculatedTime).then(function(resu3) {
                        $scope.loading = false;
                        $scope.modeQueue == 1;
                    });
                });
            });
        };

        $scope.saveNewWOBP = function(isParts, isWashing, isWaiting) {
            $scope.mData.Fuel = $scope.slider.value;
            $scope.mData.flag = {
                parts: isParts,
                wash: isWashing,
                wait: isWaiting
            };
            _.forEach($scope.gridWork, function(e) {
                _.forEach($scope.gridTimeEst.data, function(f) {
                    if (e.TaskName === f.OrderName) {
                        e.BodyEstimationMinute = f.Body;
                        e.FIEstimationMinute = f.Final_Inspection;
                        e.SprayingEstimationMinute = f.Painting;
                        e.PolishingEstimationMinute = f.Polishing;
                        e.PutyEstimationMinute = f.Putty;
                        e.ReassemblyEstimationMinute = f.Reassembbly;
                        e.SurfacerEstimationMinute = f.Surfacer;
                    }
                })
            });
            console.log('data CP & PK', $scope.mDataDM);
            console.log('data CRM', $scope.mDataCrm);
            console.log('jobList', $scope.gridWork);
            console.log('jobRequest', $scope.JobRequest);
            console.log('jobComplaint', $scope.JobComplaint);
            console.log('mData', $scope.mData);
            console.log('mDataWAC', $scope.mDataWAC);
            console.log('gridDataEst', $scope.gridTimeEst.data);
            console.log('gridWAC Ext', $scope.gridExt.data);
            console.log('gridWAC Int', $scope.gridInt.data);
            console.log('gridWAC Elec', $scope.gridElec.data);
            console.log('gridWAC Equip', $scope.gridEquip.data);
            console.log('gridWAC Doc', $scope.gridDoc.data);
            console.log('gridWAC Other', $scope.gridOther.data);
            console.log('gridWAC Union', _.unionBy($scope.gridExt.data, $scope.gridInt.data, $scope.gridElec.data, $scope.gridEquip.data, $scope.gridDoc.data, $scope.gridOther.data, 'JobWacItemId'));
            $scope.counterTimeEst = 0;

            if ($scope.JobId != null || $scope.JobId !== undefined) {
                //Update WO if Appointment
                WOBP.updateWOBP(
                    $scope.mData,
                    $scope.mDataCrm,
                    $scope.mDataDM,
                    $scope.JobRequest,
                    $scope.JobComplaint,
                    $scope.gridWork,
                    $scope.gridTimeEst.data
                ).then(function(resu) {
                    // console.log('resu.data', resu.data);
                    var wobpData = resu.data.ResponseMessage.split('#').pop();
                    var union = _.unionBy($scope.gridExt.data, $scope.gridInt.data, $scope.gridElec.data, $scope.gridEquip.data, $scope.gridDoc.data, $scope.gridOther.data, 'JobWacItemId');
                    _.forEach(union, function(e) {
                        e.JobId = parseInt(wobpData)
                    });

                    var getCalculatedTime = {
                        // JobId: 1000,
                        JobId: parseInt(wobpData),
                        Body: $scope.getCalculatedTime[0],
                        Putty: $scope.getCalculatedTime[1],
                        Surfacer: $scope.getCalculatedTime[2],
                        Spraying: $scope.getCalculatedTime[3],
                        Polishing: $scope.getCalculatedTime[4],
                        ReAssembly: $scope.getCalculatedTime[5],
                        FinalInspection: $scope.getCalculatedTime[6]
                    };
                    console.log('getCalculatedTime', getCalculatedTime);
                    console.log('union', union);
                    WOBP.createWACBPbyJobId(union).then(function(resu2) {
                        // console.log('res', resu2.data);
                        WOBP.createWOBPEstimation(wobpData, getCalculatedTime).then(function(resu3) {
                            $scope.loading = false;
                            $scope.modeQueue == 1;

                            // console.log('resu3.data', resu3.data)
                        });
                    });
                });
            } else {
                //Create WO if Walk-In
                WOBP.createWOBPnoJobId();
            };
        };

        $scope.clearData = function() {
            $scope.mData = [];
            $scope.mDataCrm = [];
            $scope.JobRequest = [];
            $scope.JobComplaint = [];
            $scope.gridWork = [];
        };

        $scope.clearEditCustomer = function() {
            $scope.filterSearch.filterValue = "";
            $scope.showFieldInst = false;
            $scope.showFieldPersonal = false;
        };

    })