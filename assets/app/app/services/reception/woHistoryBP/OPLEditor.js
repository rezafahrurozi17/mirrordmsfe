var peScopeWOListBP,peCompileWOListBP;
function initOPLEditor($scope,$compile){
    // console.log("init PartEditor=>");
    peScopeWOListBP = $scope;
    peCompileWOListBP = $compile;
}

function OPLEditor() {};
        OPLEditor.prototype.init = function (params) {
            this.eGui = document.createElement('div');
            this.eGui.style.display = "inline-block";
            this.eGui.style.minWidth = "190px";
            this.eGui.style.width = "250px";
            this.eGui.style.border = "none";
            this.eGui.tabIndex = "0";
            // this.eGui.innerHTML = '<input style="width: 150px; float: left" type="text" class="form-control" '+
            //                       ' uib-datepicker-popup="{{format}}" ng-model="dt" is-open="popup.opened" datepicker-options="dateOptions" ' +
            //                       ' ng-required="true" close-text="Close"/>' +
            //                       '<button style="float: left" type="button" class="btn btn-default" ng-click="open()"><i class="glyphicon glyphicon-calendar"></i></button>';
            this.eGui.innerHTML =
                '<bstypeahead placeholder="Nama Pekerjaan" name="oplName" ng-model="oplModel" get-data="getOplSrc" item-text="OPLWorkName" loading="loadingUsers" noresult="noResults" selected="selected" on-select="selectOPl" on-noresult="onNoResultOplNew" on-gotresult="onGotResult" '+
                  'template-url="customTemplateOpl.html" icon="fa-id-card-o" required>'+
                '</bstypeahead>';
            // this.eGui.innerHTML =
            //         '<bstypeahead placeholder="No. Material" name="noMaterial" ng-model="ldModel.PartsCode" get-data="getParts" item-text="PartsCode" ng-maxlength="250" disallow-space loading="loadingUsers" noresult="noResults" selected="selected" '+
            //             'on-select="onSelectParts"'+
            //             'on-noresult="onNoPartResult" on-gotresult="onGotResult" ta-minlength="4"'+ 
            //             // 'template-url="customTemplate.html"'+ 
            //             'icon="fa-id-card-o">'+
            //         '</bstypeahead>';

            // create and compile AngularJS scope for this component
            this.$scope = peScopeWOListBP.$new();
            peCompileWOListBP(this.eGui)(this.$scope);
            this.$scope.params = params;

            var that = this;
            if (params.value) {
                this.$scope.pcode = params.value
            } else {
                this.$scope.pcode = null
            }
            // this.$scope.open = function () {
            //     that.$scope.popup.opened = true;
            // };
            // this.$scope.popup = {
            //     opened: false
            // };
            this.$scope.$watch('popup.opened', function (newVal, oldVal) {
                if (!newVal && oldVal) {
                    window.setTimeout(function () {
                        return that.$scope.params.stopEditing();
                    }, 0)
                }
            });
            this.eGui.addEventListener('keypress', function (event) {
                console.log("keypress event=>",event,that.eGui);
                    that.eGui.children[0].children[1].focus();
                    // if (event.preventDefault) event.preventDefault();
                    if (that.isKeyPressedNavigation(event)){
                        event.stopPropagation();
                    }
            });
            // this.eGui.addEventListener('keydown', function (event) {
            //     that.onKeyDown(event)
            // });

            // in case we are running outside of angular (ie in an ag-grid started VM turn)
            // we call $apply. we put in timeout in case we are inside apply already.
            window.setTimeout(this.$scope.$apply.bind(this.$scope), 0);
        };
        OPLEditor.prototype.onKeyDown = function (event) {
            console.log("onKeydown event=>",event);
            var key = event.which || event.keyCode;
            if (key == 37 ||  // left
                key == 39) {  // right
                event.stopPropagation();
            }
        };
        OPLEditor.prototype.isKeyPressedNavigation = function (event){
            return event.keyCode===39
                || event.keyCode===37;
        };
        OPLEditor.prototype.afterGuiAttached = function () {
            // console.log("children[0]=>",this.eGui.children[0].children);
            this.eGui.children[0].children[1].focus();
        };
        OPLEditor.prototype.getGui = function () {
            return this.eGui;
        };
        OPLEditor.prototype.destroy = function () {
            this.$scope.$destroy();
        };
        OPLEditor.prototype.isPopup = function () {
            // and we could leave this method out also, false is the default
            return true;
        };
        OPLEditor.prototype.getValue = function () {
            if (!this.$scope.pcode) {
                return null;
            }else{
                // this.$scope.SearchMaterialAg(this.$scope.pcode);
            }
            return this.$scope.pcode;
        };
