var peScopeWOListBP,peCompileWOListBP;
function initDateEditor($scope,$compile){
    // console.log("init PartEditor=>");
    peScopeWOListBP = $scope;
    peCompileWOListBP = $compile;
}

function DateEditor() {};
        DateEditor.prototype.init = function (params) {
            this.eGui = document.createElement('div');
            this.eGui.style.display = "inline-block";
            this.eGui.style.minWidth = "190px";
            this.eGui.style.width = "250px";
            this.eGui.style.border = "none";
            this.eGui.tabIndex = "0";
            this.eGui.innerHTML = '<bsdatepicker name="DateAppoint" min-date="minDate" date-options="dateOptions"  ng-model="mOpl.DateStart"></bsdatepicker>';
            // this.eGui.innerHTML = '<div id="date-picker-example" class="md-form md-outline input-with-post-icon datepicker" inline="true">'+
            //                           '<input placeholder="Select date" type="text" id="example" class="form-control">'+
            //                           '<i class="fas fa-calendar input-prefix"></i>'+
            //                        '</div>';

            // create and compile AngularJS scope for this component
            this.$scope = peScopeWOListBP.$new();
            peCompileWOListBP(this.eGui)(this.$scope);
            this.$scope.params = params;

            var that = this;
            if (params.value) {
                this.$scope.pcode = params.value
            } else {
                this.$scope.pcode = null
            }
            // this.$scope.open = function () {
            //     that.$scope.popup.opened = true;
            // };
            // this.$scope.popup = {
            //     opened: false
            // };
            this.$scope.$watch('popup.opened', function (newVal, oldVal) {
                if (!newVal && oldVal) {
                    window.setTimeout(function () {
                        return that.$scope.params.stopEditing();
                    }, 0)
                }
            });
            this.eGui.addEventListener('keypress', function (event) {
                console.log("keypress event=>",event,that.eGui);
                    that.eGui.children[0].children[1].focus();
                    // if (event.preventDefault) event.preventDefault();
                    if (that.isKeyPressedNavigation(event)){
                        event.stopPropagation();
                    }
            });
            // this.eGui.addEventListener('keydown', function (event) {
            //     that.onKeyDown(event)
            // });

            // in case we are running outside of angular (ie in an ag-grid started VM turn)
            // we call $apply. we put in timeout in case we are inside apply already.
            window.setTimeout(this.$scope.$apply.bind(this.$scope), 0);
        };

        DateEditor.prototype.onKeyDown = function (event) {
            console.log("onKeydown event=>",event);
            var key = event.which || event.keyCode;
            if (key == 37 ||  // left
                key == 39) {  // right
                event.stopPropagation();
            }
        };
        DateEditor.prototype.isKeyPressedNavigation = function (event){
            return event.keyCode===39
                || event.keyCode===37;
        };
        DateEditor.prototype.afterGuiAttached = function () {
            // console.log("children[0]=>",this.eGui.children[0].children);
            this.eGui.children[0].children[1].focus();
        };
        DateEditor.prototype.getGui = function () {
            return this.eGui;
        };
        DateEditor.prototype.destroy = function () {
            this.$scope.$destroy();
        };
        DateEditor.prototype.isPopup = function () {
            // and we could leave this method out also, false is the default
            return true;
        };
        DateEditor.prototype.getValue = function () {
            if (!this.$scope.pcode) {
                return null;
            }else{
                // this.$scope.SearchMaterialAg(this.$scope.pcode);
            }
            return this.$scope.pcode;
        };
