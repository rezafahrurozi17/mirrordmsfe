angular.module('app')
    .factory('WoHistoryGR', function ($http, CurrentUser, $filter, $q) {
        var currUser = CurrentUser.user();
        console.log(currUser);
        return {
            getDataMasterDiscountBooking: function() {
                var res = $http.get('/api/as/MAppointmentDiscount/GetAktif/?start=1&limit=100000');                
                return res;
            },
            getData: function () {
                var res = $http.get('/api/as/Vehiclefare');
                //console.log('res=>',res);
                //res.data.Result = null;
                return res;
            },
            create: function (data) {
                console.log('data create', data);
                return $http.put('/api/as/Jobs/updateDetil/2', [{
                    JobId: data.JobId,
                    JobTask: data.JobTask,
                    JobTaskOpl: data.OPL
                }]);
            },
            FormatingTimeZone: function (data){
                var pecahData = data.toString().split(' ');
                console.log('pecah data', pecahData);
                // Jan:0, Feb:0, Mar:0, Apr:0, May:0, Jun:0, Jul:0, Aug:0, Sep:0, Oct:0, Nov:0, Dec:0
                if (pecahData[1] == 'Jan'){
                    pecahData[1] = '01'
                } else if (pecahData[1] == 'Feb'){
                    pecahData[1] = '02'
                } else if (pecahData[1] == 'Mar'){
                    pecahData[1] = '03'
                } else if (pecahData[1] == 'Apr'){
                    pecahData[1] = '04'
                } else if (pecahData[1] == 'May'){
                    pecahData[1] = '05'
                } else if (pecahData[1] == 'Jun'){
                    pecahData[1] = '06'
                } else if (pecahData[1] == 'Jul'){
                    pecahData[1] = '07'
                } else if (pecahData[1] == 'Aug'){
                    pecahData[1] = '08'
                } else if (pecahData[1] == 'Sep'){
                    pecahData[1] = '09'
                } else if (pecahData[1] == 'Oct'){
                    pecahData[1] = '10'
                } else if (pecahData[1] == 'Nov'){
                    pecahData[1] = '11'
                } else if (pecahData[1] == 'Dec'){
                    pecahData[1] = '12'
                } else {
                    pecahData[1] = '01'
                }

                var hasilFormat = pecahData[3] + '-' + pecahData[1] + '-' + pecahData[2] + ' ' + pecahData[4]
                return hasilFormat;
            },
            update: function (mData, mDataDM, mDataCrm) {
                console.log('data update', mData);
                console.log('mDataDM update', mDataDM);

                //     return $http.post('/api/as/Jobs/postWO', [{
                //         JobId: data.JobId,
                //         JobTask: data.JobTask
                //     }]);

                // =========== start buat fixedDelivery nih ===============

                var FD1 = new Date(mData.EstimateDate);
                var FD2 = new Date(mData.AdjusmentDate);

                var yearFD1 = FD1.getFullYear();
                var monthFD1 = FD1.getMonth() + 1;
                monthFD1 = monthFD1 < 10 ? '0' + monthFD1 : monthFD1;
                var dayFD1 = FD1.getDate();
                dayFD1 = dayFD1 < 10 ? '0' + dayFD1 : dayFD1;
                var hourFD1 = FD1.getHours();
                hourFD1 = hourFD1 < 10 ? '0' + hourFD1 : hourFD1;
                var minuteFD1 = FD1.getMinutes();
                minuteFD1 = minuteFD1 < 10 ? '0' + minuteFD1 : minuteFD1;

                var yearFD2 = FD2.getFullYear();
                var monthFD2 = FD2.getMonth() + 1;
                monthFD2 = monthFD2 < 10 ? '0' + monthFD2 : monthFD2;
                var dayFD2 = FD2.getDate();
                dayFD2 = dayFD2 < 10 ? '0' + dayFD2 : dayFD2;
                var hourFD2 = FD2.getHours();
                hourFD2 = hourFD2 < 10 ? '0' + hourFD2 : hourFD2;
                var minuteFD2 = FD2.getMinutes();
                minuteFD2 = minuteFD2 < 10 ? '0' + minuteFD2 : minuteFD2;

                var FD1Final = yearFD1 + '-' + monthFD1 + '-' + dayFD1 + ' ' + hourFD1 + ':' + minuteFD1
                var FD2Final = yearFD2 + '-' + monthFD2 + '-' + dayFD2 + ' ' + hourFD2 + ':' + minuteFD2
                // =========== end buat fixedDelivery nih ===============

                // ======= Added Code For Changing Format Date ======= u can use this code if have problem for changing format date
                mData.AppointmentDate = this.changeFormatDate(mData.AppointmentDate);
                mData.PlanDateStart = this.changeFormatDate(mData.PlanDateStart);
                mData.PlanDateFinish = this.changeFormatDate(mData.PlanDateFinish);
                // mData.WoCreatedDate = this.changeFormatDate(mData.WoCreatedDate);
                // mData.WoCreatedDate = $filter('date')(mData.WoCreatedDate, 'yyyy-MM-dd HH:mm:ss') // karena ini time zone naik turun.. susah
                // mData.WoCreatedDate = this.FormatingTimeZone(mData.WoCreatedDate)
                var sampleWoCreatedDate = angular.copy(mData.WoCreatedDate)
                sampleWoCreatedDate = this.FormatingTimeZone(sampleWoCreatedDate)

                // mData.EstimateDeliveryTime = this.changeFormatDate(mData.EstimateDeliveryTime);
                // ===========================================
                //isi log buat task part opl ============================== start
                mData.JobTask = this.isiGuidLogTaskPartOPL(mData.JobTask,mData.Guid_Log,1)
                mData.OPL = this.isiGuidLogTaskPartOPL(mData.OPL,mData.Guid_Log,2)
                //isi log buat task part opl ============================== end
                return $http.put('/api/as/Jobs/updateDetil/2', [{
                    JobId: mData.JobId,
                    JobTask: mData.JobTask,
                    JobComplaint: mData.JobComplaint,
                    JobRequest: mData.JobRequest,
                    JobWAC: mData.JobWAC,
                    JobNo: mData.JobNo,
                    OutletId: mData.OutletId,
                    CalId: mData.CalId,
                    StallId: mData.StallId,
                    Stall: mData.Stall,
                    VehicleId: mData.VehicleId,
                    PlanStart: mData.PlanStart,
                    PlanFinish: mData.PlanFinish,
                    CustRequest: mData.CustRequest,
                    CustComplaint: mData.CustComplaint,
                    TechnicianAction: mData.TechnicianAction1,
                    JobSuggest: mData.JobSuggest1,
                    T1: mData.T1,
                    T2: mData.T2,
                    RSA: mData.RSA,
                    AdditionalTime: mData.AdditionalTime,
                    JobType: mData.JobType,
                    Status: mData.Status,
                    AppointmentDate: mData.AppointmentDate,
                    AppointmentTime: mData.AppointmentTime,
                    AppointmentNo: mData.AppointmentNo,
                    isAppointment: mData.isAppointment,
                    AppointmentVia: mData.AppointmentVia,
                    isGr: mData.isGr,
                    Km: mData.KmNormal,
                    // isCash: mData.isCash,
                    isCash: 1,
                    isSpk: mData.isSpk,
                    SpkNo: mData.SpkNo ? mData.SpkNo : '-',
                    InsuranceName: mData.InsuranceName,
                    isWOBase: mData.isWOBase,
                    WoCategoryId: mData.WoCategoryId,
                    WoNo: mData.WoNo,
                    IsEstimation: mData.IsEstimation,
                    EstimationDate: mData.EstimateDeliveryTime,
                    EstimationNo: mData.EstimationNo,
                    JobDate: mData.JobDate,
                    PermissionPartChange: mData.PermissionPartChange,
                    PaymentMethod: mData.PaymentMethod,
                    FinalDP: mData.FinalDP,

                    DPPaidAmount: mData.DPPaidAmount,
                    ORPaidAmount: mData.ORPaidAmount,
                    DPRawatJalan: mData.DPRawatJalan,
                    IsPaidRawatJalan: mData.IsPaidRawatJalan,

                    // DecisionMaker: mData.Name,
                    // PhoneDecisionMaker1: mData.HP1,
                    // PhoneDecisionMaker2: mData.HP2,
                    // ContactPerson: mData.Name,
                    // PhoneContactPerson1: mData.HP1,
                    // PhoneContactPerson2: mData.HP2,
                    DecisionMaker: mDataDM.PK.Name ? mDataDM.PK.Name : null,
                    PhoneDecisionMaker1: mDataDM.PK.HP1 ? mDataDM.PK.HP1 : null,
                    PhoneDecisionMaker2: mDataDM.PK.HP2 ? mDataDM.PK.HP2 : null,
                    ContactPerson: mDataDM.CP.Name ? mDataDM.CP.Name : null,
                    PhoneContactPerson1: mDataDM.CP.HP1 ? mDataDM.CP.HP1 : null,
                    PhoneContactPerson2: mDataDM.CP.HP2 ? mDataDM.CP.HP2 : null,
                    UsedPart: mData.UsedPart,
                    IsWaiting: mData.IsWaiting,
                    IsWash: mData.IsWash,
                    // JobTaskOpl: mData.OPL, // di komen karena uda ada service update sendiri untuk opl
                    ModelType: mData.ModelType,
                    PoliceNumber: mData.PoliceNumber,
                    isFoOneHour: mData.isFoOneHour,
                    isFoOneDay: mData.isFoOneDay,
                    HandOverDate: mData.HandOverDate,
                    // KatashikiCode: mData.KatashikiCode,
                    KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    Revision: mData.Revision,
                    NewAppointmentRel: mData.NewAppointmentRel,
                    RescheduleReason: mData.RescheduleReason,
                    JobCatgForBPid: mData.JobCatgForBPid,
                    JobCatgForBP: mData.JobCatgForBP,
                    ActiveChip: mData.ActiveChip,
                    IsStopPage: mData.IsStopPage,
                    Suggestion: mData.Suggestion,
                    SuggestionCatg: mData.SuggestionCatg,
                    SuggestionDate: mData.SuggestionDate,
                    PlanDateStart: mData.PlanDateStart,
                    PlanDateFinish: mData.PlanDateFinish,
                    closeWoId: mData.closeWoId,
                    CloseWODate: mData.CloseWODate,
                    MaterialRequestNo: mData.MaterialRequestNo,
                    MaterialRequestStatusId: mData.MaterialRequestStatusId,
                    // WoCreatedDate: mData.WoCreatedDate,
                    WoCreatedDate: sampleWoCreatedDate,
                    GateInPushTime: $filter('date')(mData.GateInPushTime, 'yyyy-MM-dd HH:mm:ss'),
                    GateInUserId: mData.GateInUserId,
                    CarWashTime: mData.CarWashTime,
                    CarWashUserId: mData.CarWashUserId,
                    // TecoTime: mData.TecoTime,
                    TecoTime: mData.TecoTime == null ? mData.TecoTime : $filter('date')(mData.TecoTime, 'yyyy-MM-dd HH:mm:ss'),
                    TecoUserId: mData.TecoUserId,
                    BillingCreatedTime: mData.BillingCreatedTime,
                    BillingUserId: mData.BillingUserId,
                    CustCallTime: mData.CustCallTime == null ? mData.CustCallTime : $filter('date')(mData.CustCallTime, 'yyyy-MM-dd HH:mm:ss'),
                    CustCallUserId: mData.CustCallUserId,
                    ServiceExplainCallTime: mData.ServiceExplainCallTime,
                    ServiceExplainUserId: mData.ServiceExplainUserId,
                    AdditionalTimeAmount: mData.AdditionalTimeAmount,
                    IsAllPulledBilling: mData.IsAllPulledBilling,
                    // IsInStopPageGr: mData.IsInStopPageGr,
                    IsInStopPageGr: 0, // kata ijal, kl ada edit wo list, mau itu customer ato job y chip nya yg di stopage naik ke atas (28 Mei 2021)
                    StopPageTimeGr: mData.StopPageTimeGr,
                    StopPageUserIdGr: mData.StopPageUserIdGr,
                    VendorCode: mData.VendorCode,
                    ModelCode: mData.ModelCode,
                    DealerCode: mData.DealerCode,
                    Email: mData.Email,
                    CancelDate: mData.CancelDate,
                    CancelReason: mData.CancelReason,
                    CancelUserId: mData.CancelUserId,
                    isWoFromBpSatelit: mData.isWoFromBpSatelit,
                    BpSatelitOutletId: mData.BpSatelitOutletId,
                    BpSatelitSAUserId: mData.BpSatelitSAUserId,
                    isReqOutPatient: mData.isReqOutPatient,
                    OutPatientUserIdRequest: mData.OutPatientUserIdRequest,
                    OutPatientReqDate: mData.OutPatientReqDate,
                    isOutPatient: mData.isOutPatient,
                    OutPatientUserIdApproved: mData.OutPatientUserIdApproved,
                    OutPatientApproveDate: mData.OutPatientApproveDate,
                    OutPatientApprovedNo: mData.OutPatientApprovedNo,
                    OutPatientUserIdReject: mData.OutPatientUserIdReject,
                    OutPatientRejectDate: mData.OutPatientRejectDate,
                    OutPatientFinishDate: mData.OutPatientFinishDate,
                    SKB: mData.SKB,
                    UserIdSa: mData.UserIdSa,
                    UserSa: mData.UserSa,
                    MProfileForemanId: mData.MProfileForemanId,
                    MProfileForeman: mData.MProfileForeman,
                    UserIdApp: mData.UserIdApp,
                    UserApp: mData.UserApp,
                    AppointmentCreateDate: mData.AppointmentCreateDate,
                    GroupBPId: mData.GroupBPId,
                    groupBP: mData.groupBP,
                    // FixedDeliveryTime1: mData.FixedDeliveryTime1,
                    // FixedDeliveryTime2: mData.FixedDeliveryTime2,
                    FixedDeliveryTime1: FD1Final,
                    FixedDeliveryTime2: FD2Final,
                    FixedDeliveryTime: mData.FixedDeliveryTime,
                    ORAmount: mData.ORAmount,
                    InsuranceId: mData.InsuranceId,
                    Insurance: mData.Insurance,
                    VehicleTypeId: mData.VehicleTypeId,
                    UserIdWORelease: mData.UserIdWORelease,
                    isDraftWo: mData.isDraftWo,
                    AssemblyYearForEstimation: mData.AssemblyYearForEstimation,
                    SendToTowasDate: mData.SendToTowasDate,
                    ReceiveFromTowasDate: mData.ReceiveFromTowasDate,
                    BillingInsuranceType: mData.BillingInsuranceType,
                    statusTowas: mData.statusTowas,
                    responseStatusFromTowas: mData.responseStatusFromTowas,
                    towasConfirmedBySA: mData.towasConfirmedBySA,
                    towasConfirmedUserIdSA: mData.towasConfirmedUserIdSA,
                    WarantyNoFromTowas: mData.WarantyNoFromTowas,
                    AcceptableAmount: mData.AcceptableAmount,
                    ErrCode: mData.ErrCode,
                    TechnicianNotes: mData.TechnicianNotes,
                    ClaimAmount: mData.ClaimAmount,
                    ApprovalRoleId: mData.ApprovalRoleId,
                    ApprovalUserId: mData.ApprovalUserId,
                    ApprovalBy: mData.ApprovalBy,
                    ApprovalTime: mData.ApprovalTime,
                    ApprovalStatus: mData.ApprovalStatus,
                    AddressForEstimation: mData.AddressForEstimation,
                    JobListSplitChip: mData.JobListSplitChip,
                    invoiceType: mData.CodeTransaction,
                    ToyotaIdRequest: mDataCrm.Customer.ToyotaIDFlag == null ? 0 : mDataCrm.Customer.ToyotaIDFlag,
                    Guid_Log: mData.Guid_Log,
                    QueueId: mData.QueueId,
                    IsCustomerRequest : mData.IsCustomerRequest != 1 ? 0 : 1,
                    AppointmentCreateDate: (mData.AppointmentCreateDate == null || mData.AppointmentCreateDate == undefined) ? null : $filter('date')(mData.AppointmentCreateDate, 'yyyy-MM-dd HH:mm:ss'),


                }])
            },
            delete: function (VehicleFareId) {
                return $http.delete('/api/as/Vehiclefare', { data: VehicleFareId, headers: { 'Content-Type': 'application/json' } });
            },
            updateApprovalDp: function (data) {
                console.log("data di updateApproval", data);
                return $http.put('/api/as/RMin_ApprovalChangeDP_TR', [{
                    OutletId: currUser.OrgId,
                    TransactionId: data.TransactionId,
                    JobId: data.JobId,
                    DPDefault: data.DPDefault,
                    DPRequested: data.DPRequested,
                    ApprovalCategoryId: 39,
                    JobPartsId: data.JobPartsId,
                    seq: 1,
                    ApproverId: 1128,
                    RequesterId: 1028,
                    ApproverRoleId: data.ApproverRoleId,
                    RequestorRoleId: data.RequestorRoleId,
                    StatusApprovalId: data.NewStatusApprovalId,
                    VehicleTypeId: data.VehicleTypeId,
                    Discount: 0,
                    RequestReason: data.RequestReason,
                    RequestDate: data.RequestDate,
                    RejectReason: data.Reason,
                    ApproveRejectDate: new Date(),
                    StatusCode: 1,
                }]);
            },
            updateApprovalDiskonTasks: function (data) {
                console.log("data di updateApproval", data, 'api/as/ApprovalDiscount/ApproveDiscount');
                return $http.put('/api/as/NewSAggrement_ApprovalDiscount_TR', [{
                // return $http.put('/api/as/SAggrement_ApprovalDiscount_TR', [{
                    // OutletId: currUser.OrgId,
                    OutletId: data.OutletId,
                    // TransactionId: data.TransactionId,
                    TransactionId: data.ApprovalDiscountId,
                    JobId: data.JobId,
                    JobTaskId: data.JobTaskId,
                    ApprovalCategoryId: 45,
                    DiscountedPrice: data.DiscountedPrice,
                    NormalPrice: data.NormalPrice,
                    seq: data.Sequence,
                    ApproverId: null,
                    RequesterId: null,
                    ApproverRoleId: data.ApproverRoleId,
                    RequestorRoleId: data.RequestorRoleId,
                    StatusApprovalId: data.NewStatusApprovalId,
                    Discount: data.Discount,
                    RequestDate: data.RequestDate,
                    Reason: data.Reason,
                    ApproveRejectDate: new Date(),
                    StatusCode: 1,
                }]);
            },
            approveCustomDiscount: function (data) {
                console.log("data di updateApproval", data, 'api/as/ApprovalDiscount/ApproveDiscount');
                return $http.put('/api/as/ApprovalDiscount/ApproveDiscount', [{
                    OutletId: currUser.OrgId,
                    ApprovalDiscountId: data.ApprovalDiscountId,
                    JobTaskId: data.JobTaskId,
                    JobPartId: data.JobPartId,
                    Sequence: data.Sequence,
                    ApprovalRoleId: data.ApprovalRoleId,
                    CreateDate: data.RequestDate,
                    // StatusApproval: data.StatusApproval,
                    StatusApproval: data.NewStatusApprovalId,
                    Discount: data.Discount,
                }]);
            },
            postJobReduction: function (jobId, data) {
                console.log("data di postJobReduction", data);
                return $http.post('/api/as/PostJobReduction', [{
                    JobId: jobId,
                    JobTasks: data
                }]);
            },
            ReqCancelWO: function (jobId) {
                console.log("data di ReqCancelWO", jobId);
                return $http.post('/api/as/PostApprovalCancelWO/1', [{
                    JobId: jobId,
                    StatusApprovalId: 3,
                    // JobTasks: data
                }]);
            },
            GetAllApprovalCancelWO: function (jobId) {
                console.log("data di ReqCancelWO", jobId);
                return $http.get('/api/as/GetAllApprovalCancelWO');
            },
            CheckIsJobCancelWO: function (jobId) {
                console.log("data di ReqCancelWO", jobId);
                return $http.get('/api/as/CheckIsJobCancelWO/'+jobId);
            },
            
            updateApprovalDiskonParts: function (data) {
                console.log("data di updateApproval", data);
                return $http.put('/api/as/NewSAggrement_ApprovalDiscountPart_TR', [{
                // return $http.put('/api/as/SAggrement_ApprovalDiscountPart_TR', [{
                    OutletId: currUser.OrgId,
                    // TransactionId: data.TransactionId,
                    TransactionId: data.ApprovalDiscountId,
                    JobId: data.JobId,
                    JobPartId: data.JobPartId,
                    ApprovalCategoryId: 45,
                    DiscountedPrice: data.DiscountedPrice,
                    NormalPrice: data.NormalPrice,
                    seq: data.Sequence,
                    ApproverId: null,
                    RequesterId: null,
                    ApproverRoleId: data.ApproverRoleId,
                    RequestorRoleId: data.RequestorRoleId,
                    StatusApprovalId: data.NewStatusApprovalId,
                    Discount: data.Discount,
                    RequestDate: data.RequestDate,
                    Reason: data.Reason,
                    ApproveRejectDate: new Date(),
                    StatusCode: 1,
                }]);
            },

            updateApprovalJobReduction: function (data) {
                console.log("data di updateApproval", data);
                return $http.put('/api/as/JReduction_ApprovalWORelease_TR', [{
                    OutletId: currUser.OrgId,
                    TransactionId: data.TransactionId,
                    JobId: data.JobId,
                    JobTaskId: data.JobTaskId,
                    ApprovalCategoryId: 49,
                    seq: data.seq,
                    ApproverId: null,
                    RequesterId: null,
                    ApproverRoleId: data.ApproverRoleId,
                    RequestorRoleId: data.RequestorRoleId,
                    StatusApprovalId: data.NewStatusApprovalId,
                    RequestReason: data.RequestReason,
                    RequestDate: data.RequestDate,
                    Reason: data.Reason,
                    ApproveRejectDate: new Date(),
                    StatusCode: 1,
                }]);
            },

            updateApprovalCancelWO: function (data) {
                console.log("data di updateApproval", data);
                return $http.put('/api/as/ApproveRejectCancelWO', [{
                    TransactionId: data.TransactionId,
                    JobId: data.JobId,
                    StatusApprovalId: data.NewStatusApprovalId,
                    Reason: data.Reason,
                    StatusCode: 1,
                }]);
            },


            getWoListGr: function (paramType, paramDate1, paramDate2, ParamWO, ParamNoPolice, typeParam, tipeDefault, showdatakabeng) {
                var WoNo = ParamWO.replace(/\//g, '%20');
                var woNoFinal = angular.copy(WoNo);
                if (woNoFinal.includes("*")) {
                    woNoFinal = woNoFinal.split('*');
                    woNoFinal = woNoFinal[0];
                }

                var PoliceNo = ParamNoPolice.replace(/\//g, '%20');
                var PoliceNoFinal = angular.copy(PoliceNo);
                if (PoliceNoFinal.includes("*")) {
                    PoliceNoFinal = PoliceNoFinal.split('*');
                    PoliceNoFinal = PoliceNoFinal[0];
                }

                if (typeParam == 0) {
                    if (currUser.RoleId == 1128 || currUser.RoleId == 1174 || currUser.RoleId == 1121 || currUser.RoleId != 1028 && showdatakabeng != undefined) {
                        if ((currUser.RoleId == 1128 || currUser.RoleId == 1174 || currUser.RoleId != 1028) && showdatakabeng == 6){
                            return $http.get('/api/as/jobs/WoListAktifSA/FirstDate/' + paramDate1 + '/LastDate/' + paramDate2 +
                                '/gr/1/jenisFilter/' + paramType + '/wono/' + WoNo + '/plateNo/' + ParamNoPolice + '/Approver/' + 0 + '/isDefault/' + 2); // di hardcode 2 biar muncul data semua sa
                        } else {
                            return $http.get('/api/as/jobs/WoListAktifSA/FirstDate/' + paramDate1 + '/LastDate/' + paramDate2 +
                                '/gr/1/jenisFilter/' + paramType + '/wono/' + WoNo + '/plateNo/' + ParamNoPolice + '/Approver/' + currUser.RoleId + '/isDefault/' + 0);
                        }
                    } else {
                        return $http.get('/api/as/jobs/WoListAktifSA/FirstDate/' + paramDate1 + '/LastDate/' + paramDate2 +
                            '/gr/1/jenisFilter/' + paramType + '/wono/' + WoNo + '/plateNo/' + ParamNoPolice + '/Approver/' + 0 + '/isDefault/' + tipeDefault);
                    }
                } else {
                    if (currUser.RoleId == 1128 || currUser.RoleId == 1174 || currUser.RoleId == 1121) {
                        return $http.get('/api/as/jobs/WoListAktifSALike/FirstDate/' + paramDate1 + '/LastDate/' + paramDate2 +
                            '/gr/1/jenisFilter/' + paramType + '/wono/' + woNoFinal + '/plateNo/' + PoliceNoFinal + '/Approver/' + currUser.RoleId + '/isDefault/' + 0);
                    } else {
                        return $http.get('/api/as/jobs/WoListAktifSALike/FirstDate/' + paramDate1 + '/LastDate/' + paramDate2 +
                            '/gr/1/jenisFilter/' + paramType + '/wono/' + woNoFinal + '/plateNo/' + PoliceNoFinal + '/Approver/' + 0 + '/isDefault/' + tipeDefault);
                    }
                }
            },
            getDataTaskOpe: function (Key, Katashiki, catg, vehicleModelId) {
                var res = $http.get('/api/as/TaskListsCode?KatashikiCode=' + Katashiki + '&Name=' + Key + '&TaskCategory=' + catg + '&VehicleModelId=' + vehicleModelId);
                // var res=$http.get('/api/as/TaskLists?KatashikiCode=ASV50R-JETGKD&Name=&TaskCategory='+catg);
                console.log('/api/as/TaskLists?KatashikiCode=' + Katashiki + '&Name=' + Key + '&TaskCategory=' + catg + '&VehicleModelId=' + vehicleModelId);
                // console.log("resnya",res);
                return res;
            },
            getWObyJobId: function (id) {
                return $http.get('/api/as/Jobs/' + id);
            },
            getCategoryWO: function () {
                return $http.get('/api/as/GlobalMaster?CategoryId=1007');
            },
            getAllOPL: function () {
                return $http.get('/api/as/OPL');
            },
            getAllApprovalJobTask: function (OutletId, RoleId, Mode, isGr, status) {
                var res = $http.get('/api/as/ApprovalDiscount/GetDataApprovalDiscount', {
                    params: {
                        OutletId: OutletId,
                        RoleId: RoleId,
                        Mode: Mode,
                        IsGR: isGr,
                        Status: status
                    }
                });
                return res;
            },
            getAllApprovalJobPart: function () {
                var res = $http.get('/api/as/GetAllApprovalJobPart', {
                    params: {
                        OutletId: OutletId,
                        RoleId: RoleId,
                        Mode: Mode
                    }
                });

                return res;
            },
            getDiscountMGroupCustomer: function (data) {
                return $http.put('/api/crm/PutDiscountMGroupCustomer', [{
                    Name: data.CustomerName,
                    Phone1: data.Handphone1,
                    Phone2: data.Handphone2,
                    KtpNo: data.KTPKITAS
                }]);
            },
            getDiscountCampaign: function (today, data, crm, IsGR) {
                var KmDiscount = 0;
                if (data.Km !== undefined && data.Km !== null) {
                    if (typeof data.Km === 'string') {
                        var tmpKm = angular.copy(data.Km);
                        if (tmpKm.includes(",")) {
                            tmpKm = tmpKm.split(',');
                            if (tmpKm.length > 1) {
                                tmpKm = tmpKm.join('');
                            } else {
                                tmpKm = tmpKm[0];
                            }
                        }
                        tmpKm = parseInt(tmpKm);
                        KmDiscount = tmpKm;
                    } else {
                        KmDiscount = data.Km;
                    }
                }
                console.log("getDiscountCampaign", today, data, crm);
                // '/api/as/CampaignDiscount/Date/2017-08-07/KM/2100/AYear/2015/VechId/4642'
                return $http.get('/api/as/CampaignDiscount/Date/' + today + '/KM/' + KmDiscount + '/AYear/' + crm.Vehicle.AssemblyYear + '/VechId/' + crm.Vehicle.Type.VehicleTypeId + '/IsGR/' + IsGR);
            },
            getDiscountCampaignSecond: function (today, data, crm, IsGR) {
                var KmDiscount = 0;
                if (data.Km !== undefined && data.Km !== null) {
                    if (typeof data.Km === 'string') {
                        var tmpKm = angular.copy(data.Km);
                        if (tmpKm.includes(",")) {
                            tmpKm = tmpKm.split(',');
                            if (tmpKm.length > 1) {
                                tmpKm = tmpKm.join('');
                            } else {
                                tmpKm = tmpKm[0];
                            }
                        }
                        tmpKm = parseInt(tmpKm);
                        KmDiscount = tmpKm;
                    }
                }
                console.log("getDiscountCampaign", today, data, crm);
                // '/api/as/CampaignDiscount/Date/2017-08-07/KM/2100/AYear/2015/VechId/4642'
                return $http.get('/api/as/CampaignDiscount/Date/' + today + '/KM/' + KmDiscount + '/AYear/' + crm.Vehicle.AssemblyYear + '/VechId/' + data.VehicleTypeId + '/IsGR/' + IsGR);
            },
            postApprovalDiscountTask: function (jobId, data) {
                console.log("dataaaaa", data);
                return $http.post('/api/as/NewPostApprovalDiscountWOTaskPart', [{
                    JobId: jobId,
                    JobTask: data
                }]);
            },
            closeWO: function (id) {
                return $http.put('/api/as/Jobs/CloseWo/', [id]);
            },
            getAllApprovalJobReductionGR: function () {
                return $http.get('/api/as/GetAllApprovalJobReductionGR')
            },
            getEmployeeId: function (userName) {
                return $http.get('/api/as/getEmployeeId/' + userName);
            },
            getUserIdApprover: function (outletId, roleId) {
                return $http.get('/api/as/ApprovalDiscount/GetUserIdApprover', {
                    params: {
                        vRoleId: roleId,
                        vOutletId: outletId
                    }
                });
            },
            sendNotifApprove: function (data, userId) {
                console.log("data notif ke partmans");
                return $http.post('/api/as/SendNotification', [{
                    Message: data.Reason,
                    RecepientId: userId,
                    Param: 8
                }]);
            },
            postCountingPrint: function (jobid, typeId) {
                return $http.post("/api/as/PostPrint", [{
                    JobId: jobid,
                    PrintCountTypeId: typeId,
                    StatusCode: 1
                }]);
            },
            getJobParts: function (jobId, isGr) {
                return $http.get('/api/as/jobs/listJobPartList/' + jobId + '/3/' + isGr);
            },
            checkApprovalWarranty: function (data) {
                return $http.get('/api/as/CheckRejectedWarranty/' + data.JobId);
            },
            getReqBPCenter: function (JobId) {
                return $http.get('/api/as/GetAppointmentRequestByJobId/' + JobId);
            },
            updateReqBPCenter: function (data) {
                return $http.put('/api/as/AppointmentRequests/', [data]);
            },
            getStatWO: function (JobId, status) {
                return $http.get('/api/as/Jobs/GetStatusJob/' + JobId + '/' + status);
            },
            GetStatusApprovalWarranty: function (JobId) {
                return $http.get('/api/as/GetStatusApprovalWarranty/' + JobId);
            },
            getCheckStatusParts: function (JobId, data) {
                console.log('jobid ====>', JobId);
                console.log('ihhh datany', data)
                return $http.post('/api/as/JobParts/CekStatusParts/' + JobId, data);
            },
            getCheckStatusPartsforPO: function (JobId, data) {
                console.log('jobid ====>', JobId);
                console.log('ihhh datany', data)
                return $http.post('/api/as/JobParts/CekStatusPartsPO/' + JobId, data);
            },

            updateAfterPrintHistory: function (idtrans, status) {
                return $http.put('/api/as/approv/UpdateStatusApprovalServiceHistory/' + idtrans + '/' + status);
            },
            // insertServiceHistory: function(data) {
            //     return $http.post('/api/ct/InsertServiceHistory', [data])
            // },
            sendNotif: function (data, recepient, param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param: param
                }]);
            },


            sendNotifOne: function (data, recepient, Param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotification', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param: Param
                }]);
            },

            // ===================================================  ini factory pindahan dari menu lain ================================= start


            GetCAddressCategory: function () {
                var res = $http.get('/api/ct/GetCAddressCategory/');

                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },

            updateStatusCancelOPLMore: function (data) {
                console.log('data updateStatusCancelOPL', data);
                // return $http.post('/api/as/JobTaskOpls/Cancel', [{ JobTaskOplId: data.JobTaskOplId }]);
                return $http.post('/api/as/JobTaskOpls/Cancel', data);
            },

            getPreDiagnoseByJobId: function (id) {
                return $http.get('/api/as/GetPrediagnoses/job/' + id);
            },

            getListStall: function () {
                var res = $http.get('/api/as/Boards/Stall/' + 1);
                return res
            },

            getDataTaskListByKatashiki: function (key) {
                var res = $http.get('/api/as/TaskLists/TasklistSA?KatashikiCode=' + key);
                return res
            },

            updatePrediagnose: function (mData) {
                return $http.put('/api/as/PutPrediagnoses', [{
                    PrediagnoseId: mData.PrediagnoseId,
                    JobId: mData.JobId,
                    FirstIndication: mData.FirstIndication,
                    FrequencyIncident: mData.FrequencyIncident,
                    Mil: mData.Mil,
                    ConditionIndication: mData.ConditionIndication,
                    MachineCondition: mData.MachineCondition,
                    MachineConditionTxt: mData.MachineConditionTxt,
                    PositionShiftLever: mData.PositionShiftLever,
                    PositionShiftLeverTxt: mData.PositionShiftLeverTxt,
                    VehicleSpeed: mData.VehicleSpeed,
                    RPM: mData.RPM,
                    VehicleLoads: mData.VehicleLoads,
                    PassengerNumber: mData.PassengerNumber,
                    ClassificationProblemR: mData.ClassificationProblemR,
                    ClassificationProblemRTxt: mData.ClassificationProblemRTxt,
                    Traffic: mData.Traffic,
                    TrafficTxt: mData.TrafficTxt,
                    WeatherConditions: mData.WeatherConditions,
                    WeatherConditionsTxt: mData.WeatherConditionsTxt,
                    CustomerFinalConfirmation: mData.CustomerFinalConfirmation,
                    ClassificationProblemM: mData.ClassificationProblemM,
                    ClassificationProblemMTxt: mData.ClassificationProblemMTxt,
                    BlowerSpeed: mData.BlowerSpeed,
                    TempSetting: mData.TempSetting,

                    SittingPosition: mData.SittingPosition,
                    TechnicalSupport: mData.TechnicalSupport,
                    PreDiagnoseTxt: mData.PreDiagnoseTxt,
                    JobDetail: mData.JobDetail,

                    notes: mData.notes,
                    IsNeedTest: mData.IsNeedTest,
                    FinalConfirmation: mData.FinalConfirmation,
                    CustomerConfirmation: mData.CustomerConfirmation,
                    LastModifiedUserId: mData.LastModifiedUserId,
                    LastModifiedDate: mData.LastModifiedDate,
                    // ComplaintCatId : mData.ComplaintCatId,
                    // DetailJSON : mData.DetailJSON,
                    ScheduledTime: mData.ScheduledTime,
                    StallId: mData.StallId,
                    OutletId: mData.OutletId
                }]);
            },

            createPreadiagnose: function (mData) {
                // var tmpItemDate = angular.copy(mData.FirstIndication);
                // console.log("changeFormatDate item",tmpItemDate);
                // tmpItemDate = new Date(tmpItemDate);
                // console.log("tmpItemDate",tmpItemDate);
                // var finalDate = '';
                // var yyyy = tmpItemDate.getFullYear().toString();
                // var mm = (tmpItemDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                // var dd = (tmpItemDate.getDate()).toString();

                // // finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                // finalDate = yyyy + '-' + mm + '-' + dd;
                finalDate = this.changeFormatDate(mData.FirstIndication);
                console.log("finalDate", finalDate)
                console.log("tahu >>", mData);
                return $http.post('/api/as/PostPrediagnoses', [{
                    JobId: mData.JobId,
                    FirstIndication: finalDate,
                    FrequencyIncident: mData.FrequencyIncident,
                    Mil: mData.Mil,
                    ConditionIndication: mData.ConditionIndication,
                    MachineCondition: mData.MachineCondition,
                    MachineConditionTxt: mData.MachineConditionTxt,
                    PositionShiftLever: mData.PositionShiftLever,
                    PositionShiftLeverTxt: mData.PositionShiftLeverTxt,
                    VehicleSpeed: mData.VehicleSpeed,
                    RPM: mData.RPM,
                    VehicleLoads: mData.VehicleLoads,
                    PassengerNumber: mData.PassengerNumber,
                    ClassificationProblemR: mData.ClassificationProblemR,
                    ClassificationProblemRTxt: mData.ClassificationProblemRTxt,
                    Traffic: mData.Traffic,
                    TrafficTxt: mData.TrafficTxt,
                    WeatherConditions: mData.WeatherConditions,
                    WeatherConditionsTxt: mData.WeatherConditionsTxt,
                    CustomerFinalConfirmation: mData.CustomerFinalConfirmation,
                    ClassificationProblemM: mData.ClassificationProblemM,
                    ClassificationProblemMTxt: mData.ClassificationProblemMTxt,
                    BlowerSpeed: mData.BlowerSpeed,
                    TempSetting: mData.TempSetting,

                    SittingPosition: mData.SittingPosition,
                    TechnicalSupport: mData.TechnicalSupport,
                    PreDiagnoseTxt: mData.PreDiagnoseTxt,
                    JobDetail: mData.JobDetail,

                    notes: mData.notes,
                    IsNeedTest: mData.IsNeedTest,
                    FinalConfirmation: mData.FinalConfirmation,
                    CustomerConfirmation: mData.CustomerConfirmation,
                    LastModifiedUserId: mData.LastModifiedUserId,
                    LastModifiedDate: mData.LastModifiedDate,
                    // ComplaintCatId : mData.ComplaintCatId,
                    // DetailJSON : mData.DetailJSON,
                    ScheduledTime: mData.ScheduledTime,
                    StallId: mData.StallId,
                    OutletId: mData.OutletId
                }]);
            },

            getWoCategory: function () {
                var catId = 1007;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },

            getDataVehicle: function (key) {
                var res = $http.get('/api/crm/GetDataInfo/' + key);
                return res;
            },

            getDataVehiclePSFU: function(key) {
                var res = $http.get('/api/crm/GetVehicleListById/' + key);
                return res;
            },
            getQuestion: function() {
                return $http.get('/api/as/QuestionsManagement');
            },

            sendToCRMHistory: function (data) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/crm/InsertCHistoryDataChanged/', [{
                    CustomerId: data.CustomerId,
                    ChangeTypeId: data.ChangeTypeId,
                    Modul: data.Modul,
                    Attribute: data.Attribute,
                    NewValue: data.NewValue,
                    OldValue: data.OldValue
                }]);
            },

            getPayment: function () {
                var catId = 1008;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },

            getUnitMeasurement: function () {
                var catId = 1;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },

            getTaskCategory: function () {
                var catId = 1010;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },

            getDataPartsByTaskId: function (key) {
                var res = $http.get('/api/as/TaskLists/Parts?TaskId=' + key);
                return res
            },

            getDataTask: function (Key, Katashiki, catg, vehicleModelId) {
                var res = $http.get('/api/as/TaskLists?KatashikiCode=' + Katashiki + '&Name=' + Key + '&TaskCategory=' + catg + '&VehicleModelId=' + vehicleModelId);
                // var res=$http.get('/api/as/TaskLists?KatashikiCode=ASV50R-JETGKD&Name='+Key+'&TaskCategory='+catg);
                console.log('/api/as/TaskLists?KatashikiCode=' + Katashiki + '&Name=' + Key + '&TaskCategory=' + catg + '&VehicleModelId=' + vehicleModelId);
                // console.log("resnya",res);
                return res;
            },

            getDataParts: function (Key, servicetype, type) {
                console.log("keyyyyyy", Key);
                var res = $http.get('/api/as/StockAdjustment/GetStockAdjustmentDetailFromMaterial?PartsCode=' + Key + '&ServiceTypeId=' + servicetype + '&PartsClassId1=' + type);
                // console.log("/api/as/TaskLists?KatashikiCode="+Katashiki+"&Name="+Key);
                // console.log("resnya",res);
                return res;
            },

            getAvailableParts: function (item, jobid, qtyinput) {
                // /api/as/jobs/getAvailableParts/{partid}
                // var res = $http.get('/api/as/jobs/getPartsPriceAndDefaultETDETA/' + item);
                if (typeof item == 'string') {
                    if (item.includes('&')) {
                        item = item.split("&")[0];
                    }
                }
                if (jobid === undefined || jobid == null) {
                    jobid = 0;
                }
                var res = $http.get('/api/as/jobs/getAvailablePartsAppointment/' + item + '/1?JobId=' + jobid + '&Qty=' + qtyinput);
                return res
            },

            GetStatusTowas: function (JobId) {
                var res = $http.get('/api/as/Jobs/GetStatusTowas/' + JobId);
                //console.log('res=>',res);
                //res.data.Result = null;
                return res;
            },

            getVehicleTypeById: function (id) {
                var url = '/api/crm/GetCVehicleTypeById/' + id
                var res = $http.get(url);
                return res;
            },

            getVehicleTypeColorById: function (id) {
                var url = '/api/crm/GetMVehicleTypeColorById/' + id
                var res = $http.get(url);
                return res;
            },
            NewGetMVehicleTypeColorById: function(id) {
                var url = '/api/crm/NewGetMVehicleTypeColorById/' + id
                var res = $http.get(url);
                return res;
            },

            getDataPKS: function (PoliceNumber) {
                return $http.get('/api/as/PKS/GetByLicensePlate/' + PoliceNumber);
            },

            GetDEC: function (vin) {
                return $http.get('/api/as/getDEC/' + vin);
            },

            CheckNopolQueueListSecurity: function (type, nopol, jobid) {
                var res = $http.get('/api/as/CheckPoliceNoAvalailability/' + type + '/' + nopol + '/' + jobid);
                return res
            },

            CheckVehicleStatusStock: function(vin) {
                // api/crm/CheckVehicleStatusStock/{VehicleId}
                var res = $http.get('/api/crm/CheckVehicleStatusStock/' + vin);
                return res
            },

            CheckVINisOK: function(VIN, PoliceNo, Flag) {
                var res = $http.put('/api/crm/CheckVINisOK/' + VIN + '/' + PoliceNo + '/' + Flag);
                return res
            },

            getTowass: function (vin) {
                var res = $http.get('/api/as/Towass/getFieldAction/1/' + vin);
                return res;
            },

            updatePKS: function (PoliceNumber, totalAkhir) {
                totalAkhir = Math.ceil(totalAkhir) // di round up karena pernah ada totalnya koma, dan karena tagihan y round up hrs nya
                return $http.get('/api/as/PKS/CekPKS/' + PoliceNumber + '/' + totalAkhir);
            },

            newCustomerVehicleUser: function (vID, cID) {
                return $http.post('/api/crm/PostCustomerVehicleList/', [{
                    CustomerOwnerId: cID,
                    VehicleId: vID,
                    CurrentStatus: 1,
                    CreatedDate: new Date(),
                    CreatedUserId: currUser.UserId

                }])
            },

            createApprovalWarranty: function (tmpData) {
                console.log("jobid", tmpData);
                // for (var i = 0; i < mData.length; i++) {
                // return $http.post('/api/as/ASubmited_ApprovalTOWASS_TR', tmpData);
                return $http.post('/api/as/PostTaskListClaim', tmpData);
                //         "NoClaim": "57812",
                //         // "JobId": mData[i].JobId,
                //         "JobTaskId":[]
                //         "ApprovalCategoryId": 43,
                //         // "ApproverId": null,
                //         // "RequesterId": 1231,
                //         "ApproverRoleId": null,
                //         "RequestorRoleId": 1128,
                //         "StatusApprovalId": 3,
                //         "CategoryServices": 1, // kategori pekerjaan
                //         "RequestReason": mDataIsi.Requestmessage,
                //         "RequestDate": null, // tanggal sekarang
                //         // "RejectReason": null
                //         // ,
                //         // "ApproveRejectDate": null
                //     }]);
                // }
            },

            postApprovalDiscountTaskWO: function (jobId, data, ApprovalCategoryId) {
                console.log("dataaaaa", data);
                return $http.post('/api/as/PostApprovalDiscountTask', [{
                    JobId: jobId,
                    JobTask: data,
                    ApprovalCategoryId: ApprovalCategoryId
                }]);
            },

            createEXT: function (data) {
                return $http.post('/api/as/JobWACExts', data);
            },

            OutletBPCenter: function (id) {
                return $http.get('/api/as/OutletBPCenter?OutletBPCenter=' + id)
            },

            getListVehicleCPPK: function (id) {
                return $http.get('/api/crm/GetListVehicleUserPhone/' + id);
            },

            getVehicleUser: function (id) {
                return $http.get('/api/crm/GetVehicleUser/' + id);
            },

            getMRS: function (ReminderTypeId) {
                var res = $http.get('/api/as/Towass/getFieldMRS/' + ReminderTypeId);
                return res;
            },

            createNewVehicleNonTAM: function (data) {
                console.log("createNewVehicleNonTAM", data);
                return $http.post('/api/crm/PostVehicleNonTAM/', [{
                    VIN: data.Vehicle.VIN,
                    LicensePlate: data.Vehicle.LicensePlate,
                    EngineNo: data.Vehicle.EngineNo,
                    AssemblyYear: data.Vehicle.AssemblyYear,
                    DECDate: data.Vehicle.DECDate,
                    STNKName: data.Vehicle.STNKName,
                    STNKAddress: data.Vehicle.STNKAddress,
                    KatashikiCode: data.Vehicle.KatashikiCode,
                    STNKDate: data.Vehicle.STNKDate,
                    GasTypeId: data.Vehicle.GasTypeId,
                    OutletId: currUser.OrgId,
                    ModelName: data.Vehicle.ModelName,
                    ModelType: data.Vehicle.ModelType,
                    ColorName: data.Vehicle.ColorName,
                    KatashikiCode: data.Vehicle.KatashikiCodeNonTAM
                }]);
            },

            updateVehicleNonTAM: function (data) {
                console.log("updateVehicleNonTAM", data);
                return $http.put('/api/crm/PutVehicleNonTAM/', [{
                    VehicleId: data.Vehicle.VehicleId,
                    VIN: data.Vehicle.VIN,
                    LicensePlate: data.Vehicle.LicensePlate,
                    EngineNo: data.Vehicle.EngineNo,
                    AssemblyYear: data.Vehicle.AssemblyYear,
                    DECDate: data.Vehicle.DECDate,
                    KatashikiCode: data.Vehicle.KatashikiCode,
                    STNKName: data.Vehicle.STNKName,
                    STNKAddress: data.Vehicle.STNKAddress,
                    STNKDate: data.Vehicle.STNKDate,
                    GasTypeId: data.Vehicle.GasTypeId,
                    OutletId: currUser.OrgId,
                    ModelName: data.Vehicle.ModelName,
                    ModelType: data.Vehicle.ModelType,
                    ColorName: data.Vehicle.ColorName,
                    KatashikiCode: data.Vehicle.KatashikiCodeNonTAM
                }]);
            },

            getTransactionCode: function () {
                // var catId = 2028;
                // var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                // console.log('resnya getTransactionCode=>', res);
                // return res;

                return $http.get('/api/as/InvoiceTypes');
            },

            getDiscountMGroupCustomerWO: function (data) {
                console.log('data untuk discountMGroupCust', data)
                var Nama = '';
                var tlp1 = '';
                var tlp2 = '';
                var ktp = '';
                var npwp = '';
                if (data.CustomerTypeId == 3 || data.CustomerTypeId == 1) {
                    Nama = data.Name;
                    tlp1 = data.HandPhone1;
                    tlp2 = data.HandPhone2;
                    ktp = data.KTPKITAS;
                    npwp = data.Npwp;
                } else {
                    Nama = data.Name;
                    // tlp1 = data.PICHp;
                    npwp = data.Npwp;
                }
                if (Nama == '' || Nama == undefined || Nama == null){
                    Nama = '-';
                }
                if (tlp1 == '' || tlp1 == undefined || tlp1 == null){
                    tlp1 = '-';
                }
                if (tlp2 == '' || tlp2 == undefined || tlp2 == null){
                    tlp2 = '-';
                }
                if (ktp == '' || ktp == undefined || ktp == null){
                    ktp = '-';
                }
                if (npwp == '' || npwp == undefined || npwp == null){
                    npwp = '-';
                }
                return $http.put('/api/crm/PutDiscountMGroupCustomer', [{
                    // Name: data.Name,
                    // Phone1: data.HandPhone1,
                    // Phone2: data.HandPhone2,
                    // KtpNo: data.KTPKITAS,
                    // NPWP: data.Npwp
                    Name: Nama,
                    Phone1: tlp1,
                    Phone2: tlp2,
                    KtpNo: ktp,
                    NPWP: npwp
                }]);
            },

            getProbPoints: function (JobId, TypeId) {
                return $http.get('/api/as/JobWACExts/Job/' + JobId + '/' + TypeId);
            },

            updateStatusForWO: function (JobId, toStat) {
                return $http.put('/api/as/UpdateJob/' + JobId + '/StatusTo/' + toStat);
            },
            updateStatusForWONew: function (JobId, toStat, flag) {
                return $http.put('/api/as/UpdateJob/' + JobId + '/StatusTo/' + toStat + '/flag/'+ flag);
            },

            updateTecoOnlyWarranty: function (item) {
                console.log("itemnya", item);
                return $http.put('/api/as/Jobs/TecoStart/1', [
                    item
                ]);
            },

            getWarrantyInfo: function (JobId) {
                return $http.get('/api/as/GetWarrantyInfo/' + JobId + '/1');
            },

            getBPCenter: function () {
                return $http.get('/api/as/getOutletBPCenter');
            },

            bpCenterTrigger: function (data) {
                return $http.post('/api/as/AppointmentRequestsBPCenter', [data]);
            },

            getAvailablePartsWO: function (item, jobid, qtyinput, jobpartsid) {
                // /api/as/jobs/getAvailableParts/{partid}
                // var res = $http.get('/api/as/jobs/getPartsPriceAndDefaultETDETA/' + item);
                if (typeof item == 'string') {
                    if (item.includes('&')) {
                       item = item.split("&")[0];
                    }
                }
                if (jobid === undefined || jobid == null) {
                    jobid = 0;
                }
                if (jobpartsid === undefined || jobpartsid === null){
                    jobpartsid = 0
                }
                var res = $http.get('/api/as/jobs/getAvailableParts/' + item + '/1?JobId=' + jobid + '&Qty=' + qtyinput + '&JobPartsId=' + jobpartsid);
                return res
            },

            CancelWOWalkin: function (data) {
                if (data.PoliceNumber == null || data.PoliceNumber == undefined) {
                    data.PoliceNumber = data.LicensePlate;
                }
                return $http.put('/api/as/Gate/updateCancelWO', [{
                    PoliceNo: data.PoliceNumber,
                    nowTime: new Date(),
                    Id: data.Id
                }])
            },

            getAllTypePoints: function () {
                return $http.get('/api/as/TypePoints');
            },

            getWACBPbyItemId: function (jobItemId) {
                // var WAC = function
                return $http.get('/api/as/JobWACBPs/getByItem/' + jobItemId);
            },

            GetOplList: function (key, model) {
                var res = $http.get('/api/as/OPLFilter?oplname=' + key + '&modelName=' + model);
                return res
            },

            checkGI: function (item) {
                var res = $http.get('/api/as/jobs/cekGI/' + item.JobId);
                return res
            },

            CheckIsWarrantyAktif: function(JobId) {
                console.log("data checkApproval", JobId);
                return $http.get('/api/as/CheckIsWarrantyAktif/' + JobId);
            },

            checkIsWarranty: function (JobId) {
                console.log("data checkApproval", JobId);
                return $http.get('/api/as/IsWarranty/' + JobId);
            },

            getWobyJobId: function (filter) {
                var res = $http.get('/api/as/Jobs/' + filter);
                return res;
            },

            CekStatusJobBilling: function (JobId, flag) {
                // console.log("cek data", data);
                var res = $http.get('/api/as/Billings/CekStatusJob/' + JobId + '/' + flag);
                return res;
            },

            updateUserList: function (data, CVId) {
                console.log('updateUserList >>>', data, CVId);
                return $http.put('/api/crm/PutVehicleUser/', [{
                    VehicleUserId: data.VehicleUserId,
                    CustomerVehicleId: CVId,
                    // Name: data.name,
                    Name: this.checkIsCharacter(data.name),
                    RelationId: data.Relationship,
                    Phone1: data.phoneNumber1,
                    Phone2: data.phoneNumber2,
                    Status: 1,
                    StatusCode: 1
                }])
            },

            createNewUserList: function (data, CVId) {
                console.log('createNewUserList >>>', data, CVId);
                return $http.post('/api/crm/PostVehicleUser/', [{
                    CustomerVehicleId: CVId,
                    // Name: data.name,
                    Name: this.checkIsCharacter(data.name),
                    RelationId: data.Relationship,
                    Phone1: data.phoneNumber1,
                    Phone2: data.phoneNumber2,
                    Status: 1,
                    StatusCode: 1
                }])
            },

            getVehicleMList: function () {
                return $http.get('/api/crm/GetCVehicleModel/');
            },

            postJobWAC: function (data) {
                data.forEach(function (val) {
                    val.moneyAmount = (typeof val.moneyAmount != 'undefined' || val.moneyAmount != null ? val.moneyAmount : 0); //Billy
                });
                return $http.post('/api/as/JobWACs', data);
            },

            getCustomerVehicleList: function (value) {
                var param1 = value.TID ? value.TID : '-';
                if (value.TID == '' || value.TID == null || value.TID == undefined) {
                    param1 = '-'
                } else {
                    param1 = value.TID
                }
                var param2 = '-';
                var param3 = '-';
                var param4 = '-';
                var param5 = '-';
                var param6 = '-';
                var param7 = '-';
                var param8 = '-';
                switch (value.flag) {
                    case 1:
                        param2 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined) {
                            param2 = '-'
                        } else {
                            param2 = value.filterValue
                        }
                        break;
                    case 2:
                        param3 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined) {
                            param3 = '-'
                        } else {
                            param3 = value.filterValue
                        }
                        break;
                    case 3:
                        param4 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined) {
                            param4 = '-'
                        } else {
                            param4 = value.filterValue
                        }
                        break;
                    case 4:
                        param5 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined) {
                            param5 = '-'
                        } else {
                            param5 = value.filterValue
                        }
                        break;
                    case 5:
                        param6 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined) {
                            param6 = '-'
                        } else {
                            param6 = value.filterValue
                        }
                        break;
                    case 6:
                        param7 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined) {
                            param7 = '-'
                        } else {
                            param7 = value.filterValue
                        }
                        break;
                    case 7:
                        // param8 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param8 = '-'
                        } else {
                            param8 = angular.copy(value.filterValue.replace(/\./g, "z")) 
                            param8 = angular.copy(param8.replace(/\-/g, "x"))

                        }

                };

                return $http.get('/api/crm/GetCustomerListFilter/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7 + '/' + param8);
            },

            getCustomerVehicleList_V2: function (value) {
                var param1 = value.TID ? value.TID : '-';
                if (value.TID == '' || value.TID == null || value.TID == undefined) {
                    param1 = '-'
                } else {
                    param1 = value.TID
                }
                var param2 = '-';
                var param3 = '-';
                var param4 = '-';
                var param5 = '-';
                var param6 = '-';
                var param7 = '-';
                var param8 = '-';
                switch (value.flag) {
                    case 1:
                        param2 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined) {
                            param2 = '-'
                        } else {
                            param2 = value.filterValue
                        }
                        break;
                    case 2:
                        param3 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined) {
                            param3 = '-'
                        } else {
                            param3 = value.filterValue
                        }
                        break;
                    case 3:
                        param4 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined) {
                            param4 = '-'
                        } else {
                            param4 = value.filterValue
                        }
                        break;
                    case 4:
                        param5 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined) {
                            param5 = '-'
                        } else {
                            param5 = value.filterValue
                        }
                        break;
                    case 5:
                        param6 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined) {
                            param6 = '-'
                        } else {
                            param6 = value.filterValue
                        }
                        break;
                    case 6:
                        param7 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined) {
                            param7 = '-'
                        } else {
                            param7 = value.filterValue
                        }
                        break;
                    case 7:
                        // param8 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param8 = '-'
                        } else {
                            param8 = angular.copy(value.filterValue.replace(/\./g, "z")) 
                            param8 = angular.copy(param8.replace(/\-/g, "x"))

                        }

                };

                return $http.get('/api/crm/GetCustomerListFilter_V2/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7 + '/' + param8);
            },

            createNewCustomerList: function (data) {
                var no_hp = null
                if (data.Customer.CustomerTypeId != 1 && data.Customer.CustomerTypeId != 3){
                    no_hp = angular.copy(data.Customer.PICHp)
                } else {
                    no_hp = angular.copy(data.Customer.HandPhone1)
                }
                return $http.post('/api/crm/PostCustomerList/', [{
                    // ToyotaId: data.Customer.ToyotaId, //Masi Pakai Dummy Math.Random
                    OutletId: currUser.OrgId,
                    Npwp: data.Customer.Npwp,
                    CustomerTypeId: data.Customer.CustomerTypeId,
                    FleetId: 1, //Foreign Key, Masih Dummy
                    StatusCode: 1, //Status Create New
                    //ToyotaIdRequest: data.Customer.ToyotaIDFlag
                    CustomerName: this.checkIsCharacter(data.Customer.Name),
                    KTP: data.Customer.KTPKITAS,
                    NoHandphone: no_hp,
                    NPWP: data.Customer.Npwp,
                    AFCO: data.Customer.AFCOIdFlag,

                }]);
            },

            createNewCustomerListPersonal: function (id, data) {
                return $http.post('/api/crm/PostCustomerListPersonal/', [{
                    CustomerId: id,
                    // CustomerName: data.Customer.Name,
                    CustomerName: this.checkIsCharacter(data.Customer.Name),
                    FrontTitle: data.Customer.FrontTitle,
                    EndTitle: data.Customer.EndTitle,
                    KTPKITAS: data.Customer.KTPKITAS,
                    // BirthPlace: 'Dummy', //Not Used in WOBP
                    BirthDate: data.Customer.newBirthDate,
                    Handphone1: data.Customer.HandPhone1,
                    Handphone2: data.Customer.HandPhone2,
                    StatusCode: 1
                }]);
            },

            createNewCustomerListAddress: function (id, data) {
                _.map(data, function (val) {
                    // val.MainAddress == true ? 1 : 0;
                    val.MainAddress =  val.MainAddress == true ? 1 : 0;
                });
                return $http.post('/api/crm/PostCCustomerAddress/', data);
            },

            getCustAddress: function (id) {
                return $http.get('/api/crm/GetCCustomerAddress/' + id);
            },

            createNewCustomerListInstitusi: function (id, data) {
                return $http.post('/api/crm/PostCustomerListInstitution/', [{
                    InstitutionId: 0,
                    CustomerId: id,
                    // Name: data.Customer.Name,
                    Name: this.checkIsCharacter(data.Customer.Name),
                    SIUP: data.Customer.SIUP,
                    // PICName: data.Customer.PICName,
                    PICName: this.checkIsCharacter(data.Customer.PICName),
                    PICHp: data.Customer.PICHp,
                    PICEmail: data.Customer.PICEmail,
                    Npwp: data.Customer.Npwp,
                    NPWP: data.Customer.Npwp,
                    // PICDOB: data.Customer.PICName,
                    // PICGenderId: data.Customer.PICName,
                    // PICAddress: data.Customer.PICName,
                    // PICVillageId: data.Customer.PICName,
                    // PICRT: data.Customer.PICName,
                    // PICRW: data.Customer.PICName,
                    // SectorBusinessId: data.Customer.PICName,
                    // PICPositionId: data.Customer.PICName,
                    PICDepartmentId: data.Customer.PICDepartmentId,
                    // StatusCode: data.Customer.PICName,
                }]);

            },

            updateCustomerList: function (id, cTypeId, data) {
                var d_toyotaid = angular.copy(data.Customer.ToyotaId)
                if (d_toyotaid == undefined || d_toyotaid == null || d_toyotaid == ''){
                    d_toyotaid = '-'
                }
                return $http.put('/api/crm/PutCustomerList/', [{
                    CustomerId: id,
                    // OutletId: 655,
                    ToyotaId: d_toyotaid,
                    Npwp: data.Customer.Npwp,
                    CustomerTypeId: cTypeId,
                    // FleetId: 1,
                    StatusCode: 1,
                    Afco: data.Customer.AFCOIdFlag

                }]);
            },

            updateCustomerListPersonal: function (Pid, Cid, data) {
                // return $http.put('/api/crm/PutCListPersonal', [{
                return $http.put('/api/crm/PutCListPersonalNew', [{
                    PersonalId: Pid,
                    CustomerId: Cid,
                    FrontTitle: data.Customer.FrontTitle,
                    EndTitle: data.Customer.EndTitle,
                    CustomerName: this.checkIsCharacter(data.Customer.Name),
                    KTPKITAS: data.Customer.KTPKITAS,
                    // BirthPlace: 'Dummy', //Not Used in WOBP
                    BirthDate: data.Customer.newBirthDate,
                    Handphone1: data.Customer.HandPhone1,
                    Handphone2: data.Customer.HandPhone2,
                    StatusCode: 1

                }]);
            },

            updateVehicleList: function (data) {
                var DecDate = new Date(data.Vehicle.DECDate);
                var STNKDate = null
                if (data.Vehicle.STNKDate != null && data.Vehicle.STNKDate != undefined) {
                    STNKDate = new Date(data.Vehicle.STNKDate);
                }


                // console.log('data updateVehicleList', data, data.Vehicle.DECDate.getFullYear(), data.Vehicle.DECDate.getMonth());
                var tmpDecDate = DecDate.getFullYear() + '-' + (DecDate.getMonth() + 1) + '-' + DecDate.getDay();
                // var tmpSTNKDate = STNKDate.getFullYear() + '-' + (STNKDate.getMonth() + 1) + '-' + STNKDate.getDate();
                var finalDate1
                var yyyy = DecDate.getFullYear().toString();
                var mm = (DecDate.getMonth() + 1).toString(); // getMonth() is zero-based
                var dd = DecDate.getDate().toString();
                finalDate1 = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);

                var finalDate2 = null
                if (STNKDate != null && STNKDate != undefined) {
                    var yyyy1 = STNKDate.getFullYear().toString();
                    var mm1 = (STNKDate.getMonth() + 1).toString(); // getMonth() is zero-based
                    var dd1 = STNKDate.getDate().toString();
                    finalDate2 = yyyy1 + '-' + (mm1[1] ? mm1 : "0" + mm1[0]) + '-' + (dd1[1] ? dd1 : "0" + dd1[0]);
                }

                console.log('generate date1', finalDate1);
                console.log('generate date2', finalDate2);
                return $http.put('/api/crm/PutVehicle/' + data.Vehicle.Color.ColorId + '/' + data.Vehicle.Type.VehicleTypeId, [{
                    VehicleId: data.Vehicle.VehicleId,
                    VIN: data.Vehicle.VIN,
                    LicensePlate: data.Vehicle.LicensePlate,
                    EngineNo: data.Vehicle.EngineNo,
                    AssemblyYear: data.Vehicle.AssemblyYear,
                    DECDate: finalDate1,
                    // DECDate: data.Vehicle.DECDate,
                    // Added by Fyberz =========
                    Insurance: data.Vehicle.Insurance,
                    SPK: data.Vehicle.SPK,
                    // =========================
                    // VehicleTypeColorId: data.Vehicle.ColorId,
                    STNKName: data.Vehicle.STNKName,
                    STNKAddress: data.Vehicle.STNKAddress,
                    STNKDate: finalDate2,
                    SourceDataId: data.Vehicle.SourceDataId,
                    // STNKDate: data.Vehicle.STNKDate,
                    GasTypeId: data.Vehicle.GasTypeId,
                    isNonTAM: data.Vehicle.isNonTAM,
                    OutletId: currUser.OrgId
                    // "StatusCode": 1,

                }]);
            },

            deleteCustomerListAddress: function (id) {
                console.log('fdg', id)
                // return $http.delete('/api/crm/DeleteCCustomerAddress/', { data: id, headers: { 'Content-Type': 'application/json' } });
                return $http.put('/api/crm/DeleteCCustomerAddress/', id);
            },

            updateCustomerListAddress: function (Aid, Cid, data) {
                _.map(data, function (val) {
                    // val.MainAddress == true ? 1 : 0;
                    val.MainAddress =  val.MainAddress == true ? 1 : 0;
                });
                return $http.put('/api/crm/PutCCustomerAddress/', data);
            },

            updateCustomerListInstitusi: function (Iid, Cid, data) {
                console.log("data di factory", data);
                // return $http.put('/api/crm/PutCListInstitution', [{
                return $http.put('/api/crm/PutCListInstitutionNew', [{
                    InstitutionId: Iid,
                    CustomerId: Cid,
                    // Name: data.Customer.Name,
                    Name: this.checkIsCharacter(data.Customer.Name),
                    SIUP: data.Customer.SIUP,
                    // PICName: data.Customer.PICName,
                    PICName: this.checkIsCharacter(data.Customer.PICName),
                    PICHp: data.Customer.PICHp,
                    PICEmail: data.Customer.PICEmail,
                    // Npwp: data.Customer.Npwp,
                    NPWP: data.Customer.Npwp,
                    // PICDOB: data.Customer.PICName,
                    // PICGenderId: data.Customer.PICName,
                    // PICAddress: data.Customer.PICName,
                    // PICVillageId: data.Customer.PICName,
                    // PICRT: data.Customer.PICName,
                    // PICRW: data.Customer.PICName,
                    // SectorBusinessId: data.Customer.PICName,
                    // PICPositionId: data.Customer.PICName,
                    PICDepartmentId: data.Customer.PICDepartmentId,

                }]);
            },

            createOrUpdateCustomerListPersonal: function (Pid, Cid, data){
                if (Pid == null){
                    // ni blm pernah punya personalid jalanin post
                    var id = Cid
                    return $http.post('/api/crm/PostCustomerListPersonal/', [{
                        CustomerId: id,
                        // CustomerName: data.Customer.Name,
                        CustomerName: this.checkIsCharacter(data.Customer.Name),
                        FrontTitle: data.Customer.FrontTitle,
                        EndTitle: data.Customer.EndTitle,
                        KTPKITAS: data.Customer.KTPKITAS,
                        // BirthPlace: 'Dummy', //Not Used in WOBP
                        BirthDate: data.Customer.newBirthDate,
                        Handphone1: data.Customer.HandPhone1,
                        Handphone2: data.Customer.HandPhone2,
                        StatusCode: 1
                    }]);
                } else {
                    // ini punya personalid jalanin update
                    // return $http.put('/api/crm/PutCListPersonal', [{
                    return $http.put('/api/crm/PutCListPersonalNew', [{
                        PersonalId: Pid,
                        CustomerId: Cid,
                        FrontTitle: data.Customer.FrontTitle,
                        EndTitle: data.Customer.EndTitle,
                        // CustomerName: data.Customer.Name,
                        CustomerName: this.checkIsCharacter(data.Customer.Name),
                        KTPKITAS: data.Customer.KTPKITAS,
                        // BirthPlace: 'Dummy', //Not Used in WOBP
                        BirthDate: data.Customer.newBirthDate,
                        Handphone1: data.Customer.HandPhone1,
                        Handphone2: data.Customer.HandPhone2,
                        StatusCode: 1
    
                    }]);
                }

            },
            createOrUpdateCustomerListInstitusi: function (Iid, Cid, data){
                if (Iid == null){
                    // ni blm pernah punya institusiid jalanin post
                    var id = Cid
                    return $http.post('/api/crm/PostCustomerListInstitution/', [{
                        InstitutionId: 0,
                        CustomerId: id,
                        // Name: data.Customer.Name,
                        Name: this.checkIsCharacter(data.Customer.Name),
                        SIUP: data.Customer.SIUP,
                        // PICName: data.Customer.PICName,
                        PICName: this.checkIsCharacter(data.Customer.PICName),
                        PICHp: data.Customer.PICHp,
                        PICEmail: data.Customer.PICEmail,
                        Npwp: data.Customer.Npwp,
                        NPWP: data.Customer.Npwp,
                        // PICDOB: data.Customer.PICName,
                        // PICGenderId: data.Customer.PICName,
                        // PICAddress: data.Customer.PICName,
                        // PICVillageId: data.Customer.PICName,
                        // PICRT: data.Customer.PICName,
                        // PICRW: data.Customer.PICName,
                        // SectorBusinessId: data.Customer.PICName,
                        // PICPositionId: data.Customer.PICName,
                        PICDepartmentId: data.Customer.PICDepartmentId,
                        // StatusCode: data.Customer.PICName,
                    }]);
                } else {
                    // ini punya institusiid jalanin update
                    // return $http.put('/api/crm/PutCListInstitution', [{
                    return $http.put('/api/crm/PutCListInstitutionNew', [{
                        InstitutionId: Iid,
                        CustomerId: Cid,
                        // Name: data.Customer.Name,
                        Name: this.checkIsCharacter(data.Customer.Name),
                        SIUP: data.Customer.SIUP,
                        // PICName: data.Customer.PICName,
                        PICName: this.checkIsCharacter(data.Customer.PICName),
                        PICHp: data.Customer.PICHp,
                        PICEmail: data.Customer.PICEmail,
                        // Npwp: data.Customer.Npwp,
                        NPWP: data.Customer.Npwp,
                        // PICDOB: data.Customer.PICName,
                        // PICGenderId: data.Customer.PICName,
                        // PICAddress: data.Customer.PICName,
                        // PICVillageId: data.Customer.PICName,
                        // PICRT: data.Customer.PICName,
                        // PICRW: data.Customer.PICName,
                        // SectorBusinessId: data.Customer.PICName,
                        // PICPositionId: data.Customer.PICName,
                        PICDepartmentId: data.Customer.PICDepartmentId,
    
                    }]);
                }

            },

            getMLocationProvince: function () {
                return $http.get('/api/sales/MLocationProvince');
            },

            getMLocationCityRegency: function (id) {
                return $http.get('/api/sales/MLocationCityRegency?start=1&limit=100&filterData=ProvinceId|' + id);
            },

            getMLocationKecamatan: function (id) {
                return $http.get('/api/sales/MLocationKecamatan?start=1&limit=100&filterData=CityRegencyId|' + id);
            },

            getMLocationKelurahan: function (id) {
                return $http.get('/api/sales/MLocationKelurahan?start=1&limit=100&filterData=DistrictId|' + id)
            },

            getVehicleList: function (param1, param2) {
                return $http.get('/api/crm/GetVehicleListSpecific/' + param1 + '/' + param2);
            },
            NewGetVehicleListSpecific: function(param1, param2) {
                return $http.get('/api/crm/NewGetVehicleListSpecific/' + param1 + '/' + param2);
            },

            createNewVehicleList: function (data) {
                console.log('data createNewVehicleList', data);
                // return $http.post('/api/crm/PostVehicle/' + data.Vehicle.Color.ColorId + '/' + data.Vehicle.Type.VehicleTypeId, [{
                return $http.post('/api/crm/PostVehicleSP/' + data.Vehicle.Color.ColorId + '/' + data.Vehicle.Type.VehicleTypeId, [{
                    VIN: data.Vehicle.VIN,
                    LicensePlate: data.Vehicle.LicensePlate,
                    EngineNo: data.Vehicle.EngineNo,
                    AssemblyYear: data.Vehicle.AssemblyYear,
                    DECDate: data.Vehicle.DECDate,
                    // VehicleTypeColorId: data.Vehicle.ColorId,
                    STNKName: data.Vehicle.STNKName,
                    STNKAddress: data.Vehicle.STNKAddress,
                    STNKDate: data.Vehicle.STNKDate,
                    GasTypeId: data.Vehicle.GasTypeId,
                    isNonTAM: data.Vehicle.isNonTAM,
                    OutletId: currUser.OrgId
                    // "StatusCode": 1,

                }]);
            },

            getRelationship: function () {
                return $http.get('/api/crm/GetCCustomerRelation/');
            },

            getPICDepartment: function () {
                var res = $http.get('/api/as/CustomerPosition');
                return res
            },

            getDamage: function () {
                var catId = 2040;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId + '&Flag=1');
                console.log('resnya pause=>', res);
                return res;
            },

            getT1: function () {
                // var catId = 2023;
                var res = $http.get('/api/as/GetT1');
                // console.log('resnya pause=>',res);
                return res;
            },

            getDataWac: function (JobId) {
                // console.log("JbId",JbId);
                var res = $http.get('/api/as/JobWACs/Job/' + JobId);
                // console.log('resnya=>',res);
                return res;
            },

            getProbPointsRepairProses: function (JobId, TypeId) {
                if (TypeId == null) {
                    TypeId = 0;
                }
                return $http.get('/api/as/JobWACExts/Job/' + JobId + '/' + TypeId);
            },

            getVinFromCRM: function (vin) {
                var res = $http.get('/api/crm/GetVehicleListById/' + vin + '');
                console.log('Get Data VIN=>', res);
                return res;
            },

            getDataGeneralMaster: function (catId) {
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                return res;
            },

            insertRequestPrintHistory: function (data) {

                console.log("request", data);

                // var res=$http.get('/api/ct/PostCCustomerAccessTab/',data);

                return $http.post('/api/as/PostApprovalRequestPrintServiceHistory', data);

            },

            updatePrintHistoryRequest: function (data) {

                console.log("data update", data);
                // var res=$http.get('/api/ct/PostCCustomerAccessTab/',data);

                // return $http.put('/api/as/ApprovalPrintServiceHistory_TR', data);
                return $http.put('/api/as/approv/UpdateStatusApprovalServiceHistory/' + data.TransactionId + '/' + data.StatusApprovalId, []);
            },

            getPrintHistoryRequest: function (isGr) {
                var res = $http.get('/api/as/GetRequestedApprovalPrintService/' + isGr);

                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },

            updateJobSuggest: function(Vin, item, isTam) {
                if (isTam == 1){
                    console.log('ini tam panggil crm dan CT')
                    return $q.resolve(
                        $http.put('/api/crm/PutVehicleListJobSuggest/', [{
                            VIN: Vin,
                            JobSuggest: item
                        }]),
                        $http.put('/api/ct/PutVehicleListJobSuggest/', [{
                            VIN: Vin,
                            JobSuggest: item
                        }])
                    );
                } else {
                    console.log('ini Non tam panggil crm aja')
                    return $http.put('/api/crm/PutVehicleListJobSuggest/', [{
                            VIN: Vin,
                            JobSuggest: item
                        }])
                }
                
            },



            // ===================================================  ini factory pindahan dari menu lain ================================= end

            CheckIsOPLOpen: function (jobid) {
                return $http.get('/api/as/OPL/CheckIsOPLOpen/' + jobid);
            },
            sendNotif: function(data, recepient, param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param: param
                }]);
            },
            postApproval: function(data) {
                // ApproverRoleId
                console.log("postApproval", data);
                return $http.post('/api/as/PostApprovalRequestDP/2', [{
                    JobId: data[0].JobId,
                    DPDefault: data[0].totalDpDefault,
                    DPRequested: data[0].totalDp,
                    ApprovalCategoryId: 39,
                    ApproverId: null,
                    RequesterId: null,
                    ApproverRoleId: data[0].ApproverRoleId,
                    RequestorRoleId: currUser.RoleId,
                    RequestReason: "Request Pengurangan DP",
                    RequestDate: new Date(),
                    StatusApprovalId: 3,
                    StatusCode: 1,
                    VehicleTypeId: data[0].VehicleTypeId
                }]);
            },
            checkApproval: function(data) {
                console.log("data checkApproval", data);
                return $http.put('/api/as/CheckingApproval/', [{
                    DPRequested: data,
                    ApprovalCategoryId: 39,
                    RoleId: currUser.RoleId
                }]);
            },
            checkDataPartLatest: function(JobId) {
                return $http.get('/api/as/JobParts/CheckAllParts/' + JobId)
            },
            checkApprovalPenguranganJob: function(JobTaskId, jobid){
                return $http.put('/api/as/CheckApprovalStatusByJobTaskId/' + jobid  + '/' + 1, JobTaskId)
            },

            // ================================================= CR4 OPL AGgrid =====================================================
            getDataVendorOPL: function() {
                return $http.get('/api/as/GetVendorOPL?start=1&limit=100')
            },

            getNewOplList: function(key, model,vendorid) {
                console.log("key, model,vendorid",key, model,vendorid);
                var res = $http.get('/api/as/GetTaskOPLbyVendor?oplname='+key+'&modelName='+model+'&VendorId='+vendorid);
                return res
            },

            

            putOPlData: function(data,JobId,Status,mdata) {
                console.log("dataaaaa", data);
                console.log("Status factory",Status);
                console.log("JobId factory",JobId);

                // [
                //     {
                //         "OPLTasks": [
                //             {
                //                 "JobTaskOplId": 57, v
                //                 "JobId": 89495, v
                //                 "LastModifiedUserId": 4502, v
                //                 "LastModifiedDate": "2020-06-24T13:44:27.707", v
                //                 "OPLId": 21747, v
                //                 "VendorId": 273, v
                //                 "CreateOplDate": "2020-06-17T00:00:00", v
                //                 "TargetFinishDate": "2020-06-18T00:00:00", v
                //                 "ReceivedBy": null, v
                //                 "Price": 71500.0, v
                //                 "OPLPurchasePrice": 55000.0, v
                //                 "OPLWorkName": "Light Service - Seal Oring", v
                //                 "VendorName": "PT PANCAKARSA PUTERAJAYA", v
                //                 "Notes": null, v
                //                 "Status": 0, v
                //                 "VendorInvoiceNo": null, v
                //                 "VendorInvoiceDate": null, v
                //                 "CreatedUserId": null, v
                //                 "OutletId": 280, v
                //                 "IsBilling": null, v
                //                 "BillingNo": null, v
                //                 "CancelOplDate": null, v
                //                 "CancelUserId": null, v
                //                 "QtyPekerjaan": 1, v
                //                 "isDeleted": 0, v
                //                 "PaidById": 28, v
                //                 "isGr": 1, v
                //                 "OplNo": "5Y0/OPG/2006-000028",v
                //                 "Discount": 0
                //             },
                //             {
                //                 "JobTaskOplId": 58,
                //                 "JobId": 89495,
                //                 "LastModifiedUserId": 4502,
                //                 "LastModifiedDate": "2020-06-24T13:44:27.707",
                //                 "OPLId": 21749,
                //                 "VendorId": 273,
                //                 "CreateOplDate": "2020-06-17T00:00:00",
                //                 "TargetFinishDate": "2020-06-18T00:00:00",
                //                 "ReceivedBy": null,
                //                 "Price": 286000.0,
                //                 "OPLPurchasePrice": 220000.0,
                //                 "OPLWorkName": "Light Service - Vacum Freon + Oli",
                //                 "VendorName": "PT PANCAKARSA PUTERAJAYA",
                //                 "Notes": null,
                //                 "Status": 0,
                //                 "VendorInvoiceNo": null,
                //                 "VendorInvoiceDate": null,
                //                 "CreatedUserId": null,
                //                 "OutletId": 280,
                //                 "IsBilling": null,
                //                 "BillingNo": null,
                //                 "CancelOplDate": null,
                //                 "CancelUserId": null,
                //                 "QtyPekerjaan": 1,
                //                 "isDeleted": 0,
                //                 "PaidById": 28,
                //                 "isGr": 1,
                //                 "OplNo": "5Y0/OPG/2006-000028",
                //                 "Discount": 0
                //             }
                //         ],
                //         "VendorId": 273,
                //         "VendorName": "PT PANCAKARSA PUTERAJAYA",
                //         "OplNo": "5Y0/OPG/2006-000028",
                //         "Status": 0,
                //         "isDeleted": 0
                //     }
                // ]
                var dataSave = [];
                for(var a in data){
                    if(data[a].Status == "Open"){
                        data[a].Status = 0;
                        // $scope.mData.JobTaskOplCR[a].Status = "Open";
                    }else if(data[a].Status == "On Progress"){
                        data[a].Status = 1;
                        // $scope.mData.JobTaskOplCR[a].Status = "On Progress";
                    }else if(data[a].Status == "Completed"){
                        data[a].Status = 2;
                        // $scope.mData.JobTaskOplCR[a].Status = "Complete";
                    }else if(data[a].Status == "Closed" || data[a].Status == "Billing"){
                        data[a].Status = 3;
                        // $scope.mData.JobTaskOplCR[a].Status = "Closed";
                    }else if(data[a].Status == "Cancel"){
                        data[a].Status = 4;
                    }
                    for(var b in data[a].OPLTasks){

                        if(data[a].OPLTasks[b].JobTaskOplId == undefined){
                            data[a].OPLTasks[b].JobTaskOplId = 0;
                        }
                        data[a].OPLTasks[b].JobId = JobId;
                        data[a].OPLTasks[b].OPLId = data[a].OPLTasks[b].oplId;
                        data[a].OPLTasks[b].VendorId = data[a].VendorId;
                        data[a].OPLTasks[b].EstimationDate = this.changeFormatDate(data[a].OPLTasks[b].EstimationDate);
                        data[a].OPLTasks[b].TargetFinishDate = data[a].OPLTasks[b].EstimationDate;
                        if(data[a].OPLTasks[b].ReceivedBy == undefined){

                            data[a].OPLTasks[b].ReceivedBy = null;
                        }

                        if(data[a].OPLTasks[b].Status == 2){
                            data[a].OPLTasks[b].VendorInvoiceDate = this.changeFormatDate(data[a].OPLTasks[b].VendorInvoiceDate)
                            data[a].OPLTasks[b].FinishDate = this.changeFormatDate(data[a].OPLTasks[b].FinishDate)
                        }

                        

                        data[a].OPLTasks[b].Price =  data[a].OPLTasks[b].HargaJual;
                        data[a].OPLTasks[b].OPLPurchasePrice =  data[a].OPLTasks[b].HargaBeli;
                        data[a].OPLTasks[b].OPLWorkName =  data[a].OPLTasks[b].TaskNameOpl;
                        data[a].OPLTasks[b].Price =  data[a].OPLTasks[b].HargaJual;
                        data[a].OPLTasks[b].VendorName =  data[a].VendorName;
                        data[a].OPLTasks[b].Notes =  data[a].OPLTasks[b].Notes;
                        data[a].OPLTasks[b].Status =  data[a].OPLTasks[b].Status;
                        data[a].OPLTasks[b].OutletId = currUser.OutletId;
                        data[a].OPLTasks[b].QtyPekerjaan = data[a].OPLTasks[b].Qty;
                        data[a].OPLTasks[b].Guid_Log = mdata.Guid_Log
                        data[a].OPLTasks[b].flagId = data[a].flagId

                        if(data[a].OPLTasks[b].isDeleted == undefined){
                            data[a].OPLTasks[b].isDeleted = 0;
                        }
                        data[a].OPLTasks[b].PaidById = data[a].OPLTasks[b].PaymentId;
                        data[a].OPLTasks[b].isGr = 1;
                        if(data[a].OPLTasks[b].OplNo != null){

                            data[a].OPLTasks[b].OplNo = data[a].OPLTasks[b].OplNo;
                        }
                        if(data[a].OPLTasks[b].Discount == undefined){
                             data[a].OPLTasks[b].Discount = 0;  
                        }else{

                               data[a].OPLTasks[b].Discount = data[a].OPLTasks[b].Discount;
                        }
                    }
                }  
                dataSave = data;
                console.log("final sebelum simpan opl GR",dataSave);
                return $http.put('/api/as/JobTaskOPLCR/UpdateData', dataSave);            
            },
            
            changeFormatDate: function(item) {
                var tmpDate = item;
                tmpDate = new Date(tmpDate);
                var finalDate
                var yyyy = tmpDate.getFullYear().toString();
                var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                var dd = tmpDate.getDate().toString();
                finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                console.log("changeFormatDate finalDate", finalDate);
                return finalDate;
            },
           

            changeFormatDateStrip: function(item) {
                // console.log("param",param);
                var tmpItemDate = angular.copy(item);
                console.log("changeFormatDate item", item);
                tmpItemDate = new Date(tmpItemDate);
                var finalDate = '';
                var yyyy = tmpItemDate.getFullYear().toString();
                var mm = (tmpItemDate.getMonth() + 1).toString(); // getMonth() is zero-based
                var dd = tmpItemDate.getDate().toString();

                console.log("changeFormatDate finalDate", finalDate);
                return finalDate += yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]) + 'T00:00:00.000Z';
            },




            postOPLData: function(data,JobId) {
                console.log("dataaaaa", data);
                console.log("JobId factory",JobId);
                var dataSave = [];
                // "JobTaskOplId": 0,
                //                 "OPLName": "Light Service - Seal Oring - (Internal)",
                //                 "OPLId": 21747,
                //                 "Price": 71500,
                //                 "OPLPurchasePrice": 55000,
                //                 "NormalPrice": 71500,
                //                 "QtyPekerjaan": 1,
                //                 "PaidById": "31",
                //                 "TargetFinishDate": "2020/06/15",
                //                 "Discount": 0,
                //                 "DiscountedPriceOPL": 0,
                //                 "OutletId": 280,
                //                 "IsGR": 1,
                //                 "JobId": 8937,
                //                 "Notes": "Tidak ada catatan",
                //                 "Status": 0,
                //                 "isDeleted": 0,
                //                 "VendorId": 273
                for(var i in data){

                    for(var j in data[i].detail){
                        // Agar tdk backdate
                        var date = new Date(data[i].detail[j].EstimationDate);
                        date.setDate(date.getDate() + 1);

                        data[i].detail[j].JobTaskOplId = 0;
                        data[i].detail[j].OPLName =  data[i].detail[j].TaskNameOpl;
                        data[i].detail[j].OPLId =  data[i].detail[j].oplId;
                        data[i].detail[j].Price =  data[i].detail[j].HargaJual;
                        data[i].detail[j].OPLPurchasePrice =  data[i].detail[j].HargaBeli;
                        data[i].detail[j].NormalPrice =  data[i].detail[j].HargaJual;
                        data[i].detail[j].QtyPekerjaan =  data[i].detail[j].Qty;
                        data[i].detail[j].PaidById =  data[i].detail[j].PaymentId;
                        data[i].detail[j].TargetFinishDate =  this.changeFormatDate(date);
                        data[i].detail[j].EstimationDate =  this.changeFormatDate(date);
                        data[i].detail[j].Discount =  data[i].detail[j].Discount;
                        data[i].detail[j].DiscountedPriceOPL =  data[i].detail[j].Discount;
                        data[i].detail[j].OutletId =  currUser.OutletId;
                        data[i].detail[j].isGr =  1;
                        data[i].detail[j].JobId =  JobId;//JobId
                        data[i].detail[j].Notes =  data[i].detail[j].Notes;
                        data[i].detail[j].Status =  0;
                        data[i].detail[j].isDeleted =  0;
                        data[i].detail[j].VendorId =  data[i].VendorId;
                    }
                    dataSave.push(
                            {
                              "VendorId": data[i].VendorId,
                              "VendorName": data[i].Vendor,
                              "OplNo": null,
                              "OPLTasks": data[i].detail
                          }
                    );
                }

                return $http.post('/api/as/JobTaskOPLCR/SubmitData', dataSave);
            },



            CheckApprovalDiscountTask: function(JobId) {
                var res = $http.get('/api/as/CheckApprovalDiscountTask/' + JobId );
                return res;
            },
            CheckApprovalDiscountPart: function(JobId) {
                var res = $http.get('/api/as/CheckApprovalDiscountPart/' + JobId );
                return res;
            },
            checkIsCharacter: function(data){
                if(typeof data == 'string'){
                    data = data.replace(/[^\x20-\x7f\xA\xD]/g, '');
                }
                return data;
            },
            validateDeleteOPL: function (tmpData) {
                console.log("data jobtaskoplid", tmpData);
                return $http.get('/api/as/OPL/CheckGateOPLWO/'+tmpData);
            },
            CheckCustomerMainAddress: function (customerid) {
                // api/crm/CheckCustomerMainAddress/{CustomerId}
                return $http.get('/api/crm/CheckCustomerMainAddress/' + customerid);
            },
            validasiCekOPLCancelWO: function (jobid) {
                // api/as/validasiCekOPLCancelWO/{jobid}
                return $http.get('/api/as/validasiCekOPLCancelWO/' + jobid);
            },
            CheckApprovalDPOutStandingWO: function (JobId) {
                // api/as/CheckApprovalDPOutStandingWO/{JobId}/{isGR}
                return $http.get('/api/as/CheckApprovalDPOutStandingWO/' + JobId + '/' + 1);
            },

            
            CheckIsOPLGateOut: function(data, jobid) {
                // api/as/OPL/IsOPLGateOut
                console.log("CheckIsOPLGateOut", data);
                return $http.put('/api/as/OPL/IsOPLGateOut/' + jobid , data);
            },

            CekStatusOPL: function(data, jobid) {
                // api/as/OPL/CekStatusOPL/{JobId}
                console.log("CekStatusOPL", data);
                return $http.put('/api/as/OPL/CekStatusOPL/' + jobid , data);
            },

            CheckIsPDS: function(vin) {
                // api/as/CheckIsPDS/{VIN}
                return $http.get('/api/as/CheckIsPDS/' + vin )
            },
            GetResetCounterOdometer: function(vin) {
                // api/ct/GetResetCounterOdometer/{VIN}
                return $http.get('/api/ct/GetResetCounterOdometer/' + vin)
            },

            GetCVehicleTypeListByKatashiki: function(KatashikiCode) {
                // api/crm/GetCVehicleTypeListByKatashiki/{KatashikiCode}
                return $http.get('/api/crm/GetCVehicleTypeListByKatashiki/' + KatashikiCode)
            },

            CekGatePDS: function(QueueId) {
                // api/as/CekGatePDS/{QueueId}
                // 666 berarti gatein pds
                // 0 berarti bukan
                return $http.get('/api/as/CekGatePDS/' + QueueId)
            },

            getCheckDataPartTyre: function (JobId, data) {
                console.log('jobid ====>', JobId);
                console.log('ihhh datany', data)
                return $http.post('/api/as/CheckDataPart/' + JobId, data);
            },

            UpdateKM: function(data, jobid) {
                // api/as/Jobs/UpdateKM/{JobId}
                console.log("UpdateKM", data);
                // [{OldKM : 100, NewKM : 200}]
                return $http.post('/api/as/Jobs/UpdateKM/' + jobid , data);
            },
           
            GetHistoryKM: function(JobId) {
                // api/as/Jobs/GetHistoryKM/{JobId}
                return $http.get('/api/as/Jobs/GetHistoryKM/' + JobId)
            },

            CheckQuueueFrown: function(IdQueue, JobId) {
                // api/as/queue/CheckQuueue/{IdQueue}/{JobId}
                // 666 Frown
                // 0 Biasa
                return $http.get('/api/as/queue/CheckQuueue/' + IdQueue + '/' + JobId)
            },
            
            isiGuidLogTaskPartOPL: function(item, GL, tipe) {
                if (tipe == 2){ // untuk opl
                    if (item !== null && item !== undefined){
                        for(var i=0; i<item.length; i++){
                            item[i].Guid_Log = GL
                        }
                    }
                    return item
                } else { // untuk job dan parts
                    if (item !== null && item !== undefined){
                        for(var i=0; i<item.length; i++){
                            item[i].Guid_Log = GL
                            if (item[i].JobParts !== null && item[i].JobParts !== undefined){
                                for (var j=0; j<item[i].JobParts.length; j++){
                                    item[i].JobParts[j].Guid_Log = GL
                                }
                            }
                        }
                    }
                    return item;
                }
                
            },
            listHistoryClaim: function(vin){
                var res = $http.get('/api/as/ToWass/ListHistoryClaim?VIN=' + vin)
                return res;
            },
            getDataVIN: function(vin){
                var res = $http.get('/api/as/MasterVehicle_FPCFLCPLC/' + vin)
                return res;
            }
        }
    });