var xwo1 = angular.module('app');
	xwo1.expandController_WOGRController2 =function($scope,$window, $filter, $interval, $q, $http, CurrentUser, Role, bsNotify, ASPricingEngine, WoHistoryGR, AsbGrFactory, bsAlert, $timeout, ngDialog, PrintRpt, VehicleHistory, $compile) {
		
            // using ag-grid change CR4 Phase2
            console.log("$scope",$scope);
           // var tmpScope = angular.copy($scope);
           initOPLEditor($scope, $compile);
           initDateEditor($scope, $compile);
           $scope.viewMode = false;
           $scope.editmode = false;
           $scope.disableSimpan = false;
           $scope.mOpl = {};
           $scope.masterOPL = [];
           $scope.minDate = new Date();
           $scope.$on('$destroy', function() {
            angular.element(".ui.modal.ModalOPLWoList").remove();
            });

           WoHistoryGR.getDataVendorOPL().then(function (res) {
               for(var i in res.data.Result){
                 res.data.Result[i].VendorName = res.data.Result[i].Name;
                 res.data.Result[i].Name = ""+res.data.Result[i].VendorCode+" - "+res.data.Result[i].Name;
               }
               $scope.OplVendor = res.data.Result;
               console.log("$scope.OplVendor",$scope.OplVendor);
           });

           var OPLPaymentData = [];

           $scope.chooseVendor = function(){
            console.log("chooseVendor | mOpl ===>",$scope.mOpl);
            for(var a in $scope.OplVendor){
                   if($scope.OplVendor[a].VendorId == $scope.mOpl.vendor){
    
                       $scope.mOpl.VendorCode = $scope.OplVendor[a].VendorCode;
                   }
               }
           }
    
           $scope.plushOrderOpl = function(){
               console.log("$scope.OplVendor",$scope.OplVendor);
               console.log('$scope.isLock ===>',$scope.isLock)

               $scope.masterOPL = [];
               $scope.mOpl.VendorCode = null;
               $scope.mOpl.vendor = null;
    
               $scope.gridDetailOPLWoListApi.setRowData($scope.masterOPL);
               flagedit = 0;
               WoHistoryGR.getDataVIN($scope.mDataCrm.Vehicle.VIN).then(function(restu){
                    console.log('cek input rangka opl', $scope.mDataCrm.Vehicle.VIN)
                    $scope.vinOPL = restu.data.Result;
                    console.log('cek res input opl', $scope.vinOPL)
               })
               // ModalOPL
               $scope.viewMode = false;
               $scope.editmode = false;
               $scope.disableSimpan = true;
               $scope.disableComplete = false;
               modeOpl = 2;
               angular.element(".ui.modal.ModalOPLWoList").modal("show");
               angular.element(".ui.modal.ModalOPLWoList").modal({backdrop: 'static'});
               sizeToFit();
               $scope.dialogPosition(1);


           }

           var count = 0;
           $scope.dialogPosition = function(data){
                count = count + data;
                if(count > 1){
                    angular.element(".ui.modal.ModalOPLWoList").css('top', '10%');
                }
                $scope.calenderPosition();
           }
           // ag gridworkview
           $scope.agGridWorkViewApi
           function onFirstDataRendered(params){
               params.api.sizeColumnsToFit();
               setTimeout(function(){ params.api.getDisplayedRowAtIndex(1).setExpanded(false) }, 0);
           }
    
           function onGridSizeChanged(params){
            params.api.sizeColumnsToFit();
            console.log('JALAN RESIZE')
           }
           
           $scope.finalDataOPL = [];
           var DataChoosedDelete = [];
           // $scope.gridWorkView.data = $scope.master;
           $scope.reziseColumnAG = function(){
             var allColumnIds = [];
               $scope.gridWorkView.columnApi.getAllColumns().forEach(function (column) {
                 allColumnIds.push(column.colId);
               });
               console.log("allColumnIds",allColumnIds);
               $scope.gridWorkView.columnApi.autoSizeColumns(allColumnIds, false);
           }

           $scope.gridWorkView = {
               //DEFAULT AG-GRID OPTIONS
               // default ColDef, gets applied to every column
               defaultColDef: {
                   width: 150,
                   editable: false,
                   filter: false,
                   sortable: true,
                   resizable: true
               },

               rowSelection: 'multiple',
               suppressRowClickSelection:true,
               angularCompileRows: true,
               overlayNoRowsTemplate: '<h1 style="font-size:3em; opacity:.25";position:absolute;top:4rem;width:100%;text-align:center;z-index:1000;>No data available</h1>',
               toolPanelSuppressSideButtons:true,
               masterDetail:true,
               detailCellRendererParams:{
                   detailGridOptions : {
                       columnDefs : [
                           // {field : 'idChild'},
                           {headerName: 'No.',field : 'No', suppressSizeToFit:true},
                           {headerName: 'Nama OPL',field : 'TaskNameOpl', suppressSizeToFit:true},
                           {headerName: 'Pembayaran',field : 'Payment', suppressSizeToFit:true},
                           {headerName: 'Tgl Selesai',field : 'EstimationDate', suppressSizeToFit:true, cellRenderer: customDate},
                           {headerName: 'Qty',field : 'Qty', suppressSizeToFit:true , cellEditor: 'numericCellEditor'},
                           {headerName: 'Harga Beli',field : 'HargaBeli', suppressSizeToFit:true , cellEditor: 'numericCellEditor' , cellStyle: {textAlign: "right"},
                               cellRenderer: function(params){ 
                                   var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
                                   var notOk = '<span style="float:left">Rp.</span>'+ 0;
                                   if(params.value ==  null || params.value ==  undefined || params.value == "" ){
                                       return notOk;
                                   } else{
                                       return isOke;
                                   }
                               }
                           },
                           {headerName: 'Harga Jual',field : 'HargaJual', suppressSizeToFit:true  , cellEditor: 'numericCellEditor', cellStyle: {textAlign: "right"},
                               cellRenderer: function(params){ 
                                   var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
                                   var notOk = '<span style="float:left">Rp.</span>'+ 0;
                                   if(params.value ==  null || params.value ==  undefined || params.value == "" ){
                                       return notOk;
                                   } else{
                                       return isOke;
                                   }
                               }
                           },
                           {headerName: 'Diskon (%)',field : 'Discount', suppressSizeToFit:true , cellEditor: 'numericCellEditor'},
                           {headerName: 'Note',field : 'Notes', suppressSizeToFit:true, resizable: true, minWidth: 700},
                           // {headerName: 'DP (%)',field : 'DownPayment', suppressSizeToFit:true},
                           // {headerName: 'Qty',field : 'Reserve'},
                       ],
                       onFirstDataRendered: function(params){
                           // params.api.sizeColumnsToFit();
                           var allColumnIds = [];
                           params.columnApi.getAllColumns().forEach(function(column) {
                               allColumnIds.push(column.colId);
                           });
                       
                           params.columnApi.autoSizeColumns(allColumnIds, false);
                       },
    
                      
    
    
                   },
                   getDetailRowData: function(params){
                       console.log("params",params)
                       params.successCallback(params.data.detail);        
                   },
    
               },
    
    
               onFirstDataRendered: onFirstDataRendered,
               onGridSizeChanged: onGridSizeChanged,
               // components: {
               //     partEditor: PartEditor,
               //     numericEditor: NumericCellEditor,
               //     numericEditorPct: NumericCellEditorPct
               // },
               onGridReady: function(params) {
                   $scope.agGridWorkViewApi = params.api;
                   sizeToFit();
               },
    
               rowData: $scope.finalDataOPL,
               onCellEditingStopped: function(e) {
                   console.log('opl cellEditingStopped=>',e.rowIndex,e.colDef.field,e.value,e.node,e);
               },
    
               getDetailRowData: function(params){
                      
               },
               
               autoSizeAll : function () {
                   var allColumnIds = [];
                   gridWorkView.columnApi.getAllColumns().forEach(function(column) {
                       allColumnIds.push(column.colId);
                   });
               
                   gridWorkView.columnApi.autoSizeColumns(allColumnIds, false);
               },
               
               isRowSelectable: function(rowNode) {
                   if(rowNode.data.IsUse == null || rowNode.data.IsUse == 0  || typeof rowNode.data.IsUse == undefined){
                       return true
                   }else{
                       return false;
                   }
               },
               
               onRowSelected: function(event) {
                   console.log("event",event);
                   console.log(" selected = " + event.node.selected);
                   var tmpHit = 0;
                   var anotherTMp = 0;
                   if(event.node.selected == true){
                       DataChoosedDelete.push(event.data);

                       for(var a in DataChoosedDelete){
                         for(var b in DataChoosedDelete[a].OPLTasks){
                           anotherTMp++
                           if(DataChoosedDelete[a].OPLTasks[b].Status == 2 || DataChoosedDelete[a].OPLTasks[b].Status == 3){
                             tmpHit++
                           }

                           console.log("anotherTMp",anotherTMp);
                           console.log("tmpHit",tmpHit);
                           if(tmpHit == anotherTMp){
                             $scope.disableDelte = true;
                           }else{
                             $scope.disableDelte = false;
                           }
                         }
                       }
                   }else{
                       var filtered = DataChoosedDelete.filter(function(item) { 
                          return item.FlagID !== event.data.FlagID;  
                       });
                       console.log("filtered",filtered);
                       DataChoosedDelete = filtered;
                       if(DataChoosedDelete.length == 0){
                         $scope.disableDelte = false;
                       }
                   }
                   console.log("DataChoosedDelete",DataChoosedDelete);
               },
               columnDefs: [
                 
                    { headerName: 'No.',field: 'No', width: 115, cellRenderer:'agGroupCellRenderer', headerCheckboxSelection: true, checkboxSelection: true},
                    { headerName: 'No. OPL',field: 'OplNo', width: 150},
                    { headerName: 'Vendor',field: 'Vendor', width: 400},
                    { headerName: 'Qty. Pekerjaan',field: 'Qty', width: 160},
                    { headerName: 'Status OPL',field: 'Status', width: 130},
                    { headerName: 'Action', width: 90, minWidth: 100, suppressFilter: true, pinned: 'right',suppressMenu:true, cellRenderer: actionRendererTask}
                //    { headerName: 'Action',width: 100, minWidth:100,maxWidth:100, suppressFilter: true,pinned: 'right', suppressMenu:true, cellRenderer: actionRendererTask}
               ]
           };

           $scope.disableDelte = false;
           function actionRendererTask(param){
            console.log("actionRendererTask | param ===>",param);
            console.log("$scope.modeOPL",$scope.modeOPL);
            if ($scope.modeOPL == 'view') {
                 $timeout(function () {
                       $('.viewOPL').attr('ng-disabled', false).attr('skip-enable', false).removeAttr('disabled', 'disabled');
                     // }
                     $('.editOPL').hide();
                     // $('#rowCustDisableCompleted').find('*').attr('skip-enable', true).attr('disabled', 'disabled').removeAttr('skip-disable');
                 }, 1000);
             }else{
                $timeout(function () {
                        // $('.viewOPL').attr('ng-disabled', false).attr('skip-enable', false).removeAttr('disabled', 'disabled');
                    // }

                    if($scope.isLock == true){
                        $('.editOPL').hide();
                    }else{
                        // if(param.data.xStatus == 2) $('.editOPL').attr('ng-disabled', true).attr('skip-enable', true)
                        $('.editOPL').show();
                    }

                    
                    


                    // $('#rowCustDisableCompleted').find('*').attr('skip-enable', true).attr('disabled', 'disabled').removeAttr('skip-disable');
                }, 1000);
             }

               var actionHtml =' <button class="viewOPL ui icon inverted grey button"\
                                   style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px"\
                                   ng-click="gridClickViewDetailHandler(data,1)" ng-disabled="false">\
                                   <i class="fa fa-fw fa-lg fa-list-alt"></i>\
                               </button>\
                               <button class="editOPL ui icon inverted grey button" ng-if="data.xStatus != 2 "\
                                   style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px"\
                                   ng-click="gridClickEditHandler(data,2)">\
                                   <i class="fa fa-fw fa-lg fa-pencil"></i>\
                               </button>';
               return actionHtml;
           }
    
           $scope.disJob = false;
           // $scope.disFlag =
           var modeOpl;
           $scope.gridClickViewDetailHandler = function(data,flag){
               $scope.masterOPL = []; 
               console.log("OPLPaymentData",OPLPaymentData);
               console.log('data view',data,flag);
               $scope.viewMode = true;
               tmpDataOPL = angular.copy(data);
               $scope.mOpl.vendor = data.VendorId;
               $scope.chooseVendor();
               $scope.masterOPL = data.detail;
               for(var a = 0; a < data.detail.length; a++){
                   data.detail[a].No = a + 1;
               }
               console.log("$scope.masterOPL",$scope.masterOPL);
               $scope.gridDetailOPLWoListApi.setRowData($scope.masterOPL);
               angular.element(".ui.modal.ModalOPLWoList").modal("show");
               angular.element(".ui.modal.ModalOPLWoList").modal({backdrop: 'static'});
               $scope.dialogPosition(1);
               modeOpl = 1;
               console.log("modeOpl",modeOpl);
           }

           function disabledInputOPL(params){
               console.log("params",params);
               console.log("modeOpl",modeOpl);
               //jika 1 = view
               if(modeOpl == 1 || params.data.Status == 2){
                   return false;
               }else{
                   return true;
               }
               
           }

            function disabledInputOPL_Diskon(params){
                console.log("params",params);
                console.log("modeOpl",modeOpl);
                //jika 1 = view
                if(modeOpl == 1){
                    return false;
                }else{
                    if (params.data.PaymentId == 30) {
                        return false;
                    } else {
                        return true;
                    }
                    // return true;
                }
                
            }
    
            var flagedit = 0;
            var tmpDataOPL = {};
            $scope.disableComplete = false;
            $scope.gridClickEditHandler = function(data,flag){
                $scope.masterOPL = [];
                
                tmpDataOPL = angular.copy(data);
                console.log("OPLPaymentData",OPLPaymentData);
                console.log('data edit',data,flag);
                flagedit = 1;
                console.log("gridClickEditHandler | flagedit ====>",flagedit);
                console.log("gridClickEditHandler | tmpDataOPL===>",tmpDataOPL);
                $scope.dataEdit = angular.copy(data);
                console.log("$scope.dataEdit",$scope.dataEdit);
                $scope.viewMode = false;
                $scope.editmode = true;
                $scope.mOpl.vendor = data.VendorId;
                $scope.chooseVendor();
                $scope.masterOPL = data.detail;
                var tmpNOdis = 0;
                $scope.disableComplete = false;
                for(var a = 0; a < data.detail.length; a++){
                    data.detail[a].No = a + 1;
                    if(data.detail[a].Status == 2){
                      tmpNOdis++
                      if(tmpNOdis == data.detail.length){
                        $scope.disableComplete = true;
                      }else{
                        $scope.disableComplete = false;
                      }
                    }
                }

                WoHistoryGR.getDataVIN($scope.mDataCrm.Vehicle.VIN).then(function(restu){
                    console.log('cek input rangka opl', $scope.mDataCrm.Vehicle.VIN)
                    $scope.vinOPL = restu.data.Result;
                    console.log('cek res input opl', $scope.vinOPL)
                })

                // 					2675 -> FPC - Free Parts Claim
                // 					2677 -> FLC - Free Labor Claim
                // 					2678 -> PLC - Parts Labor Claim
					//----------------------------------------CEK LIST ADA FPC FLC PLC START------------------------------//
					var x_adaFPC = 0
					var x_adaFLC = 0
					var x_adaPLC = 0
					for(var i=0; i<$scope.gridWork.length; i++){
						if ($scope.gridWork[i].JobTypeId == 2675) {
							x_adaFPC++;
						}
						if ($scope.gridWork[i].JobTypeId == 2677) {
							x_adaFLC++;
						}
						if ($scope.gridWork[i].JobTypeId == 2678) {
							x_adaPLC++;
						}
					}
					//----------------------------------------CEK LIST ADA FPC FLC PLC END------------------------------//
					// MHKE8FB3JKK037194 - TCARE
									// JTNGF3DH3N8035733 - GGBSB
					if (x_adaFLC == 0 && x_adaFPC == 0 && x_adaPLC == 0) {
						OPLPaymentData.splice(0,6)
						for(var a in $scope.OPLPaymentData){
							OPLPaymentData.push($scope.OPLPaymentData[a].Name)
						}
					}else{
						if(OPLPaymentData.length == 0){
							for(var a in $scope.OPLPaymentData){
								//Ngecek programName GBSB pembayraan muncul semua, kalau bukan warranty tidak ada
								if ($scope.vinOPL.length > 0) {
									if ($scope.vinOPL[0].ProgramName == "GBSB") {
										console.log('$scope.vinOPL 1', $scope.vinOPL)
										console.log('sini == 0 ===> 2')
										OPLPaymentData.push($scope.OPLPaymentData[a].Name)
										//Jiga data norangkanya tidak ada munculin semua		
									}else if($scope.vinOPL[0].ProgramName != "" && $scope.vinOPL[0].ProgramName != undefined && $scope.vinOPL[0].ProgramName != null && $scope.vinOPL[0].ProgramName != 'GBSB'){
										console.log('$scope.vinOPL', $scope.vinOPL)
										console.log('typeof $scope.vinOPL', typeof $scope.vinOPL)
										console.log('$scope.vinOPL.ProgramName', $scope.vinOPL.ProgramName)
										console.log('sini == 0 ===> 1')
										console.log('vinOPL 123', $scope.vinOPL)
										if ($scope.OPLPaymentData[a].Name !== 'Warranty') {
											OPLPaymentData.push($scope.OPLPaymentData[a].Name)
										}
									}else{
										//kalau tidak ada programName pembyaran muncul semua
										OPLPaymentData.push($scope.OPLPaymentData[a].Name)
									}
								}else{
									OPLPaymentData.push($scope.OPLPaymentData[a].Name)
								}
								
							}
									console.log("STATUS MASUK");
						}else{
							OPLPaymentData.splice(0,6)
							for(var a in $scope.OPLPaymentData){
								//Ngecek programName GBSB pembayraan muncul semua, kalau bukan warranty tidak ada
								if ($scope.vinOPL.length > 0) {
									if ($scope.vinOPL[0].ProgramName == "GBSB") {
										console.log('$scope.vinOPL 1', $scope.vinOPL)
										console.log('sini == 0 ===> 2')
										OPLPaymentData.push($scope.OPLPaymentData[a].Name)
										//Jiga data norangkanya tidak ada munculin semua		
									}else if($scope.vinOPL[0].ProgramName != "" && $scope.vinOPL[0].ProgramName != undefined && $scope.vinOPL[0].ProgramName != null && $scope.vinOPL[0].ProgramName != 'GBSB'){
										console.log('$scope.vinOPL', $scope.vinOPL)
										console.log('typeof $scope.vinOPL', typeof $scope.vinOPL)
										console.log('$scope.vinOPL.ProgramName', $scope.vinOPL.ProgramName)
										console.log('sini == 0 ===> 1')
										console.log('vinOPL 123', $scope.vinOPL)
										if ($scope.OPLPaymentData[a].Name !== 'Warranty') {
											OPLPaymentData.push($scope.OPLPaymentData[a].Name)
										}
									}else{
										//kalau tidak ada programName pembyaran muncul semua
										OPLPaymentData.push($scope.OPLPaymentData[a].Name)
									}
								}else{
									OPLPaymentData.push($scope.OPLPaymentData[a].Name)
								}
							}
	
						}
					}

                console.log("$scope.disableComplete",$scope.disableComplete);
                console.log("$scope.masterOPL",$scope.masterOPL);
                $scope.gridDetailOPLWoListApi.setRowData($scope.masterOPL);
                angular.element(".ui.modal.ModalOPLWoList").modal("show");
                angular.element(".ui.modal.ModalOPLWoList").modal({backdrop: 'static'});
                $scope.dialogPosition(1);
                modeOpl = 2;
                console.log("modeOpl",modeOpl);
                
            }
           // ag gridworkview
    
           // ag grid detail
           $scope.gridDetailOPLWoListApi;
           $scope.gridDetailOPLWoList = {
               //DEFAULT AG-GRID OPTIONS
               // default ColDef, gets applied to every column
               defaultColDef: {
                   width: 150,
                   editable: false,
                   filter: false,
                   sortable: true,
                   resizable: true
               },
               rowSelection: 'multiple',
               suppressRowClickSelection: true,
               suppressCellSelection: true,
               angularCompileRows: true,
               overlayNoRowsTemplate: '<h1 style="font-size:3em; opacity:.25";position:absolute;top:4rem;width:100%;text-align:center;z-index:1000;>No data available</h1>',
               components: {
                   OPLEditor: OPLEditor,
                   DateEditor : DateEditor,
               //     PartNameEditor : PartNameEditor,
                   numericCellEditor : NumericCellEditor,
               //     checkboxEditor: agCheckboxCellEditor
                   noteCellEditor : noteCellEditor,
               },
               onGridReady: function (params) {
                   // params.api.sizeColumnsToFit();
                   $scope.gridDetailOPLWoListApi = params.api;
    
                   // var allColumnIds = [];
                   // $scope.GridParts.columnApi.getAllColumns().forEach(function(column) {
                   //     allColumnIds.push(column.colId);
                   // });
    
                   // $scope.GridParts.columnApi.autoSizeColumns(allColumnIds, false);
    
               },
               
    
               rowData: $scope.masterOPL,
               onCellEditingStopped: function (e) {
                   console.log('parts cellEditingStopped=>', e.rowIndex, e.colDef.field, e.value, e.node, e);
                   // if(e.colDef.field == 'PartCode'){
                   if($scope.disJob == false){
                       $scope.onAfterCellEditAg(e.rowIndex,e.colDef.field,e.value,e.node);
                       if(e.colDef.field == "Notes"){

                         var value = e.value.substr(0,200);
                         e.node.setDataValue(e.column.colId, value);
                         console.log('cellEditingStopped');
                       }
                   }
                   // }
               },
    
               sideBar: {
                       toolPanels: ['columns'],
                       hiddenByDefault: false
               },
               toolPanelSuppressSideButtons:true,
           
               
               columnDefs: [
    
                   { headerName: 'No.', field: 'No', width: 70},
                   { headerName: 'Nama Pekerjaan*', field: 'TaskNameOpl', width: 350,  editable: disabledInputOPL,cellEditor: 'OPLEditor'},
                   { headerName: 'Pembayaran*', field: 'Payment', width: 122, editable: disabledInputOPL, cellEditor: 'agRichSelectCellEditor',cellEditorParams: {values: OPLPaymentData}}, 
                   { headerName: 'Estimasi Selesai*', field: 'EstimationDate', width: 150, editable: disabledInputOPL, cellEditor:'DateEditor', cellRenderer: customDate}, 
                   { headerName: 'Qty*', field: 'Qty', width: 70, editable: disabledInputOPL, cellEditor: 'numericCellEditor'},     
                   { headerName: 'Harga Beli*', field: 'HargaBeli', width: 121, editable: disabledInputOPL ,  cellEditor: 'numericCellEditor', suppressSizeToFit:true , cellStyle: {textAlign: "right"},
                               cellRenderer: function(params){ 
                                   var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
                                   var notOk = '<span style="float:left">Rp.</span>'+ 0;
                                   if(params.value ==  null || params.value ==  undefined || params.value == "" ){
                                       return notOk;
                                   } else{
                                       return isOke;
                                   }
                               }
                   },   
                   { headerName: 'Harga Jual*', field: 'HargaJual', width: 121, editable: disabledInputOPL ,  cellEditor: 'numericCellEditor', suppressSizeToFit:true  , cellStyle: {textAlign: "right"},
                               cellRenderer: function(params){ 
                                   var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
                                   var notOk = '<span style="float:left">Rp.</span>'+ 0;
                                   if(params.value ==  null || params.value ==  undefined || params.value == "" ){
                                       return notOk;
                                   } else{
                                       return isOke;
                                   }
                               }
                   }, 
                   { headerName: 'Diskon', field: 'Discount', width: 81, editable: disabledInputOPL_Diskon, cellEditor: 'numericCellEditor',suppressSizeToFit:true , cellStyle: {textAlign: "right"},
                               cellRenderer: function(params){ 
                                   var isOke = formatNumberDecimal(params.value)+'<span style="float:right">%</span>';
                                   var notOk = 0+'<span style="float:right">%</span>';
                                   if(params.value ==  null || params.value ==  undefined || params.value == "" ){
                                       return notOk;
                                   } else{
                                       return isOke;
                                   }
                               }
                   },     
                   { headerName: 'Notes', field: 'Notes', width: 150, editable: disabledInputOPL,cellEditor: 'noteCellEditor'},
                   { headerName: 'Action', width: 100, minWidth: 100, maxWidth: 100, suppressFilter: true, suppressMenu: true, pinned: 'right', cellRenderer: actionRenderer2}
               ]
           };

           // ----------------------- note editor -------------------------------------------------------------------------- start

				function noteCellEditor() {
				}
				noteCellEditor.prototype.init = function (params) {
					// create the cell

					var maxL = 200
					if (params.column.colId == 'Notes') {
						maxL = 200
					} 

					this.eInput = document.createElement('input')
					this.eInput.setAttribute('maxLength',maxL);
					console.log('DATAAA1 =>',params.charPress)
					console.log('DATAAA2 =>',params.value)
					
				
				};

				noteCellEditor.prototype.isKeyPressedNavigation = function (event){
					
				};

				// gets called once when grid ready to insert the element
				noteCellEditor.prototype.getGui = function () {
					return this.eInput;
				};

				// focus and select can be done after the gui is attached
				noteCellEditor.prototype.afterGuiAttached = function () {
					this.eInput.focus();
				};

				// returns the new value after editing
				noteCellEditor.prototype.isCancelBeforeStart = function () {
					return this.cancelBeforeStart;
				};

				// returns the new value after editing
				noteCellEditor.prototype.getValue = function () {
					return this.eInput.value;
				};

				// any cleanup we need to be done here
				noteCellEditor.prototype.destroy = function () {
					// but this example is simple, no cleanup, we could  even leave this method out as it's optional
				};

				// if true, then this editor will appear in a popup 
				noteCellEditor.prototype.isPopup = function () {
					// and we could leave this method out also, false is the default
					return false;
				};

				// ----------------------- note editor -------------------------------------------------------------------------- end

           //numeric editornya AG GRID
           // function to act as a class
           function NumericCellEditor() {
           }

           function getCharCodeFromEvent(event) {
             event = event || window.event;
             return (typeof event.which == "undefined") ? event.keyCode : event.which;
           }

           function isCharNumeric(charStr) {
             return !!/\d/.test(charStr);
           }

           function isKeyPressedNumeric(event) {
             var charCode = getCharCodeFromEvent(event);
             var charStr = String.fromCharCode(charCode);
             return isCharNumeric(charStr);
           }

           // gets called once before the renderer is used
             NumericCellEditor.prototype.init = function (params) {
					// create the cell
					var maxL = 9
					if (params.column.colId == 'Qty') {
						maxL = 6
					} else if (params.column.colId == 'HargaBeli') {
						maxL = 9
					} else if (params.column.colId == 'HargaJual') {
						maxL = 9
					} else if (params.column.colId == 'Discount') {
						maxL = 5
					}
					this.eInput = document.createElement('input');
					this.eInput.setAttribute('maxLength',maxL);
             if (isCharNumeric(params.charPress)) {
               this.eInput.value = params.charPress;
             } else {
               if (params.value !== undefined && params.value !== null) {
                 this.eInput.value = params.value;
               }
             }

             var that = this;
             this.eInput.addEventListener('keypress', function (event) {
               if (!isKeyPressedNumeric(event)) {
                   if (that.eInput.value.includes('.') || (event.key === '.' && (that.eInput.value === '' || that.eInput.value === null || that.eInput.value === undefined))){
                    that.eInput.focus();
                    event.preventDefault();
                   } else {
                    that.eInput.focus();
                    if (event.preventDefault && event.key !== '.'){
                        event.preventDefault();
                    } 
                   }
                
               } else if (that.isKeyPressedNavigation(event)){
                 event.stopPropagation();
               }
             });

             // only start edit if key pressed is a number, not a letter
             var charPressIsNotANumber = params.charPress && ('1234567890'.indexOf(params.charPress) < 0);
             this.cancelBeforeStart = charPressIsNotANumber;
           };

           NumericCellEditor.prototype.isKeyPressedNavigation = function (event){
             return event.keyCode===39
               || event.keyCode===37;
           };

           // gets called once when grid ready to insert the element
           NumericCellEditor.prototype.getGui = function () {
             return this.eInput;
           };

           // focus and select can be done after the gui is attached
           NumericCellEditor.prototype.afterGuiAttached = function () {
             this.eInput.focus();
           };

           // returns the new value after editing
           NumericCellEditor.prototype.isCancelBeforeStart = function () {
             return this.cancelBeforeStart;
           };

           // example - will reject the number if it contains the value 007
           // - not very practical, but demonstrates the method.
           NumericCellEditor.prototype.isCancelAfterEnd = function () {
             var value = this.getValue();
             return value.indexOf('007') >= 0;
           };

           // returns the new value after editing
           NumericCellEditor.prototype.getValue = function () {
             return this.eInput.value;
           };

           // any cleanup we need to be done here
           NumericCellEditor.prototype.destroy = function () {
             // but this example is simple, no cleanup, we could  even leave this method out as it's optional
           };

           // if true, then this editor will appear in a popup 
           NumericCellEditor.prototype.isPopup = function () {
             // and we could leave this method out also, false is the default
             return false;
           };
           //numeric editornya AG GRID

           function currencyFormatter(params) {
               console.log('params currencyFormatter ===>',params.value);
    
               if(params.value == "" || params.value == undefined){
                   return '0';
               }else{
                   return formatNumber(params.value);
               }
           }
    
           function formatNumber(number) {
               return Math.floor(number)
               .toString()
               .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
           }
            function formatNumberDecimal(number) {
                return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            }
    
           function gridPartsIsOPB(params){
               console.log("gridPartsIsOPB => ",params.data.IsOPB,params);
               if( (params.data.PartsName == "" &&  params.data.PartsCode != "") || (params.data.PartsName != "" &&  params.data.PartsCode == "") || params.data.IsOPB == true ){
                   return true;
               }else{
                   return false
               }
               
           }
    
           var dateFormat = 'dd/MM/yyyy';
           $scope.DateOptions = {
               startingDay: 1,
               format: dateFormat,
           }
    
           
           function actionRenderer2() {
               var actionHtml = ' <button ng-disabled="viewMode || data.Status == 2 || data.Status == 3 || isLock" skip-enable class="ui icon inverted grey button"\
                                   style="font-size:15px;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px"\
                                   ng-click="deleteDataOplDetail(data)">\
                                   <i class="fa fa-trash"></i>\
                               </button>';
               return actionHtml;
           }
           // ag grid detail
           
    
           $scope.deleteAllDataGridViewWork = function(){
               console.log("delete data opl",DataChoosedDelete);
               bsAlert.alert({
                           title: "Apakah anda yakin untuk menghapus OPL?",
                           text: "",
                           type: "question",
                           showCancelButton: true
                       },
                       function() {
                           
                         if(DataChoosedDelete.length == 0){
                             bsAlert.warning("Pilih Data Yang Akan Dihapus", "");
                         }else{
                            
                            var arrOPLNo = []
                            for (var i=0; i<DataChoosedDelete.length; i++){
                                if (DataChoosedDelete[i].OplNo != null && DataChoosedDelete[i].OplNo != undefined){
                                    var obj = {}
                                    obj.OPLNo = ''
                                    obj.OPLNo = DataChoosedDelete[i].OplNo
                                    arrOPLNo.push(obj)
                                }
                            }
                            WoHistoryGR.CheckIsOPLGateOut(arrOPLNo, $scope.mData.JobId).then(function(res) {

                                if (res.data == 666){
                                    bsAlert.warning("Status kendaraan sedang gate out OPL, silahkan gate in  OPL dahulu di Security Gate", '');
                                } else {
                                    var tmpItung = 0;
                                    for(var x = 0; x < DataChoosedDelete.length; x++){
                                    for(var y in DataChoosedDelete[x].OPLTasks){
                                        if(DataChoosedDelete[x].OPLTasks[y].Status == 2 || DataChoosedDelete[x].OPLTasks[y].Status == 1 || DataChoosedDelete[x].OPLTasks[y].Status == 3){
                                        tmpItung++
                                        }
                                    }
                                    }
                                    console.log("tmpItung",tmpItung);
                                    if(tmpItung == 0){
                                    for(var a = 0; a < DataChoosedDelete.length; a++){
                                        console.log("aye aye", DataChoosedDelete[a]);

                                        for(var c = 0; c < $scope.finalDataOPL.length; c++){
                                            console.log("losal",$scope.finalDataOPL[c]);
                                            if($scope.finalDataOPL[c].flagId == DataChoosedDelete[a].flagId){
                                    
                                                if($scope.finalDataOPL[c].OplNo == null || typeof  $scope.finalDataOPL[c].OplNo == undefined){
                                                    $scope.finalDataOPL.splice(c,1);
                                                    console.log('ini langsung splice karna blm ada id nya')
                                                }else{
                                                    $scope.finalDataOPL[c].isDeleted = 1;
                                                    console.log('ini cuma ganti status is Deleted nyaa')
                                    
                                                }
                                            }
                                            
                                            
                                        }
                                    }
                                    $scope.setDataMaster();
                                    $scope.countSummaryOPl();
                                    DataChoosedDelete = [];
                                    }else{
                                    bsAlert.warning("Sudah ada Job yang on progress / completed", '');
                                    }

                                }
                            })
                           
                             
                         }
                       },
                       function() {

                       }
               )
               
           }
    
           $scope.countSummaryOPl = function(){
             console.log("count summary opl",$scope.finalDataOPL);
             
             var totalWOPL = 0;
             var totalDiscountedPrice = 0;
             for(var a in $scope.finalDataOPL){
               if($scope.finalDataOPL[a].OPLTasks == undefined){
                 $scope.finalDataOPL[a].OPLTasks = $scope.finalDataOPL[a].detail;
               }
               // for (var i = 0; i < $scope.finalDataOPL[a].OPLTasks.length; i++) {
               //     if($scope.finalDataOPL[a].isDeleted != 1){
               //         if($scope.finalDataOPL[a].OPLTasks[i].isDeleted != 1){
               //             if ($scope.finalDataOPL[a].OPLTasks[i].Price != undefined) {
               //               if($scope.finalDataOPL[a].OPLTasks[i].PaymentId == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId  == 30 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId == 32){
               //                 totalWOPL += 0;
               //               }else{
               //                 totalWOPL += parseFloat($scope.finalDataOPL[a].OPLTasks[i].Price  / 1.1) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty);
               //               }
               //             }else if($scope.finalDataOPL[a].OPLTasks[i].HargaJual != undefined){
               //               if($scope.finalDataOPL[a].OPLTasks[i].PaymentId == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId  == 30 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId == 32){
               //                 totalWOPL += 0;
               //               }else{
               //                 totalWOPL += parseFloat($scope.finalDataOPL[a].OPLTasks[i].HargaJual  / 1.1) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty);
               //               }
               //             }
               //         }
               //     }
               // };
               // // $scope.sumWorkOpl = totalWOPL;
               // $scope.sumWorkOpl = Math.round(totalWOPL / (1.1));
               for (var i = 0; i < $scope.finalDataOPL[a].OPLTasks.length; i++) {
                 if($scope.finalDataOPL[a].isDeleted != 1){
                   if($scope.finalDataOPL[a].OPLTasks[i].isDeleted != 1){
                     if ($scope.finalDataOPL[a].OPLTasks[i].Price !== undefined) {
                    //    if ($scope.finalDataOPL[a].OPLTasks[i].PaidById == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaidById == 30|| $scope.finalDataOPL[a].OPLTasks[i].PaidById == 32){
                       if ($scope.finalDataOPL[a].OPLTasks[i].PaymentId != 28 && $scope.finalDataOPL[a].OPLTasks[i].PaymentId != 2458 && $scope.finalDataOPL[a].OPLTasks[i].PaymentId != 32){
                           totalWOPL += 0
                       } else {
                           // totalWOPL += $scope.finalDataOPL[a].OPLTasks[i].Price * parseFloat($scope.finalDataOPL[a].OPLTasks[i].QtyPekerjaan);
                           totalWOPL += ASPricingEngine.calculate({
                                           InputPrice: $scope.finalDataOPL[a].OPLTasks[i].Price,
                                           // Discount: 5,
                                           Qty: parseFloat($scope.finalDataOPL[a].OPLTasks[i].Qty),
                                           Tipe: 102, 
                                           PPNPercentage: $scope.PPNPerc,
                                       });
                       }
                     }else if($scope.finalDataOPL[a].OPLTasks[i].HargaJual != undefined){
                    //    if ($scope.finalDataOPL[a].OPLTasks[i].PaidById == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaidById == 30|| $scope.finalDataOPL[a].OPLTasks[i].PaidById == 32){
                       if ($scope.finalDataOPL[a].OPLTasks[i].PaymentId != 28 && $scope.finalDataOPL[a].OPLTasks[i].PaymentId != 2458 && $scope.finalDataOPL[a].OPLTasks[i].PaymentId != 32){

                           totalWOPL += 0
                       } else {
                           // totalWOPL += $scope.finalDataOPL[a].OPLTasks[i].Price * parseFloat($scope.finalDataOPL[a].OPLTasks[i].QtyPekerjaan);
                           totalWOPL += ASPricingEngine.calculate({
                                           InputPrice: $scope.finalDataOPL[a].OPLTasks[i].HargaJual,
                                           // Discount: 5,
                                           Qty: parseFloat($scope.finalDataOPL[a].OPLTasks[i].Qty),
                                           Tipe: 102, 
                                           PPNPercentage: $scope.PPNPerc,
                                       });
                       }
                     }
                   }
                 }
                   
               };
               $scope.sumWorkOpl = totalWOPL;
    
    
               // for (var i = 0; i < $scope.finalDataOPL[a].OPLTasks.length; i++) {
    
               //     if($scope.finalDataOPL[a].isDeleted != 1){
               //         if($scope.finalDataOPL[a].OPLTasks[i].isDeleted != 1){
               //             $scope.finalDataOPL[a].OPLTasks[i].Discount = $scope.finalDataOPL[a].OPLTasks[i].Discount ? $scope.finalDataOPL[a].OPLTasks[i].Discount : 0;
                           
               //             if ($scope.finalDataOPL[a].OPLTasks[i].Price != undefined) {
                             
               //                 $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = (parseFloat($scope.finalDataOPL[a].OPLTasks[i].Price / 1.1)* parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) - ((parseFloat($scope.finalDataOPL[a].OPLTasks[i].Price / 1.1) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) * $scope.finalDataOPL[a].OPLTasks[i].Discount / 100);
                             
               //             }else if($scope.finalDataOPL[a].OPLTasks[i].HargaJual != undefined){

               //                 $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = (parseFloat($scope.finalDataOPL[a].OPLTasks[i].HargaJual / 1.1)* parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) - ((parseFloat($scope.finalDataOPL[a].OPLTasks[i].HargaJual / 1.1)* parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) * $scope.finalDataOPL[a].OPLTasks[i].Discount / 100);
                            
               //             }
               //             if ($scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL !== undefined) {
               //               if($scope.finalDataOPL[a].OPLTasks[i].PaymentId == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId == 30 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId == 32){
               //               //  totalDiscountedPrice += 0;
               //               }else{
               //                 totalDiscountedPrice += $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL;
               //                 console.log("ga masuk else opl");
               //               }
               //             } else {
               //                 totalDiscountedPrice = 0;
               //                 console.log("masuk else opl");
               //             }
               //             console.log("$scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL", $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL);   
               //         }
               //     }
               // };
               // console.log("totalDiscountedPrice", totalDiscountedPrice);
               // // $scope.sumWorkOplDiscounted = totalDiscountedPrice;
               // $scope.sumWorkOplDiscounted = Math.round(totalDiscountedPrice / (1.1));
               // console.log('$scope.sumWorkOpl', $scope.sumWorkOpl);
               // console.log("$scope.sumWorkOplDiscounted",$scope.sumWorkOplDiscounted);
               for (var i = 0; i < $scope.finalDataOPL[a].OPLTasks.length; i++) {

                   if($scope.finalDataOPL[a].isDeleted != 1){
                       if($scope.finalDataOPL[a].OPLTasks[i].isDeleted != 1){
                           // $scope.finalDataOPL[a].OPLTasks[i].Qty = $scope.finalDataOPL[a].OPLTasks[i].QtyPekerjaan;
                           $scope.finalDataOPL[a].OPLTasks[i].Discount = $scope.finalDataOPL[a].OPLTasks[i].Discount ? $scope.finalDataOPL[a].OPLTasks[i].Discount : 0;
                           
                           if ($scope.finalDataOPL[a].OPLTasks[i].Price != undefined) {
                               // if ($scope.finalDataOPL[a].OPLTasks[i].PaidById == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaidById == 30|| $scope.finalDataOPL[a].OPLTasks[i].PaidById == 32){
                               //     $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = 0
                               // } else {
                                   $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = ASPricingEngine.calculate({
                                                                           InputPrice: $scope.finalDataOPL[a].OPLTasks[i].Price,
                                                                           Discount: $scope.finalDataOPL[a].OPLTasks[i].Discount,
                                                                           Qty: $scope.finalDataOPL[a].OPLTasks[i].Qty,
                                                                           Tipe: 104, 
                                                                           PPNPercentage: $scope.PPNPerc,
                                                                       }); 
                               // }
                               // $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = (parseFloat($scope.finalDataOPL[a].OPLTasks[i].Price) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) - ((parseFloat($scope.finalDataOPL[a].OPLTasks[i].Price) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) * $scope.finalDataOPL[a].OPLTasks[i].Discount / 100);
                             
                           }else if($scope.finalDataOPL[a].OPLTasks[i].HargaJual != undefined){
                               $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = ASPricingEngine.calculate({
                                                                       InputPrice: $scope.finalDataOPL[a].OPLTasks[i].HargaJual,
                                                                       Discount: $scope.finalDataOPL[a].OPLTasks[i].Discount,
                                                                       Qty: $scope.finalDataOPL[a].OPLTasks[i].Qty,
                                                                       Tipe: 104, 
                                                                       PPNPercentage: $scope.PPNPerc,
                                                                   }); 
                               // $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = (parseFloat($scope.finalDataOPL[a].OPLTasks[i].HargaJual) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) - ((parseFloat($scope.finalDataOPL[a].OPLTasks[i].HargaJual)  * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) * $scope.finalDataOPL[a].OPLTasks[i].Discount / 100);
                            
                           }
                           if ($scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL !== undefined) {
                            //  if($scope.finalDataOPL[a].OPLTasks[i].PaymentId == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId  == 30 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId == 32){
                             if($scope.finalDataOPL[a].OPLTasks[i].PaymentId != 28 && $scope.finalDataOPL[a].OPLTasks[i].PaymentId  != 2458 && $scope.finalDataOPL[a].OPLTasks[i].PaymentId != 32){

                               totalDiscountedPrice += 0;
                             }else{
                               totalDiscountedPrice += $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL;
                               console.log("ga masuk else opl");
                             }
                           } else {
                               totalDiscountedPrice = 0;
                               console.log("masuk else opl");
                           }
                           console.log("$scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL", $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL);   
                       }
                   }
               };
               console.log("totalDiscountedPrice", totalDiscountedPrice);
               $scope.sumWorkOplDiscounted = totalDiscountedPrice;
               // $scope.sumWorkOplDiscounted = Math.round(totalDiscountedPrice / (1.1));
               console.log('$scope.sumWorkOpl', $scope.sumWorkOpl);
               console.log("$scope.sumWorkOplDiscounted",$scope.sumWorkOplDiscounted);
             }
             $scope.sumAllPrice();
           }
    
            $scope.closeModalOpl = function(){
                console.log("dataTerpisahkan",dataTerpisahkan);
                console.log("$scope.dataEdit",$scope.dataEdit);
                console.log("ini cancel",$scope.masterOPL);

                console.log("closeModalOpl | $scope.masterOPL ===>",$scope.masterOPL);
                console.log("closeModalOpl | tmpDataOPL =========>",tmpDataOPL);
                        
                console.log("flagedit",flagedit);
                console.log("$scope.finalDataOPL",$scope.finalDataOPL);
                if(flagedit == 1){
                  
                  if($scope.masterOPL.length > 0){

                    for(var b in $scope.dataEdit.detail){

                      for(var a in dataTerpisahkan){
                        // if($scope.dataEdit[b].Id != null){

                          console.log("$scope.dataEdit.detail[b].Id",$scope.dataEdit.detail[b].Id);
                          if($scope.dataEdit.detail[b].Id == dataTerpisahkan[a].Id){

                            dataTerpisahkan.splice(a,1);
                          }
                        // }
                      }
                    }
                  }else{
                    if($scope.dataEdit == undefined){
                      console.log("bukan dari edit tapi dari tambah");
                    }else{
                      for(var b in $scope.dataEdit.detail){

                        for(var a in dataTerpisahkan){
                          // if($scope.dataEdit[b].Id != null){

                            console.log("$scope.dataEdit.detail[b].Id",$scope.dataEdit.detail[b].Id);
                            if($scope.dataEdit.detail[b].Id == dataTerpisahkan[a].Id){

                              dataTerpisahkan.splice(a,1);
                            }
                          // }
                        }
                      }
                    }
                    
                  }
                }
                if(flagedit == 1){
                    if($scope.finalDataOPL.length > 0){
                        for(var i in $scope.finalDataOPL){
                            if($scope.finalDataOPL[i].flagId == tmpDataOPL.flagId){
                                $scope.finalDataOPL[i] = tmpDataOPL;
                            }
                        }
                    }
                    
                    
                        
                    
                    $scope.masterOPL = [];
                    $scope.mOpl = {};
                    $scope.gridDetailOPLWoListApi.setRowData($scope.masterOPL);

                    for(var a = 0; a < $scope.finalDataOPL.length; a++){
                        $scope.finalDataOPL[a].No = a + 1;
                        $scope.finalDataOPL[a].Qty = 0;
                        for(var b = 0; b < $scope.finalDataOPL[a].detail.length; b++){
                               $scope.finalDataOPL[a].detail[b].No = $scope.finalDataOPL[a].No +"."+ (b+1);

                               $scope.finalDataOPL[a].Qty += parseInt($scope.finalDataOPL[a].detail[b].Qty);
                        }
                    }
                    var DataTMPShow =[];
                    for(var lo in $scope.finalDataOPL){
                      if($scope.finalDataOPL[lo].isDeleted != 1){
                        DataTMPShow.push($scope.finalDataOPL[lo]);
                      }
                    }
                    for(var a = 0; a < DataTMPShow.length; a++){
                        DataTMPShow[a].No = a + 1;
                        DataTMPShow[a].Qty = 0;
                        for(var b = 0; b < DataTMPShow[a].detail.length; b++){
                               DataTMPShow[a].detail[b].No = DataTMPShow[a].No +"."+ (b+1);
                               DataTMPShow[a].Qty += parseInt(DataTMPShow[a].detail[b].Qty);

                        }
                    }
                    console.log("DataTMPShow",DataTMPShow);
                    $scope.agGridWorkViewApi.setRowData(DataTMPShow);
                }
                
                // karena kl uda select beberapa data trs buka modal dan modal nya di cancel, selected nya jg ilang
                DataChoosedDelete = [];
                // karena kl uda select beberapa data trs buka modal dan modal nya di cancel, selected nya jg ilang


                angular.element(".ui.modal.ModalOPLWoList").modal("hide");
            };
        
            $scope.saveFinalOpl = function(){
                console.log("ini simpan",$scope.masterOPL);
                console.log("ini simpan",$scope.masterOPL);
                $scope.gridDetailOPLWoListApi.stopEditing();
                var filterVendor = $scope.OplVendor.filter(function(item) { 
                    return item.VendorId ==  $scope.mOpl.vendor;  
                });
                console.log("filterVendor",filterVendor);
                console.log("flagedit",flagedit);
                if(flagedit == 1){
                    console.log('saveFinalOpl | edit =========>',flagedit);
                    console.log('saveFinalOpl | masterOPL ====>',$scope.masterOPL);

                    for(var a in $scope.masterOPL){
                        console.log("$scope.masterOPL[a].IsSaveFin",$scope.masterOPL[a].IsSaveFin);
                        if($scope.masterOPL[a].IsSaveFin == undefined){
        
                            if($scope.masterOPL[a].Discount == ""){
                                $scope.masterOPL[a].Discount = 0;
                            }else{
                                $scope.masterOPL[a].Discount = parseFloat($scope.masterOPL[a].Discount);
                                // $scope.masterOPL[a].Discount = parseInt($scope.masterOPL[a].Discount);
                            }
                            $scope.masterOPL[a].IsSaveFin = 1;
                        }
                    }


		            console.log("saveFinalOpl | tmpDataOPL===>",tmpDataOPL);
                    for(var i in $scope.finalDataOPL){
                        if($scope.finalDataOPL[i].flagId == tmpDataOPL.flagId){
                            $scope.finalDataOPL[i].Qty = $scope.masterOPL.length
                        }
                    }
                }else{
                    console.log('saveFinalOpl | add ====>',flagedit)
                    if($scope.finalDataOPL.length == 0){
                        var Id =  Math.floor(Math.random() * 10000) + 1;

                        var detailTask = []
                        
                        for(var a in $scope.masterOPL){
                            if($scope.masterOPL[a].Discount == ""){
                                $scope.masterOPL[a].Discount = 0;
                            }else{
                                $scope.masterOPL[a].Discount = parseFloat($scope.masterOPL[a].Discount);
                                // $scope.masterOPL[a].Discount = parseInt($scope.masterOPL[a].Discount);
                            }
                            $scope.masterOPL[a].IsSaveFin = 1;
                            $scope.masterOPL[a].flagId = Id;

                            $scope.masterOPL[a].VendorId = filterVendor[0].VendorId;
                            detailTask.push($scope.masterOPL[a])
                        }

                        $scope.finalDataOPL.push(
                            {
                                flagId  : Id,
                                detail  : detailTask,
                                OPLNo   : null,
                                Vendor  : filterVendor[0].Name,
                                VendorId: filterVendor[0].VendorId,
                                Qty     : $scope.masterOPL.length,
                                Status  : "",
                            }
                        );
                    }else{
                        var filterSave = $scope.finalDataOPL.filter(function(item) { 
                            
                            return item.VendorId ==  filterVendor[0].VendorId && item.isDeleted != 1;  
                        });
                        console.log("filterSave",filterSave);
                        if(filterSave.length == 0){
                            var Id =  Math.floor(Math.random() * 10000) + 1;
                            var detailTask = []

                            for(var a in $scope.masterOPL){
                                if($scope.masterOPL[a].Discount == ""){
                                    $scope.masterOPL[a].Discount = 0;
                                }else{
                                    $scope.masterOPL[a].Discount = parseFloat($scope.masterOPL[a].Discount);
                                    // $scope.masterOPL[a].Discount = parseInt($scope.masterOPL[a].Discount);
                                }
                                $scope.masterOPL[a].IsSaveFin = 1;
                                $scope.masterOPL[a].flagId = Id;


                                $scope.masterOPL[a].VendorId = filterVendor[0].VendorId;
                                detailTask.push($scope.masterOPL[a])
                                
                            }
                            $scope.finalDataOPL.push(
                                {
                                    flagId  : Id,
                                    detail  : detailTask,
                                    OPLNo   : null,
                                    Vendor  : filterVendor[0].Name,
                                    VendorId: filterVendor[0].VendorId,
                                    Qty     : $scope.masterOPL.length,
                                    Status  : "",
                                }
                            );

                        }else{
                        bsAlert.warning("Data Vendor Sudah Ada", 'Silakan melakukan edit untuk menambahkan pekerjaan');
                        }
                    }
                    
                }
                console.log("$scope.finalDataOPL",$scope.finalDataOPL);
                console.log("dataTerpisahkan",dataTerpisahkan);
                // $scope.masterOPL = [];
                console.log("filterSave under",filterSave);
                tmpDataOPL = angular.copy($scope.finalDataOPL);
                $scope.mOpl = {};
                $scope.setDataMaster(2,false);
                $scope.countSummaryOPl();
                $scope.gridDetailOPLWoListApi.setRowData($scope.masterOPL);
                angular.element(".ui.modal.ModalOPLWoList").modal("hide");  
                console.log("saveFinalOpl | dataFinalForDisplay ===>",dataFinalForDisplay);
                console.log("saveFinalOpl | masterOPL ===>",$scope.masterOPL);
                sizeToFit()

                // karena kl uda select beberapa data trs buka modal dan modal nya di save, selected nya jg ilang
                DataChoosedDelete = [];
                // karena kl uda select beberapa data trs buka modal dan modal nya di save, selected nya jg ilang

                // $timeout(function() {
                //     $scope.reziseColumnAG();
                // }, 1000);
            }
            
            function sizeToFit() {
                $scope.agGridWorkViewApi.sizeColumnsToFit();
            }

            $scope.calenderPosition = function(){
                var cols = document.getElementsByClassName('ag-root-wrapper ag-layout-normal ag-ltr');
                for(i = 0; i < cols.length; i++) {
                    cols[i].style.overflow = 'auto';
                }
            }

            window.addEventListener( 
                "resize", getSizes, false); 
      
            function getSizes() { 
                var zoomNow = 0;
                var zoom = ((window.outerWidth - 10) 
                    / window.innerWidth) * 100; 
                
                if(zoomNow != zoom){
                    sizeToFit()
                }
            } 
            
            $scope.saveOPLEnd = function(){
                console.log("$scope.mData",$scope.mData);
                console.log("$scope.finalDataOPL",$scope.finalDataOPL);
                if($scope.finalDataOPL == undefined){
                    console.log("sayangnya data opl undefined")
                }else{
        
                    console.log("dataTerpisahkan",dataTerpisahkan);
                    console.log("$scope.dataEdit",$scope.dataEdit);
                    if($scope.finalDataOPL.length > 0){
                        // var dataSimpanFinalOPl = [];
                        for(var a in $scope.finalDataOPL){
                          for(var b in dataTerpisahkan){
                              if($scope.finalDataOPL[a].OplNo == dataTerpisahkan[b].OplNo){
                              dataTerpisahkan[b].isDeleted = 1;
                              $scope.finalDataOPL[a].detail.push(dataTerpisahkan[b]);
                              }
                          }
                        }
                        console.log("Status",Status);
                        console.log("$scope.finalDataOPL last",$scope.finalDataOPL);

                        // nyamain status isdeleted di dalem opltask dan detail --------------------- start
                        for (var i=0; i<$scope.finalDataOPL.length; i++){
                            if ($scope.finalDataOPL[i].isDeleted == 1){
                                for (var j=0; j<$scope.finalDataOPL[i].OPLTasks.length; j++){
                                    $scope.finalDataOPL[i].OPLTasks[j].isDeleted = 1;
                                }
                                for (var k=0; k<$scope.finalDataOPL[i].detail.length; k++){
                                    $scope.finalDataOPL[i].detail[k].isDeleted = 1;
                                }
                            }
                        }


                        // nyamain status isdeleted di dalem opltask dan detail --------------------- end

                        
                            // if($scope.mData.JobTaskOplCR.length > 0){s
                                WoHistoryGR.putOPlData($scope.finalDataOPL,$scope.mData.JobId,Status, $scope.mData).then(function (res) {});
                            // }else{
                            //     WoHistoryGR.postOPLData($scope.finalDataOPL,$scope.mData.JobId).then(function (res) {});
                            // }
                    }
                }
            }
           
            var dataFinalForDisplay = [];
            $scope.setDataMaster = function(flag,addKeterangan){
                dataFinalForDisplay = [];
                console.log("flag",flag);
                for(var a in $scope.finalDataOPL){
                    if($scope.finalDataOPL[a].isDeleted != 1){
        
                        dataFinalForDisplay.push($scope.finalDataOPL[a]);
                        
                    }
                }
                if(flag == 1){
                    // 1 = dari close modal
                    if($scope.dataEdit == undefined){
                    }else{
                        // for(var a in dataFinalForDisplay){
                        //     if(dataFinalForDisplay[a].flagId == $scope.dataEdit.flagId){
                        //     dataFinalForDisplay[a] = $scope.dataEdit;
                        //     }
                        // }


                        if(dataFinalForDisplay.length > 0){
                            for(var i in dataFinalForDisplay){
                                if(dataFinalForDisplay[i].flagId == tmpDataOPL.flagId){
                                    dataFinalForDisplay[i] = tmpDataOPL;
                                }
                            }
                        }
                    }   
                }else if(flag == 2){
                    // 2 = dari save final opl
                    console.log("didalem 2 $scope.masterOPL",$scope.masterOPL);
                    for(var a in dataFinalForDisplay){
                        for(var b in $scope.masterOPL){
                            if(dataFinalForDisplay[a].flagId == $scope.masterOPL[b].flagId){
                                dataFinalForDisplay[a].detail = $scope.masterOPL;
                            }
                        }
                    }
                }
                console.log("woiioioio",dataFinalForDisplay);
                for(var a = 0; a < dataFinalForDisplay.length; a++){
                  var flagStatsOpen = 0;
                  var flagStats = 0;
                  // var disabledFlag = 0;
                    console.log("dataFinalForDisplay[a].isDeleted",dataFinalForDisplay[a].isDeleted);
                    if(dataFinalForDisplay[a].isDeleted != 1){
                        dataFinalForDisplay[a].Qty = 0;
                        dataFinalForDisplay[a].No = a + 1;
                        
                        for(var b = 0; b < dataFinalForDisplay[a].detail.length; b++){
                            // if(dataFinalForDisplay[a].detail[b].isDeleted != 1){
                                console.log("dataFinalForDisplay[a].detail[b].No",dataFinalForDisplay[a].detail[b].No);
                                dataFinalForDisplay[a].detail[b].No = dataFinalForDisplay[a].No +"."+ (b+1);
                                console.log("addKeterangan",addKeterangan);
                                if(addKeterangan == true){
                                     // dataFinalForDisplay[a].detail[b].isExternal =  parseInt(dataFinalForDisplay[a].detail[b].isExternal);
                                    // console.log("dataFinalForDisplay[a].detail[b].isExternal",dataFinalForDisplay[a].detail[b]);
                                    // if(dataFinalForDisplay[a].detail[b].isExternal == 1){
                                    //     dataFinalForDisplay[a].detail[b].TaskNameOpl = dataFinalForDisplay[a].detail[b].TaskNameOpl + " (External)"
                                    //   }else{
                                    //     dataFinalForDisplay[a].detail[b].TaskNameOpl = dataFinalForDisplay[a].detail[b].TaskNameOpl + " (Internal)"
                                    //   }
                                  }

                                dataFinalForDisplay[a].Qty += parseInt(dataFinalForDisplay[a].detail[b].Qty);
                                if(dataFinalForDisplay[a].detail[b].Status == 0 || dataFinalForDisplay[a].detail[b].Status == undefined){
                                  flagStatsOpen++
                                }
                                if(dataFinalForDisplay[a].detail[b].Status == 1){
                                  flagStats++
                                }
                                if(flagStatsOpen > 0){

                                  dataFinalForDisplay[a].Status = "Open";
                                }else{

                                  if(flagStats > 0){
                                    //kalo status jobtaskoplnya ada yg 1 maka status parentoplnya jadi on progress
                                    dataFinalForDisplay[a].Status = "On Progress";
                                  }
                                }
                            // }
                        }
                        console.log("flagStats",flagStats);
                        console.log("flagStatsOpen",flagStatsOpen);
                         
                    }
                }
                // console.log("dataFinalForDisplay",dataFinalForDisplay);
                $scope.agGridWorkViewApi.setRowData(dataFinalForDisplay);
            }
    
           var Status;
           $scope.getdataOpl = function(){

               console.log("$scope.minDateOption",$scope.minDateOption);
               console.log("$scope.mData in opl controller",$scope.mData);
               var ampunn = angular.copy($scope.mData.JobTaskOplCR);
               var totalWOPL = 0;
               var totalDiscountedPrice = 0;
               dataTerpisahkan = [];
               console.log("ampunn",ampunn);
               if($scope.mData.JobTaskOplCR != undefined){
    
                   if($scope.mData.JobTaskOplCR.length > 0){
    
                       for(var a in $scope.mData.JobTaskOplCR){
                         var flagStats = 0;
                         var flagStatsOpen = 0;
                          var Id =  Math.floor(Math.random() * 10000) + 1;
                          $scope.mData.JobTaskOplCR[a].flagId = Id;
                          $scope.mData.JobTaskOplCR[a].Vendor = $scope.mData.JobTaskOplCR[a].VendorName;
                          $scope.mData.JobTaskOplCR[a].Qty = $scope.mData.JobTaskOplCR[a].OPLTasks.length;
                          // if($scope.mData.JobTaskOplCR[a].VendorName == null){
                            var filteredOPL = $scope.OplVendor.filter(function(item) { 
                               return item.VendorId == $scope.mData.JobTaskOplCR[a].VendorId;  
                            });
                            console.log("filteredOPL",filteredOPL);
                            $scope.mData.JobTaskOplCR[a].Vendor = filteredOPL[0].Name;
                            $scope.mData.JobTaskOplCR[a].VendorName = filteredOPL[0].Name;
                          // }
                          Status = $scope.mData.JobTaskOplCR[a].OPLTasks[0].Status;
                          if($scope.mData.JobTaskOplCR[a].OPLTasks[0].Status == 0){
    
                              $scope.mData.JobTaskOplCR[a].Status = "Open";
                              $scope.mData.JobTaskOplCR[a].xStatus = 0;
                          }else if($scope.mData.JobTaskOplCR[a].OPLTasks[0].Status == 1){
    
                              $scope.mData.JobTaskOplCR[a].Status = "On Progress";

                              $scope.mData.JobTaskOplCR[a].xStatus = 1;
                          }else if($scope.mData.JobTaskOplCR[a].OPLTasks[0].Status == 2){
    
                              $scope.mData.JobTaskOplCR[a].Status = "Completed";

                              $scope.mData.JobTaskOplCR[a].xStatus = 2;
                          }else if($scope.mData.JobTaskOplCR[a].OPLTasks[0].Status == 3){
    
                              $scope.mData.JobTaskOplCR[a].Status = "Billing";

                              $scope.mData.JobTaskOplCR[a].xStatus = 3;
                          }else if($scope.mData.JobTaskOplCR[a].OPLTasks[0].Status == 4){
    
                              $scope.mData.JobTaskOplCR[a].Status = "Cancel";

                              $scope.mData.JobTaskOplCR[a].xStatus = 4;
                          }else{
    
                              $scope.mData.JobTaskOplCR[a].Status = " - ";
                          }


                          
                          for(var b in $scope.mData.JobTaskOplCR[a].OPLTasks){
                              
                                if ($scope.mData.JobTaskOplCR[a].OPLTasks[b].Status != 3 && $scope.mData.JobTaskOplCR[a].OPLTasks[b].Status != 4){
                                    if($scope.mData.JobTaskOplCR[a].OPLTasks[b].VendorInvoiceDate != null && $scope.mData.JobTaskOplCR[a].OPLTasks[b].VendorInvoiceNo != null && $scope.mData.JobTaskOplCR[a].OPLTasks[b].FinishDate != null){
                                        $scope.mData.JobTaskOplCR[a].OPLTasks[b].Status = 2;
                                    }
                                }
                              
                              if($scope.mData.JobTaskOplCR[a].OPLTasks[b].Status == 1){
                                flagStats++
                              }

                              if($scope.mData.JobTaskOplCR[a].OPLTasks[b].Status == 0){
                                flagStatsOpen++
                              }
                              $scope.mData.JobTaskOplCR[a].OPLTasks[b].Id = Math.floor(Math.random() * 10000) + 1;
                              $scope.mData.JobTaskOplCR[a].OPLTasks[b].TaskNameOpl = $scope.mData.JobTaskOplCR[a].OPLTasks[b].OPLWorkName;
                              $scope.mData.JobTaskOplCR[a].OPLTasks[b].PaymentId = $scope.mData.JobTaskOplCR[a].OPLTasks[b].PaidById;
                              $scope.mData.JobTaskOplCR[a].OPLTasks[b].EstimationDate = $scope.mData.JobTaskOplCR[a].OPLTasks[b].TargetFinishDate;
                              $scope.mData.JobTaskOplCR[a].OPLTasks[b].Qty = $scope.mData.JobTaskOplCR[a].OPLTasks[b].QtyPekerjaan;
                              $scope.mData.JobTaskOplCR[a].OPLTasks[b].HargaBeli = $scope.mData.JobTaskOplCR[a].OPLTasks[b].OPLPurchasePrice;
                              $scope.mData.JobTaskOplCR[a].OPLTasks[b].HargaJual = $scope.mData.JobTaskOplCR[a].OPLTasks[b].Price;
                              $scope.mData.JobTaskOplCR[a].OPLTasks[b].oplId = $scope.mData.JobTaskOplCR[a].OPLTasks[b].OPLId;
                              for(var c in $scope.OPLPaymentData){
                                  // console.log("$scope.mData.JobTaskOplCR[a].OPLTasks[b].PaidById",$scope.mData.JobTaskOplCR[a].OPLTasks[b].PaidById);
                                  if($scope.OPLPaymentData[c].MasterId == $scope.mData.JobTaskOplCR[a].OPLTasks[b].PaidById){
                                      $scope.mData.JobTaskOplCR[a].OPLTasks[b].Payment = $scope.OPLPaymentData[c].Name;
                                     
                                  }
                              }
                              // $scope.mData.JobTaskOplCR[a].OPLTasks[b].IsExternal = parseInt( $scope.mData.JobTaskOplCR[a].OPLTasks[b].IsExternal);
                              console.log("$scope.mData.JobTaskOplCR[a].OPLTasks[b].IsExternal",$scope.mData.JobTaskOplCR[a].OPLTasks[b].IsExternal);
                              if($scope.mData.JobTaskOplCR[a].OPLTasks[b].IsExternal == "1"){
                                console.log("masuk external",$scope.mData.JobTaskOplCR[a].OPLTasks[b]);
                                  $scope.mData.JobTaskOplCR[a].OPLTasks[b].TaskNameOpl = $scope.mData.JobTaskOplCR[a].OPLTasks[b].TaskNameOpl + " (External)"
                              }else if($scope.mData.JobTaskOplCR[a].OPLTasks[b].IsExternal == "0"){
                                console.log("masuk internal",$scope.mData.JobTaskOplCR[a].OPLTasks[b]);
                                  $scope.mData.JobTaskOplCR[a].OPLTasks[b].TaskNameOpl = $scope.mData.JobTaskOplCR[a].OPLTasks[b].TaskNameOpl + " (Internal)"
                              }
                          }
                          $scope.mData.JobTaskOplCR[a].detail = $scope.mData.JobTaskOplCR[a].OPLTasks;
                          
                          
                          for (var i = 0; i < $scope.mData.JobTaskOplCR[a].OPLTasks.length; i++) {
                              if ($scope.mData.JobTaskOplCR[a].OPLTasks[i].Price != undefined) {

                                if ($scope.mData.JobTaskOplCR[a].OPLTasks[i].PaidById != 28 && $scope.mData.JobTaskOplCR[a].OPLTasks[i].PaidById != 2458){
                                    totalWOPL += 0
                                } else {
                                    // totalWOPL += $scope.mData.JobTaskOplCR[a].OPLTasks[i].Price * parseFloat($scope.mData.JobTaskOplCR[a].OPLTasks[i].QtyPekerjaan);
                                    totalWOPL += ASPricingEngine.calculate({
                                                    InputPrice: $scope.mData.JobTaskOplCR[a].OPLTasks[i].Price,
                                                    // Discount: 5,
                                                    Qty: parseFloat($scope.mData.JobTaskOplCR[a].OPLTasks[i].QtyPekerjaan),
                                                    Tipe: 102, 
                                                    PPNPercentage: $scope.PPNPerc,
                                                }); 
                                }

                                // ini ga pake pricing engine
                                // if($scope.mData.JobTaskOplCR[a].OPLTasks[i].PaymentId == 2277 || $scope.mData.JobTaskOplCR[a].OPLTasks[i].PaymentId  == 30 || $scope.mData.JobTaskOplCR[a].OPLTasks[i].PaymentId == 32){
                                //   totalWOPL += 0;
                                // }else{
                                //   totalWOPL += parseFloat($scope.mData.JobTaskOplCR[a].OPLTasks[i].Price) * parseInt($scope.mData.JobTaskOplCR[a].OPLTasks[i].Qty);
                                // }
                              }
                          };
                          $scope.sumWorkOpl = totalWOPL;
                        //   $scope.sumWorkOpl = totalWOPL / (1.1);
      
                          
                          for (var i = 0; i < $scope.mData.JobTaskOplCR[a].OPLTasks.length; i++) {

                            $scope.mData.JobTaskOplCR[a].OPLTasks[i].Discount = $scope.mData.JobTaskOplCR[a].OPLTasks[i].Discount ? $scope.mData.JobTaskOplCR[a].OPLTasks[i].Discount : 0;
                          
                            if ($scope.mData.JobTaskOplCR[a].OPLTasks[i].PaidById != 28 && $scope.mData.JobTaskOplCR[a].OPLTasks[i].PaidById != 2458){
                                $scope.mData.JobTaskOplCR[a].OPLTasks[i].DiscountedPriceOPL = 0
                            } else {
                                $scope.mData.JobTaskOplCR[a].OPLTasks[i].DiscountedPriceOPL = ASPricingEngine.calculate({
                                                                            InputPrice: $scope.mData.JobTaskOplCR[a].OPLTasks[i].Price,
                                                                            Discount: $scope.mData.JobTaskOplCR[a].OPLTasks[i].Discount,
                                                                            Qty: $scope.mData.JobTaskOplCR[a].OPLTasks[i].QtyPekerjaan,
                                                                            Tipe: 104, 
                                                                            PPNPercentage: $scope.PPNPerc,
                                                                        }); 
                            }
                            
                            if ($scope.mData.JobTaskOplCR[a].OPLTasks[i].DiscountedPriceOPL !== undefined) {
                                totalDiscountedPrice += $scope.mData.JobTaskOplCR[a].OPLTasks[i].DiscountedPriceOPL;
                                console.log("ga masuk else opl");
                            } else {
                                totalDiscountedPrice = 0;
                                console.log("masuk else opl");
                            }
                            console.log("$scope.mData.JobTaskOplCR[a].OPLTasks[i].DiscountedPriceOPL", $scope.mData.JobTaskOplCR[a].OPLTasks[i].DiscountedPriceOPL);

                            //   // if($scope.mData.JobTaskOplCR[a].OPLTasks[i].FinishDate != null){
                            //   //   var event2 = new Date($scope.mData.JobTaskOplCR[a].OPLTasks[i].FinishDate);
                            //   //   console.log("event2 atas",event2);
                            //   //   var event1 = new Date('Fri Jan 01 2021');
                            //   //   console.log("event1",event1);
                            //   //   event2.setTime(event1.getTime());
                            //   //   console.log("event2 bawah",event2);
                            //   //   $scope.mData.JobTaskOplCR[a].OPLTasks[i].FinishDate = event2;
                            //   // }
                              
                            //   $scope.mData.JobTaskOplCR[a].OPLTasks[i].Discount = $scope.mData.JobTaskOplCR[a].OPLTasks[i].Discount ? $scope.mData.JobTaskOplCR[a].OPLTasks[i].Discount : 0;
                            //   $scope.mData.JobTaskOplCR[a].OPLTasks[i].DiscountedPriceOPL = parseFloat($scope.mData.JobTaskOplCR[a].OPLTasks[i].Price) - (parseFloat($scope.mData.JobTaskOplCR[a].OPLTasks[i].Price) * $scope.mData.JobTaskOplCR[a].OPLTasks[i].Discount / 100);
                            //   if ($scope.mData.JobTaskOplCR[a].OPLTasks[i].DiscountedPriceOPL !== undefined) {
                            //     if($scope.mData.JobTaskOplCR[a].OPLTasks[i].PaymentId == 2277 || $scope.mData.JobTaskOplCR[a].OPLTasks[i].PaymentId  == 30 || $scope.mData.JobTaskOplCR[a].OPLTasks[i].PaymentId == 32){
                            //       totalDiscountedPrice += 0;
                            //     }else{
                            //       totalDiscountedPrice += $scope.mData.JobTaskOplCR[a].OPLTasks[i].DiscountedPriceOPL;
                            //       console.log("ga masuk else opl");
                            //     }
                            //   } else {
                            //       totalDiscountedPrice = 0;
                            //       console.log("masuk else opl");
                            //   }
                            //   console.log("$scope.mData.JobTaskOplCR[a].OPLTasks[i].DiscountedPriceOPL", $scope.mData.JobTaskOplCR[a].OPLTasks[i].DiscountedPriceOPL);
                          };
                          console.log("totalDiscountedPrice", totalDiscountedPrice);
                          // $scope.sumWorkOplDiscounted = totalDiscountedPrice;
                          // Math.round(
                          $scope.sumWorkOplDiscounted = totalDiscountedPrice
                          console.log('$scope.sumWorkOpl', $scope.sumWorkOpl);
                          console.log("$scope.sumWorkOplDiscounted",$scope.sumWorkOplDiscounted);
                          // console.log('$scope.mData.JobTaskOplCR[a].OPLTasks[i] >>>', $scope.mData.JobTaskOplCR[a].OPLTasks[i]);
    
                           
                           console.log("flagStats",flagStats);
                           console.log("flagStatsOpen",flagStatsOpen);
                           if(flagStatsOpen > 0){

                             $scope.mData.JobTaskOplCR[a].Status = "Open";
                             $scope.mData.JobTaskOplCR[a].xStatus = 0;
                           }else{

                             if(flagStats > 0){
                               //kalo status jobtaskoplnya ada yg 1 maka status parentoplnya jadi on progress
                               $scope.mData.JobTaskOplCR[a].Status = "On Progress";
                               $scope.mData.JobTaskOplCR[a].xStatus = 1;
                             }  
                           } 

                           // if($scope.mData.JobTaskOplCR[a].Status = "Open"){
                           //   $scope.mData.JobTaskOplCR[a].xStatus = 0;
                           // }else if($scope.mData.JobTaskOplCR[a].Status = "On Progress"){
                           //   $scope.mData.JobTaskOplCR[a].xStatus = 1;
                           // }else if($scope.mData.JobTaskOplCR[a].Status = "Completed"){
                           //   $scope.mData.JobTaskOplCR[a].xStatus = 2;
                           // }else if($scope.mData.JobTaskOplCR[a].Status = "Closed"){
                           //   $scope.mData.JobTaskOplCR[a].xStatus = 3;
                           // }else if($scope.mData.JobTaskOplCR[a].Status = "Cancel"){
                           //   $scope.mData.JobTaskOplCR[a].xStatus = 4;
                           // }
                          
                       }
                       $scope.finalDataOPL = $scope.mData.JobTaskOplCR;
                       console.log("$scope.finalDataOPL nih",$scope.finalDataOPL);
                       $scope.setDataMaster(0,true)
                   }else {
                    console.log('masuk else 820');
                    $scope.finalDataOPL = []
                    console.log('finalDataOPL 821 ->', $scope.finalDataOPL);
                    $scope.agGridWorkViewApi.setRowData($scope.finalDataOPL);
                   }
               }

               WoHistoryGR.getDataVIN($scope.mDataCrm.Vehicle.VIN).then(function(restu){
                console.log('cek input rangka opl', $scope.mDataCrm.Vehicle.VIN)
                $scope.vinOPL = restu.data.Result;
                console.log('cek res input opl', $scope.vinOPL)
               })

           }

            
            $scope.plushOPL = function(){
                console.log("plushOPL | dataTerpisahkan ==>",dataTerpisahkan);     
                
                var Id =  Math.floor(Math.random() * 10000) + 1;
                var newData = {
                    "flagNew" :true,
                    "Id":Id,
                    "No":$scope.masterOPL.length + 1,
                    "TaskNameOpl": "",
                    "Payment": "",
                    "EstimationDate": "",
                    "Qty":"",
                    "HargaJual":"",
                    "HargaBeli":"",
                    "Discount":"",
                    "Notes":""
                };
        
                if($scope.masterOPL.length > 0){
                    for(var i in $scope.masterOPL){
                        if(i == $scope.masterOPL.length -1){
                            newData.Discount       = $scope.masterOPL[i].Discount;
                            newData.EstimationDate = $scope.masterOPL[i].EstimationDate;
                            newData.PaymentId      = $scope.masterOPL[i].PaymentId;
                            newData.Payment        = $scope.masterOPL[i].Payment;
                            newData.Qty            = $scope.masterOPL[i].Qty;
                        }
                    }
                }
                
                console.log("newData",newData);
                $scope.masterOPL.push(newData);
                console.log("$scope.masterOPL",$scope.masterOPL);
                // $scope.GridParts.api.setRowData($scope.GridParts.rowData);
                var res = $scope.gridDetailOPLWoListApi.updateRowData({add: [newData]});
                console.log("res",res);
                res.add.forEach(function(rowNode) {
                    console.log("Added Row Node", rowNode);
                    $scope.gridDetailOPLWoListApi.setFocusedCell(rowNode.rowIndex,"TaskNameOpl");
                });
                $scope.disableSimpan = true;
                $scope.dateOptions.minDate = new Date();

                // 					2675 -> FPC - Free Parts Claim
// 					2677 -> FLC - Free Labor Claim
// 					2678 -> PLC - Parts Labor Claim
					//----------------------------------------CEK LIST ADA FPC FLC PLC START------------------------------//
					var x_adaFPC = 0
					var x_adaFLC = 0
					var x_adaPLC = 0
					for(var i=0; i<$scope.gridWork.length; i++){
						if ($scope.gridWork[i].JobTypeId == 2675) {
							x_adaFPC++;
						}
						if ($scope.gridWork[i].JobTypeId == 2677) {
							x_adaFLC++;
						}
						if ($scope.gridWork[i].JobTypeId == 2678) {
							x_adaPLC++;
						}
					}
					//----------------------------------------CEK LIST ADA FPC FLC PLC END------------------------------//
					// MHKE8FB3JKK037194 - TCARE
									// JTNGF3DH3N8035733 - GGBSB
					if (x_adaFLC == 0 && x_adaFPC == 0 && x_adaPLC == 0) {
						OPLPaymentData.splice(0,6)
						for(var a in $scope.OPLPaymentData){
							OPLPaymentData.push($scope.OPLPaymentData[a].Name)
						}
					}else{
						if(OPLPaymentData.length == 0){
							for(var a in $scope.OPLPaymentData){
								//Ngecek programName GBSB pembayraan muncul semua, kalau bukan warranty tidak ada
								if ($scope.vinOPL.length > 0) {
									if ($scope.vinOPL[0].ProgramName == "GBSB") {
										console.log('$scope.vinOPL 1', $scope.vinOPL)
										console.log('sini == 0 ===> 2')
										OPLPaymentData.push($scope.OPLPaymentData[a].Name)
										//Jiga data norangkanya tidak ada munculin semua		
									}else if($scope.vinOPL[0].ProgramName != "" && $scope.vinOPL[0].ProgramName != undefined && $scope.vinOPL[0].ProgramName != null && $scope.vinOPL[0].ProgramName != 'GBSB'){
										console.log('$scope.vinOPL', $scope.vinOPL)
										console.log('typeof $scope.vinOPL', typeof $scope.vinOPL)
										console.log('$scope.vinOPL.ProgramName', $scope.vinOPL.ProgramName)
										console.log('sini == 0 ===> 1')
										console.log('vinOPL 123', $scope.vinOPL)
										if ($scope.OPLPaymentData[a].Name !== 'Warranty') {
											OPLPaymentData.push($scope.OPLPaymentData[a].Name)
										}
									}else{
										//kalau tidak ada programName pembyaran muncul semua
										OPLPaymentData.push($scope.OPLPaymentData[a].Name)
									}
								}else{
									OPLPaymentData.push($scope.OPLPaymentData[a].Name)
								}
								
							}
									console.log("STATUS MASUK");
						}else{
							OPLPaymentData.splice(0,6)
							for(var a in $scope.OPLPaymentData){
								//Ngecek programName GBSB pembayraan muncul semua, kalau bukan warranty tidak ada
								if ($scope.vinOPL.length > 0) {
									if ($scope.vinOPL[0].ProgramName == "GBSB") {
										console.log('$scope.vinOPL 1', $scope.vinOPL)
										console.log('sini == 0 ===> 2')
										OPLPaymentData.push($scope.OPLPaymentData[a].Name)
										//Jiga data norangkanya tidak ada munculin semua		
									}else if($scope.vinOPL[0].ProgramName != "" && $scope.vinOPL[0].ProgramName != undefined && $scope.vinOPL[0].ProgramName != null && $scope.vinOPL[0].ProgramName != 'GBSB'){
										console.log('$scope.vinOPL', $scope.vinOPL)
										console.log('typeof $scope.vinOPL', typeof $scope.vinOPL)
										console.log('$scope.vinOPL.ProgramName', $scope.vinOPL.ProgramName)
										console.log('sini == 0 ===> 1')
										console.log('vinOPL 123', $scope.vinOPL)
										if ($scope.OPLPaymentData[a].Name !== 'Warranty') {
											OPLPaymentData.push($scope.OPLPaymentData[a].Name)
										}
									}else{
										//kalau tidak ada programName pembyaran muncul semua
										OPLPaymentData.push($scope.OPLPaymentData[a].Name)
									}
								}else{
									OPLPaymentData.push($scope.OPLPaymentData[a].Name)
								}
							}
	
						}
					}

            }
            var dataTerpisahkan = [];
            $scope.deleteDataOplDetail = function(data){
                console.log("deleteDataOplDetail | data delete =======>",data);
                console.log("deleteDataOplDetail |$scope.masterOPL ===>",$scope.masterOPL);
                var arrOPLNo = []
                if (data.OplNo != undefined && data.OplNo != null){
                    var obj = {}
                    obj.OPLNo = ''
                    obj.OPLNo = data.OplNo
                    arrOPLNo.push(obj)
                }
                WoHistoryGR.CheckIsOPLGateOut(arrOPLNo, $scope.mData.JobId).then(function(res) {
                    if (res.data == 666) {
                        bsAlert.warning("Status kendaraan sedang gate out OPL, silahkan gate in  OPL dahulu di Security Gate", '');
                    } else {
                        if (data.Status == 1 || data.Status == 2 || data.Status == 3){
                            bsAlert.warning("Job sudah on progress / completed", '');
                            return;
                        }
                        bsAlert.alert({
                                    title: "Apakah anda yakin untuk menghapus Job OPL?",
                                    text: "",
                                    type: "question",
                                    showCancelButton: true
                                },
                                function() {
                                  for(var x in $scope.masterOPL){
                                          if($scope.masterOPL[x].OplNo == undefined){
                                  
                                              if($scope.masterOPL[x].Id == data.Id){
                                                  $scope.masterOPL.splice(x,1);
                                                  console.log("deleteDataOplDetail job baru di opl baru | langsung delete");
                                              } 
                                          }else{
                                              if($scope.masterOPL[x].Id == data.Id){
                                                  if( $scope.masterOPL[x].flagNew == true){
                                                      $scope.masterOPL.splice(x,1);
                                                      console.log("deleteDataOplDetail job baru di opl lama | langsung delete");
                                                  }else{
                                                      $scope.masterOPL[x].isDeleted = 1;
                                                      dataTerpisahkan.push($scope.masterOPL[x]);
                                                      $scope.masterOPL.splice(x,1);
                                                  }
                                              }
                                              console.log("deleteDataOplDetail | dataTerpisahkan ==>",dataTerpisahkan);     
                                          }
                                  }
                                  getNoDetail(1);
        
                                  console.log('deleteDataOplDetail | masterOPL ===>',$scope.masterOPL);
                                  console.log('deleteDataOplDetail | lengthOPL ===>',$scope.masterOPL.length);
        
                                  if($scope.masterOPL.length == 0){
                                      console.log('deleteDataOplDetail | hapus vendor ====>',$scope.mOpl)
                                      $scope.mOpl.vendor = null;
                                      $scope.mOpl.VendorCode = null;                
                                  }else{
                                      console.log('deleteDataOplDetail | ga hapus vendor ====>',$scope.mOpl)
                                  }
                                },
                               function() {
        
                               }
                        )
                    }
                    

                })
                
               
        
            }
        
    
           function getNoDetail(data){
               console.log("data flag",data);
               var DataFinal = [];
               for(var a in $scope.masterOPL){
                   if($scope.masterOPL[a].isDeleted != 1){
                       DataFinal.push($scope.masterOPL[a]);
                   }
               }
               for(var a in DataFinal){
                   DataFinal[a].No = parseInt(a) + 1;
               }
    
               console.log("$scope.masterOPL.length",$scope.masterOPL.length);
               if(DataFinal.length == 0){
                   $scope.disableSimpan = true;
               }else{
                   $scope.disableSimpan = false;
               }
    
               if(data == 1){
                   console.log("$scope.masterOPL",$scope.masterOPL);
                   console.log("DataFinal after delete",DataFinal);
                   // data == 1 that from delete
                   $scope.gridDetailOPLWoListApi.setRowData(DataFinal);
               }
               console.log("get no for detail");
               $scope.disableSimpan = validateSimpan();
            }
    
            function validateSimpan(){
                var invalidInput = 0;
                if($scope.masterOPL.length > 0){
                    for(var x in $scope.masterOPL){
                        if($scope.masterOPL[x].TaskNameOpl == "" || $scope.masterOPL[x].Payment == ""|| $scope.masterOPL[x].EstimationDate == ""|| $scope.masterOPL[x].EstimationDate == null || $scope.masterOPL[x].EstimationDate == undefined || $scope.masterOPL[x].Qty == "" || $scope.masterOPL[x].HargaBeli == "" || $scope.masterOPL[x].HargaJual == ""){
                            invalidInput++;
                            console.log('JALAN =>',x, $scope.masterOPL[x])
                        }
                    }
                    if(invalidInput >0){
                        return true
                    }else{
                        return false
                    }
                }else{
                    return true
                }
            }


           function customDate(param){
               console.log('param customDate ===>',param.value);
               console.log('param customDate ===>',typeof param.value);
    
    
               if(param.value == null){
                   return '-';
               }else{
                   if(param.value !== ""){
                       if(typeof param.value == 'object'){
                           var date = new Date(param.value);
                           console.log('date parsin ===>',date);
                           var thn = date.getFullYear();
                           var bln = ('0' + (date.getMonth()+1)).slice(-2);
                           var tgl = ('0' + date.getDate()).slice(-2);
                           var result = '' + tgl +'-'+  bln +'-'+ thn +'';
                           console.log('thn --->',thn);
                           console.log('bln --->',bln);
                           console.log('tgl --->',tgl);
                           console.log('result parsing --->',result)
                           return result
                       }else if (typeof param.value == 'string'){
                           console.log('date normal ===>',date);
                           var date = param.value.split('-');
                           var thn = date[0];
                           var bln = date[1];
                           var tgl = date[2];
                           var result = '' + tgl +'-'+  bln +'-'+ thn +'';
                           console.log('thn --->',thn);
                           console.log('bln --->',bln);
                           console.log('tgl --->',tgl);
                           console.log('result normal--->',result)
                           return result
                       }else{
                           return  '-';
                       }
                   }else{
                       return '-';
                   }
               }
    
               
           }

            $scope.alertOPL = function(text, mode) {
                bsAlert.alert({
                        title: text,
                        text: "",
                        type: mode,
                        showCancelButton: false
                    },
                    function() {
                    
                    },
                    function() {
        
                    }
                )
            };

    
           var dataforGrid = {};
           $scope.onAfterCellEditAg = function(idx,field,val,node) {
               console.log("idx =>",idx, "field =>",field, "val =>",val, "node =>",node);
               console.log("node.data",node.data);
               console.log("$scope.masterOPL",$scope.masterOPL);
    
                if(field == 'TaskNameOpl'){
                    console.log("onAfterCellEditAg | oplDataChoosed ===>",oplDataChoosed);
                    console.log("onAfterCellEditAg | $scope.mOpl ======>",$scope.mOpl);
                    // $scope.mOpl.vendor = oplDataChoosed.VendorId;
                    // $scope.chooseVendor();
                    var flagCount = 0;

                    $scope.gridDetailOPLWoListApi.forEachNode( function(node,index){
                      console.log("node to Update oi oi=>",node,node.data,index);
                      console.log("oplDataChoosed",typeof oplDataChoosed);
                      if(oplDataChoosed != undefined){
                        if(oplDataChoosed.OPLWorkName != undefined){
                          
                        //   if(node.data.TaskNameOpl == oplDataChoosed.OPLWorkName){
                            if(node.data.oplId == oplDataChoosed.Id){
                            flagCount++;
                          }
                        }
                      }else{
                        flagCount = 0;
                      }
                    });
                    console.log("flagCount",flagCount);
                    console.log("oplDataChoosed new",oplDataChoosed);
                    console.log("dataforGrid",dataforGrid);
                    var datakosong = _.isEmpty(oplDataChoosed);
                    console.log("datakosong",datakosong);
                    if(datakosong == true){
                       $scope.alertOPL('Pekerjaan OPL tidak ditemukan','warning');
                       oplDataChoosed = {};
                    }else{
                      dataforGrid = node.data;
                      dataforGrid.oplId = oplDataChoosed.Id;
                      dataforGrid.TaskNameOpl = oplDataChoosed.OPLWorkName +" "+oplDataChoosed.isExternalDesc;
                      dataforGrid.HargaJual = oplDataChoosed.Price;
                      dataforGrid.HargaBeli = oplDataChoosed.OPLPurchasePrice;
                      if(dataforGrid.TaskNameOpl == undefined){
                              dataforGrid.Qty = "";
                              console.log('MASUKKKKK')
                          }else{
                              dataforGrid.Qty = 1;
                              if($scope.mOpl.vendor == null){
                                  $scope.mOpl.vendor = oplDataChoosed.VendorId;
                                  $scope.chooseVendor();
                              }
                          }

                      if((oplDataChoosed.VendorId != $scope.mOpl.vendor) && $scope.mOpl.vendor != null){
                        if(oplDataChoosed.OPLWorkName != undefined){
                          $scope.alertOPL('Pekerjaan OPL  tidak sesuai dengan vendor yang dipilih','warning');
                          dataforGrid.Qty         = "";
                          dataforGrid.TaskNameOpl = "";
                          dataforGrid.HargaJual   = "";
                          dataforGrid.HargaBeli   = "";
                          node.setData(dataforGrid);
                        }
                      }else{
                          // if(flagCount > 0){

                          //   $scope.alertOPL('Pekerjaan OPL tidak boleh sama silakan ubah qty','warning');
                          //   dataforGrid.Qty         = "";
                          //   dataforGrid.TaskNameOpl = "";
                          //   dataforGrid.HargaJual   = "";
                          //   dataforGrid.HargaBeli   = "";
                          // }else{
                            node.setData(dataforGrid);
                          // }
                      }
                      oplDataChoosed = {};
                    }
                    
                }
    
               if(field == 'EstimationDate'){
                   console.log("$scope.mOpl.DateStart",$scope.mOpl.DateStart);

                   dataforGrid = node.data;
                   if($scope.mOpl.DateStart == undefined){

                   }else{

                     dataforGrid.EstimationDate = $scope.mOpl.DateStart;
                   }
                   console.log("dataforGrid",dataforGrid);
                   node.setData(dataforGrid);
               }
    
               if(field == 'Notes'){
                   dataforGrid = node.data;
                   console.log("dataforGrid",dataforGrid);
                   node.setData(dataforGrid);
               }
    
               if(field == 'Discount'){
    
                   if(node.data.Discount > 100){
                       bsNotify.show({
                           size: 'big',
                           type: 'danger',
                           title: "Diskon Tidak Boleh Melebihi 100",
                       }); 
                       node.data.Discount = 0;
                       dataforGrid = node.data;
                   }else{
    
                       dataforGrid = node.data;
                       console.log("dataforGrid",dataforGrid);
                       node.setData(dataforGrid);    
                   }
               }
    
               if(field == 'Payment'){
                   for(var a in $scope.OPLPaymentData){
                       if($scope.OPLPaymentData[a].Name == node.data.Payment){
                           node.data.PaymentId = $scope.OPLPaymentData[a].MasterId;
                       }
                   }
                    if (node.data.PaymentId == 30) {
                        node.data.Discount = '0'
                    }
                   dataforGrid = node.data;
                   console.log("dataforGrid",dataforGrid);
               }
    
               if(field == 'HargaBeli'){
                   // if(node.data.HargaBeli == 0){
    
                   //     bsNotify.show({
                   //         size: 'big',
                   //         type: 'danger',
                   //         title: "Harga Beli Tidak Boleh 0",
                   //     }); 
                   // }else{
    
                       dataforGrid = node.data;
                       console.log("dataforGrid",dataforGrid);
                       node.setData(dataforGrid);
                   // }
               }
    
    
    
               if(field == 'HargaJual'){
                   // if(node.data.HargaJual == 0){
    
                   //     bsNotify.show({
                   //         size: 'big',
                   //         type: 'danger',
                   //         title: "Harga Jual Tidak Boleh 0",
                   //     }); 
                   // }else{
    
                       dataforGrid = node.data;
                       console.log("dataforGrid",dataforGrid);
                       node.setData(dataforGrid);
                   // }
               }
    
    
               $scope.disableSimpan = validateSimpan();

           }




    
           $scope.getOplSrc = function (key) {
                console.log('data CRM', $scope.mDataCrm);
                var modelname = '';
                if ($scope.mDataCrm.Vehicle.isNonTAM == 0){
                    modelname = $scope.mDataCrm.Vehicle.Model.VehicleModelName
                } else {
                    modelname = $scope.mDataCrm.Vehicle.ModelName
                }
                var vendorid = null;
                console.log(" $scope.mOpl.vendor", $scope.mOpl.vendor);
                if($scope.mOpl.vendor == null || $scope.mOpl.vendor == undefined || $scope.mOpl.vendor == ''){
                   vendorid = 0;
                   console.log('getOplSrc | ini sebenernya belom ada vendor Id nya | $scope.mOpl.vendor ===>',$scope.mOpl.vendor)
                }else{
                    vendorid = 0;
                    // vendorid = $scope.mOpl.vendor;
                    console.log('getOplSrc | ini sebenernya udah ada vendor Id nya | $scope.mOpl.vendor ===>',$scope.mOpl.vendor)
                }
               console.log("vendorid",vendorid);
               var ress = WoHistoryGR.getNewOplList(key, modelname, vendorid).then(function (resTask) {
                   var data = resTask.data.Result;
                   console.log("resTask resTask", data);
                   _.map(data, function (a) {
                       // _.map(a.value, function(b) {
                       if (a.isExternal == 1) {
                           a.isExternalDesc = ' (External)';
                           // a.VendorName = a.VendorName + a.isExternalDesc;
                       } else {
                           a.isExternalDesc = ' (Internal)';
                           // a.VendorName = a.VendorName + a.isExternalDesc;
                       };
                       // });
                   });
                   console.log("data data", data);
                   return data;
               });
               console.log("ress", ress);
    
               return ress;
           };
    
           $scope.onGotResult = function (data) {
               console.log("onGotResult=>", data);
           }
    
           $scope.onNoResultOplNew = function () {
               var row = {};
               // $scope.listApiOpl.addDetail(false, row);
               // $scope.listApiOpl.clearDetail();
           };
           var oplDataChoosed
           $scope.selectOPl = function($item, $model, $label) {
               console.log("onSelectPartCode item=>", $item);
               console.log("onSelectPartCode model=>", $model);
               console.log("onSelectPartCode label=>", $label);
               oplDataChoosed = $item;
               console.log("oplDataChoosed",oplDataChoosed);
           };
    
           $scope.allowPatternFilter = function(event) {
               console.log("event allowPatternFilter", event);
               if (event.key == "Escape") {
                   $scope.closeModalOpl();
               }
           };
           
           $scope.emptyDeleteVarOPL = function(){
             DataChoosedDelete = [];
           };


           // var PPNPerc = 0;
           $scope.dataForOPL = function(data,flag) {
            // $scope.getPPN()
            console.log("PPNPerc",$scope.PPNPerc);
               $scope.tmpDataOPL = [];
               $scope.tmpDataOPLFinal = [];
               $scope.oplData = [];
               $scope.tmpDataOPLDeleted = [];
               _.map(data.JobTaskOpl, function(val, idx) {
                   console.log('data.JobTaskOpl vall >>', val, idx);
                   // val.RequiredDate = val.TargetFinishDate;
                   // val.OplName = val.OPL;
                   switch (val.Status) {
                       case 0:
                           val.xStatus = "Open";
                           break;
                       case 1:
                           val.xStatus = "On Progress";
                           break;
                       case 2:
                           val.xStatus = "Completed";
                           break;
                       case 3:
                           // val.xStatus = "Billing";
                           val.xStatus = "Billing";
                           break;
                       case 4:
                           val.xStatus = "Cancel";
                           break;
                       default:
                           break;
                   }
                   val.index = "$$" + idx;
                   if (val.OPL != null) {
                       val.OPLName = val.OPL.OPLWorkName;
                       val.VendorName = val.OPL.Vendor.Name;
                   }
                   // val.
               });
               for (var i in data.JobTaskOpl) {
                   if (data.JobTaskOpl[i].Status !== 4 && data.JobTaskOpl[i].isDeleted != 1) {
                       if (data.JobTaskOpl[i].TargetFinishDate !== null && data.JobTaskOpl[i].TargetFinishDate !== undefined) {
                           data.JobTaskOpl[i].TargetFinishDate = $scope.changeFormatDate(data.JobTaskOpl[i].TargetFinishDate);
                       }
                       if (data.JobTaskOpl[i].CreateOplDate !== null && data.JobTaskOpl[i].CreateOplDate !== undefined) {
                           data.JobTaskOpl[i].CreateOplDate = $scope.changeFormatDate(data.JobTaskOpl[i].CreateOplDate);
                       }
                       $scope.oplData.push(data.JobTaskOpl[i]);
                   }
               }
               // $scope.oplData = data.JobTaskOpl;
               // $scope.tmpDataOPL = angular.copy(data.JobTaskOpl);
               $scope.tmpDataOPL = angular.copy($scope.oplData); // ambil yg di scope.opldata aja soal na uda di filter by status dan isdeleted
    
               $scope.tmpDataOPLFinal = [];
               if ($scope.oplData.length > 0) {
                  // Added new Code =================
                  console.log("anita opl");
                  $scope.showOPLlabel = 1;
                  var cStat = 0;
                  if ($scope.oplData.length == 1) {
                      switch ($scope.oplData[0].Status) {
                          case 0:
                              $scope.statusOPL = "Open";
                              break;
                          case 1:
                              $scope.statusOPL = "On Progress";
                              break;
                          case 2:
                              $scope.statusOPL = "Completed";
                              break;
                          case 3:
                              // $scope.statusOPL = "Billing";
                              $scope.statusOPL = "Billing";
                              break;
                          case 4:
                              $scope.statusOPL = "Cancel";
                              break;
                      }
                  } else {
                      for (var i = 0; i < $scope.oplData.length; i++) {
                          if ($scope.oplData[i].Status == 2) {
                              cStat++;
                          }
                      }

                      if (cStat == $scope.oplData.length) {
                          $scope.statusOPL = "Completed"
                      } else {
                          $scope.statusOPL = "On Progress"
                      }

                  }
                  console.log("$scope.statusOPL =>", $scope.statusOPL);
                  console.log("$scope.oplData =>", $scope.oplData);

                //   ==================================================

                var totalWOPL = 0;
                for (var i = 0; i < $scope.oplData.length; i++) {
                    if ($scope.oplData[i].Price !== undefined) {
                        // if ($scope.oplData[i].PaidById == 2277 || $scope.oplData[i].PaidById == 30|| $scope.oplData[i].PaidById == 32){
                        if ($scope.oplData[i].PaidById != 28 && $scope.oplData[i].PaidById != 2458 && $scope.oplData[i].PaidById != 32){
                            totalWOPL += 0
                        } else {
                            // totalWOPL += $scope.oplData[i].Price * parseFloat($scope.oplData[i].QtyPekerjaan);
                            totalWOPL += ASPricingEngine.calculate({
                                            InputPrice: $scope.oplData[i].Price,
                                            // Discount: 5,
                                            Qty: parseFloat($scope.oplData[i].QtyPekerjaan),
                                            Tipe: 102, 
                                            PPNPercentage: $scope.PPNPerc,
                                        });
                        }
                    }
                };
                // $scope.sumWorkOpl = totalWOPL / (1.1); //request pak dodi 19/3/19
                // $scope.sumWorkOpl = Math.round(totalWOPL / (1.1)); // akhirnya di round lagi dong
                // $scope.sumWorkOpl =  Math.round(totalWOPL / (1.1));
                $scope.sumWorkOpl = totalWOPL;


                var totalDiscountedPrice = 0;
                for (var i = 0; i < $scope.oplData.length; i++) {
                    $scope.oplData[i].Discount = $scope.oplData[i].Discount ? $scope.oplData[i].Discount : 0;
                    // $scope.oplData[i].DiscountedPriceOPL = $scope.oplData[i].Price - ($scope.oplData[i].Price * $scope.oplData[i].Discount / 100);
                    // $scope.oplData[i].DiscountedPriceOPL = $scope.oplData[i].Price * $scope.oplData[i].QtyPekerjaan - ($scope.oplData[i].Price * $scope.oplData[i].QtyPekerjaan * $scope.oplData[i].Discount / 100);
                    // if ($scope.oplData[i].PaidById == 2277 || $scope.oplData[i].PaidById == 30|| $scope.oplData[i].PaidById == 32){
                    if ($scope.oplData[i].PaidById != 28 && $scope.oplData[i].PaidById != 2458 && $scope.oplData[i].PaidById != 32){
                        $scope.oplData[i].DiscountedPriceOPL = 0
                    } else {
                        $scope.oplData[i].DiscountedPriceOPL = ASPricingEngine.calculate({
                                                                InputPrice: $scope.oplData[i].Price,
                                                                Discount: $scope.oplData[i].Discount,
                                                                Qty: $scope.oplData[i].QtyPekerjaan,
                                                                Tipe: 104, 
                                                                PPNPercentage: $scope.PPNPerc,
                                                            }); 
                    }
                    
                    if ($scope.oplData[i].DiscountedPriceOPL !== undefined) {
                        totalDiscountedPrice += $scope.oplData[i].DiscountedPriceOPL;
                        console.log("ga masuk else opl");
                    } else {
                        totalDiscountedPrice = 0;
                        console.log("masuk else opl");
                    }
                    console.log("$scope.oplData[i].DiscountedPriceOPL", $scope.oplData[i].DiscountedPriceOPL);
                };
                console.log("totalDiscountedPrice", totalDiscountedPrice);
                // $scope.sumWorkOplDiscounted = totalDiscountedPrice / (1.1);
                // $scope.sumWorkOplDiscounted = Math.round(totalDiscountedPrice / (1.1)); // akhirnya di round lagi dong
                // $scope.sumWorkOplDiscounted = Math.round($scope.sumWorkOplDiscounted) // katanya biar sama ky di billing
                $scope.sumWorkOplDiscounted = totalDiscountedPrice; 


                // di komen pake yang di atas ambil dari wo list gr ============= start
                // var totalDiscountedPrice = 0;
                // for (var i = 0; i < $scope.oplData.length; i++) {
                //     if ($scope.oplData[i].DiscountedPriceOPL !== undefined) {
                //         totalDiscountedPrice += $scope.oplData[i].DiscountedPriceOPL;
                //         console.log("ga masuk else opl");
                //     } else {
                //         totalDiscountedPrice = 0;
                //         console.log("masuk else opl");
                //     }
                //     console.log("$scope.oplData[i].DiscountedPriceOPL", $scope.oplData[i].DiscountedPriceOPL);
                // };
                // console.log("totalDiscountedPrice", totalDiscountedPrice);
                // $scope.sumWorkOplDiscounted = totalDiscountedPrice;
                // di komen pake yang di atas ambil dari wo list gr ============= end

                console.log('$scope.sumWorkOpl', $scope.sumWorkOpl);
                console.log('$scope.sumWorkOplDiscounted', $scope.sumWorkOpl);
                console.log('$scope.oplData >>>', $scope.oplData);

               }
               console.log("flag",flag);
               if(flag == 1){
                   $scope.finalDataOPL = data.JobTaskOplCR;
                   console.log("count summary opl",$scope.finalDataOPL);
                   
                   var totalWOPL = 0;
                   var totalDiscountedPrice = 0;
                   if($scope.finalDataOPL.length == 0){
                     $scope.sumWorkOpl = totalWOPL;
                     $scope.sumWorkOplDiscounted = totalDiscountedPrice;
                   }else{
                      for(var a in $scope.finalDataOPL){
                        if($scope.finalDataOPL[a].OPLTasks == undefined){
                          $scope.finalDataOPL[a].OPLTasks = $scope.finalDataOPL[a].detail;
                        }
                        // for (var i = 0; i < $scope.finalDataOPL[a].OPLTasks.length; i++) {
                        //     if($scope.finalDataOPL[a].isDeleted != 1){
                        //         if($scope.finalDataOPL[a].OPLTasks[i].isDeleted != 1){
                        //             if ($scope.finalDataOPL[a].OPLTasks[i].Price != undefined) {
                        //               if($scope.finalDataOPL[a].OPLTasks[i].PaymentId == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId  == 30 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId == 32){
                        //                 totalWOPL += 0;
                        //               }else{
                        //                 totalWOPL += parseFloat($scope.finalDataOPL[a].OPLTasks[i].Price) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty);
                        //               }
                        //             }else if($scope.finalDataOPL[a].OPLTasks[i].HargaJual != undefined){
                        //                  if($scope.finalDataOPL[a].OPLTasks[i].PaymentId == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId  == 30 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId == 32){
                        //                    totalWOPL += 0;
                        //                  }else{
                        //                   totalWOPL += parseFloat($scope.finalDataOPL[a].OPLTasks[i].HargaJual) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty);
                        //                  }
                        //             }
                        //         }
                        //     }
                        // };
                        for (var i = 0; i < $scope.finalDataOPL[a].OPLTasks.length; i++) {
                          if($scope.finalDataOPL[a].isDeleted != 1){
                            if($scope.finalDataOPL[a].OPLTasks[i].isDeleted != 1){
                              if ($scope.finalDataOPL[a].OPLTasks[i].Price !== undefined) {
                                // if ($scope.finalDataOPL[a].OPLTasks[i].PaidById == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaidById == 30|| $scope.finalDataOPL[a].OPLTasks[i].PaidById == 32){
                                if ($scope.finalDataOPL[a].OPLTasks[i].PaidById != 28 && $scope.finalDataOPL[a].OPLTasks[i].PaidById != 2458 && $scope.finalDataOPL[a].OPLTasks[i].PaidById != 32){
                                    totalWOPL += 0
                                } else {
                                    // totalWOPL += $scope.finalDataOPL[a].OPLTasks[i].Price * parseFloat($scope.finalDataOPL[a].OPLTasks[i].QtyPekerjaan);
                                    totalWOPL += ASPricingEngine.calculate({
                                                    InputPrice: $scope.finalDataOPL[a].OPLTasks[i].Price,
                                                    // Discount: 5,
                                                    Qty: parseFloat($scope.finalDataOPL[a].OPLTasks[i].Qty),
                                                    Tipe: 102, 
                                                    PPNPercentage: $scope.PPNPerc,
                                                });
                                }
                              }else if($scope.finalDataOPL[a].OPLTasks[i].HargaJual != undefined){
                                // if ($scope.finalDataOPL[a].OPLTasks[i].PaidById == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaidById == 30|| $scope.finalDataOPL[a].OPLTasks[i].PaidById == 32){
                                if ($scope.finalDataOPL[a].OPLTasks[i].PaidById != 28 && $scope.finalDataOPL[a].OPLTasks[i].PaidById != 2458 && $scope.finalDataOPL[a].OPLTasks[i].PaidById != 32){
                                    totalWOPL += 0
                                } else {
                                    // totalWOPL += $scope.finalDataOPL[a].OPLTasks[i].Price * parseFloat($scope.finalDataOPL[a].OPLTasks[i].QtyPekerjaan);
                                    totalWOPL += ASPricingEngine.calculate({
                                                    InputPrice: $scope.finalDataOPL[a].OPLTasks[i].HargaJual,
                                                    // Discount: 5,
                                                    Qty: parseFloat($scope.finalDataOPL[a].OPLTasks[i].Qty),
                                                    Tipe: 102, 
                                                    PPNPercentage: $scope.PPNPerc,
                                                });
                                }
                              }
                            }
                          }
                            
                        };
                        $scope.sumWorkOpl = totalWOPL;
                        // $scope.sumWorkOpl = Math.round(totalWOPL / (1.1));


                        for (var i = 0; i < $scope.finalDataOPL[a].OPLTasks.length; i++) {

                            if($scope.finalDataOPL[a].isDeleted != 1){
                                if($scope.finalDataOPL[a].OPLTasks[i].isDeleted != 1){
                                    $scope.finalDataOPL[a].OPLTasks[i].Qty = $scope.finalDataOPL[a].OPLTasks[i].QtyPekerjaan;
                                    $scope.finalDataOPL[a].OPLTasks[i].Discount = $scope.finalDataOPL[a].OPLTasks[i].Discount ? $scope.finalDataOPL[a].OPLTasks[i].Discount : 0;
                                    
                                    if ($scope.finalDataOPL[a].OPLTasks[i].Price != undefined) {
                                        // if ($scope.finalDataOPL[a].OPLTasks[i].PaidById == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaidById == 30|| $scope.finalDataOPL[a].OPLTasks[i].PaidById == 32){
                                        //     $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = 0
                                        // } else {
                                            $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = ASPricingEngine.calculate({
                                                                                    InputPrice: $scope.finalDataOPL[a].OPLTasks[i].Price,
                                                                                    Discount: $scope.finalDataOPL[a].OPLTasks[i].Discount,
                                                                                    Qty: $scope.finalDataOPL[a].OPLTasks[i].Qty,
                                                                                    Tipe: 104, 
                                                                                    PPNPercentage: $scope.PPNPerc,
                                                                                }); 
                                        // }
                                        // $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = (parseFloat($scope.finalDataOPL[a].OPLTasks[i].Price) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) - ((parseFloat($scope.finalDataOPL[a].OPLTasks[i].Price) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) * $scope.finalDataOPL[a].OPLTasks[i].Discount / 100);
                                      
                                    }else if($scope.finalDataOPL[a].OPLTasks[i].HargaJual != undefined){
                                        $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = ASPricingEngine.calculate({
                                                                                InputPrice: $scope.finalDataOPL[a].OPLTasks[i].HargaJual,
                                                                                Discount: $scope.finalDataOPL[a].OPLTasks[i].Discount,
                                                                                Qty: $scope.finalDataOPL[a].OPLTasks[i].Qty,
                                                                                Tipe: 104, 
                                                                                PPNPercentage: $scope.PPNPerc,
                                                                            }); 
                                        // $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = (parseFloat($scope.finalDataOPL[a].OPLTasks[i].HargaJual) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) - ((parseFloat($scope.finalDataOPL[a].OPLTasks[i].HargaJual)  * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) * $scope.finalDataOPL[a].OPLTasks[i].Discount / 100);
                                     
                                    }
                                    if ($scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL !== undefined) {
                                    //   if($scope.finalDataOPL[a].OPLTasks[i].PaymentId == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId  == 30 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId == 32){
                                      if($scope.finalDataOPL[a].OPLTasks[i].PaymentId != 28 && $scope.finalDataOPL[a].OPLTasks[i].PaymentId  != 2458 && $scope.finalDataOPL[a].OPLTasks[i].PaymentId != 32){
                                        totalDiscountedPrice += 0;
                                      }else{
                                        totalDiscountedPrice += $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL;
                                        console.log("ga masuk else opl");
                                      }
                                    } else {
                                        totalDiscountedPrice = 0;
                                        console.log("masuk else opl");
                                    }
                                    console.log("$scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL", $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL);   
                                }
                            }
                        };
                        console.log("totalDiscountedPrice", totalDiscountedPrice);
                        $scope.sumWorkOplDiscounted = totalDiscountedPrice;
                        // $scope.sumWorkOplDiscounted = Math.round(totalDiscountedPrice / (1.1));
                        console.log('$scope.sumWorkOpl', $scope.sumWorkOpl);
                        console.log("$scope.sumWorkOplDiscounted",$scope.sumWorkOplDiscounted);
                      }
                 }
               }
               // if ($scope.oplData.length > 0) {
    
               //     var totalWOPL = 0;
               //     for (var i = 0; i < $scope.oplData.length; i++) {
               //         if ($scope.oplData[i].Price !== undefined) {
               //             totalWOPL += parseFloat($scope.oplData[i].Price);
               //         }
               //     };
               //     // $scope.sumWorkOpl = totalWOPL;
               //     $scope.sumWorkOpl = totalWOPL / (1.1);
    
               //     var totalDiscountedPrice = 0;
               //     for (var i = 0; i < $scope.oplData.length; i++) {
               //         $scope.oplData[i].Discount = $scope.oplData[i].Discount ? $scope.oplData[i].Discount : 0;
               //         $scope.oplData[i].DiscountedPriceOPL = parseFloat($scope.oplData[i].Price) - (parseFloat($scope.oplData[i].Price) * $scope.oplData[i].Discount / 100);
               //         if ($scope.oplData[i].DiscountedPriceOPL !== undefined) {
               //             totalDiscountedPrice += $scope.oplData[i].DiscountedPriceOPL;
               //             console.log("ga masuk else opl");
               //         } else {
               //             totalDiscountedPrice = 0;
               //             console.log("masuk else opl");
               //         }
               //         console.log("$scope.oplData[i].DiscountedPriceOPL", $scope.oplData[i].DiscountedPriceOPL);
               //     };
               //     console.log("totalDiscountedPrice", totalDiscountedPrice);
               //     // $scope.sumWorkOplDiscounted = totalDiscountedPrice;
               //     $scope.sumWorkOplDiscounted = totalDiscountedPrice / (1.1);
               //     console.log('$scope.sumWorkOpl', $scope.sumWorkOpl);
               //     console.log('$scope.oplData >>>', $scope.oplData);
               // }
               $scope.copyDataFinalOPL = angular.copy($scope.finalDataOPL)
    
           };
            // using ag-grid change CR4 Phase2
    
        };