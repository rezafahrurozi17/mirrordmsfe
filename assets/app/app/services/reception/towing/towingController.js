angular.module('app')
    .controller('TowingController', function($scope, $http, CurrentUser, Towing, bsAlert, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            $scope.getDataPreparation();
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mData = []; //Model
        $scope.xRole = { selected: [] };

        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.ismeridian = false;
        $scope.hstep = 1;
        $scope.mstep = 1;

        $scope.DateOptions = {
            startingDay: 1,
            format: 'dd/MM/yyyy',
        }

        $scope.maxDateOption = {
            maxDate: new Date()
        };

        //------------data Model---------------------
        $scope.getDataPreparation = function() {
                Towing.getVehicleMList().then(
                    function(res) {
                        // DataModel = [];
                        //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
                        // console.log("role=>", res.data.Result);
                        //console.log("grid data=>",$scope.grid.data);
                        //$scope.roleData = res.data;
                        // $scope.loading = false;
                        $scope.VMD = res.data.Result;
                        console.log("testzzzz", res);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

                Towing.getOutletFunction().then(
                    function(res) {
                       
                        $scope.dataOutlet = res.data;
                        console.log("testzzzz dataOutlet", res);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
                // Towing.getColor().then(function(res) {
                //         $scope.CLR = res.data.Result;
                //         console.log("color", $scope.CLR);
                //     },
                //     function(err) {
                //         console.log("err=>", err);
                //     }
                // );

                $scope.dataBengkelGR = [
                    {
                        BengkelName: 'GR',
                        BengkelId: 1,
                    }
                ]

                $scope.dataBengkelALL = [
                    {
                        BengkelName: 'GR',
                        BengkelId: 1,
                    },
                    {
                        BengkelName: 'BP',
                        BengkelId: 0,
                    }
                ]
            }
            //--------------data model-------------------------

        $scope.onBeforeNewMode = function(row, mode) {
            $scope.mData.EntryTime = new Date();
        };
        //  $scope.onBeforeEdit = function(row,mode){
        //     // $scope.mData = {};
        //     // console.log("===>",$scope.mData.EntryTime);
        //     // var d = new Date("Wed Jan 18 2017 "+row.TimeFrom+" GMT+0700 (SE Asia Standard Time)");
        //     var tFrom = new Date("Wed Jan 18 2017 "+$scope.mData.EntryTime+" GMT+0700 (SE Asia Standard Time)");
        //     tFrom = tFrom.toISOString();
        //     // var ConvDate = new Dae("2017-01-18T23:24:52.966Z");
        //     $scope.mData.EntryTime = tFrom;
        // }
        $scope.onShowDetail = function(row, mode) {
            console.log("row", row);
            var LcValue = angular.copy(row);
            $scope.mData = angular.copy(row);
            // console.log("===>",$scope.mData.EntryTime);
            // var d = new Date("Wed Jan 18 2017 "+row.TimeFrom+" GMT+0700 (SE Asia Standard Time)");
            var tFrom = new Date("Wed Jan 18 2017 " + LcValue.EntryTime + " GMT+0700 (SE Asia Standard Time)");
            // var tFrom = new Date("01 18 2017 "+row.EntryTime+" GMT+0700 (SE Asia Standard Time)");
            // tFrom = tFrom.toISOString();
            // var ConvDate = new Date("2017-01-18T23:24:52.966Z");
            $scope.mData.EntryTime = tFrom;
            // $scope.dThru = true;
            // $scope.dFrom = true;
            console.log("Ini bukan");
        };

        // $scope.mData.EntryTime = new Date();

        // var Vehicle = [{VehicleModelId : 1,
        //                 ModelName : "Inova"
        //                 }];
        // $scope.VM = Vehicle;


        $scope.doCustomSave = function(mdl, mode) {
            console.log("mdl : " + JSON.stringify(mdl));
            console.log("mode : " + mode);
            console.log('mdata', $scope.mData);
            var data = $scope.mData;
            if (mode == 'create') {
                Towing.create(mdl).then(function(resu) {
                    if (resu.data == -1) {
                        bsAlert.alert({
                            title: "Kendaraan dengan No. Polisi " + mdl.PoliceNumber + " sedang dalam proses.",
                            text: "",
                            type: "error",
                            showCancelButton: false
                        });
                    } else {
                        $scope.getData();
                    };
                });
            } else {
                Towing.update(mdl).then(function(resu) {
                    $scope.getData();
                });
            }
        };


        $scope.onValidateSave = function() {
            console.log('mdata', $scope.mData);
            var date = new Date($scope.mData.EntryDate);
            date.setDate(date.getDate() + 1);
            $scope.mData.EntryDate = date;
            var dt = new Date('"' + $scope.mData.EntryTime + '"');
            dt.setSeconds(0);
            var dtFrom = dt.toTimeString();
            dtFrom = dtFrom.split(' ');
            $scope.mData.EntryTime = dtFrom[0];
            console.log('mdata v2', $scope.mData);

            return true;
        };

        // var DataModel = [];
        // $scope.getDataModel = function() {

        //   // console.log("nih", $scope.VM );
        //   VehicleModel.getData()
        //     .then(

        //       function(res) {
        //         DataModel = [];
        //         //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
        //         $scope.grid.data = res.data;
        //         // console.log("role=>", res.data.Result);
        //         //console.log("grid data=>",$scope.grid.data);
        //         //$scope.roleData = res.data;
        //         $scope.loading = false;
        //         console.log("test",res);
        //       },
        //       function(err) {
        //         console.log("err=>", err);
        //       }
        //     );
        // }



        // var xColor = [{ Color: "MERAH" }, { Color: "MARUN" }, { Color: "BIRU" }, { Color: "HIJAU" }, { Color: "EMAS" }, { Color: "SILVER" }, { Color: "ABU-ABU" }, { Color: "UNGU" }, { Color: "KUNING" }, { Color: "PUTIH" }, { Color: "COKLAT" }, { Color: "ORANGE" }];

        var gridData = [];
        $scope.getData = function() {

            // console.log("nih", $scope.VM );
            Towing.getData().then(function(res) {
                    gridData = [];
                    //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
                    $scope.grid.data = res.data;
                    // console.log("role=>", res.data.Result);
                    //console.log("grid data=>",$scope.grid.data);
                    //$scope.roleData = res.data;                   
                    console.log(res.data);
                    var temp = [];
                    _.forEach(res.data, function(e) {
                        var obj = {};
                        // if (e.Status == 0) {
                        //     e.StatusName = 'Belum Follow Up';
                        // } else if (e.Status == 1) {
                        //     e.StatusName = 'Sudah Follow Up';
                        // } else if (e.Status == 2) {
                        //     e.StatusName = 'Cancel / Batal';
                        // } else if (e.Status == 3) {
                        //     e.StatusName = 'On Progress';
                        // };
                        if (e.Status == 0) {
                            e.StatusName = 'Open';
                        } else if (e.Status == 1) {
                            e.StatusName = 'On Progress';
                        } else if (e.Status == 2) {
                            e.StatusName = 'Completed';
                        } else if (e.Status == 3) {
                            e.StatusName = 'Close';
                        } else if (e.Status == 4) {
                            e.StatusName = 'Cancel';
                        };

                        if (e.OutletId > 2000000) {
                            e.Bengkel = 'BP'
                            e.IsGR = 0
                        } else {
                            e.Bengkel = 'GR'
                            e.IsGR = 1
                        }

                        temp.push(e);
                    });
                    $scope.grid.data = temp;
                    console.log('$scope.grid.data', $scope.grid.data);
                    $scope.loading = false;
                    console.log("test", res);
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        };

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        };
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        };
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        };
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'TowingId', field: 'TowingId', width: '7%', visible: false },
                { name: 'Bengkel', field: 'Bengkel', cellFilter: 'uppercase' },
                { name: 'No Polisi', field: 'PoliceNumber', cellFilter: 'uppercase' },
                { name: 'Model', field: 'VM.VehicleModelName' },
                { name: 'Warna', field: 'Color' },
                { name: 'No. Telepone', field: 'OwnerPhone' },
                { name: 'Status', field: 'StatusName' },
                { name: 'Nama', field: 'OwnerName' },
            ]
        };
    });