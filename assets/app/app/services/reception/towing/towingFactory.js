angular.module('app')
    .factory('Towing', function($http, CurrentUser) {
        var currentUser = CurrentUser.user();
        console.log('korban lingkungan', currentUser);
        return {
            getData: function() {
                var res = $http.get('/api/as/Towings');
                //console.log('res=>',res);
                return res;
            },
            create: function(mData) {
                return $http.post('/api/as/Towings', [{
                    PoliceNumber: mData.PoliceNumber,
                    VehicleModelId: mData.VehicleModelId,
                    Color: mData.Color,
                    EntryDate: mData.EntryDate,
                    EntryTime: mData.EntryTime,
                    OwnerName: mData.OwnerName,
                    OwnerPhone: mData.OwnerPhone,
                    OwnerDetail: mData.OwnerDetail,
                    Status: mData.Status,
                    IsGR: mData.IsGR,
                }]);
            },
            update: function(mData) {
                return $http.put('/api/as/Towings', [{
                    TowingId: mData.TowingId,
                    PoliceNumber: mData.PoliceNumber,
                    VehicleModelId: mData.VehicleModelId,
                    Color: mData.Color,
                    EntryDate: mData.EntryDate,
                    EntryTime: mData.EntryTime,
                    OwnerName: mData.OwnerName,
                    OwnerPhone: mData.OwnerPhone,
                    OwnerDetail: mData.OwnerDetail,
                    Status: 0,
                    IsGR: mData.IsGR,
                }]);
            },
            delete: function(TowingId) {
                return $http.delete('/api/as/Towings', { data: TowingId, headers: { 'Content-Type': 'application/json' } });
            },
            getColor: function() {
                var res = $http.get('/api/as/VehicleColor');
                return res;
            },
            getVehicleMList: function() {
                return $http.get('/api/crm/GetCVehicleModel/');
            },
            getOutletFunction: function() {
                //ini buata rosikhul
                var res = $http.get('/api/as/Gate/OutletFunction');
                return res;
            },
        }
    });