angular.module('app')
  .factory('QueueSettingBp', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    console.log(currentUser);
    return {
      getData: function() {
        var res=$http.get('/api/as/WorkingShift');
        return res;
      },
      create: function(data) {
        return $http.post('/api/as/WorkingShift', [{
                                            ShiftByte:data.ShiftByte,
                                            Name:data.Name,
                                            TimeFrom: data.TimeFrom,
                                            TimeThru: data.TimeThru}]);
      },
      update: function(data){
        return $http.put('/api/as/WorkingShift', [{
                                            Id: data.Id,
                                            ShiftByte:data.ShiftByte,
                                            Name:data.Name,
                                            TimeFrom: data.TimeFrom,
                                            TimeThru: data.TimeThru}]);
      },
      delete: function(id) {
        return $http.delete('/api/as/WorkingShift',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
