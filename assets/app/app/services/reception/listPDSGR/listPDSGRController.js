angular.module('app')
  .controller('listPDSGRController', function($scope, AppointmentGrService, $http, CurrentUser, Role, bsNotify, listPDSGR, RepairSupportActivityGR, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------


    $scope.$on('$viewContentLoaded', function() {
      // $scope.loading = true;
      $scope.gridData = [];
      $scope.loading = false;
      $scope.getAllOPL();
    });


    //----------------------------------
    // Initialization
    //----------------------------------


    console.log('currentuser >>', CurrentUser.user());
    $scope.currUser = CurrentUser.user();
    $scope.user = CurrentUser.user();
    $scope.mData = null; //Model
    $scope.filter = {};
    $scope.xRole = {
      selected: []
    };
    $scope.JobIdforCloseWO = null;
    $scope.formApi = {};


    $scope.filterCounter = false;
    var gridData = [];
    // $scope.filter = [];
    $scope.Param = null;
    $scope.CatWO = null;
    $scope.dataParam = [{
      Id: 3,
      Name: 'No Polisi'
    }, {
      Id: 2,
      Name: 'No WO'
    }, {
      Id: 1,
      Name: 'Tanggal'
    }];

    //----------------------------------
    // Get Data
    //----------------------------------

    $scope.getData = function() {
      //console.log("masuk get data");
      listPDSGR.getData().then(function(res) {
          //console.log("hasil get two", res);
          $scope.grid.data = res.data;
          $scope.loading = false;
        },
        function(err) {
          console.log("err=>", err);
        }
      );
    };

    $scope.selectFilter = function(item) {
      //console.log('item >>', item);
      if (item != null) {
        $scope.Param = item.Id;
        if ($scope.Param == 1) {
          $scope.filter.NoPolisi = "";
          $scope.filter.KodeWO = "";
        } else if ($scope.Param == 2) {
          $scope.filter.NoPolisi = "";
          $scope.filter.DateFrom = undefined;
          $scope.filter.DateTo = undefined;
        } else if ($scope.Param == 3) {
          $scope.filter.KodeWO = "";
          $scope.filter.DateFrom = undefined;
          $scope.filter.DateTo = undefined;
        }
      }

    };

    $scope.onValidateSave = function(data) {
      //console.log('data on JobReq', $scope.JobRequest);
      //console.log('data on OPL', $scope.oplData);
      //console.log('data on onValidateSave', data);
      var JobTaskId = null;
      if ($scope.copyGridWork.length != $scope.gridWork.length) {
        _.map($scope.gridWork, function(val) {
          //console.log('masuk gridWork', val);
          //IF New JobTask and Parts
          if (val.JobId == null || val.JobId === undefined) {
            //console.log('masuk no jobId');
            val.JobTaskId = 0;
            val.JobId = data.JobId;
            if (val.child.length != 0) {
              _.map(val.child, function(e) {
                if (e.Parts.JobTaskId == null || e.Parts.JobTaskId === undefined) {
                  // console.log('masuk childs no jobtask', e);
                  e.Parts.JobPartsId = 0;
                  e.Parts.JobTaskId = 0;
                }
              })
            }
          }
        })
      } else {
        _.map($scope.gridWork, function(a) {
          if (a.child.length != 0) {
            _.map(a.child, function(r) {
              //console.log('masuk ada jobId');
              if (r.Parts.JobTaskId != null) {
                JobTaskId = r.Parts.JobTaskId;
              }
              if (r.Parts.JobTaskId == null || r.Parts.JobTaskId === undefined) {
                r.Parts.JobPartsId = 0;
                r.Parts.JobTaskId = JobTaskId;
                //console.log('masuk ada jobId', r);
              }
            })
          }
        })
      };

      if ($scope.copyOplData.length != $scope.oplData.length) {
        _.map($scope.oplData, function(o) {
          if (o.JobId == null || o.JobId === undefined) {
            o.JobId = data.JobId;
            o.JobTaskOplId = 0;
            o.TargetFinishDate = data.RequiredDate;
          }
        })
      } else {
        _.map($scope.oplData, function(o) {
          o.TargetFinishDate = data.RequiredDate;
        })
      };

      var tempArrParts = [];
      _.map($scope.gridWork, function(value, key) {
        if (value.child.length != null) {
          _.map(value.child, function(v, k) {
            tempArrParts.push(v.Parts);
          });
          value.JobParts = tempArrParts;
          tempArrParts = [];
          delete value.child;
        }
      });
      data.JobTask = $scope.gridWork;
      data.WoCategoryId = $scope.CatWO;
      data.OPL = $scope.oplData;
      //console.log('data on save', data);
      //console.log('data on JobList', $scope.gridWork);
      $scope.CatWO = null;

      return data;
    };

    $scope.onBeforeCancel = function(data) {
      //console.log('data on onBeforeCancel', data);
      $scope.CatWO = null;
      //console.log('data $scope.CatWO', $scope.CatWO);

    };

    $scope.onShowDetail = function(row, mode) {
      //console.log('row onShowDetail >>>', row, mode);
      // $scope.CatWO = null;
      // if (mode == 'edit') {
      $scope.KatashikiCode = row.KatashikiCode;
      listPDSGR.getWObyJobId(row.JobId).then(function(resu) {
        //console.log('resu <<<>>>', resu.data.Result[0]);
        var data = resu.data.Result[0];
        $scope.JobIdforCloseWO = data.JobId;
        $scope.dataForBslist(data);
        $scope.editData(data);
        $scope.dataForOPL(data);
      });
      // }
    };

    $scope.editData = function(data) {
      $scope.JobRequest = data.JobRequest;
      // $scope.oplData = data.JobTaskOpl;
      $scope.JobComplaint = data.JobComplaint;
      $scope.CatWO = data.WoCategoryId;
      $scope.copyOplData = angular.copy($scope.oplData);
      //console.log('resu <<<000>>>', $scope.CatWO, data.WoCategoryId);
    };

    $scope.dataForOPL = function(data) {
      _.map(data.JobTaskOpl, function(val) {
        //console.log('vall >>', val);
        val.RequiredDate = val.TargetFinishDate;
        val.OplName = val.OPL.OPLWorkName;
        val.VendorName = val.OPL.Vendor.Name;
      });
      $scope.oplData = data.JobTaskOpl;
      //console.log('$scope.oplData >>>', $scope.oplData);

    };

    $scope.dataForBslist = function(data) {
      gridTemp = [];

      if (data.JobTask.length > 0) {
        var tmpJob = data.JobTask;
        for (var i = 0; i < tmpJob.length; i++) {
          //console.log("tmpJob", tmpJob);
          var Parts = []
          for (var j = 0; j < tmpJob[i].JobParts.length; j++) {
            Parts.push({
              Parts: tmpJob[i].JobParts[j],
              Qty: tmpJob[i].JobParts[j].Qty
            });
          };
          gridTemp.push({
            ActualRate: tmpJob[i].ActualRate,
            JobTaskId: tmpJob[i].JobTaskId,
            JobTypeId: tmpJob[i].JobTypeId,
            Price: tmpJob[i].Price,
            PaidById: tmpJob[i].PaidById,
            JobId: tmpJob[i].JobId,
            TaskId: tmpJob[i].TaskId,
            TaskName: tmpJob[i].TaskName,
            FR: tmpJob[i].FR,
            Fare: tmpJob[i].Fare,
            ProcessId: tmpJob[i].ProcessId,
            child: Parts
          });
        }
        $scope.gridWork = gridTemp;
        $scope.copyGridWork = angular.copy($scope.gridWork);
        $scope.onAfterSave();
        //console.log("totalw", $scope.totalWork);
      }
      //console.log("$scope.gridWork", $scope.gridWork);
      //console.log("$scope.JobRequest", $scope.JobRequest);
      //console.log("$scope.JobComplaint", $scope.JobComplaint);
    };

    $scope.doCloseWO = function() {
      var id = $scope.JobIdforCloseWO;
      //console.log('id close id', id);
      listPDSGR.closeWO(id).then(function(resu) {
        //console.log('resu close wo', resu.data);
        $scope.formApi.setMode('grid');
        $scope.getData();
      })
    };

    $scope.showAlert = function() {
      $scope.grid.data = [];
      bsNotify.show({
        size: 'big',
        type: 'danger',
        title: "Mohon Input Filter",
      });
    };

    $scope.setData = function(type, data) {
      var startDate = null;
      var endDate = null;
      var noWO = null;
      var noPolice = null;
      if (type == 1) {
        var dateFrom = new Date(data.DateFrom);
        dateFrom.setSeconds(0);
        var yearFrom = dateFrom.getFullYear();
        var monthFrom = ('0' + (dateFrom.getMonth() + 1)).slice(-2);
        var dayFrom = ('0' + dateFrom.getDate()).slice(-2);
        startDate = yearFrom + '-' + monthFrom + '-' + dayFrom;

        var dateTo = new Date(data.DateTo);
        dateTo.setSeconds(0);
        var yearTo = dateTo.getFullYear();
        var monthTo = ('0' + (dateTo.getMonth() + 1)).slice(-2);
        var dayTo = ('0' + dateTo.getDate()).slice(-2);
        endDate = yearTo + '-' + monthTo + '-' + dayTo;

        noWO = '-';
        noPolice = '-';
      } else if (type == 2) {
        startDate = '2017-01-01';
        endDate = '2017-01-31';
        noWO = data.KodeWO;
        noPolice = '-';
      } else if (type == 3) {
        startDate = '2017-01-01';
        endDate = '2017-01-31';
        noWO = '-';
        noPolice = data.NoPolisi;
      }

      listPDSGR.getWoListGr(type, startDate, endDate, noWO, noPolice).then(function(resu) {
        $scope.loading = false;
        //console.log('resu wo list gr >>', resu.data);
        var data = resu.data.Result;
        var arr = [];
        if (data.length != 0) {
          _.map(data, function(e) {
            //console.log('ee >', e);
            if ($scope.currUser.OrgId == e.OutletId) {
              var obj = {};
              obj.Customer = e.NamaKons;
              obj.NoWo = e.WoNo;
              obj.JobId = e.JobId;
              obj.VehicleId = e.VehicleId;
              obj.JobDate = e.JobDate;
              obj.PoliceNumber = e.LicensePlate;
              obj.SA = e.namasa;
              obj.Status = e.status;
              obj.KatashikiCode = e.KatashikiCode;
            } else {
              obj.Customer = e.NamaKons;
              obj.NoWo = e.WoNo;
              obj.JobId = e.JobId;
              obj.VehicleId = e.VehicleId;
              obj.JobDate = e.JobDate;
              obj.PoliceNumber = e.LicensePlate;
              obj.SA = '-';
              obj.Status = e.status;
              obj.KatashikiCode = e.KatashikiCode;
            }

            arr.push(obj);
          });
          $scope.grid.data = arr;
        } else {
          bsNotify.show({
            size: 'big',
            type: 'warning',
            title: "Data Tidak Ditemukan",
          });
          $scope.grid.data = [];
        }

      });
    };

    $scope.selectRole = function(rows) {
      //console.log("onSelectRows=>", rows);
      $timeout(function() {
        $scope.$broadcast('show-errors-check-validity');
      });
    }

    $scope.onSelectRows = function(rows) {
      //console.log("onSelectRows=>", rows);
    };


    //----------------------------------
    // Grid Setup
    //----------------------------------
    var dateFilter = 'date:"dd/MM/yyyy"';
    $scope.gridActionTemplate = '<div class="ui-grid-cell-contents"> \
        <a href="#" ng-hide="row.entity.Status != 18" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="View WO" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-list-alt fa-lg" style="padding:8px 8px 8px 0px;margin-left:25px;"></i></a> \
        <a href="#" ng-hide="row.entity.Status == 18" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Edit WO" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-pencil fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
        </div>';
    $scope.grid = {
      enableSorting: true,
      enableRowSelection: true,
      multiSelect: true,
      enableSelectAll: true,
      //showTreeExpandNoChildren: true,
      // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
      // paginationPageSize: 15,
      columnDefs: [{
          name: 'IdReceptionQueuePDS',
          field: 'IdReceptionQueuePDS',
          visible: false
        },
        {
          name: 'JobId',
          field: 'JobId',
          visible: false
        },
        {
          name: 'No Rangka',
          field: 'VIN'
        }
      ]
    };

    //JOB LIST=============================
    AppointmentGrService.getPayment().then(function(res) {
      $scope.paymentData = res.data.Result;
    });
    AppointmentGrService.getUnitMeasurement().then(function(res) {
      $scope.unitData = res.data.Result;
    });
    AppointmentGrService.getTaskCategory().then(function(res) {
      $scope.taskCategory = res.data.Result;
    });
    AppointmentGrService.getWoCategory().then(function(res) {
      $scope.woCategory = res.data.Result;
      $scope.dataCatWO = angular.copy($scope.woCategory);

      //console.log('$scope.woCategory', $scope.woCategory);
    });
    var gridTemp = [];
    $scope.listApi = {};
    $scope.listSelectedRows = [];
    $scope.modalMode = 'new';
    $scope.totalWork = 0;
    $scope.totalMaterial = 0;
    $scope.totalAll = 0;
    $scope.lmComplaint = {};
    $scope.lmRequest = {};
    $scope.lmModel = {};
    $scope.ldModel = {};

    $scope.ComplaintCategory = [{
        ComplaintCatg: 'Body'
      },
      {
        ComplaintCatg: 'Body Electrical'
      },
      {
        ComplaintCatg: 'Brake'
      },
      {
        ComplaintCatg: 'Drive Train'
      },
      {
        ComplaintCatg: 'Engine'
      },
      {
        ComplaintCatg: 'Heater System & AC'
      },
      {
        ComplaintCatg: 'Restraint'
      },
      {
        ComplaintCatg: 'Steering'
      },
      {
        ComplaintCatg: 'Suspension and Axle'
      },
      {
        ComplaintCatg: 'Transmission'
      },
    ];
    $scope.buttonSettingModal = {
      save: {
        text: 'Service Explanation'
      }
    };
    $scope.conditionData = [{
      Id: 1,
      Name: "Ok"
    }, {
      Id: 2,
      Name: "Not Ok"
    }];
    $scope.typeMData = [{
      Id: 1,
      Name: 1
    }, {
      Id: 2,
      Name: 2
    }, {
      Id: 3,
      Name: 3
    }];
    $scope.JobRequest = [];
    $scope.oplData = [];
    $scope.JobComplaint = [];
    $scope.listView = {
      new: {
        enable: false
      },
      view: {
        enable: false
      },
      delete: {
        enable: false
      },
      select: {
        enable: false
      },
      selectall: {
        enable: false
      },
      edit: {
        enable: false
      },
    }
    $scope.listCustomer = {
      new: {
        enable: false
      },
      view: {
        enable: false
      },
      delete: {
        enable: false
      },
      select: {
        enable: false
      },
      selectall: {
        enable: false
      },
      edit: {
        enable: false
      },
    }
    $scope.buttonSummaryData = {
      new: {
        enable: false
      },
      view: {
        enable: false
      },
      delete: {
        enable: false
      },
      select: {
        enable: false
      },
      selectall: {
        enable: false
      },
      edit: {
        enable: false
      },
    };
    $scope.listButtonSettings = {
      new: {
        enable: false,
        icon: "fa fa-fw fa-car"
      },
      delete: {
        enable: false
      }
    };
    $scope.listButtonSettingsOPL = {
      new: {
        enable: true,
        icon: "fa fa-fw fa-car"
      },
      delete: {
        enable: false
      }
    };
    $scope.listCustomButtonSettings = {
      enable: true,
      icon: "fa fa-fw fa-car",
      func: function(row) {
        console.log("customButton", row);
      }
    };
    // $scope.listButtonFalse = {
    //     new: {
    //         enable: false
    //     }
    // };
    // ==================================================
    $scope.addDetail = function(data, row) {
      console.log("row", row);
      console.log("ini datanya", data);
      $scope.listApi.clearDetail();
      row.FR = data.FR;
      row.Price = data.price;
      row.TaskId = data.TaskId;
      var tmp = {}
      for (var i = 0; i < data.TaskParts.length; i++) {
        tmp = data.TaskParts[i]
        $scope.listApi.addDetail(tmp, row);
      }
    };
    $scope.addDetailOPL = function(data, row) {
      //console.log("row OPL", row);
      //console.log("ini datanya OPL", data);
      //console.log("ini $scope.listApi", $scope.listApi);
      // $scope.listApi.clearDetail();
      row.VendorName = data.VendorName;
      row.OplId = data.Id;
      $scope.listApi.addDetail("", row);

    };
    $scope.onListSelectRows = function(rows) {
      console.log("form controller=>", rows);
    };
    $scope.gridPartsDetail = {
      columnDefs: [{
          name: "NoMaterial",
          field: "PartsCode"
        },
        {
          name: "NamaMaterial",
          field: "PartsName"
        },
        {
          name: "Qty",
          field: "Qty"
        },
        {
          name: "Satuan",
          field: "SatuanId",
          cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.unitDataGrid[row.entity.SatuanId]}}</div>'
        },
        {
          name: "Ketersedian",
          field: "Availbility"
        },
        {
          name: "Tipe",
          field: "Type"
        },
        {
          name: "ETA",
          field: "ETA",
          cellFilter: dateFilter
        },
        {
          name: "Harga",
          field: "RetailPrice"
        },
        {
          name: "Disc",
          field: "Discount"
        },
        {
          name: "SubTotal",
          field: "subTotal"
        },
        {
          name: "NilaiDp",
          field: "Dp"
        },
        {
          name: "StatusDp",
          field: "StatusDp",
          cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>'
        },
        {
          name: "Pembayaran",
          field: "PaidById"
        },
        {
          name: "OPB",
          field: "isOPB",
          cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>'
        }
      ]
    };
    $scope.gridWork = gridTemp;
    $scope.sendWork = function(key, data) {
      var taskandParts = [];
      var taskList = {};
      AppointmentGrService.getDataPartsByTaskId(data.TaskId).then(function(res) {
        //console.log("resabis ambil parts", res.data.Result);
        taskandParts = res.data.Result;
        AppointmentGrService.getDataTaskListByKatashiki($scope.KatashikiCode).then(function(restask) {
          taskList = restask.data.Result;
          console.log("taskList", taskList);
          $scope.addDetail(data, taskandParts, taskList, $scope.lmModel)
        });
      });
    }
    $scope.sendParts = function(key, data) {
      var tmpMaterialId = angular.copy($scope.ldModel.MaterialTypeId);
      //console.log("a $scope.ldModel.Parts", $scope.ldModel);
      $scope.ldModel = data;
      var tmp = {};
      tmp = data;
      $scope.ldModel.PartsName = data.PartsName;
      $scope.ldModel.MaterialTypeId = tmpMaterialId;
      $scope.ldModel.SatuanId = data.UomId;
      // $scope.listApi.addDetail(tmp);
      //console.log("key Parts", key);
      //console.log("data Parts", data);
      //console.log("b $scope.ldModel.Parts", $scope.ldModel);
    }
    $scope.sendOPL = function(key, data) {
      $scope.addDetailOPL(data, $scope.lmModel);
    };
    $scope.allOPL = [];
    $scope.getAllOPL = function() {
      listPDSGR.getAllOPL().then(function(resu) {
        $scope.allOPL = resu.data.Result;
        //console.log('$scope.allOPL', $scope.allOPL);
      });
      RepairSupportActivityGR.getT1().then(function(res) {
        $scope.TFirst = res.data.Result;
        //console.log("T1", $scope.T1);
      });
    };
    //----------------------------------
    // Type Ahead
    //----------------------------------
    // $scope.getWork = function(key) {
    //     var Katashiki = $scope.mData.KatashikiCode;
    //     var catg = $scope.tmpCatg.MasterId;
    //     console.log("$scope.mData.KatashikiCode", Katashiki);
    //     console.log("$scope.lmModel.JobType", catg);
    //     // if (Katashiki != null && catg != null) { //Belum Get Data
    //     var res = AppointmentGrService.getDataTask(key, Katashiki, catg);
    //     return res;
    //     // }
    // };
    $scope.getOPL = function(key) {
      // var Katashiki = $scope.mData.KatashikiCode;
      // var catg = $scope.tmpCatg.MasterId;
      //console.log("key key", key);
      var search = key;
      var results = _.filter($scope.allOPL, function(item) {
        return item.OPLWorkName.indexOf(search) > -1;
      });
      //console.log(results);
      return results;
      // console.log("$scope.lmModel.JobType", catg);
      // if (Katashiki != null && catg != null) { //Belum Get Data
      // var res = AppointmentGrService.getDataTask(key, Katashiki, catg);
      // return res;
      // }
    };
    // $scope.getParts = function(key) {
    //     // if (Katashiki != null && catg != null) {
    //     var res = AppointmentGrService.getDataParts(key);
    //     return res;
    //     // return ress
    //     // }
    // };
    $scope.loadingUsers = false;
    $scope.modelOptions = {
      debounce: {
        default: 500,
        blur: 250
      },
      getterSetter: true
    };
    $scope.noResults = false;
    // $scope.onSelectWork = function($item, $model, $label) {
    //     $scope.sendWork($label, $item);
    // };
    $scope.onSelectOPL = function($item, $model, $label) {
      //console.log('$item', $item);
      //console.log('$model', $model);
      //console.log('$label', $label);
      $scope.sendOPL($label, $item);
    };
    $scope.getWork = function(key) {
      var Katashiki = $scope.KatashikiCode;
      var catg = $scope.tmpCatg.MasterId;
      //console.log("$scope.mDataDetail.KatashikiCode", Katashiki);
      //console.log("$scope.lmModel.JobType", catg);
      if (Katashiki != null && catg != null) {
        var ress = AppointmentGrService.getDataTask(key, Katashiki, catg).then(function(resTask) {
          return resTask.data.Result;
        });
        //console.log("ress", ress);
        return ress
      }
    };
    $scope.getParts = function(key) {
      var ress = AppointmentGrService.getDataParts(key).then(function(resparts) {
        return resparts.data.Result;
      });
      //console.log("ress", ress);
      return ress
        // }
    };
    $scope.modelOptions = {
      debounce: {
        default: 500,
        blur: 250
      },
      getterSetter: true
    };
    $scope.noResults = true;
    // var idx = gridTemp.length;
    $scope.onSelectWork = function($item, $model, $label) {
      // console.log("onSelectWork=>", idx);
      //console.log("onSelectWork=>", $item);
      //console.log("onSelectWork=>", $model);
      //console.log("onSelectWork=>", $label);
      // $scope.mData.Work = $label;
      // console.log('materialArray', materialArray);
      // materialArray = [];
      // materialArray.push($item);
      if ($item.FlatRate != null) {
        //console.log("FlatRate", $item.FlatRate);
        $scope.FlatRateAvailable = true;
      } else {
        $scope.FlatRateAvailable = false;
      }
      $scope.sendWork($label, $item);
      //console.log("modelnya", $scope.lmModel);
      // console.log('materialArray',materialArray);
    }
    $scope.onSelectParts = function($item, $model, $label) {
      // console.log("onSelectWork=>", idx);
      //console.log("onSelectParts=>", $item);
      //console.log("onSelectParts=>", $model);
      //console.log("onSelectParts=>", $label);
      // $scope.mData.Work = $label;
      // console.log('materialArray', materialArray);
      // materialArray = [];
      // materialArray.push($item);
      if ($item.PartsName !== null) {
        $scope.partsAvailable = true;
      } else {
        $scope.partsAvailable = false;
      }
      $scope.sendParts($label, $item);
      //console.log("modelnya", $item);
      // console.log('materialArray',materialArray);
    }
    $scope.onNoResult = function() {
      //console.log("gak", $scope.lmModel);
      $scope.listApi.clearDetail();
      var row = $scope.lmModel;
      row.FlatRate = "";
      row.Fare = "";
      row.Summary = "";
      row.catName = $scope.tmpCatg.Name;
      // row.cusType =$scope.tmpCus.Name;
      $scope.listApi.addDetail('', row);
      //console.log("uunnnnchhh", $scope.lmModel)
      $scope.jobAvailable = false;
      $scope.PriceAvailable = false;
      $scope.FlatRateAvailable = false;
    }
    $scope.onNoPartResult = function() {
      //console.log("onGotResult=>");
      $scope.partsAvailable = false;
      $scope.ldModel.PartsName = null;
      $scope.ldModel.RetailPrice = null;
      $scope.ldModel.PercentDP = null;
      $scope.ldModel.ETA = null;
      $scope.ldModel.SatuanId = null;
      $scope.ldModel.Availbility = null;
    }
    $scope.onGotResult = function() {
      //console.log("onGotResult=>");
    }
    $scope.selected = {};
    $scope.selectTypeWork = function(data) {
      //console.log("data", data);
      $scope.tmpCatg = data;
      //console.log("gridTemp", gridTemp);
      //console.log("gridWork", $scope.lmModel);
    }
    $scope.selectTypeCust = function(data) {
      //console.log("data", data);
      $scope.tmpCus = data;
      $scope.lmModel.cusType = data.Name;
    }
    $scope.onListSave = function(item) {
      //console.log("save_modal=>", item);
      // if(item[$scope.id]){ //Update
      //   //find the master Id
      //   var key = {};
      //   key[$scope.mId] = item[$scope.mId];
      //   var match = _.find($scope.contacts, key);
      //   var xitem = angular.copy(item);
      //   if(match){
      //       var index = _.indexOf($scope.contacts, _.find($scope.contacts, key));
      //       $scope.contacts.splice(index, 1, xitem);
      //   }
      // }else{
      //   $scope.contacts.push(item);
      // }
    }
    $scope.onListCancel = function(item) {
      //console.log("cancel_modal=>", item);
    }
    $scope.onAfterCancel = function() {
      $scope.jobAvailable = false;
      $scope.FlatRateAvailable = false;
      $scope.PriceAvailable = false;
    }
    $scope.onShowModal = function(mode, data) {
      //console.log("====", mode);
      //console.log("=====", data);
    }
    $scope.onAfterSave = function() {
      //console.log("ini sih wkwwk", $scope.taskCategory);
      var totalW = 0;
      for (var i = 0; i < gridTemp.length; i++) {
        if (gridTemp[i].Summary !== undefined) {
          if (gridTemp[i].Summary == "") {
            gridTemp[i].Summary = gridTemp[i].Fare;
          }
          totalW += gridTemp[i].Summary;
        }
      }
      $scope.totalWork = totalW;
      var totalM = 0;
      for (var i = 0; i < gridTemp.length; i++) {
        if (gridTemp[i].child !== undefined) {
          for (var j = 0; j < gridTemp[i].child.length; j++) {
            if (gridTemp[i].child[j].subTotal !== undefined) {
              totalM += gridTemp[i].child[j].subTotal;
              //console.log("gridTemp[i].child.Price", gridTemp[i].child[j].subTotal);
            }
          }
        }
      }
      $scope.totalMaterial = totalM;
      //console.log("gridTemp", gridTemp);
      //console.log("gridWork", $scope.gridWork);
    }
    $scope.onAfterDelete = function() {
      var totalW = 0;
      for (var i = 0; i < gridTemp.length; i++) {
        if (gridTemp[i].Summary !== undefined) {
          if (gridTemp[i].Summary == "") {
            gridTemp[i].Summary = gridTemp[i].Fare;
          }
          totalW += gridTemp[i].Summary;
        }
      }
      $scope.totalWork = totalW;
      // ============
      var totalM = 0;
      for (var i = 0; i < gridTemp.length; i++) {
        if (gridTemp[i].child !== undefined) {
          for (var j = 0; j < gridTemp[i].child.length; j++) {
            if (gridTemp[i].child[j].subTotal !== undefined) {
              totalM += gridTemp[i].child[j].subTotal;
              //console.log("gridTemp[i].child.Price", gridTemp[i].child[j].subTotal);
            }
          }
        }
      }
      $scope.totalMaterial = totalM;
    }

    // $scope.onSelectUser = function(item, model, label){
    //     console.log("onSelectUser=>",item,model,label);
    // }
    //====================
    $scope.checkPrice = function(data) {
      //console.log("data", data);
      var sum = 0;
      if ($scope.ldModel.Qty !== null) {
        var qty = $scope.ldModel.Qty;
        if (qty !== null) {
          sum = qty * data;
          $scope.ldModel.subTotal = sum;
        } else {
          $scope.ldModel.subTotal = 0;
        }
      }
    }
    $scope.checkAvailabilityParts = function(data) {
      //console.log("data data data Parts", data);
      if (data.PartId !== undefined) {
        AppointmentGrService.getAvailableParts(data.PartId).then(function(res) {
          //console.log("Ressss abis availability", res.data);
          var tmpAvailability = res.data.Result[0];
          $scope.ldModel.RetailPrice = tmpAvailability.RetailPrice;
          $scope.ldModel.PercentDP = tmpAvailability.PercentDP;
          $scope.ldModel.ETA = tmpAvailability.DefaultETA;
          $scope.ldModel.Type = 3;
          if (tmpAvailability.isAvailable == 0) {
            $scope.ldModel.Availbility = "Tidak Tersedia";
          } else {
            $scope.ldModel.Availbility = "Tersedia";
          }
        });
      }
    }
    $scope.onAfterSaveOpl = function() {
      var totalW = 0;
      for (var i = 0; i < $scope.oplData.length; i++) {
        if ($scope.oplData[i].Price !== undefined) {
          totalW += $scope.oplData[i].Price;
        }
      }
      $scope.sumWork = totalW;
    };
  });
