angular.module('app')
    .factory('listPDSGR', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        //console.log(currentUser);
        return {
            getData: function() {
                var res = $http.get('/api/as/QueuePDS?isGr=0');
                //console.log('res=>',res);
                //res.data.Result = null;
                return res;
            },
            create: function(data) {
                console.log('data create', data);
                return $http.put('/api/as/Jobs/updateDetil/2', [{
                    JobId: data.JobId,
                    JobTask: data.JobTask,
                    JobTaskOpl: data.OPL
                }]);
            },
            update: function(data) {
                console.log('data update', data);
                //     return $http.post('/api/as/Jobs/postWO', [{
                //         JobId: data.JobId,
                //         JobTask: data.JobTask
                //     }]);
            },
            delete: function(VehicleFareId) {
                return $http.delete('/api/as/Vehiclefare', { data: VehicleFareId, headers: { 'Content-Type': 'application/json' } });
            },
            getWoListGr: function(paramType, paramDate1, paramDate2, ParamWO, ParamNoPolice) {
                return $http.get('/api/as/jobs/WoListAktifSA/FirstDate/' + paramDate1 + '/LastDate/' + paramDate2 +
                    '/gr/1/jenisFilter/' + paramType + '/wono/' + ParamWO + '/plateNo/' + ParamNoPolice + '/Approver/' + 0);
            },
            getWObyJobId: function(id) {
                return $http.get('/api/as/Jobs/' + id);
            },
            getCategoryWO: function() {
                return $http.get('/api/as/GlobalMaster?CategoryId=1007');
            },
            getAllOPL: function() {
                return $http.get('/api/as/OPL');
            },
            closeWO: function(id) {
                return $http.put('/api/as/Jobs/CloseWo/', [id]);

            }
        }
    });