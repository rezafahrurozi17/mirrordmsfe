angular.module('app')
    .factory('oplList', function($http, CurrentUser) {
        var currentUser = CurrentUser.user();
        console.log(currentUser);
        return {
            getData: function(firstDate, lastDate) {
                // var res = $http.get('/api/as/JobTaskOpls/tglOpl/' + firstDate + '/' + lastDate + '');
                var res = $http.get('/api/as/GetOPLList?StartDate=' + firstDate + '&FinishDate=' + lastDate);
                return res;
            },
            // create: function(mOPLdata) {
            //   return $http.post('/api/as/QueueCategories', [{
            //                                       Name: mOPLdata.Name,
            //                                       AutoCallWeight: mOPLdata.AutoCallWeight,
            //                                       Status: mOPLdata.Status
            //                                       }]);
            // },
            update: function(Data) {
                return $http.put('/api/as/JobTaskOpls/In/' + Data.JobTaskOplId + '', [{
                    InvoiceNo: Data.VendorInvoiceNo,
                    InvoiceDate: Data.VendorInvoiceDate
                }]);
            },
            updateStatusPrintOPL: function(data) {
                return $http.get('/api/as/JobTaskOpls/print/' + data.JobTaskOplId + '/' + data.ReceivedBy);
            },
            updateStatusCompleteOPL: function(data, Price, OPLPurchasePrice, Discount) {
                return $http.put('/api/as/JobTaskOpls/In/' + data.JobTaskOplId, [{
                    InvoiceNo: data.VendorInvoiceNo,
                    InvoiceDate: data.VendorInvoiceDate,
                    Price: Price,
                    OPLPurchasePrice : OPLPurchasePrice,
                    Discount: Discount,
                    VendorName: data.VendorName
                }]);
            },
            updateStatusCancelOPL: function(data) {
                console.log('data updateStatusCancelOPL', data);
                // return $http.post('/api/as/JobTaskOpls/Cancel', [{ JobTaskOplId: data.JobTaskOplId }]);
                return $http.post('/api/as/JobTaskOpls/Cancel', [data.JobTaskOplId]);
            },
            updateStatusCancelOPLMore: function(data) {
                console.log('data updateStatusCancelOPL', data);
                // return $http.post('/api/as/JobTaskOpls/Cancel', [{ JobTaskOplId: data.JobTaskOplId }]);
                return $http.post('/api/as/JobTaskOpls/Cancel', data);
            },
            ReOpenOPL: function(data) {
                console.log('data ReOpenOPL', data);
                // return $http.post('/api/as/JobTaskOpls/Cancel', [{ JobTaskOplId: data.JobTaskOplId }]);
                return $http.put('/api/as/JobTaskOpls/oplToOnProgress/', [data.JobId,data.OPLId]);
            },


            ReOpenOPLCR: function(data) {
                console.log('data ReOpenOPL', data);
                return $http.put('/api/as/JobTaskOPLCR/oplOnProgress/', [{JobId:data.JobId, OPLId:data.OPLId, JobTaskOplId:data.JobTaskOplId}]);
            },

            printOPL: function(jobId, jobOplId, flag) {
                return $http.get('/api/as/PrintOpl/' + jobId + '/' + currentUser.OrgId + '/' + jobOplId + '/' + flag);
            },
            // delete: function(QCategoryId) {
            //   return $http.delete('/api/as/QueueCategories',{data:QCategoryId,headers: {'Content-Type': 'application/json'}});
            // }
            checkGateInOPL: function(jobId, policeNo) {
                var resOpl = $http.get('/api/as/JobTaskOpls/checkGateInOPL?jobId=' + jobId + '&policeNo=' + policeNo);
                return resOpl;
            },
            checkStatOpldanJob: function(JobId, JobTaskOPLId, StatusOpl) {
                var resOpl = $http.get('/api/as/Jobs/GetStatusOPL/' + JobId + '/' + JobTaskOPLId + '/' + StatusOpl);
                return resOpl;
            },


            getDetailOPL: function (OplNo) {
                return $http.get('/api/as/GetTaksOPL?OplNo=' + OplNo );
                
            },

            updateStatusOPL:function(status,data){
                var statusFinal = status;
                for(var a in data){
                    for(var b in data[a].OPLTasks){
                        if(data[a].OPLTasks[b].VendorInvoiceDate != null){

                            data[a].OPLTasks[b].VendorInvoiceDate = this.changeFormatDate(data[a].OPLTasks[b].VendorInvoiceDate);
                        }
                        
                        if(data[a].OPLTasks[b].TargetFinishDate != null){

                            data[a].OPLTasks[b].TargetFinishDate = this.changeFormatDate(data[a].OPLTasks[b].TargetFinishDate);
                        }
                        
                        if(data[a].OPLTasks[b].FinishDate != null){
                            data[a].OPLTasks[b].FinishDate = this.changeFormatDate(data[a].OPLTasks[b].FinishDate);
                        }

                        if(data[a].OPLTasks[b].ReceivedBy == null || data[a].OPLTasks[b].ReceivedBy == ""){
                            data[a].OPLTasks[b].Status = 0;
                            console.log("masuk atas",data[a].OPLTasks[b]);
                        }else{
                            data[a].OPLTasks[b].Status = 1;
                            
                            console.log("masuk bawah",data[a].OPLTasks[b]);
                        }
                        
                        if(data[a].OPLTasks[b].VendorInvoiceDate != null && data[a].OPLTasks[b].VendorInvoiceNo != null && data[a].OPLTasks[b].FinishDate != null){
                            data[a].OPLTasks[b].Status = 2;
                        }


                        if(status == 2){
                            if(data[a].OPLTasks[b].VendorInvoiceDate == null){
                                statusFinal = 1;
                            }
                            if(data[a].OPLTasks[b].VendorInvoiceNo == null){
                                statusFinal = 1;
                            }
                            if(data[a].OPLTasks[b].FinishDate == null){
                                statusFinal = 1;
                            }
                        }
                    }
                }
                var datacopy = angular.copy(data)
                var oplaktif = []
                for (var i=0; i<datacopy.length; i++) {
                    for (var j=0; j<datacopy[i].OPLTasks.length; j++) {
                        if (datacopy[i].OPLTasks[j].isDeleted != 1) {
                            oplaktif.push(datacopy[i].OPLTasks[j])
                        }
                    }
                    datacopy[i].OPLTasks = oplaktif
                }
                console.log("data sebelum disimpan",data);
                console.log("datacopy sebelum disimpan",datacopy);



                if (statusFinal == 1) {
                    return $http.put('/api/as/JobTaskOPLCR/UpdateDataStatus/'+statusFinal, datacopy);

                } else {
                    return $http.put('/api/as/JobTaskOPLCR/UpdateDataStatus/'+statusFinal, datacopy); // kirim yg ga ada deleted nya aja, suka eror kl di kirim yg deleted 1
                    // return $http.put('/api/as/JobTaskOPLCR/UpdateDataStatus/'+statusFinal, data);

                }


                // return $http.put('/api/as/JobTaskOPLCR/UpdateDataStatus/'+statusFinal, datacopy);
                // return $http.put('/api/as/JobTaskOPLCR/UpdateDataStatus/'+statusFinal, data);


            },
            changeFormatDate: function(item) {
                var tmpDate = item;
                tmpDate = new Date(tmpDate);
                var finalDate
                var yyyy = tmpDate.getFullYear().toString();
                var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                var dd = tmpDate.getDate().toString();
                finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                console.log("changeFormatDate finalDate", finalDate);
                return finalDate;
            },
            gateOutOpl:function(data){
                
                var dataSave =[];
                // for(var a in data){
                    dataSave.push({
                        PoliceNo : data.PoliceNumber,
                        JobId : data.JobId
                    })
                // }
                console.log("data sebelum disimpan gateout",dataSave);
                return $http.put('/api/as/JobTaskOPLCR/GateOutNewRow/', dataSave);

            },
            getGateOutNewFlag: function(JobId){
                return $http.get('/api/as/CheckGetOut?jobId=' + JobId);
            },
            
            CheckStatusWO: function(JobId) {
                var resOpl = $http.get('/api/as/OPL/CheckStatusWO/' + JobId );
                return resOpl;
            },
            
            CheckStatusGateOutNew: function(PoliceNumber) {
                    var resOpl = $http.get('/api/as/Gate/checkGateOut/' + PoliceNumber );
                    return resOpl;
            },
            CheckKendaraanGateInOut: function(JobId,PoliceNo,flag) {
                //Flag 1 : Cek Dari OPL
                //Flag 2 : Cek Dari Final Inspection Test Drive
                //Flag 3 : Cek Dari Production Rawat Jalan
                //Flag 4 : Cek Dari Prediagnose Test Drive
                // belum gatein 666#TestDrive 666#OPL 666#RawatJalan 666#Prediagnose
                // belum gateout 999#TestDrive999#OPL 999#RawatJalan 999#Prediagnose
                var res = $http.get('/api/as/CheckKendaraanGateInOut/' + JobId + '/' + PoliceNo + '/' + flag);
                return res;
            },
            CekStatusOPL: function(data, jobid) {
                // api/as/OPL/CekStatusOPL/{JobId}
                console.log("CekStatusOPL", data);
                return $http.put('/api/as/OPL/CekStatusOPL/' + jobid , data);
            },
            

            

        }
    });