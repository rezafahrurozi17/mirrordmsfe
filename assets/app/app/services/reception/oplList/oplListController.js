angular.module('app')
    .controller('oplListController', function($scope, $q, $http, CurrentUser, bsAlert, oplList, $timeout, bsNotify, PrintRpt, $compile, AppointmentGrService) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        console.log("$scope opl list",$scope);
        console.log("this",this);
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            $scope.getNewData();
            initOPLEditor_Independent($scope, $compile);
            initDateEditorFinishDate_Independent($scope, $compile);
            initDateEditorEstimasi_Independent($scope, $compile);
            $scope.detail = false;
            $scope.edit = false;

        });

        $scope.$on('tabChange', function(){
            console.log("masuk tab opl list");     
            $scope.gridScrollRightToEnd(500);       
           
        });
        $scope.gridScrollRightToEnd = function(speed){
            // var scrollWidth = $(targetElement).get(0).scrollWidth;
            // var clientWidth = $(targetElement).get(0).clientWidth;
            console.log("scrollWidth",scrollWidth);
            console.log("clientWidth",clientWidth);
            var scrollWidth = 0;
            var clientWidth = 0;
            var test = '#gridopllistEdit .ag-center-cols-viewport';
            // $(targetElement).animate({ scrollLeft: scrollWidth - clientWidth },
            // {
            //     duration: speed,
            //     easing :'swing'
                
            // });
            $(test).animate({ scrollLeft: scrollWidth - clientWidth },
            {
                duration: speed,
                easing :'swing'
                
            }, 800);
        }
        var resizeLayout = function() {
            console.log("Make sure this function called");
            $timeout(function() {
                // $("#layoutContainer_oplListForm > .stretch").css("overflow-y","hidden");
                $("#layoutContainer_oplListForm").height($(window).height());
                // $("#layoutContainer_oplListForm").css("height",500);
                // $(".bs-listx > .bs-list > .panel-body > .ui-list-view-striped").height($(window).height() - 5);
                // $(".bs-listx > .bs-list > .panel-body > .ui-list-view-striped").style.setProperty("overflow","hidden","important");
             
                // $(".bs-listx > .bs-list > .panel-body > .ui-list-view-striped .ui-list-view-cell-content").each(function(){
                //   this.style.setProperty("position","relative");
                // });
            }, 100);
        };

        $scope.onBeforeCancel = function(hiya) {
             $timeout(function() {
                $("#layoutContainer_oplListForm").height($(window).height());
            }, 100)
        }


        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.xmode=null;
        // $scope.formGridApi = {};
        $scope.getNewData = function() {
            $scope.filter.DateStopOpl = new Date();
            $scope.filter.DateStartOpl = new Date();
            $scope.loading = true;
            var a = new Date();
            var yearFirst = a.getFullYear();
            var monthFirst = a.getMonth() + 1;
            var dayFirst = a.getDate();
            var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
            // var b = new Date($scope.filter.DateStopOpl);
            // var yearLast = b.getFullYear();
            // var monthLast = b.getMonth() + 1;
            // var dayLast = b.getDate();
            // var lastDate = yearLast + '-' + monthLast + '-' + dayLast;
            return $q.resolve(
                oplList.getData(firstDate, firstDate).then(function(res) {
                        gridData = [];
                        //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);

                        for (var i = 0; i < res.data.Result.length; i++) {
                            if (res.data.Result[i].isDeleted == 1 && res.data.Result[i].Status != 4){
                                res.data.Result[i].Status = 4; // di timpa biar jadi cancel status nya
                                console.log('ini dia uda di delete tp status ga cancel', res.data.Result[i].OplNo)
                            }
                            res.data.Result[i].OldPrice = angular.copy(res.data.Result[i].OPLPurchasePrice);
                            if(res.data.Result[i].OPL !== null && res.data.Result[i].OPLPurchasePrice == null){
                                res.data.Result[i].OPLPurchasePrice = res.data.Result[i].OPL.OPLPurchasePrice;
                                res.data.Result[i].Price = res.data.Result[i].OPL.Price;
                            } else if (res.data.Result[i].OPLPurchasePrice !== null){
                                res.data.Result[i].OPLPurchasePrice = res.data.Result[i].OPLPurchasePrice;
                                res.data.Result[i].Price = res.data.Result[i].Price;
                            }
                            switch (res.data.Result[i].Status) {
                                case 0:
                                    res.data.Result[i].xStatus = "Open";
                                    break;
                                case 1:
                                    res.data.Result[i].xStatus = "On Progress";
                                    break;
                                case 2:
                                    res.data.Result[i].xStatus = "Completed";
                                    break;
                                case 3:
                                    res.data.Result[i].xStatus = "Billing";
                                    break;
                                case 4:
                                    res.data.Result[i].xStatus = "Cancel";
                                    break;
                            }
                            console.log('data masuk ke 4 3', res.data.Result[i]);
                        }
                        if ($scope.user.RoleId == 1029) {
                            for (var j in res.data.Result) {
                                if (res.data.Result[j].isGr == 0) {
                                    gridData.push(res.data.Result[j]);
                                }
                            }
                        } else {
                            for (var j in res.data.Result) {
                                if (res.data.Result[j].isGr == 1) {
                                    gridData.push(res.data.Result[j]);
                                }
                            }
                        }
                        // $scope.grid.data = res.data.Result;
                        $scope.grid.data = gridData;
                        console.log("role=>", res.data.Result);
                        //console.log("grid data=>",$scope.grid.data);
                        //$scope.roleData = res.data;

                        $scope.loading = false;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                ));
        };

        $scope.user = CurrentUser.user();
        $scope.mOPLdata = []; //Model
        $scope.filter = [];
        $scope.xRole = { selected: [] };
        $scope.dateOptions = {
            startingDay: 1,
            format: 'dd-MM-yyyy',
            // disableWeekend: 1
        };

        $scope.DateOptions = {
            startingDay: 1,
            format: 'dd/MM/yyyy',
            // disableWeekend: 1
        };
        $scope.showOpl = true;

        $scope.GateInOuts = { GateInOut: 0, isExternal: 0 };

        //----------------------------------
        // Get Data
        //----------------------------------

        // var res = [{
        //     oplId : 1,
        //     NoOpl : 1234,
        //     BarangOpl: 'AC',
        //     Vendor : 'Denso',
        //     TanggalTerbit : '2017-04-29',
        //     TanggalDibutuhkan : '2017-05-30',
        //     Harga : 30000,
        //     Penerima : 'Dj Soda',
        //     Status : 'Open'
        // }];
        var dateFormat = 'dd/MM/yyyy';
        $scope.DateFilterOptions1 = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.DateFilterOptions2 = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.dateOptionsNew = {
                    startingDay: 1,
                    format: dateFormat,
                    // mode:"'month'",
        };
        $scope.minDateOption = {
                    minDate: new Date()
        }
        console.log("$scope.minDateOption di opllist",$scope.minDateOption);
        $scope.PrintOpl = function(Data) {
            console.log("Data Print", Data);

            // $scope.DataNota = 'as/PrintOpl/' + Data.JobId + '/' + Data.OutletId + '/' + Data.JobTaskOplId + '/' + Data.isGr;
            $scope.DataNota = 'as/PrintOpl/' + Data.JobId + '/' + Data.OutletId + '/' + Data.VendorId + '/' + Data.isGr +'/' + Data.JobTaskOplId; //CR4 phase2
            $scope.cetakan2($scope.DataNota);

        }

        $scope.cetakan2 = function(data) {
            console.log('cetakan2 ====>',data)


            var pdfFile = null;

            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame 
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    } else {
                        printJS(pdfFile);
                    }
                } else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };

        $scope.gridActionTemplate = '<div class="ui-grid-cell-contents"> \
                <a href="#" ng-click="grid.appScope.actEditOPL(row.entity)" uib-tooltip="Buat Billing" tooltip-placement="bottom" style="color:#777;"> \
                <i class="fa fa-plus-circle fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
             </div>';

        var gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
            '<a href="#" ng-click="grid.appScope.actViewOPL(row.entity)" uib-tooltip="Lihat" tooltip-placement="right" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-hide="row.entity.Status > 1 || row.entity.IsEditable == 0" ng-click="grid.appScope.actEditOPL(row.entity)" uib-tooltip="Edit" tooltip-placement="right" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-hide="row.entity.Status == 4 || row.entity.Status == 0 || row.entity.IsEditable == 0" ng-click="grid.appScope.PrintOpl(row.entity)" uib-tooltip="Print" tooltip-placement="right" style="color:#777;"><i class="fa fa-fw fa-lg fa-print" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-show="(row.entity.Status == 2 && ( row.entity.StatusJob < 14 || row.entity.StatusJob > 15)) && row.entity.IsEditable > 0" ng-click="grid.appScope.reOpen(row.entity)" uib-tooltip="Re Open" tooltip-placement="right" style="color:#777;"><i class="fa fa-fw fa-lg fa-undo" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';
            // row.entity.Status == 2 && !(row.entity.StatusJob >= 14)) || (row.entity.Status == 2 && (row.entity.StatusJob == 22)) ng-show reopen
        var gridData = [];
        $scope.getDataOPLList = function() {
            $scope.loading = true;

            var a = new Date($scope.filter.DateStartOpl);
            var yearFirst = a.getFullYear();
            var monthFirst = a.getMonth() + 1;
            var dayFirst = a.getDate();
            var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;

            var b = new Date($scope.filter.DateStopOpl);
            var yearLast = b.getFullYear();
            var monthLast = b.getMonth() + 1;
            var dayLast = b.getDate();
            var lastDate = yearLast + '-' + monthLast + '-' + dayLast;
            // console.log("last date", lastDate);
            if ($scope.filter.DateStartOpl == null || $scope.filter.DateStopOpl == null) {

                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Tanggal Billing",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            } else {
                return $q.resolve(
                    oplList.getData(firstDate, lastDate).then(
                        function(res) {
                            gridData = [];
                            console.log("res.data.Result | getData ====>", res.data.Result);

                            for (var i = 0; i < res.data.Result.length; i++) {
                                if (res.data.Result[i].isDeleted == 1 && res.data.Result[i].Status != 4){
                                    res.data.Result[i].Status = 4; // di timpa biar jadi cancel status nya
                                    console.log('ini dia uda di delete tp status ga cancel', res.data.Result[i].OplNo)
                                }

                                switch (res.data.Result[i].Status) {
                                    case 0:
                                        res.data.Result[i].xStatus = "Open";
                                        break;
                                    case 1:
                                        res.data.Result[i].xStatus = "On Progress";
                                        break;
                                    case 2:
                                        res.data.Result[i].xStatus = "Completed";
                                        break;
                                    case 3:
                                        res.data.Result[i].xStatus = "Billing";
                                        break;
                                    case 4:
                                        res.data.Result[i].xStatus = "Cancel";
                                        break;
                                }
                            }
                            // console.log('data masuk ke 4 4', res.data.Result[i]);

                            if ($scope.user.RoleId == 1029) {
                                for (var j in res.data.Result) {
                                    res.data.Result[j].isGr = 0;
                                }
                            } else if ($scope.user.RoleId == 1028) {
                                for (var j in res.data.Result) {
                                    res.data.Result[j].isGr = 1;
                                }
                            }

                            gridData = res.data.Result;
                            $scope.grid.data = gridData;
                            $scope.gridApi.core.refresh();
                            $scope.gridApi.core.refreshRows();
                            console.log("gridData ====>", gridData);

                            $scope.loading = false;

               


                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    ));
            }
        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        };
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        };
        $scope.formApi = {};
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        };

        $scope.onShowDetail = function(row, mode) {
            console.log("onShowDetail=>", row, mode);
            console.log("fromApi=>", $scope.formApi);
            resizeLayout();
        };

        $scope.onBeforeEdit = function(row, mode) {
            console.log("onBeforeEdit=>", row, mode);
            console.log("fromApi=>", $scope.formApi);
            resizeLayout();
            // $scope.formApi.setMode('detail');
        };

        $scope.cetakOPL = function() {
            console.log('mOPLdata cetakOPL', $scope.mOPLdata);
            var data = $scope.mOPLdata;
            // $scope.cetakanOPL = 'as/PrintOpl/83/655/2/0';
            // $scope.cetakanOPL = 'as/PrintOpl/' + data.JobId + '/' + $scope.user.OrgId + '/' + data.JobTaskOplId + '/' + data.isGr;
            $scope.cetakanOPL = 'as/PrintOpl/' + data.JobId + '/' + $scope.user.OrgId + '/' + data.VendorId + '/' + data.isGr +'/' + data.JobTaskOplId; //CR4 Phase2
            
            // return $q.resolve($scope.cetakanOPL);
            $scope.cetakan($scope.cetakanOPL);

        };
        $scope.setRowDataBarbar = function(){
            console.log("DisCheck",$scope.DisCheck);
            console.log("$sc",$scope.minDateOption);
        }
        $scope.reOpen = function(row) {
                bsAlert.alert({
                            title: "Apakah anda yakin mengembalikan status menjadi Reopen",
                            text: "",
                            type: "question",
                            showCancelButton: true
                        },
                        function() {
                            oplList.checkStatOpldanJob(row.JobId, row.JobTaskOplId, row.Status).then(function(ress) {
                                if (ress.data == 666){
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Status OPL Sudah Berubah, Silahkan Refresh Data.",
                                    });
                                } else {
                                    console.log('reOpen | row ====>',row)
                                    oplList.ReOpenOPLCR(row).then(function(res) {
                                        $scope.getDataOPLList();
                                    });
                                }

                            });
                        },
                        function () {

                        }
                    )
                }
            // });

            // ==================
        // }
            
            // $scope.cetakOPL2 = function() {
            //     console.log('mOPLdata cetakOPL', $scope.mOPLdata);
            //     var data = $scope.mOPLdata;
            //     // $scope.cetakanOPL = 'as/PrintOpl/83/655/2/0';
            //     $scope.cetakanOPL2 = 'as/PrintOpl/' + data.JobId + '/' + $scope.user.OrgId + '/' + data.JobTaskOplId + '/' + 1;
            //     $scope.cetakan($scope.cetakanOPL2);
            // };

        $scope.cetakan = function(data) {
            console.log('cetakan ====>',data)
            var pdfFile = null;

            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame 
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    } else {
                        printJS(pdfFile);
                    }
                } else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };

        // issue backdate tgl invoice
        // $scope.addDays = function(date, days) {
        //     console.log('jalano adddays 409')
        //     var result = new Date(date);
        //     result.setDate(result.getDate() + days);
        //     return result;
        // }

        $scope.tombol = [
            {
                actionType: 'back',
                title: 'Kembali',
                // icon: 'fa fa-fw fa-print',
                //rightsBit: 1
            },
 
            {
                func: function(row, formScope) {
                    console.log("print 2", row);
                    // if(row.Status == 0){

                    // CR4 Phase2
                    var countInvalidReceivedBy        = 0;
                    var countInvalidVendorInvoiceNo   = 0;
                    var countInvalidVendorInvoiceDate = 0;
                    var taskLength                    = 0;

                    for(var i in row.OPLTasks){
                        taskLength++;
                        if(row.OPLTasks[i].ReceivedBy == "" || row.OPLTasks[i].ReceivedBy == null || row.OPLTasks[i].ReceivedBy == undefined){
                            countInvalidReceivedBy++;
                        }


                        if (row.OPLTasks[i].Discount == '' || row.OPLTasks[i].Discount == ' ' || row.OPLTasks[i].Discount == null || row.OPLTasks[i].Discount == undefined) {
                            row.OPLTasks[i].Discount = 0
                        } else if (row.OPLTasks[i].Discount > 0) {
                            
                        } else {
                            row.OPLTasks[i].Discount = 0
                        }
                    }
                    console.log('countInvalidReceivedBy        === >',countInvalidReceivedBy);
                    console.log('countInvalidVendorInvoiceNo   === >',countInvalidVendorInvoiceNo);
                    console.log('countInvalidVendorInvoiceDate === >',countInvalidVendorInvoiceDate);
                    console.log('taskLength                    === >',taskLength);

                    oplList.CekStatusOPL($scope.cekStatus, row.JobId).then(function(result2) {
                        if (result2.data == 666) {
                            bsAlert.warning("Status OPL sudah berubah", "silahkan refresh terlebih dahulu");
                            return;

                        } else {
                            // CR4 Phase2
                            oplList.CheckKendaraanGateInOut(row.JobId, row.PoliceNumber, 1).then(function(resultnya) {
                                var splitRes = resultnya.data.split('#')
                                
                                if (splitRes[0] == 999){
                                    bsAlert.warning("Kendaraan sudah berada di list Gate Out security untuk aktivitas " + splitRes[1]);
                                    return;
                                } else if (splitRes[0] == 666){
                                    bsAlert.warning("Kendaraan sedang berada di luar Cabang/Bengkel untuk aktivitas " + splitRes[1]);
                                    return;
                                } else {
                                    // boleh gateout nih karena ga ada proses lain yg butuh gateout gatein
                                    // if(row.Status == 0){
                                    // if (row.ReceivedBy != undefined && row.ReceivedBy != null) {
                                        if (taskLength == countInvalidReceivedBy) {
                                                            bsAlert.warning("Mohon isi data penanggung jawab", "silahkan cek kembali");
                                                        } else {
                                                            // $scope.cetakOPL(); function cetak

                                                            console.log('dataGridDetail AGgrid ===>',$scope.dataGridDetail);
                                                            console.log('row ===>',row);

                                                            var idx = _.findIndex(row.OPLTasks, { "ReceivedBy": null, "ReceivedBy": "" });
                                                            console.log("si idx",idx);
                                                            if(idx > -1){
                                                                console.log("jangan update status , masih ada yg kosong ");
                                                                oplList.updateStatusOPL('0',[row]).then(function(resu) { //open
                                                                    console.log('update status Print 2>>', resu.data);
                                                                    $scope.loading = false;
                                                                    $scope.formApi.setMode('grid');
                                                                    $scope.getDataOPLList();
                                                                    bsNotify.show({
                                                                            title: "OPL List",
                                                                            content: "Data berhasil disimpan",
                                                                            type: 'success'
                                                                        });
                                                                });
                                                            }
                                                            else {
                                                                console.log("ini si row B", row.OPLTasks);
                                                                oplList.updateStatusOPL('1',[row]).then(function(resu) { //open
                                                                    console.log('update status Print 2>>', resu.data);
                                                                    $scope.loading = false;
                                                                    $scope.formApi.setMode('grid');
                                                                    $scope.getDataOPLList();
                                                                    bsNotify.show({
                                                                            title: "OPL List",
                                                                            content: "Data berhasil disimpan",
                                                                            type: 'success'
                                                                        });
                                                                });
                                                            }

                                                        }
                                    // } else {
                                    //     bsAlert.warning("Mohon isi data penanggung jawab", "silahkan cek kembali");
                                    // }
                                }

                            })
                        }
                    })

                    

                   
          
                },
                title: 'Simpan',
                // icon: 'fa fa-fw fa-print',
                visible: true
                    //rightsBit: 1
            },
 
            {
                func: function(row, formScope) {
                    console.log('complete opl', row)
                    console.log('complete opl mOPLdata', $scope.mOPLdata)

                    // CR4 Phase2
                    var countInvalidReceivedBy        = 0;
                    var countInvalidVendorInvoiceNo   = 0;
                    var countInvalidVendorInvoiceDate = 0;
                    var taskLength                    = 0;

                    for(var i in row.OPLTasks){
                        taskLength++;
                        if(row.OPLTasks[i].ReceivedBy == "" || row.OPLTasks[i].ReceivedBy == null || row.OPLTasks[i].ReceivedBy == undefined){
                            countInvalidReceivedBy++;
                        }
                        if(row.OPLTasks[i].VendorInvoiceNo == "" || row.OPLTasks[i].VendorInvoiceNo == null || row.OPLTasks[i].VendorInvoiceNo == undefined){
                            countInvalidVendorInvoiceNo++;
                        }
                        if(row.OPLTasks[i].VendorInvoiceDate == "" || row.OPLTasks[i].VendorInvoiceDate == null || row.OPLTasks[i].VendorInvoiceDate == undefined){
                            countInvalidVendorInvoiceDate++;
                        }

                        if (row.OPLTasks[i].Discount == '' || row.OPLTasks[i].Discount == ' ' || row.OPLTasks[i].Discount == null || row.OPLTasks[i].Discount == undefined) {
                            row.OPLTasks[i].Discount = 0
                        } else if (row.OPLTasks[i].Discount > 0) {
                            
                        } else {
                            row.OPLTasks[i].Discount = 0
                        }
                    }
                    console.log('countInvalidReceivedBy        === >',countInvalidReceivedBy);
                    console.log('countInvalidVendorInvoiceNo   === >',countInvalidVendorInvoiceNo);
                    console.log('countInvalidVendorInvoiceDate === >',countInvalidVendorInvoiceDate);
                    console.log('taskLength                    === >',taskLength);
                    // CR4 Phase2

                    if (taskLength == countInvalidReceivedBy) {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Mohon Isi Penanggung Jawab",
                            timeout: 2000
                        });
                        return -1;
                    }
                    
                    if (taskLength == countInvalidVendorInvoiceNo) {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Mohon Isi Nomor Vendor Invoice",
                            timeout: 2000
                        });
                        return -1;
                    }

                    if (taskLength == countInvalidVendorInvoiceDate) {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Mohon Isi Tanggal Vendor Invoice",
                            timeout: 2000
                        });
                        return -1;
                    }

                    var gateInOuts = angular.copy($scope.GateInOuts);
                    if (gateInOuts.isExternal == 1) {
                        if (gateInOuts.GateInOut == -1) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "No. Polisi " + row.Job.PoliceNumber + " belum keluar dari bengkel",
                                timeout: 4000
                            });
                            return -1;
                        } else if (gateInOuts.GateInOut == -2) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "No. Polisi " + row.Job.PoliceNumber + " belum kembali ke bengkel",
                                timeout: 4000
                            });
                            return -1;
                        }
                    }

                    var idx = _.findIndex(row.OPLTasks, { "VendorInvoiceDate": null , "VendorInvoiceNo": null, "FinishDate": null });
                    var idx2 = _.findIndex(row.OPLTasks, { "VendorInvoiceDate": "" , "VendorInvoiceNo": "", "FinishDate": "" });
                    var idx3 = _.findIndex(row.OPLTasks, { "VendorInvoiceDate": null , "VendorInvoiceNo":"","FinishDate": "" });
                    
                    // issue backdate tgl invoice
                    // console.log('data 561 oplfact-> ', row);
                    // for (var i = 0; i < row.OPLTasks.length; i++) {
                    //     console.log('sbelum convert ', i, ' -> ', row.OPLTasks[i].VendorInvoiceDate);
                    //     var tgl_baru = $scope.addDays(row.OPLTasks[i].VendorInvoiceDate, 1);
                    //     console.log('stelah convert ', i, ' -> ', tgl_baru);
                    //     row.OPLTasks[i].VendorInvoiceDate = tgl_baru;
                    // }
                    
                    console.log("IDX1", idx);
                    console.log("IDX2", idx2);

                    oplList.CekStatusOPL($scope.cekStatus, row.JobId).then(function(result2) {
                        if (result2.data == 666) {
                            bsAlert.warning("Status OPL sudah berubah", "silahkan refresh terlebih dahulu");
                            return -1;

                        } else {
                            if(idx > -1 || idx2 > -1 || idx3 > -1){
                                console.log("Simpan OPL | 1 OPLTask ===>", row.OPLTasks);
                                console.log("Simpan OPL | 1 row =======>", row);
                                oplList.updateStatusOPL('1',[row]).then(function(resu) { //on-progress
                                        console.log('update status Complete >>', resu.data);
                                        $scope.formApi.setMode('grid');
                                        $scope.getDataOPLList();
                                });
                            }else{
                                console.log("Simpan OPL | 2 OPLTask ===>", row.OPLTasks);
                                console.log("Simpan OPL | 2 row =======>", row);
        
                                oplList.updateStatusOPL('2',[row]).then(function(resu) { //on-progress
                                    console.log('update status Complete >>', resu.data);
                                    $scope.formApi.setMode('grid');
                                    $scope.getDataOPLList();
                                });
                            }

                        }
                    })

                    
                },

                title: 'Simpan OPL',
                icon: 'fa fa-fw fa-book',
                visible: true
            }
        ];

        // $scope.statusOpl = function(param){

        // };

        $scope.setModeTombol = function(param) {
            resizeLayout();
            _.map($scope.tombol, function(val) {
                val.visible = param;
            });
        };


        $scope.arr_checkGate = [];
        $scope.result = 0;
        var flagNotOut = 0;
        var flagNotIn = 0;
        $scope.cekGetInOut = function(JobId,PoliceNumber){
			console.log('JobId stv : ', JobId);
			console.log('PoliceNumber stv : ', PoliceNumber);
            var resGateIn = 0;
            oplList.checkGateInOPL(JobId, PoliceNumber).then(
                function(resu) {
                    console.log('cekGetInOut data =====>: ', resu);
                    $scope.result = angular.copy(resu.data)
                    resGateIn = resu.data;
                    $scope.GateInOuts.GateInOut = resGateIn;
                    console.log("$scope.GateInOuts",typeof $scope.GateInOuts.GateInOut);
                    if(resu.data == -1){
                        flagNotOut++
                        console.log("flagNotOut",flagNotOut);
                        if(flagNotOut == 1){
                            oplList.getGateOutNewFlag(JobId).then(function(res) {
                               console.log("res.data get new opl",res.data);
                               // 1 matiin tombol
                               // 0 nyalain tombol
                                if(res.data == 0){
                                    $scope.disableGateOut = false;
                                }else{
                                     $scope.disableGateOut = true;
                                }
                            },function (err) {

                            });

                            $scope.bsAlertConfirmDelete("No. Polisi " + $scope.mOPLdata.PoliceNumber + " belum keluar dari bengkel").then(function (res) {
                                if (res) {
                                    console.log('ini kalo klik ok',res);
                                    return true
                                }else{
                                    console.log("ini kalo klik cancle", res);
                                    return false
                                }
                            });    
                        }
                       
                    } else
                    if(resu.data == -2){
                        flagNotIn++
                        console.log("flagNotIn",flagNotIn);
                        if(flagNotIn == 1){

                            $scope.disableGateOut = true;
                            $scope.bsAlertConfirmDelete("No. Polisi " + $scope.mOPLdata.PoliceNumber + " belum kembali ke bengkel").then(function (res) {
                                if (res) {
                                    console.log('ini kalo klik ok',res);
                                    return true
                                }else{
                                    console.log("ini kalo klik cancle", res);
                                    return false
                                }
                            });
                        }
                    }

                    if(typeof $scope.GateInOuts.GateInOut == "object"){
                        oplList.getGateOutNewFlag(JobId).then(function(res) {
                           console.log("res.data get new opl after in out",res.data);
                           // 1 matiin tombol
                           // 0 nyalain tombol
                            if(res.data == 0){
                                $scope.disableGateOut = false;
                            }else{
                                 $scope.disableGateOut = true;
                            }
                        },function (err) {

                        });
                    }
                    console.log('cekGetInOut result ===>',$scope.result);
                }
            )

        }

        $scope.actViewOPL = function(data) {
            $scope.detail = true;
            $scope.edit = false;
            
            console.log("actViewOPL=>", data);
            $scope.xmode = 'view';
            switch (data.Status) {
                case 0:
                    data.xStatus = "Open";
                    break;
                case 1:
                    data.xStatus = "On Progress";
                    break;
                case 2:
                    data.xStatus = "Completed";
                    break;
                case 3:
                    data.xStatus = "Billing";
                    break;
                case 4:
                    data.xStatus = "Cancel";
                    break;
            }
            console.log('data masuk ke 4 1', data);
            $scope.mOPLdata = data;
            console.log("actViewOPL=> $scope.mOPLdata", $scope.mOPLdata);
            $scope.showOpl = false;
            $scope.setModeTombol(false);
            $scope.tombol[0].visible = true;
            $scope.tombol[1].visible = false;
            $scope.tombol[2].visible = false;
            $scope.formApi.setMode('detail');
            resizeLayout();


            // CR4 Phase2
            // $scope.mOPLdata.VendorId       = $scope.mOPLdata.OPL.Vendor.VendorId;
            // $scope.mOPLdata.VendorName     = $scope.mOPLdata.OPL.Vendor.VendorName;
            // $scope.mOPLdata.VendorOutletId = $scope.mOPLdata.OPL.Vendor.VendorOutletId;
            // $scope.mOPLdata.VendorCode     = $scope.mOPLdata.OPL.Vendor.VendorCode;
            $timeout(function(){
                oplList.getDetailOPL(data.OplNo).then(
                    function(res) {
                        console.log("getDetailOPL success ====>", res.data.Result);
                        $scope.gridDetailOPLlist_IndependentApi.setRowData([]);
                        $scope.setDataForAGgrid(res.data.Result,true);
                        
                    },function (err) {
                        console.log("getDetailOPL errorr =====>", err);
    
                    }
                );
              },1000);
            // CR4 Phase2

        };


        $scope.dataGridDetail = [];
        $scope.setDataForAGgrid = function (data,addKeterangan) {
            console.log('setDataForAGgrid ===>',data)
            $scope.dataGridDetail = [];
            $scope.dataGridDetail = data;
            var no = 1;
            for(var i in $scope.dataGridDetail){
                $scope.dataGridDetail[i].No = no ++;
                var findPayment = _.find($scope.paymentData,{"MasterId":$scope.dataGridDetail[i].PaidById});
                $scope.dataGridDetail[i].PaidBy = findPayment.Name;
                if(addKeterangan == true){
                    if( $scope.dataGridDetail[i].isExternal == 0){
                        $scope.dataGridDetail[i].isExternalDesc = " (Internal)"
                    }else{
                        $scope.dataGridDetail[i].isExternalDesc = " (External)"
                    }
                    $scope.dataGridDetail[i].OPLWorkName = $scope.dataGridDetail[i].OPLWorkName +" "+$scope.dataGridDetail[i].isExternalDesc;
                }
                if($scope.dataGridDetail[i].isDeleted == 0){
                    $scope.dataGridDetail[i].isDeletedDesc = "Aktif"
                }else{
                    $scope.dataGridDetail[i].isDeletedDesc = "Non Aktif"
                }
                console.log("Cek IsDeleted",$scope.dataGridDetail[i].isDeletedDesc)
            }
            $scope.mOPLdata.OPLTasks = $scope.dataGridDetail;
            $scope.gridDetailOPLlist_IndependentApi.setRowData($scope.dataGridDetail);
            


        }


        $scope.bsAlertConfirm = function (title, text) {
            var defer = $q.defer();
            bsAlert.alert({
                title: title || 'Estimasi BP',
                text: text || '',
                type: "question",
                showCancelButton: false
            },
                function () {
                    defer.resolve(true);
                },
                function () {
                    defer.resolve(false);
                }
            )
            return defer.promise;
        };

        $scope.bsAlertConfirmDelete = function (title, text) {
            var defer = $q.defer();
            bsAlert.alert({
                title: title || 'Delete Data Estimasi BP',
                text: text || '',
                type: "warning",
                showCancelButton: false
            },
                function () {
                    defer.resolve(true);
                },
                function () {
                    defer.resolve(false);
                }
            )
            return defer.promise;
        };
       
        $scope.tmpStatus = null;
        $scope.actEditOPL = function(data,mode) {
            $scope.detail = false;
            $scope.edit = true;
            $scope.mOPLdata = data;
            $scope.tmpStatus = $scope.mOPLdata.Status
            console.log("actEditOPL=>", data,mode);
            console.log("actEditOPL=> $scope.mOPLdata", $scope.mOPLdata);

            $scope.cekStatus = [];

            $scope.showOpl = false;
            $scope.xmode = 'edit';
            switch (data.Status) {
                case 0:
                    data.xStatus = "Open";
                    break;
                case 1:
                    data.xStatus = "On Progress";
                    // Checking for validate on Gate In
                    if ((data.Job != undefined || data.Job != null) && data.OPL.isExternal == 1) {
                        var resGateIn = 0;
                        oplList.checkGateInOPL(data.Job.JobId, data.Job.PoliceNumber).then(function(resu) {
                            console.log('check GateIn: ', resu);
                            resGateIn = resu.data;
                            $scope.GateInOuts.GateInOut = resGateIn;
                            $scope.GateInOuts.isExternal = data.OPL.isExternal;

                            $scope.bsAlertConfirmDelete("No. Polisi " + data.PoliceNumber + " belum keluar dari bengkel").then(function (res) {
                                if (res) {
                                    console.log('ini kalo klik ok',res);
                                    return true
                                }else{
                                    console.log("ini kalo klik cancle", res);
                                    return false
            
                                }
                            });

                        });

                        
                    }
                    
                    break;
                case 2:
                    data.xStatus = "Completed";
                    break;
                case 3:
                    data.xStatus = "Billing";
                    break;
                case 4:
                    data.xStatus = "Cancel";
                    break;
            }
            console.log('data masuk ke 4 2', data);

            

          


            _.map($scope.tombol, function(a) {
                if (data.Status == 0) {
                    if (a.title === "Simpan OPL") {
                        console.log('masuk status 1');
                        a.visible = false;
                    } else {
                        a.visible = true;
                    };
                } else if (data.Status == 1) {
                    if (a.title === "Simpan") {
                        console.log('masuk status 0');
                        a.visible = false;
                    } else {
                        a.visible = true;
                    };
                };
            });




            
            $scope.formApi.setMode('detail');


            $timeout(function(){
                $scope.setDetailOPLGrid(data.OplNo);
                console.log('actEditOPL | tmpStatus ========>',$scope.tmpStatus)
                console.log('actEditOPL | disableGateOut ===>',$scope.disableGateOut)
                console.log('actEditOPL | detail ===========>',$scope.detail)
            },1000);
            
            
            
        };
        
        
        var sebelumEditJobList = [];
        
        $scope.result = 0;
        $scope.arr_checkGate = [];
        $scope.dataOPLDetail = [];
        $scope.setDetailOPLGrid = function(OplNo){
            sebelumEditJobList = [];
            $scope.dataOPLDetail = [];
            flagNotOut = 0;
            flagNotIn = 0;
            oplList.getDetailOPL(OplNo).then(
                function(res) {
                    console.log("setDetailOPLGrid success ====>", res.data.Result);
                    $scope.dataOPLDetail = res.data.Result;
                    $scope.dataOPLDetailx = angular.copy(res.data.Result);

                    var objS = {}
                    for (var x=0; x<$scope.dataOPLDetailx.length; x++) {
                        objS = {}
                        objS.JobTaskOplId = $scope.dataOPLDetailx[x].JobTaskOplId
                        objS.Status = $scope.dataOPLDetailx[x].Status
                        $scope.cekStatus.push(objS)
                    }

                    console.log('duar', $scope.cekStatus)


                    var flagext = 0;
                    // $scope.GateInOuts.isExternal = data.OPL.isExternal;
                    for(var i in $scope.dataOPLDetail){
                        

                        if($scope.dataOPLDetail[i].VendorInvoiceNo != null && $scope.dataOPLDetail[i].VendorInvoiceDate != null && $scope.dataOPLDetail[i].FinishDate != null ){
                            $scope.dataOPLDetail[i].Status = 2; 
                        }
                        sebelumEditJobList.push(angular.copy($scope.dataOPLDetail[i]));
                        
                        // if ( $scope.mOPLdata.Status == 1) {
                        //if ($scope.dataOPLDetail[i].isExternal == 1 && $scope.mOPLdata.Status == 1) {
						if ($scope.dataOPLDetail[i].isExternal == 1 && $scope.dataOPLDetail[i].Status == 1) {
                            flagext++
                            $scope.cekGetInOut($scope.dataOPLDetail[i].JobId, $scope.mOPLdata.PoliceNumber);
                           
                            // $scope.setModeTombol(true);
                            // $scope.tombol[0].visible = true;
                            // $scope.tombol[1].visible = false;
                            // $scope.tombol[2].visible = false;
                        }
                    }
                    console.log("flagext",flagext);
                    if(flagext == 0){
                        $scope.disableGateOut = true;                       
                    }
                    console.log('cekGetInOut arr_checkGate ===>', $scope.arr_checkGate)
                    console.log("setDetailOPLGrid sebelumEditJobList ====>", sebelumEditJobList);
                    $scope.gridDetailOPLlist_IndependentApi.setRowData([]);
                          
                    $scope.setDataForAGgrid($scope.dataOPLDetail,true);   
                },function (err) {
                    console.log("setDetailOPLGrid errorr =====>", err);

                }
            );

        }


        // $scope.onValidateSave = function(data){
        //     oplList.update(data).then(
        //                     function(res){
        //                        // $scope.getDataOPLList();
        //                        console.log("simpan success");
        //                     },
        //                     function(err){
        //                         console.log("err=>",err);
        //                     }
        //         );
        //     console.log("validate save", data);
        // }
        //----------------------------------
        // Grid Setup
        //----------------------------------
        var dateFilter = 'date:"dd/MM/yyyy"';

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            enableFiltering: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'oplId', field: 'JobTaskOplId', width: 95, visible: false },
                { name: 'No. OPL', field: 'OplNo', width: 200 },
                { name: 'Nama Pekerjaan', field: 'OPLWorkName', width: 150,visible: false },
                { name: 'Vendor', field: 'VendorName', width: 200 },
                { name: 'No. Polisi', field: 'PoliceNumber', width: 95 },
                { name: 'No. Wo', field: 'WoNo', width: 200 },
                { name: 'Tanggal Terbit', field: 'CreateOplDate', cellFilter: dateFilter, width: 120 },
                { name: 'Estimasi selesai', field: 'TargetFinishDate', cellFilter: dateFilter, width: 120 },
                //{ name: 'Harga', field: 'OPL.OPLPurchasePrice', width: 100, cellFilter: "rupiahC" },
                { name: 'Total Harga', field: 'Price', width: 100, cellFilter: 'currency:"":0' },
                { name: 'Status', field: 'xStatus', width: 120 },
                { name: 'Action', width: 180, pinnedRight: true, cellTemplate: gridActionButtonTemplate }
            ],
            onRegisterApi: function(gridApi) {
                // set gridApi on $scope
                $scope.gridApi = gridApi;
                $scope.gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                    // console.log("selected=>",$scope.selectedRows);
                    if ($scope.onSelectRows) {
                        $scope.onSelectRows($scope.selectedRows);
						console.log('hover on');
                    }
                });
            }
        };

        // =======Adding Custom Filter==============
        $scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
        $scope.gridApiAppointment = {};
        $scope.filterColIdx = 1;
        var x = -1;
        for (var i = 0; i < $scope.grid.columnDefs.length; i++) {
            if ($scope.grid.columnDefs[i].visible == undefined && $scope.grid.columnDefs[i].name !== 'Action') {
                x++;
                $scope.gridCols.push($scope.grid.columnDefs[i]);
                $scope.gridCols[x].idx = i;
            }
            // console.log("$scope.gridCols", $scope.gridCols);
            // console.log("$scope.grid.columnDefs[i]", $scope.grid.columnDefs[i]);
        }
        $scope.filterBtnLabel = $scope.gridCols[0].name;
        $scope.filterBtnChange = function(col) {
            console.log("col", col);
            $scope.filterBtnLabel = col.name;
            $scope.filterColIdx = col.idx + 1;
        };


        $scope.autoFillData = function(field){
            $scope.gridDetailOPLlist_IndependentApi.setRowData([]);
            var VendorInvoiceNo   = $scope.dataGridDetail[0].VendorInvoiceNo; 
            var VendorInvoiceDate = $scope.dataGridDetail[0].VendorInvoiceDate ; 
            var ReceivedBy        = $scope.dataGridDetail[0].ReceivedBy
            console.log("dataGridDetail",$scope.dataGridDetail);
            for(var i in $scope.dataGridDetail){
                if(field == 'VendorInvoiceDate'){
                    if($scope.dataGridDetail[i].Status != 2 && $scope.dataGridDetail[i].isDeleted != 1){
                        if( $scope.dataGridDetail[i].isExternal == 1){

                            if(typeof $scope.GateInOuts.GateInOut == "object"){
                                $scope.dataGridDetail[i].VendorInvoiceDate = VendorInvoiceDate;
                            }
                        }else{
                           $scope.dataGridDetail[i].VendorInvoiceDate = VendorInvoiceDate; 
                        }

                    }
                }else if(field == 'VendorInvoiceNo'){
                    if($scope.dataGridDetail[i].Status != 2 && $scope.dataGridDetail[i].isDeleted != 1){

                        if( $scope.dataGridDetail[i].isExternal == 1){
                            if(typeof $scope.GateInOuts.GateInOut == "object"){
                                $scope.dataGridDetail[i].VendorInvoiceNo   = VendorInvoiceNo; 
                            }
                        }else{
                            $scope.dataGridDetail[i].VendorInvoiceNo   = VendorInvoiceNo;                            
                        }
                    }
                }else if(field == 'ReceivedBy'){
                    
                    if($scope.dataGridDetail[i].Status != 2 && $scope.dataGridDetail[i].isDeleted != 1){
                        $scope.dataGridDetail[i].ReceivedBy   = ReceivedBy;
                    }
                }
            }


            $scope.setDataForAGgrid($scope.dataGridDetail,false);

        }
        $scope.disableGateOut = false;
        $scope.gateOutOpl = function(){
                    bsAlert.alert({
                                title: "Apakah anda yakin ingin melakukan Gate Out OPL ?",
                                text: "",
                                type: "question",
                                showCancelButton: true
                            },
                            function() {
                                console.log("$scope.mOPLdata",$scope.mOPLdata);
                                  oplList.CheckStatusGateOutNew($scope.mOPLdata.PoliceNumber).then(
                                      function(res) {
                                          console.log("res.data new",res.data);
                                          if(res.data == true){
                                              bsAlert.alert({
                                                  title: "Kendaraan sudah ada di list security dengan pekerjaan yang lain",
                                                  text: "",
                                                  type: "warning",
                                                  showCancelButton: false
                                              });
                                          }else{
                                              
                                              oplList.gateOutOpl($scope.mOPLdata).then(
                                                  function(res) {
                                                         bsAlert.alert({
                                                             title: "No. Polisi "+$scope.mOPLdata.PoliceNumber+" berhasil disimpan",
                                                             text: "",
                                                             type: "success",
                                                             showCancelButton: false
                                                         });
                                                         $scope.loading = false;
                                                         $scope.formApi.setMode('grid');

                                                        
                                                  },function (err) {
                                                      console.log("setDetailOPLGrid errorr =====>", err);

                                                  }
                                              );
                                          }
                                            
                                      },function (err) {
                                          console.log("setDetailOPLGrid errorr =====>", err);

                                      }
                                  );
                                  
                            },
                            function() {

                            }
                        )
                    
        }

        


        // CR4 Phase2
        //jancok
        $scope.onAfterCellEditAg = function(idx,field,val,node) {
            console.log("$scope.mOPLdata",$scope.mOPLdata);
            var dataforGrid = {};
            console.log("onAfterCellEditAg | idx =>",idx, "field =>",field, "val =>",val, "node =>",node);
            console.log("onAfterCellEditAg | dataforGrid",dataforGrid);
            console.log("onAfterCellEditAg | node.data",node.data);
            console.log('onAfterCellEditAg | sebelumEditJobList ===>',sebelumEditJobList);
            
            var findOldObj = _.find(sebelumEditJobList, {'JobTaskOplId':node.data.JobTaskOplId});
            console.log('onAfterCellEditAg | findOldObj ===>',findOldObj);
            var findIndex = _.findIndex($scope.dataGridDetail, {'JobTaskOplId':node.data.JobTaskOplId});
            console.log('onAfterCellEditAg | findIndex ====>',findIndex);


            if(findOldObj != undefined){

                if(field == 'Discount' || field == 'OPLPurchasePrice' ||field == 'Price' || field == 'QtyPekerjaan'){
                    if (val == '' || val == ' ' || val == null || val == undefined) {
                        val = 0
                    } else  if (val > 0) {
                        val = parseInt(val)
                    } else {
                        val = 0
                    }
                }
                console.log('findOldObj[field] ===>',findOldObj[field] );
                console.log("val",val);
                if(findOldObj[field] == val){
                    console.log('tidak ada perubahan');
                    // node.data.Status = 0
                    // kata SA status berubah pas udah disimpan
                }else{
                    console.log('ada perubahan');
                    // node.data.Status = 1
                    
                    // kata SA status berubah pas udah disimpan
                }
            }


            if(field == 'TargetFinishDate'){
                console.log("$scope.mOPLdata.TargetFinishDate",$scope.mOPLdata.TargetFinishDate);
                dataforGrid = node.data;
                dataforGrid.TargetFinishDate = $scope.mOPLdata.TargetFinishDate;
                console.log("dataforGrid",dataforGrid);
                node.setData(dataforGrid);
            }

            if(field == 'VendorInvoiceDate'){
                console.log("$scope.mOPLdata.VendorInvoiceDate",$scope.mOPLdata.VendorInvoiceDate);
                dataforGrid = node.data;
                dataforGrid.VendorInvoiceDate = $scope.mOPLdata.VendorInvoiceDate;
                console.log("dataforGrid",dataforGrid);
                // if(node.data.VendorInvoiceNo == null || node.data.VendorInvoiceDate == null || node.data.VendorInvoiceNo == "" || node.data.VendorInvoiceDate == "" ){

                //     $scope.disCheckButton = true;
                // }else{
                //     $scope.disCheckButton = false;
                // }
                node.setData(dataforGrid);
            }

            if(field == 'Notes'){
                dataforGrid = node.data;
                console.log("dataforGrid",dataforGrid);
                node.setData(dataforGrid);
            }

            if(field == 'Discount'){
                node.data.Discount = parseInt(val);
                if(node.data.Discount > 100){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Diskon Tidak Boleh Melebihi 100",
                    }); 
                    dataforGrid = node.data;
                    dataforGrid.Discount = 0;
                    node.setData(dataforGrid);
                }else{

                    dataforGrid = node.data;
                    console.log("dataforGrid",dataforGrid);
                    node.setData(dataforGrid);	
                }
            }

       

            if(field == 'OPLPurchasePrice'){
                node.data.OPLPurchasePrice = parseInt(val);
                console.log('Harga Beli - OPLPurchasePrice ===>',$scope.mOPLdata.OPLPurchasePrice);
                // if(node.data.OPLPurchasePrice == 0){

                //     bsNotify.show({
                //         size: 'big',
                //         type: 'danger',
                //         title: "Harga Beli Tidak Boleh 0",
                //     }); 
                // }else{

                    dataforGrid = node.data;
                    console.log("dataforGrid",dataforGrid);
                    node.setData(dataforGrid);
                // }
            }



            if(field == 'Price'){
                node.data.Price = parseInt(val);
                console.log('Harga Beli - Price ===>',$scope.mOPLdata.Price);
                // if(node.data.Price == 0){

                //     bsNotify.show({
                //         size: 'big',
                //         type: 'danger',
                //         title: "Harga Jual Tidak Boleh 0",
                //     }); 
                // }else{

                    dataforGrid = node.data;
                    console.log("dataforGrid",dataforGrid);
                    node.setData(dataforGrid);
                // }
            }


            if(field == 'VendorInvoiceNo'){
                console.log('Harga Beli - VendorInvoiceNo ===>',$scope.mOPLdata.VendorInvoiceNo);
                console.log('node.data  - VendorInvoiceNo ===>', node.data.VendorInvoiceNo);
                console.log(" node.data.VendorInvoiceNo", typeof(node.data.VendorInvoiceNo))
                if(node.data.VendorInvoiceNo == ''){
                    node.data.VendorInvoiceNo = null;
                }
                dataforGrid = node.data;
                // if(node.data.VendorInvoiceNo == null || node.data.VendorInvoiceDate == null || node.data.VendorInvoiceNo == "" || node.data.VendorInvoiceDate == "" ){

                //     $scope.disCheckButton = true;
                // }else{
                //     $scope.disCheckButton = false;
                // }

                node.setData(dataforGrid);
                
            }

            if(field == 'QtyPekerjaan'){
                node.data.QtyPekerjaan = parseInt(val);
                console.log('mOPLdata      - QtyPekerjaan ===>',$scope.mOPLdata.QtyPekerjaan);
                console.log('node.data  - QtyPekerjaan ===>', node.data.QtyPekerjaan);
                dataforGrid = node.data;
                node.setData(dataforGrid);
            }


            if(field == 'Notes'){
                console.log('mOPLdata      - Notes ===>',$scope.mOPLdata.Notes);
                console.log('node.data  - Notes ===>', node.data.Notes);
                dataforGrid = node.data;
                node.setData(dataforGrid);
            }


            if(findIndex == 0){
                console.log('onAfterCellEditAg | ini update semua data di bawahnya karna indexnya 0 ===>',findIndex);
                $scope.autoFillData(field);
            }else{
                console.log('onAfterCellEditAg | ini update dirinya sendiri karna indexnya bukan 0 ===>',findIndex);
            }


            
            


            // if(node.data.TaskNameOpl == ""|| node.data.Payment == ""|| node.data.EstimationDate == "" || node.data.Qty == "" || node.data.HargaBeli == ""|| node.data.HargaBeli == 0 || node.data.HargaJual == ""|| node.data.HargaJual == 0){
            //     $scope.disableSimpan = true;
            // }else{
            //     $scope.disableSimpan = false;
            // }
        }
                
        AppointmentGrService.getPayment().then(function(res) {
            $scope.paymentData = res.data.Result;
            console.log("$scope.paymentData",$scope.paymentData);
        });

        function onFirstDataRendered(params){
            params.api.sizeColumnsToFit();
            setTimeout(function(){ params.api.getDisplayedRowAtIndex(1).setExpanded(true) }, 0);
        }
                

        function formatNumber(number) {
            return Math.floor(number)
            .toString()
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }
        function formatNumberDecimal(number) {
            return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }

        function actionRenderer2() {
            var actionHtml = ' <button ng-disabled="disJob" skip-enable class="ui icon inverted grey button"\
                                style="font-size:15px;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px"\
                                ng-click="deleteDataOplDetail(data)">\
                                <i class="fa fa-trash"></i>\
                            </button>';
            return actionHtml;
        }

        function customDate_OPL_Independent(param){
            console.log('param customDate_OPL_Independent ===>',param.value);
            console.log('param customDate_OPL_Independent ===>',typeof param.value);


            if(param.value == null){
                return '-';
            }else{
                if(param.value !== ""){
                    if(typeof param.value == 'object'){
                        var date = new Date(param.value);
                        console.log('date parsin ===>',date);
                        var thn = date.getFullYear();
                        var bln = ('0' + (date.getMonth()+1)).slice(-2);
                        var tgl = ('0' + date.getDate()).slice(-2);
                        var result = '' + tgl +'-'+  bln +'-'+ thn +'';
                        console.log('thn --->',thn);
                        console.log('bln --->',bln);
                        console.log('tgl --->',tgl);
                        console.log('result parsing --->',result)
                        return result
                    }else if (typeof param.value == 'string'){
                        console.log('date normal ===>',date);
                        var date = param.value.split('-');
                        var thn = date[0];
                        var bln = date[1];
                        var tgl = date[2];
                        var result = '' + tgl +'-'+  bln +'-'+ thn +'';
                        console.log('thn --->',thn);
                        console.log('bln --->',bln);
                        console.log('tgl --->',tgl);
                        console.log('result normal--->',result)
                        return result
                    }else{
                        return  '-';
                    }
                }else{
                    return '-';
                }
            }

            
        }

        // ag grid detail
        $scope.gridDetailOPLlist_IndependentApi;
        $scope.gridDetailOPLlist_Independent = {
            //DEFAULT AG-GRID OPTIONS
            // default ColDef, gets applied to every column
            defaultColDef: {
                width: 150,
                editable: false,
                filter: false,
                sortable: true,
                resizable: true
            },
            rowSelection: 'multiple',
            suppressRowClickSelection: true,
            suppressCellSelection: true,
            angularCompileRows: true,
            components: {
                // OPLEditor_Independent: OPLEditor_Independent,
                DateEditorFinishDate_Independent : DateEditorFinishDate_Independent,
                DateEditorEstimasi_Independent : DateEditorEstimasi_Independent,
            //     PartNameEditor : PartNameEditor,
                numericCellEditor : NumericCellEditor,
            //     checkboxEditor: agCheckboxCellEditor
            },
            onGridReady: function (params) {
                // params.api.sizeColumnsToFit();
                $scope.gridDetailOPLlist_IndependentApi = params.api;

                // var allColumnIds = [];
                // $scope.GridParts.columnApi.getAllColumns().forEach(function(column) {
                //     allColumnIds.push(column.colId);
                // });

                // $scope.GridParts.columnApi.autoSizeColumns(allColumnIds, false);

            },
            

            rowData: $scope.dataGridDetail,
            onCellEditingStopped: function (e) {
                console.log('parts cellEditingStopped=>', e.rowIndex, e.colDef.field, e.value, e.node, e);
                // if(e.colDef.field == 'PartCode'){
                // if($scope.disJob == false){
                    $scope.onAfterCellEditAg(e.rowIndex,e.colDef.field,e.value,e.node);
                // }
                // }
            },

            

            sideBar: {
                    toolPanels: ['columns'],
                    hiddenByDefault: false
            },
            toolPanelSuppressSideButtons:true,
        
            
            columnDefs: [

                { headerName: 'No.', field: 'No', width:60},
                { headerName: 'Nama Pekerjaan*', field: 'OPLWorkName', width: 350,  editable: false},
                { headerName: 'Pembayaran*', field: 'PaidBy', width: 113, editable: false}, 
                { headerName: 'Qty*', field: 'QtyPekerjaan', width: 65, editable: false, cellEditor: 'numericCellEditor'},     
                { headerName: 'Harga Beli*', field: 'OPLPurchasePrice', width: 121,  editable: false, suppressSizeToFit:true, cellEditor: 'numericCellEditor' , cellStyle: {textAlign: "right"},
                            cellRenderer: function(params){ 
                                var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
                                var notOk = '<span style="float:left">Rp.</span>'+ 0;
                                if(params.value ==  null || params.value ==  undefined || params.value == "" ){
                                    return notOk;
                                } else{
                                    return isOke;
                                }
                            }
                },   
                { headerName: 'Harga Jual*', field: 'Price', width: 121,  editable: false, suppressSizeToFit:true, cellEditor: 'numericCellEditor' , cellStyle: {textAlign: "right"},
                            cellRenderer: function(params){ 
                                var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
                                var notOk = '<span style="float:left">Rp.</span>'+ 0;
                                if(params.value ==  null || params.value ==  undefined || params.value == "" ){
                                    return notOk;
                                } else{
                                    return isOke;
                                }
                            }
                },      
                { headerName: 'Diskon', field: 'Discount', width: 78, editable: false,suppressSizeToFit:true,cellEditor: 'numericCellEditor' , cellStyle: {textAlign: "right"},
                            cellRenderer: function(params){ 
                                var isOke = formatNumberDecimal(params.value)+'<span style="float:right">%</span>';
                                var notOk = 0+'<span style="float:right">%</span>';
                                if(params.value ==  null || params.value ==  undefined || params.value == "" ){
                                    return notOk;
                                } else{
                                    return isOke;
                                }
                            }
                },     
                { headerName: 'Estimasi Selesai*', field: 'TargetFinishDate', width: 150, editable: false, cellEditor:'DateEditorFinishDate_Independent', cellRenderer: customDate_OPL_Independent}, 
                { headerName: 'Penanggung Jawab', field: 'ReceivedBy', width: 200, editable: false},
                { headerName: 'No. Vendor Invoice', field: 'VendorInvoiceNo', width: 190, editable: false},
                { headerName: 'Tgl. Vendor Invoice', field: 'VendorInvoiceDate', width: 162, editable: false, cellEditor:'DateEditorEstimasi_Independent', cellRenderer: customDate_OPL_Independent}, 
                { headerName: 'Tanggal Selesai', field: 'FinishDate', width: 142, editable: false, cellEditor:'DateEditorTanggalSelesai_Independent', cellRenderer: customDate_OPL_Independent},
                { headerName: 'Notes', field: 'Notes', width: 150, editable: false},
                { headerName: 'Status', field: 'isDeletedDesc', width: 150, editable: false},
                { headerName: 'Action',width: 90, minWidth:100, maxWidth:100, suppressFilter: true, suppressMenu:true, cellRenderer: actionRendererTaskOPLList}

            ]
        };
        $scope.disCheckButton = true;
       
        
        $scope.clickCheck = function(data){
            console.log("data",data);
			console.log('action checklist pressed');
            console.log("$scope.dataGridDetail",$scope.dataGridDetail);
            $scope.mOPLdata.FinishDate = new Date();
            console.log(" $scope.mOPLdata.FinishDate",$scope.mOPLdata);
            for(var a in $scope.dataGridDetail){
                if($scope.dataGridDetail[a].OPLId == data.OPLId){
                    $scope.dataGridDetail[a].FinishDate = $scope.mOPLdata.FinishDate;
                }
            }
            $scope.setDataForAGgrid($scope.dataGridDetail,false);
        }

        $scope.calenderPosition = function(){
            console.log('jalakno calendar');
            var cols = document.getElementsByClassName('ag-root-wrapper ag-layout-normal ag-ltr');
            for(i = 0; i < cols.length; i++) {
                cols[i].style.overflow = 'auto';
            }
        }

        $scope.gridEditOPLlist_Independent = {
            //DEFAULT AG-GRID OPTIONS
            // default ColDef, gets applied to every column
            defaultColDef: {
                width: 150,
                editable: false,
                filter: false,
                sortable: true,
                resizable: true
            },
            rowSelection: 'multiple',
            suppressRowClickSelection: true,
            overlayNoRowsTemplate: '<h1 style="font-size:3em; opacity:.25";position:absolute;top:4rem;width:100%;text-align:center;z-index:1000;>No data available</h1>',
            overlayLoadingTemplate: '<h1 style="font-size:3em; opacity:.25";position:absolute;top:4rem;width:100%;text-align:center;z-index:1000;>No data available</h1>',
            suppressCellSelection: true,
            angularCompileRows: true,
            components: {
                // OPLEditor_Independent: OPLEditor_Independent,
                DateEditorFinishDate_Independent : DateEditorFinishDate_Independent,
                DateEditorEstimasi_Independent : DateEditorEstimasi_Independent,
            //     PartNameEditor : PartNameEditor,
                numericCellEditor : NumericCellEditor,
            //     checkboxEditor: agCheckboxCellEditor
            },
            onGridReady: function (params) {
                // params.api.sizeColumnsToFit();
                $scope.gridDetailOPLlist_IndependentApi = params.api;

                // var allColumnIds = [];
                // $scope.GridParts.columnApi.getAllColumns().forEach(function(column) {
                //     allColumnIds.push(column.colId);
                // });

                // $scope.GridParts.columnApi.autoSizeColumns(allColumnIds, false);
                $scope.calenderPosition();
            },
            

            rowData: $scope.dataGridDetail,
            onCellEditingStopped: function (e) {
                console.log('parts cellEditingStopped=>', e.rowIndex, e.colDef.field, e.value, e.node, e);
                // if(e.colDef.field == 'PartCode'){
                // if($scope.disJob == false){
                    
                    if(e.colDef.field == "Notes"){

                      var value = e.value.substr(0,200);
                      e.node.setDataValue(e.column.colId, value);
                      console.log('cellEditingStopped notes');
                    }

                    if(e.colDef.field == "ReceivedBy"){

                      var value = e.value.substr(0,50);
                      e.node.setDataValue(e.column.colId, value);
                      console.log('cellEditingStopped penanggung jawab');
                    }

                    if(e.colDef.field == "VendorInvoiceNo"){

                      var value = e.value.substr(0,25);
                      e.node.setDataValue(e.column.colId, value);
                      console.log('cellEditingStopped VendorInvoiceNo');
                    }
                    $scope.onAfterCellEditAg(e.rowIndex,e.colDef.field,e.value,e.node);
                // }
                // }
            },

            

            sideBar: {
                    toolPanels: ['columns'],
                    hiddenByDefault: false
            },
            toolPanelSuppressSideButtons:true,
        
            
            columnDefs: [

                { headerName: 'No.', field: 'No', width:60},
                { headerName: 'Nama Pekerjaan*', field: 'OPLWorkName', width: 350,  editable: false},
                { headerName: 'Pembayaran*', field: 'PaidBy', width: 113, editable: false}, 
                { headerName: 'Qty*', field: 'QtyPekerjaan', width: 65, editable: false, cellEditor: 'numericCellEditor'},     
                { headerName: 'Harga Beli*', field: 'OPLPurchasePrice', width: 110,  editable: 
                    function(params) {

                        console.log("params",params);
                        if(params.data.Status == 1 || params.data.Status == 0){//open & progress true
                            if(params.data.isExternal == 1){//kalo eksternal false
                                // if($scope.GateInOuts.GateInOut == 0){

                                //      return false   
                                // }else{
                                    return true 
                                // }
                            }else{
                                return true 

                            }
                        }else{
                            return false
                        }

                    }, suppressSizeToFit:true, cellEditor: 'numericCellEditor' , cellStyle: {textAlign: "right"},
                    cellRenderer: function(params){ 
                                var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
                                var notOk = '<span style="float:left">Rp.</span>'+ 0;
                                if(params.value ==  null || params.value ==  undefined || params.value == "" ){
                                    return notOk;
                                } else{
                                    return isOke;
                                }
                    }
                },   
                { headerName: 'Harga Jual*', field: 'Price', width: 110,  editable: 
                    function(params) {

                        console.log("params",params);

                        if(params.data.Status == 1 || params.data.Status == 0){//open & progress true
                            if(params.data.isExternal == 1){//kalo eksternal false
                                // if($scope.GateInOuts.GateInOut == 0){

                                //      return false   
                                // // }else{
                                    return true 
                                // }
                            }else{
                                return true 
                            }
                        }else{
                            return false
                        }


                    }, suppressSizeToFit:true, cellEditor: 'numericCellEditor' , cellStyle: {textAlign: "right"},
                    cellRenderer: function(params){ 
                                var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
                                var notOk = '<span style="float:left">Rp.</span>'+ 0;
                                if(params.value ==  null || params.value ==  undefined || params.value == "" ){
                                    return notOk;
                                } else{
                                    return isOke;
                                }
                    }
                },      
                { headerName: 'Diskon', field: 'Discount', width: 78, editable: 
                    function(params) {

                        if(params.data.Status == 1 || params.data.Status == 0){//open & progress true
                            if(params.data.isExternal == 1){//kalo eksternal false
                                // if($scope.GateInOuts.GateInOut == 0){

                                //      return false   
                                // }else{
                                    return true
                                // }
                            }else{
                                return true
                            }
                        }else{
                            return false
                        }


                    },suppressSizeToFit:true,cellEditor: 'numericCellEditor' , cellStyle: {textAlign: "right"},
                    cellRenderer: function(params){ 
                                var isOke = formatNumberDecimal(params.value)+'<span style="float:right">%</span>';
                                var notOk = 0+'<span style="float:right">%</span>';
                                if(params.value ==  null || params.value ==  undefined || params.value == "" ){
                                    return notOk;
                                } else{
                                    return isOke;
                                }
                    }
                },     
                { headerName: 'Estimasi Selesai*', field: 'TargetFinishDate', width: 150, editable: 
                    function(params) {

                        console.log("params",params);
                        if(params.data.Status == 1 || params.data.Status == 0){//open & progress true
                            if(params.data.isExternal == 1){//kalo eksternal false
                                // if($scope.GateInOuts.GateInOut == 0){

                                //      return false   
                                // }else{
                                    return true
                                // }
                            }else{
                                return true
                            }
                        }else{
                            return false
                        }
                    }, cellEditor:'DateEditorFinishDate_Independent', cellRenderer: customDate_OPL_Independent
                }, 

                { headerName: 'Penanggung Jawab*', field: 'ReceivedBy', width: 200, editable: 
                    function(params) {
                        console.log("ini apaan sih ", params);
                        console.log("dia kesini gak ya ?",params.data.Status);
                        // return $scope.mOPLdata.Status == 0;
                        if(params.data.Status == 0){
                            return true
                        }else{
                            return false
                        }
                    }
                },
                { headerName: 'No. Vendor Invoice*', field: 'VendorInvoiceNo', width: 177, editable:
                    function(params) {

                        console.log("params",params);
                        if($scope.mOPLdata.Status == 1){

                             if(params.data.Status == 1){
                                 if(params.data.isExternal == 1) {
                                     if(typeof $scope.GateInOuts.GateInOut == "object"){

                                          return true   
                                     }else{
                                         return false
                                     }
                                 }else{
                                     return true
                                 }
                             }else{
                                 return false
                             }   
                        }else{
                            return false
                        }
                    }
                },
                { headerName: 'Tgl. Vendor Invoice*', field: 'VendorInvoiceDate', width: 160, editable:
                    function(params) {
                        console.log("params",params);
                        if($scope.mOPLdata.Status == 1){

                            if(params.data.Status == 1){
                                if(params.data.isExternal == 1){
                                    if(typeof $scope.GateInOuts.GateInOut == "object"){

                                         return true   
                                    }else{
                                        return false
                                    }
                                }else{
                                  return true  
                                } 
                            }else{
                                return false
                            }
                        }else{
                            return false
                        }
                    }
                , cellEditor:'DateEditorEstimasi_Independent', cellRenderer: customDate_OPL_Independent}, 
                { headerName: 'Tanggal Selesai', field: 'FinishDate', width: 140, editable: false, cellEditor:'DateEditorTanggalSelesai_Independent', cellRenderer: customDate_OPL_Independent},

                { headerName: 'Notes', field: 'Notes', width: 150, editable: 
                    function(params) {
                        console.log("params",params);
                        if(params.data.Status == 0){
                            return true
                        }else{
                            return false
                        }
                    }
                },
                { headerName: 'Status', field: 'isDeletedDesc', width: 150, editable: false},
                { headerName: 'Action',width: 90, minWidth:100, maxWidth:100, suppressFilter: true, suppressMenu:true, cellRenderer: actionRendererTaskOPLList, editable: 
                    function(params) {
                        // console.log("ini apaan sih ", params);
                        console.log("param for action",params);
                        $scope.stopediting();
                        
                        if(params.data.Status == 1){
                            if(params.data.isExternal == 1){
                                if(typeof $scope.GateInOuts.GateInOut == "object"){

                                     return true   
                                }else{
                                    return false
                                }
                            }else{
                                if(params.data.VendorInvoiceDate == null || params.data.VendorInvoiceNo == null || params.data.Status == 2){
                                    return false;
                                }else{
                                    return true;
                                }
                            }
                        }else{
                            return false
                        }


                        
                    }
                }
            ]
        };
        $scope.stopediting = function(){
            
            setTimeout(function () {
                console.log("masuk stop editing");
                $scope.gridDetailOPLlist_IndependentApi.stopEditing();
                
            }, 0);
        }

        function actionRendererTaskOPLList(data){
            console.log("data wew",data);
            // if(node.data.VendorInvoiceNo == null || node.data.VendorInvoiceDate == null || node.data.VendorInvoiceNo == "" || node.data.VendorInvoiceDate == "" ){
            $scope.DisCheck = false;
            if($scope.xmode == "view"){
                $scope.DisCheck = true;
            }else{
                $scope.DisCheck = false;
            }
            // 
            var actionHtml =' <button title="Complete Menampilkan Tanggal Selesai" class="ui icon inverted grey button" ng-disabled="DisCheck ||data.VendorInvoiceNo == null || data.VendorInvoiceDate == null || data.Status == 2"\
                                style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px"\
                                onclick="this.blur()"\
                                ng-click="clickCheck(data)">\
                                <i class="fa fa-check-circle-o"></i>\
                            </button>';
            return actionHtml;
        }

        //numeric editornya AG GRID
        // function to act as a class
        function NumericCellEditor() {
        }

        // gets called once before the renderer is used
        NumericCellEditor.prototype.init = function (params) {
            // create the cell
            this.eInput = document.createElement('input');

            if (isCharNumeric(params.charPress)) {
                this.eInput.value = params.charPress;
            } else {
                if (params.value !== undefined && params.value !== null) {
                    this.eInput.value = params.value;
                }
            }

            var that = this;
            this.eInput.addEventListener('keypress', function (event) {
                if (!isKeyPressedNumeric(event)) {
                    if (that.eInput.value.includes('.') || (event.key === '.' && (that.eInput.value === '' || that.eInput.value === null || that.eInput.value === undefined))){
                        that.eInput.focus();
                        event.preventDefault();
                    } else {
                        that.eInput.focus();
                        if (event.preventDefault && event.key !== '.'){
                            event.preventDefault();
                        } 
                    }
                } else if (that.isKeyPressedNavigation(event)){
                    event.stopPropagation();
                }
            });

            // only start edit if key pressed is a number, not a letter
            var charPressIsNotANumber = params.charPress && ('1234567890'.indexOf(params.charPress) < 0);
            this.cancelBeforeStart = charPressIsNotANumber;
        };

        NumericCellEditor.prototype.isKeyPressedNavigation = function (event){
            return event.keyCode===39
                || event.keyCode===37;
        };


        // gets called once when grid ready to insert the element
        NumericCellEditor.prototype.getGui = function () {
            return this.eInput;
        };

        // focus and select can be done after the gui is attached
        NumericCellEditor.prototype.afterGuiAttached = function () {
            this.eInput.focus();
        };

        // returns the new value after editing
        NumericCellEditor.prototype.isCancelBeforeStart = function () {
            return this.cancelBeforeStart;
        };

        // example - will reject the number if it contains the value 007
        // - not very practical, but demonstrates the method.
        NumericCellEditor.prototype.isCancelAfterEnd = function () {
            var value = this.getValue();
            return value.indexOf('007') >= 0;
        };

        // returns the new value after editing
        NumericCellEditor.prototype.getValue = function () {
            return this.eInput.value;
        };

        // any cleanup we need to be done here
        NumericCellEditor.prototype.destroy = function () {
            // but this example is simple, no cleanup, we could  even leave this method out as it's optional
        };

        // if true, then this editor will appear in a popup 
        NumericCellEditor.prototype.isPopup = function () {
            // and we could leave this method out also, false is the default
            return false;
        };
        //numeric editornya AG GRID



 

        // CR4 Phase2


    });

app.filter('rupiahC', function() {
    return function(val) {
        console.log("gggttt",val);
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
            console.log("tostringgggg",val);
        }
        return val;
    };
});