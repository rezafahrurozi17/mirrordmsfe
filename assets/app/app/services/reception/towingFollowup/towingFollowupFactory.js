angular.module('app')
    .factory('TowingFU', function($http, CurrentUser, $q) {

        return {
            getData: function(filter) {
                return $http.get('/api/as/Towings/FollowUpDetail/' + filter);
                // return $
            },
            update: function(data) {
                console.log('data', data);
                return $http.put('/api/as/Towings/FUTowingUpdate', [{
                    TowingId: data.TowingId,
                    VehicleModelId: data.VehicleModelId,
                    ModelName: data.VehicleModelName,
                    PoliceNumber: data.PoliceNumber,
                    EntryDate: data.EntryDate,
                    OwnerName: data.OwnerName,
                    OwnerDetail: data.OwnerDetail,
                    OwnerPhone: data.OwnerPhone,
                    Status: data.Status,
                    FollowUpDate: data.newFUDate
                }]);
            }
        }
    })