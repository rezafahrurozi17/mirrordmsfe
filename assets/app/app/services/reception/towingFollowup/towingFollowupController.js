angular.module('app')
    .controller('TowingFollowUpController', function($scope, $q, $http, CurrentUser, TowingFU, $timeout, bsAlert) {

        //STATUS FOLLOWUP TOWING & TOWING
        // 0 = OPEN
        // 1 = COMPLETED
        // 2 = CANCEL
        // 3 = ON PROGRESS
        // 4 = CLOSE
        /////--------------------------------
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
        });
        $scope.mData = [];
        $scope.formApi = {};
        $scope.selectedRows = [];
        $scope.showTowing = true;
        $scope.getData = function() {
            var filter = $scope.towingType;
            return $q.resolve(
                TowingFU.getData(filter).then(function(resu) {
                    $scope.loading = false;
                    console.log(resu.data);
                    var temp = [];
                    _.forEach(resu.data, function(e) {
                        var obj = {};
                        // if (e.Status == 0) {
                        //     e.StatusName = 'Belum Follow Up';
                        // } else if (e.Status == 1) {
                        //     e.StatusName = 'Sudah Follow Up';
                        // } else if (e.Status == 2) {
                        //     e.StatusName = 'Cancel / Batal';
                        // } else if (e.Status == 3) {
                        //     e.StatusName = 'On Progress';
                        // };

                        //STATUS FOLLOWUP TOWING & TOWING
                        // 0 = OPEN
                        // 1 = COMPLETED
                        // 2 = CANCEL
                        // 3 = ON PROGRESS
                        // 4 = CLOSE
                        /////--------------------------------

                        if (e.Status == 0) {
                            e.StatusName = 'Open';
                        } else if (e.Status == 3) {
                            e.StatusName = 'On Progress';
                        } else if (e.Status == 1) {
                            e.StatusName = 'Completed';
                        } else if (e.Status == 4) {
                            e.StatusName = 'Close';
                        } else if (e.Status == 2) {
                            e.StatusName = 'Cancel';
                        };
                        temp.push(e);
                    });
                    $scope.grid.data = temp;
                    console.log('$scope.grid.data', $scope.grid.data);
                })
            );
        };

        $scope.onSelectRows = function(rows) {
            $scope.dataRows = rows;
        };

        var dateFormat = 'dd/MM/yyyy';
        var dateFormat2 = 'dd-MM-yyyy';
        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };

        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat2,
            //disableWeekend: 1
        };

        $scope.maxDateOption = {
            maxDate: new Date()
        };

        $scope.stsFU = [{
            Id: 0,
            Name: 'Open'
                // Name: 'Belum Follow Up'
        }, {
            Id: 3,
            Name: 'On Progress'
        }, {
            Id: 1,
            Name: 'Completed'
                // Name: 'Sudah Follow Up'
        }, {
            Id: 2,
            Name: 'Cancel / Batal'
        }];

        $scope.onChangeSFU = function(item) {
            console.log('item status', item);
        };

        $scope.itemCall = {};
        $scope.show_modal = { show: false };
        $scope.modal_model = [];
        $scope.buttonSettingModal = { save: { text: 'OK' } };
        $scope.gridClickCall = function(item) {
            console.log('klik call', item);
            $scope.show_modal.show = !$scope.show_modal.show;
            $scope.modal_model = angular.copy(item);
            $scope.goCall = "Tel:" + $scope.modal_model.OwnerPhone;
            console.log('klik goCall', $scope.goCall);

        };
        $scope.goodCall = function() {
            $scope.show_modal.show = !$scope.show_modal.show;
        };
        $scope.cancelCall = function() {

        };
        $scope.actView = function(row, mode) {
            console.log('klik actView', row, mode);
            $scope.mData = row;
            $scope.formApi.setMode('view');
            $scope.showTowing = false;
            $scope.setModeButon(false);
        };

        $scope.actEdit = function(row, mode) {
            console.log('klik actEdit', row, mode);
            $scope.mData = row;
            $scope.formApi.setMode('detail');
            $scope.setModeButon(true);
            $scope.showTowing = false;
        };
        $scope.buttonSettings = [{
                actionType: 'back',
                title: 'Kembali',
                // icon: 'fa fa-fw fa-print',
                //rightsBit: 1
            },
            {
                func: function(row, formScope) {
                    var a = new Date(row.FollowUpDate);
                    var yearFirst = a.getFullYear();
                    var monthFirst = a.getMonth() + 1;
                    var dayFirst = a.getDate();
                    var followUpDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                    row.newFUDate = followUpDate;
                    console.log("update", row);
                    if(row.FollowUpDate == "" || row.FollowUpDate == null || row.FollowUpDate == " "){
                        bsAlert.warning("Tanggal Follow Up Belom di isi");
                        return false;
                    }
                    TowingFU.update(row).then(function(resu) {
                        // if (resu.data == -1) {
                        //     bsAlert.warning("Kendaraan dengan No. Polisi", "silahkan cek kembali");

                        // } else {
                        console.log('update status Print >>', resu.data);
                        $scope.formApi.setMode('grid');
                        $scope.getData();
                        // }

                    });
                },
                title: 'Update',
                // icon: 'fa fa-fw fa-print',
                //rightsBit: 1
            }
        ];
        $scope.setModeButon = function(param) {
            _.map($scope.buttonSettings, function(val) {
                val.visible = param;
            });
        };

        var gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
            // '<a href="#" uib-tooltip="Buat" tooltip-placement="bottom" ng-click="grid.appScope.actView(row.entity)" style="">Appointment</a>'+
            '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="View" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.gridClickCall(row.entity)" uib-tooltip="Call" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-phone" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.actEdit(row.entity)" ng-hide="row.entity.Status == 4" uib-tooltip="Edit" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableFiltering: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            paginationPageSize: 250,
            paginationPageSizes: [250, 500, 1000],
            columnDefs: [
                { name: 'TowingId', field: 'TowingId', width: '7%', visible: false },
                { name: 'Mobil', field: 'VehicleModelName' },
                { name: 'No Polisi', field: 'PoliceNumber' , cellFilter:'uppercase'},
                { name: 'Nama Pemilik', field: 'OwnerName' },
                { name: 'Telp', field: 'OwnerPhone' },
                { name: 'Status', field: 'StatusName' }, {
                    name: 'Action',
                    width: 120,
                    pinnedRight: true,
                    cellTemplate: gridActionButtonTemplate
                }
            ],
            onRegisterApi: function(gridApi) {
                $scope.gridApi = gridApi;
            }
        };
        $scope.filter = null;
        $scope.filterGrid = function(filter) {
            console.log('filter grid', filter);
            console.log('filter $scope.gridApi.grid.columns', $scope.gridApi.grid.columns);
            $scope.gridApi.grid.columns[3].filters[0].term = filter;
        };
        $scope.getTableHeight = function() {
            var rowHeight = 30; // your row height
            var headerHeight = 50; // your header height
            var filterHeight = 40; // your filter height
            var pageSize = $scope.grid.paginationPageSize;
            if ($scope.grid.paginationPageSize > $scope.grid.data.length) {
                pageSize = $scope.grid.data.length;
            };
            if (pageSize < 4) {
                pageSize = 3;
            };
            return {
                height: (pageSize * rowHeight + headerHeight) + 50 + "px"
            };
        };
        // $scope.gridActionTemplate = '<button class="ui icon inverted grey button"' +
        //     'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
        //     ' onclick="this.blur()" uib-tooltip="Call" tooltip-placement="bottom"' +
        //     ' ng-click="grid.appScope.gridClickCall(row.entity)">' +
        //     '<i class="fa fa-fw fa-lg fa-phone"></i>' +
        //     '</button>';

        $scope.dateChange = function(item) {
            console.log('item date >>', item);
        };
        $scope.optionTowing = [{
            Id: 0,
            Name: 'Open'
                // Name: 'Belum Follow Up'
        }, {
            Id: 3,
            Name: 'On Progress'
        }, {
            Id: 1,
            Name: 'Completed'
                // Name: 'Sudah Follow Up'
        }, {
            Id: 2,
            Name: 'Cancel / Batal'
        }, {
            Id: 4,
            Name: 'Close'
        }];
        $scope.selectDataTowing = function(data) {
            console.log('data select towing type', data);
            $scope.towingType = data;
        };
    });