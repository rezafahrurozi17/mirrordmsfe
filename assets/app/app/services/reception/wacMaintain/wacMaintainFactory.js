  angular.module('app')
      .factory('WacMaintain', function($http) {
          return {
              getWac: function() {
                  var res = $http.get('/api/as/GroupWACs');
                  // console.log("res", res);
                  return res;
              },
              getWacByJobId: function(WacId) {
                  var res = $http.get('/api/as/ItemWACs/GetByGroupId/' + WacId + '');
                  // console.log("res", res);
                  return res;
              },
              create: function(obj) {
                  console.log(obj);
                  return $http.post('/api/as/ItemWACs', [obj]);
              },
              update: function(obj) {
                  console.log(obj);
                  return $http.put('/api/as/ItemWACs/Update/', [obj]);
              },
              delete: function(data) {
                console.log("data delete",data);
                return $http.put('/api/as/ItemWACs/Delete/', [{
                    ItemId: data
                }]);
            }
          }
      });