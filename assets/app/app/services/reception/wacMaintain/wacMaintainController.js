angular.module('app')
    .controller('WacMaintainController', function($scope, $http, CurrentUser, WacMaintain, $timeout, bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mGeneralMaster = {}; //Model
    $scope.xGeneralMaster = {};
    $scope.xGeneralMaster.selected=[];
    $scope.masterCategories = [];
    $scope.filter = {MasterCategoryId:0};
    //----------------------------------
    // Get Data
    //----------------------------------
        WacMaintain.getWac().then(
            function(res){
                console.log("dara bang amik", res.data.Result);
                $scope.masterCategories = res.data.Result;
                $scope.loading=false;            
            }
        )

    $scope.selectCategory = function(data){
        console.log("data", data);
    }

    $scope.getData = function() {

        if($scope.filter.Id>0){
            WacMaintain.getWacByJobId($scope.filter.Id).then(
                function(res){
                    console.log("data item",res.data.Result);
                    $scope.grid.data = res.data.Result;
                    $scope.loading=false;
                    return res.data.Result;
                },
                function(err){
                    console.log("err=>",err);
                }
            );
        }else{
            $scope.grid.data = [];
            bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "Mohon Input Filter",
                // content: error.join('<br>'),
                // number: error.length
            });
        }
    }

    $scope.doCustomSave = function(model,mode){
        var newModel = angular.copy(model);
       console.log("mode",mode);
       if($scope.filter.Id < 0){
            console.log("kosong");
       }else{
            if(mode=='create'){
                newModel.GroupId = $scope.filter.Id;
                newModel.ItemName = $scope.mGeneralMaster.ItemName;
                console.log("id",newModel.GroupId);
                WacMaintain.create(newModel).then(
                    function(res){

                    },
                    function(err){
                        console.log("err=>",err);
                    }
                );
                
            }else{
                newModel.ItemId = $scope.mGeneralMaster.ItemId;
                newModel.ItemName = $scope.mGeneralMaster.ItemName;
                newModel.GroupId = $scope.filter.Id;
                WacMaintain.update(newModel).then(
                    function(res){
                        $scope.getData(newModel.GroupId);
                    },
                    function(err){
                        console.log("err=>",err);
                    }
                );
            }
            $scope.getData(newModel.GroupId);
        }
        $scope.getData(newModel.GroupId);
    }

    $scope.onSelectRows = function(rows){
        console.log("data",rows);
    }

    $scope.onBeforeDelete = function(rows) {
        console.log("itemnya", rows, rows[0].ItemId);
        for(var x=0;x<rows.length;x++){
            console.log("data data", rows[x].ItemId)
            WacMaintain.delete(rows[x].ItemId).then(function() {  
            });
            $scope.getData(rows[x].GroupId);
        }
        
    };

    $scope.onShowDetail = function(rows){
        console.log("queue",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        columnDefs: [
            { name:'ItemId', field:'ItemId', visible:false },
            { name:'Name', field: 'ItemName' },
        ]
    };

});
