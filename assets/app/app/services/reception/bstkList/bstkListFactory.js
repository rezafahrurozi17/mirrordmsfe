angular.module('app')
    .factory('BstkList', function($http, $filter, CurrentUser, $q) {
        var currUser = CurrentUser.user();
        console.log(currUser);
        return {
            getDataMasterDiscountBooking: function() {
                var res = $http.get('/api/as/MAppointmentDiscount/GetAktif/?start=1&limit=100000');                
                return res;
            },
            getData: function() {
                var res = $http.get('/api/as/Vehiclefare');
                //console.log('res=>',res);
                //res.data.Result = null;
                return res;
            },
            changeFormatDate: function(item) {
                var tmpDate = item;
                tmpDate = new Date(tmpDate);
                var finalDate
                var yyyy = tmpDate.getFullYear().toString();
                var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                var dd = tmpDate.getDate().toString();
                finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                console.log("changeFormatDate finalDate", finalDate);
                return finalDate;
            },
            FormatingTimeZone: function (data){
                var pecahData = data.toString().split(' ');
                console.log('pecah data', pecahData);
                // Jan:0, Feb:0, Mar:0, Apr:0, May:0, Jun:0, Jul:0, Aug:0, Sep:0, Oct:0, Nov:0, Dec:0
                if (pecahData[1] == 'Jan'){
                    pecahData[1] = '01'
                } else if (pecahData[1] == 'Feb'){
                    pecahData[1] = '02'
                } else if (pecahData[1] == 'Mar'){
                    pecahData[1] = '03'
                } else if (pecahData[1] == 'Apr'){
                    pecahData[1] = '04'
                } else if (pecahData[1] == 'May'){
                    pecahData[1] = '05'
                } else if (pecahData[1] == 'Jun'){
                    pecahData[1] = '06'
                } else if (pecahData[1] == 'Jul'){
                    pecahData[1] = '07'
                } else if (pecahData[1] == 'Aug'){
                    pecahData[1] = '08'
                } else if (pecahData[1] == 'Sep'){
                    pecahData[1] = '09'
                } else if (pecahData[1] == 'Oct'){
                    pecahData[1] = '10'
                } else if (pecahData[1] == 'Nov'){
                    pecahData[1] = '11'
                } else if (pecahData[1] == 'Dec'){
                    pecahData[1] = '12'
                } else {
                    pecahData[1] = '01'
                }

                var hasilFormat = pecahData[3] + '-' + pecahData[1] + '-' + pecahData[2] + ' ' + pecahData[4]
                return hasilFormat;
            },
            validationNonTaskList: function(data){
                console.log('cek data nontasklist aja', data)
                for (var i=0; i<data.length; i++){
                    if (data[i].TaskBPId !== null && data[i].TaskBPId !== undefined){
                        if (parseInt(data[i].TaskBPId) < 0){
                            data[i].TaskBPId = null
                        }
                    }
                }
                return data
            },
            create: function(data) {
                console.log('data create', data);
                return $http.put('/api/as/Jobs/updateDetil/2', [{
                    JobId: data.JobId,
                    JobTask: data.JobTask,
                    JobTaskOpl: data.OPL
                }]);
            },
            update: function(data) {
                console.log('data update', data);
                return $q.resolve();
                //     return $http.post('/api/as/Jobs/postWO', [{
                //         JobId: data.JobId,
                //         JobTask: data.JobTask
                //     }]);
            },
            delete: function(VehicleFareId) {
                return $http.delete('/api/as/Vehiclefare', {
                    data: VehicleFareId,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });
            },
            getWoListGr: function(paramType, paramDate1, paramDate2, ParamWO, ParamNoPolice, typeParam, tipeDefault) {

                if (ParamWO != null) {
                    var WoNo = ParamWO.replace(/\//g, '%20');
                    var woNoFinal = angular.copy(WoNo);
                    if (woNoFinal.includes("*")) {
                        woNoFinal = woNoFinal.split('*');
                        woNoFinal = woNoFinal[0];
                    }
                }

                if (ParamNoPolice != null) {
                    var PoliceNo = ParamNoPolice.replace(/\//g, '%20');
                    var PoliceNoFinal = angular.copy(PoliceNo);
                    if (PoliceNoFinal.includes("*")) {
                        PoliceNoFinal = PoliceNoFinal.split('*');
                        PoliceNoFinal = PoliceNoFinal[0];
                    }
                }

                if (typeParam == 0) {
                    if (currUser.RoleId == 1129 || currUser.RoleId == 1121) {
                        return $http.get('/api/as/jobs/BSTKListAktifSA/FirstDate/' + paramDate1 + '/LastDate/' + paramDate2 +
                            '/gr/0/jenisFilter/' + paramType + '/wono/' + WoNo + '/plateNo/' + ParamNoPolice + '/Approver/' + currUser.RoleId + '/isDefault/' + 1);
                    } else {
                        return $http.get('/api/as/jobs/BSTKListAktifSA/FirstDate/' + paramDate1 + '/LastDate/' + paramDate2 +
                            // '/gr/0/jenisFilter/' + paramType + '/wono/' + WoNo + '/plateNo/' + ParamNoPolice + '/Approver/' + 0 + '/isDefault/' + tipeDefault);
                            '/gr/0/jenisFilter/' + paramType + '/wono/' + WoNo + '/plateNo/' + ParamNoPolice + '/Approver/' + 0 + '/isDefault/' + 1);
                    }
                } else {
                    if (currUser.RoleId == 1129 || currUser.RoleId == 1142) {
                        return $http.get('/api/as/jobs/BSTKListAktifSA/FirstDate/' + paramDate1 + '/LastDate/' + paramDate2 +
                            '/gr/0/jenisFilter/' + paramType + '/wono/' + woNoFinal + '/plateNo/' + PoliceNoFinal + '/Approver/' + currUser.RoleId + '/isDefault/' + 1);
                    } else {
                        return $http.get('/api/as/jobs/BSTKListAktifSA/FirstDate/' + paramDate1 + '/LastDate/' + paramDate2 +
                            '/gr/0/jenisFilter/' + paramType + '/wono/' + woNoFinal + '/plateNo/' + PoliceNoFinal + '/Approver/' + 0 + '/isDefault/' + 1);
                    }
                }
                // if (currUser.RoleId == 1129 || currUser.RoleId == 1129) {
                //     return $http.get('/api/as/jobs/WoListAktifSA/FirstDate/' + paramDate1 + '/LastDate/' + paramDate2 +
                //         '/gr/0/jenisFilter/' + paramType + '/wono/' + ParamWO + '/plateNo/' + ParamNoPolice + '/Approver/' + currUser.RoleId);
                // }
                // return $http.get('/api/as/jobs/WoListAktifSA/FirstDate/' + paramDate1 + '/LastDate/' + paramDate2 +
                //     '/gr/0/jenisFilter/' + paramType + '/wono/' + ParamWO + '/plateNo/' + ParamNoPolice + '/Approver/' + 0);
            },
            getWObyJobId: function(id) {
                return $http.get('/api/as/Jobs/' + id);
            },
            getCategoryWO: function() {
                return $http.get('/api/as/GlobalMaster?CategoryId=1007');
            },
            getAllOPL: function() {
                return $http.get('/api/as/OPL');
            },
            closeWO: function(id) {
                return $http.put('/api/as/Jobs/CloseWo/', [id]);

            },
            updateApproval: function(data) {
                console.log("data di updateApproval", data);
                return $http.put('/api/as/RReception_ApprovalBPChangeDP_TR', [{
                    OutletId: currUser.OrgId,
                    TransactionId: data.TransactionId,
                    JobId: data.JobId,
                    DPDefault: data.DPDefault,
                    DPRequested: data.DPRequested,
                    ApprovalCategoryId: 42,
                    seq: 1,
                    ApproverId: null,
                    RequesterId: null,
                    ApproverRoleId: data.ApproverRoleId,
                    RequestorRoleId: data.RequestorRoleId,
                    StatusApprovalId: data.NewStatusApprovalId,
                    VehicleTypeId: data.VehicleTypeId,
                    Discount: 0,
                    RequestReason: data.RequestReason,
                    RequestDate: data.RequestDate,
                    RejectReason: data.Reason,
                    ApproveRejectDate: new Date(),
                    StatusCode: 1,
                }]);
            },
            GetAllApprovalJobReductionBP: function() {
                return $http.get('/api/as/GetAllApprovalJobReductionBP');
            },
            updateApprovalJobReduction: function(data) {
                console.log("dataaaaa", data);
                return $http.put('/api/as/RReception_ApprovalBPWORelese_TR', [{
                    OutletId: currUser.OrgId,
                    TransactionId: data.TransactionId,
                    JobId: data.JobId,
                    JobTaskId: data.JobTaskId,
                    ApprovalCategoryId: 50,
                    seq: data.seq,
                    ApproverId: null,
                    RequesterId: null,
                    ApproverRoleId: data.ApproverRoleId,
                    RequestorRoleId: data.RequestorRoleId,
                    StatusApprovalId: data.NewStatusApprovalId,
                    RequestReason: data.RequestReason,
                    RequestDate: data.RequestDate,
                    Reason: data.Reason,
                    ApproveRejectDate: new Date(),
                    StatusCode: 1,
                }]);
            },

            updateApprovalCancelWO: function (data) {
                console.log("data di updateApproval", data);
                return $http.put('/api/as/ApproveRejectCancelWO', [{
                    TransactionId: data.TransactionId,
                    JobId: data.JobId,
                    StatusApprovalId: data.NewStatusApprovalId,
                    Reason: data.Reason,
                    StatusCode: 1,
                }]);
            },

            getDataWACBP: function(jobid) {
                return $http.get('/api/as/JobWACPBs/getByJob/' + jobid);
            },
            updateWOBP: function(mData, mDataCrm, mDataDM, JobRequest, JobComplaint, JobList) {
                console.log("Factory mData ==================>", mData);
                // debugger;

                // ======= Added Code For Changing Format Date ======= u can use this code if have problem for changing format date
                // mData.AppointmentDate = this.changeFormatDate(mData.AppointmentDate);
                // mData.PlanDateStart =  this.changeFormatDate(mData.PlanDateStart);
                // mData.PlanDateFinish = this.changeFormatDate(mData.PlanDateFinish);
                // mData.WoCreatedDate = this.changeFormatDate(mData.WoCreatedDate);
                // mData.WoCreatedDate = $filter('date')(mData.WoCreatedDate, 'yyyy-MM-dd HH:mm:ss') // karena ini time zone naik turun.. susah
                // mData.WoCreatedDate = this.FormatingTimeZone(mData.WoCreatedDate)
                var sampleWoCreatedDate = angular.copy(mData.WoCreatedDate)
                sampleWoCreatedDate = this.FormatingTimeZone(sampleWoCreatedDate)
                
                // ini kalo timezonenya udah bisa hapus aja
                if(sampleWoCreatedDate == 'undefined-01-undefined undefined'){

                    sampleWoCreatedDate = angular.copy(mData.WoCreatedDate)
                }
                // ini kalo timezonenya udah bisa hapus aja
                
                // ini tambahan cr4 kalo ada salah dikit dikit dibenerin bisa kali
                for(var t in JobList){
                    if(JobList[t].EstimationBPId != undefined){
                        JobList[t].IsCustomDiscount = 0; 
                        if(JobList[t].JobParts != undefined){
                            for(var k in JobList[t].JobParts){
                                JobList[t].JobParts[k].IsCustomDiscount = 0;
                                if(JobList[t].JobParts[k].JobTaskId == null){

                                    JobList[t].JobParts[k].JobTaskId = 0;
                                }
                            }
                        }
                        // JobList[t].EstimationBPId ;
                        // JobList[t].EstimationBPId ;
                        // JobList[t].EstimationBPId ;
                    }
                }
                // ini tambahan cr4 kalo ada salah dikit dikit dibenerin bisa kali
                //mData.WoCreatedDate = $filter('date')(mData.WoCreatedDate, 'yyyy-MM-dd HH:mm:ss')
                // ===========================================
                mData.JobTask = this.validationNonTaskList(mData.JobTask)

                if (mData.VehicleModelName == null || mData.VehicleModelName == undefined){
                    mData.VehicleModelName = mDataCrm.Vehicle.ModelName
                }

                var appointmentdate2 = angular.copy(mData.AppointmentDate)
                var cektahun = appointmentdate2.getFullYear()
                var cekbulan = appointmentdate2.getMonth()
                var cektgl = appointmentdate2.getDate()

                if (cektahun == 1 && cekbulan == 0 && cektgl == 1){
                    appointmentdate2 = '0001-01-01'
                }
                

                return $http.put('/api/as/Jobs/updateDetilBSTK/4', [{
                    JobId: mData.JobId,
                    OutletId: mData.OutletId,
                    TechnicianAction: mData.TechnicianAction,
                    JobSuggest: mData.JobSuggest,
                    StallId: null,
                    // VehicleId: mDataCrm.Vehicle.VehicleId,
                    VehicleId: mData.VehicleId,
                    isGr: mData.isGr,
                    Km: mData.Km,
                    // EstimationDate: mData.EstimateDeliveryTime,
                    EstimationDate: $filter('date')(mData.EstimationDate, 'yyyy-MM-dd HH:mm:ss'),
                    WoCategoryId: mData.WoCategoryId,
                    ModelCode: mDataCrm.Vehicle.ModelCode,
                    // JobDate: mData.JobDate,
                    JobDate: $filter('date')(mData.JobDate, 'yyyy-MM-dd HH:mm:ss'),
                    ModelType: mData.VehicleModelName,
                    KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    VehicleTypeId: mData.VehicleTypeId,
                    PoliceNumber: mDataCrm.Vehicle.LicensePlate,
                    InsuranceName: mData.InsuranceName,
                    ActiveChip: mData.ActiveChip,
                    DecisionMaker: mDataDM.PK.Name ? mDataDM.PK.Name : null,
                    PhoneDecisionMaker1: mDataDM.PK.HP1 ? mDataDM.PK.HP1 : null,
                    PhoneDecisionMaker2: mDataDM.PK.HP2 ? mDataDM.PK.HP2 : null,
                    ContactPerson: mDataDM.CP.Name ? mDataDM.CP.Name : null,
                    PhoneContactPerson1: mDataDM.CP.HP1 ? mDataDM.CP.HP1 : null,
                    PhoneContactPerson2: mDataDM.CP.HP2 ? mDataDM.CP.HP2 : null,
                    WoCreatedDate: sampleWoCreatedDate,
                    JobCatgForBPid: mData.JobCatgForBPid,
                    UserIdSa: mData.UserIdSa,
                    JobTask: mData.JobTask,
                    JobComplaint: JobComplaint,
                    JobRequest: JobRequest,
                    VehicleConditionTypeId: mData.VehicleConditionTypeId,
                    BpSatelitOutletId: mData.BpSatelitOutletId,
                    isWoFromBpSatelit: mData.isWoFromBpSatelit,
                    WoNo: mData.WoNo,
                    BSTKNo: mData.BSTKNo,
                    UserIdSa: mData.UserIdSa,
                    UserIdApp: mData.UserIdApp,
                    UserIdWORelease: mData.UserIdWORelease,
                    BpSatelitSAUserId: mData.BpSatelitSAUserId,
                    GateInPushTime: $filter('date')(mData.GateInPushTime, 'yyyy-MM-dd HH:mm:ss'),
                    // AppointmentDate: mData.AppointmentDate,
                    AppointmentDate: appointmentdate2,
                    AppointmentNo: mData.AppointmentNo,
                    FinalDP: mData.FinalDP,
                    DPPaidAmount: mData.DPPaidAmount,
                    ORPaidAmount: mData.ORPaidAmount,
                    DPRawatJalan: mData.DPRawatJalan,
                    IsPaidRawatJalan: mData.IsPaidRawatJalan,


                }]);

                // yg ini ga kepake, ini copy an dr wo list bp
                return $http.put('/api/as/Jobs/updateDetil/2', [{
                    ActiveChip: mData.ActiveChip,
                    JobId: mData.JobId,
                    OutletId: mData.OutletId,
                    TechnicianAction: mData.TechnicianAction,
                    JobSuggest: mData.JobSuggest,
                    // JobTask: JobList,
                    JobTask: mData.JobTask,
                    // isGr: 1,
                    Km: mData.Km,
                    isCash: mData.isCash,
                    isSpk: mData.isSpk,
                    SpkNo: mData.SpkNo,
                    // IsEstimation: 0,
                    // EstimationDate: mData.EstimateDeliveryTime,
                    // WoCategoryId: mData.WOCategory,
                    JobDate: mData.JobDate,
                    // KatashikiCode: 'ASV50R-JETGKD',
                    KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    JobComplaint: JobComplaint,
                    JobRequest: JobRequest,
                    // DecisionMaker: mData.DecisionMaker,
                    // PhoneDecisionMaker1: mData.PhoneDecisionMaker1,
                    // PhoneDecisionMaker2: mData.PhoneDecisionMaker2,
                    // ContactPerson: mData.ContactPerson,
                    // PhoneContactPerson1: mData.PhoneContactPerson1,
                    // PhoneContactPerson2: mData.PhoneContactPerson2,
                    DecisionMaker: mDataDM.PK.Name ? mDataDM.PK.Name : null,
                    PhoneDecisionMaker1: mDataDM.PK.HP1 ? mDataDM.PK.HP1 : null,
                    PhoneDecisionMaker2: mDataDM.PK.HP2 ? mDataDM.PK.HP2 : null,
                    ContactPerson: mDataDM.CP.Name ? mDataDM.CP.Name : null,
                    PhoneContactPerson1: mDataDM.CP.HP1 ? mDataDM.CP.HP1 : null,
                    PhoneContactPerson2: mDataDM.CP.HP2 ? mDataDM.CP.HP2 : null,
                    UsedPart: mData.isParts,
                    IsWash: mData.isWashing,
                    ServiceWithVehicle: mData.isPerbaikan,
                    Status: mData.Status,
                    ModelType: mData.VehicleModelName,
                    // =======================
                    VehicleId: mData.VehicleId,
                    WoNo: mData.WoNo,
                    isWash: mData.isWash,
                    UsedParts: mData.UsedParts,
                    PoliceNumber: mDataCrm.Vehicle.LicensePlate,
                    JobCatgForBPid: mData.JobCatgForBPid,
                    WoCategoryId: mData.WoCategoryId,
                    // WoCreatedDate: mData.WoCreatedDate,
                    WoCreatedDate: sampleWoCreatedDate,
                    // GateInPushTime: mData.GateInPushTime,
                    GateInPushTime: $filter('date')(mData.GateInPushTime, 'yyyy-MM-dd HH:mm:ss'),
                    UserIdSa: mData.UserIdSa,
                    InsuranceId: mData.InsuranceId,
                    InsuranceName: mData.InsuranceName,
                    //   FixedDeliveryTime1: mData.FixedDeliveryTime1,
                    //   FixedDeliveryTime2: mData.FixedDeliveryTime2,
                    FixedDeliveryTime1: new Date(mData.EstimateDeliveryTime),
                    FixedDeliveryTime2: new Date(mData.AdjusmentDate),
                    VehicleTypeId: mData.VehicleTypeId,
                    UserIdWORelease: mData.UserIdWORelease,
                    // FinalDP: mData.FinalDP,
                    FinalDP: mData.isPerbaikan == 1 ? mData.FinalDP : mData.TotalSummaryPrice,
                    // ==============================
                    JobNo: mData.JobNo,
                    OutletId: mData.OutletId,
                    CalId: mData.CalId,
                    PlanStart: mData.PlanStart,
                    PlanFinish: mData.PlanFinish,
                    TechnicianAction: mData.TechnicianAction1,
                    T1: mData.T1,
                    JobType: mData.JobType,
                    AppointmentDate: $filter('date')(mData.AppointmentDate , 'yyyy-MM-dd'),
                    AppointmentTime: mData.AppointmentTime,
                    AppointmentNo: mData.AppointmentNo,
                    isAppointment: mData.isAppointment,
                    isGr: mData.isGr,
                    isCash: mData.isCash,
                    isSpk: mData.isSpk,
                    SpkNo: mData.SpkNo ? mData.SpkNo : '-',
                    InsuranceName: mData.InsuranceName,
                    isWOBase: mData.isWOBase,
                    // WoCategoryId: mData.WOCategory,
                    WoNo: mData.WoNo,
                    //=================================
                    IsEstimation: mData.IsEstimation,
                    // EstimationDate: mData.EstimateDeliveryTime,
                    EstimationDate: $filter('date')(mData.EstimationDate, 'yyyy-MM-dd HH:mm:ss'),
                    EstimationNo: mData.EstimationNo,
                    JobDate: mData.JobDate,
                    PermissionPartChange: mData.PermissionPartChange,
                    PaymentMethod: mData.PaymentMethod,
                    // DecisionMaker: mData.Name,
                    // PhoneDecisionMaker1: mData.HP1,
                    // PhoneDecisionMaker2: mData.HP2,
                    // ContactPerson: mData.Name,
                    // PhoneContactPerson1: mData.HP1,
                    // PhoneContactPerson2: mData.HP2,
                    UsedPart: mData.UsedPart,
                    IsWaiting: mData.IsWaiting,
                    IsWash: mData.IsWash,
                    JobTaskOpl: mData.JobTaskOpl,
                    invoiceType: mData.CodeTransaction,
                    ORAmount: mData.ORAmount,
                    MProfileForeman: mData.MProfileForeman,
                    MProfileForemanId: mData.MProfileForemanId,
                    ToyotaIdRequest: mDataCrm.Customer.ToyotaIDFlag == null ? 0 : mDataCrm.Customer.ToyotaIDFlag,
                    // ServWithoutVehicAmount: mData.TotalSummaryPrice
                }]);
            },

            getDiscountMGroupCustomer: function(data) {
                return $http.put('/api/crm/PutDiscountMGroupCustomer', [{
                    Name: data.CustomerName,
                    Phone1: data.Handphone1,
                    Phone2: data.Handphone2,
                    KtpNo: data.KTPKITAS
                }]);
            },

            GetStatusTowas: function(JobId) {
                var res = $http.get('/api/as/Jobs/GetStatusTowas/' + JobId);
                //console.log('res=>',res);
                //res.data.Result = null;
                return res;
            },

            CheckStatusJobBP: function(JobId) {
                return $http.get('/api/as/CheckStatusJobBp/' + JobId);
            },
            postJobReduction: function(jobId, data) {
                // api/as/PostJobReductionBP
                console.log("data di postJobReduction", data);
                return $http.post('/api/as/PostJobReductionBP', [{
                    JobId: jobId,
                    JobTasks: data
                }]);
            },
            ReqCancelWO: function (jobId) {
                console.log("data di ReqCancelWO", jobId);
                return $http.post('/api/as/PostApprovalCancelWO/0', [{
                    JobId: jobId,
                    StatusApprovalId: 3,
                    // JobTasks: data
                }]);
            },
            GetAllApprovalCancelWO: function (jobId) {
                console.log("data di ReqCancelWO", jobId);
                return $http.get('/api/as/GetAllApprovalCancelWO');
            },
            CheckIsJobCancelWO: function (jobId) {
                console.log("data di ReqCancelWO", jobId);
                return $http.get('/api/as/CheckIsJobCancelWO/'+jobId);
            },
            
            sendNotif: function(data, recepient, param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param: param
                }]);
            },
            sendNotifOne: function(data, recepient, Param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotification', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param: Param
                }]);
            },

            updateBPEstimation: function(jobId, arrTaskwithId) {
                for(var a in arrTaskwithId){
                    if(arrTaskwithId[a].Putty == null){
                        arrTaskwithId[a].Putty = 0;
                    }
                    if(arrTaskwithId[a].Body == null){
                        arrTaskwithId[a].Body = 0;
                    }
                    if(arrTaskwithId[a].Surfacer == null){
                        arrTaskwithId[a].Surfacer = 0;
                    }
                    if(arrTaskwithId[a].Spraying == null){
                        arrTaskwithId[a].Spraying = 0;
                    }
                    if(arrTaskwithId[a].Polishing == null){
                        arrTaskwithId[a].Polishing = 0;
                    }
                    if(arrTaskwithId[a].ReAssembly == null){
                        arrTaskwithId[a].ReAssembly = 0;
                    }
                    if(arrTaskwithId[a].FinalInspection == null){
                        arrTaskwithId[a].FinalInspection = 0;
                    }
                    console.log("arrTaskwithId[a]",arrTaskwithId[a]);
                }
                // return $http.put('/api/as/updateBPEstimation/' + jobId, [{
                return $http.put('/api/as/updateBPEstimationBSTK/' + jobId, [{
                    Jobtasks: arrTaskwithId,
                    // JobTaskBP: dataEst
                }])
            },
            updateApprovalDiskonParts: function(data) {
                console.log("data di updateApproval", data);
                return $http.put('/api/as/SAggrement_ApprovalDiscountPart_TR', [{
                    OutletId: currUser.OrgId,
                    // TransactionId: data.TransactionId,
                    TransactionId: data.ApprovalDiscountId,
                    JobId: data.JobId,
                    JobPartId: data.JobPartId,
                    ApprovalCategoryId: 46,
                    DiscountedPrice: data.DiscountedPrice,
                    NormalPrice: data.NormalPrice,
                    seq: data.Sequence,
                    ApproverId: null,
                    RequesterId: null,
                    ApproverRoleId: data.ApproverRoleId,
                    RequestorRoleId: data.RequestorRoleId,
                    StatusApprovalId: data.NewStatusApprovalId,
                    Discount: data.Discount,
                    RequestDate: data.RequestDate,
                    Reason: data.Reason,
                    ApproveRejectDate: new Date(),
                    StatusCode: 1,
                }]);
            },
            updateApprovalDiskonTasks: function(data) {
                console.log("data di updateApproval", data, 'api/as/ApprovalDiscount/ApproveDiscount');
                return $http.put('/api/as/SAggrement_ApprovalDiscount_TR', [{
                    // OutletId: currUser.OrgId,
                    OutletId: data.OutletId,
                    // TransactionId: data.TransactionId,
                    TransactionId: data.ApprovalDiscountId,
                    JobId: data.JobId,
                    JobTaskId: data.JobTaskId,
                    ApprovalCategoryId: 46,
                    DiscountedPrice: data.DiscountedPrice,
                    NormalPrice: data.NormalPrice,
                    seq: data.Sequence,
                    ApproverId: null,
                    RequesterId: null,
                    ApproverRoleId: data.ApproverRoleId,
                    RequestorRoleId: data.RequestorRoleId,
                    StatusApprovalId: data.NewStatusApprovalId,
                    Discount: data.Discount,
                    RequestDate: data.RequestDate,
                    Reason: data.Reason,
                    ApproveRejectDate: new Date(),
                    StatusCode: 1,
                }]);
            },
            getUserIdApprover: function(outletId, roleId) {
                return $http.get('/api/as/ApprovalDiscount/GetUserIdApprover', {
                    params: {
                        vRoleId: roleId,
                        vOutletId: outletId
                    }
                });
            },

            // rejectApprovalDiscountJob: function(jobId,data){
            //     console.log("dataaaaa",data);
            //     return $http.post('/api/as/RReception_ApprovalBPWORelese_TR',[{
            //         JobId : jobId,
            //         JobTask:data
            //     }]);
            // }

            //----------------------------- Pindah Data------------------------------

            getDataWac: function(JobId) {
                // console.log("JbId",JbId);
                var res = $http.get('/api/as/JobWACs/Job/' + JobId);
                // console.log('resnya=>',res);
                return res;
            },

            getProbPoints: function(JobId, TypeId) {
                if(TypeId == null){
                    TypeId = 0;
                }
                return $http.get('/api/as/JobWACExts/Job/' + JobId + '/' + TypeId);
            },

            getVinFromCRM: function(vin) {
                var res = $http.get('/api/crm/GetVehicleListById/' + vin + '');
                console.log('Get Data VIN=>', res);
                return res;
            },

            getQuestion: function() {
                return $http.get('/api/as/QuestionsManagement');
            },

            getT1: function() {
                // var catId = 2023;
                var res = $http.get('/api/as/GetT1');
                // console.log('resnya pause=>',res);
                return res;
            },

            getDataTask: function(taskName, vehicleTypeId, vehicleModelId) {
                console.log("name", taskName, vehicleTypeId, vehicleModelId);
                var res = $http.get('/api/as/TaskListBP/' + taskName + '/' + vehicleTypeId + '/' + vehicleModelId);
                // var res=$http.get('/api/as/TaskLists?KatashikiCode=ASV50R-JETGKD&Name='+Key+'&TaskCategory='+catg);
                // console.log('/api/as/TaskLists?KatashikiCode='+Katashiki+'&Name='+Key+'&TaskCategory='+catg);
                // console.log("resnya",res);
                return res;
            },

            getInsurence: function() {
                var res = $http.get('/api/as/Insurance');
                return res
            },

            getWoCategory: function() {
                var catId = 2039;
                // var catId = 1012;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId+'&Flag=1');
                console.log('resnya pause=>', res);
                return res;
            },

            getDataPartsByTaskId: function(key) {
                var res = $http.get('/api/as/TaskLists/Parts?TaskId=' + key);
                return res
            },

            getDataParts: function(Key, servicetype, type) {
                console.log("keyyyyyy", Key);
                // var res = $http.get('/api/as/StockAdjustment/GetStockAdjustmentDetailFromMaterial?PartsCode=' + Key + '&ServiceTypeId=' + servicetype + '&PartsClassId1=' + type);
                var res = $http.get('/api/as/StockAdjustment/GetStockAdjustmentDetailSateliteFromMaterial?PartsCode=' + Key + '&ServiceTypeId=' + servicetype + '&PartsClassId1=' + type);
                // console.log("/api/as/TaskLists?KatashikiCode="+Katashiki+"&Name="+Key);
                // console.log("resnya",res);
                return res;
            },

            getAvailableParts: function(item, jobid, qtyinput) {
                // /api/as/jobs/getAvailableParts/{partid}
                // var res = $http.get('/api/as/jobs/getPartsPriceAndDefaultETDETA/' + item);
                if (typeof item == 'string') {
                    if (item.includes('&')) {
                       item = item.split("&")[0];
                    }
                }
                // var res = $http.get('/api/as/jobs/getAvailablePartsAppointment/' + item + '/0?JobId='+jobid + '&Qty='+qtyinput);
                var res = $http.get('/api/as/jobs/getAvailablePartsSatelitAppointment/' + item + '/0?JobId='+jobid + '&Qty='+qtyinput);
                return res
            },

            getDataVehicle: function(key) {
                var res = $http.get('/api/crm/GetDataInfo/' + key);
                return res;
            },

            getDataVehiclePSFU: function(key) {
                var res = $http.get('/api/crm/GetVehicleListById/' + key);
                return res;
            },

            sendToCRMHistory: function(data) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/crm/InsertCHistoryDataChanged/', [{
                    CustomerId : data.CustomerId,
                    ChangeTypeId : data.ChangeTypeId,
                    Modul : data.Modul,
                    Attribute : data.Attribute,
                    NewValue : data.NewValue,
                    OldValue : data.OldValue
                }]);
            },

            getDataTaskListByKatashiki: function(key) {
                var res = $http.get('/api/as/TaskLists/TasklistSA?KatashikiCode=' + key);
                return res
            },

            getPayment: function() {
                var catId = 1008;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },

            getUnitMeasurement: function() {
                var catId = 1;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },

            getTaskCategory: function() {
                var catId = 1010;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },

            GetCAddressCategory: function() {
                var res=$http.get('/api/ct/GetCAddressCategory/');
                
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },

            getDataGMaster: function(catId) {
                var res=$http.get('/api/as/GlobalMaster?CategoryId='+catId);
                return res;
            },

            getPrintHistoryRequest: function(isGr) {
                var res=$http.get('/api/as/GetRequestedApprovalPrintService/'+isGr);
              
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },

            updatePrintHistoryRequest: function(data) {

                console.log("data update",data);
                // var res=$http.get('/api/ct/PostCCustomerAccessTab/',data);
                
                // return $http.put('/api/as/ApprovalPrintServiceHistory_TR', data);
                return $http.put('/api/as/approv/UpdateStatusApprovalServiceHistory/'+data.TransactionId+'/'+data.StatusApprovalId, []);
            },

            insertRequestPrintHistory: function(data) {

                console.log("request",data);
        
                // var res=$http.get('/api/ct/PostCCustomerAccessTab/',data);
                
                return $http.post('/api/as/PostApprovalRequestPrintServiceHistory', data);
                
            },

            updateUserList: function(data, CVId) {
                console.log('updateUserList >>>', data, CVId);
                return $http.put('/api/crm/PutVehicleUser/', [{
                    VehicleUserId: data.VehicleUserId,
                    CustomerVehicleId: CVId,
                    Name: data.name,
                    RelationId: data.Relationship,
                    Phone1: data.phoneNumber1,
                    Phone2: data.phoneNumber2,
                    Status: 1,
                    StatusCode: 1
                }])
            },

            createNewUserList: function(data, CVId) {
                console.log('createNewUserList >>>', data, CVId);
                return $http.post('/api/crm/PostVehicleUser/', [{
                    CustomerVehicleId: CVId,
                    Name: data.name,
                    RelationId: data.Relationship,
                    Phone1: data.phoneNumber1,
                    Phone2: data.phoneNumber2,
                    Status: 1,
                    StatusCode: 1
                }])
            },

            postJobWAC: function(data) {
                data.forEach(function(val) {
                    val.moneyAmount = (typeof val.moneyAmount != 'undefined' || val.moneyAmount != null ? val.moneyAmount : 0); //Billy
                });
                return $http.post('/api/as/JobWACs', data);
            },

            getWACBPbyJobId: function (jobid) {
                // var WAC = function
                return $http.get('/api/as/JobWACPBs/getByJob/' + jobid);
            },

            createWACBPbyJobId: function(data) {
                // var WAC = function
                return $http.post('/api/as/JobWACBPs/NewList', data);
            },

            updateWACBPbyJobId: function(data) {
                // var WAC = function
                return $http.put('/api/as/JobWACBPs/UpdateList', data);
            },

            createWOBPEstimation: function(jobId, dataEst) {
                if(dataEst.Putty == null){
                    dataEst.Putty = 0;
                }
                if(dataEst.Body == null){
                    dataEst.Body = 0;
                }
                if(dataEst.Surfacer == null){
                    dataEst.Surfacer = 0;
                }
                if(dataEst.Spraying == null){
                    dataEst.Spraying = 0;
                }
                if(dataEst.Polishing == null){
                    dataEst.Polishing = 0;
                }
                if(dataEst.ReAssembly == null){
                    dataEst.ReAssembly = 0;
                }
                if(dataEst.FinalInspection == null){
                    dataEst.FinalInspection = 0;
                }
                console.log("dataEst",dataEst);
                // return $http.put('/api/as/jobs/BpEstimation/' + jobId, [{
                return $http.put('/api/as/jobs/BpEstimationBSTK/' + jobId, [{
                    JobTasks: [],
                    JobTaskBP: dataEst
                }])
            },

            getVehicleMList: function() {
                return $http.get('/api/crm/GetCVehicleModel/');
            },

            getCustomerVehicleList: function(value) {
                var param1 = value.TID ? value.TID : '-';
                if (value.TID == '' || value.TID == null || value.TID == undefined){
                    param1 = '-'
                } else {
                    param1 = value.TID
                }
                var param2 = '-';
                var param3 = '-';
                var param4 = '-';
                var param5 = '-';
                var param6 = '-';
                var param7 = '-';
                var param8 = '-';

                switch (value.flag) {
                    case 1:
                        param2 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param2 = '-'
                        } else {
                            param2 = value.filterValue
                        }
                        break;
                    case 2:
                        param3 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param3 = '-'
                        } else {
                            param3 = value.filterValue
                        }
                        break;
                    case 3:
                        param4 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param4 = '-'
                        } else {
                            param4 = value.filterValue
                        }
                        break;
                    case 4:
                        param5 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param5 = '-'
                        } else {
                            param5 = value.filterValue
                        }
                        break;
                    case 5:
                        param6 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param6 = '-'
                        } else {
                            param6 = value.filterValue
                        }
                        break;
                    case 6:
                        param7 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param7 = '-'
                        } else {
                            param7 = value.filterValue
                        }
                        break;
                    case 7:
                        // param8 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param8 = '-'
                        } else {
                            param8 = angular.copy(value.filterValue.replace(/\./g, "z")) 
                            param8 = angular.copy(param8.replace(/\-/g, "x"))

                        }

                };

                // return $http.get('/api/crm/GetCustomerListFilter/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7);
                return $http.get('/api/crm/GetCustomerListFilterSatellite/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7 + '/' + param8);
            },

            getCustomerVehicleList_V2: function(value) {
                var param1 = value.TID ? value.TID : '-';
                if (value.TID == '' || value.TID == null || value.TID == undefined){
                    param1 = '-'
                } else {
                    param1 = value.TID
                }
                var param2 = '-';
                var param3 = '-';
                var param4 = '-';
                var param5 = '-';
                var param6 = '-';
                var param7 = '-';
                var param8 = '-';

                switch (value.flag) {
                    case 1:
                        param2 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param2 = '-'
                        } else {
                            param2 = value.filterValue
                        }
                        break;
                    case 2:
                        param3 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param3 = '-'
                        } else {
                            param3 = value.filterValue
                        }
                        break;
                    case 3:
                        param4 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param4 = '-'
                        } else {
                            param4 = value.filterValue
                        }
                        break;
                    case 4:
                        param5 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param5 = '-'
                        } else {
                            param5 = value.filterValue
                        }
                        break;
                    case 5:
                        param6 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param6 = '-'
                        } else {
                            param6 = value.filterValue
                        }
                        break;
                    case 6:
                        param7 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param7 = '-'
                        } else {
                            param7 = value.filterValue
                        }
                        break;
                    case 7:
                        // param8 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param8 = '-'
                        } else {
                            param8 = angular.copy(value.filterValue.replace(/\./g, "z")) 
                            param8 = angular.copy(param8.replace(/\-/g, "x"))

                        }

                };

                // return $http.get('/api/crm/GetCustomerListFilter/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7);
                return $http.get('/api/crm/GetCustomerListFilterSatellite_V2/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7 + '/' + param8);
            },

            getMLocationProvince: function() {
                return $http.get('/api/sales/MLocationProvince');
            },

            getMLocationCityRegency: function(id) {
                return $http.get('/api/sales/MLocationCityRegency?start=1&limit=100&filterData=ProvinceId|' + id);
            },

            getMLocationKecamatan: function(id) {
                return $http.get('/api/sales/MLocationKecamatan?start=1&limit=100&filterData=CityRegencyId|' + id);
            },

            getMLocationKelurahan: function(id) {
                return $http.get('/api/sales/MLocationKelurahan?start=1&limit=100&filterData=DistrictId|' + id)
            },

            getVehicleList: function(param1, param2) {
                // return $http.get('/api/crm/GetVehicleListSpecific/' + param1 + '/' + param2);
                return $http.get('/api/crm/GetVehicleListSpecificSatellite/' + param1 + '/' + param2);
            },

            NewGetVehicleListSpecific: function(param1, param2) {
                // return $http.get('/api/crm/GetVehicleListSpecific/' + param1 + '/' + param2);
                return $http.get('/api/crm/GetVehicleListSpecificSatellite_V2/' + param1 + '/' + param2);
            },

            createNewVehicleList: function(data) {
                console.log('data createNewVehicleList', data);
                // return $http.post('/api/crm/PostVehicle/' + data.Vehicle.Color.ColorId + '/' + data.Vehicle.Type.VehicleTypeId, [{
                return $http.post('/api/crm/PostVehicleSatellite/' + data.Vehicle.Color.ColorId + '/' + data.Vehicle.Type.VehicleTypeId, [{
                    VIN: data.Vehicle.VIN,
                    LicensePlate: data.Vehicle.LicensePlate,
                    EngineNo: data.Vehicle.EngineNo,
                    AssemblyYear: data.Vehicle.AssemblyYear,
                    DECDate: data.Vehicle.DECDate,
                    // VehicleTypeColorId: data.Vehicle.ColorId,
                    STNKName: data.Vehicle.STNKName,
                    STNKAddress: data.Vehicle.STNKAddress,
                    STNKDate: data.Vehicle.STNKDate,
                    GasTypeId: data.Vehicle.GasTypeId,
                    isNonTAM: data.Vehicle.isNonTAM,
                    OutletId: currUser.OrgId
                        // "StatusCode": 1,

                }]);
            },

            updateVehicleList: function(data) {
                var DecDate = new Date(data.Vehicle.DECDate);
                var STNKDate = null
                if (data.Vehicle.STNKDate != null && data.Vehicle.STNKDate != undefined) {
                    STNKDate = new Date(data.Vehicle.STNKDate);
                }


                // console.log('data updateVehicleList', data, data.Vehicle.DECDate.getFullYear(), data.Vehicle.DECDate.getMonth());
                var tmpDecDate = DecDate.getFullYear() + '-' + (DecDate.getMonth() + 1) + '-' + DecDate.getDay();
                // var tmpSTNKDate = STNKDate.getFullYear() + '-' + (STNKDate.getMonth() + 1) + '-' + STNKDate.getDate();
                var finalDate1
                var yyyy = DecDate.getFullYear().toString();
                var mm = (DecDate.getMonth() + 1).toString(); // getMonth() is zero-based
                var dd = DecDate.getDate().toString();
                finalDate1 = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);

                var finalDate2 = null
                if (STNKDate != null && STNKDate != undefined) {
                    var yyyy1 = STNKDate.getFullYear().toString();
                    var mm1 = (STNKDate.getMonth() + 1).toString(); // getMonth() is zero-based
                    var dd1 = STNKDate.getDate().toString();
                    finalDate2 = yyyy1 + '-' + (mm1[1] ? mm1 : "0" + mm1[0]) + '-' + (dd1[1] ? dd1 : "0" + dd1[0]);
                }

                console.log('generate date1', finalDate1);
                console.log('generate date2', finalDate2);
                // return $http.put('/api/crm/PutVehicle/' + data.Vehicle.Color.ColorId + '/' + data.Vehicle.Type.VehicleTypeId, [{
                return $http.put('/api/crm/PutVehicleSatellite/' + data.Vehicle.Color.ColorId + '/' + data.Vehicle.Type.VehicleTypeId, [{
                    VehicleId: data.Vehicle.VehicleId,
                    VIN: data.Vehicle.VIN,
                    LicensePlate: data.Vehicle.LicensePlate,
                    EngineNo: data.Vehicle.EngineNo,
                    AssemblyYear: data.Vehicle.AssemblyYear,
                    DECDate: finalDate1,
                    // DECDate: data.Vehicle.DECDate,
                    // Added by Fyberz =========
                    Insurance: data.Vehicle.Insurance,
                    SPK: data.Vehicle.SPK,
                    // =========================
                    // VehicleTypeColorId: data.Vehicle.ColorId,
                    STNKName: data.Vehicle.STNKName,
                    STNKAddress: data.Vehicle.STNKAddress,
                    STNKDate: finalDate2,
                    SourceDataId: data.Vehicle.SourceDataId,
                    // STNKDate: data.Vehicle.STNKDate,
                    GasTypeId: data.Vehicle.GasTypeId,
                    isNonTAM: data.Vehicle.isNonTAM,
                    OutletId: currUser.OrgId
                        // "StatusCode": 1,

                }]);
            },

            createNewCustomerList: function(data) {
                // return $http.post('/api/crm/PostCustomerList/', [{
                    var no_hp = null
                    if (data.Customer.CustomerTypeId != 1 && data.Customer.CustomerTypeId != 3){
                        no_hp = angular.copy(data.Customer.PICHp)
                    } else {
                        no_hp = angular.copy(data.Customer.HandPhone1)
                    }
                return $http.post('/api/crm/PostCustomerListSatellite/', [{
                    // ToyotaId: data.Customer.ToyotaId, //Masi Pakai Dummy Math.Random
                    OutletId: currUser.OrgId,
                    Npwp: data.Customer.Npwp,
                    CustomerTypeId: data.Customer.CustomerTypeId,
                    FleetId: 1, //Foreign Key, Masih Dummy
                    StatusCode: 1, //Status Create New
                        //ToyotaIdRequest: data.Customer.ToyotaIDFlag
                    CustomerName: this.checkIsCharacter(data.Customer.Name),
                    KTP: data.Customer.KTPKITAS,
                    NoHandphone: no_hp,
                    NPWP: data.Customer.Npwp,
                    AFCO: data.Customer.AFCOIdFlag,

                }]);
            },

            createNewCustomerListPersonal: function(id, data) {
                return $http.post('/api/crm/PostCustomerListPersonal/', [{
                    CustomerId: id,
                    CustomerName: data.Customer.Name,
                    FrontTitle: data.Customer.FrontTitle,
                    EndTitle: data.Customer.EndTitle,
                    KTPKITAS: data.Customer.KTPKITAS,
                    // BirthPlace: 'Dummy', //Not Used in WOBP
                    BirthDate: data.Customer.newBirthDate,
                    Handphone1: data.Customer.HandPhone1,
                    Handphone2: data.Customer.HandPhone2,
                    StatusCode: 1
                }]);
            },

            createNewCustomerListAddress: function(id, data) {
                _.map(data, function(val) {
                    val.MainAddress == true ? 1 : 0;
                });
                return $http.post('/api/crm/PostCCustomerAddress/', data);
            },

            getCustAddress: function(id) {
                return $http.get('/api/crm/GetCCustomerAddress/' + id);
            },

            createNewCustomerListInstitusi: function(id, data) {
                return $http.post('/api/crm/PostCustomerListInstitution/', [{
                    InstitutionId: 0,
                    CustomerId: id,
                    Name: data.Customer.Name,
                    SIUP: data.Customer.SIUP,
                    PICName: data.Customer.PICName,
                    PICHp: data.Customer.PICHp,
                    PICEmail: data.Customer.PICEmail,
                    Npwp: data.Customer.Npwp,
                    // PICDOB: data.Customer.PICName,
                    // PICGenderId: data.Customer.PICName,
                    // PICAddress: data.Customer.PICName,
                    // PICVillageId: data.Customer.PICName,
                    // PICRT: data.Customer.PICName,
                    // PICRW: data.Customer.PICName,
                    // SectorBusinessId: data.Customer.PICName,
                    // PICPositionId: data.Customer.PICName,
                    PICDepartmentId: data.Customer.PICDepartmentId,
                    // StatusCode: data.Customer.PICName,
                }]);

            },

            updateCustomerList: function(id, cTypeId, data) {
                // return $http.put('/api/crm/PutCustomerList/', [{
                return $http.put('/api/crm/PutCustomerListSatellite/', [{
                    CustomerId: id,
                    // OutletId: 655,
                    ToyotaId: data.Customer.ToyotaId,
                    Npwp: data.Customer.Npwp,
                    CustomerTypeId: cTypeId,
                    // FleetId: 1,
                    StatusCode: 1,
                    Afco: data.Customer.AFCOIdFlag

                }]);
            },

            updateCustomerListPersonal: function(Pid, Cid, data) {
                return $http.put('/api/crm/PutCListPersonal', [{
                    PersonalId: Pid,
                    CustomerId: Cid,
                    FrontTitle: data.Customer.FrontTitle,
                    EndTitle: data.Customer.EndTitle,
                    CustomerName: data.Customer.Name,
                    KTPKITAS: data.Customer.KTPKITAS,
                    // BirthPlace: 'Dummy', //Not Used in WOBP
                    BirthDate: data.Customer.newBirthDate,
                    Handphone1: data.Customer.HandPhone1,
                    Handphone2: data.Customer.HandPhone2,
                    StatusCode: 1

                }]);
            },

            deleteCustomerListAddress: function(id) {
                console.log('fdg', id)
                // return $http.delete('/api/crm/DeleteCCustomerAddress/', { data: id, headers: { 'Content-Type': 'application/json' } });
                return $http.put('/api/crm/DeleteCCustomerAddress/', id);
            },

            updateCustomerListAddress: function(Aid, Cid, data) {
                _.map(data, function(val) {
                    val.MainAddress == true ? 1 : 0;
                });
                return $http.put('/api/crm/PutCCustomerAddress/', data);
            },

            updateCustomerListInstitusi: function(Iid, Cid, data) {
                console.log("data di factory", data);
                return $http.put('/api/crm/PutCListInstitution', [{
                    InstitutionId: Iid,
                    CustomerId: Cid,
                    Name: data.Customer.Name,
                    SIUP: data.Customer.SIUP,
                    PICName: data.Customer.PICName,
                    PICHp: data.Customer.PICHp,
                    PICEmail: data.Customer.PICEmail,
                    Npwp: data.Customer.Npwp,
                    // PICDOB: data.Customer.PICName,
                    // PICGenderId: data.Customer.PICName,
                    // PICAddress: data.Customer.PICName,
                    // PICVillageId: data.Customer.PICName,
                    // PICRT: data.Customer.PICName,
                    // PICRW: data.Customer.PICName,
                    // SectorBusinessId: data.Customer.PICName,
                    // PICPositionId: data.Customer.PICName,
                    PICDepartment: data.Customer.PICDepartment,

                }]);
            },

            getPICDepartment: function() {
                var res = $http.get('/api/as/CustomerPosition');
                return res
            },

            getDataWAC: function() {
                return $http.get('/api/as/GroupWACsOnly');
            },

            getCategoryWOBP: function() {
                return $http.get('/api/as/GlobalMaster?CategoryId=1010')
            },

            getCategoryRepair: function() {
                return $http.get('/api/as/GlobalMaster?CategoryId=1012')
            },

            getRelationship: function() {
                return $http.get('/api/crm/GetCCustomerRelation/');
            },

            getDamage: function() {
                var catId = 2040;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId + '&Flag=1');
                console.log('resnya pause=>', res);
                return res;
            },

            getSatelliteByCenter: function(OutIdCtr) {
                return $http.get('/api/as/OutletBPCenter?OutletBPCenter=' + OutIdCtr);
            },

            getCenterBySatellite: function(OutIdSat) {
                return $http.get('/api/as/OutletBPSatellite?OutletBPSatellite=' + OutIdSat);
                // var defer = $q.defer();
                //     defer.resolve(
                //       {
                //         "data" : {
                //           "Result":[{
                //               "Id":1,
                //               "OutletIdSatellite":655,
                //               "OutletBPCenter":180,
                //               "SAName":"Advisor Name",
                //               "StatusCode":1,
                //               "LastModifiedDate":"2017-10-04",
                //               "LastModifiedUserId":1
                //           }],
                //           "Start":1,
                //           "Limit":10,
                //           "Total":1
                //         }
                //       }
                //     );
                // return defer.promise;
            },

            getDataWACItem: function(jobid, groupId) {
                return $http.get('/api/as/ItemWACs/GetDataWACItem/' + jobid + '/' + groupId);
            },

            getAllTypePoints: function() {
                return $http.get('/api/as/TypePoints');
            },

            getEmployeeId: function(userName) {
                return $http.get('/api/as/getEmployeeId/' + userName);
            },

            getStatWO: function(JobId, status) {
                return $http.get('/api/as/Jobs/GetStatusJob/' + JobId + '/' + status);
            },

            postCountingPrint: function(jobid, typeId) {
                return $http.post("/api/as/PostPrint", [{
                    JobId: jobid,
                    PrintCountTypeId: typeId,
                    StatusCode: 1
                }]);
            },

            getDataTaskOpe: function(Key, Katashiki, catg, vehicleModelId) {
                var res = $http.get('/api/as/TaskListsCode?KatashikiCode=' + Katashiki + '&Name=' + Key + '&TaskCategory=' + catg + '&VehicleModelId=' + vehicleModelId);
                // var res=$http.get('/api/as/TaskLists?KatashikiCode=ASV50R-JETGKD&Name=&TaskCategory='+catg);
                console.log('/api/as/TaskLists?KatashikiCode=' + Katashiki + '&Name=' + Key + '&TaskCategory=' + catg + '&VehicleModelId=' + vehicleModelId);
                // console.log("resnya",res);
                return res;
            },

            getDiscountCampaign: function(today, data, crm, IsGR) {
                var KmDiscount = 0;
                if (data.Km !== undefined && data.Km !== null) {
                    if (typeof data.Km === 'string') {
                        var tmpKm = angular.copy(data.Km);
                        if (tmpKm.includes(",")) {
                            tmpKm = tmpKm.split(',');
                            if (tmpKm.length > 1) {
                                tmpKm = tmpKm.join('');
                            } else {
                                tmpKm = tmpKm[0];
                            }
                        }
                        tmpKm = parseInt(tmpKm);
                        KmDiscount = tmpKm;
                    } else {
                        KmDiscount = data.Km;
                    }
                }
                console.log("getDiscountCampaign", today, data, crm);
                // '/api/as/CampaignDiscount/Date/2017-08-07/KM/2100/AYear/2015/VechId/4642'
                // return $http.get('/api/as/CampaignDiscount/Date/' + today + '/KM/' + KmDiscount + '/AYear/' + crm.Vehicle.AssemblyYear + '/VechId/' + crm.Vehicle.Type.VehicleTypeId + '/IsGR/' + IsGR);
                return $http.get('/api/as/CampaignDiscount/Date/' + today + '/KM/' + KmDiscount + '/AYear/' + crm.Vehicle.AssemblyYear + '/VechId/' + data.VehicleTypeId + '/IsGR/' + IsGR);
            },

            getCheckStatusParts: function(JobId, data) {
                console.log('jobid ====>',JobId);
                console.log('ihhh datany', data)
                return $http.post('/api/as/JobParts/CekStatusParts/' + JobId, data);
            },

            GetStatusApprovalWarranty: function(JobId) {
                return $http.get('/api/as/GetStatusApprovalWarranty/' + JobId );
            },

            getAllApprovalJobTask: function(OutletId, RoleId, Mode, isGr) {
                var res = $http.get('/api/as/ApprovalDiscount/GetDataApprovalDiscount', {
                    params: {
                        OutletId: OutletId,
                        RoleId: RoleId,
                        Mode: Mode,
                        IsGR: isGr
                    }
                });
                return res;
            },

            getJobParts: function(jobId, isGr) {
                return $http.get('/api/as/jobs/listJobPartList/' + jobId + '/3/' + isGr);
            },

            getTowass: function(vin) {
                var res = $http.get('/api/as/Towass/getFieldAction/1/' + vin);
                return res;
            },

            getMRS: function(ReminderTypeId) {
                var res = $http.get('/api/as/Towass/getFieldMRS/' + ReminderTypeId);
                return res;
            },

            getVehicleTypeById: function(id) {
                var url = '/api/crm/GetCVehicleTypeById/' + id
                var res = $http.get(url);
                return res;
            },

            getVehicleTypeColorById: function(id) {
                var url = '/api/crm/GetMVehicleTypeColorById/' + id
                var res = $http.get(url);
                return res;
            },
            NewGetMVehicleTypeColorById: function(id) {
                var url = '/api/crm/NewGetMVehicleTypeColorById/' + id
                var res = $http.get(url);
                return res;
            },

            getDataPKS: function(PoliceNumber) {
                return $http.get('/api/as/PKS/GetByLicensePlate/' + PoliceNumber);
            },

            updatePKS: function(PoliceNumber, totalAkhir) {
                totalAkhir = Math.ceil(totalAkhir) // di round up karena pernah ada totalnya koma, dan karena tagihan y round up hrs nya
                return $http.get('/api/as/PKS/CekPKS/' + PoliceNumber + '/' + totalAkhir);
            },

            newCustomerVehicleUser: function(vID, cID) {
                return $http.post('/api/crm/PostCustomerVehicleList/', [{
                    CustomerOwnerId: cID,
                    VehicleId: vID,
                    CurrentStatus: 1,
                    CreatedDate: new Date(),
                    CreatedUserId: currUser.UserId

                }])
            },

            updateEXT: function(data) {
                return $http.put('/api/as/JobWACExts', data);
            },

            getVehicleUser: function(id) {
                return $http.get('/api/crm/GetVehicleUser/' + id);
            },

            CheckNopolQueueListSecurity: function(type, nopol, jobid) {
                var res = $http.get('/api/as/CheckPoliceNoAvalailability/' + type + '/' + nopol + '/' + jobid);
                return res
            },

            CheckVINisOK: function(VIN, PoliceNo, Flag) {
                var res = $http.put('/api/crm/CheckVINisOK/' + VIN + '/' + PoliceNo + '/' + Flag);
                return res
            },

            updateVehicleNonTAM: function(data) {
                console.log("updateVehicleNonTAM", data);
                return $http.put('/api/crm/PutVehicleNonTAMSatellite/', [{
                    VehicleId: data.Vehicle.VehicleId,
                    VIN: data.Vehicle.VIN,
                    LicensePlate: data.Vehicle.LicensePlate,
                    EngineNo: data.Vehicle.EngineNo,
                    AssemblyYear: data.Vehicle.AssemblyYear,
                    DECDate: data.Vehicle.DECDate,
                    KatashikiCode: data.Vehicle.KatashikiCode,
                    STNKName: data.Vehicle.STNKName,
                    STNKAddress: data.Vehicle.STNKAddress,
                    STNKDate: data.Vehicle.STNKDate,
                    GasTypeId: data.Vehicle.GasTypeId,
                    OutletId: currUser.OrgId,
                    ModelName: data.Vehicle.ModelName,
                    ModelType: data.Vehicle.ModelType,
                    ColorName: data.Vehicle.ColorName,
                    KatashikiCode: data.Vehicle.KatashikiCodeNonTAM
                }]);
            },

            createNewVehicleNonTAM: function(data) {
                console.log("createNewVehicleNonTAM", data);
                return $http.post('/api/crm/PostVehicleNonTAMSatellite/', [{
                    VIN: data.Vehicle.VIN,
                    LicensePlate: data.Vehicle.LicensePlate,
                    EngineNo: data.Vehicle.EngineNo,
                    AssemblyYear: data.Vehicle.AssemblyYear,
                    DECDate: data.Vehicle.DECDate,
                    STNKName: data.Vehicle.STNKName,
                    STNKAddress: data.Vehicle.STNKAddress,
                    KatashikiCode: data.Vehicle.KatashikiCode,
                    STNKDate: data.Vehicle.STNKDate,
                    GasTypeId: data.Vehicle.GasTypeId,
                    OutletId: currUser.OrgId,
                    ModelName: data.Vehicle.ModelName,
                    ModelType: data.Vehicle.ModelType,
                    ColorName: data.Vehicle.ColorName,
                    KatashikiCode: data.Vehicle.KatashikiCodeNonTAM
                }]);
            },

            getListVehicleCPPK: function(id) {
                return $http.get('/api/crm/GetListVehicleUserPhone/' + id);
            },

            updateStatusForWO: function(JobId, toStat) {
                return $http.put('/api/as/UpdateJob/' + JobId + '/StatusTo/' + toStat);
            },
            updateStatusForWONew: function (JobId, toStat, flag) {
                return $http.put('/api/as/UpdateJob/' + JobId + '/StatusTo/' + toStat + '/flag/'+ flag);
            },

            CancelWOWalkin: function(data) {
                if (data.PoliceNumber == null || data.PoliceNumber == undefined) {
                    data.PoliceNumber = data.LicensePlate;
                }
                return $http.put('/api/as/Gate/updateCancelWO', [{
                    PoliceNo: data.PoliceNumber,
                    nowTime: new Date(),
                    Id: data.Id
                }])
            },

            CheckApprovalDiscountTask: function(JobId) {
                var res = $http.get('/api/as/CheckApprovalDiscountTask/' + JobId );
                return res;
            },
            CheckApprovalDiscountPart: function(JobId) {
                var res = $http.get('/api/as/CheckApprovalDiscountPart/' + JobId );
                return res;
            },

            getDiscountMGroupCustomerWO: function(data) {
                console.log('data untuk discountMGroupCust', data)
                var Nama = '';
                var tlp1 = '';
                var tlp2 = '';
                var ktp = '';
                var npwp = '';
                if (data.CustomerTypeId == 3 || data.CustomerTypeId == 1) {
                    Nama = data.Name;
                    tlp1 = data.HandPhone1;
                    tlp2 = data.HandPhone2;
                    ktp = data.KTPKITAS;
                    npwp = data.Npwp;
                } else {
                    Nama = data.Name;
                    // tlp1 = data.PICHp;
                    npwp = data.Npwp;
                }
                if (Nama == '' || Nama == undefined || Nama == null){
                    Nama = '-';
                }
                if (tlp1 == '' || tlp1 == undefined || tlp1 == null){
                    tlp1 = '-';
                }
                if (tlp2 == '' || tlp2 == undefined || tlp2 == null){
                    tlp2 = '-';
                }
                if (ktp == '' || ktp == undefined || ktp == null){
                    ktp = '-';
                }
                if (npwp == '' || npwp == undefined || npwp == null){
                    npwp = '-';
                }
                return $http.put('/api/crm/PutDiscountMGroupCustomer', [{
                    // Name: data.Name,
                    // Phone1: data.HandPhone1,
                    // Phone2: data.HandPhone2,
                    // KtpNo: data.KTPKITAS,
                    // NPWP: data.Npwp
                    Name: Nama,
                    Phone1: tlp1,
                    Phone2: tlp2,
                    KtpNo: ktp,
                    NPWP: npwp
                }]);
            },

            OutletBPCenter: function(id) {
                return $http.get('/api/as/OutletBPCenter?OutletBPCenter=' + id)
            },

            getWACBPbyItemId: function(jobItemId) {
                // var WAC = function
                return $http.get('/api/as/JobWACBPs/getByItem/' + jobItemId);
            },
            
            CheckIsWarrantyAktif: function(JobId) {
                console.log("data checkApproval", JobId);
                return $http.get('/api/as/CheckIsWarrantyAktif/' + JobId);
            },

            checkIsWarranty: function(JobId) {
                console.log("data checkApproval", JobId);
                return $http.get('/api/as/IsWarranty/' + JobId);
            },

            CekStatusJobBilling: function(JobId, flag) {
                // console.log("cek data", data);
                var res = $http.get('/api/as/Billings/CekStatusJob/' + JobId + '/' + flag );
                return res;
            },

            getTransactionCode: function() {
                // var catId = 2028;
                // var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                // console.log('resnya getTransactionCode=>', res);
                // return res;

                return $http.get('/api/as/InvoiceTypes');
            },

            GetOplList: function(key, model) {
                var res = $http.get('/api/as/OPLFilter?oplname=' + key + '&modelName=' + model);
                return res
            },

            checkGI: function(item) {
                var res = $http.get('/api/as/jobs/cekGI/' + item.JobId);
                return res
            },

            getWobyJobId: function(filter) {
                var res = $http.get('/api/as/Jobs/' + filter);
                return res;
            },

            getDataVehicleHistory: function(category, vin, StartDate1, StartDate2) {
                // var res=$http.get('/api/as/Jobs/'+category+'/'+vehicleid);
                // var res=$http.get('/api/ct/GetVehicleService/'+category+'/'+vehicleid+'');
                var res = $http.get('/api/ct/GetVehicleService/' + category + '/' + vin + '/' + StartDate1 + '/' + StartDate2);

                console.log('res dari wo history crm=>', res);
                return res;
            },
            sendNotif: function(data, recepient, param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param: param
                }]);
            },
            postApproval: function(data) {
                // ApproverRoleId
                console.log("postApproval", data);
                return $http.post('/api/as/PostApprovalRequestDP/2', [{
                    JobId: data[0].JobId,
                    DPDefault: data[0].totalDpDefault,
                    DPRequested: data[0].totalDp,
                    ApprovalCategoryId: 39,
                    ApproverId: null,
                    RequesterId: null,
                    ApproverRoleId: data[0].ApproverRoleId,
                    RequestorRoleId: currUser.RoleId,
                    RequestReason: "Request Pengurangan DP",
                    RequestDate: new Date(),
                    StatusApprovalId: 3,
                    StatusCode: 1,
                    VehicleTypeId: data[0].VehicleTypeId
                }]);
            },
            checkApproval: function(data) {
                console.log("data checkApproval", data);
                return $http.put('/api/as/CheckingApproval/', [{
                    DPRequested: data,
                    ApprovalCategoryId: 42,
                    RoleId: currUser.RoleId
                }]);
            },
            postApprovalDiscountTask: function(jobId, data, ApprovalCategoryId) {
                console.log("dataaaaa", data);
                return $http.post('/api/as/PostApprovalDiscountTask', [{
                    JobId: jobId,
                    JobTask: data,
                    ApprovalCategoryId: ApprovalCategoryId
                }]);
            },

            GetCVehicleTypeListByKatashiki: function(KatashikiCode) {
                // api/crm/GetCVehicleTypeListByKatashiki/{KatashikiCode}
                return $http.get('/api/crm/GetCVehicleTypeListByKatashiki/' + KatashikiCode)
            },

            // Tambahan CR 4 [start]

            // getDataCekEstimasi: function (param) {
            //     // var res = $http.get('/api/as/AEstimationBP/Get/?start=1&limit=100000');
            //     var res = $http.get('/api/as/AEstimationBP/Get/?start=1&limit=100000' + param);
            //     return res;
            // },

            getDetailEstimasi: function (params,PoliceNo) {
               var res = $http.get('/api/as/AEstimationBP/GetDetail/?EstId='+params+'&PoliceNumber='+PoliceNo+'&Ref=0');
               // var res = $http.get('/api/as/AEstimationBP/GetDetail/?EstId='+ EstId);
               return res;
            },
            
            getDataEstimasi: function(data) {
                // var url = "&NoPolisi=" + $scope.mDataDetail.LicensePlate; 
                var res = $http.get('/api/as/AEstimationBP/Get/?start=1&limit=100000&NoPolisi=' + data);
                return res;
            },

            
            updateDataSPK: function(data) {
                console.log('data update spk factory >>>', data);

                return $http.post('/api/as/AEstimationBP/UpdateDataSPK/',data)
            },


            updateNewSPK: function(data) {
                console.log('data update spk New factory >>>', data);

                return $http.put('/api/as/AEstimationBP/UpdateDataWOListSPK/',data)
            },
            
            UpdateJobAppointmentAddEstimasiId: function(jobId, arrayEstimasiId) {
                return $http.put('/api/as/AEstimationBP/UpdateJobAppointment?JobId=' + jobId, arrayEstimasiId);
            },
            saveNewSPK: function(data){
                return $http.post('/api/as/AEstimationBP/SimpanSPKUtama',data);
            },


            getDataJobTask: function(data) {
                // var url = "&NoPolisi=" + $scope.mDataDetail.LicensePlate; 
                var res = $http.get('/api/as/GetJobTaskSPK/' +data);
                return res;
            },


            getDataEstTidakLanjutWO: function(data) {
                // var url = "&NoPolisi=" + $scope.mDataDetail.LicensePlate; 
                var res = $http.get('/api/as/AEstimationBP/GetListJobEstimation?nopol='+data);
                return res;
            },

            getWoListEstimasiNext: function(data){
                var res = $http.get('/api/as/AEstimationBP/GetListEstimationWOList?start=1&limit=1000&nopol='+data);
                return res;
            },
            GetResetCounterOdometer: function(vin) {
                // api/ct/GetResetCounterOdometer/{VIN}
                return $http.get('/api/ct/GetResetCounterOdometer/' + vin)
            },
            
            // Tambahan CR 4 [_end_]
            getStatusAllChip: function(jobId){
                return $http.get('/api/as/JobTaskBP/CheckJobPlanStatus/'+jobId)
            },
            checkDataPartLatest: function(JobId) {
                return $http.get('/api/as/JobParts/CheckAllParts/' + JobId)
            },
            checkApprovalPenguranganJob: function(JobTaskId, jobid){
                return $http.put('/api/as/CheckApprovalStatusByJobTaskId/' + jobid  + '/' + 2, JobTaskId)
            },
            updateORSPK: function(JobID,Data){
                return $http.put('/api/as/AEstimationBP/PutValueJobSPK?JobId='+JobID+'&ORAmount='+Data);
            }
        }
    });