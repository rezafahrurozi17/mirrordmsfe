angular.module('app')
    .factory('WOBP', function($http, $filter, CurrentUser) {
        var currUser = CurrentUser.user();
        // var a = new Date();
        // var yearFirst = a.getFullYear();
        // var monthFirst = a.getMonth() + 1;
        // var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
        // var dayFirst = a.getDate();
        // var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
        // var hour = a.getHours();
        // var hours = hour < 10 ? '0' + hour : hour;
        // var minutes = a.getMinutes();
        // var minutess = minutes < 10 ? '0' + minutes : minutes;
        // var seconds = a.getSeconds();
        // var secondss = seconds < 10 ? '0' + seconds : seconds;
        // var WoCreatedDateX = yearFirst + '-' + monthFirsts + '-' + dayFirsts + 'T' + hours + ':' + minutess + ':' + secondss;
        return {

            

            checkHistoryDiscount: function(dateFrom,dateTo) {
                // var res = $http.get('/api/as/MAppointmentDiscount/CheckPerubahanDiskon?dateFrom=2020-03-08&dateTo=2020-03-15');                
                var res = $http.get('/api/as/MAppointmentDiscount/CheckPerubahanDiskon?dateFrom='+dateFrom+'&dateTo='+ dateTo);                
                return res;
            },


            getDataAktif: function() {
                var res = $http.get('/api/as/MAppointmentDiscount/GetAktif/?start=1&limit=100000');                
                return res;
            },


            getCategoryWO: function() {
                return $http.get('/api/as/GlobalMaster?CategoryId=1010')
            },
            findTotalDPDefaul:function(JobList){
                var totalDpDefault = 0;
                for(var i in JobList){
                    if(JobList[i].JobParts.length >0){
                        for(var j in JobList[i].JobParts){
                            if(JobList[i].JobParts[j].DP == undefined || typeof JobList[i].JobParts[j].DP == undefined ){
                                totalDpDefault += JobList[i].JobParts[j].minimalDP;
                            }else{
                                totalDpDefault += JobList[i].JobParts[j].DP;
                            }
                        }
                    }
                }
                console.log('releaseWOBP totalDpDefault ===>',totalDpDefault)

                return totalDpDefault;
            },
            changeFormatDate: function(item) {
                var tmpAppointmentDate = item;
                console.log("changeFormatDate item", item);
                var finalDate
                if (item == null || item == undefined) {
                    finalDate = '1900-01-01'; //gak boleh di buat null, error nanti di BE
                } else {
                    tmpAppointmentDate = new Date(tmpAppointmentDate);

                    if (tmpAppointmentDate !== null && tmpAppointmentDate !== undefined) {
                        var yyyy = tmpAppointmentDate.getFullYear().toString();
                        var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
                        var dd = tmpAppointmentDate.getDate().toString();
                        finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                    } else {
                        finalDate = '';
                    }
                }

                console.log("changeFormatDate finalDate", finalDate);
                return finalDate;
            },
            validationNonTaskList: function(data){
                console.log('cek data nontasklist aja', data)
                for (var i=0; i<data.length; i++){
                    if (data[i].TaskBPId !== null && data[i].TaskBPId !== undefined){
                        if (parseInt(data[i].TaskBPId) < 0){
                            data[i].TaskBPId = null
                        }
                    }
                }
                return data
            },
            getWoCategory: function() {
                var catId = 2039;
                // var catId = 1012;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId + '&Flag=1');
                console.log('resnya pause=>', res);
                return res;
            },
            getAllTypePoints: function() {
                return $http.get('/api/as/TypePoints');
            },
            getCategoryRepair: function() {
                return $http.get('/api/as/GlobalMaster?CategoryId=1012')
            },
            getDataWAC: function() {
                return $http.get('/api/as/GroupWACsOnly');
            },

            getDataWACItem: function(jobid, groupId) {
                return $http.get('/api/as/ItemWACs/GetDataWACItem/' + jobid + '/' + groupId);
            },

            getItemWAC: function(groupId) {
                return $http.get('/api/as/ItemWACs/GetByGroupId/' + groupId);
            },
            getDataQueueList: function() {
                var res = $http.get('/api/as/queue/QueueList/0');
                return res
            },
            getPayment: function() {
                var catId = 1008;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getUnitMeasurement: function() {
                var catId = 1;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getTaskCategory: function() {
                var catId = 1010;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getDetailCustomer: function(arr) {
                var res = $http.get('/api/as/MRS/' + arr);
                // api/as/MRS/{MrsId}/
                return res;
            },
            getRelationship: function() {
                return $http.get('/api/crm/GetCCustomerRelation/');
            },
            getUserLists: function(vId) {
                return $http.get('/api/crm/GetVehicleUser/' + vId);
            },
            createNewUserList: function(data, CVId) {
                console.log('createNewUserList >>>', data, CVId);
                return $http.post('/api/crm/PostVehicleUser/', [{
                    CustomerVehicleId: CVId,
                    // Name: data.name,
                    Name: this.checkIsCharacter(data.name),
                    RelationId: data.Relationship,
                    Phone1: data.phoneNumber1,
                    Phone2: data.phoneNumber2,
                    Status: 1,
                    StatusCode: 1
                }])
            },
            updateUserList: function(data, CVId) {
                console.log('updateUserList >>>', data, CVId);
                return $http.put('/api/crm/PutVehicleUser/', [{
                    VehicleUserId: data.VehicleUserId,
                    CustomerVehicleId: CVId,
                    // Name: data.name,
                    Name: this.checkIsCharacter(data.name),
                    RelationId: data.Relationship,
                    Phone1: data.phoneNumber1,
                    Phone2: data.phoneNumber2,
                    Status: 1,
                    StatusCode: 1
                }])
            },


            WOTimeStamp: function(data) {
                // api/as/jobs/WOTimeStamp
                return $http.put('/api/as/jobs/WOTimeStamp', [{
                    JobId: data.JobId,
                    StartTime: data.TimeStampStart,
                    FinishTime: data.TimeStampFinish,
                    Type: data.Type   // 1. teco, 2. call cust
                }]);
            },

            updateWOBP: function(mData, mDataCrm, mDataDM, JobRequest, JobComplaint, JobList, oplData, JobId) {
                console.log('updateWOBP');
                var a = new Date();
                var yearFirst = a.getFullYear();
                var monthFirst = a.getMonth() + 1;
                var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                var dayFirst = a.getDate();
                var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                var hour = a.getHours();
                var hours = hour < 10 ? '0' + hour : hour;
                var minutes = a.getMinutes();
                var minutess = minutes < 10 ? '0' + minutes : minutes;
                var seconds = a.getSeconds();
                var secondss = seconds < 10 ? '0' + seconds : seconds;
                var WoCreatedDateX = yearFirst + '-' + monthFirsts + '-' + dayFirsts + 'T' + hours + ':' + minutess + ':' + secondss;
                JobList = this.validationNonTaskList(JobList)
                if (mDataCrm.Vehicle.isNonTAM == 1){
                    mData.VehicleModelName = mDataCrm.Vehicle.ModelName 
                }

                JobList = this.isiGuidLogTaskPartOPL(JobList,mData.GUIDWOBP,1)
                
                return $http.put('/api/as/Jobs/updateDetil/2', [{
                    isAppointment: mData.isAppointment == null || typeof mData.isAppointment == "undefined" ? 0 : mData.isAppointment,
                    AppointmentDate: mData.AppointmentDate,
                    AppointmentTime: mData.AppointmentTime,
                    AppointmentNo: mData.AppointmentNo,
                    JobId: JobId,
                    StallId: mData.StallId === 0 ? null : mData.StallId,
                    OutletId: currUser.OrgId,
                    TechnicianAction: 1,
                    VehicleId: mDataCrm.Vehicle.VehicleId,
                    JobSuggest: 1,
                    JobTask: JobList,
                    JobComplaint: JobComplaint,
                    JobRequest: JobRequest,
                    isGr: 0,
                    Km: mData.KmNormal,
                    isCash: mData.isCash,
                    isSpk: mData.isSpk,
                    SpkNo: mData.SpkNo ? mData.SpkNo : '-',
                    // EstimationDate: mData.EstimateDeliveryTime,
                    // EstimationDate: mData.EstimationDate,
                    EstimationDate: mData.ClickLanjut,
                    WoCategoryId: mData.WoCategoryId,
                    JobDate: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
                    KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    VehicleTypeId: mDataCrm.Vehicle.VehicleTypeId,
                    PoliceNumber: mDataCrm.Vehicle.LicensePlate == null ? mData.PoliceNumber : mDataCrm.Vehicle.LicensePlate,
                    FinalDP: mData.totalDp,

                    DPPaidAmount: mData.DPPaidAmount,
                    ORPaidAmount: mData.ORPaidAmount,
                    DPRawatJalan: mData.DPRawatJalan,
                    IsPaidRawatJalan: mData.IsPaidRawatJalan,

                    // FinalDP: mData.flag.service == 1 ? mData.totalDpDefault : mData.TotalSummaryPrice,
                    ModelType: mData.VehicleModelName,
                    JobTaskOpl: oplData,
                    InsuranceName: mData.InsuranceName,
                    ActiveChip: mData.ActiveChip,
                    DecisionMaker: mDataDM.PK.Name ? mDataDM.PK.Name : null,
                    PhoneDecisionMaker1: mDataDM.PK.HP1 ? mDataDM.PK.HP1 : null,
                    PhoneDecisionMaker2: mDataDM.PK.HP2 ? mDataDM.PK.HP2 : null,
                    ContactPerson: mDataDM.CP.Name ? mDataDM.CP.Name : null,
                    PhoneContactPerson1: mDataDM.CP.HP1 ? mDataDM.CP.HP1 : null,
                    PhoneContactPerson2: mDataDM.CP.HP2 ? mDataDM.CP.HP2 : null,
                    UsedPart: mData.flag.parts,
                    IsWaiting: mData.flag.wait,
                    IsWash: mData.flag.wash,
                    ServiceWithVehicle: mData.flag.service,
                    WoCreatedDate: WoCreatedDateX,
                    UserIdSa: currUser.UserId,
                    UserIdApp: mData.UserIdApp,
                    JobCatgForBPid: mData.JobCatgForBPid,
                    // FixedDeliveryTime1: mData.EstimateDeliveryTime,
                    // FixedDeliveryTime2: mData.AdjusmentDate,
                    FixedDeliveryTime1: $filter('date')(mData.EstimateDeliveryTime, 'yyyy-MM-dd HH:mm:ss'),
                    FixedDeliveryTime2: $filter('date')(mData.AdjusmentDate, 'yyyy-MM-dd HH:mm:ss'),
                    FixedDeliveryTime: mData.FixedDeliveryTime,
                    PaymentMethod: mData.PaymentMethod,
                    invoiceType: mData.CodeTransaction,
                    PlanDateStart: mData.PlanDateStart,
                    PlanDateFinish: mData.PlanDateFinish,
                    // VehicleTypeId: mData.VehicleTypeId,
                    ModelType: mData.VehicleModelName,
                    VehicleConditionTypeId: mData.VehicleConditionTypeId,
                    VehicleColor: mData.VehicleColor,
                    // ServWithoutVehicAmount: mData.TotalSummaryPrice
                    QueueId: mData.QueueId,
                    IsCustomerRequest : mData.IsCustomerRequest != 1 ? 0 : 1,
                    GateInPushTime: $filter('date')(mData.GateInPushTime, 'yyyy-MM-dd HH:mm:ss'),
                    AppointmentCreateDate: (mData.AppointmentCreateDate == null || mData.AppointmentCreateDate == undefined) ? null : $filter('date')(mData.AppointmentCreateDate, 'yyyy-MM-dd HH:mm:ss'),
                    Guid_Log: mData.GUIDWOBP,


                }]);
            },

            updateWOBPBSTK: function(mData, mDataCrm, mDataDM, JobRequest, JobComplaint, JobList, oplData, JobId) {
                console.log('updateWOBP');
                var a = new Date();
                var yearFirst = a.getFullYear();
                var monthFirst = a.getMonth() + 1;
                var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                var dayFirst = a.getDate();
                var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                var hour = a.getHours();
                var hours = hour < 10 ? '0' + hour : hour;
                var minutes = a.getMinutes();
                var minutess = minutes < 10 ? '0' + minutes : minutes;
                var seconds = a.getSeconds();
                var secondss = seconds < 10 ? '0' + seconds : seconds;
                var WoCreatedDateX = yearFirst + '-' + monthFirsts + '-' + dayFirsts + 'T' + hours + ':' + minutess + ':' + secondss;
                JobList = this.validationNonTaskList(JobList)
                if (mDataCrm.Vehicle.isNonTAM == 1){
                    mData.VehicleModelName = mDataCrm.Vehicle.ModelName 
                }

                return $http.put('/api/as/Jobs/updateDetil/2', [{
                    JobId: JobId,
                    isAppointment: mData.isAppointment == null || typeof mData.isAppointment == "undefined" ? 0 : mData.isAppointment,
                    AppointmentDate: mData.AppointmentDate,
                    AppointmentTime: mData.AppointmentTime,
                    AppointmentNo: mData.AppointmentNo,
                    StallId: mData.StallId === 0 ? null : mData.StallId,
                    OutletId: currUser.OrgId,
                    TechnicianAction: 1,
                    VehicleId: mDataCrm.Vehicle.VehicleId,
                    JobSuggest: 1,
                    JobTask: JobList,
                    JobComplaint: JobComplaint,
                    JobRequest: JobRequest,
                    isGr: 0,
                    Km: mData.KmNormal,
                    isCash: mData.isCash,
                    isSpk: mData.isSpk,
                    SpkNo: mData.SpkNo ? mData.SpkNo : '-',
                    // EstimationDate: mData.EstimateDeliveryTime,
                    // EstimationDate: mData.EstimationDate,
                    EstimationDate: mData.ClickLanjut,
                    WoCategoryId: mData.WoCategoryId,
                    JobDate: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
                    KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    VehicleTypeId: mDataCrm.Vehicle.VehicleTypeId,
                    PoliceNumber: mDataCrm.Vehicle.LicensePlate == null ? mData.PoliceNumber : mDataCrm.Vehicle.LicensePlate,
                    FinalDP: mData.totalDp,

                    DPPaidAmount: mData.DPPaidAmount,
                    ORPaidAmount: mData.ORPaidAmount,
                    DPRawatJalan: mData.DPRawatJalan,
                    IsPaidRawatJalan: mData.IsPaidRawatJalan,

                    // FinalDP: mData.flag.service == 1 ? mData.totalDpDefault : mData.TotalSummaryPrice,
                    ModelType: mData.VehicleModelName,
                    JobTaskOpl: oplData,
                    InsuranceName: mData.InsuranceName,
                    ActiveChip: mData.ActiveChip,
                    DecisionMaker: mDataDM.PK.Name ? mDataDM.PK.Name : null,
                    PhoneDecisionMaker1: mDataDM.PK.HP1 ? mDataDM.PK.HP1 : null,
                    PhoneDecisionMaker2: mDataDM.PK.HP2 ? mDataDM.PK.HP2 : null,
                    ContactPerson: mDataDM.CP.Name ? mDataDM.CP.Name : null,
                    PhoneContactPerson1: mDataDM.CP.HP1 ? mDataDM.CP.HP1 : null,
                    PhoneContactPerson2: mDataDM.CP.HP2 ? mDataDM.CP.HP2 : null,
                    // UsedPart: mData.flag.parts,
                    // IsWaiting: mData.flag.wait,
                    // IsWash: mData.flag.wash,
                    WoCreatedDate: WoCreatedDateX,
                    UserIdSa: currUser.UserId,
                    JobCatgForBPid: mData.JobCatgForBP,
                    // FixedDeliveryTime1: mData.EstimateDeliveryTime,
                    // FixedDeliveryTime2: mData.AdjusmentDate,
                    FixedDeliveryTime1: $filter('date')(mData.EstimateDeliveryTime, 'yyyy-MM-dd HH:mm:ss'),
                    FixedDeliveryTime2: $filter('date')(mData.AdjusmentDate, 'yyyy-MM-dd HH:mm:ss'),
                    FixedDeliveryTime: mData.FixedDeliveryTime,
                    PaymentMethod: mData.PaymentMethod,
                    invoiceType: mData.CodeTransaction,
                    PlanDateStart: mData.PlanDateStart,
                    PlanDateFinish: mData.PlanDateFinish,
                    VehicleTypeId: mData.VehicleTypeId,
                    ModelType: mData.VehicleModelName,
                    VehicleConditionTypeId: mData.VehicleConditionTypeId,
                    VehicleColor: mData.VehicleColor,
                    // ServWithoutVehicAmount: mData.TotalSummaryPrice
                    QueueId: mData.QueueId,
                    IsCustomerRequest : mData.IsCustomerRequest != 1 ? 0 : 1,
                    GateInPushTime: $filter('date')(mData.GateInPushTime, 'yyyy-MM-dd HH:mm:ss'),
                    AppointmentCreateDate: (mData.AppointmentCreateDate == null || mData.AppointmentCreateDate == undefined) ? null : $filter('date')(mData.AppointmentCreateDate, 'yyyy-MM-dd HH:mm:ss'),
                    


                }]);
            },

            getTowass: function(VIN) {
                var res = $http.get('/api/as/Towass/getFieldAction/0/' + VIN);
                return res;
            },

            getCheckData: function(checkTypeId) {
                var url = '/api/as/AfterSalesCheck/GetCheckData/?TypeId=' + checkTypeId;
                var res = $http.get(url);
                return res;
            },

            createWOBPnoJobId: function(mData, mDataCrm, mDataDM, JobRequest, JobComplaint, JobList, oplData) {
                console.log('createWOBPnoJobId');
                var a = new Date();
                var yearFirst = a.getFullYear();
                var monthFirst = a.getMonth() + 1;
                var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                var dayFirst = a.getDate();
                var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                var hour = a.getHours();
                var hours = hour < 10 ? '0' + hour : hour;
                var minutes = a.getMinutes();
                var minutess = minutes < 10 ? '0' + minutes : minutes;
                var seconds = a.getSeconds();
                var secondss = seconds < 10 ? '0' + seconds : seconds;
                var WoCreatedDateX = yearFirst + '-' + monthFirsts + '-' + dayFirsts + 'T' + hours + ':' + minutess + ':' + secondss;
                JobList = this.validationNonTaskList(JobList)
                if (mDataCrm.Vehicle.isNonTAM == 1){
                    mData.VehicleModelName = mDataCrm.Vehicle.ModelName 
                }

                JobList = this.isiGuidLogTaskPartOPL(JobList,mData.GUIDWOBP,1)

                return $http.put('/api/as/Jobs/updateDetil/2', [{
                    ToyotaIdRequest: mDataCrm.Customer.ToyotaIDFlag == null ? 0 : mDataCrm.Customer.ToyotaIDFlag,
                    isAppointment: mData.isAppointment == null || typeof mData.isAppointment == "undefined" ? 0 : mData.isAppointment,
                    AppointmentDate: mData.AppointmentDate,
                    AppointmentTime: mData.AppointmentTime,
                    AppointmentNo: mData.AppointmentNo,
                    JobId: 0,
                    StallId: mData.StallId === 0 ? null : mData.StallId,
                    OutletId: currUser.OrgId,
                    TechnicianAction: 1,
                    VehicleId: mDataCrm.Vehicle.VehicleId,
                    JobSuggest: 1,
                    JobTask: JobList,
                    JobComplaint: JobComplaint,
                    JobRequest: JobRequest,
                    isGr: 0,
                    Km: mData.KmNormal,
                    isCash: mData.isCash,
                    isSpk: mData.isSpk,
                    SpkNo: mData.SpkNo ? mData.SpkNo : '-',
                    // EstimationDate: mData.EstimateDeliveryTime,
                    // EstimationDate: mData.EstimationDate,
                    EstimationDate: mData.ClickLanjut,
                    WoCategoryId: mData.WoCategoryId,
                    JobDate: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
                    KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    VehicleTypeId: mDataCrm.Vehicle.VehicleTypeId,
                    PoliceNumber: mDataCrm.Vehicle.LicensePlate == null ? mData.PoliceNumber : mDataCrm.Vehicle.LicensePlate,
                    FinalDP: mData.totalDp,

                    DPPaidAmount: mData.DPPaidAmount,
                    ORPaidAmount: mData.ORPaidAmount,
                    DPRawatJalan: mData.DPRawatJalan,
                    IsPaidRawatJalan: mData.IsPaidRawatJalan,

                    // FinalDP: mData.flag.service == 1 ? mData.totalDpDefault : mData.TotalSummaryPrice,
                    ModelType: mData.VehicleModelName,
                    JobTaskOpl: oplData,
                    InsuranceName: mData.InsuranceName,
                    ActiveChip: mData.ActiveChip,
                    DecisionMaker: mDataDM.PK.Name ? mDataDM.PK.Name : null,
                    PhoneDecisionMaker1: mDataDM.PK.HP1 ? mDataDM.PK.HP1 : null,
                    PhoneDecisionMaker2: mDataDM.PK.HP2 ? mDataDM.PK.HP2 : null,
                    ContactPerson: mDataDM.CP.Name ? mDataDM.CP.Name : null,
                    PhoneContactPerson1: mDataDM.CP.HP1 ? mDataDM.CP.HP1 : null,
                    PhoneContactPerson2: mDataDM.CP.HP2 ? mDataDM.CP.HP2 : null,
                    UsedPart: mData.flag.parts,
                    IsWaiting: mData.flag.wait,
                    IsWash: mData.flag.wash,
                    ServiceWithVehicle: mData.flag.service,
                    WoCreatedDate: WoCreatedDateX,
                    UserIdSa: currUser.UserId,
                    UserIdApp: mData.UserIdApp,
                    JobCatgForBPid: mData.JobCatgForBP,
                    // FixedDeliveryTime1: mData.EstimateDeliveryTime,
                    // FixedDeliveryTime2: mData.AdjusmentDate,
                    FixedDeliveryTime1: $filter('date')(mData.EstimateDeliveryTime, 'yyyy-MM-dd HH:mm:ss'),
                    FixedDeliveryTime2: $filter('date')(mData.AdjusmentDate, 'yyyy-MM-dd HH:mm:ss'),
                    FixedDeliveryTime: mData.FixedDeliveryTime,
                    PaymentMethod: mData.PaymentMethod,
                    invoiceType: mData.CodeTransaction,
                    PlanDateStart: mData.PlanDateStart,
                    PlanDateFinish: mData.PlanDateFinish,
                    // VehicleTypeId: mData.VehicleTypeId,
                    ModelType: mData.VehicleModelName,
                    VehicleConditionTypeId: mData.VehicleConditionTypeId,
                    VehicleColor: mData.VehicleColor,
                    QueueId: mData.QueueId,
                    // ServWithoutVehicAmount: mData.TotalSummaryPrice
                    IsCustomerRequest : mData.IsCustomerRequest != 1 ? 0 : 1,
                    GateInPushTime: $filter('date')(mData.GateInPushTime, 'yyyy-MM-dd HH:mm:ss'),
                    AppointmentCreateDate: (mData.AppointmentCreateDate == null || mData.AppointmentCreateDate == undefined) ? null : $filter('date')(mData.AppointmentCreateDate, 'yyyy-MM-dd HH:mm:ss'),
                    Guid_Log: mData.GUIDWOBP,


                }]);
            },

            releaseWOBP: function(convertedDate, mData, mDataCrm, mDataDM, JobRequest, JobComplaint, JobList, gridEstTime, oplData, JobId) {
                console.log('convertedDate factory', convertedDate);
                console.log('convertedDate mData', mData);
                console.log('convertedDate mDataCrm', mDataCrm);
                console.log('convertedDate mDataDM', mDataDM);
                console.log('convertedDate JobList', JobList);

                console.log(' convertedDate mDataDM totalDpDefault =======>',mData.totalDpDefault);
                console.log(' convertedDate mDataDM TotalSummaryPrice ====>',mData.TotalSummaryPrice);
                // var filtered = JobList.filter(function(item) { 
                //    return item.EstimationBPId == undefined;  
                // });

                for(var t in JobList){
                    if(JobList[t].EstimationBPId != undefined){
                        JobList[t].IsCustomDiscount = 0; 
                        if(JobList[t].JobParts != undefined){
                            for(var k in JobList[t].JobParts){
                                JobList[t].JobParts[k].IsCustomDiscount = 0;
                            }
                        }
                        // JobList[t].EstimationBPId ;
                        // JobList[t].EstimationBPId ;
                        // JobList[t].EstimationBPId ;
                    }
                }

            
                // JobList = filtered;
                // var AdjusmentDate = angular.copy(mData.AdjusmentDate);
                // var yearFirst = AdjusmentDate.getFullYear();
                // var monthFirst = AdjusmentDate.getMonth() + 1;
                // var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                // var dayFirst = AdjusmentDate.getDate();
                // var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                // var hour = AdjusmentDate.getHours();
                // var hours = hour < 10 ? '0' + hour : hour;
                // var minutes = AdjusmentDate.getMinutes();
                // var minutess = minutes < 10 ? '0' + minutes : minutes;
                // var FixedDeliveryTime2 = yearFirst + '-' + monthFirsts + '-' + dayFirsts + 'T' + hours + ':' + minutess;
                
                var FixedDeliveryTime2 = angular.copy(mData.AdjusmentDate);

                var a = new Date();
                var yearFirst = a.getFullYear();
                var monthFirst = a.getMonth() + 1;
                var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                var dayFirst = a.getDate();
                var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                var hour = a.getHours();
                var hours = hour < 10 ? '0' + hour : hour;
                var minutes = a.getMinutes();
                var minutess = minutes < 10 ? '0' + minutes : minutes;
                var seconds = a.getSeconds();
                var secondss = seconds < 10 ? '0' + seconds : seconds;
                var WoCreatedDateX = yearFirst + '-' + monthFirsts + '-' + dayFirsts + 'T' + hours + ':' + minutess + ':' + secondss;
                mData.AppointmentDate = this.changeFormatDate(mData.AppointmentDate);

                if (mData.isWoFromBpSatelit != 1){ // kl 1 itu wo dari satelite, kl 0 itu wo bp biasa
                    mData.isWoFromBpSatelit = 0;
                }
                JobList = this.validationNonTaskList(JobList)


                // =================================================+
                //isi log buat task part opl ============================== start
                JobList = this.isiGuidLogTaskPartOPL(JobList,mData.GUIDWOBP,1)
                oplData = this.isiGuidLogTaskPartOPL(oplData,mData.GUIDWOBP,2)
                //isi log buat task part opl ============================== end

                if (mDataCrm.Vehicle.isNonTAM == 1){
                    mData.VehicleModelName = mDataCrm.Vehicle.ModelName 
                }

                var pdstart = new Date();

                return $http.put('/api/as/Jobs/updateDetil/2?createNo=1', [{
                    isAppointment: mData.isAppointment == null || typeof mData.isAppointment == "undefined" ? 0 : mData.isAppointment,
                    AppointmentDate: mData.AppointmentDate,
                    AppointmentTime: mData.AppointmentTime,
                    AppointmentNo: mData.AppointmentNo,
                    ToyotaIdRequest: mDataCrm.Customer.ToyotaIDFlag == null ? 0 : mDataCrm.Customer.ToyotaIDFlag,
                    JobId: JobId,
                    StallId: mData.StallId === 0 ? null : mData.StallId,
                    OutletId: currUser.OrgId,
                    TechnicianAction: 1,
                    VehicleId: mDataCrm.Vehicle.VehicleId,
                    JobCatgForBPid: mData.JobCatgForBPid,
                    JobSuggest: 1,
                    JobTask: JobList,
                    JobComplaint: JobComplaint,
                    JobRequest: JobRequest,
                    isGr: 0,
                    Km: mData.KmNormal,
                    isCash: mData.isCash,
                    isSpk: mData.isSpk,
                    SpkNo: mData.SpkNo ? mData.SpkNo : '-',
                    // EstimationDate: mData.EstimateDeliveryTime,
                    // EstimationDate: convertedDate,
                    // EstimationDate: mData.EstimationDate,
                    EstimationDate: mData.ClickLanjut,
                    WoCategoryId: mData.WoCategoryId,
                    JobDate: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
                    KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    VehicleTypeId: mDataCrm.Vehicle.VehicleTypeId,
                    PoliceNumber: mDataCrm.Vehicle.LicensePlate == null ? mData.PoliceNumber : mDataCrm.Vehicle.LicensePlate,
                    FinalDP: mData.totalDp,

                    DPPaidAmount: mData.DPPaidAmount,
                    ORPaidAmount: mData.ORPaidAmount,
                    DPRawatJalan: mData.DPRawatJalan,
                    IsPaidRawatJalan: mData.IsPaidRawatJalan,

                    // FinalDP: mData.flag.service == 1 ? this.findTotalDPDefaul(JobList) : mData.TotalSummaryPrice,
                    // FinalDP: mData.flag.service == 1 ? totalDpDefault : mData.TotalSummaryPrice,
                    ModelType: mData.VehicleModelName,
                    JobTaskOpl: oplData,
                    InsuranceId: mData.InsuranceId,
                    InsuranceName: mData.InsuranceName,
                    ActiveChip: mData.ActiveChip,
                    DecisionMaker: mDataDM.PK.Name ? mDataDM.PK.Name : null,
                    PhoneDecisionMaker1: mDataDM.PK.HP1 ? mDataDM.PK.HP1 : null,
                    PhoneDecisionMaker2: mDataDM.PK.HP2 ? mDataDM.PK.HP2 : null,
                    ContactPerson: mDataDM.CP.Name ? mDataDM.CP.Name : null,
                    PhoneContactPerson1: mDataDM.CP.HP1 ? mDataDM.CP.HP1 : null,
                    PhoneContactPerson2: mDataDM.CP.HP2 ? mDataDM.CP.HP2 : null,
                    UsedPart: mData.flag.parts,
                    IsWaiting: mData.flag.wait,
                    IsWash: mData.flag.wash,
                    ServiceWithVehicle: mData.flag.service,
                    WoCreatedDate: WoCreatedDateX,
                    UserIdSa: currUser.UserId,
                    UserIdApp: mData.UserIdApp,
                    // JobCatgForBPid: mData.JobCatgForBP,
                    // FixedDeliveryTime1: mData.EstimateDeliveryTime,
                    // FixedDeliveryTime2: FixedDeliveryTime2,
                    FixedDeliveryTime1: $filter('date')(mData.EstimateDeliveryTime, 'yyyy-MM-dd HH:mm:ss'),
                    FixedDeliveryTime2: $filter('date')(mData.AdjusmentDate, 'yyyy-MM-dd HH:mm:ss'),
                    FixedDeliveryTime: mData.FixedDeliveryTime,
                    PaymentMethod: mData.PaymentMethod,
                    invoiceType: mData.CodeTransaction,
                    // PlanDateStart: mData.PlanDateStart,
                    // PlanDateFinish: mData.PlanDateFinish,
                    PlanDateStart: $filter('date')(pdstart, 'yyyy-MM-dd HH:mm:ss'),
                    // PlanDateFinish: mData.EstimateDeliveryTime,
                    PlanDateFinish: convertedDate,
                    // VehicleTypeId: mData.VehicleTypeId,
                    ModelType: mData.VehicleModelName,
                    ORAmount: mData.ORAmount,
                    VehicleConditionTypeId: mData.VehicleConditionTypeId,
                    VehicleColor: mData.VehicleColor,
                    Guid_Log: mData.GUIDWOBP,
                    // ServWithoutVehicAmount: mData.TotalSummaryPrice
                    isWoFromBpSatelit: mData.isWoFromBpSatelit,
                    BpSatelitOutletId: mData.BpSatelitOutletId,
                    BpSatelitSAUserId: mData.BpSatelitSAUserId,
                    BSTKNo: mData.BSTKNo,
                    QueueId: mData.QueueId,
                    IsCustomerRequest : mData.IsCustomerRequest != 1 ? 0 : 1,
                    GateInPushTime: $filter('date')(mData.GateInPushTime, 'yyyy-MM-dd HH:mm:ss'),
                    AppointmentCreateDate: (mData.AppointmentCreateDate == null || mData.AppointmentCreateDate == undefined) ? null : $filter('date')(mData.AppointmentCreateDate, 'yyyy-MM-dd HH:mm:ss'),



                }] );
                // =========================================
                // result();
                // console.log('releaseWOBP', result);
                // debugger;
                // return result;


                // JobId: 0,
                // JobTask: JobList,
                // JobComplaint: JobComplaint,
                // JobRequest: JobRequest,
                // JobWAC: mData.JobWAC,
                // JobNo: mData.JobNo,
                // OutletId: currUser.OrgId,
                // CalId: mData.CalId,
                // StallId: mData.StallId,
                // Stall: mData.Stall,
                // VehicleId: mDataCrm.Vehicle.VehicleId,
                // PlanStart: mData.PlanStart,
                // PlanFinish: mData.PlanFinish,
                // CustRequest: mData.CustRequest,
                // CustComplaint: mData.CustComplaint,
                // TechnicianAction: mData.TechnicianAction1,
                // JobSuggest: mData.JobSuggest1,
                // T1: mData.T1,
                // T2: mData.T2,
                // RSA: mData.RSA,
                // AdditionalTime: mData.AdditionalTime,
                // JobType: mData.JobType,
                // Status: mData.Status,
                // AppointmentDate: mData.AppointmentDate,
                // AppointmentTime: mData.AppointmentTime,
                // AppointmentNo: mData.AppointmentNo,
                // isAppointment: mData.isAppointment,
                // AppointmentVia: mData.AppointmentVia,
                // isGr: 1,
                // Km: mData.Km,
                // isCash: mData.isCash,
                // isSpk: mData.isSpk,
                // SpkNo: mData.SpkNo ? mData.SpkNo : '-',
                // InsuranceName: mData.InsuranceName,
                // isWOBase: mData.isWOBase,
                // WoCategoryId: mData.WoCategoryId,
                // WoNo: mData.WoNo,
                // IsEstimation: mData.IsEstimation,
                // EstimationDate: mData.EstimateDeliveryTime,
                // EstimationNo: mData.EstimationNo,
                // JobDate: mData.JobDate,
                // PermissionPartChange: mData.PermissionPartChange,
                // PaymentMethod: mData.PaymentMethod,
                // DecisionMaker: mDataDM.PK.Name,
                // PhoneDecisionMaker1: mDataDM.PK.HP1,
                // PhoneDecisionMaker2: mDataDM.PK.HP2,
                // ContactPerson: mDataDM.CP.Name,
                // PhoneContactPerson1: mDataDM.CP.HP1,
                // PhoneContactPerson2: mDataDM.CP.HP2,
                // UsedPart: mData.flag.parts,
                // IsWaiting: mData.flag.wait,
                // IsWash: mData.flag.wash,
                // PermissionPartChange: mData.flag.cPart,
                // FixedDeliveryTime1: mData.EstimateDate,
                // FixedDeliveryTime2: mData.Adjusment,
                // invoiceType: mData.CodeTransaction,
                // JobTaskOpl: oplData,
                // ModelType: mData.ModelType,
                // PoliceNumber: mData.PoliceNumber,
                // isFoOneHour: mData.isFoOneHour,
                // isFoOneDay: mData.isFoOneDay,
                // HandOverDate: mData.HandOverDate,
                // KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                // Revision: mData.Revision,
                // NewAppointmentRel: mData.NewAppointmentRel,
                // RescheduleReason: mData.RescheduleReason,
                // JobCatgForBPid: mData.JobCatgForBPid,
                // JobCatgForBP: mData.JobCatgForBP,
                // ActiveChip: mData.ActiveChip,
                // IsStopPage: mData.IsStopPage,
                // Suggestion: mData.Suggestion,
                // SuggestionCatg: mData.SuggestionCatg,
                // SuggestionDate: mData.SuggestionDate,
                // PlanDateStart: mData.PlanDateStart,
                // PlanDateFinish: mData.PlanDateFinish,
                // closeWoId: mData.closeWoId,
                // CloseWODate: mData.CloseWODate,
                // MaterialRequestNo: mData.MaterialRequestNo,
                // MaterialRequestStatusId: mData.MaterialRequestStatusId,
                // WoCreatedDate: WoCreatedDateX,
                // GateInPushTime: mData.GateInPushTime,
                // GateInUserId: mData.GateInUserId,
                // CarWashTime: mData.CarWashTime,
                // CarWashUserId: mData.CarWashUserId,
                // TecoTime: mData.TecoTime,
                // TecoUserId: mData.TecoUserId,
                // BillingCreatedTime: mData.BillingCreatedTime,
                // BillingUserId: mData.BillingUserId,
                // CustCallTime: mData.CustCallTime,
                // CustCallUserId: mData.CustCallUserId,
                // ServiceExplainCallTime: mData.ServiceExplainCallTime,
                // ServiceExplainUserId: mData.ServiceExplainUserId,
                // AdditionalTimeAmount: mData.AdditionalTimeAmount,
                // IsAllPulledBilling: mData.IsAllPulledBilling,
                // IsInStopPageGr: mData.IsInStopPageGr,
                // StopPageTimeGr: mData.StopPageTimeGr,
                // StopPageUserIdGr: mData.StopPageUserIdGr,
                // VendorCode: mData.VendorCode,
                // ModelCode: mData.ModelCode,
                // DealerCode: mData.DealerCode,
                // Email: mData.Email,
                // CancelDate: mData.CancelDate,
                // CancelReason: mData.CancelReason,
                // CancelUserId: mData.CancelUserId,
                // isWoFromBpSatelit: mData.isWoFromBpSatelit,
                // BpSatelitOutletId: mData.BpSatelitOutletId,
                // BpSatelitSAUserId: mData.BpSatelitSAUserId,
                // isReqOutPatient: mData.isReqOutPatient,
                // OutPatientUserIdRequest: mData.OutPatientUserIdRequest,
                // OutPatientReqDate: mData.OutPatientReqDate,
                // isOutPatient: mData.isOutPatient,
                // OutPatientUserIdApproved: mData.OutPatientUserIdApproved,
                // OutPatientApproveDate: mData.OutPatientApproveDate,
                // OutPatientApprovedNo: mData.OutPatientApprovedNo,
                // OutPatientUserIdReject: mData.OutPatientUserIdReject,
                // OutPatientRejectDate: mData.OutPatientRejectDate,
                // OutPatientFinishDate: mData.OutPatientFinishDate,
                // SKB: mData.SKB,
                // UserIdSa: mData.UserIdSa,
                // UserSa: mData.UserSa,
                // MProfileForemanId: mData.MProfileForemanId,
                // MProfileForeman: mData.MProfileForeman,
                // UserIdApp: mData.UserIdApp,
                // UserApp: mData.UserApp,
                // AppointmentCreateDate: mData.AppointmentCreateDate,
                // GroupBPId: mData.GroupBPId,
                // groupBP: mData.groupBP,
                // FixedDeliveryTime1: mData.FixedDeliveryTime1,
                // FixedDeliveryTime2: mData.FixedDeliveryTime2,
                // ORAmount: mData.ORAmount,
                // InsuranceId: mData.InsuranceId,
                // Insurance: mData.Insurance,
                // VehicleTypeId: mData.VehicleTypeId,
                // UserIdWORelease: mData.UserIdWORelease,
                // isDraftWo: mData.isDraftWo,
                // AssemblyYearForEstimation: mData.AssemblyYearForEstimation,
                // SendToTowasDate: mData.SendToTowasDate,
                // ReceiveFromTowasDate: mData.ReceiveFromTowasDate,
                // BillingInsuranceType: mData.BillingInsuranceType,
                // statusTowas: mData.statusTowas,
                // responseStatusFromTowas: mData.responseStatusFromTowas,
                // towasConfirmedBySA: mData.towasConfirmedBySA,
                // towasConfirmedUserIdSA: mData.towasConfirmedUserIdSA,
                // WarantyNoFromTowas: mData.WarantyNoFromTowas,
                // AcceptableAmount: mData.AcceptableAmount,
                // ErrCode: mData.ErrCode,
                // TechnicianNotes: mData.TechnicianNotes,
                // ClaimAmount: mData.ClaimAmount,
                // ApprovalRoleId: mData.ApprovalRoleId,
                // ApprovalUserId: mData.ApprovalUserId,
                // ApprovalBy: mData.ApprovalBy,
                // ApprovalTime: mData.ApprovalTime,
                // ApprovalStatus: mData.ApprovalStatus,
                // AddressForEstimation: mData.AddressForEstimation
            },
            releaseBSTK: function(mData, mDataCrm, mDataDM, gridEstTime) {
                var a = new Date();
                var yearFirst = a.getFullYear();
                var monthFirst = a.getMonth() + 1;
                var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                var dayFirst = a.getDate();
                var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                var hour = a.getHours();
                var hours = hour < 10 ? '0' + hour : hour;
                var minutes = a.getMinutes();
                var minutess = minutes < 10 ? '0' + minutes : minutes;
                var seconds = a.getSeconds();
                var secondss = seconds < 10 ? '0' + seconds : seconds;
                var WoCreatedDateX = yearFirst + '-' + monthFirsts + '-' + dayFirsts + 'T' + hours + ':' + minutess + ':' + secondss;
                // JobList = this.validationNonTaskList(JobList)

                if (mDataCrm.Vehicle.isNonTAM == 1){
                    mData.VehicleModelName = mDataCrm.Vehicle.ModelName 
                }

                return $http.put('/api/as/Jobs/updateDetil/4?createNo=1', [{
                    JobId: 0,
                    OutletId: currUser.OrgId,
                    TechnicianAction: 1,
                    JobSuggest: 1,
                    StallId: null,
                    VehicleId: mDataCrm.Vehicle.VehicleId,
                    isGr: 0,
                    Km: mData.KmNormal,
                    EstimationDate: mData.EstimateDeliveryTime,
                    WoCategoryId: mData.WoCategoryId,
                    ModelCode: mDataCrm.Vehicle.ModelCode,
                    JobDate: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
                    ModelType: mData.VehicleModelName,
                    KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    VehicleTypeId: mDataCrm.Vehicle.VehicleTypeId,
                    PoliceNumber: mDataCrm.Vehicle.LicensePlate,
                    InsuranceName: mData.InsuranceName,
                    ActiveChip: mData.ActiveChip,
                    DecisionMaker: mDataDM.PK.Name,
                    PhoneDecisionMaker1: mDataDM.PK.HP1,
                    PhoneDecisionMaker2: mDataDM.PK.HP2,
                    ContactPerson: mDataDM.CP.Name,
                    PhoneContactPerson1: mDataDM.CP.HP1,
                    PhoneContactPerson2: mDataDM.CP.HP2,
                    WoCreatedDate: WoCreatedDateX,
                    JobCatgForBPid: mData.JobCatgForBP,
                    UserIdSa: currUser.UserId,
                    QueueId: mData.QueueId,
                    IsCustomerRequest : mData.IsCustomerRequest != 1 ? 0 : 1,
                    GateInPushTime: $filter('date')(mData.GateInPushTime, 'yyyy-MM-dd HH:mm:ss'),


                    DPPaidAmount: mData.DPPaidAmount,
                    ORPaidAmount: mData.ORPaidAmount,
                    DPRawatJalan: mData.DPRawatJalan,
                    IsPaidRawatJalan: mData.IsPaidRawatJalan,
                    AppointmentCreateDate: (mData.AppointmentCreateDate == null || mData.AppointmentCreateDate == undefined) ? null : $filter('date')(mData.AppointmentCreateDate, 'yyyy-MM-dd HH:mm:ss'),



                }]);
                // return $http.put('/api/as/Jobs/updateDetil/3?createNo=1', [{
                //     JobId: 0,
                //     OutletId: currUser.OrgId,
                //     TechnicianAction: 1,
                //     JobType: 0,
                //     Status: 0,
                //     JobTask: mData.JobTask,
                //     JobComplaint: mData.JobComplaint,
                //     JobRequest: mData.JobRequest,
                //     AppointmentNo: null,
                //     NewAppointmentRel: null,
                //     isAppointment: 0,
                //     isGr: 0,
                //     isWOBase: 1,
                //     AppointmentVia: 0,
                //     Process: null,
                //     Km: mData.Km,
                //     WoNo: null,
                //     ModelCode: mDataCrm.Vehicle.ModelCode,
                //     DecisionMaker: mDataDM.PK.Name,
                //     PhoneDecisionMaker1: mDataDM.PK.HP1,
                //     PhoneDecisionMaker2: mDataDM.PK.HP2,
                //     ContactPerson: mDataDM.CP.Name,
                //     PhoneContactPerson1: mDataDM.CP.HP1,
                //     PhoneContactPerson2: mDataDM.CP.HP2,
                //     AssemblyYearForEstimation: mDataCrm.Vehicle.AssemblyYear,
                //     // IsEstimation: 0,
                //     PoliceNumber: mDataCrm.Vehicle.LicensePlate,
                //     ModelType: mDataCrm.Vehicle.TypeCode,
                //     KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                //     EstimationDate: mData.EstimationDate,
                //     VehicleTypeId: mDataCrm.Vehicle.VehicleTypeId,
                //     EstimationNo: "",
                //     WoCategoryId: mData.WoCategoryId,
                //     JobDate: new Date(),
                // }]);
            },

            releaseBSTKCenter: function(mData, mDataCrm, mDataDM, gridEstTime) {
                var a = new Date();
                var yearFirst = a.getFullYear();
                var monthFirst = a.getMonth() + 1;
                var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                var dayFirst = a.getDate();
                var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                var hour = a.getHours();
                var hours = hour < 10 ? '0' + hour : hour;
                var minutes = a.getMinutes();
                var minutess = minutes < 10 ? '0' + minutes : minutes;
                var seconds = a.getSeconds();
                var secondss = seconds < 10 ? '0' + seconds : seconds;
                var WoCreatedDateX = yearFirst + '-' + monthFirsts + '-' + dayFirsts + 'T' + hours + ':' + minutess + ':' + secondss;
                mData.JobTask = this.validationNonTaskList(mData.JobTask)

                if (mDataCrm.Vehicle.isNonTAM == 1){
                    mData.VehicleModelName = mDataCrm.Vehicle.ModelName 
                }

                return $http.put('/api/as/Jobs/updateDetilBSTK/4?createNo=1', [{
                    JobId: 0,
                    OutletId: currUser.OrgId,
                    TechnicianAction: 1,
                    JobSuggest: 1,
                    StallId: null,
                    VehicleId: mDataCrm.Vehicle.VehicleId,
                    isGr: 0,
                    Km: mData.KmNormal,
                    // EstimationDate: mData.EstimateDeliveryTime, //pake yg di bawah, yg ini ga ada isi na terakhir di cek
                    WoCategoryId: mData.WoCategoryId,
                    ModelCode: mDataCrm.Vehicle.ModelCode,
                    JobDate: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
                    ModelType: mData.VehicleModelName,
                    KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    VehicleTypeId: mDataCrm.Vehicle.VehicleTypeId,
                    PoliceNumber: mDataCrm.Vehicle.LicensePlate,
                    InsuranceName: mData.InsuranceName,
                    ActiveChip: mData.ActiveChip,
                    DecisionMaker: mDataDM.PK.Name,
                    PhoneDecisionMaker1: mDataDM.PK.HP1,
                    PhoneDecisionMaker2: mDataDM.PK.HP2,
                    ContactPerson: mDataDM.CP.Name,
                    PhoneContactPerson1: mDataDM.CP.HP1,
                    PhoneContactPerson2: mDataDM.CP.HP2,
                    WoCreatedDate: WoCreatedDateX,
                    JobCatgForBPid: mData.JobCatgForBPid,
                    UserIdSa: currUser.UserId,
                    isWoFromBpSatelit: 1, // khusus untuk releaseBSTK wo dr sa GR (satelite)
                    JobTask: mData.JobTask,
                    JobComplaint: mData.JobComplaint,
                    JobRequest: mData.JobRequest,
                    VehicleConditionTypeId: mData.VehicleConditionTypeId,
                    EstimationDate: mData.EstimationDate,
                    GateInPushTime: $filter('date')(mData.GateInPushTime, 'yyyy-MM-dd HH:mm:ss'),

                    FinalDP: mData.totalDp,

                    DPPaidAmount: mData.DPPaidAmount,
                    ORPaidAmount: mData.ORPaidAmount,
                    DPRawatJalan: mData.DPRawatJalan,
                    IsPaidRawatJalan: mData.IsPaidRawatJalan,
                    AppointmentCreateDate: (mData.AppointmentCreateDate == null || mData.AppointmentCreateDate == undefined) ? null : $filter('date')(mData.AppointmentCreateDate, 'yyyy-MM-dd HH:mm:ss'),


                }]);
                // return $http.put('/api/as/Jobs/updateDetil/3?createNo=1', [{
                //     JobId: 0,
                //     OutletId: currUser.OrgId,
                //     TechnicianAction: 1,
                //     JobType: 0,
                //     Status: 0,
                //     JobTask: mData.JobTask,
                //     JobComplaint: mData.JobComplaint,
                //     JobRequest: mData.JobRequest,
                //     AppointmentNo: null,
                //     NewAppointmentRel: null,
                //     isAppointment: 0,
                //     isGr: 0,
                //     isWOBase: 1,
                //     AppointmentVia: 0,
                //     Process: null,
                //     Km: mData.Km,
                //     WoNo: null,
                //     ModelCode: mDataCrm.Vehicle.ModelCode,
                //     DecisionMaker: mDataDM.PK.Name,
                //     PhoneDecisionMaker1: mDataDM.PK.HP1,
                //     PhoneDecisionMaker2: mDataDM.PK.HP2,
                //     ContactPerson: mDataDM.CP.Name,
                //     PhoneContactPerson1: mDataDM.CP.HP1,
                //     PhoneContactPerson2: mDataDM.CP.HP2,
                //     AssemblyYearForEstimation: mDataCrm.Vehicle.AssemblyYear,
                //     // IsEstimation: 0,
                //     PoliceNumber: mDataCrm.Vehicle.LicensePlate,
                //     ModelType: mDataCrm.Vehicle.TypeCode,
                //     KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                //     EstimationDate: mData.EstimationDate,
                //     VehicleTypeId: mDataCrm.Vehicle.VehicleTypeId,
                //     EstimationNo: "",
                //     WoCategoryId: mData.WoCategoryId,
                //     JobDate: new Date(),
                // }]);
            },

            getWACBPbyJobId: function (jobid) {
                // var WAC = function
                return $http.get('/api/as/JobWACPBs/getByJob/' + jobid);
            },
            createWACBPbyJobId: function(data) {
                // var WAC = function
                return $http.post('/api/as/JobWACBPs/NewList', data);
            },
            updateWACBPbyJobId: function(data) {
                // var WAC = function
                return $http.put('/api/as/JobWACBPs/UpdateList', data);
            },
            createWOBPEstimation: function(jobId, dataEst) {
                return $http.put('/api/as/jobs/BpEstimation/' + jobId, [{
                    JobTasks: [],
                    JobTaskBP: dataEst
                }])
            },

            updateBPEstimation: function(jobId, arrTaskwithId) {
                for(var a in arrTaskwithId){
                    if(arrTaskwithId[a].Putty == null){
                        arrTaskwithId[a].Putty = 0;
                    }
                    if(arrTaskwithId[a].Body == null){
                        arrTaskwithId[a].Body = 0;
                    }
                    if(arrTaskwithId[a].Surfacer == null){
                        arrTaskwithId[a].Surfacer = 0;
                    }
                    if(arrTaskwithId[a].Spraying == null){
                        arrTaskwithId[a].Spraying = 0;
                    }
                    if(arrTaskwithId[a].Polishing == null){
                        arrTaskwithId[a].Polishing = 0;
                    }
                    if(arrTaskwithId[a].ReAssembly == null){
                        arrTaskwithId[a].ReAssembly = 0;
                    }
                    if(arrTaskwithId[a].FinalInspection == null){
                        arrTaskwithId[a].FinalInspection = 0;
                    }
                    arrTaskwithId[a].isFI = 0 // karena baru release wo, ga mungkin isfi na 1
                    console.log("arrTaskwithId[a]",arrTaskwithId[a]);
                }
                return $http.put('/api/as/updateBPEstimation/' + jobId, [{
                    Jobtasks: arrTaskwithId,
                    // JobTaskBP: dataEst
                }])
            },

            createWOBPEstimationBSTK: function(jobId, dataEst) {
                return $http.put('/api/as/jobs/BpEstimationBSTK/' + jobId, [{
                    JobTasks: [],
                    JobTaskBP: dataEst
                }])
            },

            getVehicleList: function(param1, param2, isSatelite) {
                if (isSatelite != true){
                    return $http.get('/api/crm/GetVehicleListSpecific/' + param1 + '/' + param2);

                } else {
                    return $http.get('/api/crm/GetVehicleListSpecificSatellite/' + param1 + '/' + param2);
                }
            },

            NewGetVehicleListSpecific: function(param1, param2, isSatelite, from_clickbutton) {
                if (isSatelite != true){
                    if (from_clickbutton == 1) {
                        return $http.get('/api/crm/NewGetVehicleListSpecific/' + param1 + '/' + param2);
                    } else {
                        return $http.get('/api/crm/NewGetVehicleListSpecificNotSync/' + param1 + '/' + param2);
                    }

                } else {
                    if (from_clickbutton == 1) {
                        return $http.get('/api/crm/GetVehicleListSpecificSatellite_V2/' + param1 + '/' + param2);
                    } else {
                        return $http.get('/api/crm/GetVehicleListSpecificSatelliteNoSync_V2/' + param1 + '/' + param2);
                    }
                }
            },

            NewGetVehicleListSpecificNotSync: function(param1, param2, isSatelite) {
                // return $http.get('/api/crm/NewGetVehicleListSpecificNotSync/' + param1 + '/' + param2);
                if (isSatelite != true){
                    return $http.get('/api/crm/NewGetVehicleListSpecificNotSync/' + param1 + '/' + param2);

                } else {
                    return $http.get('/api/crm/GetVehicleListSpecificSatelliteNoSync_V2/' + param1 + '/' + param2);
                }
            },

            getVehicleMList: function() {
                return $http.get('/api/crm/GetCVehicleModel/');
            },
            getVehicleTList: function() {
                return $http.get('/api/crm/GetCVehicleType/');
            },
            createNewVehicleList: function(data, isSatelite) {
                console.log('data createNewVehicleList', data);
                if (isSatelite != true){
                    return $http.post('/api/crm/PostVehicleSP/' + data.Vehicle.Color.ColorId + '/' + data.Vehicle.Type.VehicleTypeId, [{
                        VIN: data.Vehicle.VIN,
                        LicensePlate: data.Vehicle.LicensePlate,
                        EngineNo: data.Vehicle.EngineNo,
                        AssemblyYear: data.Vehicle.AssemblyYear,
                        DECDate: data.Vehicle.DECDate,
                        // VehicleTypeColorId: data.Vehicle.ColorId,
                        STNKName: data.Vehicle.STNKName,
                        STNKAddress: data.Vehicle.STNKAddress,
                        STNKDate: data.Vehicle.STNKDate,
                        GasTypeId: data.Vehicle.GasTypeId,
                        isNonTAM: data.Vehicle.isNonTAM,
                        OutletId: currUser.OrgId
                            // "StatusCode": 1,
    
                    }]);
                } else {
                    return $http.post('/api/crm/PostVehicleSatellite/' + data.Vehicle.Color.ColorId + '/' + data.Vehicle.Type.VehicleTypeId, [{
                        VIN: data.Vehicle.VIN,
                        LicensePlate: data.Vehicle.LicensePlate,
                        EngineNo: data.Vehicle.EngineNo,
                        AssemblyYear: data.Vehicle.AssemblyYear,
                        DECDate: data.Vehicle.DECDate,
                        // VehicleTypeColorId: data.Vehicle.ColorId,
                        STNKName: data.Vehicle.STNKName,
                        STNKAddress: data.Vehicle.STNKAddress,
                        STNKDate: data.Vehicle.STNKDate,
                        GasTypeId: data.Vehicle.GasTypeId,
                        isNonTAM: data.Vehicle.isNonTAM,
                        OutletId: currUser.OrgId
                            // "StatusCode": 1,
    
                    }]);
                }
            },
            updateVehicleList: function(data, isSatelite) {
                var DecDate = new Date(data.Vehicle.DECDate);
                var STNKDate = null
                if (data.Vehicle.STNKDate != null && data.Vehicle.STNKDate != undefined) {
                    STNKDate = new Date(data.Vehicle.STNKDate);
                }


                // console.log('data updateVehicleList', data, data.Vehicle.DECDate.getFullYear(), data.Vehicle.DECDate.getMonth());
                var tmpDecDate = DecDate.getFullYear() + '-' + (DecDate.getMonth() + 1) + '-' + DecDate.getDay();
                // var tmpSTNKDate = STNKDate.getFullYear() + '-' + (STNKDate.getMonth() + 1) + '-' + STNKDate.getDate();
                var finalDate1
                var yyyy = DecDate.getFullYear().toString();
                var mm = (DecDate.getMonth() + 1).toString(); // getMonth() is zero-based
                var dd = DecDate.getDate().toString();
                finalDate1 = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);

                var finalDate2 = null
                if (STNKDate != null && STNKDate != undefined) {
                    var yyyy1 = STNKDate.getFullYear().toString();
                    var mm1 = (STNKDate.getMonth() + 1).toString(); // getMonth() is zero-based
                    var dd1 = STNKDate.getDate().toString();
                    finalDate2 = yyyy1 + '-' + (mm1[1] ? mm1 : "0" + mm1[0]) + '-' + (dd1[1] ? dd1 : "0" + dd1[0]);
                }
                

                console.log('generate date1', finalDate1);
                console.log('generate date2', finalDate2);
                if (isSatelite != true){
                    return $http.put('/api/crm/PutVehicle/' + data.Vehicle.Color.ColorId + '/' + data.Vehicle.Type.VehicleTypeId, [{
                        VehicleId: data.Vehicle.VehicleId,
                        VIN: data.Vehicle.VIN,
                        LicensePlate: data.Vehicle.LicensePlate,
                        EngineNo: data.Vehicle.EngineNo,
                        AssemblyYear: data.Vehicle.AssemblyYear,
                        DECDate: finalDate1,
                        // DECDate: data.Vehicle.DECDate,
                        // Added by Fyberz =========
                        Insurance: data.Vehicle.Insurance,
                        SPK: data.Vehicle.SPK,
                        // =========================
                        // VehicleTypeColorId: data.Vehicle.ColorId,
                        STNKName: data.Vehicle.STNKName,
                        STNKAddress: data.Vehicle.STNKAddress,
                        STNKDate: finalDate2,
                        SourceDataId: data.Vehicle.SourceDataId,
                        // STNKDate: data.Vehicle.STNKDate,
                        GasTypeId: data.Vehicle.GasTypeId,
                        isNonTAM: data.Vehicle.isNonTAM,
                        OutletId: currUser.OrgId
                            // "StatusCode": 1,
    
                    }]);
                } else {
                    return $http.put('/api/crm/PutVehicleSatellite/' + data.Vehicle.Color.ColorId + '/' + data.Vehicle.Type.VehicleTypeId, [{
                        VehicleId: data.Vehicle.VehicleId,
                        VIN: data.Vehicle.VIN,
                        LicensePlate: data.Vehicle.LicensePlate,
                        EngineNo: data.Vehicle.EngineNo,
                        AssemblyYear: data.Vehicle.AssemblyYear,
                        DECDate: finalDate1,
                        // DECDate: data.Vehicle.DECDate,
                        // Added by Fyberz =========
                        Insurance: data.Vehicle.Insurance,
                        SPK: data.Vehicle.SPK,
                        // =========================
                        // VehicleTypeColorId: data.Vehicle.ColorId,
                        STNKName: data.Vehicle.STNKName,
                        STNKAddress: data.Vehicle.STNKAddress,
                        STNKDate: finalDate2,
                        SourceDataId: data.Vehicle.SourceDataId,
                        // STNKDate: data.Vehicle.STNKDate,
                        GasTypeId: data.Vehicle.GasTypeId,
                        isNonTAM: data.Vehicle.isNonTAM,
                        OutletId: currUser.OrgId
                            // "StatusCode": 1,
    
                    }]);
                }
            },
            getCustomerList: function(param) {
                return $http.get('/api/crm/GetCCustomerListSpecific/' + param + '/-/-/-/-/-/-');
            },
            getCustomerVehicleList: function(value, isSatelite) {
                var param1 = value.TID ? value.TID : '-';
                if (value.TID == '' || value.TID == null || value.TID == undefined){
                    param1 = '-'
                } else {
                    param1 = value.TID
                }
                var param2 = '-';
                var param3 = '-';
                var param4 = '-';
                var param5 = '-';
                var param6 = '-';
                var param7 = '-';
                var param8 = '-';
                switch (value.flag) {
                    case 1:
                        param2 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param2 = '-'
                        } else {
                            param2 = value.filterValue
                        }
                        break;
                    case 2:
                        param3 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param3 = '-'
                        } else {
                            param3 = value.filterValue
                        }
                        break;
                    case 3:
                        param4 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param4 = '-'
                        } else {
                            param4 = value.filterValue
                        }
                        break;
                    case 4:
                        param5 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param5 = '-'
                        } else {
                            param5 = value.filterValue
                        }
                        break;
                    case 5:
                        param6 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param6 = '-'
                        } else {
                            param6 = value.filterValue
                        }
                        break;
                    case 6:
                        param7 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param7 = '-'
                        } else {
                            param7 = value.filterValue
                        }
                        break;
                    case 7:
                        // param8 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param8 = '-'
                        } else {
                            param8 = angular.copy(value.filterValue.replace(/\./g, "z")) 
                            param8 = angular.copy(param8.replace(/\-/g, "x"))

                        }

                };

                if (isSatelite != true){
                    return $http.get('/api/crm/GetCustomerListFilter/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7 + '/' + param8);
                } else {
                    return $http.get('/api/crm/GetCustomerListFilterSatellite/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7 + '/' + param8);
                }

            },

            getCustomerVehicleList_V2: function(value, isSatelite) {
                var param1 = value.TID ? value.TID : '-';
                if (value.TID == '' || value.TID == null || value.TID == undefined){
                    param1 = '-'
                } else {
                    param1 = value.TID
                }
                var param2 = '-';
                var param3 = '-';
                var param4 = '-';
                var param5 = '-';
                var param6 = '-';
                var param7 = '-';
                var param8 = '-';
                switch (value.flag) {
                    case 1:
                        param2 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param2 = '-'
                        } else {
                            param2 = value.filterValue
                        }
                        break;
                    case 2:
                        param3 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param3 = '-'
                        } else {
                            param3 = value.filterValue
                        }
                        break;
                    case 3:
                        param4 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param4 = '-'
                        } else {
                            param4 = value.filterValue
                        }
                        break;
                    case 4:
                        param5 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param5 = '-'
                        } else {
                            param5 = value.filterValue
                        }
                        break;
                    case 5:
                        param6 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param6 = '-'
                        } else {
                            param6 = value.filterValue
                        }
                        break;
                    case 6:
                        param7 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param7 = '-'
                        } else {
                            param7 = value.filterValue
                        }
                        break;
                    case 7:
                        // param8 = value.filterValue ? value.filterValue : '-';
                        if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                            param8 = '-'
                        } else {
                            param8 = angular.copy(value.filterValue.replace(/\./g, "z")) 
                            param8 = angular.copy(param8.replace(/\-/g, "x"))

                        }

                };

                if (isSatelite != true){
                    return $http.get('/api/crm/GetCustomerListFilter_V2/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7 + '/' + param8);
                } else {
                    return $http.get('/api/crm/GetCustomerListFilterSatellite_V2/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7 + '/' + param8);
                }

            },

            getDataEstimasi: function(filter) {
                // ====== yang ini service by filter nopolisi tapi tetep ajah kosong 
                // var res=$http.get('/api/as/jobs/EstimationList/1/NoPolisi/'+filter);
                // ====== yang ini service dengan filter vehicle id =============
                // var res = $http.get('/api/as/jobs/getEstimation/' + filter);
                // ====== yang ini service dari get data estimasi di menu estimasi
                var res = $http.get('/api/as/jobs/getListEstimationbyPoliceNumberRev/PoliceNumber/' + filter + '?isGR=0');
                console.log('resnya pause=>', res);
                return res;
            },

            createNewCustomerList: function(data, isSatelite) {
                if (isSatelite != true){
                    var no_hp = null
                    if (data.Customer.CustomerTypeId != 1 && data.Customer.CustomerTypeId != 3){
                        no_hp = angular.copy(data.Customer.PICHp)
                    } else {
                        no_hp = angular.copy(data.Customer.HandPhone1)
                    }
                    return $http.post('/api/crm/PostCustomerList/', [{
                        // ToyotaId: data.Customer.ToyotaId, //Masi Pakai Dummy Math.Random
                        OutletId: currUser.OrgId,
                        Npwp: data.Customer.Npwp,
                        CustomerTypeId: data.Customer.CustomerTypeId,
                        FleetId: 1, //Foreign Key, Masih Dummy
                        StatusCode: 1, //Status Create New
                            //ToyotaIdRequest: data.Customer.ToyotaIDFlag
                        CustomerName: this.checkIsCharacter(data.Customer.Name),
                        KTP: data.Customer.KTPKITAS,
                        NoHandphone: no_hp,
                        NPWP: data.Customer.Npwp,
                        AFCO: data.Customer.AFCOIdFlag,

                    }]);
                } else {
                    var no_hp = null
                    if (data.Customer.CustomerTypeId != 1 && data.Customer.CustomerTypeId != 3){
                        no_hp = angular.copy(data.Customer.PICHp)
                    } else {
                        no_hp = angular.copy(data.Customer.HandPhone1)
                    }
                    return $http.post('/api/crm/PostCustomerListSatellite/', [{
                        // ToyotaId: data.Customer.ToyotaId, //Masi Pakai Dummy Math.Random
                        OutletId: currUser.OrgId,
                        Npwp: data.Customer.Npwp,
                        CustomerTypeId: data.Customer.CustomerTypeId,
                        FleetId: 1, //Foreign Key, Masih Dummy
                        StatusCode: 1, //Status Create New
                            //ToyotaIdRequest: data.Customer.ToyotaIDFlag
                        CustomerName: this.checkIsCharacter(data.Customer.Name),
                        KTP: data.Customer.KTPKITAS,
                        NoHandphone: no_hp,
                        NPWP: data.Customer.Npwp,
                        AFCO: data.Customer.AFCOIdFlag,

                    }]);
                }
                
            },
            createNewCustomerListPersonal: function(id, data) {
                return $http.post('/api/crm/PostCustomerListPersonal/', [{
                    CustomerId: id,
                    // CustomerName: data.Customer.Name,
                    CustomerName: this.checkIsCharacter(data.Customer.Name),
                    FrontTitle: data.Customer.FrontTitle,
                    EndTitle: data.Customer.EndTitle,
                    KTPKITAS: data.Customer.KTPKITAS,
                    // BirthPlace: 'Dummy', //Not Used in WOBP
                    BirthDate: data.Customer.newBirthDate,
                    Handphone1: data.Customer.HandPhone1,
                    Handphone2: data.Customer.HandPhone2,
                    StatusCode: 1
                }]);
            },
            createNewCustomerListInstitusi: function(id, data) {
                return $http.post('/api/crm/PostCustomerListInstitution/', [{
                    InstitutionId: 0,
                    CustomerId: id,
                    // Name: data.Customer.Name,
                    Name: this.checkIsCharacter(data.Customer.Name),
                    SIUP: data.Customer.SIUP,
                    // PICName: data.Customer.PICName,
                    PICName: this.checkIsCharacter(data.Customer.PICName),
                    PICHp: data.Customer.PICHp,
                    PICEmail: data.Customer.PICEmail,
                    Npwp: data.Customer.Npwp,
                    NPWP: data.Customer.Npwp,
                    // PICDOB: data.Customer.PICName,
                    // PICGenderId: data.Customer.PICName,
                    // PICAddress: data.Customer.PICName,
                    // PICVillageId: data.Customer.PICName,
                    // PICRT: data.Customer.PICName,
                    // PICRW: data.Customer.PICName,
                    // SectorBusinessId: data.Customer.PICName,
                    // PICPositionId: data.Customer.PICName,
                    PICDepartmentId: data.Customer.PICDepartmentId,
                    // StatusCode: data.Customer.PICName,
                }]);

            },
            createNewCustomerListAddress: function(id, data) {
                _.map(data, function(val) {
                    val.MainAddress = val.MainAddress == true ? 1 : 0;
                });
                return $http.post('/api/crm/PostCCustomerAddress/', data);
            },
            updateCustomerListAddress: function(Aid, Cid, data) {
                _.map(data, function(val) {
                    val.MainAddress = val.MainAddress == true ? 1 : 0;
                });
                return $http.put('/api/crm/PutCCustomerAddress/', data);
            },
            updateCustomerList: function(id, cTypeId, data, isSatelite) {
                var d_toyotaid = angular.copy(data.Customer.ToyotaId)
                if (d_toyotaid == undefined || d_toyotaid == null || d_toyotaid == ''){
                    d_toyotaid = '-'
                }
                if (isSatelite != true){
                    return $http.put('/api/crm/PutCustomerList/', [{
                        CustomerId: id,
                        // OutletId: 655,
                        ToyotaId: d_toyotaid,
                        Npwp: data.Customer.Npwp,
                        CustomerTypeId: cTypeId,
                        // FleetId: 1,
                        StatusCode: 1,
                        Afco: data.Customer.AFCOIdFlag

                    }]);
                } else {
                    return $http.put('/api/crm/PutCustomerListSatellite/', [{
                        CustomerId: id,
                        // OutletId: 655,
                        ToyotaId: d_toyotaid,
                        Npwp: data.Customer.Npwp,
                        CustomerTypeId: cTypeId,
                        // FleetId: 1,
                        StatusCode: 1,
                        Afco: data.Customer.AFCOIdFlag

                    }]);
                }
                
            },
            deleteCustomerListAddress: function(id) {
                console.log('fdg', id)
                // return $http.delete('/api/crm/DeleteCCustomerAddress/', { data: id, headers: { 'Content-Type': 'application/json' } });
                return $http.put('/api/crm/DeleteCCustomerAddress/', id);
            },
            updateCustomerListPersonal: function(Pid, Cid, data) {
                // return $http.put('/api/crm/PutCListPersonal', [{
                return $http.put('/api/crm/PutCListPersonalNew', [{
                    PersonalId: Pid,
                    CustomerId: Cid,
                    FrontTitle: data.Customer.FrontTitle,
                    EndTitle: data.Customer.EndTitle,
                    // CustomerName: data.Customer.Name,
                    CustomerName: this.checkIsCharacter(data.Customer.Name),
                    KTPKITAS: data.Customer.KTPKITAS,
                    // BirthPlace: 'Dummy', //Not Used in WOBP
                    BirthDate: data.Customer.newBirthDate,
                    Handphone1: data.Customer.HandPhone1,
                    Handphone2: data.Customer.HandPhone2,
                    StatusCode: 1

                }]);
            },
            updateCustomerListInstitusi: function(Iid, Cid, data) {
                console.log("data di factory", data);
                // return $http.put('/api/crm/PutCListInstitution', [{
                return $http.put('/api/crm/PutCListInstitutionNew', [{
                    InstitutionId: Iid,
                    CustomerId: Cid,
                    // Name: data.Customer.Name,
                    Name: this.checkIsCharacter(data.Customer.Name),
                    SIUP: data.Customer.SIUP,
                    // PICName: data.Customer.PICName,
                    PICName: this.checkIsCharacter(data.Customer.PICName),
                    PICHp: data.Customer.PICHp,
                    PICEmail: data.Customer.PICEmail,
                    // Npwp: data.Customer.Npwp,
                    NPWP: data.Customer.Npwp,
                    // PICDOB: data.Customer.PICName,
                    // PICGenderId: data.Customer.PICName,
                    // PICAddress: data.Customer.PICName,
                    // PICVillageId: data.Customer.PICName,
                    // PICRT: data.Customer.PICName,
                    // PICRW: data.Customer.PICName,
                    // SectorBusinessId: data.Customer.PICName,
                    // PICPositionId: data.Customer.PICName,
                    PICDepartmentId: data.Customer.PICDepartmentId,

                }]);
            },

            createOrUpdateCustomerListPersonal: function (Pid, Cid, data){
                if (Pid == null){
                    // ni blm pernah punya personalid jalanin post
                    var id = Cid
                    return $http.post('/api/crm/PostCustomerListPersonal/', [{
                        CustomerId: id,
                        // CustomerName: data.Customer.Name,
                        CustomerName: this.checkIsCharacter(data.Customer.Name),
                        FrontTitle: data.Customer.FrontTitle,
                        EndTitle: data.Customer.EndTitle,
                        KTPKITAS: data.Customer.KTPKITAS,
                        // BirthPlace: 'Dummy', //Not Used in WOBP
                        BirthDate: data.Customer.newBirthDate,
                        Handphone1: data.Customer.HandPhone1,
                        Handphone2: data.Customer.HandPhone2,
                        StatusCode: 1
                    }]);
                } else {
                    // ini punya personalid jalanin update
                    // return $http.put('/api/crm/PutCListPersonal', [{
                    return $http.put('/api/crm/PutCListPersonalNew', [{
                        PersonalId: Pid,
                        CustomerId: Cid,
                        FrontTitle: data.Customer.FrontTitle,
                        EndTitle: data.Customer.EndTitle,
                        // CustomerName: data.Customer.Name,
                        CustomerName: this.checkIsCharacter(data.Customer.Name),
                        KTPKITAS: data.Customer.KTPKITAS,
                        // BirthPlace: 'Dummy', //Not Used in WOBP
                        BirthDate: data.Customer.newBirthDate,
                        Handphone1: data.Customer.HandPhone1,
                        Handphone2: data.Customer.HandPhone2,
                        StatusCode: 1
    
                    }]);
                }

            },
            createOrUpdateCustomerListInstitusi: function (Iid, Cid, data){
                if (Iid == null){
                    // ni blm pernah punya institusiid jalanin post
                    var id = Cid
                    return $http.post('/api/crm/PostCustomerListInstitution/', [{
                        InstitutionId: 0,
                        CustomerId: id,
                        // Name: data.Customer.Name,
                        Name: this.checkIsCharacter(data.Customer.Name),
                        SIUP: data.Customer.SIUP,
                        // PICName: data.Customer.PICName,
                        PICName: this.checkIsCharacter(data.Customer.PICName),
                        PICHp: data.Customer.PICHp,
                        PICEmail: data.Customer.PICEmail,
                        Npwp: data.Customer.Npwp,
                        NPWP: data.Customer.Npwp,
                        // PICDOB: data.Customer.PICName,
                        // PICGenderId: data.Customer.PICName,
                        // PICAddress: data.Customer.PICName,
                        // PICVillageId: data.Customer.PICName,
                        // PICRT: data.Customer.PICName,
                        // PICRW: data.Customer.PICName,
                        // SectorBusinessId: data.Customer.PICName,
                        // PICPositionId: data.Customer.PICName,
                        PICDepartmentId: data.Customer.PICDepartmentId,
                        // StatusCode: data.Customer.PICName,
                    }]);
                } else {
                    // ini punya institusiid jalanin update
                    // return $http.put('/api/crm/PutCListInstitution', [{
                    return $http.put('/api/crm/PutCListInstitutionNew', [{
                        InstitutionId: Iid,
                        CustomerId: Cid,
                        // Name: data.Customer.Name,
                        Name: this.checkIsCharacter(data.Customer.Name),
                        SIUP: data.Customer.SIUP,
                        // PICName: data.Customer.PICName,
                        PICName: this.checkIsCharacter(data.Customer.PICName),
                        PICHp: data.Customer.PICHp,
                        PICEmail: data.Customer.PICEmail,
                        // Npwp: data.Customer.Npwp,
                        NPWP: data.Customer.Npwp,
                        // PICDOB: data.Customer.PICName,
                        // PICGenderId: data.Customer.PICName,
                        // PICAddress: data.Customer.PICName,
                        // PICVillageId: data.Customer.PICName,
                        // PICRT: data.Customer.PICName,
                        // PICRW: data.Customer.PICName,
                        // SectorBusinessId: data.Customer.PICName,
                        // PICPositionId: data.Customer.PICName,
                        PICDepartmentId: data.Customer.PICDepartmentId,
    
                    }]);
                }

            },

            // IsCuustomerInstitutionExists: function (Iid, Cid, data){
            //     // api/crm/IsCuustomerInstitutionExists

            //     return $http.put('/api/crm/IsCuustomerInstitutionExists', [{
            //         InstitutionId: 0,
            //         CustomerId: 0,
            //         Name: this.checkIsCharacter(data.Customer.Name),
            //         SIUP: data.Customer.SIUP,
            //         PICName: this.checkIsCharacter(data.Customer.PICName),
            //         PICHp: data.Customer.PICHp,
            //         PICEmail: data.Customer.PICEmail,
            //         NPWP: data.Customer.Npwp,
            //         PICDepartmentId: data.Customer.PICDepartmentId,

            //     }]);

            // },

            // IsCuustomerPersonalExists: function (Pid, Cid, data){
            //     // api/crm/IsCuustomerPersonalExists

            //     return $http.put('/api/crm/IsCuustomerPersonalExists', [{
            //             PersonalId: 0,
            //             CustomerId: 0,
            //             FrontTitle: data.Customer.FrontTitle,
            //             EndTitle: data.Customer.EndTitle,
            //             CustomerName: this.checkIsCharacter(data.Customer.Name),
            //             KTPKITAS: data.Customer.KTPKITAS,
            //             BirthDate: data.Customer.newBirthDate,
            //             Handphone1: data.Customer.HandPhone1,
            //             Handphone2: data.Customer.HandPhone2,
            //             StatusCode: 1
            //         }]);

            // },

            IsCuustomerExists: function (Pid, Cid, data, custtype, isSatelite){

                var d_toyotaid = angular.copy(data.Customer.ToyotaId)
                if (d_toyotaid == undefined || d_toyotaid == null || d_toyotaid == ''){
                    d_toyotaid = '-'
                }

                if (isSatelite != true){
                    if (custtype == 3) {
                        // personal
                        return $http.put('/api/crm/IsCuustomerPersonalExists', [{
                            PersonalId: 0,
                            CustomerId: Cid,
                            FrontTitle: data.Customer.FrontTitle,
                            EndTitle: data.Customer.EndTitle,
                            CustomerName: this.checkIsCharacter(data.Customer.Name),
                            KTPKITAS: data.Customer.KTPKITAS,
                            BirthDate: data.Customer.newBirthDate,
                            Handphone1: data.Customer.HandPhone1,
                            Handphone2: data.Customer.HandPhone2,
                            StatusCode: 1,
                            ToyotaId: d_toyotaid,
                        }]);
    
                    } else {
                        // institusi
                        return $http.put('/api/crm/IsCuustomerInstitutionExists', [{
                            InstitutionId: 0,
                            CustomerId: Cid,
                            Name: this.checkIsCharacter(data.Customer.Name),
                            SIUP: data.Customer.SIUP,
                            PICName: this.checkIsCharacter(data.Customer.PICName),
                            PICHp: data.Customer.PICHp,
                            PICEmail: data.Customer.PICEmail,
                            NPWP: data.Customer.Npwp,
                            PICDepartmentId: data.Customer.PICDepartmentId,
                            ToyotaId: d_toyotaid,
        
                        }]);
                    }
                } else {
                    if (custtype == 3) {
                        // personal
                        return $http.put('/api/crm/IsCuustomerPersonalSatelliteExists', [{
                            PersonalId: 0,
                            CustomerId: Cid,
                            FrontTitle: data.Customer.FrontTitle,
                            EndTitle: data.Customer.EndTitle,
                            CustomerName: this.checkIsCharacter(data.Customer.Name),
                            KTPKITAS: data.Customer.KTPKITAS,
                            BirthDate: data.Customer.newBirthDate,
                            Handphone1: data.Customer.HandPhone1,
                            Handphone2: data.Customer.HandPhone2,
                            StatusCode: 1,
                            ToyotaId: d_toyotaid,
                        }]);
    
                    } else {
                        // institusi
                        return $http.put('/api/crm/IsCuustomerInstitutioSatellitenExists', [{
                            InstitutionId: 0,
                            CustomerId: Cid,
                            Name: this.checkIsCharacter(data.Customer.Name),
                            SIUP: data.Customer.SIUP,
                            PICName: this.checkIsCharacter(data.Customer.PICName),
                            PICHp: data.Customer.PICHp,
                            PICEmail: data.Customer.PICEmail,
                            NPWP: data.Customer.Npwp,
                            PICDepartmentId: data.Customer.PICDepartmentId,
                            ToyotaId: d_toyotaid,
        
                        }]);
                    }

                }
                


            },

            
            // updateCustomerListUser: function(data) {
            //     return $http.put('/api/crm/PutVehicleUser/', [{
            //         VehicleUserId: data.VehicleUserId,
            //         CustomerVehicleId: data.CustomerVehicleId,
            //         Name: data.Name,
            //         Phone: data.Phone,
            //         StatusCode: 1
            //     }]);
            // },
            // createNewCustomerListUser: function(data) {
            //     return $http.post('/api/crm/PostVehicleUser/', [{
            //         CustomerVehicleId: data.CustomerVehicleId,
            //         Name: data.Name,
            //         Phone: data.Phone,
            //         StatusCode: 1
            //     }]);
            // },
            getOwnerUser: function() {
                return $http.get('');
            },
            getDataWOBP: function() {
                return $http.get('');
            },
            getMLocationProvince: function() {
                return $http.get('/api/sales/MLocationProvince');
            },
            getMLocationCityRegency: function(id) {
                return $http.get('/api/sales/MLocationCityRegency?start=1&limit=100&filterData=ProvinceId|' + id);
            },
            getMLocationKecamatan: function(id) {
                return $http.get('/api/sales/MLocationKecamatan?start=1&limit=100&filterData=CityRegencyId|' + id);
            },
            getMLocationKelurahan: function(id) {
                return $http.get('/api/sales/MLocationKelurahan?start=1&limit=100&filterData=DistrictId|' + id)
            },
            createEstimation: function(mData, mDataCrm, mDataDM, jamServer) {
                console.log("data di factory", mData, mDataCrm, mDataDM);

                // var newDate = new Date();
                var newDate = new Date(mData.EstimationDate);
                var newDateOri = new Date()
                var yearFirst = newDate.getFullYear();
                var monthFirst = newDate.getMonth() + 1;
                var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                var dayFirst = newDate.getDate();
                var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                var hour = newDateOri.getHours();
                var hours = hour < 10 ? '0' + hour : hour;
                var minutes = newDateOri.getMinutes();
                var minutess = minutes < 10 ? '0' + minutes : minutes;
                var newEstimateDate = yearFirst + '-' + monthFirsts + '-' + dayFirsts + 'T' + hours + ':' + minutess;
                console.log("data di newDate", newEstimateDate);
                // if (mData.JobId > 0) {
                //     return $http.put('/api/as/Jobs/updateDetil/3', [{
                //         JobId: mData.JobId,
                //         // JobNo: 0,
                //         OutletId: currUser.OrgId,
                //         // CalId: 1,
                //         TechnicianAction: 1,
                //         // JobSuggest: 1,
                //         JobType: 0,
                //         Status: 0,
                //         // Status: 21,
                //         JobTask: mData.JobTask,
                //         JobComplaint: mData.JobComplaint,
                //         JobRequest: mData.JobRequest,
                //         AppointmentNo: null,
                //         NewAppointmentRel: null,
                //         isAppointment: 0,
                //         isGr: 0,
                //         VehicleId: mDataCrm.Vehicle.VehicleId,
                //         // isCash: 0,
                //         // isSpk: 0,
                //         // SpkNo: "-",
                //         // Km: mData.Km,
                //         // InsuranceName: "-",
                //         isWOBase: 1,
                //         AppointmentVia: 0,
                //         Process: null,
                //         WoNo: null,
                //         ModelCode: mDataCrm.Vehicle.ModelCode,
                //         // DecisionMaker: null,
                //         // PhoneDecisionMaker1: null,
                //         // PhoneDecisionMaker2: null,
                //         // ContactPerson: null,
                //         // PhoneContactPerson1: null,
                //         // PhoneContactPerson2: null,
                //         DecisionMaker: mDataDM.PK.Name ? mDataDM.PK.Name : null,
                //         PhoneDecisionMaker1: mDataDM.PK.HP1 ? mDataDM.PK.HP1 : null,
                //         PhoneDecisionMaker2: mDataDM.PK.HP2 ? mDataDM.PK.HP2 : null,
                //         ContactPerson: mDataDM.CP.Name ? mDataDM.CP.Name : null,
                //         PhoneContactPerson1: mDataDM.CP.HP1 ? mDataDM.CP.HP1 : null,
                //         PhoneContactPerson2: mDataDM.CP.HP2 ? mDataDM.CP.HP2 : null,
                //         // AddressForEstimation: mData.AddressForEstimation,
                //         // Email: mData.Email,
                //         AssemblyYearForEstimation: mDataCrm.Vehicle.AssemblyYear,
                //         IsEstimation: 1,
                //         PoliceNumber: mDataCrm.Vehicle.LicensePlate,
                //         // PoliceNumber: mData.LicensePlate,
                //         ModelType: mData.VehicleModelName,
                //         KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                //         EstimationDate: mData.EstimationDate,
                //         VehicleTypeId: mDataCrm.Vehicle.VehicleTypeId,
                //         EstimationNo: "",
                //         WoCategoryId: mData.WoCategoryId,
                //         JobDate: new Date(),
                //         CustomerId: mDataCrm.Customer.CustomerId
                //     }]);
                // } else {
                mData.JobTask = this.validationNonTaskList(mData.JobTask)

                if (mDataCrm.Vehicle.isNonTAM == 1){
                    mData.VehicleModelName = mDataCrm.Vehicle.ModelName 
                }

                return $http.put('/api/as/Jobs/updateDetil/3?createNo=1', [{
                    JobId: 0,
                    OutletId: currUser.OrgId,
                    TechnicianAction: 1,
                    JobType: 0,
                    Status: 0,
                    JobTask: mData.JobTask,
                    JobComplaint: mData.JobComplaint,
                    JobRequest: mData.JobRequest,
                    AppointmentNo: null,
                    NewAppointmentRel: null,
                    isAppointment: mData.isAppointment == null || typeof mData.isAppointment == "undefined" ? 0 : mData.isAppointment,
                    isGr: 0,
                    VehicleId: mDataCrm.Vehicle.VehicleId,
                    isWOBase: 1,
                    AppointmentVia: 0,
                    Process: null,
                    WoNo: null,
                    ModelCode: mDataCrm.Vehicle.ModelCode,
                    DecisionMaker: mDataDM.PK.Name ? mDataDM.PK.Name : null,
                    PhoneDecisionMaker1: mDataDM.PK.HP1 ? mDataDM.PK.HP1 : null,
                    PhoneDecisionMaker2: mDataDM.PK.HP2 ? mDataDM.PK.HP2 : null,
                    ContactPerson: mDataDM.CP.Name ? mDataDM.CP.Name : null,
                    PhoneContactPerson1: mDataDM.CP.HP1 ? mDataDM.CP.HP1 : null,
                    PhoneContactPerson2: mDataDM.CP.HP2 ? mDataDM.CP.HP2 : null,
                    AssemblyYearForEstimation: mDataCrm.Vehicle.AssemblyYear,
                    IsEstimation: 1,
                    PoliceNumber: mDataCrm.Vehicle.LicensePlate,
                    ModelType: mData.VehicleModelName,
                    KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    // EstimationDate: newEstimateDate, // ini hrs dari jam server
                    EstimationDate: jamServer, // ini hrs dari jam server jamServer
                    VehicleTypeId: mDataCrm.Vehicle.VehicleTypeId,
                    EstimationNo: "",
                    WoCategoryId: mData.WoCategoryId,
                    JobCatgForBPid: mData.JobCatgForBPid,
                    JobDate: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
                    CustomerId: mDataCrm.Customer.CustomerId,
                    UserIdSa: currUser.UserId,
                    VehicleConditionTypeId: mData.VehicleConditionTypeId,
                    UserIdEstimasi: currUser.UserId,
                    QueueId: mData.QueueId,
                    IsCustomerRequest : mData.IsCustomerRequest != 1 ? 0 : 1,
                    GateInPushTime: $filter('date')(mData.GateInPushTime, 'yyyy-MM-dd HH:mm:ss'),


                    DPPaidAmount: mData.DPPaidAmount,
                    ORPaidAmount: mData.ORPaidAmount,
                    DPRawatJalan: mData.DPRawatJalan,
                    IsPaidRawatJalan: mData.IsPaidRawatJalan,
                    isCash: mData.isCash,
                    InsuranceId: mData.InsuranceId,

                    AppointmentCreateDate: (mData.AppointmentCreateDate == null || mData.AppointmentCreateDate == undefined) ? null : $filter('date')(mData.AppointmentCreateDate, 'yyyy-MM-dd HH:mm:ss'),


                }]);
                // }
            },

            releaseBSTKEstimation: function(mData, mDataCrm, mDataDM) {
                console.log("data di factory", mData, mDataCrm, mDataDM);
                mData.JobTask = this.validationNonTaskList(mData.JobTask)

                if (mDataCrm.Vehicle.isNonTAM == 1){
                    mData.VehicleModelName = mDataCrm.Vehicle.ModelName 
                }

                return $http.put('/api/as/Jobs/updateDetilBSTK/3?createNo=1', [{
                    JobId: 0,
                    // JobNo: 0,
                    OutletId: currUser.OrgId,
                    // CalId: 1,
                    TechnicianAction: 1,
                    // JobSuggest: 1,
                    JobType: 0,
                    Status: 21,
                    JobTask: mData.JobTask,
                    JobComplaint: mData.JobComplaint,
                    JobRequest: mData.JobRequest,
                    AppointmentNo: null,
                    NewAppointmentRel: null,
                    isAppointment: mData.isAppointment == null || typeof mData.isAppointment == "undefined" ? 0 : mData.isAppointment,
                    isGr: 0,
                    VehicleId: mDataCrm.Vehicle.VehicleId,
                    // isCash: 0,
                    // isSpk: 0,
                    // SpkNo: "-",
                    // Km: mData.Km,
                    // InsuranceName: "-",
                    isWOBase: 1,
                    AppointmentVia: 0,
                    Process: null,
                    WoNo: null,
                    ModelCode: mDataCrm.Vehicle.ModelCode,
                    DecisionMaker: null,
                    PhoneDecisionMaker1: null,
                    PhoneDecisionMaker2: null,
                    ContactPerson: null,
                    PhoneContactPerson1: null,
                    PhoneContactPerson2: null,
                    // AddressForEstimation: mData.AddressForEstimation,
                    // Email: mData.Email,

                    ContactPerson: mDataDM.CP.Name,
                    PhoneContactPerson1: mDataDM.CP.HP1,
                    PhoneContactPerson2: mDataDM.CP.HP2,
                    AssemblyYearForEstimation: mDataCrm.Vehicle.AssemblyYear,
                    IsEstimation: 1,
                    PoliceNumber: mDataCrm.Vehicle.LicensePlate,
                    // PoliceNumber: mData.LicensePlate,
                    ModelType: mData.VehicleModelName,
                    KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    EstimationDate: mData.EstimationDate,
                    VehicleTypeId: mDataCrm.Vehicle.VehicleTypeId,
                    EstimationNo: "",
                    WoCategoryId: mData.WoCategoryId,
                    JobDate: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
                    CustomerId: mDataCrm.Customer.CustomerId,
                    UserIdSa: currUser.UserId,
                    VehicleConditionTypeId: mData.VehicleConditionTypeId,
                    UserIdEstimasi: currUser.UserId,
                    JobCatgForBPid: mData.JobCatgForBPid,
                    GateInPushTime: $filter('date')(mData.GateInPushTime, 'yyyy-MM-dd HH:mm:ss'),

                    // FinalDP: mData.totalDp,

                    DPPaidAmount: mData.DPPaidAmount,
                    ORPaidAmount: mData.ORPaidAmount,
                    DPRawatJalan: mData.DPRawatJalan,
                    IsPaidRawatJalan: mData.IsPaidRawatJalan,

                    AppointmentCreateDate: (mData.AppointmentCreateDate == null || mData.AppointmentCreateDate == undefined) ? null : $filter('date')(mData.AppointmentCreateDate, 'yyyy-MM-dd HH:mm:ss'),

                }]);
            },
            getGroupTeam: function(isSatelite) {
                if (isSatelite != true){
                    return $http.get('/api/as/Boards/BP/GroupList/0');
                } else {
                    return $http.get('/api/as/Boards/BP/GroupListSatellite/0');
                }
            },

            // added by 2017-09-26
            getOutletBPCenter: function() {
                return $http.get('/api/as/OutletBPCenter');
            },
            getSatelliteByCenter: function(OutIdCtr) {
                return $http.get('/api/as/OutletBPCenter?OutletBPCenter=' + OutIdCtr);
            },
            getCenterBySatellite: function(OutIdSat) {
                return $http.get('/api/as/OutletBPSatellite?OutletBPSatellite=' + OutIdSat);
                // var defer = $q.defer();
                //     defer.resolve(
                //       {
                //         "data" : {
                //           "Result":[{
                //               "Id":1,
                //               "OutletIdSatellite":655,
                //               "OutletBPCenter":180,
                //               "SAName":"Advisor Name",
                //               "StatusCode":1,
                //               "LastModifiedDate":"2017-10-04",
                //               "LastModifiedUserId":1
                //           }],
                //           "Start":1,
                //           "Limit":10,
                //           "Total":1
                //         }
                //       }
                //     );
                // return defer.promise;
            },
            setBSTK: function(mData, mDataCrm, mDataDM, sentOutletId) {
                console.log("data di factory", mData, mDataCrm, mDataDM, sentOutletId);
                return $http.post('/api/as/jobs/setBSTK', [{
                    JobId: 0,
                    OutletId: sentOutletId,
                    BpSatelitOutletId: currUser.OrgId,
                    VehicleId: mDataCrm.Vehicle.VehicleId,
                    BpSatelitSAUserId: currUser.UserId,
                    TechnicianAction: 1,
                    JobType: 0,
                    Status: 0,
                    JobTask: mData.JobTask,
                    JobComplaint: mData.JobComplaint,
                    JobRequest: mData.JobRequest,
                    AppointmentNo: null,
                    NewAppointmentRel: null,
                    isAppointment: mData.isAppointment == null || typeof mData.isAppointment == "undefined" ? 0 : mData.isAppointment,
                    isGr: 0,
                    isWOBase: 1,
                    AppointmentVia: 0,
                    Process: null,
                    WoNo: null,
                    ModelCode: mDataCrm.Vehicle.ModelCode,
                    DecisionMaker: mDataDM.PK.Name,
                    PhoneDecisionMaker1: mDataDM.PK.HP1,
                    PhoneDecisionMaker2: mDataDM.PK.HP2,
                    ContactPerson: mDataDM.CP.Name,
                    PhoneContactPerson1: mDataDM.CP.HP1,
                    PhoneContactPerson2: mDataDM.CP.HP2,
                    AssemblyYearForEstimation: mDataCrm.Vehicle.AssemblyYear,
                    IsEstimation: 1,
                    PoliceNumber: mDataCrm.Vehicle.LicensePlate,
                    ModelType: mData.VehicleModelName,
                    KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    EstimationDate: mData.EstimationDate,
                    VehicleTypeId: mDataCrm.Vehicle.VehicleTypeId,
                    EstimationNo: "",
                    WoCategoryId: mData.WoCategoryId,
                    JobDate: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm:ss'),
                    VehicleConditionTypeId: mData.VehicleConditionTypeId,
                    UserIdSa: currUser.UserId,
                }]);
            },
            backtoSatellite: function(jobId) {
                return $http.post('/api/as/jobs/backToSatellite?jobId=' + jobId);
            },
            getClaim: function(jobId) {
                return $http.get('/api/as/SPKClaim/' + jobId);
            },
            createClaim: function(data) {
                return $http.post('/api/as/SPKClaim', data);
            },
            updateClaim: function(data) {
                return $http.put('/api/as/SPKClaim', data);
            },
            checkApproval: function(data) {
                console.log("data checkApproval", data);
                return $http.put('/api/as/CheckingApproval/', [{
                    DPRequested: data,
                    ApprovalCategoryId: 42,
                    RoleId: currUser.RoleId
                }]);
            },
            postApproval: function(data, mDataCrm) {
                // ApproverRoleId
                console.log("postApproval", data);
                return $http.post('/api/as/PostApprovalRequestDP/3', [{
                    JobId: data.JobId,
                    DPDefault: data.totalDpDefault,
                    DPRequested: data.totalDp,
                    ApprovalCategoryId: 42,
                    ApproverId: null,
                    RequesterId: null,
                    ApproverRoleId: data.ApproverRoleId,
                    RequestorRoleId: currUser.RoleId,
                    RequestReason: "Request Pengurangan DP",
                    RequestDate: new Date(),
                    StatusApprovalId: 3,
                    StatusCode: 1,
                    VehicleTypeId: mDataCrm.Vehicle.VehicleTypeId
                }]);
            },
            updateStatusQueue: function(QueueId, updateMode) {
                // Update Mode Index :
                // 0 : Available For Call
                // 1 : On Call Process
                // 2 : Start Reception

                return $http.put('/api/as/queue/updateQueue/' + QueueId + '/' + updateMode);
            },
            getSAAssignmentTemp: function(isGr) {
                return $http.get('/api/as/queue/getSAAssignmentGRBP/' + isGr);
                // return $http.get('/api/as/queue/getSAAssignmentTemp/' + isGr);
            },
            insertSAAssignmentTemp: function(NoPol, JobId, isGr) {
                return $http.post('/api/as/queue/insertSAAssignmentTemp/' + NoPol + '/' + JobId + '/' + isGr);
            },

            insertSAAssignmentTempWithQid: function(NoPol, JobId, isGr, Qid) {
                // api/as/queue/insertSAAssignmentTemp/{NoPol}/{JobId}/{IsGr}/{QId}
                return $http.post('/api/as/queue/insertSAAssignmentTemp/' + NoPol + '/' + JobId + '/' + isGr + '/' + Qid);
            },

            dropSAAssignmentTemp: function(NoPol) {
                return $http.delete('/api/as/queue/dropSAAssignmentTemp/' + NoPol);
            },
            clearQueueCounters: function(NoPol) {
                return $http.delete('/api/as/queue/clearQueueCounters/' + NoPol);
            },
            checkAccess: function(param1, param2) {
                return $http.get('/api/as/queue/checkAccess/' + param2.CounterId + '/' + param1.Id);
            },
            checkAccessQCat: function(param) {
                return $http.get('/api/as/queue/checkAccessQCat/' + param);
            },
            gateSatelliteCenter: function(param) {
                return $http.post('/api/as/GateSateliteCenter/create', [{
                    PoliceNumber: param
                }]);
            },
            getCustAddress: function(id) {
                return $http.get('/api/crm/GetCCustomerAddress/' + id);
            },
            getDataWaitingChips: function() {
                return $http.get('/api/as/GetChipforWaiting/0');
            },

            getDataWaitingOPL: function() {
                return $http.get('/api/as/GetChipforOPL/0');
            },

            updateSAAssignmentTemp: function(NoPol, JobId, IsGr) {
                return $http.put('/api/as/queue/updateSAAssignmentTemp/' + NoPol + '/' + JobId + '/' + IsGr);
            },

            postJobWAC: function(data) {
                data.forEach(function(val) {
                    val.moneyAmount = (typeof val.moneyAmount != 'undefined' || val.moneyAmount != null ? val.moneyAmount : 0); //Billy
                    val.moneyAmount = (typeof val.MoneyAmount != 'undefined' || val.MoneyAmount != null ? val.MoneyAmount : 0);
                });
                return $http.post('/api/as/JobWACs', data);
            },
            // updateJobWAC: function (data) {
            //     return $http.put('/api/as/JobWACs', [{
            //         JobWacId:Id,
            //         JobWAC:data,
            //     }]);
            // },
            getDamage: function() {
                var catId = 2040;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId + '&Flag=1');
                console.log('resnya pause=>', res);
                return res;
            },
            getStatusVehicle: function() {
                var catId = 2036;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId + '&Flag=1');
                console.log('resnya pause=>', res);
                return res;
            },
            getPointVehicle: function() {
                var catId = 2037;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId + '&Flag=1');
                console.log('resnya pause=>', res);
                return res;
            },
            getJobTaskList: function(jobId) {
                return $http.get('/api/as/JobTasks/' + jobId)
            },
            getVehicleConditionTask: function(data, tipeMobil) {
                return $http.get('/api/as/TaskListBpFilter/' + data.ItemId + '/' + data.PointId + '/' + data.StatusVehicleId + '/' + tipeMobil);
                // return $http.get('/api/as/TaskListBpFilter/11/2441/2443');
            },
            // ini getdataparts ini di komen aja karena nama factory na ada 2 dan sama persis, tapi jd ada if di yg bawah. cek aja getDataParts yg bawah
            // getDataParts: function(Key, servicetype, type) {
            //     console.log("keyyyyyy", Key);
            //     var res = $http.get('/api/as/StockAdjustment/GetStockAdjustmentDetailSateliteFromMaterial?PartsCode=' + Key + '&ServiceTypeId=' + servicetype + '&PartsClassId1=' + type);
            //     // console.log("resnya",res);
            //     return res;
            // },
            getAvailablePartsSatelite: function(item, jobid, qtyinput) {
                // /api/as/jobs/getAvailableParts/{partid}
                // var res = $http.get('/api/as/jobs/getPartsPriceAndDefaultETDETA/' + item);
                if (typeof item == 'string') {
                    if (item.includes('&')) {
                        item = item.split("&")[0];
                    }
                }
                var res = $http.get('/api/as/jobs/getAvailablePartsSatelitAppointment/' + item + '/0?JobId=' + jobid + '&Qty=' + qtyinput);
                return res
            },
            getAvailableParts: function(item, jobid, qtyinput, isSatelite) {
                // /api/as/jobs/getAvailableParts/{partid}
                // var res = $http.get('/api/as/jobs/getPartsPriceAndDefaultETDETA/' + item);
                if (typeof item == 'string') {
                    if (item.includes('&')) {
                       item = item.split("&")[0];
                    }
                }
                if (jobid === undefined || jobid == null) {
                    jobid = 0;
                }
                if (isSatelite != true){
                    var res = $http.get('/api/as/jobs/getAvailableParts/' + item + '/0?JobId=' + jobid + '&Qty=' + qtyinput);
                } else {
                    var res = $http.get('/api/as/jobs/getAvailablePartsSatelitAppointment/' + item + '/0?JobId=' + jobid + '&Qty=' + qtyinput);
                }
                
                return res
            },
            sendNotif: function(data, recepient, param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param: param
                }]);
            },
            skipQueue: function(data) {
                return $http.put('/api/as/queue/skipQueue/' + data);
            },
            getWarrantyInfo: function(JobId) {
                return $http.get('/api/as/GetWarrantyInfo/' + JobId + '/0');
            },
            getDataTask: function(taskName, vehicleTypeId, vehicleModelId, jobIsWarranty) {
                console.log("name", taskName, vehicleTypeId);
                var res = $http.get('/api/as/TaskListBP/' + taskName + '/' + vehicleTypeId + '/' + vehicleModelId + '/' + jobIsWarranty);
                // var res=$http.get('/api/as/TaskLists?KatashikiCode=ASV50R-JETGKD&Name='+Key+'&TaskCategory='+catg);
                // console.log('/api/as/TaskLists?KatashikiCode='+Katashiki+'&Name='+Key+'&TaskCategory='+catg);
                // console.log("resnya",res);
                return res;
            },
            getDataClaim: function(insuranceId, licensePlate, policyNo) {
                var res = $http.get('/api/as/SPKClaim/' + insuranceId + '/' + licensePlate + '/' + policyNo);
                // var res=$http.get('/api/as/TaskLists?KatashikiCode=ASV50R-JETGKD&Name='+Key+'&TaskCategory='+catg);
                // console.log('/api/as/TaskLists?KatashikiCode='+Katashiki+'&Name='+Key+'&TaskCategory='+catg);
                // console.log("resnya",res);
                return res;
            },
            updateDataClaim: function(data) {
                var res = $http.put('/api/as/SPKClaimUpdate', data);
                return res;
            },
            getPICDepartment: function() {
                var res = $http.get('/api/as/CustomerPosition');
                return res
            },
            getQCTechnician: function(jobId, isSatelite) {
                if (isSatelite != true){
                    var res = $http.get('/api/as/JobTaskBPs/GetQCTechnician/' + jobId)
                } else {
                    var res = $http.get('/api/as/JobTaskBPs/GetQCTechnicianSatellite/' + jobId)
                }
                return res;
            },
            GetTeknisiBP: function(jobId, isSatelite) {
                // api/as/ServiceExplanation/GetTeknisiBP/{JobId}
                var res = $http.get('/api/as/ServiceExplanation/GetTeknisiBP/' + jobId)
                return res;
            },
            checkGateInSE: function(policeNo, jobId) {
                var res = $http.get('/api/as/ServiceExplanations/GateInCheck/' + policeNo + '/' + jobId)
                return res;
            },
            //----------------------------------------------------
            insertRequestPrintHistory: function(data) {

                console.log("request",data);
        
                // var res=$http.get('/api/ct/PostCCustomerAccessTab/',data);
                
                return $http.post('/api/as/PostApprovalRequestPrintServiceHistory', data);
                
            },
            getDiscountCampaign: function(today, data, crm, IsGR) {
                var KmDiscount = 0;
                if (data.Km !== undefined && data.Km !== null) {
                    if (typeof data.Km === 'string') {
                        var tmpKm = angular.copy(data.Km);
                        if (tmpKm.includes(",")) {
                            tmpKm = tmpKm.split(',');
                            if (tmpKm.length > 1) {
                                tmpKm = tmpKm.join('');
                            } else {
                                tmpKm = tmpKm[0];
                            }
                        }
                        tmpKm = parseInt(tmpKm);
                        KmDiscount = tmpKm;
                    } else {
                        KmDiscount = data.Km;
                    }
                }
                console.log("getDiscountCampaign", today, data, crm);
                // '/api/as/CampaignDiscount/Date/2017-08-07/KM/2100/AYear/2015/VechId/4642'
                return $http.get('/api/as/CampaignDiscount/Date/' + today + '/KM/' + KmDiscount + '/AYear/' + crm.Vehicle.AssemblyYear + '/VechId/' + crm.Vehicle.Type.VehicleTypeId + '/IsGR/' + IsGR);
            },
            postCountingPrint: function(jobid, typeId) {
                return $http.post("/api/as/PostPrint", [{
                    JobId: jobid,
                    PrintCountTypeId: typeId,
                    StatusCode: 1
                }]);
            },
            
            getFinalInspectionById: function(JobId) {
                var res = $http.get('/api/as/JobInspections/job/' + JobId + '');
                // console.log('resnya=>',res);
                return res;
            },
            getJobSuggest: function(JobID) {
                var res = $http.get('/api/as/JobSuggestion/' + JobID + '');
                // console.log('resnya employee=>',res);
                return res;
            },
            getVinFromCRM: function(vin) {
                var res = $http.get('/api/crm/GetVehicleListById/' + vin + '');
                console.log('Get Data VIN=>', res);
                return res;
            },
            getDataWac: function(JobId) {
                // console.log("JbId",JbId);
                var res = $http.get('/api/as/JobWACs/Job/' + JobId);
                // console.log('resnya=>',res);
                return res;
            },    
            getJobSuggest: function(JobID) {
                var res = $http.get('/api/as/JobSuggestion/' + JobID + '');
                // console.log('resnya employee=>',res);
                return res;
            },
            getDataWac: function(JobId) {
                // console.log("JbId",JbId);
                var res = $http.get('/api/as/JobWACs/Job/' + JobId);
                // console.log('resnya=>',res);
                return res;
            },
            getData: function(NoPolice, NoWO, Employee, date) {
                console.log("date", date);
                var finalDate;
                if (date != '-') {
                    var yyyy = date.getFullYear().toString();
                    var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based         
                    var dd = date.getDate().toString();
                    finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
                } else {
                    finalDate = date;
                };
                // var res=$http.get('/api/as/jobs/WoList/FirstDate/'+dateNow+'/LastDate/'+dateNow+'');
                // var res=$http.get('/api/as/jobs/WoList/FirstDate/'+dateNow+'/LastDate/'+dateNow+'/1');
                var res = $http.get('/api/as/jobs/EstimationList/1/NoPolisi/' + NoPolice + '/idTeknisi/' + Employee + '/WoNo/' + NoWO + '/Date/' + finalDate);
                // var res = $http.get('/api/as/jobs/EstimationList/1/NoPolisi/-/idTeknisi/-/WoNo/-/Date/-');
                // console.log('resnya=>',res);
                return res;
            },   
            getDataTaskApp: function(taskName, vehicleTypeId, vehicleModelId) {
                console.log("name", taskName, vehicleTypeId, vehicleModelId);
                var res = $http.get('/api/as/TaskListBP/' + taskName + '/' + vehicleTypeId + '/' + vehicleModelId);
                // var res=$http.get('/api/as/TaskLists?KatashikiCode=ASV50R-JETGKD&Name='+Key+'&TaskCategory='+catg);
                // console.log('/api/as/TaskLists?KatashikiCode='+Katashiki+'&Name='+Key+'&TaskCategory='+catg);
                // console.log("resnya",res);
                return res;
            },
            getWoCategory: function() {
                var catId = 2039;
                // var catId = 1012;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId+'&Flag=1');
                console.log('resnya pause=>', res);
                return res;
            },
            getDataPartsByTaskId: function(key) {
                var res = $http.get('/api/as/TaskLists/Parts?TaskId=' + key);
                return res
            },
            getDataParts: function(Key, servicetype, type, isSatelite) {
                console.log("keyyyyyy", Key);
                if (isSatelite != true){
                    var res = $http.get('/api/as/StockAdjustment/GetStockAdjustmentDetailFromMaterial?PartsCode=' + Key + '&ServiceTypeId=' + servicetype + '&PartsClassId1=' + type);
                } else {
                    // ini kl satelite
                    var res = $http.get('/api/as/StockAdjustment/GetStockAdjustmentDetailSateliteFromMaterial?PartsCode=' + Key + '&ServiceTypeId=' + servicetype + '&PartsClassId1=' + type);
                }
                
                // console.log("/api/as/TaskLists?KatashikiCode="+Katashiki+"&Name="+Key);
                // console.log("resnya",res);
                return res;
            },
            getInsurence: function() {
                var res = $http.get('/api/as/Insurance');
                return res
            },
            getWoCategory: function() {
                var catId = 2039;
                // var catId = 1012;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId+'&Flag=1');
                console.log('resnya pause=>', res);
                return res;
            },
            getAvailablePartsApp: function(item, jobid, qtyinput, isSatelite) {
                // /api/as/jobs/getAvailableParts/{partid}
                // var res = $http.get('/api/as/jobs/getPartsPriceAndDefaultETDETA/' + item);
                if (typeof item == 'string') {
                    if (item.includes('&')) {
                        item = item.split("&")[0];
                    }
                }
                if (isSatelite != true){
                    var res = $http.get('/api/as/jobs/getAvailablePartsAppointment/' + item + '/0?JobId='+jobid + '&Qty='+qtyinput);
                } else {
                    var res = $http.get('/api/as/jobs/getAvailablePartsSatelitAppointment/' + item + '/0?JobId=' + jobid + '&Qty=' + qtyinput);

                }
                return res
            },
            getDataTaskListByKatashiki: function(key) {
                var res = $http.get('/api/as/TaskLists/TasklistSA?KatashikiCode=' + key);
                return res
            },
            getDataVehicle: function(key) {
                var res = $http.get('/api/crm/GetDataInfo/' + key);
                return res;
            },
            getDataVehiclePSFU: function(key) {
                var res = $http.get('/api/crm/GetVehicleListById/' + key);
                return res;
            },
            sendToCRMHistory: function(data) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/crm/InsertCHistoryDataChanged/', [{
                    CustomerId : data.CustomerId,
                    ChangeTypeId : data.ChangeTypeId,
                    Modul : data.Modul,
                    Attribute : data.Attribute,
                    NewValue : data.NewValue,
                    OldValue : data.OldValue
                }]);
            },
            getQuestion: function() {
                return $http.get('/api/as/QuestionsManagement');
            },
            getVehicleTypeColorById: function(id) {
                var url = '/api/crm/GetMVehicleTypeColorById/' + id
                var res = $http.get(url);
                return res;
            },
            NewGetMVehicleTypeColorById: function(id) {
                var url = '/api/crm/NewGetMVehicleTypeColorById/' + id
                var res = $http.get(url);
                return res;
            },
            getCekCustomerList: function(data, type) {
                var typeBaru = 0;
                if (type == 3 || type == 1) {
                    typeBaru = 1;
                } else {
                    typeBaru = 2;
                }

                // data.Customer.newBirthDate == "NaN/NaN/NaN"
                console.log("Data Cek Customer ==>", data, typeBaru);
                if (typeBaru == 1) {
                    return $http.put('/api/crm/CekCustomerList/' + typeBaru, [{
                        KTP: data.Customer.KTPKITAS,
                        // NPWP: data.Customer.Npwp,
                        // SIUP: data.Customer.SIUP,
                        CustomerName: data.Customer.Name,
                        BirthDate: data.Customer.newBirthDate == "NaN/NaN/NaN" ? '-' : data.Customer.newBirthDate,
                        Phone: data.Customer.HandPhone1
                    }]);
                } else if (typeBaru == 2) {
                    return $http.put('/api/crm/CekCustomerList/' + typeBaru, [{
                        // KTP: data.Customer.KTPKITAS,
                        NPWP: data.Customer.Npwp,
                        SIUP: data.Customer.SIUP,
                        CustomerName: data.Customer.Name
                            // BirthDate: data.Customer.newBirthDate,
                            // Phone: data.Customer.HandPhone1
                    }]);
                }
            },
            GetCAddressCategory: function() {
                var res = $http.get('/api/crm/GetCAddressCategory/');

                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },

            getStatWO: function (JobId, status) {
                return $http.get('/api/as/Jobs/GetStatusJob/' + JobId + '/' + status);
            },
            
            getTowassWO: function(vin) {
                // var res = $http.get('/api/as/Towass/getFieldAction/1/' + vin);
                var res = $http.get('/api/as/Towass/getFieldAction/0/' + vin);
                return res;
            },
            getMRS: function(ReminderTypeId) {
                var res = $http.get('/api/as/Towass/getFieldMRS/' + ReminderTypeId);
                return res;
            },
            CheckNopolQueueListSecurity: function(type, nopol, jobid) {
                var res = $http.get('/api/as/CheckPoliceNoAvalailability/' + type + '/' + nopol + '/' + jobid);
                return res
            },

            CheckVehicleStatusStock: function(vin) {
                // api/crm/CheckVehicleStatusStock/{VehicleId}
                var res = $http.get('/api/crm/CheckVehicleStatusStock/' + vin);
                return res
            },

            CheckVINisOK: function(VIN, PoliceNo, Flag) {
                var res = $http.put('/api/crm/CheckVINisOK/' + VIN + '/' + PoliceNo + '/' + Flag);
                return res
            },
            UpdateChangeNopol: function(id, oldPoliceNo, newPoliceNo, vid) {
                if (id == null || id == undefined){
                    id = 0;
                }
                return $http.put('/api/as/UpdateQueueGateVehicle/' + id + '/'+ oldPoliceNo + '/' + newPoliceNo + '/' + vid);
            },
            createNewVehicleNonTAM: function(data, isSatelite) {
                console.log("createNewVehicleNonTAM", data);
                if (isSatelite != true){
                    return $http.post('/api/crm/PostVehicleNonTAM/', [{
                        VIN: data.Vehicle.VIN,
                        LicensePlate: data.Vehicle.LicensePlate,
                        EngineNo: data.Vehicle.EngineNo,
                        AssemblyYear: data.Vehicle.AssemblyYear,
                        DECDate: data.Vehicle.DECDate,
                        STNKName: data.Vehicle.STNKName,
                        STNKAddress: data.Vehicle.STNKAddress,
                        KatashikiCode: data.Vehicle.KatashikiCode,
                        STNKDate: data.Vehicle.STNKDate,
                        GasTypeId: data.Vehicle.GasTypeId,
                        OutletId: currUser.OrgId,
                        ModelName: data.Vehicle.ModelName,
                        ModelType: data.Vehicle.ModelType,
                        ColorName: data.Vehicle.ColorName,
                        KatashikiCode: data.Vehicle.KatashikiCodeNonTAM
                    }]);
                } else {
                    return $http.post('/api/crm/PostVehicleNonTAMSatellite/', [{
                        VIN: data.Vehicle.VIN,
                        LicensePlate: data.Vehicle.LicensePlate,
                        EngineNo: data.Vehicle.EngineNo,
                        AssemblyYear: data.Vehicle.AssemblyYear,
                        DECDate: data.Vehicle.DECDate,
                        STNKName: data.Vehicle.STNKName,
                        STNKAddress: data.Vehicle.STNKAddress,
                        KatashikiCode: data.Vehicle.KatashikiCode,
                        STNKDate: data.Vehicle.STNKDate,
                        GasTypeId: data.Vehicle.GasTypeId,
                        OutletId: currUser.OrgId,
                        ModelName: data.Vehicle.ModelName,
                        ModelType: data.Vehicle.ModelType,
                        ColorName: data.Vehicle.ColorName,
                        KatashikiCode: data.Vehicle.KatashikiCodeNonTAM
                    }]);
                }
                
            },
            updateVehicleNonTAM: function(data, isSatelite) {
                console.log("updateVehicleNonTAM", data);
                if (isSatelite != true){
                    return $http.put('/api/crm/PutVehicleNonTAM/', [{
                        VehicleId: data.Vehicle.VehicleId,
                        VIN: data.Vehicle.VIN,
                        LicensePlate: data.Vehicle.LicensePlate,
                        EngineNo: data.Vehicle.EngineNo,
                        AssemblyYear: data.Vehicle.AssemblyYear,
                        DECDate: data.Vehicle.DECDate,
                        KatashikiCode: data.Vehicle.KatashikiCode,
                        STNKName: data.Vehicle.STNKName,
                        STNKAddress: data.Vehicle.STNKAddress,
                        STNKDate: data.Vehicle.STNKDate,
                        GasTypeId: data.Vehicle.GasTypeId,
                        OutletId: currUser.OrgId,
                        ModelName: data.Vehicle.ModelName,
                        ModelType: data.Vehicle.ModelType,
                        ColorName: data.Vehicle.ColorName,
                        KatashikiCode: data.Vehicle.KatashikiCodeNonTAM
                    }]);
                } else {
                    return $http.put('/api/crm/PutVehicleNonTAMSatellite/', [{
                        VehicleId: data.Vehicle.VehicleId,
                        VIN: data.Vehicle.VIN,
                        LicensePlate: data.Vehicle.LicensePlate,
                        EngineNo: data.Vehicle.EngineNo,
                        AssemblyYear: data.Vehicle.AssemblyYear,
                        DECDate: data.Vehicle.DECDate,
                        KatashikiCode: data.Vehicle.KatashikiCode,
                        STNKName: data.Vehicle.STNKName,
                        STNKAddress: data.Vehicle.STNKAddress,
                        STNKDate: data.Vehicle.STNKDate,
                        GasTypeId: data.Vehicle.GasTypeId,
                        OutletId: currUser.OrgId,
                        ModelName: data.Vehicle.ModelName,
                        ModelType: data.Vehicle.ModelType,
                        ColorName: data.Vehicle.ColorName,
                        KatashikiCode: data.Vehicle.KatashikiCodeNonTAM
                    }]);
                }
                
            },
            getDataWOSAList: function(isGr) {
                // if(isGr == 0){
                //     return $http.get('/api/as/jobs/WoListAktif/' + isGr + '?SAId=1');
                // }
                // return $http.get('/api/as/jobs/WoListAktif/' + isGr + '?SAId=1');
                if (isGr == 3){
                    // berarti login di satelite
                    return $http.get('/api/as/jobs/WoListAktifSP/' + isGr + '?SAId=0');
                } else {
                    // berarti login di center
                    return $http.get('/api/as/jobs/WoListAktifSP/' + isGr + '?SAId=1');
                }
            },
            getTransactionCode: function() {
                // var catId = 2028;
                // var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                // console.log('resnya getTransactionCode=>', res);
                // return res;

                return $http.get('/api/as/InvoiceTypes');
            },
            checkGI: function(item, isSatelite) {
                if (isSatelite != true){
                    var res = $http.get('/api/as/jobs/cekGI/' + item.JobId);
                } else {
                    var res = $http.get('/api/as/jobs/cekGISatellite/' + item.JobId);
                }
                return res
            },
            getWobyJobId: function(filter) {
                var res = $http.get('/api/as/Jobs/' + filter);
                return res;
            },
            GetDataServiceExplanation: function(JobID) {
                var res = $http.get('/api/as/ServiceExplanations/getByJobId/' + JobID);
                //console.log('res=>',res);
                return res;

            },
            UpdateServiceExplanation: function(data) {
                data.SuggestionDate = this.changeFormatDate(data.SuggestionDate);
                data.FollowUpDate = this.changeFormatDate(data.FollowUpDate);
                return $http.put('/api/as/ServiceExplanations/Update/' + data.JobId + '', [
                    [{
                        InsideCleaness: data.InsideCleaness,
                        OutsideCleaness: data.OutsideCleaness,
                        CompletenessVehicle: data.CompletenessVehicle,
                        UsedPart: data.UsedPart,
                        DriverCarpetPosition: data.DriverCarpetPosition,
                        IsJobResultExplainOk: data.IsJobResultExplainOk,
                        JobResultExplainReason: data.JobResultExplainReason,
                        IsSparePartExplainOk: data.IsSparePartExplainOk,
                        SparePartExplainReason: data.SparePartExplainReason,
                        IsConfirmPriceOk: data.IsConfirmPriceOk,
                        ConfirmPriceReason: data.ConfirmPriceReason,
                        IsJobSuggestExplainOk: data.IsJobSuggestExplainOk,
                        JobSuggestExplainReason: data.JobSuggestExplainReason,
                        IsConfirmVehicleCheckOk: data.IsConfirmVehicleCheckOk,
                        ConfirmVehicleCheckReason: data.ConfirmVehicleCheckReason,
                        OldPartsKeepPlace: data.OldPartKeepPlace,
                        TimeStartSE: data.TimeStartSE,
                        TimeFinishSE: data.TimeFinishSE
                    }],
                    [{
                        FollowUpDate: data.FollowUpDate,
                        PhoneType: data.phoneType,
                        PhoneNumber: data.PhoneNumber,
                        StatusFUTime: data.StatusFUTime
                    }],
                    [{
                        SuggestionDate: data.SuggestionDate,
                        SuggestionCatg: data.SuggestionCatg,
                        Suggestion: data.Suggestion
                    }]
                ]);

                // return $http.put('/api/as/Washings/', [{
                //     Id: data.Id,
                //     JobId: data.JobId,
                //     TimeStart: data.TimeStart,
                //     TimeFinish: data.TimeFinish,
                //     TimeThru: data.TimeThru,
                //     StatusWashing: data.StatusWashing
                // }])
            },
            CheckStatusBilling: function(jobid) {
                // api/as/ServiceExplanations/CheckStatusBilling/{jobId}

                var res = $http.get('/api/as/ServiceExplanations/CheckStatusBilling/' + jobid);

                return res;
            },
            
            CheckIsWarrantyAktif: function(JobId) {
                console.log("data checkApproval", JobId);
                return $http.get('/api/as/CheckIsWarrantyAktif/' + JobId);
            },
            checkIsWarranty: function(JobId) {
                console.log("data checkApproval", JobId);
                return $http.get('/api/as/IsWarranty/' + JobId);
            },
            GetDEC: function(vin) {
                return $http.get('/api/as/getDEC/' + vin);
            },
            getDataPKS: function(PoliceNumber) {
                return $http.get('/api/as/PKS/GetByLicensePlate/' + PoliceNumber);
            },
            updatePKS: function(PoliceNumber, totalAkhir) {
                totalAkhir = Math.ceil(totalAkhir) // di round up karena pernah ada totalnya koma, dan karena tagihan y round up hrs nya
                return $http.get('/api/as/PKS/CekPKS/' + PoliceNumber + '/' + totalAkhir);
            },
            createApprovalWarranty: function(tmpData) {
                console.log("jobid", tmpData);
                // for (var i = 0; i < mData.length; i++) {
                // return $http.post('/api/as/ASubmited_ApprovalTOWASS_TR', tmpData);
                return $http.post('/api/as/PostTaskListClaim', tmpData);
                //         "NoClaim": "57812",
                //         // "JobId": mData[i].JobId,
                //         "JobTaskId":[]
                //         "ApprovalCategoryId": 43,
                //         // "ApproverId": null,
                //         // "RequesterId": 1231,
                //         "ApproverRoleId": null,
                //         "RequestorRoleId": 1128,
                //         "StatusApprovalId": 3,
                //         "CategoryServices": 1, // kategori pekerjaan
                //         "RequestReason": mDataIsi.Requestmessage,
                //         "RequestDate": null, // tanggal sekarang
                //         // "RejectReason": null
                //         // ,
                //         // "ApproveRejectDate": null
                //     }]);
                // }
            },
            updateTecoOnlyWarranty: function(item) {
                console.log("itemnya", item);
                return $http.put('/api/as/Jobs/TecoStart/1', [
                    item
                ]);
            },
            getWarrantyInfoWO: function(JobId) {
                return $http.get('/api/as/GetWarrantyInfo/' + JobId + '/1');
            },
            updateStatus17: function(id) {
                return $http.put('/api/as/jobs/UpdateStatusJobs17/' + id);
            },
            updateTeco: function(item) {
                console.log("itemnya", item);
                return $http.put('/api/as/Jobs/TecoStart/0', [
                    item
                ]);
            },
            CekDP: function(JobId) {
                return $http.get('/api/as/TECOCheckDP/'+ JobId);
            },
            CekStatusJobBilling: function(JobId, flag) {
                // console.log("cek data", data);
                var res = $http.get('/api/as/Billings/CekStatusJob/' + JobId + '/' + flag );
                return res;
            },
            updateStatusForWO: function(JobId, toStat) {
                return $http.put('/api/as/UpdateJob/' + JobId + '/StatusTo/' + toStat);
            },
            updateStatusForWONew: function (JobId, toStat, flag) {
                return $http.put('/api/as/UpdateJob/' + JobId + '/StatusTo/' + toStat + '/flag/'+ flag);
            },
            saveServiceExplanations: function(item) {
                console.log("itemnya", item);
                return $http.post('/api/as/ServiceExplanations', [{
                    ServiceExplanation: {
                        ServiceExplainId: 0,
                        JobId: item.JobId,
                        OutsideCleaness: item.OutsideCleaness,
                        InsideCleaness: item.InsideCleaness,
                        CompletenessVehicle: item.CompletenessVehicle,
                        UsedPart: item.UsedPart,
                        DriverCarpetPosition: item.DriverCarpetPosition,
                        IsJobResultExplainOk: item.IsJobResultExplainOk,
                        JobResultExplainReason: item.JobResultExplainReason,
                        IsSparePartExplainOk: item.IsSparePartExplainOk,
                        SparePartExplainReason: item.SparePartExplainReason,
                        IsJobSuggestExplainOk: item.IsJobSuggestExplainOk,
                        JobSuggestExplainReason: item.JobSuggestExplainReason,
                        IsConfirmVehicleCheckOk: item.IsConfirmVehicleCheckOk,
                        IsConfirmPriceOk: item.IsConfirmPriceOk,
                        ConfirmPriceReason: item.ConfirmPriceReason,
                        ConfirmVehicleCheckReason: item.ConfirmVehicleCheckReason
                    },
                    FollowUpDate: item.FollowUpDate,
                    PhoneNo: item.PhoneNo,
                    PhoneType: "Handphone",
                    Time: item.Time
                }]);
            },
            updatePlatNo: function(id, NewPlat) {
                return $http.put('/api/as/queue/updateLicence/1/' + id + '/' + NewPlat + '');
            },
            removeCall: function(item) {
                return $http.put('/api/as/queue/remove/' + item.LicensePlate);
            },
            manualCall: function(item, itemCounter) {
                // Counter Id = 4,its default
                console.log("manualCall.data.Result counter", item, itemCounter);
                return $http.put('/api/as/queue/manualCall/' + item.Id + '/' + item.LicensePlate);
            },

            CheckIsCall: function(IdQueue) {
                // api/as/queue/CheckIsCall/{IdQueue}
                return $http.get('/api/as/queue/CheckIsCall/' + IdQueue);
            },

            getListVehicleCPPK: function(id) {
                return $http.get('/api/crm/GetListVehicleUserPhone/' + id);
            },

            CheckLastContactPerson: function(filter, flag) {
                // api/as/CheckLastContactPerson/{filter}/{flag}
                // flag 1 kirim vehicleid
                // flag 2 kirim nopol

                return $http.get('/api/as/CheckLastContactPerson/' + filter + '/' + flag);
            },
            
            GetCounter: function() {
                var res = $http.get('/api/as/QueueCounters');
                return res
            },
            reCall: function(itemCounter, item) {
                return $http.put('/api/as/queue/recallCall/' + item.Id + '/' + item.LicensePlate);
            },
            updateSkipQueue: function(id) {
                return $http.put('/api/as/queue/updateSkipQueue/' + id);
            },
            getVehicleTypeById: function(id) {
                var url = '/api/crm/GetCVehicleTypeById/' + id
                var res = $http.get(url);
                return res;
            },
            newCustomerVehicleUser: function(vID, cID) {
                return $http.post('/api/crm/PostCustomerVehicleList/', [{
                    CustomerOwnerId: cID,
                    VehicleId: vID,
                    CurrentStatus: 1,
                    CreatedDate: new Date(),
                    CreatedUserId: currUser.UserId

                }])
            },
            GetOplList: function(key, model) {
                var res = $http.get('/api/as/OPLFilter?oplname=' + key + '&modelName=' + model);
                return res
            },
            getDiscountMGroupCustomer: function(data) {
                console.log('data untuk discountMGroupCust', data)
                var Nama = '';
                var tlp1 = '';
                var tlp2 = '';
                var ktp = '';
                var npwp = '';
                if (data.CustomerTypeId == 3 || data.CustomerTypeId == 1) {
                    Nama = data.Name;
                    tlp1 = data.HandPhone1;
                    tlp2 = data.HandPhone2;
                    ktp = data.KTPKITAS;
                    npwp = data.Npwp;
                } else {
                    Nama = data.Name;
                    // tlp1 = data.PICHp;
                    npwp = data.Npwp;
                }
                if (Nama == '' || Nama == undefined || Nama == null){
                    Nama = '-';
                }
                if (tlp1 == '' || tlp1 == undefined || tlp1 == null){
                    tlp1 = '-';
                }
                if (tlp2 == '' || tlp2 == undefined || tlp2 == null){
                    tlp2 = '-';
                }
                if (ktp == '' || ktp == undefined || ktp == null){
                    ktp = '-';
                }
                if (npwp == '' || npwp == undefined || npwp == null){
                    npwp = '-';
                }
                return $http.put('/api/crm/PutDiscountMGroupCustomer', [{
                    // Name: data.Name,
                    // Phone1: data.HandPhone1,
                    // Phone2: data.HandPhone2,
                    // KtpNo: data.KTPKITAS,
                    // NPWP: data.Npwp
                    Name: Nama,
                    Phone1: tlp1,
                    Phone2: tlp2,
                    KtpNo: ktp,
                    NPWP: npwp
                }]);
            },
            BackToQueue: function(data, param, jobid) {
                return $http.post('/api/as/queue/BackToQueue/'+ param + '/' + jobid , [{
                    CallTime: data.CallTime,
                    CounterId: data.CounterId,
                    EndTime: data.EndTime,
                    Id : data.Id,
                    LicensePlate : data.LicensePlate,
                    NumberOfCalls : data.NumberOfCalls,
                }]);
            },
            postApprovalDiscountTask: function(jobId, data, ApprovalCategoryId) {
                console.log("dataaaaa", data);
                for(var t in data){
                    if(data[t].EstimationBPId != undefined){
                        data[t].IsCustomDiscount = 0; 
                        if(data[t].JobParts != undefined){

                            for(var k in data[t].JobParts){
                                data[t].JobParts[k].IsCustomDiscount = 0;
                            }
                        }
                        // JobList[t].EstimationBPId ;
                        // JobList[t].EstimationBPId ;
                        // JobList[t].EstimationBPId ;
                    }
                }
                return $http.post('/api/as/PostApprovalDiscountTask', [{
                    JobId: jobId,
                    JobTask: data,
                    ApprovalCategoryId: ApprovalCategoryId
                }]);
            },
            createEXT: function(data) {
                return $http.post('/api/as/JobWACExts', data);
            },
            updateEXT: function(data) {
                return $http.put('/api/as/JobWACExts', data);
            },
            getProbPoints: function(JobId, TypeId) {
                return $http.get('/api/as/JobWACExts/Job/' + JobId + '/' + TypeId);
            },
            CancelWOWalkin: function(data) {
                if (data.PoliceNumber == null || data.PoliceNumber == undefined) {
                    data.PoliceNumber = data.LicensePlate;
                }
                return $http.put('/api/as/Gate/updateCancelWO', [{
                    PoliceNo: data.PoliceNumber,
                    nowTime: new Date(),
                    Id: data.Id
                }])
            },
            GetStatusJobByPoliceNo: function(data) {
                //api/as/Gates/CheckInProduction/4239
                console.log("cek data", data);
                var res = $http.get('/api/as/Jobs/GetStatusJobByPoliceNo/' + data.LicensePlate + '/' + data.Id + '/' + 2);
                return res;
            },
            getT1: function() {
                // var catId = 2023;
                var res = $http.get('/api/as/GetT1');
                // console.log('resnya pause=>',res);
                return res;
            },
            getDataGM: function(catId) {
                var res=$http.get('/api/as/GlobalMaster?CategoryId='+catId);
                return res;
            },
            getDataVH: function(category, vin, StartDate1, StartDate2) {
                // var res=$http.get('/api/as/Jobs/'+category+'/'+vehicleid);
                // var res=$http.get('/api/ct/GetVehicleService/'+category+'/'+vehicleid+'');
                var res = $http.get('/api/ct/GetVehicleService/' + category + '/' + vin + '/' + StartDate1 + '/' + StartDate2);

                console.log('res dari wo history crm=>', res);
                return res;
            },
            getDataDetail: function(ClaimNo, AppStatus) {
                var res = $http.get('/api/as/Towass/getCreateClaim/' + ClaimNo + '/' + AppStatus);
                return res;
            },
            getDataDetailGParam: function(id) {
                var res = $http.get('/api/as/GeneralParameters/' + id);
                //console.log('res=>',res);
                //res.data.Result = null;
                return res;
              },
            getPreDiagnoseByJobId: function(id) {
                return $http.get('/api/as/GetPrediagnoses/job/' + id);
            },
            checkAvailableByPoliceNo: function(type, data,flag, jobid) {
                var res = $http.get('/api/as/Gate/checkAvailableByPoliceNo/' + type + '/' + data.LicensePlate +'/'+flag+'/'+jobid);
                return res;
            },
            getJamServer: function() {
                var res = $http.get('/api/as/GetDateTimeServer/');
                return res;
            },

            postOPLData: function(data,JobId,mdata) {
                console.log("dataaaaa", data);
                console.log("JobId factory",JobId);
                var dataSave = [];
                // "JobTaskOplId": 0,
                //                 "OPLName": "Light Service - Seal Oring - (Internal)",
                //                 "OPLId": 21747,
                //                 "Price": 71500,
                //                 "OPLPurchasePrice": 55000,
                //                 "NormalPrice": 71500,
                //                 "QtyPekerjaan": 1,
                //                 "PaidById": "31",
                //                 "TargetFinishDate": "2020/06/15",
                //                 "Discount": 0,
                //                 "DiscountedPriceOPL": 0,
                //                 "OutletId": 280,
                //                 "IsGR": 1,
                //                 "JobId": 8937,
                //                 "Notes": "Tidak ada catatan",
                //                 "Status": 0,
                //                 "isDeleted": 0,
                //                 "VendorId": 273
                for(var i in data){

                    for(var j in data[i].detail){
                    // Agar tdk backdate
                        var date = new Date(data[i].detail[j].EstimationDate);
                        date.setDate(date.getDate() + 1);
                        data[i].detail[j].JobTaskOplId = 0;
                        data[i].detail[j].OPLName =  data[i].detail[j].TaskNameOpl;
                        data[i].detail[j].OPLId =  data[i].detail[j].oplId;
                        data[i].detail[j].Price =  data[i].detail[j].HargaJual;
                        data[i].detail[j].OPLPurchasePrice =  data[i].detail[j].HargaBeli;
                        data[i].detail[j].NormalPrice =  data[i].detail[j].HargaJual;
                        data[i].detail[j].QtyPekerjaan =  data[i].detail[j].Qty;
                        data[i].detail[j].PaidById =  data[i].detail[j].PaymentId;
                        data[i].detail[j].TargetFinishDate =  date;
                        data[i].detail[j].EstimationDate =  date;
                        data[i].detail[j].Discount =  data[i].detail[j].Discount;
                        data[i].detail[j].DiscountedPriceOPL =  data[i].detail[j].Discount;
                        data[i].detail[j].OutletId =  currUser.OutletId;
                        data[i].detail[j].isGr =  0;
                        data[i].detail[j].JobId =  JobId;//JobId
                        data[i].detail[j].Notes =  data[i].detail[j].Notes;
                        data[i].detail[j].Status =  0;
                        data[i].detail[j].isDeleted =  0;
                        data[i].detail[j].VendorId =  data[i].VendorId;
                        data[i].detail[j].Guid_Log =  mdata.GUIDWOBP
                    }
                    dataSave.push(
                            {
                              "VendorId": data[i].VendorId,
                              "VendorName": data[i].Vendor,
                              "OplNo": null,
                              "OPLTasks": data[i].detail
                          }
                    );
                }

                return $http.post('/api/as/JobTaskOPLCR/SubmitData', dataSave);
            },
            CheckIsJobCancelWO: function (jobId) {
                console.log("data di ReqCancelWO", jobId);
                return $http.get('/api/as/CheckIsJobCancelWO/'+jobId);
            },

            CheckIsReception: function (IdQueue) {
                console.log("data di ReqCancelWO", IdQueue);
                return $http.get('/api/as/queue/CheckIsReception/'+IdQueue);
            },

            //CR4
            getDataAktif: function() {
                var res = $http.get('/api/as/MAppointmentDiscount/GetAktif/?start=1&limit=100000');                
                return res;
            },

            updateWODiscount: function(dataTask,dataParts) {
                console.log("dataTask difac",dataTask);
                console.log("dataParts difac",dataParts);
                return $http.put('/api/as/DiskonUpdateWO/Update', [{
                    JobTask : dataTask,
                    JobParts : dataParts
                }]);
            },

            getEstimateDataMaster: function(data) {
                console.log("NoPolisi Estimasi",data);
                var res = $http.get('/api/as/AEstimationBP/GetListEstimationWOList?start=1&limit=1000&nopol='+data);
                return res;
                // return $http.get('/api/as/AEstimationBP/Get?start=1&limit=1000&NoPolisi='+data)
            },

           getDetailEstimasi: function (params,PoliceNo) {
               var res = $http.get('/api/as/AEstimationBP/GetDetail?EstId='+params+'&PoliceNumber='+PoliceNo+'&Ref=1');
               return res;
           },


           getDetailEstimasiView: function (params,PoliceNo) {
               var res = $http.get('/api/as/AEstimationBP/GetDetail?EstId='+params+'&PoliceNumber='+PoliceNo+'&Ref=0');
               return res;
           },


           updateDataSPK: function(data) {
               console.log('data update spk factory >>>', data);

               return $http.post('/api/as/AEstimationBP/UpdateDataSPK/',data)
           },

           UpdateJobAppointmentAddEstimasiId: function(jobId, arrayEstimasiId) {
               return $http.put('/api/as/AEstimationBP/UpdateJobAppointment?JobId=' + jobId, arrayEstimasiId);
           },
           
           getDataJobTask: function(data) {
               // var url = "&NoPolisi=" + $scope.mDataDetail.LicensePlate; 
               var res = $http.get('/api/as/GetJobTaskSPK/' +data);
               return res;
           },
           getKeluhan: function(data) {
                var res = $http.get('/api/as/AEstimationBP/GetComplaintEstimation?jobId='+data);
                return res;
           },

           createEstimateCR: function(data) {
               console.log('ini factory post data | create ===>', [data])
               return $http.post('/api/as/AEstimationBP/SubmitData', [data]);
           },
            CekStatusPenguranganJob: function(JobId) {
                // console.log("cek data", data);
                var res = $http.get('/api/as/CekJobIdApprovalJobReduction/' + JobId );
                return res;
            },
            CekStatusDiskonJob: function(JobId) {
                // console.log("cek data", data);
                var res = $http.get('/api/as/CekJobIdApprovalDiscount/' + JobId );
                return res;
            },

           getDataVendorOPL: function() {
               return $http.get('/api/as/GetVendorOPL?start=1&limit=100')
           },

           getNewOplList: function(key, model,vendorid) {
               console.log("key, model,vendorid",key, model,vendorid);
               var res = $http.get('/api/as/GetTaskOPLbyVendor?oplname='+key+'&modelName='+model+'&VendorId='+vendorid);
               return res
           },
            // getDataEstimasi: function(data) {
            //     var res = $http.get('/api/as/AEstimationBP/GetNopolAppointment?NoPolisi='+data);
            //     return res;
            // },
            checkStatusdiGate: function(QueueId, Nopol) {
                //cek kalo mobil tiba2 gateout
                // api/as/queue/CheckGate/{QueueId}/{Nopol}
                var res = $http.get('/api/as/queue/CheckGate/' + QueueId + '/' + Nopol);
                return res;
            },
            
            CheckApprovalDiscountTask: function(JobId) {
                var res = $http.get('/api/as/CheckApprovalDiscountTask/' + JobId );
                return res;
            },
            CheckApprovalDiscountPart: function(JobId) {
                var res = $http.get('/api/as/CheckApprovalDiscountPart/' + JobId );
                return res;
            },
            CekStatusGateDataSatelite: function(JobId, PoliceNo) {
                // untuk cek ga boleh SE kl 666
                var res = $http.get('/api/as/Satellite/CekStatusGate/' + JobId + '/' + PoliceNo );
                return res;
            },
            CekStatusBillingDataSatelite: function(JobId) {
                // untuk cek ga boleh callcust kl 666
                var res = $http.get('/api/as/Satellite/CekStatusBilling/' + JobId  );
                return res;
            },
            
            checkIsCharacter: function(data){
                if(typeof data == 'string'){
                    data = data.replace(/[^\x20-\x7f\xA\xD]/g, '');
                }
                return data;
            },
            checkVehicleVsJob: function(katasiki, arrayTaskid) {
                // api/as/CheckTaskKatashikiCode/{isGR}
                return $http.put('/api/as/CheckTaskKatashikiCode/0', [{
                    KatashikiCode : katasiki,
                    Task : arrayTaskid
                }]);
            },
            CheckTaskFieldAction: function (vin) {
                // api/as/Towass/CheckTaskFieldAction/{isGr}/{VIN}
                return $http.get('/api/as/Towass/CheckTaskFieldAction/0/' + vin);
            },
            CheckCustomerMainAddress: function (customerid) {
                // api/crm/CheckCustomerMainAddress/{CustomerId}
                return $http.get('/api/crm/CheckCustomerMainAddress/' + customerid);
            },

            CekJobIdApprovalJobReductionBP: function (JobId) {
                // api/as/CekJobIdApprovalJobReductionBP/{JobId}
                return $http.get('/api/as/CekJobIdApprovalJobReductionBP/' + JobId);
            },

            CheckApprovalDiscountOutStandingWO: function (JobId) {
                // api/as/CheckApprovalDiscountOutStandingWO/{JobId}
                // res 888 ada job dan part outstanding
                // res 666 ada job  outstanding
                // res 999 ada part outstanding
                return $http.get('/api/as/CheckApprovalDiscountOutStandingWO/' + JobId);
            },

            CheckApprovalDPOutStandingWO: function (JobId) {
                // api/as/CheckApprovalDPOutStandingWO/{JobId}/{isGR}
                return $http.get('/api/as/CheckApprovalDPOutStandingWO/' + JobId + '/' + 0);
            },
            manualCallSAAssignment: function(item, itemCounter) {
                // Counter Id = 4,its default
                console.log("manualCallSAAssignment.data.Result counter", item, itemCounter);
                return $http.put('/api/as/queue/manualCall/' + -1 + '/' + item.LicensePlate);
            },
            CheckIsSAAssigmentValid: function(PoliceNumber) {
                // api/as/CheckIsSAAssigmentValid/{PoliceNumber}
                return $http.get('/api/as/CheckIsSAAssigmentValid/' + PoliceNumber )
            },

            CheckIsReceptionBackToQueue: function(IdQueue) {
                // api/as/queue/CheckIsReceptionBackToQueue/{IdQueue}
                return $http.get('/api/as/queue/CheckIsReceptionBackToQueue/' + IdQueue )
            },

            CheckIsPDS: function(vin) {
                // api/as/CheckIsPDS/{VIN}
                return $http.get('/api/as/CheckIsPDS/' + vin )
            },

            CekVehicleList: function(PoliceNo) {
                // api/crm/CekVehicleList/{PoliceNo}
                return $http.get('/api/crm/CekVehicleList/' + PoliceNo)
            },

            CallCustCekBilling: function(JobId) {
                // api/as/CallCustCekBilling/{JobId}
                return $http.get('/api/as/CallCustCekBilling/' + JobId)
            },

            

            NewCalculateFinalDPServiceWithoutVehicle: function (JobId) {
                // api/as/NewCalculateFinalDPServiceWithoutVehicle/{JobId}
                return $http.put('/api/as/NewCalculateFinalDPServiceWithoutVehicle/' + JobId );
            },

            GetCVehicleTypeListByKatashiki: function(KatashikiCode) {
                // api/crm/GetCVehicleTypeListByKatashiki/{KatashikiCode}
                return $http.get('/api/crm/GetCVehicleTypeListByKatashiki/' + KatashikiCode)
            },
            GetResetCounterOdometer: function(vin) {
                // api/ct/GetResetCounterOdometer/{VIN}
                return $http.get('/api/ct/GetResetCounterOdometer/' + vin)
            },

            CekGatePDS: function(QueueId) {
                // api/as/CekGatePDS/{QueueId}
                // 666 berarti gatein pds
                // 0 berarti bukan
                return $http.get('/api/as/CekGatePDS/' + QueueId)
            },

            CekStatusPartsTECO: function(JobId, data) {
                // api/as/JobParts/CekStatusPartsTECO/{JobId}
                return $http.post('/api/as/JobParts/CekStatusPartsTECO/' + JobId, data);
            },
            


            GetListCalledQueue: function() {
                // api/as/queue/GetListCalledQueue/
                return $http.get('/api/as/queue/GetListCalledQueue/')
            },


            BackToQueueList: function(id) {
                // api/as/queue/BackToQueueList/{Id}
                return $http.post('/api/as/queue/BackToQueueList/' + id)
            },

            CheckQuueueFrown: function(IdQueue, JobId) {
                // api/as/queue/CheckQuueue/{IdQueue}/{JobId}
                // 666 Frown
                // 0 Biasa
                return $http.get('/api/as/queue/CheckQuueue/' + IdQueue + '/' + JobId)
            },

            isiGuidLogTaskPartOPL: function(item, GL, tipe) {
                if (tipe == 2){ // untuk opl
                    if (item !== null && item !== undefined){
                        for(var i=0; i<item.length; i++){
                            item[i].Guid_Log = GL
                        }
                    }
                    return item
                } else { // untuk job dan parts
                    if (item !== null && item !== undefined){
                        for(var i=0; i<item.length; i++){
                            item[i].Guid_Log = GL
                            if (item[i].JobParts !== null && item[i].JobParts !== undefined){
                                for (var j=0; j<item[i].JobParts.length; j++){
                                    item[i].JobParts[j].Guid_Log = GL
                                }
                            }
                        }
                    }
                    return item;
                }
                
            },
            getDataVIN: function(vin){
                var res = $http.get('/api/as/MasterVehicle_FPCFLCPLC/' + vin)
                return res;
            },
            listHistoryClaim: function(vin){
                var res = $http.get('/api/as/ToWass/ListHistoryClaim?VIN=' + vin)
                return res;
            } 
            
        };
    });