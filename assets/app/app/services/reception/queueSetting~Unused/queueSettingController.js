angular.module('app')
    .controller('queueSettingController', function($scope, $http, CurrentUser, QueueSetting,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mQSetting = null; //Model
    $scope.xQSetting = {};
    $scope.xQSetting.selected=[];

    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    $scope.ismeridian = false;
    $scope.hstep = 1;
    $scope.mstep = 1;
    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    var res=[{
            Id:1,
            CarName:"Camry",
            PoliceNumber:"B5775SDA",
            Name:"Steve Rogers",
            HoursIn:"08:50:20"
        },
        {
            Id:1,
            CarName:"Avanza",
            PoliceNumber:"B5678SDA",
            Name:"Tony Stark",
            HoursIn:"08:10:20"
        }];
    $scope.getData = function() {
        // gridData = []
        $scope.grid.data = res;
        // QueueSetting.getData()
        // .then(
        //     function(res){
        //         gridData = [];
        //         //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
        //         $scope.grid.data = res.data.Result;
        //         console.log("role=>",res);
        //         $scope.loading=false;
        //     },
        //     function(err){
        //         // console.log("err=>",err);
        //     }
        // );
    }
    $scope.DateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    $scope.onValidateSave = function(){
        var dt = new Date('"'+$scope.mQSetting.HoursIn+'"');
        dt.setSeconds(0);
        var dtHours = dt.toTimeString();
        dtHours = dtHours.split(' ');
        $scope.date = dtHours[0];
        $scope.mQSetting.HoursIn = dtHours[0];
        console.log("Waktu masuk",$scope.mQSetting);
        return true;
    }

    $scope.onBeforeEdit = function(row,mode){
        var tHours= new Date("Wed Jan 18 2017 "+$scope.mQSetting.HoursIn+" GMT+0700 (SE Asia Standard Time)");
        tHours = tHours.toISOString();
    }
    $scope.onShowDetail = function(row,mode){
        console.log("===>",$scope.mQSetting.HoursIn);
        var tHours = new Date("Wed Jan 18 2017 "+$scope.mQSetting.HoursIn+" GMT+0700 (SE Asia Standard Time)");
        $scope.mQSetting.HoursIn = tHours;
        console.log("Ini bukan");
    }
    // $scope.hIn = function(){
    //     var d = new Date($scope.mS.TimeThru);
    //     d.getHours();
    //     d.getMinutes();
    //     $scope.maxFrom = d;
    //     if($scope.mShift.TimeFrom > $scope.mShift.TimeThru){
    //         $scope.mShift.TimeFrom =  $scope.mShift.TimeThru;
    //     }
    // }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'Id', width:'7%', visible:false },
            { name:'Mobil', field:'CarName' },
            { name:'Plat Nomor', field:'PoliceNumber' },
            { name:'Nama', field:'Name' },
            { name:'Jam Masuk', field:'HoursIn'},
        ]
    };
});
