angular.module('app')
    .controller('oplTeknisiController', function($scope, $q, $http, CurrentUser, bsAlert, oplTeknisiFactory, AppointmentGrService, $timeout, bsNotify, WO, PrintRpt, MasterVendorFactory) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            // $scope.getNewData();

        });
        //----------------------------------
        // Initialization
        //----------------------------------

        $scope.user = CurrentUser.user();
        $scope.mData = []; //Model
        $scope.filter = {};
        $scope.xRole = { selected: [] };
        $scope.dataGrid = [];
        //----------------------------------
        // Get Data
        //----------------------------------
        var dateFormat = 'dd/MM/yyyy';
        $scope.DateFilterOptions1 = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.DateFilterOptions2 = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        MasterVendorFactory.getDataGroup().then(function(res) {
            console.log("ress.data.Result ===>", res.data.Result);
            $scope.groupVendor = res.data.Result;
        });
        AppointmentGrService.getUnitMeasurement().then(function(res) {
            $scope.unitData = res.data.Result;
        });




        $scope.selectVendor = function (params) {
            console.log('selectVendor ===>',params)
        }

        $scope.gridActionTemplate = '<div class="ui-grid-cell-contents"> \
                <a href="#" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Buat Billing" tooltip-placement="bottom" style="color:#777;"> \
                <i class="fa fa-plus-circle fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
             </div>';

        var gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
            '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Lihat" tooltip-placement="right" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.actEdit(row.entity)" ng-show="row.entity.Status < 3" uib-tooltip="Edit" tooltip-placement="right" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.cetakOPL(row.entity)" ng-show="row.entity.Status == 2" uib-tooltip="Print" tooltip-placement="right" style="color:#777;"><i class="fa fa-fw fa-lg fa-print" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';
        var gridData = [];


        $scope.getData = function() {
            $scope.loading = true;
            // $scope.grid.data = [
            //     { 'JobTaskOplId':1,'OplNo':1,'PoliceNo':'B556AS','EstimationNo':'23E/EST/00001','WoNo':'23E/WO/00001','WoDate':'03 / Juni / 2018','VendorGroupName':'TAM','Status':0 ,'xStatus':'Open'},
            //     { 'JobTaskOplId':2,'OplNo':2,'PoliceNo':'B557AS','EstimationNo':'24E/EST/00002','WoNo':'24E/WO/00002','WoDate':'26 / Desember / 2018','VendorGroupName':'TAM','Status':1 ,'xStatus':'On Progress'},
            //     { 'JobTaskOplId':3,'OplNo':3,'PoliceNo':'B558AS','EstimationNo':'24E/EST/00003','WoNo':'24E/WO/00003','WoDate':'17 / Juni / 2018','VendorGroupName':'TAM','Status':2 ,'xStatus':'Completed'},
            // ];

            // if ($scope.filter.ShowData !== null && typeof $scope.filter.ShowData == 'undefined') {
            if ($scope.filter.ShowData != null && $scope.filter.ShowData !== undefined) {
                if ($scope.filter.ShowData == 1 || $scope.filter.ShowData == 2) {
                    if ($scope.filter.Value == null || $scope.filter.Value === undefined || $scope.filter.Value == "") {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Mohon Input Filter",
                        });
                        return;
                    }
                } else {
                    if ($scope.filter.GroupId == null || $scope.filter.GroupId === undefined || $scope.filter.GroupId == "") {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Mohon Input Filter Group",
                        });
                        return;
                    }
                }

                var dataFromGetData = [];
                var getEstimasiFromMasterEstimasiBP = [];
                console.log('filter.Value ===>', $scope.filter)
                oplTeknisiFactory.getData($scope.filter.ShowData, $scope.filter).then(function(res) {
                    $scope.loading = false;
                    for (var i = 0; i < res.data.Result.length; i++) {

                        switch (res.data.Result[i].Status) {
                            case 0:
                                res.data.Result[i].xStatus = "Open";
                                break;
                            case 1:
                                res.data.Result[i].xStatus = "On Progress";
                                break;
                            case 2:
                                res.data.Result[i].xStatus = "Completed";
                                break;
                            case 3:
                                //res.data.Result[i].xStatus = "Billing"; dicomment dulu kata Kang Dody
                                res.data.Result[i].xStatus = "Closed";
                                break;
                            case 4:
                                res.data.Result[i].xStatus = "Cancel";
                                break;
                        }
                        console.log('data masuk ke 4 3', res.data.Result[i]);
                    }

                    dataFromGetData = res.data.Result;







                    if($scope.filter.ShowData !==3){ //no polisi atau no wo
                        if($scope.filter.GroupId == null || $scope.filter.GroupId == undefined){
                            var generateUrl = '?NoPolisi='+ $scope.filter.Value + '&type=1&groupid=0'
                        }else{
                            var generateUrl = '?NoPolisi='+ $scope.filter.Value + '&type=1&groupid='+ $scope.filter.GroupId
                        }
                    }else{ //all
                        var generateUrl = '?NoPolisi=0&type=2&groupid='+$scope.filter.GroupId
                    }
                    
                    console.log('Generate Url ====>',generateUrl)
                    oplTeknisiFactory.getEstimasiFromMasterEstimasiBP(generateUrl).then(function(result) {
                        console.log('getEstimasiFromMasterEstimasiBP ===>',result)
                        getEstimasiFromMasterEstimasiBP = result.data.Result;
                        console.log('getEstimasiFromMasterEstimasiBP | JobId ====>',getEstimasiFromMasterEstimasiBP);
                        console.log('dataFromGetData                 | JobId ====>',dataFromGetData);
    
                        if(getEstimasiFromMasterEstimasiBP.length > 0){
                            for(var y in  getEstimasiFromMasterEstimasiBP){
                                for(var x in dataFromGetData){ 
                                    if(dataFromGetData[x].JobId == getEstimasiFromMasterEstimasiBP[y].JobId){
                                        dataFromGetData[x].Job.EstimationNo = getEstimasiFromMasterEstimasiBP[y].EstimationNo;
                                    }
                                }
                            }
                        }
                        
                        $scope.grid.data = dataFromGetData;
                        console.log('$scope.grid.data ===>',$scope.grid.data)
                    });


                    

                    

                });

                

            }

            // if ($scope.filter.ShowData !== null && $scope.filter.ShowData !== undefined && $scope.filter.Value !== null && $scope.filter.Value !== undefined ||
            //     $scope.filter.ShowData !== null && $scope.filter.ShowData !== undefined && $scope.filter.NoPol !== null && $scope.filter.NoPol !== undefined &&
            //     $scope.filter.NoWo !== null && $scope.filter.NoWo !== undefined) {

            //     oplTeknisiFactory.getData($scope.filter.ShowData, $scope.filter).then(function(res) {
            //         $scope.loading = false;
            //         for (var i = 0; i < res.data.Result.length; i++) {

            //             switch (res.data.Result[i].Status) {
            //                 case 0:
            //                     res.data.Result[i].xStatus = "Open";
            //                     break;
            //                 case 1:
            //                     res.data.Result[i].xStatus = "On Progress";
            //                     break;
            //                 case 2:
            //                     res.data.Result[i].xStatus = "Completed";
            //                     break;
            //                 case 3:
            //                     //res.data.Result[i].xStatus = "Billing"; dicomment dulu kata Kang Dody
            //                     res.data.Result[i].xStatus = "Closed";
            //                     break;
            //                 case 4:
            //                     res.data.Result[i].xStatus = "Cancel";
            //                     break;
            //             }
            //             console.log('data masuk ke 4 3', res.data.Result[i]);
            //         }
            //         $scope.grid.data = res.data.Result
            //     });

            // } else {
            //     bsNotify.show({
            //         size: 'big',
            //         type: 'danger',
            //         title: "Mohon Input Filter",
            //     });
            // }
        }
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        };
        $scope.formApi = {};
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        };

        $scope.onShowDetail = function(row, mode) {
            console.log("onShowDetail=>", row, mode);
            console.log("fromApi=>", $scope.formApi);
        };

        $scope.onBeforeEdit = function(row, mode) {
            console.log("onBeforeEdit=>", row, mode);
            console.log("fromApi=>", $scope.formApi);
            // $scope.formApi.setMode('detail');
        };
        // ====== NEW CODE =====
        $scope.select = function(param) {
                console.log('Filter By ===>',param);
                $scope.filter.Value = null;
                $scope.filter.NoWo = null;
                $scope.filter.NoPol = null;
            }
            // =====================
        $scope.cetakOPL = function(data) {
            console.log('mdata cetakOPL', data);
            // var data = $scope.mData;
            // $scope.cetakanOPL = 'as/PrintOpl/83/655/2/0';
            $scope.cetakanOPL = 'as/PrintOplTeknisiBP/' + data.JobId + '/' + $scope.user.OrgId + '/' + data.JobTaskOplBPId + '/0/0';
            // return $q.resolve($scope.cetakanOPL);
            $scope.cetakan($scope.cetakanOPL);

        };
        $scope.cetakan = function(data) {
            var pdfFile = null;

            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame 
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    } else {
                        printJS(pdfFile);
                    }
                } else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };

        $scope.tombol = [{
                actionType: 'back',
                title: 'Kembali',
            },
            {
                func: function(row, formScope) {

                    // billllllllllllllllllllllllllllllllllllllllllllllll
                    console.log('complete opl', row);
                    oplTeknisiFactory.checkstatusopl(row.JobId, row.Status).then(function(resxx) {
                        if (resxx.data == 0){
                            oplTeknisiFactory.saveUpdateOPL(row).then(function(res) {
                                console.log("Result After Save", res.data);
                                $scope.getData();
                                $scope.formApi.setMode('grid');
                            });
                        } else {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "OPL Teknisi Tidak Bisa di Edit Karena Sudah Billing, Silahkan Lakukan Refresh",
                            });
                        }
                        
                    });
                    
                },
                title: 'Simpan',
                icon: 'fa fa-fw fa-save',
                visible: true
            }
        ];

        // $scope.statusOpl = function(param){

        // };

        $scope.actView = function(data) {
            console.log("DATA =====> ", data);
            console.log('tombol', $scope.tombol);
            
            for (var i=0; i<$scope.tombol.length; i++){
                // if ($scope.tombol[i].title == 'Simpan'){
                    $scope.tombol[i].visible = false
                // }
            }
            
            $scope.mData = data;
            $scope.dataGrid = [];
            var dataGridTemp = [];
            WO.getWobyJobId(data.JobId).then(function(res) {
                console.log("res.data.Result Data By JobId", res.data);
                // $scope.dataGrid = res.data.Result[0].JobTask;
                for (var i in res.data.Result[0].JobTask) {
                    _.map($scope.unitData, function (val) {
                        if (val.MasterId == res.data.Result[0].JobTask[i].ProcessId) {
                            res.data.Result[0].JobTask[i].xProsess = val.Name;
                        }
                    });


                    if (res.data.Result[0].JobTask[i].isDeleted == 1) {
                        $scope.dataGrid.jmlPanel = '';
                        delete res.data.Result[0].JobTask[i];
                    } else {
                        res.data.Result[0].JobTask[i].jmlPanel = 1;
                        dataGridTemp.push(res.data.Result[0].JobTask[i]);
                    }
                }
                $scope.dataGrid = dataGridTemp;
            })
            $scope.formApi.setMode('view');
        };

        $scope.actEdit = function(data) {
            console.log("DATA =====> ", data);
            console.log('tombol12121', $scope.tombol);
            for (var i=0; i<$scope.tombol.length; i++){
                // if ($scope.tombol[i].title == 'Simpan'){
                    $scope.tombol[i].visible = true
                // }
            }
            $scope.mData = data;
            $scope.dataGrid = [];
            var dataGridTemp = [];
            WO.getWobyJobId(data.JobId).then(function(res) {
                console.log("res.data.Result Data By JobId", res.data);
                for (var i in res.data.Result[0].JobTask) {
                    _.map($scope.unitData, function(val) {
                        if (val.MasterId == res.data.Result[0].JobTask[i].ProcessId) {
                            res.data.Result[0].JobTask[i].xProsess = val.Name;
                        }
                    });


                    if (res.data.Result[0].JobTask[i].isDeleted == 1) {
                        $scope.dataGrid.jmlPanel = '';
                        delete res.data.Result[0].JobTask[i];
                    } else {
                        res.data.Result[0].JobTask[i].jmlPanel = 1;
                        dataGridTemp.push(res.data.Result[0].JobTask[i]);
                    }
                }
                $scope.dataGrid = dataGridTemp;
            })
            $scope.formApi.setMode('detail');
        };

        // $scope.onValidateSave = function(data){
        //     oplList.update(data).then(
        //                     function(res){
        //                        // $scope.getData();
        //                        console.log("simpan success");
        //                     },
        //                     function(err){
        //                         console.log("err=>",err);
        //                     }
        //         );
        //     console.log("validate save", data);
        // }
        //----------------------------------
        // Grid Setup
        //----------------------------------
        var dateFilter = 'date:"dd/MM/yyyy"';

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            enableFiltering: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'oplId', field: 'JobTaskOplBPId', width: 95, visible: false },
                { name: 'NoOPL', displayName: "No. OPL", field: 'OplNo', width: '15%' },
                { name: 'NoPolisi', displayName: "No. Polisi", field: 'Job.PoliceNumber' },
                { name: 'NoEstimasi', displayName: "No. Estimasi", field: 'Job.EstimationNo' },
                { name: 'NoWo', displayName: "No. WO", field: 'Job.WoNo', width: '15%' },
                { name: 'TglWo', displayName: "Tgl. WO", field: 'Job.WoCreatedDate', cellFilter: dateFilter },
                { name: 'GroupVendor', displayName: "Group Vendor", field: 'Vendor.Name', width: '15%' },
                { name: 'Status', displayName: "Status", field: 'xStatus', width: 120 },
                { name: 'Action', width: 180, cellTemplate: gridActionButtonTemplate }
            ],
            onRegisterApi: function(gridApi) {
                // set gridApi on $scope
                $scope.gridApi = gridApi;
                $scope.gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                    // console.log("selected=>",$scope.selectedRows);
                    if ($scope.onSelectRows) {
                        $scope.onSelectRows($scope.selectedRows);
                    }
                });
            }
        };

        // =======Adding Custom Filter==============
        $scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
        $scope.gridApiAppointment = {};
        $scope.filterColIdx = 1;
        var x = -1;
        for (var i = 0; i < $scope.grid.columnDefs.length; i++) {
            if ($scope.grid.columnDefs[i].visible == undefined && $scope.grid.columnDefs[i].name !== 'Action') {
                x++;
                $scope.gridCols.push($scope.grid.columnDefs[i]);
                $scope.gridCols[x].idx = i;
            }
            // console.log("$scope.gridCols", $scope.gridCols);
            // console.log("$scope.grid.columnDefs[i]", $scope.grid.columnDefs[i]);
        }
        $scope.filterBtnLabel = $scope.gridCols[0].name;
        $scope.filterBtnChange = function(col) {
            console.log("col", col);
            $scope.filterBtnLabel = col.name;
            $scope.filterColIdx = col.idx + 1;
        };
    });

app.filter('rupiahC', function() {
    return function(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d+)(\d{3})/, '$1' + '.' + '$2');
        }
        return val;
    };
});