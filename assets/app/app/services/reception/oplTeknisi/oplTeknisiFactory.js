angular.module('app')
    .factory('oplTeknisiFactory', function($http, CurrentUser) {
        var currentUser = CurrentUser.user();
        console.log(currentUser);
        return {
            getData: function(param, item) {
                var tmpItemNopol = '-';
                var tmpItemWO = '-';
                var tmpGrupId = '-'
                if (param == 1) {
                    console.log("isi item ==>", item);
                    tmpItemNopol = item.Value;
                    tmpItemWO = '-'
                } else if (param == 2) {
                    tmpItemWO = item.Value;
                    tmpItemNopol = '-'
                } else if (param == 3) {
                    // tmpItemWO = item.NoWo; //gk dipake, pencarian hanya berdasarkan Group
                    // tmpItemNopol = item.NoPol;
                    tmpItemWO = '-';
                    tmpItemNopol = '-';
                    console.log(" masuk case 3");
                }

                if (item.GroupId !== undefined) {
                    tmpGrupId = item.GroupId.toString();
                }
                var res = $http.put('/api/as/GetListOPLBP/' + param, [{
                    NoPol: tmpItemNopol,
                    NoWO: tmpItemWO,
                    GroupId: tmpGrupId
                }]);
                //console.log('res=>',res);
                //res.data.Result = null;
                return res;
            },

            

            getEstimasiFromMasterEstimasiBP: function(param) {
                return $http.get('/api/as/AEstimationBP/OPLTeknisi/GetEstimasi'+ param);
            },
            saveUpdateOPL: function(item) {
                return $http.put('/api/as/UpdateStatusOPLTeknisi/' + item.JobTaskOplBPId);
            },
            update: function(Data) {
                return $http.put('/api/as/JobTaskOpls/In/' + Data.JobTaskOplId + '', [{
                    InvoiceNo: Data.VendorInvoiceNo,
                    InvoiceDate: Data.VendorInvoiceDate
                }]);
            },
            updateStatusPrintOPL: function(data) {
                return $http.get('/api/as/JobTaskOpls/print/' + data.JobTaskOplId + '/' + data.ReceivedBy);
            },
            updateStatusCompleteOPL: function(data, Price, Discount) {
                return $http.put('/api/as/JobTaskOpls/In/' + data.JobTaskOplId, [{
                    InvoiceNo: data.VendorInvoiceNo,
                    InvoiceDate: data.VendorInvoiceDate,
                    Price: Price,
                    Discount: Discount
                }]);
            },
            updateStatusCancelOPL: function(data) {
                console.log('data updateStatusCancelOPL', data);
                // return $http.post('/api/as/JobTaskOpls/Cancel', [{ JobTaskOplId: data.JobTaskOplId }]);
                return $http.post('/api/as/JobTaskOpls/Cancel', [data.JobTaskOplId]);
            },
            updateStatusCancelOPLMore: function(data) {
                console.log('data updateStatusCancelOPL', data);
                // return $http.post('/api/as/JobTaskOpls/Cancel', [{ JobTaskOplId: data.JobTaskOplId }]);
                return $http.post('/api/as/JobTaskOpls/Cancel', data);
            },
            ReOpenOPL: function(data) {
                console.log('data ReOpenOPL', data);
                // return $http.post('/api/as/JobTaskOpls/Cancel', [{ JobTaskOplId: data.JobTaskOplId }]);
                return $http.put('/api/as/JobTaskOpls/oplToOnProgress/', [data.JobTaskOplId]);
            },
            printOPL: function(jobId, jobOplId, flag) {
                // return $http.get('/api/as/PrintOpl/' + jobId + '/' + currentUser.OrgId + '/' + jobOplId + '/' + flag);
                return $http.get('/api/as/PrintOplTeknisiBP/' + jobId + '/' + currentUser.OrgId + '/' + jobOplId + '/0/0');
            },
            // delete: function(QCategoryId) {
            //   return $http.delete('/api/as/QueueCategories',{data:QCategoryId,headers: {'Content-Type': 'application/json'}});
            // }
            checkGateInOPL: function(jobId, policeNo) {
                var resOpl = $http.get('/api/as/JobTaskOpls/checkGateInOPL?jobId=' + jobId + '&policeNo=' + policeNo);
                return resOpl;
            },
            checkstatusopl: function(jobId, stat) {
                var resOpl = $http.get('/api/as/OPLBP/GetStatusOPLTeknisi/' + jobId + '/' + stat);
                return resOpl;
            },
        }
    });