var xwo1 = angular.module('app');
	xwo1.expandController_OPLController = function($scope, $anchorScroll, $location, $filter, $http, $interval, $timeout, CurrentUser, $q, WO, VehicleHistory, ngDialog, AsbGrFactory, bsAlert, bsNotify, ASPricingEngine, PrintRpt, $compile) {
		console.log("controller2 $scope",$scope);
				// test
		        // using ag-grid change CR4 Phase2
		        initOPLEditor($scope, $compile);
		        initDateEditor($scope, $compile);
		        $scope.viewMode = false;
		        $scope.disableSimpan = false;
		        $scope.mOpl = {};
		        $scope.masterOPL = [];
				$scope.minDate = new Date();
				$scope.finalDataOPL = [];
				$scope.$on('$destroy', function() {
					angular.element(".ui.modal.ModalOPL").remove();
				});

		        WO.getDataVendorOPL().then(function (res) {
		        	for(var i in res.data.Result){
		        	  res.data.Result[i].VendorName = res.data.Result[i].Name;
		        	  res.data.Result[i].Name = ""+res.data.Result[i].VendorCode+" - "+res.data.Result[i].Name;
		        	}
		        	$scope.OplVendor = res.data.Result;
		        	console.log("$scope.OplVendor",$scope.OplVendor);
		        });

		        var OPLPaymentData = [];

		        $scope.chooseVendor = function(){
					console.log("chooseVendor | mOpl ===>",$scope.mOpl);
		        	for(var a in $scope.OplVendor){
		        		if($scope.OplVendor[a].VendorId == $scope.mOpl.vendor){

		        			$scope.mOpl.VendorCode = $scope.OplVendor[a].VendorCode;
		        		}
		        	}
		        }

		        $scope.getdataopl = function(){
		        	console.log("modal close");
		        	// angular.element(".ui.modal.ModalOPL").modal("hide");
		        	setTimeout(function () {
		        	    angular.element('#modalId').modal('refresh');
		        		
		        		// angular.element('.ui.modal.ModalOPL').modal('show');

		        		angular.element('#modalId').remove();
		        	}, 0);
		        }

		        $scope.plushOrderOpl = function(){
		        	// $scope.$on('$destroy', function() {
		        	console.log("$scope.PPNPerc opl",$scope.PPNPerc);
		        	// if(PPNPerc == undefined){
		        	// 	$scope.getPPN();
		        	// }	
		        	// });
		            console.log("$scope.OplVendor",$scope.OplVendor);
		            flagedit = 0;
		            console.log("$scope.OPLPaymentData",$scope.OPLPaymentData);
					WO.getDataVIN($scope.mDataCrm.Vehicle.VIN).then(function(res){
						console.log('cek input rangka opl', $scope.mDataCrm.Vehicle.VIN)
						$scope.vinOPL = res.data.Result;
						console.log('cek res input opl', $scope.vinOPL)
					});
					
		            console.log("OPLPaymentData",OPLPaymentData)
		            // ModalOPL
		            $scope.viewMode = false;
					$scope.disableSimpan = true;
					$scope.masterOPL = [];

					$scope.mOpl.VendorCode = null;
		   			$scope.mOpl.vendor = null;
		   

					$scope.gridDetailOPLApi.setRowData($scope.masterOPL);
					angular.element("#modalId").modal("show");
					angular.element("#modalId").modal({backdrop: 'static'});
					sizeToFit();
					$scope.dialogPosition(1);
					modeOpl = 2;
		        }

				var count = 0;
				$scope.dialogPosition = function(data){
					count = count + data;
					if(count > 1){
						angular.element(".ui.modal.ModalOPL").css('top', '10%');
					}
					$scope.calenderPosition();
				}
		   
		        // ag gridworkview
		        $scope.agGridWorkViewApi
		        function onFirstDataRendered(params){
		            params.api.sizeColumnsToFit();
		            setTimeout(function(){ params.api.getDisplayedRowAtIndex(1).setExpanded(true) }, 0);
		        }

				function onGridSizeChanged(params){
					params.api.sizeColumnsToFit();
					console.log('JALAN RESIZE')
				}

		        $scope.finalDataOPL = [];
		        var DataChoosedDelete = [];
		        // $scope.gridWorkView.data = $scope.master;
		        $scope.reziseColumnAG = function(){
		          var allColumnIds = [];
		            $scope.gridWorkView.columnApi.getAllColumns().forEach(function (column) {
		              allColumnIds.push(column.colId);
		            });
		            console.log("allColumnIds",allColumnIds);
		            $scope.gridWorkView.columnApi.autoSizeColumns(allColumnIds, false);
		        }
		        $scope.gridWorkView = {
		            //DEFAULT AG-GRID OPTIONS
		            // default ColDef, gets applied to every column
		            defaultColDef: {
		                width: 150,
		                editable: false,
		                filter: false,
		                sortable: true,
		                resizable: true
		            },
		            rowSelection: 'multiple',
		            suppressRowClickSelection:true,
					angularCompileRows: true,
					overlayNoRowsTemplate: '<h1 style="font-size:3em; opacity:.25";position:absolute;top:4rem;width:100%;text-align:center;z-index:1000;>No data available</h1>',
		            toolPanelSuppressSideButtons:true,
		            masterDetail:true,
		            detailCellRendererParams:{
		                detailGridOptions : {
		                    columnDefs : [
		                        // {field : 'idChild'},
		                        {headerName: 'No.',field : 'No', suppressSizeToFit:true},
		                        {headerName: 'Nama OPL',field : 'TaskNameOpl', suppressSizeToFit:true},
		                        {headerName: 'Pembayaran',field : 'Payment', suppressSizeToFit:true},
		                        {headerName: 'Tgl Selesai',field : 'EstimationDate', suppressSizeToFit:true, cellRenderer: customDate},
		                        { headerName: 'Qty', field: 'Qty', width: 150},
		                        {headerName: 'Harga Beli',field : 'HargaBeli', suppressSizeToFit:true , cellStyle: {textAlign: "right"},
		                            cellRenderer: function(params){ 
		                                var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
		                                var notOk = '<span style="float:left">Rp.</span>'+ 0;
		                                if(isNaN(params.value) || params.value ==  null || params.value ==  undefined || params.value == "" ){
		                                    return notOk;
		                                } else{
		                                    return isOke;
		                                }
		                            }
		                        },
		                        {headerName: 'Harga Jual',field : 'HargaJual', suppressSizeToFit:true , cellStyle: {textAlign: "right"},
		                            cellRenderer: function(params){ 
		                                var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
		                                var notOk = '<span style="float:left">Rp.</span>'+ 0;
		                                if( isNaN(params.value) || params.value ==  null || params.value ==  undefined || params.value == "" ){
		                                    return notOk;
		                                } else{
		                                    return isOke;
		                                }
		                            }
		                        },
		                        {headerName: 'Diskon (%)',field : 'Discount', suppressSizeToFit:true},
		                        {headerName: 'Note',field : 'Notes', suppressSizeToFit:true, resizable: true, minWidth: 700},
		                        // {headerName: 'DP (%)',field : 'DownPayment', suppressSizeToFit:true},
		                        // {headerName: 'Qty',field : 'Reserve'},
		                    ],
		                    onFirstDataRendered: function(params){
		                        // params.api.sizeColumnsToFit();
		                        var allColumnIds = [];
		                        params.columnApi.getAllColumns().forEach(function(column) {
		                            allColumnIds.push(column.colId);
		                        });
		                    
		                        params.columnApi.autoSizeColumns(allColumnIds, false);
		                    },

		                   


		                },
		                getDetailRowData: function(params){
		                    console.log("params",params)
		                    params.successCallback(params.data.detail);        
		                },

		            },


					onFirstDataRendered: onFirstDataRendered,
					onGridSizeChanged: onGridSizeChanged,
		            // components: {
		            //     partEditor: PartEditor,
		            //     numericEditor: NumericCellEditor,
		            //     numericEditorPct: NumericCellEditorPct
		            // },
		            onGridReady: function(params) {
						$scope.agGridWorkViewApi = params.api;
						sizeToFit();
		            },

		            rowData: $scope.finalDataOPL,
		            onCellEditingStopped: function(e) {
		                console.log('opl cellEditingStopped=>',e.rowIndex,e.colDef.field,e.value,e.node,e);
		            },

		            getDetailRowData: function(params){
		                   
		            },
		            
		            autoSizeAll : function () {
		                var allColumnIds = [];
		                gridWorkView.columnApi.getAllColumns().forEach(function(column) {
		                    allColumnIds.push(column.colId);
		                });
		            
		                gridWorkView.columnApi.autoSizeColumns(allColumnIds, false);
		            },
		            
		            isRowSelectable: function(rowNode) {
		                if(rowNode.data.IsUse == null || rowNode.data.IsUse == 0  || typeof rowNode.data.IsUse == undefined){
		                    return true
		                }else{
		                    return false;
		                }
		            },
		            
		            onRowSelected: function(event) {
		                console.log("event",event);
		                console.log(" selected = " + event.node.selected);
		                if(event.node.selected == true){
		                    DataChoosedDelete.push(event.data);
		                }else{
		                    var filtered = DataChoosedDelete.filter(function(item) { 
		                       return item.FlagID !== event.data.FlagID;  
		                    });
		                    console.log("filtered",filtered);
		                    DataChoosedDelete = filtered;
		                }
		                console.log("DataChoosedDelete",DataChoosedDelete);
		            },
		            columnDefs: [
		              
		                { headerName: 'No.',field: 'No', width: 115, cellRenderer:'agGroupCellRenderer', headerCheckboxSelection: true, checkboxSelection: true},
						{ headerName: 'No. OPL',field: 'OplNo', width: 150},
						{ headerName: 'Vendor',field: 'Vendor', width: 400},
						{ headerName: 'Qty. Pekerjaan',field: 'Qty', width: 160},
						{ headerName: 'Status OPL',field: 'Status', width: 130},
						{ headerName: 'Action', width: 90, minWidth: 100, suppressFilter: true, pinned: 'right',suppressMenu:true, cellRenderer: actionRendererTask}
		                // { headerName: 'Action',width: 100, minWidth:100, maxWidth:100, suppressFilter: true, pinned: 'right', suppressMenu:true, cellRenderer: actionRendererTask}
		            ]
		        };
		        
		        function actionRendererTask(){
		            var actionHtml =' <button class="ui icon inverted grey button"\
		                                style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px"\
		                                onclick="this.blur()"\
		                                ng-click="gridClickViewDetailHandler(data,1)">\
		                                <i class="fa fa-fw fa-lg fa-list-alt"></i>\
		                            </button>\
		                            <button class="ui icon inverted grey button" ng-if="data.IsUse !== 1"\
		                                style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px"\
		                                onclick="this.blur()"\
		                                ng-click="gridClickEditHandler(data,2)">\
		                                <i class="fa fa-fw fa-lg fa-pencil"></i>\
		                            </button>';
		            return actionHtml;
		        }

		        $scope.disJob = false;
		        // $scope.disFlag =
		        $scope.gridClickViewDetailHandler = function(data,flag){
		            console.log('data view',data,flag);
					tmpDataOPL = angular.copy(data);
		            $scope.viewMode = true;
		            $scope.mOpl.vendor = data.VendorId;
		            $scope.chooseVendor();
		            $scope.masterOPL = data.detail;
		            for(var a = 0; a < data.detail.length; a++){
		            	data.detail[a].No = a + 1;
		            }
		            console.log("$scope.masterOPL",$scope.masterOPL);
		            $scope.gridDetailOPLApi.setRowData($scope.masterOPL);
					angular.element(".ui.modal.ModalOPL").modal("show");
					angular.element(".ui.modal.ModalOPL").modal({backdrop: 'static'});
					$scope.dialogPosition(1);
					modeOpl = 1;
		        }
		        var modeOpl;
		        function disabledInputOPL(params){
		                       console.log("params",params);
		                       console.log("modeOpl",modeOpl);
		                       //jika 1 = view
		                       if(modeOpl == 1){
		                           return false;
		                       }else{
		                           return true;
		                       }
		                       
		        }
				function disabledInputOPL_Diskon(params){
					console.log("params",params);
					console.log("modeOpl",modeOpl);
					//jika 1 = view
					if(modeOpl == 1){
						return false;
					}else{
						if (params.data.PaymentId == 30) {
							return false;
						} else {
							return true;
						}
						// return true;
					}
					
				}

				var flagedit = 0;
				var tmpDataOPL = {};
		        $scope.gridClickEditHandler = function(data,flag){
					console.log('gridClickEditHandler | data edit ===>',data,flag);
					tmpDataOPL = angular.copy(data);
		            flagedit = 1;
		            console.log("gridClickEditHandler | flagedit ====>",flagedit);
		            console.log("gridClickEditHandler | tmpDataOPL===>",tmpDataOPL);
					$scope.viewMode = false;
		            $scope.mOpl.vendor = data.VendorId;
		            $scope.chooseVendor();
		            $scope.masterOPL = data.detail;
		            for(var a = 0; a < data.detail.length; a++){
		            	data.detail[a].No = a + 1;
		            }
		            console.log("$scope.masterOPL",$scope.masterOPL);
		            $scope.gridDetailOPLApi.setRowData($scope.masterOPL);
					angular.element(".ui.modal.ModalOPL").modal("show");
					angular.element(".ui.modal.ModalOPL").modal({backdrop: 'static'});
					$scope.dialogPosition(1);
					modeOpl = 2;
				}
				
				function sizeToFit() {
					$scope.agGridWorkViewApi.sizeColumnsToFit();
				}

				$scope.calenderPosition = function(){
					var cols = document.getElementsByClassName('ag-root-wrapper ag-layout-normal ag-ltr');
					for(i = 0; i < cols.length; i++) {
						cols[i].style.overflow = 'auto';
					}
				}

				window.addEventListener( 
					"resize", getSizes, false); 
		  
				function getSizes() { 
					var zoomNow = 0;
					var zoom = ((window.outerWidth - 10) 
						/ window.innerWidth) * 100; 
					
					if(zoomNow != zoom){
						sizeToFit()
					}
				} 

		        // ag gridworkview

		        // ag grid detail
		        $scope.gridDetailOPLApi;
		        $scope.gridDetailOPL = {
		            //DEFAULT AG-GRID OPTIONS
		            // default ColDef, gets applied to every column
		            defaultColDef: {
		                width: 150,
		                editable: false,
		                filter: false,
		                sortable: true,
		                resizable: true
		            },
		            rowSelection: 'multiple',
		            suppressRowClickSelection: true,
		            suppressCellSelection: true,
					angularCompileRows: true,
					overlayNoRowsTemplate: '<h1 style="font-size:3em; opacity:.25";position:absolute;top:4rem;width:100%;text-align:center;z-index:1000;>No data available</h1>',
		            components: {
		                OPLEditor: OPLEditor,
		                DateEditor : DateEditor,
		            //     PartNameEditor : PartNameEditor,
		                 numericCellEditor : NumericCellEditor,
		            //     checkboxEditor: agCheckboxCellEditor
						 noteCellEditor : noteCellEditor,
		            },
		            onGridReady: function (params) {
		                // params.api.sizeColumnsToFit();
		                $scope.gridDetailOPLApi = params.api;

		                // var allColumnIds = [];
		                // $scope.GridParts.columnApi.getAllColumns().forEach(function(column) {
		                //     allColumnIds.push(column.colId);
		                // });

		                // $scope.GridParts.columnApi.autoSizeColumns(allColumnIds, false);

		            },
		            

		            rowData: $scope.masterOPL,
		            onCellEditingStopped: function (e) {
		                console.log('parts cellEditingStopped=>', e.rowIndex, e.colDef.field, e.value, e.node, e);
		                // if(e.colDef.field == 'PartCode'){
		                if($scope.disJob == false){
		                    $scope.onAfterCellEditAg(e.rowIndex,e.colDef.field,e.value,e.node);
		                	if(e.colDef.field == "Notes"){

		                	  var value = e.value.substr(0,200);
		                	  e.node.setDataValue(e.column.colId, value);
		                	  console.log('cellEditingStopped');
		                	}
		                }
		                // }
		            },

		            sideBar: {
		                    toolPanels: ['columns'],
		                    hiddenByDefault: false
		            },
		            toolPanelSuppressSideButtons:true,
		        
		            
		            columnDefs: [

		                { headerName: 'No.', field: 'No', width: 70},
		                { headerName: 'Nama Pekerjaan*', field: 'TaskNameOpl', width: 350,  editable: disabledInputOPL,cellEditor: 'OPLEditor'},
		                { headerName: 'Pembayaran*', field: 'Payment', width: 122, editable: disabledInputOPL, cellEditor: 'agRichSelectCellEditor',cellEditorParams: {values: OPLPaymentData}}, 
		                { headerName: 'Estimasi Selesai*', field: 'EstimationDate', width: 150, editable: disabledInputOPL, cellEditor:'DateEditor', cellRenderer: customDate}, 
		                { headerName: 'Qty*', field: 'Qty', width: 70, editable: disabledInputOPL,cellEditor: 'numericCellEditor'},      
		                { headerName: 'Harga Beli*', field: 'HargaBeli', width: 121, editable: disabledInputOPL,  suppressSizeToFit:true ,cellEditor: 'numericCellEditor', cellStyle: {textAlign: "right"},
		                            cellRenderer: function(params){ 
		                                var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
		                                var notOk = '<span style="float:left">Rp.</span>'+ 0;
		                                if(isNaN(params.value) || params.value ==  null || params.value ==  undefined || params.value == "" ){
		                                    return notOk;
		                                } else{
		                                    return isOke;
		                                }
		                            }
		                },   
		                { headerName: 'Harga Jual*', field: 'HargaJual', width: 121, editable: disabledInputOPL, suppressSizeToFit:true  , cellEditor: 'numericCellEditor', cellStyle: {textAlign: "right"},
		                            cellRenderer: function(params){ 
		                                var isOke = '<span style="float:left">Rp.</span>'+ formatNumber(params.value);
		                                var notOk = '<span style="float:left">Rp.</span>'+ 0;
										if(isNaN(params.value) || params.value ==  null || params.value ==  undefined || params.value == "" ){
		                                    return notOk;
		                                } else{
		                                    return isOke;
		                                }
		                            }
		                }, 
		                { headerName: 'Diskon', field: 'Discount', width: 81, editable: disabledInputOPL_Diskon,suppressSizeToFit:true , cellEditor: 'numericCellEditor', cellStyle: {textAlign: "right"},
		                            cellRenderer: function(params){ 
		                                var isOke = formatNumberDecimal(params.value)+'<span style="float:right">%</span>';
		                                var notOk = 0+'<span style="float:right">%</span>';
		                                if(isNaN(params.value) || params.value ==  null || params.value ==  undefined || params.value == "" ){
		                                    return notOk;
		                                } else{
		                                    return isOke;
		                                }
		                            }
		                },     
		                { headerName: 'Notes', field: 'Notes', width: 150, editable: disabledInputOPL,cellEditor: 'noteCellEditor'},
		                { headerName: 'Action', width: 100, minWidth: 100, maxWidth: 100, suppressFilter: true, suppressMenu: true, pinned: 'right', cellRenderer: actionRenderer2}
		            ]
				};
				

				// ----------------------- note editor -------------------------------------------------------------------------- start

				function noteCellEditor() {
				}
				noteCellEditor.prototype.init = function (params) {
					// create the cell

					var maxL = 200
					if (params.column.colId == 'Notes') {
						maxL = 200
					} 

					this.eInput = document.createElement('input')
					this.eInput.setAttribute('maxLength',maxL);
					console.log('DATAAA1 =>',params.charPress)
					console.log('DATAAA2 =>',params.value)
					
				
				};

				noteCellEditor.prototype.isKeyPressedNavigation = function (event){
					
				};

				// gets called once when grid ready to insert the element
				noteCellEditor.prototype.getGui = function () {
					return this.eInput;
				};

				// focus and select can be done after the gui is attached
				noteCellEditor.prototype.afterGuiAttached = function () {
					this.eInput.focus();
				};

				// returns the new value after editing
				noteCellEditor.prototype.isCancelBeforeStart = function () {
					return this.cancelBeforeStart;
				};

				// returns the new value after editing
				noteCellEditor.prototype.getValue = function () {
					return this.eInput.value;
				};

				// any cleanup we need to be done here
				noteCellEditor.prototype.destroy = function () {
					// but this example is simple, no cleanup, we could  even leave this method out as it's optional
				};

				// if true, then this editor will appear in a popup 
				noteCellEditor.prototype.isPopup = function () {
					// and we could leave this method out also, false is the default
					return false;
				};

				// ----------------------- note editor -------------------------------------------------------------------------- end
				//numeric editornya AG GRID
				// function to act as a class
				function NumericCellEditor() {
				}

				
				function getCharCodeFromEvent(event) {
					event = event || window.event;
					return (typeof event.which == "undefined") ? event.keyCode : event.which;
				}

				function isCharNumeric(charStr) {
					return !!/\d/.test(charStr);
				}

				function isKeyPressedNumeric(event) {
					var charCode = getCharCodeFromEvent(event);
					var charStr = String.fromCharCode(charCode);
					return isCharNumeric(charStr);
				}

				// gets called once before the renderer is used
				NumericCellEditor.prototype.init = function (params) {
					// create the cell
					var maxL = 9
					if (params.column.colId == 'Qty') {
						maxL = 6
					} else if (params.column.colId == 'HargaBeli') {
						maxL = 9
					} else if (params.column.colId == 'HargaJual') {
						maxL = 9
					} else if (params.column.colId == 'Discount') {
						maxL = 5
					}
					this.eInput = document.createElement('input');
					this.eInput.setAttribute('maxLength',maxL);
					console.log('DATAAA1 =>',params.charPress)
					console.log('DATAAA2 =>',params.value)
					if (isCharNumeric(params.charPress)) {
						this.eInput.value = params.charPress;
					} else {
						if (params.value !== undefined && params.value !== null) {
							this.eInput.value = params.value;
						}
					}

					var that = this;
					this.eInput.addEventListener('keypress', function (event) {
						if (!isKeyPressedNumeric(event)) {
							if (that.eInput.value.includes('.') || (event.key === '.' && (that.eInput.value === '' || that.eInput.value === null || that.eInput.value === undefined))){
								that.eInput.focus();
								event.preventDefault();
							} else {
								that.eInput.focus();
								if (event.preventDefault && event.key !== '.'){
									event.preventDefault();
								} 
							}
						} else if (that.isKeyPressedNavigation(event)){
							event.stopPropagation();
						}
					});

					// only start edit if key pressed is a number, not a letter
					var charPressIsNotANumber = params.charPress && ('1234567890'.indexOf(params.charPress) < 0);
					this.cancelBeforeStart = charPressIsNotANumber;
				};

				NumericCellEditor.prototype.isKeyPressedNavigation = function (event){
					return event.keyCode===39
						|| event.keyCode===37;
				};

				// gets called once when grid ready to insert the element
				NumericCellEditor.prototype.getGui = function () {
					return this.eInput;
				};

				// focus and select can be done after the gui is attached
				NumericCellEditor.prototype.afterGuiAttached = function () {
					this.eInput.focus();
				};

				// returns the new value after editing
				NumericCellEditor.prototype.isCancelBeforeStart = function () {
					return this.cancelBeforeStart;
				};

				// example - will reject the number if it contains the value 007
				// - not very practical, but demonstrates the method.
				NumericCellEditor.prototype.isCancelAfterEnd = function () {
					var value = this.getValue();
					return value.indexOf('007') >= 0;
				};

				// returns the new value after editing
				NumericCellEditor.prototype.getValue = function () {
					return this.eInput.value;
				};

				// any cleanup we need to be done here
				NumericCellEditor.prototype.destroy = function () {
					// but this example is simple, no cleanup, we could  even leave this method out as it's optional
				};

				// if true, then this editor will appear in a popup 
				NumericCellEditor.prototype.isPopup = function () {
					// and we could leave this method out also, false is the default
					return false;
				};
				//numeric editornya AG GRID

		        function currencyFormatter(params) {
		            console.log('params currencyFormatter ===>',params.value);

		            if(params.value == "" || params.value == undefined){
		                return '0';
		            }else{
		                return formatNumber(params.value);
		            }
		        }

		        function formatNumber(number) {
		            return Math.floor(number)
		            .toString()
		            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
		        }
				function formatNumberDecimal(number) {
					return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
				}

		        function gridPartsIsOPB(params){
		            console.log("gridPartsIsOPB => ",params.data.IsOPB,params);
		            if( (params.data.PartsName == "" &&  params.data.PartsCode != "") || (params.data.PartsName != "" &&  params.data.PartsCode == "") || params.data.IsOPB == true ){
		                return true;
		            }else{
		                return false
		            }
		            
		        }

		        var dateFormat = 'dd/MM/yyyy';
		        $scope.DateOptions = {
		            startingDay: 1,
		            format: dateFormat,
		        }

		       
		        function actionRenderer2() {
		            var actionHtml = ' <button ng-disabled="viewMode" skip-enable class="ui icon inverted grey button"\
		                                style="font-size:15px;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:0px 1px 0px 2px"\
		                                ng-click="deleteDataOplDetail(data)">\
		                                <i class="fa fa-trash"></i>\
		                            </button>';
		            return actionHtml;
		        }
		        // ag grid detail
		        

		        $scope.deleteAllDataGridViewWork = function(){
		        	console.log("delete data opl",DataChoosedDelete);
		        	bsAlert.alert({
		        	            title: "Apakah anda yakin untuk menghapus OPL?",
		        	            text: "",
		        	            type: "question",
		        	            showCancelButton: true
		        	        },
		        	        function() {
		        	        	if(DataChoosedDelete.length == 0){
		        	        		bsAlert.warning("Pilih Data Yang Akan Dihapus", "");
		        	        	}else{

		        	        		for(var a = 0; a < DataChoosedDelete.length; a++){
		        	        		    console.log("aye aye", DataChoosedDelete[a]);
		        	        		    for(var c = 0; c < $scope.finalDataOPL.length; c++){
		        	        		        console.log("losal",$scope.finalDataOPL[c]);
		        	        		        if($scope.finalDataOPL[c].flagId == DataChoosedDelete[a].flagId){

		        	        		            if($scope.finalDataOPL[c].EstTaskBPId == null || typeof  $scope.finalDataOPL[c].EstTaskBPId == undefined){
		        	        		                $scope.finalDataOPL.splice(c,1);
		        	        		                console.log('ini langsung splice karna blm ada id nya')
		        	        		            }else{
		        	        		                $scope.finalDataOPL[c].isDeleted = 1;
		        	        		                console.log('ini cuma ganti status is Deleted nyaa')

		        	        		            }
		        	        		        }
		        	        		        
		        	        		        
		        	        		    }
		        	        		}
		        	        		$scope.setDataMaster();
		        	        		$scope.countSummaryOPl();
		        	        	}
		        	        },
		                    function() {

		                    }
		           )
		        	
		        }

		        $scope.closeModalOpl = function(){
					console.log("closeModalOpl | $scope.masterOPL ===>",$scope.masterOPL);
					console.log("closeModalOpl | tmpDataOPL =========>",tmpDataOPL);


					if($scope.finalDataOPL.length > 0){
						for(var i in $scope.finalDataOPL){
							if($scope.finalDataOPL[i].flagId == tmpDataOPL.flagId){
								$scope.finalDataOPL[i] = tmpDataOPL;
							}
						}
					}
					

		            $scope.masterOPL = [];
		            $scope.mOpl = {};
		            $scope.gridDetailOPLApi.setRowData($scope.masterOPL);
		            $scope.setDataMaster(1);
					// angular.element(".ui.modal.ModalOPL").modal("hide");
					angular.element("#modalId").modal("hide");
					// angular.element('#modalId').remove();
				};
				

				// var today = new Date();
        		// today = today.setDate(today.getDate() + 1);
				// var dateFormatOPLnew = 'dd/MM/yyyy';
				// var dateFilterOPLnew = 'date:"dd/MM/yyyy"';
				// $scope.dateOptionsOPLnew = {
				// 	startingDay: 1,
				// 	format: dateFormatOPLnew,
				// 	minDate = today
				// 	//disableWeekend: 1
				// };

		        $scope.saveFinalOpl = function(){
					console.log("ini simpan",$scope.masterOPL);
					$scope.gridDetailOPLApi.stopEditing();
					var filterVendor = $scope.OplVendor.filter(function(item) { 
					   return item.VendorId ==  $scope.mOpl.vendor;  
					});
					console.log("filterVendor",filterVendor);
					console.log("flagedit",flagedit);
					if(flagedit == 1){
						for(var a in $scope.masterOPL){
							console.log("$scope.masterOPL[a].IsSaveFin",$scope.masterOPL[a].IsSaveFin);
							if($scope.masterOPL[a].IsSaveFin == undefined){
				
								if($scope.masterOPL[a].Discount == ""){
									$scope.masterOPL[a].Discount = 0;
								}else{
									$scope.masterOPL[a].Discount = parseFloat($scope.masterOPL[a].Discount);
									// $scope.masterOPL[a].Discount = parseInt($scope.masterOPL[a].Discount);
								}
								$scope.masterOPL[a].VendorId = filterVendor[0].VendorId;
								$scope.masterOPL[a].IsSaveFin = 1;
								$scope.masterOPL[a].isCancel = false;
								// $scope.finalDataOPL.push(
								// 		{
								// 			flagId: Id,
								// 			detail : [$scope.masterOPL[a]],
								// 			OPLNo: 1,
								// 			Vendor :filterVendor[0].Name,
								// 			VendorId : filterVendor[0].VendorId,
								// 			Qty : $scope.masterOPL.length,
								// 			Status : "",
								// 		}
								// 	);
							}
						}
					}else{
						console.log('flagedit 0 ===>',flagedit)
						if($scope.finalDataOPL.length == 0){
							console.log('$scope.finalDataOPL 0 ===>',$scope.finalDataOPL)
							var Id =  Math.floor(Math.random() * 10000) + 1;
							
							var detailTask = []
				
							for(var a in $scope.masterOPL){
								if($scope.masterOPL[a].Discount == ""){
									$scope.masterOPL[a].Discount = 0;
								}else{
									$scope.masterOPL[a].Discount = parseFloat($scope.masterOPL[a].Discount);
									// $scope.masterOPL[a].Discount = parseInt($scope.masterOPL[a].Discount);
								}
								$scope.masterOPL[a].IsSaveFin = 1;
								$scope.masterOPL[a].VendorId = filterVendor[0].VendorId;
								$scope.masterOPL[a].isCancel = false;

				
				
								detailTask.push($scope.masterOPL[a])
							}
							
							
							$scope.finalDataOPL.push(
								{
									flagId: Id,
									detail : detailTask,
									OPLNo: 1,
									Vendor :filterVendor[0].Name,
									VendorId : filterVendor[0].VendorId,
									Qty : $scope.masterOPL.length,
									Status : "",
								}
							);
						}else{
							console.log('$scope.finalDataOPL !0 ===>',$scope.finalDataOPL)
				
							var filterSave = $scope.finalDataOPL.filter(function(item) { 
							   return item.VendorId ==  filterVendor[0].VendorId;  
							});
							console.log("filterSave",filterSave);
							if(filterSave.length == 0){
								var Id =  Math.floor(Math.random() * 10000) + 1;
								var detailTask = []
				
								for(var a in $scope.masterOPL){
									if($scope.masterOPL[a].Discount == ""){
										$scope.masterOPL[a].Discount = 0;
									}else{
										$scope.masterOPL[a].Discount = parseFloat($scope.masterOPL[a].Discount);
										// $scope.masterOPL[a].Discount = parseInt($scope.masterOPL[a].Discount);
									}
									$scope.masterOPL[a].IsSaveFin = 1;
									$scope.masterOPL[a].VendorId = filterVendor[0].VendorId;
									detailTask.push($scope.masterOPL[a])
									$scope.masterOPL[a].isCancel = false;

				
									
								}
								$scope.finalDataOPL.push(
									{
										flagId: Id,
										detail : detailTask,
										OPLNo: 1,
										Vendor :filterVendor[0].Name,
										VendorId : filterVendor[0].VendorId,
										Qty : $scope.masterOPL.length,
										Status : "",
									}
								);
								
							}else{
								bsAlert.warning("Data Vendor Sudah Ada", 'Silakan melakukan edit untuk menambahkan pekerjaan');
							}
						}
					
				
					}
					console.log("$scope.finalDataOPL",$scope.finalDataOPL);
					$scope.masterOPL = [];
					
					$scope.mOpl = {};
					$scope.setDataMaster();
					$scope.countSummaryOPl();

					$scope.sumAllPrice();
					$scope.gridDetailOPLApi.setRowData($scope.masterOPL);
					angular.element(".ui.modal.ModalOPL").modal("hide");  
					sizeToFit();


					// $timeout(function() {
					// 	$scope.reziseColumnAG();
					// }, 1000);


				}

		        $scope.countSummaryOPl = function(){
                   // totalWD +=  Math.round(Math.round($scope.gridWork[i].Summary /1.1 ) * ($scope.gridWork[i].Discount/100));
                   console.log("PPNPerc",$scope.PPNPerc);
		          console.log("count summary opl",$scope.finalDataOPL);
				  var totalWOPL = 0;
		          var totalDiscountedPrice = 0;
		          if($scope.finalDataOPL.length == 0){
		         
		          	$scope.sumWorkOpl = totalWOPL;
		          	$scope.sumWorkOplDiscounted = totalDiscountedPrice;
		          }else{
		         	for(var a in $scope.finalDataOPL){
		         	  if($scope.finalDataOPL[a].OPLTasks == undefined){
		         	    $scope.finalDataOPL[a].OPLTasks = $scope.finalDataOPL[a].detail;
		         	  }
		       //   	  for (var i = 0; i < $scope.finalDataOPL[a].OPLTasks.length; i++) {
		       //   	      if($scope.finalDataOPL[a].isDeleted != 1){
		       //   	          if($scope.finalDataOPL[a].OPLTasks[i].isDeleted != 1){
		       //   	              if ($scope.finalDataOPL[a].OPLTasks[i].Price != undefined) {
									// // console.log('1111111=====XXXXXXXXXXXXXX>>>>>>>>>>', $scope.finalDataOPL[a].OPLTasks[i]);
									// if($scope.finalDataOPL[a].OPLTasks[i].PaymentId == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId  == 30 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId == 32){
									// 	totalWOPL += 0;
									// }
									// else{
									// 	totalWOPL += Math.round($scope.finalDataOPL[a].OPLTasks[i].Price / 1.1) *  $scope.finalDataOPL[a].OPLTasks[i].Qty; 
									// }
		       //   	              }else if($scope.finalDataOPL[a].OPLTasks[i].HargaJual != undefined){	  
									// if($scope.finalDataOPL[a].OPLTasks[i].PaymentId == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId  == 30 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId == 32){
									// 	totalWOPL += 0;
									// }
									// else{
									// 	totalWOPL += Math.round($scope.finalDataOPL[a].OPLTasks[i].HargaJual / 1.1) *  $scope.finalDataOPL[a].OPLTasks[i].Qty; 
									// }
		       //   	              }
		       //   	          }
		       //   	      }
		       //   	  };
		         	  // $scope.sumWorkOpl = totalWOPL;
					   // $scope.sumWorkOpl = Math.round(totalWOPL);
					   // console.log('====>TOTAL WO OPL',$scope.sumWorkOpl);
					   for (var i = 0; i < $scope.finalDataOPL[a].OPLTasks.length; i++) {
					     if($scope.finalDataOPL[a].isDeleted != 1){
					       if($scope.finalDataOPL[a].OPLTasks[i].isDeleted != 1){
					         if ($scope.finalDataOPL[a].OPLTasks[i].Price !== undefined) {
					        //    if ($scope.finalDataOPL[a].OPLTasks[i].PaidById == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaidById == 30){
								if ($scope.finalDataOPL[a].OPLTasks[i].PaymentId != 28 && $scope.finalDataOPL[a].OPLTasks[i].PaymentId != 2458 && $scope.finalDataOPL[a].OPLTasks[i].PaymentId != 32){

					               totalWOPL += 0
					           } else {
					               // totalWOPL += $scope.finalDataOPL[a].OPLTasks[i].Price * parseFloat($scope.finalDataOPL[a].OPLTasks[i].QtyPekerjaan);
					               totalWOPL += ASPricingEngine.calculate({
					                               InputPrice: $scope.finalDataOPL[a].OPLTasks[i].Price,
					                               // Discount: 5,
					                               Qty: parseFloat($scope.finalDataOPL[a].OPLTasks[i].Qty),
					                               Tipe: 102, 
					                               PPNPercentage: $scope.PPNPerc,
					                           });
					           }
					         }else if($scope.finalDataOPL[a].OPLTasks[i].HargaJual != undefined){
					        //    if ($scope.finalDataOPL[a].OPLTasks[i].PaidById == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaidById == 30){
								if ($scope.finalDataOPL[a].OPLTasks[i].PaymentId != 28 && $scope.finalDataOPL[a].OPLTasks[i].PaymentId != 2458 && $scope.finalDataOPL[a].OPLTasks[i].PaymentId != 32){

					               totalWOPL += 0
					           } else {
					               // totalWOPL += $scope.finalDataOPL[a].OPLTasks[i].Price * parseFloat($scope.finalDataOPL[a].OPLTasks[i].QtyPekerjaan);
					               totalWOPL += ASPricingEngine.calculate({
					                               InputPrice: $scope.finalDataOPL[a].OPLTasks[i].HargaJual,
					                               // Discount: 5,
					                               Qty: parseFloat($scope.finalDataOPL[a].OPLTasks[i].Qty),
					                               Tipe: 102, 
					                               PPNPercentage: $scope.PPNPerc,
					                           });
					           }
					         }
					       }
					     }
					       
					   };
					   $scope.sumWorkOpl = totalWOPL;
		         	  	for (var i = 0; i < $scope.finalDataOPL[a].OPLTasks.length; i++) {

		         	  	    if($scope.finalDataOPL[a].isDeleted != 1){
		         	  	        if($scope.finalDataOPL[a].OPLTasks[i].isDeleted != 1){
		         	  	            // $scope.finalDataOPL[a].OPLTasks[i].Qty = $scope.finalDataOPL[a].OPLTasks[i].QtyPekerjaan;
		         	  	            $scope.finalDataOPL[a].OPLTasks[i].Discount = $scope.finalDataOPL[a].OPLTasks[i].Discount ? $scope.finalDataOPL[a].OPLTasks[i].Discount : 0;
		         	  	            
		         	  	            if ($scope.finalDataOPL[a].OPLTasks[i].Price != undefined) {
		         	  	                // if ($scope.finalDataOPL[a].OPLTasks[i].PaidById == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaidById == 30|| $scope.finalDataOPL[a].OPLTasks[i].PaidById == 32){
		         	  	                //     $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = 0
		         	  	                // } else {
		         	  	                    $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = ASPricingEngine.calculate({
		         	  	                                                            InputPrice: $scope.finalDataOPL[a].OPLTasks[i].Price,
		         	  	                                                            Discount: $scope.finalDataOPL[a].OPLTasks[i].Discount,
		         	  	                                                            Qty: $scope.finalDataOPL[a].OPLTasks[i].Qty,
		         	  	                                                            Tipe: 104, 
		         	  	                                                            PPNPercentage: $scope.PPNPerc,
		         	  	                                                        }); 
		         	  	                // }
		         	  	                // $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = (parseFloat($scope.finalDataOPL[a].OPLTasks[i].Price) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) - ((parseFloat($scope.finalDataOPL[a].OPLTasks[i].Price) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) * $scope.finalDataOPL[a].OPLTasks[i].Discount / 100);
		         	  	              
		         	  	            }else if($scope.finalDataOPL[a].OPLTasks[i].HargaJual != undefined){
		         	  	                $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = ASPricingEngine.calculate({
		         	  	                                                        InputPrice: $scope.finalDataOPL[a].OPLTasks[i].HargaJual,
		         	  	                                                        Discount: $scope.finalDataOPL[a].OPLTasks[i].Discount,
		         	  	                                                        Qty: $scope.finalDataOPL[a].OPLTasks[i].Qty,
		         	  	                                                        Tipe: 104, 
		         	  	                                                        PPNPercentage: $scope.PPNPerc,
		         	  	                                                    }); 
		         	  	                // $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = (parseFloat($scope.finalDataOPL[a].OPLTasks[i].HargaJual) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) - ((parseFloat($scope.finalDataOPL[a].OPLTasks[i].HargaJual)  * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) * $scope.finalDataOPL[a].OPLTasks[i].Discount / 100);
		         	  	             
		         	  	            }
		         	  	            if ($scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL !== undefined) {
		         	  	            //   if($scope.finalDataOPL[a].OPLTasks[i].PaymentId == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId  == 30){
										if($scope.finalDataOPL[a].OPLTasks[i].PaymentId != 28 && $scope.finalDataOPL[a].OPLTasks[i].PaymentId  != 2458 && $scope.finalDataOPL[a].OPLTasks[i].PaymentId  != 32){

		         	  	                totalDiscountedPrice += 0;
		         	  	              }else{
		         	  	                totalDiscountedPrice += $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL;
		         	  	                console.log("ga masuk else opl");
		         	  	              }
		         	  	            } else {
		         	  	                totalDiscountedPrice = 0;
		         	  	                console.log("masuk else opl");
		         	  	            }
		         	  	            console.log("$scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL", $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL);   
		         	  	        }
		         	  	    }
		         	  	};
		         	  	console.log("totalDiscountedPrice", totalDiscountedPrice);
		         	  	$scope.sumWorkOplDiscounted = totalDiscountedPrice;
		         	  	// $scope.sumWorkOplDiscounted = Math.round(totalDiscountedPrice / (1.1));
		         	  	console.log('$scope.sumWorkOpl', $scope.sumWorkOpl);
		         	  	console.log("$scope.sumWorkOplDiscounted",$scope.sumWorkOplDiscounted);
					   //console.log('===================================Discount =================', $scope.finalDataOPL[a].OPLTasks[i]);
		       //   	  for (var i = 0; i < $scope.finalDataOPL[a].OPLTasks.length; i++) {

		       //   	      if($scope.finalDataOPL[a].isDeleted != 1){
		       //   	          if($scope.finalDataOPL[a].OPLTasks[i].isDeleted != 1){
		       //   	              $scope.finalDataOPL[a].OPLTasks[i].Discount = $scope.finalDataOPL[a].OPLTasks[i].Discount ? $scope.finalDataOPL[a].OPLTasks[i].Discount : 0;
		         	              
		       //   	              if ($scope.finalDataOPL[a].OPLTasks[i].Price != undefined) {
									// // console.log('Price nya'+i,$scope.finalDataOPL[a].OPLTasks[i].Price);
									
									// $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = (parseFloat($scope.finalDataOPL[a].OPLTasks[i].Price / 1.1) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) - ((parseFloat($scope.finalDataOPL[a].OPLTasks[i].Price / 1.1) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) * $scope.finalDataOPL[a].OPLTasks[i].Discount / 100);
									
									// // $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL=  Math.round(Math.round($scope.finalDataOPL[a].OPLTasks[i].Price/1.1 ) * ($scope.finalDataOPL[a].OPLTasks[i].Discount/100)) *  $scope.finalDataOPL[a].OPLTasks[i].Qty; 

		       //   	                // $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL =  (parseFloat($scope.finalDataOPL[a].OPLTasks[i].Price) * $scope.finalDataOPL[a].OPLTasks[i].Discount / 100);
		       //   	              }else if($scope.finalDataOPL[a].OPLTasks[i].HargaJual != undefined){
		       //   	              	$scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = (parseFloat($scope.finalDataOPL[a].OPLTasks[i].HargaJual / 1.1) * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) - ((parseFloat($scope.finalDataOPL[a].OPLTasks[i].HargaJual / 1.1)  * parseInt($scope.finalDataOPL[a].OPLTasks[i].Qty)) * $scope.finalDataOPL[a].OPLTasks[i].Discount / 100);
		         	              	
									// // console.log('harga jual nya'+i,$scope.finalDataOPL[a].OPLTasks[i].HargaJual); 
									// // $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL=  Math.round(Math.round($scope.finalDataOPL[a].OPLTasks[i].HargaJual/1.1 ) * ($scope.finalDataOPL[a].OPLTasks[i].Discount/100)) *  $scope.finalDataOPL[a].OPLTasks[i].Qty; 
									// // $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL = (parseFloat($scope.finalDataOPL[a].OPLTasks[i].HargaJual) * $scope.finalDataOPL[a].OPLTasks[i].Discount / 100);
								 //   }
								 //  // console.log('Cek data loop', $scope.finalDataOPL[a].OPLTasks[i]);
								   
		       //   	              if ($scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL !== undefined) {
									// 	if($scope.finalDataOPL[a].OPLTasks[i].PaymentId == 2277 || $scope.finalDataOPL[a].OPLTasks[i].PaymentId == 30|| $scope.finalDataOPL[a].OPLTasks[i].PaymentId == 32){
									// 	//	totalDiscountedPrice += 0;
									// 	}
									// 	else
									// 	{
									// 		totalDiscountedPrice += $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL;
									// 		// console.log('nilai diskon ke -- '+i, $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL);
									// 	}									   
		       //   	              } else {
		       //   	                 // totalDiscountedPrice = 0;
		       //   	                  console.log("masuk else opl");
		       //   	              }
		       //   	            //   console.log("$scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL", $scope.finalDataOPL[a].OPLTasks[i].DiscountedPriceOPL);   
		       //   	          }
		       //   	      }
		       //   	  };
		       //   	  console.log("totalDiscountedPrice", totalDiscountedPrice);
		       //   	  $scope.sumWorkOplDiscounted = Math.round(totalDiscountedPrice);
		       //   	  console.log('$scope.sumWorkOpl', $scope.sumWorkOpl);
		       //   	  console.log("$scope.sumWorkOplDiscounted",$scope.sumWorkOplDiscounted);
		         	} 	
		          }
		         
		          
		        }

		        $scope.saveOPLEnd = function(JobId){
		        	console.log("JobId",JobId);

		        	console.log("$scope.finalDataOPL",$scope.finalDataOPL);
		        	if($scope.finalDataOPL == undefined){
		        		console.log("sayangnya data opl undefined")
		        	}else{
		        		if($scope.finalDataOPL.length > 0){

		        			WO.postOPLData($scope.finalDataOPL,JobId,$scope.mData).then(function (res) {
		        				$scope.getdataopl()
		        			});
		        		}
		        	}
		        }

		        $scope.setDataMaster = function(data){
		        	for(var a = 0; a < $scope.finalDataOPL.length; a++){
						$scope.finalDataOPL[a].No = a + 1;
						$scope.finalDataOPL[a].Qty = 0;
		        	    for(var b = 0; b < $scope.finalDataOPL[a].detail.length; b++){
		        	           $scope.finalDataOPL[a].detail[b].No = $scope.finalDataOPL[a].No +"."+ (b+1);
		        	           $scope.finalDataOPL[a].Qty += parseInt($scope.finalDataOPL[a].detail[b].Qty);
		        	    }
		        	}
		        	console.log("data",data);
		        	if(data == 1){
		        		if($scope.finalDataOPL.length > 0){
		        			for(var i in $scope.finalDataOPL){
		        				if($scope.finalDataOPL[i].flagId == tmpDataOPL.flagId){
		        					$scope.finalDataOPL[i] = tmpDataOPL;
		        				}
		        			}
		        		}
		        		console.log("$scope.finalDataOPL",$scope.finalDataOPL);
		        	}
		        	if(data == undefined){
		        		tmpDataOPL = angular.copy($scope.finalDataOPL);
		        	}
		        	$scope.agGridWorkViewApi.setRowData($scope.finalDataOPL);
				}
				
				$scope.allowPatternFilter = function(event) {
					console.log("event allowPatternFilter", event);
					if (event.key == "Escape") {
						$scope.closeModalOpl();
					}
				};

		        $scope.plushOPL = function(){
					


		            var Id =  Math.floor(Math.random() * 10000) + 1;
		            var newData = {
		                "Id"            : Id,
		                "No"            : $scope.masterOPL.length + 1,
		                "TaskNameOpl"   : "",
		                "Payment"       : "",
		                "EstimationDate": "",
		                "Qty"           : "",
		                "HargaJual"     : "",
		                "HargaBeli"     : "",
		                "Discount"      : "",
						"Notes"         : "",
						"isCancel"      : true,
		            };

		            if($scope.masterOPL.length > 0){
		            	
						
						for(var i in $scope.masterOPL){
							if(i == $scope.masterOPL.length -1){
								newData.Discount       = $scope.masterOPL[i].Discount;
								newData.EstimationDate = $scope.masterOPL[i].EstimationDate;
								newData.PaymentId      = $scope.masterOPL[i].PaymentId;
								newData.Payment        = $scope.masterOPL[i].Payment;
								newData.Qty            = $scope.masterOPL[i].Qty;
								
								
							}
						}

		            }
		            
		            console.log("newData",newData);
		            $scope.masterOPL.push(newData);
		            console.log("$scope.masterOPL",$scope.masterOPL);
		            // $scope.GridParts.api.setRowData($scope.GridParts.rowData);
		            var res = $scope.gridDetailOPLApi.updateRowData({add: [newData]});
		            console.log("res",res);
		            res.add.forEach(function(rowNode) {
		                console.log("Added Row Node", rowNode);
		                $scope.gridDetailOPLApi.setFocusedCell(rowNode.rowIndex,"PartCode");
					});
					$scope.disableSimpan = true;
					$scope.dateOptions.minDate = new Date();

// 					2675 -> FPC - Free Parts Claim
// 					2677 -> FLC - Free Labor Claim
// 					2678 -> PLC - Parts Labor Claim
					//----------------------------------------CEK LIST ADA FPC FLC PLC START------------------------------//
					var x_adaFPC = 0
					var x_adaFLC = 0
					var x_adaPLC = 0
					for(var i=0; i<$scope.gridWork.length; i++){
						if ($scope.gridWork[i].JobTypeId == 2675) {
							x_adaFPC++;
						}
						if ($scope.gridWork[i].JobTypeId == 2677) {
							x_adaFLC++;
						}
						if ($scope.gridWork[i].JobTypeId == 2678) {
							x_adaPLC++;
						}
					}
					//----------------------------------------CEK LIST ADA FPC FLC PLC END------------------------------//
					// MHKE8FB3JKK037194 - TCARE
									// JTNGF3DH3N8035733 - GGBSB
					if (x_adaFLC == 0 && x_adaFPC == 0 && x_adaPLC == 0) {
						OPLPaymentData.splice(0,6)
						for(var a in $scope.OPLPaymentData){
							OPLPaymentData.push($scope.OPLPaymentData[a].Name)
						}
					}else{
						if(OPLPaymentData.length == 0){
							for(var a in $scope.OPLPaymentData){
								//Ngecek programName GBSB pembayraan muncul semua, kalau bukan warranty tidak ada
								if ($scope.vinOPL.length > 0) {
									if ($scope.vinOPL[0].ProgramName == "GBSB") {
										console.log('$scope.vinOPL 1', $scope.vinOPL)
										console.log('sini == 0 ===> 2')
										OPLPaymentData.push($scope.OPLPaymentData[a].Name)
										//Jiga data norangkanya tidak ada munculin semua		
									}else if($scope.vinOPL[0].ProgramName != "" && $scope.vinOPL[0].ProgramName != undefined && $scope.vinOPL[0].ProgramName != null && $scope.vinOPL[0].ProgramName != 'GBSB'){
										console.log('$scope.vinOPL', $scope.vinOPL)
										console.log('typeof $scope.vinOPL', typeof $scope.vinOPL)
										console.log('$scope.vinOPL.ProgramName', $scope.vinOPL.ProgramName)
										console.log('sini == 0 ===> 1')
										console.log('vinOPL 123', $scope.vinOPL)
										if ($scope.OPLPaymentData[a].Name !== 'Warranty') {
											OPLPaymentData.push($scope.OPLPaymentData[a].Name)
										}
									}else{
										//kalau tidak ada programName pembyaran muncul semua
										OPLPaymentData.push($scope.OPLPaymentData[a].Name)
									}
								}else{
									OPLPaymentData.push($scope.OPLPaymentData[a].Name)
								}
								
							}
									console.log("STATUS MASUK");
						}else{
							OPLPaymentData.splice(0,6)
							for(var a in $scope.OPLPaymentData){
								//Ngecek programName GBSB pembayraan muncul semua, kalau bukan warranty tidak ada
								if ($scope.vinOPL.length > 0) {
									if ($scope.vinOPL[0].ProgramName == "GBSB") {
										console.log('$scope.vinOPL 1', $scope.vinOPL)
										console.log('sini == 0 ===> 2')
										OPLPaymentData.push($scope.OPLPaymentData[a].Name)
										//Jiga data norangkanya tidak ada munculin semua		
									}else if($scope.vinOPL[0].ProgramName != "" && $scope.vinOPL[0].ProgramName != undefined && $scope.vinOPL[0].ProgramName != null && $scope.vinOPL[0].ProgramName != 'GBSB'){
										console.log('$scope.vinOPL', $scope.vinOPL)
										console.log('typeof $scope.vinOPL', typeof $scope.vinOPL)
										console.log('$scope.vinOPL.ProgramName', $scope.vinOPL.ProgramName)
										console.log('sini == 0 ===> 1')
										console.log('vinOPL 123', $scope.vinOPL)
										if ($scope.OPLPaymentData[a].Name !== 'Warranty') {
											OPLPaymentData.push($scope.OPLPaymentData[a].Name)
										}
									}else{
										//kalau tidak ada programName pembyaran muncul semua
										OPLPaymentData.push($scope.OPLPaymentData[a].Name)
									}
								}else{
									OPLPaymentData.push($scope.OPLPaymentData[a].Name)
								}
							}
	
						}
					}

		        }

		        $scope.deleteDataOplDetail = function(data){
		            console.log("data delete",data);
		            bsAlert.alert({
		                        title: "Apakah anda yakin untuk menghapus Job OPL?",
		                        text: "",
		                        type: "question",
		                        showCancelButton: true
		                    },
		                    function() {
		                    	
		                    	
		                    	console.log('deleteDataOplDetail | masterOPL ===>',$scope.masterOPL);
		                    	console.log('deleteDataOplDetail | lengthOPL ===>',$scope.masterOPL.length);
		                    	
		                    	if($scope.masterOPL.length == 0){
		                    		console.log('deleteDataOplDetail | hapus vendor ====>',$scope.mOpl)
		                    		$scope.mOpl.vendor = null;
		                    		$scope.mOpl.VendorCode = null;                
		                    	}else{
		                    		console.log('deleteDataOplDetail | ga hapus vendor ====>',$scope.mOpl)
		                    	}

		                    	for(var x in $scope.masterOPL){
		                    	    if($scope.masterOPL[x].Id == data.Id){
		                    	        $scope.masterOPL.splice(x,1);
		                    	    } 
		                    	}
		                    	getNoDetail(1);
		                    },
                   			function() {

                   			}
           			)
					

					
		        }


		        function getNoDetail(data){
		            console.log("data flag",data);
		            for(var a in $scope.masterOPL){
		                $scope.masterOPL[a].No = parseInt(a) + 1;
		            }

		            if(data == 1){
		                // data == 1 that from delete
		                $scope.gridDetailOPLApi.setRowData($scope.masterOPL);
		            }

					console.log("get no for detail");
					$scope.disableSimpan = validateSimpan();
		        }

				function validateSimpan(){
					var invalidInput = 0;
					if($scope.masterOPL.length > 0){
						for(var x in $scope.masterOPL){
							if($scope.masterOPL[x].TaskNameOpl == "" || $scope.masterOPL[x].Payment == ""|| $scope.masterOPL[x].EstimationDate == ""|| $scope.masterOPL[x].EstimationDate == null || $scope.masterOPL[x].EstimationDate == undefined || $scope.masterOPL[x].Qty == "" || $scope.masterOPL[x].HargaBeli == "" || $scope.masterOPL[x].HargaJual == ""){
								invalidInput++;
								console.log('JALAN =>',x, $scope.masterOPL[x])
							}
						}
						if(invalidInput >0){
							return true
						}else{
							return false
						}
					}else{
						return true
					}
				}

		        function customDate(param){
		            console.log('param customDate ===>',param.value);
		            console.log('param customDate ===>',typeof param.value);


		            if(param.value == null){
		                return '-';
		            }else{
		                if(param.value !== ""){
		                    if(typeof param.value == 'object'){
		                        var date = new Date(param.value);
		                        console.log('date parsin ===>',date);
		                        var thn = date.getFullYear();
		                        var bln = ('0' + (date.getMonth()+1)).slice(-2);
		                        var tgl = ('0' + date.getDate()).slice(-2);
		                        var result = '' + tgl +'-'+  bln +'-'+ thn +'';
		                        console.log('thn --->',thn);
		                        console.log('bln --->',bln);
		                        console.log('tgl --->',tgl);
		                        console.log('result parsing --->',result)
		                        return result
		                    }else if (typeof param.value == 'string'){
		                        console.log('date normal ===>',date);
		                        var date = param.value.split('-');
		                        var thn = date[0];
		                        var bln = date[1];
		                        var tgl = date[2];
		                        var result = '' + tgl +'-'+  bln +'-'+ thn +'';
		                        console.log('thn --->',thn);
		                        console.log('bln --->',bln);
		                        console.log('tgl --->',tgl);
		                        console.log('result normal--->',result)
		                        return result
		                    }else{
		                        return  '-';
		                    }
		                }else{
		                    return '-';
		                }
		            }

		            
				}
				
				$scope.alertOPL = function(text, mode) {
					bsAlert.alert({
							title: text,
							text: "",
							type: mode,
							showCancelButton: false
						},
						function() {
						   
						},
						function() {
			
						}
					)
				};



		        var dataforGrid = {};
		        $scope.onAfterCellEditAg = function(idx,field,val,node) {
		            console.log("onAfterCellEditAg | idx ==========>",idx);
		            console.log("onAfterCellEditAg | field ========>",field);
		            console.log("onAfterCellEditAg | val ==========>",val);
		            console.log("onAfterCellEditAg | node =========>",node);
		            console.log("onAfterCellEditAg | node.data ====>",node.data);
					console.log("onAfterCellEditAg | masterOPL ====>",$scope.masterOPL);

		            if(field == 'TaskNameOpl'){
						console.log("onAfterCellEditAg | oplDataChoosed ===>",oplDataChoosed);
            			console.log("onAfterCellEditAg | $scope.mOpl ======>",$scope.mOpl);
						var flagCount = 0;

						$scope.gridDetailOPLApi.forEachNode( function(node,index){
						  console.log("node to Update oi oi=>",node,node.data,index);
						  if(oplDataChoosed != undefined){
							if(oplDataChoosed.OPLWorkName != undefined){
							  
							  // if(node.data.TaskNameOpl == oplDataChoosed.OPLWorkName){
							  if(node.data.oplId == oplDataChoosed.Id){
								  flagCount++;
							  }
							}
						  }else{
							flagCount = 0;
						  }
						});
						console.log("onAfterCellEditAg | flagCount ===>",flagCount);
						var datakosong = _.isEmpty(oplDataChoosed);
						console.log("datakosong",datakosong);
						if(datakosong == true){
						   $scope.alertOPL('Pekerjaan OPL tidak ditemukan','warning');
						   oplDataChoosed = {};
						}else{
							dataforGrid = node.data;
			                dataforGrid.oplId = oplDataChoosed.Id;
			                // dataforGrid.TaskNameOpl = oplDataChoosed.OPLWorkName;
			                dataforGrid.TaskNameOpl = oplDataChoosed.OPLWorkName +" "+oplDataChoosed.isExternalDesc;
			                dataforGrid.HargaJual = oplDataChoosed.Price;
							dataforGrid.HargaBeli = oplDataChoosed.OPLPurchasePrice;
							console.log('MAAAAA', dataforGrid.TaskNameOpl)
							if(dataforGrid.TaskNameOpl == undefined){
								dataforGrid.Qty = "";
								console.log('MASUKKKKK')
							}else{
								dataforGrid.Qty = 1;
								if($scope.mOpl.vendor == null){
	                                $scope.mOpl.vendor = oplDataChoosed.VendorId;
	                                $scope.chooseVendor();
	                            }
							}
			                console.log("dataforGrid",dataforGrid);
							if((oplDataChoosed.VendorId != $scope.mOpl.vendor) && $scope.mOpl.vendor != null){
								if(oplDataChoosed.OPLWorkName != undefined){
									$scope.alertOPL('Pekerjaan OPL  tidak sesuai dengan vendor yang dipilih','warning');
									dataforGrid.Qty         = "";
									dataforGrid.TaskNameOpl = "";
									dataforGrid.HargaJual   = "";
									dataforGrid.HargaBeli   = "";
									node.setData(dataforGrid);
								}
							}else{
								// if(flagCount > 0){

								//   $scope.alertOPL('Pekerjaan OPL tidak boleh sama silakan ubah qty','warning');
								//   dataforGrid.Qty         = "";
								//   dataforGrid.TaskNameOpl = "";
								//   dataforGrid.HargaJual   = "";
								//   dataforGrid.HargaBeli   = "";
								// }else{
								  node.setData(dataforGrid);
								// }
							}
							oplDataChoosed = {};
						}
						
		            }

		            if(field == 'EstimationDate'){
		                console.log("$scope.mOpl.DateStart",$scope.mOpl.DateStart);
		                dataforGrid = node.data;
		                dataforGrid.EstimationDate = $scope.mOpl.DateStart;
		                console.log("dataforGrid",dataforGrid);
		                node.setData(dataforGrid);
		            }

		            if(field == 'Notes'){
		            	dataforGrid = node.data;
		            	console.log("dataforGrid",dataforGrid);
		            	node.setData(dataforGrid);
		            }

		            if(field == 'Discount'){

		            	if(node.data.Discount > 100){
		            	    bsNotify.show({
		            	        size: 'big',
		            	        type: 'danger',
		            	        title: "Diskon Tidak Boleh Melebihi 100",
		            	    });
		            	}else{

		            		dataforGrid = node.data;
		            		console.log("dataforGrid",dataforGrid);
		            		node.setData(dataforGrid);	
		            	}
		            }

		            if(field == 'Payment'){
		            	for(var a in $scope.OPLPaymentData){
		            		if($scope.OPLPaymentData[a].Name == node.data.Payment){
		            			node.data.PaymentId = $scope.OPLPaymentData[a].MasterId;
		            		}
		            	}
						if (node.data.PaymentId == 30) {
							node.data.Discount = '0'
						}
		            	dataforGrid = node.data;
		            	console.log("dataforGrid",dataforGrid);
		            }

		            if(field == 'HargaBeli'){
		            	// if(node.data.HargaBeli == 0){

		            	// 	bsNotify.show({
		            	// 	    size: 'big',
		            	// 	    type: 'danger',
		            	// 	    title: "Harga Beli Tidak Boleh 0",
		            	// 	}); 
		            	// }else{

		            		dataforGrid = node.data;
		            		console.log("dataforGrid",dataforGrid);
		            		node.setData(dataforGrid);
		            	// }
		            }



		            if(field == 'HargaJual'){
		            	// if(node.data.HargaJual == 0){

		            	// 	bsNotify.show({
		            	// 	    size: 'big',
		            	// 	    type: 'danger',
		            	// 	    title: "Harga Jual Tidak Boleh 0",
		            	// 	}); 
		            	// }else{

		            		dataforGrid = node.data;
		            		console.log("dataforGrid",dataforGrid);
		            		node.setData(dataforGrid);
		            	// }
		            }


					$scope.disableSimpan = validateSimpan();

					
		        }

		        $scope.getOplSrc = function (key) {
					console.log('getOplSrc | data CRM ========>', $scope.mDataCrm);
					console.log('getOplSrc | mOpl.vendor ====>', $scope.mOpl.vendor);
					var modelname = '';
		            if ($scope.mDataCrm.Vehicle.isNonTAM == 0){
		                modelname = $scope.mDataCrm.Vehicle.Model.VehicleModelName
		            } else {
		                modelname = $scope.mDataCrm.Vehicle.ModelName
		            }
		            var vendorid = null;
		            console.log(" $scope.mOpl.vendor", $scope.mOpl.vendor);
		            if($scope.mOpl.vendor == null || $scope.mOpl.vendor == undefined || $scope.mOpl.vendor == ''){
		            	vendorid = 0;
						console.log('getOplSrc | ini sebenernya belom ada vendor Id nya | $scope.mOpl.vendor ===>',$scope.mOpl.vendor)
		            }else{
						vendorid = 0;
						// vendorid = $scope.mOpl.vendor;
						console.log('getOplSrc | ini sebenernya udah ada vendor Id nya | $scope.mOpl.vendor ===>',$scope.mOpl.vendor)
		            }
		            console.log("vendorid",vendorid);
		            var ress = WO.getNewOplList(key, modelname, vendorid).then(function (resTask) {
		                var data = resTask.data.Result;
		                console.log("resTask resTask", data);
		                _.map(data, function (a) {
		                    // _.map(a.value, function(b) {
		                    if (a.isExternal == 1) {
		                        a.isExternalDesc = ' (External)';
		                        // a.VendorName = a.VendorName + a.isExternalDesc;
		                    } else {
		                        a.isExternalDesc = ' (Internal)';
		                        // a.VendorName = a.VendorName + a.isExternalDesc;
		                    };
		                    // });
		                });
		                console.log("data data", data);
		                return data;
		            });
		            console.log("ress", ress);

		            return ress;
		        };

		        $scope.onGotResult = function (data) {
		            console.log("onGotResult=>", data);
		        }

		        $scope.onNoResultOplNew = function () {
		            var row = {};
		            // $scope.listApiOpl.addDetail(false, row);
		            // $scope.listApiOpl.clearDetail();
		        };
		        var oplDataChoosed
		        $scope.selectOPl = function($item, $model, $label) {
		            console.log("onSelectPartCode item=>", $item);
		            console.log("onSelectPartCode model=>", $model);
		            console.log("onSelectPartCode label=>", $label);
		            oplDataChoosed = $item;
		            console.log("oplDataChoosed",oplDataChoosed);
				};
				// using ag-grid change CR4 Phase2
				// CR4
				$scope.ValidateModelCarChange = function(){
					for(var c = 0; c < $scope.finalDataOPL.length; c++){
						console.log("losal",$scope.finalDataOPL[c]);			
							$scope.finalDataOPL = [];
					}
					$scope.setDataMaster();
					$scope.countSummaryOPl();
				};
				// CR4
};