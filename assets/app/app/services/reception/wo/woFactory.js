    angular.module('app')
        .factory('WO', function($http, $q, $filter, CurrentUser) {
            var currUser = CurrentUser.user();
            return {
                getData: function() {
                    var res = $http.get('/api/as/Towings');
                    //console.log('res=>',res);
                    return res;
                },
                getDataTechnician: function(item) {
                    var tmpItemJoin = item.join(',')
                    var res = $http.get('/api/as/GetTechnician/' + tmpItemJoin);
                    console.log('res=>', item);
                    return res;
                },
                getTransactionCode: function() {
                    // var catId = 2028;
                    // var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                    // console.log('resnya getTransactionCode=>', res);
                    // return res;

                    return $http.get('/api/as/InvoiceTypes');
                },
                getCheckStatusParts: function (JobId, data) {
                    console.log('jobid ====>', JobId);
                    console.log('ihhh datany', data)
                    return $http.post('/api/as/JobParts/CekStatusParts/' + JobId, data);
                },

                changeFormatDate: function(item) {
                    var tmpAppointmentDate = item;
                    console.log("changeFormatDate item", item);
                    var finalDate
                    if (item == null || item == undefined) {
                        finalDate = null;
                    } else {
                        tmpAppointmentDate = new Date(tmpAppointmentDate);

                        if (tmpAppointmentDate !== null && tmpAppointmentDate !== undefined) {
                            var yyyy = tmpAppointmentDate.getFullYear().toString();
                            var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
                            var dd = tmpAppointmentDate.getDate().toString();
                            finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                        } else {
                            finalDate = '';
                        }
                    }

                    console.log("changeFormatDate finalDate", finalDate);
                    return finalDate;
                },
                OutletBPCenter: function(id) {
                    return $http.get('/api/as/OutletBPCenter?OutletBPCenter=' + id)
                },
                getDataQueueList: function() {
                    var res = $http.get('/api/as/queue/QueueList/1');
                    return res
                },
                GetCounter: function() {
                    var res = $http.get('/api/as/QueueCounters');
                    return res
                },
                autoCall: function(item) {
                    return $http.put('/api/as/queue/autoCall/' + item);
                },
                GetOplList: function(key, model) {
                    var res = $http.get('/api/as/OPLFilter?oplname=' + key + '&modelName=' + model);
                    return res
                },
                skipCall: function(itemCounter, item) {
                    return $http.put('/api/as/queue/skipCall/' + itemCounter.CounterId + '/' + item.LicensePlate);
                },
                removeCall: function(item) {
                    return $http.put('/api/as/queue/remove/' + item.LicensePlate);
                },
                reCall: function(itemCounter, item) {
                    return $http.put('/api/as/queue/recallCall/' + item.Id + '/' + item.LicensePlate);
                },
                manualCall: function(item, itemCounter) {
                    // Counter Id = 4,its default
                    console.log("manualCall.data.Result counter", item, itemCounter);
                    return $http.put('/api/as/queue/manualCall/' + item.Id + '/' + item.LicensePlate);
                },

                CheckIsCall: function(IdQueue) {
                    // api/as/queue/CheckIsCall/{IdQueue}
                    return $http.get('/api/as/queue/CheckIsCall/' + IdQueue);
                },

                manualCallQueue: function(item, itemCounter) {
                    // Counter Id = 4,its default
                    console.log("manualCallQueue.data.Result counter", item, itemCounter);
                    return $http.put('/api/as/queue/manualCallQueue/' + item.Id + '/' + item.LicensePlate + '/' + itemCounter.CounterId);
                },
                manualCallSAAssignment: function(item, itemCounter) {
                    // Counter Id = 4,its default
                    console.log("manualCallSAAssignment.data.Result counter", item, itemCounter);
                    return $http.put('/api/as/queue/manualCall/0/' + item.LicensePlate);
                },
                checkGI: function(item) {
                    var res = $http.get('/api/as/jobs/cekGI/' + item.JobId);
                    return res
                },
                getDataVehicle: function(key) {
                    var res = $http.get('/api/crm/GetDataInfo/' + key);
                    return res;
                },
                updateJobSuggest: function(Vin, item, isTam) {
                    if (isTam == 1){
                        console.log('ini tam panggil crm dan CT')
                        return $q.resolve(
                            $http.put('/api/crm/PutVehicleListJobSuggest/', [{
                                VIN: Vin,
                                JobSuggest: item
                            }]),
                            $http.put('/api/ct/PutVehicleListJobSuggest/', [{
                                VIN: Vin,
                                JobSuggest: item
                            }])
                        );
                    } else {
                        console.log('ini Non tam panggil crm aja')
                        return $http.put('/api/crm/PutVehicleListJobSuggest/', [{
                                VIN: Vin,
                                JobSuggest: item
                            }])
                    }
                    
                },
                // GetCAddressCategory: function() {
                //   var res=$http.get('/api/crm/GetCAddressCategory/');

                //   // console.log('hasil=>',res);
                //   //res.data.Result = null;
                //   return res;
                // },
                // GetLocation: function() {
                //   var res=$http.get('/api/crm/GetLocation/');

                //   // console.log('hasil=>',res);
                //   //res.data.Result = null;
                //   return res;
                // },
                create: function(mData) {
                    return $http.post('/api/as/Towings', [{
                        PoliceNumber: mData.PoliceNumber,
                        VehicleModelId: mData.VehicleModelId,
                        Color: mData.Color,
                        EntryDate: mData.EntryDate,
                        EntryTime: mData.EntryTime,
                        OwnerName: mData.OwnerName,
                        Phone: mData.Phone,
                        Detail: mData.Detail
                    }]);
                },
                update: function(mData) {
                    return $http.put('/api/as/Towings', [{
                        TowingId: mData.TowingId,
                        PoliceNumber: mData.PoliceNumber,
                        VehicleModelId: mData.VehicleModelId,
                        Color: mData.Color,
                        EntryDate: mData.EntryDate,
                        EntryTime: mData.EntryTime,
                        OwnerName: mData.OwnerName,
                        Phone: mData.Phone,
                        Detail: mData.Detail
                    }]);
                },

                WOTimeStamp: function(data) {
                    // api/as/jobs/WOTimeStamp
                    return $http.put('/api/as/jobs/WOTimeStamp', [{
                        JobId: data.JobId,
                        StartTime: data.TimeStampStart,
                        FinishTime: data.TimeStampFinish,
                        Type: data.Type   // 1. teco, 2. call cust
                    }]);
                },

                getDataQueue: function() {
                    var res = $http.get('/api/as/Towings');
                    //console.log('res=>',res);
                    return res;
                },
                createQueue: function(mData) {
                    return $http.post('/api/as/Towings', [{
                        PoliceNumber: mData.PoliceNumber,
                        VehicleModelId: mData.VehicleModelId,
                        Color: mData.Color,
                        EntryDate: mData.EntryDate,
                        EntryTime: mData.EntryTime,
                        OwnerName: mData.OwnerName,
                        Phone: mData.Phone,
                        Detail: mData.Detail
                    }]);
                },
                updateQueue: function(mData) {
                    return $http.put('/api/as/Towings', [{
                        TowingId: mData.TowingId,
                        PoliceNumber: mData.PoliceNumber,
                        VehicleModelId: mData.VehicleModelId,
                        Color: mData.Color,
                        EntryDate: mData.EntryDate,
                        EntryTime: mData.EntryTime,
                        OwnerName: mData.OwnerName,
                        Phone: mData.Phone,
                        Detail: mData.Detail
                    }]);
                },
                getDataWOSAList: function(isGr) {
                    // if(isGr == 0){
                    //     return $http.get('/api/as/jobs/WoListAktif/' + isGr + '?SAId=1');
                    // }
                    // return $http.get('/api/as/jobs/WoListAktif/' + isGr + '?SAId=1');
                    return $http.get('/api/as/jobs/WoListAktifSP/' + isGr + '?SAId=1');
                },
                getDataJobInspection: function(item) {
                    return $http.get('/api/as/JobInspections/job/' + item);
                },
                getWobyJobId: function(filter) {
                    var res = $http.get('/api/as/Jobs/' + filter);
                    return res;
                },
                updateTeco: function(item) {
                    console.log("itemnya", item);
                    return $http.put('/api/as/Jobs/TecoStart/0', [
                        item
                    ]);
                },
                updateTecoOnlyWarranty: function(item) {
                    console.log("itemnya", item);
                    return $http.put('/api/as/Jobs/TecoStart/1', [
                        item
                    ]);
                },
                CekDP: function(JobId) {
                    return $http.get('/api/as/TECOCheckDP/'+ JobId);
                },
                saveServiceExplanations: function(item) {
                    console.log("itemnya", item);
                    return $http.post('/api/as/ServiceExplanations', [{
                        ServiceExplanation: {
                            ServiceExplainId: 0,
                            JobId: item.JobId,
                            OutsideCleaness: item.OutsideCleaness,
                            InsideCleaness: item.InsideCleaness,
                            CompletenessVehicle: item.CompletenessVehicle,
                            UsedPart: item.UsedPart,
                            DriverCarpetPosition: item.DriverCarpetPosition,
                            IsJobResultExplainOk: item.IsJobResultExplainOk,
                            JobResultExplainReason: item.JobResultExplainReason,
                            IsSparePartExplainOk: item.IsSparePartExplainOk,
                            SparePartExplainReason: item.SparePartExplainReason,
                            IsJobSuggestExplainOk: item.IsJobSuggestExplainOk,
                            JobSuggestExplainReason: item.JobSuggestExplainReason,
                            IsConfirmVehicleCheckOk: item.IsConfirmVehicleCheckOk,
                            IsConfirmPriceOk: item.IsConfirmPriceOk,
                            ConfirmPriceReason: item.ConfirmPriceReason,
                            ConfirmVehicleCheckReason: item.ConfirmVehicleCheckReason
                        },
                        FollowUpDate: item.FollowUpDate,
                        PhoneNo: item.PhoneNo,
                        PhoneType: "Handphone",
                        Time: item.Time
                    }]);
                },

                SimpanWO: function(mData, mDataCrm, mDataDM, JobRequest, JobComplaint, oplData, JobList, JobId) {
                    console.log('mdata simpanwo >>>', mData, mDataCrm, mDataDM, JobRequest, JobComplaint, oplData, JobList);
                    var a = new Date();
                    var yearFirst = a.getFullYear();
                    var monthFirst = a.getMonth() + 1;
                    var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                    var dayFirst = a.getDate();
                    var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                    var hour = a.getHours();
                    var hours = hour < 10 ? '0' + hour : hour;
                    var minutes = a.getMinutes();
                    var minutess = minutes < 10 ? '0' + minutes : minutes;
                    var seconds = a.getSeconds();
                    var secondss = seconds < 10 ? '0' + seconds : seconds;
                    var WoCreatedDateX = yearFirst + '-' + monthFirsts + '-' + dayFirsts + ' ' + hours + ':' + minutess + ':' + secondss;

                    // =========== start buat fixedDelivery nih ===============

                    var FD1 = new Date(mData.EstimateDate);
                    var FD2 = new Date(mData.AdjusmentDate);

                    var yearFD1 = FD1.getFullYear();
                    var monthFD1 = FD1.getMonth() + 1;
                    monthFD1 = monthFD1 < 10 ? '0' + monthFD1 : monthFD1;
                    var dayFD1 = FD1.getDate();
                    dayFD1 = dayFD1 < 10 ? '0' + dayFD1 : dayFD1;
                    var hourFD1 = FD1.getHours();
                    hourFD1 = hourFD1 < 10 ? '0' + hourFD1 : hourFD1;
                    var minuteFD1 = FD1.getMinutes();
                    minuteFD1 = minuteFD1 < 10 ? '0' + minuteFD1 : minuteFD1;

                    var yearFD2 = FD2.getFullYear();
                    var monthFD2 = FD2.getMonth() + 1;
                    monthFD2 = monthFD2 < 10 ? '0' + monthFD2 : monthFD2;
                    var dayFD2 = FD2.getDate();
                    dayFD2 = dayFD2 < 10 ? '0' + dayFD2 : dayFD2;
                    var hourFD2 = FD2.getHours();
                    hourFD2 = hourFD2 < 10 ? '0' + hourFD2 : hourFD2;
                    var minuteFD2 = FD2.getMinutes();
                    minuteFD2 = minuteFD2 < 10 ? '0' + minuteFD2 : minuteFD2;

                    var FD1Final = yearFD1 + '-' + monthFD1 + '-' + dayFD1 + ' ' + hourFD1 + ':' + minuteFD1
                    var FD2Final = yearFD2 + '-' + monthFD2 + '-' + dayFD2 + ' ' + hourFD2 + ':' + minuteFD2
                        // =========== end buat fixedDelivery nih ===============


                    JobList = this.isiGuidLogTaskPartOPL(JobList,mData.GUIDWOGR,1)

                    return $http.put('/api/as/Jobs/updateDetil/2', [{
                        isCash: 1,
                        ToyotaIdRequest: mDataCrm.Customer.ToyotaIDFlag == null ? 0 : mDataCrm.Customer.ToyotaIDFlag,
                        JobId: JobId,
                        JobTask: JobList,
                        JobComplaint: JobComplaint,
                        JobRequest: JobRequest,
                        JobWAC: [],
                        JobNo: mData.JobNo,
                        OutletId: currUser.OrgId,
                        CalId: mData.CalId,
                        StallId: mData.StallId,
                        JobListSplitChip: mData.JobListSplitChip,

                        FinalDP: mData.FinalDP,
                        DPPaidAmount: mData.DPPaidAmount,
                        ORPaidAmount: mData.ORPaidAmount,
                        DPRawatJalan: mData.DPRawatJalan,
                        IsPaidRawatJalan: mData.IsPaidRawatJalan,

                        // Stall: mData.Stall,
                        VehicleId: mDataCrm.Vehicle.VehicleId,
                        PlanStart: mData.PlanStart,
                        PlanFinish: mData.PlanFinish,
                        CustRequest: mData.CustRequest,
                        CustComplaint: mData.CustComplaint,
                        TechnicianAction: mData.TechnicianAction1,
                        JobSuggest: mData.JobSuggest1,
                        T1: mData.T1,
                        T2: mData.T2,
                        RSA: mData.RSA,
                        AdditionalTime: mData.AdditionalTime,
                        JobType: mData.JobType,
                        Status: mData.Status,
                        AppointmentDate: $filter('date')(mData.AppointmentDate, 'yyyy-MM-dd'),
                        AppointmentTime: mData.AppointmentTime,
                        AppointmentNo: mData.AppointmentNo,
                        isAppointment: mData.isAppointment,
                        AppointmentVia: mData.AppointmentVia,
                        isGr: 1,
                        Km: mData.KmNormal,
                        // isCash: mData.isCash,
                        isSpk: mData.isSpk,
                        SpkNo: mData.SpkNo ? mData.SpkNo : '-',
                        InsuranceName: mData.InsuranceName,
                        isWOBase: mData.isWOBase,
                        WoCategoryId: mData.WoCategoryId,
                        WoNo: mData.WoNo,
                        IsEstimation: mData.IsEstimation,
                        EstimationDate: mData.EstimateDeliveryTime,
                        EstimationNo: mData.EstimationNo,
                        JobDate: mData.JobDate,
                        // PermissionPartChange: mData.PermissionPartChange,
                        PaymentMethod: mData.PaymentMethod,
                        // DecisionMaker: mDataDM.PK.Name ? mDataDM.PK.Name : null,
                        // PhoneDecisionMaker1: mDataDM.PK.HP1 ? mDataDM.PK.HP1 : null,
                        // PhoneDecisionMaker2: mDataDM.PK.HP2 ? mDataDM.PK.HP2 : null,
                        DecisionMaker: mDataDM.PK.Name,
                        PhoneDecisionMaker1: mDataDM.PK.HP1,
                        PhoneDecisionMaker2: mDataDM.PK.HP2,
                        ContactPerson: mDataDM.CP.Name ? mDataDM.CP.Name : null,
                        PhoneContactPerson1: mDataDM.CP.HP1 ? mDataDM.CP.HP1 : null,
                        PhoneContactPerson2: mDataDM.CP.HP2 ? mDataDM.CP.HP2 : null,
                        // UsedPart: mData.flag.parts ? mData.flag.parts : null,
                        // IsWaiting: mData.flag.wait ? mData.flag.wait : null,
                        // IsWash: mData.flag.wash ? mData.flag.wash : null,
                        // PermissionPartChange: mData.flag.cParts ? mData.flag.cParts : null,
                        UsedPart: mData.flag.parts,
                        IsWaiting: mData.flag.wait,
                        IsWash: mData.flag.wash,
                        PermissionPartChange: mData.flag.cParts,
                        // FixedDeliveryTime1: new Date(mData.EstimateDate),
                        // FixedDeliveryTime2: new Date(mData.AdjusmentDate),
                        FixedDeliveryTime1: FD1Final,
                        FixedDeliveryTime2: FD2Final,
                        FixedDeliveryTime: mData.FixedDeliveryTime,
                        invoiceType: mData.CodeTransaction,
                        JobTaskOpl: oplData,
                        ModelType: mData.VehicleModelName,
                        PoliceNumber: mDataCrm.Vehicle.LicensePlate,
                        isFoOneHour: mData.isFoOneHour,
                        isFoOneDay: mData.isFoOneDay,
                        HandOverDate: mData.HandOverDate,
                        KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                        Revision: mData.Revision,
                        NewAppointmentRel: mData.NewAppointmentRel,
                        RescheduleReason: mData.RescheduleReason,
                        JobCatgForBPid: mData.JobCatgForBPid,
                        JobCatgForBP: mData.JobCatgForBP,
                        ActiveChip: mData.ActiveChip,
                        IsStopPage: mData.IsStopPage,
                        Suggestion: mData.Suggestion,
                        SuggestionCatg: mData.SuggestionCatg,
                        SuggestionDate: mData.SuggestionDate,
                        PlanDateStart: $filter('date')(mData.PlanDateStart, 'yyyy-MM-dd'),
                        PlanDateFinish: $filter('date')(mData.PlanDateFinish, 'yyyy-MM-dd'),
                        closeWoId: mData.closeWoId,
                        CloseWODate: mData.CloseWODate,
                        MaterialRequestNo: mData.MaterialRequestNo,
                        MaterialRequestStatusId: mData.MaterialRequestStatusId,
                        WoCreatedDate: WoCreatedDateX,
                        GateInPushTime: $filter('date')(mData.GateInPushTime, 'yyyy-MM-dd HH:mm:ss'),
                        GateInUserId: mData.GateInUserId,
                        CarWashTime: mData.CarWashTime,
                        CarWashUserId: mData.CarWashUserId,
                        TecoTime: mData.TecoTime,
                        TecoUserId: mData.TecoUserId,
                        BillingCreatedTime: mData.BillingCreatedTime,
                        BillingUserId: mData.BillingUserId,
                        CustCallTime: mData.CustCallTime,
                        CustCallUserId: mData.CustCallUserId,
                        ServiceExplainCallTime: mData.ServiceExplainCallTime,
                        ServiceExplainUserId: mData.ServiceExplainUserId,
                        AdditionalTimeAmount: mData.AdditionalTimeAmount,
                        IsAllPulledBilling: mData.IsAllPulledBilling,
                        IsInStopPageGr: mData.IsInStopPageGr,
                        StopPageTimeGr: mData.StopPageTimeGr,
                        StopPageUserIdGr: mData.StopPageUserIdGr,
                        VendorCode: mData.VendorCode,
                        ModelCode: mData.ModelCode,
                        DealerCode: mData.DealerCode,
                        Email: mData.Email,
                        CancelDate: mData.CancelDate,
                        CancelReason: mData.CancelReason,
                        CancelUserId: mData.CancelUserId,
                        isWoFromBpSatelit: mData.isWoFromBpSatelit,
                        BpSatelitOutletId: mData.BpSatelitOutletId,
                        BpSatelitSAUserId: mData.BpSatelitSAUserId,
                        isReqOutPatient: mData.isReqOutPatient,
                        OutPatientUserIdRequest: mData.OutPatientUserIdRequest,
                        OutPatientReqDate: mData.OutPatientReqDate,
                        isOutPatient: mData.isOutPatient,
                        OutPatientUserIdApproved: mData.OutPatientUserIdApproved,
                        OutPatientApproveDate: mData.OutPatientApproveDate,
                        OutPatientApprovedNo: mData.OutPatientApprovedNo,
                        OutPatientUserIdReject: mData.OutPatientUserIdReject,
                        OutPatientRejectDate: mData.OutPatientRejectDate,
                        OutPatientFinishDate: mData.OutPatientFinishDate,
                        SKB: mData.SKB,
                        UserIdSa: currUser.UserId,
                        UserSa: mData.UserSa,
                        MProfileForemanId: mData.MProfileForemanId,
                        MProfileForeman: mData.MProfileForeman,
                        UserIdApp: mData.UserIdApp,
                        UserApp: mData.UserApp,
                        AppointmentCreateDate: mData.AppointmentCreateDate,
                        GroupBPId: mData.GroupBPId,
                        groupBP: mData.groupBP,
                        ORAmount: mData.ORAmount,
                        InsuranceId: mData.InsuranceId,
                        Insurance: mData.Insurance,
                        VehicleTypeId: mDataCrm.Vehicle.isNonTAM == 0 ? (mData.VehicleTypeId == null ? mDataCrm.Vehicle.Type.VehicleTypeId : mData.VehicleTypeId) : null,
                        UserIdWORelease: currUser.UserId,
                        isDraftWo: mData.isDraftWo,
                        AssemblyYearForEstimation: mData.AssemblyYearForEstimation,
                        SendToTowasDate: mData.SendToTowasDate,
                        ReceiveFromTowasDate: mData.ReceiveFromTowasDate,
                        BillingInsuranceType: mData.BillingInsuranceType,
                        statusTowas: mData.statusTowas,
                        responseStatusFromTowas: mData.responseStatusFromTowas,
                        towasConfirmedBySA: mData.towasConfirmedBySA,
                        towasConfirmedUserIdSA: mData.towasConfirmedUserIdSA,
                        WarantyNoFromTowas: mData.WarantyNoFromTowas,
                        AcceptableAmount: mData.AcceptableAmount,
                        ErrCode: mData.ErrCode,
                        TechnicianNotes: mData.TechnicianNotes,
                        ClaimAmount: mData.ClaimAmount,
                        ApprovalRoleId: mData.ApprovalRoleId,
                        ApprovalUserId: mData.ApprovalUserId,
                        ApprovalBy: mData.ApprovalBy,
                        ApprovalTime: mData.ApprovalTime,
                        ApprovalStatus: mData.ApprovalStatus,
                        AddressForEstimation: mData.AddressForEstimation,
                        VehicleColor: mData.VehicleColor,
                        StatusPreDiagnose: mData.StatusPreDiagnose,
                        QueueId: mData.QueueId,
                        IsCustomerRequest : mData.IsCustomerRequest != 1 ? 0 : 1,
                        AppointmentCreateDate: (mData.AppointmentCreateDate == null || mData.AppointmentCreateDate == undefined) ? null : $filter('date')(mData.AppointmentCreateDate, 'yyyy-MM-dd HH:mm:ss'),
                        Guid_Log: mData.GUIDWOGR,

                    }]);
                    //  [{
                    //     JobId: JobId,
                    //     OutletId: currUser.OrgId,
                    //     TechnicianAction: 1,
                    //     JobSuggest: 1,
                    //     StallId: mData.StallId,
                    //     VehicleId: mDataCrm.Vehicle.VehicleId,
                    //     PlanStart: mData.PlanStart,
                    //     PlanFinish: mData.PlanFinish,
                    //     PlanDateStart: mData.PlanDateStart,
                    //     PlanDateFinish: mData.PlanDateFinish, // Status: 0,
                    //     AppointmentDate: mData.AppointmentDate,
                    //     JobTask: JobList,
                    //     isGr: 1,
                    //     Km: mData.Km,
                    //     isCash: mData.isCash,
                    //     isSpk: mData.isSpk,
                    //     SpkNo: mData.SpkNo ? mData.SpkNo : '-',
                    //     // IsEstimation: 0,
                    //     EstimationDate: mData.EstimateDeliveryTime,
                    //     WoCategoryId: mData.WoCategoryId,
                    //     JobDate: new Date(),
                    //     KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    //     JobComplaint: JobComplaint,
                    //     JobRequest: JobRequest,
                    //     DecisionMaker: mDataDM.PK.Name,
                    //     PhoneDecisionMaker1: mDataDM.PK.HP1,
                    //     PhoneDecisionMaker2: mDataDM.PK.HP2,
                    //     ContactPerson: mDataDM.CP.Name ? mDataDM.CP.Name : null,
                    //     PhoneContactPerson1: mDataDM.CP.HP1 ? mDataDM.CP.HP1 : null,
                    //     PhoneContactPerson2: mDataDM.CP.HP2 ? mDataDM.CP.HP2 : null,
                    //     UsedPart: mData.flag.parts,
                    //     IsWaiting: mData.flag.wait,
                    //     IsWash: mData.flag.wash,
                    //     JobTaskOpl: oplData
                    // }]

                },
                SimpanWOWalkIn: function(mData, mDataCrm, mDataDM, JobRequest, JobComplaint, oplData, JobList) {
                    console.log('mdata SimpanWOWalkIn >>>', mData, mDataCrm, mDataDM, JobRequest, JobComplaint, oplData, JobList);
                    var a = new Date();
                    var yearFirst = a.getFullYear();
                    var monthFirst = a.getMonth() + 1;
                    var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                    var dayFirst = a.getDate();
                    var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                    var hour = a.getHours();
                    var hours = hour < 10 ? '0' + hour : hour;
                    var minutes = a.getMinutes();
                    var minutess = minutes < 10 ? '0' + minutes : minutes;
                    var seconds = a.getSeconds();
                    var secondss = seconds < 10 ? '0' + seconds : seconds;
                    var WoCreatedDateX = yearFirst + '-' + monthFirsts + '-' + dayFirsts + ' ' + hours + ':' + minutess + ':' + secondss;

                    // =========== start buat fixedDelivery nih ===============

                    var FD1 = new Date(mData.EstimateDate);
                    var FD2 = new Date(mData.AdjusmentDate);

                    var yearFD1 = FD1.getFullYear();
                    var monthFD1 = FD1.getMonth() + 1;
                    monthFD1 = monthFD1 < 10 ? '0' + monthFD1 : monthFD1;
                    var dayFD1 = FD1.getDate();
                    dayFD1 = dayFD1 < 10 ? '0' + dayFD1 : dayFD1;
                    var hourFD1 = FD1.getHours();
                    hourFD1 = hourFD1 < 10 ? '0' + hourFD1 : hourFD1;
                    var minuteFD1 = FD1.getMinutes();
                    minuteFD1 = minuteFD1 < 10 ? '0' + minuteFD1 : minuteFD1;

                    var yearFD2 = FD2.getFullYear();
                    var monthFD2 = FD2.getMonth() + 1;
                    monthFD2 = monthFD2 < 10 ? '0' + monthFD2 : monthFD2;
                    var dayFD2 = FD2.getDate();
                    dayFD2 = dayFD2 < 10 ? '0' + dayFD2 : dayFD2;
                    var hourFD2 = FD2.getHours();
                    hourFD2 = hourFD2 < 10 ? '0' + hourFD2 : hourFD2;
                    var minuteFD2 = FD2.getMinutes();
                    minuteFD2 = minuteFD2 < 10 ? '0' + minuteFD2 : minuteFD2;

                    var FD1Final = yearFD1 + '-' + monthFD1 + '-' + dayFD1 + ' ' + hourFD1 + ':' + minuteFD1
                    var FD2Final = yearFD2 + '-' + monthFD2 + '-' + dayFD2 + ' ' + hourFD2 + ':' + minuteFD2
                        // =========== end buat fixedDelivery nih ===============

                    // UsedPart: mData.flag.parts,
                    // IsWaiting: mData.flag.wait,
                    // IsWash: mData.flag.wash,
                    // PermissionPartChange: mData.flag.cParts,
                    // detect itu object bukan soal nya pernah kejadian tgl 18/2/2019 kedetect object.
                    if (typeof mData.flag.parts == "object") {
                        mData.flag.parts = null;
                    }
                    if (typeof mData.flag.wait == "object") {
                        mData.flag.wait = null;
                    }
                    if (typeof mData.flag.wash == "object") {
                        mData.flag.wash = null;
                    }
                    if (typeof mData.flag.cParts == "object") {
                        mData.flag.cParts = null;
                    }

                    JobList = this.isiGuidLogTaskPartOPL(JobList,mData.GUIDWOGR,1)

                    return $http.put('/api/as/Jobs/updateDetil/2', [{
                        isCash: 1,
                        ToyotaIdRequest: mDataCrm.Customer.ToyotaIDFlag == null ? 0 : mDataCrm.Customer.ToyotaIDFlag,
                        JobId: 0,
                        JobTask: JobList,
                        JobComplaint: JobComplaint,
                        JobRequest: JobRequest,
                        JobWAC: [],
                        JobNo: mData.JobNo,
                        OutletId: currUser.OrgId,
                        CalId: mData.CalId,
                        StallId: null,
                        JobListSplitChip: mData.JobListSplitChip,

                        FinalDP: mData.FinalDP,
                        DPPaidAmount: mData.DPPaidAmount,
                        ORPaidAmount: mData.ORPaidAmount,
                        DPRawatJalan: mData.DPRawatJalan,
                        IsPaidRawatJalan: mData.IsPaidRawatJalan,
                        
                        // Stall: mData.Stall,
                        VehicleId: mDataCrm.Vehicle.VehicleId,
                        PlanStart: mData.PlanStart,
                        PlanFinish: mData.PlanFinish,
                        CustRequest: mData.CustRequest,
                        CustComplaint: mData.CustComplaint,
                        TechnicianAction: mData.TechnicianAction1,
                        JobSuggest: mData.JobSuggest1,
                        T1: mData.T1,
                        T2: mData.T2,
                        RSA: mData.RSA,
                        AdditionalTime: mData.AdditionalTime,
                        JobType: mData.JobType,
                        Status: mData.Status,
                        // AppointmentDate: mData.AppointmentDate,
                        AppointmentDate: $filter('date')(mData.AppointmentDate, 'yyyy-MM-dd'),
                        AppointmentTime: mData.AppointmentTime,
                        AppointmentNo: mData.AppointmentNo,
                        isAppointment: mData.isAppointment,
                        AppointmentVia: mData.AppointmentVia,
                        isGr: 1,
                        Km: mData.KmNormal,
                        // isCash: mData.isCash,
                        isSpk: mData.isSpk,
                        SpkNo: mData.SpkNo ? mData.SpkNo : '-',
                        InsuranceName: mData.InsuranceName,
                        isWOBase: mData.isWOBase,
                        WoCategoryId: mData.WoCategoryId,
                        WoNo: mData.WoNo,
                        IsEstimation: mData.IsEstimation,
                        EstimationDate: mData.EstimateDeliveryTime,
                        EstimationNo: mData.EstimationNo,
                        JobDate: mData.JobDate,
                        // PermissionPartChange: mData.PermissionPartChange,
                        PaymentMethod: mData.PaymentMethod,
                        // DecisionMaker: mDataDM.PK.Name ? mDataDM.PK.Name : null,
                        // PhoneDecisionMaker1: mDataDM.PK.HP1 ? mDataDM.PK.HP1 : null,
                        // PhoneDecisionMaker2: mDataDM.PK.HP2 ? mDataDM.PK.HP2 : null,
                        DecisionMaker: mDataDM.PK.Name,
                        PhoneDecisionMaker1: mDataDM.PK.HP1,
                        PhoneDecisionMaker2: mDataDM.PK.HP2,
                        ContactPerson: mDataDM.CP.Name ? mDataDM.CP.Name : null,
                        PhoneContactPerson1: mDataDM.CP.HP1 ? mDataDM.CP.HP1 : null,
                        PhoneContactPerson2: mDataDM.CP.HP2 ? mDataDM.CP.HP2 : null,
                        UsedPart: mData.flag.parts,
                        IsWaiting: mData.flag.wait,
                        IsWash: mData.flag.wash,
                        PermissionPartChange: mData.flag.cParts,
                        // FixedDeliveryTime1: new Date(mData.EstimateDate),
                        // FixedDeliveryTime2: new Date(mData.AdjusmentDate),
                        FixedDeliveryTime1: FD1Final,
                        FixedDeliveryTime2: FD2Final,
                        FixedDeliveryTime: mData.FixedDeliveryTime,
                        invoiceType: mData.CodeTransaction,
                        JobTaskOpl: oplData,
                        ModelType: mData.VehicleModelName,
                        PoliceNumber: mDataCrm.Vehicle.LicensePlate,
                        isFoOneHour: mData.isFoOneHour,
                        isFoOneDay: mData.isFoOneDay,
                        HandOverDate: mData.HandOverDate,
                        KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                        Revision: mData.Revision,
                        NewAppointmentRel: mData.NewAppointmentRel,
                        RescheduleReason: mData.RescheduleReason,
                        JobCatgForBPid: mData.JobCatgForBPid,
                        JobCatgForBP: mData.JobCatgForBP,
                        ActiveChip: mData.ActiveChip,
                        IsStopPage: mData.IsStopPage,
                        Suggestion: mData.Suggestion,
                        SuggestionCatg: mData.SuggestionCatg,
                        SuggestionDate: mData.SuggestionDate,
                        // PlanDateStart: mData.PlanDateStart,
                        // PlanDateFinish: mData.PlanDateFinish,
                        PlanDateStart: $filter('date')(mData.PlanDateStart, 'yyyy-MM-dd'),
                        PlanDateFinish: $filter('date')(mData.PlanDateFinish, 'yyyy-MM-dd'),
                        closeWoId: mData.closeWoId,
                        CloseWODate: mData.CloseWODate,
                        MaterialRequestNo: mData.MaterialRequestNo,
                        MaterialRequestStatusId: mData.MaterialRequestStatusId,
                        WoCreatedDate: WoCreatedDateX,
                        GateInPushTime: $filter('date')(mData.GateInPushTime, 'yyyy-MM-dd HH:mm:ss'),
                        GateInUserId: mData.GateInUserId,
                        CarWashTime: mData.CarWashTime,
                        CarWashUserId: mData.CarWashUserId,
                        TecoTime: mData.TecoTime,
                        TecoUserId: mData.TecoUserId,
                        BillingCreatedTime: mData.BillingCreatedTime,
                        BillingUserId: mData.BillingUserId,
                        CustCallTime: mData.CustCallTime,
                        CustCallUserId: mData.CustCallUserId,
                        ServiceExplainCallTime: mData.ServiceExplainCallTime,
                        ServiceExplainUserId: mData.ServiceExplainUserId,
                        AdditionalTimeAmount: mData.AdditionalTimeAmount,
                        IsAllPulledBilling: mData.IsAllPulledBilling,
                        IsInStopPageGr: mData.IsInStopPageGr,
                        StopPageTimeGr: mData.StopPageTimeGr,
                        StopPageUserIdGr: mData.StopPageUserIdGr,
                        VendorCode: mData.VendorCode,
                        ModelCode: mData.ModelCode,
                        DealerCode: mData.DealerCode,
                        Email: mData.Email,
                        CancelDate: mData.CancelDate,
                        CancelReason: mData.CancelReason,
                        CancelUserId: mData.CancelUserId,
                        isWoFromBpSatelit: mData.isWoFromBpSatelit,
                        BpSatelitOutletId: mData.BpSatelitOutletId,
                        BpSatelitSAUserId: mData.BpSatelitSAUserId,
                        isReqOutPatient: mData.isReqOutPatient,
                        OutPatientUserIdRequest: mData.OutPatientUserIdRequest,
                        OutPatientReqDate: mData.OutPatientReqDate,
                        isOutPatient: mData.isOutPatient,
                        OutPatientUserIdApproved: mData.OutPatientUserIdApproved,
                        OutPatientApproveDate: mData.OutPatientApproveDate,
                        OutPatientApprovedNo: mData.OutPatientApprovedNo,
                        OutPatientUserIdReject: mData.OutPatientUserIdReject,
                        OutPatientRejectDate: mData.OutPatientRejectDate,
                        OutPatientFinishDate: mData.OutPatientFinishDate,
                        SKB: mData.SKB,
                        UserIdSa: currUser.UserId,
                        UserSa: mData.UserSa,
                        MProfileForemanId: mData.MProfileForemanId,
                        MProfileForeman: mData.MProfileForeman,
                        UserIdApp: mData.UserIdApp,
                        UserApp: mData.UserApp,
                        AppointmentCreateDate: mData.AppointmentCreateDate,
                        GroupBPId: mData.GroupBPId,
                        groupBP: mData.groupBP,
                        ORAmount: mData.ORAmount,
                        InsuranceId: mData.InsuranceId,
                        Insurance: mData.Insurance,
                        VehicleTypeId: mDataCrm.Vehicle.isNonTAM == 0 ? (mData.VehicleTypeId == null ? mDataCrm.Vehicle.Type.VehicleTypeId : mData.VehicleTypeId) : null,
                        UserIdWORelease: currUser.UserId,
                        isDraftWo: mData.isDraftWo,
                        AssemblyYearForEstimation: mData.AssemblyYearForEstimation,
                        SendToTowasDate: mData.SendToTowasDate,
                        ReceiveFromTowasDate: mData.ReceiveFromTowasDate,
                        BillingInsuranceType: mData.BillingInsuranceType,
                        statusTowas: mData.statusTowas,
                        responseStatusFromTowas: mData.responseStatusFromTowas,
                        towasConfirmedBySA: mData.towasConfirmedBySA,
                        towasConfirmedUserIdSA: mData.towasConfirmedUserIdSA,
                        WarantyNoFromTowas: mData.WarantyNoFromTowas,
                        AcceptableAmount: mData.AcceptableAmount,
                        ErrCode: mData.ErrCode,
                        TechnicianNotes: mData.TechnicianNotes,
                        ClaimAmount: mData.ClaimAmount,
                        ApprovalRoleId: mData.ApprovalRoleId,
                        ApprovalUserId: mData.ApprovalUserId,
                        ApprovalBy: mData.ApprovalBy,
                        ApprovalTime: mData.ApprovalTime,
                        ApprovalStatus: mData.ApprovalStatus,
                        AddressForEstimation: mData.AddressForEstimation,
                        VehicleColor: mData.VehicleColor,
                        StatusPreDiagnose: mData.StatusPreDiagnose,
                        QueueId: mData.QueueId,
                        IsCustomerRequest : mData.IsCustomerRequest != 1 ? 0 : 1,
                        AppointmentCreateDate: (mData.AppointmentCreateDate == null || mData.AppointmentCreateDate == undefined) ? null : $filter('date')(mData.AppointmentCreateDate, 'yyyy-MM-dd HH:mm:ss'),
                        Guid_Log: mData.GUIDWOGR,
                        
                    }]);
                },
                releaseWO: function(mData, mDataCrm, mDataDM, JobRequest, JobComplaint, oplData, JobList, JobId) {
                    // debugger;
                    console.log('mdata releaseWO >>>', mData, mDataCrm, mDataDM, JobRequest, JobComplaint, oplData, JobList);
                    var a = new Date();
                    var yearFirst = a.getFullYear();
                    var monthFirst = a.getMonth() + 1;
                    var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                    var dayFirst = a.getDate();
                    var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                    var hour = a.getHours();
                    var hours = hour < 10 ? '0' + hour : hour;
                    var minutes = a.getMinutes();
                    var minutess = minutes < 10 ? '0' + minutes : minutes;
                    var seconds = a.getSeconds();
                    var secondss = seconds < 10 ? '0' + seconds : seconds;
                    var WoCreatedDateX = yearFirst + '-' + monthFirsts + '-' + dayFirsts + ' ' + hours + ':' + minutess + ':' + secondss;

                    // =========== start buat fixedDelivery nih ===============

                    var FD1 = new Date(mData.EstimateDate);
                    var FD2 = new Date(mData.AdjusmentDate);

                    var yearFD1 = FD1.getFullYear();
                    var monthFD1 = FD1.getMonth() + 1;
                    monthFD1 = monthFD1 < 10 ? '0' + monthFD1 : monthFD1;
                    var dayFD1 = FD1.getDate();
                    dayFD1 = dayFD1 < 10 ? '0' + dayFD1 : dayFD1;
                    var hourFD1 = FD1.getHours();
                    hourFD1 = hourFD1 < 10 ? '0' + hourFD1 : hourFD1;
                    var minuteFD1 = FD1.getMinutes();
                    minuteFD1 = minuteFD1 < 10 ? '0' + minuteFD1 : minuteFD1;

                    var yearFD2 = FD2.getFullYear();
                    var monthFD2 = FD2.getMonth() + 1;
                    monthFD2 = monthFD2 < 10 ? '0' + monthFD2 : monthFD2;
                    var dayFD2 = FD2.getDate();
                    dayFD2 = dayFD2 < 10 ? '0' + dayFD2 : dayFD2;
                    var hourFD2 = FD2.getHours();
                    hourFD2 = hourFD2 < 10 ? '0' + hourFD2 : hourFD2;
                    var minuteFD2 = FD2.getMinutes();
                    minuteFD2 = minuteFD2 < 10 ? '0' + minuteFD2 : minuteFD2;

                    var FD1Final = yearFD1 + '-' + monthFD1 + '-' + dayFD1 + ' ' + hourFD1 + ':' + minuteFD1
                    var FD2Final = yearFD2 + '-' + monthFD2 + '-' + dayFD2 + ' ' + hourFD2 + ':' + minuteFD2
                        // =========== end buat fixedDelivery nih ===============
                    //isi log buat task part opl ============================== start
                    JobList = this.isiGuidLogTaskPartOPL(JobList,mData.GUIDWOGR,1)
                    oplData = this.isiGuidLogTaskPartOPL(oplData,mData.GUIDWOGR,2)
                    //isi log buat task part opl ============================== end

                    return $http.put('/api/as/Jobs/updateDetil/2?createNo=1', [{
                        isCash: 1,
                        ToyotaIdRequest: mDataCrm.Customer.ToyotaIDFlag == null ? 0 : mDataCrm.Customer.ToyotaIDFlag,
                        JobId: JobId,
                        JobTask: JobList,
                        JobComplaint: JobComplaint,
                        JobRequest: JobRequest,
                        JobWAC: [],
                        JobNo: mData.JobNo,
                        OutletId: currUser.OrgId,
                        CalId: mData.CalId,
                        StallId: mData.StallId,
                        JobListSplitChip: mData.JobListSplitChip,
                        FinalDP: mData.FinalDP,
                        
                        DPPaidAmount: mData.DPPaidAmount,
                        ORPaidAmount: mData.ORPaidAmount,
                        DPRawatJalan: mData.DPRawatJalan,
                        IsPaidRawatJalan: mData.IsPaidRawatJalan,

                        // Stall: mData.Stall,
                        VehicleId: mDataCrm.Vehicle.VehicleId,
                        PlanStart: mData.PlanStart,
                        PlanFinish: mData.PlanFinish,
                        CustRequest: mData.CustRequest,
                        CustComplaint: mData.CustComplaint,
                        TechnicianAction: mData.TechnicianAction1,
                        JobSuggest: mData.JobSuggest1,
                        T1: mData.T1,
                        T2: mData.T2,
                        RSA: mData.RSA,
                        AdditionalTime: mData.AdditionalTime,
                        JobType: mData.JobType,
                        Status: mData.Status,
                        AppointmentDate: mData.AppointmentDate,
                        AppointmentTime: mData.AppointmentTime,
                        AppointmentNo: mData.AppointmentNo,
                        isAppointment: mData.isAppointment,
                        AppointmentVia: mData.AppointmentVia,
                        isGr: 1,
                        Km: mData.KmNormal,
                        // isCash: mData.isCash,
                        isSpk: mData.isSpk,
                        SpkNo: mData.SpkNo ? mData.SpkNo : '-',
                        InsuranceName: mData.InsuranceName,
                        isWOBase: mData.isWOBase,
                        WoCategoryId: mData.WoCategoryId,
                        WoNo: mData.WoNo,
                        IsEstimation: mData.IsEstimation,
                        EstimationDate: mData.EstimateDeliveryTime,
                        EstimationNo: mData.EstimationNo,
                        JobDate: mData.JobDate,
                        // PermissionPartChange: mData.PermissionPartChange,
                        PaymentMethod: mData.PaymentMethod,
                        // DecisionMaker: mDataDM.PK.Name ? mDataDM.PK.Name : null,
                        // PhoneDecisionMaker1: mDataDM.PK.HP1 ? mDataDM.PK.HP1 : null,
                        // PhoneDecisionMaker2: mDataDM.PK.HP2 ? mDataDM.PK.HP2 : null,
                        DecisionMaker: mDataDM.PK.Name,
                        PhoneDecisionMaker1: mDataDM.PK.HP1,
                        PhoneDecisionMaker2: mDataDM.PK.HP2,
                        ContactPerson: mDataDM.CP.Name ? mDataDM.CP.Name : null,
                        PhoneContactPerson1: mDataDM.CP.HP1 ? mDataDM.CP.HP1 : null,
                        PhoneContactPerson2: mDataDM.CP.HP2 ? mDataDM.CP.HP2 : null,
                        UsedPart: mData.flag.parts,
                        IsWaiting: mData.flag.wait,
                        IsWash: mData.flag.wash,
                        PermissionPartChange: mData.flag.cParts,
                        // FixedDeliveryTime1: new Date(mData.EstimateDate),
                        // FixedDeliveryTime2: new Date(mData.AdjusmentDate),
                        FixedDeliveryTime1: FD1Final,
                        FixedDeliveryTime2: FD2Final,
                        FixedDeliveryTime: mData.FixedDeliveryTime,
                        invoiceType: mData.CodeTransaction,
                        JobTaskOpl: oplData,
                        ModelType: mData.VehicleModelName,
                        PoliceNumber: mDataCrm.Vehicle.LicensePlate,
                        isFoOneHour: mData.isFoOneHour,
                        isFoOneDay: mData.isFoOneDay,
                        HandOverDate: mData.HandOverDate,
                        KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                        Revision: mData.Revision,
                        NewAppointmentRel: mData.NewAppointmentRel,
                        RescheduleReason: mData.RescheduleReason,
                        JobCatgForBPid: mData.JobCatgForBPid,
                        JobCatgForBP: mData.JobCatgForBP,
                        ActiveChip: mData.ActiveChip,
                        IsStopPage: mData.IsStopPage,
                        Suggestion: mData.Suggestion,
                        SuggestionCatg: mData.SuggestionCatg,
                        SuggestionDate: mData.SuggestionDate,
                        PlanDateStart: mData.PlanDateStart,
                        PlanDateFinish: mData.PlanDateFinish,
                        closeWoId: mData.closeWoId,
                        CloseWODate: mData.CloseWODate,
                        MaterialRequestNo: mData.MaterialRequestNo,
                        MaterialRequestStatusId: mData.MaterialRequestStatusId,
                        WoCreatedDate: WoCreatedDateX,
                        GateInPushTime: $filter('date')(mData.GateInPushTime, 'yyyy-MM-dd HH:mm:ss'),
                        GateInUserId: mData.GateInUserId,
                        CarWashTime: mData.CarWashTime,
                        CarWashUserId: mData.CarWashUserId,
                        TecoTime: mData.TecoTime,
                        TecoUserId: mData.TecoUserId,
                        BillingCreatedTime: mData.BillingCreatedTime,
                        BillingUserId: mData.BillingUserId,
                        CustCallTime: mData.CustCallTime,
                        CustCallUserId: mData.CustCallUserId,
                        ServiceExplainCallTime: mData.ServiceExplainCallTime,
                        ServiceExplainUserId: mData.ServiceExplainUserId,
                        AdditionalTimeAmount: mData.AdditionalTimeAmount,
                        IsAllPulledBilling: mData.IsAllPulledBilling,
                        IsInStopPageGr: mData.IsInStopPageGr,
                        StopPageTimeGr: mData.StopPageTimeGr,
                        StopPageUserIdGr: mData.StopPageUserIdGr,
                        VendorCode: mData.VendorCode,
                        ModelCode: mData.ModelCode,
                        DealerCode: mData.DealerCode,
                        Email: mData.Email,
                        CancelDate: mData.CancelDate,
                        CancelReason: mData.CancelReason,
                        CancelUserId: mData.CancelUserId,
                        isWoFromBpSatelit: mData.isWoFromBpSatelit,
                        BpSatelitOutletId: mData.BpSatelitOutletId,
                        BpSatelitSAUserId: mData.BpSatelitSAUserId,
                        isReqOutPatient: mData.isReqOutPatient,
                        OutPatientUserIdRequest: mData.OutPatientUserIdRequest,
                        OutPatientReqDate: mData.OutPatientReqDate,
                        isOutPatient: mData.isOutPatient,
                        OutPatientUserIdApproved: mData.OutPatientUserIdApproved,
                        OutPatientApproveDate: mData.OutPatientApproveDate,
                        OutPatientApprovedNo: mData.OutPatientApprovedNo,
                        OutPatientUserIdReject: mData.OutPatientUserIdReject,
                        OutPatientRejectDate: mData.OutPatientRejectDate,
                        OutPatientFinishDate: mData.OutPatientFinishDate,
                        SKB: mData.SKB,
                        UserIdSa: currUser.UserId,
                        UserSa: mData.UserSa,
                        MProfileForemanId: mData.MProfileForemanId,
                        MProfileForeman: mData.MProfileForeman,
                        UserIdApp: mData.UserIdApp,
                        UserApp: mData.UserApp,
                        AppointmentCreateDate: mData.AppointmentCreateDate,
                        GroupBPId: mData.GroupBPId,
                        groupBP: mData.groupBP,
                        ORAmount: mData.ORAmount,
                        InsuranceId: mData.InsuranceId,
                        Insurance: mData.Insurance,
                        VehicleTypeId: mDataCrm.Vehicle.isNonTAM == 0 ? (mData.VehicleTypeId == null ? mDataCrm.Vehicle.Type.VehicleTypeId : mData.VehicleTypeId) : null,
                        UserIdWORelease: currUser.UserId,
                        isDraftWo: mData.isDraftWo,
                        AssemblyYearForEstimation: mData.AssemblyYearForEstimation,
                        SendToTowasDate: mData.SendToTowasDate,
                        ReceiveFromTowasDate: mData.ReceiveFromTowasDate,
                        BillingInsuranceType: mData.BillingInsuranceType,
                        statusTowas: mData.statusTowas,
                        responseStatusFromTowas: mData.responseStatusFromTowas,
                        towasConfirmedBySA: mData.towasConfirmedBySA,
                        towasConfirmedUserIdSA: mData.towasConfirmedUserIdSA,
                        WarantyNoFromTowas: mData.WarantyNoFromTowas,
                        AcceptableAmount: mData.AcceptableAmount,
                        ErrCode: mData.ErrCode,
                        TechnicianNotes: mData.TechnicianNotes,
                        ClaimAmount: mData.ClaimAmount,
                        ApprovalRoleId: mData.ApprovalRoleId,
                        ApprovalUserId: mData.ApprovalUserId,
                        ApprovalBy: mData.ApprovalBy,
                        ApprovalTime: mData.ApprovalTime,
                        ApprovalStatus: mData.ApprovalStatus,
                        AddressForEstimation: mData.AddressForEstimation,
                        VehicleColor: mData.VehicleColor,
                        StatusPreDiagnose: mData.StatusPreDiagnose,
                        Guid_Log: mData.GUIDWOGR,
                        QueueId: mData.QueueId,
                        IsCustomerRequest : mData.IsCustomerRequest != 1 ? 0 : 1,
                        AppointmentCreateDate: (mData.AppointmentCreateDate == null || mData.AppointmentCreateDate == undefined) ? null : $filter('date')(mData.AppointmentCreateDate, 'yyyy-MM-dd HH:mm:ss'),

                    }]);
                },
                releaseWOWalkIn: function(mData, mDataCrm, mDataDM, JobRequest, JobComplaint, oplData, JobList) {
                    console.log('mdata releaseWOWalkIn >>>', mData, mDataCrm, mDataDM, JobRequest, JobComplaint, oplData, JobList);
                    var a = new Date();
                    var yearFirst = a.getFullYear();
                    var monthFirst = a.getMonth() + 1;
                    var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                    var dayFirst = a.getDate();
                    var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                    var hour = a.getHours();
                    var hours = hour < 10 ? '0' + hour : hour;
                    var minutes = a.getMinutes();
                    var minutess = minutes < 10 ? '0' + minutes : minutes;
                    var seconds = a.getSeconds();
                    var secondss = seconds < 10 ? '0' + seconds : seconds;
                    var WoCreatedDateX = yearFirst + '-' + monthFirsts + '-' + dayFirsts + ' ' + hours + ':' + minutess + ':' + secondss;

                    // =========== start buat fixedDelivery nih ===============

                    var FD1 = new Date(mData.EstimateDate);
                    var FD2 = new Date(mData.AdjusmentDate);

                    var yearFD1 = FD1.getFullYear();
                    var monthFD1 = FD1.getMonth() + 1;
                    monthFD1 = monthFD1 < 10 ? '0' + monthFD1 : monthFD1;
                    var dayFD1 = FD1.getDate();
                    dayFD1 = dayFD1 < 10 ? '0' + dayFD1 : dayFD1;
                    var hourFD1 = FD1.getHours();
                    hourFD1 = hourFD1 < 10 ? '0' + hourFD1 : hourFD1;
                    var minuteFD1 = FD1.getMinutes();
                    minuteFD1 = minuteFD1 < 10 ? '0' + minuteFD1 : minuteFD1;

                    var yearFD2 = FD2.getFullYear();
                    var monthFD2 = FD2.getMonth() + 1;
                    monthFD2 = monthFD2 < 10 ? '0' + monthFD2 : monthFD2;
                    var dayFD2 = FD2.getDate();
                    dayFD2 = dayFD2 < 10 ? '0' + dayFD2 : dayFD2;
                    var hourFD2 = FD2.getHours();
                    hourFD2 = hourFD2 < 10 ? '0' + hourFD2 : hourFD2;
                    var minuteFD2 = FD2.getMinutes();
                    minuteFD2 = minuteFD2 < 10 ? '0' + minuteFD2 : minuteFD2;

                    var FD1Final = yearFD1 + '-' + monthFD1 + '-' + dayFD1 + ' ' + hourFD1 + ':' + minuteFD1
                    var FD2Final = yearFD2 + '-' + monthFD2 + '-' + dayFD2 + ' ' + hourFD2 + ':' + minuteFD2
                        // =========== end buat fixedDelivery nih ===============
                    //isi log buat task part opl ============================== start
                    JobList = this.isiGuidLogTaskPartOPL(JobList,mData.GUIDWOGR,1)
                    oplData = this.isiGuidLogTaskPartOPL(oplData,mData.GUIDWOGR,2)
                    //isi log buat task part opl ============================== end

                    return $http.put('/api/as/Jobs/updateDetil/2?createNo=1', [{
                        isCash: 1,
                        ToyotaIdRequest: mDataCrm.Customer.ToyotaIDFlag == null ? 0 : mDataCrm.Customer.ToyotaIDFlag,
                        JobId: 0,
                        JobTask: JobList,
                        JobComplaint: JobComplaint,
                        JobRequest: JobRequest,
                        JobWAC: [],
                        JobNo: mData.JobNo,
                        OutletId: currUser.OrgId,
                        CalId: mData.CalId,
                        StallId: mData.StallId,
                        JobListSplitChip: mData.JobListSplitChip,
                        FinalDP: mData.FinalDP,

                        DPPaidAmount: mData.DPPaidAmount,
                        ORPaidAmount: mData.ORPaidAmount,
                        DPRawatJalan: mData.DPRawatJalan,
                        IsPaidRawatJalan: mData.IsPaidRawatJalan,

                        // Stall: mData.Stall,
                        VehicleId: mDataCrm.Vehicle.VehicleId,
                        PlanStart: mData.PlanStart,
                        PlanFinish: mData.PlanFinish,
                        CustRequest: mData.CustRequest,
                        CustComplaint: mData.CustComplaint,
                        TechnicianAction: mData.TechnicianAction1,
                        JobSuggest: mData.JobSuggest1,
                        T1: mData.T1,
                        T2: mData.T2,
                        RSA: mData.RSA,
                        AdditionalTime: mData.AdditionalTime,
                        JobType: mData.JobType,
                        Status: mData.Status,
                        AppointmentDate: mData.AppointmentDate,
                        AppointmentTime: mData.AppointmentTime,
                        AppointmentNo: mData.AppointmentNo,
                        isAppointment: mData.isAppointment,
                        AppointmentVia: mData.AppointmentVia,
                        isGr: 1,
                        Km: mData.KmNormal,
                        // isCash: mData.isCash,
                        isSpk: mData.isSpk,
                        SpkNo: mData.SpkNo ? mData.SpkNo : '-',
                        InsuranceName: mData.InsuranceName,
                        isWOBase: mData.isWOBase,
                        WoCategoryId: mData.WoCategoryId,
                        WoNo: mData.WoNo,
                        IsEstimation: mData.IsEstimation,
                        EstimationDate: mData.EstimateDeliveryTime,
                        EstimationNo: mData.EstimationNo,
                        JobDate: mData.JobDate,
                        // PermissionPartChange: mData.PermissionPartChange,
                        PaymentMethod: mData.PaymentMethod,
                        // DecisionMaker: mDataDM.PK.Name ? mDataDM.PK.Name : null,
                        // PhoneDecisionMaker1: mDataDM.PK.HP1 ? mDataDM.PK.HP1 : null,
                        // PhoneDecisionMaker2: mDataDM.PK.HP2 ? mDataDM.PK.HP2 : null,
                        DecisionMaker: mDataDM.PK.Name,
                        PhoneDecisionMaker1: mDataDM.PK.HP1,
                        PhoneDecisionMaker2: mDataDM.PK.HP2,
                        ContactPerson: mDataDM.CP.Name ? mDataDM.CP.Name : null,
                        PhoneContactPerson1: mDataDM.CP.HP1 ? mDataDM.CP.HP1 : null,
                        PhoneContactPerson2: mDataDM.CP.HP2 ? mDataDM.CP.HP2 : null,
                        UsedPart: mData.flag.parts,
                        IsWaiting: mData.flag.wait,
                        IsWash: mData.flag.wash,
                        PermissionPartChange: mData.flag.cParts,
                        // FixedDeliveryTime1: new Date(mData.EstimateDate),
                        // FixedDeliveryTime2: new Date(mData.AdjusmentDate),
                        FixedDeliveryTime1: FD1Final,
                        FixedDeliveryTime2: FD2Final,
                        FixedDeliveryTime: mData.FixedDeliveryTime,
                        invoiceType: mData.CodeTransaction,
                        JobTaskOpl: oplData,
                        ModelType: mData.VehicleModelName,
                        PoliceNumber: mDataCrm.Vehicle.LicensePlate,
                        isFoOneHour: mData.isFoOneHour,
                        isFoOneDay: mData.isFoOneDay,
                        HandOverDate: mData.HandOverDate,
                        KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                        Revision: mData.Revision,
                        NewAppointmentRel: mData.NewAppointmentRel,
                        RescheduleReason: mData.RescheduleReason,
                        JobCatgForBPid: mData.JobCatgForBPid,
                        JobCatgForBP: mData.JobCatgForBP,
                        ActiveChip: mData.ActiveChip,
                        IsStopPage: mData.IsStopPage,
                        Suggestion: mData.Suggestion,
                        SuggestionCatg: mData.SuggestionCatg,
                        SuggestionDate: mData.SuggestionDate,
                        PlanDateStart: mData.PlanDateStart,
                        PlanDateFinish: mData.PlanDateFinish,
                        closeWoId: mData.closeWoId,
                        CloseWODate: mData.CloseWODate,
                        MaterialRequestNo: mData.MaterialRequestNo,
                        MaterialRequestStatusId: mData.MaterialRequestStatusId,
                        WoCreatedDate: WoCreatedDateX,
                        GateInPushTime: $filter('date')(mData.GateInPushTime, 'yyyy-MM-dd HH:mm:ss'),
                        GateInUserId: mData.GateInUserId,
                        CarWashTime: mData.CarWashTime,
                        CarWashUserId: mData.CarWashUserId,
                        TecoTime: mData.TecoTime,
                        TecoUserId: mData.TecoUserId,
                        BillingCreatedTime: mData.BillingCreatedTime,
                        BillingUserId: mData.BillingUserId,
                        CustCallTime: mData.CustCallTime,
                        CustCallUserId: mData.CustCallUserId,
                        ServiceExplainCallTime: mData.ServiceExplainCallTime,
                        ServiceExplainUserId: mData.ServiceExplainUserId,
                        AdditionalTimeAmount: mData.AdditionalTimeAmount,
                        IsAllPulledBilling: mData.IsAllPulledBilling,
                        IsInStopPageGr: mData.IsInStopPageGr,
                        StopPageTimeGr: mData.StopPageTimeGr,
                        StopPageUserIdGr: mData.StopPageUserIdGr,
                        VendorCode: mData.VendorCode,
                        ModelCode: mData.ModelCode,
                        DealerCode: mData.DealerCode,
                        Email: mData.Email,
                        CancelDate: mData.CancelDate,
                        CancelReason: mData.CancelReason,
                        CancelUserId: mData.CancelUserId,
                        isWoFromBpSatelit: mData.isWoFromBpSatelit,
                        BpSatelitOutletId: mData.BpSatelitOutletId,
                        BpSatelitSAUserId: mData.BpSatelitSAUserId,
                        isReqOutPatient: mData.isReqOutPatient,
                        OutPatientUserIdRequest: mData.OutPatientUserIdRequest,
                        OutPatientReqDate: mData.OutPatientReqDate,
                        isOutPatient: mData.isOutPatient,
                        OutPatientUserIdApproved: mData.OutPatientUserIdApproved,
                        OutPatientApproveDate: mData.OutPatientApproveDate,
                        OutPatientApprovedNo: mData.OutPatientApprovedNo,
                        OutPatientUserIdReject: mData.OutPatientUserIdReject,
                        OutPatientRejectDate: mData.OutPatientRejectDate,
                        OutPatientFinishDate: mData.OutPatientFinishDate,
                        SKB: mData.SKB,
                        UserIdSa: currUser.UserId,
                        UserSa: mData.UserSa,
                        MProfileForemanId: mData.MProfileForemanId,
                        MProfileForeman: mData.MProfileForeman,
                        UserIdApp: mData.UserIdApp,
                        UserApp: mData.UserApp,
                        AppointmentCreateDate: mData.AppointmentCreateDate,
                        GroupBPId: mData.GroupBPId,
                        groupBP: mData.groupBP,
                        ORAmount: mData.ORAmount,
                        InsuranceId: mData.InsuranceId,
                        Insurance: mData.Insurance,
                        VehicleTypeId: mDataCrm.Vehicle.isNonTAM == 0 ? (mData.VehicleTypeId == null ? mDataCrm.Vehicle.Type.VehicleTypeId : mData.VehicleTypeId) : null,
                        UserIdWORelease: currUser.UserId,
                        isDraftWo: mData.isDraftWo,
                        AssemblyYearForEstimation: mData.AssemblyYearForEstimation,
                        SendToTowasDate: mData.SendToTowasDate,
                        ReceiveFromTowasDate: mData.ReceiveFromTowasDate,
                        BillingInsuranceType: mData.BillingInsuranceType,
                        statusTowas: mData.statusTowas,
                        responseStatusFromTowas: mData.responseStatusFromTowas,
                        towasConfirmedBySA: mData.towasConfirmedBySA,
                        towasConfirmedUserIdSA: mData.towasConfirmedUserIdSA,
                        WarantyNoFromTowas: mData.WarantyNoFromTowas,
                        AcceptableAmount: mData.AcceptableAmount,
                        ErrCode: mData.ErrCode,
                        TechnicianNotes: mData.TechnicianNotes,
                        ClaimAmount: mData.ClaimAmount,
                        ApprovalRoleId: mData.ApprovalRoleId,
                        ApprovalUserId: mData.ApprovalUserId,
                        ApprovalBy: mData.ApprovalBy,
                        ApprovalTime: mData.ApprovalTime,
                        ApprovalStatus: mData.ApprovalStatus,
                        AddressForEstimation: mData.AddressForEstimation,
                        VehicleColor: mData.VehicleColor,
                        StatusPreDiagnose: mData.StatusPreDiagnose,
                        Guid_Log: mData.GUIDWOGR,
                        QueueId: mData.QueueId,
                        IsCustomerRequest : mData.IsCustomerRequest != 1 ? 0 : 1,
                        AppointmentCreateDate: (mData.AppointmentCreateDate == null || mData.AppointmentCreateDate == undefined) ? null : $filter('date')(mData.AppointmentCreateDate, 'yyyy-MM-dd HH:mm:ss'),

                    }]);

                    // JobId: 0,
                    // TechnicianAction: 1,
                    // OutletId: currUser.OrgId,
                    // JobSuggest: 1,
                    // VehicleId: mDataCrm.Vehicle.VehicleId,
                    // StallId: mData.StallId,
                    // PlanStart: mData.PlanStart,
                    // PlanFinish: mData.PlanFinish,
                    // PlanDateStart: mData.PlanDateStart,
                    // PlanDateFinish: mData.PlanDateFinish, // Status: 0,
                    // AppointmentDate: mData.AppointmentDate,
                    // JobTask: JobList,
                    // isGr: 1,
                    // Km: mData.Km,
                    // isCash: mData.isCash,
                    // isSpk: mData.isSpk,
                    // SpkNo: mData.SpkNo ? mData.SpkNo : '-',
                    // // IsEstimation: 0,
                    // WoCategoryId: mData.WoCategoryId,
                    // EstimationDate: mData.EstimateDeliveryTime,
                    // JobDate: new Date(),
                    // KatashikiCode: mDataCrm.Vehicle.KatashikiCode,
                    // JobComplaint: JobComplaint,
                    // JobRequest: JobRequest,
                    // DecisionMaker: mDataDM.PK.Name,
                    // PhoneDecisionMaker1: mDataDM.PK.HP1,
                    // PhoneDecisionMaker2: mDataDM.PK.HP2,
                    // ContactPerson: mDataDM.CP.Name ? mDataDM.CP.Name : null,
                    // PhoneContactPerson1: mDataDM.CP.HP1 ? mDataDM.CP.HP1 : null,
                    // PhoneContactPerson2: mDataDM.CP.HP2 ? mDataDM.CP.HP2 : null,
                    // UsedPart: mData.flag.parts,
                    // IsWaiting: mData.flag.wait,
                    // IsWash: mData.flag.wash,
                    // JobTaskOpl: oplData

                },
                getTowass: function(vin) {
                    var res = $http.get('/api/as/Towass/getFieldAction/1/' + vin);
                    return res;
                },
                getMRS: function(ReminderTypeId) {
                    var res = $http.get('/api/as/Towass/getFieldMRS/' + ReminderTypeId);
                    return res;
                },
                updatePlatNo: function(id, NewPlat) {
                    return $http.put('/api/as/queue/updateLicence/1/' + id + '/' + NewPlat + '');
                },
                getVehicleUser: function(id) {
                    return $http.get('/api/crm/GetVehicleUser/' + id);
                },
                getWACBPbyItemId: function(jobItemId) {
                    // var WAC = function
                    return $http.get('/api/as/JobWACBPs/getByItem/' + jobItemId);
                },
                getVehicleTypeColor: function() {
                    return $http.get('/api/sales/VehicleTypeColorJoin')
                },
                newCustomerVehicleUser: function(vID, cID) {
                    return $http.post('/api/crm/PostCustomerVehicleList/', [{
                        CustomerOwnerId: cID,
                        VehicleId: vID,
                        CurrentStatus: 1,
                        CreatedDate: new Date(),
                        CreatedUserId: currUser.UserId

                    }])
                },
                getAllTypePoints: function() {
                    return $http.get('/api/as/TypePoints');
                },
                getProbPoints: function(JobId, TypeId) {
                    return $http.get('/api/as/JobWACExts/Job/' + JobId + '/' + TypeId);
                },
                // ---------- added by sss 2017-08-14
                createEXT: function(data) {
                    return $http.post('/api/as/JobWACExts', data);
                },
                updateEXT: function(data) {
                    return $http.put('/api/as/JobWACExts', data);
                },
                getDataPKS: function(PoliceNumber) {
                    return $http.get('/api/as/PKS/GetByLicensePlate/' + PoliceNumber);
                },
                updatePKS: function(PoliceNumber, totalAkhir) {
                    totalAkhir = Math.ceil(totalAkhir) // di round up karena pernah ada totalnya koma, dan karena tagihan y round up hrs nya
                    return $http.get('/api/as/PKS/CekPKS/' + PoliceNumber + '/' + totalAkhir);
                },
                // added by sss on 2017-09-07
                bpCenterTrigger: function(data) {
                    return $http.post('/api/as/AppointmentRequestsBPCenter', [data]);
                },
                getListVehicleCPPK: function(id) {
                    return $http.get('/api/crm/GetListVehicleUserPhone/' + id);
                },

                CheckLastContactPerson: function(filter, flag) {
                    // api/as/CheckLastContactPerson/{filter}/{flag}
                    // flag 1 kirim vehicleid
                    // flag 2 kirim nopol

                    return $http.get('/api/as/CheckLastContactPerson/' + filter + '/' + flag);
                },


                getDiscountMGroupCustomer: function(data) {
                    console.log('data untuk discountMGroupCust', data)
                    var Nama = '';
                    var tlp1 = '';
                    var tlp2 = '';
                    var ktp = '';
                    var npwp = '';
                    if (data.CustomerTypeId == 3 || data.CustomerTypeId == 1) {
                        Nama = data.Name;
                        tlp1 = data.HandPhone1;
                        tlp2 = data.HandPhone2;
                        ktp = data.KTPKITAS;
                        npwp = data.Npwp;
                    } else {
                        Nama = data.Name;
                        // tlp1 = data.PICHp;
                        npwp = data.Npwp;
                    }
                    if (Nama == '' || Nama == undefined || Nama == null){
                        Nama = '-';
                    }
                    if (tlp1 == '' || tlp1 == undefined || tlp1 == null){
                        tlp1 = '-';
                    }
                    if (tlp2 == '' || tlp2 == undefined || tlp2 == null){
                        tlp2 = '-';
                    }
                    if (ktp == '' || ktp == undefined || ktp == null){
                        ktp = '-';
                    }
                    if (npwp == '' || npwp == undefined || npwp == null){
                        npwp = '-';
                    }
                    return $http.put('/api/crm/PutDiscountMGroupCustomer', [{
                        // Name: data.Name,
                        // Phone1: data.HandPhone1,
                        // Phone2: data.HandPhone2,
                        // KtpNo: data.KTPKITAS,
                        // NPWP: data.Npwp
                        Name: Nama,
                        Phone1: tlp1,
                        Phone2: tlp2,
                        KtpNo: ktp,
                        NPWP: npwp
                    }]);
                },
                createApprovalWarranty: function(tmpData) {
                    console.log("jobid", tmpData);
                    // for (var i = 0; i < mData.length; i++) {
                    // return $http.post('/api/as/ASubmited_ApprovalTOWASS_TR', tmpData);
                    return $http.post('/api/as/PostTaskListClaim', tmpData);
                    //         "NoClaim": "57812",
                    //         // "JobId": mData[i].JobId,
                    //         "JobTaskId":[]
                    //         "ApprovalCategoryId": 43,
                    //         // "ApproverId": null,
                    //         // "RequesterId": 1231,
                    //         "ApproverRoleId": null,
                    //         "RequestorRoleId": 1128,
                    //         "StatusApprovalId": 3,
                    //         "CategoryServices": 1, // kategori pekerjaan
                    //         "RequestReason": mDataIsi.Requestmessage,
                    //         "RequestDate": null, // tanggal sekarang
                    //         // "RejectReason": null
                    //         // ,
                    //         // "ApproveRejectDate": null
                    //     }]);
                    // }
                },
                CheckIsWarrantyAktif: function(JobId) {
                    console.log("data checkApproval", JobId);
                    return $http.get('/api/as/CheckIsWarrantyAktif/' + JobId);
                },
                checkIsWarranty: function(JobId) {
                    console.log("data checkApproval", JobId);
                    return $http.get('/api/as/IsWarranty/' + JobId);
                },
                checkApproval: function(data) {
                    console.log("data checkApproval", data);
                    return $http.put('/api/as/CheckingApproval/', [{
                        DPRequested: data,
                        ApprovalCategoryId: 39,
                        RoleId: currUser.RoleId
                    }]);
                },
                postApproval: function(data) {
                    // ApproverRoleId
                    console.log("postApproval", data);
                    return $http.post('/api/as/PostApprovalRequestDP/2', [{
                        JobId: data[0].JobId,
                        DPDefault: data[0].totalDpDefault,
                        DPRequested: data[0].totalDp,
                        ApprovalCategoryId: 39,
                        ApproverId: null,
                        RequesterId: null,
                        ApproverRoleId: data[0].ApproverRoleId,
                        RequestorRoleId: currUser.RoleId,
                        RequestReason: "Request Pengurangan DP",
                        RequestDate: new Date(),
                        StatusApprovalId: 3,
                        StatusCode: 1,
                        VehicleTypeId: data[0].VehicleTypeId
                    }]);
                },
                postApprovalDiscountTask: function(jobId, data, ApprovalCategoryId) {
                    console.log("dataaaaa", data);
                    return $http.post('/api/as/PostApprovalDiscountTask', [{
                        JobId: jobId,
                        JobTask: data,
                        ApprovalCategoryId: ApprovalCategoryId
                    }]);
                },
                postApprovalDiscountPart: function(jobId, data) {
                    console.log("dataaaaa", data);
                    return $http.post('/api/as/PostApprovalDiscountPart', [{
                        JobId: jobId,
                        JobParts: data
                    }]);
                },
                GateOutFromWO: function(Id, NoPol) {
                    return $http.put('/api/as/GateOutFrom/' + Id + '/' + NoPol + '/' + 12)
                },
                updateStatus17: function(id) {
                    return $http.put('/api/as/jobs/UpdateStatusJobs17/' + id);
                },
                updateStatusForWO: function(JobId, toStat) {
                    return $http.put('/api/as/UpdateJob/' + JobId + '/StatusTo/' + toStat);
                },
                updateStatusForWONew: function (JobId, toStat, flag) {
                    return $http.put('/api/as/UpdateJob/' + JobId + '/StatusTo/' + toStat + '/flag/'+ flag);
                },
                // =================================
                // Service Explanation
                // =================================
                GetDataServiceExplanation: function(JobID) {
                    var res = $http.get('/api/as/ServiceExplanations/getByJobId/' + JobID);
                    //console.log('res=>',res);
                    return res;

                },
                UpdateServiceExplanation: function(data) {
                    data.SuggestionDate = this.changeFormatDate(data.SuggestionDate);
                    data.FollowUpDate = this.changeFormatDate(data.FollowUpDate);
                    return $http.put('/api/as/ServiceExplanations/Update/' + data.JobId + '', [
                        [{
                            InsideCleaness: data.InsideCleaness,
                            OutsideCleaness: data.OutsideCleaness,
                            CompletenessVehicle: data.CompletenessVehicle,
                            UsedPart: data.UsedPart,
                            DriverCarpetPosition: data.DriverCarpetPosition,
                            IsJobResultExplainOk: data.IsJobResultExplainOk,
                            JobResultExplainReason: data.JobResultExplainReason,
                            IsSparePartExplainOk: data.IsSparePartExplainOk,
                            SparePartExplainReason: data.SparePartExplainReason,
                            IsConfirmPriceOk: data.IsConfirmPriceOk,
                            ConfirmPriceReason: data.ConfirmPriceReason,
                            IsJobSuggestExplainOk: data.IsJobSuggestExplainOk,
                            JobSuggestExplainReason: data.JobSuggestExplainReason,
                            IsConfirmVehicleCheckOk: data.IsConfirmVehicleCheckOk,
                            ConfirmVehicleCheckReason: data.ConfirmVehicleCheckReason,
                            OldPartsKeepPlace: data.OldPartKeepPlace,
                            TimeStartSE: data.TimeStartSE,
                            TimeFinishSE: data.TimeFinishSE
                        }],
                        [{
                            FollowUpDate: data.FollowUpDate,
                            PhoneType: data.phoneType,
                            PhoneNumber: data.PhoneNumber,
                            StatusFUTime: data.StatusFUTime
                        }],
                        [{
                            SuggestionDate: data.SuggestionDate,
                            SuggestionCatg: data.SuggestionCatg,
                            Suggestion: data.Suggestion
                        }]
                    ]);

                    // return $http.put('/api/as/Washings/', [{
                    //     Id: data.Id,
                    //     JobId: data.JobId,
                    //     TimeStart: data.TimeStart,
                    //     TimeFinish: data.TimeFinish,
                    //     TimeThru: data.TimeThru,
                    //     StatusWashing: data.StatusWashing
                    // }])
                },

                CheckStatusBilling: function(jobid) {
                    // api/as/ServiceExplanations/CheckStatusBilling/{jobId}

                    var res = $http.get('/api/as/ServiceExplanations/CheckStatusBilling/' + jobid);

                    return res;
                },

                GetCAddressCategory: function() {
                    var res = $http.get('/api/crm/GetCAddressCategory/');

                    // console.log('hasil=>',res);
                    //res.data.Result = null;
                    return res;
                },
                GetDEC: function(vin) {
                    return $http.get('/api/as/getDEC/' + vin);
                },
                CancelWOWalkin: function(data) {
                    if (data.PoliceNumber == null || data.PoliceNumber == undefined) {
                        data.PoliceNumber = data.LicensePlate;
                    }
                    return $http.put('/api/as/Gate/updateCancelWO', [{
                        PoliceNo: data.PoliceNumber,
                        nowTime: new Date(),
                        Id: data.Id
                    }])
                },
                createNewVehicleNonTAM: function(data) {
                    console.log("createNewVehicleNonTAM", data);
                    return $http.post('/api/crm/PostVehicleNonTAM/', [{
                        VIN: data.Vehicle.VIN,
                        LicensePlate: data.Vehicle.LicensePlate,
                        EngineNo: data.Vehicle.EngineNo,
                        AssemblyYear: data.Vehicle.AssemblyYear,
                        DECDate: data.Vehicle.DECDate,
                        STNKName: data.Vehicle.STNKName,
                        STNKAddress: data.Vehicle.STNKAddress,
                        KatashikiCode: data.Vehicle.KatashikiCode,
                        STNKDate: data.Vehicle.STNKDate,
                        GasTypeId: data.Vehicle.GasTypeId,
                        OutletId: currUser.OrgId,
                        ModelName: data.Vehicle.ModelName,
                        ModelType: data.Vehicle.ModelType,
                        ColorName: data.Vehicle.ColorName,
                        KatashikiCode: data.Vehicle.KatashikiCodeNonTAM
                    }]);
                },
                updateVehicleNonTAM: function(data) {
                    console.log("updateVehicleNonTAM", data);
                    return $http.put('/api/crm/PutVehicleNonTAM/', [{
                        VehicleId: data.Vehicle.VehicleId,
                        VIN: data.Vehicle.VIN,
                        LicensePlate: data.Vehicle.LicensePlate,
                        EngineNo: data.Vehicle.EngineNo,
                        AssemblyYear: data.Vehicle.AssemblyYear,
                        DECDate: data.Vehicle.DECDate,
                        KatashikiCode: data.Vehicle.KatashikiCode,
                        STNKName: data.Vehicle.STNKName,
                        STNKAddress: data.Vehicle.STNKAddress,
                        STNKDate: data.Vehicle.STNKDate,
                        GasTypeId: data.Vehicle.GasTypeId,
                        OutletId: currUser.OrgId,
                        ModelName: data.Vehicle.ModelName,
                        ModelType: data.Vehicle.ModelType,
                        ColorName: data.Vehicle.ColorName,
                        KatashikiCode: data.Vehicle.KatashikiCodeNonTAM
                    }]);
                },
                CustomerInsertTOCRMTAM: function(CID) {
                    return $http.get('/api/crm/GetInsertToCRMTAM/' + CID);
                },

                // Testing: function(date) {
                //     var b = new Date();
                //     console.log('aaa 2>', date);
                //     // console.log('aaa 3>', date.toISOString());
                //     console.log('bbb 1>', b);
                //     console.log('bbb 2>', b.toString());
                //     console.log('bbb 3>', b.toISOString());
                //     var a = new Date();
                //     var yearFirst = a.getFullYear();
                //     var monthFirst = a.getMonth() + 1;
                //     var dayFirst = a.getDate();
                //     var hour = a.getHours();
                //     var minutes = a.getMinutes();
                //     var seconds = a.getSeconds();
                //     var secondss = seconds < 10 ? '0' + seconds : seconds;

                //     var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst + 'T' + hour + ':' + minutes + ':' + secondss;

                //     return $http.put('/api/as/cobaGetDate/', [{
                //         Date1: date,
                //         Date2: firstDate
                //     }]);
                // }

                //,
                // delete: function(id) {
                //   return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
                // },
                updateSkipQueue: function(id) {
                    return $http.put('/api/as/queue/updateSkipQueue/' + id);
                },

                getDataWaitingChips: function() {
                    return $http.get('/api/as/GetChipforWaiting/1')
                },

                getDataWaitingOPL: function() {
                    return $http.get('/api/as/GetChipforOPL/1')
                },

                getPartsPerMaterialDiscount: function(partClassId, partCode) {
                    return $http.get('/api/as/MaterialRetailDiscount/GetDiscountPartsMaterial', {
                        params: {
                            PartsClassId: partClassId,
                            PartsCode: partCode
                        }
                    })
                },

                getCheckData: function(checkTypeId) {
                    var url = '/api/as/AfterSalesCheck/GetCheckData/?TypeId=' + checkTypeId;
                    var res = $http.get(url);
                    return res;
                },
                getVehicleTypeById: function(id) {
                    var url = '/api/crm/GetCVehicleTypeById/' + id
                    var res = $http.get(url);
                    return res;
                },
                getVehicleTypeColorById: function(id) {
                    var url = '/api/crm/GetMVehicleTypeColorById/' + id
                    var res = $http.get(url);
                    return res;
                },
                NewGetMVehicleTypeColorById: function(id) {
                    var url = '/api/crm/NewGetMVehicleTypeColorById/' + id
                    var res = $http.get(url);
                    return res;
                },
                getAvailableParts: function(item, jobid, qtyinput) {
                    // /api/as/jobs/getAvailableParts/{partid}
                    // var res = $http.get('/api/as/jobs/getPartsPriceAndDefaultETDETA/' + item);
                    if (typeof item == 'string') {
                        if (item.includes('&')) {
                           item = item.split("&")[0];
                        }
                    }
                    if (jobid === undefined || jobid == null) {
                        jobid = 0;
                    }
                    var res = $http.get('/api/as/jobs/getAvailableParts/' + item + '/1?JobId=' + jobid + '&Qty=' + qtyinput);
                    return res
                },
                sendNotif: function(data, recepient, param) {
                    // console.log("model", IdSA);
                    // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                    return $http.post('/api/as/SendNotificationForRole', [{
                        Message: data.Message,
                        RecepientId: recepient,
                        Param: param
                    }]);
                },
                BackToQueue: function(data, param, jobid) {
                    return $http.post('/api/as/queue/BackToQueue/'+ param + '/' + jobid , [{
                        CallTime: data.CallTime,
                        CounterId: data.CounterId,
                        EndTime: data.EndTime,
                        Id : data.Id,
                        LicensePlate : data.LicensePlate,
                        NumberOfCalls : data.NumberOfCalls,
                    }]);
                },
                getJobTaskProgress: function(JobId) {
                    return $http.get('/api/as/JobTaskProgressByJobId/' + JobId);
                },
                getWarrantyInfo: function(JobId) {
                    return $http.get('/api/as/GetWarrantyInfo/' + JobId + '/1');
                },
                getBPCenter: function() {
                    return $http.get('/api/as/getOutletBPCenter');
                },
                getCekCustomerList: function(data, type) {
                    var typeBaru = 0;
                    if (type == 3 || type == 1) {
                        typeBaru = 1;
                    } else {
                        typeBaru = 2;
                    }

                    // data.Customer.newBirthDate == "NaN/NaN/NaN"
                    console.log("Data Cek Customer ==>", data, typeBaru);
                    if (typeBaru == 1) {
                        return $http.put('/api/crm/CekCustomerList/' + typeBaru, [{
                            KTP: data.Customer.KTPKITAS,
                            // NPWP: data.Customer.Npwp,
                            // SIUP: data.Customer.SIUP,
                            CustomerName: data.Customer.Name,
                            BirthDate: data.Customer.newBirthDate == "NaN/NaN/NaN" ? '-' : data.Customer.newBirthDate,
                            Phone: data.Customer.HandPhone1
                        }]);
                    } else if (typeBaru == 2) {
                        return $http.put('/api/crm/CekCustomerList/' + typeBaru, [{
                            // KTP: data.Customer.KTPKITAS,
                            NPWP: data.Customer.Npwp,
                            SIUP: data.Customer.SIUP,
                            CustomerName: data.Customer.Name
                                // BirthDate: data.Customer.newBirthDate,
                                // Phone: data.Customer.HandPhone1
                        }]);
                    }
                },
                clearQueueCounters: function(NoPol) {
                    return $http.delete('/api/as/queue/clearQueueCounters/' + NoPol);
                },
                UpdateChangeNopol: function(id, oldPoliceNo, newPoliceNo, vid) {
                    if (id == null || id == undefined){
                        id = 0;
                    }
                    return $http.put('/api/as/UpdateQueueGateVehicle/' + id + '/'+ oldPoliceNo + '/' + newPoliceNo + '/' + vid);
                },
                CheckNopolQueueListSecurity: function(type, nopol, jobid) {
                    var res = $http.get('/api/as/CheckPoliceNoAvalailability/' + type + '/' + nopol + '/' + jobid);
                    return res
                },
                CheckVehicleStatusStock: function(vin) {
                    // api/crm/CheckVehicleStatusStock/{VehicleId}
                    var res = $http.get('/api/crm/CheckVehicleStatusStock/' + vin);
                    return res
                },
                
                CheckVINisOK: function(VIN, PoliceNo, Flag) {
                    var res = $http.put('/api/crm/CheckVINisOK/' + VIN + '/' + PoliceNo + '/' + Flag);
                    return res
                },
                GetStatusJobByPoliceNo: function(data) {
                    //api/as/Gates/CheckInProduction/4239
                    console.log("cek data", data);
                    var res = $http.get('/api/as/Jobs/GetStatusJobByPoliceNo/' + data.LicensePlate + '/' + data.Id + '/' + 2);
                    return res;
                },

                getStatWO: function (JobId, status) {
                    return $http.get('/api/as/Jobs/GetStatusJob/' + JobId + '/' + status);
                },

                CekStatusJobBilling: function(JobId, flag) {
                    // console.log("cek data", data);
                    var res = $http.get('/api/as/Billings/CekStatusJob/' + JobId + '/' + flag );
                    return res;
                },

                CekStatusPenguranganJob: function(JobId) {
                    // console.log("cek data", data);
                    var res = $http.get('/api/as/CekJobIdApprovalJobReduction/' + JobId );
                    return res;
                },
                CekStatusDiskonJob: function(JobId) {
                    // console.log("cek data", data);
                    var res = $http.get('/api/as/CekJobIdApprovalDiscount/' + JobId );
                    return res;
                },

                CheckApprovalDiscountTask: function(JobId) {
                    var res = $http.get('/api/as/CheckApprovalDiscountTask/' + JobId );
                    return res;
                },
                CheckApprovalDiscountPart: function(JobId) {
                    var res = $http.get('/api/as/CheckApprovalDiscountPart/' + JobId );
                    return res;
                },

                // ===================================================  ini factory pindahan dari menu lain ================================= start
                getDataTaskOpe: function(Key, Katashiki, catg, vehicleModelId) {
                    var res = $http.get('/api/as/TaskListsCode?KatashikiCode=' + Katashiki + '&Name=' + Key + '&TaskCategory=' + catg + '&VehicleModelId=' + vehicleModelId);
                    // var res=$http.get('/api/as/TaskLists?KatashikiCode=ASV50R-JETGKD&Name=&TaskCategory='+catg);
                    console.log('/api/as/TaskLists?KatashikiCode=' + Katashiki + '&Name=' + Key + '&TaskCategory=' + catg + '&VehicleModelId=' + vehicleModelId);
                    // console.log("resnya",res);
                    return res;
                },

                getDiscountCampaign: function(today, data, crm, IsGR) {
                    var KmDiscount = 0;
                    if (data.Km !== undefined && data.Km !== null) {
                        if (typeof data.Km === 'string') {
                            var tmpKm = angular.copy(data.Km);
                            if (tmpKm.includes(",")) {
                                tmpKm = tmpKm.split(',');
                                if (tmpKm.length > 1) {
                                    tmpKm = tmpKm.join('');
                                } else {
                                    tmpKm = tmpKm[0];
                                }
                            }
                            tmpKm = parseInt(tmpKm);
                            KmDiscount = tmpKm;
                        } else {
                            KmDiscount = data.Km;
                        }
                    }
                    console.log("getDiscountCampaign", today, data, crm);
                    // '/api/as/CampaignDiscount/Date/2017-08-07/KM/2100/AYear/2015/VechId/4642'
                    return $http.get('/api/as/CampaignDiscount/Date/' + today + '/KM/' + KmDiscount + '/AYear/' + crm.Vehicle.AssemblyYear + '/VechId/' + crm.Vehicle.Type.VehicleTypeId + '/IsGR/' + IsGR);
                },

                getDataRepairProses: function(NoPolice, NoWO, Employee, date) {
                    console.log("date", date);
                    var finalDate;
                    if (date != '-') {
                        var yyyy = date.getFullYear().toString();
                        var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based         
                        var dd = date.getDate().toString();
                        finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
                    } else {
                        finalDate = date;
                    };
                    // var res=$http.get('/api/as/jobs/WoList/FirstDate/'+dateNow+'/LastDate/'+dateNow+'');
                    // var res=$http.get('/api/as/jobs/WoList/FirstDate/'+dateNow+'/LastDate/'+dateNow+'/1');
                    var res = $http.get('/api/as/jobs/EstimationList/1/NoPolisi/' + NoPolice + '/idTeknisi/' + Employee + '/WoNo/' + NoWO + '/Date/' + finalDate);
                    // var res = $http.get('/api/as/jobs/EstimationList/1/NoPolisi/-/idTeknisi/-/WoNo/-/Date/-');
                    // console.log('resnya=>',res);
                    return res;
                },

                getJobSuggest: function(JobID) {
                    var res = $http.get('/api/as/JobSuggestion/' + JobID + '');
                    // console.log('resnya employee=>',res);
                    return res;
                },

                getVinFromCRM: function(vin) {
                    var res = $http.get('/api/crm/GetVehicleListById/' + vin + '');
                    console.log('Get Data VIN=>', res);
                    return res;
                },

                getFinalInspectionById: function(JobId) {
                    var res = $http.get('/api/as/JobInspections/job/' + JobId + '');
                    // console.log('resnya=>',res);
                    return res;
                },

                getDataTeknisi: function() {
                    // var res=$http.get('/api/as/EmployeeRoles/2');Mprofile_Employee_LastPosition
                    var res = $http.get('/api/as/Mprofile_Employee_LastPosition/TGR');
                    // var res = $http.get('/api/as/Mprofile_Employee/TGR');
                    // console.log('resnya employee=>',res);
                    return res;
                },

                getDamage: function() {
                    var catId = 2040;
                    var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId + '&Flag=1');
                    console.log('resnya pause=>', res);
                    return res;
                },

                getRelationship: function() {
                    return $http.get('/api/crm/GetCCustomerRelation/');
                },

                getSAAssignmentTemp: function(isGr) {
                    return $http.get('/api/as/queue/getSAAssignmentGRBP/' + isGr);
                    // return $http.get('/api/as/queue/getSAAssignmentTemp/' + isGr);
                },

                getCustomerVehicleList: function(value) {
                    var param1 = value.TID ? value.TID : '-';
                    if (value.TID == '' || value.TID == null || value.TID == undefined){
                        param1 = '-'
                    } else {
                        param1 = value.TID
                    }
                    var param2 = '-';
                    var param3 = '-';
                    var param4 = '-';
                    var param5 = '-';
                    var param6 = '-';
                    var param7 = '-';
                    var param8 = '-';
                    console.log("wew o wew",value);
                    switch (value.flag) {
                        case 1:
                            param2 = value.filterValue ? value.filterValue : '-';
                            if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                                param2 = '-'
                            } else {
                                param2 = value.filterValue
                            }
                            break;
                        case 2:
                            param3 = value.filterValue ? value.filterValue : '-';
                            if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                                param3 = '-'
                            } else {
                                param3 = value.filterValue
                            }
                            break;
                        case 3:
                            param4 = value.filterValue ? value.filterValue : '-';
                            if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                                param4 = '-'
                            } else {
                                param4 = value.filterValue
                            }
                            break;
                        case 4:
                            param5 = value.filterValue ? value.filterValue : '-';
                            if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                                param5 = '-'
                            } else {
                                param5 = value.filterValue
                            }
                            break;
                        case 5:
                            param6 = value.filterValue ? value.filterValue : '-';
                            if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                                param6 = '-'
                            } else {
                                param6 = value.filterValue
                            }
                            break;
                        case 6:
                            param7 = value.filterValue ? value.filterValue : '-';
                            if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                                param7 = '-'
                            } else {
                                param7 = value.filterValue
                            }
                            break;
                        case 7:
                            // param8 = value.filterValue ? value.filterValue : '-';
                            if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                                param8 = '-'
                            } else {
                                param8 = angular.copy(value.filterValue.replace(/\./g, "z")) 
                                param8 = angular.copy(param8.replace(/\-/g, "x"))

                            }

    
                    };
    
                    return $http.get('/api/crm/GetCustomerListFilter/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7 + '/' + param8);
                },

                getCustomerVehicleList_V2: function(value) {
                    var param1 = value.TID ? value.TID : '-';
                    if (value.TID == '' || value.TID == null || value.TID == undefined){
                        param1 = '-'
                    } else {
                        param1 = value.TID
                    }
                    var param2 = '-';
                    var param3 = '-';
                    var param4 = '-';
                    var param5 = '-';
                    var param6 = '-';
                    var param7 = '-';
                    var param8 = '-';
                    console.log("wew o wew",value);
                    switch (value.flag) {
                        case 1:
                            param2 = value.filterValue ? value.filterValue : '-';
                            if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                                param2 = '-'
                            } else {
                                param2 = value.filterValue
                            }
                            break;
                        case 2:
                            param3 = value.filterValue ? value.filterValue : '-';
                            if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                                param3 = '-'
                            } else {
                                param3 = value.filterValue
                            }
                            break;
                        case 3:
                            param4 = value.filterValue ? value.filterValue : '-';
                            if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                                param4 = '-'
                            } else {
                                param4 = value.filterValue
                            }
                            break;
                        case 4:
                            param5 = value.filterValue ? value.filterValue : '-';
                            if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                                param5 = '-'
                            } else {
                                param5 = value.filterValue
                            }
                            break;
                        case 5:
                            param6 = value.filterValue ? value.filterValue : '-';
                            if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                                param6 = '-'
                            } else {
                                param6 = value.filterValue
                            }
                            break;
                        case 6:
                            param7 = value.filterValue ? value.filterValue : '-';
                            if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                                param7 = '-'
                            } else {
                                param7 = value.filterValue
                            }
                            break;
                        case 7:
                            // param8 = value.filterValue ? value.filterValue : '-';
                            if (value.filterValue == '' || value.filterValue == null || value.filterValue == undefined){
                                param8 = '-'
                            } else {
                                param8 = angular.copy(value.filterValue.replace(/\./g, "z")) 
                                param8 = angular.copy(param8.replace(/\-/g, "x"))

                            }

    
                    };
    
                    return $http.get('/api/crm/GetCustomerListFilter_V2/' + param1 + '/' + param2 + '/' + param3 + '/' + param4 + '/' + param5 + '/' + param6 + '/' + param7 + '/' + param8);
                },

                getPICDepartment: function() {
                    var res = $http.get('/api/as/CustomerPosition');
                    return res
                },

                updateStatusQueue: function(QueueId, updateMode) {
                    // Update Mode Index :
                    // 0 : Available For Call
                    // 1 : On Call Process
                    // 2 : Start Reception
    
                    return $http.put('/api/as/queue/updateQueue/' + QueueId + '/' + updateMode);
                },

                insertSAAssignmentTemp: function(NoPol, JobId, isGr) {
                    return $http.post('/api/as/queue/insertSAAssignmentTemp/' + NoPol + '/' + JobId + '/' + isGr);
                },

                insertSAAssignmentTempWithQid: function(NoPol, JobId, isGr, Qid) {
                    // api/as/queue/insertSAAssignmentTemp/{NoPol}/{JobId}/{IsGr}/{QId}
                    return $http.post('/api/as/queue/insertSAAssignmentTemp/' + NoPol + '/' + JobId + '/' + isGr + '/' + Qid);
                },

                getVehicleList: function(param1, param2) {
                    return $http.get('/api/crm/GetVehicleListSpecific/' + param1 + '/' + param2);
                },

                NewGetVehicleListSpecific: function(param1, param2, from_clickbutton) {
                    if (from_clickbutton == 1) {
                        return $http.get('/api/crm/NewGetVehicleListSpecific/' + param1 + '/' + param2);
                    } else {
                        return $http.get('/api/crm/NewGetVehicleListSpecificNotSync/' + param1 + '/' + param2);
                    }
                },

                NewGetVehicleListSpecificNotSync: function(param1, param2) {
                    return $http.get('/api/crm/NewGetVehicleListSpecificNotSync/' + param1 + '/' + param2);
                },

                getDetailCustomer: function(arr) {
                    var res = $http.get('/api/as/MRS/' + arr);
                    // api/as/MRS/{MrsId}/
                    return res;
                },

                createNewCustomerList: function(data) {
                    var no_hp = null
                    if (data.Customer.CustomerTypeId != 1 && data.Customer.CustomerTypeId != 3){
                        no_hp = angular.copy(data.Customer.PICHp)
                    } else {
                        no_hp = angular.copy(data.Customer.HandPhone1)
                    }
                    return $http.post('/api/crm/PostCustomerList/', [{
                        // ToyotaId: data.Customer.ToyotaId, //Masi Pakai Dummy Math.Random
                        OutletId: currUser.OrgId,
                        Npwp: data.Customer.Npwp,
                        CustomerTypeId: data.Customer.CustomerTypeId,
                        FleetId: 1, //Foreign Key, Masih Dummy
                        StatusCode: 1, //Status Create New
                            //ToyotaIdRequest: data.Customer.ToyotaIDFlag
                        CustomerName: this.checkIsCharacter(data.Customer.Name),
                        KTP: data.Customer.KTPKITAS,
                        NoHandphone: no_hp,
                        NPWP: data.Customer.Npwp,
                        AFCO: data.Customer.AFCOIdFlag,

                    }]);
                },

                createNewCustomerListPersonal: function(id, data) {
                    return $http.post('/api/crm/PostCustomerListPersonal/', [{
                        CustomerId: id,
                        // CustomerName: data.Customer.Name,
                        CustomerName: this.checkIsCharacter(data.Customer.Name),
                        FrontTitle: data.Customer.FrontTitle,
                        EndTitle: data.Customer.EndTitle,
                        KTPKITAS: data.Customer.KTPKITAS,
                        // BirthPlace: 'Dummy', //Not Used in WOBP
                        BirthDate: data.Customer.newBirthDate,
                        Handphone1: data.Customer.HandPhone1,
                        Handphone2: data.Customer.HandPhone2,
                        StatusCode: 1
                    }]);
                },
                createNewCustomerListInstitusi: function(id, data) {
                    return $http.post('/api/crm/PostCustomerListInstitution/', [{
                        InstitutionId: 0,
                        CustomerId: id,
                        // Name: data.Customer.Name,
                        Name: this.checkIsCharacter(data.Customer.Name),
                        SIUP: data.Customer.SIUP,
                        // PICName: data.Customer.PICName,
                        PICName: this.checkIsCharacter(data.Customer.PICName),
                        PICHp: data.Customer.PICHp,
                        PICEmail: data.Customer.PICEmail,
                        Npwp: data.Customer.Npwp,
                        NPWP: data.Customer.Npwp,
                        // PICDOB: data.Customer.PICName,
                        // PICGenderId: data.Customer.PICName,
                        // PICAddress: data.Customer.PICName,
                        // PICVillageId: data.Customer.PICName,
                        // PICRT: data.Customer.PICName,
                        // PICRW: data.Customer.PICName,
                        // SectorBusinessId: data.Customer.PICName,
                        // PICPositionId: data.Customer.PICName,
                        PICDepartmentId: data.Customer.PICDepartmentId,
                        // StatusCode: data.Customer.PICName,
                    }]);
    
                },
                createNewCustomerListAddress: function(id, data) {
                    _.map(data, function(val) {
                        val.MainAddress =  val.MainAddress == true ? 1 : 0;
                    });
                    return $http.post('/api/crm/PostCCustomerAddress/', data);
                },

                updateCustomerList: function(id, cTypeId, data) {
                    var d_toyotaid = angular.copy(data.Customer.ToyotaId)
                    if (d_toyotaid == undefined || d_toyotaid == null || d_toyotaid == ''){
                        d_toyotaid = '-'
                    }
                    return $http.put('/api/crm/PutCustomerList/', [{
                        CustomerId: id,
                        // OutletId: 655,
                        ToyotaId: d_toyotaid,
                        Npwp: data.Customer.Npwp,
                        CustomerTypeId: cTypeId,
                        // FleetId: 1,
                        StatusCode: 1,
                        Afco: data.Customer.AFCOIdFlag
                    }]);
                },

                updateCustomerListPersonal: function(Pid, Cid, data) {
                    // return $http.put('/api/crm/PutCListPersonal', [{
                    return $http.put('/api/crm/PutCListPersonalNew', [{
                        PersonalId: Pid,
                        CustomerId: Cid,
                        FrontTitle: data.Customer.FrontTitle,
                        EndTitle: data.Customer.EndTitle,
                        // CustomerName: data.Customer.Name,
                        CustomerName: this.checkIsCharacter(data.Customer.Name),
                        KTPKITAS: data.Customer.KTPKITAS,
                        // BirthPlace: 'Dummy', //Not Used in WOBP
                        BirthDate: data.Customer.newBirthDate,
                        Handphone1: data.Customer.HandPhone1,
                        Handphone2: data.Customer.HandPhone2,
                        StatusCode: 1
    
                    }]);
                },

                updateCustomerListAddress: function(Aid, Cid, data) {
                    _.map(data, function(val) {
                        val.MainAddress = val.MainAddress == true ? 1 : 0;
                    });
                    console.log("updateCustomerListAddress updateCustomerListAddress", data);
                    return $http.put('/api/crm/PutCCustomerAddress/', data);
                },

                updateCustomerListInstitusi: function(Iid, Cid, data) {
                    console.log("data di factory", data);
                    // return $http.put('/api/crm/PutCListInstitution', [{
                    return $http.put('/api/crm/PutCListInstitutionNew', [{
                        InstitutionId: Iid,
                        CustomerId: Cid,
                        // Name: data.Customer.Name,
                        Name: this.checkIsCharacter(data.Customer.Name),
                        SIUP: data.Customer.SIUP,
                        // PICName: data.Customer.PICName,
                        PICName: this.checkIsCharacter(data.Customer.PICName),
                        PICHp: data.Customer.PICHp,
                        PICEmail: data.Customer.PICEmail,
                        // Npwp: data.Customer.Npwp,
                        NPWP: data.Customer.Npwp,
                        // PICDOB: data.Customer.PICName,
                        // PICGenderId: data.Customer.PICName,
                        // PICAddress: data.Customer.PICName,
                        // PICVillageId: data.Customer.PICName,
                        // PICRT: data.Customer.PICName,
                        // PICRW: data.Customer.PICName,
                        // SectorBusinessId: data.Customer.PICName,
                        // PICPositionId: data.Customer.PICName,
                        PICDepartmentId: data.Customer.PICDepartmentId,
    
                    }]);
                },

                createOrUpdateCustomerListPersonal: function (Pid, Cid, data){
                    if (Pid == null){
                        // ni blm pernah punya personalid jalanin post
                        var id = Cid
                        return $http.post('/api/crm/PostCustomerListPersonal/', [{
                            CustomerId: id,
                            // CustomerName: data.Customer.Name,
                            CustomerName: this.checkIsCharacter(data.Customer.Name),
                            FrontTitle: data.Customer.FrontTitle,
                            EndTitle: data.Customer.EndTitle,
                            KTPKITAS: data.Customer.KTPKITAS,
                            // BirthPlace: 'Dummy', //Not Used in WOBP
                            BirthDate: data.Customer.newBirthDate,
                            Handphone1: data.Customer.HandPhone1,
                            Handphone2: data.Customer.HandPhone2,
                            StatusCode: 1
                        }]);
                    } else {
                        // ini punya personalid jalanin update
                        // return $http.put('/api/crm/PutCListPersonal', [{
                        return $http.put('/api/crm/PutCListPersonalNew', [{
                            PersonalId: Pid,
                            CustomerId: Cid,
                            FrontTitle: data.Customer.FrontTitle,
                            EndTitle: data.Customer.EndTitle,
                            // CustomerName: data.Customer.Name,
                            CustomerName: this.checkIsCharacter(data.Customer.Name),
                            KTPKITAS: data.Customer.KTPKITAS,
                            // BirthPlace: 'Dummy', //Not Used in WOBP
                            BirthDate: data.Customer.newBirthDate,
                            Handphone1: data.Customer.HandPhone1,
                            Handphone2: data.Customer.HandPhone2,
                            StatusCode: 1
        
                        }]);
                    }
    
                },
                createOrUpdateCustomerListInstitusi: function (Iid, Cid, data){
                    if (Iid == null){
                        // ni blm pernah punya institusiid jalanin post
                        var id = Cid
                        return $http.post('/api/crm/PostCustomerListInstitution/', [{
                            InstitutionId: 0,
                            CustomerId: id,
                            // Name: data.Customer.Name,
                            Name: this.checkIsCharacter(data.Customer.Name),
                            SIUP: data.Customer.SIUP,
                            // PICName: data.Customer.PICName,
                            PICName: this.checkIsCharacter(data.Customer.PICName),
                            PICHp: data.Customer.PICHp,
                            PICEmail: data.Customer.PICEmail,
                            Npwp: data.Customer.Npwp,
                            NPWP: data.Customer.Npwp,
                            // PICDOB: data.Customer.PICName,
                            // PICGenderId: data.Customer.PICName,
                            // PICAddress: data.Customer.PICName,
                            // PICVillageId: data.Customer.PICName,
                            // PICRT: data.Customer.PICName,
                            // PICRW: data.Customer.PICName,
                            // SectorBusinessId: data.Customer.PICName,
                            // PICPositionId: data.Customer.PICName,
                            PICDepartmentId: data.Customer.PICDepartmentId,
                            // StatusCode: data.Customer.PICName,
                        }]);
                    } else {
                        // ini punya institusiid jalanin update
                        // return $http.put('/api/crm/PutCListInstitution', [{
                        return $http.put('/api/crm/PutCListInstitutionNew', [{
                            InstitutionId: Iid,
                            CustomerId: Cid,
                            // Name: data.Customer.Name,
                            Name: this.checkIsCharacter(data.Customer.Name),
                            SIUP: data.Customer.SIUP,
                            // PICName: data.Customer.PICName,
                            PICName: this.checkIsCharacter(data.Customer.PICName),
                            PICHp: data.Customer.PICHp,
                            PICEmail: data.Customer.PICEmail,
                            // Npwp: data.Customer.Npwp,
                            NPWP: data.Customer.Npwp,
                            // PICDOB: data.Customer.PICName,
                            // PICGenderId: data.Customer.PICName,
                            // PICAddress: data.Customer.PICName,
                            // PICVillageId: data.Customer.PICName,
                            // PICRT: data.Customer.PICName,
                            // PICRW: data.Customer.PICName,
                            // SectorBusinessId: data.Customer.PICName,
                            // PICPositionId: data.Customer.PICName,
                            PICDepartmentId: data.Customer.PICDepartmentId,
        
                        }]);
                    }
    
                },


                // IsCuustomerInstitutionExists: function (Iid, Cid, data){
                //     // api/crm/IsCuustomerInstitutionExists

                //     return $http.put('/api/crm/IsCuustomerInstitutionExists', [{
                //         InstitutionId: 0,
                //         CustomerId: 0,
                //         Name: this.checkIsCharacter(data.Customer.Name),
                //         SIUP: data.Customer.SIUP,
                //         PICName: this.checkIsCharacter(data.Customer.PICName),
                //         PICHp: data.Customer.PICHp,
                //         PICEmail: data.Customer.PICEmail,
                //         NPWP: data.Customer.Npwp,
                //         PICDepartmentId: data.Customer.PICDepartmentId,

                //     }]);

                // },

                // IsCuustomerPersonalExists: function (Pid, Cid, data){
                //     // api/crm/IsCuustomerPersonalExists

                //     return $http.put('/api/crm/IsCuustomerPersonalExists', [{
                //             PersonalId: 0,
                //             CustomerId: 0,
                //             FrontTitle: data.Customer.FrontTitle,
                //             EndTitle: data.Customer.EndTitle,
                //             CustomerName: this.checkIsCharacter(data.Customer.Name),
                //             KTPKITAS: data.Customer.KTPKITAS,
                //             BirthDate: data.Customer.newBirthDate,
                //             Handphone1: data.Customer.HandPhone1,
                //             Handphone2: data.Customer.HandPhone2,
                //             StatusCode: 1
                //         }]);

                // },

                IsCuustomerExists: function (Pid, Cid, data, custtype){
                    var d_toyotaid = angular.copy(data.Customer.ToyotaId)
                    if (d_toyotaid == undefined || d_toyotaid == null || d_toyotaid == ''){
                        d_toyotaid = '-'
                    }

                    if (custtype == 3) {
                        // personal
                        return $http.put('/api/crm/IsCuustomerPersonalExists', [{
                            PersonalId: 0,
                            CustomerId: Cid,
                            FrontTitle: data.Customer.FrontTitle,
                            EndTitle: data.Customer.EndTitle,
                            CustomerName: this.checkIsCharacter(data.Customer.Name),
                            KTPKITAS: data.Customer.KTPKITAS,
                            BirthDate: data.Customer.newBirthDate,
                            Handphone1: data.Customer.HandPhone1,
                            Handphone2: data.Customer.HandPhone2,
                            StatusCode: 1,
                            ToyotaId: d_toyotaid,
                        }]);

                    } else {
                        // institusi
                        return $http.put('/api/crm/IsCuustomerInstitutionExists', [{
                            InstitutionId: 0,
                            CustomerId: Cid,
                            Name: this.checkIsCharacter(data.Customer.Name),
                            SIUP: data.Customer.SIUP,
                            PICName: this.checkIsCharacter(data.Customer.PICName),
                            PICHp: data.Customer.PICHp,
                            PICEmail: data.Customer.PICEmail,
                            NPWP: data.Customer.Npwp,
                            PICDepartmentId: data.Customer.PICDepartmentId,
                            ToyotaId: d_toyotaid,
        
                        }]);
                    }


                },



                deleteCustomerListAddress: function(id) {
                    console.log('fdg', id)
                    // return $http.delete('/api/crm/DeleteCCustomerAddress/', { data: id, headers: { 'Content-Type': 'application/json' } });
                    return $http.put('/api/crm/DeleteCCustomerAddress/', id);
                },

                getCustAddress: function(id) {
                    return $http.get('/api/crm/GetCCustomerAddress/' + id);
                },

                getMLocationProvince: function() {
                    return $http.get('/api/sales/MLocationProvince');
                },

                getMLocationCityRegency: function(id) {
                    return $http.get('/api/sales/MLocationCityRegency?start=1&limit=100&filterData=ProvinceId|' + id);
                },

                getMLocationKecamatan: function(id) {
                    return $http.get('/api/sales/MLocationKecamatan?start=1&limit=100&filterData=CityRegencyId|' + id);
                },

                getMLocationKelurahan: function(id) {
                    return $http.get('/api/sales/MLocationKelurahan?start=1&limit=100&filterData=DistrictId|' + id)
                },

                createNewVehicleList: function(data) {
                    console.log('data createNewVehicleList', data);
                    // return $http.post('/api/crm/PostVehicle/' + data.Vehicle.Color.ColorId + '/' + data.Vehicle.Type.VehicleTypeId, [{
                    return $http.post('/api/crm/PostVehicleSP/' + data.Vehicle.Color.ColorId + '/' + data.Vehicle.Type.VehicleTypeId, [{
                        VIN: data.Vehicle.VIN,
                        LicensePlate: data.Vehicle.LicensePlate,
                        EngineNo: data.Vehicle.EngineNo,
                        AssemblyYear: data.Vehicle.AssemblyYear,
                        DECDate: data.Vehicle.DECDate,
                        // VehicleTypeColorId: data.Vehicle.ColorId,
                        STNKName: data.Vehicle.STNKName,
                        STNKAddress: data.Vehicle.STNKAddress,
                        STNKDate: data.Vehicle.STNKDate,
                        GasTypeId: data.Vehicle.GasTypeId,
                        isNonTAM: data.Vehicle.isNonTAM,
                        OutletId: currUser.OrgId
                            // "StatusCode": 1,
    
                    }]);
                },

                updateVehicleList: function(data) {
                    var DecDate = new Date(data.Vehicle.DECDate);
                    var STNKDate = null
                    if (data.Vehicle.STNKDate != null && data.Vehicle.STNKDate != undefined) {
                        STNKDate = new Date(data.Vehicle.STNKDate);
                    }
    
    
                    // console.log('data updateVehicleList', data, data.Vehicle.DECDate.getFullYear(), data.Vehicle.DECDate.getMonth());
                    var tmpDecDate = DecDate.getFullYear() + '-' + (DecDate.getMonth() + 1) + '-' + DecDate.getDay();
                    // var tmpSTNKDate = STNKDate.getFullYear() + '-' + (STNKDate.getMonth() + 1) + '-' + STNKDate.getDate();
                    var finalDate1
                    var yyyy = DecDate.getFullYear().toString();
                    var mm = (DecDate.getMonth() + 1).toString(); // getMonth() is zero-based
                    var dd = DecDate.getDate().toString();
                    finalDate1 = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
    
                    var finalDate2 = null
                    if (STNKDate != null && STNKDate != undefined) {
                        var yyyy1 = STNKDate.getFullYear().toString();
                        var mm1 = (STNKDate.getMonth() + 1).toString(); // getMonth() is zero-based
                        var dd1 = STNKDate.getDate().toString();
                        finalDate2 = yyyy1 + '-' + (mm1[1] ? mm1 : "0" + mm1[0]) + '-' + (dd1[1] ? dd1 : "0" + dd1[0]);
                    }
    
                    console.log('generate date1', finalDate1);
                    console.log('generate date2', finalDate2);
                    return $http.put('/api/crm/PutVehicle/' + data.Vehicle.Color.ColorId + '/' + data.Vehicle.Type.VehicleTypeId, [{
                        VehicleId: data.Vehicle.VehicleId,
                        VIN: data.Vehicle.VIN,
                        LicensePlate: data.Vehicle.LicensePlate,
                        EngineNo: data.Vehicle.EngineNo,
                        AssemblyYear: data.Vehicle.AssemblyYear,
                        DECDate: finalDate1,
                        // DECDate: data.Vehicle.DECDate,
                        // Added by Fyberz =========
                        Insurance: data.Vehicle.Insurance,
                        SPK: data.Vehicle.SPK,
                        // =========================
                        // VehicleTypeColorId: data.Vehicle.ColorId,
                        STNKName: data.Vehicle.STNKName,
                        STNKAddress: data.Vehicle.STNKAddress,
                        STNKDate: finalDate2,
                        SourceDataId: data.Vehicle.SourceDataId,
                        // STNKDate: data.Vehicle.STNKDate,
                        GasTypeId: data.Vehicle.GasTypeId,
                        isNonTAM: data.Vehicle.isNonTAM,
                        OutletId: currUser.OrgId
                            // "StatusCode": 1,
    
                    }]);
                },

                createNewUserList: function(data, CVId) {
                    console.log('createNewUserList >>>', data, CVId);
                    return $http.post('/api/crm/PostVehicleUser/', [{
                        CustomerVehicleId: CVId,
                        // Name: data.name,
                        Name: this.checkIsCharacter(data.name),
                        RelationId: data.Relationship,
                        Phone1: data.phoneNumber1,
                        Phone2: data.phoneNumber2,
                        Status: 1,
                        StatusCode: 1
                    }])
                },

                updateUserList: function(data, CVId) {
                    console.log('updateUserList >>>', data, CVId);
                    return $http.put('/api/crm/PutVehicleUser/', [{
                        VehicleUserId: data.VehicleUserId,
                        CustomerVehicleId: CVId,
                        // Name: data.name,
                        Name: this.checkIsCharacter(data.name),
                        RelationId: data.Relationship,
                        Phone1: data.phoneNumber1,
                        Phone2: data.phoneNumber2,
                        Status: 1,
                        StatusCode: 1
                    }])
                },

                getVehicleMList: function() {
                    return $http.get('/api/crm/GetCVehicleModel/');
                },

                postJobWAC: function(data, jobidSimpan) {
                    data.forEach(function(val) {
                        val.moneyAmount = (typeof val.moneyAmount != 'undefined' || val.moneyAmount != null ? val.moneyAmount : 0); //Billy
                    });
                    if (jobidSimpan != undefined && jobidSimpan != null){
                        data[0].JobId = jobidSimpan
                    }
                    return $http.post('/api/as/JobWACs', data);
                },

                getDataWac: function (JobId) {
                    // console.log("JbId",JbId);
                    var res = $http.get('/api/as/JobWACs/Job/' + JobId);
                    // console.log('resnya=>',res);
                    return res;
                },

                dropSAAssignmentTemp: function(NoPol) {
                    return $http.delete('/api/as/queue/dropSAAssignmentTemp/' + NoPol);
                },

                getListStall: function(){
                    var res = $http.get('/api/as/Boards/Stall/'+1);
                    return res
                },

                getDataVehiclePSFU: function(key) {
                    var res = $http.get('/api/crm/GetVehicleListById/' + key);
                    return res;
                },

                sendToCRMHistory: function(data) {
                    // console.log("model", IdSA);
                    // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                    return $http.post('/api/crm/InsertCHistoryDataChanged/', [{
                        CustomerId : data.CustomerId,
                        ChangeTypeId : data.ChangeTypeId,
                        Modul : data.Modul,
                        Attribute : data.Attribute,
                        NewValue : data.NewValue,
                        OldValue : data.OldValue
                    }]);
                },

                getDataTask: function(Key, Katashiki, catg, vehicleModelId) {
                    var res = $http.get('/api/as/TaskLists?KatashikiCode=' + Katashiki + '&Name=' + Key + '&TaskCategory=' + catg + '&VehicleModelId=' + vehicleModelId);
                    // var res=$http.get('/api/as/TaskLists?KatashikiCode=ASV50R-JETGKD&Name='+Key+'&TaskCategory='+catg);
                    console.log('/api/as/TaskLists?KatashikiCode=' + Katashiki + '&Name=' + Key + '&TaskCategory=' + catg + '&VehicleModelId=' + vehicleModelId);
                    // console.log("resnya",res);
                    return res;
                },

                getPayment: function() {
                    var catId = 1008;
                    var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                    console.log('resnya pause=>', res);
                    return res;
                },

                getWoCategory: function() {
                    var catId = 1007;
                    var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                    console.log('resnya pause=>', res);
                    return res;
                },

                getTaskCategory: function() {
                    var catId = 1010;
                    var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                    console.log('resnya pause=>', res);
                    return res;
                },

                getUnitMeasurement: function() {
                    var catId = 1;
                    var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                    console.log('resnya pause=>', res);
                    return res;
                },

                getDataPartsByTaskId: function(key) {
                    var res = $http.get('/api/as/TaskLists/Parts?TaskId=' + key);
                    return res
                },

                getDataParts: function(Key, servicetype, type) {
                    console.log("keyyyyyy", Key);
                    var res = $http.get('/api/as/StockAdjustment/GetStockAdjustmentDetailFromMaterial?PartsCode=' + Key + '&ServiceTypeId=' + servicetype + '&PartsClassId1=' + type);
                    // console.log("/api/as/TaskLists?KatashikiCode="+Katashiki+"&Name="+Key);
                    // console.log("resnya",res);
                    return res;
                },

                getDataTaskListByKatashiki: function(key) {
                    var res = $http.get('/api/as/TaskLists/TasklistSA?KatashikiCode=' + key);
                    return res
                },

                getAvailablePartsAppointment: function(item, jobid, qtyinput) {
                    // /api/as/jobs/getAvailableParts/{partid}
                    // var res = $http.get('/api/as/jobs/getPartsPriceAndDefaultETDETA/' + item);
                    if (typeof item == 'string') {
                        if (item.includes('&')) {
                            item = item.split("&")[0];
                        }
                    }
                        if(jobid === undefined || jobid == null){
                        jobid = 0 ;
                    }
                    var res = $http.get('/api/as/jobs/getAvailablePartsAppointment/' + item + '/1?JobId='+jobid + '&Qty='+qtyinput);
                    return res
                },

                getDataEstimasi: function(filter) {
                    // ====== yang ini service by filter nopolisi tapi tetep ajah kosong 
                    // var res=$http.get('/api/as/jobs/EstimationList/1/NoPolisi/'+filter);
                    // ====== yang ini service dengan filter vehicle id =============
                    // var res = $http.get('/api/as/jobs/getEstimation/' + filter);
                    // ====== yang ini service dari get data estimasi di menu estimasi
                    var res = $http.get('/api/as/jobs/getListEstimationbyPoliceNumberRev/PoliceNumber/' + filter + '?isGR=1');
                    console.log('resnya pause=>', res);
                    return res;
                },

                updatePrediagnose: function(mData) {
                    return $http.put('/api/as/PutPrediagnoses', [{
                        PrediagnoseId: mData.PrediagnoseId,
                        JobId: mData.JobId,
                        FirstIndication: mData.FirstIndication,
                        FrequencyIncident: mData.FrequencyIncident,
                        Mil: mData.Mil,
                        ConditionIndication: mData.ConditionIndication,
                        MachineCondition: mData.MachineCondition,
                        MachineConditionTxt: mData.MachineConditionTxt,
                        PositionShiftLever: mData.PositionShiftLever,
                        PositionShiftLeverTxt: mData.PositionShiftLeverTxt,
                        VehicleSpeed: mData.VehicleSpeed,
                        RPM: mData.RPM,
                        VehicleLoads: mData.VehicleLoads,
                        PassengerNumber: mData.PassengerNumber,
                        ClassificationProblemR: mData.ClassificationProblemR,
                        ClassificationProblemRTxt: mData.ClassificationProblemRTxt,
                        Traffic: mData.Traffic,
                        TrafficTxt: mData.TrafficTxt,
                        WeatherConditions: mData.WeatherConditions,
                        WeatherConditionsTxt: mData.WeatherConditionsTxt,
                        CustomerFinalConfirmation: mData.CustomerFinalConfirmation,
                        ClassificationProblemM: mData.ClassificationProblemM,
                        ClassificationProblemMTxt: mData.ClassificationProblemMTxt,
                        BlowerSpeed: mData.BlowerSpeed,
                        TempSetting: mData.TempSetting,
    
                        SittingPosition: mData.SittingPosition,
                        TechnicalSupport: mData.TechnicalSupport,
                        PreDiagnoseTxt: mData.PreDiagnoseTxt,
                        JobDetail: mData.JobDetail,
    
                        notes: mData.notes,
                        IsNeedTest: mData.IsNeedTest,
                        FinalConfirmation: mData.FinalConfirmation,
                        CustomerConfirmation: mData.CustomerConfirmation,
                        LastModifiedUserId: mData.LastModifiedUserId,
                        LastModifiedDate: mData.LastModifiedDate,
                        // ComplaintCatId : mData.ComplaintCatId,
                        // DetailJSON : mData.DetailJSON,
                        ScheduledTime: mData.ScheduledTime,
                        StallId: mData.StallId,
                        OutletId: mData.OutletId
                    }]);
                },

                createPreadiagnose: function(mData) {
                    // var tmpItemDate = angular.copy(mData.FirstIndication);
                    // console.log("changeFormatDate item",tmpItemDate);
                    // tmpItemDate = new Date(tmpItemDate);
                    // console.log("tmpItemDate",tmpItemDate);
                    // var finalDate = '';
                    // var yyyy = tmpItemDate.getFullYear().toString();
                    // var mm = (tmpItemDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                    // var dd = (tmpItemDate.getDate()).toString();
                    
                    // // finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                    // finalDate = yyyy + '-' + mm + '-' + dd;
                    finalDate = this.changeFormatDate(mData.FirstIndication);
                    console.log("finalDate",finalDate)
                    console.log("tahu >>", mData);
                    return $http.post('/api/as/PostPrediagnoses', [{
                        JobId: mData.JobId,
                        FirstIndication: finalDate,
                        FrequencyIncident: mData.FrequencyIncident,
                        Mil: mData.Mil,
                        ConditionIndication: mData.ConditionIndication,
                        MachineCondition: mData.MachineCondition,
                        MachineConditionTxt: mData.MachineConditionTxt,
                        PositionShiftLever: mData.PositionShiftLever,
                        PositionShiftLeverTxt: mData.PositionShiftLeverTxt,
                        VehicleSpeed: mData.VehicleSpeed,
                        RPM: mData.RPM,
                        VehicleLoads: mData.VehicleLoads,
                        PassengerNumber: mData.PassengerNumber,
                        ClassificationProblemR: mData.ClassificationProblemR,
                        ClassificationProblemRTxt: mData.ClassificationProblemRTxt,
                        Traffic: mData.Traffic,
                        TrafficTxt: mData.TrafficTxt,
                        WeatherConditions: mData.WeatherConditions,
                        WeatherConditionsTxt: mData.WeatherConditionsTxt,
                        CustomerFinalConfirmation: mData.CustomerFinalConfirmation,
                        ClassificationProblemM: mData.ClassificationProblemM,
                        ClassificationProblemMTxt: mData.ClassificationProblemMTxt,
                        BlowerSpeed: mData.BlowerSpeed,
                        TempSetting: mData.TempSetting,
    
                        SittingPosition: mData.SittingPosition,
                        TechnicalSupport: mData.TechnicalSupport,
                        PreDiagnoseTxt: mData.PreDiagnoseTxt,
                        JobDetail: mData.JobDetail,
    
                        notes: mData.notes,
                        IsNeedTest: mData.IsNeedTest,
                        FinalConfirmation: mData.FinalConfirmation,
                        CustomerConfirmation: mData.CustomerConfirmation,
                        LastModifiedUserId: mData.LastModifiedUserId,
                        LastModifiedDate: mData.LastModifiedDate,
                        // ComplaintCatId : mData.ComplaintCatId,
                        // DetailJSON : mData.DetailJSON,
                        ScheduledTime: mData.ScheduledTime,
                        StallId: mData.StallId,
                        OutletId: mData.OutletId
                    }]);
                },

                getT1: function() {
                    // var catId = 2023;
                    var res = $http.get('/api/as/GetT1');
                    // console.log('resnya pause=>',res);
                    return res;
                },

                getDataDetail: function(id) {
                    var res = $http.get('/api/as/GeneralParameters/' + id);
                    //console.log('res=>',res);
                    //res.data.Result = null;
                    return res;
                },

                getQuestion: function() {
                    return $http.get('/api/as/QuestionsManagement');
                },

                getPreDiagnoseByJobId: function(id) {
                    return $http.get('/api/as/GetPrediagnoses/job/' + id);
                },

                getDataGeneralMaster: function(catId) {
                    var res=$http.get('/api/as/GlobalMaster?CategoryId='+catId);
                    return res;
                },

                getDataDetailWarranty: function(ClaimNo, AppStatus) {
                    var res = $http.get('/api/as/Towass/getCreateClaim/' + ClaimNo + '/' + AppStatus);
                    return res;
                },

                updateMainAddress: function(x){
                    console.log("===>",x);
                    var res=$http.put('/api/ct/PutCMainAddress/'+x.CustomerId+'/'+x.CustomerAddressId+'/');
                    return res;
                },

                insertRequestPrintHistory: function(data) {
                    console.log("request",data);
                    // var res=$http.get('/api/ct/PostCCustomerAccessTab/',data);
                    return $http.post('/api/as/PostApprovalRequestPrintServiceHistory', data);
                },

                checkAvailableByPoliceNo: function(type, data,flag, jobid) {
                    var res = $http.get('/api/as/Gate/checkAvailableByPoliceNo/' + type + '/' + data.LicensePlate +'/'+flag+'/'+jobid);
                    return res;
                },

                checkStatusdiGate: function(QueueId, Nopol) {
                    //cek kalo mobil tiba2 gateout
                    // api/as/queue/CheckGate/{QueueId}/{Nopol}
                    var res = $http.get('/api/as/queue/CheckGate/' + QueueId + '/' + Nopol);
                    return res;
                },

                getDataAktif: function() {
                    var res = $http.get('/api/as/MAppointmentDiscount/GetAktif/?start=1&limit=100000');                
                    return res;
                },
                updateQueueBackWO: function(QueueId, PoliceNo){
                    // api/as/queue/UpdateQueueBackWO/{QueueId}/{LicensePlate}/
                    return $http.put('/api/as/queue/UpdateQueueBackWO/'+QueueId+'/'+PoliceNo+'/');
                },

                CheckIsJobCancelWO: function (jobId) {
                    console.log("data di ReqCancelWO", jobId);
                    return $http.get('/api/as/CheckIsJobCancelWO/'+jobId);
                },

                CheckIsReception: function (IdQueue) {
                    console.log("data di ReqCancelWO", IdQueue);
                    return $http.get('/api/as/queue/CheckIsReception/'+IdQueue);
                },

                updateWODiscount: function(dataTask,dataParts) {
                    console.log("dataTask difac",dataTask);
                    console.log("dataParts difac",dataParts);
                    return $http.put('/api/as/DiskonUpdateWO/Update', [{
                        JobTask : dataTask,
                        JobParts : dataParts
                    }]);
                },

                getDiscountCampaign: function(today, data, crm, IsGR) {
                    var KmDiscount = 0;
                    if (data.Km !== undefined && data.Km !== null) {
                        if (typeof data.Km === 'string') {
                            var tmpKm = angular.copy(data.Km);
                            if (tmpKm.includes(",")) {
                                tmpKm = tmpKm.split(',');
                                if (tmpKm.length > 1) {
                                    tmpKm = tmpKm.join('');
                                } else {
                                    tmpKm = tmpKm[0];
                                }
                            }
                            tmpKm = parseInt(tmpKm);
                            KmDiscount = tmpKm;
                        } else {
                            KmDiscount = data.Km;
                        }
                    }
                    console.log("getDiscountCampaign", today, data, crm);
                    // '/api/as/CampaignDiscount/Date/2017-08-07/KM/2100/AYear/2015/VechId/4642'
                    return $http.get('/api/as/CampaignDiscount/Date/' + today + '/KM/' + KmDiscount + '/AYear/' + crm.Vehicle.AssemblyYear + '/VechId/' + crm.Vehicle.Type.VehicleTypeId + '/IsGR/' + IsGR);
                },


                // ===================================================  ini factory pindahan dari menu lain ================================= end
                checkIsCharacter: function(data){
                    if(typeof data == 'string'){
                        data = data.replace(/[^\x20-\x7f\xA\xD]/g, '');
                    }
                    return data;
                },
                checkVehicleVsJob: function(katasiki, arrayTaskid) {
                    // api/as/CheckTaskKatashikiCode/{isGR}
                    return $http.put('/api/as/CheckTaskKatashikiCode/1', [{
                        KatashikiCode : katasiki,
                        Task : arrayTaskid
                    }]);
                },
                CheckTaskFieldAction: function (vin) {
                    // api/as/Towass/CheckTaskFieldAction/{isGr}/{VIN}
                    return $http.get('/api/as/Towass/CheckTaskFieldAction/1/' + vin);
                },
                CheckCustomerMainAddress: function (customerid) {
                    // api/crm/CheckCustomerMainAddress/{CustomerId}
                    return $http.get('/api/crm/CheckCustomerMainAddress/' + customerid);
                },
                MergeAppointmentEstimation: function (JobIdAppointment, JobIdEstimation) {
                    // api/as/job/MergeAppointmentEstimation/{JobIdAppointment}/{JobIdEstimation}
                    return $http.put('/api/as/job/MergeAppointmentEstimation/' + JobIdAppointment + '/' + JobIdEstimation);
                },
                                
                CheckApprovalDiscountOutStandingWO: function (JobId) {
                    // api/as/CheckApprovalDiscountOutStandingWO/{JobId}
                    // res 888 ada job dan part outstanding
                    // res 666 ada job  outstanding
                    // res 999 ada part outstanding
                    return $http.get('/api/as/CheckApprovalDiscountOutStandingWO/' + JobId);
                },

                CheckApprovalDPOutStandingWO: function (JobId) {
                    // api/as/CheckApprovalDPOutStandingWO/{JobId}/{isGR}
                    return $http.get('/api/as/CheckApprovalDPOutStandingWO/' + JobId + '/' + 1);
                },

                
                

                getDataVendorOPL: function() {
                    return $http.get('/api/as/GetVendorOPL?start=1&limit=100')
                },
     
                getNewOplList: function(key, model,vendorid) {
                    console.log("key, model,vendorid",key, model,vendorid);
                    var res = $http.get('/api/as/GetTaskOPLbyVendor?oplname='+key+'&modelName='+model+'&VendorId='+vendorid);
                    return res
                },

                postOPLData: function(data,JobId,mdata) {
                    console.log("dataaaaa", data);
                    console.log("JobId factory",JobId);
                    var dataSave = [];
                    // "JobTaskOplId": 0,
                    //                 "OPLName": "Light Service - Seal Oring - (Internal)",
                    //                 "OPLId": 21747,
                    //                 "Price": 71500,
                    //                 "OPLPurchasePrice": 55000,
                    //                 "NormalPrice": 71500,
                    //                 "QtyPekerjaan": 1,
                    //                 "PaidById": "31",
                    //                 "TargetFinishDate": "2020/06/15",
                    //                 "Discount": 0,
                    //                 "DiscountedPriceOPL": 0,
                    //                 "OutletId": 280,
                    //                 "IsGR": 1,
                    //                 "JobId": 8937,
                    //                 "Notes": "Tidak ada catatan",
                    //                 "Status": 0,
                    //                 "isDeleted": 0,
                    //                 "VendorId": 273
                    for(var i in data){
                        console.log("JALAN INI");
                        for(var j in data[i].detail){
                            // Agar tdk backdate
                            var date = new Date(data[i].detail[j].EstimationDate);
                            date.setDate(date.getDate() + 1);

                            data[i].detail[j].JobTaskOplId = 0;
                            data[i].detail[j].OPLName =  data[i].detail[j].TaskNameOpl;
                            data[i].detail[j].OPLId =  data[i].detail[j].oplId;
                            data[i].detail[j].Price =  data[i].detail[j].HargaJual;
                            data[i].detail[j].OPLPurchasePrice =  data[i].detail[j].HargaBeli;
                            data[i].detail[j].NormalPrice =  data[i].detail[j].HargaJual;
                            data[i].detail[j].QtyPekerjaan =  data[i].detail[j].Qty;
                            data[i].detail[j].PaidById =  data[i].detail[j].PaymentId;
                            data[i].detail[j].TargetFinishDate =  date;
                            data[i].detail[j].EstimationDate =  date;
                            data[i].detail[j].Discount =  data[i].detail[j].Discount;
                            data[i].detail[j].DiscountedPriceOPL =  data[i].detail[j].Discount;
                            data[i].detail[j].OutletId =  currUser.OutletId;
                            data[i].detail[j].isGr =  1;
                            data[i].detail[j].JobId =  JobId;//JobId
                            data[i].detail[j].Notes =  data[i].detail[j].Notes;
                            data[i].detail[j].Status =  0;
                            data[i].detail[j].isDeleted =  0;
                            data[i].detail[j].VendorId =  data[i].VendorId;
                            data[i].detail[j].Guid_Log =  mdata.GUIDWOGR
                        }
                        dataSave.push(
                                {
                                  "VendorId": data[i].VendorId,
                                  "VendorName": data[i].Vendor,
                                  "OplNo": null,
                                  "OPLTasks": data[i].detail
                              }
                        );
                    }
    
                    return $http.post('/api/as/JobTaskOPLCR/SubmitData', dataSave);
                },
                UpdateEstimationJobId: function(PoliceNo,JobId) {
                    // api/as/UpdateEstimationJobId/{PoliceNo}/{JobId}
                    return $http.put('/api/as/UpdateEstimationJobId/' + PoliceNo + '/' + JobId)
                },
                CheckIsSAAssigmentValid: function(PoliceNumber) {
                    // api/as/CheckIsSAAssigmentValid/{PoliceNumber}
                    return $http.get('/api/as/CheckIsSAAssigmentValid/' + PoliceNumber )
                },

                CheckIsReceptionBackToQueue: function(IdQueue) {
                    // api/as/queue/CheckIsReceptionBackToQueue/{IdQueue}
                    return $http.get('/api/as/queue/CheckIsReceptionBackToQueue/' + IdQueue )
                },

                CheckIsPDS: function(vin) {
                    // api/as/CheckIsPDS/{VIN}
                    return $http.get('/api/as/CheckIsPDS/' + vin )
                },

                CekVehicleList: function(PoliceNo) {
                    // api/crm/CekVehicleList/{PoliceNo}
                    return $http.get('/api/crm/CekVehicleList/' + PoliceNo)
                },

                getUserLists: function(vId) {
                    return $http.get('/api/crm/GetVehicleUser/' + vId);
                },

                CallCustCekBilling: function(JobId) {
                    // api/as/CallCustCekBilling/{JobId}
                    return $http.get('/api/as/CallCustCekBilling/' + JobId)
                },

                GetCVehicleTypeListByKatashiki: function(KatashikiCode) {
                    // api/crm/GetCVehicleTypeListByKatashiki/{KatashikiCode}
                    return $http.get('/api/crm/GetCVehicleTypeListByKatashiki/' + KatashikiCode)
                },
                GetResetCounterOdometer: function(vin) {
                    // api/ct/GetResetCounterOdometer/{VIN}
                    return $http.get('/api/ct/GetResetCounterOdometer/' + vin)
                },

                CekGatePDS: function(QueueId) {
                    // api/as/CekGatePDS/{QueueId}
                    // 666 berarti gatein pds
                    // 0 berarti bukan
                    return $http.get('/api/as/CekGatePDS/' + QueueId)
                },

                CekStatusPartsTECO: function(JobId, data) {
                    // api/as/JobParts/CekStatusPartsTECO/{JobId}
                    return $http.post('/api/as/JobParts/CekStatusPartsTECO/' + JobId, data);
                },

                GetListCalledQueue: function() {
                    // api/as/queue/GetListCalledQueue/
                    return $http.get('/api/as/queue/GetListCalledQueue/')
                },

                BackToQueueList: function(id) {
                    // api/as/queue/BackToQueueList/{Id}
                    return $http.post('/api/as/queue/BackToQueueList/' + id)
                },


                CheckQuueueFrown: function(IdQueue, JobId) {
                    // api/as/queue/CheckQuueue/{IdQueue}/{JobId}
                    // 666 Frown
                    // 0 Biasa
                    return $http.get('/api/as/queue/CheckQuueue/' + IdQueue + '/' + JobId)
                },

                
                
                isiGuidLogTaskPartOPL: function(item, GL, tipe) {
                    if (tipe == 2){ // untuk opl
                        if (item !== null && item !== undefined){
                            for(var i=0; i<item.length; i++){
                                item[i].Guid_Log = GL
                            }
                        }
                        return item
                    } else { // untuk job dan parts
                        if (item !== null && item !== undefined){
                            for(var i=0; i<item.length; i++){
                                item[i].Guid_Log = GL
                                if (item[i].JobParts !== null && item[i].JobParts !== undefined){
                                    for (var j=0; j<item[i].JobParts.length; j++){
                                        item[i].JobParts[j].Guid_Log = GL
                                    }
                                }
                            }
                        }
                        return item;
                    }
                    
                },
                listHistoryClaim: function(vin){
                    var res = $http.get('/api/as/ToWass/ListHistoryClaim?VIN=' + vin)
                    return res;
                },
                getDataVIN: function(vin){
                    var res = $http.get('/api/as/MasterVehicle_FPCFLCPLC/' + vin)
                    return res;
                } 
            }
        });