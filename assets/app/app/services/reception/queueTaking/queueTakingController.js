angular.module('app')
  .controller('queueTakingController', function($scope, $http, $interval, $timeout, CurrentUser, QueueTaking, ngDialog) {
    //----------------------------------
    // Function
    //----------------------------------

    $scope.pickEstimates = function() {
      ngDialog.open({
        template: 'app/services/reception/queueTaking/queueTakingPickEstimatesDialog.html',
        controller: 'queueTakingController',
        scope: $scope
      });
    };

    $scope.createWO = function() {
      console.log($scope);
      window.scrollTo(0, 0);
      $interval.cancel(intervalCallTimer);
      ngDialog.closeAll();
    };

    function countInterval() {
      if ($scope.callTimer > 0) {
        $scope.callTimer = $scope.callTimer - 1;
      } else {
        $interval.cancel(intervalCallTimer);
      }
    }

    $scope.callCostumer = function() {
      $scope.pageMode = 'call';
      window.scrollTo(0, 0);
      $scope.callTimer = 60;

      intervalCallTimer = $interval(countInterval, 1000);
    }

    $scope.callCostumerAgain = function() {
      $interval.cancel(intervalCallTimer);
      $scope.callTimer = 60;
      intervalCallTimer = $interval(countInterval, 1000);
    }

    $scope.callCancel = function() {
      $interval.cancel(intervalCallTimer);
      $scope.pageMode = 'view';
      window.scrollTo(0, 0);
    }

    //----------------------------------
    // Get Data
    //----------------------------------


    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    //mode nya ada 3 view : untuk tampilan sebelum manggil costumer, call : untuk tampilan panggil costumer,
    //call orang jadi yang di batasi form call nya
    $scope.pageMode = 'view';
    $scope.callTimer = 60;
    var intervalCallTimer;

    $scope.mDataEMOnTime = [{
        'policeNumber': 'B 1234 GAT',
        'owner': 'Qilin',
        'psfu': '<i class="fa fa-minus-circle" aria-hidden="true"></i>',
        'meetTime': '05:55',
        'waitTime': '00:60'
      },
      {
        'policeNumber': 'B 1234 GAT',
        'owner': 'Rathalos',
        'psfu': '<i class="fa fa-frown-o" aria-hidden="true"></i>',
        'meetTime': '05:55',
        'waitTime': '00:60'
      }
    ];
    $scope.mDataEM = [{
        'policeNumber': 'B 1234 GAT',
        'owner': 'Qilin',
        'psfu': '<i class="fa fa-minus-circle" aria-hidden="true"></i>',
        'meetTime': '05:55',
        'waitTime': '00:60'
      },
      {
        'policeNumber': 'B 1234 GAT',
        'owner': 'Rathalos',
        'psfu': '<i class="fa fa-frown-o" aria-hidden="true"></i>',
        'meetTime': '05:55',
        'waitTime': '00:60'
      }
    ];
    $scope.mDataBookingOnTime = [{
        'policeNumber': 'B 1234 GAT',
        'owner': 'Qilin',
        'psfu': '<i class="fa fa-minus-circle" aria-hidden="true"></i>',
        'meetTime': '05:55',
        'waitTime': '00:60'
      },
      {
        'policeNumber': 'B 1234 GAT',
        'owner': 'Rathalos',
        'psfu': '<i class="fa fa-frown-o" aria-hidden="true"></i>',
        'meetTime': '05:55',
        'waitTime': '00:60'
      }
    ];
    $scope.mDataBooking = [{
        'policeNumber': 'B 1234 GAT',
        'owner': 'Qilin',
        'psfu': '<i class="fa fa-minus-circle" aria-hidden="true"></i>',
        'meetTime': '05:55',
        'waitTime': '00:60'
      },
      {
        'policeNumber': 'B 1234 GAT',
        'owner': 'Rathalos',
        'psfu': '<i class="fa fa-frown-o" aria-hidden="true"></i>',
        'meetTime': '05:55',
        'waitTime': '00:60'
      }
    ];
    $scope.mDataWalkIn = [{
        'policeNumber': 'B 1234 GAT',
        'owner': 'Qilin',
        'psfu': '<i class="fa fa-minus-circle" aria-hidden="true"></i>',
        'meetTime': '05:55',
        'waitTime': '00:60'
      },
      {
        'policeNumber': 'B 1234 GAT',
        'owner': 'Rathalos',
        'psfu': '<i class="fa fa-frown-o" aria-hidden="true"></i>',
        'meetTime': '05:55',
        'waitTime': '00:60'
      }
    ];
    $scope.mDataOthers = [{
        'policeNumber': 'B 1234 GAT',
        'owner': 'Qilin',
        'psfu': '<i class="fa fa-minus-circle" aria-hidden="true"></i>',
        'meetTime': '05:55',
        'waitTime': '00:60'
      },
      {
        'policeNumber': 'B 1234 GAT',
        'owner': 'Rathalos',
        'psfu': '<i class="fa fa-frown-o" aria-hidden="true"></i>',
        'meetTime': '05:55',
        'waitTime': '00:60'
      }
    ];

    $scope.mDataCostumer = {
      'owner': "Tesug",
      'user': "Gunawan",
      'address': "Inaba",
      'model': "All New Fortuner",
      'policeNumber': "B 1234 RK",
      'late': "00:00 (kalau ontime artinya ini pasti 00:00 donk)",
      'waitTime': "00:60",
      'jobSuggest': "ganti oli",
      'estimates': [{
          'number': "12345",
          'createDate': "25-12-2525",
          'sa': "Gunawan"
        },
        {
          'number': "678910",
          'createDate': "11-01-1111",
          'sa': "Gunaawan Kedua"
        }
      ]
    };
    //----------------------------------
    // Grid Setup
    //----------------------------------

    var cellTemplatePSFU = '<div class="ui-grid-cell-contents" ng-bind-html="COL_FIELD"></div>';
    $scope.columnDefs = [{
        name: 'Customer',
        cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.policeNumber}} - {{row.entity.owner}}</div>',
        width: '75%'
      },
      {
        name: 'PSFU',
        field: 'psfu',
        cellTemplate: cellTemplatePSFU,
        width: '5%'
      },
      {
        name: 'Janji',
        field: 'meetTime',
        width: '10%'
      },
      {
        name: 'Waktu Tunggu',
        field: 'waitTime',
        width: '10%'
      }
    ];

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
      $scope.loading = true;
    });


  });
