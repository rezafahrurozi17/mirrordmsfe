angular.module('app')
  .controller('AttendancePlanController', function($scope, $http, CurrentUser, AttendancePlan, WorkingShift, $timeout, bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------

    $scope.$on('$viewContentLoaded', function() {
      $scope.loading = true;
      $scope.gridData = [];
      $scope.dataEmployee();
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mAttendancePlan = {}; //Model
    $scope.activeShift = [];
    $scope.shifts = [];
    $scope.employeeData = [];
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    }
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {

      console.log("arigato", $scope.mAttendancePlan.EmployeeFilter);
      if ($scope.mAttendancePlan.EmployeeFilter != null && $scope.mAttendancePlan.EmployeeFilter != "" && typeof $scope.mAttendancePlan.EmployeeFilter != "undefined") {
        AttendancePlan.getData($scope.mAttendancePlan.EmployeeFilter).then(
          function(res) {

            $scope.grid.data = res.data.Result.map(function(obj) {

              // var CalIdFromDateFormat = new Date(obj.CalIdFrom.toString().substr(0, 4) + "-" +
              //   obj.CalIdFrom.toString().substr(4, 2) + "-" + obj.CalIdFrom.toString().substr(6, 2));
              //
              // var CalIdThruDateFormat = new Date(obj.CalIdThru.toString().substr(0, 4) + "-" +
              //   obj.CalIdThru.toString().substr(4, 2) + "-" + obj.CalIdThru.toString().substr(6, 2));

              var CalIdFromDateFormat = obj.CalIdFrom.toString().substr(0, 4) + "-" +
                obj.CalIdFrom.toString().substr(4, 2) + "-" + obj.CalIdFrom.toString().substr(6, 2);

              var CalIdThruDateFormat = obj.CalIdThru.toString().substr(0, 4) + "-" +
                obj.CalIdThru.toString().substr(4, 2) + "-" + obj.CalIdThru.toString().substr(6, 2);

              var receh = {
                "Id": obj.Id,
                "EmployeeId": obj.EmployeeId,
                "CalIdFrom": obj.CalIdFrom,
                "CalIdThru": obj.CalIdThru,
                "CalIdFromDateFormat": CalIdFromDateFormat,
                "CalIdThruDateFormat": CalIdThruDateFormat,
                "ShiftByte": obj.ShiftByte,
                "LastModifiedDate": obj.LastModifiedDate,
                "LastModifiedUserId": obj.LastModifiedUserId,
                "Mprofile_Employee": obj.Mprofile_Employee
              };

              return receh;
            });

            $scope.loading = false;
            return res.data.Result;
          },
          function(err) {
            console.log("err=>", err);
          }
        );
      } else {
        /*bsNotify.show({
          size: 'big',
          type: 'danger',
          title: "Mohon Input Employee",
          // content: error.join('<br>'),
          // number: error.length
        });*/
      }
    }

    $scope.dataEmployee = function() {
      AttendancePlan.getDataEmployee().then(

        function(res) {
          // console.log("get data", res);
          $scope.EmployeeFilter = res.data.Result;
          //console.log("$scope.EmployeeFilter", $scope.EmployeeFilter);
          // $scope.loading=false;
          // return res.data.Result;
        },
        function(err) {
          console.log("err=>", err);
        }
      );
    }

    // $scope.checkShiftActive = function(){
    //     $scope.activeShift = [];
    //     for(shift in $scope.shifts){
    //         $scope.activeShift.push($scope.mCalendar.ShiftByte & $scope.shifts[shift].ShiftByte);
    //     }
    //     console.log("$scope.activeShift",$scope.activeShift);
    // }

    $scope.onBeforeNew = function() {
      $scope.activeShift = [];
      $scope.shifts.forEach(function(obj) {
        $scope.activeShift.push(0);
      });

      // console.log("onBeforeNew row", rows);
      console.log("onBeforeNew master shift", $scope.shifts);
      // console.log("onBeforeNew mAttendancePlan", $scope.mAttendancePlan);
    }

    $scope.onBeforeEdit = function(rows) {
      $scope.activeShift = [];
      //console.log("row ", rows);
      $scope.shifts.forEach(function(obj) {
        // console.log("obj.ShiftByte ", obj.ShiftByte);
        // console.log("rows.ShiftByte ", rows.ShiftByte);
        // console.log("--> ", obj.ShiftByte & rows.ShiftByte);

        if ((obj.ShiftByte & rows.ShiftByte) != 0) {
          $scope.activeShift.push(1);
        } else {
          $scope.activeShift.push(0);
        }
      });

      //console.log("hasil", $scope.activeShift);


      //console.log("onBeforeEdit row", rows);
      // console.log("onBeforeEdit master shift", $scope.shifts);
      // console.log("onBeforeEdit mAttendancePlan", $scope.mAttendancePlan);
    }

    $scope.onBeforeSave = function() {
      $scope.mAttendancePlan.ShiftByte = 0;
      for (var i = 0; i < $scope.activeShift.length; i++) {
        if ($scope.activeShift[i] == 1) {
          $scope.mAttendancePlan.ShiftByte += $scope.shifts[i].ShiftByte;
        }
      }
    }

    /*$scope.onShowDetail = function(rows) {
      console.log("on show detail row", rows);
      console.log("on show detail master shift", $scope.shifts);
      console.log("on show mAttendancePlan", $scope.mAttendancePlan);

      $scope.shifts.forEach(function(obj) {

      });

      // var total = rows.ShiftByte;
      // var arr = [];
      // var co = [];
      // for (var a = 0; a < $scope.shifts.length; a++) {
      //   arr.push($scope.shifts[a].ShiftByte);
      //   co.push("activeShift" + [a]);
      // }
      // var arrBin = arr;
      // console.log("arrBin", arrBin, "Total", total);
      // for (var i in arrBin) {
      //   if (total & arrBin[i]) {
      //     var xc = co[i];
      //     $scope.mAttendancePlan[xc] = arrBin[i];
      //
      //     // $scope.this = xc;
      //     // console.log("hahaha", $scope.this);
      //     // $scope.mAttendancePlan.activeShift[i] = arrBin[i];
      //     console.log("wtf", xc);
      //     console.log(co[i] + ' : nyala');
      //   }
      // }
    }*/

    //var JobTaskIdFI = [];

    /*$scope.checkShiftActive = function(ShiftByte) {

      console.log("ShiftByte", ShiftByte);
      var myTotal = 0;
      // for(var a = 0; a <= ShiftByte; a++){
      //   // test += ShiftByte[a];

      //   test += ShiftByte[a];
      //   console.log("test",test);
      //
      var a = JobTaskIdFI.indexOf(ShiftByte);
      //console.log("wew", a);
      if (a > -1) {
        JobTaskIdFI.splice(a, 1);
        // test = test - JobTaskIdFI.length;
        for (var i = 0; i < JobTaskIdFI.length; i++) {
          myTotal += JobTaskIdFI[i];
        }
        $scope.mAttendancePlan.ShiftByte = myTotal;
        // console.log("nih di if 1", JobTaskIdFI);
        //console.log("jumlah", myTotal);
      } else {
        JobTaskIdFI.push(ShiftByte);
        // test += ShiftByte;
        var myTotal = 0;
        for (var i = 0; i < JobTaskIdFI.length; i++) {
          myTotal += JobTaskIdFI[i];
        }
        $scope.mAttendancePlan.ShiftByte = myTotal;
        // console.log("nih else", JobTaskIdFI);
        //console.log("jumlah", myTotal);
      }

    }*/

    WorkingShift.getData().then(

      function(res) {
        $scope.shifts = res.data.Result;
        //console.log("data shifts", $scope.shifts);
      },
      function(err) {
        console.log("err=>", err);
      }
    );
    //----------------------------------
    // Grid Setup
    //----------------------------------
    // var dateFormat='dd/MM/yyyy';
    $scope.grid = {
      enableSorting: true,
      enableRowSelection: true,
      multiSelect: true,
      enableSelectAll: true,
      columnDefs: [{
          name: 'Id',
          field: 'Id',
          visible: false
        },
        {
          name: 'EmployeeCode',
          field: 'Mprofile_Employee.EmployeeCode'
        },
        {
          name: 'Nama',
          field: 'Mprofile_Employee.EmployeeName'
        },
        {
          name: 'Tanggal Mulai',
          field: 'CalIdFromDateFormat'
        },
        {
          name: 'Tanggal Selesai',
          field: 'CalIdThruDateFormat'
        },
      ]
    };


  });
