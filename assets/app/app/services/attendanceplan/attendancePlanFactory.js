angular.module('app')
  .factory('AttendancePlan', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(EmployeeId) {
        return res=$http.get('/api/as/AttendancePlans?EmployeeId='+EmployeeId+'');
      },
      create: function(mData) {

        var datestart = new Date(mData.CalIdFromDateFormat);
        datestart.setDate(datestart.getDate() + 1);

        var datefinish = new Date(mData.CalIdThruDateFormat);
        datefinish.setDate(datefinish.getDate() + 1);

        return $http.post('/api/as/AttendancePlans', [
                                                        {
                                                          EmployeeId:mData.EmployeeId,
                                                          DateFrom:datestart,
                                                          DateThru:datefinish,
                                                          ShiftByte:mData.ShiftByte
                                                        }
                                                      ]);
      },
      update: function(mData){
        var datestart = new Date(mData.CalIdFromDateFormat);
        datestart.setDate(datestart.getDate() + 1);

        var datefinish = new Date(mData.CalIdThruDateFormat);
        datefinish.setDate(datefinish.getDate() + 1);

        return $http.put('/api/as/AttendancePlans/', [
                                                        {
                                                          Id : mData.Id,
                                                          EmployeeId:mData.EmployeeId,
                                                          DateFrom:datestart,
                                                          DateThru:datefinish,
                                                          ShiftByte:mData.ShiftByte
                                                        }
                                                      ]);
      },
      delete: function(EmployeeId) {
        return $http.delete('/api/as/AttendancePlans/', {data:EmployeeId,headers: {'Content-Type': 'application/json'}});
      },
      getDataEmployee: function(){
        return res=$http.get('/api/as/Mprofile_Employee/OutletId');
      }
    }
  });
