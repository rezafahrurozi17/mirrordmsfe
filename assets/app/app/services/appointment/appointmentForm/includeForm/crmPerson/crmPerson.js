angular.module('app').directive('crmPerson', function() {
  return {
    restrict: 'E',
    templateUrl: 'app/services/appointment/appointmentForm/includeForm/crmPerson/crmPerson.html',
    scope: {
      id: '=',
      editenable: '='
    },
    link: function(scope) {

      scope.changeOwner = function() {
        if (scope.action == 3) {
          scope.mode = 'form';
        } else {
          scope.mode = 'search';
        }
      };

      scope.mode = 'view'; //form-->insert pemilik, view-->untuk tampilan awal, search-->untuk ganti pemilik
      scope.listAction = [{
          'id': 1,
          'text': 'Ganti Pemilik dengan ToyotaID'
        },
        {
          'id': 2,
          'text': 'Ganti Pemilik dengan Nomor Handphone'
        },
        {
          'id': 3,
          'text': 'Ganti Pemilik dengan Input Data'
        }
      ];
      //scope.changeMode


      scope.mDataCrm = {
        'toyotaID': "2344543-3",
        'catCust': "",
        'typeCust': "",
        'ktp': "43543545356",
        'npwp': "34534545454",
        'name': "Budi Namanya",
        'nameShort': "Budi",
        'sex': "Pria",
        'birthPlace': "Jakarta",
        'birthDate': "17-08-1945",
        'addr': "jln hayalan no 1",
        'province': "Papaya",
        'city': "Jakarta Utara",
        'kecamatan': "asdf",
        'kelurahan': "asdf",
        'postalCode': "14633",
        'email': "m@email.com",
        'phoneNumber1': "087423234243",
        'phoneNumber2': "087423234244"
      }

      scope.mDataCrmNew = {
        'name': "",
        'phoneNumber': "",
        'birthDate': "",
        'ktp': "",
        'npwp': ""
      }
    }
  };
});
