angular.module('app').directive('crmVehicle', function(VehicleModelLocal) {
  return {
    restrict: 'E',
    templateUrl: 'app/services/appointment/appointmentForm/includeForm/crmVehicle/crmVehicle.html',
    link: function(scope) {
      VehicleModelLocal.getData().then(function(res) {
        scope.listMasterVehicleData = res.data.Result;
      });


    }
  };
});
