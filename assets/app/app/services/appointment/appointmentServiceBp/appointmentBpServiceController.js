angular.module('app')
    .controller('appointmentBpServiceController', function($scope, $http, GeneralMaster, RepairProcessGR, $q, $window, $filter, $state, JscbFactory, $timeout, bsNotify, ASPricingEngine, WOBP, CurrentUser, uiGridConstants, AppointmentBpService, AppointmentGrService, $timeout, bsAlert, ngDialog, AsbEstimasiFactory, AsbGrFactory, AsbProduksiFactory, MrsList, CMaster, WO, PrintRpt, VehicleHistory, MasterDiscountFactory, FollowupService) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        var PPNPerc = 0;

        $scope.asb = { currentDate: new Date(), showMode: 'main', nopol: "D 1234 ABC", tipe: "Avanza" }
        $scope.asb.endDate = new Date();
        $scope.asb.endDate.setDate($scope.asb.endDate.getDate() + 7);
        var currentDate = new Date();
        currentDate.setDate(currentDate.getDate() + 1);
        $scope.minDateASB = new Date(currentDate);
        $scope.minDateJSCB = new Date();
        $scope.stallAsb = 0;
        $scope.saIdAsb = 0;
        var AlltotalEstimation = 0;
        var totalEstimation = 0;

        $scope.loadingSimpan = 0

        $scope.DateOptionsHistory = {
            startingDay: 1,
            format: 'dd/MM/yyyy',
        };
        $scope.DateOptionsASB = {
            startingDay: 1,
            format: 'dd/MM/yyyy',
            //disableWeekend: 1
        };
        // var showBoardAsbGr = function(vitem){
        //     AsbGrFactory.showBoard({container: 'asb_asbgr_view', currentChip: vitem, onPlot: contoh});
        // }
        var allBoardHeight = 500;
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            //showBoardAsbGr(vobj);
            $scope.getPPN()

            WOBP.getT1().then(function(res) {
                $scope.TFirst = res.data.Result;
                //console.log("T1", $scope.T1);
            })
        });

        $scope.getPPN = function (noNpwp) {
            if (noNpwp !== undefined && noNpwp !== null && noNpwp !== ''){
                noNpwp = noNpwp.toString();
                if (noNpwp.length === 20){
                    if (noNpwp !== '00.000.000.0-000.000'){
                        ASPricingEngine.getPPN(1, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    } else {
                        ASPricingEngine.getPPN(0, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    }
                } else {
                    ASPricingEngine.getPPN(0, 3).then(function (res) {
                        PPNPerc = res.data
                    });
                }

            } else {
                // param 1 adalah isnpwp, param 2 tipe ppn
                ASPricingEngine.getPPN(0, 3).then(function (res) {
                    PPNPerc = res.data
                });
            }
            
        };


        $scope.model_info_km = {}
        $scope.model_info_km.show = false;
        $scope.viewButtoninfokm = {save:{text:'Ok'}};

        $scope.Saveinfokm = function(){
            $scope.model_info_km.show = false;
        }
        $scope.Cancelinfokm = function(){
            $scope.model_info_km.show = false;
        }
        $scope.show_km_info = function() {
            $scope.model_info_km.show = true;
        }

        var showBoardAsbGr = function(item) {
            console.log("item item", item);
            var vtime
            var tmphour
            var tmpMinute
            if (item !== undefined && item !== null) {
                vtime = item;
                tmphour = item.getHours();
                tmpMinute = item.getMinutes();
            } else {
                vtime = new Date();
                tmphour = vtime.getHours();
                tmpMinute = vtime.getMinutes();
            }

            vtime.setHours(tmphour);
            vtime.setMinutes(tmpMinute);
            vtime.setSeconds(0);
            $scope.currentItem = {
                mode: 'main',
                currentDate: $scope.asb.startDate,
                nopol: $scope.mDataDetail.LicensePlate,
                vin: $scope.mDataDetail.VIN,
                tipe: $scope.mDataDetail.VehicleModelName,
                durasi: $scope.tmpActRate,
                startTime: vtime,
                saId: $scope.stallAsb,
                visible: $scope.visibleAsb,
                asbStatus: $scope.asb,
                stallId: $scope.stallAsb
                    // JobId: $scope.mData.JobId,
                    /*stallId :1,
                    startDate :,
                    endDate:,
                    durasi:,*/
            }
            console.log("actRe", $scope.tmpActRate);
            console.log("harusnya tanggal", $scope.asb.startDate);
            console.log("ini vtime", vtime);
            console.log("stallId", $scope.currentItem);
            $timeout(function() {
                AsbEstimasiFactory.showBoard({ container: 'asb_estimasi_view', currentChip: $scope.currentItem, onPlot: getDateEstimasi });

            }, 100);

        }



        $scope.reloadBoard = function(item) {
                $scope.mData.PlanStart = null;
                $scope.mData.PlanFinish = null;
                $scope.mData.PlanDateStart = null;
                $scope.mData.AppointmentDate = null;
                $scope.mData.AppointmentTime = null;
                $scope.mData.TargetDateAppointment = null;
                $scope.mData.TimeTarget = null;
                $scope.mData.PlanDateFinish = null;
                console.log("dari ng-change", item);

                console.log('$scope.asb.startDate ===>',$scope.asb.startDate);
                $scope.asbDate = item;

                if($scope.mData.isCash == 1){
                    if(item > $scope.DiskonTaskEndDate){
                        console.log('Tidak Masuk periode diskon booking Task');
                        $scope.DiscountTask = 0;
                        $scope.mData.disTask = 0;
                    }else{
                        console.log('masuk periode diskon booking Task');
                    }

                    if(item > $scope.DiskonPartsEndDate){
                        console.log('Tidak Masuk periode diskon booking Parts');
                        $scope.DiscountParts = 0;
                        $scope.mData.disParts = 0;
                    }else{
                        console.log('masuk periode diskon booking Parts');
                    }

                }


                console.log("$scope.mData.FullDate", $scope.mData.FullDate);
                var vtime = item;
                var tmphour
                var tmpMinute
                if ($scope.mData.FullDate !== undefined) {
                    console.log("$scope.mData.FullDate", $scope.mData.FullDate);
                    var tmpTime = $scope.mData.FullDate;
                    tmphour = tmpTime.getHours();
                    tmpMinute = tmpTime.getMinutes();
                } else {
                    tmphour = 9;
                    tmpMinute = 0;
                }
                vtime.setHours(tmphour);
                vtime.setMinutes(tmpMinute);
                vtime.setSeconds(0);
                $scope.currentItem = {
                    mode: 'main',
                    currentDate: $scope.asb.startDate,
                    nopol: $scope.mDataDetail.LicensePlate,
                    vin: $scope.mDataDetail.VIN,
                    tipe: $scope.mDataDetail.VehicleModelName,
                    durasi: $scope.tmpActRate,
                    startTime: vtime,
                    saId: 0,
                    visible: $scope.visibleAsb,
                    stallId: 0,
                    // JobId: $scope.mData.JobId,
                    /*stallId :1,
                    startDate :,
                    endDate:,
                    durasi:,*/
                }
                console.log("actRe", $scope.tmpActRate);
                console.log("harusnya tanggal", $scope.asb.startDate);
                console.log("ini vtime", vtime);
                console.log("stallId", $scope.currentItem.stallId);
                $timeout(function() {
                    AsbEstimasiFactory.showBoard({ container: 'asb_estimasi_view', currentChip: $scope.currentItem, onPlot: getDateEstimasi });

                }, 100);
            }
            //----------------------------------
            // Initialization
            //----------------------------------
        var getDateEstimasi = function(data) {
                console.log('onplot data...', data);
                var dt = new Date(data.startTime);
                var dtt = new Date(data.endTime);
                console.log("date", dt);
                dt.setSeconds(0);
                dtt.setSeconds(0);
                var timeStart = dt.toTimeString();
                var timeFinish = dtt.toTimeString();
                timeStart = timeStart.split(' ');
                timeFinish = timeFinish.split(' ');
                // ===============================
                var yearFirst = dt.getFullYear();
                var monthFirst = dt.getMonth() + 1;
                var dayFirst = dt.getDate();
                var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                // ===============================
                var yearFinish = dtt.getFullYear();
                var monthFinish = dtt.getMonth() + 1;
                var dayFinish = dtt.getDate();
                var FinishDate = yearFinish + '-' + monthFinish + '-' + dayFinish;
                // ===============================
                // if(timeStart[0] !== null && timeFinish[0] !== null){
                $scope.mData.StallId = data.stallId;
                $scope.mData.FullDate = dt;
                $scope.mData.PlanStart = timeStart[0];
                $scope.mData.PlanFinish = timeFinish[0];
                $scope.mData.PlanDateStart = firstDate;
                $scope.mData.AppointmentDate = dt;
                $scope.mData.AppointmentTime = timeStart[0];
                $scope.mData.TargetDateAppointment = FinishDate;
                $scope.mData.TimeTarget = timeFinish[0];
                $scope.mData.PlanDateFinish = FinishDate;
                // =======================
                $scope.mData.TargetPlanFinish = timeFinish[0];
                $scope.mData.TargetPlanDateFinish = FinishDate;
                // =======================
                $scope.calculateforwork();
                // }

                // }
            }
            //----------------------------------


        //----------------------------------

        $scope.user = CurrentUser.user();
        $scope.mData = {}; //Model
        $scope.mData.isCash = undefined;
        $scope.gridWorkViewEstimasi = [];
        $scope.xAppoint = {};
        $scope.xAppoint.selected = [];
        $scope.filter = {};
        $scope.edit = false;
        $scope.show_modal = { show: false };
        $scope.modalMode = 'new';
        $scope.tmpActRate = 1;
        $scope.stallAsb = 0;
        $scope.saIdAsb = 0;
        $scope.isASB = 0;
        var currentDate = new Date();
        $scope.currentDate = currentDate.setDate(currentDate.getDate() + 1);
        $scope.ComplaintCategory = [
            { ComplaintCatg: 'Body' },
            { ComplaintCatg: 'Body Electrical' },
            { ComplaintCatg: 'Brake' },
            { ComplaintCatg: 'Chassis' },
            { ComplaintCatg: 'Drive Train' },
            { ComplaintCatg: 'Engine' },
            { ComplaintCatg: 'Heater System & AC' },
            { ComplaintCatg: 'Restraint' },
            { ComplaintCatg: 'Steering' },
            { ComplaintCatg: 'Suspension and Axle' },
            { ComplaintCatg: 'Transmission' },
        ]
        $scope.jobAvailable = false;
        $scope.partsAvailable = false;
        $scope.JobRequest = [];
        $scope.JobComplaint = [];
        $scope.listButtonSettings = { new: { enable: true, icon: "fa fa-fw fa-car" } };
        $scope.listCustomButtonSettings = {
            enable: true,
            icon: "fa fa-fw fa-car",
            func: function(row) {
                console.log("customButton", row);
            }
        };
        $scope.listApi = {};
        $scope.listApiRequest = {};
        $scope.listSelectedRows = [];
        $scope.lmComplaint = {};
        $scope.lmRequest = {};
        $scope.lmModel = {};
        $scope.ldModel = {};
        $scope.typeMData = [
            { Id: 1, Name: "1" }, 
            { Id: 2, Name: "2" }, 
            { Id: 3, Name: "3" },
            { Id: 6, Name: "T" },
            // { Id: 7, Name: "C" }
        ];
        $scope.ismeridian = false;
        $scope.hstep = 1;
        $scope.mstep = 1;
        $scope.formApi = {};
        $scope.checkAsb = false;
        // $scope.typeSearch = [{ Id: 0, Name: "No. Polisi" }, { Id: 1, Name: "No. Rangka" }];
        $scope.typeSearch = [{ Id: 0, Name: "No. Polisi" }, { Id: 1, Name: "Toyota ID" }];
        $scope.filter.typeSearch = 0;
        $scope.vehicleStatus = 0;
        $scope.showModalReason = false;
        // =============================
        // JSCB
        // =============================
        $scope.jscb = { currentDate: new Date(), showMode: 'main', nopol: "D 1234 ABC", tipe: "Avanza" }
        $scope.jscb.startDate = new Date();
        $scope.jscb.endDate = new Date();
        $scope.jscb.endDate.setDate($scope.jscb.endDate.getDate() + 7);
        $scope.jscb.teamList = []
        $scope.jscb.team = '';
        $scope.jscb.displayBoard = true;
        $scope.jscb.boardName = 'JSCB_VIEWBP';
        $scope.jscb.container = 'JSCB_VIEWBP';
        $scope.BpCategory = {}
        $scope.BpGroupList = []
        $scope.slider = {
            value: 0,
            options: {
                step: 3.125,
                hidePointerLabels: true,
                noSwitching: true,
                minRange: 1,
                floor: 0,
                disabled: true,
                readOnly: true,
                ceil: 100,
                showSelectionBar: true,
                // translate: function(value) {
                //     return value + '%';
                // }
                translate: function(value, id, label) {
                    if (value == 0) {
                        return value = 'E'
                    } else if (value == 100) {
                        return value = 'F'
                    }
                }
            }
        };

        

        

        $scope.refreshSlider = function() {
            // $scope.gridWAC.columnDefs[2].editDropdownOptionsArray = $scope.DamageVehicle;
            $timeout(function() {
                $scope.$broadcast('rzSliderForceRender');
            });
        };
        $scope.jscb.updateCategory = function(vcategory) {
            this.BpCategory = vcategory;
            if (this.BpCategory && this.team) {
                $scope.jscb.displayBoard = true;
                this.reloadBoard();
            }
        }

        $scope.jscb.updateTeam = function(vteam) {
            this.team = vteam;
            if (this.BpCategory && this.team) {
                $scope.jscb.displayBoard = true;
                this.reloadBoard();
            }
        }

        vconfig = {
            urlCategoryBp: '/api/as/Boards/MGlobal/1012',
            urlGroupList: '/api/as/Boards/BP/GroupList/0'
        }

        JscbFactory.loadMainData(vconfig, function(vBpCategory, vBpGroupList) {
            delete vBpCategory[64];
            delete vBpCategory[65];
            $scope.BpCategory = vBpCategory;
            $scope.BpGroupList = vBpGroupList;
            console.log("BpCategory", vBpCategory);
            console.log("BpGroupList", vBpGroupList);
        });

        var getTeamList = function(vcat) {
            return $scope.BpGroupList;
            var result = []
            for (var i = 0; i < $scope.BpGroupList.length; i++) {
                var vitem = $scope.BpGroupList[i];
                if (vitem.BpCategoryId == vcat) {
                    var resitem = { name: vitem.GroupName }
                    result.push(vitem);
                }
            }
            return result;
        }

        $scope.jscb.reloadBoard = function() {
                $timeout(function() {
                    JscbFactory.showBoard($scope.jscb)
                }, 200);

            }
            // ===================================
            // var det = new Date("October 13, 2014 11:13:00");
            // var appd = new Date("December 12, 2015 11:13:00");
            // var res=[{
            //     ToyotaId:12345678,
            //     NoPolice:"B406DS",
            //     JalurAppointment:1,
            //     Name:"John",
            //     Address:"Jln Boulevard",
            //     Telp:"021-987865",
            //     NoAppointment:"AP-1234567890",
            //     Model:"Avanza",
            //     Colour:"Hitam",
            //     DateBuild:det,
            //     RequestAppointmentDate:appd,
            //     Category:3,
            //     Source:"MRS"
            // },
            // {
            //     ToyotaId:42345678,
            //     NoAppointment:"AP-1234567891",
            //     JalurAppointment:2,
            //     NoPolice:"B502DO",
            //     Name:"Titor",
            //     Address:"Jln Boulevard",
            //     Telp:"021-3461237",
            //     Model:"Yaris",
            //     Colour:"Putih",
            //     DateBuild:det,
            //     Category:2,
            //     RequestAppointmentDate:appd,
            //     Source:"MRS"
            // }]
        $scope.filter.Type == "Langsung";

        $scope.appDate = true;


        var gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
            // '<a href="#" uib-tooltip="Buat" tooltip-placement="bottom" ng-click="grid.appScope.actView(row.entity)" style="">Appointment</a>'+
            '<a href="#" ng-click="grid.appScope.actView(row.entity)" ng-hide="row.entity.StatusCode == 0" uib-tooltip="Buat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-plus-circle" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.actCancel(row.entity)" ng-hide="row.entity.StatusCode == 0 || row.entity.StatusCode == undefined" uib-tooltip="Batal" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-times-circle-o" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.actReschedule(row.entity)" ng-hide="row.entity.StatusCode == 0 || row.entity.StatusCode == undefined" uib-tooltip="Reschedule" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>'
        $scope.cekType = function(item) {
                console.log(item)
                    // if(item == 'Langsung'){
                    //     $scope.gridActionButtonTemplate ='<div class="ui-grid-cell-contents">' +
                    //     '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Buat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-plus-circle" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>'+
                    // '</div>'
                    // }else{
                    //     $scope.gridActionButtonTemplate =gridLain;
                    // }
            }
            // added by sss on 2017-09-18
        $scope.selectUnit = function(data) {
            console.log("$scope.tmpUnt", data);
            $scope.tmpUnt = data;
            $scope.lmModel.UnitBy = data.Name;
        }
    

        $scope.JobIsWarranty = 0;
        $scope.tipePembayaranJob = null;
        $scope.selectTypeCust = function(data, model) {
            console.log("selectTypeCust ===>", data);

            // ---------------- cek kl dari warranty di ganti jd bukan warranty / sebaliknya ga blh ganti ------------------------ start

            if ($scope.copyAwalPembayaranJob != null && $scope.copyAwalPembayaranJob != undefined) {
                // berarti dia edit job dan mau ubah pembayaran
                if ($scope.copyAwalPembayaranJob == 30) {
                    if (model.PaidById != 30) {
                        model.PaidBy = "Warranty"
                        model.PaidById = 30
                        // data.MasterId = 30
                        bsAlert.warning('Tidak dapat mengganti pembayaran job warranty ke non-warranty, silahkan hapus & input baru.')
                    }
                } else if ($scope.copyAwalPembayaranJob != 30 && $scope.copyAwalPembayaranJob != null) {
                    if (model.PaidById == 30) {
                        // model.PaidBy = "Warranty"
                        _.map($scope.paymentData, function(val) {
                            if (val.MasterId == $scope.copyAwalPembayaranJob) {
                                model.PaidBy = angular.copy(val.Name)
                                // data.Name = angular.copy(val.Name)
                            }
                        });
                        model.PaidById = $scope.copyAwalPembayaranJob
                        // data.MasterId = $scope.copyAwalPembayaranJob
                        
                        bsAlert.warning('Tidak dapat mengganti pembayaran job non-warranty ke warranty, silahkan hapus & input baru.')
                    }
                }
            } else {
                var cekpembayaranawal = angular.copy($scope.lmModel.PaidBy)
                var bedapembayaran_W = 0;

                if (cekpembayaranawal == 'Warranty' && model.PaidById != 30){
                    // ini dari warranty di ganti jd bukan warranty
                    model.TaskName = null
                    model.FlatRate = null
                    model.Fare = 0
                    model.FareMask = 0
                    model.UOM = null
                    model.Discount = 0
                    model.DiscountTypeId = -1
                    model.DiscountTypeListId = null
                    model.UnitBy = "Panel"
                    model.TFirst1 = null
                    model.child = []
                    model.WarrantyRate = null

                    bedapembayaran_W = 1
                }

                if (cekpembayaranawal != 'Warranty' && model.PaidById == 30){
                    // ini dari bukan warranty di ganti jd warranty
                    model.TaskName = null
                    model.FlatRate = null
                    model.Fare = 0
                    model.FareMask = 0
                    model.UOM = null
                    model.Discount = 0
                    model.DiscountTypeId = -1
                    model.DiscountTypeListId = null
                    model.UnitBy = "Panel"
                    model.TFirst1 = null
                    model.child = []
                    model.WarrantyRate = null

                    bedapembayaran_W = 1
                }
            }

            // ---------------- cek kl dari warranty di ganti jd bukan warranty / sebaliknya ga blh ganti ------------------------ end

            
            $scope.tmpCus = data;
            $scope.lmModel.PaidBy = data.Name;

            _.map($scope.paymentData, function(val) {
                if (val.MasterId == model.PaidById) {
                    $scope.lmModel.PaidBy = angular.copy(val.Name)
                    model.PaidBy = angular.copy(val.Name)
                }
            });

            // $scope.tipePembayaranJob = data.MasterId;
            $scope.tipePembayaranJob = model.PaidById

            console.log('tipe bayar', $scope.tipePembayaranJob)

            if (model.PaidById == 30) {
                $scope.JobIsWarranty = 1;
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }]; // kl pekerjaan twc / pwc ga blh pilih bahan
                console.log('cek ldmodel', $scope.ldModel)
                $scope.ldModel.MaterialTypeId = null;
            } else {
                $scope.JobIsWarranty = 0;
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
                model.TFirst1 = null
                model.WarrantyRate = null
            }

            if (bedapembayaran_W == 1){
                $scope.listApi.addDetail(false, model);
                $scope.listApi.clearDetail();
                return
                
            }
            

            $scope.listApi.addDetail(false, model);

        };
        $scope.selectTypeUnitParts = function(data) {
            console.log("selectTypeUnitParts", data);
            $scope.tmpUnit = data;
            $scope.ldModel.satuanName = data.Name;
        };
        //
        $scope.disableBro = function(index) {
            $scope.disableDate = true;
            console.log('disableDate', $scope.disableDate);

            $scope.MRS.category = 2;
            var category = 2;
            var VIN = index;
            var startDate = '-';
            var endDate = '-';
            var param = 1; // tab riwayat servis

            if ($scope.mDataDetail !== undefined && (index !== undefined && index !== 1 && index !== 0 && index !== null)) {
                // if($scope.mDataDetail !== undefined){
                if ($scope.mDataDetail.VIN !== null && $scope.mDataDetail.VIN !== undefined) {
                    $scope.ServiceHistory(category, VIN, startDate, endDate, param);
                }
            }
            // $scope.ServiceHistory(category, VIN, startDate, endDate);
        };
        $scope.mDataVehicleHistory = [];
        $scope.change = function(startDate, endDate, category, vin) {

            console.log("start Date 1", startDate);
            console.log("start Date 2", endDate);
            category = category.toString();
            var copyAngular = angular.copy(category);
            console.log("category", category);
            console.log("Copycategory", copyAngular);
            console.log("vin", vin);
            if (category == "" || category == null || category == undefined) {
                bsNotify.show({
                    size: 'big',
                    title: 'Mohon Isi Kategori Pekerjaan',
                    text: 'I will close in 2 seconds.',
                    timeout: 2000,
                    type: 'danger'
                });
            } else {

                if (startDate == undefined || startDate == "" || startDate == null || startDate == "Invalid Date") {
                    bsNotify.show({
                        size: 'big',
                        title: 'Mohon Isi periode servis',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                } else {

                    var tmpDate = startDate;
                    var tmpMonth = startDate;
                    console.log("tmpMonth", tmpMonth);
                    var tmpTime = $scope.startDate;
                    // if (tmpDate.getMonth() < 10) {
                    // tmpMonth = "0" + (tmpDate.getMonth() + 1);
                    // } else {
                    tmpMonth = (tmpDate.getMonth() + 1).toString();
                    // }
                    var ini = tmpDate.getFullYear() + "-" + (tmpMonth[1] ? tmpMonth : "0" + tmpMonth[0]) + "-" + tmpDate.getDate();
                    var d = new Date(ini);
                    console.log('data yang diinginkan', ini);
                    startDate = ini;
                };


                if (endDate == undefined || endDate == "" || endDate == null || endDate == "Invalid Date") {
                    bsNotify.show({
                        size: 'big',
                        title: 'Mohon Isi periode servis',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                } else {

                    var tmpDate2 = endDate;
                    var tmpMonth2 = endDate;
                    console.log("tmpMonth2", tmpMonth2);
                    var tmpTime2 = endDate;

                    // if (tmpDate2.getMonth() < 10) {
                    //     tmpMonth2 = "0" + (tmpDate2.getMonth() + 1);
                    // } else {
                    tmpMonth2 = (tmpDate2.getMonth() + 1).toString();
                    // }
                    var ini2 = tmpDate2.getFullYear() + "-" + (tmpMonth2[1] ? tmpMonth2 : "0" + tmpMonth2[0]) + "-" + tmpDate2.getDate();
                    var d2 = new Date(ini2);
                    console.log('data yang diinginkan 2', ini2);
                    endDate = ini2;
                };

                if (typeof vin !== "undefined") {
                    category = parseInt(category);
                    $scope.ServiceHistory(category, vin, startDate, endDate);
                } else {
                    bsNotify.show({
                        size: 'big',
                        title: 'VIN Tidak Ditemukan',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                };
            };
        };

        $scope.ServiceHistory = function(category, vin, startDate, endDate, param) {
            VehicleHistory.getData(category, vin, startDate, endDate).then(
                function(res) {
                    if (res.data.length == 0) {
                        $scope.mDataVehicleHistory = [];
                        bsNotify.show({
                            size: 'big',
                            title: 'Data Riwayat Service Tidak Ditemukan',
                            text: 'I will close in 2 seconds.',
                            timeout: 2000,
                            type: 'danger'
                        });
                    } else {
                        res.data.sort(function(a, b) {
                            // Turn your strings into dates, and then subtract them
                            // to get a value that is either negative, positive, or zero.
                            return new Date(b.ServiceDate) - new Date(a.ServiceDate);
                        });

                        for(var i in res.data){
                            var tmpArray = [];
                            for(var j = 0; j<res.data[i].VehicleJobService.length; j++){
                                if(res.data[i].VehicleJobService[j].KTPNo == null || res.data[i].VehicleJobService[j].KTPNo == undefined){
                                    res.data[i].VehicleJobService[j].KTPNo = ''
                                }
                                if(!(res.data[i].VehicleJobService[j].KTPNo.includes('/'))){
                                    if(res.data[i].VehicleJobService[j].EmployeeName !== null){
                                        tmpArray.push(res.data[i].VehicleJobService[j].EmployeeName);
                                    }
                                }
                                
                            }
                            tmpArray = $scope.getUnique(tmpArray);
                            res.data[i].namaTeknisi = tmpArray.join(', ');
                        }
                        $scope.mDataVehicleHistory = res.data;
                        console.log("kesini ga?");
                        $scope.value = angular.copy($scope.mDataVehicleHistory);

                        $scope.MRS.category = category;

                        if (param == 1) { // tab riwayat servis

                            var dateEnd = $scope.mDataVehicleHistory[0].ServiceDate;
                            $scope.MRS.endDate = dateEnd;

                            var terakhir = $scope.mDataVehicleHistory.length;
                            $scope.MRS.startDate = $scope.mDataVehicleHistory[terakhir - 1].ServiceDate;

                        }
                        console.log('category', $scope.MRS.category);
                        console.log('startDate', $scope.MRS.startDate);
                        console.log('EndDate', $scope.MRS.endDate);
                    };

                    console.log("mDataVehicleHistory=> Followup ", $scope.mDataVehicleHistory);
                },
                function(err) {}
            );
        }

        //
        $scope.activeWAC = 0;
        $scope.activeJustified = 0;
        $scope.inisudahTerdaftar = false;
        $scope.actView = function(data) {
            console.log('ini pas actView');
            $scope.getPPN();

            $scope.inisudahTerdaftar = true;
            
            $scope.loadingSimpan = 0


            

            // bersihin data yang tertinggal
            $scope.DiscountParts           = 0;
            $scope.DiscountTask            = 0;
            $scope.totalMaterialDiscounted = 0;
            $scope.subTotalMaterialSummary = 0;
            $scope.totalPPN                = 0;
            $scope.totalEstimasi           = 0;
            $scope.subTotalWorkSummary     = 0;
            $scope.totalWorkDiscounted     = 0;
            // bersihin data yang tertinggal
            


            MasterDiscountFactory.getDataAktif().then(
                function (res) {
                    console.log("res dis | ", res.data.Result, res.data.Result.length);
                    $scope.MasterDiscountBooking = res.data.Result;
                },
                function (err) { }
            );


            // if($scope.MasterDiscountBooking.length !== 2){
            //     $scope.bsAlertConfirm("Master Diskon Booking Belum di Input").then(function (res) {
            //         if (res) {
            //             console.log('ini kalo klik ok',res);
            //         }else{
            //             console.log("ini kalo klik cancle", res);
            //         }
            //     });
            // }else{
                //jancok
                $scope.restart_digit_odo = 0
                $scope.isKmHistory = 0
                $scope.nilaiKM = 0
                $scope.KMTerakhir = 0

                $scope.restart_digit_odo = 0;
                $scope.model_info_km.show = false;

                $scope.estHoursWork = 0;
                $scope.estDaysWork = 0;
                $scope.mDataVehicleHistory = [];
                $scope.MRS = {};
                console.log("sukses", data);
                var Apppoint = angular.copy(data);
                $scope.mData = {};
                $scope.dataTowass = []
                gridTemp = [];
                $scope.ShowEdit = true;
                $scope.EnableEdit = true;
                $scope.ShowEditInstitusi = true;
                $scope.EnableEditInstitusi = true;
                $scope.EnableEditContactPerson = true;
                $scope.JobComplaint = [];
                $scope.JobRequest = [];
                $scope.gridEstimation.data = [];
                $scope.gridWork = gridTemp;
                $scope.totalWork = 0;
                $scope.totalMaterial = 0;
                $scope.totalEstimasi =0;
                $scope.totalPPN = 0;
                $scope.subTotalMaterialSummary = 0;
                $scope.subTotalWorkSummary = 0;
                $scope.totalWorkDiscounted = 0;
                $scope.activeWAC = 0;
                $scope.estHours = null;
                $scope.estDays = null;
                $scope.getCalculatedTimeEst = [];
                $scope.activeJustified = 0;
                if (data.JobId !== undefined || data.JobId !== null) {
                    $scope.mData.ContactPerson = data.ContactPerson;
                    $scope.mData.PhoneContactPerson1 = parseInt(data.PhoneContactPerson1);
                    $scope.dataForWAC(data);
                };
                AppointmentBpService.getDataVehicle(data.VehicleId).then(function(ress) {
                    var dataDetail = ress.data.Result[0];
                    $scope.mDataDetail = dataDetail;
                    AppointmentBpService.getDataVIN($scope.mDataDetail.VIN).then(function(res){
                        console.log('cek input rangka', $scope.mDataDetail.VIN)
                        $scope.tempVIN = res.data.Result[0];
                        if ($scope.tempVIN != null && $scope.tempVIN != undefined) {
                            if ($scope.tempVIN.DEC_Date != null && $scope.tempVIN.DEC_Date != undefined) {
                                $scope.tempVIN.DEC_Date = $filter('date')($scope.tempVIN.DEC_Date, 'dd/MM/yyyy')
                            }else{
                                $scope.tempVIN.DEC_Date = '-'
                            }
                        }
                        console.log('cek res input', $scope.tempVIN)
                    })

                    AppointmentBpService.listHistoryClaim($scope.mDataDetail.VIN).then(function(res){
                        $scope.listClaim = res.data.data;
                        $scope.tempListClaim = [];
                        console.log('cek list claim', res)
                        console.log('list claim', res.data.data)
                        console.log('list history claim', $scope.tempListClaim)
                        for (var i = 0; i < $scope.listClaim.historyClaimDealerList.length; i++) {
                            $scope.tempListClaim.push($scope.listClaim.historyClaimDealerList[i]);
                        }
                    })

                    AppointmentBpService.getTowass($scope.mDataDetail.VIN).then(function(res) {
                        console.log("data Towasssssss", res.data);
                        $scope.dataTowass = res.data.Result;
                    });

                    $scope.getAllParameter(dataDetail);
                    if (dataDetail.Email == null) {
                        $scope.disableEmail(false);
                    } else {
                        // $scope.disableEmail(true); // dikomen dl krn email gtw jln gtw ga
                        $scope.disableEmail(false);

                    };
                    console.log("$scope.mDataDetail", $scope.mDataDetail);
                    // AppointmentBpService.getDataTaskList($scope.mDataDetail.KatashikiCode).then(function(result){
                    //     console.log("result",result);
                    // });
                    AppointmentGrService.getDataContactPerson($scope.mDataDetail.CustomerVehicleId).then(function(res) {
                        // console.log("List Province====>",res.data.Result);
                        $scope.ContactPersonData = res.data.Result;
                        // $scope.ShowEdit = false;
                        // $scope.EnableEditContactPerson = false;
                        // console.log("Data ContactPerson", $scope.ContactPersonData);
                        AppointmentBpService.CheckLastContactPerson($scope.mDataDetail.VehicleId, 1).then(function(res1) {
                            if (res1.data.length > 0) {
                                $scope.mData.ContactPerson = res1.data[0].ContactPerson;
                                $scope.mData.PhoneContactPerson1 = res1.data[0].PhoneContactPerson1;
                            }
                        })
                    })

                    WO.getDataPKS($scope.mDataDetail.LicensePlate).then(function(res) {
                        $scope.dataPKS = res.data.Result;
                        console.log("PKS", $scope.dataPKS);
                    });

                    var vin = dataDetail.VIN;
                    if (vin === null || vin === undefined || vin === "") {
                        console.log("Tidak ada VIN");

                    } else {

                        console.log('CEK SERVIS ====>');
                        //CEK SERVICE HISTORY
                        var category = 2; // KATEGORI BP
                        var VIN = vin;
                        var startDate = '1900-01-1';
                        var endDate = new Date();
    
                        var d = endDate.getDate();
                        var m = endDate.getMonth() + 1;
                        var y = endDate.getFullYear();
    
                        endDate = y + '-' + m + '-' + d;
                        console.log('endDate ==>', endDate);
                        $scope.CekServiceHistory(category, VIN, startDate, endDate);
                    }
                });
                AppointmentGrService.getDataVehiclePSFU(data.VehicleId).then(function(resu) {
                    console.log("getDataVehiclePSFU", resu.data.Result);
                    var dataQuestionAnswerPSFU = resu.data.Result[0];
                    //------- validasi tgl riwayat servis -----------------------
                    var minTglDEC = new Date(dataQuestionAnswerPSFU.DECDate);
                    console.log("minTglDEC", minTglDEC);
                    $scope.DateOptionsHistory.minDate = minTglDEC.setDate(minTglDEC.getDate() + 1);
                    var maxTglDEC = new Date();
                    console.log("maxTglDEC", maxTglDEC);
                    $scope.DateOptionsHistory.maxDate = maxTglDEC;
                    //------------------------------------------------------------
    
                    AppointmentBpService.getQuestionPSFU(data.VehicleId).then(function(resgg){
                        console.log('suma suma', resgg.data)
                        var realDataQnAPSFU = resgg.data

                        FollowupService.getQuestion().then(function(resu2) {
                            console.log("getQuestion", resu2.data.Result);
                            var dataQuestionAnswer = resu2.data.Result;
                            var arr = [];

                            for (var i=0; i<realDataQnAPSFU.length; i++){
                                var isiReal = realDataQnAPSFU[i];
                                var obj = {};
                                for (var j=0; j<dataQuestionAnswer.length; j++){
                                    var val = dataQuestionAnswer[j]
                                    if(isiReal.QuestionId == val.QuestionId){
                                        obj.QuestionId = val.QuestionId;
                                        obj.Description = val.Description;
                                        obj.Answer = [];
                                        for (var k=0; k<val.Answer.length; k++){
                                            if(isiReal.AnswerId == val.Answer[k].AnswerId){
                                                obj.Answer.push(val.Answer[k])
                                            }
                                        }
                                        obj.Reason = isiReal.AnswerReason ? isiReal.AnswerReason : null;
                                        obj.ReasonStatus = isiReal.AnswerReason ? false : true;
                                        obj.jawaban = isiReal.AnswerId
                                        arr.push(obj);
                                    }
                                }
                            }

    
                            // for (var i=0; i<dataQuestionAnswer.length; i++){
                            //     var obj = {};
                            //     var val = dataQuestionAnswer[i]
                            //     if (dataQuestionAnswerPSFU['QuestionId' + (i+1)] != undefined){
                            //         if (dataQuestionAnswerPSFU['QuestionId' + (i+1)] == val.QuestionId){
                            //             obj.QuestionId = val.QuestionId;
                            //             obj.Description = val.Description;
                            //             // obj.Answer = val.Answer; // karena ga bs di edit jg, lgsg aja bind data yg di get
                            //             obj.Answer = [];
                            //             for (var j=0; j<val.Answer.length; j++){
                            //                 if (dataQuestionAnswerPSFU['AnswerId' + (i+1)] != undefined && dataQuestionAnswerPSFU['AnswerId' + (i+1)] != null){
                            //                     if (dataQuestionAnswerPSFU['AnswerId' + (i+1)] == val.Answer[j].AnswerId){
                            //                         obj.Answer.push(val.Answer[j]);
                            //                     }
                            //                 }
                            //             }
                            //             obj.Reason = dataQuestionAnswerPSFU['Reason' + (i+1)] ? dataQuestionAnswerPSFU['Reason' + (i+1)] : null;
                            //             obj.ReasonStatus = dataQuestionAnswerPSFU['Reason' + (i+1)] ? false : true;
                            //             arr.push(obj);
                            //         }
                            //     }
                            // }
                            console.log('arr >>>', arr);
                            $scope.Question = arr;
                        })

                    })
                    

                });
                MrsList.getDataModel().then(function(res) {
                    console.log("result : ", res.data.Result);
                    $scope.modelData = res.data.Result;
                });
                //agar muncul PKS, Field Action Dll
                $scope.vehicleStatus = 0;
                $scope.checkAsb = false;
                $scope.formApi.setMode('detail');
                $scope.mData = Apppoint;
                // added by sss 2017-09-19
                if ($scope.gridEstimation.data.length > 0) {
                    $scope.gridEstimation.data = [];
                }
                $('#layoutWAC').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');

                // =============================Get=============
            // }

            
        }
        var DataPersonal = {};
        $scope.getAllParameter = function(item) {
                console.log("itemnya", item);

                DataPersonal = item;
                AppointmentBpService.getPayment().then(function(res) {
                    console.log("resPay", res.data.Result);
                    // pembayaran free service mau di ilangin di bp kata BU nya
                    for (var j=0; j<res.data.Result.length; j++){
                        if (res.data.Result[j].MasterId == 2277){
                            res.data.Result.splice(j,1);
                        }
                    }
                    $scope.paymentData = res.data.Result;
                    $scope.paymentDataNoWarranty = angular.copy(res.data.Result);
                    for (var i=0; i<$scope.paymentDataNoWarranty.length; i++){
                        if ($scope.paymentDataNoWarranty[i].Name == 'Warranty'){
                            $scope.paymentDataNoWarranty.splice(i,1);
                        }
                    }
                });
                AppointmentBpService.getUnitMeasurement().then(function(res) {
                    console.log("resUnit", res.data.Result);
                    $scope.unitData = res.data.Result;
                });
                AppointmentBpService.getWoCategory().then(function(res) {
                    console.log("resWOCat", res.data.Result);
                    $scope.woCategory = res.data.Result;
                });
                AppointmentBpService.getTaskCategory().then(function(res) {
                    console.log("resTaskCat", res.data.Result);
                    $scope.taskCategory = res.data.Result;
                });
                AppointmentBpService.getDamage().then(function(res) {
                    console.log('DamageVehicle ===>',res.data.Result)
                    $scope.DamageVehicle = res.data.Result;
                    _.map($scope.DamageVehicle, function(val) {
                        val.Id = val.MasterId;
                    })
                }),
                AppointmentBpService.getDataGender().then(function(res) {
                    console.log("result gender : ", res.data.Result);
                    var temp = res.data.Result;
                    for (var i = 0; i < temp.length; i++) {
                        temp[i].value = temp[i].CustomerGenderId;
                        temp[i].text = (temp[i].CustomerGenderName == "Laki-Laki" ? "L" : "P");
                    }
                    console.log("tmp", temp);
                    $scope.genderData = temp;
                });
                AppointmentBpService.getInsurence().then(function(res) {
                    console.log("res getInsurence", res.data);
                    for(var a in res.data){
                        if(res.data[a].DiscountJasa == null){
                            res.data[a].DiscountJasa = 0;
                        }
                        
                        if(res.data[a].DiscountPart == null){
                            res.data[a].DiscountPart = 0;
                        }
                    }
                    $scope.Asuransi = res.data;
                });
                MrsList.getDataCusRole().then(function(res) {
                    console.log("result role : ", res.data.Result);
                    $scope.RoleData = res.data.Result;
                });
                MrsList.getCCustomerCategory().then(function(res) {
                    $scope.CustomerTypeData = res.data.Result;
                });
                // $scope.CustomerTypeData = [{CustomerTypeId:1,CustomerTypeDesc:"Individu"},{CustomerTypeId:2,CustomerTypeDesc:"Institusi"}]
                // MrsList.getDataCusType().then(function(res) {
                //     console.log("result type : ",res.data);
                //     $scope.CustomerTypeData = res.data.Result;
                // });
                // MrsList.getDataModel().then(function(res) {
                //     console.log("result : ",res.data.Result);
                //     $scope.modelData = res.data.Result;
                // });
                if (item !== undefined) {
                    if (item.ProvinceId !== undefined) {
                        // MrsList.getDataProvinceById(item.ProvinceId).then(function(res) {
                        //     console.log("result province : ", res.data.Result);
                        //     $scope.ProvinceData = res.data.Result;
                        // });
                        WOBP.getMLocationProvince().then(function(res) {
                            console.log("List Province====>", res.data.Result);
                            $scope.ProvinceData = res.data.Result;
                        })
                    }
                    if (item.CityRegencyId !== undefined) {
                        // MrsList.getDataCityById(item.CityRegencyId).then(function(res) {
                        //     console.log("result city : ", res.data.Result);
                        //     $scope.CityRegencyData = res.data.Result;
                        // });
                        WOBP.getMLocationCityRegency(item.ProvinceId).then(function(resu) {
                            $scope.CityRegencyData = resu.data.Result;
                        });
                    }
                    if (item.DistrictId !== undefined) {
                        // MrsList.getDataDistrictById(item.DistrictId).then(function(res) {
                        //     console.log("result district : ", res.data.Result);
                        //     $scope.DistrictData = res.data.Result;
                        // });
                        WOBP.getMLocationKecamatan(item.CityRegencyId).then(function(resu) {
                            $scope.DistrictData = resu.data.Result;
                        });
                    }
                    if (item.VillageId !== undefined) {
                        // MrsList.getDataVillageById(item.VillageId).then(function(res) {
                        //     console.log("result village : ", res.data.Result);
                        //     $scope.VillageData = res.data.Result;
                        // });
                        WOBP.getMLocationKelurahan(item.DistrictId).then(function(resu) {
                            $scope.VillageData = resu.data.Result;
                        });
                    }
                    if (item.GasTypeId !== null || item.GasTypeId !== undefined) {
                        $scope.mDataDetail.VehicleGasTypeId = item.GasTypeId;
                    }
                    if (item.VIN != null) {
                        AppointmentBpService.getTowass(item.VIN).then(function(res) {
                            console.log("data Towasssssss", res.data);
                            $scope.dataTowass = res.data.Result;

                            // var isLengthOpeNo = res.data.Result.length;
                            // if (isLengthOpeNo > 0) {

                            //     var category = 0;   // KATEGORI BP
                            //     var vin = item.VIN;
                            //     var startDate = '2001-01-1';
                            //     var endDate = new Date();

                            //     var d = endDate.getDate();
                            //     var m = endDate.getMonth() + 1;
                            //     var y = endDate.getFullYear();

                            //     endDate = y + '-' + m + '-' + d;
                            //     console.log('endDate ==>', endDate);

                            //     VehicleHistory.getData(category, vin, startDate, endDate).then(
                            //         function (resu) {

                            //             console.log('DATA HISTORY TOWASS ====>', resu.data);
                            //             $scope.mDataVehicleHistory = resu.data;
                            //             var isLengthHistory = $scope.mDataVehicleHistory.length;

                            //             if (isLengthHistory != 0) {
                            //                 console.log("panjang isLengthHistory", isLengthHistory);

                            //                 for (var k = 0; k < isLengthOpeNo; k++) {
                            //                     console.log("k ==>", k);
                            //                     $scope.OpeNo = res.data.Result[k].OpeNo;
                            //                     console.log("$scope.OpeNo", $scope.OpeNo);

                            //                     for (var i = 0; i < isLengthHistory; i++) {

                            //                         console.log("i ==>", i);
                            //                         var isLengthVehicleJob = $scope.mDataVehicleHistory[i].VehicleJobService.length;
                            //                         console.log("panjang isLengthVehicleJob", isLengthVehicleJob);

                            //                         if (isLengthVehicleJob > 0) {

                            //                             for (var j = 0; j < isLengthVehicleJob; j++) {

                            //                                 console.log("j ==>", j);
                            //                                 var jobdesc = $scope.mDataVehicleHistory[i].VehicleJobService[j].JobDescription

                            //                                 if (jobdesc.includes($scope.OpeNo)) {
                            //                                     $scope.dataTowass = [];
                            //                                     console.log('masuk includes ===>');
                            //                                     console.log("$scope.OpeNo", $scope.OpeNo);
                            //                                     console.log("jobdesc", jobdesc);

                            //                                 } else {
                            //                                     console.log('tidak masuk includes ===>');
                            //                                 }
                            //                             }
                            //                         }

                            //                     }
                            //                 }

                            //             } else {

                            //                 $scope.dataTowass = res.data.Result;
                            //                 console.log('$scope.dataTowass ELSE =>', $scope.dataTowass);
                            //             }
                            //         },
                            //         function (err) { }
                            //     );
                            //     console.log('isLengthOpeNo ==>', isLengthOpeNo);
                            // }
                        });
                    }
                }

                MrsList.getInvitationMedia().then(function(res) {
                    console.log("result media : ", res.data.Result);
                    $scope.InvMediaData = res.data.Result;
                });
                MrsList.getMrsInvitation().then(function(res) {
                    console.log("result mrs invit : ", res.data.Result);
                    $scope.MrsInvData = res.data.Result;
                });
                // =======================
                WOBP.getDataWAC().then(function(resu) {
                    console.log('cek data WAC', resu.data);
                    $scope.headerWAC = resu.data;
                });
                // ------- Model
                // AppointmentBpService.getDataModel().then(function(res) {
                //     $scope.modelData = res.data.Result;
                // });
            }
            // =============================
        $scope.actReschedule = function(item) {
            console.log("itemm", item);
            if ($scope.filter.Type !== "Langsung") {
                $scope.show_modal.show = !$scope.show_modal.show;
                $scope.modal_model = angular.copy(item);
            }
        }
        $scope.actCancel = function(item) {
            if ($scope.filter.Type !== "Langsung") {
                $scope.modal_model = angular.copy(item);
                $scope.showModalReason = true;
                // bsAlert.alert({
                //         title: "Apakah anda yakin untuk membatalkan request appointment?",
                //         text: "descripsi",
                //         type: "warning",
                //         showCancelButton: true
                //     },
                //     function() {
                //         $scope.cancelAppointment(item);
                //     },
                //     function() {

                //     }
                // )
            }
        }
        $scope.doCancelAppointment = function(item) {
            console.log("doCancelAppointment Item", item);
            if (item.Reason !== undefined) {
                $scope.cancelAppointment(item);
                $scope.showModalReason = false;
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Data masih belum terisi",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            }
        };
        $scope.doBackAppointment = function(item) {
            console.log('itemcancel', item);
            $scope.showModalReason = false;
        }
        $scope.actReschedule = function(item) {
                $scope.show_modal.show = !$scope.show_modal.show;
                $scope.modal_model = angular.copy(item);
                // console.log("$scope.modal_model",$scope.modal_model);
                // console.log("item",item);
            }
            // $scope.actCancel = function(item) {
            //     bsAlert.alert({
            //             title: "Apakah anda yakin untuk membatalkan request appointment?",
            //             text: "descripsi",
            //             type: "warning",
            //             showCancelButton: true
            //         },
            //         function() {
            //             $scope.cancelAppointment(item);
            //         },
            //         function() {

        //         }
        //     )
        // }
        $scope.disableEmail = function(param) {
            _.map($scope.actionButtonSettingsDetail, function(val) {
                if (val.title === 'Simpan & Kirim Email') {
                    val.visible = param;
                }
            });
        }
        $scope.cancelAppointment = function(item) {
            console.log("itemnya", item);
            AppointmentBpService.cancelRequest([item.Id]).then(function() {
                $scope.getData(true);
            });
        }
        $scope.rescheduleSave = function(item) {
            // console.log('itemini',item);
            AppointmentBpService.rescheduleRequest(item).then(function() {
                $scope.getData(true);
                // $scope.show_modal.show = !$scope.show_modal.show;
                $scope.show_modal.show = false;
            });
        }
        $scope.rescheduleCancel = function(item) {
            console.log('itemcancel', item);
            // $scope.show_modal.show = !$scope.show_modal.show;
            $scope.show_modal.show = false;
        }
        $scope.saveApprovalDP = function(data) {
            AppointmentBpService.getWobyJobId(data.JobId).then(function(res) {
                var tmpDataForApprovalParts = [];
                var tmpApproval = res.data.Result[0];
                console.log("sukses tmpApproval", tmpApproval);
                // var tmpDataForApprovalPart = tmpDataForApproval.JobParts;
                for (var k in tmpApproval.JobTask) {
                    for (var i in tmpApproval.JobTask[k].JobParts) {
                        if (tmpApproval.JobTask[k].JobParts[i].DownPayment < tmpApproval.JobTask[k].JobParts[i].minimalDp) {
                            console.log("mpApproval.JobTask[k].JobParts[i]", tmpApproval.JobTask[k].JobParts[i]);
                            tmpDataForApprovalParts.push({
                                JobId: data.JobId,
                                DPDefault: tmpApproval.JobTask[k].JobParts[i].minimalDp,
                                DPRequested: tmpApproval.JobTask[k].JobParts[i].DownPayment,
                                JobPartsId: tmpApproval.JobTask[k].JobParts[i].JobPartsId,
                                ApprovalCategoryId: 41,
                                ApproverId: null,
                                RequesterId: null,
                                ApproverRoleId: data.ApproverRoleId,
                                RequestorRoleId: $scope.user.RoleId,
                                RequestReason: "Request Pengurangan DP",
                                RequestDate: new Date(),
                                StatusApprovalId: 3,
                                StatusCode: 1,
                                VehicleTypeId: tmpApproval.VehicleTypeId
                            })
                        }
                    }
                }
                console.log("tmpDataForApprovalParts", tmpDataForApprovalParts);
                AppointmentBpService.postApprovalDp(tmpDataForApprovalParts).then(function(res) {
                    console.log("sukses oostapproval");
                });
            });
        }

        $scope.bsAlertConfirm = function (title, text) {
            var defer = $q.defer();
            bsAlert.alert({
                title: title || 'Appointment Service BP',
                text: text || '',
                type: "warning",
                showCancelButton: false
            },
                function () {
                    defer.resolve(true);
                },
                function () {
                    defer.resolve(false);
                }
            )
            return defer.promise;
        };

        MasterDiscountFactory.getDataAktif().then(
            function (res) {
                console.log("res dis | ", res.data.Result, res.data.Result.length);

                if(res.data.Result.length > 0){
                    $scope.MasterDiscountBooking = res.data.Result;
                    console.log('ini ada data master diskon bookingnya');
                }else{
                    $scope.MasterDiscountBooking = [];
                    console.log('ini ga data master diskon bookingnya');
                }
            },
            function (err) { }
        );


        $scope.bsAlertFunc = function(prompt, cb) {
            
            // bersihin data yang tertinggal
            $scope.DiscountParts           = 0;
            $scope.DiscountTask            = 0;
            $scope.totalMaterialDiscounted = 0;
            $scope.subTotalMaterialSummary = 0;
            $scope.totalPPN                = 0;
            $scope.totalEstimasi           = 0;
            $scope.subTotalWorkSummary     = 0;
            $scope.totalWorkDiscounted     = 0;
            // bersihin data yang tertinggal
            bsAlert.alert({
                    title: prompt,
                    text: "",
                    type: "warning",
                    showCancelButton: true
                },
                function() {
                    // console.log('$scope.MasterDiscountBooking ===>',$scope.MasterDiscountBooking, $scope.MasterDiscountBooking.length);
                    // if($scope.MasterDiscountBooking.length !== 2){
                    //     $scope.bsAlertConfirm("Master Diskon Booking Belum di Input").then(function (res) {
                    //         if (res) {
                    //             console.log('ini kalo klik ok',res);
                    //         }else{
                    //             console.log("ini kalo klik cancle", res);
                    //         }
                    //     });
                    // }else{

                        $scope.restart_digit_odo = 0
                        $scope.isKmHistory = 0
                        $scope.nilaiKM = 0
                        $scope.KMTerakhir = 0

                        $scope.restart_digit_odo = 0;
                        $scope.model_info_km.show = false;

                        $scope.estHoursWork = 0;
                        $scope.estDaysWork = 0;
                        $scope.vehicleStatus = 1;
                        // cb();
                        $scope.activeWAC = 0;
                        $scope.activeJustified = 0;
                        $scope.getAllParameter();
                        MrsList.getDataModel().then(function(res) {
                            console.log("result : ", res.data.Result);
                            $scope.modelData = res.data.Result;
                        });
                        var filter = $scope.filter.noPolice.toUpperCase();
                        if (filter.includes("*")) {
                            filter = filter.split('*');
                            filter = filter[0];
                        }
                        $scope.mDataDetail = {};
                        $scope.mDataDetail.LicensePlate = filter;
                        $scope.stallAsb = 0;
                        gridTemp = [];
                        $scope.mData = {};
                        $scope.dataTowass = []
                        $scope.JobComplaint = [];
                        $scope.JobRequest = [];
                        $scope.gridEstimation.data = [];
                        $scope.gridWork = gridTemp;
                        $scope.totalMaterial = 0;
                        $scope.totalWork = 0;
                        $scope.DisplayDiscountTask = 0;
                        $scope.DisplayDiscountParts = 0;

                        $scope.totalFinalTask = 0;
                        $scope.totalFinalParts = 0;
                        $scope.totalMaterialDiscounted = 0;
                        $scope.totalWorkDiscounted = 0;
                        $scope.PPN = 0;
                        $scope.totalEstimasi = 0;


                        $scope.totalDp = 0;
                        $scope.checkAsb = false;
                        $scope.mData.AppointmentSourceId = 1;
                        $scope.asb.endDate = new Date();
                        $scope.asb.endDate.setDate($scope.asb.endDate.getDate() + 7);
                        $("input[name='DateAppoint']").attr("disabled", "disabled");
                        $('#layoutWAC').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
                        $scope.formApi.setMode('detail');
                        $scope.noRef = 0;
                    // }
                    
                },
                function() {

                }
            )
        }
        $scope.actionButtonSettingsDetail = [
            // {
            //     title: 'Simpan Draft',
            //     icon: 'fa fa-fw fa-save',
            //     func: function(row, formScope) {
            //         console.log("mData", $scope.mData);
            //         // console.log("row",row);
            //         var tmpGrid = [];
            //         // added by sss on 2017-09-22
            //         var tlBPObj = { "JobTaskBP": {}, "Jobtasks": [] };
            //         //
            //         tmpGrid = angular.copy(gridTemp);
            //         // if(gridTemp.length > 0 && $scope.mData.AppointmentDate !== undefined && $scope.mData.ContactPerson !== undefined && $scope.mData.PhoneContactPerson1 !== undefined && $scope.mData.Category !== undefined && $scope.mData.Km !== undefined ){
            //         if ($scope.mData.AppointmentDate !== undefined && $scope.mData.ContactPerson !== undefined && $scope.mData.PhoneContactPerson1 !== undefined && $scope.mData.WoCategoryId !== undefined && $scope.mData.Km !== undefined) {
            //             var tmpKm = angular.copy($scope.mData.Km);
            //             var tmpKmComma = angular.copy($scope.mData.Km);
            //             if (tmpKm.toString().includes(",")) {
            //                 tmpKm = tmpKm.toString().split(',');
            //                 if (tmpKm.length > 1) {
            //                     tmpKm = tmpKm.join('');
            //                 } else {
            //                     tmpKm = tmpKm[0];
            //                 }
            //             }
            //             tmpKm = parseInt(tmpKm);
            //             $scope.mData.Km = tmpKm;
            //             for (var i = 0; i < tmpGrid.length; i++) {
            //                 tmpGrid[i].JobParts = [];
            //                 tmpGrid[i].FlatRate = 1;
            //                 angular.forEach(tmpGrid[i].child, function(v, k) {
            //                     tmpGrid[i].JobParts[k] = v;
            //                     delete tmpGrid[i].child;
            //                 })
            //             }
            //             console.log("=====>", tmpGrid);
            //             // added by sss on 2017-09-22
            //             tlBPObj = {
            //                 "JobTaskBP": {
            //                     "Body": 0,
            //                     "Putty": 0,
            //                     "Surfacer": 0,
            //                     "Spraying": 0,
            //                     "Polishing": 0,
            //                     "Reassembly": 0,
            //                     "FinalInspection": 0
            //                 },
            //                 "Jobtasks": []
            //             };

            //             for (var i in $scope.gridEstimation.data) {
            //                 tlBPObj["JobTaskBP"]["Body"] += parseInt($scope.gridEstimation.data[i]["BodyEstimationMinute"]);
            //                 tlBPObj["JobTaskBP"]["Putty"] += parseInt($scope.gridEstimation.data[i]["PuttyEstimationMinute"]);
            //                 tlBPObj["JobTaskBP"]["Surfacer"] += parseInt($scope.gridEstimation.data[i]["SurfacerEstimationMinute"]);
            //                 tlBPObj["JobTaskBP"]["Spraying"] += parseInt($scope.gridEstimation.data[i]["SprayingEstimationMinute"]);
            //                 tlBPObj["JobTaskBP"]["Polishing"] += parseInt($scope.gridEstimation.data[i]["PolishingEstimationMinute"]);
            //                 tlBPObj["JobTaskBP"]["Reassembly"] += parseInt($scope.gridEstimation.data[i]["ReassemblyEstimationMinute"]);
            //                 tlBPObj["JobTaskBP"]["FinalInspection"] += parseInt($scope.gridEstimation.data[i]["FIEstimationMinute"]);
            //             }
            //             console.log('tlBPObj', tlBPObj);
            //             //
            //             // added by sss
            //             for (var i in tmpGrid) {
            //                 // tmpGrid[i].PaidById = angular.copy(tmpGrid[i].Payment);
            //                 // tmpGrid[i].ProcessId = angular.copy(tmpGrid[i].Unit);
            //                 tmpGrid[i].BodyEstimationMinute = parseInt($scope.gridEstimation.data[i]["BodyEstimationMinute"]);
            //                 tmpGrid[i].PutyEstimationMinute = parseInt($scope.gridEstimation.data[i]["PuttyEstimationMinute"]);
            //                 tmpGrid[i].SurfacerEstimationMinute = parseInt($scope.gridEstimation.data[i]["SurfacerEstimationMinute"]);
            //                 tmpGrid[i].SprayingEstimationMinute = parseInt($scope.gridEstimation.data[i]["SprayingEstimationMinute"]);
            //                 tmpGrid[i].PolishingEstimationMinute = parseInt($scope.gridEstimation.data[i]["PolishingEstimationMinute"]);
            //                 tmpGrid[i].ReassemblyEstimationMinute = parseInt($scope.gridEstimation.data[i]["ReassemblyEstimationMinute"]);
            //                 tmpGrid[i].FIEstimationMinute = parseInt($scope.gridEstimation.data[i]["FIEstimationMinute"]);
            //                 delete tmpGrid[i].PaidBy;
            //                 delete tmpGrid[i].UnitBy;
            //                 for (var j in tmpGrid[i].JobParts) {
            //                     tmpGrid[i].JobParts[j].Price = tmpGrid[i].JobParts[j].RetailPrice;
            //                     tmpGrid[i].JobParts[j].DPRequest = parseInt(tmpGrid[i].JobParts[j].DownPayment);
            //                     delete tmpGrid[i].JobParts[j].ETA;
            //                     delete tmpGrid[i].JobParts[j].PaidBy;
            //                 }

            //             }
            //             //
            //             $scope.mData.JobTask = tmpGrid;
            //             $scope.mData.JobComplaint = $scope.JobComplaint;
            //             $scope.mData.JobRequest = $scope.JobRequest;
            //             $scope.mData.detail = $scope.mDataDetail;
            //             $scope.mData.totalDpDefault = $scope.totalDpDefault;
            //             $scope.mData.totalDp = $scope.totalDp;
            //             if ($scope.totalDp != 0) {
            //                 $scope.totalDp = $scope.totalDp.toString().replace(/,/g, '');
            //                 $scope.totalDp = parseInt($scope.totalDp);
            //             }
            //             $scope.mData.totalDpDefault = $scope.totalDpDefault;
            //             console.log("mData ======> ", $scope.mData);
            //             var tmpData = angular.copy($scope.mData);
            //             var tmpAppointmentDate = tmpData.AppointmentDate;
            //             tmpAppointmentDate = new Date(tmpAppointmentDate);
            //             var finalDate
            //             var yyyy = tmpAppointmentDate.getFullYear().toString();
            //             var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
            //             var dd = tmpAppointmentDate.getDate().toString();
            //             finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
            //             $scope.mData.AppointmentDate = finalDate;
            //             console.log("mData ======> AppointmentDate", $scope.mData);
            //             console.log("mData ======> finalDate", finalDate);
            //             AppointmentBpService.createAppointmentDraft($scope.mData).then(function(res) {
            //                 var tmpDataJob = angular.copy($scope.mData);
            //                 var tmpJobId = res.data.ResponseMessage;
            //                 tmpJobId = tmpJobId.split('#');
            //                 console.log("tmpJobId", tmpJobId[1]);
            //                 tmpDataJob.JobId = parseInt(tmpJobId[1]);
            //                 // added by sss on 2017-09-20
            //                 //remove by rzk on 2018-09-26 karena jadi double seharusan save saat bikin WO saja
            //                 // AppointmentBpService.createJobPlan(tmpJobId[1], tlBPObj).then(function(res) {
            //                 //     console.log("success Job Task BP..");
            //                 // });
            //                 //
            //                 console.log("abis save", res);
            //                 $scope.formApi.setMode('grid');
            //             }, function(err) {
            //                 $scope.mData.Km = tmpKmComma;
            //             });

            //         } else {
            //             bsAlert.warning("Form masih ada yang belum terisi", "silahkan cek kembali");
            //         }
            //     },
            //     type: 'custom' //for link
            // },
            {
                func: function(row, formScope) {
                    $scope.mData.JobTask = gridTemp;
                    console.log("mData", $scope.mData);

                },
                title: 'Simpan & Kirim Email',
                visible: $scope.disableEmail(false),
                icon: '',
                //rightsBit: 1
            },
            {
                func: function(row, formScope) {

                    console.log('click simpan aja terus', $scope.actionButtonSettingsDetail)

                    if ($scope.loadingSimpan > 0) {
                        bsNotify.show({
                            size: 'small',
                            type: 'danger',
                            title: "Data sedang proses simpan..",
                            // timeout: 1000,
                        });
                        return false;
                    } else {
                        $scope.loadingSimpan++
                        $timeout(function() {
                            $scope.loadingSimpan = 0
                        }, 30000);

                        console.log("mData simpan", $scope.mData);
                        // console.log("row",row);
                        var tmpGrid = [];
                        // added by sss on 2017-09-22
                        var tlBPObj = { "JobTaskBP": {}, "Jobtasks": [] };
                        //
                        tmpGrid = angular.copy(gridTemp);
                        // if(gridTemp.length > 0 && $scope.mData.AppointmentDate !== undefined && $scope.mData.ContactPerson !== undefined && $scope.mData.PhoneContactPerson1 !== undefined && $scope.mData.Category !== undefined && $scope.mData.Km !== undefined ){
                        if ($scope.mData.AppointmentDate !== undefined && $scope.mData.ContactPerson !== undefined && $scope.mData.PhoneContactPerson1 !== undefined && $scope.mData.WoCategoryId !== undefined && $scope.mData.Km !== undefined && $scope.mData.StallId !== 0 && $scope.gridWork.length !== 0 && ($scope.mDataDetail.LicensePlate !== null && $scope.mDataDetail.LicensePlate !== undefined && $scope.mDataDetail.LicensePlate !== '')) {
                            var tmpKm = angular.copy($scope.mData.Km);
                            var tmpKmComma = angular.copy($scope.mData.Km);
                            if (tmpKm.toString().includes(",")) {
                                tmpKm = tmpKm.toString().split(',');
                                if (tmpKm.length > 1) {
                                    tmpKm = tmpKm.join('');
                                } else {
                                    tmpKm = tmpKm[0];
                                }
                            }
                            tmpKm = parseInt(tmpKm);

                            if (tmpKm < $scope.KMTerakhir && $scope.KMTerakhir < 999999) {
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "KM tidak boleh lebih kecil dari " + $scope.KMTerakhir 
                                });
                                $scope.loadingSimpan = 0
                                return false;
                            }
                            
                            $scope.mData.Km = tmpKm;
                            for (var i = 0; i < tmpGrid.length; i++) {
                                tmpGrid[i].JobParts = [];
                                // tmpGrid[i].FlatRate = 1;
                                // tmpGrid[i].FlatRate = tmpGrid[i].FlatRate ? tmpGrid[i].FlatRate : 1;
                                tmpGrid[i].FlatRate = (tmpGrid[i].FlatRate == null || tmpGrid[i].FlatRate == undefined) ? 1 : tmpGrid[i].FlatRate


                                angular.forEach(tmpGrid[i].child, function(v, k) {
                                    tmpGrid[i].JobParts[k] = v;
                                    delete tmpGrid[i].child;
                                })
                            }
                            console.log("=====>", tmpGrid);
                            // added by sss on 2017-09-22
                            tlBPObj = {
                                "JobTaskBP": {
                                    "Body": 0,
                                    "Putty": 0,
                                    "Surfacer": 0,
                                    "Spraying": 0,
                                    "Polishing": 0,
                                    "Reassembly": 0,
                                    "FinalInspection": 0
                                },
                                "Jobtasks": []
                            };

                            for (var i in $scope.gridEstimation.data) {
                                tlBPObj["JobTaskBP"]["Body"] += parseInt($scope.gridEstimation.data[i]["BodyEstimationMinute"]);
                                tlBPObj["JobTaskBP"]["Putty"] += parseInt($scope.gridEstimation.data[i]["PuttyEstimationMinute"]);
                                tlBPObj["JobTaskBP"]["Surfacer"] += parseInt($scope.gridEstimation.data[i]["SurfacerEstimationMinute"]);
                                tlBPObj["JobTaskBP"]["Spraying"] += parseInt($scope.gridEstimation.data[i]["SprayingEstimationMinute"]);
                                tlBPObj["JobTaskBP"]["Polishing"] += parseInt($scope.gridEstimation.data[i]["PolishingEstimationMinute"]);
                                tlBPObj["JobTaskBP"]["Reassembly"] += parseInt($scope.gridEstimation.data[i]["ReassemblyEstimationMinute"]);
                                tlBPObj["JobTaskBP"]["FinalInspection"] += parseInt($scope.gridEstimation.data[i]["FIEstimationMinute"]);
                            }
                            console.log('tlBPObj', tlBPObj);
                            //
                            // added by sss
                            for (var i in tmpGrid) {
                                // tmpGrid[i].PaidById = angular.copy(tmpGrid[i].Payment);
                                // tmpGrid[i].ProcessId = angular.copy(tmpGrid[i].Unit);
                                tmpGrid[i].BodyEstimationMinute = parseInt($scope.gridEstimation.data[i]["BodyEstimationMinute"]);
                                tmpGrid[i].PutyEstimationMinute = parseInt($scope.gridEstimation.data[i]["PuttyEstimationMinute"]);
                                tmpGrid[i].SurfacerEstimationMinute = parseInt($scope.gridEstimation.data[i]["SurfacerEstimationMinute"]);
                                tmpGrid[i].SprayingEstimationMinute = parseInt($scope.gridEstimation.data[i]["SprayingEstimationMinute"]);
                                tmpGrid[i].PolishingEstimationMinute = parseInt($scope.gridEstimation.data[i]["PolishingEstimationMinute"]);
                                tmpGrid[i].ReassemblyEstimationMinute = parseInt($scope.gridEstimation.data[i]["ReassemblyEstimationMinute"]);
                                tmpGrid[i].FIEstimationMinute = parseInt($scope.gridEstimation.data[i]["FIEstimationMinute"]);
                                delete tmpGrid[i].PaidBy;
                                delete tmpGrid[i].UnitBy;
                                for (var j in tmpGrid[i].JobParts) {
                                    tmpGrid[i].JobParts[j].Price = tmpGrid[i].JobParts[j].RetailPrice;
                                    tmpGrid[i].JobParts[j].DPRequest = parseInt(tmpGrid[i].JobParts[j].DownPayment);
                                    delete tmpGrid[i].JobParts[j].ETA;
                                    delete tmpGrid[i].JobParts[j].PaidBy;
                                }

                            }
                            //


                            //pindahan dari factory
                            for(var i in tmpGrid){
                                // if(tmpGrid[i].PaidById == 2277 || tmpGrid[i].PaidById == 30 || tmpGrid[i].PaidById == 31 ||  tmpGrid[i].PaidById == 32){
                                if(tmpGrid[i].PaidById == 2277 || tmpGrid[i].PaidById == 30){
                                    tmpGrid[i].Discount = 0;
                                    tmpGrid[i].DiscountTypeId = -1;
                                    if(tmpGrid[i].JobParts.length > 0 || tmpGrid[i].JobPart !== null || tmpGrid[i].JobParts !== undefined ){
                                        for(var j in tmpGrid[i].JobParts){
                                            // if(tmpGrid[i].JobParts[j].PaidById == 2277 || tmpGrid[i].JobParts[j].PaidById == 30 || tmpGrid[i].JobParts[j].PaidById == 31 ||  tmpGrid[i].PaidById == 32){
                                            if(tmpGrid[i].JobParts[j].PaidById == 2277 || tmpGrid[i].JobParts[j].PaidById == 30){
                                                tmpGrid[i].JobParts[j].Discount = 0;
                                                tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                            }else{
                                                // if(tmpGrid[i].JobParts[j].MaterialTypeId == 1){
                                                // //    tmpGrid[i].JobParts[j].Discount = data.disParts;
                                                //     tmpGrid[i].JobParts[j].DiscountTypeId = 10;
                                                // }else{
                                                //     tmpGrid[i].JobParts[j].Discount = 0;
                                                //     tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                                // }

                                                //req by pak eko bahan tetap dapet diskon 08-juli-2020
                                                tmpGrid[i].JobParts[j].DiscountTypeId = 10;
                                            }
                                        }
                                    }
                                }else{
                                    // tmpGrid[i].Discount = data.disTask;
                                    tmpGrid[i].DiscountTypeId = 10;
                                    if(tmpGrid[i].JobParts.length > 0 || tmpGrid[i].JobPart !== null || tmpGrid[i].JobParts !== undefined ){
                                        for(var j in tmpGrid[i].JobParts){
                                            if(tmpGrid[i].JobParts[j].PaidById == 2277 || tmpGrid[i].JobParts[j].PaidById == 30){
                                                tmpGrid[i].JobParts[j].Discount = 0;
                                                tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                            }else{
                                                // if(tmpGrid[i].JobParts[j].MaterialTypeId == 1){
                                                // //    tmpGrid[i].JobParts[j].Discount = data.disParts;
                                                //     tmpGrid[i].JobParts[j].DiscountTypeId = 10;
                                                // }else{
                                                //     tmpGrid[i].JobParts[j].Discount = 0;
                                                //     tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                                // }

                                                //req by pak eko bahan tetap dapet diskon 08-juli-2020
                                                tmpGrid[i].JobParts[j].DiscountTypeId = 10;
                                            }
                                        }
                                    }
                                }
                            }
                            //pindahan dari factory

                            $scope.mData.JobTask = tmpGrid;
                            $scope.mData.JobComplaint = $scope.JobComplaint;
                            $scope.mData.JobRequest = $scope.JobRequest;
                            $scope.mData.detail = $scope.mDataDetail;
                            $scope.mData.totalDpDefault = $scope.totalDpDefault;
                            $scope.mData.totalDp = $scope.totalDp;
                            if ($scope.totalDp != 0) {
                                $scope.totalDp = $scope.totalDp.toString().replace(/,/g, '');
                                $scope.totalDp = parseInt($scope.totalDp);
                            }

                            
                            _.map($scope.Asuransi, function(val) {
                                if (val.InsuranceName === $scope.mData.InsuranceName) {
                                    $scope.mData.InsuranceId = val.InsuranceId;
                                    $scope.mData.InsuranceName = val.InsuranceName;
                                }
                            });

                            $scope.mData.FinalDP = $scope.totalDP;
                            console.log("mData FinalDP ======> ", $scope.mData.FinalDP);




                            var tmpData = angular.copy($scope.mData);
                            var tmpAppointmentDate = tmpData.AppointmentDate;
                            tmpAppointmentDate = new Date(tmpAppointmentDate);
                            var finalDate
                            var yyyy = tmpAppointmentDate.getFullYear().toString();
                            var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
                            var dd = tmpAppointmentDate.getDate().toString();
                            finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                            $scope.mData.AppointmentDate = finalDate;
                            console.log("mData ======> AppointmentDate 1", $scope.mData);
                            console.log("mData ======> finalDate", finalDate);
                            if ($scope.totalDpDefault !== 0 && ($scope.totalDp < $scope.totalDpDefault)) {
                                AppointmentBpService.checkApproval($scope.totalDp).then(function(res) {
                                    console.log("ressss checkApproval", res.data);
                                    var ApproverRoleID = res.data;
                                    if (res.data !== -1) {
                                        AppointmentBpService.createAppointment($scope.mData).then(function(res) {
                                            
                                            if (res.data == -1) {
                                                bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                                $scope.mData.Km = tmpKmComma;
                                                $scope.loadingSimpan = 0
                                            } else if (res.data == -2) {
                                                bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                                $scope.mData.Km = tmpKmComma;
                                                $scope.loadingSimpan = 0
                                            } else {
                                                var tmpDataJob = angular.copy($scope.mData);
                                                var tmpJobId = res.data.ResponseMessage;
                                                tmpJobId = tmpJobId.split('#');
                                                console.log("tmpJobId", tmpJobId[1]);
                                                tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                                tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                                // =======================
                                                $scope.saveApprovalDP(tmpDataJob);
                                                // =======================

                                                // AppointmentBpService.postApproval(tmpDataJob).then(function(res) {
                                                //     console.log("sukses oostapproval");
                                                // });

                                                // console.log("tmpPrediagnose",tmpPrediagnose);
                                                // tmpPrediagnose.JobId = tmpJobId[1];
                                                // AppointmentBpService.createPreadiagnose(tmpPrediagnose).then(function(res){

                                                // });

                                                //Notif Perubahan DP


                                                if ($scope.mData.Id !== undefined) {
                                                    AppointmentGrService.updateMRS($scope.mData.Id).then(function(resz) {
        
                                                    });
                                                }


                                                //CR4
                                                console.log('createAppointment 1===>',res);
                                                console.log('tampung estimasi buat updet detil ===>',tampungEstimasi);
                                                var Jsonparser = res.data.ResponseMessage.split("#");
                                                var JobsId = parseInt(Jsonparser[1]);
                                                for(var x in tampungEstimasi){
                                                    tampungEstimasi[x].IsWO = 0;
                                                }
                                                AppointmentBpService.UpdateJobAppointmentAddEstimasiId(JobsId, tampungEstimasi).then(function(res){
                                                    console.log('updatejobappointmentaddestimationid SUCCESS ===>',res.data);
                                                },function(err){
                                                    console.log('updatejobappointmentaddestimationid ERROR 1  ===>',res.data);
                                                });
                                                //CR4


                                                AppointmentBpService.getNoAppointment(tmpJobId[1]).then(function(resu) {
                                                    console.log('getNoAppointment', resu.data);
                                                    $scope.alertAfterSave(resu.data);
                                                });
                                                // added by sss on 2017-09-20
                                                //remove by rzk on 2018-09-26 karena jadi double seharusan save saat bikin WO saja
                                                // AppointmentBpService.createJobPlan(tmpJobId[1], tlBPObj).then(function(res) {
                                                //     console.log("success Job Task BP..");
                                                // });
                                                //

                                                //kirim notifikasi Perubahan DP
                                                var DataNotif = {};
                                                var messagetemp = "";
                                                messagetemp = "No. Appointment : " + $scope.mData.AppointmentNo + " <br> " +
                                                    "No. Polisi : " + $scope.mData.PoliceNumber + " <br> " +
                                                    "DP Awal : " + $scope.totalDpDefault + " <br> " +
                                                    "DP Akhir : " + $scope.totalDp + " <br> ";



                                                DataNotif.Message = messagetemp;

                                                AppointmentBpService.sendNotif(DataNotif, 1129, 18).then(
                                                    function(res) {

                                                    },
                                                    function(err) {
                                                        //console.log("err=>", err);
                                                    });
                                                console.log("abis save", res);
                                                $scope.formApi.setMode('grid');
                                                $scope.getData(true);
                                            }
                                        }, function(err) {
                                            $scope.mData.Km = tmpKmComma;
                                        });
                                    }
                                });
                            } else {
                                AppointmentBpService.createAppointment($scope.mData).then(function(res) {
                                    
                                    
                                    // var tmpDataJob = angular.copy($scope.mData);
                                    if (res.data == -1) {
                                        bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                        $scope.mData.Km = tmpKmComma;
                                        $scope.loadingSimpan = 0
                                    } else if (res.data == -2) {
                                        bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                        $scope.mData.Km = tmpKmComma;
                                        $scope.loadingSimpan = 0
                                    } else {
                                        var tmpJobId = res.data.ResponseMessage;
                                        tmpJobId = tmpJobId.split('#');
                                        console.log("tmpJobId", tmpJobId[1]);
                                        // tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                        // tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                        // console.log("tmpPrediagnose",tmpPrediagnose);
                                        // tmpPrediagnose.JobId = tmpJobId[1];
                                        // AppointmentBpService.createPreadiagnose(tmpPrediagnose).then(function(res){

                                        if ($scope.mData.Id !== undefined) {
                                            AppointmentGrService.updateMRS($scope.mData.Id).then(function(resz) {

                                            });
                                        }


                                        //CR4
                                        console.log('createAppointment 2===>',res);
                                        console.log('tampung estimasi buat updet detil ===>',tampungEstimasi);
                                        var Jsonparser = res.data.ResponseMessage.split("#");
                                        var JobsId = parseInt(Jsonparser[1]);
                                        for(var x in tampungEstimasi){
                                            tampungEstimasi[x].IsWO = 0;
                                        }
                                        AppointmentBpService.UpdateJobAppointmentAddEstimasiId(JobsId, tampungEstimasi).then(function(res){
                                            console.log('updatejobappointmentaddestimationid 2===>',res.data);
                                        },function(err){
                                            console.log('updatejobappointmentaddestimationid ERROR 2===>',res.data);
                                        });
                                        //CR4


                                        // });
                                        AppointmentBpService.getNoAppointment(tmpJobId[1]).then(function(resu) {
                                            console.log('getNoAppointment', resu.data);
                                            $scope.alertAfterSave(resu.data);
                                        });
                                        // added by sss on 2017-09-20
                                        //remove by rzk on 2018-09-26 karena jadi double seharusan save saat bikin WO saja
                                        // AppointmentBpService.createJobPlan(tmpJobId[1], tlBPObj).then(function(res) {
                                        //     console.log("success Job Task BP..");
                                        // });
                                        //
                                        console.log("abis save", res);
                                        $scope.formApi.setMode('grid');
                                        $scope.getData(true);
                                    }
                                }, function(err) {
                                    $scope.mData.Km = tmpKmComma;
                                });
                            }

                        } else {
                            if ($scope.gridWork.length == 0){
                                bsAlert.warning("Data Order Pekerjaan Masih Kosong");
                                $scope.loadingSimpan = 0
                            } else {
                                bsAlert.warning("Form masih ada yang belum terisi", "silahkan cek kembali");
                                $scope.loadingSimpan = 0
                            }
                        }


                    }
                    
                },
                title: 'Simpan',
                icon: 'fa fa-fw fa-save',
                //rightsBit: 1
            },
            {
                title: 'Simpan & Print',
                icon: 'fa fa-fw fa-print',
                func: function(row, formScope) {

                    console.log('click simpan aja terus', $scope.actionButtonSettingsDetail)

                    if ($scope.loadingSimpan > 0) {
                        bsNotify.show({
                            size: 'small',
                            type: 'danger',
                            title: "Data sedang proses simpan..",
                            // timeout: 1000,
                        });
                        return false;
                    } else {
                        $scope.loadingSimpan++
                        $timeout(function() {
                            $scope.loadingSimpan = 0
                        }, 30000);

                        console.log("mData Simpan & Print", $scope.mData);

                        $scope.mData.PoliceNumber = $scope.mDataDetail.LicensePlate;
                        $scope.mData.VIN = $scope.mDataDetail.VIN;
                        $scope.mData.LicensePlate = $scope.mDataDetail.LicensePlate;
                        // console.log("row",row);
                        var tmpGrid = [];
                        // added by sss on 2017-09-22
                        var tlBPObj = { "JobTaskBP": {}, "Jobtasks": [] };
                        //
                        tmpGrid = angular.copy(gridTemp);
                        // if(gridTemp.length > 0 && $scope.mData.AppointmentDate !== undefined && $scope.mData.ContactPerson !== undefined && $scope.mData.PhoneContactPerson1 !== undefined && $scope.mData.Category !== undefined && $scope.mData.Km !== undefined ){
                        if ($scope.mData.AppointmentDate !== undefined && $scope.mData.ContactPerson !== undefined && $scope.mData.PhoneContactPerson1 !== undefined && $scope.mData.WoCategoryId !== undefined && $scope.mData.Km !== undefined && $scope.mData.StallId !== 0 && $scope.gridWork.length !== 0 && ($scope.mDataDetail.LicensePlate !== null && $scope.mDataDetail.LicensePlate !== undefined && $scope.mDataDetail.LicensePlate !== '')) {
                            var tmpKm = angular.copy($scope.mData.Km);
                            var tmpKmComma = angular.copy($scope.mData.Km);
                            if (tmpKm.toString().includes(",")) {
                                tmpKm = tmpKm.toString().split(',');
                                if (tmpKm.length > 1) {
                                    tmpKm = tmpKm.join('');
                                } else {
                                    tmpKm = tmpKm[0];
                                }
                            }
                            tmpKm = parseInt(tmpKm);

                            if (tmpKm < $scope.KMTerakhir && $scope.KMTerakhir < 999999) {
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "KM tidak boleh lebih kecil dari " + $scope.KMTerakhir 
                                });
                                $scope.loadingSimpan = 0
                                return false;
                            }

                            $scope.mData.Km = tmpKm;
                            for (var i = 0; i < tmpGrid.length; i++) {
                                tmpGrid[i].JobParts = [];
                                // tmpGrid[i].FlatRate = 1;
                                // tmpGrid[i].FlatRate = tmpGrid[i].FlatRate ? tmpGrid[i].FlatRate : 1;
                                tmpGrid[i].FlatRate = (tmpGrid[i].FlatRate == null || tmpGrid[i].FlatRate == undefined) ? 1 : tmpGrid[i].FlatRate


                                angular.forEach(tmpGrid[i].child, function(v, k) {
                                        tmpGrid[i].JobParts[k] = v;
                                        delete tmpGrid[i].child;
                                    })
                                    // for(var j=0;j<tmpGrid[i].JobParts.length;j++){

                                //     var a = tmpGrid[i].JobParts[j].Parts;
                                //     for(var key in a){
                                //         // if(key !== 'LastModifiedUserId' || key !== 'LastModifiedDate'){
                                //             tmpGrid[i].JobParts[j][key] = a[key];
                                //         // }
                                //     }
                                //     // delete tmpGrid[i].JobParts[j]["LastModifiedUserId"];
                                //     // delete tmpGrid[i].JobParts[j]["LastModifiedDate"];

                                // }
                            }


                            console.log("=====>", tmpGrid);
                            // added by sss on 2017-09-22
                            tlBPObj = {
                                "JobTaskBP": {
                                    "Body": 0,
                                    "Putty": 0,
                                    "Surfacer": 0,
                                    "Spraying": 0,
                                    "Polishing": 0,
                                    "Reassembly": 0,
                                    "FinalInspection": 0
                                },
                                "Jobtasks": []
                            };

                            for (var i in $scope.gridEstimation.data) {
                                tlBPObj["JobTaskBP"]["Body"] += parseInt($scope.gridEstimation.data[i]["BodyEstimationMinute"]);
                                tlBPObj["JobTaskBP"]["Putty"] += parseInt($scope.gridEstimation.data[i]["PuttyEstimationMinute"]);
                                tlBPObj["JobTaskBP"]["Surfacer"] += parseInt($scope.gridEstimation.data[i]["SurfacerEstimationMinute"]);
                                tlBPObj["JobTaskBP"]["Spraying"] += parseInt($scope.gridEstimation.data[i]["SprayingEstimationMinute"]);
                                tlBPObj["JobTaskBP"]["Polishing"] += parseInt($scope.gridEstimation.data[i]["PolishingEstimationMinute"]);
                                tlBPObj["JobTaskBP"]["Reassembly"] += parseInt($scope.gridEstimation.data[i]["ReassemblyEstimationMinute"]);
                                tlBPObj["JobTaskBP"]["FinalInspection"] += parseInt($scope.gridEstimation.data[i]["FIEstimationMinute"]);
                            }
                            console.log('tlBPObj', tlBPObj);
                            //
                            // added by sss
                            for (var i in tmpGrid) {
                                // tmpGrid[i].PaidById = angular.copy(tmpGrid[i].Payment);
                                // tmpGrid[i].ProcessId = angular.copy(tmpGrid[i].Unit);
                                tmpGrid[i].BodyEstimationMinute = parseInt($scope.gridEstimation.data[i]["BodyEstimationMinute"]);
                                tmpGrid[i].PutyEstimationMinute = parseInt($scope.gridEstimation.data[i]["PuttyEstimationMinute"]);
                                tmpGrid[i].SurfacerEstimationMinute = parseInt($scope.gridEstimation.data[i]["SurfacerEstimationMinute"]);
                                tmpGrid[i].SprayingEstimationMinute = parseInt($scope.gridEstimation.data[i]["SprayingEstimationMinute"]);
                                tmpGrid[i].PolishingEstimationMinute = parseInt($scope.gridEstimation.data[i]["PolishingEstimationMinute"]);
                                tmpGrid[i].ReassemblyEstimationMinute = parseInt($scope.gridEstimation.data[i]["ReassemblyEstimationMinute"]);
                                tmpGrid[i].FIEstimationMinute = parseInt($scope.gridEstimation.data[i]["FIEstimationMinute"]);
                                delete tmpGrid[i].PaidBy;
                                delete tmpGrid[i].UnitBy;
                                for (var j in tmpGrid[i].JobParts) {
                                    tmpGrid[i].JobParts[j].Price = tmpGrid[i].JobParts[j].RetailPrice;
                                    tmpGrid[i].JobParts[j].DPRequest = parseInt(tmpGrid[i].JobParts[j].DownPayment);
                                    delete tmpGrid[i].JobParts[j].ETA;
                                    delete tmpGrid[i].JobParts[j].PaidBy;
                                }

                            }

                            


                            
                            //pindahan dari factory
                            for(var i in tmpGrid){
                                // if(tmpGrid[i].PaidById == 2277 || tmpGrid[i].PaidById == 30 || tmpGrid[i].PaidById == 31 ||  tmpGrid[i].PaidById == 32){
                                if(tmpGrid[i].PaidById == 2277 || tmpGrid[i].PaidById == 30){
                                    tmpGrid[i].Discount = 0;
                                    tmpGrid[i].DiscountTypeId = -1;
                                    if(tmpGrid[i].JobParts.length > 0 || tmpGrid[i].JobPart !== null || tmpGrid[i].JobParts !== undefined ){
                                        for(var j in tmpGrid[i].JobParts){
                                            // if(tmpGrid[i].JobParts[j].PaidById == 2277 || tmpGrid[i].JobParts[j].PaidById == 30 || tmpGrid[i].JobParts[j].PaidById == 31 ||  tmpGrid[i].PaidById == 32){
                                            if(tmpGrid[i].JobParts[j].PaidById == 2277 || tmpGrid[i].JobParts[j].PaidById == 30){
                                                tmpGrid[i].JobParts[j].Discount = 0;
                                                tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                            }else{
                                                // if(tmpGrid[i].JobParts[j].MaterialTypeId == 1){
                                                // //    tmpGrid[i].JobParts[j].Discount = data.disParts;
                                                //     tmpGrid[i].JobParts[j].DiscountTypeId = 10;
                                                // }else{
                                                //     tmpGrid[i].JobParts[j].Discount = 0;
                                                //     tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                                // }

                                                //req by pak eko bahan tetap dapet diskon 08-juli-2020
                                                tmpGrid[i].JobParts[j].DiscountTypeId = 10;
                                            }
                                        }
                                    }
                                }else{
                                    // tmpGrid[i].Discount = data.disTask;
                                    tmpGrid[i].DiscountTypeId = 10;
                                    if(tmpGrid[i].JobParts.length > 0 || tmpGrid[i].JobPart !== null || tmpGrid[i].JobParts !== undefined ){
                                        for(var j in tmpGrid[i].JobParts){
                                            if(tmpGrid[i].JobParts[j].PaidById == 2277 || tmpGrid[i].JobParts[j].PaidById == 30){
                                                tmpGrid[i].JobParts[j].Discount = 0;
                                                tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                            }else{
                                                // if(tmpGrid[i].JobParts[j].MaterialTypeId == 1){
                                                // //    tmpGrid[i].JobParts[j].Discount = data.disParts;
                                                //     tmpGrid[i].JobParts[j].DiscountTypeId = 10;
                                                // }else{
                                                //     tmpGrid[i].JobParts[j].Discount = 0;
                                                //     tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                                // }

                                                //req by pak eko bahan tetap dapet diskon 08-juli-2020
                                                tmpGrid[i].JobParts[j].DiscountTypeId = 10;
                                            }
                                        }
                                    }
                                }
                            }
                            //pindahan dari factory


                            $scope.mData.JobTask = tmpGrid;
                            $scope.mData.JobComplaint = $scope.JobComplaint;
                            $scope.mData.JobRequest = $scope.JobRequest;
                            $scope.mData.detail = $scope.mDataDetail;
                            $scope.mData.totalDpDefault = $scope.totalDpDefault;
                            $scope.mData.totalDp = $scope.totalDp;
                            if ($scope.totalDp != 0) {
                                $scope.totalDp = $scope.totalDp.toString().replace(/,/g, '');
                                $scope.totalDp = parseInt($scope.totalDp);
                            }


                            
                            

                            var tmpData = angular.copy($scope.mData);
                            var tmpAppointmentDate = tmpData.AppointmentDate;
                            tmpAppointmentDate = new Date(tmpAppointmentDate);
                            var finalDate
                            var yyyy = tmpAppointmentDate.getFullYear().toString();
                            var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
                            var dd = tmpAppointmentDate.getDate().toString();
                            finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                            $scope.mData.AppointmentDate = finalDate;
                            console.log("mData ======> AppointmentDate 2", $scope.mData);
                            console.log("mData ======> finalDate", finalDate);
                            // Insurance Payment
                            // get Insurance
                            _.map($scope.Asuransi, function(val) {
                                if (val.InsuranceName === $scope.mData.InsuranceName) {
                                    $scope.mData.InsuranceId = val.InsuranceId;
                                    $scope.mData.InsuranceName = val.InsuranceName;
                                }
                            });



                            $scope.mData.FinalDP = $scope.totalDP;
                            console.log("mData FinalDP ======> ", $scope.mData.FinalDP);


                            console.log("mData JSON FINAL======>", JSON.stringify($scope.mData));


                            if ($scope.totalDpDefault !== 0 && ($scope.totalDp < $scope.totalDpDefault)) {
                                AppointmentBpService.checkApproval($scope.totalDp).then(function(res) {
                                    console.log("ressss checkApproval", res.data);
                                    var ApproverRoleID = res.data;
                                    if (res.data !== -1) {
                                        AppointmentBpService.createAppointment($scope.mData).then(function(res) {
                                            
                                            
                                            if (res.data == -1) {
                                                bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                                $scope.mData.Km = tmpKmComma;
                                                $scope.loadingSimpan = 0
                                            } else if (res.data == -2) {
                                                bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                                $scope.mData.Km = tmpKmComma;
                                                $scope.loadingSimpan = 0
                                            } else {
                                                var tmpDataJob = angular.copy($scope.mData);
                                                var tmpJobId = res.data.ResponseMessage;
                                                tmpJobId = tmpJobId.split('#');
                                                console.log("tmpJobId", tmpJobId[1]);
                                                tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                                $scope.actPrintAppointment(tmpJobId[1]);
                                                tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                                // =======================
                                                $scope.saveApprovalDP(tmpDataJob);
                                                // =======================
                                                // console.log("tmpPrediagnose",tmpPrediagnose);
                                                // tmpPrediagnose.JobId = tmpJobId[1];
                                                // AppointmentBpService.createPreadiagnose(tmpPrediagnose).then(function(res){

                                                if ($scope.mData.Id !== undefined) {
                                                    AppointmentGrService.updateMRS($scope.mData.Id).then(function(resz) {
        
                                                    });
                                                }

                                                //CR4
                                                console.log('createAppointment 3===>',res);
                                                console.log('tampung estimasi buat updet detil ===>',tampungEstimasi);
                                                var Jsonparser = res.data.ResponseMessage.split("#");
                                                var JobsId = parseInt(Jsonparser[1]);
                                                for(var x in tampungEstimasi){
                                                    tampungEstimasi[x].IsWO = 0;
                                                }
                                                AppointmentBpService.UpdateJobAppointmentAddEstimasiId(JobsId, tampungEstimasi).then(function(res){
                                                    console.log('updatejobappointmentaddestimationid 3===>',res.data);
                                                },function(err){
                                                    console.log('updatejobappointmentaddestimationid ERROR 3===>',res.data);
                                                });
                                                //CR4

                                                // });
                                                AppointmentBpService.getNoAppointment(tmpJobId[1]).then(function(resu) {
                                                    console.log('getNoAppointment', resu.data);
                                                    $scope.alertAfterSave(resu.data);
                                                });
                                                // added by sss on 2017-09-20
                                                //remove by rzk on 2018-09-26 karena jadi double seharusan save saat bikin WO saja
                                                // AppointmentBpService.createJobPlan(tmpJobId[1], tlBPObj).then(function(res) {
                                                //     console.log("success Job Task BP..");
                                                // });
                                                //


                                                //kirim notifikasi Perubahan DP
                                                var DataNotif = {};
                                                var messagetemp = "";
                                                messagetemp = "No. Appointment : " + $scope.mData.AppointmentNo + " <br> " +
                                                    ", No. Polisi : " + $scope.mData.PoliceNumber + " <br> " +
                                                    ", DP Awal : " + $scope.totalDpDefault + " <br> " +
                                                    ", DP Akhir : " + $scope.totalDp + " <br> ";



                                                DataNotif.Message = messagetemp;

                                                AppointmentBpService.sendNotif(DataNotif, 1129, 18).then(
                                                    function(res) {

                                                    },
                                                    function(err) {
                                                        //console.log("err=>", err);
                                                    });

                                                console.log("abis save", res);
                                                $scope.formApi.setMode('grid');
                                                $scope.getData(true);
                                            }
                                        }, function(err) {
                                            $scope.mData.Km = tmpKmComma;
                                        });
                                    }
                                });
                            } else { 
                                AppointmentBpService.createAppointment($scope.mData).then(function(res) {
                                    
                                    
                                    if (res.data == -1) {
                                        bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                        $scope.mData.Km = tmpKmComma;
                                        $scope.loadingSimpan = 0
                                    } else if (res.data == -2) {
                                        bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                        $scope.mData.Km = tmpKmComma;
                                        $scope.loadingSimpan = 0
                                    } else {
                                        // var tmpDataJob = angular.copy($scope.mData);
                                        var tmpJobId = res.data.ResponseMessage;
                                        tmpJobId = tmpJobId.split('#');
                                        console.log("tmpJobId", tmpJobId[1]);
                                        $scope.actPrintAppointment(tmpJobId[1]);
                                        // tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                        // tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                        // console.log("tmpPrediagnose",tmpPrediagnose);
                                        // tmpPrediagnose.JobId = tmpJobId[1];
                                        // AppointmentBpService.createPreadiagnose(tmpPrediagnose).then(function(res){

                                        if ($scope.mData.Id !== undefined) {
                                            AppointmentGrService.updateMRS($scope.mData.Id).then(function(resz) {

                                            });
                                        }


                                        //CR4
                                        console.log('createAppointment 4===>',res);
                                        console.log('tampung estimasi buat updet detil ===>',tampungEstimasi);
                                        var Jsonparser = res.data.ResponseMessage.split("#");
                                        var JobsId = parseInt(Jsonparser[1]);
                                        for(var x in tampungEstimasi){
                                            tampungEstimasi[x].IsWO = 0;
                                        }
                                        AppointmentBpService.UpdateJobAppointmentAddEstimasiId(JobsId, tampungEstimasi).then(function(res){
                                            console.log('updatejobappointmentaddestimationid 4===>',res.data);
                                        },function(err){
                                            console.log('updatejobappointmentaddestimationid ERROR 4===>',res.data);
                                        });
                                        //CR4

                                        // });
                                        AppointmentBpService.getNoAppointment(tmpJobId[1]).then(function(resu) {
                                            console.log('getNoAppointment', resu.data);
                                            $scope.alertAfterSave(resu.data);
                                        });
                                        // added by sss on 2017-09-20
                                        //remove by rzk on 2018-09-26 karena jadi double seharusan save saat bikin WO saja
                                        // AppointmentBpService.createJobPlan(tmpJobId[1], tlBPObj).then(function(res) {
                                        //     console.log("success Job Task BP..");
                                        // });
                                        //
                                        console.log("abis save", res);
                                        $scope.formApi.setMode('grid');
                                        $scope.getData(true);
                                    }
                                }, function(err) {
                                    $scope.mData.Km = tmpKmComma;
                                });
                            }

                        } else {
                            if ($scope.gridWork.length == 0){
                                bsAlert.warning("Data Order Pekerjaan Masih Kosong");
                                $scope.loadingSimpan = 0
                            } else{
                                bsAlert.warning("Form masih ada yang belum terisi", "silahkan cek kembali");
                                $scope.loadingSimpan = 0
                            }
                        }


                    }
                    
                },
            },
            {
                actionType: 'back', //Use 'Back Action' of bsForm
                title: 'Back',
                //icon: 'fa fa-fw fa-edit',
                // func: function(){
                //     console.log('ini pas klik back djancuk');
                //     $scope.gridEstimation.data = [];
                // }
            },
        ]
        $scope.alertAfterSave = function(item) {
            bsAlert.alert({
                    title: "Data tersimpan dengan Nomor Appointment",
                    text: item.AppointmentNo,
                    type: "warning",
                    showCancelButton: false
                },
                function() {
                    $scope.formApi.setMode('grid');
                    $scope.getData(true);
                },
                function() {

                }
            )
        }
        $scope.detailActionButtonSetting = [{
                func: function(row, formScope) {
                    $scope.mData.JobTask = gridTemp;
                    console.log("mData", $scope.mData);

                },
                title: 'Simpan Draft',
                icon: '',
                //rightsBit: 1
            },
            {
                func: function(row, formScope) {
                    $scope.mData.JobTask = gridTemp;
                    console.log("mData", $scope.mData);

                },
                title: 'Simpan & Kirim Email',
                icon: '',
                //rightsBit: 1
            },
            {
                func: function(row, formScope) {
                    console.log("mData", $scope.mData);
                    // console.log("row",row);
                    var tmpGrid = [];
                    tmpGrid = angular.copy(gridTemp);
                    if (gridTemp.length > 0) {
                        for (var i = 0; i < tmpGrid.length; i++) {
                            tmpGrid[i].JobParts = [];
                            // tmpGrid[i].FlatRate = 1;
                            // tmpGrid[i].FlatRate = tmpGrid[i].FlatRate ? tmpGrid[i].FlatRate : 1;
                            tmpGrid[i].FlatRate = (tmpGrid[i].FlatRate == null || tmpGrid[i].FlatRate == undefined) ? 1 : tmpGrid[i].FlatRate


                            angular.forEach(tmpGrid[i].child, function(v, k) {
                                tmpGrid[i].JobParts[k] = v;
                                delete tmpGrid[i].child;
                            })
                            for (var j = 0; j < tmpGrid[i].JobParts.length; j++) {

                                var a = tmpGrid[i].JobParts[j];
                                for (var key in a) {
                                    // if(key !== 'LastModifiedUserId' || key !== 'LastModifiedDate'){
                                    tmpGrid[i].JobParts[j][key] = a[key];
                                    // }
                                }
                                // delete tmpGrid[i].JobParts[j]["LastModifiedUserId"];
                                // delete tmpGrid[i].JobParts[j]["LastModifiedDate"];

                            }
                        }
                        console.log("=====>", tmpGrid);
                        // for(var i=0;i<gridTemp.length;i++){
                        //     for(var j=0;j<gridTemp[i].TaskParts;j++){

                        //     }
                        // }
                        $scope.mData.JobTask = tmpGrid;
                        $scope.mData.JobRequest = $scope.JobRequest;
                        $scope.mData.JobComplaint = $scope.JobComplaint;
                        $scope.mData.detail = $scope.mDataDetail;
                        AppointmentBpService.createAppointment($scope.mData).then(function(res) {
                            
                            if(res.data !== -1 || res.data !== -2){
                                console.log('createAppointment 5===>',res);
                                console.log('tampung estimasi buat updet detil ===>',tampungEstimasi);
                                var Jsonparser = res.data.ResponseMessage.split("#");
                                var JobsId = parseInt(Jsonparser[1]);
                                for(var x in tampungEstimasi){
                                    tampungEstimasi[x].IsWO = 0;
                                }
                                if ($scope.mData.Id !== undefined) {
                                    AppointmentGrService.updateMRS($scope.mData.Id).then(function(resz) {

                                    });
                                }
                                AppointmentBpService.UpdateJobAppointmentAddEstimasiId(JobsId, tampungEstimasi).then(function(res){
                                    console.log('updatejobappointmentaddestimationid 5===>',res.data);
                                },function(err){
                                    console.log('updatejobappointmentaddestimationid ERROR 5===>',res.data);
                                });
                            }
                            

                        });

                    } else {
                        bsAlert.warning("Form masih ada yang belum terisi", "silahkan cek kembali");
                    }
                },
                title: 'Simpan',
                icon: 'fa fa-fw fa-save',
                //rightsBit: 1
            },
        ];
        var date = new Date();
        var typeSearch = "";

        $scope.MasterDiscountBooking = [];
        $scope.getData = function(from_autocall) {

            tampungEstimasi = [];
            


            MasterDiscountFactory.getDataAktif().then(
                function (res) {
                    console.log("res dis | ", res.data.Result, res.data.Result.length);
                    $scope.MasterDiscountBooking = res.data.Result;
                },
                function (err) { }
            );


            $scope.mDataVehicleHistory = [];
            $scope.MRS = {};
            $scope.mDataDetail = {}
            $scope.restart_digit_odo = 0
            $scope.isKmHistory = 0
            $scope.nilaiKM = 0
            $scope.KMTerakhir = 0

            $scope.restart_digit_odo = 0;
            $scope.model_info_km.show = false;
            
            $scope.loading = false;
            $scope.gridEstimation.data = [];

            if ($scope.filter.Type == 'Langsung'){
                if ($scope.filter.noPolice.includes('*') === false) {
                    WO.getDataPKS($scope.filter.noPolice).then(function(res) {
                        $scope.dataPKS = res.data.Result;
                        console.log("PKS", $scope.dataPKS);
                    });
                }
            }        

            if ($scope.filter.Type == "Langsung" && $scope.filter.typeSearch == 0 && $scope.filter.noPolice !== "" && $scope.filter.noPolice !== undefined) {
                console.log("filterno", $scope.filter.noPolice);
                if ($scope.filter.noPolice.includes("*")) {
                    typeParam = 1;
                } else {
                    typeParam = 0;
                }
                typeSearch = 0;
                $scope.loading = true;
                return $q.resolve(
                    AppointmentBpService.getData(typeSearch, $scope.filter.noPolice, typeParam).then(
                        function(res) {
                            console.log("getData res=>", res);
                            var gridData = res.data.Result;
                            if (gridData.length > 0) {
                                $scope.grid.data = gridData
                                $scope.isKmHistory = 0;
                                $scope.KMTerakhir = 0;


                            } else {
                                $scope.grid.data = [];
                                if (from_autocall != true) {
                                    $scope.bsAlertFunc("No. Polisi tidak ditemukan");
                                }
                                $scope.getPPN()
                                $scope.disableEmail(false);
                                $scope.isKmHistory = 0;
                                $scope.KMTerakhir = 0;
                                $scope.loadingSimpan = 0
                            }
                            $scope.loading = false;
                            return res;
                        },
                        function(err) {
                            console.log("error=>", err);
                            $scope.loading = false;
                            return err;
                        }
                    )
                )
            } else if ($scope.filter.Type == "Langsung" && $scope.filter.typeSearch == 1 && $scope.filter.noRangka !== "" && $scope.filter.noRangka !== undefined) {
                console.log("filterno", $scope.filter.noRangka);
                // if ($scope.filter.noPolice.includes("*")) {
                //     typeParam = 1;
                // } else {
                typeParam = 0;
                // }
                typeSearch = 1;
                $scope.loading = true;
                return $q.resolve(
                    AppointmentBpService.getData(typeSearch, $scope.filter.noRangka, typeParam).then(
                        function(res) {
                            var gridData = res.data.Result;
                            if (gridData.length > 0) {
                                $scope.grid.data = gridData
                            } else {
                                $scope.grid.data = [];
                                bsAlert.warning("Toyota Id tidak ditemukan");
                            }
                            $scope.loading = false;
                            return res;
                        },
                        function(err) {
                            console.log("error=>", err);
                            $scope.loading = false;
                            return err;
                        }
                    )
                )
            } else if ($scope.filter.Type == "Lain") {
                $scope.filter.noPolice = "";
                gridData = [];
                return $q.resolve(
                    AppointmentBpService.getDataLain().then(
                        function(res) {
                            console.log("res", res);
                            var gridData = res.data;
                            $scope.grid.data = gridData;
                            $scope.loading = false;
                            return res;
                        },
                        function(err) {
                            console.log("error=>", err);
                            $scope.loading = false;
                            return err;
                        }
                    )
                )
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Filter",
                    // content: error.join('<br>'),
                    // number: error.length
                });
                $scope.grid.data = [];
            }
        }

        var linkSaveCb = function(mode, model) {
            console.log("mode", mode);
            console.log("mode", model);
        }
        var linkBackCb = function() {

        }
        $scope.preDiagnose = function() {

        }

        // $scope.ShowEdit = true;
        $scope.EnableEdit = true;
        $scope.ShowEditInstitusi = true;
        $scope.EnableEditInstitusi = true;

        $scope.EnableEditContactPerson = true;

        // $scope.EditInfoUmum = function() {
        //     AppointmentBpService.getLocation().then(function(res) {
        //         // console.log("List Province====>",res.data.Result);
        //         $scope.ProvinceData = res.data.Result;
        //         $scope.ShowEdit = false;
        //         $scope.EnableEdit = false;
        //         console.log("ProvinceData", $scope.ProvinceData);
        //     })
        // }
        $scope.EditInfoUmum = function() {
            $scope.mDataDetailDefault = angular.copy($scope.mDataDetail);
            // AppointmentBpService.getLocation().then(function(res) {
            //     // console.log("List Province====>",res.data.Result);
            //     $scope.ProvinceData = res.data.Result;
            //     $scope.ShowEdit = false;
            //     $scope.EnableEdit = false;
            //     console.log("ProvinceData", $scope.ProvinceData);
            // })
            if ($scope.mDataDetail.CustomerVehicleId !== undefined) {
                $scope.getAddressList();
                console.log("$scope.mDataDetail.CustomerVehicleId", $scope.mDataDetail.CustomerVehicleId);
                AppointmentGrService.getDataContactPerson($scope.mDataDetail.CustomerVehicleId).then(function(res) {
                    // console.log("List Province====>",res.data.Result);
                    $scope.ContactPersonData = res.data.Result;
                    $scope.ShowEdit = false;
                    $scope.EnableEdit = false;
                    $scope.EnableEditContactPerson = false;
                    console.log("Data ContactPerson", $scope.ShowEdit);
                })
            }
        }

        // $scope.BatalInfoUmum = function() {
        //     $scope.ShowEdit = true;
        //     $scope.EnableEdit = true;
        //     // $scope.onBeforeEdit();
        //     // $scope.getAllParameter(DataPersonal.VehicleId);
        // }
        $scope.BatalInfoUmum = function() {
            $scope.ShowEdit = true;
            $scope.EnableEdit = true;
            $scope.EnableEditContactPerson = true;
            // $scope.onBeforeEdit();
            // $scope.getAllParameter(DataPersonal.VehicleId);
            $scope.mDataDetail = angular.copy($scope.mDataDetailDefault);
            if ($scope.mDataDetail.ProvinceId !== undefined && $scope.mDataDetail.ProvinceId !== null) {
                WOBP.getMLocationProvince().then(function(res) {
                    console.log("List Province====>", res.data.Result);
                    $scope.ProvinceData = res.data.Result;

                    if ($scope.mDataDetail.CityRegencyId !== undefined && $scope.mDataDetail.CityRegencyId !== null) {
                        WOBP.getMLocationCityRegency($scope.mDataDetail.ProvinceId).then(function(resu) {
                            $scope.CityRegencyData = resu.data.Result;

                            if ($scope.mDataDetail.DistrictId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                WOBP.getMLocationKecamatan($scope.mDataDetail.CityRegencyId).then(function(resu) {
                                    $scope.DistrictData = resu.data.Result;

                                    if ($scope.mDataDetail.VillageId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                        WOBP.getMLocationKelurahan($scope.mDataDetail.DistrictId).then(function(resu) {
                                            $scope.VillageData = resu.data.Result;
                                        });
                                    };
                                });
                            };
                        });
                    };
                })
            };
        }

        $scope.CekPerbedaan = function() {
            console.log("$scope.mDataDetail A", $scope.mDataDetail);
            console.log("$scope.DataSebelumBerubahCRM B", $scope.DataSebelumBerubahCRM);
            $scope.datahasil = {};
            $scope.datahasilsudah = {};
            $scope.dataakhir = [];
            $scope.datasatu = [];
            $scope.datadua = [];
            var counter = 0;
            var keyofData = Object.keys($scope.DataSebelumBerubahCRM);

            for (var i in keyofData) {
                if ($scope.DataSebelumBerubahCRM[keyofData[i]] !== $scope.mDataDetail[keyofData[i]]) {
                    counter++;
                    $scope.datahasil[keyofData[i]] = $scope.mDataDetail[keyofData[i]];
                    $scope.datahasilsudah[keyofData[i]] = $scope.DataSebelumBerubahCRM[keyofData[i]];
                    // $scope.datasatu.push($scope.datahasil);
                    // $scope.datadua.push($scope.datahasilsudah);
                }
            }
            console.log('keyofData', keyofData);
            var fieldofData = Object.keys($scope.datahasilsudah);

            for (var i in fieldofData) {
                if ($scope.datahasilsudah[keyofData[i]] !== null) {
                    counter++;
                    $scope.datasatu.push($scope.datahasilsudah[fieldofData[i]]);

                }
            }

            for (var i = 0; i < fieldofData.length; i++) {
                var tmpDataAkhir = {};
                tmpDataAkhir.CustomerId = $scope.mDataDetail.CustomerId;
                tmpDataAkhir.ChangeTypeId = 2
                    tmpDataAkhir.Modul = 'Appointment BP'
                    tmpDataAkhir.Attribute = fieldofData[i]
                    if (tmpDataAkhir.Attribute == "BirthDate"){
                        tmpDataAkhir.NewValue = $filter('date')($scope.datahasil[fieldofData[i]], 'yyyy-MM-dd');
                        tmpDataAkhir.OldValue = $filter('date')($scope.datahasilsudah[fieldofData[i]], 'yyyy-MM-dd');
                    } else {
                        tmpDataAkhir.NewValue = $scope.datahasil[fieldofData[i]];
                        tmpDataAkhir.OldValue = $scope.datahasilsudah[fieldofData[i]];
                        // tmpDataAkhir.NewValue = $scope.datahasilsudah[fieldofData[i]]
                        // tmpDataAkhir.OldValue = $scope.datahasil[fieldofData[i]]
                    }
                    // tmpDataAkhir.NewValue = $scope.datahasilsudah[fieldofData[i]],
                    // tmpDataAkhir.OldValue = $scope.datahasil[fieldofData[i]]
                console.log(' $scope.datadua', $scope.datadua);
                AppointmentGrService.sendToCRMHistory(tmpDataAkhir);
            }

        };

        $scope.UpdateInfoUmum = function(mDataDetail) {
            console.log("data sialan", DataPersonal);
            console.log("data sialan", mDataDetail);

            AppointmentGrService.getDataVehicle(mDataDetail.VehicleId).then(function(ress) {
                var dataDetail = ress.data.Result[0]
                $scope.DataSebelumBerubahCRM = dataDetail;
                console.log("data CRM A", $scope.DataSebelumBerubahCRM);
                $scope.CekPerbedaan();
            });

            // if ($scope.mDataDetail.VehicleUserId !== undefined) {
            // AppointmentGrService.updateStatus(mDataDetail).then(function(res) {
            AppointmentBpService.putCustomerPersonal(DataPersonal, mDataDetail).then(function(res) {
                AppointmentBpService.putCustomerAddress(DataPersonal, mDataDetail, 1).then(function(res) {
                    // $scope.BatalInfoUmum();
                    console.log("save addres")
                    AppointmentGrService.putGastype($scope.mDataDetail).then(function(res) {
                        // $scope.BatalInfoUmum();
                        $scope.ShowEdit = true;
                            $scope.EnableEdit = true;
                            $scope.EnableEditContactPerson = true;
                        console.log("update type Gas")
                    });
                });
            });
            // });
            // }
            // if ($scope.mDataDetail.VehicleUserId !== undefined) {
            //     AppointmentGrService.updateStatus(mDataDetail).then(
            //         function(res) {
            //             $scope.BatalInfoUmum();
            //         },
            //         function(err) {
            //             console.log("err=>", err);
            //         }
            //     );
            // }
        }

        $scope.ShowEditInstitusi = true;
        $scope.EnableEditInstitusi = true;

        // WOBP.getDamage().then(function(res) {
        //     $scope.DamageVehicle = res.data.Result;
        //     _.map($scope.DamageVehicle, function(val) {
        //         val.Id = val.MasterId;
        //     })
        //     console.log("$scope.dramagaaaa", $scope.DamageVehicle);
        // })
        // $scope.DamageVehicle = [];

        $scope.EditInfoUmumInstitusi = function() {
            $scope.mDataDetailDefault = angular.copy($scope.mDataDetail);
            $scope.getAddressList();
            if ($scope.mDataDetail.CustomerVehicleId !== undefined) {
                console.log("$scope.mDataDetail.CustomerVehicleId", $scope.mDataDetail.CustomerVehicleId);
                AppointmentGrService.getDataContactPerson($scope.mDataDetail.CustomerVehicleId).then(function(res) {
                    // console.log("List Province====>",res.data.Result);
                    $scope.ContactPersonData = res.data.Result;
                    $scope.ShowEditInstitusi = false;
                    $scope.EnableEditInstitusi = false;
                    $scope.EnableEditContactPerson = false;
                    console.log("Data ContactPerson", $scope.ContactPersonData);
                })
            }
            // AppointmentBpService.getLocation().then(function(res) {
            //     // console.log("List Province====>",res.data.Result);
            //     $scope.ProvinceData = res.data.Result;
            //     $scope.ShowEditInstitusi = false;
            //     // $scope.EnableEditInstitusi = false;
            //     $scope.EnableEditContactPerson = false;
            //     console.log("ProvinceData", $scope.ProvinceData);
            // })
        }

        $scope.BatalInfoUmumInstitusi = function() {
            $scope.ShowEditInstitusi = true;
            $scope.EnableEditInstitusi = true;
            $scope.EnableEditContactPerson = true;
            // $scope.onBeforeEdit();
            // $scope.getAllParameter(DataPersonal.VehicleId);
            $scope.mDataDetail = angular.copy($scope.mDataDetailDefault);
            if ($scope.mDataDetail.ProvinceId !== undefined && $scope.mDataDetail.ProvinceId !== null) {
                WOBP.getMLocationProvince().then(function(res) {
                    console.log("List Province====>", res.data.Result);
                    $scope.ProvinceData = res.data.Result;

                    if ($scope.mDataDetail.CityRegencyId !== undefined && $scope.mDataDetail.CityRegencyId !== null) {
                        WOBP.getMLocationCityRegency($scope.mDataDetail.ProvinceId).then(function(resu) {
                            $scope.CityRegencyData = resu.data.Result;

                            if ($scope.mDataDetail.DistrictId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                WOBP.getMLocationKecamatan($scope.mDataDetail.CityRegencyId).then(function(resu) {
                                    $scope.DistrictData = resu.data.Result;

                                    if ($scope.mDataDetail.VillageId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                        WOBP.getMLocationKelurahan($scope.mDataDetail.DistrictId).then(function(resu) {
                                            $scope.VillageData = resu.data.Result;
                                        });
                                    };
                                });
                            };
                        });
                    };
                })
            };
        }

        $scope.UpdateInfoUmumInstitusi = function(mDataDetail) {
            // if ($scope.mDataDetail.VehicleUserId !== undefined) {
            // AppointmentGrService.updateStatus(mDataDetail).then(function(res) {
            AppointmentBpService.putCustomerAddress(DataPersonal, mDataDetail, 2).then(function(res) {
                $scope.BatalInfoUmumInstitusi();
                console.log("save addres")
            });
            // });
            // }

        };
        // ====== Alamat Added by Fyberz===================
        $scope.CustomerAddress = [];
        $scope.getAddressList = function() {
            WOBP.getMLocationProvince().then(function(res) {
                console.log("List Province====>", res.data.Result);
                // ProvinceId == row.ProvinceId
                $scope.ProvinceData = res.data.Result;
                // $scope.tempProvinsiData = angular.copy($scope.provinsiData);
            })
        };

        $scope.selectProvince = function(row) {
            console.log('row selectProvince', row);
            WOBP.getMLocationCityRegency(row.ProvinceId).then(function(resu) {
                $scope.CityRegencyData = resu.data.Result;

                $scope.mDataDetail.CityRegencyId = null
                $scope.mDataDetail.DistrictId = null
                $scope.mDataDetail.VillageId = null
                $scope.mDataDetail.PostalCode = null

                $scope.DistrictData = []
                $scope.VillageData = []
            });
            // $scope.selectedProvince = angular.copy(row);
        };
        $scope.selectRegency = function(row) {
            console.log('row selectRegency', row);
            WOBP.getMLocationKecamatan(row.CityRegencyId).then(function(resu) {
                $scope.DistrictData = resu.data.Result;

                $scope.mDataDetail.DistrictId = null
                $scope.mDataDetail.VillageId = null
                $scope.mDataDetail.PostalCode = null

                $scope.VillageData = []
            });
            // $scope.kecamatanData = row.MDistrict;
            // $scope.selectedRegency = angular.copy(row);
        };
        $scope.selectDistrict = function(row) {
            console.log('row selectDistrict', row);
            WOBP.getMLocationKelurahan(row.DistrictId).then(function(resu) {
                $scope.VillageData = resu.data.Result;

                $scope.mDataDetail.VillageId = null
                $scope.mDataDetail.PostalCode = null
            });
            // $scope.selectedDistrict = angular.copy(row);
        };
        $scope.selectVillage = function(row) {
            console.log('row selectVillage', row);
            // $scope.selecedtVillage = angular.copy(row);
            $scope.mDataDetail.PostalCode = row.PostalCode == 0 ? '-' : row.PostalCode;
        };
        // $scope.selectProvince = function(row) {
        //     $scope.CityRegencyData = row.MCityRegency;
        // }

        // $scope.selectRegency = function(row) {
        //     $scope.DistrictData = row.MDistrict;
        // }

        // $scope.selectDistrict = function(row) {
        //     $scope.VillageData = row.MVillage;
        // }

        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.disableTgl = function(){
            $('#tglPenyerahan').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
            $('#tglAppointment').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
        }

        var dataFinal = [];
        var gridTemp = [];
        var gridTempViewEstimasi = [];
        var tmp = [];
        var materialArray = [];
        $scope.onBeforeEdit = function(data, mode) {
            // $scope.getAllParameter();
            gridTemp = [];
            $scope.gridWork = gridTemp;
            AppointmentBpService.getDataVehicle(data.VehicleId).then(function(ress) {
                var dataDetail = ress.data.Result[0]
                $scope.mDataDetail = dataDetail;
                $scope.getAllParameter(dataDetail);
            });
            MrsList.getDataModel().then(function(res) {
                console.log("result : ", res.data.Result);
                $scope.modelData = res.data.Result;
                
            });
        }

        //by CK
        $scope.VehicleGasType = [
            { VehicleGasTypeId: 1, VehicleGasTypeName: "Bensin" },
            { VehicleGasTypeId: 2, VehicleGasTypeName: "Diesel" },
            { VehicleGasTypeId: 3, VehicleGasTypeName: "Hybrid" }
        ];
        $scope.clearOrderPekerjaan = function(){
            // $scope.gridWork = [];
            // $scope.gridEstimation.data = [];
            // gridTemp = [];
            // $scope.sumAllPrice();
        }
        $scope.selectModel = function(selected) {
            console.log("selectModel selected : ", selected);
            if(selected != null && selected != undefined){
                $scope.mData.VehicleModelName       = selected.VehicleModelName;
                $scope.mData.VehicleModelId         = selected.VehicleModelId;
                $scope.mDataDetail.VehicleModelName = selected.VehicleModelName;
                $scope.mDataDetail.VehicleModelId   = selected.VehicleModelId;
                $scope.mData.LicensePlate           = $scope.mDataDetail.LicensePlate;
                $scope.mData.VIN           = $scope.mDataDetail.VIN;
                $scope.mDataDetail.KatashikiCodex    = undefined
                $scope.mDataDetail.VehicleTypeId      = undefined

                WO.getVehicleTypeById(selected.VehicleModelId).then(function(res) {
                    // $scope.typeData = res.data.Result
                    // console.log('typeData ===>', $scope.typeData);

                    $scope.katashikiData = _.uniq(res.data.Result, 'KatashikiCode');

                    for (var i=0; i<$scope.katashikiData.length; i++){
                        $scope.katashikiData[i].NewDescription = selected.VehicleModelName + ' - ' + $scope.katashikiData[i].KatashikiCode
                    }

                    console.log("$scope.katashikiData : ", $scope.katashikiData);

                });
                if(selected.VehicleModelId == $scope.mDataDetail.VehicleModelId){
                    console.log('ini mobilnya sama | ga usah apus Task |===>',selected.VehicleModelId + ' | ' + $scope.mDataDetail.VehicleModelId);
                } else {
                    console.log('ini mobilnya beda | Harusss apus Task |===>',selected.VehicleModelId + ' | ' + $scope.mDataDetail.VehicleModelId);
                    $scope.mDataDetail.KatashikiCode = undefined;
                    $scope.mDataDetail.Description   = undefined;
                    $scope.mDataDetail.VehicleTypeId = undefined;
                }
               
                $scope.clearOrderPekerjaan();
            }else{
                $scope.mDataDetail.KatashikiCode = undefined;
                $scope.mDataDetail.Description   = undefined;
                $scope.mDataDetail.VehicleTypeId = undefined;
                $scope.mDataDetail.KatashikiCodex    = undefined
                $scope.mDataDetail.VehicleTypeId      = undefined
            }
        };

        $scope.selectKatashiki = function (selected) {
            $scope.mDataDetail.Description      = "";
            $scope.mDataDetail.VehicleTypeId      = "";


            if(selected != null && selected != undefined){
                AppointmentGrService.GetCVehicleTypeListByKatashiki(selected.KatashikiCode).then(function(res) {
                    // $scope.typeData = res.data.Result[0];
                    $scope.typeData = res.data.Result;

                    for (var z = 0; z < $scope.typeData.length; z++) {
                        $scope.typeData[z].NewDescription = $scope.typeData[z].Description + ' - ' + $scope.typeData[z].KatashikiCode + ' - ' + $scope.typeData[z].SuffixCode
                    }
                    $scope.mDataDetail.KatashikiCode    = selected.KatashikiCode;
    
                })
            } else {
                $scope.mDataDetail.KatashikiCodex    = "";
                $scope.mDataDetail.KatashikiCode    = "";
                $scope.mDataDetail.Description   = "";
                $scope.mDataDetail.VehicleTypeId = "";
            }
            
        }

        $scope.selectType = function(selected) {
            console.log("selectedtype", selected);
            if(selected != null && selected != undefined){
                $scope.mDataDetail.KatashikiCode = selected.KatashikiCode;
                $scope.mDataDetail.Description   = selected.Description;
                $scope.mDataDetail.VehicleTypeId = selected.VehicleTypeId;
                $scope.clearOrderPekerjaan();
            }else{
                $scope.mDataDetail.KatashikiCode = undefined;
                $scope.mDataDetail.Description   = undefined;
                $scope.mDataDetail.VehicleTypeId = undefined;
            }
        }
        $scope.estimateAsb = function() {
                if (gridTemp.length > 0) {
                    var totalW = 0;
                    for (var i = 0; i < gridTemp.length; i++) {
                        if (gridTemp[i].ActualRate !== undefined) {
                            totalW += gridTemp[i].ActualRate;
                        }
                    }
                    $scope.visibleAsb = 1;
                    $scope.tmpActRate = 1;
                } else {
                    $scope.tmpActRate = 1;
                    $scope.visibleAsb = 0;
                }
                $scope.actionButtonSettingsDetail[0].visible = false;
                $scope.actionButtonSettingsDetail[1].visible = false;
                $scope.actionButtonSettingsDetail[2].visible = false;
                $scope.actionButtonSettingsDetail[3].visible = false;
                // $scope.actionButtonSettingsDetail[4].visible = false;
                console.log('nih button', $scope.actionButtonSettingsDetail)
                $scope.visibleAsb = 1;
                $scope.tmpActRate = 1;
                $scope.checkAsb = true;
                $scope.isASB = 1;
                $scope.asb.startDate = "";
                currentDate = new Date();
                currentDate.setDate(currentDate.getDate() + 1);
                $scope.minDateASB = new Date(currentDate);
                $scope.currentDate = new Date(currentDate);
                if ($scope.mData.AppointmentDate !== undefined && $scope.mData.AppointmentDate !== null) {
                    console.log("Estimate  ASB $scope.mData.AppointmentDate", $scope.mData.AppointmentDate);
                    var tmpDate = $scope.mData.AppointmentDate;
                    var tmpTime = $scope.mData.AppointmentTime;
                    var finalDate
                    var yyyy = tmpDate.getFullYear().toString();
                    var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based
                    var dd = tmpDate.getDate().toString();
                    finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + " " + tmpTime;
                    var d = new Date(finalDate);
                    $scope.asb.startDate = d;
                } else {
                    console.log("Estimate ASB $scope.mData.AppointmentDate", $scope.mData.AppointmentDate);
                    $scope.asb.startDate = new Date(currentDate);
                }
                // $scope.asb.startDate = new Date(currentDate);
                $scope.asb.JobStallId = $scope.mData.StallId;
                $scope.asb.JobStartTime = $scope.mData.PlanStart;
                $scope.asb.JobEndTime = $scope.mData.PlanFinish;
                $scope.stallAsb = $scope.mData.StallId;
                console.log("curDate", currentDate);
                console.log("$scope.asb.startDate", $scope.asb.startDate);
                console.log("$scope.stallAsb", $scope.stallAsb, $scope.mData.StallId);
                showBoardAsbGr($scope.mData.FullDate);
            }
            // $scope.checkAsb = function(item){
            //     $scope.hideForm = true;
            //     $scope.showAsb = item;
            //     console.log("itemAsb",item);
            // }
        $scope.jscbViewEstimate = function() {
            $scope.checkAsb = true;
            $scope.isASB = 2;
        }
        $scope.backEstimate = function() {
                $scope.checkAsb = false;
                $scope.isASB = 0;
                $scope.asb.startDate = []; 
                $scope.hideForm = false;
                $scope.showAsb = '';
                // $scope.actionButtonSettingsDetail[0].visible = true; // ini simpan kirim email nya, 
                $scope.actionButtonSettingsDetail[1].visible = true;

                $scope.actionButtonSettingsDetail[2].visible = true;
                $scope.actionButtonSettingsDetail[3].visible = true;
                $timeout(function(){
                    $scope.disableTgl();
                },100)
                // $scope.actionButtonSettingsDetail[4].visible = true;
            }
            // added by sss 2017-09-13

        
        $scope.recalculateSummary = function(isCashParam,isCalculateSummary){

            if(isCalculateSummary == true){
                console.log('recalculateSummary  | isCalculateSummary true do something ===>',isCalculateSummary)
                console.log('recalculateSummary  | isCashParam ===>',isCashParam);
                console.log('recalculateSummary  | gridTemp ===>',gridTemp);
                console.log('recalculateSummary  | gridWork ===>',$scope.gridWork);
                console.log('recalculateSummary  | DiscountTask ===>',$scope.DiscountTask);
                console.log('recalculateSummary  | DiscountParts ===>',$scope.DiscountParts);

                if(isCashParam == 1){//cash

                    for (var i = 0; i < gridTemp.length; i++) {
                        gridTemp[i].PaidById = 28;
                        gridTemp[i].PaidBy = "Customer";
                        gridTemp[i].Discount = $scope.DiscountTask;

                        if (gridTemp[i].child.length > 0) {
                            for (var j = 0; j < gridTemp[i].child.length; j++) {
                                gridTemp[i].child[j].PaidById = 28;
                                gridTemp[i].child[j].PaidBy = "Customer";
                                gridTemp[i].child[j].Discount = $scope.DiscountParts;
                            }
                        }
                    }

                    for (var i = 0; i < $scope.gridWork.length; i++) {
                        $scope.gridWork[i].PaidById = 28;
                        $scope.gridWork[i].PaidBy = "Customer";
                        $scope.gridWork[i].Discount = $scope.DiscountTask;

                        if ($scope.gridWork[i].child.length > 0) {
                            for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                                $scope.gridWork[i].child[j].PaidById = 28;
                                $scope.gridWork[i].child[j].PaidBy = "Customer";
                                $scope.gridWork[i].child[j].Discount = $scope.DiscountParts;
                            }
                        }
                    }

                }else{//insurance

                    for (var i = 0; i < gridTemp.length; i++) {
                        gridTemp[i].PaidById = 29;
                        gridTemp[i].PaidBy = "Insurance";
                        gridTemp[i].Discount = $scope.DiscountTask;

                        if (gridTemp[i].child.length > 0) {
                            for (var j = 0; j < gridTemp[i].child.length; j++) {
                                gridTemp[i].child[j].PaidById = 29;
                                gridTemp[i].child[j].PaidBy = "Insurance";
                                gridTemp[i].child[j].Discount = $scope.DiscountParts;
                            }
                        }
                    }

                    for (var i = 0; i < $scope.gridWork.length; i++) {
                        $scope.gridWork[i].PaidById = 29;
                        $scope.gridWork[i].PaidBy = "Insurance";
                        $scope.gridWork[i].Discount = $scope.DiscountTask;

                        if ($scope.gridWork[i].child.length > 0) {
                            for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                                $scope.gridWork[i].child[j].PaidById = 29;
                                $scope.gridWork[i].child[j].PaidBy = "Insurance";
                                $scope.gridWork[i].child[j].Discount = $scope.DiscountParts;
                            }
                        }
                    }

                }

                if($scope.jejeranDP.length>0){
                    $scope.arrayDP(gridTemp, $scope.jejeranDP);
                }
               
                $scope.sumAllPrice();

            }else{
                console.log('recalculateSummary | isCalculateSummary false do Nothing ===>',isCalculateSummary)
            }    
        }

        var insuranceId = null;
        $scope.selectInsurance = function(selected,isCalculateSummary) {
            if(selected != undefined){
                insuranceId = selected.InsuranceId;
                $scope.mData.InsuranceName = selected.InsuranceName;
                console.log("selected ada: ", selected);

                console.log('jejeranDiskonTask ',jejeranDiskonTask, jejeranDiskonTask.length);
                console.log('jejeranDiskonParts ',jejeranDiskonParts, jejeranDiskonParts.length);
                if(jejeranDiskonParts.length == 0 ){
                    $scope.DisplayDisParts = selected.DiscountPart;
                    console.log('$scope.DisplayDisParts ===>',$scope.DisplayDisParts );
                }
                if(jejeranDiskonTask.length == 0){
                    $scope.DisplayDisTask =selected.DiscountJasa;                    
                    console.log('$scope.DisplayDisTask ===>',$scope.DisplayDisTask );
                }


                $scope.DiscountTask = selected.DiscountJasa;
                $scope.DiscountParts = selected.DiscountPart;
                $scope.mData.disTask = selected.DiscountJasa;
                $scope.mData.disParts = selected.DiscountPart;
                if (selected && (!selected.IntegrationBit || selected.IntegrationBit == 0)) {
                    console.log("intBit false");
                    $("#btnCekSPK").attr('disabled', 'disabled');
                    // $scope.disButton.disable = true;
                } else {
                    $("#btnCekSPK").removeAttr('disabled', 'disabled');
                    // $scope.disButton.disable = false;
                }


                $scope.recalculateSummary(0,isCalculateSummary);



            }else{
                console.log("selected ga ada: ", selected);
                $scope.mData.InsuranceName = undefined;
                if(jejeranDiskonParts.length == 0 ){
                    $scope.DisplayDisParts = 0;
                }
                if(jejeranDiskonTask.length == 0){
                    $scope.DisplayDisTask = 0;
                }
            }
        }
            //
            //----------------------------------
            // Grid Setup
            //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'Toyota ID',
                    displayName: 'Toyota ID',
                    field: 'ToyotaId'
                },
                {
                    name: 'No. Polisi',
                    field: 'LicensePlate'
                },
                {
                    name: 'Nama',
                    field: 'Name'
                },
                {
                    name: 'Alamat',
                    field: 'Address'
                },
                {
                    name: 'Telepon',
                    field: 'Phone'
                },
                {
                    name: 'Model',
                    field: 'VehicleModelName'
                },
                {
                    name: 'Warna',
                    field: 'ColorName'
                },
                {
                    name: 'Tahun Rakit',
                    field: 'AssemblyYear'
                },
                {
                    name: 'Tanggal Reminder',
                    field: 'RequestAppointmentDate',
                    cellFilter: dateFilter
                },
                {
                    name: 'Sumber Data',
                    field: 'AppointmentSourceName'
                },
                {
                    name: 'Action',
                    width: 120,
                    pinnedRight: true,
                    cellTemplate: gridActionButtonTemplate
                }
            ]
        };
        //----------------------------------
        // Type Ahead
        //----------------------------------
        //===============BS List============
        var gridTemp = [];
        $scope.listApi = {};
        $scope.listRequestSelectedRows = [];
        $scope.listComplaintSelectedRows = [];
        $scope.listJoblistSelectedRows = [];
        $scope.lmModel = {};
        $scope.ldModel = {};
        $scope.typeMData = [
            { Id: 1, Name: "1" }, 
            { Id: 2, Name: "2" }, 
            { Id: 3, Name: "3" },
            { Id: 6, Name: "T" },
            // { Id: 7, Name: "C" },
        ];
        $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
        // $scope.addDetail = function(data,item,itemPrice,row){
        //     $scope.listApi.clearDetail();
        //     row.FlatRate=data.FlatRate;
        //     console.log("itemPrice",itemPrice);
        //     if(itemPrice.length > 0){
        //         for(var i=0;i<itemPrice.length;i++){
        //             if(itemPrice[i].AdjustmentServiceRate !== undefined){
        //                 var harganya=itemPrice[i].AdjustmentServiceRate;
        //                 var harganyaa=data.FlatRate;
        //                 var summary = harganya * harganyaa;
        //                 var summaryy=parseInt(summary);
        //                 row.Fare=itemPrice[i].AdjustmentServiceRate;
        //                 row.Summary=summaryy;
        //             }else{
        //                 row.Fare=itemPrice[i].AdjustmentServiceRate;
        //                 row.Summary=row.Fare;
        //             }
        //         }
        //     }else{
        //         row.Fare="";
        //         row.Summary="";
        //     }
        //     row.TaskId=data.TaskId;
        //     var tmp = {}
        //     // $scope.listApi.addDetail(tmp,row)
        //     if(item.length > 0){
        //         for(var i=0;i<item.length;i++){
        //             tmp = item[i]
        //             $scope.listApi.addDetail(tmp,row);
        //         }
        //     }else{
        //         $scope.listApi.addDetail('',row);
        //     }
        // }
        $scope.onListSelectRows = function(rows) {
            console.log("onListSelectRows ==>", rows);
            console.log('tampungEstimasi ==>', tampungEstimasi);
        }

        // var FareNyangkut = 0;
        var tempTaskBPId = null;
        $scope.onBeforeEditList = function(rows) {
            console.log("form onBeforeEditList=>", rows);
            tempTaskBPId = rows.TaskBPId;

            $scope.copyAwalPembayaranJob = angular.copy(rows.PaidById)

            if (rows.PaidById == 30){
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }]; // kl pekerjaan twc / pwc ga blh pilih bahan

                if (rows.FlatRate == 0) {
                    $scope.FlatRateAvailable_new = false;
                } else {
                    $scope.FlatRateAvailable_new = true;
                }

                if (rows.Fare_w == null || rows.Fare_w == undefined) {
                    rows.Fare_w = rows.FlatRate * rows.Fare
                }
            } else {
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
            }

            $scope.cekJob = rows.TaskBPId;
            // if (typeof rows.Fare !== 'string') {
                // $scope.checkPrice(rows.FareMask.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','));
                // $scope.checkPrice(parseInt(rows.FareMask.toString().replace(/,/g, '')));
                // if(rows.Fare !== undefined){
                //     rows.Fare = rows.Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                // }else{
                //     rows.Fare = parseInt(rows.FareMask.toString().replace(/,/g, ''));
                //     // rows.Fare = rows.FareMask
                // }
                // $scope.lmModel.Fare = rows.Fare;

            //     if (rows.child.length > 0) {
            //         var tmpChild = angular.copy(rows.child);
            //         for (var j in tmpChild) {
            //             tmpChild[j].RetailPrice = tmpChild[j].RetailPrice.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            //             tmpChild[j].DPRequest = tmpChild[j].DPRequest.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            //             if (tmpChild[j].DownPayment != 0) {
            //                 tmpChild[j].DownPayment = tmpChild[j].DownPayment.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            //             }
            //             tmpChild[j].subTotal = tmpChild[j].subTotal.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            //         }
            //         $scope.listApi.clearDetail();
            //     }
            // // }
            // $scope.listApi.addDetail(tmpChild, rows);
        }

        $scope.onBeforeCancel = function() {
            console.log('onBeforeCancel cukkkk jancok | $scope.gridWork ===>',$scope.gridWork);
            console.log('lmModel ===>',$scope.lmModel);
            // for (var i in $scope.gridWork) {
            //     if($scope.gridWork[i].Fare !== undefined){
            //         if(typeof $scope.gridWork[i].Fare == 'string'){
            //             $scope.gridWork[i].Fare = $scope.gridWork[i].Fare.toString().replace(/,/g, '');
            //         }
            //     }else{
            //         $scope.gridWork[i].Fare = $scope.gridWork[i].FareMask.toString().replace(/,/g, '');
            //     }
            //     $scope.gridWork[i].Fare = parseInt($scope.gridWork[i].Fare);
            //     // if ($scope.gridWork[i].child.length > 0) {
            //     //     for (var j in $scope.gridWork[i].child) {
            //     //         $scope.gridWork[i].child[j].DPRequest = $scope.gridWork[i].child[j].DPRequest ? $scope.gridWork[i].child[j].DPRequest.replace(/,/g, '') : 0;
            //     //         // $scope.gridWork[i].child[j].DPRequest = $scope.gridWork[i].child[j].DPRequest.replace(/./g,'');
            //     //         $scope.gridWork[i].child[j].DPRequest = parseFloat($scope.gridWork[i].child[j].DPRequest);

            //     //         $scope.gridWork[i].child[j].DownPayment = $scope.gridWork[i].child[j].DownPayment ? $scope.gridWork[i].child[j].DownPayment.replace(/,/g, '') : 0;
            //     //         // $scope.gridWork[i].child[j].RetailPrice = $scope.gridWork[i].child[j].RetailPrice.replace(/./g,'');
            //     //         $scope.gridWork[i].child[j].DownPayment = parseInt($scope.gridWork[i].child[j].DownPayment);

            //     //         $scope.gridWork[i].child[j].RetailPrice = $scope.gridWork[i].child[j].RetailPrice ? $scope.gridWork[i].child[j].RetailPrice.replace(/,/g, '') : 0;
            //     //         // $scope.gridWork[i].child[j].RetailPrice = $scope.gridWork[i].child[j].RetailPrice.replace(/./g,'');
            //     //         $scope.gridWork[i].child[j].RetailPrice = parseInt($scope.gridWork[i].child[j].RetailPrice);

            //     //         $scope.gridWork[i].child[j].subTotal = $scope.gridWork[i].child[j].subTotal ? $scope.gridWork[i].child[j].subTotal.replace(/,/g, '') : 0;
            //     //         // $scope.gridWork[i].child[j].subTotal = $scope.gridWork[i].child[j].subTotal.replace(/./g,'');
            //     //         $scope.gridWork[i].child[j].subTotal = parseInt($scope.gridWork[i].child[j].subTotal);

            //     //         $scope.gridWork[i].child[j].subTotalForDisplay = $scope.gridWork[i].child[j].subTotalForDisplay ? $scope.gridWork[i].child[j].subTotalForDisplay.replace(/,/g, '') : 0;
            //     //         $scope.gridWork[i].child[j].subTotalForDisplay = parseInt($scope.gridWork[i].child[j].subTotalForDisplay);

            //     //     }
            //     // }
            // }
        }

        $scope.gridPartsDetail = {

            columnDefs: [
                { name: "No. Material", field: "PartsCode", width: '18%' },
                { name: "NamaMaterial", field: "PartsName", width: '18%' },
                { name: "Qty", field: "Qty", width: '12%' },
                { name: "Satuan", field: "satuanName", width: '12%' },
                // { name: "Satuan", field: "SatuanId", cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.unitDataGrid[row.entity.SatuanId]}}</div>' },
                { name: "Ketersediaan", field: "Availbility", width: '12%' },
                { name: "Tipe", field: "OrderType", width: '12%',
                        cellTemplate: '<div style="text-align:center;margin-top: 5px;" ng-show="row.entity.OrderType == 6">T</div><div style="text-align:center;margin-top: 5px;" ng-show="row.entity.OrderType != 6">{{row.entity.OrderType}}</div>' 
                },
                { name: "ETA", displayName: "ETA", field: "ETA", cellFilter: dateFilter, width: '12%' },
                { name: "Harga", field: "RetailPrice", width: '12%', cellFilter: 'currency:"":0' },

                // replace CR4
                { name: "Diskon", field: "DiskonForDisplay", width: '12%' },
                { name: "SubTotal", field: "subTotalForDisplay", width: '12%',cellFilter: 'currency:"":0' },
                // replace CR4

                // { name: "Diskon", field: "Discount", width: '12%' },// ini sebelum CR4 
                // { name: "SubTotal", field: "subTotal", width: '12%',cellFilter: 'currency:"":0'},// ini sebelum CR4 

                { name: "Nilai DP", displayName: "Nilai  DP", field: "DownPayment", width: '12%', cellFilter: 'currency:"":0' },
                { name: "Status DP", displayName: "Status  DP", field: "StatusDp", width: '12%', cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>' },
                { name: "Pembayaran", field: "PaidBy", width: '12%' },
                { name: "OPB", displayName: "OPB", field: "IsOPB", width: '12%', cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="null" disabled="disabled"></div>' }
            ]
        };


        $scope.gridPartsDetailViewEstimasi = {

            columnDefs: [
                { name: "No. Material", field: "PartsCode", width: '18%' },
                { name: "NamaMaterial", field: "PartsName", width: '18%' },
                { name: "Qty", field: "Qty", width: '12%' },
                { name: "Satuan", field: "satuanName", width: '12%' },
                // { name: "Satuan", field: "SatuanId", cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.unitDataGrid[row.entity.SatuanId]}}</div>' },
                { name: "Ketersediaan", field: "Availbility", width: '12%' },
                { name: "Tipe", field: "OrderType", width: '12%',
                        cellTemplate: '<div style="text-align:center;margin-top: 5px;" ng-show="row.entity.OrderType == 6">T</div><div style="text-align:center;margin-top: 5px;" ng-show="row.entity.OrderType != 6">{{row.entity.OrderType}}</div>' 
                },
                { name: "ETA", displayName: "ETA", field: "ETA", cellFilter: dateFilter, width: '12%' },
                { name: "Harga", field: "RetailPrice", width: '12%', cellFilter: 'currency:"":0' },

                // replace CR4
                { name: "Diskon", field: "DiskonForDisplay", width: '12%' },
                { name: "SubTotal", field: "subTotalForDisplay", width: '12%',cellFilter: 'currency:"":0' },
                // replace CR4

                // { name: "Diskon", field: "Discount", width: '12%' },// ini sebelum CR4 
                // { name: "SubTotal", field: "subTotal", width: '12%',cellFilter: 'currency:"":0'},// ini sebelum CR4 

                { name: "Nilai DP", displayName: "Nilai  DP", field: "DownPayment", width: '12%', cellFilter: 'currency:"":0' },
                { name: "Status DP", displayName: "Status  DP", field: "StatusDp", width: '12%', cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>' },
                { name: "Pembayaran", field: "PaidBy", width: '12%' },
                { name: "OPB", displayName: "OPB", field: "IsOPB", width: '12%', cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="null" disabled="disabled"></div>' }
            ]
        };

        // added by sss 2017-09-14
        $scope.gridEstimation = {
            enableCellEditOnFocus: true,
            showColumnFooter: true,

            columnDefs: [{
                'name': 'Order Pekerjaan',
                'field': 'TaskName',
                enableCellEdit: false,
                footerCellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;">Time Per Stall</div>'
            }, {
                'name': 'Body',
                'field': 'BodyEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Putty',
                'field': 'PuttyEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Surfacer',
                'field': 'SurfacerEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Painting',
                'field': 'SprayingEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Polishing',
                'field': 'PolishingEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Re-Assembly',
                displayName: "Re-Assembly",
                'field': 'ReassemblyEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Final Inspection',
                'field': 'FIEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Total',
                'field': 'Total',
                enableCellEdit: false,
                cellTemplate: '<div class="ui-grid-cell-contents" ng-model="MODEL_COL_FIELD">{{row.entity.BodyEstimationMinute + row.entity.PuttyEstimationMinute + row.entity.SurfacerEstimationMinute + row.entity.SprayingEstimationMinute +  row.entity.PolishingEstimationMinute + row.entity.ReassemblyEstimationMinute + row.entity.FIEstimationMinute }}</div>',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                footerCellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;">{{grid.appScope.gridEstimationApi.grid.columns[8].getAggregationValue()}}</div>',
                aggregationHideLabel: true

            }],
            onRegisterApi: function(gridApi) {
                $scope.gridEstimationApi = gridApi;
                $scope.gridEstimationApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                        console.log("afterCellEdit rowEntity", rowEntity);
                        console.log("afterCellEdit colDef", colDef);
                        console.log("afterCellEdit newValue", newValue);
                        console.log("afterCellEdit oldValue", oldValue);
                        if (newValue == null || newValue == undefined) {
                            newValue = 0;
                            return newValue;
                            $scope.calculateforwork();
                        }

                        setTimeout(function(){
                            $scope.calculateforwork();
                        },300);
                        
                    })
                    // $scope.gridEstimationApi.selection.on.rowSelectionChanged($scope, function (row) {
                    //     $scope.selectedRow = row.entity;
                    // });
                    // if ($scope.gridEstimationApi.selection.selectRow) {
                    //     $scope.gridEstimationApi.selection.selectRow($scope.gridEstimation.data[0]);
                    // }
            }
        };


        $scope.gridEstimationViewEstimasi = {
            enableCellEditOnFocus: true,
            showColumnFooter: true,

            columnDefs: [{
                'name': 'Order Pekerjaan',
                'field': 'TaskName',
                enableCellEdit: false,
                footerCellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;">Time Per Stall</div>'
            }, {
                'name': 'Body',
                'field': 'BodyEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Putty',
                'field': 'PuttyEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Surfacer',
                'field': 'SurfacerEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Painting',
                'field': 'SprayingEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Polishing',
                'field': 'PolishingEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Re-Assembly',
                displayName: "Re-Assembly",
                'field': 'ReassemblyEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Final Inspection',
                'field': 'FIEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Total',
                'field': 'Total',
                enableCellEdit: false,
                cellTemplate: '<div class="ui-grid-cell-contents" ng-model="MODEL_COL_FIELD">{{row.entity.BodyEstimationMinute + row.entity.PuttyEstimationMinute + row.entity.SurfacerEstimationMinute + row.entity.SprayingEstimationMinute +  row.entity.PolishingEstimationMinute + row.entity.ReassemblyEstimationMinute + row.entity.FIEstimationMinute }}</div>',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                footerCellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;">{{grid.appScope.gridEstimationApi.grid.columns[8].getAggregationValue()}}</div>',
                aggregationHideLabel: true

            }],
            onRegisterApi: function(gridApi) {
                $scope.gridEstimationApi = gridApi;
                $scope.gridEstimationApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                        console.log("afterCellEdit rowEntity", rowEntity);
                        console.log("afterCellEdit colDef", colDef);
                        console.log("afterCellEdit newValue", newValue);
                        console.log("afterCellEdit oldValue", oldValue);
                        if (newValue == null || newValue == undefined) {
                            newValue = 0;
                            return newValue;
                            $scope.calculateforwork();
                        }
                        $scope.calculateforwork();
                    })
                    // $scope.gridEstimationApi.selection.on.rowSelectionChanged($scope, function (row) {
                    //     $scope.selectedRow = row.entity;
                    // });
                    // if ($scope.gridEstimationApi.selection.selectRow) {
                    //     $scope.gridEstimationApi.selection.selectRow($scope.gridEstimation.data[0]);
                    // }
            }
        };
        //
        $scope.gridWork = gridTemp;
        $scope.sendWork = function(key, data) {
            var taskandParts = [];
            var taskList = {};
            console.log('resabis ambil parts ======', data, data.TaskListBPId);
            AppointmentBpService.getDataPartsByTaskId(data.TaskListBPId).then(function(res) {
                console.log("resabis ambil parts", res.data.Result);
                taskandParts = res.data.Result;
                // added by sss on 2017-09-15
                // get fare bukan task list..
                // AppointmentBpService.getDataTaskListByKatashiki($scope.mDataDetail.KatashikiCode).then(function(restask){
                //     taskList=restask.data.Result;
                //     console.log("taskList",taskList);
                //     $scope.addDetail(data,taskandParts,taskList,$scope.lmModel)
                // });
                //
            });
        }
        $scope.sendParts = function(key, data) {
                console.log('tipe material', $scope.ldModel);
                var tmpMaterialType = angular.copy($scope.ldModel.MaterialTypeId);
                console.log("a $scope.ldModel.Parts", $scope.ldModel.Parts);
                $scope.ldModel = data;
                var tmp = {};
                tmp = data;
                $scope.ldModel.PartsId = data.PartsId;
                $scope.ldModel.PartsName = data.PartsName;
                $scope.ldModel.SatuanId = data.UomId;
                $scope.ldModel.RetailPrice = null;
                _.map($scope.unitData,function(val){
                    if(data.UomId == val.MasterId){
                        $scope.ldModel.satuanName = val.Name;
                    }
                })
                $scope.ldModel.MaterialTypeId = tmpMaterialType;


                if ($scope.JobIsWarranty == 1) {
                    console.log('pekerjaan nya warranty nih parts nya jg harus', $scope.ldModel.PaidById);
                    _.map($scope.paymentData, function(val) {
                        console.log("valll tipe", val);
                        if (val.Name === "Warranty") {
                            $scope.ldModel.PaidById = val.MasterId;
                            $scope.ldModel.DownPayment = 0;
                            $scope.ldModel.DPRequest = 0;
                            $scope.ldModel.minimalDp = 0;
                            $scope.disableDP = true;
                        }
                    })

                } else {
                    console.log('pekerjaan nya bukan warranty');
                }


                // $scope.listApi.addDetail(tmp);
                console.log("key Parts", key);
                console.log("data Parts", data);
                console.log("b $scope.ldModel.Parts", $scope.ldModel);
            }
            //----------------------------------
            // Type Ahead
            //----------------------------------
        $scope.getWork = function(key) {
            var VehicleType = $scope.mDataDetail.VehicleTypeId;
            var vehModelId = $scope.mDataDetail.VehicleModelId;
            // console.log("$scope.mDataDetail.KatashikiCode", Katashiki);
            if (VehicleType != null && vehModelId != null) {
                if($scope.mData.isCash != null|| $scope.mData.isCash != undefined){
                    if($scope.mData.isCash == 0 && $scope.mData.InsuranceName != null ){
                        var ress = AppointmentBpService.getDataTask(key, VehicleType, vehModelId, $scope.JobIsWarranty).then(function(resTask) {
                            return resTask.data.Result;
                        });
                        console.log("cek ress", ress);
                        return ress
                    }else if($scope.mData.isCash == 1){
                        var ress = AppointmentBpService.getDataTask(key, VehicleType, vehModelId, $scope.JobIsWarranty).then(function(resTask) {
                            return resTask.data.Result;
                        });
                        console.log("cek ress", ress);
                        return ress
                    }else{
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Mohon Pilih Asuransi Terlebih Dahulu",
                        });
                    }
                }else{
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Mohon Pilih Matode Pembayaran Terlebih Dahulu",
                    });
                }
            }else{
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Data Mobil Terlebih Dahulu",
                });
            }
        };
        $scope.getParts = function(key) {
            var isGr = 0;
            var materialTypeId = $scope.ldModel.MaterialTypeId;
            var ress = AppointmentBpService.getDataParts(key, isGr, materialTypeId).then(function(resparts) {
                console.log("respart BP", resparts);
                var tempResult = [];
                if (resparts.data.Result.length > 0) {
                    // tempResult.push(resparts.data.Result[0]);
                    for (var i = 0; i < resparts.data.Result.length; i++) {
                        tempResult.push(resparts.data.Result[i]);
                    }
                }
                console.log('isGr COYYY 0', tempResult);
                return tempResult;
            });
            console.log("ress disini", ress);
            return ress
        };
        $scope.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };
        $scope.noResults = true;
        // var idx = gridTemp.length;
        var selectedTaskBP = null;
        $scope.onSelectWork = function($item, $model, $label) {
            console.log("onSelectWork=>", $item);
            console.log("onSelectWork=>", $model);
            console.log("onSelectWork=>", $label);
            if ($item.ServiceRate != null) {
                $item.Fare = $item.ServiceRate;
                $scope.PriceAvailable = true;
                // $item.Fare = $item.Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                $item.FareMask = $item.Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');

                var satuan = null;
                for (var i = 0; i < $scope.unitData.length; i++) {
                    if ($scope.unitData[i].Name == $item.UOM) {
                        satuan = $scope.unitData[i].MasterId;
                    }
                }
                $item.Unit = satuan;
                $item.ProcessId = satuan;
                $item.UnitBy = $item.UOM;

            } else {
                if ($item.WarrantyRate != null && $item.ServiceRate == null) {
                    $item.Fare = $item.WarrantyRate;

                    $item.Fare_w = $item.WarrantyRate * $item.FlatRate;

                    $item.FareMask = $item.Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                    var satuan = null;
                    for (var i = 0; i < $scope.unitData.length; i++) {
                        if ($scope.unitData[i].Name == $item.UOM) {
                            satuan = $scope.unitData[i].MasterId;
                        }
                    }
                    $item.Unit = satuan;
                    $item.ProcessId = satuan;
                    $item.UnitBy = $item.UOM;
                    console.log("abc", $item.WarrantyRate, $item.Fare)
                } else {
                    $item.Fare = 0;
                    $scope.PriceAvailable = false;
                    console.log("abc", $scope.PriceAvailable)
                }

                if ($item.FlatRate == 0) {
                    $scope.FlatRateAvailable_new = false;
                } else {
                    $scope.FlatRateAvailable_new = true;
                }
                
            }
            // added by sss on 2017-09-14
            selectedTaskBP = $item;
            $scope.listApi.addDetail(false, $item);
            $scope.sendWork($label, $item);
            console.log("modelnya", $scope.lmModel);
        }

        $scope.checkServiceRate = function (data_fr, data, from_check) {
            console.log($scope.lmModel)
            data.Fare_w = data_fr * data.WarrantyRate

            $scope.listApi.addDetail(false, data);
        }

        $scope.onSelectParts = function($item, $model, $label) {
            // console.log("onSelectWork=>", idx);
            console.log("onSelectParts=>", $item);
            console.log("onSelectParts=>", $model);
            console.log("onSelectParts=>", $label);
            console.log("anita parts", $scope.gridPartsDetail);
            // ==== ARI SAID ======
            // for (var i=0; i<$scope.gridPartsDetail.data.length; i++){
            //     if ($label == $scope.gridPartsDetail.data[i].PartsCode){
            //         console.log('parts sudah ada');
            //         $scope.ldModel.PartsCode = null;
            //         $scope.ldModel.PartsName = null;
            //         $item.PartsName = null;
            //         $item.PartsCode = null;
            //         bsNotify.show({
            //             size: 'big',
            //             title: 'Parts Sudah Ada!',
            //             text: 'I will close in 2 seconds.',
            //             timeout: 2000,
            //             type: 'danger'
            //         });

            //     }
            // }
            // $scope.mData.Work = $label;
            // console.log('materialArray', materialArray);
            // materialArray = [];
            // materialArray.push($item);
            if ($item.PartsName != null) {
                $scope.partsAvailable = true;
            } else {
                $scope.partsAvailable = false;
            }
            $scope.sendParts($label, $item);
            console.log("modelnya", $item);
            // console.log('materialArray',materialArray);
        }
        $scope.onBeforeNew = function() {
            console.log('on before new');
            console.log('Diskon Task ===>', $scope.DiscountTask, $scope.mData.disTask);
            console.log('Diskon Parts ===>', $scope.DiscountParts, $scope.mData.disParts);

            $scope.copyAwalPembayaranJob = null

            $scope.FlatRateAvailable_new = true

            $scope.cekJob = null;
            $scope.partsAvailable = false;
            $scope.jobAvailable = false;
            $scope.PriceAvailable = false;
            $scope.FlatRateAvailable = false;
            $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];

            if ($scope.mData.WoCategoryId == 2653) {
                // default pembayaran ke warranty
                var row = {}
                row.PaidById = 30
                row.PaidBy = "Warranty"
                $scope.lmModel.PaidBy = "Warranty"
                $scope.JobIsWarranty = 1
                $scope.listApi.addDetail(false, row);
            } else {
                var row = {}
                $scope.JobIsWarranty = 0
                $scope.listApi.addDetail(false, row);
            }

        }
        $scope.onBeforeEditJob = function() {

        }
        $scope.onNoResult = function() {
            console.log("gak", $scope.lmModel);
            var jmlPart = 0;
            if ($scope.cekJob != null && $scope.cekJob != undefined){
                AppointmentBpService.getDataPartsByTaskId($scope.cekJob).then(function(res) {
                    console.log("resabis ambil parts", res.data.Result);
                    jmlPart = res.data.Result.length;
                });
            }
            // if (jmlPart > 0 || $scope.gridPartsDetail.data.length > 0){
            if (jmlPart > 0){

                $scope.listApi.clearDetail();
            }
            var row = $scope.lmModel;
            row.FlatRate = 1;
            row.Fare = "";
            row.Summary = "";
            row.TaskId = null;
            row.tmpTaskId = null;
            // added by sss on 2017-09-19
            selectedTaskBP = null;
            //
            row.PaidById = 29;
            row.PaidBy = "Insurance"
            // $scope.lmModel.PaidById = row.PaidById
            $scope.lmModel.PaidBy = row.PaidBy
            row.TFirst1 = null;

            $scope.listApi.addDetail(false, row);
            console.log("uunnnnchhh", $scope.lmModel)
            $scope.partsAvailable = false;
            $scope.jobAvailable = false;
            $scope.PriceAvailable = false;
            $scope.FlatRateAvailable = false;
            $scope.FlatRateAvailable_new = true;
        }
        $scope.onNoPartResult = function() {
            console.log("onGotResult=>");
            $scope.partsAvailable = false;
            $scope.ldModel.PartsName = null;
            $scope.ldModel.RetailPrice = null;
            $scope.ldModel.minimalDp = 0;
            $scope.ldModel.DownPayment = null;
            $scope.ldModel.ETA = null;
            $scope.ldModel.SatuanId = null;
            $scope.ldModel.Availbility = null;
            $scope.ldModel.satuanName = null;
            $scope.ldModel.PartsId = null;
            $scope.ldModel.PartId = null;
            $scope.ldModel.TaskId = null;
            // $scope.ldModel.paidName = null;
        };
        $scope.onGotResult = function() {
            console.log("onGotResult=>");
        }
        $scope.selected = {};
        $scope.onListSave = function(item) {
            console.log("save_modal=>", item);
        }
       
        $scope.selectTypePaidParts = function(item) {//jancokkk
            console.log('selectTypePaidParts ===>',item);
            if($scope.gridPartsDetail.data.length > 0){
                for (var i = 0; i < $scope.gridPartsDetail.data.length; i++) {
                    if ( ($scope.ldModel.PartsCode !== null && $scope.ldModel.PartsCode !== undefined) && $scope.ldModel.PartsCode == $scope.gridPartsDetail.data[i].PartsCode && $scope.ldModel.PaidById == $scope.gridPartsDetail.data[i].PaidById ) {
                        console.log('parts sudah ada');
                        // bsNotify.show({
                        //     size: 'big',
                        //     title: 'Parts Sudah Ada!',
                        //     text: 'I will close in 2 seconds.',
                        //     timeout: 2000,
                        //     type: 'danger'
                        // });
                        bsAlert.warning('Parts Sudah Ada!','Silahkan ganti tipe pembayaran');
                        $scope.ldModel.PaidById = null;
                    }else{
                        $scope.ldModel.PaidBy = item.Name;
                        if ($scope.JobIsWarranty == 1) {
                            console.log('pekerjaan nya warranty nih parts nya jg harus', $scope.ldModel.PaidById);
                            _.map($scope.paymentData, function(val) {
                                console.log("valll tipe", val);
                                if (val.Name === "Warranty") {
                                    $scope.ldModel.PaidById = val.MasterId;
                                    $scope.ldModel.PaidBy = val.Name;
                                    $scope.ldModel.DownPayment = 0;
                                    $scope.ldModel.DPRequest = 0;
                                    $scope.ldModel.minimalDp = 0;
                                    $scope.disableDP = true;
                                }
                            })
                            

                            if($scope.ldModel.RetailPrice !== null || $scope.ldModel.RetailPrice !== undefined || $scope.ldModel.RetailPrice !== ''){
                                $scope.checkPrice( $scope.ldModel.RetailPrice);
                            } 
                        } else {
                            console.log('pekerjaan nya bukan warranty');
                            if($scope.ldModel.RetailPrice !== null || $scope.ldModel.RetailPrice !== undefined || $scope.ldModel.RetailPrice !== ''){
                                $scope.checkPrice( $scope.ldModel.RetailPrice);
                            } 
                        }
                    }
                }
            }else{
                $scope.ldModel.PaidBy = item.Name;
    
                    if ($scope.JobIsWarranty == 1) {
                        console.log('pekerjaan nya warranty nih parts nya jg harus', $scope.ldModel.PaidById);
                        _.map($scope.paymentData, function(val) {
                            console.log("valll tipe", val);
                            if (val.Name === "Warranty") {
                                $scope.ldModel.PaidById = val.MasterId;
                                $scope.ldModel.PaidBy = val.Name;
                                $scope.ldModel.DownPayment = 0;
                                $scope.ldModel.DPRequest = 0;
                                $scope.ldModel.minimalDp = 0;
                                $scope.disableDP = true;
                            }
                        })
                        if($scope.ldModel.RetailPrice !== null || $scope.ldModel.RetailPrice !== undefined || $scope.ldModel.RetailPrice !== ''){
                            $scope.checkPrice( $scope.ldModel.RetailPrice);
                        } 
                    } else {
                        console.log('pekerjaan nya bukan warranty');
                        if($scope.ldModel.RetailPrice !== null || $scope.ldModel.RetailPrice !== undefined || $scope.ldModel.RetailPrice !== ''){
                            $scope.checkPrice( $scope.ldModel.RetailPrice);
                        } 
                    }
            }
            if (item.MasterId != 28){
                $scope.ldModel.DownPayment = 0;
                $scope.ldModel.DPRequest = 0;
                $scope.ldModel.minimalDp = 0;
                $scope.disableDP = true;
            } else {
                $scope.ldModel.minimalDp = $scope.ldModel.temporaryDp;
                $scope.checkPrice( $scope.ldModel.RetailPrice);
                $scope.disableDP = false;
            }
        };
        $scope.onListCancel = function(item) {
                console.log("cancel_modal=>", item);
            }
            // added by sss on 2017-09-14



        $scope.onBeforeSave = function(data, mode) {
            
            // data.PaidBy = $scope.tmpCus.Name;


            if($scope.mData.isCash == 1){ //cash
                $scope.choosePembayaran($scope.mData.isCash,false);
                console.log('Diskon Parts | Cash | Spare Parts===>', $scope.DiscountParts, $scope.mData.disParts ) ;

            }else{ //isurance
                for (i in $scope.Asuransi){
                    if($scope.Asuransi[i].InsuranceId == insuranceId){
                        $scope.mData.InsuranceName = $scope.Asuransi[i].InsuranceName;
                        $scope.selectInsurance($scope.Asuransi[i],false);
                    }
                }
                console.log('Diskon Parts | Isurance| Spare Parts===>', $scope.DiscountParts, $scope.mData.disParts ) ;

            }


            console.log("onBeforeSave data", data);
            console.log("onBeforeSave mode  ===>", mode + " | tipenya ===> " + typeof mode );
            // console.log("onBeforeSave data", JSON.stringify(data));
            // console.log('Fare', data.Fare.includes(','));
            // data.Fare = data.Fare.toString().replace(/,/g, '');
            
            console.log('yhis is faree before ===>',data.Fare);
            
        
            if(data.Fare == null){
                data.Fare = data.ServiceRate;//tambahan karena kadang farenya null
            }
            
            // console.log("EstimationBPId ======>", data.EstimationBPId);
            // if(typeof data.EstimationBPId != undefined){
            //     if(Farenyangkut !== undefined){
            //         data.Fare = data.Farenyangkut;
            //     }
            // }else{
            if (data.PaidById != 30) {
                data.Discount = $scope.DiscountTask;
            } else {
                data.Discount = 0
            }
            // }
            
            

            // if (data.child.length > 0) {
            //     for (var j in data.child) {
                
            //         if (typeof data.child[j].DPRequest == 'string') {
            //             data.child[j].DPRequest = data.child[j].DPRequest ? data.child[j].DPRequest.replace(/,/g, '') : 0;
            //             // data.child[j].DPRequest = data.child[j].DPRequest.replace(/./g,'');
            //             data.child[j].DPRequest = parseFloat(data.child[j].DPRequest);
            //         }
            //         if (typeof data.child[j].DownPayment == 'string') {
            //             data.child[j].DownPayment = data.child[j].DownPayment ? data.child[j].DownPayment.replace(/,/g, '') : 0;
            //             // data.child[j].RetailPrice = data.child[j].RetailPrice.replace(/./g,'');
            //             data.child[j].DownPayment = parseInt(data.child[j].DownPayment);
            //         }
            //         if (typeof data.child[j].RetailPrice == 'string') {
            //             data.child[j].RetailPrice = data.child[j].RetailPrice ? data.child[j].RetailPrice.replace(/,/g, '') : 0;
            //             // data.child[j].RetailPrice = data.child[j].RetailPrice.replace(/./g,'');
            //             data.child[j].RetailPrice = parseInt(data.child[j].RetailPrice);
            //         }
            //         if (typeof data.child[j].subTotal == 'string') {
            //             data.child[j].subTotal = data.child[j].subTotal ? data.child[j].subTotal.replace(/,/g, '') : 0;
            //             // data.child[j].subTotal = data.child[j].subTotal.replace(/./g,'');
            //             data.child[j].subTotal = parseInt(data.child[j].subTotal); 
                        
            //         }

            //     }
            // }

            var tmpData = angular.copy(data);
            console.log('tmpdata', tmpData);
            var tmpGridWork = $scope.gridWork;
            var lengthGrid = tmpGridWork.length - 1;
            if ($scope.tmpUnt !== undefined) {
                tmpData.UnitBy = $scope.tmpUnt.Name;
            }
            // if ($scope.tmpCus !== undefined) {
            //     tmpData.PaidBy = $scope.tmpCus.Name;
            // }
            _.map($scope.paymentData, function(val) {
                if (val.MasterId == tmpData.PaidById) {
                    tmpData.PaidBy = val.Name;
                }
            });
            if (mode == 'new') {
                if (tmpData.TaskListBPId !== undefined && tmpData.TaskListBPId !== null) {
                    tmpData.TaskBPId = tmpData.TaskListBPId;
                } else {
                    if (tmpData.TaskBPId == null) {
                        // if (tmpGridWork.length > 0) {
                        //     tmpData.TaskBPId = tmpGridWork[lengthGrid].TaskBPId + 1;
                        // } else {
                        //     tmpData.TaskBPId = 1;
                        // }

                        if (tmpGridWork.length > 0) {
                            // tmpData.TaskBPId = (Math.abs(tmpGridWork[lengthGrid].TaskBPId) + 1) * -1; // kl non tasklist penanda nya dia minus
                            
                            var idNontasklist = [];
                            for (var i=0; i<tmpGridWork.length; i++){
                                if (tmpGridWork[i].TaskBPId < 0){
                                    idNontasklist.push(tmpGridWork[i].TaskBPId)
                                }
                            }

                            if (idNontasklist.length === 0){
                                // berarti blm ada non tasklist sebelumnya (baru nambahin 1 ini)
                                tmpData.TaskBPId = 1 * -1;
                            } else {
                                // sebelom na uda ada yg non tasklist
                                var SortedData = angular.copy(idNontasklist);
                                SortedData = SortedData.sort(function(a, b){
                                    return a-b //sort by date ascending
                                })
                                // di sort dl biar yg angka minus na gede di array pertama
                                tmpData.TaskBPId = SortedData[0] - 1; // kl non tasklist penanda nya dia minus

                            }

                            //Cek tambah pekerjaan atau tidak


                        } else {
                            tmpData.TaskBPId = 1 * -1;
                        }
                    }
                }
                
                if (data.child.length > 0) {
                    var tmpDataChild = angular.copy(data.child);
                    for (var i = 0; i < tmpDataChild.length; i++) {
                        tmpDataChild[i].DPRequest = Math.round(tmpDataChild[i].DownPayment);
                        delete data.child[i];
                    }
                    $scope.listApi.addDetail(tmpDataChild, tmpData);
                } else {
                    $scope.listApi.addDetail(false, tmpData);
                };
                // if (selectedTaskBP == null) {
                //     $scope.gridEstimation.data.push({
                //         "TaskBPId": data.TaskBPId,
                //         "TaskName": data.TaskName,
                //         "BodyEstimationMinute": 0,
                //         "PuttyEstimationMinute": 0,
                //         "SurfacerEstimationMinute": 0,
                //         "SprayingEstimationMinute": 0,
                //         "PolishingEstimationMinute": 0,
                //         "ReassemblyEstimationMinute": 0,
                //         "FIEstimationMinute": 0
                //     });
                // } else {
                //     var findEl = _.find($scope.gridEstimation.data, selectedTaskBP);
                //     if (typeof findEl === 'undefined') {
                //         $scope.gridEstimation.data.push({
                //             "TaskBPId": data.TaskBPId,
                //             "TaskName": selectedTaskBP.TaskName,
                //             "BodyEstimationMinute": selectedTaskBP.BodyEstimationMinute,
                //             "PuttyEstimationMinute": selectedTaskBP.PuttyEstimationMinute,
                //             "SurfacerEstimationMinute": selectedTaskBP.SurfacerEstimationMinute,
                //             "SprayingEstimationMinute": selectedTaskBP.SprayingEstimationMinute,
                //             "PolishingEstimationMinute": selectedTaskBP.PolishingEstimationMinute,
                //             "ReassemblyEstimationMinute": selectedTaskBP.ReassemblyEstimationMinute,
                //             "FIEstimationMinute": selectedTaskBP.FIEstimationMinute,
                //         });
                //     }
                //     selectedTaskBP = null;
                // }
                // $scope.calculateforwork();

                // if ($scope.gridEstimation.data.length > 0) {
                //     $scope.appDate = false;
                // }
            }else{
                if (data.child.length > 0) {
                    var tmpDataChild = angular.copy(data.child);

                    console.log('onBeforeSave | tempTaskBPId ===>',tempTaskBPId)
                    for (var i = 0; i < tmpDataChild.length; i++) {
                        tmpDataChild[i].DPRequest = Math.round(tmpDataChild[i].DownPayment);
                        tmpDataChild[i].TaskBPId = tempTaskBPId;

                        delete data.child[i];
                    }
                    console.log('onBeforeSave | edit 1 | tmpDataChild ===>',tmpDataChild)
                    console.log('onBeforeSave | edit 1 | tmpData ========>',tmpData)
                    tmpEditedTaskParts = angular.copy(tmpData);
                    $scope.listApi.addDetail(tmpDataChild, tmpData);
                } else {
                    console.log('onBeforeSave | edit 2 | tmpData ========>',tmpData)
                    $scope.listApi.addDetail(false, tmpData);
                };
            }
        }
        

        var flagArrayDP = 0;
        var tmpEditedTaskParts = [];
        $scope.onAfterSave = function(item) {
            console.log('onAfterSave | gridWork  =============>',$scope.gridWork);
            console.log('onAfterSave | gridTemp  =============>',gridTemp);
            console.log('onAfterSave | tmpEditedTaskParts  ===>',tmpEditedTaskParts);
            console.log('onAfterSave | tempTaskBPId  =========>',tempTaskBPId);

            // if(tempTaskBPId != null){
            //     for(var i in $scope.gridWork){
            //         if($scope.gridWork[i].TaskBPId == tempTaskBPId){
            //             $scope.gridWork[i] = tmpEditedTaskParts;
            //         }
            //     }
            // }
            

            if($scope.jejeranDP.length>0){
                $scope.arrayDP($scope.gridWork, $scope.jejeranDP, flagArrayDP);
            }

            var NumberParent = 1;
            var NumberChild = 1;
            for (i in $scope.gridWork){
                $scope.gridWork[i].NumberParent = NumberParent+parseInt(i);
                for (j in $scope.gridWork[i].child){
                    $scope.gridWork[i].child[j].NumberChild = $scope.gridWork[i].NumberParent + "."  + parseInt(++j);
                }
            }


            console.log('onaftersave ===>',item);


            var totalW = 0;
            totalEstimation = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if(gridTemp[i].PaidById == 2277 || gridTemp[i].PaidById == 30){
                    totalW += 0;
                }else{
                    // totalW += Math.round(gridTemp[i].Fare / 1.1);
                    totalW += ASPricingEngine.calculate({
                                    InputPrice: gridTemp[i].Fare,
                                    // Discount: 5,
                                    // Qty: 2,
                                    Tipe: 2, 
                                    PPNPercentage: PPNPerc,
                                });
                }
            }
            $scope.totalWork = totalW;


            var totalDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].DownPayment !== null) {
                            // totalDp += $scope.gridWork[i].child[j].DownPayment * 1.1;
                            // console.log("TotalDP bukan dari Estimasi", $scope.gridWork[i].child[j].DownPayment * 1.1);
                            totalDp += $scope.gridWork[i].child[j].DownPayment;
                            console.log("TotalDP bukan dari Estimasi", $scope.gridWork[i].child[j].DownPayment);
                        }
                    }
                }
            }
            $scope.totalDp = totalDp;
            console.log("TotalDP bukan dari Estimasi2", $scope.totalDp);

            var totalDefaultDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].minimalDp !== undefined) {
                            totalDefaultDp += $scope.gridWork[i].child[j].minimalDp;
                            console.log("$scope.gridWork[i].child.minimalDp", $scope.gridWork[i].child[j].minimalDp);
                        }

                        
                    }
                }
            }
            $scope.totalDpDefault = totalDefaultDp;



            var totalMaterial_byKevin = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if(gridTemp[i].child[j].PaidById == 2277 || gridTemp[i].child[j].PaidById == 30){
                            totalMaterial_byKevin += 0;
                        }else{
                            // totalMaterial_byKevin += Math.round(gridTemp[i].child[j].RetailPrice/1.1) * gridTemp[i].child[j].Qty;
                            totalMaterial_byKevin += ASPricingEngine.calculate({
                                                        InputPrice: gridTemp[i].child[j].RetailPrice,
                                                        // Discount: 5,
                                                        Qty: gridTemp[i].child[j].Qty,
                                                        Tipe: 102, 
                                                        PPNPercentage: PPNPerc,
                                                    }); 
                        }
                    }
                }
            };
            
            var totalMD_byKevin = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if(gridTemp[i].child[j].PaidById == 2277 || gridTemp[i].child[j].PaidById == 30 ){ //req by pak eko bahan tetap dapet diskon 08-juli-2020
                            totalMD_byKevin += 0;
                        }else{
                            // totalMD_byKevin +=  Math.round(Math.round(gridTemp[i].child[j].RetailPrice /1.1 ) * (gridTemp[i].child[j].Discount/100)) *  gridTemp[i].child[j].Qty;
                            totalMD_byKevin +=  ASPricingEngine.calculate({
                                                    InputPrice: gridTemp[i].child[j].RetailPrice,
                                                    Discount: gridTemp[i].child[j].Discount,
                                                    Qty: gridTemp[i].child[j].Qty,
                                                    Tipe: 112, 
                                                    PPNPercentage: PPNPerc,
                                                }); 
                            
                        }
                    }
                }
            };

            var totalWD_byKevin = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if(gridTemp[i].PaidById == 2277 || gridTemp[i].PaidById == 30){
                    totalWD_byKevin += 0;
                }else{
                    // totalWD_byKevin +=  Math.round(Math.round(gridTemp[i].Fare /1.1 ) * (gridTemp[i].Discount/100));
                    totalWD_byKevin +=  ASPricingEngine.calculate({
                                            InputPrice: gridTemp[i].Fare,
                                            Discount: gridTemp[i].Discount,
                                            // Qty: 2,
                                            Tipe: 12, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                }
            };

            





            $scope.totalMaterial           = totalMaterial_byKevin;
            $scope.totalMaterialDiscounted = totalMD_byKevin;
            $scope.totalWorkDiscounted     = totalWD_byKevin;
            $scope.subTotalMaterialSummary = $scope.totalMaterial - $scope.totalMaterialDiscounted
            $scope.subTotalWorkSummary     = $scope.totalWork - $scope.totalWorkDiscounted;
            // $scope.totalPPN                = Math.floor(($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * 0.1);    
            $scope.totalPPN                = ASPricingEngine.calculate({
                                                InputPrice: (($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * (1+PPNPerc/100)),
                                                // Discount: 0,
                                                // Qty: 0,
                                                Tipe: 33, 
                                                PPNPercentage: PPNPerc,
                                            });       
         
            $scope.totalEstimasi           = $scope.totalPPN + $scope.subTotalMaterialSummary + $scope.subTotalWorkSummary;





            // $scope.DiscountTask = 0;

            console.log('$scope.gridEstimation.data  cari index====>', $scope.gridEstimation.data);
            var idxEstimation = _.findIndex($scope.gridEstimation.data, { "TaskBPId": item.TaskBPId });
            console.log('idxEstimation ===>',idxEstimation);
            if (idxEstimation > -1) {
                selectedTaskBP = item;
                $scope.gridEstimation.data.splice(idxEstimation, 1);
                if (selectedTaskBP !== null) {
                    var findEl = _.find($scope.gridEstimation.data, selectedTaskBP);
                    if (typeof findEl === 'undefined') {

                        totalEstimation = parseInt(item.BodyEstimationMinute) +
                            parseInt(item.PuttyEstimationMinute) +
                            parseInt(item.SurfacerEstimationMinute) +
                            parseInt(item.SprayingEstimationMinute) +
                            parseInt(item.PolishingEstimationMinute) +
                            parseInt(item.ReassemblyEstimationMinute) +
                            parseInt(item.FIEstimationMinute);

                        $scope.gridEstimation.data.push({
                            "TaskBPId": item.TaskBPId,
                            "TaskName": selectedTaskBP.TaskName,
                            "BodyEstimationMinute": item.BodyEstimationMinute ? selectedTaskBP.BodyEstimationMinute : 0,
                            "PuttyEstimationMinute": item.PuttyEstimationMinute ? selectedTaskBP.PuttyEstimationMinute : 0,
                            "SurfacerEstimationMinute": item.SurfacerEstimationMinute ? selectedTaskBP.SurfacerEstimationMinute : 0,
                            "SprayingEstimationMinute": item.SprayingEstimationMinute ? selectedTaskBP.SprayingEstimationMinute : 0,
                            "PolishingEstimationMinute": item.PolishingEstimationMinute ? selectedTaskBP.PolishingEstimationMinute : 0,
                            "ReassemblyEstimationMinute": item.ReassemblyEstimationMinute ? selectedTaskBP.ReassemblyEstimationMinute : 0,
                            "FIEstimationMinute": item.FIEstimationMinute ? selectedTaskBP.FIEstimationMinute : 0,
                            "Total": totalEstimation

                        });
                    }
                } else {
                    $scope.gridEstimation.data.push({
                        "TaskBPId": item.TaskBPId,
                        "TaskName": item.TaskName,
                        "BodyEstimationMinute": 0,
                        "PuttyEstimationMinute": 0,
                        "SurfacerEstimationMinute": 0,
                        "SprayingEstimationMinute": 0,
                        "PolishingEstimationMinute": 0,
                        "ReassemblyEstimationMinute": 0,
                        "FIEstimationMinute": 0,
                        "Total": 0,

                    });
                }
                selectedTaskBP = null
            } else {
                if (selectedTaskBP == null) {
                    $scope.gridEstimation.data.push({
                        "TaskBPId": item.TaskBPId,
                        "TaskName": item.TaskName,
                        "BodyEstimationMinute": 0,
                        "PuttyEstimationMinute": 0,
                        "SurfacerEstimationMinute": 0,
                        "SprayingEstimationMinute": 0,
                        "PolishingEstimationMinute": 0,
                        "ReassemblyEstimationMinute": 0,
                        "FIEstimationMinute": 0,
                        "Total": 0

                    });
                } else {

                    totalEstimation = parseInt(item.BodyEstimationMinute) +
                        parseInt(item.PuttyEstimationMinute) +
                        parseInt(item.SurfacerEstimationMinute) +
                        parseInt(item.SprayingEstimationMinute) +
                        parseInt(item.PolishingEstimationMinute) +
                        parseInt(item.ReassemblyEstimationMinute) +
                        parseInt(item.FIEstimationMinute);

                    $scope.gridEstimation.data.push({
                        "TaskBPId": item.TaskBPId,
                        "TaskName": selectedTaskBP.TaskName,
                        "BodyEstimationMinute": item.BodyEstimationMinute ? selectedTaskBP.BodyEstimationMinute : 0,
                        "PuttyEstimationMinute": item.PuttyEstimationMinute ? selectedTaskBP.PuttyEstimationMinute : 0,
                        "SurfacerEstimationMinute": item.SurfacerEstimationMinute ? selectedTaskBP.SurfacerEstimationMinute : 0,
                        "SprayingEstimationMinute": item.SprayingEstimationMinute ? selectedTaskBP.SprayingEstimationMinute : 0,
                        "PolishingEstimationMinute": item.PolishingEstimationMinute ? selectedTaskBP.PolishingEstimationMinute : 0,
                        "ReassemblyEstimationMinute": item.ReassemblyEstimationMinute ? selectedTaskBP.ReassemblyEstimationMinute : 0,
                        "FIEstimationMinute": item.FIEstimationMinute ? selectedTaskBP.FIEstimationMinute : 0,
                        "Total": totalEstimation

                    });
                    selectedTaskBP = null;
                }
                selectedTaskBP = null;
            }


            if ($scope.gridEstimation.data.length > 0) {
                //$scope.appDate = false;
                $scope.appDate = true;
            }

            //calculate auto

            // AlltotalEstimation = AlltotalEstimation + totalEstimation;
            // var newDate = new Date($scope.mData.AppointmentDate);

            // function roundToTwo(num) {
            //     return +(Math.round(num + "e+2") + "e-2");
            // };
            // var days = Math.floor(Math.abs(AlltotalEstimation / (8 * 60)));
            // var hours = Math.abs((AlltotalEstimation % (8 * 60)) / 60);

            // var days2 = Math.abs(AlltotalEstimation / (8 * 60));
            // var hours2 = Math.abs(AlltotalEstimation / 60);

            // newDate.setDate(newDate.getDate() + days);
            // newDate.setHours(newDate.getHours() + roundToTwo(hours));
            // // console.log("hours", hours);
            // // $scope.estHoursWork = roundToTwo(hours);
            // // $scope.estDaysWork = days;

            // var daysString = days2.toString();
            // var daysXX = '';
            // if (daysString.includes('.')) {
            //     var daysSplit = daysString.split('.');
            //     var daysOne = daysSplit[0];
            //     var daysTwo = daysSplit[1];
            //     var daysOneX = parseInt(daysOne);
            //     var daysTwoX = daysTwo.substr(0, 1);
            //     daysTwoX = parseInt(daysTwoX);

            //     if (daysTwoX <= 5) {
            //         daysTwoX = 5;
            //         daysXX = daysOneX.toString() + '.' + daysTwoX.toString();
            //     } else {
            //         daysOneX = daysOneX + 1;
            //         daysXX = daysOneX.toString();
            //     }
            // } else {
            //     daysXX = daysString;
            // }

            // days2 = parseFloat(daysXX);
            // console.log('nih days', days2);



            // // console.log("hours", hours);
            // $scope.estHoursWork = roundToTwo(hours2);
            // $scope.estDaysWork = days2;

            // console.log("newDate 2", newDate);

            // var hh = newDate.getHours().toString();
            // var mm = newDate.getMinutes().toString();
            // var ss = "00";
            // var finalDate = (hh[1] ? hh : "0" + hh[0]) + ':' + (mm[1] ? mm : "0" + mm[0]) + ":" + ss;
            // var finalHour = (hh[1] ? hh : "0" + hh[0]);
            // var finalMinute = (mm[1] ? mm : "0" + mm[0]);
            // if (!isNaN(finalHour) && !isNaN(finalMinute)) {
            //     $scope.mData.TargetPlanFinish = finalDate;
            //     $scope.mData.TargetPlanDateFinish = newDate;
            //     $scope.mData.AdjusmentDate = newDate;
            // }
            // console.log("finalDate with Hours", finalDate);
            // console.log("$scope.estHoursWork", $scope.estHoursWork);
            // console.log("$scope.estDaysWork", $scope.estDaysWork);
            // console.log("newDate", newDate);




            // added by sss on 2017-09-14
            // if (selectedTaskBP == null) {
            //     $scope.gridEstimation.data.push({
            //         "TaskBPId": item.TaskBPId,
            //         "TaskName": item.TaskName,
            //         "BodyEstimationMinute": 0,
            //         "PuttyEstimationMinute": 0,
            //         "SurfacerEstimationMinute": 0,
            //         "SprayingEstimationMinute": 0,
            //         "PolishingEstimationMinute": 0,
            //         "ReassemblyEstimationMinute": 0,
            //         "FIEstimationMinute": 0
            //     });
            // } else {
            //     var exstEl = _.find($scope.gridEstimation.data, { "TaskBPId": item.TaskBPId });
            //     console.log("exstEl", exstEl);               
            //     if (typeof exstEl !== 'undefined') {    
            //         var index = $scope.gridEstimation.data.indexOf(exstEl);    
            //         if (index > -1) {         $scope.gridEstimation.data.splice(index, 1);     }                    
            //     }
            //     var findEl = _.find($scope.gridEstimation.data, selectedTaskBP);
            //     if (typeof findEl === 'undefined') {
            //         $scope.gridEstimation.data.push({
            //             "TaskBPId": item.TaskBPId,
            //             "TaskName": selectedTaskBP.TaskName,
            //             "BodyEstimationMinute": selectedTaskBP.BodyEstimationMinute,
            //             "PuttyEstimationMinute": selectedTaskBP.PuttyEstimationMinute,
            //             "SurfacerEstimationMinute": selectedTaskBP.SurfacerEstimationMinute,
            //             "SprayingEstimationMinute": selectedTaskBP.SprayingEstimationMinute,
            //             "PolishingEstimationMinute": selectedTaskBP.PolishingEstimationMinute,
            //             "ReassemblyEstimationMinute": selectedTaskBP.ReassemblyEstimationMinute,
            //             "FIEstimationMinute": selectedTaskBP.FIEstimationMinute,
            //         });
            //     }
            //     selectedTaskBP = null;
            // }


            //nentuin diskon task & parts
            jejeranDiskonTask = [];
            jejeranDiskonParts = [];
            for(var i in $scope.gridWork){
                // if($scope.gridWork.PaidById == 30|| $scope.gridWork.PaidById == 31 || $scope.gridWork.PaidById == 32 || $scope.gridWork.PaidById == 2277){
                if($scope.gridWork.PaidById == 30|| $scope.gridWork.PaidById == 2277){
                    jejeranDiskonTask.push(0);
                }else{
                    jejeranDiskonTask.push($scope.gridWork[i].Discount);
                }
                if($scope.gridWork[i].child.length > 0 ){
                    for(var j in $scope.gridWork[i].child){
                        // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 31 || $scope.gridWork[i].child[j].PaidById == 32 || $scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 2277){
                            jejeranDiskonParts.push(0);
                        }else{
                            jejeranDiskonParts.push($scope.gridWork[i].child[j].Discount);
                        }

                        
                    }
                }
            }
            //nentuin diskon task & parts
            $scope.displayDiscountAlgorithm();



            setTimeout(function(){
                $scope.calculateforwork();
            },500);
        }
        
        var jejeranDiskonTask = [];
        var jejeranDiskonParts = [];

        $scope.calculateforwork = function() {
            $scope.getCalculatedTimeWork = [];
            $scope.arrWorkTime = [];
            var newDate = new Date($scope.mData.AppointmentDate);
            console.log("newDate 1", newDate);
            // newDate.getHours(newDate.setHours() + 7);
            newDate.setSeconds(0);

            var total = 0;
            for (var i = 0; i < $scope.gridEstimationApi.grid.columns.length; i++) {
                if ($scope.gridEstimationApi.grid.columns[i].field != 'Total'){
                    var a = $scope.gridEstimationApi.grid.columns[i].getAggregationValue();
                    // $scope.arrWorkTime.push(a + ' minutes');
                    if (a != null || a !== undefined) {
                        $scope.arrWorkTime.push(a + ' minutes');
                        $scope.getCalculatedTimeWork.push(a);
                        total = total + a;
                    };
                }
                
            };

            function roundToTwo(num) {
                return +(Math.round(num + "e+2") + "e-2");
            };

            var days = Math.floor(Math.abs(total / (8 * 60)));
            var hours = Math.abs((total % (8 * 60)) / 60);

            var days2 = Math.abs(total / (8 * 60));
            var hours2 = Math.abs(total / 60);


            newDate.setDate(newDate.getDate() + days);
            newDate.setHours(newDate.getHours() + roundToTwo(hours));
            // console.log("hours", hours);
            // $scope.estHoursWork = roundToTwo(hours);
            // $scope.estDaysWork = days;

            var daysString = days2.toString();
            var daysXX = '';
            if (daysString.includes('.')) {
                var daysSplit = daysString.split('.');
                var daysOne = daysSplit[0];
                var daysTwo = daysSplit[1];
                var daysOneX = parseInt(daysOne);
                var daysTwoX = daysTwo.substr(0, 1);
                daysTwoX = parseInt(daysTwoX);

                if (daysTwoX <= 5) {
                    daysTwoX = 5;
                    daysXX = daysOneX.toString() + '.' + daysTwoX.toString();
                } else {
                    daysOneX = daysOneX + 1;
                    daysXX = daysOneX.toString();
                }
            } else {
                daysXX = daysString;
            }

            days2 = parseFloat(daysXX);
            console.log('nih days', days2);



            // console.log("hours", hours);
            $scope.estHoursWork = roundToTwo(hours2);
            $scope.estDaysWork = days2;

            console.log("newDate 2", newDate);

            var hh = newDate.getHours().toString();
            var mm = newDate.getMinutes().toString();
            var ss = "00";
            var finalDate = (hh[1] ? hh : "0" + hh[0]) + ':' + (mm[1] ? mm : "0" + mm[0]) + ":" + ss;
            var finalHour = (hh[1] ? hh : "0" + hh[0]);
            var finalMinute = (mm[1] ? mm : "0" + mm[0]);
            if (!isNaN(finalHour) && !isNaN(finalMinute)) {
                $scope.mData.TargetPlanFinish = finalDate;
                $scope.mData.TargetPlanDateFinish = newDate;
                $scope.mData.AdjusmentDate = newDate;
            }
            console.log("finalDate with Hours", finalDate);
            console.log("$scope.getCalculatedTimeWork", $scope.getCalculatedTimeWork);
            console.log("$scope.estHoursWork", $scope.estHoursWork);
            console.log("$scope.estDaysWork", $scope.estDaysWork);
            console.log("newDate pas chip", newDate);

            if($scope.mData.isCash == 1){
                if(newDate > $scope.DiskonTaskEndDate){
                    console.log('Tidak Masuk periode diskon booking Task');
                    $scope.DiscountTask = 0;
                    $scope.mData.disTask = 0;
                }else{
                    console.log('masuk periode diskon booking Task');
                }

                if(newDate > $scope.DiskonPartsEndDate){
                    console.log('Tidak Masuk periode diskon booking Parts');
                    $scope.DiscountParts = 0;
                    $scope.mData.disParts = 0;
                }else{
                    console.log('masuk periode diskon booking Parts');
                }

            }

        };


        $scope.onBeforeDelete = function(data){
            console.log('ini onBeforeDelete',data);
            console.log('$scope.gridWork ===>', $scope.gridWork)
   
            if(tampungEstimasi.length > 0){
                for(var i in tampungEstimasi){
                    if($scope.gridWork.length > 0){
                        for(var x in $scope.gridWork){
                            var count = 0;
                            if(typeof tampungEstimasi[i].EstimationBPId !== undefined){
                                console.log('ini ada EstimationBPId nya ==>',tampungEstimasi[i].EstimationBPId);
                                if(tampungEstimasi[i].EstimationBPId == $scope.gridWork[x].EstimationBPId){
                                    count++;
                                    // console.log('EstNo =>',tampungEstimasi[i].EstimationBPId + ' | count => ' + count + ' | EstTaskLength => '+ tampungEstimasi[i].EstTaskLength + ' DO SPLICE');
                                    if(count == tampungEstimasi[i].EstTaskLength){
                                        tampungEstimasi.splice(x, 1);
                                        console.log('DO SPLICE | tampungEstimasi ===>',tampungEstimasi);
                                    }else{
                                        tampungEstimasi[i].EstTaskLength = tampungEstimasi[i].EstTaskLength - 1;
                                        // console.log('DONT SPLICE | tampungEstimasi ===>',tampungEstimasi);

                                    }
                                }
                            }
                        }
                    }
                }
            }

            console.log('tampung estimasi ==>',tampungEstimasi);

        }

     

        $scope.onAfterDelete = function(id) {
            console.log('$scope.gridEstimation.data | onAfterDelete===>', $scope.gridEstimation.data);
            console.log('gridTemp | onAfterDelete ===>',gridTemp);
            if($scope.jejeranDP.length>0){
                $scope.arrayDP(gridTemp, $scope.jejeranDP);
            }


            var totalW = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if(gridTemp[i].PaidById == 2277 || gridTemp[i].PaidById == 30){
                    totalW += 0;
                }else{
                    // totalW += Math.round(gridTemp[i].Fare / 1.1);
                    totalW += ASPricingEngine.calculate({
                                    InputPrice: gridTemp[i].Fare,
                                    // Discount: 5,
                                    // Qty: 2,
                                    Tipe: 2, 
                                    PPNPercentage: PPNPerc,
                                });
                }
            }

            $scope.totalWork = totalW ;
            console.log(totalW);

            var totalDp = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].DownPayment !== null) {
                            
                            // totalDp += gridTemp[i].child[j].DownPayment * 1.1;
                            // console.log("TotalDP bukan dari Estimasi", gridTemp[i].child[j].DownPayment * 1.1);
                            totalDp += gridTemp[i].child[j].DownPayment;
                            console.log("TotalDP bukan dari Estimasi", gridTemp[i].child[j].DownPayment);
                            
                            
                        }
                    }
                }
            }
            $scope.totalDP = totalDp


            var totalDefaultDp = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].minimalDp !== undefined) {
                            totalDp += gridTemp[i].child[j].minimalDp;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].minimalDp);
                        }
                    }
                }
            }
            $scope.totalDpDefault = totalDefaultDp;

            
            var totalMaterial_byKevin = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if(gridTemp[i].child[j].PaidById == 2277 || gridTemp[i].child[j].PaidById == 30){
                            totalMaterial_byKevin += 0;
                        }else{
                            // totalMaterial_byKevin += Math.round(gridTemp[i].child[j].RetailPrice/1.1) * gridTemp[i].child[j].Qty;
                            totalMaterial_byKevin += ASPricingEngine.calculate({
                                                        InputPrice: gridTemp[i].child[j].RetailPrice,
                                                        // Discount: 5,
                                                        Qty: gridTemp[i].child[j].Qty,
                                                        Tipe: 102, 
                                                        PPNPercentage: PPNPerc,
                                                    }); 
                        }
                    }
                }
            };
            
            var totalMD_byKevin = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if(gridTemp[i].child[j].PaidById == 2277 || gridTemp[i].child[j].PaidById == 30 ){ //req by pak eko bahan tetap dapet diskon 08-juli-2020
                            totalMD_byKevin += 0;
                        }else{
                            // totalMD_byKevin +=  Math.round(Math.round(gridTemp[i].child[j].RetailPrice /1.1 ) * (gridTemp[i].child[j].Discount/100)) *  gridTemp[i].child[j].Qty;
                            totalMD_byKevin +=  ASPricingEngine.calculate({
                                                    InputPrice: gridTemp[i].child[j].RetailPrice,
                                                    Discount: gridTemp[i].child[j].Discount,
                                                    Qty: gridTemp[i].child[j].Qty,
                                                    Tipe: 112, 
                                                    PPNPercentage: PPNPerc,
                                                }); 
                            
                        }
                    }
                }
            };

            var totalWD_byKevin = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if(gridTemp[i].PaidById == 2277 || gridTemp[i].PaidById == 30){
                    totalWD_byKevin += 0;
                }else{
                    // totalWD_byKevin +=  Math.round(Math.round(gridTemp[i].Fare /1.1 ) * (gridTemp[i].Discount/100));
                    totalWD_byKevin +=  ASPricingEngine.calculate({
                                            InputPrice: gridTemp[i].Fare,
                                            Discount: gridTemp[i].Discount,
                                            // Qty: 2,
                                            Tipe: 12, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                }
            };





            $scope.totalMaterial           = totalMaterial_byKevin;
            $scope.totalMaterialDiscounted = totalMD_byKevin;
            $scope.totalWorkDiscounted     = totalWD_byKevin;
            $scope.subTotalMaterialSummary = $scope.totalMaterial - $scope.totalMaterialDiscounted
            $scope.subTotalWorkSummary     = $scope.totalWork - $scope.totalWorkDiscounted;
            // $scope.totalPPN                = Math.floor(($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * 0.1);     
            $scope.totalPPN                = ASPricingEngine.calculate({
                                                InputPrice: (($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * (1+PPNPerc/100)),
                                                // Discount: 0,
                                                // Qty: 0,
                                                Tipe: 33, 
                                                PPNPercentage: PPNPerc,
                                            });
            $scope.totalEstimasi           = $scope.totalPPN + $scope.subTotalMaterialSummary + $scope.subTotalWorkSummary;


            // added by sss on 2017-09-14
            console.log('id yang di delete ====>',id);
            // var indexFnd = _.findIndex($scope.gridEstimation.data, { TaskBPId: id[0] });
            // if (indexFnd > -1) {
            //     $scope.gridEstimation.data.splice(indexFnd, 1);
            // }

            for( var x in $scope.gridEstimation.data ){
                for (var y in id){
                    if($scope.gridEstimation.data[x].TaskBPId == id[y]){
                        $scope.gridEstimation.data.splice(x, 1);
                    }
                }
            }

            if ($scope.gridEstimation.data.length == 0) {
                $scope.appDate = true;
            }

            //nentuin diskon task & parts
            jejeranDiskonTask = [];
            jejeranDiskonParts = [];
            for(var i in $scope.gridWork){
                // if($scope.gridWork.PaidById == 30|| $scope.gridWork.PaidById == 31 || $scope.gridWork.PaidById == 32 || $scope.gridWork.PaidById == 2277){
                if($scope.gridWork.PaidById == 30|| $scope.gridWork.PaidById == 2277){
                    jejeranDiskonTask.push(0);
                }else{
                    jejeranDiskonTask.push($scope.gridWork[i].Discount);
                }
                if($scope.gridWork[i].child.length > 0 ){
                    for(var j in $scope.gridWork[i].child){
                        // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 31 || $scope.gridWork[i].child[j].PaidById == 32 || $scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 2277){
                            jejeranDiskonParts.push(0);
                        }else{
                            jejeranDiskonParts.push($scope.gridWork[i].child[j].Discount);
                        }

                        
                    }
                }
            }
            //nentuin diskon task & parts
            $scope.displayDiscountAlgorithm();

            setTimeout(function(){
                $scope.calculateforwork();
            },300);
            //
        }
        $scope.checkPrice = function(data) {
            console.log("checkPrice data ===>", data);
            console.log('$scope.ldModel | checkPrice ===>',$scope.ldModel)

            // req by pak eko bahan tetap dapet diskon 08-juli-2020
            // if($scope.ldModel.MaterialTypeId == 2){ //bahan 
            //     $scope.DiscountParts = 0;
            //     $scope.mData.disParts = 0;
            //     console.log('Diskon Parts | Bahan ===>', $scope.DiscountParts, $scope.mData.disParts ) ;
            // }else{ //parts
            
                if($scope.mData.isCash == 1){ //cash
                    $scope.choosePembayaran($scope.mData.isCash,false);
                    console.log('Diskon Parts | Cash | Spare Parts===>', $scope.DiscountParts, $scope.mData.disParts ) ;

                }else{ //isurance
                    for (i in $scope.Asuransi){
                        if($scope.Asuransi[i].InsuranceId == insuranceId){
                            $scope.mData.InsuranceName = $scope.Asuransi[i].InsuranceName;
                            $scope.selectInsurance($scope.Asuransi[i],false);
                        }
                    }
                    console.log('Diskon Parts | Isurance| Spare Parts===>', $scope.DiscountParts, $scope.mData.disParts ) ;

                }
            // }



            var tmpharga = angular.copy(data);
            // tmpharga = tmpharga.toString().replace(/,/g, '');
            if (tmpharga.toString().includes(',')){
                tmpharga = tmpharga.toString().replace(/,/g, '');
            }
            tmpharga = parseInt(tmpharga)
            var sum = 0;
            var sumDp = 0;
            if (data != null && data!= undefined){
                if ($scope.ldModel.Qty !== null) {
                    var qty = $scope.ldModel.Qty;
                    console.log('$scope.ldModel.minimalDp =====>',$scope.ldModel.minimalDp, $scope.ldModel)
                    var tmpDp = $scope.ldModel.minimalDp ? $scope.ldModel.minimalDp : 0;
                    if (qty !== null) {
                        sum = qty * tmpharga;
                        sumDp = qty * tmpDp;
                        var sum2 = angular.copy(sum);
                        sum2 = sum.toString();
                        // ==== new code ===
                        $scope.ldModel.subTotalMask = sum2.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        $scope.ldModel.RetailPrice = tmpharga
                        $scope.ldModel.DiscountTypeId = -1; 
                        // ===============
                        //DI COMMENT KARENA HARGA TIDAK UPDATE DI GRID
                        // $scope.ldModel.subTotal = sum2.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        //
                        // var XDp = sumDp.toString();
                        // var XSumDp = '';
                        // if (XDp.includes('.')) {
                        //     XDp = sumDp.toString().split('.');
                        //     XDp[0] = XDp[0].toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        //     XSumDp = XDp[0] + '.' + XDp[1];
                        // } else {
                        //     XSumDp = XDp;
                        // }
                        // if (XSumDp == 'NaN') {
                        //     XSumDp = '';
                        // }
                        // $scope.ldModel.DPRequest = XSumDp;
                        // $scope.ldModel.DownPayment = XSumDp;
                        // var XSumDp2 = angular.copy(XSumDp)
                        // XSumDp2 = XSumDp.toString();
                        // $scope.ldModel.DPRequest = XSumDp2.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        // $scope.ldModel.DownPayment = XSumDp2.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    
                        //tambahan CR4  start ==> request okrivia (qty * harga parts ) - diskon
                        // if($scope.ldModel.PaidById == 30 || $scope.ldModel.PaidById == 31 || $scope.ldModel.PaidById == 32 || ($scope.ldModel.MaterialTypeId == 2 && $scope.ldModel.PaidById == 28)){ 
                        // if($scope.ldModel.PaidById == 30 || $scope.ldModel.PaidById == 31 || $scope.ldModel.PaidById == 32){ //req by pak eko bahan tetap dapet diskon 08-juli-2020
                        if($scope.ldModel.PaidById == 30 || $scope.ldModel.PaidById == 2277){ //req by pak eko bahan tetap dapet diskon 08-juli-2020
                            $scope.ldModel.Discount = 0;
                            $scope.ldModel.DiskonForDisplay = 0+'%';
                            $scope.ldModel.NominalDiscount = 0;
                            $scope.ldModel.subTotalForDisplay = sum - $scope.ldModel.NominalDiscount;
                            $scope.ldModel.subTotal = sum;
                        }else{
                            $scope.ldModel.Discount = $scope.mData.disParts;
                            $scope.ldModel.DiskonForDisplay = $scope.mData.disParts+'%';
                            $scope.ldModel.NominalDiscount = (qty*tmpharga)*($scope.mData.disParts/100);
                            $scope.ldModel.subTotalForDisplay = sum - $scope.ldModel.NominalDiscount;
                            $scope.ldModel.subTotal = sum;
                        }
                        // tambahan CR4 end
    
                        // $scope.ldModel.subTotal = sum;
                        $scope.ldModel.DPRequest = sumDp;
                        $scope.ldModel.DownPayment = sumDp;
    
                    } else {
                        $scope.ldModel.subTotal = 0;
                        $scope.ldModel.RetailPrice = tmpharga;
                        var XDp = tmpDp.toString();
                        var XtmpDp = '';
                        if (XDp.includes('.')) {
                            XDp = tmpDp.toString().split('.');
                            XDp[0] = XDp[0].toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                            XtmpDp = XDp[0] + '.' + XDp[1];
                        } else {
                            $scope.ldModel.subTotal = 0;
                            $scope.ldModel.RetailPrice = tmpharga;
                            var XDp = tmpDp.toString();
                            var XtmpDp = '';
                            if (XDp.includes('.')) {
                                XDp = tmpDp.toString().split('.');
                                XDp[0] = XDp[0].toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                                XtmpDp = XDp[0] + '.' + XDp[1];
                            } else {
                                XtmpDp = XDp;
                            }
                            if (XSumDp == 'NaN') {
                                XSumDp = '';
                            }
                            $scope.ldModel.DPRequest = XSumDp;
                            $scope.ldModel.DownPayment = XSumDp;
                            var XSumDp2 = angular.copy(XSumDp)
                            XSumDp2 = XSumDp.toString();
                            $scope.ldModel.DPRequest = XSumDp2.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                            $scope.ldModel.DownPayment = XSumDp2.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        }
                    }
                } else {
                    $scope.ldModel.subTotal = 0;
                    // $scope.ldModel.Discount = 0;
                    $scope.ldModel.DownPayment = 0;
                    $scope.ldModel.DPRequest = 0;
                    $scope.ldModel.minimalDp = 0;
                }

            } else {
                $scope.ldModel.subTotal = 0;
                // $scope.ldModel.Discount = 0;
                $scope.ldModel.DownPayment = 0;
                $scope.ldModel.DPRequest = 0;
                $scope.ldModel.minimalDp = 0;
            }
            
            
        };
        $scope.checkDp = function(valueDP, param){
            console.log('DP =====>', valueDP, param);
            if(param.nightmareIjal == 0 && param.Availbility == "Tidak Tersedia"){
                $scope.ldModel.minimalDp = valueDP;
            }
        }
        $scope.checkIsOPB = function(data, obj){
            $timeout(function(){
                if(obj.IsOPB == 1){
                    console.log("=====", data, obj)
                    $scope.ldModel.PartsCode = null;
                    $scope.ldModel.PartsId = null;
                    $scope.ldModel.PartId = null;
                    $scope.ldModel.PartsName = null
                    $scope.partsAvailable = false;
                }else{
                    $scope.partsAvailable = true;
                }
            },100)
        }
        $scope.checkAvailabilityParts = function(data) {
            flagArrayDP = 0;
            console.log("data data data Parts", data);
            if (data.PartsId !== undefined) {
                AppointmentBpService.getAvailableParts(data.PartsId, 0, $scope.ldModel.Qty).then(function(res) {
                    // ====== validasi cek kl retailprice nya null ato 0 ga blh di pake ==================================================== start
                    if ((res.data.Result[0].RetailPrice === 0 || res.data.Result[0].RetailPrice === null || res.data.Result[0].RetailPrice === undefined) && data.MaterialTypeId == 1){
                        bsAlert.warning('Harga Retail Belum Tersedia. Harap hubungi Partsman/Petugas Gudang Bahan.');
                        return
                    }
                    // ====== validasi cek kl retailprice nya null ato 0 ga blh di pake ==================================================== end
                    console.log("Ressss abis availability", res.data);
                    var tmpAvailability = res.data.Result[0];
                    tmpAvailability.RetailPrice = Math.round(tmpAvailability.RetailPrice);
                    $scope.ldModel.RetailPriceMask =  tmpAvailability.RetailPrice.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                    $scope.ldModel.RetailPrice = tmpAvailability.RetailPrice; //tmpAvailability.RetailPrice.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                    $scope.ldModel.PaidById = $scope.tipePembayaranJob;
                   
                    _.map($scope.paymentData,function(val){
                        // if(val.MasterId == $scope.tipePembayaranJob ){
                        if(val.Name == $scope.lmModel.PaidBy ){

                            $scope.ldModel.PaidBy = val.Name;
                            $scope.selectTypePaidParts(val);
                        }
                    })
                    //ddddd
                    // $scope.ldModel.minimalDp = tmpAvailability.ValueDP;
                    $scope.ldModel.minimalDp = tmpAvailability.PriceDP;
                    $scope.ldModel.temporaryDp = Math.round(tmpAvailability.PriceDP);

                    // if ($scope.ldModel.PaidBy == "Internal" || $scope.ldModel.PaidBy == "Warranty" || $scope.ldModel.PaidBy == " Free Service" || $scope.JobIsWarranty == 1) {
                    if ($scope.ldModel.PaidById != 28 || $scope.JobIsWarranty == 1) {
                        $scope.ldModel.DownPayment = 0;
                        $scope.ldModel.DPRequest = 0;
                        $scope.ldModel.minimalDp = 0;
                        $scope.disableDP = true;
                    } else {
                        $scope.ldModel.minimalDp = Math.round(tmpAvailability.PriceDP);
                        $scope.ldModel.DPRequest = Math.round(tmpAvailability.PriceDP);
                        $scope.ldModel.DownPayment = Math.round(tmpAvailability.PriceDP);
                        $scope.disableDP = false;
                    }
                    if ($scope.ldModel.Qty !== undefined && $scope.ldModel.Qty !== null) {
                        $scope.checkPrice(tmpAvailability.RetailPrice);
                    }

                    switch (tmpAvailability.isAvailable) {
                        case 0:
                            $scope.ldModel.Availbility = "Tidak Tersedia";
                            if (tmpAvailability.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                $scope.ldModel.ETA = tmpAvailability.DefaultETA;
                            } else {
                                $scope.ldModel.ETA = tmpAvailability.DefaultETA;
                            }

                            if(tmpAvailability.PartsClassId3 == 113 && tmpAvailability.PartsClassId1 == 1) {
                                $scope.ldModel.OrderType = 6;
                                data.OrderType = 6;
                            }
                            else {
                                $scope.ldModel.OrderType = 3;
                                data.OrderType = 3;
                            }
                            break;
                        case 1:
                            $scope.ldModel.Availbility = "Tersedia";
                            //kl tersedia dp nya 0 in dan disable
                            $scope.ldModel.DownPayment = 0;
                            break;
                        case 2:
                            $scope.ldModel.Availbility = "Tersedia Sebagian";
                            //kl tersedia dp nya 0 in dan disable
                            $scope.ldModel.DownPayment = 0; //ngikutin yang tersedia 
                            if(tmpAvailability.PartsClassId3 == 113 && tmpAvailability.PartsClassId1 == 1) {
                                $scope.ldModel.OrderType = 6;
                            }    
                            break;
                    }
                });
            }
        }
        $scope.selectTypeParts = function(data) {
            console.log('selectTypeParts ===>',data);

            // // jancok
            // if(data.MaterialTypeId == 2){ //bahan
            //     $scope.DiscountParts = 0;
            //     $scope.mData.disParts = 0;
            //     console.log('Diskon Parts | Bahan ===>', $scope.DiscountParts, $scope.mData.disParts ) ;
            // }else{ //parts

            //     if($scope.mData.isCash == 1){ //cash
            //         $scope.choosePembayaran($scope.mData.isCash);
            //         console.log('Diskon Parts | Cash | Spare Parts===>', $scope.DiscountParts, $scope.mData.disParts ) ;

            //     }else{ //isurance
            //         for (i in $scope.Asuransi){
            //             if($scope.Asuransi[i].InsuranceId == insuranceId){
            //                 $scope.mData.InsuranceName = $scope.Asuransi[i].InsuranceName;
            //                 $scope.selectInsurance($scope.Asuransi[i]);
            //             }
            //         }
            //         console.log('Diskon Parts | Isurance| Spare Parts===>', $scope.DiscountParts, $scope.mData.disParts ) ;

            //     }
            // }
            
            $scope.ldModel.SatuanId = 4;
            $scope.ldModel.OrderType = 3;
            $scope.ldModel.PartsCode = null;
            $scope.ldModel.PartsId = null;
            if(data.MaterialTypeId == 1){
                $scope.ldModel.IsOPB = 0;
            }
            _.map($scope.unitData,function(val){
                if(val.MasterId == 4){
                    $scope.ldModel.satuanName = val.Name;
                    console.log('finded satuan name ===>',$scope.ldModel.satuanName);
                }
            });

            if ($scope.JobIsWarranty == 1) {
                console.log('pekerjaan nya warranty nih parts nya jg harus', $scope.ldModel.PaidById);
                _.map($scope.paymentData, function(val) {
                    console.log("valll tipe", val);
                    if (val.Name === "Warranty") {
                        $scope.ldModel.PaidById = val.MasterId;
                        $scope.ldModel.DownPayment = 0;
                        $scope.ldModel.DPRequest = 0;
                        $scope.ldModel.minimalDp = 0;
                        $scope.disableDP = true;
                    }
                })

            } else {
                console.log('pekerjaan nya bukan warranty');
            }

        }


       
        $scope.getAvailablePartsService = function(item, row) {
            if (item.PartsId !== null) {
                AppointmentBpService.getAvailableParts(item.PartsId, 0 ,item.Qty).then(function(res) {
                    var tmpRes = res.data.Result[0];
                    console.log("tmpRes", tmpRes);
                    tmpParts = tmpRes;
                    if (tmpParts !== null) {
                        console.log("have data RetailPrice");
                        item.RetailPrice = tmpParts.RetailPrice;
                        item.RetailPriceMask = tmpParts.RetailPrice.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        item.subTotal = item.Qty * tmpParts.RetailPrice;
                        item.subTotalMask = item.subTotal.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        item.DPRequest = tmpParts.PriceDP * item.Qty;
                        item.DownPayment = tmpParts.PriceDP * item.Qty;
                        

                        if (tmpParts.isAvailable == 0) {
                            item.Availbility = "Tidak Tersedia";
                            if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                item.ETA = "";
                            } else {
                                item.ETA = tmpParts.DefaultETA;
                            }

                            if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                item.OrderType = 6;
                            }
                            else {
                                item.OrderType = 3;
                            }

                        } else if(tmpParts.isAvailable == 1){
                            item.Availbility = "Tersedia";
                            //kl tersedia dp nya 0 in dan disable
                            item.DownPayment = 0;
                        }else if(tmpParts.isAvailable == 2){
                            item.Availbility = "Tersedia Sebagian";
                            //kl tersedia dp nya 0 in dan disable
                            item.DownPayment = 0;
                            if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                item.OrderType = 6;
                            }
                        }
                    } else {
                        console.log("haven't data RetailPrice");
                    }
                    if ($scope.tmpPaidById !== null) {
                        item.PaidById = $scope.tmpPaidById;
                        item.paidName = $scope.tmpPaidName;
                    }
                    tmp = item;
                    $scope.listApi.addDetail(tmp, row);
                    return item;
                });
            } else {
                if ($scope.tmpPaidById !== null && $scope.tmpPaidById !== undefined) {
                    item.PaidById = $scope.tmpPaidById;
                    item.paidName = $scope.tmpPaidName;
                }
                item.RetailPrice = item.Price;
                item.subTotal = item.Qty * item.Price;
                item.DPRequest = item.DPRequest * item.Qty;
                item.DownPayment = item.DPRequest * item.Qty;
                

                tmp = item;
                $scope.listApi.addDetail(tmp, row);
                return item;
            }
        }
        // $scope.getAvailablePartsServiceManualFromEstimasiBP = function(item) {
        //     console.log('getAvailablePartsServiceManualFromEstimasiBP satu param | item ===>',item)
        //     if (item.PartsId !== null) {

        //         AppointmentBpService.getAvailableParts(item.PartsId).then(function(res) {
        //             var tmpRes = res.data.Result[0];
        //             tmpParts = tmpRes;
        //             if (tmpParts !== null) {
        //                 console.log("have data RetailPrice");
        //                 item.RetailPrice = tmpParts.RetailPrice;
        //                 item.minimalDp = tmpParts.PriceDP;
        //                 item.subTotal = item.Qty * item.RetailPrice;
        //                 item.subTotalForDisplay = item.Qty * tmpParts.RetailPrice - ((item.Qty * tmpParts.RetailPrice) * item.Discount/100);
        //                 item.DiskonForDisplay = item.Discount + '%';
        //                 if (tmpParts.isAvailable == 0) {
        //                     item.Availbility = "Tidak Tersedia";
        //                     if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
        //                         item.ETA = "";
        //                     } else {
        //                         item.ETA = tmpParts.DefaultETA;
        //                     }
        //                     item.OrderType = 3;
        //                 } else {
        //                     item.Availbility = "Tersedia";
        //                 }
        //             } else {
        //                 console.log("haven't data RetailPrice");
        //             }
        //             Parts.push(item);
        //             $scope.sumAllPrice();
        //             return item;
        //         });
        //     } else {
        //         item.RetailPrice = item.Price;
        //         item.subTotal = item.Qty * item.Price;
        //         item.DPRequest = item.DPRequest * item.Qty;
        //         item.DownPayment = item.DPRequest * item.Qty;
        //         item.subTotalForDisplay = item.Qty * tmpParts.RetailPrice - ((item.Qty * tmpParts.RetailPrice) * item.Discount/100);
        //         item.DiskonForDisplay = item.Discount + '%';
        //         tmp = item;
        //         Parts.push(item);
        //         $scope.sumAllPrice();
        //         return item;
        //     }
        // };


        
        // ===========================
        // Allow Pattern
        // ===========================
        $scope.allowPattern = function(event, type, item) {
            var patternRegex
            if (type == 1) {
                patternRegex = /\d/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /\d|[a-z]|[*]/i; //ALPHANUMERIC ONLY
                if (item.includes("*")) {
                    event.preventDefault();
                    return false;
                }
            }
            console.log("event", event);
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };
        // $scope.givePattern = function(item, event) {
        //     console.log("item", item);
        //     if (event.which > 37 && event.which < 40) {
        //         event.preventDefault();
        //         return false;
        //     } else {
        //         $scope.mData.Km = $scope.mData.Km.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        //         return;
        //     }
        // };

        $scope.givePatternHarga = function(item, event) {
            console.log("item", item);
            var temp = {};
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
                var tmpItem = angular.copy(item);
                temp.Fare = parseInt(tmpItem.replace(/,/g,''));
                item = item.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                temp.FareMask = item;
                $scope.listApi.addDetail(false, temp);
                return;
            }
        };
        $scope.givePatternHargaPart = function(item, event) {
            console.log("item", item);
            var temp = {};
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
                item = item.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                // temp.RetailPrice = item;
                // $scope.listApi.addDetail(false, temp);
                console.log("givePatternDp", item);
                // $scope.ldModel.RetailPrice = item;
                $scope.ldModel.RetailPriceMask = item;

                return;
            }
        };
        $scope.givePatternDp = function(item, event) {
            console.log("item", item);
            var temp = {};
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
                item = item.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                temp.DownPayment = item;
                $scope.listApi.addDetail(false, temp);
                console.log("givePatternDp", item);
                $scope.ldModel.DownPayment = item;
                return;
            }
        };
        $scope.harga = function() {
            console.log('aso', $scope.gridWork);
        }


        // ==============
        // Print
        // ==============
        $scope.actPrintAppointment = function(data) {
            console.log('JobId cetakAppointment', data);
            $scope.printAppoinmentBP = 'as/PrintAppointmentBp/' + data + '/' + $scope.user.OrgId + '/' + 0;
            $scope.cetakan($scope.printAppoinmentBP);
        }
        $scope.cetakan = function(data) {
            var pdfFile = null;
            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    } else {
                        printJS(pdfFile);
                    }
                } else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };
        // ========= WAC=====
        $scope.conditionData = [{ Id: 0, Name: "Not Ok" }, { Id: 1, Name: "Ok" }];
        $scope.mDataWAC = {};
        $scope.clickWACcount = 0;
        $scope.dataForWAC = function(data) {
            console.log('data | dataForWAC ===>',data);
            // if ($scope.clickWACcount == 0) {
            RepairProcessGR.getDataWac(data.JobId).then(
                function(res) {
                    var dataWac = res.data;
                    $scope.finalDataWac = dataWac;

                    console.log("Data WAC", dataWac);
                    $scope.mData.OtherDescription = dataWac.Other;
                    dataWac.MoneyAmount = String(dataWac.MoneyAmount);
                    $scope.mData.moneyAmount = dataWac.MoneyAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                    // $scope.mData.moneyAmount = dataWac.MoneyAmount;
                    $scope.mData.BanSerep = dataWac.isSpareTire;
                    $scope.mData.CdorKaset = dataWac.isCD;
                    $scope.mData.Payung = dataWac.isUmbrella;
                    $scope.mData.Dongkrak = dataWac.isJack;
                    $scope.mData.STNK = dataWac.isVehicleRegistrationNumber;
                    $scope.mData.P3k = dataWac.isP3k;
                    $scope.mData.KunciSteer = dataWac.isSteerLock;
                    $scope.mData.ToolSet = dataWac.isToolSet;
                    $scope.mData.ClipKarpet = dataWac.isCarpetClip;
                    $scope.mData.BukuService = dataWac.isServiceBook;
                    $scope.mData.AlarmCondition = dataWac.isAlarmConditionOk;
                    $scope.mData.AcCondition = dataWac.isAcOk;
                    $scope.mData.PowerWindow = dataWac.isPowerWindowOk;
                    $scope.mData.CarType = dataWac.isPowerWindowOk;
                    // $scope.slider.options = 100;
                    $scope.slider.value = dataWac.Fuel;
                    $scope.mData.Km = dataWac.Km;
                    // $scope.mData.TechnicianNotes = TeknisiNote[0].TechnicianNotes;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            RepairProcessGR.getProbPoints(data.JobId, null).then(
                function(res) {
                    if (res.data.Result.length > 0) {
                        $scope.disableWACExt = false;
                    } else {
                        $scope.disableWACExt = true;
                    }
                    console.log("RepairProcessGR.getProbPoints", res.data);
                    mstId = angular.copy(res.data.Result[0].TypeId);
                    console.log("ini MstId", mstId);
                    $scope.filter.vType = mstId;
                    console.log("probPoints", res.data.Result);
                    // $scope.chooseVType(mstId);
                    $scope.areasArray = getImgAndPoints(mstId);
                    console.log("$scope.areasArray", $scope.areasArray);
                    if (res.data.Result.length > 0) {
                        for (var i in res.data.Result) {
                            var getTemp = JSON.parse(res.data.Result[i]["Points"]);
                            for (var j in getTemp) {
                                var xtemp = angular.copy(getTemp[j]);
                                delete xtemp['status'];
                                console.log("xtemp : ", xtemp);
                                if (typeof _.find($scope.areasArray, xtemp) !== 'undefined') {
                                    console.log("find..", xtemp);
                                    var idx = _.findIndex($scope.areasArray, xtemp);
                                    xtemp.cssClass = "choosen-area";
                                    $scope.areasArray[idx] = xtemp;

                                    dotsData[mstId].push({ "ItemId": xtemp.areaid, "ItemName": xtemp.name, "Points": xtemp, "Status": getTemp[j].status });
                                }
                            }
                        }
                        setDataGrid(dotsData[mstId]);
                    }
                },
                function(err) {
                    $scope.areasArray = getImgAndPoints(mstId);
                }
            );
            $scope.clickWACcount++;
            // } else {
            // console.log("di click mulu, rese but sih lu");
            // }
        }
        $scope.tabClick = function(param) {
            $scope.getDataExt(param);
        };
        $scope.getDataExt = function(id) {
            console.log('id header', id)
            WOBP.getItemWAC(id).then(function(resu) {
                console.log('item wac', resu.data.Result);
                if (id == 2 && $scope.gridExt.data.length == 0) {
                    $scope.dataExt = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataExt, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = '';
                        obj.OtherDescription = null;

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Ext = arr;
                    $scope.gridExt.data = $scope.mDataWAC.Ext;
                    // console.log('$scope.mDataWAC.Ex', $scope.mDataWAC.Ext);
                } else if (id == 4 && $scope.gridInt.data.length == 0) {
                    $scope.dataInt = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataInt, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = '';
                        obj.OtherDescription = null;

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Int = arr;
                    $scope.gridInt.data = $scope.mDataWAC.Int;
                } else if (id == 1003 && $scope.gridElec.data.length == 0) {
                    $scope.dataElec = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataElec, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = '';
                        obj.OtherDescription = null;

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Elec = arr;
                    $scope.gridElec.data = $scope.mDataWAC.Elec;
                } else if (id == 1004 && $scope.gridEquip.data.length == 0) {
                    $scope.dataEquip = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataEquip, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = '';
                        obj.OtherDescription = null;

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Equip = arr;
                    $scope.gridEquip.data = $scope.mDataWAC.Equip;
                } else if (id == 1005 && $scope.gridDoc.data.length == 0) {
                    $scope.dataDoc = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataDoc, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = '';
                        obj.OtherDescription = null;

                        arr.push(obj);
                    });
                    console.log('grid dataDoc arr', arr);
                    $scope.mDataWAC.Doc = arr;
                    $scope.gridDoc.data = $scope.mDataWAC.Doc;
                } else if (id == 1006 && $scope.gridOther.length == 0) {
                    $scope.dataOther = resu.data.Result;
                    console.log('grid other ', $scope.dataOther[0]);
                    var arr = [];
                    angular.forEach($scope.dataOther, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = null;
                        obj.OtherDescription = '';
                        arr.push(obj);
                    });
                    $scope.mDataWAC.Oth = arr;
                    $scope.gridOther = $scope.mDataWAC.Oth;
                    console.log('grid other arr', $scope.gridOther);
                } else if (id == 1007 && $scope.gridEquipSafe.data.length == 0) {
                    $scope.dataOther = resu.data.Result;
                    console.log('grid other ', $scope.dataOther[0]);
                    var arr = [];
                    angular.forEach($scope.dataOther, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = null;
                        obj.OtherDescription = '';
                        arr.push(obj);
                    });
                    $scope.mDataWAC.Oth = arr;
                    $scope.gridEquipSafe.data = $scope.mDataWAC.Oth;
                    console.log('grid other arr', $scope.gridEquipSafe.data);
                } else if (id == 1008 && $scope.gridPersonal.data.length == 0) {
                    $scope.dataOther = resu.data.Result;
                    console.log('grid other ', $scope.dataOther[0]);
                    var arr = [];
                    angular.forEach($scope.dataOther, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = null;
                        obj.OtherDescription = '';
                        arr.push(obj);
                    });
                    $scope.mDataWAC.Oth = arr;
                    $scope.gridPersonal.data = $scope.mDataWAC.Oth;
                    console.log('grid other arr', $scope.gridPersonal.data);
                }
            })
        };
        //WAC LIST > Body ------------------------------------

        $scope.coorX = null;
        $scope.coorY = null;
        $scope.drawData = [];
        $scope.showPopup = false;
        $scope.openPopup = function(e) {
            ////
            $scope.showPopup = true;
            var left = e.offsetX;
            var top = e.offsetY;
            $scope.coorX = angular.copy(left) + 'px';
            $scope.coorY = angular.copy(top) + 'px';
            console.log('event X', $scope.coorX);
            console.log('event Y', $scope.coorY);
            $scope.popoverStyle = {
                'position': 'absolute',
                'background': '#fff',
                'border': '1px solid #999',
                'padding': '10px',
                'width': 'auto',
                'box-shadow': '0 0 10px rgba(0, 0, 0, .5)',
                'left': $scope.coorX,
                'top': $scope.coorY
            };
            $scope.drawData = [{
                x: $scope.coorX,
                y: $scope.coorY
            }];
            console.log('scope.drawData', $scope.drawData);
        };

        $scope.closePopover = function(param) {
            $scope.paramClosePopover = param;
            $scope.showPopup = false;
            console.log('scope.paramClosePopover', $scope.paramClosePopover);
        };


        //WAC LIST > Exterior --------------------------------
        $scope.ItemStatusTemplate = '<div ng-disabled="true" class="btn-group ui-grid-cell-contents">' +
            '<input ng-disabled="true" ng-model="row.entity.ItemStatus" type="radio" ng-value="1" style="width:20px">' +
            '&nbsp;Baik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
            '<input ng-disabled="true" ng-model="row.entity.ItemStatus" type="radio" ng-value="2" style="width:20px">' +
            '&nbsp;Rusak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
            '<input ng-disabled="true" ng-model="row.entity.ItemStatus" type="radio" ng-value="3" style="width:20px">' +
            '&nbsp;Tidak Ada</div>';

        $scope.ItemStatus = null;
        $scope.gridExt = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                field: 'ItemStatus',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridExtApi = gridApi;

                $scope.gridExtApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridExtApi.selection.selectRow) {
                    $scope.gridExtApi.selection.selectRow($scope.gridExt.data[0]);
                }
            }
        };
        //WAC LIST > Interior --------------------------------
        $scope.gridInt = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridIntApi = gridApi;

                $scope.gridIntApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridIntApi.selection.selectRow) {
                    $scope.gridIntApi.selection.selectRow($scope.gridInt.data[0]);
                }
            }
        };

        //WAC LIST > Electricity --------------------------------
        $scope.gridElec = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridElecApi = gridApi;

                $scope.gridElecApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridElecApi.selection.selectRow) {
                    $scope.gridElecApi.selection.selectRow($scope.gridElec.data[0]);
                }
            }
        };

        //WAC LIST > Equipment --------------------------------
        $scope.gridEquip = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridEquipApi = gridApi;

                $scope.gridEquipApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridEquipApi.selection.selectRow) {
                    $scope.gridEquipApi.selection.selectRow($scope.gridEquip.data[0]);
                }
            }
        };

        //WAC LIST > Document --------------------------------
        $scope.gridDoc = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridDocApi = gridApi;

                $scope.gridDocApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridDocApi.selection.selectRow) {
                    $scope.gridDocApi.selection.selectRow($scope.gridDoc.data[0]);
                }
            }
        };
        //WAC LIST > Other ----------------------------------
        $scope.summernoteOptions2 = {
            dialogsInBody: true,
            dialogsFade: false,
            height: 300
        };
        //WAC LIST > Equipment Safety --------------------------------
        $scope.gridEquipSafe = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridEquipSafeApi = gridApi;

                $scope.gridEquipSafeApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridEquipSafeApi.selection.selectRow) {
                    $scope.gridEquipSafeApi.selection.selectRow($scope.gridEquipSafe.data[0]);
                }
            }
        };
        //WAC LIST > Personal Item --------------------------------
        $scope.gridPersonal = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridPersonalApi = gridApi;

                $scope.gridPersonalApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridPersonalApi.selection.selectRow) {
                    $scope.gridPersonalApi.selection.selectRow($scope.gridPersonal.data[0]);
                }
            }
        };
        $scope.areasArray = [];
        $scope.show_modal = { show: false };
        $scope.groupWACData = [];
        $scope.jobWACData = [];
        $scope.imageSel = null;
        $scope.imageWACArr = [];

        // added by sss on 2017-10-11
        $scope.areasArrayVC = [];
        $scope.imageSelVC = null;
        //

        $scope.chgChk = function() {
            if (!$scope.disableWACExt) {
                // console.log("false check..");
                // console.log("mstId : ",mstId);
                // var chAreas = document.getElementsByClassName('choosen-area');
                // for (var i in chAreas) {
                //     console.log(chAreas[i]);
                //     if (typeof chAreas[i].classList !== 'undefined') {
                //         chAreas[i].classList.remove('choosen-area');
                //     }
                // }
                $('.choosen-area').removeClass('choosen-area');
                $scope.grid.data = [];
                dotsData[mstId] = [];
            }
            $scope.disableWACExt = !$scope.disableWACExt;
        }

        // added by sss on 2017-10-11
        $scope.chgChkVC = function() {
                if (!$scope.disableWACExtVC) {
                    // console.log("false check..");
                    // console.log("mstId : ",mstId);
                    // var chAreas = document.getElementsByClassName('choosen-area');
                    // for (var i in chAreas) {
                    //     console.log(chAreas[i]);
                    //     if (typeof chAreas[i].classList !== 'undefined') {
                    //         chAreas[i].classList.remove('choosen-area');
                    //     }
                    // }
                    $('.choosen-area').removeClass('choosen-area');
                    $scope.gridVC.data = [];
                    dotsDataVC[mstIdVC] = [];
                }
                $scope.disableWACExtVC = !$scope.disableWACExtVC;
            }
            //

        GeneralMaster.getData(2030).then(
            function(res) {
                $scope.VTypeData = res.data.Result;
                console.log("$scope.VTypeData !!!!!!!!!!!!!!!!!!!!!!!!!!!!", $scope.VTypeData);
                return res.data;
            }
        );

        WOBP.getAllTypePoints().then(
            function(res) {
                // console.log(res);
                $scope.imageWACArr = res.data.Result;
                console.log("$scope.imageWACArr", $scope.imageWACArr);
                $scope.loading = false;
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );

        var dotsData = { 2221: [], 2222: [], 2223: [], 2224: [], 2225: [], 2226: [], 2227: [] };
        var mstId = null;

        // added by sss on 2017-10-11
        var dotsDataVC = { 2221: [], 2222: [], 2223: [], 2224: [], 2225: [], 2226: [], 2227: [] };
        var mstIdVC = null;
        //

        function getImgAndPoints(mstId) {
            switch (mstId) {
                case 2221:
                    $scope.imageSel = "../images/wacImages/Type A.png";
                    break;
                case 2222:
                    $scope.imageSel = "../images/wacImages/Type B.png";
                    break;
                case 2223:
                    $scope.imageSel = "../images/wacImages/Type C.png";
                    break;
                case 2224:
                    $scope.imageSel = "../images/wacImages/Type D.png";
                    break;
                case 2225:
                    $scope.imageSel = "../images/wacImages/Type E.png";
                    break;
                case 2226:
                    $scope.imageSel = "../images/wacImages/Type F.png";
                    break;
                case 2227:
                    $scope.imageSel = "../images/wacImages/Type G.png";
            }
            var temp = _.find($scope.imageWACArr, function(o) {
                return o.TypeId == mstId;
            });

            return JSON.parse(temp.Points);
        };

        // added by sss on 2017-10-11
        function getImgAndPointsVC(mstIdVC) {
            console.log("mstIdVC : ", mstIdVC);
            switch (mstIdVC) {
                case 2221:
                    $scope.imageSelVC = "../images/wacImages/Type A.png";
                    break;
                case 2222:
                    $scope.imageSelVC = "../images/wacImages/Type B.png";
                    break;
                case 2223:
                    $scope.imageSelVC = "../images/wacImages/Type C.png";
                    break;
                case 2224:
                    $scope.imageSelVC = "../images/wacImages/Type D.png";
                    break;
                case 2225:
                    $scope.imageSelVC = "../images/wacImages/Type E.png";
                    break;
                case 2226:
                    $scope.imageSelVC = "../images/wacImages/Type F.png";
                    break;
                case 2227:
                    $scope.imageSelVC = "../images/wacImages/Type G.png";
            }
            var temp = _.find($scope.imageWACArr, function(o) {
                return o.TypeId == mstIdVC;
            });

            console.log("temp : ", temp);

            return JSON.parse(temp.Points);
        };
        //

        $scope.chooseVType = function(selected) {
            // dotsData = {2221:[],2222:[],2223:[],2224:[],2225:[],2226:[],2227:[]};
            mstId = selected.MasterId;
            $scope.grid.data = angular.copy(dotsData[mstId]);

            WO.getProbPoints(1221, mstId).then(
                function(res) {
                    $scope.areasArray = getImgAndPoints(mstId);
                    console.log("$scope.areasArray", $scope.areasArray);
                    if (res.data.Result.length > 0) {
                        for (var i in res.data.Result) {
                            var getTemp = JSON.parse(res.data.Result[i]["Points"]);
                            for (var j in getTemp) {
                                var xtemp = angular.copy(getTemp[j]);
                                delete xtemp['status'];
                                console.log("xtemp : ", xtemp);
                                if (typeof _.find($scope.areasArray, xtemp) !== 'undefined') {
                                    console.log("find..", xtemp);
                                    var idx = _.findIndex($scope.areasArray, xtemp);
                                    xtemp.cssClass = "choosen-area";
                                    $scope.areasArray[idx] = xtemp;

                                    dotsData[mstId].push({ "ItemId": xtemp.areaid, "ItemName": xtemp.name, "Points": xtemp, "Status": getTemp[j].status });
                                }
                            }
                        }
                        setDataGrid(dotsData[mstId]);
                    }
                },
                function(err) {
                    $scope.areasArray = getImgAndPoints(mstId);
                }
            );
        };

        // added by sss on 2017-10-11
        $scope.mVehicleCondition = {};
        $scope.chooseVTypeVC = function(selected) {
            // dotsData = {2221:[],2222:[],2223:[],2224:[],2225:[],2226:[],2227:[]};
            mstIdVC = selected.MasterId;
            $scope.gridVC.data = angular.copy(dotsDataVC[mstIdVC]);

            $scope.areasArrayVC = getImgAndPointsVC(mstIdVC);
        };
        //

        $scope.onChangeAreas = function(ev, boxId, areas, area) {
            console.log(areas);
            $scope.logDots = JSON.stringify(areas);
            $scope.$apply();
        };

        $scope.onAddArea = function(ev, boxId, areas, area) {
            console.log(areas);
            $scope.logDots = JSON.stringify(areas);
            $scope.$apply();
        };

        $scope.onRemoveArea = function(ev, boxId, areas, area) {
            console.log(areas);
            $scope.logDots = JSON.stringify(areas);
            $scope.$apply();
        };

        $scope.disableWACExt = true;

        // added by sss on 2017-10-11
        $scope.disableWACExtVC = true;
        //

        var sel;
        $scope.doClick = function(ev, boxId, areas, area, stat, selection) {
            // console.log(boxId);
            // console.log(areas);
            // console.log(area);
            // console.log(stat);
            // if (stat) {
            //     console.log("true");
            //     // For @lifachristian
            //     if (selection.hasClass("choosen-area")) {
            //         if (sel != undefined) {
            //             sel.empty();
            //         }
            //         $name = $(selection).empty().append($("<div><span class=\"select-area-field-label border-thin name-area-id-" + area.areaid + "\">" + area.name + "</span></div>"));
            //         sel = selection;
            //     }
            // } else {
            console.log("false");
            // For @amieklo2204
            if (!$scope.disableWACExt) {
                if (selection.hasClass("choosen-area")) {
                    selection.removeClass("choosen-area");
                    if (_.find(dotsData[mstId], { "ItemId": area.areaid }) !== undefined) {
                        // console.log("find false");
                        dotsData[mstId].splice(_.findIndex(dotsData[mstId], { "ItemId": area.areaid }), 1);
                    }
                } else {
                    selection.addClass("choosen-area");
                    if (_.find(dotsData[mstId], { "ItemId": area.areaid }) == undefined) {
                        // console.log("if 1");
                        if (typeof _.find($scope.areasArray, { "areaid": area.areaid }) !== 'undefined') {
                            // console.log("if 2");
                            var addtemp = _.find($scope.areasArray, { "areaid": area.areaid });
                            dotsData[mstId].push({ "ItemId": area.areaid, "ItemName": area.name, "Points": addtemp, "Status": null, "Tasks": null });
                        }
                    }
                }
                setDataGrid(dotsData[mstId]);
            }
            // }
        }

        // added by sss on 2017-10-11
        $scope.doClickVC = function(ev, boxId, areas, area, stat, selection) {
                console.log("false");
                // For @amieklo2204
                if (!$scope.disableWACExtVC) {
                    if (selection.hasClass("choosen-area")) {
                        selection.removeClass("choosen-area");
                        if (_.find(dotsDataVC[mstIdVC], { "ItemId": area.areaid }) !== undefined) {
                            // console.log("find false");
                            dotsDataVC[mstIdVC].splice(_.findIndex(dotsDataVC[mstIdVC], { "ItemId": area.areaid }), 1);
                        }
                    } else {
                        selection.addClass("choosen-area");
                        console.log("boxId : ", boxId);
                        console.log("area : ", area);
                        console.log("areas : ", areas);
                        console.log("stat : ", stat);
                        console.log("selection : ", selection);
                        console.log("dotsDataVC[mstIdVC] : ", dotsDataVC[mstIdVC]);
                        console.log("$scope.areasArrayVC : ", $scope.areasArrayVC);

                        if (_.find(dotsDataVC[mstIdVC], { "ItemId": area.areaid }) == undefined) {
                            // console.log("if 1");
                            // if (typeof _.find($scope.areasArrayVC, { "areaid": area.areaid }) !== 'undefined') {
                            //     // console.log("if 2");
                            //     var addtemp = _.find($scope.areasArrayVC, { "areaid": area.areaid });
                            //     dotsDataVC[mstIdVC].push({ "ItemId": area.areaid, "ItemName": area.name, "Points": addtemp, "Status": null, "Tasks": null });
                            // }
                            if (typeof _.find(areas, { "areaid": area.areaid }) !== 'undefined') {
                                var addtemp = _.find(areas, { "areaid": area.areaid });
                                if (typeof _.find($scope.areasArrayVC, { "name": addtemp.name, "x": addtemp.x, "y": addtemp.y }) !== 'undefined') {
                                    // console.log("if 2");
                                    var addtempx = _.find($scope.areasArrayVC, { "name": addtemp.name, "x": addtemp.x, "y": addtemp.y });
                                    dotsDataVC[mstIdVC].push({ "ItemId": addtempx.areaid, "ItemName": addtempx.name, "Points": addtempx, "Status": null, "Tasks": null });
                                }
                            }
                        }
                        // added by sss on 2017-10-10
                        console.log("taskListObj[addtempx.areaid] : ", taskListObj[addtempx.areaid]);
                        $scope.gridVC.columnDefs[2].editDropdownOptionsArray = angular.copy(taskListObj[addtempx.areaid]);
                        // var temp = taskListObj[1].concat([{ 'TaskName': 'Perbaikan Sisi Bumper', 'TaskListBPId': 2 }, { 'TaskName': 'Amplas Samping Bumper', 'TaskListBPId': 3 }]);
                        // console.log('temp : ',temp);
                        // $scope.gridVC.columnDefs[2].editDropdownOptionsArray = angular.copy(temp);
                        //
                    }
                    setDataGridVC(dotsDataVC[mstIdVC]);
                }
                // }
            }
            //

        function setDataGrid(data) {
            console.log('data setDataGrid', data);
            $scope.gridWAC.data = angular.copy(data);
            console.log('$scope.gridWAC.data ==========>',$scope.gridWAC.data );
        }

        // added by sss on 2017-10-11
        function setDataGridVC(data) {
            console.log('data setDataGrid VC', data);
            $scope.gridVC.data = angular.copy(data);
        }
        //

        $scope.onDropdownChange = function(ent) {
            console.log(ent);
            var temp = _.findIndex(dotsData[mstId], { "ItemId": ent.ItemId, "ItemName": ent.ItemName, "Points": ent.Points });
            if (temp > -1) {
                dotsData[mstId][temp].Status = ent.Status;
            }
        }

        $scope.onMultiDropdownChange = function(ent) {
            console.log(ent);
            var xtemp = [];
            // added by sss on 2017-10-12
            $scope.gridWorkEstimate = ent.Tasks;
            _.forEach(ent.Tasks, function(e) {
                $scope.gridTimeEst.data.push({
                    "TaskBPId": e.TaskBPId,
                    "OrderName": e.TaskName,
                    "Body": e.BodyEstimationMinute,
                    "Putty": e.PuttyEstimationMinute,
                    "Surfacer": e.SurfacerEstimationMinute,
                    "Painting": e.SprayingEstimationMinute,
                    "Polishing": e.PolishingEstimationMinute,
                    "Reassembbly": e.ReassemblyEstimationMinute,
                    "Final_Inspection": e.FIEstimationMinute,
                });
            });
            //
            for (var i in ent.Tasks) {
                xtemp.push(ent.Tasks[i].TaskBPId);
            }
            // console.log("xtemp.length : ",xtemp.length);
            // if ((xtemp.length % 2 > 0) && (xtemp.length > 2)) {
            //     $scope.grid.rowHeight = (xtemp.length / 2 * 30)+15;
            // } else if (xtemp.length <= 2) {
            //     $scope.grid.rowHeight = 30;
            // }
            var temp = _.findIndex(dotsDataVC[mstIdVC], { "ItemId": ent.ItemId, "ItemName": ent.ItemName, "Points": ent.Points });
            if (temp > -1) {
                dotsDataVC[mstIdVC][temp].Tasks = xtemp;
            }
        }

        $scope.saveData = function() {
            var grdData = $scope.grid.data;
            var lastArr = [];

            var pointsArr = [];
            for (var i in dotsData[mstId]) {
                var tmpObj = dotsData[mstId][i].Points;
                tmpObj.status = dotsData[mstId][i].Status;
                pointsArr.push(tmpObj);
            }
            var temp = {};
            temp.JobWACExtId = 0;
            temp.JobId = 1221;
            temp.TypeId = mstId;
            temp.Points = JSON.stringify(pointsArr);
            temp.DataUploadUrl = {
                strBase64: $scope.mData.Signature
            };
            for (var i = 0; i < $scope.uploadFiles.length; i++) {
                if ($scope.uploadFiles[i].strBase64 !== null && typeof $scope.uploadFiles[i].strBase64 !== 'undefined') {
                    temp["DataUploadFoto" + (i == 0 ? "" : i + 1)] = {
                        strBase64: $scope.uploadFiles[i].strBase64
                    };
                }
            }

            lastArr.push(temp);
            console.log("lastArr : " + JSON.stringify(lastArr));

            WO.createEXT(lastArr).then(function(res) {
                console.log(res);
            });
        };

        //----------------------------------
        // Grid WAC EXTERIOR Setup
        //----------------------------------
        // var customCellTemplate = '<div><select ui-grid-edit-dropdown ng-model=\"MODEL_COL_FIELD\" ng-change="grid.appScope.onDropdownChange(row.entity)" ng-options=\"field[editDropdownIdLabel] as field[editDropdownValueLabel] CUSTOM_FILTERS for field in editDropdownOptionsArray\"></select></div>';
        var customCellTemplate = '<div><select style="height:40px;" ui-grid-edit-dropdown ng-model=\"MODEL_COL_FIELD\" ng-change="grid.appScope.onDropdownChange(row.entity)" ng-options=\"field[editDropdownIdLabel] as field[editDropdownValueLabel] CUSTOM_FILTERS for field in editDropdownOptionsArray\" style="height:30px;"></select></div>';

        // added by sss on 2017-10-10
        // var customCellMultiTemplate = '<div><select multiple ui-grid-edit-dropdown ng-model=\"MODEL_COL_FIELD\" ng-change="grid.appScope.onDropdownChange(row.entity)" ng-options=\"field[editDropdownIdLabel] as field[editDropdownValueLabel] CUSTOM_FILTERS for field in editDropdownOptionsArray\"></select></div>'
        var customCellMultiTemplate =
            '<div class="ui-select">' +
            '   <ui-select-wrap>' +
            '       <ui-select multiple ng-model=\"MODEL_COL_FIELD\" theme="select2" append-to-body="true" ng-change="grid.appScope.onMultiDropdownChange(row.entity)">' +
            '           <ui-select-match placeholder="Choose...">{{ $item.TaskName }}</ui-select-match>' +
            '           <ui-select-choices repeat="item in col.colDef.editDropdownOptionsArray | filter: $select.search">' +
            '               <span>{{ item.TaskName }}</span>' +
            '           </ui-select-choices>' +
            '       </ui-select>' +
            '   </ui-select-wrap>' +
            '</div>';

        var multiCell = '<div class="ui-grid-cell-contents">{{ COL_FIELD.join(\', \') }}</div>';

        $scope.gridWAC = {
            enableSorting: true,
            enableRowSelection: false,
            // added by sss on 2017-10-11
            // rowHeight: 30,
            //
            // multiSelect: true,
            // enableSelectAll: true,
            rowHeight: 40,
            enableCellEdit: true,
            enableCellEditOnFocus: true,
            // showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'JobWACExtId', field: 'JobWACExtId', width: '7%', visible: false },
                { name: 'Nama Item', field: 'ItemName', enableCellEdit: false },
                {
                    name: 'Status Kerusakan',
                    // width: '15%',
                    field: 'Status',
                    // editableCellTemplate: 'ui-grid/dropdownEditor',
                    editableCellTemplate: customCellTemplate,
                    editDropdownValueLabel: 'Name',
                    editDropdownIdLabel: 'Id',
                    editDropdownOptionsArray: $scope.DamageVehicle,
                    cellFilter: "griddropdownStat:this"
                },
            ]
        };

        $scope.gridVC = {
            enableSorting: true,
            enableRowSelection: false,
            // added by sss on 2017-10-11
            // rowHeight: 30,
            //
            // multiSelect: true,
            // enableSelectAll: true,
            enableCellEdit: true,
            // showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'JobWACExtId', field: 'JobWACExtId', width: '7%', visible: false },
                { name: 'Nama Item', field: 'ItemName', width: '30%', enableCellEdit: false },
                // {
                //     name: 'Status Kerusakan',
                //     width: '15%',
                //     field: 'Status',
                //     // editableCellTemplate: 'ui-grid/dropdownEditor',
                //     editableCellTemplate: customCellTemplate,
                //     editDropdownValueLabel: 'Name',
                //     editDropdownIdLabel: 'Id',
                //     editDropdownOptionsArray: [
                //         { 'Id': 1, 'Name': 'Penyok' }, { 'Id': 2, 'Name': 'Baret' }, { 'Id': 3, 'Name': 'Rusak' },
                //     ],
                //     cellFilter: "griddropdownStat:this"
                // },
                {
                    name: 'Pekerjaan',
                    field: 'Tasks',
                    // editableCellTemplate: 'ui-grid/dropdownEditor',
                    editableCellTemplate: customCellMultiTemplate,
                    editDropdownValueLabel: 'TaskName',
                    editDropdownIdLabel: 'TaskListBPId',
                    editDropdownOptionsArray: [],
                    cellFilter: "griddropdownTask:this",
                    // cellTemplate: multiCell,
                },
            ]
        };

        //================================================================= wac ==============================================================================//



        $scope.clearEditCustomer = function() {
            $scope.filterSearch.filterValue = "";
            $scope.showFieldInst = false;
            $scope.showFieldPersonal = false;
        };

        // added by sss on 2017-09-25
        $scope.fClaim = { show: false };
        // $scope.modalMode = 'new';
        $scope.mClaim = {};
        AppointmentBpService.getInsurence().then(function(res) {
            console.log("res getInsurence", res.data);
            $scope.Asuransi = res.data;
        });
        // WO.getVehicleTypeColor().then(function(resu) {
        //     console.log('getVehicleTypeColor', resu.data.Result);
        //     $scope.Color = resu.data.Result;
        // })
        $scope.claimShow = function() {
            console.log("claimShow..");
            console.log("modalMode : ", $scope.modalMode)
            if ($scope.modalMode == 'new') {
                $scope.mClaim = {};
                if (typeof $scope.mData.LicensePlate !== 'undefined' && $scope.mData.LicensePlate !== null) {
                    $scope.mClaim.LicensePlate = angular.copy($scope.mData.LicensePlate);
                }
                if (typeof $scope.mData.VehicleModelId !== 'undefined' && $scope.mData.VehicleModelId !== null) {
                    $scope.mClaim.VehicleModelId = angular.copy($scope.mData.VehicleModelId);
                }
                if (typeof $scope.mData.VehicleTypeId !== 'undefined' && $scope.mData.VehicleTypeId !== null) {
                    $scope.mClaim.VehicleTypeId = angular.copy($scope.mData.VehicleTypeId);
                    var temp = _.find($scope.Color, { VehicleTypeId: $scope.mData.VehicleTypeId });
                    if (typeof temp !== 'undefined') {
                        $scope.selectTypeClaim(temp);
                    }
                }
            }

            $scope.fClaim.show = true;
        };
        // $scope.selectModelClaim = function(item) {
        //     console.log('selectedModel >>>', item);
        //     $scope.VehicT = [];
        //     $scope.mDataCrm.Vehicle.Type = null;
        //     var dVehic = item.MVehicleType;
        //     _.map(dVehic, function(a) {
        //         a.NewDescription = a.Description + ' - ' + a.KatashikiCode + ' - ' + a.SuffixCode;
        //     });
        //     console.log('dVehic >>>', dVehic);
        //     $scope.VehicT = dVehic;
        //     // $scope.enableType = false;
        // };
        $scope.selectModelClaim = function(item) {
            // $scope.typeData
            $scope.typeData = [];
            $scope.mDataCrm.Vehicle.Type = null;
            // var dVehic = item.MVehicleType;
            var dVehic = _.uniq(item.MVehicleType, 'KatashikiCode');
            _.map(dVehic, function(a) {
                a.NewDescription = a.Description + ' - ' + a.KatashikiCode + ' - ' + a.SuffixCode;
            });
            console.log('dVehic >>>', dVehic);
            $scope.typeData = dVehic;
            // $scope.enableType = false;
        };
        $scope.enableColorClaim = true;
        $scope.selectTypeClaim = function(item) {
            console.log('selectedType >>>', item);
            $scope.colorData = [];
            var arrCol = [];
            _.map($scope.Color, function(val) {
                if (val.VehicleTypeId == item.VehicleTypeId) {
                    arrCol = val.ListColor;
                };
            });
            $scope.enableColorClaim = false;
            $scope.colorData = arrCol;
        };

        $scope.selectColorClaim = function(item) {
            console.log('selectedColor >>>', item);
            $scope.mClaim.ColorCode = item.ColorCode;
            console.log('$scope.mClaim >>>', $scope.mClaim);
        };
        // ===========================
        // Allow Pattern
        // ===========================
        $scope.allowPattern = function(event, type, item) {
            console.log("event", event);
            // if(event.charCode == 13){
            //     $scope.getData();
            // }
            var patternRegex
            if (type == 1) {
                patternRegex = /\d/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /\d|[a-z]|[*]/i; //ALPHANUMERIC ONLY
                if (item.includes("*")) {
                    event.preventDefault();
                    return false;
                }
            } else if (type == 3) {
                patternRegex = /\d|[a-z]/i;
            }
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };

        $scope.allowPatternFilter = function(event, type, item) {
            console.log("event", event);
            if (event.charCode == 13) {
                $scope.getData(true);
            }
            var patternRegex
            if (type == 1) {
                patternRegex = /\d/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /\d|[a-z]|[*]/i; //ALPHANUMERIC ONLY
                if (item.includes("*")) {
                    event.preventDefault();
                    return false;
                }
            } else if (type == 3) {
                patternRegex = /\d|[a-z]/i;
            }
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };

        $scope.givePattern = function(item, event) {
            console.log("item", item);
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
                $scope.nilaiKM = item;
                if ($scope.nilaiKM.includes(",")) {
                    $scope.nilaiKM = $scope.nilaiKM.split(',');
                    if ($scope.nilaiKM.length > 1) {
                        $scope.nilaiKM = $scope.nilaiKM.join('');
                    } else {
                        $scope.nilaiKM = $scope.nilaiKM[0];
                    }
                }
                $scope.nilaiKM = parseInt($scope.nilaiKM);
                $scope.mData.Km = $scope.mData.Km.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                return;
            }
        };

        $scope.getUnique = function (array){
            var uniqueArray = [];
            
            // Loop through array values
            for(i=0; i < array.length; i++){
                if(uniqueArray.indexOf(array[i]) === -1) {
                    uniqueArray.push(array[i]);
                }
            }
            return uniqueArray;
        }

        $scope.CekServiceHistory = function(category, vin, startDate, endDate) {
            VehicleHistory.getData(category, vin, startDate, endDate).then(
                function(res) {

                    for(var i in res.data){
                        var tmpArray = [];
                        for(var j = 0; j<res.data[i].VehicleJobService.length; j++){
                            if(res.data[i].VehicleJobService[j].KTPNo == null || res.data[i].VehicleJobService[j].KTPNo == undefined){
                                res.data[i].VehicleJobService[j].KTPNo = ''
                            }
                            if(!(res.data[i].VehicleJobService[j].KTPNo.includes('/'))){
                                if(res.data[i].VehicleJobService[j].EmployeeName !== null){
                                    tmpArray.push(res.data[i].VehicleJobService[j].EmployeeName);
                                }
                            }
                            
                        }
                        tmpArray = $scope.getUnique(tmpArray);
                        res.data[i].namaTeknisi = tmpArray.join(', ');
                    }

                    console.log('DATA HISTORY ====>', res.data);
                    $scope.mDataVehicleHistory = res.data;
                    $scope.isKmHistory = $scope.mDataVehicleHistory.length;

                    if ($scope.isKmHistory >= 1) {

                        $scope.KMTerakhir = $scope.mDataVehicleHistory[0].Km;

                        $scope.restart_digit_odo = 0
                        WO.GetResetCounterOdometer(vin).then(function(res) {
                            $scope.restart_digit_odo = res.data
                        })

                    }

                    console.log('Data History', $scope.isKmHistory);
                    console.log("kesini ga?");

                    console.log("mDataVehicleHistory=> CEK SERVICE TERAKHIR ", $scope.mDataVehicleHistory);
                },
                function(err) {}
            );
        }

        $scope.estHours = null;
        $scope.estDays = null;
        $scope.getCalculatedTimeEst = [];
        

        // Tambahan CR 4 [Start]

        $scope.DiskonPartsEndDate = null;
        $scope.DiskonTaskEndDate = null;
        $scope.asbDate = undefined;
        $scope.choosePembayaran = function(data,isCalculateSummary){
            console.log("data",data);
            if(data == 1){
                console.log('ini pembayaran cash ===>',data)
                MasterDiscountFactory.getDataAktif().then(
                    function (res) {
                        console.log("res dis", res.data.Result);
                        console.log('$scope.asbDate ===>',$scope.asbDate);
                        console.log('$scope.mData.AppointmentDate ===>',$scope.mData.AppointmentDate);
                        $scope.DiscountParts = 0;
                        $scope.mData.disParts = 0;
                        $scope.DiscountTask = 0;
                        $scope.mData.disTask = 0;
                        for(var i = 0; i < res.data.Result.length; i++){
                            if(res.data.Result[i].Category == 2){
                                $scope.DiskonPartsEndDate = res.data.Result[i].DateTo;
                                $scope.DiscountParts = res.data.Result[i].Discount;
                                $scope.mData.disParts = res.data.Result[i].Discount;

                            }else if(res.data.Result[i].Category == 1){
                                $scope.DiskonTaskEndDate = res.data.Result[i].DateTo;
                                $scope.DiscountTask = res.data.Result[i].Discount;
                                $scope.mData.disTask = res.data.Result[i].Discount;
                            }
                        }

                        

                        if(jejeranDiskonParts.length == 0 ){
                            $scope.DisplayDisParts = $scope.DiscountParts;
                            console.log('$scope.DisplayDisParts ===>',$scope.DisplayDisParts + ' | jejeranDiskonParts ===> '+jejeranDiskonParts);
                        }
                        if(jejeranDiskonTask.length == 0){
                            $scope.DisplayDisTask = $scope.DiscountTask;
                            console.log('$scope.DisplayDisTask ===>',$scope.DisplayDisTask + ' | jejeranDiskonTask ===> '+jejeranDiskonTask);
                        }

                        if($scope.mData.AppointmentDate != null){
                            $scope.asbDate = $scope.mData.AppointmentDate;
                        }

                        if($scope.asbDate == undefined){
                            console.log('kalo asbnya undefined | [] ===>',$scope.asbDate);
                            console.log('DiscountParts => ', $scope.DiscountParts  + ' | DiscountTask => ' +  $scope.DiscountTask );
                        }else{
                            if($scope.asbDate > $scope.DiskonTaskEndDate){
                                console.log('Tidak Masuk periode diskon booking Task');
                                $scope.DiscountTask = 0;
                                $scope.mData.disTask = 0;
                            }else{
                                console.log('masuk periode diskon booking Task');
                            }
                            if($scope.asbDate > $scope.DiskonPartsEndDate){
                                console.log('Tidak Masuk periode diskon booking Parts');
                                $scope.DiscountParts = 0;
                                $scope.mData.disParts = 0;
                            }else{
                                console.log('masuk periode diskon booking Parts');
                            }
                        }

                        console.log("$scope.mData",$scope.mData);


                        $scope.recalculateSummary(1,isCalculateSummary);

                    },
                    function (err) { }
                );


                
            }else{
                console.log('ini pembayaran insurance ===>',data)
                $scope.DiscountParts = 0;
                $scope.mData.disParts = 0;
                $scope.DiscountTask = 0;
                $scope.mData.disTask = 0;
                $scope.mData.InsuranceName = undefined;

                if(jejeranDiskonParts.length == 0 ){
                    $scope.DisplayDisParts = 0;
                }
                if(jejeranDiskonTask.length == 0){
                    $scope.DisplayDisTask = 0;
                }

            }
        }


        
        $scope.btnBatalViewEstimasi = function(){  
            $scope.showModalViewEstimasi = false;
            $scope.showModalEstimasiYangBelumDigunakan = true;
        }

        $scope.cekEstimasi = function () {
            $scope.disableBtnPilihEstimasi = true;
            $scope.showModalViewEstimasi = false;
            $scope.showModalEstimasiYangBelumDigunakan = true;

            $scope.getDataEstimasi();

            var numLength = angular.element('.ui.modal.ModalEstimasiYangBelumDigunakan').length;
            setTimeout(function () {
                angular.element('.ui.modal.ModalEstimasiYangBelumDigunakan').modal('refresh');
            }, 0);

            if (numLength > 1) {
                angular.element('.ui.modal.ModalEstimasiYangBelumDigunakan').not(':first').remove();
            }

            angular.element(".ui.modal.ModalEstimasiYangBelumDigunakan").modal("show");
            //angular.element('.ui.modal.PengurusanJasaDokumenSO').modal('show');
        }


      


        $scope.btnBatalCekEstimasi = function () {

            angular.element('.ui.modal.ModalEstimasiYangBelumDigunakan').modal('hide');
        }

        $scope.getDataEstimasi = function () {
            // var url = "&NoPolisi=" + $scope.mDataDetail.LicensePlate + "&DateFrom=2004-01-01&DateTo=9999-09-09";   




            // var url = "&NoPolisi=" + $scope.mDataDetail.LicensePlate;       
            var url = $scope.mDataDetail.LicensePlate;       
            AppointmentBpService.getDataCekEstimasiNew(url).then(function (res) {
                console.log('ini data get estimasi ===>',res.data.Result);

                var filteredEstimasi = [];
                for(var i in res.data.Result){
                    if(res.data.Result[i].isAppointment == 1){
                        res.data.Result[i].isUsedInAppointment = true;
                        console.log(' ini data gabisa dipake ===>',res.data.Result[i].EstimationNo);
                        filteredEstimasi.push(res.data.Result[i]);
                    }else{
                        console.log(' ini data bisa dipake =====>',res.data.Result[i].EstimationNo);
                        filteredEstimasi.push(res.data.Result[i]);
                    }
                }

                if(tampungEstimasi.length > 0){
                    for(var i in tampungEstimasi){
                        for(var j in filteredEstimasi){
                            if(tampungEstimasi[i].EstimationNo == filteredEstimasi[j].EstimationNo){
                                filteredEstimasi[j].isUsedInAppointment = true;
                            }
                        }
                    }
                }
            

                $scope.listEstimasiYangBelumDigunakan.data = filteredEstimasi;
            });             
            
        }

        $scope.fixDate = function (date) {
            if (date != null || date != undefined) {
                var fix = 
                    ('0' + date.getDate()).slice(-2) + "/"  + 
                    ('0' + (date.getMonth() + 1)).slice(-2) + "/" +
                    date.getFullYear() ;
                return fix;
            } else {
                return null;
            }
        };

        $scope.tanggaljamViewEstimasi = function (mentah){
            mentah = new Date(mentah)
            var date = ('0' + mentah.getDate()).slice(-2)
            var month =('0' + (mentah.getMonth() + 1)).slice(-2) 
            var year = mentah.getFullYear();
            var hour = ('0' + mentah.getHours()).slice(-2)
            var minute =('0' + mentah.getMinutes()).slice(-2)
            var second = ('0' + mentah.getSeconds()).slice(-2)
            var final = date + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second;
            return final;
        }


        $scope.listButtonSettingsAddress = { 
            view  : { enable: false}, 
            new   : { enable: false}, 
            edit  : { enable: false}, 
            delete: { enable: false}, 
            selectall: { enable: false} 
        };



        
        $scope.jejeranDPViewEstimasi = [];
        $scope.dataViewEstimasi = undefined;
        $scope.lihatEstimasi = function(data){  
            console.log('ini lihat estimasi ===>',data);


            if(data.EstimationBPId !== 0){
                data.EstimationBPId = data.EstimationBPId;
                console.log('ini Estimasi belum WO ===> EstimationBPId => ' ,data.EstimationBPId)
            }else{
                data.EstimationBPId = data.JobId;
                console.log('ini Estimasi sudah WO ===> JobId => ',data.JobId)
            }

            AppointmentBpService.getDetailEstimasi(data.EstimationBPId, $scope.mDataDetail.LicensePlate).then(function (res) {
                $scope.dataViewEstimasi =  angular.copy(res.data.Result[0]);
                console.log('getDetail selected estimasi ===>',$scope.dataViewEstimasi);
                $scope.showModalViewEstimasi = true;
                $scope.showModalEstimasiYangBelumDigunakan = false;

                $scope.jejeranDPViewEstimasi = [];
                for (var i = 0; i < $scope.dataViewEstimasi.AEstimationBP_TaskList_TT.length; i++) {
                    $scope.jejeranDPViewEstimasi.push({EstTaskBPId:$scope.dataViewEstimasi.AEstimationBP_TaskList_TT[i].EstTaskBPId, partsList:[]});
                }
                var dpViewEstimasi = $scope.arrayDPViewEstimasi($scope.dataViewEstimasi, $scope.jejeranDPViewEstimasi);
                console.log('jancokkkkk dpViewEstimasi ===>',dpViewEstimasi)
                console.log('jancokkkkk jejeranDPViewEstimasi ===>',$scope.jejeranDPViewEstimasi)




                for(var x in  $scope.VehicleGasType){
                    if($scope.mDataDetail.VehicleGasTypeId == $scope.VehicleGasType[x].VehicleGasTypeId){
                        $scope.mDataDetail.VehicleGasTypeName = $scope.VehicleGasType[x].VehicleGasTypeName;
                    }

                }


                // cari diskon buat summary view estimasi
                if($scope.dataViewEstimasi.isCash == 1){ //kalo bayarnya cash
                    // $scope.DiscountTaskViewEstimasi = 0;
                    // $scope.DiscountPartsViewEstimasi = 0;
                }else{// kalo bayarnya asuransi
                    for(var i in $scope.Asuransi){
                        if($scope.Asuransi[i].InsuranceId == $scope.dataViewEstimasi.InsuranceId){
                            // $scope.DiscountTaskViewEstimasi = $scope.Asuransi[i].DiscountJasa;
                            // $scope.DiscountPartsViewEstimasi = $scope.Asuransi[i].DiscountPart;
                            $scope.dataViewEstimasi.InsuranceName = $scope.Asuransi[i].InsuranceName;
                        }
                    }
                }



                $scope.dataViewEstimasi.EstimationDate = $scope.tanggaljamViewEstimasi($scope.dataViewEstimasi.EstimationDate);
                $scope.dataViewEstimasi.PolisDateFrom = $scope.fixDate($scope.dataViewEstimasi.PolisDateFrom);
                $scope.dataViewEstimasi.PolisDateTo = $scope.fixDate($scope.dataViewEstimasi.PolisDateTo);

                

                //untuk dapetin Model, Tipe & Katashiki dari estimasi
                var tampunganVehicleTypeId = $scope.dataViewEstimasi.VehicleTypeId;
                for (var i = 0; i < $scope.modelData.length; i++) {
                    if ($scope.modelData[i].VehicleModelId == parseInt($scope.dataViewEstimasi.ModelCode)){
                        $scope.dataViewEstimasi.VehicleModelName = $scope.modelData[i].VehicleModelName
                        WO.getVehicleTypeById(parseInt($scope.dataViewEstimasi.ModelCode)).then(function(res) {
                            $scope.typeDataViewEstimasi = res.data.Result;
                            for (var z in $scope.typeDataViewEstimasi) {
                                if($scope.typeDataViewEstimasi[z].VehicleTypeId == tampunganVehicleTypeId){
                                    $scope.dataViewEstimasi.VehicleTypeDesc = $scope.typeDataViewEstimasi[z].Description;
                                }
                            }
                        })
                    }
                }
                


                //penyesuaian dari estimasi [Tasklist & Partlist]
                $scope.dataViewEstimasi.JobTask = $scope.dataViewEstimasi.AEstimationBP_TaskList_TT;
                var TempJobParts = [];


                for (var i = 0; i < $scope.dataViewEstimasi.JobTask.length; i++) {
                    for (var j = 0; j < $scope.dataViewEstimasi.JobTask[i].AEstimationBP_PartList.length; j++) {
                        $scope.dataViewEstimasi.JobTask[i].AEstimationBP_PartList[j].Qty = $scope.dataViewEstimasi.JobTask[i].AEstimationBP_PartList[j].qty;
                        TempJobParts.push($scope.dataViewEstimasi.JobTask[i].AEstimationBP_PartList[j]);
                    }
                    $scope.dataViewEstimasi.JobTask[i].JobParts = [];
                    for (var k = 0; k < TempJobParts.length; k++) {
                        if($scope.dataViewEstimasi.JobTask[i].EstTaskBPId == TempJobParts[k].EstTaskBPId){
                            $scope.dataViewEstimasi.JobTask[i].JobParts.push(TempJobParts[k]);
                        }
                    }
                }


                for(var m in $scope.dataViewEstimasi.JobTask){
                    for(var n in $scope.dataViewEstimasi.JobTask[m].JobParts){      
                        $scope.dataViewEstimasi.JobTask[m].JobParts[n].Satuan = null;
                        for(var o in $scope.unitData){
                            if($scope.unitData[o].MasterId == $scope.dataViewEstimasi.JobTask[m].JobParts[n].SatuanId){
                                $scope.dataViewEstimasi.JobTask[m].JobParts[n].Name = $scope.unitData[o].Name;
                            }
                        }
                    }
                }


                for(var m in $scope.dataViewEstimasi.JobTask){
                    $scope.dataViewEstimasi.JobTask[m].PaidBy = null;
                    delete $scope.dataViewEstimasi.JobTask[m].JobId;

                    
                    for(var x in $scope.paymentData ){
                        if($scope.paymentData[x].MasterId == $scope.dataViewEstimasi.JobTask[m].PaidById){
                            console.log('dapet nih task ===>',$scope.dataViewEstimasi.JobTask[m].PaidById);
                            $scope.dataViewEstimasi.JobTask[m].PaidBy = $scope.paymentData[x].Name;
                        }else{
                            console.log('tak dapet nih task ===>',$scope.dataViewEstimasi.JobTask[m].PaidById);
                        }
                    }
                    for(var n in $scope.dataViewEstimasi.JobTask[m].JobParts){
                        $scope.dataViewEstimasi.JobTask[m].JobParts[n].PaidBy = null;
                        // delete $scope.dataViewEstimasi.JobTask[m].JobParts[n].IsCustomDiscount;
                        delete $scope.dataViewEstimasi.JobTask[m].JobParts[n].JobId ;


                        if($scope.dataViewEstimasi.JobTask[m].JobParts[n].ETA == null){
                            $scope.dataViewEstimasi.JobTask[m].JobParts[n].isAvailable = 1;
                        }else{
                            $scope.dataViewEstimasi.JobTask[m].JobParts[n].isAvailable = 0;
                        }


                        
                        for(var z in $scope.paymentData ){
                            if($scope.paymentData[z].MasterId == $scope.dataViewEstimasi.JobTask[m].JobParts[n].PaidById){
                                console.log('dapett nih parts ===>  $scope.dataViewEstimasi.JobTask[m].JobParts[n].PaidById')
                                $scope.dataViewEstimasi.JobTask[m].JobParts[n].PaidBy = $scope.paymentData[z].Name;
                            }
                        }
                    }
                }


                // var tempDataPartViewEstimasi = {};
                // // Pengecekan DP, DP alay dari estimasi BP minggir lo
                // for(var m in $scope.dataViewEstimasi.JobTask){
                //     if($scope.dataViewEstimasi.JobTask[m].JobParts.length > 0){
                //         for(var n in $scope.dataViewEstimasi.JobTask[m].JobParts){


                //             var findtask = _.find(dpViewEstimasi, {'EstTaskBPId':$scope.dataViewEstimasi.JobTask[m].EstTaskBPId});
                //             var parts  = _.find(findtask.partsList, {'EstPartsId':$scope.dataViewEstimasi.JobTask[m].JobParts[n].EstPartsId});

                //             console.log('data findtask ====>',findtask)
                //             console.log('data parts ====>',parts)

                //             for(var i in dpViewEstimasi){
                //                 // if(dpViewEstimasi[i].partsList.length>0){
                //                     for(var j in dpViewEstimasi[i].partsList){
                //                         if(dpViewEstimasi[i].partsList[j].EstPartsId == $scope.dataViewEstimasi.JobTask[m].JobParts[n].EstPartsId){
                //                             console.log(' data ketemu | dpViewEstimasi[i].partsList[j]', dpViewEstimasi[i].partsList[j]);
                //                             $scope.dataViewEstimasi.JobTask[m].JobParts[n].DownPayment = dpViewEstimasi[i].partsList[j].DownPaymentRecalculate;
                //                         }else{
                //                             console.log('data tidak ketemu');
                //                         }
                //                     }
                //                 // }
                //             }

                //             console.log('Result pengecekann ',$scope.dataViewEstimasi.JobTask[m].JobParts[n])

                //         }
                //     }
                // }
                // // Pengecekan DP, DP alay dari estimasi BP minggir lo



                //penyesuaian dari estimasi [Tasklist & Partlist]
                $scope.dataForBslistViewEstimasi($scope.dataViewEstimasi);




            });

            
        }


        $scope.cekPartsViewEstimasi = function(tempParts, tempTask, idxTask, idxParts){
            var result = {};
            AppointmentBpService.getAvailableParts(tempParts.PartsId, 0, tempParts.qty).then(function(res) { 
                dataHasilPengecekanParts = res.data.Result[0];
                console.log('dataHasilPengecekanParts ====>',dataHasilPengecekanParts);
                if(tempParts.PartsId == dataHasilPengecekanParts.PartsId){
                    var indexTask = _.findIndex($scope.jejeranDPViewEstimasi, {'EstTaskBPId':tempTask.EstTaskBPId});
                    if(indexTask != -1){

                        console.log('DP ====> tempParts.PaidById =====>',tempParts.PaidById)
                        console.log('DP ====> tempParts.qty ==========>',tempParts.qty)
                        console.log('DP ====> tempParts.RetailPrice ==>',tempParts.RetailPrice)
                        console.log('DP ====> tempParts.Discount =====>',tempParts.Discount)
                        console.log('DP ====> cekParts.PercentDP =====>',dataHasilPengecekanParts.PercentDP)
                        console.log('DP ====> cekParts.RetailPrice ===>',dataHasilPengecekanParts.RetailPrice)
                        console.log('DP ====> cekParts.QtyFree =======>',dataHasilPengecekanParts.QtyFree)

                        if(tempParts.PaidById == 28){
                            console.log('DP ====> pembayarannya Customer | ada kemungkinan dapet DP')
                            if(dataHasilPengecekanParts.isAvailable == 0){ // tidak tersedia
                                // var subTotalMaterial = (dataHasilPengecekanParts.RetailPrice/1.1) * tempParts.Qty;
                                var subTotalMaterial = (dataHasilPengecekanParts.RetailPrice/(1+(PPNPerc/100))) * tempParts.Qty;

                                // var subTotalDiscountMaterial =  ((dataHasilPengecekanParts.RetailPrice /1.1 ) * (tempParts.Discount/100)) *  tempParts.Qty;
                                // var totalMaterialDiscounted = subTotalMaterial - subTotalDiscountMaterial;
                                var totalMaterialDiscounted = subTotalMaterial;
    
                                $scope.jejeranDPViewEstimasi[indexTask].partsList.push({
                                    EstTaskBPId  : tempTask.EstTaskBPId,
                                    Qty          : tempParts.qty,
                                    PartsId      : tempParts.PartsId,
                                    EstPartsId   : tempParts.EstPartsId,
                                    PaidById     : tempParts.PaidById,
                                    PercentDP    : dataHasilPengecekanParts.PercentDP,
                                    RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                    isAvailable  : dataHasilPengecekanParts.isAvailable,
                                    PriceDP      : dataHasilPengecekanParts.PriceDP,
                                    QtyFree      : dataHasilPengecekanParts.QtyFree,
                                    Available    : "Tidak Tersedia",
                                    // DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                    DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                })
    
                                // $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].jancok = "kondisi satu";
                                // console.log('DP ====> dapet DP karna Parts Tidak Tersedia ===>',(totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1);



                            }else if(dataHasilPengecekanParts.isAvailable == 1){ //tersedia 
                                $scope.jejeranDPViewEstimasi[indexTask].partsList.push({
                                    EstTaskBPId  : tempTask.EstTaskBPId,
                                    Qty          : tempParts.qty,
                                    PartsId      : tempParts.PartsId,
                                    EstPartsId   : tempParts.EstPartsId,
                                    PaidById     : tempParts.PaidById,
                                    PercentDP    : dataHasilPengecekanParts.PercentDP,
                                    RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                    isAvailable  : dataHasilPengecekanParts.isAvailable,
                                    PriceDP      : dataHasilPengecekanParts.PriceDP,
                                    QtyFree      : dataHasilPengecekanParts.QtyFree,
                                    DownPaymentRecalculate : 0,
                                    Available    : "Tersedia",
                                })
    
                                $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].DownPayment = 0;
                                $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].jancok = "kondisi dua";
                                console.log('DP ====> Tidak dapet DP karna Parts Tersedia ===>',0);

    
                            }else if(dataHasilPengecekanParts.isAvailable == 2){// tersedia sebagian

                                var qtyTidakTersedia = tempParts.Qty -  dataHasilPengecekanParts.QtyFree;
                                // var subTotalMaterial = (dataHasilPengecekanParts.RetailPrice/1.1) * qtyTidakTersedia;
                                var subTotalMaterial = (dataHasilPengecekanParts.RetailPrice/(1+(PPNPerc/100))) * qtyTidakTersedia;

                                // var subTotalDiscountMaterial =  ((dataHasilPengecekanParts.RetailPrice /1.1 ) * (tempParts.Discount/100)) *  qtyTidakTersedia;
                                // var totalMaterialDiscounted = subTotalMaterial - subTotalDiscountMaterial ;
                                var totalMaterialDiscounted = subTotalMaterial;


                                $scope.jejeranDPViewEstimasi[indexTask].partsList.push({
                                    EstTaskBPId  : tempTask.EstTaskBPId,
                                    Qty          : tempParts.qty,
                                    PartsId      : tempParts.PartsId,
                                    EstPartsId   : tempParts.EstPartsId,
                                    PaidById     : tempParts.PaidById,
                                    PercentDP    : dataHasilPengecekanParts.PercentDP,
                                    RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                    isAvailable  : dataHasilPengecekanParts.isAvailable,
                                    PriceDP      : dataHasilPengecekanParts.PriceDP,
                                    QtyFree      : dataHasilPengecekanParts.QtyFree,
                                    Available    : "Tersedia Sebagian",
                                    // DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                    DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                })
    
                                // $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1;
                                $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100));

                                $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].jancok = "kondisi tiga";
                                // console.log('DP ====> Seharusnya dapet DP Sebagian karna Parts Tersedia Sebagian ===>',(totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1);

                            }

                        }else{
                            console.log('DP ====> pembayarannya bukan Customer | tidak ada kemungkinan dapet DP');
                            $scope.jejeranDPViewEstimasi[indexTask].partsList.push({
                                EstTaskBPId  : tempTask.EstTaskBPId,
                                Qty          : tempParts.qty,
                                PartsId      : tempParts.PartsId,
                                EstPartsId   : tempParts.EstPartsId,
                                PaidById     : tempParts.PaidById,
                                PercentDP    : dataHasilPengecekanParts.PercentDP,
                                RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                isAvailable  : dataHasilPengecekanParts.isAvailable,
                                PriceDP      : dataHasilPengecekanParts.PriceDP,
                                QtyFree      : dataHasilPengecekanParts.QtyFree,
                                Available    : "ga tau ga ngecek, cuma cari DP bukan ketersediaan barang ",
                                DownPaymentRecalculate : 0
                            })

                            $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].DownPayment = 0;
                            $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].jancok = "yg dapet diskon cuma Paidby 28 dan isAvailable 0 atau 2";
                        }

                       

                        result = $scope.jejeranDPViewEstimasi[indexTask];
                    }
                }
            });

           
            console.log('index task ==>',idxTask + " | index Parts ===> ",idxParts);
            return result;
        }

        $scope.cekParts = function(tempParts, tempTask, idxTaskEstimasi, idxPartsEstimasi){
            var result = {};
            console.log('gridWork ===>',$scope.gridWork);
            console.log('gridTemp ===>',gridTemp);


            console.log('cekParts | mDataByEstimasi ===>',$scope.mDataByEstimasi)
            AppointmentBpService.getAvailableParts(tempParts.PartsId, 0, tempParts.qty).then(function(res) { 
                dataHasilPengecekanParts = res.data.Result[0];
                console.log('dataHasilPengecekanParts ====>',dataHasilPengecekanParts);
                if(tempParts.PartsId == dataHasilPengecekanParts.PartsId){
                    var indexTask = _.findIndex($scope.jejeranDP, {'TaskBPId':tempTask.TaskBPId});
                    if(indexTask != -1){

                        console.log('DP ====> tempParts.PaidById =====>',tempParts.PaidById)
                        console.log('DP ====> tempParts.qty ==========>',tempParts.qty)
                        console.log('DP ====> tempParts.RetailPrice ==>',tempParts.RetailPrice)
                        console.log('DP ====> tempParts.Discount =====>',tempParts.Discount)
                        console.log('DP ====> cekParts.PercentDP =====>',dataHasilPengecekanParts.PercentDP)
                        console.log('DP ====> cekParts.RetailPrice ===>',dataHasilPengecekanParts.RetailPrice)
                        console.log('DP ====> cekParts.QtyFree =======>',dataHasilPengecekanParts.QtyFree)

                        if(tempParts.PaidById == 28){

                            if(dataHasilPengecekanParts.isAvailable == 0){ // tidak tersedia
                                // var subTotalMaterial = (dataHasilPengecekanParts.RetailPrice/1.1) * tempParts.Qty;
                                var subTotalMaterial = (dataHasilPengecekanParts.RetailPrice/(1+(PPNPerc/100))) * tempParts.Qty;

                                // var subTotalDiscountMaterial =  ((dataHasilPengecekanParts.RetailPrice /1.1 ) * (tempParts.Discount/100)) *  tempParts.Qty;
                                // var totalMaterialDiscounted = subTotalMaterial - subTotalDiscountMaterial;
                                var totalMaterialDiscounted = subTotalMaterial;

                                // var totalMaterialDiscounted = (dataHasilPengecekanParts.RetailPrice * tempParts.Qty) - (dataHasilPengecekanParts.RetailPrice * tempParts.Qty)*((tempParts.Discount)/100);

                                $scope.jejeranDP[indexTask].partsList.push({
                                    TaskBPId     : tempTask.TaskBPId,
                                    Qty          : tempParts.qty,
                                    PartsId      : tempParts.PartsId,
                                    EstPartsId   : tempParts.EstPartsId,
                                    PaidById     : tempParts.PaidById,
                                    PercentDP    : dataHasilPengecekanParts.PercentDP,
                                    RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                    isAvailable  : dataHasilPengecekanParts.isAvailable,
                                    PriceDP      : dataHasilPengecekanParts.PriceDP,
                                    QtyFree      : dataHasilPengecekanParts.QtyFree,
                                    Available    : "Tidak Tersedia",
                                    // DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                    DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                })
    
                                // $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "kondisi satu";
                                
                                // gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "kondisi satu";
                                // console.log('DP ====> dapet DP karna Parts Tidak Tersedia ===>',(totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1);
                                // console.log('DP ====> total Parts | tidak Tersedia ===>',totalMaterialDiscounted, (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1);
                                
                                
                            }else if(dataHasilPengecekanParts.isAvailable == 1){ //tersedia
                                $scope.jejeranDP[indexTask].partsList.push({
                                    TaskBPId     : tempTask.TaskBPId,
                                    Qty          : tempParts.qty,
                                    PartsId      : tempParts.PartsId,
                                    EstPartsId   : tempParts.EstPartsId,
                                    PaidById     : tempParts.PaidById,
                                    PercentDP    : dataHasilPengecekanParts.PercentDP,
                                    RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                    isAvailable  : dataHasilPengecekanParts.isAvailable,
                                    PriceDP      : dataHasilPengecekanParts.PriceDP,
                                    QtyFree      : dataHasilPengecekanParts.QtyFree,
                                    DownPaymentRecalculate : 0,
                                    Available    : "Tersedia",
                                })
    
                                
                                $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = 0;
                                $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "kondisi dua";
    
                                gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = 0;
                                gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "kondisi dua";
                                console.log('DP ====> Tidak dapet DP karna Parts Tersedia ===>',0);

    
                            }else if(dataHasilPengecekanParts.isAvailable == 2){// tersedia sebagian

                                var qtyTidakTersedia = tempParts.Qty -  dataHasilPengecekanParts.QtyFree;
                                // var subTotalMaterial = (dataHasilPengecekanParts.RetailPrice/1.1) * qtyTidakTersedia;
                                var subTotalMaterial = (dataHasilPengecekanParts.RetailPrice/(1+(PPNPerc/100))) * qtyTidakTersedia;

                                // var subTotalDiscountMaterial =  ((dataHasilPengecekanParts.RetailPrice /1.1 ) * (tempParts.Discount/100)) *  qtyTidakTersedia;
                                // var totalMaterialDiscounted = subTotalMaterial - subTotalDiscountMaterial ;
                                var totalMaterialDiscounted = subTotalMaterial;
                                // var totalMaterialDiscounted = (dataHasilPengecekanParts.RetailPrice * qtyTidakTersedia) - (dataHasilPengecekanParts.RetailPrice * qtyTidakTersedia)*((tempParts.Discount)/100);


                                $scope.jejeranDP[indexTask].partsList.push({
                                    TaskBPId     : tempTask.TaskBPId,
                                    Qty          : tempParts.qty,
                                    PartsId      : tempParts.PartsId,
                                    EstPartsId   : tempParts.EstPartsId,
                                    PaidById     : tempParts.PaidById,
                                    PercentDP    : dataHasilPengecekanParts.PercentDP,
                                    RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                    isAvailable  : dataHasilPengecekanParts.isAvailable,
                                    PriceDP      : dataHasilPengecekanParts.PriceDP,
                                    QtyFree      : dataHasilPengecekanParts.QtyFree,
                                    Available    : "Tersedia Sebagian",
                                    // DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                    DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                })
    
                                
                                // $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment =  (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment =  (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "kondisi tiga";
    
                                // gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "kondisi tiga";
                                // console.log('DP ====> Seharusnya dapet DP Sebagian karna Parts Tersedia Sebagian ===>',(totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1);
                                // console.log('DP ====> total Parts | Tersedia Sebagian ===>',totalMaterialDiscounted, (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1);

                            }

                        }else{

                            console.log('DP ====> pembayarannya bukan Customer | tidak ada kemungkinan dapet DP');
                            $scope.jejeranDP[indexTask].partsList.push({
                                TaskBPId     : tempTask.TaskBPId,
                                Qty          : tempParts.qty,
                                PartsId      : tempParts.PartsId,
                                EstPartsId   : tempParts.EstPartsId,
                                PaidById     : tempParts.PaidById,
                                PercentDP    : dataHasilPengecekanParts.PercentDP,
                                RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                isAvailable  : dataHasilPengecekanParts.isAvailable,
                                PriceDP      : dataHasilPengecekanParts.PriceDP,
                                QtyFree      : dataHasilPengecekanParts.QtyFree,
                                Available    : "ga tau ga ngecek, cuma cari DP bukan ketersediaan barang ",
                                DownPaymentRecalculate : 0
                            })

                            
                            $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment =  0;
                            $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "yg dapet diskon cuma Paidby 28 dan isAvailable 0 atau 2";

                            gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = 0;
                            gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "yg dapet diskon cuma Paidby 28 dan isAvailable 0 atau 2";


                        }
                        

                        $scope.sumAllPrice();

                        result = $scope.jejeranDP[indexTask];
                    }
                }
            });

           
            console.log('index task ==>',idxTaskEstimasi + " | index Parts ===> ",idxPartsEstimasi);
            return result;
        }


        $scope.arrayDPViewEstimasi = function(dataEstimasi, simpleTaskParts){
            console.log(' arrayDPViewEstimasi | dataEstimasi ======>',dataEstimasi);
            console.log(' arrayDPViewEstimasi | simpleTaskParts ===>',simpleTaskParts);
            for (var i = 0; i < dataEstimasi.AEstimationBP_TaskList_TT.length; i++) {
                var tempTask = dataEstimasi.AEstimationBP_TaskList_TT[i];
                if(dataEstimasi.AEstimationBP_TaskList_TT[i].AEstimationBP_PartList.length > 0){
                    for (var j = 0; j < dataEstimasi.AEstimationBP_TaskList_TT[i].AEstimationBP_PartList.length; j++) {
                        var tempParts = dataEstimasi.AEstimationBP_TaskList_TT[i].AEstimationBP_PartList[j]
                        $scope.cekPartsViewEstimasi(tempParts, tempTask, i,j);
                    }
                }
            }
            console.log(' $scope.jejeranDPViewEstimasi | arrayDPViewEstimasi ====> ', $scope.jejeranDPViewEstimasi)
            $scope.jejeranDPViewEstimasi = simpleTaskParts;
            return $scope.jejeranDPViewEstimasi
        }

        $scope.arrayDP = function(gridWork, dummyPengecekanParts,flag){
            console.log(' arrayDP | gridWork ======>',gridWork);
            console.log(' arrayDP | dummyPengecekanParts ===>',dummyPengecekanParts);
            console.log(' arrayDP | flag ===>',flag);
            if(flag == 1){
                console.log(' arrayDP | do something ===>',flag)

                for (var x = 0; x < gridWork.length; x++) {
                    var tempTaskEstimasi = gridWork[x];
                    if(gridWork[x].child.length > 0){
                        for (var y = 0; y < gridWork[x].child.length; y++) {s
                            var tempPartsEstimasi = gridWork[x].child[y]
                            $scope.cekParts(tempPartsEstimasi, tempTaskEstimasi, x, y);
                        }
                    }
                }
    
                console.log(' $scope.jejeranDP | arrayDP ====> ', $scope.jejeranDP)
                $scope.jejeranDP = dummyPengecekanParts;
                return $scope.jejeranDP
            }else{
                console.log(' arrayDP | do nothing ===>',flag)
            }
        }


        $scope.onSelectRows = function(rows){
            console.log("onSelectRows=>",rows);
            $scope.selectedCekEstimasi  = rows;

            if($scope.selectedCekEstimasi.length > 0){ //kalo ada isinya
              $scope.disableBtnPilihEstimasi = false;
            }else{ //kalo kosong
              $scope.disableBtnPilihEstimasi = true; 
            }
        }



        var gridActionLihatEstimasi = '<div class="ui-grid-cell-contents">' +
            // '<a href="#" uib-tooltip="Buat" tooltip-placement="bottom" ng-click="grid.appScope.actView(row.entity)" style="">Appointment</a>'+
            '<a href="#" ng-click="grid.appScope.lihatEstimasi(row.entity)"  uib-tooltip="Lihat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-id-card-o" style="padding:4px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>'


        $scope.listEstimasiYangBelumDigunakan = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            isRowSelectable: function(row) {
                if(row.entity.isUsedInAppointment === true) return false; // ini ga bisa di select
                return true; //yang lainnya bisa
            },
            
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15, 
            columnDefs: [
                { displayName: 'No. Estimasi', name: 'No. Estimasi', field: 'EstimationNo', visible: true, cellTemplate: '<p style="padding-top:7px">&nbsp; {{row.entity.EstimationNo}}<bsreqlabel ng-if="(row.entity.isUsedInAppointment == true && row.entity.UsedBy == 1) || (row.entity.isUsedInAppointment == true && row.entity.UsedBy == null)"></bsreqlabel><span style="color:red" ng-if="row.entity.UsedBy == 2">**</span></p>'},
                { displayName: 'Tanggal Estimasi', name: 'EstimationDate', field: 'EstimationDate', cellFilter: 'date:\"dd-MM-yyyy\"' },        
                { displayName: 'Pembayaran', name: 'Pembayaran', field: 'isCash', visible: true, cellTemplate:'<p ng-if="row.entity.isCash==1" style="padding-top:7px" >&nbsp; Cash</p><p ng-if="row.entity.isCash==0" style="padding-top:7px" >&nbsp; Insurance</p>'},
                { displayName: 'Pembuat', name: 'Pembuat', field: 'LastModifiedEmployeeName', visible: true, },
                { displayName: 'Status', name: 'Status', field: 'Status', width: 80,visible: true, cellTemplate:'<p ng-if="row.entity.Status==1" style="padding-top:7px" >&nbsp; Close</p><p ng-if="row.entity.Status==0" style="padding-top:7px" >&nbsp; Open</p><p ng-if="row.entity.Status==2" style="padding-top:7px" >&nbsp; Draft</p>'},
                { 
                    name: 'Action',
                    width: 120,
                    pinnedRight: true,
                    cellTemplate: gridActionLihatEstimasi 
                }, //jancok

            ],

            onRegisterApi: function(gridApi) {
                $scope.listEstimasiYangBelumDigunakan = gridApi;

                $scope.listEstimasiYangBelumDigunakan.selection.on.rowSelectionChanged($scope,function(row) {
                    $scope.selectedRows = $scope.listEstimasiYangBelumDigunakan.selection.getSelectedRows();
                    // console.log("selected=>",$scope.selectedRows);
                    if($scope.onSelectRows){
                        $scope.onSelectRows($scope.selectedRows);
                    }
                });
                $scope.listEstimasiYangBelumDigunakan.selection.on.rowSelectionChangedBatch($scope,function(row) {
                    $scope.selectedRows = $scope.listEstimasiYangBelumDigunakan.selection.getSelectedRows();
                    if($scope.onSelectRows){
                        $scope.onSelectRows($scope.selectedRows);
                    }
                });

            }
        };


        $scope.onSelectCategory =  function (selected){
            console.log('onSelectCategory ===>',selected);
        }


        var tampungparts = [];
        var tampungtask = [];
        var taskAntiDuplicate = [];
        var partsAntiDuplicate = [];

        $scope.removeDuplicateTask = function(data){
            var dups = [];
            var arr = data.filter(function(el) {
                for(var x in data){
                    if (dups.indexOf(el.TaskBPId) == -1 && data[x].EstimationBPId == el.EstimationBPId) {
                      dups.push(el.TaskBPId);              
                      return true;
                    }else{
                      return false;
                    }
                }
              });
            return arr
        }

        $scope.removeDuplicateParts = function(data){
            console.log(data)
            var dups = [];
            var arr = data.filter(function(el) {
                console.log('heyhoo',el.EstPartsId)
                for(var x in data){
                    if (dups.indexOf(el.PartsId) == -1 && data[x].EstimationBPId == el.EstimationBPId) {
                      dups.push(el.EstimationBPId);              
                      return true;
                    }else{
                      return false;
                  
                    }
                }
              });
            return arr
        }


        var jejeranDiskonTask = [];
        var jejeranDiskonParts = [];
        $scope.displayDiscountAlgorithm = function(){
            console.log('jejeranDiskonTask ==>',jejeranDiskonTask);
            console.log('jejeranDiskonParts ==>',jejeranDiskonParts);
            if(jejeranDiskonTask.length > 0){
                var checkDisTskDouble = _.uniqBy(jejeranDiskonTask, function (e) { return e;});
                console.log("jejeranDiskonTask Unik ===>",checkDisTskDouble);
                if(checkDisTskDouble.length > 0){
                    if(checkDisTskDouble.length == 1) $scope.DisplayDiscountTask = jejeranDiskonTask[0];
                    else $scope.DisplayDiscountTask = '';
                }else $scope.DisplayDiscountTask = '';
            }else $scope.DisplayDiscountTask = 0;
        
            if(jejeranDiskonParts.length > 0){
                var checkDisPrtsDouble = _.uniqBy(jejeranDiskonParts, function (e) { return e;});
                console.log("jejeranDiskonParts Unik ===>",checkDisPrtsDouble);
                if(checkDisPrtsDouble.length > 0){
                    if(checkDisPrtsDouble.length == 1) $scope.DisplayDiscountParts = jejeranDiskonParts[0];
                    else $scope.DisplayDiscountParts = '';
                }else $scope.DisplayDiscountParts = '';
            }else $scope.DisplayDiscountParts = 0;


            if ($scope.gridWork.length > 0) {
                var allWarranty = 0;
                for (var i = 0; i<$scope.gridWork.length; i++){
                    if ($scope.gridWork[i].PaidById == 30) {
                        allWarranty++;
                    }
                }

                if (allWarranty != 0 && allWarranty == $scope.gridWork.length) {
                    $scope.DisplayDiscountTask = 0;
                    $scope.DisplayDiscountParts = 0;
                }
            }
        }
        


        
        var jejeranDiskonTaskViewEstimasi = [];
        var jejeranDiskonPartsViewEstimasi = [];
        $scope.displayDiscountAlgorithmViewEstimasi = function(){
            console.log('jejeranDiskonTaskViewEstimasi ==>',jejeranDiskonTaskViewEstimasi);
            console.log('jejeranDiskonPartsViewEstimasi ==>',jejeranDiskonPartsViewEstimasi);
            if(jejeranDiskonTaskViewEstimasi.length > 0){
                var checkDisTskDouble = _.uniqBy(jejeranDiskonTaskViewEstimasi, function (e) {return e;});
                console.log("jejeranDiskonTaskViewEstimasi uniq ===>",checkDisTskDouble);
                if(checkDisTskDouble.length > 0){
                    if(checkDisTskDouble.length == 1) $scope.DiscountTaskViewEstimasi = jejeranDiskonTaskViewEstimasi[0];
                    else $scope.DiscountTaskViewEstimasi = "";
                }else $scope.DiscountTaskViewEstimasi = "";
            }else $scope.DiscountTaskViewEstimasi = 0 
            
        
        
            if(jejeranDiskonPartsViewEstimasi.length > 0){
                var checkDisPrtsDouble = _.uniqBy(jejeranDiskonPartsViewEstimasi, function (e) {return e; });
                console.log("jejeranDiskonPartsViewEstimasi uniq ==>",checkDisPrtsDouble);
                if(checkDisPrtsDouble.length > 0){
                    if(checkDisPrtsDouble.length == 1)$scope.DiscountPartsViewEstimasi = jejeranDiskonPartsViewEstimasi[0];
                    else $scope.DiscountPartsViewEstimasi = "";
                }else $scope.DiscountPartsViewEstimasi = "";
            }else $scope.DiscountPartsViewEstimasi = 0;
        }


        $scope.collapseGridworkAndEstimasiTaskList = function(gridWork, mDataByEstimasi){
            console.log('already data from bslist     ====>',gridWork);
            console.log('new data from estimasi data  ====>',mDataByEstimasi);

            countEstimasiMasuk += 1;

            for(var i in mDataByEstimasi.JobTask){
                mDataByEstimasi.JobTask[i].EstimationBPId = mDataByEstimasi.EstimationBPId;
                if(mDataByEstimasi.JobTask[i].JobParts.length > 0){
                    for(var j in mDataByEstimasi.JobTask[i].JobParts){
                        console.log('convertDPpercentToNominal | Price       ===>',mDataByEstimasi.JobTask[i].JobParts[j].Price);
                        console.log('convertDPpercentToNominal | qty         ===>',mDataByEstimasi.JobTask[i].JobParts[j].qty );
                        console.log('convertDPpercentToNominal | Qty         ===>',mDataByEstimasi.JobTask[i].JobParts[j].Qty );
                        console.log('convertDPpercentToNominal | Discount    ===>',mDataByEstimasi.JobTask[i].JobParts[j].Discount);
                        console.log('convertDPpercentToNominal | DownPayment ===>',mDataByEstimasi.JobTask[i].JobParts[j].DownPayment );
                        console.log('convertDPpercentToNominal | DPestimasi  ===>',mDataByEstimasi.JobTask[i].JobParts[j].DPestimasi  );
                        mDataByEstimasi.JobTask[i].JobParts[j].EstimationBPId = mDataByEstimasi.JobTask[i].EstimationBPId;
                        mDataByEstimasi.JobTask[i].JobParts[j].TaskBPId = mDataByEstimasi.JobTask[i].TaskBPId;
                        tampungparts.push(mDataByEstimasi.JobTask[i].JobParts[j]);
                    }
                }
            }





            for(var i in gridWork){
                gridWork[i].isDeleted = 0;
                if(typeof gridWork[i].PutyEstimationMinute){
                    gridWork[i].PutyEstimationMinute = gridWork[i].PuttyEstimationMinute;
                }
                for(var j in gridWork[i].JobParts){
                    gridWork[i].JobParts[j].Price = gridWork[i].JobParts[j].RetailPrice;    
                    gridWork[i].JobParts[j].isDeleted = 0;
                    if(typeof gridWork[i].EstimationBPId != undefined){
                        gridWork[i].JobParts[j].EstimationBPId = gridWork[i].EstimationBPId;
                        //gridWork[i].JobParts[j].EstTaskBPId = gridWork[i].EstTaskBPId;
                        //gridWork[i].JobParts[j].EstPartsId = gridWork[i].EstPartsId;
                    }
                    tampungparts.push(gridWork[i].JobParts[j]);
                }
            }

            for(var i in mDataByEstimasi.JobTask){
                delete mDataByEstimasi.JobTask[i].JobParts
                delete mDataByEstimasi.JobTask[i].AEstimationBP_PartList
                tampungtask.push(mDataByEstimasi.JobTask[i]);
            }

            for(var i in gridWork){
                delete gridWork[i].JobParts;
                delete gridWork[i].AEstimationBP_PartList
                tampungtask.push(gridWork[i]);
            }

            partsAntiDuplicate = $scope.removeDuplicateParts(tampungparts);
            taskAntiDuplicate = $scope.removeDuplicateTask(tampungtask);

            for(var x in partsAntiDuplicate){
                for (var y in tampungparts){
                    if( partsAntiDuplicate[x].PartsId == tampungparts[y].PartsId && partsAntiDuplicate[x].TaskBPId == tampungparts[y].TaskBPId && partsAntiDuplicate[x].EstimationBPId != tampungparts[y].EstimationBPId ){
                        partsAntiDuplicate[x].Qty += tampungparts[y].Qty;
                        partsAntiDuplicate[x].qty += tampungparts[y].qty;
                    }
                }
            }

            for (var x in taskAntiDuplicate){
                taskAntiDuplicate[x].JobParts = [];
                for (var y in partsAntiDuplicate){
                    if(taskAntiDuplicate[x].EstimationBPId == partsAntiDuplicate[y].EstimationBPId && taskAntiDuplicate[x].EstTaskBPId == partsAntiDuplicate[y].EstTaskBPId){
                        taskAntiDuplicate[x].JobParts.push(partsAntiDuplicate[y]);
                        console.log('masuk sini')
                    }else{
                        if(taskAntiDuplicate[x].TaskBPId == partsAntiDuplicate[y].TaskBPId){
                            taskAntiDuplicate[x].JobParts.push(partsAntiDuplicate[y]);
                            console.log('masuk sana')
                        }
                    }
                }
            }

            console.log('tampungtask ===>',tampungtask);
            console.log('taskAntiDuplicate ===>',taskAntiDuplicate);
            console.log('tampungparts ===>',tampungparts);
            console.log('partsAntiDuplicate ===>',partsAntiDuplicate);
            console.log('result JOBTASK ==========================>',JSON.stringify(taskAntiDuplicate));
            return taskAntiDuplicate;
        }


        var tampungEstimasi = [];
        $scope.jejeranDP = [];
        $scope.processedDataEstimasi = function(index){

            flagArrayDP = 1;

            if($scope.selectedCekEstimasi[index].EstimationBPId !== 0){
                $scope.selectedCekEstimasi[index].EstimationBPId = $scope.selectedCekEstimasi[index].EstimationBPId;
                console.log('ini Estimasi belum WO ===> EstimationBPId => ' ,$scope.selectedCekEstimasi[index].EstimationBPId)
            }else{
                $scope.selectedCekEstimasi[index].EstimationBPId = $scope.selectedCekEstimasi[index].JobId;
                console.log('ini Estimasi sudah WO ===> JobId => ',$scope.selectedCekEstimasi[index].JobId)
            }

            AppointmentBpService.getDetailEstimasi($scope.selectedCekEstimasi[index].EstimationBPId, $scope.selectedCekEstimasi[index].PoliceNumber).then(function (res) {
            // AppointmentBpService.getDetailEstimasi($scope.selectedCekEstimasi[index].EstimationBPId).then(function (res) {
                //$scope.mDataByEstimasi =  angular.copy(res.data.Result[0]);
                $scope.mDataByEstimasi =  res.data.Result[0];
                console.log('res',$scope.mDataByEstimasi)
                $scope.mDataByEstimasi.Km = String($scope.mDataByEstimasi.Km);
                $scope.mDataByEstimasi.Km = $scope.mDataByEstimasi.Km.replace(/\B(?=(\d{3})+(?!\d))/g, ',');

                tampungEstimasi.push({ 
                    EstimationNo  : $scope.mDataByEstimasi.EstimationNo, 
                    EstTaskLength : $scope.mDataByEstimasi.AEstimationBP_TaskList_TT.length, 
                    EstimationBPId: $scope.mDataByEstimasi.EstimationBPId
                });

                console.log('tampungEstimasi | processedDataEstimasi ===>',tampungEstimasi);

                $scope.jejeranDP = [];
                for (var i = 0; i < $scope.mDataByEstimasi.AEstimationBP_TaskList_TT.length; i++) {
                    $scope.jejeranDP.push({TaskBPId:$scope.mDataByEstimasi.AEstimationBP_TaskList_TT[i].TaskBPId, partsList:[]});
                }
                


                //ngisi bslist permintaan
                if($scope.JobRequest.length > 0){ //kalo udah ada isinya
                    for(var xxx in $scope.JobRequest){
                        for(var yyy in $scope.mDataByEstimasi.AEstimationBP_Request_TT)
                        {
                            if($scope.JobRequest[xxx].RequestDesc == $scope.mDataByEstimasi.AEstimationBP_Request_TT[yyy].RequestDesc){
                                console.log('tidak ada yang di push')
                            }
                            else{
                                console.log('sssss',$scope.JobRequest)
                                $scope.JobRequest.push($scope.mDataByEstimasi.AEstimationBP_Request_TT[yyy]);
                            }
                        }
                    }
                }else{ //kalo kosong
                    console.log('ada yang di push')
                    $scope.JobRequest = $scope.mDataByEstimasi.AEstimationBP_Request_TT;
                }


                
                
                //buat dapetin diskon berdasarkan pembayaran (cash / asuransi)
                console.log('$scope.mData.isCash | kenyataan ====>',$scope.mData.isCash);
                if($scope.mData.isCash == undefined){
                    console.log('$scope.mData.isCash | undefined ====>',$scope.mData.isCash);
                    $scope.choosePembayaran($scope.mDataByEstimasi.isCash,false);
                    $scope.mData.isCash = $scope.mDataByEstimasi.isCash;
                    if($scope.mDataByEstimasi.isCash == 0){ // untuk dapetin nama asuransi + diskonnya
                        for (i in $scope.Asuransi){
                            if($scope.Asuransi[i].InsuranceId == $scope.mDataByEstimasi.InsuranceId){
                                $scope.mData.InsuranceName = $scope.Asuransi[i].InsuranceName;
                                $scope.selectInsurance($scope.Asuransi[i],false);
                            }
                        }
                    }
                }else{
                    console.log('$scope.mData.isCash | ada isinya ====>',$scope.mData.isCash);
                }


                if($scope.inisudahTerdaftar = false){

                }
                
                //untuk dapetin Model, Tipe & Katashiki dari estimasi
                var tampunganVehicleTypeId = $scope.mDataByEstimasi.VehicleTypeId;
                for (var i = 0; i < $scope.modelData.length; i++) {
                    if ($scope.modelData[i].VehicleModelId == parseInt($scope.mDataByEstimasi.ModelCode)){
                        $scope.mData.VehicleModelName       = $scope.modelData[i].VehicleModelName;
                        $scope.mData.VehicleModelId         = $scope.modelData[i].VehicleModelId;
                        $scope.mDataDetail.VehicleModelName = $scope.modelData[i].VehicleModelName;
                        $scope.mDataDetail.VehicleModelId   = $scope.modelData[i].VehicleModelId;
                        console.log('model gue ===> ',parseInt($scope.mDataByEstimasi.ModelCode) +" == $scope.modelData "+ $scope.modelData[i].VehicleModelId );

                        WO.getVehicleTypeById(parseInt($scope.mDataByEstimasi.ModelCode)).then(function(res) {
                            // $scope.typeData = res.data.Result;

                            $scope.katashikiData = _.uniq(res.data.Result, 'KatashikiCode');

                            for (var i=0; i<$scope.katashikiData.length; i++){
                                $scope.katashikiData[i].NewDescription = $scope.mDataDetail.VehicleModelName + ' - ' + $scope.katashikiData[i].KatashikiCode
                            }

                            for (var i=0; i < $scope.katashikiData.length; i++) {
                                if ($scope.katashikiData[i].KatashikiCode == $scope.mDataByEstimasi.KatashikiCode) {
                                    $scope.mDataDetail.KatashikiCodex   = $scope.mDataByEstimasi.KatashikiCode;

                                }
                            }

                            if ($scope.mDataDetail.KatashikiCodex != null && $scope.mDataDetail.KatashikiCodex != undefined && $scope.mDataDetail.KatashikiCodex != '') {
                                AppointmentGrService.GetCVehicleTypeListByKatashiki($scope.mDataByEstimasi.KatashikiCode).then(function(res) {
                                    // $scope.typeData = res.data.Result[0];
                                    $scope.typeData = res.data.Result;

                                    for (var z = 0; z < $scope.typeData.length; z++) {
                                        $scope.typeData[z].NewDescription = $scope.typeData[z].Description + ' - ' + $scope.typeData[z].KatashikiCode + ' - ' + $scope.typeData[z].SuffixCode
                                    }

                                    for (var z = 0; z < $scope.typeData.length; z++) {
                                        if($scope.typeData[z].VehicleTypeId == tampunganVehicleTypeId){
                                            $scope.selectType($scope.typeData[z])
                                            console.log('Type Gue ===> ',tampunganVehicleTypeId +" == ModelOptions "+$scope.typeData[z].VehicleTypeId);
                                        }
                                    }

                                })
                            }

                            
                        })
                    }
                }


                
                //penyesuaian dari estimasi [Tasklist & Partlist]
                $scope.mDataByEstimasi.JobTask = $scope.mDataByEstimasi.AEstimationBP_TaskList_TT;
                var TempJobParts = [];
                console.log('estimasi tasklist dan partlist',$scope.mDataByEstimasi.JobTask)


                for (var i = 0; i < $scope.mDataByEstimasi.JobTask.length; i++) {
                    for (var j = 0; j < $scope.mDataByEstimasi.JobTask[i].AEstimationBP_PartList.length; j++) {
                        $scope.mDataByEstimasi.JobTask[i].AEstimationBP_PartList[j].Qty = $scope.mDataByEstimasi.JobTask[i].AEstimationBP_PartList[j].qty;
                        TempJobParts.push($scope.mDataByEstimasi.JobTask[i].AEstimationBP_PartList[j]);
                    }
                    $scope.mDataByEstimasi.JobTask[i].JobParts = [];
                    for (var k = 0; k < TempJobParts.length; k++) {
                        if($scope.mDataByEstimasi.JobTask[i].EstTaskBPId == TempJobParts[k].EstTaskBPId){
                            $scope.mDataByEstimasi.JobTask[i].JobParts.push(TempJobParts[k]);
                        }
                    }
                }


                for(var m in $scope.mDataByEstimasi.JobTask){
                    for(var n in $scope.mDataByEstimasi.JobTask[m].JobParts){
                        $scope.mDataByEstimasi.JobTask[m].JobParts[n].Satuan = null;
                        for(var o in $scope.unitData){
                            if($scope.unitData[o].MasterId == $scope.mDataByEstimasi.JobTask[m].JobParts[n].SatuanId){
                                $scope.mDataByEstimasi.JobTask[m].JobParts[n].Name = $scope.unitData[o].Name;
                                $scope.mDataByEstimasi.JobTask[m].JobParts[n].satuanName = $scope.unitData[o].Name;
                            }
                        }
                    }
                }

                for(var m in $scope.mDataByEstimasi.JobTask){
                    $scope.mDataByEstimasi.JobTask[m].PaidBy = null;
                    // $scope.mDataByEstimasi.JobTask[m].Discount = $scope.DiscountTask;
                    $scope.mDataByEstimasi.JobTask[m].EstimationBPId = $scope.mDataByEstimasi.EstimationBPId;
                    delete $scope.mDataByEstimasi.JobTask[m].JobId;

                    
                    for(var x in $scope.paymentData ){
                        if($scope.paymentData[x].MasterId == $scope.mDataByEstimasi.JobTask[m].PaidById){
                            console.log('dapet nih task ===>',$scope.mDataByEstimasi.JobTask[m].PaidById);
                            $scope.mDataByEstimasi.JobTask[m].PaidBy = $scope.paymentData[x].Name;
                        }else{
                            console.log('tak dapet nih task ===>',$scope.mDataByEstimasi.JobTask[m].PaidById);
                        }
                    }
                    
                    if($scope.mDataByEstimasi.JobTask[m].JobParts.length > 0){
                        for(var n in $scope.mDataByEstimasi.JobTask[m].JobParts){
                            $scope.mDataByEstimasi.JobTask[m].JobParts[n].PaidBy = null;
                            $scope.mDataByEstimasi.JobTask[m].JobParts[n].RetailPriceMask = $scope.mDataByEstimasi.JobTask[m].JobParts[n].Price.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                            $scope.mDataByEstimasi.JobTask[m].JobParts[n].subTotalMask = ($scope.mDataByEstimasi.JobTask[m].JobParts[n].Price * $scope.mDataByEstimasi.JobTask[m].JobParts[n].qty).toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                            if($scope.mDataByEstimasi.JobTask[m].JobParts[n].ETA == null  ){
                                $scope.mDataByEstimasi.JobTask[m].JobParts[n].isAvailable = 1; // tersedia partsnya
                            }else{
                                $scope.mDataByEstimasi.JobTask[m].JobParts[n].isAvailable = 0; //tidak tersedia partsnya          
                            }
                            delete $scope.mDataByEstimasi.JobTask[m].JobParts[n].IsCustomDiscount;
                            delete $scope.mDataByEstimasi.JobTask[m].JobParts[n].JobId ;
    
    
                            
                            for(var z in $scope.paymentData ){
                                if($scope.paymentData[z].MasterId == $scope.mDataByEstimasi.JobTask[m].JobParts[n].PaidById){
                                    console.log('dapett nih parts ===>  $scope.mDataByEstimasi.JobTask[m].JobParts[n].PaidById')
                                    $scope.mDataByEstimasi.JobTask[m].JobParts[n].PaidBy = $scope.paymentData[z].Name;
                                }
                            }
                        }
                    }
                }

                var x = 0
                for(var i in $scope.mDataByEstimasi.JobTask){

                    if($scope.mDataByEstimasi.JobTask[i].TaskBPId == null){
                        x++;
                        $scope.mDataByEstimasi.JobTask[i].TaskBPId = x + "flagNonTaskList" + new Date().getTime();                        
                    }

                    if($scope.mDataByEstimasi.isCash == 1){//apapun diskon dr estimasi BP, BUANG ganti pake diskon booking, kecuali asuransi | by BU 9-Jun-2020 
                        if ($scope.mDataByEstimasi.JobTask[i].PaidById != 30) {
                            $scope.mDataByEstimasi.JobTask[i].Discount = tmpDiskonTask; // -1 tidak ada diskon | kalo ga ada diskon ambil dari master diskon booking
                        } else {
                            $scope.mDataByEstimasi.JobTask[i].Discount = 0
                        }
                        console.log('TaskName Disc From Master ===>', $scope.mDataByEstimasi.JobTask[i].TaskName+ ' | disType =>' + $scope.mDataByEstimasi.JobTask[i].isCash + ' | discount => ' + $scope.mDataByEstimasi.JobTask[i].Discount + ' | tmpDiskonTask =>'+tmpDiskonTask);
                    }else{
                        console.log('TaskName Disc From Asuransi ===>', $scope.mDataByEstimasi.JobTask[i].TaskName+ ' | disType =>' + $scope.mDataByEstimasi.JobTask[i].DiscountTypeId + ' | discount => ' + $scope.mDataByEstimasi.JobTask[i].Discount);
                    }
                    for(var j in $scope.mDataByEstimasi.JobTask[i].JobParts){ //apapun diskon dr estimasi BP, BUANG ganti pake diskon booking, kecuali asuransi | by BU 9-Jun-2020 
                        if($scope.mDataByEstimasi.isCash == 1){
                            if ($scope.mDataByEstimasi.JobTask[i].JobParts[j].PaidById != 30) {
                                $scope.mDataByEstimasi.JobTask[i].JobParts[j].Discount = tmpDiskonParts; // -1 tidak ada diskon | kalo ga ada diskon ambil dari master diskon booking
                            } else {
                                $scope.mDataByEstimasi.JobTask[i].JobParts[j].Discount = 0
                            }
                            console.log('PartsName Disc From Master ===>', $scope.mDataByEstimasi.JobTask[i].JobParts[j].PartsName+ ' | disType =>' + $scope.mDataByEstimasi.JobTask[i].JobParts[j].DiscountTypeId + ' | discount => ' + $scope.mDataByEstimasi.JobTask[i].JobParts[j].Discount + ' | tmpDiskonParts =>'+tmpDiskonParts);
                        }else{
                            console.log('PartsName Disc From Asuransi ===>', $scope.mDataByEstimasi.JobTask[i].JobParts[j].PartsName+ ' | disType =>' + $scope.mDataByEstimasi.JobTask[i].JobParts[j].DiscountTypeId + ' | discount => ' + $scope.mDataByEstimasi.JobTask[i].JobParts[j].Discount);
                        }
                    }
                }

                //penyesuaian dari estimasi [Tasklist & Partlist]


                //kalo data parent kosong masukin data dari estimasi
                if($scope.mData.WoCategoryId        == undefined){$scope.mData.WoCategoryId        = $scope.mDataByEstimasi.CategoryId}
                if($scope.mData.Km                  == undefined){$scope.mData.Km                  = $scope.mDataByEstimasi.Km}
                if($scope.mData.ContactPerson       == undefined){$scope.mData.ContactPerson       = $scope.mDataByEstimasi.ContactPerson}
                if($scope.mData.PhoneContactPerson1 == undefined){$scope.mData.PhoneContactPerson1 = $scope.mDataByEstimasi.PhoneContactPerson1}
                if($scope.mData.Email               == undefined){$scope.mData.Email               = $scope.mDataByEstimasi.Email}
                if($scope.mData.isCash              == undefined){$scope.mData.isCash              = $scope.mDataByEstimasi.isCash}

                $scope.mData.VIN = $scope.mDataByEstimasi.VIN;
                $scope.mData.VehicleTypeDesc = $scope.mDataByEstimasi.VehicleTypeDesc;
                $scope.mData.Alamat = $scope.mDataByEstimasi.Alamat;
                $scope.mData.ColorName = $scope.mDataByEstimasi.ColorName;
                $scope.mData.ColorCode = $scope.mDataByEstimasi.ColorCode;
                $scope.mData.ColorId = $scope.mDataByEstimasi.ColorId;
                $scope.mData.EngineNo = $scope.mDataByEstimasi.EngineNo;
                
                
                
                //untuk dapetin tasklist
                dataJobTaskPlan = []; // jancok
                console.log('grid work pas klik estimasi ===>', $scope.gridWork);

                for( var j in $scope.gridWork){
                    $scope.gridWork[j].JobParts = $scope.gridWork[j].child;
                    for(var m in $scope.gridWork[j].JobParts){
                        $scope.gridWork[j].JobParts[m].Satuan = null; //dapetin satuan
                        for(var o in $scope.unitData){
                            if($scope.unitData[o].MasterId == $scope.gridWork[j].JobParts[m].SatuanId){
                                // $scope.gridWork[j].JobParts[m].Satuan.MasterId = $scope.unitData[o].MasterId;
                                $scope.gridWork[j].JobParts[m].Name = $scope.unitData[o].Name;
                                $scope.gridWork[j].JobParts[m].satuanName = $scope.unitData[o].satuanName;
                            }
                        }

                        $scope.gridWork[j].JobParts[m].PaidBy = null; //dapetin tipe pembayaran
                        for(var z in $scope.paymentData ){
                            if($scope.paymentData[z].MasterId == $scope.gridWork[j].JobParts[m].PaidById){
                                // $scope.gridWork[j].JobParts[m].PaidBy.MasterId = $scope.paymentData[z].MasterId;
                                // $scope.gridWork[j].JobParts[m].PaidBy.Name = $scope.paymentData[z].Name;
                                $scope.gridWork[j].JobParts[m].PaidBy = $scope.paymentData[z].Name;
                            }
                        }
                    }
                }


                var TaskTanpaDuplikat = $scope.collapseGridworkAndEstimasiTaskList($scope.gridWork, $scope.mDataByEstimasi);

                
        

                if(countEstimasiMasuk == jumlahEstimasiMasuk){
                    console.log('countEstimasiMasuk =>',countEstimasiMasuk +' | jumlahEstimasiMasuk => '+ jumlahEstimasiMasuk);
                    // console.log('estimasi ini masuk ===>',$scope.mDataByEstimasi.JobTask[0].JobParts.length);
                    $scope.mDataByEstimasi.JobTask =[];
                    $scope.mDataByEstimasi.JobTask = TaskTanpaDuplikat;
                    $scope.mDataByEstimasi.AEstimationBP_TaskList_TT = [];
                    $scope.mDataByEstimasi.AEstimationBP_TaskList_TT = TaskTanpaDuplikat;

                    


                    $scope.dataForBslist($scope.mDataByEstimasi);
                    // $scope.sumAllPrice();
                    countEstimasiMasuk = 0;
                    jumlahEstimasiMasuk = 0;
                }else{
                    console.log('countEstimasiMasuk =>',countEstimasiMasuk +' | jumlahEstimasiMasuk => '+ jumlahEstimasiMasuk);
                    // console.log('estimasi ini ga masuk ===>',$scope.mDataByEstimasi.JobTask[0].JobParts.length);
                }

                
                
                // dari estimasi BP ga usah bawa data Vehicle condition


                // $scope.areasArray = [];
                // $scope.customHeight = { height: '500px' };
                // mstId = $scope.mDataByEstimasi.AEstimationBP_WAC_Ext[0].TypeId;
                // $scope.filter.vType = mstId;
                // // $scope.filter.vType = mstId;
                // $scope.areasArray = getImgAndPoints(mstId);
                // var resu = $scope.mDataByEstimasi.AEstimationBP_WAC_Ext;

                // console.log('resuu ===>',resu);

                

                // console.log("$scope.areasArray 1 ====>", $scope.areasArray);
                // $scope.filter.vType = mstId;

                // dotsData[mstId] = [];
                // for (var i in resu) {
                //     var getTemp = [];
                //     getTemp =  JSON.parse(resu[i].Points);
                //     var xtemp = {};


                //     console.log('gettemp', getTemp);
                //     for (var j in getTemp) {
                //         console.log('gettempJstat', getTemp[j]);
                //         var xtemp = {};
                //         xtemp = angular.copy(getTemp[j]);
                //         delete xtemp['status'];
                //         delete xtemp['PointId']; //jancok
                //         console.log("xtemp for J : ", xtemp);
                //         console.log("scope.areasArray 2 : ", $scope.areasArray);
                //         if (typeof _.find($scope.areasArray, xtemp) !== 'undefined') {
                //             console.log("merah");
                //             console.log("find..", xtemp);
                //             console.log("scope.areasArray 3 : ", $scope.areasArray);
                //             var idx = _.findIndex($scope.areasArray, xtemp);
                //             console.log('idx', idx);
                //             xtemp.cssClass = "choosen-area";
                //             $scope.areasArray[idx] = xtemp;

                //             dotsData[mstId].push({
                //                 "ItemId": xtemp.areaid,
                //                 "ItemName": xtemp.name,
                //                 "Points": xtemp,
                //                 "Status": getTemp[j].status
                //             });
                //         }else{
                //             console.log('gamasuk kunyukkk');
                //             console.log('$scope.areasArray ====>',$scope.areasArray);
                //             console.log('xtemp ==================>',xtemp);
                //         }
                //     }
                // }

                // setDataGrid(dotsData[mstId]);    
                // $scope.disableWACExt = true;

            });

            $scope.btnBatalCekEstimasi();
        }


        $scope.mData.WoCategoryId        = undefined;
        $scope.mData.Km                  = undefined;
        $scope.mData.ContactPerson       = undefined;
        $scope.mData.PhoneContactPerson1 = undefined;
        $scope.mData.Email               = undefined;
        $scope.mData.isCash              = undefined;


        

        var countEstimasiMasuk = 0;
        var jumlahEstimasiMasuk = 0;
        var tmpDiskonTask = 0;
        var tmpDiskonParts = 0;
        MasterDiscountFactory.getDataAktif().then(
            function (res) {
                for(var i = 0; i < res.data.Result.length; i++){
                    if(res.data.Result[i].Category == 2){
                        tmpDiskonParts = res.data.Result[i].Discount;
                    }else if(res.data.Result[i].Category == 1){
                        tmpDiskonTask = res.data.Result[i].Discount;
                    }
                }
            },
            function (err) {
             }
        );
        $scope.btnPilihEstimasi = function () {
            tampungparts = [];
            tampungtask = [];
            taskAntiDuplicate = [];
            partsAntiDuplicate = [];
            
            jumlahEstimasiMasuk = $scope.selectedCekEstimasi.length;
            
            for(var i in  $scope.selectedCekEstimasi){
                console.log('ini estimasi yang dipilih ===>',$scope.selectedCekEstimasi[i]);
                console.log('$scope.selectedCekEstimasi[i].ModelCode ===>',$scope.selectedCekEstimasi[i].ModelCode);
                console.log('$scope.mDataDetail.VehicleModelId         ===>',$scope.mDataDetail.VehicleModelId);

                console.log('tampung estimasi ===>',tampungEstimasi)
                if($scope.mDataDetail.VehicleModelId !== undefined){
                    if (parseInt($scope.selectedCekEstimasi[i].ModelCode) !== $scope.mDataDetail.VehicleModelId){
                        $scope.bsAlertConfirm("Anda tidak dapat menggunakan data Estimasi ini karna Model kendaraan anda berbeda dengan Estimasi ini").then(function (res) {
                            if (res) {
                                console.log('ini kalo jenis mobilnya sama | OK');
                            }else{
                                console.log('ini kalo jenis mobilnya sama | !OK');
                            }
                        });
                    }else{
                        console.log('ini masuk processedDataEstimasi | kalo katashikinya ada isinya');
                        $scope.processedDataEstimasi(i);
                    }
                }else{
                    console.log('ini masuk processedDataEstimasi | kalo katashikinya kosong');
                    $scope.processedDataEstimasi(i);
                }
            }
           

        };        


        $scope.sumAllPrice = function() {
            console.log('sumAllPrice gridWork ===>',$scope.gridWork);

            var NumberParent = 1;
            var NumberChild = 1;
            for (i in $scope.gridWork){
                $scope.gridWork[i].NumberParent = NumberParent+parseInt(i);
                for (j in $scope.gridWork[i].child){
                    $scope.gridWork[i].child[j].NumberChild = $scope.gridWork[i].NumberParent + "."  + parseInt(++j);
                }
            }


            var totalW = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                    totalW += 0;
                }else{
                    // totalW += Math.round($scope.gridWork[i].Fare /1.1);
                    totalW += ASPricingEngine.calculate({
                                    InputPrice: $scope.gridWork[i].Fare,
                                    // Discount: 5,
                                    // Qty: 2,
                                    Tipe: 2, 
                                    PPNPercentage: PPNPerc,
                                });
                }
            }
            $scope.totalWork = totalW;

            var totalDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child.length > 0) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].DownPayment !== null) {
                            
                            // totalDp +=  $scope.gridWork[i].child[j].DownPayment * 1.1 ;
                            totalDp +=  $scope.gridWork[i].child[j].DownPayment ;
                            console.log("TotalDP pake 1.1",  $scope.gridWork[i].child[j].DownPayment);
                            
                        }
                    }
                }
            }
            $scope.totalDp = totalDp;

            
            var totalDefaultDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child.length > 0) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].minimalDp !== undefined) {
                            totalDefaultDp += $scope.gridWork[i].child[j].minimalDp;
                            console.log("$scope.gridWork[i].child.totalDefaultDp  ===>", $scope.gridWork[i].child[j].minimalDp);
                        }
                    }
                }
            }
            $scope.totalDpDefault = totalDefaultDp;
            var totalActualRate = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].ActualRate !== undefined) {
                    totalActualRate += $scope.gridWork[i].ActualRate;
                }

            }
            $scope.tmpActRate = totalActualRate;


            var totalMaterial_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child.length > 0) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30){
                            totalMaterial_byKevin += 0;
                        }else{
                            // totalMaterial_byKevin += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                            totalMaterial_byKevin += ASPricingEngine.calculate({
                                                        InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                        // Discount: 5,
                                                        Qty: $scope.gridWork[i].child[j].Qty,
                                                        Tipe: 102, 
                                                        PPNPercentage: PPNPerc,
                                                    }); 
                        }
                    }
                }
            };
            
            var totalMD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child.length > 0) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30){//req by pak eko bahan tetap dapet diskon 08-juli-2020
                            totalMD_byKevin += 0;
                        }else{
                            // totalMD_byKevin +=  Math.round(Math.round($scope.gridWork[i].child[j].RetailPrice /1.1 ) * ($scope.gridWork[i].child[j].Discount/100)) *  $scope.gridWork[i].child[j].Qty;
                            totalMD_byKevin +=  ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                    Discount: $scope.gridWork[i].child[j].Discount,
                                                    Qty: $scope.gridWork[i].child[j].Qty,
                                                    Tipe: 112, 
                                                    PPNPercentage: PPNPerc,
                                                }); 
                            
                        }
                    }
                }
            };

            var totalWD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                    totalWD_byKevin += 0;
                }else{
                    // totalWD_byKevin +=  Math.round(Math.round($scope.gridWork[i].Fare /1.1 ) * ($scope.gridWork[i].Discount/100));
                    totalWD_byKevin +=  ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWork[i].Fare,
                                            Discount: $scope.gridWork[i].Discount,
                                            // Qty: 2,
                                            Tipe: 12, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                }
            };





            $scope.totalMaterial           = totalMaterial_byKevin;
            $scope.totalMaterialDiscounted = totalMD_byKevin;
            $scope.totalWorkDiscounted     = totalWD_byKevin;
            $scope.subTotalMaterialSummary = $scope.totalMaterial - $scope.totalMaterialDiscounted
            $scope.subTotalWorkSummary     = $scope.totalWork - $scope.totalWorkDiscounted;
            // $scope.totalPPN                = Math.floor(($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * 0.1);  
            $scope.totalPPN                = ASPricingEngine.calculate({
                                                InputPrice: (($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * (1+PPNPerc/100)),
                                                // Discount: 0,
                                                // Qty: 0,
                                                Tipe: 33, 
                                                PPNPercentage: PPNPerc,
                                            });        

            $scope.totalEstimasi           = $scope.totalPPN + $scope.subTotalMaterialSummary + $scope.subTotalWorkSummary;


          
            console.log("$scope.gridWork", $scope.gridWork);
            console.log("gridWork", $scope.gridWork);


            //nentuin diskon task & parts
            jejeranDiskonTask = [];
            jejeranDiskonParts = [];
            for(var i in $scope.gridWork){
                // if($scope.gridWork.PaidById == 30|| $scope.gridWork.PaidById == 31 || $scope.gridWork.PaidById == 32 || $scope.gridWork.PaidById == 2277){
                if($scope.gridWork.PaidById == 30|| $scope.gridWork.PaidById == 2277){
                    jejeranDiskonTask.push(0);
                }else{
                    jejeranDiskonTask.push($scope.gridWork[i].Discount);
                }
                if($scope.gridWork[i].child.length > 0 ){
                    for(var j in $scope.gridWork[i].child){
                        // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 31 || $scope.gridWork[i].child[j].PaidById == 32 || $scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 2277){
                            jejeranDiskonParts.push(0);
                        }else{
                            jejeranDiskonParts.push($scope.gridWork[i].child[j].Discount);
                        }

                        
                    }
                }
            }
            //nentuin diskon task & parts
            $scope.displayDiscountAlgorithm();
        };

        $scope.sumAllPriceViewEstimasi = function() {

            var NumberParent = 1;
            var NumberChild = 1;
            for (i in $scope.gridWorkViewEstimasi){
                $scope.gridWorkViewEstimasi[i].NumberParent = NumberParent+parseInt(i);
                for (j in $scope.gridWorkViewEstimasi[i].child){
                    $scope.gridWorkViewEstimasi[i].child[j].NumberChild = $scope.gridWorkViewEstimasi[i].NumberParent + "."  + parseInt(++j);
                }
            }


            console.log('sumAllPrice gridWorkViewEstimasi ===>',$scope.gridWorkViewEstimasi);
            var totalW = 0;
            for (var i = 0; i < $scope.gridWorkViewEstimasi.length; i++) {
                if ($scope.gridWorkViewEstimasi[i].Summary !== undefined) {
                    if ($scope.gridWorkViewEstimasi[i].Summary == "") {
                        $scope.gridWorkViewEstimasi[i].Summary = $scope.gridWorkViewEstimasi[i].Fare;
                    }
                    if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                        totalW += 0;
                    }else{
                        // totalW += Math.round($scope.gridWorkViewEstimasi[i].Summary / 1.1);
                        totalW += ASPricingEngine.calculate({
                                    InputPrice: $scope.gridWorkViewEstimasi[i].Summary,
                                    // Discount: 5,
                                    // Qty: 2,
                                    Tipe: 2, 
                                    PPNPercentage: PPNPerc,
                                });
                    }
                    
                }
            }


            $scope.totalWorkViewEstimasi = totalW ;


            // var totalM = 0;
            // for (var i = 0; i < $scope.gridWorkViewEstimasi.length; i++) {
            //     if ($scope.gridWorkViewEstimasi[i].child !== undefined) {
            //         for (var j = 0; j < $scope.gridWorkViewEstimasi[i].child.length; j++) {
            //             if ($scope.gridWorkViewEstimasi[i].child[j].subTotal !== undefined) {

            //                 // totalM += $scope.gridWorkViewEstimasi[i].child[j].subTotal;
            //                 totalM += $scope.gridWorkViewEstimasi[i].child[j].subTotalForDisplay;
            //                     console.log("$scope.gridWorkViewEstimasi[i].child.Price totalM ===>", $scope.gridWorkViewEstimasi[i].child[j].subTotal);
                            
            //             }
            //         }
            //     }
            // }
            // $scope.totalMaterialViewEstimasi = parseInt(totalM / (1.1));  //request pak dodi dibagi 1.1 13/02/2019

            var totalDp = 0; 
            for (var i = 0; i < $scope.gridWorkViewEstimasi.length; i++) {
                if ($scope.gridWorkViewEstimasi[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWorkViewEstimasi[i].child.length; j++) {
                        if ($scope.gridWorkViewEstimasi[i].child[j].DownPayment !== null) {
                            // totalDp += $scope.gridWorkViewEstimasi[i].child[j].DownPayment * 1.1;
                            totalDp += $scope.gridWorkViewEstimasi[i].child[j].DownPayment;
                            console.log("$scope.gridWorkViewEstimasi[i].totalDp ===>", $scope.gridWorkViewEstimasi[i].child[j].DownPayment);
                        }
                    }
                }
            }
            $scope.totalDpViewEstimasi = totalDp;

            



            
            var totalActualRate = 0;
            for (var i = 0; i < $scope.gridWorkViewEstimasi.length; i++) {
                if ($scope.gridWorkViewEstimasi[i].ActualRate !== undefined) {
                    totalActualRate += $scope.gridWorkViewEstimasi[i].ActualRate;
                }

            }
            $scope.tmpActRate = totalActualRate;



            var totalMaterial_byKevin = 0;
            for (var i = 0; i < $scope.gridWorkViewEstimasi.length; i++) {
                if ($scope.gridWorkViewEstimasi[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWorkViewEstimasi[i].child.length; j++) {
                        if($scope.gridWorkViewEstimasi[i].child[j].PaidById == 2277 || $scope.gridWorkViewEstimasi[i].child[j].PaidById == 30){
                            totalMaterial_byKevin += 0;
                        }else{
                            // totalMaterial_byKevin += Math.round($scope.gridWorkViewEstimasi[i].child[j].RetailPrice/1.1) * $scope.gridWorkViewEstimasi[i].child[j].Qty;
                            totalMaterial_byKevin += ASPricingEngine.calculate({
                                                        InputPrice: $scope.gridWorkViewEstimasi[i].child[j].RetailPrice,
                                                        // Discount: 5,
                                                        Qty: $scope.gridWorkViewEstimasi[i].child[j].Qty,
                                                        Tipe: 102, 
                                                        PPNPercentage: PPNPerc,
                                                    }); 
                            
                        }
                    }
                }
            };
            
            var totalMD_byKevin = 0;
            for (var i = 0; i < $scope.gridWorkViewEstimasi.length; i++) {
                if ($scope.gridWorkViewEstimasi[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWorkViewEstimasi[i].child.length; j++) {
                        if($scope.gridWorkViewEstimasi[i].child[j].PaidById == 2277 || $scope.gridWorkViewEstimasi[i].child[j].PaidById == 30){//req by pak eko bahan tetap dapet diskon 08-juli-2020
                                totalMD_byKevin += 0;
                        }else{
                            // totalMD_byKevin +=  Math.round(Math.round($scope.gridWorkViewEstimasi[i].child[j].RetailPrice /1.1 ) * ($scope.gridWorkViewEstimasi[i].child[j].Discount/100)) *  $scope.gridWorkViewEstimasi[i].child[j].Qty;
                            totalMD_byKevin +=  ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWorkViewEstimasi[i].child[j].RetailPrice,
                                                    Discount: $scope.gridWorkViewEstimasi[i].child[j].Discount,
                                                    Qty: $scope.gridWorkViewEstimasi[i].child[j].Qty,
                                                    Tipe: 112, 
                                                    PPNPercentage: PPNPerc,
                                                }); 
                            
                        }
                    }
                }
            };

            var totalWD_byKevin = 0;
            for (var i = 0; i < $scope.gridWorkViewEstimasi.length; i++) {
                if($scope.gridWorkViewEstimasi[i].PaidById == 2277 || $scope.gridWorkViewEstimasi[i].PaidById == 30){
                    totalWD_byKevin += 0;
                }else{
                    // totalWD_byKevin +=  Math.round(Math.round($scope.gridWorkViewEstimasi[i].Fare /1.1 ) * ($scope.gridWorkViewEstimasi[i].Discount/100));
                    totalWD_byKevin +=  ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWorkViewEstimasi[i].Fare,
                                            Discount: $scope.gridWorkViewEstimasi[i].Discount,
                                            // Qty: 2,
                                            Tipe: 12, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                }
            };


            $scope.totalMaterialViewEstimasi           = totalMaterial_byKevin;
            $scope.totalMaterialDiscountedViewEstimasi = totalMD_byKevin;
            $scope.totalWorkDiscountedViewEstimasi     = totalWD_byKevin;
            $scope.subTotalMaterialSummaryViewEstimasi = $scope.totalMaterialViewEstimasi - $scope.totalMaterialDiscountedViewEstimasi
            $scope.subTotalWorkSummaryViewEstimasi     = $scope.totalWorkViewEstimasi - $scope.totalWorkDiscountedViewEstimasi;
            // $scope.totalPPNViewEstimasi                = Math.floor(($scope.subTotalMaterialSummaryViewEstimasi + $scope.subTotalWorkSummaryViewEstimasi) * 0.1);    
            $scope.totalPPNViewEstimasi                = ASPricingEngine.calculate({
                                                            InputPrice: (($scope.subTotalMaterialSummaryViewEstimasi + $scope.subTotalWorkSummaryViewEstimasi) * (1+PPNPerc/100)),
                                                            // Discount: 0,
                                                            // Qty: 0,
                                                            Tipe: 33, 
                                                            PPNPercentage: PPNPerc,
                                                        });             
            $scope.totalEstimasiViewEstimasi           = $scope.totalPPNViewEstimasi + $scope.subTotalMaterialSummaryViewEstimasi + $scope.subTotalWorkSummaryViewEstimasi;


            
          
            console.log("$scope.gridWorkViewEstimasi", $scope.gridWorkViewEstimasi);
            console.log("gridWorkViewEstimasi", $scope.gridWorkViewEstimasi);


            //nentuin diskon task & parts
            jejeranDiskonTaskViewEstimasi = [];
            jejeranDiskonPartsViewEstimasi = [];
            for(var i in $scope.gridWorkViewEstimasi){
                // if($scope.gridWorkViewEstimasi.PaidById == 30|| $scope.gridWorkViewEstimasi.PaidById == 31 || $scope.gridWorkViewEstimasi.PaidById == 32 || $scope.gridWorkViewEstimasi.PaidById == 2277){
                if($scope.gridWorkViewEstimasi.PaidById == 30|| $scope.gridWorkViewEstimasi.PaidById == 2277){
                    jejeranDiskonTaskViewEstimasi.push(0);
                }else{
                    jejeranDiskonTaskViewEstimasi.push($scope.gridWorkViewEstimasi[i].Discount);
                }
                if($scope.gridWorkViewEstimasi[i].child.length > 0 ){
                    for(var j in $scope.gridWorkViewEstimasi[i].child){
                        // if($scope.gridWorkViewEstimasi[i].child[j].PaidById == 30|| $scope.gridWorkViewEstimasi[i].child[j].PaidById == 31 || $scope.gridWorkViewEstimasi[i].child[j].PaidById == 32 || $scope.gridWorkViewEstimasi[i].child[j].PaidById == 2277){
                        if($scope.gridWorkViewEstimasi[i].child[j].PaidById == 30|| $scope.gridWorkViewEstimasi[i].child[j].PaidById == 2277){
                            jejeranDiskonPartsViewEstimasi.push(0);
                        }else{
                            jejeranDiskonPartsViewEstimasi.push($scope.gridWorkViewEstimasi[i].child[j].Discount);
                        }

                        
                    }
                }
            }
            $scope.displayDiscountAlgorithmViewEstimasi();
            //nentuin diskon task & parts
        };


        $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi = function(item, x, y) {
            console.log("getAvailablePartsServiceManualFromEstimasiBPViewEstimasi", item);
            // console.log("getAvailablePartsServiceManualFromEstimasiBPViewEstimasi  ===>", JSON.stringify(item));            
            console.log("x : ", x);
            console.log("y : ", y);

            if (x > item.length - 1) {
                return item;
            }
            if (item[x].isDeleted !== 1) {
                console.log('kondisi 1');
                console.log('item[x].JobParts[y] ===>',item[x].JobParts[y]);
                console.log('item[0].JobParts[0] ===>',item[0].JobParts[0]);
                console.log('x ===>',x);
                console.log('y ===>',y);
                
                // console.log('item[x].JobParts[y].PartsId ===>',item[x].JobParts[y].PartsId);
                if (item[x].JobParts[y] !== undefined && item[x].JobParts[y].PartsId !== null) {
                    console.log('kondisi 1.1');
                    var itemTemp = item[x].JobParts[y];
                    // if (itemTemp.isDeleted == 0){
                        AppointmentBpService.getAvailableParts(itemTemp.PartsId,0,itemTemp.Qty).then(function(res) {
                            var tmpParts = res.data.Result[0];
                            console.log('getAvailablePartsServiceManualFromEstimasiBPViewEstimasi | getAvailableParts ===> ',tmpParts)
                            if (tmpParts !== null) {
                                // console.log("have data RetailPrice",item,tmpRes);
                                // item[x].JobParts[y].RetailPrice = tmpParts.RetailPrice;
                                item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                                item[x].JobParts[y].minimalDp = tmpParts.PriceDP;
                                item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                item[x].JobParts[y].nightmareIjal = item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty;

                                item[x].JobParts[y].DiskonForDisplay = item[x].JobParts[y].Discount + '%';
                                item[x].JobParts[y].subTotalForDisplay = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice - ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice)* item[x].JobParts[y].Discount /100);


                                // item.DiscountedPrice =  (normalPrice * item.Discount)/100;
                                if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                    item[x].JobParts[y].typeDiskon = -1;
                                    item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                    item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
                                    item[x].JobParts[y].typeDiskon = 0;
                                }
                                if (tmpParts.isAvailable == 0) {
                                    item[x].JobParts[y].Availbility = "Tidak Tersedia";
                                    if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                        item[x].JobParts[y].ETA = "";
                                    } else {
                                        item[x].JobParts[y].ETA = tmpParts.DefaultETA;
                                    }
                                    
                                    if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                        item[x].JobParts[y].OrderType = 6;
                                    }
                                    else {
                                        item[x].JobParts[y].OrderType = 3;
                                    }        
                                }else if (tmpParts.isAvailable == 1){
                                    item[x].JobParts[y].Availbility = "Tersedia";
                                }else if (tmpParts.isAvailable == 2){
                                    item[x].JobParts[y].Availbility = "Tersedia Sebagian";
                                    if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                        item[x].JobParts[y].OrderType = 6;
                                    }
                                }
                            } else {
                                console.log("haven't data RetailPrice");
                                item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                                item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                                item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                                item[x].JobParts[y].nightmareIjal = Math.round(item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty);

                                item[x].JobParts[y].DiskonForDisplay = item[x].JobParts[y].Discount + '%';
                                item[x].JobParts[y].subTotalForDisplay = item[x].JobParts[y].Qty * item[x].JobParts[y].Price - ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price)* item[x].JobParts[y].Discount /100);
                            }
                            if (item[x].JobParts[y].Part !== null && item[x].JobParts[y].Part !== undefined) {
                                item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                                delete item[x].JobParts[y].Part;
                            }
                            if (itemTemp.isDeleted == 0){
                                PartsViewEstimasi.push(item[x].JobParts[y]);
                                $scope.sumAllPriceViewEstimasi();
                            }
                            
                            // return item;
    
                            if (x <= (item.length - 1)) {
                                console.log('kondisi 2');
                                if (y >= (item[x].JobParts.length - 1)) {
                                    if (item[x].Fare !== undefined) {
                                        var sum = item[x].Fare;
                                        // var summ = item[x].FlatRate;
                                        // var summ = item[x].FlatRate ? item[x].FlatRate : 1;
                                        var summ = (item[x].FlatRate == null || item[x].FlatRate == undefined) ? 1 : item[x].FlatRate

                                        var summaryy = sum * summ;
                                        var discountedPrice = (summaryy * item[x].Discount) / 100;
                                        var normalPrice = summaryy
                                        summaryy = parseInt(sum);
                                    }
                                    if (item[x].JobType == null) {
                                        item[x].JobType = { Name: "" };
                                    }
                                    if (item[x].PaidBy == null) {
                                        item[x].PaidBy = { Name: "" }
                                    }                     
                                    if (item[x].Satuan == null) {
                                        item[x].Satuan = { Name: "" }
                                    }
                                    if (item[x].AdditionalTaskId == null) {
                                        item[x].isOpe = 0;
                                    } else {
                                        item[x].isOpe = 1;
                                    }
                                    if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                        item[x].typeDiskon = -1;
                                        item[x].DiscountedPrice = normalPrice;
                                    } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                        item[x].typeDiskon = 0;
                                        item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                    }
                                    gridTempViewEstimasi.push({
                                        Discount: item[x].Discount,
                                        ActualRate: item[x].ActualRate,
                                        JobTaskId: item[x].JobTaskId,
                                        JobTypeId: item[x].JobTypeId,
                                        BodyEstimationMinute: item[x].BodyEstimationMinute,
                                        FIEstimationMinute: item[x].FIEstimationMinute,
                                        PolishingEstimationMinute: item[x].PolishingEstimationMinute,
                                        PutyEstimationMinute: item[x].PutyEstimationMinute,
                                        ReassemblyEstimationMinute: item[x].ReassemblyEstimationMinute,
                                        SprayingEstimationMinute: item[x].SprayingEstimationMinute,
                                        SurfacerEstimationMinute: item[x].SurfacerEstimationMinute,
                                        // catName: item[x].JobType.Name,
                                        Fare: item[x].Fare == null ? item[x].TaskListBP.ServiceRate : item[x].Fare,
                                        FareMask: item[x].Fare == null ? item[x].TaskListBP.ServiceRate.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',') : item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                        // FareMask:item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                        Summary: summaryy,
                                        // PaidBy: item[x].PaidBy.Name, //JANCOKK nih bikin Pembayaran Task Ga masuk
                                        PaidBy: item[x].PaidBy,
                                        PaidById: item[x].PaidById,
                                        JobId: item[x].JobId,
                                        TaskId: item[x].JobTaskId,
                                        TaskName: item[x].TaskName,
                                        FlatRate: item[x].FlatRate,
                                        ProcessId: item[x].ProcessId,
                                        TFirst1: item[x].TFirst1,
                                        // TaskBPId: x,
                                        // TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : x,
                                        // UnitBy: item[x].Satuan.Name,
                                        UnitBy: $scope.checkSatuan(item[x].ProcessId),
                                        child: PartsViewEstimasi,
                                        index: "$$" + x,
                                        TaskListBPId: item[x].TaskListBP ? item[x].TaskListBP.TaskListBPId : null,
                                        TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : (x+1) * -1,
    
                                    });
                                    // dataJobTaskPlan.push({
                                    //   TaskBPId: item[x].JobTaskId,
                                    //   TaskName: item[x].TaskName,
                                    //   BodyEstimationMinute:item[x].BodyEstimationMinute,
                                    //   FIEstimationMinute:item[x].FIEstimationMinute,
                                    //   PolishingEstimationMinute:item[x].PolishingEstimationMinute,
                                    //   PutyEstimationMinute:item[x].PutyEstimationMinute,
                                    //   ReassemblyEstimationMinute:item[x].ReassemblyEstimationMinute,
                                    //   SprayingEstimationMinute:item[x].SprayingEstimationMinute,
                                    //   SurfacerEstimationMinute:item[x].SurfacerEstimationMinute
                                    // });
                                    // $scope.gridEstimation.data = dataJobTaskPlan;
                                    // $scope.calculateforwork();
                                    $scope.sumAllPriceViewEstimasi();
                                    PartsViewEstimasi.splice();
                                    PartsViewEstimasi = [];
                                    console.log("if 1 nilai x", x);
                                    $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(item, x + 1, 0);
    
                                } else {
                                    console.log('kondisi 3');
                                    $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(item, x, y + 1);
                                }
                            }
                        });

                    // }
                    
                } else if (item[x].JobParts[y] !== undefined && (item[x].JobParts[y].PartsId == undefined || item[x].JobParts[y].PartsId == null)) {
                    console.log('kondisi 4');

                    // if (item[x].JobParts[y].isDeleted == 0){
                        item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                        item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                        item[x].JobParts[y].DPRequest = item[x].JobParts[y].DPRequest * item[x].JobParts[y].Qty;
                        item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                        item[x].JobParts[y].nightmareIjal = Math.round(item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty);

                        item[x].JobParts[y].DiskonForDisplay = item[x].JobParts[y].Discount + '%';
                        item[x].JobParts[y].subTotalForDisplay = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice - ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice)* item[x].JobParts[y].Discount /100);

                        tmp = item[x].JobParts[y];
                        if (item[x].JobParts[y].Part !== null && item[x].JobParts[y].Part !== undefined) {
                            item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                            delete item[x].JobParts[y].Part;
                        }
                        if (item[x].JobParts[y].isDeleted == 0){
                            PartsViewEstimasi.push(item[x].JobParts[y]);
                            $scope.sumAllPriceViewEstimasi();
                        }

                        if (x <= (item.length - 1)) {
                            console.log('kondisi 5');
                            if (y >= (item[x].JobParts.length - 1)) {
                                if (item[x].Fare !== undefined) {
                                    var sum = item[x].Fare;
                                    // var summ = item[x].FlatRate;
                                    // var summ = item[x].FlatRate ? item[x].FlatRate : 1;
                                    var summ = (item[x].FlatRate == null || item[x].FlatRate == undefined) ? 1 : item[x].FlatRate

                                    var summaryy = sum * summ;
                                    var discountedPrice = (summaryy * item[x].Discount) / 100;
                                    var normalPrice = summaryy
                                    summaryy = parseInt(sum);
                                }
                                if (item[x].JobType == null) {
                                    item[x].JobType = { Name: "" };
                                }
                                if (item[x].PaidBy == null) {
                                    item[x].PaidBy = { Name: "" }
                                }
                                if (item[x].Satuan == null) {
                                    item[x].Satuan = { Name: "" }
                                }
                                if (item[x].AdditionalTaskId == null) {
                                    item[x].isOpe = 0;
                                } else {
                                    item[x].isOpe = 1;
                                }
                                if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                    item[x].typeDiskon = -1;
                                    item[x].DiscountedPrice = normalPrice;
                                } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                    item[x].typeDiskon = 0;
                                    item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                }
                                gridTempViewEstimasi.push({
                                    Discount: item[x].Discount,
                                    ActualRate: item[x].ActualRate,
                                    JobTaskId: item[x].JobTaskId,
                                    JobTypeId: item[x].JobTypeId,
                                    BodyEstimationMinute: item[x].BodyEstimationMinute,
                                    FIEstimationMinute: item[x].FIEstimationMinute,
                                    PolishingEstimationMinute: item[x].PolishingEstimationMinute,
                                    PutyEstimationMinute: item[x].PutyEstimationMinute,
                                    ReassemblyEstimationMinute: item[x].ReassemblyEstimationMinute,
                                    SprayingEstimationMinute: item[x].SprayingEstimationMinute,
                                    SurfacerEstimationMinute: item[x].SurfacerEstimationMinute,
                                    // catName: item[x].JobType.Name,
                                    // Fare: item[x].Fare,
                                    // FareMask:item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                    Fare: item[x].Fare == null ? item[x].TaskListBP.ServiceRate : item[x].Fare,
                                    FareMask: item[x].Fare == null ? item[x].TaskListBP.ServiceRate.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',') : item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                    Summary: summaryy,
                                    PaidBy: item[x].PaidBy,
                                    PaidById: item[x].PaidById,

                                    JobId: item[x].JobId,
                                    TaskId: item[x].JobTaskId,
                                    TaskName: item[x].TaskName,
                                    FlatRate: item[x].FlatRate,
                                    ProcessId: item[x].ProcessId,
                                    TFirst1: item[x].TFirst1,
                                    // TaskBPId: x,
                                    // TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : x,
                                    // UnitBy: item[x].Satuan.Name,
                                    UnitBy: $scope.checkSatuan(item[x].ProcessId),
                                    // PaidBy: tmpJob[i].PaidBy.Name,
                                    child: PartsViewEstimasi,
                                    index: "$$" + x,
                                    TaskListBPId: item[x].TaskListBP ? item[x].TaskListBP.TaskListBPId : null,
                                    TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : (x+1) * -1,

                                });
                                // dataJobTaskPlan.push({
                                //       TaskBPId: item[x].JobTaskId,
                                //       TaskName: item[x].TaskName,
                                //       BodyEstimationMinute:item[x].BodyEstimationMinute,
                                //       FIEstimationMinute:item[x].FIEstimationMinute,
                                //       PolishingEstimationMinute:item[x].PolishingEstimationMinute,
                                //       PutyEstimationMinute:item[x].PutyEstimationMinute,
                                //       ReassemblyEstimationMinute:item[x].ReassemblyEstimationMinute,
                                //       SprayingEstimationMinute:item[x].SprayingEstimationMinute,
                                //       SurfacerEstimationMinute:item[x].SurfacerEstimationMinute
                                // });
                                // $scope.gridEstimation.data = dataJobTaskPlan;
                                // $scope.calculateforwork();
                                $scope.sumAllPriceViewEstimasi();
                                PartsViewEstimasi.splice();
                                PartsViewEstimasi = [];
                                $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(item, x + 1, 0);
                                console.log("if 2 nilai x", x);
                            } else {
                                console.log('kondisi 6');
                                $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(item, x, y + 1);
                            }
                        }

                    // }
                    
                } else if (item[x].JobParts.length == 0) {
                    if (x <= (item.length - 1)) {
                        console.log('kondisi 7');
                        if (y >= (item[x].JobParts.length - 1)) {
                            if (item[x].Fare !== undefined) {
                                var sum = item[x].Fare;
                                // var summ = item[x].FlatRate;
                                // var summ = item[x].FlatRate ? item[x].FlatRate : 1;
                                var summ = (item[x].FlatRate == null || item[x].FlatRate == undefined) ? 1 : item[x].FlatRate

                                var summaryy = sum * summ;
                                var discountedPrice = (summaryy * item[x].Discount) / 100;
                                var normalPrice = summaryy;
                                summaryy = parseInt(sum);
                            };
                            if (item[x].JobType == null) {
                                item[x].JobType = { Name: "" };
                            };
                            if (item[x].PaidBy == null) {
                                item[x].PaidBy = { Name: "" };
                            };
                            if (item[x].Satuan == null) {
                                item[x].Satuan = { Name: "" };
                            };
                            if (item[x].AdditionalTaskId == null) {
                                item[x].isOpe = 0;
                            } else {
                                item[x].isOpe = 1;
                            };
                            if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                item[x].typeDiskon = -1;
                                item[x].DiscountedPrice = normalPrice;
                            } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                item[x].typeDiskon = 0;
                                item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                            };
                            gridTempViewEstimasi.push({
                                Discount: item[x].Discount,
                                ActualRate: item[x].ActualRate,
                                JobTaskId: item[x].JobTaskId,
                                JobTypeId: item[x].JobTypeId,
                                BodyEstimationMinute: item[x].BodyEstimationMinute,
                                FIEstimationMinute: item[x].FIEstimationMinute,
                                PolishingEstimationMinute: item[x].PolishingEstimationMinute,
                                PutyEstimationMinute: item[x].PutyEstimationMinute,
                                ReassemblyEstimationMinute: item[x].ReassemblyEstimationMinute,
                                SprayingEstimationMinute: item[x].SprayingEstimationMinute,
                                SurfacerEstimationMinute: item[x].SurfacerEstimationMinute,
                                // catName: item[x].JobType.Name,
                                // Fare: item[x].Fare,
                                // FareMask:item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                Fare: item[x].Fare == null ? item[x].TaskListBP.ServiceRate : item[x].Fare,
                                FareMask: item[x].Fare == null ? item[x].TaskListBP.ServiceRate.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',') : item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                Summary: summaryy,
                                PaidBy: item[x].PaidBy,
                                PaidById: item[x].PaidById,

                                JobId: item[x].JobId,
                                TaskId: item[x].JobTaskId,
                                TaskName: item[x].TaskName,
                                FlatRate: item[x].FlatRate,
                                ProcessId: item[x].ProcessId,
                                TFirst1: item[x].TFirst1,
                                // TaskBPId: x,
                                // TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : x,
                                // UnitBy: item[x].Satuan.Name
                                UnitBy: $scope.checkSatuan(item[x].ProcessId),
                                // PaidBy: tmpJob[i].PaidBy.Name,
                                child: PartsViewEstimasi,
                                index: "$$" + x,
                                TaskListBPId: item[x].TaskListBP ? item[x].TaskListBP.TaskListBPId : null,
                                TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : (x+1) * -1,

                            });
                            // dataJobTaskPlan.push({
                            //       TaskBPId: item[x].JobTaskId,
                            //       TaskName: item[x].TaskName,
                            //       BodyEstimationMinute:item[x].BodyEstimationMinute,
                            //       FIEstimationMinute:item[x].FIEstimationMinute,
                            //       PolishingEstimationMinute:item[x].PolishingEstimationMinute,
                            //       PutyEstimationMinute:item[x].PutyEstimationMinute,
                            //       ReassemblyEstimationMinute:item[x].ReassemblyEstimationMinute,
                            //       SprayingEstimationMinute:item[x].SprayingEstimationMinute,
                            //       SurfacerEstimationMinute:item[x].SurfacerEstimationMinute
                            // });
                            // $scope.gridEstimation.data = dataJobTaskPlan;
                            // $scope.calculateforwork();
                            $scope.sumAllPriceViewEstimasi();
                            PartsViewEstimasi.splice();
                            PartsViewEstimasi = [];
                            $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(item, x + 1, 0);
                        } else {
                            console.log('kondisi 8');
                            $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(item, x, y + 1);
                        }
                    }
                }

                if (x > item.length - 1) {
                    return item;
                }
            } else {
                console.log('kondisi 9');
                $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(item, x + 1, 0);
            }
        };


        $scope.getAvailablePartsServiceManualFromEstimasiBP = function(item, x, y) {
            console.log('getAvailablePartsServiceManualFromEstimasiBP banyak param| item ===>',item)
            // console.log("getAvailablePartsServiceManualFromEstimasiBPFromEstimasiBP  ===>", JSON.stringify(item));            
            console.log("x : ", x);
            console.log("y : ", y);

            if (x > item.length - 1) {
                return item;
            }
            if (item[x].isDeleted !== 1) {
                console.log('kondisi 1');
                console.log('item[x].JobParts[y] ===>',item[x].JobParts[y]);
                console.log('item[0].JobParts[0] ===>',item[0].JobParts[0]);
                console.log('x ===>',x);
                console.log('y ===>',y);
                
                // console.log('item[x].JobParts[y].PartsId ===>',item[x].JobParts[y].PartsId);
                if (item[x].JobParts[y] !== undefined && item[x].JobParts[y].PartsId !== null) {
                    console.log('kondisi 1.1');
                    var itemTemp = item[x].JobParts[y];
                    // if (itemTemp.isDeleted == 0){
                        AppointmentBpService.getAvailableParts(itemTemp.PartsId, 0, itemTemp.Qty).then(function(res) {
                            var tmpParts = res.data.Result[0];
                            console.log('getAvailablePartsServiceManualFromEstimasiBP | getAvailableParts ===> ',tmpParts)
                            if (tmpParts !== null) {
                                // console.log("have data RetailPrice",item,tmpRes);
                                // item[x].JobParts[y].RetailPrice = tmpParts.RetailPrice;
                                item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                                item[x].JobParts[y].minimalDp = tmpParts.PriceDP;
                                item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                item[x].JobParts[y].nightmareIjal = item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty;

                                item[x].JobParts[y].DiskonForDisplay = item[x].JobParts[y].Discount + '%';
                                item[x].JobParts[y].subTotalForDisplay = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice - ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice)* item[x].JobParts[y].Discount /100);


                                // item.DiscountedPrice =  (normalPrice * item.Discount)/100;
                                if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                    item[x].JobParts[y].typeDiskon = -1;
                                    item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                    item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
                                    item[x].JobParts[y].typeDiskon = 0;
                                }
                                if (tmpParts.isAvailable == 0) {
                                    item[x].JobParts[y].Availbility = "Tidak Tersedia";
                                    if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                        item[x].JobParts[y].ETA = "";
                                    } else {
                                        item[x].JobParts[y].ETA = tmpParts.DefaultETA;
                                    }
                                    
                                    if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                        item[x].JobParts[y].OrderType = 6;
                                    }
                                    else {
                                        item[x].JobParts[y].OrderType = 3;
                                    }
                                }else if (tmpParts.isAvailable == 1){
                                    item[x].JobParts[y].Availbility = "Tersedia";
                                    //kl tersedia dp nya 0 in dan disable
                                    // item[x].JobParts[y].DownPayment = 0;
                                }else if (tmpParts.isAvailable == 2){
                                    item[x].JobParts[y].Availbility = "Tersedia Sebagian";
                                    //kl tersedia dp nya 0 in dan disable
                                    // item[x].JobParts[y].DownPayment = 0;
                                    if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                        item[x].JobParts[y].OrderType = 6;
                                    }
                                }
                            } else {
                                console.log("haven't data RetailPrice");
                                item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                                item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                                item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                                item[x].JobParts[y].nightmareIjal = Math.round(item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty);

                                item[x].JobParts[y].DiskonForDisplay = item[x].JobParts[y].Discount + '%';
                                item[x].JobParts[y].subTotalForDisplay = item[x].JobParts[y].Qty * item[x].JobParts[y].Price - ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price)* item[x].JobParts[y].Discount /100);
                            }
                            if (item[x].JobParts[y].Part !== null && item[x].JobParts[y].Part !== undefined) {
                                item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                                delete item[x].JobParts[y].Part;
                            }
                            if (itemTemp.isDeleted == 0){
                                Parts.push(item[x].JobParts[y]);
                                $scope.arrayDP(gridTemp, $scope.jejeranDP,flagArrayDP);
                                $scope.sumAllPrice();
                            }
                            
                            // return item;
    
                            if (x <= (item.length - 1)) {
                                console.log('kondisi 2');
                                if (y >= (item[x].JobParts.length - 1)) {
                                    if (item[x].Fare !== undefined) {
                                        var sum = item[x].Fare;
                                        // var summ = item[x].FlatRate;
                                        // var summ = item[x].FlatRate ? item[x].FlatRate : 1;
                                        var summ = (item[x].FlatRate == null || item[x].FlatRate == undefined) ? 1 : item[x].FlatRate

                                        var summaryy = sum * summ;
                                        var discountedPrice = (summaryy * item[x].Discount) / 100;
                                        var normalPrice = summaryy
                                        summaryy = parseInt(sum);
                                    }
                                    if (item[x].JobType == null) {
                                        item[x].JobType = { Name: "" };
                                    }
                                    if (item[x].PaidBy == null) {
                                        item[x].PaidBy = { Name: "" }
                                    }                     
                                    if (item[x].Satuan == null) {
                                        item[x].Satuan = { Name: "" }
                                    }
                                    if (item[x].AdditionalTaskId == null) {
                                        item[x].isOpe = 0;
                                    } else {
                                        item[x].isOpe = 1;
                                    }
                                    if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                        item[x].typeDiskon = -1;
                                        item[x].DiscountedPrice = normalPrice;
                                    } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                        item[x].typeDiskon = 0;
                                        item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                    }
                                    gridTemp.push({
                                        EstimationBPId : typeof item[x].EstimationBPId == undefined ? 0 : item[x].EstimationBPId,
                                        ActualRate: item[x].ActualRate,
                                        JobTaskId: item[x].JobTaskId,
                                        JobTypeId: item[x].JobTypeId,
                                        BodyEstimationMinute: item[x].BodyEstimationMinute,
                                        FIEstimationMinute: item[x].FIEstimationMinute,
                                        PolishingEstimationMinute: item[x].PolishingEstimationMinute,
                                        PutyEstimationMinute: item[x].PutyEstimationMinute,
                                        ReassemblyEstimationMinute: item[x].ReassemblyEstimationMinute,
                                        SprayingEstimationMinute: item[x].SprayingEstimationMinute,
                                        SurfacerEstimationMinute: item[x].SurfacerEstimationMinute,
                                        // catName: item[x].JobType.Name,
                                        Fare: item[x].Fare == null ? item[x].TaskListBP.ServiceRate : item[x].Fare,
                                        FareMask: item[x].Fare == null ? item[x].TaskListBP.ServiceRate.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',') : item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                        // FareMask:item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                        Summary: summaryy,
                                        // PaidBy: item[x].PaidBy.Name, //JANCOKK nih bikin Pembayaran Task Ga masuk
                                        PaidBy: item[x].PaidBy,
                                        Discount:item[x].Discount, 
                                        PaidById: item[x].PaidById,
                                        JobId: item[x].JobId,
                                        TaskId: item[x].JobTaskId,
                                        TaskName: item[x].TaskName,
                                        FlatRate: item[x].FlatRate,
                                        ProcessId: item[x].ProcessId,
                                        TFirst1: item[x].TFirst1,
                                        // TaskBPId: x,
                                        // TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : x,
                                        // UnitBy: item[x].Satuan.Name,
                                        UnitBy: $scope.checkSatuan(item[x].ProcessId),
                                        child: Parts,
                                        index: "$$" + x,
                                        TaskListBPId: item[x].TaskListBP ? item[x].TaskListBP.TaskListBPId : null,
                                        TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : (x+1) * -1,
    
                                    });
                                    // dataJobTaskPlan.push({
                                    //   TaskBPId: item[x].JobTaskId,
                                    //   TaskName: item[x].TaskName,
                                    //   BodyEstimationMinute:item[x].BodyEstimationMinute,
                                    //   FIEstimationMinute:item[x].FIEstimationMinute,
                                    //   PolishingEstimationMinute:item[x].PolishingEstimationMinute,
                                    //   PutyEstimationMinute:item[x].PutyEstimationMinute,
                                    //   ReassemblyEstimationMinute:item[x].ReassemblyEstimationMinute,
                                    //   SprayingEstimationMinute:item[x].SprayingEstimationMinute,
                                    //   SurfacerEstimationMinute:item[x].SurfacerEstimationMinute
                                    // });
                                    // $scope.gridEstimation.data = dataJobTaskPlan;
                                    // $scope.calculateforwork();
                                    $scope.arrayDP(gridTemp, $scope.jejeranDP,flagArrayDP);
                                    $scope.sumAllPrice();
                                    Parts.splice();
                                    Parts = [];
                                    console.log("if 1 nilai x", x);
                                    $scope.getAvailablePartsServiceManualFromEstimasiBP(item, x + 1, 0);
    
                                } else {
                                    console.log('kondisi 3');
                                    $scope.getAvailablePartsServiceManualFromEstimasiBP(item, x, y + 1);
                                }
                            }
                        });

                    // }
                    
                } else if (item[x].JobParts[y] !== undefined && (item[x].JobParts[y].PartsId == undefined || item[x].JobParts[y].PartsId == null)) {
                    console.log('kondisi 4');

                    // if (item[x].JobParts[y].isDeleted == 0){
                        item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                        item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                        item[x].JobParts[y].DPRequest = item[x].JobParts[y].DPRequest * item[x].JobParts[y].Qty;
                        item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                        item[x].JobParts[y].nightmareIjal = Math.round(item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty);

                        item[x].JobParts[y].DiskonForDisplay = item[x].JobParts[y].Discount + '%';
                        item[x].JobParts[y].subTotalForDisplay = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice - ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice)* item[x].JobParts[y].Discount /100);

                        tmp = item[x].JobParts[y];
                        if (item[x].JobParts[y].Part !== null && item[x].JobParts[y].Part !== undefined) {
                            item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                            delete item[x].JobParts[y].Part;
                        }
                        if (item[x].JobParts[y].isDeleted == 0){
                            Parts.push(item[x].JobParts[y]);
                            $scope.arrayDP(gridTemp, $scope.jejeranDP,flagArrayDP);
                            $scope.sumAllPrice();
                        }

                        if (x <= (item.length - 1)) {
                            console.log('kondisi 5');
                            if (y >= (item[x].JobParts.length - 1)) {
                                if (item[x].Fare !== undefined) {
                                    var sum = item[x].Fare;
                                    // var summ = item[x].FlatRate;
                                    // var summ = item[x].FlatRate ? item[x].FlatRate : 1;
                                    var summ = (item[x].FlatRate == null || item[x].FlatRate == undefined) ? 1 : item[x].FlatRate

                                    var summaryy = sum * summ;
                                    var discountedPrice = (summaryy * item[x].Discount) / 100;
                                    var normalPrice = summaryy
                                    summaryy = parseInt(sum);
                                }
                                if (item[x].JobType == null) {
                                    item[x].JobType = { Name: "" };
                                }
                                if (item[x].PaidBy == null) {
                                    item[x].PaidBy = { Name: "" }
                                }
                                if (item[x].Satuan == null) {
                                    item[x].Satuan = { Name: "" }
                                }
                                if (item[x].AdditionalTaskId == null) {
                                    item[x].isOpe = 0;
                                } else {
                                    item[x].isOpe = 1;
                                }
                                if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                    item[x].typeDiskon = -1;
                                    item[x].DiscountedPrice = normalPrice;
                                } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                    item[x].typeDiskon = 0;
                                    item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                }
                                gridTemp.push({
                                    EstimationBPId : typeof item[x].EstimationBPId == undefined ? 0 : item[x].EstimationBPId,
                                    ActualRate: item[x].ActualRate,
                                    JobTaskId: item[x].JobTaskId,
                                    JobTypeId: item[x].JobTypeId,
                                    BodyEstimationMinute: item[x].BodyEstimationMinute,
                                    FIEstimationMinute: item[x].FIEstimationMinute,
                                    PolishingEstimationMinute: item[x].PolishingEstimationMinute,
                                    PutyEstimationMinute: item[x].PutyEstimationMinute,
                                    ReassemblyEstimationMinute: item[x].ReassemblyEstimationMinute,
                                    SprayingEstimationMinute: item[x].SprayingEstimationMinute,
                                    SurfacerEstimationMinute: item[x].SurfacerEstimationMinute,
                                    // catName: item[x].JobType.Name,
                                    // Fare: item[x].Fare,
                                    // FareMask:item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                    Fare: item[x].Fare == null ? item[x].TaskListBP.ServiceRate : item[x].Fare,
                                    FareMask: item[x].Fare == null ? item[x].TaskListBP.ServiceRate.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',') : item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                    Summary: summaryy,
                                    PaidBy: item[x].PaidBy,
                                    // PaidBy: item[x].PaidBy.Name,
                                    PaidById: item[x].PaidById,
                                    Discount:item[x].Discount,
                                    JobId: item[x].JobId,
                                    TaskId: item[x].JobTaskId,
                                    TaskName: item[x].TaskName,
                                    FlatRate: item[x].FlatRate,
                                    ProcessId: item[x].ProcessId,
                                    TFirst1: item[x].TFirst1,
                                    // TaskBPId: x,
                                    // TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : x,
                                    // UnitBy: item[x].Satuan.Name,
                                    UnitBy: $scope.checkSatuan(item[x].ProcessId),
                                    // PaidBy: tmpJob[i].PaidBy.Name,
                                    child: Parts,
                                    index: "$$" + x,
                                    TaskListBPId: item[x].TaskListBP ? item[x].TaskListBP.TaskListBPId : null,
                                    TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : (x+1) * -1,

                                });
                                // dataJobTaskPlan.push({
                                //       TaskBPId: item[x].JobTaskId,
                                //       TaskName: item[x].TaskName,
                                //       BodyEstimationMinute:item[x].BodyEstimationMinute,
                                //       FIEstimationMinute:item[x].FIEstimationMinute,
                                //       PolishingEstimationMinute:item[x].PolishingEstimationMinute,
                                //       PutyEstimationMinute:item[x].PutyEstimationMinute,
                                //       ReassemblyEstimationMinute:item[x].ReassemblyEstimationMinute,
                                //       SprayingEstimationMinute:item[x].SprayingEstimationMinute,
                                //       SurfacerEstimationMinute:item[x].SurfacerEstimationMinute
                                // });
                                // $scope.gridEstimation.data = dataJobTaskPlan;
                                // $scope.calculateforwork();
                                $scope.arrayDP(gridTemp, $scope.jejeranDP,flagArrayDP);
                                $scope.sumAllPrice();
                                Parts.splice();
                                Parts = [];
                                $scope.getAvailablePartsServiceManualFromEstimasiBP(item, x + 1, 0);
                                console.log("if 2 nilai x", x);
                            } else {
                                console.log('kondisi 6');
                                $scope.getAvailablePartsServiceManualFromEstimasiBP(item, x, y + 1);
                            }
                        }

                    // }
                    
                } else if (item[x].JobParts.length == 0) {
                    if (x <= (item.length - 1)) {
                        console.log('kondisi 7');
                        if (y >= (item[x].JobParts.length - 1)) {
                            if (item[x].Fare !== undefined) {
                                var sum = item[x].Fare;
                                // var summ = item[x].FlatRate;
                                // var summ = item[x].FlatRate ? item[x].FlatRate : 1;
                                var summ = (item[x].FlatRate == null || item[x].FlatRate == undefined) ? 1 : item[x].FlatRate

                                var summaryy = sum * summ;
                                var discountedPrice = (summaryy * item[x].Discount) / 100;
                                var normalPrice = summaryy;
                                summaryy = parseInt(sum);
                            };
                            if (item[x].JobType == null) {
                                item[x].JobType = { Name: "" };
                            };
                            if (item[x].PaidBy == null) {
                                item[x].PaidBy = { Name: "" };
                            };
                            if (item[x].Satuan == null) {
                                item[x].Satuan = { Name: "" };
                            };
                            if (item[x].AdditionalTaskId == null) {
                                item[x].isOpe = 0;
                            } else {
                                item[x].isOpe = 1;
                            };
                            if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                item[x].typeDiskon = -1;
                                item[x].DiscountedPrice = normalPrice;
                            } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                item[x].typeDiskon = 0;
                                item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                            };
                            gridTemp.push({
                                EstimationBPId : typeof item[x].EstimationBPId == undefined ? 0 : item[x].EstimationBPId,
                                ActualRate: item[x].ActualRate,
                                JobTaskId: item[x].JobTaskId,
                                JobTypeId: item[x].JobTypeId,
                                BodyEstimationMinute: item[x].BodyEstimationMinute,
                                FIEstimationMinute: item[x].FIEstimationMinute,
                                PolishingEstimationMinute: item[x].PolishingEstimationMinute,
                                PutyEstimationMinute: item[x].PutyEstimationMinute,
                                ReassemblyEstimationMinute: item[x].ReassemblyEstimationMinute,
                                SprayingEstimationMinute: item[x].SprayingEstimationMinute,
                                SurfacerEstimationMinute: item[x].SurfacerEstimationMinute,
                                // catName: item[x].JobType.Name,
                                // Fare: item[x].Fare,
                                // FareMask:item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                Fare: item[x].Fare == null ? item[x].TaskListBP.ServiceRate : item[x].Fare,
                                FareMask: item[x].Fare == null ? item[x].TaskListBP.ServiceRate.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',') : item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                Summary: summaryy,
                                PaidBy: item[x].PaidBy,
                                // PaidBy: item[x].PaidBy.Name,
                                PaidById: item[x].PaidById,
                                Discount:item[x].Discount,

                                JobId: item[x].JobId,
                                TaskId: item[x].JobTaskId,
                                TaskName: item[x].TaskName,
                                FlatRate: item[x].FlatRate,
                                ProcessId: item[x].ProcessId,
                                TFirst1: item[x].TFirst1,
                                // TaskBPId: x,
                                // TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : x,
                                // UnitBy: item[x].Satuan.Name
                                UnitBy: $scope.checkSatuan(item[x].ProcessId),
                                // PaidBy: tmpJob[i].PaidBy.Name,
                                child: Parts,
                                index: "$$" + x,
                                TaskListBPId: item[x].TaskListBP ? item[x].TaskListBP.TaskListBPId : null,
                                TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : (x+1) * -1,

                            });
                            // dataJobTaskPlan.push({
                            //       TaskBPId: item[x].JobTaskId,
                            //       TaskName: item[x].TaskName,
                            //       BodyEstimationMinute:item[x].BodyEstimationMinute,
                            //       FIEstimationMinute:item[x].FIEstimationMinute,
                            //       PolishingEstimationMinute:item[x].PolishingEstimationMinute,
                            //       PutyEstimationMinute:item[x].PutyEstimationMinute,
                            //       ReassemblyEstimationMinute:item[x].ReassemblyEstimationMinute,
                            //       SprayingEstimationMinute:item[x].SprayingEstimationMinute,
                            //       SurfacerEstimationMinute:item[x].SurfacerEstimationMinute
                            // });
                            // $scope.gridEstimation.data = dataJobTaskPlan;
                            // $scope.calculateforwork();
                            $scope.arrayDP(gridTemp, $scope.jejeranDP,flagArrayDP);
                            $scope.sumAllPrice();
                            Parts.splice();
                            Parts = [];
                            $scope.getAvailablePartsServiceManualFromEstimasiBP(item, x + 1, 0);
                        } else {
                            console.log('kondisi 8');
                            $scope.getAvailablePartsServiceManualFromEstimasiBP(item, x, y + 1);
                        }
                    }
                }

                if (x > item.length - 1) {
                    return item;
                }
            } else {
                console.log('kondisi 9');
                $scope.getAvailablePartsServiceManualFromEstimasiBP(item, x + 1, 0);
            }
        };

        $scope.CheckedVIN = function(){
            if ($scope.mDataDetail.VIN != null || $scope.mDataDetail.VIN != "") {
                var count = $scope.mDataDetail.VIN;
                console.log('mdata vin', count.length)
                    if (count.length <= 17) {
                        AppointmentBpService.getDataVIN($scope.mDataDetail.VIN).then(function(res){
                            console.log('cek input rangka', $scope.mDataDetail.VIN)
                            $scope.tempVIN = res.data.Result[0]
                            console.log('cek res rangka', $scope.tempVIN)
                            $scope.tempVIN = res.data.Result[0];
                            if ($scope.tempVIN.DEC_Date != null && $scope.tempVIN.DEC_Date != undefined) {
                                $scope.tempVIN.DEC_Date = $filter('date')($scope.tempVIN.DEC_Date, 'dd/MM/yyyy')
                            }else{
                                $scope.tempVIN.DEC_Date = '-'
                            }
                        console.log('cek res input', $scope.tempVIN)
                                console.log('cek res input', $scope.tempVIN)
                                console.log('$scope.taskCategory ', $scope.taskCategory )
                        })

                        AppointmentBpService.listHistoryClaim($scope.mDataDetail.VIN).then(function(res){
                            // res.data.data = $scope.ubahFormatTanggalIndo(res.data.data.historyClaimDealerList)
                            $scope.listClaim = res.data.data;
                            $scope.tempListClaim = [];
                            // $scope.historyDate = new Date($scope.tempListClaim.claimDate)
                            console.log('cek list claim', res)
                            console.log('list claim', res.data.data)
                            console.log('list history claim', $scope.tempListClaim)
                            for (var i = 0; i < $scope.listClaim.historyClaimDealerList.length; i++) {
                                $scope.tempListClaim.push($scope.listClaim.historyClaimDealerList[i]);
                            }
                        })

                        AppointmentBpService.getTowass($scope.mDataDetail.VIN).then(function(res) {
                            console.log("data Towasssssss", res.data);
                            $scope.dataTowass = res.data.Result;
                        });

                    }else{
                        console.log('tanpa service')
                        $scope.taskCategory = $scope.notFlcPlcFpc
                    }
            }
        }

        $scope.checkSatuan = function(id) {
            console.log("ideeeeeeeee", id, $scope.unitData.length);
            for (var i = 0; i < $scope.unitData.length; i++) {
                if ($scope.unitData[i].MasterId == id) {
                    return $scope.unitData[i].Name;
                }
            }
        }

        $scope.ubahPartsCode = function(data){
            console.log('ubur ubur',data)
            // kata pa ari kl part yang ga pake partscode (manual), ga blh di kasi diskon pas release wo nya
            // hrs isi dr listodo dl partscode nya, baru kasi diskon di wo list
            if (data.PartsCode != null && data.PartsCode != undefined){
                if (data.PartsCode.length < 4){
                    $scope.ldModel.DiscountTypeId = -1;
                    $scope.ldModel.DiscountTypeListId = null;
                    $scope.ldModel.Discount = 0;
                    $scope.ldModel.PartsId = null; // kl mau harga nya enable waktu dia ubah partscode

                    // ========================= hilangkan tipe diskon material jika ganti part, hrs click "Cek" dl ====================== start
                    // for (var p=0; p<$scope.diskonData.length; p++){
                    //     if ($scope.diskonData[p].MasterId == 3){
                    //         $scope.diskonData.splice(p,1)
                    //     }
                    // }
                    // ========================= hilangkan tipe diskon material jika ganti part, hrs click "Cek" dl ====================== end
                }
            }
        }

        $scope.checkNameParts = function(data, model) {
            if ($scope.ldModel.IsOPB == null && data != null) {
                $scope.ldModel.RetailPrice = 0;
            } else {
                $scope.ldModel.RetailPrice = "";
            }

            if (model.MaterialTypeId == 1 && (model.PartsCode == null || model.PartsCode == undefined || model.PartsCode == '')) {
                model.OrderType = 3
                model.Type = 3

            }

        }


        var Parts = [];
        var dataJobTaskPlan = [];
        $scope.dataForBslist = function(data) {
            console.log('dataForBslist ===>',data);
            console.log('dataForBslist ===>', JSON.stringify(data));

            gridTemp = []
            Parts.splice();
            Parts = [];


            data.JobTask = data.AEstimationBP_TaskList_TT;

            if (data.JobTask.length > 0) {
                var tmpJob = data.JobTask;

                console.log("tmpJob dataForBslist ===>", tmpJob);
                // console.log("tmpJob dataForBslist ===>", JSON.stringify(tmpJob));
           
                for (var i = 0; i < tmpJob.length; i++) {
                    console.log('Json Parse ===>',JSON.stringify(tmpJob[i]));

                    // for(var z in $scope.paymentData ){
                    //     if(tmpJob[i].PaidById == $scope.paymentData[z].MasterId)
                    //     tmpJob[i].PaidBy = $scope.paymentData[z].Name;
                    // }
                    
                    if(tmpJob[i].JobParts.length > 0){
                        for (var j = 0; j < tmpJob[i].JobParts.length; j++) {
                        

                            console.log("tmpJob[i].JobParts[j]", tmpJob[i].JobParts[j]);
                            // if (tmpJob[i].JobParts[j].PaidBy !== null) {
                            //     tmpJob[i].JobParts[j].paidName = tmpJob[i].JobParts[j].PaidBy.Name;
                            //     delete tmpJob[i].JobParts[j].PaidBy;
    
                            //     // for(var v in $scope.paymentData ){
                            //     //     if(tmpJob[i].JobParts[j].PaidById == $scope.paymentData[v].MasterId)
                            //     //     tmpJob[i].JobParts[j].PaidBy = $scope.paymentData[v].Name;
                            //     // }                   
                            // }
                            // if (tmpJob[i].JobParts[j].Satuan !== null) {
                            //     tmpJob[i].JobParts[j].satuanName = tmpJob[i].JobParts[j].Satuan.Name;
                            //     delete tmpJob[i].JobParts[j].Satuan;
                            // }
                            tmpJob[i].subTotal = tmpJob[i].Qty * tmpJob[i].RetailPrice;
                        }
                    }else{
                        console.log('ini ga ada partsyaaa ====>',tmpJob[i].JobParts);
                    }
                    
                    if (tmpJob[i].isDeleted == 0) {
                        dataJobTaskPlan.push({
                            EstimationBPId : typeof tmpJob[i].EstimationBPId == undefined ? 0 : tmpJob[i].EstimationBPId,
                            TaskBPId: tmpJob[i].TaskBPId ? tmpJob[i].TaskBPId : i,
                            TaskName: tmpJob[i].TaskName,
                            BodyEstimationMinute: tmpJob[i].BodyEstimationMinute,
                            FIEstimationMinute: tmpJob[i].FIEstimationMinute,
                            PolishingEstimationMinute: tmpJob[i].PolishingEstimationMinute,
                            PuttyEstimationMinute: tmpJob[i].PutyEstimationMinute,
                            // PutyEstimationMinute: tmpJob[i].PutyEstimationMinute, lama
                            
                            ReassemblyEstimationMinute: tmpJob[i].ReassemblyEstimationMinute,
                            SprayingEstimationMinute: tmpJob[i].SprayingEstimationMinute,
                            SurfacerEstimationMinute: tmpJob[i].SurfacerEstimationMinute,

                            Total: tmpJob[i].BodyEstimationMinute+tmpJob[i].FIEstimationMinute+tmpJob[i].PolishingEstimationMinute+tmpJob[i].PutyEstimationMinute+tmpJob[i].ReassemblyEstimationMinute+tmpJob[i].SprayingEstimationMinute+tmpJob[i].SurfacerEstimationMinute
                        });
                    }
                }
                $scope.gridEstimation.data = dataJobTaskPlan;
                // $scope.calculateforwork();
                // added by sss on 2017-11-03
                $scope.getAvailablePartsServiceManualFromEstimasiBP(tmpJob, 0, 0);
                //
                $scope.gridWork = gridTemp;
                $scope.gridOriginal = gridTemp;
            }
            $scope.sumAllPrice();
            console.log("$scope.gridWork", $scope.gridWork);
            console.log("$scope.JobRequest", $scope.JobRequest);
            console.log("$scope.JobComplaint", $scope.JobComplaint);
            console.log("$scope.gridEstimation.data", $scope.gridEstimation.data);

        };


        var PartsViewEstimasi = [];
        var dataJobTaskPlanViewEstimasi = [];
        $scope.dataForBslistViewEstimasi = function(data) {
            console.log('dataForBslistViewEstimasi ===>',data);

            gridTempViewEstimasi = []
            PartsViewEstimasi.splice();
            PartsViewEstimasi = [];

            data.JobTask = data.AEstimationBP_TaskList_TT;
            if (data.JobTask.length > 0) {
                var tmpJob = data.JobTask;
                console.log("tmpJob dataForBslistViewEstimasi ===>", tmpJob);
           
                for (var i = 0; i < tmpJob.length; i++) {
                    for (var j = 0; j < tmpJob[i].JobParts.length; j++) {
                        tmpJob[i].subTotal = tmpJob[i].Qty * tmpJob[i].RetailPrice;
                    }
                    if (tmpJob[i].isDeleted == 0) {
                        dataJobTaskPlanViewEstimasi.push({
                            TaskBPId: tmpJob[i].TaskBPId ? tmpJob[i].TaskBPId : i,
                            TaskName: tmpJob[i].TaskName,
                            BodyEstimationMinute: tmpJob[i].BodyEstimationMinute,
                            FIEstimationMinute: tmpJob[i].FIEstimationMinute,
                            PolishingEstimationMinute: tmpJob[i].PolishingEstimationMinute,
                            PuttyEstimationMinute: tmpJob[i].PutyEstimationMinute,                            
                            ReassemblyEstimationMinute: tmpJob[i].ReassemblyEstimationMinute,
                            SprayingEstimationMinute: tmpJob[i].SprayingEstimationMinute,
                            SurfacerEstimationMinute: tmpJob[i].SurfacerEstimationMinute,
                            Total: tmpJob[i].BodyEstimationMinute+tmpJob[i].FIEstimationMinute+tmpJob[i].PolishingEstimationMinute+tmpJob[i].PutyEstimationMinute+tmpJob[i].ReassemblyEstimationMinute+tmpJob[i].SprayingEstimationMinute+tmpJob[i].SurfacerEstimationMinute
                        });
                    }
                }
                $scope.gridEstimationViewEstimasi.data = dataJobTaskPlanViewEstimasi;
                $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(tmpJob, 0, 0);
                $scope.gridWorkViewEstimasi = gridTempViewEstimasi;
                $scope.gridOriginalViewEstimasi = gridTempViewEstimasi;
            }
            $scope.sumAllPriceViewEstimasi();
            console.log("$scope.gridWorkViewEstimasi", $scope.gridWorkViewEstimasi);
            console.log("$scope.JobRequest", $scope.JobRequest);
            console.log("$scope.JobComplaint", $scope.JobComplaint);
            console.log("$scope.gridEstimationViewEstimasi.data", $scope.gridEstimationViewEstimasi.data);
        };
        $scope.dissallowSemicolon = function(event){
            var patternRegex = /[\x3B]/i;
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        }

        


        // Tambahan CR 4 [_END_]
    }).filter('griddropdown', function() {
        return function(input, xthis) {
            console.log("filter..");
            console.log(input);
            console.log(xthis);
            if (xthis !== undefined) {
                if (xthis.col !== undefined) {
                    var map = xthis.col.colDef.editDropdownOptionsArray;
                    var idField = xthis.col.colDef.editDropdownIdLabel;
                    var valueField = xthis.col.colDef.editDropdownValueLabel;
                    for (var i = 0; i < map.length; i++) {
                        if (map[i][idField] == input) {
                            return map[i][valueField];
                        }
                    }
                }
            }

            return '';
        };


    }) // added by sss on 2017-10-10
    // .filter('griddropdownStat', function() {
    //     return function(input, xthis) {
    //         // console.log("filter..");
    //         // console.log(input);
    //         // console.log(xthis);
    //         if (xthis !== undefined) {
    //             if (xthis.col !== undefined) {
    //                 var map = xthis.col.colDef.editDropdownOptionsArray;
    //                 var idField = xthis.col.colDef.editDropdownIdLabel;
    //                 var valueField = xthis.col.colDef.editDropdownValueLabel;
    //                 for (var i = 0; i < map.length; i++) {
    //                     if (map[i][idField] == input) {
    //                         return map[i][valueField];
    //                     }
    //                 }
    //             }
    //         }

    //         return '';
    //     };
    // })
    .filter('griddropdownTask', function() {
        return function(input, xthis) {
            // console.log("filter..");
            // console.log(input);
            // console.log(xthis);
            if (xthis !== undefined) {
                if (xthis.col !== undefined) {
                    if (input !== null) {
                        return input.map(function(elem) {
                            return elem.TaskName;
                        }).join(", ");
                    }
                }
            }

            return '';
        };
    }).filter('Currency', function() {
        return function(x) {
            x = x.toString().replace(".", ",");
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        };
    });


    



    