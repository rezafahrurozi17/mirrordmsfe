angular.module('app')
    .factory('AppointmentBpService', function($http, CurrentUser, $q) {
        var currentUser = CurrentUser.user();
        //console.log(currentUser);
        return {

            UpdateJobAppointmentAddEstimasiId: function(jobId, arrayEstimasi) {
                return $http.put('/api/as/AEstimationBP/UpdateJobAppointment?JobId=' + jobId, arrayEstimasi);
            },
            getData: function(type, filter, param) {
                if (type == 0) {
                    if (filter.includes("*")) {
                        filter = filter.split('*');
                        filter = filter[0];
                    }
                    var res = $http.get('/api/crm/GetCustomerListFilterByLicenePlate/' + filter + '/-/0/' + param);
                    console.log("filterFactory", filter);
                } else {
                    var res = $http.get('/api/crm/GetCustomerListFilterByLicenePlate/-/' + filter + '/0/' + param);
                    console.log("filterFactory", filter);
                }

                return res;
            },
            getDataTask: function(taskName, vehicleTypeId, vehicleModelId, jobIsWarranty) {
                console.log("name", taskName, vehicleTypeId, vehicleModelId);
                var res = $http.get('/api/as/TaskListBP/' + taskName + '/' + vehicleTypeId + '/' + vehicleModelId + '/' + jobIsWarranty);
                // var res=$http.get('/api/as/TaskLists?KatashikiCode=ASV50R-JETGKD&Name='+Key+'&TaskCategory='+catg);
                // console.log('/api/as/TaskLists?KatashikiCode='+Katashiki+'&Name='+Key+'&TaskCategory='+catg);
                // console.log("resnya",res);
                return res;
            },
            getNoAppointment: function(item) {
                return $http.get('/api/as/jobs/getAppointmentNo/' + item);
            },
            getDataParts: function(Key, servicetype, type) {
                console.log("keyyyyyy", Key);
                var res = $http.get('/api/as/StockAdjustment/GetStockAdjustmentDetailFromMaterial?PartsCode=' + Key + '&ServiceTypeId=' + servicetype + '&PartsClassId1=' + type);
                // console.log("/api/as/TaskLists?KatashikiCode="+Katashiki+"&Name="+Key);
                // console.log("resnya",res);
                return res;
            },
            getInsurence: function() {
                var res = $http.get('/api/as/Insurance');
                return res
            },
            getLocation: function() {
                var res = $http.get('/api/crm/GetLocation/');

                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            getAvailableParts: function(item, jobid, qtyinput) {
                // /api/as/jobs/getAvailableParts/{partid}
                // var res = $http.get('/api/as/jobs/getPartsPriceAndDefaultETDETA/' + item);
                var res = $http.get('/api/as/jobs/getAvailablePartsAppointment/' + item + '/0?JobId='+jobid + '&Qty='+qtyinput);
                return res
            },
            getVehicleTypeById: function(id) {
                var url = '/api/crm/GetCVehicleTypeById/' + id
                var res = $http.get(url);
                return res;
            },
            getDataPartsByTaskId: function(key) {
                var res = $http.get('/api/as/TaskLists/Parts?TaskId=' + key);
                return res
            },
            getDataModel: function() {
                var res = $http.get('/api/param/VehicleModels?start=1&limit=1000');
                return res;
            },
            getDataTaskList: function(item) {
                var res = $http.get('/api/as/TaskLists/TasklistSA?KatashikiCode=' + item);
                return res
            },
            getDataTaskListByKatashiki: function(key) {
                var res = $http.get('/api/as/TaskLists/TasklistSA?KatashikiCode=' + key);
                return res
            },
            getDataLain: function() {
                var res = $http.get('/api/as/AppointmentRequests/0');
                return res;
            },
            getDataVehicle: function(key) {
                var res = $http.get('/api/crm/GetDataInfo/' + key);
                return res;
            },
            checkApproval: function(data) {
                console.log("data checkApproval", data);
                return $http.put('/api/as/CheckingApproval/', [{
                    DPRequested: data,
                    ApprovalCategoryId: 41,
                    RoleId: currentUser.RoleId
                }]);
            },
            postApproval: function(data) {
                // ApproverRoleId
                console.log("postApproval", data);
                return $http.post('/api/as/PostApprovalRequestDP/0', [{
                    JobId: data.JobId,
                    DPDefault: data.totalDpDefault,
                    DPRequested: data.totalDp,
                    ApprovalCategoryId: 41,
                    ApproverId: null,
                    RequesterId: null,
                    ApproverRoleId: data.ApproverRoleId,
                    RequestorRoleId: currentUser.RoleId,
                    RequestReason: "Request Pengurangan DP",
                    RequestDate: new Date(),
                    StatusApprovalId: 3,
                    StatusCode: 1,
                    VehicleTypeId: data.detail.VehicleTypeId
                }]);
            },
            postApprovalDp: function(data) {
                // ApproverRoleId
                console.log("postApproval", data);
                return $http.post('/api/as/PostApprovalRequestDP/0', data);
            },
            // =========================================
            // 
            // ============== Global Master =====
            getPayment: function() {
                var catId = 1008;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getUnitMeasurement: function() {
                var catId = 1;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getWoCategory: function() {
                var catId = 2039;
                // var catId = 1012;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId+'&Flag=1');
                console.log('resnya pause=>', res);
                return res;
            },

            getTaskCategory: function() {
                var catId = 1010;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getWobyJobId: function(filter) {
                var res = $http.get('/api/as/Jobs/' + filter);
                return res;
            },
            getDataGender: function() {
                var res = $http.get('/api/as/Gender/');
                return res;
            },
            getTowass: function(vin) {
                var res = $http.get('/api/as/Towass/getFieldAction/0/' + vin);
                return res;
            },
            cancelRequest: function(data) {
                return $http.delete('/api/as/AppointmentRequests/', { data: data, headers: { 'Content-Type': 'application/json' } });
            },
            rescheduleRequest: function(data) {
                return $http.put('/api/as/AppointmentRequests', [{
                    Id: data.Id,
                    VehicleId: data.VehicleId,
                    RequestAppointmentDate: data.RequestAppointmentDateNew,
                    isGr: 0,
                    OutletId: data.OutletId
                }]);
            },
            createAppointment: function(data) {

                // ilangin flag jika tasknya adalah nontasklist
                for(var x in data.JobTask){
                    if(typeof data.JobTask[x].TaskBPId == "string"){
                        data.JobTask[x].TaskBPId = null;
                    }
                }

                console.log("datan sebelum kirim | createAppointment ===>", data);
                console.log("datan sebelum kirim | createAppointment ===>", JSON.stringify(data));
                console.log('data.AppointmentDate ====>',data.AppointmentDate);
                console.log('data.AppointmentTime ====>',data.AppointmentTime);
                console.log('data.FullDate ===========>',data.FullDate);

                // if(data.disTask !== null || data.disTask !== undefined){
                //     for(var a = 0; a < data.JobTask.length; a++){
                //         data.JobTask[a].Discount = data.disTask;
                //         data.JobTask[a].DiscountTypeId = 10;
                //         if(data.disParts !== null || data.disParts !== undefined && data.JobTask[a].JobParts.length > 0){
                //             for(var b = 0; b < data.JobTask[a].JobParts.length; b++){
                //                 data.JobTask[a].JobParts[b].Discount = data.disParts;
                //                 data.JobTask[a].JobParts[b].DiscountTypeId = 10;
                //             }
                //         }
                //     }
                // }

                //pindahin ke controller
                // if(data.disTask !== null || data.disTask !== undefined){
                //     console.log("oleh oleh oleh");
                //     for(var a = 0; a < data.JobTask.length; a++){
                //         if(data.JobTask[a].PaidById == 2277 || data.JobTask[a].PaidById == 30 || data.JobTask[a].PaidById == 31){

                //             console.log("keatas dong");
                //             data.JobTask[a].Discount = 0;
                //             data.JobTask[a].DiscountTypeId = -1;
                //             for(var b = 0; b < data.JobTask[a].JobParts.length; b++){
                //                if(data.JobTask[a].JobParts[b].PaidById == 2277 || data.JobTask[a].JobParts[b].PaidById == 30 || data.JobTask[a].JobParts[b].PaidById == 31){

                //                    data.JobTask[a].JobParts[b].Discount = 0;
                //                    data.JobTask[a].JobParts[b].DiscountTypeId = -1;
                                  
                //                }else{
                //                    if(data.disParts !== null || data.disParts !== undefined && data.JobTask[a].JobParts.length > 0){
                                          
                //                           data.JobTask[a].JobParts[b].Discount = data.disParts;
                //                           data.JobTask[a].JobParts[b].DiscountTypeId = 10;
                //                    }
                //                }
                //             }

                            
                //         }else{
                //             console.log("masuk bawah dong");
                //             data.JobTask[a].Discount = data.disTask;
                //             data.JobTask[a].DiscountTypeId = 10;
                //             if(data.disParts !== null || data.disParts !== undefined && data.JobTask[a].JobParts.length > 0){
                //                 for(var b = 0; b < data.JobTask[a].JobParts.length; b++){
                //                     data.JobTask[a].JobParts[b].Discount = data.disParts;
                //                     data.JobTask[a].JobParts[b].DiscountTypeId = 10;
                //                 }
                //             }
                        
                //         }
                       
                //     }
                // }
                //pindahin ke controller
                
                return $http.put('/api/as/Jobs/updateDetil/1?createNo=1', [{
                    OutletId: data.OutletId,
                    CalId: 1,
                    UserIdEstimasi: data.StallId,
                    AppointmentVia: data.AppointmentSourceId,
                    ContactPerson: data.ContactPerson,
                    PhoneContactPerson1: data.PhoneContactPerson1,
                    PhoneContactPerson2: data.PhoneContactPerson2,
                    VehicleId: data.VehicleId,
                    PlanStart: data.PlanStart,
                    PlanFinish: data.PlanFinish,
                    Email: data.Email,
                    CustRequest: data.CustRequest,
                    CustComplaint: data.CustComplaint,
                    WoCategoryId: data.WoCategoryId,
                    TechnicianAction: 1,
                    JobSuggest: 1,
                    JobType: 0,
                    Status: 0,
                    JobTask: data.JobTask,
                    JobComplaint: data.JobComplaint,
                    JobRequest: data.JobRequest,
                    // AppointmentDate: data.AppointmentDate,
                    AppointmentDate: data.FullDate,
                    AppointmentTime: data.AppointmentTime,
                    AppointmentNo: null,
                    NewAppointmentRel: null,
                    isAppointment: 1,
                    isGr: 0,
                    // FinalDP: data.totalDpDefault,
                    FinalDP: data.totalDp,
                    Km: data.Km,
                    isCash: data.isCash,
                    isSpk: data.isSpk,
                    SpkNo: data.SpkNo ? data.SpkNo : '-',
                    InsuranceId: data.InsuranceId,
                    InsuranceName: data.InsuranceName,
                    isWOBase: 1,
                    Process: null,
                    WoNo: 0,
                    IsEstimation: 0,
                    EstimationNo: "",
                    PoliceNumber: data.LicensePlate,
                    VIN: data.VIN,
                    ModelType: data.VehicleModelName,
                    KatashikiCode: data.detail.KatashikiCode,
                    PlanDateStart: data.PlanDateStart,
                    PlanDateFinish: data.PlanDateFinish,
                    VehicleTypeId: data.detail.VehicleTypeId,
                    JobDate: "",
                    ColorName: data.ColorName,
                    ColorId: data.ColorId,
                    ColorCode: data.ColorCode,
                    Alamat: data.Alamat,
                    VIN: data.VIN,
                    EngineNo: data.EngineNo
         
                }]);
            },
            createAppointmentDraft: function(data) {
                console.log("datan sebelum kirim | createAppointmentDraft ===>", data);
                console.log("datan sebelum kirim | createAppointmentDraft ===>", JSON.stringify(data));
                return $http.put('/api/as/Jobs/updateDetil/1', [{
                    OutletId: data.OutletId,
                    CalId: 1,
                    UserIdEstimasi: data.StallId,
                    AppointmentVia: data.AppointmentSourceId,
                    ContactPerson: data.ContactPerson,
                    PhoneContactPerson1: data.PhoneContactPerson1,
                    PhoneContactPerson2: data.PhoneContactPerson2,
                    VehicleId: data.VehicleId,
                    PlanStart: data.PlanStart,
                    PlanFinish: data.PlanFinish,
                    CustRequest: data.CustRequest,
                    CustComplaint: data.CustComplaint,
                    WoCategoryId: data.WoCategoryId,
                    TechnicianAction: 1,
                    JobSuggest: 1,
                    JobType: 0,
                    Status: 0,
                    Email: data.Email,
                    JobTask: data.JobTask,
                    JobComplaint: data.JobComplaint,
                    JobRequest: data.JobRequest,
                    AppointmentDate: data.AppointmentDate,
                    AppointmentTime: data.AppointmentTime,
                    AppointmentNo: null,
                    NewAppointmentRel: null,
                    isAppointment: 1,
                    isGr: 0,
                    FinalDP: data.totalDpDefault,
                    Km: data.Km,
                    isCash: data.isCash,
                    isSpk: data.isSpk,
                    SpkNo: data.SpkNo ? data.SpkNo : '-',
                    InsuranceId: data.InsuranceId,
                    InsuranceName: data.InsuranceName,
                    isWOBase: 1,
                    Process: null,
                    WoNo: 0,
                    IsEstimation: 0,
                    EstimationNo: "",
                    PoliceNumber: data.LicensePlate,
                    VIN: data.VIN,
                    ModelType: data.VehicleModelName,
                    KatashikiCode: data.detail.KatashikiCode,
                    PlanDateStart: data.PlanDateStart,
                    PlanDateFinish: data.PlanDateFinish,
                    VehicleTypeId: data.detail.VehicleTypeId,
                    JobDate: "",
                    ColorName: data.ColorName,
                    ColorId: data.ColorId,
                    ColorCode: data.ColorCode,
                    Alamat: data.Alamat,
                    VIN: data.VIN,
                    EngineNo: data.EngineNo
                }]);
            },
            putCustomerPersonal: function(item, data) {
                console.log("itemmmm putCustomerPersonal ", item);
                return $http.put('/api/crm/PutCListPersonal', [{
                    PersonalId: item.PersonalId,
                    CustomerId: item.CustomerId,
                    CustomerName: data.CustomerName,
                    CustomerNickName: data.CustomerNickName,
                    OriginCode: item.OriginCode,
                    KTPKITAS: item.KTPKITAS,
                    BirthPlace: item.BirthPlace,
                    BirthDate: item.BirthDate,
                    Handphone1: data.Handphone1,
                    Handphone2: data.Handphone2,
                    Email: item.Email,
                    MaritalDate: item.MaritalDate,
                    TotalPoint: item.TotalPoint,
                    CustomerGenderId: item.CustomerGenderId,
                    CustomerReligionId: item.CustomerReligionId,
                    CustomerEthnicId: item.CustomerEthnicId,
                    CustomerLanguageId: item.CustomerLanguageId,
                    MaritalStatusId: item.MaritalStatusId,
                    FrontTitle: item.FrontTitle,
                    EndTitle: item.EndTitle,
                    PhoneStatus: item.PhoneStatus,
                    StatusCode: 1,
                    CreatedDate: item.CreateDateCPersonal,
                    CreatedUserId: item.CreateUserIdCPersonal
                }]);
            },
            putCustomerAddress: function(item, data, Code) {
                var tmpObject = {
                    CustomerAddressId: item.CustomerAddressId,
                    CustomerId: item.CustomerId,
                    AddressCategoryId: item.AddressCategoryId,
                    Address: data.Address,
                    Phone1: data.Phone1,
                    Phone2: data.Phone2,
                    Fax: item.Fax,
                    RT: item.RT,
                    RW: item.RW,
                    ProvinceId: data.ProvinceId,
                    CityRegencyId: data.CityRegencyId,
                    DistrictId: data.DistrictId,
                    PostalCode: data.PostalCode,
                    VillageId: data.VillageId,
                    MainAddress: item.MainAddress == null ? 1 : item.MainAddress,
                    StatusCode: 1,
                    CreatedDate: item.CreateDateAddress,
                    CreatedUserId: item.CreateUserIdAddress
                };
                if (item.CustomerAddressId == null) {
                    delete tmpObject.CustomerAddressId;
                };
                if (Code == 1) {
                    //this "if" below, added on 30/05/2018
                    if (item.CustomerAddressId != null) {
                        return $http.put('/api/crm/PutCCustomerAddressForAppoint', [tmpObject]);
                    } else {
                        return $http.put('/api/crm/PutCCustomerAddress', [tmpObject]);
                    }
                } else {
                    return $http.put('/api/crm/PutCCustomerAddress', [tmpObject]);
                };

            },

            // GetLocation: function() {
            //   var res=$http.get('/api/crm/GetLocation/');

            //   // console.log('hasil=>',res);
            //   //res.data.Result = null;
            //   return res;
            // },

            // added by sss on 2017-09-20
            createJobPlan: function(jobId, dataEst) {
                return $http.put('/api/as/jobs/BpEstimation/' + jobId, [dataEst])
            },
            // 
            create: function(data) {
                return $http.post('/api/as/AppointmentBpService', [{
                    Employee: data.EmployeeId,
                    Initial: data.Initial,
                    ShiftByte: 5,
                    People: {
                        Name: data.Name,
                        DateofBirth: data.DateofBirth
                    }
                }]);
            },
            update: function(role) {
                return $http.put('/api/fw/Role', [{
                    Id: role.Id,
                    //pid: role.pid,
                    Name: role.Name,
                    Description: role.Description
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/fw/Role', {
                    data: id,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });
            },
            getTowass: function(vin) {
                var res = $http.get('/api/as/Towass/getFieldAction/0/' + vin);
                return res;
            },
            sendNotif: function(data, recepient, param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param : param
                }]);
            },
            getQuestionPSFU: function(vehicleid) {
                var res = $http.get('/api/as/GetQuestionAnswer/' + vehicleid);
                return res;
            },

            // Tambahan CR 4 [start]
            getDataCekEstimasi: function (param) {
                // var res = $http.get('/api/as/AEstimationBP/Get/?start=1&limit=100000');
                var res = $http.get('/api/as/AEstimationBP/Get/?start=1&limit=100000' + param);
                return res;
            },

            getDataCekEstimasiNew: function(data){
                // var res = $http.get('/api/as/AEstimationBP/GetListEstimationWOList?start=1&limit=1000&nopol='+data);
                var res = $http.get('/api/as/AEstimationBP/GetListEstimationWOList?start=1&limit=1000&nopol='+data);
                return res;
            },


            // getDetailEstimasi: function (EstId) {
            getDetailEstimasi: function (EstId,PoliceNumber) {
                var res = $http.get('/api/as/AEstimationBP/GetDetail/?EstId='+ EstId+'&PoliceNumber='+PoliceNumber+'&Ref=0');
                // var res = $http.get('/api/as/AEstimationBP/GetDetail/?EstId='+ EstId);
                return res;
            },

            getDamage: function() {
                var catId = 2040;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId + '&Flag=1');
                console.log('resnya pause=>', res);
                return res;
            },
            // Tambahan CR 4 [_end_]
            CheckLastContactPerson: function(filter, flag) {
                // api/as/CheckLastContactPerson/{filter}/{flag}
                // flag 1 kirim vehicleid
                // flag 2 kirim nopol

                return $http.get('/api/as/CheckLastContactPerson/' + filter + '/' + flag);
            },
            getDataVIN: function(vin){
                var res = $http.get('/api/as/MasterVehicle_FPCFLCPLC/' + vin)
                return res;
            },
            listHistoryClaim: function(vin){
                var res = $http.get('/api/as/ToWass/ListHistoryClaim?VIN=' + vin)
                return res;
            }
        }
    });