angular.module('app')
    .controller('followupAppointmentBpController', function($scope, $http, $q, GeneralMaster, FollowupService, WO, WOBP, bsNotify, ASPricingEngine, RepairProcessGR, RepairSupportActivityGR, CurrentUser, uiGridConstants, AsbEstimasiFactory, FollowUpBpService, FollowUpGrService, MrsList, $timeout, bsAlert, ngDialog, AsbGrFactory, AppointmentGrService, AppointmentBpService, WoHistoryGR, PrintRpt, $filter, VehicleHistory, MasterDiscountFactory, AppointmentBpService) {
        //----------------------------------
        // Start-Up
        //---------------------------------
        var PPNPerc = 0;


        $scope.DateFilterOptions1 = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.DateFilterOptions2 = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.DateOptionsASB = {
            startingDay: 1,
            format: 'dd/MM/yyyy',
            //disableWeekend: 1
        };
        
        $scope.asb = { currentDate: new Date(), showMode: 'main', nopol: "D 1234 ABC", tipe: "Avanza" }
        $scope.asb.endDate = new Date();
        $scope.asb.endDate.setDate($scope.asb.endDate.getDate() + 7);
        var currentDate = new Date();
        // currentDate.setDate(currentDate.getDate() + 1);
        currentDate.setDate(currentDate.getDate()); // edit bisa hari ini Pak Dodi
        $scope.minDateASB = new Date(currentDate);
        $scope.stallAsb = 0;
        $scope.saIdAsb = 0;
        // var showBoardAsbGr = function(vitem){    
        //   AsbGrFactory.showBoard({container: 'asb_asbgr_view', currentChip: vitem, onPlot: contoh});
        // }
        $scope.activeWAC = 0;
        $scope.activeJustified = 0;
        $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];

        $scope.slider = {
            value: 0,
            options: {
                step: 3.125,
                hidePointerLabels: true,
                noSwitching: true,
                minRange: 1,
                disabled: true,
                readonly: true,
                floor: 0,
                ceil: 100,
                showSelectionBar: true,
                // translate: function(value) {
                //     return value + '%';
                // }
                translate: function(value, id, label) {
                    if (value == 0) {
                        return value = 'E'
                    } else if (value == 100) {
                        return value = 'F'
                    }
                }
            }
        };
        $scope.refreshSlider = function() {
            $timeout(function() {
                $scope.$broadcast('rzSliderForceRender');
            });
        };

    

        $('#layoutWAC').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
        // =======================
        WOBP.getDataWAC().then(function(resu) {
            console.log('cek data WAC', resu.data);
            $scope.headerWAC = resu.data;
        });

        var allBoardHeight = 500;
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            $scope.checkApprove();
            $scope.checkIsKabeng();
            //showBoardAsbGr(vobj);
            $scope.getPPN()

            WOBP.getT1().then(function(res) {
                $scope.TFirst = res.data.Result;
                //console.log("T1", $scope.T1);
            })
        });

        $scope.getPPN = function (noNpwp) {
            if (noNpwp !== undefined && noNpwp !== null && noNpwp !== ''){
                noNpwp = noNpwp.toString();
                if (noNpwp.length === 20){
                    if (noNpwp !== '00.000.000.0-000.000'){
                        ASPricingEngine.getPPN(1, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    } else {
                        ASPricingEngine.getPPN(0, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    }
                } else {
                    ASPricingEngine.getPPN(0, 3).then(function (res) {
                        PPNPerc = res.data
                    });
                }

            } else {
                // param 1 adalah isnpwp, param 2 tipe ppn
                ASPricingEngine.getPPN(0, 3).then(function (res) {
                    PPNPerc = res.data
                });
            }
            
        };

        $scope.model_info_km = {}
        $scope.model_info_km.show = false;
        $scope.viewButtoninfokm = {save:{text:'Ok'}};

        $scope.Saveinfokm = function(){
            $scope.model_info_km.show = false;
        }
        $scope.Cancelinfokm = function(){
            $scope.model_info_km.show = false;
        }
        $scope.show_km_info = function() {
            $scope.model_info_km.show = true;
        }

        FollowUpBpService.getUnitMeasurement().then(function(res) {
            $scope.unitData = res.data.Result;
            $scope.unitDataGrid = [];
            var tmpUnit = $scope.unitData;
            console.log("$scope.unitData=====>", $scope.unitData);
        });
        var showBoardAsbGr = function(item) {
            console.log("item item", item);
            var vtime
            var tmphour
            var tmpMinute
            if (item !== undefined && item !== null) {
                vtime = item;
                tmphour = item.getHours();
                tmpMinute = item.getMinutes();
            } else {
                vtime = new Date();
                tmphour = vtime.getHours();
                tmpMinute = vtime.getMinutes();
            }

            vtime.setHours(tmphour);
            vtime.setMinutes(tmpMinute);
            vtime.setSeconds(0);

            if($scope.filter.typeCategory == 3){
                $scope.stallAsb = 0;
            }

            $scope.currentItem = {
                mode: 'main',
                currentDate: $scope.asb.startDate,
                nopol: $scope.mDataDetail.LicensePlate,
                vin: $scope.mDataDetail.VIN,
                tipe: $scope.mDataDetail.VehicleModelName,
                durasi: $scope.tmpActRate,
                startTime: vtime,
                saId: $scope.stallAsb,
                visible: $scope.visibleAsb,
                asbStatus: $scope.asb,
                jobId: $scope.mData.JobId,
                stallId: $scope.stallAsb
                    // JobId: $scope.mData.JobId,
                    /*stallId :1,
                    startDate :,
                    endDate:,
                    durasi:,*/
            }
            console.log("actRe", $scope.tmpActRate);
            console.log("harusnya tanggal", $scope.asb.startDate);
            console.log("ini vtime", vtime);
            console.log("stallId", $scope.currentItem);
            $timeout(function() {
                AsbEstimasiFactory.showBoard({ container: 'asb_estimasi_view', currentChip: $scope.currentItem, onPlot: getDateEstimasi });

            }, 100);

        }


        //Tambahan CR4
        $scope.DiskonPartsEndDate = null;
        $scope.DiskonTaskEndDate = null;
        $scope.asbDate = undefined;
        $scope.choosePembayaran = function(data){
            console.log("data",data);
            if(data == 1){

                MasterDiscountFactory.getDataAktif().then(
                    function (res) {
                        console.log("res dis", res.data.Result);
                        $scope.DiscountParts = 0;
                        $scope.mData.disParts = 0;
                        $scope.DiscountTask = 0;
                        $scope.mData.disTask = 0;
                        for(var i = 0; i < res.data.Result.length; i++){
                            if(res.data.Result[i].Category == 2){
                                $scope.DiskonPartsEndDate = res.data.Result[i].DateTo;
                                $scope.DiscountParts = res.data.Result[i].Discount;
                                $scope.mData.disParts = res.data.Result[i].Discount;
                                if(jejeranDiskonParts.length == 0){
                                    $scope.DisplayDiscountParts = res.data.Result[i].Discount;
                                }
                            }else if(res.data.Result[i].Category == 1){
                                $scope.DiskonTaskEndDate = res.data.Result[i].DateTo;
                                $scope.DiscountTask = res.data.Result[i].Discount;
                                $scope.mData.disTask = res.data.Result[i].Discount;
                                if(jejeranDiskonTask.length == 0){
                                    $scope.DisplayDiscountTask = res.data.Result[i].Discount;
                                }
                            }
                        }

                        if(($scope.asbDate == undefined) || ($scope.asbDate.length == 0 )){
                            
                            console.log('kalo asbnya undefined | [] ===>',$scope.asbDate);
                        }else{
                            if($scope.asbDate > $scope.DiskonTaskEndDate){
                                console.log('Tidak Masuk periode diskon booking Task');
                                $scope.DiscountTask = 0;
                                $scope.mData.disTask = 0;
                            }else{
                                console.log('masuk periode diskon booking Task');
                            }
                            if($scope.asbDate > $scope.DiskonPartsEndDate){
                                console.log('Tidak Masuk periode diskon booking Parts');
                                $scope.DiscountParts = 0;
                                $scope.mData.disParts = 0;
                            }else{
                                console.log('masuk periode diskon booking Parts');
                            }
                        }
                        console.log("$scope.mData",$scope.mData);
                    },
                    function (err) { }
                );
            }else{
                $scope.DiscountParts = 0;
                $scope.mData.disParts = 0;
                $scope.DiscountTask = 0;
                $scope.mData.disTask = 0;
            }
        }



        var insuranceId = null;
        $scope.selectInsurance = function(selected) {
            console.log("selected : ", selected);
            insuranceId = selected.InsuranceId;
            $scope.DiscountTask = selected.DiscountJasa;
            $scope.mData.disTask = selected.DiscountJasa;
            $scope.mData.disParts = selected.DiscountPart;
            $scope.DiscountParts = selected.DiscountPart;
            if (selected && (!selected.IntegrationBit || selected.IntegrationBit == 0)) {
                console.log("intBit false");
                $("#btnCekSPK").attr('disabled', 'disabled');
                // $scope.disButton.disable = true;
            } else {
                $("#btnCekSPK").removeAttr('disabled', 'disabled');
                // $scope.disButton.disable = false;
            }
        }
        
        //Added Cr4 Galang End
        $scope.reloadBoard = function(item) {

            $scope.asbDate = item;

            if($scope.mData.isCash == 1){
                if(item > $scope.DiskonTaskEndDate){
                    console.log('Tidak Masuk periode diskon booking Task');
                    $scope.DiscountTask = 0;
                    $scope.mData.disTask = 0;
                }else{
                    console.log('masuk periode diskon booking Task');
                }

                if(item > $scope.DiskonPartsEndDate){
                    console.log('Tidak Masuk periode diskon booking Parts');
                    $scope.DiscountParts = 0;
                    $scope.mData.disParts = 0;
                }else{
                    console.log('masuk periode diskon booking Parts');
                }

            }

            
            $scope.mData.PlanStart = null;
            $scope.mData.PlanFinish = null;
            $scope.mData.PlanDateStart = null;
            $scope.mData.AppointmentDate = null;
            $scope.mData.AppointmentTime = null;
            $scope.mData.TargetDateAppointment = null;
            $scope.mData.TimeTarget = null;
            $scope.mData.PlanDateFinish = null;
            console.log("dari ng-change", item);
            console.log("$scope.mData.FullDate", $scope.mData.FullDate);
            var vtime = item;
            var tmphour
            var tmpMinute
            if ($scope.mData.FullDate !== undefined) {
                console.log("$scope.mData.FullDate", $scope.mData.FullDate);
                var tmpTime = $scope.mData.FullDate;
                tmphour = tmpTime.getHours();
                tmpMinute = tmpTime.getMinutes();
            } else {
                tmphour = 9;
                tmpMinute = 0;
            }
            vtime.setHours(tmphour);
            vtime.setMinutes(tmpMinute);
            vtime.setSeconds(0);
            $scope.currentItem = {
                mode: 'main',
                currentDate: $scope.asb.startDate,
                nopol: $scope.mDataDetail.LicensePlate,
                vin: $scope.mDataDetail.VIN,
                tipe: $scope.mDataDetail.VehicleModelName,
                durasi: $scope.tmpActRate,
                startTime: vtime,
                saId: 0,
                visible: $scope.visibleAsb,
                stallId: 0,
                jobId: $scope.mData.JobId,
                /*stallId :1,
                startDate :,
                endDate:,
                durasi:,*/
            }
            console.log("actRe", $scope.tmpActRate);
            console.log("harusnya tanggal", $scope.asb.startDate);
            console.log("ini vtime", vtime);
            console.log("stallId", $scope.currentItem.stallId);
            $timeout(function() {
                AsbEstimasiFactory.showBoard({ container: 'asb_estimasi_view', currentChip: $scope.currentItem, onPlot: getDateEstimasi });

            }, 100);
        }
        var getDateEstimasi = function(data) {
                console.log('onplot data...', data);
                var dt = new Date(data.startTime);
                var dtt = new Date(data.endTime);
                console.log("date", dt);

                if($scope.mData.isCash == 1){
                    if(dt > $scope.DiskonTaskEndDate){
                        console.log('Tidak Masuk periode diskon booking Task');
                        $scope.DiscountTask = 0;
                        $scope.mData.disTask = 0;
                    }else{
                        console.log('masuk periode diskon booking Task');
                    }
                    if(dt > $scope.DiskonPartsEndDate){
                        console.log('Tidak Masuk periode diskon booking Parts');
                        $scope.DiscountParts = 0;
                        $scope.mData.disParts = 0;
                    }else{
                        console.log('masuk periode diskon booking Parts');
                    }
    
                }

                dt.setSeconds(0);
                dtt.setSeconds(0);
                var timeStart = dt.toTimeString();
                var timeFinish = dtt.toTimeString();
                timeStart = timeStart.split(' ');
                timeFinish = timeFinish.split(' ');
                // ===============================
                var yearFirst = dt.getFullYear();
                var monthFirst = dt.getMonth() + 1;
                var dayFirst = dt.getDate();
                var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                // ===============================
                var yearFinish = dtt.getFullYear();
                var monthFinish = dtt.getMonth() + 1;
                var dayFinish = dtt.getDate();
                var FinishDate = yearFinish + '-' + monthFinish + '-' + dayFinish;
                // ===============================
                // if(timeStart[0] !== null && timeFinish[0] !== null){
                $scope.mData.StallId = data.stallId;
                $scope.mData.FullDate = dt;
                $scope.mData.PlanStart = timeStart[0];
                $scope.mData.PlanFinish = timeFinish[0];
                $scope.mData.PlanDateStart = firstDate;
                $scope.mData.AppointmentDate = firstDate;
                $scope.mData.AppointmentTime = timeStart[0];
                $scope.mData.TargetDateAppointment = FinishDate;
                $scope.mData.TimeTarget = timeFinish[0];
                $scope.mData.PlanDateFinish = FinishDate;
                // }

                // }
            }
            //----------------------------------
            // Initialization
            //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mData = null; //Model
        $scope.xData = {};
        $scope.xData.selected = [];
        $scope.filter = {};
        $scope.selectedRows = {};
        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        $scope.checkAsb = false;
        $scope.tabShowed = false;
        $scope.jobAvailable = false;
        $scope.partsAvailable = false;
        var Parts = [];
        $scope.tmpActRate = 1;
        $scope.totalWork = 0;
        $scope.totalMaterial = 0;
        $scope.show_modal = { show: false };
        $scope.modalMode = 'new';
        $scope.tmpActRate = 1;
        $scope.stallAsb = 0;
        var dataJobTaskPlan = [];
        $scope.listApiRequest = {};
        var currentDate = new Date();
        $scope.currentDate = currentDate.setDate(currentDate.getDate() + 1);
        $scope.VehicleGasType = [
            { VehicleGasTypeId: 1, VehicleGasTypeName: "Bensin" },
            { VehicleGasTypeId: 2, VehicleGasTypeName: "Diesel" },
            { VehicleGasTypeId: 3, VehicleGasTypeName: "Hybrid" }
        ];
        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        var gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
            '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Lihat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.onTimeAppointment(row.entity)" uib-tooltip="On-Time" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-check-circle-o" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.cancelAppointment(row.entity)" uib-tooltip="Batal" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-times-circle-o" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.actEditAppointment(row.entity)" uib-tooltip="Edit" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.actPrintAppointment(row.entity)" uib-tooltip="Print" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-print" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';
        $scope.checkApprove = function() {
            if ($scope.user.RoleId == 1129 || $scope.user.RoleId == 1047) {
                $scope.allowApprove = true;
            } else {
                $scope.allowApprove = false;
            }
        };
        $scope.buttonSettingTitle = {
            save: {
                text: 'Kirim'
            },
            cancel: {
                text: 'Kembali'
            }
        };
        $scope.actApprovel = function(data, type) {
            console.log("data====>", data);
            var tmpRow = {};
            tmpRow = angular.copy(data[0]);
            if (tmpRow.StatusApprovalId == 3) {
                tmpRow.isReject = type;
                if (type == 1) {
                    console.log("data====>  $scope.buttonSettingTitle", $scope.buttonSettingTitle);
                    $scope.isReject = true;
                    $scope.buttonSettingTitle.save.text = "Reject";
                    tmpRow.NewStatusApprovalId = 2;
                } else {
                    console.log("data====>  $scope.buttonSettingTitle", $scope.buttonSettingTitle);
                    $scope.isReject = false;
                    $scope.buttonSettingTitle.save.text = "Approve";
                    tmpRow.NewStatusApprovalId = 1;
                }
                $scope.modal_model = angular.copy(tmpRow);
                // $scope.bsAlertFuncCancel("Apakah anda yakin akan membatalkan Appointment?", data);
                $scope.show_modalApproval = {
                    show: true
                };
                $scope.dateOfApproval = new Date();
            } else if (tmpRow.StatusApprovalId == 2) {
                bsAlert.warning("Request Sudah di Reject", "");
            } else {
                bsAlert.warning("Request Sudah di Setujui", "");
            }
        };
        $scope.doSaveApproval = function(item) {
            console.log("itemsave", item);
            FollowUpBpService.updateApproval(item).then(
                function(res) {
                    $scope.show_modalApproval = {
                        show: false
                    };
                    $scope.getData();
                },
                function(err) {

                }
            );

        }
        $scope.doBack = function() {

        }
        $scope.show_modal = {
            show: false
        };
        $scope.show_modalApproval = {
            show: false
        };
        $scope.modelData = {};
        $scope.isReject = false;
        $scope.actBtnCaption = { approve: 'Approve', reject: 'Reject' };
        $scope.modalMode = 'new';
        $scope.ComplaintCategory = [
            { ComplaintCatg: 'Body' },
            { ComplaintCatg: 'Body Electrical' },
            { ComplaintCatg: 'Brake' },
            { ComplaintCatg: 'Chassis' },
            { ComplaintCatg: 'Drive Train' },
            { ComplaintCatg: 'Engine' },
            { ComplaintCatg: 'Heater System & AC' },
            { ComplaintCatg: 'Restraint' },
            { ComplaintCatg: 'Steering' },
            { ComplaintCatg: 'Suspension and Axle' },
            { ComplaintCatg: 'Transmission' },
        ]
        $scope.JobRequest = [];
        $scope.JobComplaint = [];
        $scope.listButtonSettings = {
            new: { enable: true, icon: "fa fa-fw fa-car" },
            // view:{enable:false},
            // delete:{enable:false},
            // select:{enable:false},
            // selectall:{enable:false},
            // edit:{enable:false},
        };
        $scope.listCustomButtonSettings = {
            enable: true,
            icon: "fa fa-fw fa-car",
            func: function(row) {
                console.log("customButton", row);
            }
        };
        $scope.bsAlertFuncCancel = function(prompt, item) {
            bsAlert.alert({
                    title: prompt,
                    text: item.AppointmentNo,
                    type: "warning",
                    showCancelButton: true
                },
                function() {
                    // $scope.cancelService(item);
                },
                function() {

                }
            )
        };
        $scope.bsAlertFuncOnTime = function(prompt, item) {
            bsAlert.alert({
                    title: prompt,
                    text: item.AppointmentNo,
                    type: "warning",
                    showCancelButton: true
                },
                function() {
                    $scope.onTimeService(item);
                },
                function() {

                }
            )
        };
        $scope.onTimeAppointment = function(data) {
            if ($scope.filter.typeCategory != 4) {
                $scope.bsAlertFuncOnTime("Pelanggan sudah di Follow Up ?", data);
            } else {
                bsAlert.alert({
                        title: "Tidak dapat follow Up dibagian history",
                        text: "",
                        type: "warning"
                            // showCancelButton:true
                    },
                    function() {

                    },
                    function() {

                    }
                )
            }

        }


        $scope.cancelAppointment = function(data) {
            console.log("data cancelAppointment", data);
            if (data.Status == 0) {
                $scope.modal_model = angular.copy(data);
                // $scope.bsAlertFuncCancel("Apakah anda yakin akan membatalkan Appointment?", data);
                $scope.show_modal = {
                    show: true
                };
                // $scope.bsAlertFuncCancel("Apakah anda yakin akan membatalkan Appointment?", data);
            } else {
                bsAlert.alert({
                        title: "Batal appointment hanya dapat dilakukan untuk status Outstanding",
                        text: "",
                        type: "warning"
                        // showCancelButton:true
                    },
                    function() {

                    },
                    function() {
                        
                    }
                    )
                }
            };
            
            
            
            
            $scope.updateWOforCancleAppointment = function(JobId,arrayEstimasi){
                console.log('updateWOforCancleAppointment | JobId ===>',JobId)
                console.log('updateWOforCancleAppointment | arrayEstimasi ===>',arrayEstimasi)
                FollowUpBpService.UpdateJobAppointmentAddEstimasiId(JobId,arrayEstimasi).then(
                    function(res) {
                        console.log('UpdateJobAppointmentAddEstimasiId success ====>',res);
                    },function(err){
                        console.log('UpdateJobAppointmentAddEstimasiId error ====>',err);
                    }
                );
            }
            
            
            $scope.dataUpdateWO = [];
            $scope.doCancelAppointment = function(item) {
                WoHistoryGR.getStatWO(item.JobId, item.Status).then(function(ress){
                    if (ress.data == 0) {
                        console.log("doCancelAppointment Item", item);
                        $scope.totalDPAlert = 0;


                        FollowUpBpService.getJobParts(item.JobId).then(function(resu) {
                            console.log("form onSelectRows getJobParts cancel=>", resu.data.Result);
                            var data = resu.data.Result;
                            for (var i in data) {
                                $scope.totalDPAlert += data[i].FinalDP;
                                console.log('totalDPAlert alert', $scope.totalDPAlert);
                            }

                            //galang checkpoint
                            if (item.Reason !== undefined) {

                                if ($scope.totalDPAlert !== 0) {

                                    var totaldp_withseparator = angular.copy($scope.totalDPAlert)
                                    totaldp_withseparator = totaldp_withseparator.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, '.');

                                    bsAlert.alert({
                                        title: "Apakah anda yakin?",
                                        text: "Cancel appointment dengan DP Rp. " + totaldp_withseparator + " ?",
                                        type: "question",
                                        showCancelButton: true,
                                        confirmButtonText: "Ya",
                                        cancelButtonText: "Tidak"
                                    },
                                    function() {
                                        if (item.IsAppointmentDP == 1) {
                                            // bsAlert.warning('Tidak bisa membatalkan appointment karena sudah ada DP yang dibayar')
                                            // $scope.show_modal = {
                                            //     show: false
                                            // };

                                            // bsAlert.warning('Silakan hubungi Administrasi Finance untuk laporan titipan dan pengembalian DP')
                                            // $scope.cancelService(item);

                                            bsAlert.alert({
                                                title: "Silakan hubungi Administrasi Finance untuk laporan titipan dan pengembalian DP",
                                                text: "",
                                                type: "warning",
                                                showCancelButton: false
                                            }, function(){
                                                $scope.cancelService(item);
                                            
                                            });
                                        }else{
                                            $scope.cancelService(item);
                                        }
                                    },
                                    function() {
                                        // $timeout(function() {
                                        //     $scope.show_modal = {
                                        //         show: false
                                        //     };
                                        // }, 100);
                                        // $timeout(function() {
                                        //     $scope.cancelAppointment($scope.modal_model);
                                        // }, 200);
                                    })

                                } else {
                                    bsAlert.alert({
                                        title: "Apakah anda yakin??",
                                        text: "Untuk membatalkan appointment ini",
                                        type: "question",
                                        showCancelButton: true,
                                        confirmButtonText: "Ya",
                                        cancelButtonText: "Tidak"
                                    },
                                    function() {
                                        $scope.cancelService(item);
                                    },
                                    function() {

                                    })

                                }

                                // FollowUpBpService.getWobyJobId(item.JobId).then(
                                //     function(res) {
                                //         $scope.dataAppointment = res.data.Result[0];
                                //         console.log('getWobyJobId success ====>',$scope.dataAppointment);
                    
                                //         $scope.dataUpdateWO = [];
                                //         for(var x in $scope.dataAppointment.AEstimationBPWO){
                                //             $scope.dataUpdateWO.push({
                                //                 IsWO : 2,
                                //                 EstimationNo : $scope.dataAppointment.AEstimationBPWO[x].EstimationNo,
                                //                 EstimationBPId : $scope.dataAppointment.AEstimationBPWO[x].EstimationBPId
                                //             })
                                //         }

                                //         console.log('$scope.dataUpdateWO ===>',$scope.dataUpdateWO)
                                //         $scope.updateWOforCancleAppointment(item.JobId,$scope.dataUpdateWO);
                    
                                //     },function(err){
                                //         console.log('getWobyJobId error ====>',err);
                    
                                //     }
                                // );


                                

                                // $scope.cancelService(item);
                                // $scope.show_modal = {
                                //     show: false
                                // };
                            } else {
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Data masih belum terisi",
                                    // content: error.join('<br>'),
                                    // number: error.length
                                });
                            }




                        })
                        
    
                    } else {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Ada Perubahan Status, Silahkan Refresh",
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                    }
    
                })
            
        };
        // $scope.onTimeService = function(data) {
        //     FollowUpBpService.onTime($scope.filter.typeCategory, data).then(function(res) {
        //         $scope.getData();
        //     });
        // };
        // $scope.onTimeAppointment = function(data){
        //   $scope.bsAlertFuncOnTime("Apakah konsumen bisa datang tepat waktu?",data);
        // }
        // $scope.cancelAppointment = function(data){
        //     $scope.bsAlertFuncCancel("Apakah anda yakin akan membatalkan Appointment?",data);
        // }
        $scope.onTimeService = function(data) {
            console.log('data onTimeService', data);
            FollowUpBpService.onTime($scope.filter.typeCategory, data).then(function(res) {
                $scope.getData();
            });
        }
        $scope.backEstimate = function() {
            $scope.checkAsb = false;
        }
        $scope.cancelService = function(data) {
            FollowUpBpService.cancel(data).then(function(res) {
                $scope.getData();
                $scope.show_modal = {
                    show: false
                };

                bsAlert.alert({
                        title: "Cancel Appointment berhasil",
                        text: "",
                        // type: "warning",
                        type: "success",
                        showCancelButton: false
                    },
                    function() {

                    },
                    function() {

                    }
                )


                //kirim notifikasi batal Appointment
                // var DataNotif = {};
                // var messagetemp = "";
                // messagetemp =   "No. Appointment : " + $scope.mData.AppointmentNo  + " <br> " 
                //                 +", No. Polisi : " + $scope.mData.PoliceNumber + "  <br> " ;

                //  DataNotif.Message = messagetemp;
                FollowUpBpService.sendNotif(data, 1122, 21).then(
                    function(res) {

                    },
                    function(err) {
                        //console.log("err=>", err);
                    });
            });


        }
        $scope.actEditAppointment = function(data) {
            WoHistoryGR.getStatWO(data.JobId, data.Status).then(function(ress){
                if (ress.data == 0) {
                    console.log("data====>", data);

                    $scope.asbDate = data.AppointmentDate;

                    $scope.loadingSimpan = 0
        
        
                    var tmpDateAppointment = new Date();
                    tmpDateAppointment.setHours(0);
                    tmpDateAppointment.setMinutes(0);
                    tmpDateAppointment.setSeconds(0);
                    tmpDateAppointment.setMilliseconds(0);
                    console.log(tmpDateAppointment, data.AppointmentDate);
                    if (data.Status == 0 && data.AppointmentDate >= tmpDateAppointment) {
                        $scope.actEdit(data);
                    } else {
                        bsAlert.alert({
                                title: "Edit appointment hanya dapat dilakukan untuk status Outstanding",
                                text: "",
                                type: "warning"
                                    // showCancelButton:true
                            },
                            function() {
        
                            },
                            function() {
        
                            }
                        )
                    }
                } else {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Ada Perubahan Status, Silahkan Refresh",
                        // content: error.join('<br>'),
                        // number: error.length
                    });
                }
            })
            
        }
        $scope.actPrintAppointment = function(data) {
            console.log('JobId cetakAppointment', data);
            $scope.printAppoinmentBP = 'as/PrintAppointmentBp/' + data.JobId + '/' + $scope.user.OrgId + '/' + 0;
            $scope.cetakan($scope.printAppoinmentBP);
        }
        $scope.cetakan = function(data) {
            var pdfFile = null;
            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame 
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    } else {
                        printJS(pdfFile);
                    }
                } else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };
        $scope.actEdit = function(data) {
            console.log("pas klik edottt", data);
            // var Apppoint = angular.copy(data);
            // if ($scope.filter.typeCategory != 4) {
            $scope.mData = {};
            // gridTemp = [];
            $scope.gridWork = [];
            dataJobTaskPlan = [];
            var Parts = [];
            $scope.JobComplaint = [];
            $scope.JobRequest = [];
            $scope.gridEstimation.data = [];
            $scope.gridWork = $scope.gridWork;
            $scope.totalWork = 0;
            $scope.totalMaterial = 0;
            $scope.totalMaterialDiscounted =0;
            $scope.subTotalMaterialSummary = 0;
            $scope.totalWorkDiscounted = 0;
            $scope.subTotalWorklSummary = 0
            $scope.totalPPN = 0;
            $scope.totalEstimasi = 0;
            $scope.totalDp = 0;
            $scope.mDataVehicleHistory = [];
            $scope.MRS = {};
            MrsList.getDataModel().then(function(res) {
                console.log("result : ", res.data.Result);
                $scope.modelData = res.data.Result;

                $scope.onShowDetail(data, 'edit');
            });

            // $scope.getEstimate(data.VehicleId);
            console.log("$scope.gridWork lu    l",$scope.gridWork);
            $scope.checkAsb = false;
            console.log("$scope.$scope.formApi", $scope.formApi);
            $scope.formApi.setFormReadOnly(false);
            $scope.formApi.setMode('detail');
            // $scope.mData = Apppoint;
            console.log("mData Appointment", $scope.mData);
            $scope.actionButtonSettingsDetail[0].visible = true;
            $scope.actionButtonSettingsDetail[1].visible = true;
            $('#layoutWAC').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
        };
        $scope.actView = function(data) {
            console.log("pas klik viewww", data);
            // var Apppoint = angular.copy(data);
            $scope.mData = {};
            // gridTemp = [];
            $scope.gridWork = [];
            dataJobTaskPlan = [];
            $scope.JobComplaint = [];
            $scope.JobRequest = [];
            $scope.gridEstimation.data = [];
            $scope.gridWork = $scope.gridWork;
            $scope.totalWork = 0;
            var Parts = [];
            $scope.totalMaterial = 0;
            $scope.totalMaterialDiscounted =0;
            $scope.subTotalMaterialSummary = 0;
            $scope.totalPPN = 0;
            $scope.totalEstimasi = 0;
            $scope.totalDp = 0;
            $scope.mDataVehicleHistory = [];
            $scope.MRS = {};
            // $scope.
            MrsList.getDataModel().then(function(res) {
                console.log("result : ", res.data.Result);
                $scope.modelData = res.data.Result;
                $scope.onShowDetail(data, 'view');
            });
            // $scope.getEstimate(data.VehicleId);
            $scope.checkAsb = false;
            console.log("$scope.$scope.formApi", $scope.formApi);
            // $scope.mData = Apppoint;
            console.log("mData Appointment", $scope.mData);
            // $scope.disView = true;
            $scope.formApi.setFormReadOnly(true);
            $scope.formApi.setMode('detail');
            $scope.actionButtonSettingsDetail[0].visible = false;
        };

        $scope.saveApprovalDP = function(data) {
            // AppointmentGrService.getWobyJobId(data.JobId).then(function(res) {
            //     var tmpDataForApprovalParts = [];
            //     var tmpApproval = res.data.Result[0];
            //     console.log("sukses tmpApproval", tmpApproval);
            //     // var tmpDataForApprovalPart = tmpDataForApproval.JobParts;
            //     for (var k in tmpApproval.JobTask) {
            //         for (var i in tmpApproval.JobTask[k].JobParts) {
            //             if (tmpApproval.JobTask[k].JobParts[i].DownPayment < tmpApproval.JobTask[k].JobParts[i].minimalDp) {
            //                 console.log("mpApproval.JobTask[k].JobParts[i]", tmpApproval.JobTask[k].JobParts[i]);
            //                 tmpDataForApprovalParts.push({
            //                     JobId: data.JobId,
            //                     DPDefault: tmpApproval.JobTask[k].JobParts[i].minimalDp,
            //                     DPRequested: tmpApproval.JobTask[k].JobParts[i].DownPayment,
            //                     JobPartsId: tmpApproval.JobTask[k].JobParts[i].JobPartsId,
            //                     ApprovalCategoryId: 38,
            //                     ApproverId: null,
            //                     RequesterId: null,
            //                     ApproverRoleId: data.ApproverRoleId,
            //                     RequestorRoleId: $scope.user.RoleId,
            //                     RequestReason: "Request Pengurangan DP",
            //                     RequestDate: new Date(),
            //                     StatusApprovalId: 3,
            //                     StatusCode: 1,
            //                     VehicleTypeId: tmpApproval.VehicleTypeId
            //                 })
            //             }
            //         }
            //     }
            //     console.log("tmpDataForApprovalParts", tmpDataForApprovalParts);
            //     AppointmentGrService.postApprovalDp(tmpDataForApprovalParts).then(function(res) {
            //         console.log("sukses oostapproval");
            //     });
            // });
            var tmpDataForApprovalParts = [];
            var tmpApproval = data;
            for (var k in tmpApproval.JobTask) {
                for (var i in tmpApproval.JobTask[k].JobParts) {
                    if (tmpApproval.JobTask[k].JobParts[i].DownPayment < tmpApproval.JobTask[k].JobParts[i].minimalDp) {
                        console.log("mpApproval.JobTask[k].JobParts[i]", tmpApproval.JobTask[k].JobParts[i]);
                        tmpDataForApprovalParts.push({
                            JobId: tmpApproval.JobId,
                            DPDefault: tmpApproval.JobTask[k].JobParts[i].minimalDp,
                            DPRequested: tmpApproval.JobTask[k].JobParts[i].DownPayment,
                            JobPartsId: tmpApproval.JobTask[k].JobParts[i].JobPartsId,
                            ApprovalCategoryId: 41,
                            ApproverId: null,
                            RequesterId: null,
                            ApproverRoleId: tmpApproval.ApproverRoleId,
                            RequestorRoleId: $scope.user.RoleId,
                            RequestReason: "Request Pengurangan DP",
                            RequestDate: new Date(),
                            StatusApprovalId: 3,
                            StatusCode: 1,
                            VehicleTypeId: tmpApproval.VehicleTypeId
                        })
                    }
                }
            }
            console.log("tmpDataForApprovalParts", tmpDataForApprovalParts);
            AppointmentBpService.postApprovalDp(tmpDataForApprovalParts).then(function(res) {
                console.log("sukses oostapproval");
            });
        }

        $scope.changeFormatDate = function(item) {
            var tmpItemDate = angular.copy(item);
            console.log("changeFormatDate item", item);
            tmpItemDate = new Date(tmpItemDate);
            var finalDate = '';
            var yyyy = tmpItemDate.getFullYear().toString();
            var mm = (tmpItemDate.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = tmpItemDate.getDate().toString();

            console.log("changeFormatDate finalDate", finalDate);
            return finalDate += yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
        }

        $scope.actionButtonSettingsDetail = [{
                func: function(row, formScope) {

                    console.log('click simpan aja terus', $scope.actionButtonSettingsDetail)

                    if ($scope.loadingSimpan > 0) {
                        bsNotify.show({
                            size: 'small',
                            type: 'danger',
                            title: "Data sedang proses simpan..",
                            // timeout: 1000,
                        });
                        return false;
                    } else {
                        $scope.loadingSimpan++
                        $timeout(function() {
                            $scope.loadingSimpan = 0
                        }, 30000);

                        console.log("mData actionButtonSettingsDetail", $scope.mData);
                        console.log("mdata di row", row);

                        var tmpKm = angular.copy($scope.mData.Km).toString();
                        var backupKm = angular.copy($scope.mData.Km);
                        if (tmpKm.includes(",") == true) {
                            tmpKm = tmpKm.split(',');
                            if (tmpKm.length > 1) {
                                tmpKm = tmpKm.join('');
                            } else {
                                tmpKm = tmpKm[0];
                            }
                        }
                        tmpKm = parseInt(tmpKm);

                        if (tmpKm < $scope.KMTerakhir && $scope.KMTerakhir < 999999) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "KM tidak boleh lebih kecil dari " + $scope.KMTerakhir 
                            });
                            $scope.loadingSimpan = 0
                            return false;
                        }

                        WoHistoryGR.getStatWO(row.JobId, row.Status).then(function(resux) {
                            console.log("$scope.gridWork zanzukkkkk",$scope.gridWork);
                            if($scope.gridWork.length > 0){

                                for (var i = 0; i < $scope.gridWork.length; i++) {
                                    $scope.gridWork[i].DiscountTypeId = $scope.gridWork[i].DiscountTypeId;
                                    $scope.gridWork[i].Discount = $scope.gridWork[i].Discount;
                                }
                            }

                            if (resux.data == 0){
                                var tmpGrid = [];
                                tmpGrid = angular.copy($scope.gridWork);
                                // if ($scope.gridWork.length > 0 && $scope.mData.AppointmentDate !== null && $scope.mData.ContactPerson !== null && $scope.mData.PhoneContactPerson1 !== null && $scope.mData.Category !== null && $scope.mData.Km !== null) {
                                if (($scope.mData.AppointmentDate !== null && $scope.mData.ContactPerson !== null && $scope.mData.PhoneContactPerson1 !== null && $scope.mData.WoCategoryId !== null && $scope.mData.Km !== null) &&
                                    ($scope.mData.AppointmentDate !== undefined && $scope.mData.ContactPerson !== undefined && $scope.mData.PhoneContactPerson1 !== undefined && $scope.mData.WoCategoryId !== undefined && $scope.mData.Km !== undefined)&& 
                                    ($scope.mData.AppointmentDate !== '' && $scope.mData.ContactPerson !== '' && $scope.mData.PhoneContactPerson1 !== '' && $scope.mData.WoCategoryId !== '' && $scope.mData.Km !== '')) {
                                    for (var i = 0; i < tmpGrid.length; i++) {
                                        tmpGrid[i].JobParts = [];
                                        angular.forEach(tmpGrid[i].child, function(v, k) {
                                            tmpGrid[i].JobParts[k] = v;
                                            tmpGrid[i].JobParts[k].Price = tmpGrid[i].JobParts[k].RetailPrice;
                                            if (tmpGrid[i].JobParts[k].PaidById == 2277 || tmpGrid[i].JobParts[k].PaidById == 30 || tmpGrid[i].JobParts[k].PaidById == 31) {
                                                tmpGrid[i].JobParts[k].minimalDp = 0;
                                            }
                                            tmpGrid[i].JobParts[k].DPRequest = tmpGrid[i].JobParts[k].DownPayment;
                                            if (tmpGrid[i].JobParts[k].JobPartsId == undefined || tmpGrid[i].JobParts[k].JobPartsId == null) {
                                                tmpGrid[i].JobParts[k].JobPartsId = 0;
                                            }
                                            delete tmpGrid[i].JobParts[k].ETA;
                                            delete tmpGrid[i].JobParts[k].PaidBy;
                                            // delete tmpGrid[i].JobParts[k].Dp;
                                            delete tmpGrid[i].child;
                                        })
                                        if ($scope.mData.JobId !== undefined && $scope.mData.JobId !== null) {
                                            tmpGrid[i].JobId = $scope.mData.JobId;
                                        } else {
                                            tmpGrid[i].JobId = 0;
                                        }
                                        delete tmpGrid[i].PaidBy;
                                        delete tmpGrid[i].tmpTaskId;

                                    }
                                    console.log("=====>", tmpGrid);
                                    var tmpKm = angular.copy($scope.mData.Km).toString();
                                    var backupKm = angular.copy($scope.mData.Km);
                                    if (tmpKm.includes(",") == true) {
                                        tmpKm = tmpKm.split(',');
                                        if (tmpKm.length > 1) {
                                            tmpKm = tmpKm.join('');
                                        } else {
                                            tmpKm = tmpKm[0];
                                        }
                                    }
                                    tmpKm = parseInt(tmpKm);
                                    $scope.mData.Km = tmpKm;





                                    //pindahan dari factory
                                    for(var i in tmpGrid){
                                        // if(tmpGrid[i].PaidById == 2277 || tmpGrid[i].PaidById == 30 || tmpGrid[i].PaidById == 31 ||  tmpGrid[i].PaidById == 32){
                                        if(tmpGrid[i].PaidById == 2277 || tmpGrid[i].PaidById == 30){
                                            tmpGrid[i].Discount = 0;
                                            tmpGrid[i].DiscountTypeId = -1;
                                            if(tmpGrid[i].JobParts.length > 0 || tmpGrid[i].JobPart !== null || tmpGrid[i].JobParts !== undefined ){
                                                for(var j in tmpGrid[i].JobParts){
                                                    // if(tmpGrid[i].JobParts[j].PaidById == 2277 || tmpGrid[i].JobParts[j].PaidById == 30 || tmpGrid[i].JobParts[j].PaidById == 31 ||  tmpGrid[i].PaidById == 32){
                                                    if(tmpGrid[i].JobParts[j].PaidById == 2277 || tmpGrid[i].JobParts[j].PaidById == 30){
                                                        tmpGrid[i].JobParts[j].Discount = 0;
                                                        tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                                    }else{
                                                        // if(tmpGrid[i].JobParts[j].MaterialTypeId == 1){
                                                        // //    tmpGrid[i].JobParts[j].Discount = data.disParts;
                                                        //     tmpGrid[i].JobParts[j].DiscountTypeId = 10;
                                                        // }else{
                                                        //     tmpGrid[i].JobParts[j].Discount = 0;
                                                        //     tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                                        // }

                                                        //req by pak eko bahan tetap dapet diskon 08-juli-2020
                                                        tmpGrid[i].JobParts[j].DiscountTypeId = 10;
                                                    }
                                                }
                                            }
                                        }else{
                                            // tmpGrid[i].Discount = data.disTask;
                                            tmpGrid[i].DiscountTypeId = 10;
                                            if(tmpGrid[i].JobParts.length > 0 || tmpGrid[i].JobPart !== null || tmpGrid[i].JobParts !== undefined ){
                                                for(var j in tmpGrid[i].JobParts){
                                                    if(tmpGrid[i].JobParts[j].PaidById == 2277 || tmpGrid[i].JobParts[j].PaidById == 30){
                                                        tmpGrid[i].JobParts[j].Discount = 0;
                                                        tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                                    }else{
                                                        // if(tmpGrid[i].JobParts[j].MaterialTypeId == 1){
                                                        // //    tmpGrid[i].JobParts[j].Discount = data.disParts;
                                                        //     tmpGrid[i].JobParts[j].DiscountTypeId = 10;
                                                        // }else{
                                                        //     tmpGrid[i].JobParts[j].Discount = 0;
                                                        //     tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                                        // }

                                                        //req by pak eko bahan tetap dapet diskon 08-juli-2020
                                                        tmpGrid[i].JobParts[j].DiscountTypeId = 10;
                                                    }
                                                }
                                            }
                                        }


                                        for(var y in tmpGrid[i].JobParts){
                                            tmpGrid[i].JobParts[y].Satuan = tmpGrid[i].JobParts[y].SatuanName;
                                        }
                                    }
                                    //pindahan dari factory





                                    $scope.mData.JobTask = tmpGrid;
                                    $scope.mData.JobComplaint = $scope.JobComplaint;
                                    $scope.mData.JobRequest = $scope.JobRequest;
                                    // $scope.mDataDetail.LicensePlate.toUpperCase();
                                    $scope.mData.detail = $scope.mDataDetail;
                                    // $scope.mData.PoliceNumber = $scope.mDataDetail.LicensePlate.toUpperCase();
                                    var data = $scope.mData;
                                    var tmpAppointmentDate = data.AppointmentDate;
                                    tmpAppointmentDate = new Date(tmpAppointmentDate);
                                    var finalDate
                                    var yyyy = tmpAppointmentDate.getFullYear().toString();
                                    var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                                    var dd = tmpAppointmentDate.getDate().toString();
                                    finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                                    $scope.mData.AppointmentDate = $scope.changeFormatDate($scope.mData.AppointmentDate);
                                    $scope.mData.PlanDateFinish = $scope.changeFormatDate($scope.mData.PlanDateFinish);
                                    $scope.mData.PlanDateStart = $scope.changeFormatDate($scope.mData.PlanDateStart);
                                    tmpPrediagnose.ScheduledTime = $filter('date')($scope.mData.PrediagScheduledTime, 'yyyy-MM-dd HH:mm:ss');
                                    tmpPrediagnose.StallId = $scope.mData.PrediagStallId;
                                    tmpPrediagnose.Status = $scope.mData.StatusPreDiagnose;
                                    // console.log("n", n);
                                    // ===============Data DP ================
                                    $scope.mData.totalDpDefault = $scope.totalDpDefault;
                                    $scope.mData.totalDp = $scope.totalDp;
                                    $scope.mData.FinalDP = $scope.totalDp;
                                    // ===============Appointment Date========
                                    var tmpfirstPlanStart = angular.copy($scope.firstPlanStart);
                                    var tmpfirstPlanFinish = angular.copy($scope.firstPlanFinish);
                                    var tmpPlanStart = $scope.mData.PlanStart;
                                    var tmpPlanFinish = $scope.mData.PlanFinish;
                                    tmpfirstPlanStart = tmpfirstPlanStart.split(':');
                                    tmpfirstPlanFinish = tmpfirstPlanFinish.split(':');
                                    tmpPlanStart = tmpPlanStart.split(':');
                                    tmpPlanFinish = tmpPlanFinish.split(':');
                                    var today = new Date();
                                    var myFirstPlanStart = new Date(today.getFullYear(), today.getMonth(), today.getDate(), tmpfirstPlanStart[0], tmpfirstPlanStart[1], 0);
                                    var myFirstPlanFinish = new Date(today.getFullYear(), today.getMonth(), today.getDate(), tmpfirstPlanFinish[0], tmpfirstPlanFinish[1], 0);
                                    var myPlanStart = new Date(today.getFullYear(), today.getMonth(), today.getDate(), tmpPlanStart[0], tmpPlanStart[1], 0);
                                    var myPlanFinish = new Date(today.getFullYear(), today.getMonth(), today.getDate(), tmpPlanFinish[0], tmpPlanFinish[1], 0);
                                    console.log("$scope.mData ready to save", $scope.mData);
                                    console.log("$scope.totalDpDefault", $scope.totalDpDefault);
                                    console.log("$scope.totalDp", $scope.totalDpDefault);
                                    console.log("tmpPrediagnose ready to save", tmpPrediagnose);
                                    // =======================================
                                    if ($scope.totalDpDefault !== 0 && ($scope.totalDp < $scope.totalDpDefault)) {
                                        AppointmentBpService.checkApproval($scope.totalDp).then(function(res) {
                                            console.log("ressss checkApproval", res.data);
                                            var ApproverRoleID = res.data;
                                            if (res.data !== -1) {
                                                if (myFirstPlanStart.getTime() === myPlanStart.getTime() || myFirstPlanFinish.getTime() === myPlanFinish.getTime()) {
                                                    console.log("ini edit doang harusnya");
                                                    $scope.mData.AppointmentDate = $scope.changeFormatDate($scope.mData.AppointmentDate);
                                                    $scope.mData.PlanDateFinish = $scope.changeFormatDate($scope.mData.PlanDateFinish);
                                                    $scope.mData.PlanDateStart = $scope.changeFormatDate($scope.mData.PlanDateStart);
                                                    FollowUpBpService.update($scope.mData).then(function(res) {
                                                        if (res.data == -1) {
                                                            bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                                            $scope.mData.Km = backupKm;
                                                            $scope.loadingSimpan = 0
                                                        } else if (res.data == -2) {
                                                            bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                                            $scope.mData.Km = backupKm;
                                                            $scope.loadingSimpan = 0
                                                        } else {

                                                            //CR4
                                                            console.log('createAppointment 1===>',res);
                                                            console.log('tampung estimasi buat updet detil ===>',tampungEstimasi);
                                                            for (var x in tampungEstimasi){
                                                                tampungEstimasi[x].IsWO = 0;
                                                                if(tampungEstimasi[x].EstimationNo == null && tampungEstimasi[x].EstimationBPId == null){
                                                                    tampungEstimasi.splice(x,1);
                                                                }
                                                            }
                                                            var Jsonparser = res.data.ResponseMessage.split("#");
                                                            var JobsId = parseInt(Jsonparser[1]);
                                                            AppointmentBpService.UpdateJobAppointmentAddEstimasiId(JobsId, tampungEstimasi).then(function(res){
                                                                console.log('updatejobappointmentaddestimationid 1===>',res.data);
                                                            },function(err){
                                                                console.log('updatejobappointmentaddestimationid ERROR 1===>',res.data);
                                                            });
                                                            //CR4

                                                            // AppointmentGrService.createPreadiagnose($scope.mData.JobId).then(function(res) {

                                                            // });
                                                            console.log("abis save", res);
                                                            // =====================
                                                            var tmpDataJob = angular.copy($scope.mData);
                                                            var tmpJobId = res.data.ResponseMessage;
                                                            tmpJobId = tmpJobId.split('#');
                                                            console.log("tmpJobId", tmpJobId[1]);
                                                            tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                                            tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                                            console.log("tmpJobId", tmpJobId[1]);
                                                            console.log("tmpPrediagnose", tmpPrediagnose);
                                                            console.log("tmpPrediagnose.PrediagnoseId", tmpPrediagnose.PrediagnoseId);
                                                            // =======================
                                                            $scope.saveApprovalDP(tmpDataJob);
                                                            // =======================
                                                            console.log("tmpPrediagnose", tmpPrediagnose);
                                                            tmpPrediagnose.JobId = parseInt(tmpJobId[1]);
                                                            // 
                                                            if (tmpPrediagnose.PrediagStallId !== undefined && tmpPrediagnose.PrediagStallId !== null) {
                                                                if (tmpPrediagnose.PrediagnoseId !== null && tmpPrediagnose.PrediagnoseId !== undefined) {
                                                                    AppointmentGrService.updatePrediagnose(tmpPrediagnose).then(function(res) {

                                                                    });
                                                                } else {
                                                                    AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                                                    });
                                                                };
                                                            }
                                                            console.log("abis save", res);

                                                            // //kirim notifikasi
                                                            // var DataNotif = {};
                                                            // var messagetemp = "";
                                                            // messagetemp =   "No. Appointment : " + $scope.mData.AppointmentNo  + " <br> " 
                                                            //                 +", No. Polisi : " + $scope.mData.PoliceNumber + "  <br> " 
                                                            //                 +", Periode Awal : "+ myFirstPlanStart + "  <br> "
                                                            //                 +", Periode Akhir : "+ myFirstPlanFinish + " <br> " ;



                                                            // DataNotif.Message = messagetemp;

                                                            // FollowUpBpService.sendNotif(DataNotif, 1122, 17).then(
                                                            // function(res) {

                                                            // },
                                                            //     function(err) {
                                                            //     //console.log("err=>", err);
                                                            // });


                                                            $scope.formApi.setMode('grid');
                                                            $scope.getData();
                                                        }
                                                    }, function(err) {
                                                        $scope.mData.Km = backupKm;
                                                    });
                                                } else {
                                                    console.log("Reschedule ini masuknya 1");
                                                    var RemoveVehicleType = delete $scope.mData.VehicleType;
                                                    console.log("$scope.mData", $scope.mData);
                                                    $scope.mData.AppointmentDate = $scope.changeFormatDate($scope.mData.AppointmentDate);
                                                    $scope.mData.PlanDateFinish = $scope.changeFormatDate($scope.mData.PlanDateFinish);
                                                    $scope.mData.PlanDateStart = $scope.changeFormatDate($scope.mData.PlanDateStart);
                                                    delete $scope.mData.StallId
                                                    FollowUpBpService.create($scope.mData).then(function(res) {
                                                        if (res.data == -1) {
                                                            bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                                            $scope.mData.Km = backupKm;
                                                            $scope.loadingSimpan = 0
                                                        } else if (res.data == -2) {
                                                            bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                                            $scope.mData.Km = backupKm;
                                                            $scope.loadingSimpan = 0
                                                        } else {
                                                            console.log("abis save", res);
                                                            // =====================
                                                            var tmpDataJob = angular.copy($scope.mData);
                                                            var tmpJobId = res.data.ResponseMessage;
                                                            tmpJobId = tmpJobId.split('#');
                                                            console.log("tmpJobId", tmpJobId[1]);
                                                            tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                                            tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                                            console.log("tmpJobId", tmpJobId[1]);
                                                            console.log("tmpPrediagnose", tmpPrediagnose);
                                                            console.log("tmpPrediagnose.PrediagnoseId", tmpPrediagnose.PrediagnoseId);
                                                            // =======================
                                                            $scope.saveApprovalDP(tmpDataJob);
                                                            // =======================
                                                            console.log("tmpPrediagnose", tmpPrediagnose);
                                                            tmpPrediagnose.JobId = parseInt(tmpJobId[1]);

                                                            if (tmpPrediagnose.PrediagStallId !== undefined && tmpPrediagnose.PrediagStallId !== null) {
                                                                if (tmpPrediagnose.PrediagnoseId !== null && tmpPrediagnose.PrediagnoseId !== undefined) {
                                                                    AppointmentGrService.updatePrediagnose(tmpPrediagnose).then(function(res) {

                                                                    });
                                                                } else {
                                                                    AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                                                    });
                                                                };
                                                            }

                                                            var noAppointment = $scope.mData.AppointmentNo;
                                                            FollowUpGrService.getAppointmentDate(noAppointment).then(function(res) {
                                                                // var dataRes = res.data.Result;
                                                                console.log('Data Appointment Date ', res);
                                                                var hasil = res.data;

                                                                for (var i in hasil) {
                                                                    if (hasil[i].Status === 1) {
                                                                        $scope.tglApp = hasil[i].AppointmentDate;
                                                                        $scope.startApp = hasil[i].PlanStart;
                                                                    } else {
                                                                        $scope.tglRes = hasil[i].AppointmentDate;
                                                                        $scope.startRes = hasil[i].PlanStart;
                                                                    }
                                                                }

                                                                // APPOINTMENT DATE

                                                                var today = new Date($scope.tglApp);
                                                                var year = today.getFullYear();
                                                                var month = today.getMonth() + 1;
                                                                var day = today.getDate();
                                                                month = checkTime(month);
                                                                day = checkTime(day);

                                                                var timeApp = $scope.startApp.split(":");
                                                                var hh = timeApp[0];
                                                                var mm = timeApp[1];

                                                                var lastDateNya = day + '-' + month + '-' + year;
                                                                var timeDateNya = hh + ":" + mm;

                                                                var namaHari = {
                                                                    0: "Minggu",
                                                                    1: "Senin",
                                                                    2: "Selasa",
                                                                    3: "Rabu",
                                                                    4: "Kamis",
                                                                    5: "Jumat",
                                                                    6: "Sabtu"
                                                                };
                                                                var hariKe = today.getDay();
                                                                var hari = namaHari[hariKe];

                                                                // RESCHEDULE

                                                                var today1 = new Date($scope.tglRes);
                                                                var year1 = today1.getFullYear();
                                                                var month1 = today1.getMonth() + 1;
                                                                var day1 = today1.getDate();
                                                                month1 = checkTime(month1);
                                                                day1 = checkTime(day1);

                                                                var timeRes = $scope.startRes.split(":");
                                                                var hh1 = timeRes[0];
                                                                var mm1 = timeRes[1];

                                                                var lastDateNya1 = day1 + '-' + month1 + '-' + year1;
                                                                var timeDateNya1 = hh1 + ":" + mm1;

                                                                var hariKe1 = today1.getDay();
                                                                var hari1 = namaHari[hariKe1];

                                                                console.log('tglApp', $scope.tglApp + " " + $scope.startApp);
                                                                console.log('conv tglApp', hari + ", " + lastDateNya + " " + timeDateNya + " WIB");
                                                                console.log('tglRes', $scope.tglRes + " " + $scope.startRes);
                                                                console.log('conv tglRes', hari1 + ", " + lastDateNya1 + " " + timeDateNya1 + " WIB");

                                                                $scope.dateAppointment = hari + ", " + lastDateNya + " " + timeDateNya + " WIB";
                                                                $scope.dateReschedule = hari1 + ", " + lastDateNya1 + " " + timeDateNya1 + " WIB";

                                                                console.log('dateAppointment', $scope.dateAppointment);
                                                                console.log('dateReschedule', $scope.dateReschedule);

                                                                //kirim notifikasi
                                                                var DataNotif = {};
                                                                var messagetemp = "";
                                                                messagetemp = "No. Appointment : " + $scope.mData.AppointmentNo +
                                                                    " <br> No. Polisi : " + $scope.mData.PoliceNumber
                                                                    // +" <br> Periode Awal : "+ myFirstPlanStart 
                                                                    // +" <br> Periode Akhir : "+ myFirstPlanFinish;
                                                                    +
                                                                    " <br> Tanggal Appointment : " + $scope.dateAppointment +
                                                                    " <br> Tanggal Reschedule : " + $scope.dateReschedule;



                                                                DataNotif.Message = messagetemp;

                                                                console.log('DataNotif ', DataNotif);

                                                                FollowUpGrService.sendNotifRole(DataNotif, 1123, 17).then(
                                                                    function(res) {

                                                                    },
                                                                    function(err) {
                                                                        //console.log("err=>", err);
                                                                    });
                                                            });

                                                            $scope.formApi.setMode('grid');
                                                            $scope.getData();
                                                        }
                                                    }, function(err) {
                                                        $scope.mData.Km = backupKm;
                                                    });
                                                };
                                            } else {
                                                $scope.mData.Km = backupKm;
                                            }
                                        });
                                    } else {
                                        if (myFirstPlanStart.getTime() === myPlanStart.getTime() || myFirstPlanFinish.getTime() === myPlanFinish.getTime()) {
                                            // console.log("ini edit doang harusnya");
                                            $scope.mData.AppointmentDate = $scope.changeFormatDate($scope.mData.AppointmentDate);
                                            $scope.mData.PlanDateFinish = $scope.changeFormatDate($scope.mData.PlanDateFinish);
                                            $scope.mData.PlanDateStart = $scope.changeFormatDate($scope.mData.PlanDateStart);
                                            // if (AsbGrFactory.checkChipFinished($scope.boardName)) {
                                            // var splitChip = AsbGrFactory.getChipData($scope.boardName);
                                            // splitChip = splitChip.split(';');
                                            // var tmpJobSplitChip = [];
                                            // for(var i in splitChip){
                                            //     var tmpHasilSplit = splitChip[i].split('|');
                                            //     tmpJobSplitChip.push({
                                            //         JobSplitId : tmpHasilSplit[5],
                                            //         PlanStart :tmpHasilSplit[1],
                                            //         PlanFinish : tmpHasilSplit[2],
                                            //         PlanDateStart : $scope.changeFormatDate(new Date(tmpHasilSplit[0])),
                                            //         PlanDateFinish : $scope.changeFormatDate(new Date(tmpHasilSplit[0])),
                                            //         StallId : tmpHasilSplit[3],
                                            //         isSplitActive : 1
                                            //     })
                                            // }
                                            // if(tmpJobSplitChip.length > 1){
                                            //     $scope.mData.JobListSplitChip = tmpJobSplitChip;
                                            // }
                                            console.log("ini edit doang harusnya  111111111", $scope.mData);
                                            FollowUpBpService.update($scope.mData).then(function(res) {
                                                if (res.data == -1) {
                                                    bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                                    $scope.mData.Km = backupKm;
                                                    $scope.loadingSimpan = 0
                                                } else if (res.data == -2) {
                                                    bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                                    $scope.mData.Km = backupKm;
                                                    $scope.loadingSimpan = 0
                                                } else {

                                                    //CR4
                                                    console.log('createAppointment 1===>',res);
                                                    console.log('tampung estimasi buat updet detil ===>',tampungEstimasi);
                                                    for (var x in tampungEstimasi){
                                                        tampungEstimasi[x].IsWO = 0;
                                                        if(tampungEstimasi[x].EstimationNo == null && tampungEstimasi[x].EstimationBPId == null){
                                                            tampungEstimasi.splice(x,1);
                                                        }
                                                    }
                                                    var Jsonparser = res.data.ResponseMessage.split("#");
                                                    var JobsId = parseInt(Jsonparser[1]);
                                                    AppointmentBpService.UpdateJobAppointmentAddEstimasiId(JobsId, tampungEstimasi).then(function(res){
                                                        console.log('updatejobappointmentaddestimationid 1===>',res.data);
                                                    },function(err){
                                                        console.log('updatejobappointmentaddestimationid ERROR 1===>',res.data);
                                                    });
                                                    //CR4


                                                    // AppointmentGrService.createPreadiagnose($scope.mData.JobId).then(function(res) {

                                                    // });
                                                    console.log("abis save", res);
                                                    // =====================
                                                    var tmpDataJob = angular.copy($scope.mData);
                                                    var tmpJobId = res.data.ResponseMessage;
                                                    tmpJobId = tmpJobId.split('#');
                                                    console.log("tmpJobId", tmpJobId[1]);
                                                    tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                                    // tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                                    console.log("tmpJobId", tmpJobId[1]);
                                                    console.log("tmpPrediagnose", tmpPrediagnose);
                                                    console.log("tmpPrediagnose.PrediagnoseId", tmpPrediagnose.PrediagnoseId);
                                                    // =======================
                                                    // $scope.saveApprovalDP(tmpDataJob);
                                                    // =======================
                                                    console.log("tmpPrediagnose", tmpPrediagnose);
                                                    tmpPrediagnose.JobId = parseInt(tmpJobId[1]);
                                                    // 
                                                    if (tmpPrediagnose.PrediagStallId !== undefined && tmpPrediagnose.PrediagStallId !== null) {
                                                        if (tmpPrediagnose.PrediagnoseId !== null && tmpPrediagnose.PrediagnoseId !== undefined) {
                                                            AppointmentGrService.updatePrediagnose(tmpPrediagnose).then(function(res) {

                                                            });
                                                        } else {
                                                            AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                                            });
                                                        };
                                                    }
                                                    console.log("abis save", res);
                                                    $scope.formApi.setMode('grid');
                                                    $scope.getData();
                                                }
                                            }, function(err) {
                                                $scope.mData.Km = backupKm;
                                            });
                                        } else {
                                            console.log("Reschedule ini masuknya 2");
                                            var RemoveVehicleType = delete $scope.mData.VehicleType;
                                            console.log("$scope.mData", $scope.mData);
                                            $scope.mData.AppointmentDate = $scope.changeFormatDate($scope.mData.AppointmentDate);
                                            $scope.mData.PlanDateFinish = $scope.changeFormatDate($scope.mData.PlanDateFinish);
                                            $scope.mData.PlanDateStart = $scope.changeFormatDate($scope.mData.PlanDateStart);
                                            delete $scope.mData.StallId;
                                            FollowUpBpService.create($scope.mData).then(function(res) {
                                                if (res.data == -1) {
                                                    bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                                    $scope.mData.Km = backupKm;
                                                    $scope.loadingSimpan = 0
                                                } else if (res.data == -2) {
                                                    bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                                    $scope.mData.Km = backupKm;
                                                    $scope.loadingSimpan = 0
                                                } else {
                                                    console.log("abis save", res);
                                                    // =====================
                                                    var tmpDataJob = angular.copy($scope.mData);
                                                    var tmpJobId = res.data.ResponseMessage;
                                                    tmpJobId = tmpJobId.split('#');
                                                    console.log("tmpJobId", tmpJobId[1]);
                                                    tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                                    // tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                                    console.log("tmpJobId", tmpJobId[1]);
                                                    console.log("tmpPrediagnose", tmpPrediagnose);
                                                    console.log("tmpPrediagnose.PrediagnoseId", tmpPrediagnose.PrediagnoseId);
                                                    // =======================
                                                    // $scope.saveApprovalDP(tmpDataJob);
                                                    // =======================
                                                    console.log("tmpPrediagnose", tmpPrediagnose);
                                                    tmpPrediagnose.JobId = parseInt(tmpJobId[1]);

                                                    if (tmpPrediagnose.PrediagStallId !== undefined && tmpPrediagnose.PrediagStallId !== null) {
                                                        if (tmpPrediagnose.PrediagnoseId !== null && tmpPrediagnose.PrediagnoseId !== undefined) {
                                                            AppointmentGrService.updatePrediagnose(tmpPrediagnose).then(function(res) {

                                                            });
                                                        } else {
                                                            AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                                            });
                                                        };
                                                    }

                                                    var noAppointment = $scope.mData.AppointmentNo;
                                                    FollowUpGrService.getAppointmentDate(noAppointment).then(function(res) {
                                                        // var dataRes = res.data.Result;
                                                        console.log('Data Appointment Date ', res);
                                                        var hasil = res.data;

                                                        for (var i in hasil) {
                                                            if (hasil[i].Status === 1) {
                                                                $scope.tglApp = hasil[i].AppointmentDate;
                                                                $scope.startApp = hasil[i].PlanStart;
                                                            } else {
                                                                $scope.tglRes = hasil[i].AppointmentDate;
                                                                $scope.startRes = hasil[i].PlanStart;
                                                            }
                                                        }

                                                        // APPOINTMENT DATE

                                                        var today = new Date($scope.tglApp);
                                                        var year = today.getFullYear();
                                                        var month = today.getMonth() + 1;
                                                        var day = today.getDate();
                                                        month = checkTime(month);
                                                        day = checkTime(day);

                                                        var timeApp = $scope.startApp.split(":");
                                                        var hh = timeApp[0];
                                                        var mm = timeApp[1];

                                                        var lastDateNya = day + '-' + month + '-' + year;
                                                        var timeDateNya = hh + ":" + mm;

                                                        var namaHari = {
                                                            0: "Minggu",
                                                            1: "Senin",
                                                            2: "Selasa",
                                                            3: "Rabu",
                                                            4: "Kamis",
                                                            5: "Jumat",
                                                            6: "Sabtu"
                                                        };
                                                        var hariKe = today.getDay();
                                                        var hari = namaHari[hariKe];

                                                        // RESCHEDULE

                                                        var today1 = new Date($scope.tglRes);
                                                        var year1 = today1.getFullYear();
                                                        var month1 = today1.getMonth() + 1;
                                                        var day1 = today1.getDate();
                                                        month1 = checkTime(month1);
                                                        day1 = checkTime(day1);

                                                        var timeRes = $scope.startRes.split(":");
                                                        var hh1 = timeRes[0];
                                                        var mm1 = timeRes[1];

                                                        var lastDateNya1 = day1 + '-' + month1 + '-' + year1;
                                                        var timeDateNya1 = hh1 + ":" + mm1;

                                                        var hariKe1 = today1.getDay();
                                                        var hari1 = namaHari[hariKe1];

                                                        console.log('tglApp', $scope.tglApp + " " + $scope.startApp);
                                                        console.log('conv tglApp', hari + ", " + lastDateNya + " " + timeDateNya + " WIB");
                                                        console.log('tglRes', $scope.tglRes + " " + $scope.startRes);
                                                        console.log('conv tglRes', hari1 + ", " + lastDateNya1 + " " + timeDateNya1 + " WIB");

                                                        $scope.dateAppointment = hari + ", " + lastDateNya + " " + timeDateNya + " WIB";
                                                        $scope.dateReschedule = hari1 + ", " + lastDateNya1 + " " + timeDateNya1 + " WIB";

                                                        console.log('dateAppointment', $scope.dateAppointment);
                                                        console.log('dateReschedule', $scope.dateReschedule);

                                                        //kirim notifikasi
                                                        var DataNotif = {};
                                                        var messagetemp = "";
                                                        messagetemp = "No. Appointment : " + $scope.mData.AppointmentNo +
                                                            " <br> No. Polisi : " + $scope.mData.PoliceNumber
                                                            // +" <br> Periode Awal : "+ myFirstPlanStart 
                                                            // +" <br> Periode Akhir : "+ myFirstPlanFinish;
                                                            +
                                                            " <br> Tanggal Appointment : " + $scope.dateAppointment +
                                                            " <br> Tanggal Reschedule : " + $scope.dateReschedule;



                                                        DataNotif.Message = messagetemp;

                                                        console.log('DataNotif ', DataNotif);

                                                        FollowUpGrService.sendNotifRole(DataNotif, 1123, 17).then(
                                                            function(res) {

                                                            },
                                                            function(err) {
                                                                //console.log("err=>", err);
                                                            });
                                                    });

                                                    $scope.formApi.setMode('grid');
                                                    $scope.getData();
                                                }
                                            }, function(err) {
                                                $scope.mData.Km = backupKm;
                                            });
                                        };
                                    }
                                } else {
                                    bsAlert.warning("Form masih ada yang belum terisi", "silahkan cek kembali");
                                    $scope.loadingSimpan = 0
                                };
                            } else {
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Status Appointment sudah berubah, silahkan refresh kembali"
                                });
                                $scope.loadingSimpan = 0
                            }
                        })

                    }
                    
                    
                },
                title: 'Simpan',
                icon: 'fa fa-fw fa-save',
                //rightsBit: 1
            },
            {
                actionType: 'back', //Use 'Back Action' of bsForm
                title: 'Back',
                func: function(row, formScope) {

                    }
                    //icon: 'fa fa-fw fa-edit',
            },
        ]


        $scope.actionButtonSettingsDetailOld = [{
                func: function(row, formScope) {
                    console.log("mData", $scope.mData);
                    console.log("mdata di row", row);
                    var tmpGrid = [];
                    tmpGrid = angular.copy(gridTemp);
                    if ($scope.mData.AppointmentDate !== null && $scope.mData.ContactPerson !== null && $scope.mData.PhoneContactPerson1 !== null && $scope.mData.WoCategoryId !== null && $scope.mData.Km !== null) {
                        for (var i = 0; i < tmpGrid.length; i++) {
                            tmpGrid[i].JobParts = [];
                            angular.forEach(tmpGrid[i].child, function(v, k) {
                                tmpGrid[i].JobParts[k] = v;
                                tmpGrid[i].JobParts[k].Price = tmpGrid[i].JobParts[k].RetailPrice;
                                tmpGrid[i].JobParts[k].DownPayment = tmpGrid[i].JobParts[k].minimalDp;
                                delete tmpGrid[i].JobParts[k].ETA;
                                delete tmpGrid[i].JobParts[k].PaidBy;
                                // delete tmpGrid[i].JobParts[k].Dp;
                                delete tmpGrid[i].child;
                            })
                            delete tmpGrid[i].PaidBy;
                            delete tmpGrid[i].tmpTaskId;

                        }
                        console.log("=====>", tmpGrid);
                        var tmpKm = angular.copy($scope.mData.Km).toString();
                        if (tmpKm.includes(",") == true) {
                            tmpKm = tmpKm.split(',');
                            if (tmpKm.length > 1) {
                                tmpKm = tmpKm.join('');
                            } else {
                                tmpKm = tmpKm[0];
                            }
                        }
                        tmpKm = parseInt(tmpKm);
                        $scope.mData.Km = tmpKm;
                        $scope.mData.JobTask = tmpGrid;
                        $scope.mData.JobComplaint = $scope.JobComplaint;
                        $scope.mData.JobRequest = $scope.JobRequest;
                        $scope.mData.detail = $scope.mDataDetail;
                        var data = $scope.mData;
                        var tmpAppointmentDate = data.AppointmentDate;
                        tmpAppointmentDate = new Date(tmpAppointmentDate);
                        var finalDate
                        var yyyy = tmpAppointmentDate.getFullYear().toString();
                        var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
                        var dd = tmpAppointmentDate.getDate().toString();
                        finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                        $scope.mData.AppointmentDate = finalDate;
                        // console.log("n", n);
                        // ===============Appointment Date========
                        var tmpfirstPlanStart = angular.copy($scope.firstPlanStart);
                        var tmpfirstPlanFinish = angular.copy($scope.firstPlanFinish);
                        var tmpPlanStart = $scope.mData.PlanStart;
                        var tmpPlanFinish = $scope.mData.PlanFinish;
                        tmpfirstPlanStart = tmpfirstPlanStart.split(':');
                        tmpfirstPlanFinish = tmpfirstPlanFinish.split(':');
                        tmpPlanStart = tmpPlanStart.split(':');
                        tmpPlanFinish = tmpPlanFinish.split(':');
                        var today = new Date();
                        var tmpAppointmentDateForValidation = angular.copy($scope.mData.AppointmentDate);
                        tmpAppointmentDateForValidation = new Date(tmpAppointmentDateForValidation);
                        var myFirstAppointmentDate = new Date($scope.firstAppointmentDate.getFullYear(), $scope.firstAppointmentDate.getMonth(), $scope.firstAppointmentDate.getDate(), 0, 0, 0);
                        var myAppointmentDate = new Date(tmpAppointmentDateForValidation.getFullYear(), tmpAppointmentDateForValidation.getMonth(), tmpAppointmentDateForValidation.getDate(), 0, 0, 0);
                        var myFirstPlanStart = new Date(today.getFullYear(), today.getMonth(), today.getDate(), tmpfirstPlanStart[0], tmpfirstPlanStart[1], 0);
                        var myFirstPlanFinish = new Date(today.getFullYear(), today.getMonth(), today.getDate(), tmpfirstPlanFinish[0], tmpfirstPlanFinish[1], 0);
                        var myPlanStart = new Date(today.getFullYear(), today.getMonth(), today.getDate(), tmpPlanStart[0], tmpPlanStart[1], 0);
                        var myPlanFinish = new Date(today.getFullYear(), today.getMonth(), today.getDate(), tmpPlanFinish[0], tmpPlanFinish[1], 0);
                        console.log("$scope.mData ready to save", $scope.mData);
                        console.log('myFirstPlanStart', myFirstPlanStart);
                        console.log('myPlanStart', myPlanStart);
                        console.log('myFirstPlanFinish', myFirstPlanFinish);
                        console.log('myPlanFinish', myPlanFinish);
                        // ====
                        console.log('myFirstPlanStart.getTime()', myFirstPlanStart.getTime());
                        console.log('myPlanStart.getTime()', myPlanStart.getTime());
                        console.log('myFirstPlanFinish.getTime()', myFirstPlanFinish.getTime());
                        console.log('myPlanFinish.getTime()', myPlanFinish.getTime());
                        delete $scope.mData.VehicleType;
                        // =======================================
                        if (myFirstPlanStart.getTime() === myPlanStart.getTime() && myFirstPlanFinish.getTime() === myPlanFinish.getTime() && myFirstAppointmentDate.getTime() === myAppointmentDate.getTime()) {
                            console.log("ini edit doang harusnya");
                            // FollowUpBpService.update($scope.mData).then(function(res) {
                            //     // AppointmentGrService.createPreadiagnose($scope.mData.JobId).then(function(res) {

                            //     // });
                            //     console.log("abis save", res);
                            //     $scope.formApi.setMode('grid');
                            //     $scope.getData();
                            // });
                            $scope.mData.AppointmentDate = $scope.changeFormatDate($scope.mData.AppointmentDate);
                            $scope.mData.PlanDateFinish = $scope.changeFormatDate($scope.mData.PlanDateFinish);
                            $scope.mData.PlanDateStart = $scope.changeFormatDate($scope.mData.PlanDateStart);
                            FollowUpBpService.update($scope.mData).then(function(res) {
                                if (res.data == -1) {
                                    bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                    $scope.mData.Km = backupKm;
                                } else if (res.data == -2) {
                                    bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                    $scope.mData.Km = backupKm;
                                } else {

                                    //CR4
                                    console.log('createAppointment 1===>',res);
                                    console.log('tampung estimasi buat updet detil ===>',tampungEstimasi);
                                    for (var x in tampungEstimasi){
                                        tampungEstimasi[x].IsWO = 0;
                                        if(tampungEstimasi[x].EstimationNo == null && tampungEstimasi[x].EstimationBPId == null){
                                            tampungEstimasi.splice(x,1);
                                        }
                                    }
                                    var Jsonparser = res.data.ResponseMessage.split("#");
                                    var JobsId = parseInt(Jsonparser[1]);
                                    AppointmentBpService.UpdateJobAppointmentAddEstimasiId(JobsId, tampungEstimasi).then(function(res){
                                        console.log('updatejobappointmentaddestimationid 1===>',res.data);
                                    },function(err){
                                        console.log('updatejobappointmentaddestimationid ERROR 1===>',res.data);
                                    });
                                    //CR4

                                    
                                    // AppointmentGrService.createPreadiagnose($scope.mData.JobId).then(function(res) {

                                    // });
                                    console.log("abis save", res);
                                    // =====================
                                    var tmpDataJob = angular.copy($scope.mData);
                                    var tmpJobId = res.data.ResponseMessage;
                                    tmpJobId = tmpJobId.split('#');
                                    console.log("tmpJobId", tmpJobId[1]);
                                    tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                    // tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                    console.log("tmpJobId", tmpJobId[1]);
                                    console.log("tmpPrediagnose", tmpPrediagnose);
                                    console.log("tmpPrediagnose.PrediagnoseId", tmpPrediagnose.PrediagnoseId);
                                    // =======================
                                    // $scope.saveApprovalDP(tmpDataJob);
                                    // =======================
                                    console.log("tmpPrediagnose", tmpPrediagnose);
                                    tmpPrediagnose.JobId = parseInt(tmpJobId[1]);
                                    // 
                                    if (tmpPrediagnose.PrediagStallId !== undefined && tmpPrediagnose.PrediagStallId !== null) {
                                        if (tmpPrediagnose.PrediagnoseId !== null && tmpPrediagnose.PrediagnoseId !== undefined) {
                                            AppointmentGrService.updatePrediagnose(tmpPrediagnose).then(function(res) {

                                            });
                                        } else {
                                            AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                            });
                                        };
                                    }
                                    console.log("abis save", res);
                                    $scope.formApi.setMode('grid');
                                    $scope.getData();
                                }
                            }, function(err) {
                                $scope.mData.Km = backupKm;
                            });
                        } else {
                            console.log("Reschedule ini masuknya 3");
                            // $scope.mData.UserIdEstimasi = angular.copy($scope.mData.StallId);
                            // // $scope.mData.StallId = null;
                            // FollowUpBpService.create($scope.mData).then(function(res) {
                            //     // AppointmentGrService.createPreadiagnose($scope.mData.JobId).then(function(res) {

                            //     // });
                            //     console.log("abis save", res);
                            //     $scope.formApi.setMode('grid');
                            //     $scope.getData();
                            // });
                            var RemoveVehicleType = delete $scope.mData.VehicleType;
                            console.log("$scope.mData", $scope.mData);
                            $scope.mData.AppointmentDate = $scope.changeFormatDate($scope.mData.AppointmentDate);
                            $scope.mData.PlanDateFinish = $scope.changeFormatDate($scope.mData.PlanDateFinish);
                            $scope.mData.PlanDateStart = $scope.changeFormatDate($scope.mData.PlanDateStart);
                            FollowUpBpService.create($scope.mData).then(function(res) {
                                if (res.data == -1) {
                                    bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                    $scope.mData.Km = backupKm;
                                } else if (res.data == -2) {
                                    bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                    $scope.mData.Km = backupKm;
                                } else {
                                    console.log("abis save", res);
                                    // =====================
                                    var tmpDataJob = angular.copy($scope.mData);
                                    var tmpJobId = res.data.ResponseMessage;
                                    tmpJobId = tmpJobId.split('#');
                                    console.log("tmpJobId", tmpJobId[1]);
                                    tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                    // tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                    console.log("tmpJobId", tmpJobId[1]);
                                    console.log("tmpPrediagnose", tmpPrediagnose);
                                    console.log("tmpPrediagnose.PrediagnoseId", tmpPrediagnose.PrediagnoseId);
                                    // =======================
                                    // $scope.saveApprovalDP(tmpDataJob);
                                    // =======================
                                    console.log("tmpPrediagnose", tmpPrediagnose);
                                    tmpPrediagnose.JobId = parseInt(tmpJobId[1]);

                                    if (tmpPrediagnose.PrediagStallId !== undefined && tmpPrediagnose.PrediagStallId !== null) {
                                        if (tmpPrediagnose.PrediagnoseId !== null && tmpPrediagnose.PrediagnoseId !== undefined) {
                                            AppointmentGrService.updatePrediagnose(tmpPrediagnose).then(function(res) {

                                            });
                                        } else {
                                            AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                            });
                                        };
                                    }
                                    $scope.formApi.setMode('grid');
                                    $scope.getData();
                                }
                            }, function(err) {
                                $scope.mData.Km = backupKm;
                            });
                        };

                    } else {
                        bsAlert.warning("Form masih ada yang belum terisi", "silahkan cek kembali");
                    }
                },
                title: 'Simpan',
                icon: 'fa fa-fw fa-save',
                //rightsBit: 1
            },
            {
                actionType: 'back', //Use 'Back Action' of bsForm
                title: 'Back',
                func: function(row, formScope) {

                    }
                    //icon: 'fa fa-fw fa-edit',
            },
        ]
        $scope.getAllParameter = function(item) {
            console.log("itemnya", item);
            FollowUpBpService.getPayment().then(function(res) {
                // pembayaran free service mau di ilangin di bp kata BU nya
                for (var j=0; j<res.data.Result.length; j++){
                    if (res.data.Result[j].MasterId == 2277){
                        res.data.Result.splice(j,1);
                    }
                }
                $scope.paymentData = res.data.Result;
                $scope.paymentDataNoWarranty = angular.copy(res.data.Result);
                for (var i=0; i<$scope.paymentDataNoWarranty.length; i++){
                    if ($scope.paymentDataNoWarranty[i].Name == 'Warranty'){
                        $scope.paymentDataNoWarranty.splice(i,1);
                    }
                }
            });
            AppointmentBpService.getInsurence().then(function(res) {
                console.log("res getInsurence", res.data);
                $scope.Asuransi = res.data;
            });
            // FollowUpBpService.getUnitMeasurement().then(function(res) {
            //     $scope.unitData = res.data.Result;
            //     $scope.unitDataGrid = [];
            //     var tmpUnit = $scope.unitData;
            //     // for (var i = 0; i < tmpUnit.length; i++) {
            //     //     var temp = {};
            //     //     temp[tmpUnit[i].MasterId] = tmpUnit[i].Name;
            //     //     $scope.unitDataGrid.push(temp);
            //     //     if (!$scope.unitDataGrid[tmpUnit[i].MasterId]) {
            //     //         $scope.unitDataGrid[tmpUnit[i].MasterId] = tmpUnit[i].Name;
            //     //     }
            //     //     console.log("$scope.unitDataGrid", $scope.unitDataGrid);
            //     // }
            //     console.log("$scope.unitData=====>", $scope.unitData);
            // });


            AppointmentBpService.getWoCategory().then(function(res) {
                console.log("reswo", res.data.Result);
                $scope.woCategory = res.data.Result;
            });
            FollowUpBpService.getTaskCategory().then(function(res) {
                $scope.taskCategory = res.data.Result;
            });
            FollowUpBpService.getDataGender().then(function(res) {
                console.log("result gender : ", res.data.Result);
                var temp = res.data.Result;
                for (var i = 0; i < temp.length; i++) {
                    temp[i].value = temp[i].CustomerGenderId;
                    temp[i].text = (temp[i].CustomerGenderName == "Laki-Laki" ? "L" : "P");
                }
                console.log("tmp", temp);
                $scope.genderData = temp;
            });
            MrsList.getDataCusRole().then(function(res) {
                console.log("result role : ", res.data.Result);
                $scope.RoleData = res.data.Result;
            });
            MrsList.getCCustomerCategory().then(function(res) {
                $scope.CustomerTypeData = res.data.Result;
            });
            RepairSupportActivityGR.getT1().then(function(res) {
                $scope.TFirst = res.data.Result;
                //console.log("T1", $scope.T1);
            });
            // $scope.CustomerTypeData = [{CustomerTypeId:1,CustomerTypeDesc:"Individu"},{CustomerTypeId:2,CustomerTypeDesc:"Institusi"}]
            // MrsList.getDataCusType().then(function(res) {
            //   console.log("result type : ",res.data);
            //   $scope.CustomerTypeData = res.dafta.Result;
            // });
            // MrsList.getDataModel().then(function(res) {
            //   console.log("result : ",res.data.Result);
            //   $scope.modelData = res.data.Result;
            // });
            if (item !== undefined) {
                if (item.ProvinceId !== undefined) {
                    // MrsList.getDataProvinceById(item.ProvinceId).then(function(res) {
                    //     console.log("result province : ", res.data.Result);
                    //     $scope.ProvinceData = res.data.Result;
                    // });
                    WOBP.getMLocationProvince().then(function(res) {
                        console.log("List Province====>", res.data.Result);
                        $scope.ProvinceData = res.data.Result;
                    })
                }
                if (item.CityRegencyId !== undefined) {
                    // MrsList.getDataCityById(item.CityRegencyId).then(function(res) {
                    //     console.log("result city : ", res.data.Result);
                    //     $scope.CityRegencyData = res.data.Result;
                    // });
                    WOBP.getMLocationCityRegency(item.ProvinceId).then(function(resu) {
                        $scope.CityRegencyData = resu.data.Result;
                    });
                }
                if (item.DistrictId !== undefined) {
                    // MrsList.getDataDistrictById(item.DistrictId).then(function(res) {
                    //     console.log("result district : ", res.data.Result);
                    //     $scope.DistrictData = res.data.Result;
                    // });
                    WOBP.getMLocationKecamatan(item.CityRegencyId).then(function(resu) {
                        $scope.DistrictData = resu.data.Result;
                    });
                }
                if (item.VillageId !== undefined) {
                    // MrsList.getDataVillageById(item.VillageId).then(function(res) {
                    //     console.log("result village : ", res.data.Result);
                    //     $scope.VillageData = res.data.Result;
                    // });
                    WOBP.getMLocationKelurahan(item.DistrictId).then(function(resu) {
                        $scope.VillageData = resu.data.Result;
                    });
                }
                if (item.GasTypeId !== null || item.GasTypeId !== undefined) {
                    $scope.mDataDetail.VehicleGasTypeId = item.GasTypeId;
                    console.log("$scope.mDataDetail.VehicleGasTypeId", $scope.mDataDetail.VehicleGasTypeId);
                }

                WO.getDataPKS(item.LicensePlate).then(function(res) {
                    $scope.dataPKS = res.data.Result;
                    console.log("PKS", $scope.dataPKS);
                });
            }
            MrsList.getInvitationMedia().then(function(res) {
                console.log("result media : ", res.data.Result);
                $scope.InvMediaData = res.data.Result;
            });
            MrsList.getMrsInvitation().then(function(res) {
                console.log("result mrs invit : ", res.data.Result);
                $scope.MrsInvData = res.data.Result;
            });
            // ------- Model
            // AppointmentGrService.getDataModel().then(function(res) {
            //   $scope.modelData = res.data.Result;
            // });
        };

        function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        $scope.getData = function() {
            if ($scope.filter.typeCategory <= 4 && $scope.filter.typeCategory != null) {
                console.log("tipe", $scope.filter.typeCategory);
                var today = new Date();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                var year = today.getFullYear();
                var month = today.getMonth() + 1;
                var dayLast = today.getDate();
                m = checkTime(m);
                s = checkTime(s);
                var lastDateNya = year + '-' + month + '-' + dayLast;
                var timeDateNya = h + '%20' + m + '%20' + s;
                return $q.resolve(
                    FollowUpBpService.getData(lastDateNya, lastDateNya, $scope.filter.typeCategory, lastDateNya, timeDateNya).then(function(res) {
                        xres = [];
                        if (res.data.Result.length > 0) {
                            console.log("res.data.Result", res.data.Result);
                            if ($scope.filter.typeCategory == 1) {
                                for (var i = 0; i < res.data.Result.length; i++) {
                                    if (res.data.Result[i].Status == 0 || res.data.Result[i].Status == 1) {
                                        xres.push(res.data.Result[i]);
                                    }
                                }
                            } else if ($scope.filter.typeCategory == 2) {
                                for (var i = 0; i < res.data.Result.length; i++) {
                                    if (res.data.Result[i].Status == 0 || res.data.Result[i].Status == 1) {
                                        xres.push(res.data.Result[i]);
                                    }
                                }
                            } else if ($scope.filter.typeCategory == 3) {
                                for (var i = 0; i < res.data.Result.length; i++) {
                                    if (res.data.Result[i].Status == 0 || res.data.Result[i].Status == 2) {
                                        xres.push(res.data.Result[i]);
                                    }
                                }
                            } else if ($scope.filter.typeCategory == 4) {
                                for (var i = 0; i < res.data.Result.length; i++) {
                                    if (res.data.Result[i].Status == 0 || res.data.Result[i].Status == 1 || res.data.Result[i].Status == 2) {
                                        xres.push(res.data.Result[i]);
                                    }
                                }
                            }
                            if ($scope.filter.typeCategory == 4) {
                                for (var i = 0; i < xres.length; i++) {
                                    xres[i].Category = "BP"
                                    if (xres[i].StatusApprovalId == 1) {
                                        xres[i].Xstatus = "Approve";
                                    } else if (xres[i].StatusApprovalId == 2) {
                                        xres[i].Xstatus = "Reject";
                                    } else if (xres[i].StatusApprovalId == 3) {
                                        xres[i].Xstatus = "Need Approval";
                                    }
                                    if ($scope.filter.typeCategory == 1) {
                                        xres[i].TypeFollow = "H-1";
                                    } else if ($scope.filter.typeCategory == 2) {
                                        xres[i].TypeFollow = "J-N";
                                    } else if ($scope.filter.typeCategory == 3) {
                                        xres[i].TypeFollow = "J+N";
                                    } else if ($scope.filter.typeCategory == 4) {
                                        xres[i].TypeFollow = "Need Approval";
                                    }
                                }
                            } else {
                                for (var i = 0; i < xres.length; i++) {
                                    xres[i].Category = "BP"
                                    if (xres[i].Status == 0 && xres[i].AppointmentNo !== null) {
                                        if (xres[i].isFoOneDay == 1 || xres[i].isFoOneHour == 1) {
                                            xres[i].Xstatus = "On Schedule";
                                        } else if (xres[i].NewAppointmentRel !== null) {
                                            xres[i].Xstatus = "Reschedule";
                                        } else {
                                            xres[i].Xstatus = "Outstanding";
                                        }
                                    } else if (xres[i].Status == 0 && xres[i].AppointmentNo == null) {
                                        xres[i].Xstatus = "Draft";
                                    } else if (xres[i].Status == 1) {
                                        xres[i].Xstatus = "Cancel";
                                    } else if (xres[i].Status == 2) {
                                        xres[i].Xstatus = "No Show";
                                    }
                                    if ($scope.filter.typeCategory == 1) {
                                        xres[i].TypeFollow = "H-1";
                                    } else if ($scope.filter.typeCategory == 2) {
                                        xres[i].TypeFollow = "J-N";
                                    } else if ($scope.filter.typeCategory == 3) {
                                        xres[i].TypeFollow = "J+N";
                                    } else if ($scope.filter.typeCategory == 4) {
                                        xres[i].TypeFollow = "Need Approval";
                                    }
                                    // } else if ($scope.filter.typeCategory == 3) {
                                    //     xres[i].TypeFollow = "No Show";
                                    // }
                                }
                            }
                        }
                        console.log("xresssss", xres);
                        var gridData = xres;
                        $scope.grid.data = gridData;
                        $scope.gridFrontParts.data = [];
                        $scope.loading = false;
                    })
                )
            } else if ($scope.filter.typeCategory > 4 && $scope.filter.firstDate != null && $scope.filter.lastDate != null) {
                var a = new Date($scope.filter.firstDate);
                var yearFirst = a.getFullYear();
                var monthFirst = a.getMonth() + 1;
                var dayFirst = a.getDate();
                var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;

                var b = new Date($scope.filter.lastDate);
                var yearLast = b.getFullYear();
                var monthLast = b.getMonth() + 1;
                var dayLast = b.getDate();
                var lastDate = yearLast + '-' + monthLast + '-' + dayLast;
                // =================
                var today = new Date();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                var year = today.getFullYear();
                var month = today.getMonth() + 1;
                var dayLast = today.getDate();
                m = checkTime(m);
                s = checkTime(s);
                var lastDateNya = year + '-' + month + '-' + dayLast;
                var timeDateNya = h + '%20' + m + '%20' + s;
                xres = [];
                return $q.resolve(
                    FollowUpBpService.getData(firstDate, lastDate, $scope.filter.typeCategory, lastDateNya, timeDateNya).then(function(res) {
                        xres = [];
                        if (res.data.Result.length > 0) {
                            for (var i = 0; i < res.data.Result.length; i++) {
                                // if (res.data.Result[i].Status == 0){
                                xres.push(res.data.Result[i]);
                                // }
                            }
                            for (var i = 0; i < xres.length; i++) {
                                if (xres[i].Status == 0 && xres[i].AppointmentNo !== null) {
                                    if (xres[i].isFoOneDay == 1 || xres[i].isFoOneHour == 1) {
                                        xres[i].Xstatus = "On Schedule";
                                    } else if (xres[i].NewAppointmentRel !== null) {
                                        xres[i].Xstatus = "Reschedule";
                                    } else {
                                        xres[i].Xstatus = "Outstanding";
                                    }
                                } else if (xres[i].Status == 0 && xres[i].AppointmentNo == null) {
                                    xres[i].Xstatus = "Draft";
                                } else if (xres[i].Status == 1) {
                                    xres[i].Xstatus = "Cancel";
                                } else if (xres[i].Status == 2) {
                                    xres[i].Xstatus = "No Show";
                                } else if (xres[i].Status >= 3) {
                                    xres[i].Xstatus = "Show";
                                }
                                if ($scope.filter.typeCategory == 5) {
                                    xres[i].TypeFollow = "History";
                                } else if ($scope.filter.typeCategory == 6) {
                                    xres[i].TypeFollow = "Upcoming Appointment";
                                } else if ($scope.filter.typeCategory == 7) {
                                    xres[i].TypeFollow = "Need Approval";
                                }
                                xres[i].Category = "BP"
                            }
                        }
                        var gridData = xres;
                        $scope.grid.data = gridData;
                        $scope.gridFrontParts.data = [];
                        console.log('res====>', gridData);
                        $scope.loading = false;
                    })
                );
            } else {
                // bsNotify.show({
                //     size: 'big',
                //     type: 'danger',
                //     title: "Mohon Input Filter",
                //     // content: error.join('<br>'),
                //     // number: error.length
                // });
                bsAlert.alert({
                    title: "Filter Belum Terisi",
                    text: "Mohon Input Filter",
                    type: "warning",
                    showCancelButton: true
                });
                $scope.grid.data = [];
            }
            $scope.loading = false;
        };

        // $scope.getData = function() {
        //     if ($scope.filter.typeCategory != 4 && $scope.filter.typeCategory != null) {
        //         console.log("tipe", $scope.filter.typeCategory);
        //         return $q.resolve(
        //             FollowUpBpService.getData($scope.filter.firstDate, $scope.filter.lastDate, $scope.filter.typeCategory).then(function(res) {
        //                 xres = [];
        //                 if (res.data.Result.length > 0) {
        //                     console.log("res.data.Result", res.data.Result);
        //                     if ($scope.filter.typeCategory == 1) {
        //                         for (var i = 0; i < res.data.Result.length; i++) {
        //                             if (res.data.Result[i].Status == 0 && res.data.Result[i].isFoOneDay == 0) {
        //                                 xres.push(res.data.Result[i]);
        //                             }
        //                         }
        //                     } else if ($scope.filter.typeCategory == 2) {
        //                         for (var i = 0; i < res.data.Result.length; i++) {
        //                             if (res.data.Result[i].Status == 0 && res.data.Result[i].isFoOneHour == 0) {
        //                                 xres.push(res.data.Result[i]);
        //                             }
        //                         }
        //                     } else if ($scope.filter.typeCategory == 3) {
        //                         for (var i = 0; i < res.data.Result.length; i++) {
        //                             if (res.data.Result[i].Status == 0) {
        //                                 xres.push(res.data.Result[i]);
        //                             }
        //                         }
        //                     }
        //                     for (var i = 0; i < xres.length; i++) {
        //                         xres[i].Category = "BP"
        //                         if (xres[i].Status == 0 && xres[i].AppointmentNo !== null) {
        //                             xres[i].Xstatus = "Outstanding";
        //                         } else if (xres[i].Status == 0 && xres[i].AppointmentNo == null) {
        //                             xres[i].Xstatus = "Draft";
        //                         }
        //                         if ($scope.filter.typeCategory == 1) {
        //                             xres[i].TypeFollow = "H-1";
        //                         } else if ($scope.filter.typeCategory == 2) {
        //                             xres[i].TypeFollow = "J-1";
        //                         } else if ($scope.filter.typeCategory == 3) {
        //                             xres[i].TypeFollow = "J+1";
        //                         }
        //                     }
        //                 }
        //                 console.log("xresssss", xres);
        //                 var gridData = xres;
        //                 $scope.grid.data = gridData;
        //                 $scope.loading = false;
        //             })
        //         )
        //     } else if ($scope.filter.typeCategory == 4 && $scope.filter.firstDate != null && $scope.filter.lastDate != null) {
        //         var a = new Date($scope.filter.firstDate);
        //         var yearFirst = a.getFullYear();
        //         var monthFirst = a.getMonth() + 1;
        //         var dayFirst = a.getDate();
        //         var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;

        //         var b = new Date($scope.filter.lastDate);
        //         var yearLast = b.getFullYear();
        //         var monthLast = b.getMonth() + 1;
        //         var dayLast = b.getDate();
        //         var lastDate = yearLast + '-' + monthLast + '-' + dayLast;
        //         xres = [];
        //         return $q.resolve(
        //             FollowUpBpService.getData(firstDate, lastDate, $scope.filter.typeCategory).then(function(res) {
        //                 xres = [];
        //                 if (res.data.Result.length > 0) {
        //                     for (var i = 0; i < res.data.Result.length; i++) {
        //                         // if (res.data.Result[i].Status == 0){
        //                         xres.push(res.data.Result[i]);
        //                         // }
        //                     }
        //                     for (var i = 0; i < xres.length; i++) {
        //                         if (xres[i].Status == 0 && xres[i].AppointmentNo !== null) {
        //                             xres[i].Xstatus = "Outstanding";
        //                         } else if (xres[i].Status == 0 && xres[i].AppointmentNo == null) {
        //                             xres[i].Xstatus = "Draft";
        //                         } else if (xres[i].Status == 1 && xres[i].NewAppointmentRel !== null) {
        //                             xres[i].Xstatus = "Reschedule";
        //                         } else if (xres[i].Status == 1) {
        //                             xres[i].Xstatus = "Cancel";
        //                         } else if (xres[i].Status == 2) {
        //                             xres[i].Xstatus = "No Show";
        //                         }
        //                         xres[i].TypeFollow = "All";
        //                         xres[i].Category = "BP"
        //                     }
        //                 }
        //                 var gridData = xres;
        //                 $scope.grid.data = gridData;
        //                 // var gridData = res.data.Result;
        //                 // $scope.grid.data = gridData;
        //                 console.log('res====>', gridData);
        //                 $scope.loading = false;
        //             })
        //         );
        //     } else {
        //         // bsNotify.show({
        //         //     size: 'big',
        //         //     type: 'danger',
        //         //     title: "Mohon Input Filter",
        //         //     // content: error.join('<br>'),
        //         //     // number: error.length
        //         // });
        //         bsAlert.alert({
        //             title: "Filter Belum Terisi",
        //             text: "Mohon Input Filter",
        //             type: "warning",
        //             showCancelButton: true
        //         });
        //         $scope.grid.data = [];
        //     }
        //     $scope.loading = false;
        // };

        $scope.conditionData = [{ Id: 0, Name: "Not Ok" }, { Id: 1, Name: "Ok" }];
        $scope.ShowEdit = true;
        $scope.EnableEdit = true;
        $scope.ShowEditInstitusi = true;
        $scope.EnableEditInstitusi = true;
        $scope.EnableEditContactPerson = true;

        $scope.EditInfoUmum = function() {
            $scope.mDataDetailDefault = angular.copy($scope.mDataDetail);
            // AppointmentBpService.getLocation().then(function(res) {
            //     // console.log("List Province====>",res.data.Result);
            //     $scope.ProvinceData = res.data.Result;
            //     $scope.ShowEdit = false;
            //     $scope.EnableEdit = false;
            //     console.log("ProvinceData", $scope.ProvinceData);
            // })
            if ($scope.mDataDetail.CustomerVehicleId !== undefined) {
                $scope.getAddressList();
                console.log("$scope.mDataDetail.CustomerVehicleId", $scope.mDataDetail.CustomerVehicleId);
                AppointmentGrService.getDataContactPerson($scope.mDataDetail.CustomerVehicleId).then(function(res) {
                    // console.log("List Province====>",res.data.Result);
                    $scope.ContactPersonData = res.data.Result;
                    $scope.ShowEdit = false;
                    $scope.EnableEdit = false;
                    $scope.EnableEditContactPerson = false;
                    console.log("Data ContactPerson", $scope.ShowEdit);
                })
            }
        }

        $scope.BatalInfoUmum = function() {
            $scope.ShowEdit = true;
            $scope.EnableEdit = true;
            $scope.EnableEditContactPerson = true;
            // $scope.onBeforeEdit();
            // $scope.getAllParameter(DataPersonal.VehicleId);
            $scope.mDataDetail = angular.copy($scope.mDataDetailDefault);
            if ($scope.mDataDetail.ProvinceId !== undefined && $scope.mDataDetail.ProvinceId !== null) {
                WOBP.getMLocationProvince().then(function(res) {
                    console.log("List Province====>", res.data.Result);
                    $scope.ProvinceData = res.data.Result;

                    if ($scope.mDataDetail.CityRegencyId !== undefined && $scope.mDataDetail.CityRegencyId !== null) {
                        WOBP.getMLocationCityRegency($scope.mDataDetail.ProvinceId).then(function(resu) {
                            $scope.CityRegencyData = resu.data.Result;

                            if ($scope.mDataDetail.DistrictId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                WOBP.getMLocationKecamatan($scope.mDataDetail.CityRegencyId).then(function(resu) {
                                    $scope.DistrictData = resu.data.Result;

                                    if ($scope.mDataDetail.VillageId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                        WOBP.getMLocationKelurahan($scope.mDataDetail.DistrictId).then(function(resu) {
                                            $scope.VillageData = resu.data.Result;
                                        });
                                    };
                                });
                            };
                        });
                    };
                })
            };
        }

        $scope.CekPerbedaan = function() {
            console.log("$scope.mDataDetail A", $scope.mDataDetail);
            console.log("$scope.DataSebelumBerubahCRM B", $scope.DataSebelumBerubahCRM);
            $scope.datahasil = {};
            $scope.datahasilsudah = {};
            $scope.dataakhir = [];
            $scope.datasatu = [];
            $scope.datadua = [];
            var counter = 0;
            var keyofData = Object.keys($scope.DataSebelumBerubahCRM);

            for (var i in keyofData) {
                if ($scope.DataSebelumBerubahCRM[keyofData[i]] !== $scope.mDataDetail[keyofData[i]]) {
                    counter++;
                    $scope.datahasil[keyofData[i]] = $scope.mDataDetail[keyofData[i]];
                    $scope.datahasilsudah[keyofData[i]] = $scope.DataSebelumBerubahCRM[keyofData[i]];
                    // $scope.datasatu.push($scope.datahasil);
                    // $scope.datadua.push($scope.datahasilsudah);                    
                }
            }
            console.log('keyofData', keyofData);
            var fieldofData = Object.keys($scope.datahasilsudah);

            for (var i in fieldofData) {
                if ($scope.datahasilsudah[keyofData[i]] !== null) {
                    counter++;
                    $scope.datasatu.push($scope.datahasilsudah[fieldofData[i]]);

                }
            }

            for (var i = 0; i < fieldofData.length; i++) {
                var tmpDataAkhir = {};
                tmpDataAkhir.CustomerId = $scope.mDataDetail.CustomerId;
                tmpDataAkhir.ChangeTypeId = 2
                    tmpDataAkhir.Modul = 'Follow Up Appointment BP'
                    tmpDataAkhir.Attribute = fieldofData[i]
                    if (tmpDataAkhir.Attribute == "BirthDate"){
                        tmpDataAkhir.NewValue = $filter('date')($scope.datahasil[fieldofData[i]], 'yyyy-MM-dd');
                        tmpDataAkhir.OldValue = $filter('date')($scope.datahasilsudah[fieldofData[i]], 'yyyy-MM-dd');
                    } else {
                        tmpDataAkhir.NewValue = $scope.datahasil[fieldofData[i]];
                        tmpDataAkhir.OldValue = $scope.datahasilsudah[fieldofData[i]];
                        // tmpDataAkhir.NewValue = $scope.datahasilsudah[fieldofData[i]]
                        // tmpDataAkhir.OldValue = $scope.datahasil[fieldofData[i]]
                    }
                    // tmpDataAkhir.NewValue = $scope.datahasilsudah[fieldofData[i]],
                    // tmpDataAkhir.OldValue = $scope.datahasil[fieldofData[i]]
                console.log(' $scope.datadua', $scope.datadua);
                AppointmentGrService.sendToCRMHistory(tmpDataAkhir);
            }

        };
        $scope.UpdateInfoUmum = function(mDataDetail) {

            AppointmentGrService.getDataVehicle(mDataDetail.VehicleId).then(function(ress) {
                var dataDetail = ress.data.Result[0]
                $scope.DataSebelumBerubahCRM = dataDetail;
                console.log("data CRM A", $scope.DataSebelumBerubahCRM);
                $scope.CekPerbedaan();
            });
            // console.log("data sialan",DataPersonal);
            // if ($scope.mDataDetail.VehicleUserId !== undefined) {
            // AppointmentGrService.updateStatus(mDataDetail).then(function(res) {
            AppointmentBpService.putCustomerPersonal(DataPersonal, mDataDetail).then(function(res) {
                AppointmentBpService.putCustomerAddress(DataPersonal, mDataDetail, 1).then(function(res) {
                    $scope.BatalInfoUmum();
                    console.log("save addres")
                });
            });
            // });
            // }
            // if ($scope.mDataDetail.VehicleUserId !== undefined) {
            //     AppointmentGrService.updateStatus(mDataDetail).then(
            //         function(res) {
            //             $scope.BatalInfoUmum();
            //         },
            //         function(err) {
            //             console.log("err=>", err);
            //         }
            //     );
            // }
        }

        $scope.EditInfoUmumInstitusi = function() {
            $scope.mDataDetailDefault = angular.copy($scope.mDataDetail);
            $scope.getAddressList();
            if ($scope.mDataDetail.CustomerVehicleId !== undefined) {
                console.log("$scope.mDataDetail.CustomerVehicleId", $scope.mDataDetail.CustomerVehicleId);
                AppointmentGrService.getDataContactPerson($scope.mDataDetail.CustomerVehicleId).then(function(res) {
                    // console.log("List Province====>",res.data.Result);
                    $scope.ContactPersonData = res.data.Result;
                    $scope.ShowEditInstitusi = false;
                    $scope.EnableEditInstitusi = false;
                    $scope.EnableEditContactPerson = false;
                    console.log("Data ContactPerson", $scope.ContactPersonData);
                })
            }
            // AppointmentBpService.getLocation().then(function(res) {
            //     // console.log("List Province====>",res.data.Result);
            //     $scope.ProvinceData = res.data.Result;
            //     $scope.ShowEditInstitusi = false;
            //     // $scope.EnableEditInstitusi = false;
            //     $scope.EnableEditContactPerson = false;
            //     console.log("ProvinceData", $scope.ProvinceData);
            // })
        }

        $scope.BatalInfoUmumInstitusi = function() {
            $scope.ShowEditInstitusi = true;
            $scope.EnableEditInstitusi = true;
            $scope.EnableEditContactPerson = true;
            // $scope.onBeforeEdit();
            // $scope.getAllParameter(DataPersonal.VehicleId);
            $scope.mDataDetail = angular.copy($scope.mDataDetailDefault);
            if ($scope.mDataDetail.ProvinceId !== undefined && $scope.mDataDetail.ProvinceId !== null) {
                WOBP.getMLocationProvince().then(function(res) {
                    console.log("List Province====>", res.data.Result);
                    $scope.ProvinceData = res.data.Result;

                    if ($scope.mDataDetail.CityRegencyId !== undefined && $scope.mDataDetail.CityRegencyId !== null) {
                        WOBP.getMLocationCityRegency($scope.mDataDetail.ProvinceId).then(function(resu) {
                            $scope.CityRegencyData = resu.data.Result;

                            if ($scope.mDataDetail.DistrictId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                WOBP.getMLocationKecamatan($scope.mDataDetail.CityRegencyId).then(function(resu) {
                                    $scope.DistrictData = resu.data.Result;

                                    if ($scope.mDataDetail.VillageId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                        WOBP.getMLocationKelurahan($scope.mDataDetail.DistrictId).then(function(resu) {
                                            $scope.VillageData = resu.data.Result;
                                        });
                                    };
                                });
                            };
                        });
                    };
                })
            };
        }

        $scope.UpdateInfoUmumInstitusi = function(mDataDetail) {
            // if ($scope.mDataDetail.VehicleUserId !== undefined) {
            // AppointmentGrService.updateStatus(mDataDetail).then(function(res) {
            AppointmentBpService.putCustomerAddress(DataPersonal, mDataDetail, 2).then(function(res) {
                $scope.BatalInfoUmumInstitusi();
                console.log("save addres")
            });
            // });
            // }

        }

        $scope.estimateAsb = function() {
                if (gridTemp.length > 0) {
                    var totalW = 0;
                    for (var i = 0; i < gridTemp.length; i++) {
                        if (gridTemp[i].ActualRate !== undefined) {
                            totalW += gridTemp[i].ActualRate;
                        }
                    }
                    $scope.visibleAsb = 1;
                    $scope.tmpActRate = 1;
                } else {
                    $scope.tmpActRate = 1;
                    $scope.visibleAsb = 0;
                }
                $scope.visibleAsb = 1;
                $scope.tmpActRate = 1;
                $scope.checkAsb = true;
                $scope.asb.startDate = "";
                currentDate = new Date();
                // currentDate.setDate(currentDate.getDate() + 1);
                currentDate.setDate(currentDate.getDate()); // edit bisa hari ini Pak Dodi
                $scope.minDateASB = new Date(currentDate);
                $scope.currentDate = new Date(currentDate);
                if ($scope.mData.AppointmentDate !== undefined && $scope.mData.AppointmentDate !== null) {
                    console.log("Estimate  ASB $scope.mData.AppointmentDate", $scope.mData.AppointmentDate);
                    var tmpDate = $scope.mData.AppointmentDate;
                    var tmpTime = $scope.mData.AppointmentTime;
                    var finalDate
                    var yyyy = tmpDate.getFullYear().toString();
                    var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based
                    var dd = tmpDate.getDate().toString();
                    finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + " " + tmpTime;
                    var d = new Date(finalDate);
                    $scope.asb.startDate = d;
                } else {
                    console.log("Estimate ASB $scope.mData.AppointmentDate", $scope.mData.AppointmentDate);
                    $scope.asb.startDate = new Date(currentDate);
                }
                // $scope.asb.startDate = new Date(currentDate);
                console.log("aduuhh klo ada curiga gua", $scope.mData);
                $scope.asb.JobStallId = $scope.mData.UserIdEstimasi;
                $scope.asb.JobStartTime = $scope.mData.PlanStart;
                $scope.asb.JobEndTime = $scope.mData.PlanFinish;
                $scope.stallAsb = $scope.mData.UserIdEstimasi;
                $scope.mData.FullDate = $scope.asb.startDate;
                console.log("curDate", currentDate);
                console.log("$scope.asb.startDate", $scope.asb.startDate);
                console.log("$scope.stallAsb", $scope.stallAsb, $scope.mData.UserIdEstimasi);
                showBoardAsbGr($scope.mData.FullDate);
            }
            // ====== Alamat Added by Fyberz===================
        $scope.checkSatuan = function(id) {
            console.log("ideeeeeeeee", id, $scope.unitData.length);
            for (var i = 0; i < $scope.unitData.length; i++) {
                if ($scope.unitData[i].MasterId == id) {
                    return $scope.unitData[i].Name;
                }
            }
        }
        $scope.CustomerAddress = [];
        $scope.getAddressList = function() {
            WOBP.getMLocationProvince().then(function(res) {
                console.log("List Province====>", res.data.Result);
                // ProvinceId == row.ProvinceId
                $scope.ProvinceData = res.data.Result;
                // $scope.tempProvinsiData = angular.copy($scope.provinsiData);
            })
        };

        $scope.selectProvince = function(row) {
            console.log('row selectProvince', row);
            WOBP.getMLocationCityRegency(row.ProvinceId).then(function(resu) {
                $scope.CityRegencyData = resu.data.Result;

                $scope.mDataDetail.CityRegencyId = null
                $scope.mDataDetail.DistrictId = null
                $scope.mDataDetail.VillageId = null
                $scope.mDataDetail.PostalCode = null

                $scope.DistrictData = []
                $scope.VillageData = []
            });
            // $scope.selectedProvince = angular.copy(row);
        };
        $scope.selectRegency = function(row) {
            console.log('row selectRegency', row);
            WOBP.getMLocationKecamatan(row.CityRegencyId).then(function(resu) {
                $scope.DistrictData = resu.data.Result;

                $scope.mDataDetail.DistrictId = null
                $scope.mDataDetail.VillageId = null
                $scope.mDataDetail.PostalCode = null

                $scope.VillageData = []
            });
            // $scope.kecamatanData = row.MDistrict;
            // $scope.selectedRegency = angular.copy(row);
        };
        $scope.selectDistrict = function(row) {
            console.log('row selectDistrict', row);
            WOBP.getMLocationKelurahan(row.DistrictId).then(function(resu) {
                $scope.VillageData = resu.data.Result;

                $scope.mDataDetail.VillageId = null
                $scope.mDataDetail.PostalCode = null
            });
            // $scope.selectedDistrict = angular.copy(row);
        };
        $scope.selectVillage = function(row) {
            console.log('row selectVillage', row);
            // $scope.selecedtVillage = angular.copy(row);
            $scope.mDataDetail.PostalCode = row.PostalCode == 0 ? '-' : row.PostalCode;
        };
        // $scope.selectProvince = function(row){
        //     $scope.CityRegencyData = row.MCityRegency;
        // }

        // $scope.selectRegency = function(row){
        //    $scope.DistrictData = row.MDistrict;
        // }

        // $scope.selectDistrict = function(row){
        //    $scope.VillageData = row.MVillage;
        // }

        // $scope.typeFollowUp = [
        //     { Id: 1, Name: "H-1" },
        //     { Id: 2, Name: "J-1" },
        //     { Id: 3, Name: "J+1" },
        //     { Id: 4, Name: "All" }
        // ];
        $scope.checkIsKabeng = function() {
            // if ($scope.user.RoleId == 1128 || $scope.user.RoleId == 1021) {
            $scope.typeFollowUp = [{
                    Id: 1,
                    Name: "H-1"
                },
                {
                    Id: 2,
                    Name: "J-"
                },
                {
                    Id: 3,
                    Name: "J+"
                },
                {
                    Id: 5,
                    Name: "History"
                },
                {
                    Id: 6,
                    Name: "Upcoming Appointment"
                },
                {
                    Id: 4,
                    Name: "Need Approval"
                }

            ];
            // }
        }
        $scope.typeFollowUp = [{
                Id: 1,
                Name: "H-1"
            },
            {
                Id: 2,
                Name: "J-"
            },
            {
                Id: 3,
                Name: "J+"
            },
            {
                Id: 5,
                Name: "History"
            },
            {
                Id: 6,
                Name: "Upcoming Appointment"
            }

        ];
        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        // $scope.selectCategory = function(selected) {
        //     if (selected != 3) {
        //         $scope.filter.firstDate = null;
        //         $scope.filter.lastDate = null;
        //     }
        // }
        $scope.selectCategory = function(selected) {
            // if (selected.Id < 4) {
            $scope.filter.firstDate = null;
            $scope.filter.lastDate = null;
            // }
            $scope.grid.data = [];
            console.log("selected.Id : ", selected.Id);
            changeId = selected.Id;
            var currentDateOptionUpcoming = new Date();
            var currentDateOptionHistory = new Date();
            var maxDateUpcoming = new Date(currentDateOptionUpcoming.setDate(currentDateOptionUpcoming.getDate() + 2));
            var maxDateHistory = new Date();
            // var maxDateHistory = new Date(currentDateOptionHistory.setDate(currentDateOptionHistory.getDate() - 1));
            console.log("maxDateUpcoming", maxDateUpcoming);
            console.log("maxDateHistory", maxDateHistory);
            if (selected.Id == 4 || selected.Id == 5) {
                $scope.DateFilterOptions1.minDate = null;
                $scope.DateFilterOptions1.maxDate = maxDateHistory;
                $scope.DateFilterOptions2.minDate = null;
                $scope.DateFilterOptions2.maxDate = maxDateHistory;
            } else if (selected.Id == 6) {
                $scope.DateFilterOptions1.minDate = maxDateUpcoming;
                $scope.DateFilterOptions1.maxDate = null;
                $scope.DateFilterOptions2.minDate = maxDateUpcoming;
                $scope.DateFilterOptions2.maxDate = null;
            }

        };
        // added by sss on 2018-02-20
        $scope.startDateChange = function(selected) {
            console.log(selected.dt)
            if (changeId == 4 || changeId == 5) {
                console.log("changeId == 4 | 5");
                $scope.DateFilterOptions2.minDate = selected.dt;
            } else if (changeId == 6) {
                console.log("changeId == 6");
                $scope.DateFilterOptions2.minDate = selected.dt;
                $scope.DateFilterOptions2.maxDate = null;
            }
        }

        $scope.CheckedVIN = function(){
            if ($scope.mDataDetail.VIN != null || $scope.mDataDetail.VIN != "") {
                var count = $scope.mDataDetail.VIN;
                console.log('mdata vin', count.length)
                    if (count.length <= 17) {
                        AppointmentBpService.getDataVIN($scope.mDataDetail.VIN).then(function(res){
                            console.log('cek input rangka', $scope.mDataDetail.VIN)
                            $scope.tempVIN = res.data.Result[0]
                            console.log('cek res rangka', $scope.tempVIN)
                            $scope.tempVIN = res.data.Result[0];
                            if ($scope.tempVIN.DEC_Date != null && $scope.tempVIN.DEC_Date != undefined) {
                                $scope.tempVIN.DEC_Date = $filter('date')($scope.tempVIN.DEC_Date, 'dd/MM/yyyy')
                            }else{
                                $scope.tempVIN.DEC_Date = '-'
                            }
                        console.log('cek res input', $scope.tempVIN)
                                console.log('cek res input', $scope.tempVIN)
                                console.log('$scope.taskCategory ', $scope.taskCategory )
                        })

                        AppointmentBpService.listHistoryClaim($scope.mDataDetail.VIN).then(function(res){
                            // res.data.data = $scope.ubahFormatTanggalIndo(res.data.data.historyClaimDealerList)
                            $scope.listClaim = res.data.data;
                            $scope.tempListClaim = [];
                            // $scope.historyDate = new Date($scope.tempListClaim.claimDate)
                            console.log('cek list claim', res)
                            console.log('list claim', res.data.data)
                            console.log('list history claim', $scope.tempListClaim)
                            for (var i = 0; i < $scope.listClaim.historyClaimDealerList.length; i++) {
                                $scope.tempListClaim.push($scope.listClaim.historyClaimDealerList[i]);
                            }
                        })

                        AppointmentBpService.getTowass($scope.mDataDetail.VIN).then(function(reso) {
                            console.log("data Towasssssss", reso.data);
                            $scope.dataTowass = reso.data.Result;
                        });

                    }else{
                        console.log('tanpa service')
                        $scope.taskCategory = $scope.notFlcPlcFpc
                    }
            }
        }

        $scope.onShowDetail = function(data, mode) {
            tampungEstimasi = [{EstimationNo  : null, EstTaskLength : null, EstimationBPId: null }];
            $scope.ShowEdit = true;
            $scope.EnableEdit = true;
            $scope.ShowEditInstitusi = true;
            $scope.EnableEditInstitusi = true;
            $scope.EnableEditContactPerson = true;
            $scope.hideGridParts = true;
            $scope.JobComplaint = [];
            $scope.JobRequest = [];
            $scope.gridWork = [];
            $scope.mDataDetail = {};
            $scope.VTypeData = [];
            var arrInsurance = [];

            $scope.restart_digit_odo = 0
            $scope.isKmHistory = 0
            $scope.nilaiKM = 0
            $scope.KMTerakhir = 0

            $scope.restart_digit_odo = 0;
            $scope.model_info_km.show = false;

            if (data.JobId != null) {
                FollowUpGrService.getWobyJobId(data.JobId).then(function(res) {
                    var dataWo = res.data.Result[0];
                    if (dataWo != null && dataWo != undefined) {
                        FollowUpGrService.getDataVIN(dataWo.VIN).then(function(res){
                            console.log('cek input rangka', dataWo.VIN)
                            $scope.tempVIN = res.data.Result[0];
                            if ($scope.tempVIN.DEC_Date != null && $scope.tempVIN.DEC_Date != undefined) {
                                $scope.tempVIN.DEC_Date = $filter('date')($scope.tempVIN.DEC_Date, 'dd/MM/yyyy')
                            }else{
                                $scope.tempVIN.DEC_Date = '-'
                            }
                            console.log('cek res input', $scope.tempVIN)
                        })
                        FollowUpGrService.listHistoryClaim(dataWo.VIN).then(function(res){
                            $scope.listClaim = res.data.data;
                            $scope.tempListClaim = [];
                            console.log('cek list claim', res)
                            console.log('list claim', res.data.data)
                            for (var i = 0; i < $scope.listClaim.historyClaimDealerList.length; i++) {
                                $scope.tempListClaim.push($scope.listClaim.historyClaimDealerList[i]);
                            }
                        })
    
                        FollowUpGrService.getTowass(dataWo.VIN).then(function(rese) {
                            console.log("data Towasssssss", rese.data);
                            $scope.dataTowass = rese.data.Result;
                        });
                    }

                    if (_.isNull(dataWo.InsuranceName) || _.isEmpty(dataWo.InsuranceName)) {
                        dataWo.isCash = 1;
                    }

                    console.log('master data ===>',dataWo);

                    $scope.nilaiKM = angular.copy(dataWo.Km)
                   
                    $scope.mData = dataWo;
                    $scope.choosePembayaran($scope.mData.isCash);
                    $scope.mData.Km = String($scope.mData.Km);
                    $scope.mData.Km = $scope.mData.Km.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                    // $scope.

                    
                    $scope.JobRequest = dataWo.JobRequest;
                    $scope.JobComplaint = dataWo.JobComplaint;
                    $scope.firstAppointmentDate = angular.copy(dataWo.AppointmentDate);
                    $scope.firstPlanStart = angular.copy(dataWo.PlanStart);
                    $scope.firstPlanFinish = angular.copy(dataWo.PlanFinish);
                    // get Insurance
                    // _.map($scope.Asuransi, function(val) {
                    //     if (val.InsuranceId.toString() === $scope.mData.InsuranceName) {
                    //         $scope.mDataDetail.InsuranceId = val.InsuranceId;
                    //         $scope.mDataDetail.InsuranceName = val.InsuranceName;
                    //         $scope.mData.InsuranceId = $scope.mDataDetail.InsuranceId;
                    //         $scope.mData.InsuranceName = $scope.mDataDetail.InsuranceName;
                    //     }
                    // });

                    for (i in $scope.Asuransi){
                        if($scope.Asuransi[i].InsuranceId == $scope.mData.InsuranceId){
                            $scope.mData.InsuranceName = $scope.Asuransi[i].InsuranceName;
                            $scope.mData.InsuranceId = $scope.Asuransi[i].InsuranceId;
                            $scope.mDataDetail.InsuranceId = $scope.Asuransi[i].InsuranceId;
                            $scope.mDataDetail.InsuranceName = $scope.Asuransi[i].InsuranceName;
                            $scope.selectInsurance($scope.Asuransi[i]);
                        }
                    }

                    var tmpJob = dataWo.JobTask;
                    console.log('tmpJob ==>',JSON.stringify(tmpJob))

                    //tambahan CR4
                    for (var i = 0; i < tmpJob.length; i++){
                        tmpJob[i].PaidBy = tmpJob[i].PaidBy.Name
                        for (var j = 0; j < tmpJob[i].JobParts.length; j++){
                            console.log('tmpJob[i].JobParts[j].Satuan ===>',tmpJob[i].JobParts[j].Satuan);
                            tmpJob[i].JobParts[j].RetailPriceMask =tmpJob[i].JobParts[j].Price*tmpJob[i].JobParts[j].Qty;
                            tmpJob[i].JobParts[j].subTotalMask = tmpJob[i].JobParts[j].Price*tmpJob[i].JobParts[j].Qty;
                            
                            if(tmpJob[i].JobParts[j].Satuan == null || tmpJob[i].JobParts[j].Satuan == undefined || tmpJob[i].JobParts[j].Satuan == ""){
                                
                            }else{
                                tmpJob[i].JobParts[j].satuanName = tmpJob[i].JobParts[j].Satuan.Name;

                                tmpJob[i].JobParts[j].Name = tmpJob[i].JobParts[j].Satuan.Name;
                                tmpJob[i].JobParts[j].Satuan = tmpJob[i].JobParts[j].Satuan.Name;
                            };
                            tmpJob[i].JobParts[j].paidName = tmpJob[i].JobParts[j].PaidBy.Name;
                            tmpJob[i].JobParts[j].PaidBy = tmpJob[i].JobParts[j].PaidBy.Name;
                            tmpJob[i].JobParts[j].DPestimasi = tmpJob[i].JobParts[j].DownPayment;

                            
                            
                            // if (tmpJob[i].JobParts[j].MaterialTypeId == 2){ //bahan
                            //     var Qty = parseInt(tmpJob[i].JobParts[j].Qty);
                            //     var RetailPrice = parseInt(tmpJob[i].JobParts[j].Price);
                            //     var subTotal = parseInt(tmpJob[i].JobParts[j].Price*tmpJob[i].JobParts[j].Qty);
                            //     var Discount = 0;
                            //     tmpJob[i].JobParts[j].Discount = 0;
                            //     tmpJob[i].JobParts[j].DiskonForDisplay = String(tmpJob[i].JobParts[j].Discount) +'%';
                            //     tmpJob[i].JobParts[j].RetailPrice = tmpJob[i].JobParts[j].Part.RetailPrice;
                            //     tmpJob[i].JobParts[j].NominalDiscount = (Qty*RetailPrice)*(Discount/100);
                            //     tmpJob[i].JobParts[j].subTotalForDisplay = subTotal - (Qty*RetailPrice)*(Discount/100);
                            //     tmpJob[i].JobParts[j].subTotal = tmpJob[i].JobParts[j].Price*tmpJob[i].JobParts[j].Qty;
                            //     console.log('DiskonForDisplay   ===>',tmpJob[i].JobParts[j].DiskonForDisplay);
                            //     console.log('NominalDiscount    ===>',tmpJob[i].JobParts[j].NominalDiscount);
                            //     console.log('subTotalForDisplay ===>',tmpJob[i].JobParts[j].subTotalForDisplay);
                            //     console.log('Qty,RetailPrice,subTotal,Discount ===>',Qty+'|'+RetailPrice+'|'+subTotal+'|'+Discount);
                            // }else{ //sparepart
                                var Qty = parseInt(tmpJob[i].JobParts[j].Qty);
                                var RetailPrice = parseInt(tmpJob[i].JobParts[j].Price);
                                var subTotal = parseInt(tmpJob[i].JobParts[j].Price*tmpJob[i].JobParts[j].Qty);
                                var Discount = parseInt(tmpJob[i].JobParts[j].Discount);
                                tmpJob[i].JobParts[j].DiskonForDisplay = String(tmpJob[i].JobParts[j].Discount) +'%';
                                // tmpJob[i].JobParts[j].RetailPrice = tmpJob[i].JobParts[j].Part.RetailPrice;
                                tmpJob[i].JobParts[j].RetailPrice = tmpJob[i].JobParts[j].Price;
                                tmpJob[i].JobParts[j].NominalDiscount = (Qty*RetailPrice)*(Discount/100);
                                tmpJob[i].JobParts[j].subTotalForDisplay = subTotal - (Qty*RetailPrice)*(Discount/100);
                                tmpJob[i].JobParts[j].subTotal = tmpJob[i].JobParts[j].Price*tmpJob[i].JobParts[j].Qty;
                                console.log('DiskonForDisplay   ===>',tmpJob[i].JobParts[j].DiskonForDisplay);
                                console.log('NominalDiscount    ===>',tmpJob[i].JobParts[j].NominalDiscount);
                                console.log('subTotalForDisplay ===>',tmpJob[i].JobParts[j].subTotalForDisplay);
                                console.log('Qty,RetailPrice,subTotal,Discount ===>',Qty+'|'+RetailPrice+'|'+subTotal+'|'+Discount);
                            // }


                        }
                    }
                    // tambahan CR4


                    dataWo.JobTask = [];
                    dataWo.JobTask = tmpJob;

                    


                    console.log('dataForBslist sentuhan CR4 ===>',dataWo);


                    $scope.dataForBslist(dataWo);
                    // console.log('dataWo ===>', JSON.stringify(dataWo));
                    GeneralMaster.getData(2030).then(
                        function(res) {
                            $scope.VTypeData = res.data.Result;
                            return res.data;
                        }
                    );
                    // $scope.dataForWAC();
                    RepairProcessGR.getDataWac($scope.mData.JobId).then(
                        function(res) {
                            var dataWac = res.data;
                            $scope.mData.OtherDescription = dataWac.Other;
                            dataWac.MoneyAmount = String(dataWac.MoneyAmount);
                            $scope.mData.moneyAmount = dataWac.MoneyAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                            // $scope.mData.moneyAmount = dataWac.MoneyAmount;
                            $scope.mData.BanSerep = dataWac.isSpareTire;
                            $scope.mData.CdorKaset = dataWac.isCD;
                            $scope.mData.Payung = dataWac.isUmbrella;
                            $scope.mData.Dongkrak = dataWac.isJack;
                            $scope.mData.STNK = dataWac.isVehicleRegistrationNumber;
                            $scope.mData.P3k = dataWac.isP3k;
                            $scope.mData.KunciSteer = dataWac.isSteerLock;
                            $scope.mData.ToolSet = dataWac.isToolSet;
                            $scope.mData.ClipKarpet = dataWac.isCarpetClip;
                            $scope.mData.BukuService = dataWac.isServiceBook;
                            $scope.mData.AlarmCondition = dataWac.isAlarmConditionOk;
                            $scope.mData.AcCondition = dataWac.isAcOk;
                            $scope.mData.PowerWindow = dataWac.isPowerWindowOk;
                            $scope.mData.CarType = dataWac.isPowerWindowOk;
                            // $scope.slider.options = 100;
                            $scope.slider.value = dataWac.Fuel;
                        });
                    // RepairProcessGR.getProbPoints($scope.mData.JobId, null).then(
                    //  function(res) {
                    //
                    //      console.log("RepairProcessGR.getProbPoints", res.data);
                    //      mstId = angular.copy(res.data.Result[0].TypeId);
                    //      console.log("ini MstId", mstId);
                    //      $scope.filter.vType = mstId;
                    //      $scope.chooseVType(mstId);
                    //
                    // });
                    $scope.dataForPrediagnose(dataWo);
                    if (dataWo.VehicleId !== null) {
                        console.log("Punya VehicleId");
                        FollowUpGrService.getDataVehicle(dataWo.VehicleId).then(function(ress) {
                            $scope.mDataDetail = ress.data.Result[0];
                            $scope.getAllParameter(ress.data.Result[0]);
                            console.log("onShowDetail masuknya", ress);
                            if (ress.data.Result.length <= 0) {
                                $scope.vehicleStatus = 1;
                            } else {
                                $scope.vehicleStatus = 0;
                                console.log("kalo CRM");
                            }
                            // AppointmentGrService.getDataTaskList($scope.mDataDetail.KatashikiCode).then(function(result) {
                            //     console.log("result", result);
                            // });
                            if ($scope.mDataDetail !== undefined) {
                                AppointmentGrService.getDataContactPerson($scope.mDataDetail.CustomerVehicleId).then(function(res) {
                                    $scope.ContactPersonData = res.data.Result;
                                })

                            }

                            $scope.unDisable($scope.mDataDetail.VIN)

                        });
                        AppointmentGrService.getDataVehiclePSFU(dataWo.VehicleId).then(function(resu) {
                            console.log("getDataVehiclePSFU", resu.data.Result);
                            var dataQuestionAnswerPSFU = {}
                            if (resu.data.Result.length > 0) {
                                dataQuestionAnswerPSFU = resu.data.Result[0];
                                $scope.mDataDetail.Suggest = dataQuestionAnswerPSFU.JobSuggest;
                                FollowupService.getQuestion().then(function(resu2) {
                                    console.log("getQuestion", resu2.data.Result);
                                    var dataQuestionAnswer = resu2.data.Result;
                                    var arr = [];
                                    _.map(dataQuestionAnswer, function(val, key) {
                                        var obj = {};
                                        if (dataQuestionAnswerPSFU.QuestionId1 == val.QuestionId) {
                                            obj.QuestionId = val.QuestionId;
                                            obj.Description = val.Description;
                                            obj.Answer = val.Answer;
                                            obj.Reason = dataQuestionAnswerPSFU.Reason1 ? dataQuestionAnswerPSFU.Reason1 : null;
                                            obj.ReasonStatus = dataQuestionAnswerPSFU.Reason1 ? false : true;
                                            obj.jawaban = dataQuestionAnswerPSFU.AnswerId1
                                            arr.push(obj);
                                        } else if (dataQuestionAnswerPSFU.QuestionId2 == val.QuestionId) {
                                            obj.QuestionId = val.QuestionId;
                                            obj.Description = val.Description;
                                            obj.Answer = val.Answer;
                                            obj.Reason = dataQuestionAnswerPSFU.Reason2 ? dataQuestionAnswerPSFU.Reason1 : null;
                                            obj.ReasonStatus = dataQuestionAnswerPSFU.Reason2 ? false : true;
                                            obj.jawaban = dataQuestionAnswerPSFU.AnswerId2
                                            arr.push(obj);
                                        } else if (dataQuestionAnswerPSFU.QuestionId3 == val.QuestionId) {
                                            obj.QuestionId = val.QuestionId;
                                            obj.Description = val.Description;
                                            obj.Answer = val.Answer;
                                            obj.Reason = dataQuestionAnswerPSFU.Reason3 ? dataQuestionAnswerPSFU.Reason3 : null;
                                            obj.ReasonStatus = dataQuestionAnswerPSFU.Reason3 ? false : true;
                                            obj.jawaban = dataQuestionAnswerPSFU.AnswerId3
                                            arr.push(obj);
                                        };
                                        // arr.push(obj);
                                    });
                                    console.log('arr >>>', arr);
                                    $scope.Question = arr;
                                })
                            } else {
                                dataQuestionAnswerPSFU.JobSuggest = null
                            }
                        });
                    } else {
                        console.log("gak punya VehicleIde");
                        $scope.getAllParameter();
                        $scope.vehicleStatus = 1;
                        $scope.mDataDetail = {};
                        console.log("kalo bukan CRM", $scope.modelData);
                        $scope.mDataDetail.LicensePlate = dataWo.PoliceNumber;
                        $scope.mDataDetail.VIN = dataWo.VIN;
                        $scope.mDataDetail.KatashikiCode = dataWo.KatashikiCode;
                        // $scope.mDataDetail.VehicleModelName = dataWo.VehicleType.VehicleModel.VehicleModelName;
                        // $scope.mDataDetail.Description = dataWo.VehicleType.Description;
                        _.map($scope.modelData, function(val) {
                            if (val.VehicleModelName === dataWo.ModelType) {
                                console.log('masuk kesini model val', val);
                                $scope.mDataDetail.VehicleModelId = val.VehicleModelId;
                                    $scope.mDataDetail.VehicleModelName = val.VehicleModelName;
                                console.log('masuk kesini model', $scope.mDataDetail);
                                // MrsList.getDataTypeByModel(val.VehicleModelId).then(function(res) {
                                WO.getVehicleTypeById(val.VehicleModelId).then(function(res) {
                                    console.log("result : ", res);
                                    // $scope.typeData = res.data;
                                    // $scope.typeData = res.data.Result;

                                    $scope.katashikiData = _.uniq(res.data.Result, 'KatashikiCode');

                                    for (var i=0; i<$scope.katashikiData.length; i++){
                                        $scope.katashikiData[i].NewDescription = $scope.mDataDetail.VehicleModelName + ' - ' + $scope.katashikiData[i].KatashikiCode
                                    }

                                    for (var i=0; i < $scope.katashikiData.length; i++) {
                                        if ($scope.katashikiData[i].KatashikiCode == dataWo.VehicleType.KatashikiCode) {
                                            $scope.mDataDetail.KatashikiCodex   = dataWo.VehicleType.KatashikiCode;

                                        }
                                    }

                                    if ($scope.mDataDetail.KatashikiCodex != null && $scope.mDataDetail.KatashikiCodex != undefined && $scope.mDataDetail.KatashikiCodex != '') {
                                        AppointmentGrService.GetCVehicleTypeListByKatashiki(dataWo.VehicleType.KatashikiCode).then(function(res) {
                                            // $scope.typeData = res.data.Result[0];
                                            $scope.typeData = res.data.Result;

                                            for (var z = 0; z < $scope.typeData.length; z++) {
                                                $scope.typeData[z].NewDescription = $scope.typeData[z].Description + ' - ' + $scope.typeData[z].KatashikiCode + ' - ' + $scope.typeData[z].SuffixCode
                                            }

                                            for (var z = 0; z < $scope.typeData.length; z++) {
                                                if($scope.typeData[z].VehicleTypeId == dataWo.VehicleType.VehicleTypeId){
                                                    $scope.selectType($scope.typeData[z])
                                                    console.log('Type Gue ===> ',dataWo.VehicleType.VehicleTypeId +" == ModelOptions "+$scope.typeData[z].VehicleTypeId);
                                                }
                                            }

                                        })
                                    }
                                });
                            } else if (dataWo.VehicleType !== undefined) {
                                if (val.VehicleModelId === dataWo.VehicleType.VehicleModelId) {
                                    console.log('masuk kesini model val', val);
                                    $scope.mDataDetail.VehicleModelId = val.VehicleModelId;
                                    $scope.mDataDetail.VehicleModelName = val.VehicleModelName;
                                    console.log('masuk kesini model', $scope.mDataDetail);
                                    // MrsList.getDataTypeByModel(val.VehicleModelId).then(function(res) {
                                    WO.getVehicleTypeById(val.VehicleModelId).then(function(res) {
                                        console.log("result : ", res);
                                        // $scope.typeData = res.data;
                                        
                                        $scope.katashikiData = _.uniq(res.data.Result, 'KatashikiCode');

                                        for (var i=0; i<$scope.katashikiData.length; i++){
                                            $scope.katashikiData[i].NewDescription = $scope.mDataDetail.VehicleModelName + ' - ' + $scope.katashikiData[i].KatashikiCode
                                        }

                                        for (var i=0; i < $scope.katashikiData.length; i++) {
                                            if ($scope.katashikiData[i].KatashikiCode == dataWo.VehicleType.KatashikiCode) {
                                                $scope.mDataDetail.KatashikiCodex   = dataWo.VehicleType.KatashikiCode;

                                            }
                                        }

                                        if ($scope.mDataDetail.KatashikiCodex != null && $scope.mDataDetail.KatashikiCodex != undefined && $scope.mDataDetail.KatashikiCodex != '') {
                                            AppointmentGrService.GetCVehicleTypeListByKatashiki(dataWo.VehicleType.KatashikiCode).then(function(res) {
                                                // $scope.typeData = res.data.Result[0];
                                                $scope.typeData = res.data.Result;

                                                for (var z = 0; z < $scope.typeData.length; z++) {
                                                    $scope.typeData[z].NewDescription = $scope.typeData[z].Description + ' - ' + $scope.typeData[z].KatashikiCode + ' - ' + $scope.typeData[z].SuffixCode
                                                }

                                                for (var z = 0; z < $scope.typeData.length; z++) {
                                                    if($scope.typeData[z].VehicleTypeId == dataWo.VehicleType.VehicleTypeId){
                                                        $scope.selectType($scope.typeData[z])
                                                        console.log('Type Gue ===> ',dataWo.VehicleType.VehicleTypeId +" == ModelOptions "+$scope.typeData[z].VehicleTypeId);
                                                    }
                                                }

                                            })
                                        }

                                    });
                                }
                            }
                        });
                        console.log("$scope.mDataDetail = {};", $scope.mDataDetail);
                    }
                });
            }
        };
        $scope.unDisable = function(vin) {
            console.log("wanjay");

            $scope.MRS.category = 2;
            var category = 2;
            var VIN = vin;
            var startDate = '-';
            var endDate = '-';
            var param = 1;      // tab riwayat servis
            $scope.ServiceHistory(category, VIN, startDate, endDate, param);
        }
        $scope.mDataVehicleHistory = [];
        $scope.change = function(startDate, endDate, category, vin) {

            console.log("start Date 1", startDate);
            console.log("start Date 2", endDate);
            category = category.toString();
            var copyAngular = angular.copy(category);
            console.log("category", category);
            console.log("Copycategory", copyAngular);
            console.log("vin", vin);
            if (category == "" || category == null || category == undefined) {
                bsNotify.show({
                    size: 'big',
                    title: 'Mohon Isi Kategori Pekerjaan',
                    text: 'I will close in 2 seconds.',
                    timeout: 2000,
                    type: 'danger'
                });
            } else {

                if (startDate == undefined || startDate == "" || startDate == null || startDate == "Invalid Date") {
                    bsNotify.show({
                        size: 'big',
                        title: 'Mohon Isi periode servis',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                } else {

                    var tmpDate = startDate;
                    var tmpMonth = startDate;
                    console.log("tmpMonth", tmpMonth);
                    var tmpTime = $scope.startDate;
                    // if (tmpDate.getMonth() < 10) {
                    // tmpMonth = "0" + (tmpDate.getMonth() + 1);
                    // } else {
                    tmpMonth = (tmpDate.getMonth() + 1).toString();
                    // }
                    var ini = tmpDate.getFullYear() + "-" + (tmpMonth[1] ? tmpMonth : "0" + tmpMonth[0]) + "-" + tmpDate.getDate();
                    var d = new Date(ini);
                    console.log('data yang diinginkan', ini);
                    startDate = ini;
                };


                if (endDate == undefined || endDate == "" || endDate == null || endDate == "Invalid Date") {
                    bsNotify.show({
                        size: 'big',
                        title: 'Mohon Isi periode servis',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                } else {

                    var tmpDate2 = endDate;
                    var tmpMonth2 = endDate;
                    console.log("tmpMonth2", tmpMonth2);
                    var tmpTime2 = endDate;

                    // if (tmpDate2.getMonth() < 10) {
                    //     tmpMonth2 = "0" + (tmpDate2.getMonth() + 1);
                    // } else {
                    tmpMonth2 = (tmpDate2.getMonth() + 1).toString();
                    // }
                    var ini2 = tmpDate2.getFullYear() + "-" + (tmpMonth2[1] ? tmpMonth2 : "0" + tmpMonth2[0]) + "-" + tmpDate2.getDate();
                    var d2 = new Date(ini2);
                    console.log('data yang diinginkan 2', ini2);
                    endDate = ini2;
                };

                if (typeof vin !== "undefined") {
                    category = parseInt(category);
                    $scope.ServiceHistory(category, vin, startDate, endDate);
                } else {
                    bsNotify.show({
                        size: 'big',
                        title: 'VIN Tidak Ditemukan',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                };
            };
        };


        $scope.getUnique = function (array){
            var uniqueArray = [];
            
            // Loop through array values
            for(i=0; i < array.length; i++){
                if(uniqueArray.indexOf(array[i]) === -1) {
                    uniqueArray.push(array[i]);
                }
            }
            return uniqueArray;
        }

        $scope.ServiceHistory = function(category, vin, startDate, endDate, param) {
            VehicleHistory.getData(category, vin, startDate, endDate).then(
                function(res) {
                    if (res.data.length == 0) {
                        $scope.mDataVehicleHistory = [];
                        bsNotify.show({
                            size: 'big',
                            title: 'Data Riwayat Service Tidak Ditemukan',
                            text: 'I will close in 2 seconds.',
                            timeout: 2000,
                            type: 'danger'
                        });
                    } else {
                        res.data.sort(function(a, b) {
                            // Turn your strings into dates, and then subtract them
                            // to get a value that is either negative, positive, or zero.
                            return new Date(b.ServiceDate) - new Date(a.ServiceDate);
                        });
                        for(var i in res.data){
                            var tmpArray = [];
                            for(var j = 0; j<res.data[i].VehicleJobService.length; j++){
                                if(res.data[i].VehicleJobService[j].KTPNo == null || res.data[i].VehicleJobService[j].KTPNo == undefined){
                                    res.data[i].VehicleJobService[j].KTPNo = ''
                                }
                                if(!(res.data[i].VehicleJobService[j].KTPNo.includes('/'))){
                                    if(res.data[i].VehicleJobService[j].EmployeeName !== null){
                                        tmpArray.push(res.data[i].VehicleJobService[j].EmployeeName);
                                    }
                                }
                                
                            }
                            tmpArray = $scope.getUnique(tmpArray);
                            res.data[i].namaTeknisi = tmpArray.join(', ');
                        }
                        $scope.mDataVehicleHistory = res.data;

                        $scope.isKmHistory = $scope.mDataVehicleHistory.length;

                        if ($scope.isKmHistory >= 1) {

                            $scope.KMTerakhir = $scope.mDataVehicleHistory[0].Km;

                            $scope.restart_digit_odo = 0
                            WO.GetResetCounterOdometer(vin).then(function(res) {
                                $scope.restart_digit_odo = res.data
                            })

                        }
                        
                        console.log("kesini ga?");
                        $scope.value = angular.copy($scope.mDataVehicleHistory);

                        $scope.MRS.category = category;
                        
                        if (param == 1) {       // tab riwayat servis

                            var dateEnd = $scope.mDataVehicleHistory[0].ServiceDate;
                            $scope.MRS.endDate = dateEnd;

                            var terakhir = $scope.mDataVehicleHistory.length;
                            $scope.MRS.startDate = $scope.mDataVehicleHistory[terakhir - 1].ServiceDate;

                        }
                        console.log('category', $scope.MRS.category);
                        console.log('startDate', $scope.MRS.startDate);
                        console.log('EndDate', $scope.MRS.endDate);
                    };

                    console.log("mDataVehicleHistory=> Followup ", $scope.mDataVehicleHistory);
                },
                function(err) {}
            );
        }
        $scope.dataForPrediagnose = function(data) {
            FollowUpBpService.getPreDiagnoseByJobId(data.JobId).then(function(res) {
                tmpPrediagnose = res.data;
                console.log("resss Prediagnosenya bosque", res.data);
            });
        }


        $scope.dataForWAC = function() {
            if ($scope.clickWACcount == 0) {
                RepairProcessGR.getDataWac($scope.mData.JobId).then(
                    function(res) {
                        var dataWac = res.data;
                        $scope.finalDataWac = dataWac;

                        console.log("Data WAC", dataWac);
                        $scope.mData.OtherDescription = dataWac.Other;
                        dataWac.MoneyAmount = String(dataWac.MoneyAmount);
                        $scope.mData.moneyAmount = dataWac.MoneyAmount.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        // $scope.mData.moneyAmount = dataWac.MoneyAmount;
                        $scope.mData.BanSerep = dataWac.isSpareTire;
                        $scope.mData.CdorKaset = dataWac.isCD;
                        $scope.mData.Payung = dataWac.isUmbrella;
                        $scope.mData.Dongkrak = dataWac.isJack;
                        $scope.mData.STNK = dataWac.isVehicleRegistrationNumber;
                        $scope.mData.P3k = dataWac.isP3k;
                        $scope.mData.KunciSteer = dataWac.isSteerLock;
                        $scope.mData.ToolSet = dataWac.isToolSet;
                        $scope.mData.ClipKarpet = dataWac.isCarpetClip;
                        $scope.mData.BukuService = dataWac.isServiceBook;
                        $scope.mData.AlarmCondition = dataWac.isAlarmConditionOk;
                        $scope.mData.AcCondition = dataWac.isAcOk;
                        $scope.mData.PowerWindow = dataWac.isPowerWindowOk;
                        $scope.mData.CarType = dataWac.isPowerWindowOk;
                        // $scope.slider.options = 100;
                        $scope.slider.value = dataWac.Fuel;
                        // $scope.mData.TechnicianNotes = TeknisiNote[0].TechnicianNotes;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
                RepairProcessGR.getProbPoints($scope.mData.JobId, null).then(
                    function(res) {
                        console.log("RepairProcessGR.getProbPoints", res.data);
                        mstId = angular.copy(res.data.Result[0].TypeId);
                        console.log("ini MstId", mstId);
                        $scope.filter.vType = mstId;
                        console.log("probPoints", res.data.Result);
                        $scope.chooseVType(mstId);
                        $scope.areasArray = getImgAndPoints(mstId);
                        console.log("$scope.areasArray", $scope.areasArray);
                        if (res.data.Result.length > 0) {
                            for (var i in res.data.Result) {
                                var getTemp = JSON.parse(res.data.Result[i]["Points"]);
                                for (var j in getTemp) {
                                    var xtemp = angular.copy(getTemp[j]);
                                    delete xtemp['status'];
                                    console.log("xtemp : ", xtemp);
                                    if (typeof _.find($scope.areasArray, xtemp) !== 'undefined') {
                                        console.log("find..", xtemp);
                                        var idx = _.findIndex($scope.areasArray, xtemp);
                                        xtemp.cssClass = "choosen-area";
                                        $scope.areasArray[idx] = xtemp;

                                        dotsData[mstId].push({ "ItemId": xtemp.areaid, "ItemName": xtemp.name, "Points": xtemp, "Status": getTemp[j].status });
                                    }
                                }
                            }
                            // setDataGrid(dotsData[mstId]);
                        }
                    },
                    function(err) {
                        $scope.areasArray = getImgAndPoints(mstId);
                    }
                );
                $scope.clickWACcount++;
            } else {
                console.log("di click mulu, rese but sih lu");
            }

        }

        var Parts = [];
        $scope.dataForBslist = function(data) {
            gridTemp = []
            Parts.splice();
            Parts = [];
            if (data.JobTask.length > 0) {
                var tmpJob = data.JobTask;
                console.log("tmpJob dataForBslist ===>", tmpJob);
                // console.log("tmpJob dataForBslist ===>", JSON.stringify(tmpJob));
                for (var i = 0; i < tmpJob.length; i++) {
                    for (var j = 0; j < tmpJob[i].JobParts.length; j++) {
                        console.log("tmpJob[i].JobParts[j]", tmpJob[i].JobParts[j]);
                        if (tmpJob[i].JobParts[j].PaidBy !== null) {
                            tmpJob[i].JobParts[j].paidName = typeof tmpJob[i].JobParts[j].PaidBy == "object" ? tmpJob[i].JobParts[j].PaidBy.Name : tmpJob[i].JobParts[j].paidName;
                            // delete tmpJob[i].JobParts[j].PaidBy;
                        }
                        if (tmpJob[i].JobParts[j].Satuan !== null) {
                            tmpJob[i].JobParts[j].satuanName = typeof tmpJob[i].JobParts[j].Satuan == "object" ? tmpJob[i].JobParts[j].Satuan.Name :tmpJob[i].JobParts[j].satuanName ;
                            tmpJob[i].JobParts[j].Name = typeof tmpJob[i].JobParts[j].Satuan == "object" ? tmpJob[i].JobParts[j].Satuan.Name :tmpJob[i].JobParts[j].satuanName ;
                            // delete tmpJob[i].JobParts[j].Satuan;
                        }
                        tmpJob[i].subTotal = tmpJob[i].Qty * tmpJob[i].RetailPrice;
                    }
                    if (tmpJob[i].isDeleted == 0) {
                        dataJobTaskPlan.push({
                            // TaskBPId: i,
                            EstimationBPId : typeof tmpJob[i].EstimationBPId == undefined ? 0 : tmpJob[i].EstimationBPId,
                            TaskBPId: tmpJob[i].TaskBPId ? tmpJob[i].TaskBPId : i,
                            TaskName: tmpJob[i].TaskName,
                            BodyEstimationMinute: tmpJob[i].BodyEstimationMinute,
                            FIEstimationMinute: tmpJob[i].FIEstimationMinute,
                            PolishingEstimationMinute: tmpJob[i].PolishingEstimationMinute,
                            PutyEstimationMinute: tmpJob[i].PutyEstimationMinute,
                            ReassemblyEstimationMinute: tmpJob[i].ReassemblyEstimationMinute,
                            SprayingEstimationMinute: tmpJob[i].SprayingEstimationMinute,
                            SurfacerEstimationMinute: tmpJob[i].SurfacerEstimationMinute,
                            Total: tmpJob[i].BodyEstimationMinute+tmpJob[i].FIEstimationMinute+tmpJob[i].PolishingEstimationMinute+tmpJob[i].PutyEstimationMinute+tmpJob[i].ReassemblyEstimationMinute+tmpJob[i].SprayingEstimationMinute+tmpJob[i].SurfacerEstimationMinute

                        });
                    }
                }
                $scope.gridEstimation.data = dataJobTaskPlan;
                // $scope.calculateforwork();
                // added by sss on 2017-11-03
                $scope.getAvailablePartsServiceManual(tmpJob, 0, 0);
                //
                $scope.gridWork = gridTemp;
                $scope.gridOriginal = gridTemp;
            }
            $scope.sumAllPrice();
            console.log("$scope.gridWork", $scope.gridWork);
            console.log("$scope.JobRequest", $scope.JobRequest);
            console.log("$scope.JobComplaint", $scope.JobComplaint);
            console.log("$scope.gridEstimation.data", $scope.gridEstimation.data);
        };


        $scope.countPrice = function() {
            var totalW = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                console.log("$scope.gridWork[i]", $scope.gridWork[i]);
                if ($scope.gridWork[i].Price !== undefined) {
                    totalW += $scope.gridWork[i].Price;
                }
            }
            // $scope.totalWork = totalW / (1.1);  //request pak dodi dibagi 1.1 13/02/2019
            $scope.totalWork = totalW / ((1+(PPNPerc/100)));  //request pak dodi dibagi 1.1 13/02/2019

            console.log(totalW);
            // ============
            var totalM = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].RetailPrice !== undefined) {
                            totalM += $scope.gridWork[i].child[j].RetailPrice;
                            console.log("$scope.gridWork[i].child.Price", $scope.gridWork[i].child[j].RetailPrice);
                        }
                    }
                }
            }
            // $scope.totalMaterial = totalM / (1.1);  //request pak dodi dibagi 1.1 13/02/2019
            $scope.totalMaterial = totalM / ((1+(PPNPerc/100)));  //request pak dodi dibagi 1.1 13/02/2019

        };
        $scope.onSelectRows = function(rows) {
            console.log("form onSelectRows=>", rows);
            if (rows.length != 0) {
                FollowUpBpService.getJobParts(rows[0].JobId).then(function(resu) {
                    console.log("form onSelectRows getJobParts=>", resu.data.Result);
                    var data = resu.data.Result;
                    $scope.gridFrontParts.data = data;
                })
            }
        };
        $scope.tes = function(data) {
            if (data !== undefined) {
                console.log(data);
            } else {
                console.log("Error");
            }
        };
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.gridFrontParts = {
            enableSorting: true,
            enableRowSelection: false,
            columnDefs: [{
                    name: "No. Material",
                    displayName: "No. Material",
                    field: 'PartsCode',
                    width: '18%'
                },
                {
                    name: "Nama Material",
                    displayName: "Nama Material",
                    field: 'PartsName',
                    width: '20%'
                },
                {
                    name: "Qty Order",
                    displayName: "Qty. Order",
                    field: 'qty',
                    width: '8%'
                },
                {
                    name: "ETA Part",
                    displayName: "ETA Part",
                    field: 'ETA',
                    cellFilter: dateFilter,
                    width: '18%'
                },
                {
                    name: "Nilai DP",
                    displayName: "Nilai DP",
                    field: "DownPayment",
                    width: '18%'
                },

                

                {
                    name: "Harga",
                    displayName: "Harga",
                    field: "RetailPrice",
                    width: '18%',
                    cellFilter: 'number : 0'
                },
                {
                    name: "Status DP",
                    displayName: "Status DP",
                    field: "StatusDP",
                    width: '18%'
                }
                // { name: "Keterangan" }
            ]
        };


        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
            // paginationPageSizes: [250, 500, 1000],
            // pageSize: 10,
            // widthForHorizontalScrolling: Math.floor(Math.random() * (120 - 30 + 1)) + 50,
            //showTreeExpandNoChildren: true,
            paginationPageSizes: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200],
            // paginationPageSize: 250,
            // pageSize: 10,
            columnDefs: [{
                    name: 'Tipe Follow Up',
                    field: 'TypeFollow'
                },
                {
                    name: 'Nomor Appointment',
                    field: 'AppointmentNo',
                    width: '15%'
                },
                {
                    name: 'Tanggal',
                    field: 'AppointmentDate',
                    cellFilter: dateFilter
                },
                {
                    name: 'Jam',
                    field: 'AppointmentTime'
                },
                {
                    name: 'No Polisi',
                    field: 'LicensePlate'
                },
                {
                    name: 'No Rangka',
                    field: 'VIN'
                },
                {
                    name: 'Customer',
                    field: 'Name'
                },
                {
                    name: 'Telepon',
                    field: 'Phone'
                },
                {
                    name: 'Status Appointment',
                    field: 'Xstatus'
                },
                {
                    name: 'Kategori',
                    field: 'Category'
                },
                {
                    name: 'Action',
                    width: 180,
                    pinnedRight: true,
                    cellTemplate: gridActionButtonTemplate
                }
            ],
            onRegisterApi: function(gridApi) {
                // set gridApi on $scope
                $scope.gridApi = gridApi;
                $scope.gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                    // console.log("selected=>",$scope.selectedRows);
                    if ($scope.onSelectRows) {
                        $scope.onSelectRows($scope.selectedRows);
                    }
                });
            }
        };

        $scope.getTableHeight = function() {
            var rowHeight = 30; // your row height
            var headerHeight = 50; // your header height
            var filterHeight = 40; // your filter height
            var pageSize = $scope.grid.paginationPageSize;
            if ($scope.grid.paginationPageSize > $scope.grid.data.length) {
                pageSize = $scope.grid.data.length;
            }
            if (pageSize < 4) {
                pageSize = 3;
            }

            return {
                height: (pageSize * rowHeight + headerHeight) + 50 + "px"
            };
        };
        $scope.gridDetailGr = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'No Material',
                    field: 'NoMaterial'
                },
                {
                    name: 'Nama Material',
                    field: 'NameMaterial'
                },
                {
                    name: 'Qty Order',
                    field: 'Qty'
                },
                {
                    name: 'ETA Part',
                    field: 'Eta'
                },
                {
                    name: 'Keterangan',
                    field: 'Ket'
                }
            ]
        };
        //===============BS List===========
        var gridTemp = [];
        $scope.gridWork = [];
        $scope.listApi = {};
        $scope.listSelectedRows = [];
        $scope.lmModel = {};
        $scope.ldModel = {};
        $scope.typeMData = [{ Id: 1, Name: 1 },
            { Id: 2, Name: 2 },
            { Id: 3, Name: 3 },
            { Id: 6, Name: "T" }
        ];

        $scope.addDetail = function(data, item, itemPrice, row) {
            console.log('clearDetail', $scope.listApi.clearDetail());
            $scope.listApi.clearDetail();
            row.FlatRate = data.FlatRate;
            row.catName = $scope.tmpCatg.Name;
            row.ActualRate = data.FlatRate;
            row.tmpTaskId = null;
            // row.cusType=$scope.tmpCus.Name
            if ($scope.tmpPaidById !== null) {
                row.PaidById = $scope.tmpPaidById;
                row.PaidBy = $scope.tmpPaidName;
            }
            console.log("itemPrice", itemPrice);
            if (itemPrice.length > 0) {
                for (var i = 0; i < itemPrice.length; i++) {
                    console.log("scope.tmpCatg.MasterId", $scope.tmpCatg.MasterId, $scope.tmpServiceRate);
                    // if($scope.tmpCatg.MasterId !== undefined && $scope.tmpCatg.MasterId !== null){
                    //     $scope.checkServiceRate(data.FlatRate,$scope.tmpCatg.MasterId,1);
                    // }

                    // if (itemPrice[i].ServiceRate !== undefined) {
                    //     var harganya = itemPrice[i].ServiceRate;
                    //     var harganyaa = data.FlatRate;
                    //     var summary = harganya * harganyaa;
                    //     var summaryy = parseInt(summary);
                    //     row.Summary = Math.round(summaryy);
                    //     // row.Fare = itemPrice[i].AdjustmentServiceRate;
                    //     row.Fare = itemPrice[i].ServiceRate;
                    // $scope.PriceAvailable = true;
                    // } else {
                    //     row.Fare = itemPrice[i].ServiceRate;
                    //     row.Summary = row.Fare;
                    // }
                    var sum = 0;
                    var tmpItem;
                    if ($scope.tmpCatg.MasterId == 59 || $scope.tmpCatg.MasterId == 60) {
                        if ($scope.tmpServiceRate !== null) {
                            if (data.FlatRate !== null) {
                                tmpItem = data.FlatRate.toString();
                                if (tmpItem.includes(".")) {
                                    tmpItem = tmpItem.split('.');
                                    if (tmpItem[tmpItem.length - 1] !== ".") {
                                        // item = parseInt(tmpItem);
                                        sum = $scope.tmpServiceRate.WarrantyRate * data.FlatRate;
                                        sum = Math.round(sum);
                                    }
                                } else {
                                    sum = $scope.tmpServiceRate.WarrantyRate * data.FlatRate;
                                    sum = Math.round(sum);
                                }
                            } else {
                                sum = $scope.tmpServiceRate.WarrantyRate;
                                sum = Math.round(sum);
                            }
                            $scope.PriceAvailable = true;
                            sum = Math.round(sum);
                            row.Summary = sum;
                            row.Fare = $scope.tmpServiceRate.WarrantyRate;
                            console.log("row WarrantyRate lewat $scope.addDetail", row);
                        }
                    } else {
                        if ($scope.tmpServiceRate !== null) {
                            if (data.FlatRate !== null) {
                                tmpItem = data.FlatRate.toString();
                                if (tmpItem.includes(".")) {
                                    tmpItem = tmpItem.split('.');
                                    if (tmpItem[tmpItem.length - 1] !== ".") {
                                        // data.FlatRate = parseInt(tmpItem);
                                        sum = $scope.tmpServiceRate.ServiceRate * data.FlatRate;
                                        sum = Math.round(sum);
                                    }
                                } else {
                                    sum = $scope.tmpServiceRate.ServiceRate * data.FlatRate;
                                    sum = Math.round(sum);
                                }
                            } else {
                                sum = $scope.tmpServiceRate.ServiceRate;
                                sum = Math.round(sum);
                            }
                            $scope.PriceAvailable = true;
                            sum = Math.round(sum);
                            row.Summary = sum;
                            row.Fare = $scope.tmpServiceRate.ServiceRate;
                            console.log("row ServiceRate lewat $scope.addDetail", row);
                        }
                    }
                    row.ActualRate = data.FlatRate;
                    console.log("row terakhir lewat $scope.addDetail", row);
                }
            } else {
                $scope.PriceAvailable = false;
                row.Fare = "";
                row.Summary = "";
            }
            row.TaskId = data.TaskId;
            row.IsOPB = data.IsOPB ? data.IsOPB : null;
            var tmp = {}
                // $scope.listApi.addDetail(tmp,row)
            if (item.length > 0) {
                for (var i = 0; i < item.length; i++) {
                    // var tmpItem = item[i];
                    // $scope.getAvailablePartsService(item[i]);
                    // if ($scope.tmpPaidById !== null) {
                    //     item[i].PaidById = $scope.tmpPaidById;
                    //     item[i].paidName = $scope.tmpPaidName;
                    // }
                    // item[i].subTotal = item[i].Qty * item[i].RetailPrice;
                    // // console.log("item[i].subTotal",item[i].subTotal);
                    // tmp = item[i]
                    // $scope.listApi.addDetail(tmp, row);
                    $scope.getAvailablePartsService(item[i], row);
                }
            } else {
                $scope.listApi.addDetail(false, row);
            }
        };

        $scope.onListSelectRows = function(rows) {
            console.log("form controller=>", rows);
        }
        $scope.gridPartsDetail = {

            columnDefs: [
                // { name: "NoMaterial", field: "PartsCode" },
                // { name: "NamaMaterial", field: "PartsName" },
                // { name: "Qty", field: "Qty" },
                // { name: "Satuan" },
                // { name: "Ketersedian" },
                // { name: "Tipe" },
                // { name: "ETA" },
                // { name: "Harga", field: "Parts.RetailPrice" },
                // { name: "Disc" },
                // { name: "SubTotal" },
                // { name: "NilaiDp" },
                // { name: "StatusDp" },
                // { name: "Pembayaran" },
                // { name: "OPB" }
                { name: "NoMaterial", field: "PartsCode", width: '18%' },
                { name: "NamaMaterial", field: "PartsName", width: '18%' },
                { name: "Qty", field: "Qty", width: '12%' },
                { name: "Satuan", field: "satuanName", width: '12%' },
                // { name: "Satuan", field: "SatuanId", cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.unitDataGrid[row.entity.SatuanId]}}</div>' },
                { name: "Ketersedian", field: "Availbility", width: '12%' },
                { name: "Tipe", field: "OrderType", width: '12%' ,
                        cellTemplate: '<div style="text-align:center;margin-top: 5px;" ng-show="row.entity.OrderType == 6">T</div><div style="text-align:center;margin-top: 5px;" ng-show="row.entity.OrderType != 6">{{row.entity.OrderType}}</div>' 
                },
                { name: "ETA", field: "ETA", cellFilter: dateFilter, width: '12%' },
                { name: "Harga", field: "RetailPrice", width: '12%',cellFilter: 'currency:"":0' },

                // replace CR4
                { name: "Diskon", field: "DiskonForDisplay", width: '12%' },
                { name: "SubTotal", field: "subTotalForDisplay", width: '12%',cellFilter: 'currency:"":0' },
                // replace CR4

                // { name: "Diskon", field: "Discount", width: '12%' },// ini sebelum CR4 
                // { name: "SubTotal", field: "subTotal", width: '12%',cellFilter: 'currency:"":0'},// ini sebelum CR4 
                { name: "Nilai DP", field: "DownPayment", width: '12%',cellFilter: 'currency:"":0' },

                { name: "StatusDp", field: "StatusDp", width: '12%', cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>' },
                { name: "Pembayaran", field: "paidName", width: '12%' },
                { name: "OPB", field: "IsOPB", width: '12%', cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="null" disabled="disabled"></div>' }
            ]
        }
        $scope.gridWork = $scope.gridWork;
        // $scope.gridEstimation = {
        //         enableSorting: true,
        //         enableSelectAll: true,
        //         enableCellEdit: true,
        //         enableCellEditOnFocus: true, // set any editable column to allow edit on focus
        //         columnDefs: [
        //             { name: "Task Name", field: "TaskName", enableCellEdit: false },
        //             { name: "Body", field: "BodyEstimationMinute" },
        //             { name: "Putty", field: "PutyEstimationMinute" },
        //             { name: "Surfacer", field: "SurfacerEstimationMinute" },
        //             { name: "Spraying", field: "SprayingEstimationMinute" },
        //             { name: "Polishing", field: "PolishingEstimationMinute" },
        //             { name: "Reassembly", field: "ReassemblyEstimationMinute" },
        //             { name: "FI", field: "FIEstimationMinute" }
        //         ]
        //     }
        $scope.gridEstimation = {
            enableCellEditOnFocus: true,
            showColumnFooter: true,

            columnDefs: [{
                'name': 'Order Pekerjaan',
                'field': 'TaskName',
                enableCellEdit: false,
                footerCellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;">Time Per Stall</div>'
            }, {
                'name': 'Body',
                'field': 'BodyEstimationMinute',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true,
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                cellFilter: 'number'

            }, {
                'name': 'Putty',
                'field': 'PutyEstimationMinute',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true,
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                cellFilter: 'number'

            }, {
                'name': 'Surfacer',
                'field': 'SurfacerEstimationMinute',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true,
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                cellFilter: 'number'

            }, {
                'name': 'Painting',
                'field': 'SprayingEstimationMinute',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true,
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                cellFilter: 'number'

            }, {
                'name': 'Polishing',
                'field': 'PolishingEstimationMinute',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true,
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                cellFilter: 'number'

            }, {
                'name': 'Reassembbly',
                displayName: "Re-Assembly",
                'field': 'ReassemblyEstimationMinute',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true,
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                cellFilter: 'number'

            }, {
                'name': 'Final Inspection',
                'field': 'FIEstimationMinute',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true,
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                cellFilter: 'number'

            }, {
                'name': 'Total',
                'field': 'Total',
                enableCellEdit: false,
                cellTemplate: '<div class="ui-grid-cell-contents" ng-model="MODEL_COL_FIELD =(grid.appScope.changeInt(row.entity.BodyEstimationMinute) + grid.appScope.changeInt(row.entity.PutyEstimationMinute) + grid.appScope.changeInt(row.entity.SurfacerEstimationMinute) + grid.appScope.changeInt(row.entity.SprayingEstimationMinute) +  grid.appScope.changeInt(row.entity.PolishingEstimationMinute) + grid.appScope.changeInt(row.entity.ReassemblyEstimationMinute) + grid.appScope.changeInt(row.entity.FIEstimationMinute) )">{{grid.appScope.changeInt(row.entity.BodyEstimationMinute) + grid.appScope.changeInt(row.entity.PutyEstimationMinute) + grid.appScope.changeInt(row.entity.SurfacerEstimationMinute) + grid.appScope.changeInt(row.entity.SprayingEstimationMinute) +  grid.appScope.changeInt(row.entity.PolishingEstimationMinute) + grid.appScope.changeInt(row.entity.ReassemblyEstimationMinute) + grid.appScope.changeInt(row.entity.FIEstimationMinute) }}</div>',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                footerCellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;">{{grid.appScope.gridEstimationApi.grid.columns[8].getAggregationValue()}}</div>',
                aggregationHideLabel: true,
                cellFilter: 'number'

            }],
            onRegisterApi: function(gridApi) {
                $scope.gridEstimationApi = gridApi;
                // $scope.gridEstimationApi.selection.on.rowSelectionChanged($scope, function (row) {
                //     $scope.selectedRow = row.entity;
                // });
                // if ($scope.gridEstimationApi.selection.selectRow) {
                //     $scope.gridEstimationApi.selection.selectRow($scope.gridEstimation.data[0]);
                // }
            }
        };
        $scope.changeInt = function(item) {
            if (item !== null && item != undefined && item != '') {
                item = parseInt(item);
                return item
            } else {
                return 0
            }
        }
        
        $scope.JobIsWarranty = 0;
        $scope.tipePembayaranJob = null;
        $scope.selectTypeCust = function(data, model) {
            console.log("selectTypeCust ===>", data);

            // ---------------- cek kl dari warranty di ganti jd bukan warranty / sebaliknya ga blh ganti ------------------------ start

            if ($scope.copyAwalPembayaranJob != null && $scope.copyAwalPembayaranJob != undefined) {
                // berarti dia edit job dan mau ubah pembayaran
                if ($scope.copyAwalPembayaranJob == 30) {
                    if (model.PaidById != 30) {
                        model.PaidBy = "Warranty"
                        model.PaidById = 30
                        // data.MasterId = 30
                        bsAlert.warning('Tidak dapat mengganti pembayaran job warranty ke non-warranty, silahkan hapus & input baru.')
                    }
                } else if ($scope.copyAwalPembayaranJob != 30 && $scope.copyAwalPembayaranJob != null) {
                    if (model.PaidById == 30) {
                        // model.PaidBy = "Warranty"
                        _.map($scope.paymentData, function(val) {
                            if (val.MasterId == $scope.copyAwalPembayaranJob) {
                                model.PaidBy = angular.copy(val.Name)
                                // data.Name = angular.copy(val.Name)
                            }
                        });
                        model.PaidById = $scope.copyAwalPembayaranJob
                        // data.MasterId = $scope.copyAwalPembayaranJob
                        
                        bsAlert.warning('Tidak dapat mengganti pembayaran job non-warranty ke warranty, silahkan hapus & input baru.')
                    }
                }
            } else {
                var cekpembayaranawal = angular.copy($scope.lmModel.PaidBy)
                var bedapembayaran_W = 0;

                if (cekpembayaranawal == 'Warranty' && model.PaidById != 30){
                    // ini dari warranty di ganti jd bukan warranty
                    model.TaskName = null
                    model.FlatRate = null
                    model.Fare = 0
                    model.FareMask = 0
                    model.UOM = null
                    model.Discount = 0
                    model.DiscountTypeId = -1
                    model.DiscountTypeListId = null
                    model.UnitBy = "Panel"
                    model.TFirst1 = null
                    model.child = []
                    model.WarrantyRate = null

                    bedapembayaran_W = 1
                }

                if (cekpembayaranawal != 'Warranty' && model.PaidById == 30){
                    // ini dari bukan warranty di ganti jd warranty
                    model.TaskName = null
                    model.FlatRate = null
                    model.Fare = 0
                    model.FareMask = 0
                    model.UOM = null
                    model.Discount = 0
                    model.DiscountTypeId = -1
                    model.DiscountTypeListId = null
                    model.UnitBy = "Panel"
                    model.TFirst1 = null
                    model.child = []
                    model.WarrantyRate = null

                    bedapembayaran_W = 1
                }
            }

            // ---------------- cek kl dari warranty di ganti jd bukan warranty / sebaliknya ga blh ganti ------------------------ end

            
            $scope.tmpCus = data;
            $scope.lmModel.PaidBy = data.Name;

            _.map($scope.paymentData, function(val) {
                if (val.MasterId == model.PaidById) {
                    $scope.lmModel.PaidBy = angular.copy(val.Name)
                    model.PaidBy = angular.copy(val.Name)
                }
            });

            // $scope.tipePembayaranJob = data.MasterId;
            $scope.tipePembayaranJob = model.PaidById

            console.log('tipe bayar', $scope.tipePembayaranJob)

            if (model.PaidById == 30) {
                $scope.JobIsWarranty = 1;
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }]; // kl pekerjaan twc / pwc ga blh pilih bahan
                console.log('cek ldmodel', $scope.ldModel)
                $scope.ldModel.MaterialTypeId = null;
            } else {
                $scope.JobIsWarranty = 0;
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
                model.TFirst1 = null
                model.WarrantyRate = null
            }

            if (bedapembayaran_W == 1){
                $scope.listApi.addDetail(false, model);
                $scope.listApi.clearDetail();
                return
                
            }
            

            $scope.listApi.addDetail(false, model);

        };


        $scope.selectTypeParts = function(data) {
            $scope.ldModel.SatuanId = 4;
            $scope.ldModel.OrderType = 3;
            $scope.ldModel.PartsCode = null;
            $scope.ldModel.PartsId = null;
            if(data.MaterialTypeId == 1){
                $scope.ldModel.IsOPB = 0;
            }
            _.map($scope.unitData,function(val){
                if(val.MasterId == 4){
                    $scope.ldModel.satuanName = val.Name;
                }
            });

            if ($scope.JobIsWarranty == 1) {
                console.log('pekerjaan nya warranty nih parts nya jg harus', $scope.ldModel.PaidById);
                _.map($scope.paymentData, function(val) {
                    console.log("valll tipe", val);
                    if (val.Name === "Warranty") {
                        $scope.ldModel.PaidById = val.MasterId;
                        $scope.ldModel.DownPayment = 0;
                        $scope.ldModel.DPRequest = 0;
                        $scope.ldModel.minimalDp = 0;
                        $scope.disableDP = true;
                    }
                })

            } else {
                console.log('pekerjaan nya bukan warranty');
            }

        }

        $scope.selectTypePaidParts = function(data) {
            if($scope.gridPartsDetail.data.length > 0){
                for (var i = 0; i < $scope.gridPartsDetail.data.length; i++) {
                    if (($scope.ldModel.PartsCode !== null && $scope.ldModel.PartsCode !== undefined) && $scope.ldModel.PartsCode == $scope.gridPartsDetail.data[i].PartsCode && $scope.ldModel.PaidById == $scope.gridPartsDetail.data[i].PaidById ) {
                        console.log('parts sudah ada');
                        // bsNotify.show({
                        //     size: 'big',
                        //     title: 'Parts Sudah Ada!',
                        //     text: 'I will close in 2 seconds.',
                        //     timeout: 2000,
                        //     type: 'danger'
                        // });
                        bsAlert.warning('Parts Sudah Ada!','Silahkan ganti tipe pembayaran');
                        $scope.ldModel.PaidById = null;
                    }else{
                        console.log("selectTypePaidParts", data);
                        $scope.tmpPaidName = data;
                        $scope.ldModel.paidName = data.Name;
                        // if (data.Name == "Internal" || data.Name == "Warranty" || data.Name == " Free Service") {
                        if (data.MasterId != 28) {
                            $scope.ldModel.DownPayment = 0;
                            $scope.ldModel.DPRequest = 0;
                            $scope.ldModel.minimalDp = 0;
                            $scope.disableDP = true;
                            $scope.checkPrice($scope.ldModel.RetailPrice);

                        } else {
                            $scope.ldModel.minimalDp = $scope.ldModel.temporaryDp;
                            $scope.checkPrice($scope.ldModel.RetailPrice);
                            $scope.disableDP = false;
                        };
                        console.log("selectTypePaidParts", $scope.ldModel);
                    }
                }
            }else{
                console.log("selectTypePaidParts", data);
                $scope.tmpPaidName = data;
                $scope.ldModel.paidName = data.Name;
                // if (data.Name == "Internal" || data.Name == "Warranty" || data.Name == " Free Service") {
                if (data.MasterId != 28) {
                    $scope.ldModel.DownPayment = 0;
                    $scope.ldModel.DPRequest = 0;
                    $scope.ldModel.minimalDp = 0;
                    $scope.disableDP = true;
                    $scope.checkPrice($scope.ldModel.RetailPrice);
                } else {
                    $scope.ldModel.minimalDp = $scope.ldModel.temporaryDp;
                    $scope.checkPrice($scope.ldModel.RetailPrice);
                    $scope.disableDP = false;
                };
                console.log("selectTypePaidParts", $scope.ldModel);
            }
        };

        $scope.sendWork = function(key, data) {
            var taskandParts = [];
            var taskList = {};
            AppointmentBpService.getDataPartsByTaskId(data.TaskListBPId).then(function(res) {
                console.log("resabis ambil parts", res.data.Result);
                taskandParts = res.data.Result;
                // added by sss on 2017-09-15
                // get fare bukan task list..
                // AppointmentBpService.getDataTaskListByKatashiki($scope.mDataDetail.KatashikiCode).then(function(restask){
                //     taskList=restask.data.Result;
                //     console.log("taskList",taskList);
                //     $scope.addDetail(data,taskandParts,taskList,$scope.lmModel)
                // });
                //
            });
        }
        $scope.sendParts = function(key, data) {
            var tmpMaterialId = angular.copy($scope.ldModel.MaterialTypeId);
            console.log("a $scope.ldModel.Parts", $scope.ldModel);
            $scope.ldModel = data;
            var tmp = {};
            tmp = data;
            $scope.ldModel.PartsId = data.PartsId;
            $scope.ldModel.PartsName = data.PartsName;
            $scope.ldModel.MaterialTypeId = tmpMaterialId;
            $scope.ldModel.SatuanId = data.UomId;
            $scope.ldModel.satuanName = data.Name;
            $scope.ldModel.RetailPrice = null;


            if ($scope.JobIsWarranty == 1) {
                console.log('pekerjaan nya warranty nih parts nya jg harus', $scope.ldModel.PaidById);
                _.map($scope.paymentData, function(val) {
                    console.log("valll tipe", val);
                    if (val.Name === "Warranty") {
                        $scope.ldModel.PaidById = val.MasterId;
                        $scope.ldModel.DownPayment = 0;
                        $scope.ldModel.DPRequest = 0;
                        $scope.ldModel.minimalDp = 0;
                        $scope.disableDP = true;
                    }
                })

            } else {
                console.log('pekerjaan nya bukan warranty');
            }



            console.log("key Parts", key);
            console.log("data Parts", data);
        }
        $scope.onAfterCancel = function() {
            $scope.jobAvailable = false;
        }
        $scope.checkIsOPB = function(data, obj){
            $timeout(function(){
                if(obj.IsOPB == 1){
                    console.log("=====", data, obj)
                    $scope.ldModel.PartsCode = null;
                    $scope.ldModel.PartsId = null;
                    $scope.ldModel.PartId = null;
                    $scope.ldModel.PartsName = null
                    $scope.partsAvailable = false;
                }else{
                    $scope.partsAvailable = true;
                }
            },100)
        }
        $scope.checkDp = function(valueDP, param){
            console.log('DP =====>', valueDP, param);
            if(param.nightmareIjal == 0 && param.Availbility == "Tidak Tersedia"){
                $scope.ldModel.minimalDp = valueDP;
            }
        }
        $scope.checkAvailabilityParts = function(data) {
            console.log("data data data Parts", data);
            if (data.PartsId !== undefined) {
                AppointmentBpService.getAvailableParts(data.PartsId,0, $scope.ldModel.Qty).then(function(res) {
                    // ====== validasi cek kl retailprice nya null ato 0 ga blh di pake ==================================================== start
                    if ((res.data.Result[0].RetailPrice === 0 || res.data.Result[0].RetailPrice === null || res.data.Result[0].RetailPrice === undefined) && data.MaterialTypeId == 1){
                        bsAlert.warning('Harga Retail Belum Tersedia. Harap hubungi Partsman/Petugas Gudang Bahan.');
                        return
                    }
                    // ====== validasi cek kl retailprice nya null ato 0 ga blh di pake ==================================================== end
                    console.log("Ressss abis availability", res.data);
                    var tmpAvailability = res.data.Result[0];
                    tmpAvailability.RetailPrice = Math.round(tmpAvailability.RetailPrice);
                    $scope.ldModel.PaidById = $scope.tipePembayaranJob;
                    _.map($scope.paymentData,function(val){
                        if(val.MasterId == $scope.tipePembayaranJob ){
                            $scope.ldModel.PaidBy = val.Name;
                            $scope.selectTypePaidParts(val);
                        }
                    })
                    $scope.ldModel.RetailPriceMask =  tmpAvailability.RetailPrice.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                    // $scope.ldModel.RetailPrice = tmpAvailability.RetailPrice.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');

                    //ddddd                    
                    // $scope.ldModel.minimalDp = tmpAvailability.ValueDP;
                    $scope.ldModel.minimalDp = tmpAvailability.PriceDP;
                    $scope.ldModel.temporaryDp = Math.round(tmpAvailability.PriceDP);
                    // if ($scope.ldModel.PaidBy == "Internal" || $scope.ldModel.PaidBy == "Warranty" || $scope.ldModel.PaidBy == " Free Service" || $scope.JobIsWarranty == 1) {
                    if ($scope.ldModel.PaidById != 28 || $scope.JobIsWarranty == 1) {
                        $scope.ldModel.DownPayment = 0;
                        $scope.ldModel.DPRequest = 0;
                        $scope.ldModel.minimalDp = 0;
                        $scope.ldModel.nightmareIjal = 0;
                    } else {
                        $scope.ldModel.minimalDp = Math.round(tmpAvailability.PriceDP);
                        $scope.ldModel.DPRequest = Math.round(tmpAvailability.PriceDP);
                        $scope.ldModel.DownPayment = Math.round(tmpAvailability.PriceDP);
                        $scope.ldModel.nightmareIjal = Math.round(tmpAvailability.PriceDP);
                    }
                    if ($scope.ldModel.Qty !== undefined && $scope.ldModel.Qty !== null) {
                        $scope.checkPrice(tmpAvailability.RetailPrice);
                    }
                    if (tmpAvailability.isAvailable == 0) {
                        $scope.ldModel.Availbility = "Tidak Tersedia";
                        if (tmpAvailability.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                            $scope.ldModel.ETA = tmpAvailability.DefaultETA;
                        } else {
                            $scope.ldModel.ETA = tmpAvailability.DefaultETA;
                        }
                        if(tmpAvailability.PartsClassId3 == 113 && tmpAvailability.PartsClassId1 == 1) {
                            $scope.ldModel.OrderType = 6;
                        }
                        else {
                            $scope.ldModel.OrderType = 3;
                            data.OrderType = 3;
                        }
                        

                    } else if (tmpAvailability.isAvailable == 1){
                        $scope.ldModel.Availbility = "Tersedia";
                        //kl tersedia dp nya 0 in dan disable
                        $scope.ldModel.DownPayment = 0;
                    }else if (tmpAvailability.isAvailable == 2){
                        $scope.ldModel.Availbility = "Tersedia Sebagian";
                        //kl tersedia dp nya 0 in dan disable
                        $scope.ldModel.DownPayment = 0;
                        if(tmpAvailability.PartsClassId3 == 113 && tmpAvailability.PartsClassId1 == 1) {
                            $scope.ldModel.OrderType = 6;
                        }
                    }
                    console.log("$scope.ldmodel", $scope.ldModel);
                });
            }
        }
        $scope.getAvailablePartsService = function(item, row) {
            if (item.PartsId !== null) {
                AppointmentBpService.getAvailableParts(item.PartsId, 0 , item.Qty).then(function(res) {
                    var tmpRes = res.data.Result[0];
                    console.log("tmpRes", tmpRes);
                    tmpParts = tmpRes;
                    if (tmpParts !== null) {
                        console.log("have data RetailPrice");
                        item.RetailPrice = tmpParts.RetailPrice;
                        item.subTotal = item.Qty * tmpParts.RetailPrice;
                        item.DPRequest = tmpParts.PriceDP * item.Qty;
                        if (tmpParts.isAvailable == 0) {
                            item.Availbility = "Tidak Tersedia";
                            if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                item.ETA = tmpParts.DefaultETA;
                            } else {
                                item.ETA = tmpParts.DefaultETA;
                            }

                            if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                item.OrderType = 6;
                            }
                            else {
                                item.OrderType = 3;
                            }

                        } else if (tmpParts.isAvailable == 1){
                            item.Availbility = "Tersedia";
                            //kl tersedia dp nya 0 in dan disable
                            item.DownPayment = 0;
                        } else if (tmpParts.isAvailable == 2){
                            item.Availbility = "Tersedia Sebagian";
                            //kl tersedia dp nya 0 in dan disable
                            item.DownPayment = 0;
                            if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                item.OrderType = 6;
                            }
                        }

                        
                            for (var i in $scope.paymentData) {
                                if ($scope.paymentData[i].Name == "Customer") {
                                    item.PaidById = $scope.paymentData[i].MasterId;
                                    item.PaidBy = $scope.paymentData[i].Name;
                                }
                            }

                    } else {
                        console.log("haven't data RetailPrice");
                    }
                    if ($scope.tmpPaidById !== null) {
                        item.PaidById = $scope.tmpPaidById;
                        item.paidName = $scope.tmpPaidName;
                    }
                    tmp = item;
                    $scope.listApi.addDetail(tmp, row);
                    return item;
                });
            } else {
                if ($scope.tmpPaidById !== null) {
                    item.PaidById = $scope.tmpPaidById;
                    item.paidName = $scope.tmpPaidName;
                }
                item.RetailPrice = item.Price;
                item.subTotal = item.Qty * item.Price;
                item.DPRequest = item.DPRequest * item.Qty;
                tmp = item;
                $scope.listApi.addDetail(tmp, row);
                return item;
            }
        }
        $scope.getAvailablePartsServiceManual = function(item, x, y) {
            console.log("getAvailablePartsServiceManual", item);
            // console.log("getAvailablePartsServiceManual  ===>", JSON.stringify(item));
            console.log("x : ", x);
            console.log("y : ", y);

            if (x > item.length - 1) {
                return item;
            }
            if (item[x].isDeleted !== 1) {
                if (item[x].JobParts[y] !== undefined && item[x].JobParts[y].PartsId !== null) {
                    console.log('kondisi 1');
                    var itemTemp = item[x].JobParts[y];
                    // if (itemTemp.isDeleted == 0){
                        AppointmentBpService.getAvailableParts(itemTemp.PartsId, item[x].JobId, item[x].JobParts[y].Qty).then(function(res) {
                            var tmpParts = res.data.Result[0];
                            if (tmpParts !== null && tmpParts !== undefined) {
                                // console.log("have data RetailPrice",item,tmpRes);
                                // item[x].JobParts[y].RetailPrice = tmpParts.RetailPrice;
                                item[x].JobParts[y].RetailPrice    = item[x].JobParts[y].Price;
                                item[x].JobParts[y].minimalDp      = tmpParts.PriceDP;
                                item[x].JobParts[y].subTotal       = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                item[x].JobParts[y].NormalPrice    = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                item[x].JobParts[y].nightmareIjal  = item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty;
                                item[x].JobParts[y].temporaryDp  = item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty;

                
                                
                                // item.DiscountedPrice =  (normalPrice * item.Discount)/100;
                                // if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                //     item[x].JobParts[y].typeDiskon = -1;
                                //     item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                // } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                //     item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
                                //     item[x].JobParts[y].typeDiskon = 0;
                                // }
                                if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                    item[x].JobParts[y].typeDiskon = -1;
                                    item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                    item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                    // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
                                    item[x].JobParts[y].typeDiskon = 0;
                                } else if ((item[x].JobParts[y].IsCustomDiscount == 1 || item[x].JobParts[y].IsCustomDiscount == 0) && item[x].JobParts[y].Discount == 0) {
                                    item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                    // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                                    item[x].JobParts[y].typeDiskon = 0;
                                }
                                // tambahan CR4
                                else if(item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount != 0){
                                    item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                    item[x].JobParts[y].typeDiskon = item[x].JobParts[y].DiscountTypeId;
                                }
                                if (tmpParts.isAvailable == 0) {
                                    item[x].JobParts[y].Availbility = "Tidak Tersedia";
                                    if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                        item[x].JobParts[y].ETA = tmpParts.DefaultETA;
                                    } else {
                                        item[x].JobParts[y].ETA = tmpParts.DefaultETA;
                                    }
                                    if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                        item[x].JobParts[y].OrderType = 6;
                                    }else{
                                        item[x].JobParts[y].OrderType = 3;
                                    }
                                } else if (tmpParts.isAvailable == 2) {
                                    item[x].JobParts[y].Availbility = "Tersedia Sebagian";        
                                } else {
                                    item[x].JobParts[y].Availbility = "Tersedia";
                                    if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                        item[x].JobParts[y].OrderType = 6;
                                    }
                                    //kl tersedia dp nya 0 in dan disable
                                    // item[x].JobParts[y].DownPayment = 0;
                                }
                            } else {
                                console.log("haven't data RetailPrice");
                                item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                                item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                                item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                                item[x].JobParts[y].nightmareIjal = Math.round(item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty);
                                item[x].JobParts[y].temporaryDp = Math.round(item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty);
                            }
                            if (item[x].JobParts[y].Part !== null && item[x].JobParts[y].Part !== undefined) {
                                item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                                delete item[x].JobParts[y].Part;
                            }
                            if (itemTemp.isDeleted == 0){
                                Parts.push(item[x].JobParts[y]);
                                $scope.arrayDP(gridTemp, $scope.jejeranDP);
                                $scope.sumAllPrice();

                            }
                            
                            
                            // return item;
    
                            if (x <= (item.length - 1)) {
                                console.log('kondisi 2');
                                if (y >= (item[x].JobParts.length - 1)) {
                                    if (item[x].Fare !== undefined) {
                                        var sum = item[x].Fare;
                                        // var summ = item[x].FlatRate;
                                        // var summ = item[x].FlatRate ? item[x].FlatRate : 1;
                                        var summ = (item[x].FlatRate == null || item[x].FlatRate == undefined) ? 1 : item[x].FlatRate

                                        var summaryy = sum * summ;
                                        var discountedPrice = (summaryy * item[x].Discount) / 100;
                                        var normalPrice = summaryy
                                        summaryy = parseInt(sum);
                                    }
                                    if (item[x].JobType == null) {
                                        item[x].JobType = { Name: "" };
                                    }
                                    if (item[x].PaidBy == null) {
                                        item[x].PaidBy = { Name: "" }
                                    }
                                    if (item[x].Satuan == null) {
                                        item[x].Satuan = { Name: "" }
                                    }
                                    if (item[x].AdditionalTaskId == null) {
                                        item[x].isOpe = 0;
                                    } else {
                                        item[x].isOpe = 1;
                                    }
                                    // if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                    //     item[x].typeDiskon = -1;
                                    //     item[x].DiscountedPrice = normalPrice;
                                    // } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                    //     item[x].typeDiskon = 0;
                                    //     item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                    // }

                                    if(item[x].JobParts.length > 0 ){
                                        if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                            item[x].JobParts[y].typeDiskon = -1;
                                            item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                        } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                            item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                            // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
                                            item[x].JobParts[y].typeDiskon = 0;
                                        } else if ((item[x].JobParts[y].IsCustomDiscount == 1 || item[x].JobParts[y].IsCustomDiscount == 0) && item[x].JobParts[y].Discount == 0) {
                                            item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                            // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                                            item[x].JobParts[y].typeDiskon = 0;
                                        }
                                        // tambahan CR4
                                        else if(item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount != 0){
                                            item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                            item[x].JobParts[y].typeDiskon = item[x].JobParts[y].DiscountTypeId;
                                        }
                                    }
                                    
                                    gridTemp.push({
                                        EstimationBPId : typeof item[x].EstimationBPId == undefined ? 0 : item[x].EstimationBPId,
                                        ActualRate: item[x].ActualRate,
                                        JobTaskId: item[x].JobTaskId,
                                        JobTypeId: item[x].JobTypeId,
                                        BodyEstimationMinute: item[x].BodyEstimationMinute,
                                        FIEstimationMinute: item[x].FIEstimationMinute,
                                        PolishingEstimationMinute: item[x].PolishingEstimationMinute,
                                        PutyEstimationMinute: item[x].PutyEstimationMinute,
                                        ReassemblyEstimationMinute: item[x].ReassemblyEstimationMinute,
                                        SprayingEstimationMinute: item[x].SprayingEstimationMinute,
                                        SurfacerEstimationMinute: item[x].SurfacerEstimationMinute,
                                        // catName: item[x].JobType.Name,
                                        Fare: item[x].Fare == null ? item[x].TaskListBP.ServiceRate : item[x].Fare,
                                        FareMask: item[x].Fare == null ? item[x].TaskListBP.ServiceRate.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',') : item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                        // FareMask:item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                        Summary: summaryy,
                                        // PaidBy: typeof item[x].PaidBy.Name == "object" ? item[x].PaidBy.Name : item[x].PaidBy,
                                        PaidBy: item[x].PaidBy,
                                        PaidById: item[x].PaidById,
                                        JobId: item[x].JobId,
                                        TaskId: item[x].JobTaskId,
                                        TaskName: item[x].TaskName,
                                        FlatRate: item[x].FlatRate,
                                        ProcessId: item[x].ProcessId,
                                        TFirst1: item[x].TFirst1,
                                        UnitBy:item[x].satuanName,
                                        // TaskBPId: x,
                                        // TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : x,
                                        // UnitBy: item[x].Satuan.Name,
                                        UnitBy: $scope.checkSatuan(item[x].ProcessId),
                                        // PaidBy: tmpJob[i].PaidBy.Name,
                                        child: Parts,
                                        index: "$$" + x,
                                        Discount: item[x].Discount,
                                        DiscountTypeId: item[x].DiscountTypeId,
                                        TaskListBPId: item[x].TaskListBP ? item[x].TaskListBP.TaskListBPId : null,
                                        TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : (x+1) * -1,
    
                                    });
                                    // dataJobTaskPlan.push({
                                    //   TaskBPId: item[x].JobTaskId,
                                    //   TaskName: item[x].TaskName,
                                    //   BodyEstimationMinute:item[x].BodyEstimationMinute,
                                    //   FIEstimationMinute:item[x].FIEstimationMinute,
                                    //   PolishingEstimationMinute:item[x].PolishingEstimationMinute,
                                    //   PutyEstimationMinute:item[x].PutyEstimationMinute,
                                    //   ReassemblyEstimationMinute:item[x].ReassemblyEstimationMinute,
                                    //   SprayingEstimationMinute:item[x].SprayingEstimationMinute,
                                    //   SurfacerEstimationMinute:item[x].SurfacerEstimationMinute
                                    // });
                                    // $scope.gridEstimation.data = dataJobTaskPlan;
                                    // $scope.calculateforwork();
                                    $scope.arrayDP(gridTemp, $scope.jejeranDP);
                                    $scope.sumAllPrice();
                                    Parts.splice();
                                    Parts = [];
                                    console.log("if 1 nilai x", x);
                                    $scope.getAvailablePartsServiceManual(item, x + 1, 0);
    
                                } else {
                                    console.log('kondisi 3');
                                    $scope.getAvailablePartsServiceManual(item, x, y + 1);
                                }
                            }
                        });

                    // }
                    
                } else if (item[x].JobParts[y] !== undefined && (item[x].JobParts[y].PartsId == undefined || item[x].JobParts[y].PartsId == null)) {
                    console.log('kondisi 4');
                    // if (item[x].JobParts[y].isDeleted == 0){
                        item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                        item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                        item[x].JobParts[y].DPRequest = item[x].JobParts[y].DPRequest * item[x].JobParts[y].Qty;
                        item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                        item[x].JobParts[y].nightmareIjal = Math.round(item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty);
                        item[x].JobParts[y].temporaryDp = Math.round(item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty);
        

                        tmp = item[x].JobParts[y];
                        if (item[x].JobParts[y].Part !== null && item[x].JobParts[y].Part !== undefined) {
                            item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                            delete item[x].JobParts[y].Part;
                        }
                        if (item[x].JobParts[y].isDeleted == 0){
                            Parts.push(item[x].JobParts[y]);
                            $scope.arrayDP(gridTemp, $scope.jejeranDP);
                            $scope.sumAllPrice();
                        }

                        if (x <= (item.length - 1)) {
                            console.log('kondisi 5');
                            if (y >= (item[x].JobParts.length - 1)) {
                                if (item[x].Fare !== undefined) {
                                    var sum = item[x].Fare;
                                    // var summ = item[x].FlatRate;
                                    // var summ = item[x].FlatRate ? item[x].FlatRate : 1;
                                    var summ = (item[x].FlatRate == null || item[x].FlatRate == undefined) ? 1 : item[x].FlatRate

                                    var summaryy = sum * summ;
                                    var discountedPrice = (summaryy * item[x].Discount) / 100;
                                    var normalPrice = summaryy
                                    summaryy = parseInt(sum);
                                }
                                if (item[x].JobType == null) {
                                    item[x].JobType = { Name: "" };
                                }
                                if (item[x].PaidBy == null) {
                                    item[x].PaidBy = { Name: "" }
                                }
                                if (item[x].Satuan == null) {
                                    item[x].Satuan = { Name: "" }
                                }
                                if (item[x].AdditionalTaskId == null) {
                                    item[x].isOpe = 0;
                                } else {
                                    item[x].isOpe = 1;
                                }
                                // if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                //     item[x].typeDiskon = -1;
                                //     item[x].DiscountedPrice = normalPrice;
                                // } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                //     item[x].typeDiskon = 0;
                                //     item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                // }
                                if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) { 
                                    item[x].JobParts[y].typeDiskon = -1;
                                    item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                    item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                    // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
                                    item[x].JobParts[y].typeDiskon = 0;
                                } else if ((item[x].JobParts[y].IsCustomDiscount == 1 || item[x].JobParts[y].IsCustomDiscount == 0) && item[x].JobParts[y].Discount == 0) {
                                    item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                    // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                                    item[x].JobParts[y].typeDiskon = 0;
                                }
                                // tambahan CR4
                                else if(item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount != 0){ 
                                    item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                    item[x].JobParts[y].typeDiskon = item[x].JobParts[y].DiscountTypeId;
                                }
                                gridTemp.push({
                                    EstimationBPId : typeof item[x].EstimationBPId == undefined ? 0 : item[x].EstimationBPId,
                                    ActualRate: item[x].ActualRate,
                                    JobTaskId: item[x].JobTaskId,
                                    JobTypeId: item[x].JobTypeId,
                                    BodyEstimationMinute: item[x].BodyEstimationMinute,
                                    FIEstimationMinute: item[x].FIEstimationMinute,
                                    PolishingEstimationMinute: item[x].PolishingEstimationMinute,
                                    PutyEstimationMinute: item[x].PutyEstimationMinute,
                                    ReassemblyEstimationMinute: item[x].ReassemblyEstimationMinute,
                                    SprayingEstimationMinute: item[x].SprayingEstimationMinute,
                                    SurfacerEstimationMinute: item[x].SurfacerEstimationMinute,
                                    // catName: item[x].JobType.Name,
                                    // Fare: item[x].Fare,
                                    // FareMask:item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                    Fare: item[x].Fare == null ? item[x].TaskListBP.ServiceRate : item[x].Fare,
                                    FareMask: item[x].Fare == null ? item[x].TaskListBP.ServiceRate.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',') : item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                    Summary: summaryy,
                                    // PaidBy: typeof item[x].PaidBy.Name == "object" ? item[x].PaidBy.Name : item[x].PaidBy,
                                    PaidBy: item[x].PaidBy,
                                    PaidById: item[x].PaidById,
                                    UnitBy:item[x].satuanName,
                                    JobId: item[x].JobId,
                                    TaskId: item[x].JobTaskId,
                                    TaskName: item[x].TaskName,
                                    FlatRate: item[x].FlatRate,
                                    ProcessId: item[x].ProcessId,
                                    TFirst1: item[x].TFirst1,
                                    // TaskBPId: x,
                                    // TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : x,
                                    // UnitBy: item[x].Satuan.Name,
                                    UnitBy: $scope.checkSatuan(item[x].ProcessId),
                                    // PaidBy: tmpJob[i].PaidBy.Name,
                                    child: Parts,
                                    index: "$$" + x,
                                    Discount: item[x].Discount,
                                    DiscountTypeId: item[x].DiscountTypeId,
                                    TaskListBPId: item[x].TaskListBP ? item[x].TaskListBP.TaskListBPId : null,
                                    TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : (x+1) * -1,

                                });
                                // dataJobTaskPlan.push({
                                //       TaskBPId: item[x].JobTaskId,
                                //       TaskName: item[x].TaskName,
                                //       BodyEstimationMinute:item[x].BodyEstimationMinute,
                                //       FIEstimationMinute:item[x].FIEstimationMinute,
                                //       PolishingEstimationMinute:item[x].PolishingEstimationMinute,
                                //       PutyEstimationMinute:item[x].PutyEstimationMinute,
                                //       ReassemblyEstimationMinute:item[x].ReassemblyEstimationMinute,
                                //       SprayingEstimationMinute:item[x].SprayingEstimationMinute,
                                //       SurfacerEstimationMinute:item[x].SurfacerEstimationMinute
                                // });
                                // $scope.gridEstimation.data = dataJobTaskPlan;
                                // $scope.calculateforwork();
                                $scope.arrayDP(gridTemp, $scope.jejeranDP);
                                $scope.sumAllPrice();
                                Parts.splice();
                                Parts = [];
                                $scope.getAvailablePartsServiceManual(item, x + 1, 0);
                                console.log("if 2 nilai x", x);
                            } else {
                                console.log('kondisi 6');
                                $scope.getAvailablePartsServiceManual(item, x, y + 1);
                            }
                        }

                    // }
                    
                } else if (item[x].JobParts.length < 1) {  
                    console.log('item[x] | kondisi 7  ===>',item[x]);
                    if (x <= (item.length - 1)) {
                        console.log('kondisi 7');
                        if (y >= (item[x].JobParts.length - 1)) {
                            if (item[x].Fare !== undefined) {
                                var sum = item[x].Fare;
                                // var summ = item[x].FlatRate;
                                // var summ = item[x].FlatRate ? item[x].FlatRate : 1;
                                var summ = (item[x].FlatRate == null || item[x].FlatRate == undefined) ? 1 : item[x].FlatRate

                                var summaryy = sum * summ;
                                var discountedPrice = (summaryy * item[x].Discount) / 100;
                                var normalPrice = summaryy;
                                summaryy = parseInt(sum);
                            };
                            if (item[x].JobType == null) {
                                item[x].JobType = { Name: "" };
                            };
                            if (item[x].PaidBy == null) {
                                item[x].PaidBy = { Name: "" };
                            };
                            if (item[x].Satuan == null) {
                                item[x].Satuan = { Name: "" };
                            };
                            if (item[x].AdditionalTaskId == null) {
                                item[x].isOpe = 0;
                            } else {
                                item[x].isOpe = 1;
                            };


                          

                            // if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                            //     item[x].typeDiskon = -1;
                            //     item[x].DiscountedPrice = normalPrice;
                            // } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                            //     item[x].typeDiskon = 0;
                            //     item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                            // };

                            // ==================== ini apaan ya kan parts nya aja ga ada, ini kondisi parts.length == 0 =========================
                            // if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                            //     item[x].JobParts[y].typeDiskon = -1;
                            //     item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                            // } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                            //     item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                            //     // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
                            //     item[x].JobParts[y].typeDiskon = 0;
                            // } else if ((item[x].JobParts[y].IsCustomDiscount == 1 || item[x].JobParts[y].IsCustomDiscount == 0) && item[x].JobParts[y].Discount == 0) {
                            //     item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                            //     // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                            //     item[x].JobParts[y].typeDiskon = 0;
                            // }
                            // // tambahan CR4
                            // else if(item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount != 0){
                            //     item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                            //     item[x].JobParts[y].typeDiskon = item[x].JobParts[y].DiscountTypeId;
                            // }
                            // ==================== ini apaan ya kan parts nya aja ga ada, ini kondisi parts.length == 0 =========================



                            gridTemp.push({
                                EstimationBPId : typeof item[x].EstimationBPId == undefined ? 0 : item[x].EstimationBPId,
                                ActualRate: item[x].ActualRate,
                                JobTaskId: item[x].JobTaskId,
                                JobTypeId: item[x].JobTypeId,
                                BodyEstimationMinute: item[x].BodyEstimationMinute,
                                FIEstimationMinute: item[x].FIEstimationMinute,
                                PolishingEstimationMinute: item[x].PolishingEstimationMinute,
                                PutyEstimationMinute: item[x].PutyEstimationMinute,
                                ReassemblyEstimationMinute: item[x].ReassemblyEstimationMinute,
                                SprayingEstimationMinute: item[x].SprayingEstimationMinute,
                                SurfacerEstimationMinute: item[x].SurfacerEstimationMinute,
                                // catName: item[x].JobType.Name,
                                // Fare: item[x].Fare,
                                // FareMask:item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                Fare: item[x].Fare == null ? item[x].TaskListBP.ServiceRate : item[x].Fare,
                                FareMask: item[x].Fare == null ? item[x].TaskListBP.ServiceRate.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',') : item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                Summary: summaryy,
                                // PaidBy: typeof item[x].PaidBy.Name == "object" ? item[x].PaidBy.Name : item[x].PaidBy,
                                PaidBy: item[x].PaidBy,
                                PaidById: item[x].PaidById,
                                UnitBy:item[x].satuanName,

                                JobId: item[x].JobId,
                                TaskId: item[x].JobTaskId,
                                TaskName: item[x].TaskName,
                                FlatRate: item[x].FlatRate,
                                ProcessId: item[x].ProcessId,
                                TFirst1: item[x].TFirst1,
                                // TaskBPId: x,
                                // TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : x,
                                // UnitBy: item[x].Satuan.Name
                                UnitBy: $scope.checkSatuan(item[x].ProcessId),
                                // PaidBy: tmpJob[i].PaidBy.Name,
                                child: Parts,
                                index: "$$" + x,
                                Discount: item[x].Discount,
                                DiscountTypeId: item[x].DiscountTypeId,
                                TaskListBPId: item[x].TaskListBP ? item[x].TaskListBP.TaskListBPId : null,
                                TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : (x+1) * -1,

                            });
                            // dataJobTaskPlan.push({
                            //       TaskBPId: item[x].JobTaskId,
                            //       TaskName: item[x].TaskName,
                            //       BodyEstimationMinute:item[x].BodyEstimationMinute,
                            //       FIEstimationMinute:item[x].FIEstimationMinute,
                            //       PolishingEstimationMinute:item[x].PolishingEstimationMinute,
                            //       PutyEstimationMinute:item[x].PutyEstimationMinute,
                            //       ReassemblyEstimationMinute:item[x].ReassemblyEstimationMinute,
                            //       SprayingEstimationMinute:item[x].SprayingEstimationMinute,
                            //       SurfacerEstimationMinute:item[x].SurfacerEstimationMinute
                            // });
                            // $scope.gridEstimation.data = dataJobTaskPlan;
                            // $scope.calculateforwork();
                            $scope.arrayDP(gridTemp, $scope.jejeranDP);
                            $scope.sumAllPrice();
                            Parts.splice();
                            Parts = [];
                            $scope.getAvailablePartsServiceManual(item, x + 1, 0);
                        } else {
                            console.log('kondisi 8');
                            $scope.getAvailablePartsServiceManual(item, x, y + 1);
                        }
                    }
                }

                if (x > item.length - 1) {
                    return item;
                }
            } else {
                console.log('kondisi 9');
                $scope.getAvailablePartsServiceManual(item, x + 1, 0);
            }




        };
        // =================================
        // $scope.calculateforwork = function () {
        //     $scope.getCalculatedTimeWork = [];
        //     $scope.arrWorkTime = [];
        //     var newDate = new Date($scope.mData.AppointmentDate);
        //     console.log("newDate 1", newDate);
        //     // newDate.getHours(newDate.setHours() + 7);
        //     newDate.setSeconds(0);

        //     var total = 0;
        //     for (var i = 0; i < $scope.gridEstimationApi.grid.columns.length; i++) {
        //         var a = $scope.gridEstimationApi.grid.columns[i].getAggregationValue();
        //         // $scope.arrWorkTime.push(a + ' minutes');
        //         if (a != null || a !== undefined) {
        //             $scope.arrWorkTime.push(a + ' minutes');
        //             $scope.getCalculatedTimeWork.push(a);
        //             total = total + a;
        //         };
        //     };

        //     function roundToTwo(num) {
        //         return +(Math.round(num + "e+2") + "e-2");
        //     };

        //     var days = Math.floor(Math.abs(total / (8 * 60)));
        //     var hours = Math.abs((total % (8 * 60)) / 60);
        //     newDate.setDate(newDate.getDate() + days);
        //     newDate.setHours(newDate.getHours() + roundToTwo(hours));
        //     // console.log("hours", hours);
        //     $scope.estHoursWork = roundToTwo(hours);
        //     $scope.estDaysWork = days;
        //     console.log("newDate 2", newDate);
        //     $scope.mData.PlanDateFinish = newDate;
        //     $scope.mData.AdjusmentDate = newDate;
        //     console.log("$scope.getCalculatedTimeWork",$scope.getCalculatedTimeWork);
        //     console.log("$scope.estHoursWork",$scope.estHoursWork);
        //     console.log("$scope.estDaysWork",$scope.estDaysWork);
        //     console.log("newDate",newDate);
        // };
        $scope.calculateforwork = function() {
            $scope.getCalculatedTimeWork = [];
            $scope.arrWorkTime = [];
            var newDate = new Date($scope.mData.AppointmentDate);
            console.log("newDate 1", newDate);
            // newDate.getHours(newDate.setHours() + 7);
            newDate.setSeconds(0);

            var total = 0;
            for (var i = 0; i < $scope.gridEstimationApi.grid.columns.length; i++) {
                var a = $scope.gridEstimationApi.grid.columns[i].getAggregationValue();
                // $scope.arrWorkTime.push(a + ' minutes');
                if (a != null || a !== undefined) {
                    $scope.arrWorkTime.push(a + ' minutes');
                    $scope.getCalculatedTimeWork.push(a);
                    total = total + a;
                };
            };

            function roundToTwo(num) {
                return +(Math.round(num + "e+2") + "e-2");
            };

            var days = Math.floor(Math.abs(total / (8 * 60)));
            var hours = Math.abs((total % (8 * 60)) / 60);
            newDate.setDate(newDate.getDate() + days);
            newDate.setHours(newDate.getHours() + roundToTwo(hours));
            // console.log("hours", hours);
            $scope.estHoursWork = roundToTwo(hours);
            $scope.estDaysWork = days;
            console.log("newDate 2", newDate);

            var hh = newDate.getHours().toString();
            var mm = newDate.getMinutes().toString();
            var ss = "00";
            var finalDate = (hh[1] ? hh : "0" + hh[0]) + ':' + (mm[1] ? mm : "0" + mm[0]) + ":" + ss;
            var finalHour = (hh[1] ? hh : "0" + hh[0]);
            var finalMinute = (mm[1] ? mm : "0" + mm[0]);
            if (!isNaN(finalHour) && !isNaN(finalMinute)) {
                $scope.mData.PlanFinish = finalDate;
                $scope.mData.PlanDateFinish = newDate;
                $scope.mData.AdjusmentDate = newDate;
            }
            console.log("finalDate with Hours", finalDate);
            console.log("$scope.getCalculatedTimeWork", $scope.getCalculatedTimeWork);
            console.log("$scope.estHoursWork", $scope.estHoursWork);
            console.log("$scope.estDaysWork", $scope.estDaysWork);
            console.log("newDate", newDate);
        };
        //----------------------------------
        // Type Ahead
        //----------------------------------
        // $scope.getWork = function(key) {
        //     var Katashiki = $scope.mData.KatashikiCode;
        //     var catg = $scope.tmpCatg.MasterId;
        //     console.log("$scope.mData.KatashikiCode", Katashiki);
        //     console.log("$scope.lmModel.JobType", catg);
        //     if (Katashiki != null && catg != null) {
        //         var ress = FollowUpBpService.getDataTask(key, Katashiki, catg).then(function(xres) {
        //             return xres.data.Result;
        //         });
        //         return ress
        //     }
        // };
        // $scope.getWork = function(key) {
        //     var VehicleType = $scope.mDataDetail.VehicleTypeId;
        //     // console.log("$scope.mDataDetail.KatashikiCode", Katashiki);
        //     if (VehicleType != null) {
        //         var ress = AppointmentBpService.getDataTask(key, VehicleType).then(function(resTask) {
        //             return resTask.data.Result;
        //         });
        //         console.log("ress", ress);
        //         return ress
        //     }
        // };
        $scope.getWork = function(key) {
            var VehicleType = $scope.mDataDetail.VehicleTypeId;
            var vehModelId = $scope.mDataDetail.VehicleModelId;
            // console.log("$scope.mDataDetail.KatashikiCode", Katashiki);
            if (VehicleType != null && vehModelId != null) {
                var ress = AppointmentBpService.getDataTask(key, VehicleType, vehModelId , $scope.JobIsWarranty).then(function(resTask) {
                    return resTask.data.Result;
                });
                console.log("cek ress", ress);
                return ress
            }
        };

        $scope.givePatternHarga = function(item, event) {
            console.log("item", item);
            var temp = {};
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
                var tmpItem = angular.copy(item);
                temp.Fare = parseInt(tmpItem.replace(/,/g,''));
                item = item.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                temp.FareMask = item;
                $scope.listApi.addDetail(false, temp);
                return;
            }
        };
        $scope.harga = function() {
            console.log('aso', $scope.gridWork);
        }

        // $scope.getWork = function(key) {
        //     var VehicleType = $scope.mDataDetail.VehicleTypeId;
        //     // console.log("$scope.mDataDetail.KatashikiCode", Katashiki);
        //     if (VehicleType != null) {
        //         var ress = AppointmentBpService.getDataTask(key, VehicleType).then(function(resTask) {
        //             return resTask.data.Result;
        //         });
        //         console.log("ress", ress);
        //         return ress
        //     }
        // };
        $scope.getParts = function(key) {
            var Katashiki = $scope.mData.KatashikiCode;
            var kategori = $scope.ldModel.MaterialTypeId;
            var isGr = 0;

            console.log('kata', Katashiki);
            if (kategori !== undefined) {
                var ress = AppointmentBpService.getDataParts(key, isGr, kategori).then(function(resparts) {
                    console.log("respart followup BP", resparts);
                    var tempResult = [];
                    if (resparts.data.Result.length > 0) {
                        // tempResult.push(resparts.data.Result[0]);
                        for (var i=0; i<resparts.data.Result.length; i++){
                            if(resparts.data.Result[i].PartsClassId1 == kategori){
                                tempResult.push(resparts.data.Result[i]);
                            }
                        }
                    }
                    console.log('isGr COYYY 0', tempResult);
                    return tempResult;
                });
                console.log("ress", ress);
                return ress
            }


            // if (Katashiki != null) {
            //
            //     var res = FollowUpBpService.getDataParts(key).then(function(resparts) {
            //       return resparts.data.Result;
            //       console.log("resparts", resparts);
            //     });
            //
            //     return res;
            //
            //     // var ress = AppointmentGrService.getDataParts(key).then(function(resparts) {
            //     //   console.log("nich",resparts);
            //     //   return resparts.data.Result;
            //     // });
            //     // return ress
            // }
        };
        $scope.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };
        $scope.noResults = true;
        $scope.sumAllPrice = function() {
            console.log('sumAllPrice gridWork ===>',$scope.gridWork);

            
            
            var NumberParent = 1;
            var NumberChild = 1;
            for (i in $scope.gridWork){
                $scope.gridWork[i].NumberParent = NumberParent+parseInt(i);
                for (j in $scope.gridWork[i].child){
                    $scope.gridWork[i].child[j].NumberChild = $scope.gridWork[i].NumberParent + "."  + parseInt(++j);
                }
            }

            var totalW = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                    totalW += 0;
                }else{
                    // totalW += Math.round($scope.gridWork[i].Fare / 1.1);
                    totalW += ASPricingEngine.calculate({
                                    InputPrice: $scope.gridWork[i].Fare,
                                    // Discount: 5,
                                    // Qty: 2,
                                    Tipe: 2, 
                                    PPNPercentage: PPNPerc,
                                });
                }
            }
            $scope.totalWork = totalW;
            // ====================


            var totalDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].DownPayment !== null) {
                            // if($scope.gridWork[i].child[j].DownPayment > 100.00){
                            //     totalDp += $scope.gridWork[i].child[j].DownPayment;
                            //     console.log('totalDP dari Inputan',$scope.gridWork[i].child[j].DownPayment);
                            // }else{
                            //     if($scope.gridWork[i].child[j].DPestimasi !== undefined){//kalo dari estimasi sudah * 1.1
                            //         totalDp += $scope.gridWork[i].child[j].DownPayment;
                            //         console.log("TotalDP dari Estimasi", $scope.gridWork[i].child[j].DownPayment);
                            //     }else{
                            //         //IRV = ini request Via, kalo DP itu + PPN
                            //         //SNBU = Sudah nanya BU
                            totalDp += $scope.gridWork[i].child[j].DownPayment;
                            // totalDp += $scope.gridWork[i].child[j].DownPayment * 1.1;
                            console.log("TotalDP bukan dari Estimasi", $scope.gridWork[i].child[j].DownPayment);
                                // }
                            // }

                            
                        }
                    }
                }
            }
            $scope.totalDp = totalDp;

            // ====================
            var totalDefaultDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].minimalDp !== undefined) {
                            totalDefaultDp += $scope.gridWork[i].child[j].minimalDp;
                            // console.log("$scope.gridWork[i].child.totalDefaultDp totalDefaultDp ===>", $scope.gridWork[i].child[j].minimalDp);
                        }
                    }
                }
            }
            $scope.totalDpDefault = totalDefaultDp;
            // ====================
            var totalActualRate = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].ActualRate !== undefined) {
                    totalActualRate += $scope.gridWork[i].ActualRate;
                }

            }
            $scope.tmpActRate = totalActualRate;     
            // =================== CR4 Re-Calculate following CSV, req by Kevin =======================
            var totalMaterial_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30){
                            totalMaterial_byKevin += 0;
                        }else{
                            // totalMaterial_byKevin += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                            totalMaterial_byKevin += ASPricingEngine.calculate({
                                                        InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                        // Discount: 5,
                                                        Qty: $scope.gridWork[i].child[j].Qty,
                                                        Tipe: 102, 
                                                        PPNPercentage: PPNPerc,
                                                    }); 
                        }
                    }
                }
            };
            
            var totalMD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30 ){ //req by pak eko bahan tetap dapet diskon 08-juli-2020
                                totalMD_byKevin += 0;
                        }else{
                            // totalMD_byKevin +=  Math.round(Math.round($scope.gridWork[i].child[j].RetailPrice /1.1 ) * ($scope.gridWork[i].child[j].Discount/100)) *  $scope.gridWork[i].child[j].Qty;
                            totalMD_byKevin +=  ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                    Discount: $scope.gridWork[i].child[j].Discount,
                                                    Qty: $scope.gridWork[i].child[j].Qty,
                                                    Tipe: 112, 
                                                    PPNPercentage: PPNPerc,
                                                }); 
                            
                        }
                    }
                }
            };

            var totalWD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                    totalWD_byKevin += 0;
                }else{
                    // totalWD_byKevin +=  Math.round(Math.round($scope.gridWork[i].Fare /1.1 ) * ($scope.gridWork[i].Discount/100));
                    totalWD_byKevin +=  ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWork[i].Fare,
                                            Discount: $scope.gridWork[i].Discount,
                                            // Qty: 2,
                                            Tipe: 12, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                }
            };


            

            
            // =======================================================

            $scope.totalMaterial           = totalMaterial_byKevin;
            $scope.totalMaterialDiscounted = totalMD_byKevin;
            $scope.totalWorkDiscounted     = totalWD_byKevin;
            $scope.subTotalMaterialSummary = $scope.totalMaterial - $scope.totalMaterialDiscounted;
            $scope.subTotalWorkSummary     = $scope.totalWork - $scope.totalWorkDiscounted;
            // $scope.totalPPN                = Math.floor(($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * 0.1);  
            $scope.totalPPN                = ASPricingEngine.calculate({
                                                InputPrice: (($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * (1+PPNPerc/100)),
                                                // Discount: 0,
                                                // Qty: 0,
                                                Tipe: 33, 
                                                PPNPercentage: PPNPerc,
                                            });        
            $scope.totalEstimasi           = $scope.totalPPN + $scope.subTotalMaterialSummary + $scope.subTotalWorkSummary;

            // =================== CR4 Re-Calculate following CSV, req by Kevin =======================


            console.log("$scope.gridWork", $scope.gridWork);
            console.log("gridWork", $scope.gridWork);

            //nentuin diskon task & parts
            jejeranDiskonTask = [];
            jejeranDiskonParts = [];
            for(var i in $scope.gridWork){
                // if($scope.gridWork.PaidById == 30|| $scope.gridWork.PaidById == 31 || $scope.gridWork.PaidById == 32 || $scope.gridWork.PaidById == 2277){
                if($scope.gridWork.PaidById == 30|| $scope.gridWork.PaidById == 2277){
                    jejeranDiskonTask.push(0);
                }else{
                    jejeranDiskonTask.push($scope.gridWork[i].Discount);
                }
                if($scope.gridWork[i].child.length > 0 ){
                    for(var j in $scope.gridWork[i].child){
                        // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 31 || $scope.gridWork[i].child[j].PaidById == 32 || $scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 2277){
                            jejeranDiskonParts.push(0);
                        }else{
                            jejeranDiskonParts.push($scope.gridWork[i].child[j].Discount);
                        }

                        
                    }
                }
            }
            //nentuin diskon task & parts
            $scope.displayDiscountAlgorithm();



        };
        var selectedTaskBP = null;
        $scope.onSelectWork = function($item, $model, $label) {
            console.log("onSelectWork=>", $item);
            console.log("onSelectWork=>", $model);
            console.log("onSelectWork=>", $label);
            if ($item.ServiceRate != null) {
                $item.Fare = $item.ServiceRate;
                $scope.PriceAvailable = true;
                // $item.Fare = $item.Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                $item.FareMask = $item.Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                var satuan = null;
                for (var i = 0; i < $scope.unitData.length; i++) {
                    if ($scope.unitData[i].Name == $item.UOM) {
                        satuan = $scope.unitData[i].MasterId;
                    }
                }
                $item.Unit = satuan;
                $item.ProcessId = satuan;
                $item.UnitBy = $item.UOM;

            } else {
                if ($item.WarrantyRate != null && $item.ServiceRate == null) {
                    $item.Fare = $item.WarrantyRate;

                    $item.Fare_w = $item.WarrantyRate * $item.FlatRate;

                    $item.FareMask = $item.Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                    var satuan = null;
                    for (var i = 0; i < $scope.unitData.length; i++) {
                        if ($scope.unitData[i].Name == $item.UOM) {
                            satuan = $scope.unitData[i].MasterId;
                        }
                    }
                    $item.Unit = satuan;
                    $item.ProcessId = satuan;
                    $item.UnitBy = $item.UOM;
                    console.log("abc", $item.WarrantyRate, $item.Fare)
                } else {
                    $item.Fare = 0;
                    $scope.PriceAvailable = false;
                    console.log("abc", $scope.PriceAvailable)
                }

                if ($item.FlatRate == 0) {
                    $scope.FlatRateAvailable_new = false;
                } else {
                    $scope.FlatRateAvailable_new = true;
                }

            }
            // added by sss on 2017-09-14
            selectedTaskBP = $item;
            $scope.listApi.addDetail(false, $item);
            $scope.sendWork($label, $item);
            console.log("modelnya", $scope.lmModel);
        }

        $scope.checkServiceRate = function (data_fr, data, from_check) {
            console.log($scope.lmModel)
            data.Fare_w = data_fr * data.WarrantyRate

            $scope.listApi.addDetail(false, data);
        }

        $scope.onSelectParts = function($item, $model, $label) {

            console.log("onSelectParts=>", $item);
            console.log("onSelectParts=>", $model);
            console.log("onSelectParts=>", $label);
            console.log("anita parts", $scope.gridPartsDetail);
            // ==== ARI SAID ======
            // for (var i=0; i<$scope.gridPartsDetail.data.length; i++){
            //     if ($label == $scope.gridPartsDetail.data[i].PartsCode){
            //         console.log('parts sudah ada');  
            //         $scope.ldModel.PartsCode = null;
            //         $scope.ldModel.PartsName = null;
            //         $item.PartsName = null;
            //         $item.PartsCode = null;
            //         bsNotify.show({
            //             size: 'big',
            //             title: 'Parts Sudah Ada!',
            //             text: 'I will close in 2 seconds.',
            //             timeout: 2000,
            //             type: 'danger'
            //         });

            //     }
            // }

            if ($item.PartsName != null) {
                $scope.partsAvailable = true;
            } else {
                $scope.partsAvailable = false;
            }
            $scope.sendParts($label, $item);
            console.log("modelnya", $model);
        }

        $scope.onNoResult = function() {
            console.log("gak", $scope.lmModel);
            var jmlPart = 0;
            if ($scope.cekJob != null && $scope.cekJob != undefined){
                AppointmentBpService.getDataPartsByTaskId($scope.cekJob).then(function(res) {
                    console.log("resabis ambil parts", res.data.Result);
                    jmlPart = res.data.Result.length;
                });
            }
            if (jmlPart > 0){
                $scope.listApi.clearDetail();
            }
            var row = $scope.lmModel;
            row.FlatRate = 1;
            row.Fare = "";
            row.Summary = "";
            row.TaskId = null;
            row.tmpTaskId = null;
            // added by sss on 2017-09-19
            selectedTaskBP = null;
            //
            row.PaidById = 29;
            row.PaidBy = "Insurance"
            // $scope.lmModel.PaidById = row.PaidById
            $scope.lmModel.PaidBy = row.PaidBy
            row.TFirst1 = null;
            
            $scope.listApi.addDetail(false, row);
            console.log("uunnnnchhh", $scope.lmModel);
            $scope.partsAvailable = false;
            $scope.jobAvailable = false;
            $scope.PriceAvailable = false;
            $scope.FlatRateAvailable = false;
            $scope.FlatRateAvailable_new = true;
        }
        $scope.onGotResult = function() {
            console.log("onGotResult=>");
        }
        $scope.selected = {};
        $scope.selectTypeWork = function(data) {
            $scope.tmpCatg = data;
        }
        $scope.onListSave = function(item) {
            console.log("save_modal=>", item);
        }
        $scope.onListCancel = function(item) {
            console.log("cancel_modal=>", item);
        }

        $scope.onBeforeSave = function(data, mode) {
            console.log("onBeforeSave data", data);
            console.log("onBeforeSave mode", mode);
            // data.Fare = data.Fare.replace(/,/g,'');
            // data.Fare = parseInt(data.Fare);
            // if(data.child.length > 0){
            //     for(var j in data.child){
            //         data.child[j].DPRequest = data.child[j].DPRequest?data.child[j].DPRequest.replace(/,/g,''):0;
            //         // data.child[j].DPRequest = data.child[j].DPRequest.replace(/./g,'');
            //         data.child[j].DPRequest = parseFloat(data.child[j].DPRequest);

            //         data.child[j].DownPayment = data.child[j].DownPayment?data.child[j].DownPayment.replace(/,/g,''):0;
            //         // data.child[j].RetailPrice = data.child[j].RetailPrice.replace(/./g,'');
            //         data.child[j].DownPayment = parseInt(data.child[j].DownPayment);

            //         data.child[j].RetailPrice = data.child[j].RetailPrice?data.child[j].RetailPrice.replace(/,/g,''):0;
            //         // data.child[j].RetailPrice = data.child[j].RetailPrice.replace(/./g,'');
            //         data.child[j].RetailPrice = parseInt(data.child[j].RetailPrice);

            //         data.child[j].subTotal = data.child[j].subTotal?data.child[j].subTotal.replace(/,/g,''):0;
            //         // data.child[j].subTotal = data.child[j].subTotal.replace(/./g,'');
            //         data.child[j].subTotal = parseInt(data.child[j].subTotal);
            //     }
            // }
            var tmpData = angular.copy(data);
            var tmpGridWork = $scope.gridWork;
            var lengthGrid = tmpGridWork.length - 1;
            // if ($scope.tmpCus !== undefined) {
            //     tmpData.PaidBy = $scope.tmpCus.Name;
            // }

            if (tmpData.PaidById != 30) {
                tmpData.Discount = $scope.DiscountTask;
            } else {
                tmpData.Discount = 0
            }

            


            var selectedPayment = _.find($scope.paymentData,{ 'MasterId': tmpData.PaidById});
            tmpData.PaidBy = selectedPayment.Name;
            console.log('selectedPayment ===>',selectedPayment);
            
            // tmpData.PaidBy = $scope.tmpCus.Name;
            if (mode == 'new') {
                if (tmpData.TaskListBPId !== undefined && tmpData.TaskListBPId !== null) {
                    tmpData.TaskBPId = tmpData.TaskListBPId;
                } else {
                    if (tmpData.TaskBPId == null) {
                        console.log('masuk new');
                        // if (tmpGridWork.length > 0) {
                        //     tmpData.TaskBPId = tmpGridWork[lengthGrid].TaskBPId + 1;
                        // } else {
                        //     tmpData.TaskBPId = 1;
                        // }

                        if (tmpGridWork.length > 0) {
                            // tmpData.TaskBPId = (Math.abs(tmpGridWork[lengthGrid].TaskBPId) + 1) * -1; // kl non tasklist penanda nya dia minus
                            
                            var idNontasklist = [];
                            for (var i=0; i<tmpGridWork.length; i++){
                                if (tmpGridWork[i].TaskBPId < 0){
                                    idNontasklist.push(tmpGridWork[i].TaskBPId)
                                }
                            }

                            if (idNontasklist.length === 0){
                                // berarti blm ada non tasklist sebelumnya (baru nambahin 1 ini)
                                tmpData.TaskBPId = 1 * -1;
                            } else {
                                // sebelom na uda ada yg non tasklist
                                var SortedData = angular.copy(idNontasklist);
                                SortedData = SortedData.sort(function(a, b){
                                    return a-b //sort by date ascending
                                })
                                // di sort dl biar yg angka minus na gede di array pertama
                                tmpData.TaskBPId = SortedData[0] - 1; // kl non tasklist penanda nya dia minus

                            }

                            //Cek tambah pekerjaan atau tidak


                        } else {
                            tmpData.TaskBPId = 1 * -1;
                        }
                    }
                }
                
                if (data.child.length > 0) {
                    var tmpDataChild = angular.copy(data.child);
                    for (var i = 0; i < tmpDataChild.length; i++) {
                        tmpDataChild[i].DPRequest = Math.round(tmpDataChild[i].DownPayment);
                        delete data.child[i];
                    }
                    console.log('masuk new data.child.length > 0', tmpDataChild, tmpData);
                    // if (tmpData.Fare.toString().includes(',')) {
                    //     var fareInt = tmpData.Fare.toString().replace(/,/g, '');
                    //     tmpData.Summary = parseInt(fareInt);
                    // }
                    tmpData.Summary = tmpData.Fare;
                    $scope.listApi.addDetail(tmpDataChild, tmpData);
                } else {
                    $scope.listApi.addDetail(false, tmpData);
                };


            } else if (mode == 'newedited') {
                if (tmpData.TaskBPId == null) {
                    console.log('masuk newedited');
                    if (tmpGridWork.length > 0) {
                        tmpData.TaskBPId = tmpGridWork[lengthGrid].TaskBPId + 1;
                    } else {
                        tmpData.TaskBPId = 1;
                    }
                }
                if (data.child.length > 0) {
                    var tmpDataChild = angular.copy(data.child);
                    for (var i = 0; i < tmpDataChild.length; i++) {
                        tmpDataChild[i].DPRequest = Math.round(tmpDataChild[i].DownPayment);
                        delete data.child[i];
                    }
                    console.log('masuk data.child.length > 0', tmpDataChild, tmpData);
                    // if (tmpData.Fare.toString().includes(',')) {
                    //     var fareInt = tmpData.Fare.toString().replace(/,/g, '');
                    //     tmpData.Summary = parseInt(fareInt);
                    // }
                    tmpData.Summary = tmpData.Fare;
                    $scope.listApi.addDetail(tmpDataChild, tmpData);
                } else {
                    $scope.listApi.addDetail(false, tmpData);
                };
            }
        }

        $scope.onBeforeEditList = function(rows) {
            console.log("form onBeforeEditList=>", rows);

            $scope.copyAwalPembayaranJob = angular.copy(rows.PaidById)

            if (rows.PaidById == 30){
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }]; // kl pekerjaan twc / pwc ga blh pilih bahan

                if (rows.FlatRate == 0) {
                    $scope.FlatRateAvailable_new = false;
                } else {
                    $scope.FlatRateAvailable_new = true;
                }

                if (rows.Fare_w == null || rows.Fare_w == undefined) {
                    rows.Fare_w = rows.FlatRate * rows.Fare
                }
            } else {
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
            }

            $scope.cekJob = rows.TaskBPId;
            if (typeof rows.Fare !== 'string') {
                $scope.checkPrice(rows.Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','));
                rows.Fare = rows.Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');

                if (rows.child.length > 0) {
                    var tmpChild = angular.copy(rows.child);
                    for (var j in tmpChild) {
                        tmpChild[j].RetailPrice = tmpChild[j].RetailPrice.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        tmpChild[j].DPRequest = tmpChild[j].DPRequest.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        if (tmpChild[j].DownPayment != 0) {
                            tmpChild[j].DownPayment = tmpChild[j].DownPayment.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        }
                        tmpChild[j].subTotal = tmpChild[j].subTotal.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                    }
                    $scope.listApi.clearDetail();
                }
            }
            $scope.listApi.addDetail(tmpChild, rows);
        }
        $scope.onBeforeNew = function() {

            $scope.copyAwalPembayaranJob = null

            $scope.FlatRateAvailable_new = true

            $scope.cekJob = null;
            $scope.partsAvailable = false;
            $scope.jobAvailable = false;
            $scope.PriceAvailable = false;
            $scope.FlatRateAvailable = false;
            $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];

            if ($scope.mData.WoCategoryId == 2653) {
                // default pembayaran ke warranty
                var row = {}
                row.PaidById = 30
                row.PaidBy = "Warranty"
                $scope.lmModel.PaidBy = "Warranty"
                $scope.JobIsWarranty = 1
                $scope.listApi.addDetail(false, row);
            } else {
                var row = {}
                $scope.JobIsWarranty = 0
                $scope.listApi.addDetail(false, row);
            }
            $scope.DataTempSelectedJob = [];
        }

        $scope.selectTypeUnitParts = function(data) {
            console.log("selectTypeUnitParts", data);
            $scope.tmpUnit = data;
            $scope.ldModel.satuanName = data.Name;
        };
        $scope.onAfterSave = function(item) {
            console.log('grid temp after save', gridTemp);
            console.log('$scope.gridWork after save', $scope.gridWork);
            if($scope.jejeranDP.length>0){
                $scope.arrayDP(gridTemp, $scope.jejeranDP);
            }

            for(var i in $scope.gridWork){
                var selectedPayment = _.find($scope.paymentData,{ 'MasterId': $scope.gridWork[i].PaidById});
                $scope.gridWork[i].PaidBy = selectedPayment.Name;
            }

            var NumberParent = 1;
            for (i in $scope.gridWork){
                $scope.gridWork[i].NumberParent = NumberParent+parseInt(i);
                for (j in $scope.gridWork[i].child){
                    $scope.gridWork[i].child[j].NumberChild = $scope.gridWork[i].NumberParent + "."  + parseInt(++j);
                }
            }

            var tmpDeleteData = []; //untuk kebutuhan tyre
            var tmpData = angular.copy(item);
            if ($scope.DataTempSelectedJob != undefined) {
                if ($scope.DataTempSelectedJob.child != undefined && $scope.DataTempSelectedJob.child.length > 0) {
                    for (var i = 0; i < $scope.DataTempSelectedJob.child.length; i++) {
                        var idx = 0;
                        idx = _.findIndex(tmpData.child, { JobPartsId: $scope.DataTempSelectedJob.child[i].JobPartsId });
                        if(idx == -1){
                            tmpDeleteData.push($scope.DataTempSelectedJob.child[i]); //untuk kebutuhan tyre
                        }
                    }
                }
            }

            //Cek status part Tyre
            if(tmpDeleteData.length > 0){
                $scope.checkStatusPartTyre($scope.mData.JobId, tmpDeleteData, tmpData.JobTaskId);
            }

            var totalW = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                    totalW += 0;
                }else{
                    // totalW += Math.round($scope.gridWork[i].Fare / 1.1);
                    totalW += ASPricingEngine.calculate({
                                    InputPrice: $scope.gridWork[i].Fare,
                                    // Discount: 5,
                                    // Qty: 2,
                                    Tipe: 2, 
                                    PPNPercentage: PPNPerc,
                                });
                }
            }
            $scope.totalWork = totalW;
            // ============
           
            // ==============
            var totalDp = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].DownPayment !== null) {
                            // if(gridTemp[i].child[j].DownPayment > 100.00){
                            //     totalDp += gridTemp[i].child[j].DownPayment;
                            //     console.log('totalDP dari Inputan',gridTemp[i].child[j].DownPayment);
                            // }else{
                            //     if(gridTemp[i].child[j].DPestimasi !== undefined){//kalo dari estimasi sudah * 1.1
                            //         totalDp += gridTemp[i].child[j].DownPayment;
                            //         console.log("TotalDP dari Estimasi", gridTemp[i].child[j].DownPayment);
                            //     }else{
                            //         //IRV = ini request Via, kalo DP itu + PPN
                            //         //SNBU = Sudah nanya BU
                                    // totalDp += gridTemp[i].child[j].DownPayment * 1.1;
                                    // totalDp += gridTemp[i].child[j].DownPayment * (1+(PPNPerc/100));
                                    totalDp += gridTemp[i].child[j].DownPayment;


                                    // console.log("TotalDP bukan dari Estimasi", gridTemp[i].child[j].DownPayment * 1.1);
                            //     }
                            // }
                        }
                    }
                }
            }
            $scope.totalDp = totalDp;
            // =============
            var totalDefaultDp = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].minimalDp !== undefined) {
                            totalDefaultDp += gridTemp[i].child[j].minimalDp;
                            console.log("gridTemp[i].child.minimalDp", gridTemp[i].child[j].minimalDp);
                        }
                    }
                }
            }
            $scope.totalDpDefault = totalDefaultDp;
            // =============
          

            // =================== CR4 Re-Calculate following CSV, req by Kevin =======================
            var totalMaterial_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30){
                            totalMaterial_byKevin += 0;
                        }else{
                                // totalMaterial_byKevin += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                                totalMaterial_byKevin += ASPricingEngine.calculate({
                                                            InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                            // Discount: 5,
                                                            Qty: $scope.gridWork[i].child[j].Qty,
                                                            Tipe: 102, 
                                                            PPNPercentage: PPNPerc,
                                                        }); 
                        }
                    }
                }
            };
            
            var totalMD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30 ){//req by pak eko bahan tetap dapet diskon 08-juli-2020
                            totalMD_byKevin += 0;
                        }else{
                            // totalMD_byKevin +=  Math.round(Math.round($scope.gridWork[i].child[j].RetailPrice /1.1 ) * ($scope.gridWork[i].child[j].Discount/100)) *  $scope.gridWork[i].child[j].Qty;
                            totalMD_byKevin +=  ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                    Discount: $scope.gridWork[i].child[j].Discount,
                                                    Qty: $scope.gridWork[i].child[j].Qty,
                                                    Tipe: 112, 
                                                    PPNPercentage: PPNPerc,
                                                }); 
                        }
                    }
                }
            };

            var totalWD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                    totalWD_byKevin += 0;
                }else{
                    // totalWD_byKevin +=  Math.round(Math.round($scope.gridWork[i].Fare /1.1 ) * ($scope.gridWork[i].Discount/100));
                    totalWD_byKevin +=  ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWork[i].Fare,
                                            Discount: $scope.gridWork[i].Discount,
                                            // Qty: 2,
                                            Tipe: 12, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                }
            };


            // =======================================================
            
            // =======================================================

            $scope.totalMaterial           = totalMaterial_byKevin;
            $scope.totalMaterialDiscounted = totalMD_byKevin;
            $scope.totalWorkDiscounted     = totalWD_byKevin;
            $scope.subTotalMaterialSummary = $scope.totalMaterial - $scope.totalMaterialDiscounted;
            $scope.subTotalWorkSummary     = $scope.totalWork - $scope.totalWorkDiscounted;
            // $scope.totalPPN                = Math.floor(($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * 0.1);  
            $scope.totalPPN                = ASPricingEngine.calculate({
                                                InputPrice: (($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * (1+PPNPerc/100)),
                                                // Discount: 0,
                                                // Qty: 0,
                                                Tipe: 33, 
                                                PPNPercentage: PPNPerc,
                                            });    
            $scope.totalEstimasi           = $scope.totalPPN + $scope.subTotalMaterialSummary + $scope.subTotalWorkSummary;

            // =================== CR4 Re-Calculate following CSV, req by Kevin =======================
            //nentuin diskon task & parts
            jejeranDiskonTask = [];
            jejeranDiskonParts = [];
            for(var i in $scope.gridWork){
                // if($scope.gridWork.PaidById == 30|| $scope.gridWork.PaidById == 31 || $scope.gridWork.PaidById == 32 || $scope.gridWork.PaidById == 2277){
                if($scope.gridWork.PaidById == 30|| $scope.gridWork.PaidById == 2277){
                    jejeranDiskonTask.push(0);
                }else{
                    jejeranDiskonTask.push($scope.gridWork[i].Discount);
                }
                if($scope.gridWork[i].child.length > 0 ){
                    for(var j in $scope.gridWork[i].child){
                        // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 31 || $scope.gridWork[i].child[j].PaidById == 32 || $scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 2277){
                            jejeranDiskonParts.push(0);
                        }else{
                            jejeranDiskonParts.push($scope.gridWork[i].child[j].Discount);
                        }

                        
                    }
                }
            }
            //nentuin diskon task & parts
            $scope.displayDiscountAlgorithm();



            // added by sss on 2017-09-14
            var idxEstimation = _.findIndex($scope.gridEstimation.data, { "TaskBPId": item.TaskBPId })
            if (idxEstimation > -1) {
                selectedTaskBP = item;
                $scope.gridEstimation.data.splice(idxEstimation, 1);
                if (selectedTaskBP !== null) {
                    var findEl = _.find($scope.gridEstimation.data, selectedTaskBP);
                    if (typeof findEl === 'undefined') {
                        $scope.gridEstimation.data.push({
                            "TaskBPId": item.TaskBPId,
                            "TaskName": selectedTaskBP.TaskName,
                            "BodyEstimationMinute": item.BodyEstimationMinute ? selectedTaskBP.BodyEstimationMinute : 0,
                            "PutyEstimationMinute": item.PutyEstimationMinute ? selectedTaskBP.PutyEstimationMinute : 0,
                            "SurfacerEstimationMinute": item.SurfacerEstimationMinute ? selectedTaskBP.SurfacerEstimationMinute : 0,
                            "SprayingEstimationMinute": item.SprayingEstimationMinute ? selectedTaskBP.SprayingEstimationMinute : 0,
                            "PolishingEstimationMinute": item.PolishingEstimationMinute ? selectedTaskBP.PolishingEstimationMinute : 0,
                            "ReassemblyEstimationMinute": item.ReassemblyEstimationMinute ? selectedTaskBP.ReassemblyEstimationMinute : 0,
                            "FIEstimationMinute": item.FIEstimationMinute ? selectedTaskBP.FIEstimationMinute : 0,
                        });
                    }
                } else {
                    $scope.gridEstimation.data.push({
                        "TaskBPId": item.TaskBPId,
                        "TaskName": item.TaskName,
                        "BodyEstimationMinute": 0,
                        "PutyEstimationMinute": 0,
                        "SurfacerEstimationMinute": 0,
                        "SprayingEstimationMinute": 0,
                        "PolishingEstimationMinute": 0,
                        "ReassemblyEstimationMinute": 0,
                        "FIEstimationMinute": 0
                    });
                }
                selectedTaskBP = null
            } else {
                if (selectedTaskBP == null) {
                    $scope.gridEstimation.data.push({
                        "TaskBPId": item.TaskBPId,
                        "TaskName": item.TaskName,
                        "BodyEstimationMinute": 0,
                        "PutyEstimationMinute": 0,
                        "SurfacerEstimationMinute": 0,
                        "SprayingEstimationMinute": 0,
                        "PolishingEstimationMinute": 0,
                        "ReassemblyEstimationMinute": 0,
                        "FIEstimationMinute": 0
                    });
                } else {
                    $scope.gridEstimation.data.push({
                        "TaskBPId": item.TaskBPId,
                        "TaskName": selectedTaskBP.TaskName,
                        "BodyEstimationMinute": item.BodyEstimationMinute ? selectedTaskBP.BodyEstimationMinute : 0,
                        "PutyEstimationMinute": item.PutyEstimationMinute ? selectedTaskBP.PutyEstimationMinute : 0,
                        "SurfacerEstimationMinute": item.SurfacerEstimationMinute ? selectedTaskBP.SurfacerEstimationMinute : 0,
                        "SprayingEstimationMinute": item.SprayingEstimationMinute ? selectedTaskBP.SprayingEstimationMinute : 0,
                        "PolishingEstimationMinute": item.PolishingEstimationMinute ? selectedTaskBP.PolishingEstimationMinute : 0,
                        "ReassemblyEstimationMinute": item.ReassemblyEstimationMinute ? selectedTaskBP.ReassemblyEstimationMinute : 0,
                        "FIEstimationMinute": item.FIEstimationMinute ? selectedTaskBP.FIEstimationMinute : 0,
                    });
                    selectedTaskBP = null;
                }
                selectedTaskBP = null;
            }
            // if (selectedTaskBP == null) {
            //     $scope.gridEstimation.data.push({
            //         "TaskBPId": item.TaskBPId,
            //         "TaskName": item.TaskName,
            //         "BodyEstimationMinute": 0,
            //         "PuttyEstimationMinute": 0,
            //         "SurfacerEstimationMinute": 0,
            //         "SprayingEstimationMinute": 0,
            //         "PolishingEstimationMinute": 0,
            //         "ReassemblyEstimationMinute": 0,
            //         "FIEstimationMinute": 0
            //     });
            // } else {
            //     // var exstEl = _.find($scope.gridEstimation.data, { "TaskBPId": item.TaskBPId });
            //     // console.log("exstEl", exstEl);               
            //     // if (typeof exstEl !== 'undefined') {    
            //     //     var index = $scope.gridEstimation.data.indexOf(exstEl);    
            //     //     if (index > -1) {$scope.gridEstimation.data.splice(index, 1);}                    
            //     // }
            //     var idxEstimation = _.findIndex($scope.gridEstimation.data,{ "TaskBPId": item.TaskBPId })
            //     if(idxEstimation > -1){
            //         $scope.gridEstimation.data.splice(idxEstimation, 1);
            //     }
            //     var findEl = _.find($scope.gridEstimation.data, selectedTaskBP);
            //     if (typeof findEl === 'undefined') {
            //         $scope.gridEstimation.data.push({
            //             "TaskBPId": item.TaskBPId,
            //             "TaskName": selectedTaskBP.TaskName,
            //             "BodyEstimationMinute": selectedTaskBP.BodyEstimationMinute,
            //             "PuttyEstimationMinute": selectedTaskBP.PuttyEstimationMinute,
            //             "SurfacerEstimationMinute": selectedTaskBP.SurfacerEstimationMinute,
            //             "SprayingEstimationMinute": selectedTaskBP.SprayingEstimationMinute,
            //             "PolishingEstimationMinute": selectedTaskBP.PolishingEstimationMinute,
            //             "ReassemblyEstimationMinute": selectedTaskBP.ReassemblyEstimationMinute,
            //             "FIEstimationMinute": selectedTaskBP.FIEstimationMinute,
            //         });
            //     }
            //     selectedTaskBP = null;
            // }



            $scope.calculateforwork();

            if ($scope.gridEstimation.data.length > 0) {
                $scope.appDate = false;
            }


            // setelah data task & parts tersimpan, normalisasi lagi diskon yang pada metode pembayaran yang di pilih [CR4]
            if($scope.mData.isCash == 1){ //cash
                $scope.choosePembayaran($scope.mData.isCash);
                console.log('Diskon Parts | Cash | Spare Parts===>', $scope.DiscountParts, $scope.mData.disParts ) ;

            }else{ //isurance
                for (i in $scope.Asuransi){
                    if($scope.Asuransi[i].InsuranceId == insuranceId){
                        $scope.mData.InsuranceName = $scope.Asuransi[i].InsuranceName;
                        $scope.selectInsurance($scope.Asuransi[i]);
                    }
                }
                console.log('Diskon Parts | Isurance| Spare Parts===>', $scope.DiscountParts, $scope.mData.disParts ) ;

            }


        }

        //Tyre
        $scope.checkStatusPartTyre = function (id, dataTask, jobtaskId){

            var tmpArray = [];

            for (var i in dataTask) {
                if (dataTask[i].JobPartsId !== undefined) {
                    tmpArray.push({
                        JobPartsId: dataTask[i].JobPartsId
                    })
                }
            }
            WoHistoryGR.getCheckDataPartTyre(id, tmpArray).then(
                function (res) {
                validasi = res.data;
                if(!validasi){
                    var idxTask = _.findIndex($scope.gridWork, { JobTaskId: jobtaskId });
                    if (idxTask > -1) {
                        for(var z in dataTask){
                            $scope.gridWork[idxTask].child.push(dataTask[z]);
                        }
                    }
                    bsAlert.warning("Parts Tyre tidak dapat dihapus,", 'karena PO tyre telah terbentuk');
                    $scope.sumAllPrice();
                }
            }, function (err) {
                var idxTask = _.findIndex($scope.gridWork, { JobTaskId: jobtaskId });
                if (idxTask > -1) {
                    for(var z in dataTask){
                        $scope.gridWork[idxTask].child.push(dataTask[z]);
                    }
                }
                $scope.sumAllPrice();
            });
        }
        
        $scope.onBeforeDelete = function(data){
            console.log('ini onBeforeDelete',data);
            console.log('$scope.gridWork ===>', $scope.gridWork)
   
            if(tampungEstimasi.length > 0){
                for(var i in tampungEstimasi){
                    if($scope.gridWork.length > 0){
                        for(var x in $scope.gridWork){
                            var count = 0;
                            if(typeof tampungEstimasi[i].EstimationBPId !== undefined){
                                console.log('ini ada EstimationBPId nya ==>',tampungEstimasi[i].EstimationBPId);
                                if(tampungEstimasi[i].EstimationBPId == $scope.gridWork[x].EstimationBPId){
                                    count++;
                                    // console.log('EstNo =>',tampungEstimasi[i].EstimationBPId + ' | count => ' + count + ' | EstTaskLength => '+ tampungEstimasi[i].EstTaskLength + ' DO SPLICE');
                                    if(count == tampungEstimasi[i].EstTaskLength){
                                        tampungEstimasi.splice(x, 1);
                                        console.log('DO SPLICE | tampungEstimasi ===>',tampungEstimasi);
                                    }else{
                                        tampungEstimasi[i].EstTaskLength = tampungEstimasi[i].EstTaskLength - 1;
                                        // console.log('DONT SPLICE | tampungEstimasi ===>',tampungEstimasi);

                                    }
                                }
                            }
                        }
                    }
                }
            }

            console.log('tampung estimasi ==>',tampungEstimasi);

        }


        $scope.onAfterDelete = function(idArray) {
            console.log("idArray ===>", idArray);
            console.log('$scope.gridWork ===>',$scope.gridWork);
            console.log('$scope.gridEstimation.data onAfterDelete ===>',$scope.gridEstimation.data);
            if($scope.jejeranDP.length>0){
                $scope.arrayDP(gridTemp, $scope.jejeranDP);
            }

            var NumberParent = 1;
            var NumberChild = 1;
            for (i in $scope.gridWork){
                $scope.gridWork[i].NumberParent = NumberParent+parseInt(i);
                for (j in $scope.gridWork[i].child){
                    $scope.gridWork[i].child[j].NumberChild = $scope.gridWork[i].NumberParent + "."  + parseInt(++j);
                }
            }

            for (var i in idArray) {
                var tes = _.findIndex($scope.gridEstimation.data, { TaskBPId: idArray[i] });
                console.log('tes ===>',tes);
                if (tes > -1) {
                    $scope.gridEstimation.data.splice(tes, 1);
                }
            }
            // _.map($scope.gridEstimation,function(val){

            // });
            // ===========
            var totalW = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                    totalW += 0;
                }else{
                    // totalW += Math.round($scope.gridWork[i].Fare / 1.1);
                    totalW += ASPricingEngine.calculate({
                                    InputPrice: $scope.gridWork[i].Fare,
                                    // Discount: 5,
                                    // Qty: 2,
                                    Tipe: 2, 
                                    PPNPercentage: PPNPerc,
                                });
                }
            }
            $scope.totalWork = totalW;
            // ============
           
            // ===========
            var totalDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].DownPayment !== null) {
                            // if($scope.gridWork[i].child[j].DownPayment > 100.00){
                            //     totalDp += $scope.gridWork[i].child[j].DownPayment;
                            //     console.log('totalDP dari Inputan',$scope.gridWork[i].child[j].DownPayment);
                            // }else{
                            //     if($scope.gridWork[i].child[j].DPestimasi !== undefined){//kalo dari estimasi sudah * 1.1
                            //         totalDp += $scope.gridWork[i].child[j].DownPayment;
                            //         console.log("TotalDP dari Estimasi", $scope.gridWork[i].child[j].DownPayment);
                            //     }else{
                            //         //IRV = ini request Via, kalo DP itu + PPN
                            //         //SNBU = Sudah nanya BU
                                    // totalDp += $scope.gridWork[i].child[j].DownPayment * 1.1;
                                    // totalDp += $scope.gridWork[i].child[j].DownPayment * (1+(PPNPerc/100));
                                    totalDp += $scope.gridWork[i].child[j].DownPayment ;


                                    // console.log("TotalDP bukan dari Estimasi", $scope.gridWork[i].child[j].DownPayment * 1.1);
                            //     }
                            // }

                        }
                    }
                }
            }
            $scope.totalDp = totalDp;
            // ===========
            $scope.tmpActRateChange = angular.copy($scope.tmpActRate);
            var totalActualRate = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].ActualRate !== undefined) {
                    totalActualRate += $scope.gridWork[i].ActualRate;
                }
            }
            $scope.tmpActRate = totalActualRate;
            console.log("$scope.tmpActRate", $scope.tmpActRate);
            console.log("$scope.tmpActRateChange", $scope.tmpActRateChange);
            // ============
            var totalDefaultDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].minimalDp !== undefined) {
                            totalDefaultDp += $scope.gridWork[i].child[j].minimalDp;
                            console.log("$scope.gridWork[i].child.totalDefaultDp", $scope.gridWork[i].child[j].minimalDp);
                        }
                    }
                }
            }
            $scope.totalDpDefault = totalDefaultDp;
            // ==============
      
            // =================== CR4 Re-Calculate following CSV, req by Kevin =======================
            var totalMaterial_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30){
                            totalMaterial_byKevin += 0;
                        }else{
                            // totalMaterial_byKevin += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                            totalMaterial_byKevin += ASPricingEngine.calculate({
                                                        InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                        // Discount: 5,
                                                        Qty: $scope.gridWork[i].child[j].Qty,
                                                        Tipe: 102, 
                                                        PPNPercentage: PPNPerc,
                                                    }); 
                        }
                    }
                }
            };
            
            var totalMD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30 ){//req by pak eko bahan tetap dapet diskon 08-juli-2020
                                totalMD_byKevin += 0;
                        }else{
                            // totalMD_byKevin +=  Math.round(Math.round($scope.gridWork[i].child[j].RetailPrice /1.1 ) * ($scope.gridWork[i].child[j].Discount/100)) *  $scope.gridWork[i].child[j].Qty;
                            totalMD_byKevin +=  ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                    Discount: $scope.gridWork[i].child[j].Discount,
                                                    Qty: $scope.gridWork[i].child[j].Qty,
                                                    Tipe: 112, 
                                                    PPNPercentage: PPNPerc,
                                                });
                            
                        }
                    }
                }
            };

            var totalWD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                    totalWD_byKevin += 0;
                }else{
                    // totalWD_byKevin +=  Math.round(Math.round($scope.gridWork[i].Fare /1.1 ) * ($scope.gridWork[i].Discount/100));
                    totalWD_byKevin +=  ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWork[i].Fare,
                                            Discount: $scope.gridWork[i].Discount,
                                            // Qty: 2,
                                            Tipe: 12, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                }
            };
            // =======================================================
            
            // =======================================================

            $scope.totalMaterial           = totalMaterial_byKevin;
            $scope.totalMaterialDiscounted = totalMD_byKevin;
            $scope.totalWorkDiscounted     = totalWD_byKevin;
            $scope.subTotalMaterialSummary = $scope.totalMaterial - $scope.totalMaterialDiscounted;
            $scope.subTotalWorkSummary     = $scope.totalWork - $scope.totalWorkDiscounted;
            // $scope.totalPPN                = Math.floor(($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * 0.1);     
            $scope.totalPPN                = ASPricingEngine.calculate({
                                                InputPrice: (($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * (1+PPNPerc/100)),
                                                // Discount: 0,
                                                // Qty: 0,
                                                Tipe: 33, 
                                                PPNPercentage: PPNPerc,
                                            }); 
            $scope.totalEstimasi           = $scope.totalPPN + $scope.subTotalMaterialSummary + $scope.subTotalWorkSummary;

            // =================== CR4 Re-Calculate following CSV, req by Kevin =======================
            //nentuin diskon task & parts
            jejeranDiskonTask = [];
            jejeranDiskonParts = [];
            for(var i in $scope.gridWork){
                // if($scope.gridWork.PaidById == 30|| $scope.gridWork.PaidById == 31 || $scope.gridWork.PaidById == 32 || $scope.gridWork.PaidById == 2277){
                if($scope.gridWork.PaidById == 30|| $scope.gridWork.PaidById == 2277){
                    jejeranDiskonTask.push(0);
                }else{
                    jejeranDiskonTask.push($scope.gridWork[i].Discount);
                }
                if($scope.gridWork[i].child.length > 0 ){
                    for(var j in $scope.gridWork[i].child){
                        // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 31 || $scope.gridWork[i].child[j].PaidById == 32 || $scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 2277){
                            jejeranDiskonParts.push(0);
                        }else{
                            jejeranDiskonParts.push($scope.gridWork[i].child[j].Discount);
                        }

                        
                    }
                }
            }
            //nentuin diskon task & parts
            $scope.displayDiscountAlgorithm();


            if ($scope.tmpActRate !== $scope.tmpActRateChange) {
                $scope.mData.StallId = 0;
                $scope.mData.FullDate = null;
                $scope.mData.PlanStart = null;
                $scope.mData.PlanFinish = null;
                $scope.mData.PlanDateStart = null;
                //$scope.mData.AppointmentDate = null;
                delete $scope.mData.AppointmentDate;
                //$scope.mData.AppointmentTime = null;
                delete $scope.mData.AppointmentTime;
                $scope.mData.TargetDateAppointment = null;
                $scope.mData.FixedDeliveryTime1 = null;
                $scope.mData.PlanDateFinish = null;
                $scope.mData.StatusPreDiagnose = 0;
                delete $scope.mData.PrediagScheduledTime;
                delete $scope.mData.PrediagStallId;
            }

        }

        // $scope.checkPrice = function(data) {
        //     console.log("RetailPrice nya ===>", data);
        //     console.log("$scope.ldModel.Qty nya ==>", $scope.ldModel.Qty);
        //     var sum = 0;
        //     var sumDp = 0;
        //     if (data !== undefined && data !== null) {
        //         if ($scope.ldModel.Qty !== null) {
        //             var qty = $scope.ldModel.Qty;
        //             var tmpDp = $scope.ldModel.nightmareIjal;
        //             if (qty !== null) {
        //                 sum = Math.round(qty * data);
        //                 sumDp = Math.round(qty * tmpDp);
        //                 $scope.ldModel.subTotal = sum;
        //                 $scope.ldModel.DownPayment = sumDp;
        //                 $scope.ldModel.DPRequest = sumDp;
        //                 $scope.ldModel.minimalDp = sumDp;
        //                 $scope.ldModel.Discount = 0;
        //             } else {
        //                 $scope.ldModel.subTotal = 0;
        //                 $scope.ldModel.Discount = 0;
        //                 $scope.ldModel.DownPayment = Math.round(tmpDp);
        //                 $scope.ldModel.DPRequest = Math.round(tmpDp);
        //                 $scope.ldModel.minimalDp = Math.round(tmpDp);
        //             }
        //         }
        //     } else {
        //         $scope.ldModel.subTotal = 0;
        //         $scope.ldModel.Discount = 0;
        //         $scope.ldModel.DownPayment = 0;
        //         $scope.ldModel.DPRequest = 0;
        //         $scope.ldModel.minimalDp = 0;
        //     }
        // };
        $scope.checkPrice = function(data) {
            console.log("checkPrice data ===>", data);
            console.log('$scope.ldModel | checkPrice ===>',$scope.ldModel)

            //req by pak eko bahan tetap dapet diskon 08-juli-2020
            // if($scope.ldModel.MaterialTypeId == 2){ //bahan
            //     $scope.DiscountParts = 0;
            //     $scope.mData.disParts = 0;
            //     console.log('Diskon Parts | Bahan ===>', $scope.DiscountParts, $scope.mData.disParts ) ;
            // }else{ //parts

                if($scope.mData.isCash == 1){ //cash
                    $scope.choosePembayaran($scope.mData.isCash);
                    console.log('Diskon Parts | Cash | Spare Parts===>', $scope.DiscountParts, $scope.mData.disParts ) ;

                }else{ //isurance
                    for (i in $scope.Asuransi){
                        if($scope.Asuransi[i].InsuranceId == insuranceId){
                            $scope.mData.InsuranceName = $scope.Asuransi[i].InsuranceName;
                            $scope.selectInsurance($scope.Asuransi[i]);
                        }
                    }
                    console.log('Diskon Parts | Isurance| Spare Parts===>', $scope.DiscountParts, $scope.mData.disParts ) ;

                }
            // }


            console.log("data", data);
            var tmpharga = angular.copy(data);
            // tmpharga = tmpharga.toString().replace(/,/g, '');
            tmpharga = parseInt(tmpharga)
            var sum = 0;
            var sumDp = 0;
            if (data != null && data != undefined){
                if ($scope.ldModel.Qty !== null) {
                    var qty = $scope.ldModel.Qty;
                    var tmpDp = $scope.ldModel.minimalDp ? $scope.ldModel.minimalDp : 0;
                    if (qty !== null) {
                        sum = qty * tmpharga;
                        sumDp = qty * tmpDp;
                        var sum2 = angular.copy(sum);
                        sum2 = sum.toString();
                        // ==== new code ===
                        $scope.ldModel.subTotalMask = sum2.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        $scope.ldModel.RetailPrice = tmpharga;
                        $scope.ldModel.subTotal = sum;
                        $scope.ldModel.DPRequest = sumDp;
                        $scope.ldModel.DownPayment = sumDp;
    
                        //tambahan CR4  start ==> request okrivia (qty * harga parts ) - diskon
                        // if($scope.ldModel.PaidById == 30 || $scope.ldModel.PaidById == 31 || $scope.ldModel.PaidById == 32 || ($scope.ldModel.MaterialTypeId == 2 && $scope.ldModel.PaidById == 28)){ 
                        // if($scope.ldModel.PaidById == 30 || $scope.ldModel.PaidById == 31 || $scope.ldModel.PaidById == 32 ){ //req by pak eko bahan tetap dapet diskon 08-juli-2020
                        if($scope.ldModel.PaidById == 30 || $scope.ldModel.PaidById == 2277 ){ //req by pak eko bahan tetap dapet diskon 08-juli-2020
                            $scope.ldModel.Discount = 0;
                            $scope.ldModel.DiskonForDisplay = 0+'%';
                            $scope.ldModel.NominalDiscount = 0;
                            $scope.ldModel.subTotalForDisplay = sum - $scope.ldModel.NominalDiscount;
                            $scope.ldModel.subTotal = sum;
                        }else{
                            $scope.ldModel.Discount = $scope.DiscountParts;
                            $scope.ldModel.DiskonForDisplay = $scope.DiscountParts+'%';
                            $scope.ldModel.NominalDiscount = (qty*$scope.ldModel.RetailPrice)*($scope.DiscountParts/100);
                            $scope.ldModel.subTotalForDisplay = sum - $scope.ldModel.NominalDiscount;
                            $scope.ldModel.subTotal = sum;
                        }
                        // tambahan CR4 end
    
    
                    } else {
                        $scope.ldModel.subTotal = 0;
                        $scope.ldModel.RetailPrice = tmpharga;
                        var XDp = tmpDp.toString();
                        var XtmpDp = '';
                        if (XDp.includes('.')) {
                            XDp = tmpDp.toString().split('.');
                            XDp[0] = XDp[0].toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                            XtmpDp = XDp[0] + '.' + XDp[1];
                        } else {
                            XtmpDp = XDp;
                        }
                        if (XSumDp == 'NaN') {
                            XSumDp = '';
                        }
                        $scope.ldModel.DPRequest = XSumDp;
                        $scope.ldModel.DownPayment = XSumDp;
                        var XSumDp2 = angular.copy(XSumDp);
                        XSumDp2 = XSumDp.toString();
                        $scope.ldModel.DPRequest = XSumDp2.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        $scope.ldModel.DownPayment = XSumDp2.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                    }
                } else {
                    $scope.ldModel.subTotal = 0;
                    // $scope.ldModel.Discount = 0;
                    $scope.ldModel.DownPayment = 0;
                    $scope.ldModel.DPRequest = 0;
                    $scope.ldModel.minimalDp = 0;
                }
            } else {
                $scope.ldModel.subTotal = 0;
                // $scope.ldModel.Discount = 0;
                $scope.ldModel.DownPayment = 0;
                $scope.ldModel.DPRequest = 0;
                $scope.ldModel.minimalDp = 0;
            }
            
        };
        $scope.checkServiceRate = function(item, data) {
            console.log("item", item);
            console.log("item", data);
            var sum = 0;
            var row = {};
            var tmpItem;
            if (data == 59 || data == 60) {
                if ($scope.tmpServiceRate !== null) {
                    if (item !== null) {
                        tmpItem = item.toString();
                        if (tmpItem.includes(".")) {
                            tmpItem = tmpItem.split('.');
                            if (tmpItem[tmpItem.length - 1] !== ".") {
                                // item = parseInt(tmpItem);
                                sum = $scope.tmpServiceRate.WarrantyRate * item;
                                sum = Math.round(sum);
                            }
                        } else {
                            sum = $scope.tmpServiceRate.WarrantyRate * item;
                            sum = Math.round(sum);
                        }
                    } else {
                        sum = $scope.tmpServiceRate.WarrantyRate;
                        sum = Math.round(sum);
                    }
                    $scope.PriceAvailable = true;
                    sum = Math.round(sum);
                    row.Summary = sum;
                    row.Fare = $scope.tmpServiceRate.WarrantyRate;
                }
            } else {
                if ($scope.tmpServiceRate !== null) {
                    if (item !== null) {
                        tmpItem = item.toString();
                        if (tmpItem.includes(".")) {
                            tmpItem = tmpItem.split('.');
                            if (tmpItem[tmpItem.length - 1] !== ".") {
                                // item = parseInt(tmpItem);
                                sum = $scope.tmpServiceRate.ServiceRate * item;
                                sum = Math.round(sum);
                            }
                        } else {
                            sum = $scope.tmpServiceRate.ServiceRate * item;
                            sum = Math.round(sum);
                        }
                    } else {
                        sum = $scope.tmpServiceRate.ServiceRate;
                        sum = Math.round(sum);
                    }
                    $scope.PriceAvailable = true;
                    sum = Math.round(sum);
                    row.Summary = sum;
                    row.Fare = $scope.tmpServiceRate.ServiceRate;
                }
            }
            row.ActualRate = item;
            $scope.listApi.addDetail(false, row);
        }


        // ====================================
        // ===========================
        // Allow Pattern
        // ===========================
        $scope.allowPattern = function(event, type, item) {
            console.log("event", event);
            // if(event.charCode == 13){
            //     $scope.getData();
            // }
            var patternRegex
            if (type == 1) {
                patternRegex = /\d/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /\d|[a-z]|[*]/i; //ALPHANUMERIC ONLY
                if (item.includes("*")) {
                    event.preventDefault();
                    return false;
                }
            } else if (type == 3) {
                patternRegex = /\d|[a-z]/i;
            }
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };

        $scope.allowPatternFilter = function(event, type, item) {
            console.log("event", event);
            if (event.charCode == 13) {
                $scope.getData();
            }
            var patternRegex
            if (type == 1) {
                patternRegex = /\d/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /\d|[a-z]|[*]/i; //ALPHANUMERIC ONLY
                if (item.includes("*")) {
                    event.preventDefault();
                    return false;
                }
            } else if (type == 3) {
                patternRegex = /\d|[a-z]/i;
            }
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };

        $scope.givePattern = function(item, event) {
            console.log("item", item);
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
                $scope.nilaiKM = item;
                if ($scope.nilaiKM.includes(",")) {
                    $scope.nilaiKM = $scope.nilaiKM.split(',');
                    if ($scope.nilaiKM.length > 1) {
                        $scope.nilaiKM = $scope.nilaiKM.join('');
                    } else {
                        $scope.nilaiKM = $scope.nilaiKM[0];
                    }
                }
                $scope.nilaiKM = parseInt($scope.nilaiKM);
                $scope.mData.Km = $scope.mData.Km.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                return;
            }
        };
        $scope.givePatternHargaPart = function(item, event) {
            console.log("item", item);
            var temp = {};
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
                item = item.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                // temp.RetailPrice = item;
                // $scope.listApi.addDetail(false, temp);
                console.log("givePatternDp", item);
                // $scope.ldModel.RetailPrice = item;
                $scope.ldModel.RetailPriceMask = item;

                return;
            }
        };

        // ========= WAC=====
        $scope.mDataWAC = {};
        $scope.tabClick = function(param) {
            $scope.getDataExt(param);
        };
        $scope.getDataExt = function(id) {
            console.log('id header', id)
            WOBP.getItemWAC(id).then(function(resu) {
                console.log('item wac', resu.data.Result);
                if (id == 2 && $scope.gridExt.data.length == 0) {
                    $scope.dataExt = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataExt, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = '';
                        obj.OtherDescription = null;

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Ext = arr;
                    $scope.gridExt.data = $scope.mDataWAC.Ext;
                    // console.log('$scope.mDataWAC.Ex', $scope.mDataWAC.Ext);
                } else if (id == 4 && $scope.gridInt.data.length == 0) {
                    $scope.dataInt = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataInt, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = '';
                        obj.OtherDescription = null;

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Int = arr;
                    $scope.gridInt.data = $scope.mDataWAC.Int;
                } else if (id == 1003 && $scope.gridElec.data.length == 0) {
                    $scope.dataElec = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataElec, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = '';
                        obj.OtherDescription = null;

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Elec = arr;
                    $scope.gridElec.data = $scope.mDataWAC.Elec;
                } else if (id == 1004 && $scope.gridEquip.data.length == 0) {
                    $scope.dataEquip = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataEquip, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = '';
                        obj.OtherDescription = null;

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Equip = arr;
                    $scope.gridEquip.data = $scope.mDataWAC.Equip;
                } else if (id == 1005 && $scope.gridDoc.data.length == 0) {
                    $scope.dataDoc = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataDoc, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = '';
                        obj.OtherDescription = null;

                        arr.push(obj);
                    });
                    console.log('grid dataDoc arr', arr);
                    $scope.mDataWAC.Doc = arr;
                    $scope.gridDoc.data = $scope.mDataWAC.Doc;
                } else if (id == 1006 && $scope.gridOther.length == 0) {
                    $scope.dataOther = resu.data.Result;
                    console.log('grid other ', $scope.dataOther[0]);
                    var arr = [];
                    angular.forEach($scope.dataOther, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = null;
                        obj.OtherDescription = '';
                        arr.push(obj);
                    });
                    $scope.mDataWAC.Oth = arr;
                    $scope.gridOther = $scope.mDataWAC.Oth;
                    console.log('grid other arr', $scope.gridOther);
                } else if (id == 1007 && $scope.gridEquipSafe.data.length == 0) {
                    $scope.dataOther = resu.data.Result;
                    console.log('grid other ', $scope.dataOther[0]);
                    var arr = [];
                    angular.forEach($scope.dataOther, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = null;
                        obj.OtherDescription = '';
                        arr.push(obj);
                    });
                    $scope.mDataWAC.Oth = arr;
                    $scope.gridEquipSafe.data = $scope.mDataWAC.Oth;
                    console.log('grid other arr', $scope.gridEquipSafe.data);
                } else if (id == 1008 && $scope.gridPersonal.data.length == 0) {
                    $scope.dataOther = resu.data.Result;
                    console.log('grid other ', $scope.dataOther[0]);
                    var arr = [];
                    angular.forEach($scope.dataOther, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = null;
                        obj.OtherDescription = '';
                        arr.push(obj);
                    });
                    $scope.mDataWAC.Oth = arr;
                    $scope.gridPersonal.data = $scope.mDataWAC.Oth;
                    console.log('grid other arr', $scope.gridPersonal.data);
                }
            })
        };
        //WAC LIST > Body ------------------------------------

        $scope.coorX = null;
        $scope.coorY = null;
        $scope.drawData = [];
        $scope.showPopup = false;
        $scope.openPopup = function(e) {
            ////
            $scope.showPopup = true;
            var left = e.offsetX;
            var top = e.offsetY;
            $scope.coorX = angular.copy(left) + 'px';
            $scope.coorY = angular.copy(top) + 'px';
            console.log('event X', $scope.coorX);
            console.log('event Y', $scope.coorY);
            $scope.popoverStyle = {
                'position': 'absolute',
                'background': '#fff',
                'border': '1px solid #999',
                'padding': '10px',
                'width': 'auto',
                'box-shadow': '0 0 10px rgba(0, 0, 0, .5)',
                'left': $scope.coorX,
                'top': $scope.coorY
            };
            $scope.drawData = [{
                x: $scope.coorX,
                y: $scope.coorY
            }];
            console.log('scope.drawData', $scope.drawData);
        };

        $scope.closePopover = function(param) {
            $scope.paramClosePopover = param;
            $scope.showPopup = false;
            console.log('scope.paramClosePopover', $scope.paramClosePopover);
        };


        //WAC LIST > Exterior --------------------------------
        $scope.ItemStatusTemplate = '<div ng-disabled="true" class="btn-group ui-grid-cell-contents">' +
            '<input ng-disabled="true" ng-model="row.entity.ItemStatus" type="radio" ng-value="1" style="width:20px">' +
            '&nbsp;Baik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
            '<input ng-disabled="true" ng-model="row.entity.ItemStatus" type="radio" ng-value="2" style="width:20px">' +
            '&nbsp;Rusak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
            '<input ng-disabled="true" ng-model="row.entity.ItemStatus" type="radio" ng-value="3" style="width:20px">' +
            '&nbsp;Tidak Ada</div>';

        $scope.ItemStatus = null;
        $scope.gridExt = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                field: 'ItemStatus',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridExtApi = gridApi;

                $scope.gridExtApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridExtApi.selection.selectRow) {
                    $scope.gridExtApi.selection.selectRow($scope.gridExt.data[0]);
                }
            }
        };
        //WAC LIST > Interior --------------------------------
        $scope.gridInt = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridIntApi = gridApi;

                $scope.gridIntApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridIntApi.selection.selectRow) {
                    $scope.gridIntApi.selection.selectRow($scope.gridInt.data[0]);
                }
            }
        };

        //WAC LIST > Electricity --------------------------------
        $scope.gridElec = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridElecApi = gridApi;

                $scope.gridElecApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridElecApi.selection.selectRow) {
                    $scope.gridElecApi.selection.selectRow($scope.gridElec.data[0]);
                }
            }
        };

        //WAC LIST > Equipment --------------------------------
        $scope.gridEquip = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridEquipApi = gridApi;

                $scope.gridEquipApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridEquipApi.selection.selectRow) {
                    $scope.gridEquipApi.selection.selectRow($scope.gridEquip.data[0]);
                }
            }
        };

        //WAC LIST > Document --------------------------------
        $scope.gridDoc = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridDocApi = gridApi;

                $scope.gridDocApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridDocApi.selection.selectRow) {
                    $scope.gridDocApi.selection.selectRow($scope.gridDoc.data[0]);
                }
            }
        };
        //WAC LIST > Other ----------------------------------
        $scope.summernoteOptions2 = {
            dialogsInBody: true,
            dialogsFade: false,
            height: 300
        };
        //WAC LIST > Equipment Safety --------------------------------
        $scope.gridEquipSafe = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridEquipSafeApi = gridApi;

                $scope.gridEquipSafeApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridEquipSafeApi.selection.selectRow) {
                    $scope.gridEquipSafeApi.selection.selectRow($scope.gridEquipSafe.data[0]);
                }
            }
        };
        //WAC LIST > Personal Item --------------------------------
        $scope.gridPersonal = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridPersonalApi = gridApi;

                $scope.gridPersonalApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridPersonalApi.selection.selectRow) {
                    $scope.gridPersonalApi.selection.selectRow($scope.gridPersonal.data[0]);
                }
            }
        };
        $scope.areasArray = [];
        $scope.show_modal = { show: false };
        $scope.groupWACData = [];
        $scope.jobWACData = [];
        $scope.imageSel = null;
        $scope.imageWACArr = [];

        // added by sss on 2017-10-11
        $scope.areasArrayVC = [];
        $scope.imageSelVC = null;
        //

        $scope.chgChk = function() {
            if (!$scope.disableWACExt) {
                // console.log("false check..");
                // console.log("mstId : ",mstId);
                // var chAreas = document.getElementsByClassName('choosen-area');
                // for (var i in chAreas) {
                //     console.log(chAreas[i]);
                //     if (typeof chAreas[i].classList !== 'undefined') {
                //         chAreas[i].classList.remove('choosen-area');
                //     }
                // }
                $('.choosen-area').removeClass('choosen-area');
                $scope.grid.data = [];
                dotsData[mstId] = [];
            }
            $scope.disableWACExt = !$scope.disableWACExt;
        }

        // added by sss on 2017-10-11
        $scope.chgChkVC = function() {
                if (!$scope.disableWACExtVC) {
                    // console.log("false check..");
                    // console.log("mstId : ",mstId);
                    // var chAreas = document.getElementsByClassName('choosen-area');
                    // for (var i in chAreas) {
                    //     console.log(chAreas[i]);
                    //     if (typeof chAreas[i].classList !== 'undefined') {
                    //         chAreas[i].classList.remove('choosen-area');
                    //     }
                    // }
                    $('.choosen-area').removeClass('choosen-area');
                    $scope.gridVC.data = [];
                    dotsDataVC[mstIdVC] = [];
                }
                $scope.disableWACExtVC = !$scope.disableWACExtVC;
            }
            //

        GeneralMaster.getData(2030).then(
            function(res) {
                $scope.VTypeData = res.data.Result;
                console.log("$scope.VTypeData !!!!!!!!!!!!!!!!!!!!!!!!!!!!", $scope.VTypeData);
                return res.data;
            }
        );

        WOBP.getAllTypePoints().then(
            function(res) {
                // console.log(res);
                $scope.imageWACArr = res.data.Result;
                console.log("$scope.imageWACArr", $scope.imageWACArr);
                $scope.loading = false;
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );

        var dotsData = { 2221: [], 2222: [], 2223: [], 2224: [], 2225: [], 2226: [], 2227: [] };
        var mstId = null;

        // added by sss on 2017-10-11
        var dotsDataVC = { 2221: [], 2222: [], 2223: [], 2224: [], 2225: [], 2226: [], 2227: [] };
        var mstIdVC = null;
        //

        function getImgAndPoints(mstId) {
            console.log("gambarrr", mstId);
            switch (mstId) {
                case 2221:
                    $scope.imageSel = "../images/wacImages/Type A.png";
                    break;
                case 2222:
                    $scope.imageSel = "../images/wacImages/Type B.png";
                    break;
                case 2223:
                    $scope.imageSel = "../images/wacImages/Type C.png";
                    break;
                case 2224:
                    $scope.imageSel = "../images/wacImages/Type D.png";
                    break;
                case 2225:
                    $scope.imageSel = "../images/wacImages/Type E.png";
                    break;
                case 2226:
                    $scope.imageSel = "../images/wacImages/Type F.png";
                    break;
                case 2227:
                    $scope.imageSel = "../images/wacImages/Type G.png";
            }
            var temp = _.find($scope.imageWACArr, function(o) {
                return o.TypeId == mstId;
            });

            return JSON.parse(temp.Points);
        };

        // added by sss on 2017-10-11
        function getImgAndPointsVC(mstIdVC) {
            console.log("mstIdVC : ", mstIdVC);
            switch (mstIdVC) {
                case 2221:
                    $scope.imageSelVC = "../images/wacImages/Type A.png";
                    break;
                case 2222:
                    $scope.imageSelVC = "../images/wacImages/Type B.png";
                    break;
                case 2223:
                    $scope.imageSelVC = "../images/wacImages/Type C.png";
                    break;
                case 2224:
                    $scope.imageSelVC = "../images/wacImages/Type D.png";
                    break;
                case 2225:
                    $scope.imageSelVC = "../images/wacImages/Type E.png";
                    break;
                case 2226:
                    $scope.imageSelVC = "../images/wacImages/Type F.png";
                    break;
                case 2227:
                    $scope.imageSelVC = "../images/wacImages/Type G.png";
            }
            var temp = _.find($scope.imageWACArr, function(o) {
                return o.TypeId == mstIdVC;
            });

            console.log("temp : ", temp);

            return JSON.parse(temp.Points);
        };
        //

        $scope.chooseVType = function(selected) {
            // dotsData = {2221:[],2222:[],2223:[],2224:[],2225:[],2226:[],2227:[]};
            mstId = selected;
            $scope.gridWAC.data = angular.copy(dotsData[mstId]);

            WO.getProbPoints(1221, mstId).then(
                function(res) {
                    $scope.areasArray = getImgAndPoints(mstId);
                    console.log("$scope.areasArray", $scope.areasArray);
                    if (res.data.Result.length > 0) {
                        for (var i in res.data.Result) {
                            var getTemp = JSON.parse(res.data.Result[i]["Points"]);
                            for (var j in getTemp) {
                                var xtemp = angular.copy(getTemp[j]);
                                delete xtemp['status'];
                                console.log("xtemp : ", xtemp);
                                if (typeof _.find($scope.areasArray, xtemp) !== 'undefined') {
                                    console.log("find..", xtemp);
                                    var idx = _.findIndex($scope.areasArray, xtemp);
                                    xtemp.cssClass = "choosen-area";
                                    $scope.areasArray[idx] = xtemp;

                                    dotsData[mstId].push({ "ItemId": xtemp.areaid, "ItemName": xtemp.name, "Points": xtemp, "Status": getTemp[j].status });
                                }
                            }
                        }
                        setDataGrid(dotsData[mstId]);
                    }
                },
                function(err) {
                    $scope.areasArray = getImgAndPoints(mstId);
                }
            );
        };

        // added by sss on 2017-10-11
        $scope.chooseVTypeVC = function(selected) {
            // dotsData = {2221:[],2222:[],2223:[],2224:[],2225:[],2226:[],2227:[]};
            mstIdVC = selected.MasterId;
            $scope.gridVC.data = angular.copy(dotsDataVC[mstIdVC]);

            $scope.areasArrayVC = getImgAndPointsVC(mstIdVC);
        };
        //

        $scope.onChangeAreas = function(ev, boxId, areas, area) {
            console.log(areas);
            $scope.logDots = JSON.stringify(areas);
            $scope.$apply();
        };

        $scope.onAddArea = function(ev, boxId, areas, area) {
            console.log(areas);
            $scope.logDots = JSON.stringify(areas);
            $scope.$apply();
        };

        $scope.onRemoveArea = function(ev, boxId, areas, area) {
            console.log(areas);
            $scope.logDots = JSON.stringify(areas);
            $scope.$apply();
        };

        $scope.disableWACExt = true;

        // added by sss on 2017-10-11
        $scope.disableWACExtVC = true;
        //

        var sel;
        $scope.doClick = function(ev, boxId, areas, area, stat, selection) {
            // console.log(boxId);
            // console.log(areas);
            // console.log(area);
            // console.log(stat);
            // if (stat) {
            //     console.log("true");
            //     // For @lifachristian
            //     if (selection.hasClass("choosen-area")) {
            //         if (sel != undefined) {
            //             sel.empty();
            //         }
            //         $name = $(selection).empty().append($("<div><span class=\"select-area-field-label border-thin name-area-id-" + area.areaid + "\">" + area.name + "</span></div>"));
            //         sel = selection;
            //     }
            // } else {
            console.log("false");
            // For @amieklo2204
            if (!$scope.disableWACExt) {
                if (selection.hasClass("choosen-area")) {
                    selection.removeClass("choosen-area");
                    if (_.find(dotsData[mstId], { "ItemId": area.areaid }) !== undefined) {
                        // console.log("find false");
                        dotsData[mstId].splice(_.findIndex(dotsData[mstId], { "ItemId": area.areaid }), 1);
                    }
                } else {
                    selection.addClass("choosen-area");
                    if (_.find(dotsData[mstId], { "ItemId": area.areaid }) == undefined) {
                        // console.log("if 1");
                        if (typeof _.find($scope.areasArray, { "areaid": area.areaid }) !== 'undefined') {
                            // console.log("if 2");
                            var addtemp = _.find($scope.areasArray, { "areaid": area.areaid });
                            dotsData[mstId].push({ "ItemId": area.areaid, "ItemName": area.name, "Points": addtemp, "Status": null, "Tasks": null });
                        }
                    }
                }
                setDataGrid(dotsData[mstId]);
            }
            // }
        }

        // added by sss on 2017-10-11
        $scope.doClickVC = function(ev, boxId, areas, area, stat, selection) {
                console.log("false");
                // For @amieklo2204
                if (!$scope.disableWACExtVC) {
                    if (selection.hasClass("choosen-area")) {
                        selection.removeClass("choosen-area");
                        if (_.find(dotsDataVC[mstIdVC], { "ItemId": area.areaid }) !== undefined) {
                            // console.log("find false");
                            dotsDataVC[mstIdVC].splice(_.findIndex(dotsDataVC[mstIdVC], { "ItemId": area.areaid }), 1);
                        }
                    } else {
                        selection.addClass("choosen-area");
                        console.log("boxId : ", boxId);
                        console.log("area : ", area);
                        console.log("areas : ", areas);
                        console.log("stat : ", stat);
                        console.log("selection : ", selection);
                        console.log("dotsDataVC[mstIdVC] : ", dotsDataVC[mstIdVC]);
                        console.log("$scope.areasArrayVC : ", $scope.areasArrayVC);

                        if (_.find(dotsDataVC[mstIdVC], { "ItemId": area.areaid }) == undefined) {
                            // console.log("if 1");
                            // if (typeof _.find($scope.areasArrayVC, { "areaid": area.areaid }) !== 'undefined') {
                            //     // console.log("if 2");
                            //     var addtemp = _.find($scope.areasArrayVC, { "areaid": area.areaid });
                            //     dotsDataVC[mstIdVC].push({ "ItemId": area.areaid, "ItemName": area.name, "Points": addtemp, "Status": null, "Tasks": null });
                            // }
                            if (typeof _.find(areas, { "areaid": area.areaid }) !== 'undefined') {
                                var addtemp = _.find(areas, { "areaid": area.areaid });
                                if (typeof _.find($scope.areasArrayVC, { "name": addtemp.name, "x": addtemp.x, "y": addtemp.y }) !== 'undefined') {
                                    // console.log("if 2");
                                    var addtempx = _.find($scope.areasArrayVC, { "name": addtemp.name, "x": addtemp.x, "y": addtemp.y });
                                    dotsDataVC[mstIdVC].push({ "ItemId": addtempx.areaid, "ItemName": addtempx.name, "Points": addtempx, "Status": null, "Tasks": null });
                                }
                            }
                        }
                        // added by sss on 2017-10-10
                        console.log("taskListObj[addtempx.areaid] : ", taskListObj[addtempx.areaid]);
                        $scope.gridVC.columnDefs[2].editDropdownOptionsArray = angular.copy(taskListObj[addtempx.areaid]);
                        // var temp = taskListObj[1].concat([{ 'TaskName': 'Perbaikan Sisi Bumper', 'TaskListBPId': 2 }, { 'TaskName': 'Amplas Samping Bumper', 'TaskListBPId': 3 }]);
                        // console.log('temp : ',temp);
                        // $scope.gridVC.columnDefs[2].editDropdownOptionsArray = angular.copy(temp);
                        //
                    }
                    setDataGridVC(dotsDataVC[mstIdVC]);
                }
                // }
            }
            //

        function setDataGrid(data) {
            console.log('data setDataGrid', data);
            $scope.gridWAC.data = angular.copy(data);
        }

        // added by sss on 2017-10-11
        function setDataGridVC(data) {
            console.log('data setDataGrid VC', data);
            $scope.gridVC.data = angular.copy(data);
        }
        //

        $scope.onDropdownChange = function(ent) {
            console.log(ent);
            var temp = _.findIndex(dotsData[mstId], { "ItemId": ent.ItemId, "ItemName": ent.ItemName, "Points": ent.Points });
            if (temp > -1) {
                dotsData[mstId][temp].Status = ent.Status;
            }
        }

        $scope.onMultiDropdownChange = function(ent) {
            console.log(ent);
            var xtemp = [];
            // added by sss on 2017-10-12
            $scope.gridWorkEstimate = ent.Tasks;
            _.forEach(ent.Tasks, function(e) {
                $scope.gridTimeEst.data.push({
                    "TaskBPId": e.TaskBPId,
                    "OrderName": e.TaskName,
                    "Body": e.BodyEstimationMinute,
                    "Putty": e.PuttyEstimationMinute,
                    "Surfacer": e.SurfacerEstimationMinute,
                    "Painting": e.SprayingEstimationMinute,
                    "Polishing": e.PolishingEstimationMinute,
                    "Reassembbly": e.ReassemblyEstimationMinute,
                    "Final_Inspection": e.FIEstimationMinute,
                });
            });
            //
            for (var i in ent.Tasks) {
                xtemp.push(ent.Tasks[i].TaskBPId);
            }
            // console.log("xtemp.length : ",xtemp.length);
            // if ((xtemp.length % 2 > 0) && (xtemp.length > 2)) {
            //     $scope.grid.rowHeight = (xtemp.length / 2 * 30)+15;
            // } else if (xtemp.length <= 2) {
            //     $scope.grid.rowHeight = 30;
            // }
            var temp = _.findIndex(dotsDataVC[mstIdVC], { "ItemId": ent.ItemId, "ItemName": ent.ItemName, "Points": ent.Points });
            if (temp > -1) {
                dotsDataVC[mstIdVC][temp].Tasks = xtemp;
            }
        }

        $scope.saveData = function() {
            var grdData = $scope.grid.data;
            var lastArr = [];

            var pointsArr = [];
            for (var i in dotsData[mstId]) {
                var tmpObj = dotsData[mstId][i].Points;
                tmpObj.status = dotsData[mstId][i].Status;
                pointsArr.push(tmpObj);
            }
            var temp = {};
            temp.JobWACExtId = 0;
            temp.JobId = 1221;
            temp.TypeId = mstId;
            temp.Points = JSON.stringify(pointsArr);
            temp.DataUploadUrl = {
                strBase64: $scope.mData.Signature
            };
            for (var i = 0; i < $scope.uploadFiles.length; i++) {
                if ($scope.uploadFiles[i].strBase64 !== null && typeof $scope.uploadFiles[i].strBase64 !== 'undefined') {
                    temp["DataUploadFoto" + (i == 0 ? "" : i + 1)] = {
                        strBase64: $scope.uploadFiles[i].strBase64
                    };
                }
            }

            lastArr.push(temp);
            console.log("lastArr : " + JSON.stringify(lastArr));

            WO.createEXT(lastArr).then(function(res) {
                console.log(res);
            });
        };

        //----------------------------------
        // Grid WAC EXTERIOR Setup
        //----------------------------------
        var customCellTemplate = '<div><select ui-grid-edit-dropdown ng-model=\"MODEL_COL_FIELD\" ng-change="grid.appScope.onDropdownChange(row.entity)" ng-options=\"field[editDropdownIdLabel] as field[editDropdownValueLabel] CUSTOM_FILTERS for field in editDropdownOptionsArray\"></select></div>';

        // added by sss on 2017-10-10
        // var customCellMultiTemplate = '<div><select multiple ui-grid-edit-dropdown ng-model=\"MODEL_COL_FIELD\" ng-change="grid.appScope.onDropdownChange(row.entity)" ng-options=\"field[editDropdownIdLabel] as field[editDropdownValueLabel] CUSTOM_FILTERS for field in editDropdownOptionsArray\"></select></div>'
        var customCellMultiTemplate =
            '<div class="ui-select">' +
            '   <ui-select-wrap>' +
            '       <ui-select multiple ng-model=\"MODEL_COL_FIELD\" theme="select2" append-to-body="true" ng-change="grid.appScope.onMultiDropdownChange(row.entity)">' +
            '           <ui-select-match placeholder="Choose...">{{ $item.TaskName }}</ui-select-match>' +
            '           <ui-select-choices repeat="item in col.colDef.editDropdownOptionsArray | filter: $select.search">' +
            '               <span>{{ item.TaskName }}</span>' +
            '           </ui-select-choices>' +
            '       </ui-select>' +
            '   </ui-select-wrap>' +
            '</div>';

        var multiCell = '<div class="ui-grid-cell-contents">{{ COL_FIELD.join(\', \') }}</div>';

        $scope.gridWAC = {
            enableSorting: true,
            enableRowSelection: false,
            // added by sss on 2017-10-11
            // rowHeight: 30,
            //
            // multiSelect: true,
            // enableSelectAll: true,
            enableCellEdit: true,
            // showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'JobWACExtId', field: 'JobWACExtId', width: '7%', visible: false },
                { name: 'Nama Item', field: 'ItemName', enableCellEdit: false },
                {
                    name: 'Status Kerusakan',
                    // width: '15%',
                    field: 'Status',
                    // editableCellTemplate: 'ui-grid/dropdownEditor',
                    editableCellTemplate: customCellTemplate,
                    editDropdownValueLabel: 'Name',
                    editDropdownIdLabel: 'Id',
                    editDropdownOptionsArray: [
                        { 'Id': 1, 'Name': 'Penyok' }, { 'Id': 2, 'Name': 'Baret' }, { 'Id': 3, 'Name': 'Rusak' },
                    ],
                    cellFilter: "griddropdownStat:this"
                },
                // {
                //     name: 'Pekerjaan',
                //     field: 'Tasks',
                //     // editableCellTemplate: 'ui-grid/dropdownEditor',
                //     editableCellTemplate: customCellMultiTemplate,
                //     editDropdownValueLabel: 'TaskName',
                //     editDropdownIdLabel: 'TaskListBPId',
                //     editDropdownOptionsArray: [],
                //     cellFilter: "griddropdownTask:this",
                //     // cellTemplate: multiCell,
                // },
            ]
        };

        $scope.gridVC = {
            enableSorting: true,
            enableRowSelection: false,
            // added by sss on 2017-10-11
            // rowHeight: 30,
            //
            // multiSelect: true,
            // enableSelectAll: true,
            enableCellEdit: true,
            // showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'JobWACExtId', field: 'JobWACExtId', width: '7%', visible: false },
                { name: 'Nama Item', field: 'ItemName', width: '30%', enableCellEdit: false },
                // {
                //     name: 'Status Kerusakan',
                //     width: '15%',
                //     field: 'Status',
                //     // editableCellTemplate: 'ui-grid/dropdownEditor',
                //     editableCellTemplate: customCellTemplate,
                //     editDropdownValueLabel: 'Name',
                //     editDropdownIdLabel: 'Id',
                //     editDropdownOptionsArray: [
                //         { 'Id': 1, 'Name': 'Penyok' }, { 'Id': 2, 'Name': 'Baret' }, { 'Id': 3, 'Name': 'Rusak' },
                //     ],
                //     cellFilter: "griddropdownStat:this"
                // },
                {
                    name: 'Pekerjaan',
                    field: 'Tasks',
                    // editableCellTemplate: 'ui-grid/dropdownEditor',
                    editableCellTemplate: customCellMultiTemplate,
                    editDropdownValueLabel: 'TaskName',
                    editDropdownIdLabel: 'TaskListBPId',
                    editDropdownOptionsArray: [],
                    cellFilter: "griddropdownTask:this",
                    // cellTemplate: multiCell,
                },
            ]
        };

        //================================================================= wac ==============================================================================//



        $scope.clearEditCustomer = function() {
            $scope.filterSearch.filterValue = "";
            $scope.showFieldInst = false;
            $scope.showFieldPersonal = false;
        };

        // added by sss on 2017-09-25
        $scope.fClaim = { show: false };
        // $scope.modalMode = 'new';
        $scope.mClaim = {};
        AppointmentBpService.getInsurence().then(function(res) {
            console.log("res getInsurence", res.data);
            $scope.Asuransi = res.data;
        });
        // WO.getVehicleTypeColor().then(function(resu) {
        //     console.log('getVehicleTypeColor', resu.data.Result);
        //     $scope.Color = resu.data.Result;
        // })
        $scope.claimShow = function() {
            console.log("claimShow..");
            console.log("modalMode : ", $scope.modalMode)
            if ($scope.modalMode == 'new') {
                $scope.mClaim = {};
                if (typeof $scope.mData.LicensePlate !== 'undefined' && $scope.mData.LicensePlate !== null) {
                    $scope.mClaim.LicensePlate = angular.copy($scope.mData.LicensePlate);
                }
                if (typeof $scope.mData.VehicleModelId !== 'undefined' && $scope.mData.VehicleModelId !== null) {
                    $scope.mClaim.VehicleModelId = angular.copy($scope.mData.VehicleModelId);
                }
                if (typeof $scope.mData.VehicleTypeId !== 'undefined' && $scope.mData.VehicleTypeId !== null) {
                    $scope.mClaim.VehicleTypeId = angular.copy($scope.mData.VehicleTypeId);
                    var temp = _.find($scope.Color, { VehicleTypeId: $scope.mData.VehicleTypeId });
                    if (typeof temp !== 'undefined') {
                        $scope.selectTypeClaim(temp);
                    }
                }
            }

            $scope.fClaim.show = true;
        };
        // $scope.selectModelClaim = function(item) {
        //     console.log('selectedModel >>>', item);
        //     $scope.VehicT = [];
        //     $scope.mDataCrm.Vehicle.Type = null;
        //     var dVehic = item.MVehicleType;
        //     _.map(dVehic, function(a) {
        //         a.NewDescription = a.Description + ' - ' + a.KatashikiCode + ' - ' + a.SuffixCode;
        //     });
        //     console.log('dVehic >>>', dVehic);
        //     $scope.VehicT = dVehic;
        //     // $scope.enableType = false;
        // };
       
        $scope.selectModelClaim = function(item) {
            // $scope.typeData
            $scope.typeData = [];
            $scope.mDataCrm.Vehicle.Type = null;
            // var dVehic = item.MVehicleType;
            var dVehic = _.uniq(item.MVehicleType, 'KatashikiCode');
            _.map(dVehic, function(a) {
                a.NewDescription = a.Description + ' - ' + a.KatashikiCode + ' - ' + a.SuffixCode;
            });
            console.log('dVehic >>>', dVehic);
            $scope.typeData = dVehic;
            // $scope.enableType = false;
        };
        $scope.enableColorClaim = true;
        $scope.selectTypeClaim = function(item) {
            console.log('selectedType >>>', item);
            $scope.colorData = [];
            var arrCol = [];
            _.map($scope.Color, function(val) {
                if (val.VehicleTypeId == item.VehicleTypeId) {
                    arrCol = val.ListColor;
                };
            });
            $scope.enableColorClaim = false;
            $scope.colorData = arrCol;
        };

        $scope.selectColorClaim = function(item) {
            console.log('selectedColor >>>', item);
            $scope.mClaim.ColorCode = item.ColorCode;
            console.log('$scope.mClaim >>>', $scope.mClaim);
        };


        // =====================CR4 Add Estimasi =======================

        var jejeranDiskonTask = [];
        var jejeranDiskonParts = [];
        $scope.displayDiscountAlgorithm = function(){
            console.log('jejeranDiskonTask ==>',jejeranDiskonTask);
            console.log('jejeranDiskonParts ==>',jejeranDiskonParts);
            if(jejeranDiskonTask.length > 0){
                var checkDisTskDouble = _.uniqBy(jejeranDiskonTask, function (e) { return e;});
                console.log("jejeranDiskonTask Unik ===>",checkDisTskDouble);
                if(checkDisTskDouble.length > 0){
                    if(checkDisTskDouble.length == 1) $scope.DisplayDiscountTask = jejeranDiskonTask[0];
                    else $scope.DisplayDiscountTask = '';
                }else $scope.DisplayDiscountTask = '';
            }else $scope.DisplayDiscountTask = 0;
        
            if(jejeranDiskonParts.length > 0){
                var checkDisPrtsDouble = _.uniqBy(jejeranDiskonParts, function (e) { return e;});
                console.log("jejeranDiskonParts Unik ===>",checkDisPrtsDouble);
                if(checkDisPrtsDouble.length > 0){
                    if(checkDisPrtsDouble.length == 1) $scope.DisplayDiscountParts = jejeranDiskonParts[0];
                    else $scope.DisplayDiscountParts = '';
                }else $scope.DisplayDiscountParts = '';
            }else $scope.DisplayDiscountParts = 0;


            if ($scope.gridWork.length > 0) {
                var allWarranty = 0;
                for (var i = 0; i<$scope.gridWork.length; i++){
                    if ($scope.gridWork[i].PaidById == 30) {
                        allWarranty++;
                    }
                }

                if (allWarranty != 0 && allWarranty == $scope.gridWork.length) {
                    $scope.DisplayDiscountTask = 0;
                    $scope.DisplayDiscountParts = 0;
                }
            }
        }
        
        var jejeranDiskonTaskViewEstimasi = [];
        var jejeranDiskonPartsViewEstimasi = [];
        $scope.displayDiscountAlgorithmViewEstimasi = function(){
            console.log('jejeranDiskonTaskViewEstimasi ==>',jejeranDiskonTaskViewEstimasi);
            console.log('jejeranDiskonPartsViewEstimasi ==>',jejeranDiskonPartsViewEstimasi);
            if(jejeranDiskonTaskViewEstimasi.length > 0){
                var checkDisTskDouble = _.uniqBy(jejeranDiskonTaskViewEstimasi, function (e) {return e;});
                console.log("jejeranDiskonTaskViewEstimasi uniq ===>",checkDisTskDouble);
                if(checkDisTskDouble.length > 0){
                    if(checkDisTskDouble.length == 1) $scope.DiscountTaskViewEstimasi = jejeranDiskonTaskViewEstimasi[0];
                    else $scope.DiscountTaskViewEstimasi = "";
                }else $scope.DiscountTaskViewEstimasi = "";
            }else $scope.DiscountTaskViewEstimasi = 0 
            
        
        
            if(jejeranDiskonPartsViewEstimasi.length > 0){
                var checkDisPrtsDouble = _.uniqBy(jejeranDiskonPartsViewEstimasi, function (e) {return e; });
                console.log("jejeranDiskonPartsViewEstimasi uniq ==>",checkDisPrtsDouble);
                if(checkDisPrtsDouble.length > 0){
                    if(checkDisPrtsDouble.length == 1)$scope.DiscountPartsViewEstimasi = jejeranDiskonPartsViewEstimasi[0];
                    else $scope.DiscountPartsViewEstimasi = "";
                }else $scope.DiscountPartsViewEstimasi = "";
            }else $scope.DiscountPartsViewEstimasi = 0;
        }


        $scope.btnBatalViewEstimasi = function(){  
            $scope.showModalViewEstimasi = false;
            $scope.showModalEstimasiYangBelumDigunakan = true;
        }

        $scope.mData = {};
        $scope.mData.WoCategoryId        = undefined;
        $scope.mData.Km                  = undefined;
        $scope.mData.ContactPerson       = undefined;
        $scope.mData.PhoneContactPerson1 = undefined;
        $scope.mData.Email               = undefined;
        $scope.mData.isCash              = undefined;


        var countEstimasiMasuk = 0;
        var jumlahEstimasiMasuk = 0;
        // var isUsedEstimasi = [];
        $scope.btnPilihEstimasi = function () {
            tampungparts = [];
            tampungtask = [];
            taskAntiDuplicate = [];
            partsAntiDuplicate = [];

            jumlahEstimasiMasuk = $scope.selectedCekEstimasi.length;

           
            for(var i in  $scope.selectedCekEstimasi){
                console.log('ini estimasi yang dipilih ===>',$scope.selectedCekEstimasi[i]);
                console.log('$scope.selectedCekEstimasi[i].ModelCode ===>',$scope.selectedCekEstimasi[i].ModelCode);
                console.log('$scope.mDataDetail.VehicleModelId         ===>',$scope.mDataDetail.VehicleModelId);

                console.log('tampung estimasi ===>',tampungEstimasi)
                if($scope.mDataDetail.VehicleModelId !== undefined){
                    if (parseInt($scope.selectedCekEstimasi[i].ModelCode) !== $scope.mDataDetail.VehicleModelId){
                        $scope.bsAlertConfirm("Anda tidak dapat menggunakan data Estimasi ini karna Model kendaraan anda berbeda dengan Estimasi ini").then(function (res) {
                            if (res) {
                                console.log('ini kalo jenis mobilnya sama | OK');
                            }else{
                                console.log('ini kalo jenis mobilnya sama | !OK');
                            }
                        });
                    }else{
                        console.log('ini masuk processedDataEstimasi | kalo katashikinya ada isinya');
                        $scope.processedDataEstimasi(i);
                    }
                }else{
                    console.log('ini masuk processedDataEstimasi | kalo katashikinya kosong');
                    $scope.processedDataEstimasi(i);
                }
            }
           

        }; 

        $scope.cekEstimasi = function () {
            $scope.disableBtnPilihEstimasi = true;
            $scope.showModalViewEstimasi = false;
            $scope.showModalEstimasiYangBelumDigunakan = true;

            $scope.getDataEstimasi();

            var numLength = angular.element('.ui.modal.ModalEstimasiYangBelumDigunakan').length;
            setTimeout(function () {
                angular.element('.ui.modal.ModalEstimasiYangBelumDigunakan').modal('refresh');
            }, 0);

            if (numLength > 1) {
                angular.element('.ui.modal.ModalEstimasiYangBelumDigunakan').not(':first').remove();
            }

            angular.element(".ui.modal.ModalEstimasiYangBelumDigunakan").modal("show");
            //angular.element('.ui.modal.PengurusanJasaDokumenSO').modal('show');
        }


      


        $scope.btnBatalCekEstimasi = function () {

            angular.element('.ui.modal.ModalEstimasiYangBelumDigunakan').modal('hide');
        }

        $scope.getDataEstimasi = function () {
            // var url = "&NoPolisi=" + $scope.mDataDetail.LicensePlate + "&DateFrom=2004-01-01&DateTo=9999-09-09";       
            var url = $scope.mDataDetail.LicensePlate;       
            AppointmentBpService.getDataCekEstimasiNew(url).then(function (res) {

                console.log('ini data get estimasi ===>',res.data.Result);

                var filteredEstimasi = [];
                for(var i in res.data.Result){
                    if(res.data.Result[i].isAppointment == 1){
                        res.data.Result[i].isUsedInAppointment = true;
                        console.log(' ini data gabisa dipake ===>',res.data.Result[i].EstimationNo);
                        filteredEstimasi.push(res.data.Result[i]);
                    }else{
                        console.log(' ini data bisa dipake =====>',res.data.Result[i].EstimationNo);
                        filteredEstimasi.push(res.data.Result[i]);
                    }
                }

                if(tampungEstimasi.length > 0){
                    for(var i in tampungEstimasi){
                        for(var j in filteredEstimasi){
                            if(tampungEstimasi[i].EstimationNo == filteredEstimasi[j].EstimationNo){
                                filteredEstimasi[j].isUsedInAppointment = true;
                            }
                        }
                    }
                }

                $scope.listEstimasiYangBelumDigunakan.data = filteredEstimasi;
            });             
            
        }

        $scope.fixDate = function (date) {
            if (date != null || date != undefined) {
                var fix = 
                    ('0' + date.getDate()).slice(-2) + "/"  + 
                    ('0' + (date.getMonth() + 1)).slice(-2) + "/" +
                    date.getFullYear() ;
                return fix;
            } else {
                return null;
            }
        };

        $scope.tanggaljamViewEstimasi = function (mentah){
            mentah = new Date(mentah)
            var date = ('0' + mentah.getDate()).slice(-2)
            var month =('0' + (mentah.getMonth() + 1)).slice(-2) 
            var year = mentah.getFullYear();
            var hour = ('0' + mentah.getHours()).slice(-2)
            var minute =('0' + mentah.getMinutes()).slice(-2)
            var second = ('0' + mentah.getSeconds()).slice(-2)
            var final = date + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second;
            return final;
        }

        $scope.cekPartsViewEstimasi = function(tempParts, tempTask, idxTask, idxParts){
            var result = {};
            AppointmentBpService.getAvailableParts(tempParts.PartsId, 0, tempParts.qty).then(function(res) { 
                dataHasilPengecekanParts = res.data.Result[0];
                console.log('dataHasilPengecekanParts ====>',dataHasilPengecekanParts);
                if(tempParts.PartsId == dataHasilPengecekanParts.PartsId){
                    var indexTask = _.findIndex($scope.jejeranDPViewEstimasi, {'EstTaskBPId':tempTask.EstTaskBPId});
                    if(indexTask != -1){

                        console.log('DP ====> tempParts.PaidById =====>',tempParts.PaidById)
                        console.log('DP ====> tempParts.qty ==========>',tempParts.qty)
                        console.log('DP ====> tempParts.RetailPrice ==>',tempParts.RetailPrice)
                        console.log('DP ====> tempParts.Discount =====>',tempParts.Discount)
                        console.log('DP ====> cekParts.PercentDP =====>',dataHasilPengecekanParts.PercentDP)
                        console.log('DP ====> cekParts.RetailPrice ===>',dataHasilPengecekanParts.RetailPrice)
                        console.log('DP ====> cekParts.QtyFree =======>',dataHasilPengecekanParts.QtyFree)

                        if(tempParts.PaidById == 28){
                            console.log('DP ====> pembayarannya Customer | ada kemungkinan dapet DP')
                            if(dataHasilPengecekanParts.isAvailable == 0){ // tidak tersedia
                                // var subTotalMaterial = (dataHasilPengecekanParts.RetailPrice/1.1) * tempParts.Qty;
                                var subTotalMaterial = (dataHasilPengecekanParts.RetailPrice/(1+(PPNPerc/100))) * tempParts.Qty;

                                // var subTotalDiscountMaterial =  ((dataHasilPengecekanParts.RetailPrice /1.1 ) * (tempParts.Discount/100)) *  tempParts.Qty;
                                // var totalMaterialDiscounted = subTotalMaterial - subTotalDiscountMaterial;
                                var totalMaterialDiscounted = subTotalMaterial;
    
                                $scope.jejeranDPViewEstimasi[indexTask].partsList.push({
                                    EstTaskBPId  : tempTask.EstTaskBPId,
                                    Qty          : tempParts.qty,
                                    PartsId      : tempParts.PartsId,
                                    EstPartsId   : tempParts.EstPartsId,
                                    PaidById     : tempParts.PaidById,
                                    PercentDP    : dataHasilPengecekanParts.PercentDP,
                                    RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                    isAvailable  : dataHasilPengecekanParts.isAvailable,
                                    PriceDP      : dataHasilPengecekanParts.PriceDP,
                                    QtyFree      : dataHasilPengecekanParts.QtyFree,
                                    Available    : "Tidak Tersedia",
                                    // DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                    DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                })
    
                                // $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].jancok = "kondisi satu";
                                // console.log('DP ====> dapet DP karna Parts Tidak Tersedia ===>',(totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1);



                            }else if(dataHasilPengecekanParts.isAvailable == 1){ //tersedia 
                                $scope.jejeranDPViewEstimasi[indexTask].partsList.push({
                                    EstTaskBPId  : tempTask.EstTaskBPId,
                                    Qty          : tempParts.qty,
                                    PartsId      : tempParts.PartsId,
                                    EstPartsId   : tempParts.EstPartsId,
                                    PaidById     : tempParts.PaidById,
                                    PercentDP    : dataHasilPengecekanParts.PercentDP,
                                    RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                    isAvailable  : dataHasilPengecekanParts.isAvailable,
                                    PriceDP      : dataHasilPengecekanParts.PriceDP,
                                    QtyFree      : dataHasilPengecekanParts.QtyFree,
                                    DownPaymentRecalculate : 0,
                                    Available    : "Tersedia",
                                })
    
                                $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].DownPayment = 0;
                                $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].jancok = "kondisi dua";
                                console.log('DP ====> Tidak dapet DP karna Parts Tersedia ===>',0);

    
                            }else if(dataHasilPengecekanParts.isAvailable == 2){// tersedia sebagian

                                var qtyTidakTersedia = tempParts.Qty -  dataHasilPengecekanParts.QtyFree;
                                // var subTotalMaterial = (dataHasilPengecekanParts.RetailPrice/1.1) * qtyTidakTersedia;
                                var subTotalMaterial = (dataHasilPengecekanParts.RetailPrice/(1+(PPNPerc/100))) * qtyTidakTersedia;

                                // var subTotalDiscountMaterial =  ((dataHasilPengecekanParts.RetailPrice /1.1 ) * (tempParts.Discount/100)) *  qtyTidakTersedia;
                                // var totalMaterialDiscounted = subTotalMaterial - subTotalDiscountMaterial ;
                                var totalMaterialDiscounted = subTotalMaterial;


                                $scope.jejeranDPViewEstimasi[indexTask].partsList.push({
                                    EstTaskBPId  : tempTask.EstTaskBPId,
                                    Qty          : tempParts.qty,
                                    PartsId      : tempParts.PartsId,
                                    EstPartsId   : tempParts.EstPartsId,
                                    PaidById     : tempParts.PaidById,
                                    PercentDP    : dataHasilPengecekanParts.PercentDP,
                                    RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                    isAvailable  : dataHasilPengecekanParts.isAvailable,
                                    PriceDP      : dataHasilPengecekanParts.PriceDP,
                                    QtyFree      : dataHasilPengecekanParts.QtyFree,
                                    Available    : "Tersedia Sebagian",
                                    // DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                    DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                })
    
                                // $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1;
                                $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100));

                                $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].jancok = "kondisi tiga";
                                // console.log('DP ====> Seharusnya dapet DP Sebagian karna Parts Tersedia Sebagian ===>',(totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1);

                            }

                        }else{
                            console.log('DP ====> pembayarannya bukan Customer | tidak ada kemungkinan dapet DP');
                            $scope.jejeranDPViewEstimasi[indexTask].partsList.push({
                                EstTaskBPId  : tempTask.EstTaskBPId,
                                Qty          : tempParts.qty,
                                PartsId      : tempParts.PartsId,
                                EstPartsId   : tempParts.EstPartsId,
                                PaidById     : tempParts.PaidById,
                                PercentDP    : dataHasilPengecekanParts.PercentDP,
                                RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                isAvailable  : dataHasilPengecekanParts.isAvailable,
                                PriceDP      : dataHasilPengecekanParts.PriceDP,
                                QtyFree      : dataHasilPengecekanParts.QtyFree,
                                Available    : "ga tau ga ngecek, cuma cari DP bukan ketersediaan barang ",
                                DownPaymentRecalculate : 0
                            })

                            $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].DownPayment = 0;
                            $scope.dataViewEstimasi.AEstimationBP_TaskList_TT[idxTask].AEstimationBP_PartList[idxParts].jancok = "yg dapet diskon cuma Paidby 28 dan isAvailable 0 atau 2";
                        }

                       

                        result = $scope.jejeranDPViewEstimasi[indexTask];
                    }
                }
            });

           
            console.log('index task ==>',idxTask + " | index Parts ===> ",idxParts);
            return result;
        }

        $scope.cekParts = function(tempParts, tempTask, idxTaskEstimasi, idxPartsEstimasi){
            var result = {};
            console.log('gridWork ===>',$scope.gridWork);
            console.log('gridTemp ===>',gridTemp);


            console.log('cekParts | mDataByEstimasi ===>',$scope.mDataByEstimasi)
            AppointmentBpService.getAvailableParts(tempParts.PartsId, 0, tempParts.qty).then(function(res) { 
                dataHasilPengecekanParts = res.data.Result[0];
                console.log('dataHasilPengecekanParts ====>',dataHasilPengecekanParts);
                if(tempParts.PartsId == dataHasilPengecekanParts.PartsId){
                    var indexTask = _.findIndex($scope.jejeranDP, {'TaskBPId':tempTask.TaskBPId});
                    if(indexTask != -1){

                        console.log('DP ====> tempParts.PaidById =====>',tempParts.PaidById)
                        console.log('DP ====> tempParts.qty ==========>',tempParts.qty)
                        console.log('DP ====> tempParts.RetailPrice ==>',tempParts.RetailPrice)
                        console.log('DP ====> tempParts.Discount =====>',tempParts.Discount)
                        console.log('DP ====> cekParts.PercentDP =====>',dataHasilPengecekanParts.PercentDP)
                        console.log('DP ====> cekParts.RetailPrice ===>',dataHasilPengecekanParts.RetailPrice)
                        console.log('DP ====> cekParts.QtyFree =======>',dataHasilPengecekanParts.QtyFree)

                        if(tempParts.PaidById == 28){

                            if(dataHasilPengecekanParts.isAvailable == 0){ // tidak tersedia
                                // var subTotalMaterial = Math.round(dataHasilPengecekanParts.RetailPrice/1.1) * tempParts.Qty;
                                var subTotalMaterial = Math.round(dataHasilPengecekanParts.RetailPrice/(1+(PPNPerc/100))) * tempParts.Qty;

                                // var subTotalDiscountMaterial =  Math.round(Math.round(dataHasilPengecekanParts.RetailPrice /1.1 ) * (tempParts.Discount/100)) *  tempParts.Qty;
                                // var totalMaterialDiscounted = subTotalMaterial - subTotalDiscountMaterial;
                                var totalMaterialDiscounted = subTotalMaterial;

                                // var totalMaterialDiscounted = (dataHasilPengecekanParts.RetailPrice * tempParts.Qty) - (dataHasilPengecekanParts.RetailPrice * tempParts.Qty)*((tempParts.Discount)/100);

                                $scope.jejeranDP[indexTask].partsList.push({
                                    TaskBPId     : tempTask.TaskBPId,
                                    Qty          : tempParts.qty,
                                    PartsId      : tempParts.PartsId,
                                    EstPartsId   : tempParts.EstPartsId,
                                    PaidById     : tempParts.PaidById,
                                    PercentDP    : dataHasilPengecekanParts.PercentDP,
                                    RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                    isAvailable  : dataHasilPengecekanParts.isAvailable,
                                    PriceDP      : dataHasilPengecekanParts.PriceDP,
                                    QtyFree      : dataHasilPengecekanParts.QtyFree,
                                    Available    : "Tidak Tersedia",
                                    // DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                    DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                })
    
                                // $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "kondisi satu";
                                
                                // gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "kondisi satu";
                                // console.log('DP ====> dapet DP karna Parts Tidak Tersedia ===>',(totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1);
                                // console.log('DP ====> total Parts | tidak Tersedia ===>',totalMaterialDiscounted, (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1);
                                
                                
                            }else if(dataHasilPengecekanParts.isAvailable == 1){ //tersedia
                                $scope.jejeranDP[indexTask].partsList.push({
                                    TaskBPId     : tempTask.TaskBPId,
                                    Qty          : tempParts.qty,
                                    PartsId      : tempParts.PartsId,
                                    EstPartsId   : tempParts.EstPartsId,
                                    PaidById     : tempParts.PaidById,
                                    PercentDP    : dataHasilPengecekanParts.PercentDP,
                                    RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                    isAvailable  : dataHasilPengecekanParts.isAvailable,
                                    PriceDP      : dataHasilPengecekanParts.PriceDP,
                                    QtyFree      : dataHasilPengecekanParts.QtyFree,
                                    DownPaymentRecalculate : 0,
                                    Available    : "Tersedia",
                                })
    
                                
                                $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = 0;
                                $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "kondisi dua";
    
                                gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = 0;
                                gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "kondisi dua";
                                console.log('DP ====> Tidak dapet DP karna Parts Tersedia ===>',0);

    
                            }else if(dataHasilPengecekanParts.isAvailable == 2){// tersedia sebagian

                                var qtyTidakTersedia = tempParts.Qty -  dataHasilPengecekanParts.QtyFree;
                                // var subTotalMaterial = Math.round(dataHasilPengecekanParts.RetailPrice/1.1) * qtyTidakTersedia;
                                var subTotalMaterial = Math.round(dataHasilPengecekanParts.RetailPrice/(1+(PPNPerc/100))) * qtyTidakTersedia;

                                // var subTotalDiscountMaterial =  Math.round(Math.round(dataHasilPengecekanParts.RetailPrice /1.1 ) * (tempParts.Discount/100)) *  qtyTidakTersedia;
                                // var totalMaterialDiscounted = subTotalMaterial - subTotalDiscountMaterial ;
                                var totalMaterialDiscounted = subTotalMaterial ;
                                // var totalMaterialDiscounted = (dataHasilPengecekanParts.RetailPrice * qtyTidakTersedia) - (dataHasilPengecekanParts.RetailPrice * qtyTidakTersedia)*((tempParts.Discount)/100);


                                $scope.jejeranDP[indexTask].partsList.push({
                                    TaskBPId     : tempTask.TaskBPId,
                                    Qty          : tempParts.qty,
                                    PartsId      : tempParts.PartsId,
                                    EstPartsId   : tempParts.EstPartsId,
                                    PaidById     : tempParts.PaidById,
                                    PercentDP    : dataHasilPengecekanParts.PercentDP,
                                    RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                    isAvailable  : dataHasilPengecekanParts.isAvailable,
                                    PriceDP      : dataHasilPengecekanParts.PriceDP,
                                    QtyFree      : dataHasilPengecekanParts.QtyFree,
                                    Available    : "Tersedia Sebagian",
                                    // DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                    DownPaymentRecalculate : (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                })
    
                                
                                // $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment =  (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment =  (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "kondisi tiga";
    
                                // gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1
                                gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*(1+(PPNPerc/100))

                                gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "kondisi tiga";
                                // console.log('DP ====> Seharusnya dapet DP Sebagian karna Parts Tersedia Sebagian ===>',(totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1);
                                // console.log('DP ====> total Parts | Tersedia Sebagian ===>',totalMaterialDiscounted, (totalMaterialDiscounted * (dataHasilPengecekanParts.PercentDP / 100))*1.1);

                            }

                        }else{

                            console.log('DP ====> pembayarannya bukan Customer | tidak ada kemungkinan dapet DP');
                            $scope.jejeranDP[indexTask].partsList.push({
                                TaskBPId     : tempTask.TaskBPId,
                                Qty          : tempParts.qty,
                                PartsId      : tempParts.PartsId,
                                EstPartsId   : tempParts.EstPartsId,
                                PaidById     : tempParts.PaidById,
                                PercentDP    : dataHasilPengecekanParts.PercentDP,
                                RetailPrice  : dataHasilPengecekanParts.RetailPrice,
                                isAvailable  : dataHasilPengecekanParts.isAvailable,
                                PriceDP      : dataHasilPengecekanParts.PriceDP,
                                QtyFree      : dataHasilPengecekanParts.QtyFree,
                                Available    : "ga tau ga ngecek, cuma cari DP bukan ketersediaan barang ",
                                DownPaymentRecalculate : 0
                            })

                            
                            $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment =  0;
                            $scope.gridWork[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "yg dapet diskon cuma Paidby 28 dan isAvailable 0 atau 2";

                            gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].DownPayment = 0;
                            gridTemp[idxTaskEstimasi].child[idxPartsEstimasi].jancok = "yg dapet diskon cuma Paidby 28 dan isAvailable 0 atau 2";


                        }
                        

                        $scope.sumAllPrice();

                        result = $scope.jejeranDP[indexTask];
                    }
                }
            });

           
            console.log('index task ==>',idxTaskEstimasi + " | index Parts ===> ",idxPartsEstimasi);
            return result;
        }

        $scope.arrayDPViewEstimasi = function(dataEstimasi, simpleTaskParts){
            console.log(' arrayDPViewEstimasi | dataEstimasi ======>',dataEstimasi);
            console.log(' arrayDPViewEstimasi | simpleTaskParts ===>',simpleTaskParts);
            for (var i = 0; i < dataEstimasi.AEstimationBP_TaskList_TT.length; i++) {
                var tempTask = dataEstimasi.AEstimationBP_TaskList_TT[i];
                if(dataEstimasi.AEstimationBP_TaskList_TT[i].AEstimationBP_PartList.length > 0){
                    for (var j = 0; j < dataEstimasi.AEstimationBP_TaskList_TT[i].AEstimationBP_PartList.length; j++) {
                        var tempParts = dataEstimasi.AEstimationBP_TaskList_TT[i].AEstimationBP_PartList[j]
                        $scope.cekPartsViewEstimasi(tempParts, tempTask, i,j);
                    }
                }
            }
            console.log(' $scope.jejeranDPViewEstimasi | arrayDPViewEstimasi ====> ', $scope.jejeranDPViewEstimasi)
            $scope.jejeranDPViewEstimasi = simpleTaskParts;
            return $scope.jejeranDPViewEstimasi
        }

        $scope.arrayDP = function(gridWork, dummyPengecekanParts){
            console.log(' arrayDP | gridWork ======>',gridWork);
            console.log(' arrayDP | dummyPengecekanParts ===>',dummyPengecekanParts);
            for (var x = 0; x < gridWork.length; x++) {
                var tempTaskEstimasi = gridWork[x];
                if(gridWork[x].child.length > 0){
                    for (var y = 0; y < gridWork[x].child.length; y++) {
                        var tempPartsEstimasi = gridWork[x].child[y]
                        $scope.cekParts(tempPartsEstimasi, tempTaskEstimasi, x, y);
                    }
                }
            }

            console.log(' $scope.jejeranDP | arrayDP ====> ', $scope.jejeranDP)
            $scope.jejeranDP = dummyPengecekanParts;
            return $scope.jejeranDP
        }

        $scope.jejeranDPViewEstimasi = [];
        $scope.dataViewEstimasi = undefined;
        $scope.lihatEstimasi = function(data){  
            console.log('ini lihat estimasi ===>',data);


            if(data.EstimationBPId !== 0){
                data.EstimationBPId = data.EstimationBPId;
                console.log('ini Estimasi belum WO ===> EstimationBPId => ' ,data.EstimationBPId)
            }else{
                data.EstimationBPId = data.JobId;
                console.log('ini Estimasi sudah WO ===> JobId => ',data.JobId)
            }

            AppointmentBpService.getDetailEstimasi(data.EstimationBPId, $scope.mDataDetail.LicensePlate).then(function (res) {
                $scope.dataViewEstimasi =  angular.copy(res.data.Result[0]);
                console.log('getDetail selected estimasi ===>',$scope.dataViewEstimasi);
                $scope.showModalViewEstimasi = true;
                $scope.showModalEstimasiYangBelumDigunakan = false;

                $scope.jejeranDPViewEstimasi = [];
                for (var i = 0; i < $scope.dataViewEstimasi.AEstimationBP_TaskList_TT.length; i++) {
                    $scope.jejeranDPViewEstimasi.push({EstTaskBPId:$scope.dataViewEstimasi.AEstimationBP_TaskList_TT[i].EstTaskBPId, partsList:[]});
                }
                var dpViewEstimasi = $scope.arrayDPViewEstimasi($scope.dataViewEstimasi, $scope.jejeranDPViewEstimasi);
                console.log('jancokkkkk dpViewEstimasi ===>',dpViewEstimasi)
                console.log('jancokkkkk jejeranDPViewEstimasi ===>',$scope.jejeranDPViewEstimasi)




                for(var x in  $scope.VehicleGasType){
                    if($scope.mDataDetail.VehicleGasTypeId == $scope.VehicleGasType[x].VehicleGasTypeId){
                        $scope.mDataDetail.VehicleGasTypeName = $scope.VehicleGasType[x].VehicleGasTypeName;
                    }

                }


                // cari diskon buat summary view estimasi
                if($scope.dataViewEstimasi.isCash == 1){ //kalo bayarnya cash
                    // $scope.DiscountTaskViewEstimasi = 0;
                    // $scope.DiscountPartsViewEstimasi = 0;
                }else{// kalo bayarnya asuransi
                    for(var i in $scope.Asuransi){
                        if($scope.Asuransi[i].InsuranceId == $scope.dataViewEstimasi.InsuranceId){
                            // $scope.DiscountTaskViewEstimasi = $scope.Asuransi[i].DiscountJasa;
                            // $scope.DiscountPartsViewEstimasi = $scope.Asuransi[i].DiscountPart;
                            $scope.dataViewEstimasi.InsuranceName = $scope.Asuransi[i].InsuranceName;
                        }
                    }
                }



                $scope.dataViewEstimasi.EstimationDate = $scope.tanggaljamViewEstimasi($scope.dataViewEstimasi.EstimationDate);
                $scope.dataViewEstimasi.PolisDateFrom = $scope.fixDate($scope.dataViewEstimasi.PolisDateFrom);
                $scope.dataViewEstimasi.PolisDateTo = $scope.fixDate($scope.dataViewEstimasi.PolisDateTo);

                

                //untuk dapetin Model, Tipe & Katashiki dari estimasi
                var tampunganVehicleTypeId = $scope.dataViewEstimasi.VehicleTypeId;
                for (var i = 0; i < $scope.modelData.length; i++) {
                    if ($scope.modelData[i].VehicleModelId == parseInt($scope.dataViewEstimasi.ModelCode)){
                        $scope.dataViewEstimasi.VehicleModelName = $scope.modelData[i].VehicleModelName
                        WO.getVehicleTypeById(parseInt($scope.dataViewEstimasi.ModelCode)).then(function(res) {
                            $scope.typeDataViewEstimasi = res.data.Result;
                            for (var z in $scope.typeDataViewEstimasi) {
                                if($scope.typeDataViewEstimasi[z].VehicleTypeId == tampunganVehicleTypeId){
                                    $scope.dataViewEstimasi.VehicleTypeDesc = $scope.typeDataViewEstimasi[z].Description;
                                }
                            }
                        })
                    }
                }
                


                //penyesuaian dari estimasi [Tasklist & Partlist]
                $scope.dataViewEstimasi.JobTask = $scope.dataViewEstimasi.AEstimationBP_TaskList_TT;
                var TempJobParts = [];


                for (var i = 0; i < $scope.dataViewEstimasi.JobTask.length; i++) {
                    for (var j = 0; j < $scope.dataViewEstimasi.JobTask[i].AEstimationBP_PartList.length; j++) {
                        $scope.dataViewEstimasi.JobTask[i].AEstimationBP_PartList[j].Qty = $scope.dataViewEstimasi.JobTask[i].AEstimationBP_PartList[j].qty;
                        TempJobParts.push($scope.dataViewEstimasi.JobTask[i].AEstimationBP_PartList[j]);
                    }
                    $scope.dataViewEstimasi.JobTask[i].JobParts = [];
                    for (var k = 0; k < TempJobParts.length; k++) {
                        if($scope.dataViewEstimasi.JobTask[i].EstTaskBPId == TempJobParts[k].EstTaskBPId){
                            $scope.dataViewEstimasi.JobTask[i].JobParts.push(TempJobParts[k]);
                        }
                    }
                }


                for(var m in $scope.dataViewEstimasi.JobTask){
                    for(var n in $scope.dataViewEstimasi.JobTask[m].JobParts){      
                        $scope.dataViewEstimasi.JobTask[m].JobParts[n].Satuan = null;
                        for(var o in $scope.unitData){
                            if($scope.unitData[o].MasterId == $scope.dataViewEstimasi.JobTask[m].JobParts[n].SatuanId){
                                $scope.dataViewEstimasi.JobTask[m].JobParts[n].Name = $scope.unitData[o].Name;
                            }
                        }
                    }
                }


                for(var m in $scope.dataViewEstimasi.JobTask){
                    $scope.dataViewEstimasi.JobTask[m].PaidBy = null;
                    delete $scope.dataViewEstimasi.JobTask[m].JobId;

                    
                    for(var x in $scope.paymentData ){
                        if($scope.paymentData[x].MasterId == $scope.dataViewEstimasi.JobTask[m].PaidById){
                            console.log('dapet nih task ===>',$scope.dataViewEstimasi.JobTask[m].PaidById);
                            $scope.dataViewEstimasi.JobTask[m].PaidBy = $scope.paymentData[x].Name;
                        }else{
                            console.log('tak dapet nih task ===>',$scope.dataViewEstimasi.JobTask[m].PaidById);
                        }
                    }
                    for(var n in $scope.dataViewEstimasi.JobTask[m].JobParts){
                        $scope.dataViewEstimasi.JobTask[m].JobParts[n].PaidBy = null;
                        // delete $scope.dataViewEstimasi.JobTask[m].JobParts[n].IsCustomDiscount;
                        delete $scope.dataViewEstimasi.JobTask[m].JobParts[n].JobId ;


                        if($scope.dataViewEstimasi.JobTask[m].JobParts[n].ETA == null){
                            $scope.dataViewEstimasi.JobTask[m].JobParts[n].isAvailable = 1;
                        }else{
                            $scope.dataViewEstimasi.JobTask[m].JobParts[n].isAvailable = 0;
                        }


                        
                        for(var z in $scope.paymentData ){
                            if($scope.paymentData[z].MasterId == $scope.dataViewEstimasi.JobTask[m].JobParts[n].PaidById){
                                console.log('dapett nih parts ===>  $scope.dataViewEstimasi.JobTask[m].JobParts[n].PaidById')
                                $scope.dataViewEstimasi.JobTask[m].JobParts[n].PaidBy = $scope.paymentData[z].Name;
                            }
                        }
                    }
                }


                // var tempDataPartViewEstimasi = {};
                // // Pengecekan DP, DP alay dari estimasi BP minggir lo
                // for(var m in $scope.dataViewEstimasi.JobTask){
                //     if($scope.dataViewEstimasi.JobTask[m].JobParts.length > 0){
                //         for(var n in $scope.dataViewEstimasi.JobTask[m].JobParts){


                //             var findtask = _.find(dpViewEstimasi, {'EstTaskBPId':$scope.dataViewEstimasi.JobTask[m].EstTaskBPId});
                //             var parts  = _.find(findtask.partsList, {'EstPartsId':$scope.dataViewEstimasi.JobTask[m].JobParts[n].EstPartsId});

                //             console.log('data findtask ====>',findtask)
                //             console.log('data parts ====>',parts)

                //             for(var i in dpViewEstimasi){
                //                 // if(dpViewEstimasi[i].partsList.length>0){
                //                     for(var j in dpViewEstimasi[i].partsList){
                //                         if(dpViewEstimasi[i].partsList[j].EstPartsId == $scope.dataViewEstimasi.JobTask[m].JobParts[n].EstPartsId){
                //                             console.log(' data ketemu | dpViewEstimasi[i].partsList[j]', dpViewEstimasi[i].partsList[j]);
                //                             $scope.dataViewEstimasi.JobTask[m].JobParts[n].DownPayment = dpViewEstimasi[i].partsList[j].DownPaymentRecalculate;
                //                         }else{
                //                             console.log('data tidak ketemu');
                //                         }
                //                     }
                //                 // }
                //             }

                //             console.log('Result pengecekann ',$scope.dataViewEstimasi.JobTask[m].JobParts[n])

                //         }
                //     }
                // }
                // // Pengecekan DP, DP alay dari estimasi BP minggir lo



                //penyesuaian dari estimasi [Tasklist & Partlist]
                $scope.dataForBslistViewEstimasi($scope.dataViewEstimasi);




            });

            
        }

        $scope.DataTempSelectedJob = [];
        $scope.onBeforeEditList = function(rows) {
            console.log('onBeforeEditList ====>', rows);
            $scope.DataTempSelectedJob = angular.copy(rows);
            $scope.copyAwalPembayaranJob = angular.copy(rows.PaidById)
            if (rows.PaidById == 30){
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }]; // kl pekerjaan twc / pwc ga blh pilih bahan

                if (rows.FlatRate == 0) {
                    $scope.FlatRateAvailable_new = false;
                } else {
                    $scope.FlatRateAvailable_new = true;
                }

                if (rows.Fare_w == null || rows.Fare_w == undefined) {
                    rows.Fare_w = rows.FlatRate * rows.Fare
                }
                $scope.listApi.addDetail(false, rows);
            } else {
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
            }

            $scope.cekJob = rows.TaskBPId;
        }

        $scope.selectModel = function(selected) {
            console.log("selectModel selected : ", selected);
            if(selected != null && selected != undefined){
                $scope.mData.VehicleModelName       = selected.VehicleModelName;
                $scope.mData.VehicleModelId         = selected.VehicleModelId;
                $scope.mDataDetail.VehicleModelName = selected.VehicleModelName;
                $scope.mDataDetail.VehicleModelId   = selected.VehicleModelId;
                $scope.mData.LicensePlate           = $scope.mDataDetail.LicensePlate;
                $scope.mData.VIN           = $scope.mDataDetail.VIN;
                $scope.mData.KatashikiCode    = undefined
                $scope.mDataDetail.KatashikiCodex    = undefined
                $scope.mDataDetail.VehicleTypeId      = undefined
                $scope.mData.VehicleTypeId    = undefined


                WO.getVehicleTypeById(selected.VehicleModelId).then(function(res) {
                    // $scope.typeData = res.data.Result
                    // console.log('typeData ===>', $scope.typeData);

                    $scope.katashikiData = _.uniq(res.data.Result, 'KatashikiCode');

                    for (var i=0; i<$scope.katashikiData.length; i++){
                        $scope.katashikiData[i].NewDescription = selected.VehicleModelName + ' - ' + $scope.katashikiData[i].KatashikiCode
                    }

                    console.log("$scope.katashikiData : ", $scope.katashikiData);

                });
                if(selected.VehicleModelId == $scope.mDataDetail.VehicleModelId){
                    console.log('ini mobilnya sama | ga usah apus Task |===>',selected.VehicleModelId + ' | ' + $scope.mDataDetail.VehicleModelId);
                } else {
                    console.log('ini mobilnya beda | Harusss apus Task |===>',selected.VehicleModelId + ' | ' + $scope.mDataDetail.VehicleModelId);
                    $scope.mDataDetail.KatashikiCode = undefined;
                    $scope.mDataDetail.Description   = undefined;
                    $scope.mDataDetail.VehicleTypeId = undefined;
                    $scope.mData.VehicleTypeId = undefined;

                }
               
                // $scope.clearOrderPekerjaan();
            }else{
                $scope.mDataDetail.KatashikiCode = undefined;
                $scope.mData.KatashikiCode    = undefined
                $scope.mDataDetail.Description   = undefined;
                $scope.mDataDetail.VehicleTypeId = undefined;
                $scope.mDataDetail.KatashikiCodex    = undefined
                $scope.mDataDetail.VehicleTypeId      = undefined
                $scope.mData.VehicleTypeId      = undefined

            }
        };

        $scope.selectKatashiki = function (selected) {
            $scope.mDataDetail.Description      = "";
            $scope.mDataDetail.VehicleTypeId      = "";


            if(selected != null && selected != undefined){
                AppointmentGrService.GetCVehicleTypeListByKatashiki(selected.KatashikiCode).then(function(res) {
                    // $scope.typeData = res.data.Result[0];
                    $scope.typeData = res.data.Result;

                    for (var z = 0; z < $scope.typeData.length; z++) {
                        $scope.typeData[z].NewDescription = $scope.typeData[z].Description + ' - ' + $scope.typeData[z].KatashikiCode + ' - ' + $scope.typeData[z].SuffixCode
                    }
                    $scope.mDataDetail.KatashikiCode    = selected.KatashikiCode;
                    $scope.mData.KatashikiCode    = selected.KatashikiCode;

    
                })
            } else {
                $scope.mDataDetail.KatashikiCodex    = "";
                $scope.mDataDetail.KatashikiCode    = "";
                $scope.mDataDetail.Description   = "";
                $scope.mDataDetail.VehicleTypeId = "";
                $scope.mData.VehicleTypeId = "";
                $scope.mData.KatashikiCode    = "";
            }
            
        }

        $scope.selectType = function(selected) {
            console.log("selectedtype", selected);
            if(selected != null || selected != undefined){
                $scope.mDataDetail.KatashikiCode = selected.KatashikiCode;
                $scope.mData.KatashikiCode    = selected.KatashikiCode;
                $scope.mDataDetail.Description   = selected.Description;
                $scope.mDataDetail.VehicleTypeId = selected.VehicleTypeId;
                $scope.mData.VehicleTypeId = selected.VehicleTypeId;

            }else{
                $scope.mDataDetail.KatashikiCode = undefined;
                $scope.mData.KatashikiCode = undefined;
                $scope.mDataDetail.Description   = undefined;
                $scope.mDataDetail.VehicleTypeId = undefined;
                $scope.mData.VehicleTypeId = undefined;

            }
        }

        $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi = function(item, x, y) {
            console.log("getAvailablePartsServiceManualFromEstimasiBPViewEstimasi", item);
            // console.log("getAvailablePartsServiceManualFromEstimasiBPViewEstimasi  ===>", JSON.stringify(item));            
            console.log("x : ", x);
            console.log("y : ", y);

            if (x > item.length - 1) {
                return item;
            }
            if (item[x].isDeleted !== 1) {
                console.log('kondisi 1');
                console.log('item[x].JobParts[y] ===>',item[x].JobParts[y]);
                console.log('item[0].JobParts[0] ===>',item[0].JobParts[0]);
                console.log('x ===>',x);
                console.log('y ===>',y);
                
                // console.log('item[x].JobParts[y].PartsId ===>',item[x].JobParts[y].PartsId);
                if (item[x].JobParts[y] !== undefined && item[x].JobParts[y].PartsId !== null) {
                    console.log('kondisi 1.1');
                    var itemTemp = item[x].JobParts[y];
                    // if (itemTemp.isDeleted == 0){
                        AppointmentBpService.getAvailableParts(itemTemp.PartsId,0,itemTemp.Qty).then(function(res) {
                            var tmpParts = res.data.Result[0];
                            console.log('getAvailablePartsServiceManualFromEstimasiBPViewEstimasi | getAvailableParts ===> ',tmpParts)
                            if (tmpParts !== null) {
                                // console.log("have data RetailPrice",item,tmpRes);
                                // item[x].JobParts[y].RetailPrice = tmpParts.RetailPrice;
                                item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                                item[x].JobParts[y].minimalDp = tmpParts.PriceDP;
                                item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                item[x].JobParts[y].nightmareIjal = item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty;

                                item[x].JobParts[y].DiskonForDisplay = item[x].JobParts[y].Discount + '%';
                                item[x].JobParts[y].subTotalForDisplay = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice - ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice)* item[x].JobParts[y].Discount /100);


                                // item.DiscountedPrice =  (normalPrice * item.Discount)/100;
                                if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                    item[x].JobParts[y].typeDiskon = -1;
                                    item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                    item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
                                    item[x].JobParts[y].typeDiskon = 0;
                                }
                                if (tmpParts.isAvailable == 0) {
                                    item[x].JobParts[y].Availbility = "Tidak Tersedia";
                                    if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                        item[x].JobParts[y].ETA = tmpParts.DefaultETA;
                                    } else {
                                        item[x].JobParts[y].ETA = tmpParts.DefaultETA;
                                    }

                                    if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                        item[x].JobParts[y].OrderType = 6;
                                    }
                                    else {
                                        item[x].JobParts[y].OrderType = 3;
                                    }
                                    
                                }else if (tmpParts.isAvailable == 1){
                                    item[x].JobParts[y].Availbility = "Tersedia";
                                }else if (tmpParts.isAvailable == 2){
                                    item[x].JobParts[y].Availbility = "Tersedia Sebagian";
                                    if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                        item[x].JobParts[y].OrderType = 6;
                                    }
                                }
                            } else {
                                console.log("haven't data RetailPrice");
                                item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                                item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                                item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                                item[x].JobParts[y].nightmareIjal = Math.round(item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty);

                                item[x].JobParts[y].DiskonForDisplay = item[x].JobParts[y].Discount + '%';
                                item[x].JobParts[y].subTotalForDisplay = item[x].JobParts[y].Qty * item[x].JobParts[y].Price - ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price)* item[x].JobParts[y].Discount /100);
                            }
                            if (item[x].JobParts[y].Part !== null && item[x].JobParts[y].Part !== undefined) {
                                item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                                delete item[x].JobParts[y].Part;
                            }
                            if (itemTemp.isDeleted == 0){
                                PartsViewEstimasi.push(item[x].JobParts[y]);
                                $scope.sumAllPriceViewEstimasi();
                            }
                            
                            // return item;
    
                            if (x <= (item.length - 1)) {
                                console.log('kondisi 2');
                                if (y >= (item[x].JobParts.length - 1)) {
                                    if (item[x].Fare !== undefined) {
                                        var sum = item[x].Fare;
                                        // var summ = item[x].FlatRate;
                                        // var summ = item[x].FlatRate ? item[x].FlatRate : 1;
                                        var summ = (item[x].FlatRate == null || item[x].FlatRate == undefined) ? 1 : item[x].FlatRate

                                        var summaryy = sum * summ;
                                        var discountedPrice = (summaryy * item[x].Discount) / 100;
                                        var normalPrice = summaryy
                                        summaryy = parseInt(sum);
                                    }
                                    if (item[x].JobType == null) {
                                        item[x].JobType = { Name: "" };
                                    }
                                    if (item[x].PaidBy == null) {
                                        item[x].PaidBy = { Name: "" }
                                    }                     
                                    if (item[x].Satuan == null) {
                                        item[x].Satuan = { Name: "" }
                                    }
                                    if (item[x].AdditionalTaskId == null) {
                                        item[x].isOpe = 0;
                                    } else {
                                        item[x].isOpe = 1;
                                    }
                                    if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                        item[x].typeDiskon = -1;
                                        item[x].DiscountedPrice = normalPrice;
                                    } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                        item[x].typeDiskon = 0;
                                        item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                    }
                                    gridTempViewEstimasi.push({
                                        Discount: item[x].Discount,
                                        ActualRate: item[x].ActualRate,
                                        JobTaskId: item[x].JobTaskId,
                                        JobTypeId: item[x].JobTypeId,
                                        BodyEstimationMinute: item[x].BodyEstimationMinute,
                                        FIEstimationMinute: item[x].FIEstimationMinute,
                                        PolishingEstimationMinute: item[x].PolishingEstimationMinute,
                                        PutyEstimationMinute: item[x].PutyEstimationMinute,
                                        ReassemblyEstimationMinute: item[x].ReassemblyEstimationMinute,
                                        SprayingEstimationMinute: item[x].SprayingEstimationMinute,
                                        SurfacerEstimationMinute: item[x].SurfacerEstimationMinute,
                                        // catName: item[x].JobType.Name,
                                        Fare: item[x].Fare == null ? item[x].TaskListBP.ServiceRate : item[x].Fare,
                                        FareMask: item[x].Fare == null ? item[x].TaskListBP.ServiceRate.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',') : item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                        // FareMask:item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                        Summary: summaryy,
                                        // PaidBy: item[x].PaidBy.Name, //JANCOKK nih bikin Pembayaran Task Ga masuk
                                        PaidBy: item[x].PaidBy,
                                        PaidById: item[x].PaidById,
                                        JobId: item[x].JobId,
                                        TaskId: item[x].JobTaskId,
                                        TaskName: item[x].TaskName,
                                        FlatRate: item[x].FlatRate,
                                        ProcessId: item[x].ProcessId,
                                        TFirst1: item[x].TFirst1,
                                        // TaskBPId: x,
                                        // TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : x,
                                        // UnitBy: item[x].Satuan.Name,
                                        UnitBy: $scope.checkSatuan(item[x].ProcessId),
                                        child: PartsViewEstimasi,
                                        index: "$$" + x,
                                        TaskListBPId: item[x].TaskListBP ? item[x].TaskListBP.TaskListBPId : null,
                                        TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : (x+1) * -1,
    
                                    });
                                    // dataJobTaskPlan.push({
                                    //   TaskBPId: item[x].JobTaskId,
                                    //   TaskName: item[x].TaskName,
                                    //   BodyEstimationMinute:item[x].BodyEstimationMinute,
                                    //   FIEstimationMinute:item[x].FIEstimationMinute,
                                    //   PolishingEstimationMinute:item[x].PolishingEstimationMinute,
                                    //   PutyEstimationMinute:item[x].PutyEstimationMinute,
                                    //   ReassemblyEstimationMinute:item[x].ReassemblyEstimationMinute,
                                    //   SprayingEstimationMinute:item[x].SprayingEstimationMinute,
                                    //   SurfacerEstimationMinute:item[x].SurfacerEstimationMinute
                                    // });
                                    // $scope.gridEstimation.data = dataJobTaskPlan;
                                    // $scope.calculateforwork();
                                    $scope.sumAllPriceViewEstimasi();
                                    PartsViewEstimasi.splice();
                                    PartsViewEstimasi = [];
                                    console.log("if 1 nilai x", x);
                                    $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(item, x + 1, 0);
    
                                } else {
                                    console.log('kondisi 3');
                                    $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(item, x, y + 1);
                                }
                            }
                        });

                    // }
                    
                } else if (item[x].JobParts[y] !== undefined && (item[x].JobParts[y].PartsId == undefined || item[x].JobParts[y].PartsId == null)) {
                    console.log('kondisi 4');

                    // if (item[x].JobParts[y].isDeleted == 0){
                        item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                        item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                        item[x].JobParts[y].DPRequest = item[x].JobParts[y].DPRequest * item[x].JobParts[y].Qty;
                        item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                        item[x].JobParts[y].nightmareIjal = Math.round(item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty);

                        item[x].JobParts[y].DiskonForDisplay = item[x].JobParts[y].Discount + '%';
                        item[x].JobParts[y].subTotalForDisplay = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice - ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice)* item[x].JobParts[y].Discount /100);

                        tmp = item[x].JobParts[y];
                        if (item[x].JobParts[y].Part !== null && item[x].JobParts[y].Part !== undefined) {
                            item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                            delete item[x].JobParts[y].Part;
                        }
                        if (item[x].JobParts[y].isDeleted == 0){
                            PartsViewEstimasi.push(item[x].JobParts[y]);
                            $scope.sumAllPriceViewEstimasi();
                        }

                        if (x <= (item.length - 1)) {
                            console.log('kondisi 5');
                            if (y >= (item[x].JobParts.length - 1)) {
                                if (item[x].Fare !== undefined) {
                                    var sum = item[x].Fare;
                                    // var summ = item[x].FlatRate;
                                    // var summ = item[x].FlatRate ? item[x].FlatRate : 1;
                                    var summ = (item[x].FlatRate == null || item[x].FlatRate == undefined) ? 1 : item[x].FlatRate

                                    var summaryy = sum * summ;
                                    var discountedPrice = (summaryy * item[x].Discount) / 100;
                                    var normalPrice = summaryy
                                    summaryy = parseInt(sum);
                                }
                                if (item[x].JobType == null) {
                                    item[x].JobType = { Name: "" };
                                }
                                if (item[x].PaidBy == null) {
                                    item[x].PaidBy = { Name: "" }
                                }
                                if (item[x].Satuan == null) {
                                    item[x].Satuan = { Name: "" }
                                }
                                if (item[x].AdditionalTaskId == null) {
                                    item[x].isOpe = 0;
                                } else {
                                    item[x].isOpe = 1;
                                }
                                if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                    item[x].typeDiskon = -1;
                                    item[x].DiscountedPrice = normalPrice;
                                } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                    item[x].typeDiskon = 0;
                                    item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                }
                                gridTempViewEstimasi.push({
                                    Discount: item[x].Discount,
                                    ActualRate: item[x].ActualRate,
                                    JobTaskId: item[x].JobTaskId,
                                    JobTypeId: item[x].JobTypeId,
                                    BodyEstimationMinute: item[x].BodyEstimationMinute,
                                    FIEstimationMinute: item[x].FIEstimationMinute,
                                    PolishingEstimationMinute: item[x].PolishingEstimationMinute,
                                    PutyEstimationMinute: item[x].PutyEstimationMinute,
                                    ReassemblyEstimationMinute: item[x].ReassemblyEstimationMinute,
                                    SprayingEstimationMinute: item[x].SprayingEstimationMinute,
                                    SurfacerEstimationMinute: item[x].SurfacerEstimationMinute,
                                    // catName: item[x].JobType.Name,
                                    // Fare: item[x].Fare,
                                    // FareMask:item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                    Fare: item[x].Fare == null ? item[x].TaskListBP.ServiceRate : item[x].Fare,
                                    FareMask: item[x].Fare == null ? item[x].TaskListBP.ServiceRate.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',') : item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                    Summary: summaryy,
                                    PaidBy: item[x].PaidBy,
                                    PaidById: item[x].PaidById,

                                    JobId: item[x].JobId,
                                    TaskId: item[x].JobTaskId,
                                    TaskName: item[x].TaskName,
                                    FlatRate: item[x].FlatRate,
                                    ProcessId: item[x].ProcessId,
                                    TFirst1: item[x].TFirst1,
                                    // TaskBPId: x,
                                    // TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : x,
                                    // UnitBy: item[x].Satuan.Name,
                                    UnitBy: $scope.checkSatuan(item[x].ProcessId),
                                    // PaidBy: tmpJob[i].PaidBy.Name,
                                    child: PartsViewEstimasi,
                                    index: "$$" + x,
                                    TaskListBPId: item[x].TaskListBP ? item[x].TaskListBP.TaskListBPId : null,
                                    TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : (x+1) * -1,

                                });
                                // dataJobTaskPlan.push({
                                //       TaskBPId: item[x].JobTaskId,
                                //       TaskName: item[x].TaskName,
                                //       BodyEstimationMinute:item[x].BodyEstimationMinute,
                                //       FIEstimationMinute:item[x].FIEstimationMinute,
                                //       PolishingEstimationMinute:item[x].PolishingEstimationMinute,
                                //       PutyEstimationMinute:item[x].PutyEstimationMinute,
                                //       ReassemblyEstimationMinute:item[x].ReassemblyEstimationMinute,
                                //       SprayingEstimationMinute:item[x].SprayingEstimationMinute,
                                //       SurfacerEstimationMinute:item[x].SurfacerEstimationMinute
                                // });
                                // $scope.gridEstimation.data = dataJobTaskPlan;
                                // $scope.calculateforwork();
                                $scope.sumAllPriceViewEstimasi();
                                PartsViewEstimasi.splice();
                                PartsViewEstimasi = [];
                                $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(item, x + 1, 0);
                                console.log("if 2 nilai x", x);
                            } else {
                                console.log('kondisi 6');
                                $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(item, x, y + 1);
                            }
                        }

                    // }
                    
                } else if (item[x].JobParts.length == 0) {
                    if (x <= (item.length - 1)) {
                        console.log('kondisi 7');
                        if (y >= (item[x].JobParts.length - 1)) {
                            if (item[x].Fare !== undefined) {
                                var sum = item[x].Fare;
                                // var summ = item[x].FlatRate;
                                // var summ = item[x].FlatRate ? item[x].FlatRate : 1;
                                var summ = (item[x].FlatRate == null || item[x].FlatRate == undefined) ? 1 : item[x].FlatRate

                                var summaryy = sum * summ;
                                var discountedPrice = (summaryy * item[x].Discount) / 100;
                                var normalPrice = summaryy;
                                summaryy = parseInt(sum);
                            };
                            if (item[x].JobType == null) {
                                item[x].JobType = { Name: "" };
                            };
                            if (item[x].PaidBy == null) {
                                item[x].PaidBy = { Name: "" };
                            };
                            if (item[x].Satuan == null) {
                                item[x].Satuan = { Name: "" };
                            };
                            if (item[x].AdditionalTaskId == null) {
                                item[x].isOpe = 0;
                            } else {
                                item[x].isOpe = 1;
                            };
                            if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                item[x].typeDiskon = -1;
                                item[x].DiscountedPrice = normalPrice;
                            } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                item[x].typeDiskon = 0;
                                item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                            };
                            gridTempViewEstimasi.push({
                                Discount: item[x].Discount,
                                ActualRate: item[x].ActualRate,
                                JobTaskId: item[x].JobTaskId,
                                JobTypeId: item[x].JobTypeId,
                                BodyEstimationMinute: item[x].BodyEstimationMinute,
                                FIEstimationMinute: item[x].FIEstimationMinute,
                                PolishingEstimationMinute: item[x].PolishingEstimationMinute,
                                PutyEstimationMinute: item[x].PutyEstimationMinute,
                                ReassemblyEstimationMinute: item[x].ReassemblyEstimationMinute,
                                SprayingEstimationMinute: item[x].SprayingEstimationMinute,
                                SurfacerEstimationMinute: item[x].SurfacerEstimationMinute,
                                // catName: item[x].JobType.Name,
                                // Fare: item[x].Fare,
                                // FareMask:item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                Fare: item[x].Fare == null ? item[x].TaskListBP.ServiceRate : item[x].Fare,
                                FareMask: item[x].Fare == null ? item[x].TaskListBP.ServiceRate.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',') : item[x].Fare.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ','),
                                Summary: summaryy,
                                PaidBy: item[x].PaidBy,
                                PaidById: item[x].PaidById,

                                JobId: item[x].JobId,
                                TaskId: item[x].JobTaskId,
                                TaskName: item[x].TaskName,
                                FlatRate: item[x].FlatRate,
                                ProcessId: item[x].ProcessId,
                                TFirst1: item[x].TFirst1,
                                // TaskBPId: x,
                                // TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : x,
                                // UnitBy: item[x].Satuan.Name
                                UnitBy: $scope.checkSatuan(item[x].ProcessId),
                                // PaidBy: tmpJob[i].PaidBy.Name,
                                child: PartsViewEstimasi,
                                index: "$$" + x,
                                TaskListBPId: item[x].TaskListBP ? item[x].TaskListBP.TaskListBPId : null,
                                TaskBPId: item[x].TaskBPId ? item[x].TaskBPId : (x+1) * -1,

                            });
                            // dataJobTaskPlan.push({
                            //       TaskBPId: item[x].JobTaskId,
                            //       TaskName: item[x].TaskName,
                            //       BodyEstimationMinute:item[x].BodyEstimationMinute,
                            //       FIEstimationMinute:item[x].FIEstimationMinute,
                            //       PolishingEstimationMinute:item[x].PolishingEstimationMinute,
                            //       PutyEstimationMinute:item[x].PutyEstimationMinute,
                            //       ReassemblyEstimationMinute:item[x].ReassemblyEstimationMinute,
                            //       SprayingEstimationMinute:item[x].SprayingEstimationMinute,
                            //       SurfacerEstimationMinute:item[x].SurfacerEstimationMinute
                            // });
                            // $scope.gridEstimation.data = dataJobTaskPlan;
                            // $scope.calculateforwork();
                            $scope.sumAllPriceViewEstimasi();
                            PartsViewEstimasi.splice();
                            PartsViewEstimasi = [];
                            $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(item, x + 1, 0);
                        } else {
                            console.log('kondisi 8');
                            $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(item, x, y + 1);
                        }
                    }
                }

                if (x > item.length - 1) {
                    return item;
                }
            } else {
                console.log('kondisi 9');
                $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(item, x + 1, 0);
            }
        };


        $scope.gridWorkViewEstimasi = [];
        $scope.sumAllPriceViewEstimasi = function() {

            var NumberParent = 1;
            var NumberChild = 1;
            for (i in $scope.gridWorkViewEstimasi){
                $scope.gridWorkViewEstimasi[i].NumberParent = NumberParent+parseInt(i);
                for (j in $scope.gridWorkViewEstimasi[i].child){
                    $scope.gridWorkViewEstimasi[i].child[j].NumberChild = $scope.gridWorkViewEstimasi[i].NumberParent + "."  + parseInt(++j);
                }
            }


            console.log('sumAllPrice gridWorkViewEstimasi ===>',$scope.gridWorkViewEstimasi);
            var totalW = 0;
            for (var i = 0; i < $scope.gridWorkViewEstimasi.length; i++) {
                if ($scope.gridWorkViewEstimasi[i].Summary !== undefined) {
                    if ($scope.gridWorkViewEstimasi[i].Summary == "") {
                        $scope.gridWorkViewEstimasi[i].Summary = $scope.gridWorkViewEstimasi[i].Fare;
                    }
                    if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                        totalW += 0;
                    }else{
                        // totalW += Math.round($scope.gridWorkViewEstimasi[i].Summary / 1.1);
                        totalW += ASPricingEngine.calculate({
                                    InputPrice: $scope.gridWorkViewEstimasi[i].Summary,
                                    // Discount: 5,
                                    // Qty: 2,
                                    Tipe: 2, 
                                    PPNPercentage: PPNPerc,
                                });
                    }
                }
            }


            // $scope.totalWorkViewEstimasi = totalW ;
            // var totalM = 0;
            // for (var i = 0; i < $scope.gridWorkViewEstimasi.length; i++) {
            //     if ($scope.gridWorkViewEstimasi[i].child !== undefined) {
            //         for (var j = 0; j < $scope.gridWorkViewEstimasi[i].child.length; j++) {
            //             if ($scope.gridWorkViewEstimasi[i].child[j].subTotal !== undefined) {

            //                 // totalM += $scope.gridWorkViewEstimasi[i].child[j].subTotal;
            //                 totalM += $scope.gridWorkViewEstimasi[i].child[j].subTotalForDisplay;
            //                     console.log("$scope.gridWorkViewEstimasi[i].child.Price totalM ===>", $scope.gridWorkViewEstimasi[i].child[j].subTotal);
                            
            //             }
            //         }
            //     }
            // }
            // $scope.totalMaterialViewEstimasi = parseInt(totalM / (1.1));  //request pak dodi dibagi 1.1 13/02/2019

            var totalDp = 0; 
            for (var i = 0; i < $scope.gridWorkViewEstimasi.length; i++) {
                if ($scope.gridWorkViewEstimasi[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWorkViewEstimasi[i].child.length; j++) {
                        if ($scope.gridWorkViewEstimasi[i].child[j].DownPayment !== null) {
                            totalDp += $scope.gridWorkViewEstimasi[i].child[j].DownPayment;
                            // totalDp += $scope.gridWorkViewEstimasi[i].child[j].DownPayment * 1.1;
                            console.log("$scope.gridWorkViewEstimasi[i].totalDp ===>", $scope.gridWorkViewEstimasi[i].child[j].DownPayment);
                        }
                    }
                }
            }
            $scope.totalDpViewEstimasi = totalDp;

            



            
            var totalActualRate = 0;
            for (var i = 0; i < $scope.gridWorkViewEstimasi.length; i++) {
                if ($scope.gridWorkViewEstimasi[i].ActualRate !== undefined) {
                    totalActualRate += $scope.gridWorkViewEstimasi[i].ActualRate;
                }

            }
            $scope.tmpActRate = totalActualRate;



            var totalMaterial_byKevin = 0;
            for (var i = 0; i < $scope.gridWorkViewEstimasi.length; i++) {
                if ($scope.gridWorkViewEstimasi[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWorkViewEstimasi[i].child.length; j++) {
                        if($scope.gridWorkViewEstimasi[i].child[j].PaidById == 2277 || $scope.gridWorkViewEstimasi[i].child[j].PaidById == 30){
                            totalMaterial_byKevin += 0;
                        }else{
                            // totalMaterial_byKevin += Math.round($scope.gridWorkViewEstimasi[i].child[j].RetailPrice/1.1) * $scope.gridWorkViewEstimasi[i].child[j].Qty;
                            totalMaterial_byKevin += ASPricingEngine.calculate({
                                                        InputPrice: $scope.gridWorkViewEstimasi[i].child[j].RetailPrice,
                                                        // Discount: 5,
                                                        Qty: $scope.gridWorkViewEstimasi[i].child[j].Qty,
                                                        Tipe: 102, 
                                                        PPNPercentage: PPNPerc,
                                                    }); 
                        }
                    }
                }
            };
            
            var totalMD_byKevin = 0;
            for (var i = 0; i < $scope.gridWorkViewEstimasi.length; i++) {
                if ($scope.gridWorkViewEstimasi[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWorkViewEstimasi[i].child.length; j++) {
                        if($scope.gridWorkViewEstimasi[i].child[j].PaidById == 2277 || $scope.gridWorkViewEstimasi[i].child[j].PaidById == 30){//req by pak eko bahan tetap dapet diskon 08-juli-2020
                                totalMD_byKevin += 0;
                        }else{
                            // totalMD_byKevin +=  Math.round(Math.round($scope.gridWorkViewEstimasi[i].child[j].RetailPrice /1.1 ) * ($scope.gridWorkViewEstimasi[i].child[j].Discount/100)) *  $scope.gridWorkViewEstimasi[i].child[j].Qty;
                            totalMD_byKevin +=  ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWorkViewEstimasi[i].child[j].RetailPrice,
                                                    Discount: $scope.gridWorkViewEstimasi[i].child[j].Discount,
                                                    Qty: $scope.gridWorkViewEstimasi[i].child[j].Qty,
                                                    Tipe: 112, 
                                                    PPNPercentage: PPNPerc,
                                                }); 
                            
                        }
                    }
                }
            };

            var totalWD_byKevin = 0;
            for (var i = 0; i < $scope.gridWorkViewEstimasi.length; i++) {
                if($scope.gridWorkViewEstimasi[i].PaidById == 2277 || $scope.gridWorkViewEstimasi[i].PaidById == 30){
                    totalWD_byKevin += 0;
                }else{
                    // totalWD_byKevin +=  Math.round(Math.round($scope.gridWorkViewEstimasi[i].Fare /1.1 ) * ($scope.gridWorkViewEstimasi[i].Discount/100));
                    totalWD_byKevin +=  ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWorkViewEstimasi[i].Fare,
                                            Discount: $scope.gridWorkViewEstimasi[i].Discount,
                                            // Qty: 2,
                                            Tipe: 12, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                }
            };


            $scope.totalMaterialViewEstimasi           = totalMaterial_byKevin;
            $scope.totalMaterialDiscountedViewEstimasi = totalMD_byKevin;
            $scope.totalWorkDiscountedViewEstimasi     = totalWD_byKevin;
            $scope.subTotalMaterialSummaryViewEstimasi = $scope.totalMaterialViewEstimasi - $scope.totalMaterialDiscountedViewEstimasi
            $scope.subTotalWorkSummaryViewEstimasi     = $scope.totalWorkViewEstimasi - $scope.totalWorkDiscountedViewEstimasi;
            // $scope.totalPPNViewEstimasi                = Math.floor(($scope.subTotalMaterialSummaryViewEstimasi + $scope.subTotalWorkSummaryViewEstimasi) * 0.1);  
            $scope.totalPPNViewEstimasi                = ASPricingEngine.calculate({
                                                            InputPrice: (($scope.subTotalMaterialSummaryViewEstimasi + $scope.subTotalWorkSummaryViewEstimasi) * (1+PPNPerc/100)),
                                                            // Discount: 0,
                                                            // Qty: 0,
                                                            Tipe: 33, 
                                                            PPNPercentage: PPNPerc,
                                                        });     
            $scope.totalEstimasiViewEstimasi           = $scope.totalPPNViewEstimasi + $scope.subTotalMaterialSummaryViewEstimasi + $scope.subTotalWorkSummaryViewEstimasi;


            
          
            console.log("$scope.gridWorkViewEstimasi", $scope.gridWorkViewEstimasi);
            console.log("gridWorkViewEstimasi", $scope.gridWorkViewEstimasi);


            //nentuin diskon task & parts
            jejeranDiskonTaskViewEstimasi = [];
            jejeranDiskonPartsViewEstimasi = [];
            for(var i in $scope.gridWorkViewEstimasi){
                // if($scope.gridWorkViewEstimasi.PaidById == 30|| $scope.gridWorkViewEstimasi.PaidById == 31 || $scope.gridWorkViewEstimasi.PaidById == 32 || $scope.gridWorkViewEstimasi.PaidById == 2277){
                if($scope.gridWorkViewEstimasi.PaidById == 30|| $scope.gridWorkViewEstimasi.PaidById == 2277){
                    jejeranDiskonTaskViewEstimasi.push(0);
                }else{
                    jejeranDiskonTaskViewEstimasi.push($scope.gridWorkViewEstimasi[i].Discount);
                }
                if($scope.gridWorkViewEstimasi[i].child.length > 0 ){
                    for(var j in $scope.gridWorkViewEstimasi[i].child){
                        // if($scope.gridWorkViewEstimasi[i].child[j].PaidById == 30|| $scope.gridWorkViewEstimasi[i].child[j].PaidById == 31 || $scope.gridWorkViewEstimasi[i].child[j].PaidById == 32 || $scope.gridWorkViewEstimasi[i].child[j].PaidById == 2277){
                        if($scope.gridWorkViewEstimasi[i].child[j].PaidById == 30|| $scope.gridWorkViewEstimasi[i].child[j].PaidById == 2277){
                            jejeranDiskonPartsViewEstimasi.push(0);
                        }else{
                            jejeranDiskonPartsViewEstimasi.push($scope.gridWorkViewEstimasi[i].child[j].Discount);
                        }

                        
                    }
                }
            }
            $scope.displayDiscountAlgorithmViewEstimasi();
            //nentuin diskon task & parts
        };


        var PartsViewEstimasi = [];
        var dataJobTaskPlanViewEstimasi = [];
        $scope.dataForBslistViewEstimasi = function(data) {
            console.log('dataForBslistViewEstimasi ===>',data);

            gridTempViewEstimasi = []
            PartsViewEstimasi.splice();
            PartsViewEstimasi = [];

            data.JobTask = data.AEstimationBP_TaskList_TT;
            if (data.JobTask.length > 0) {
                var tmpJob = data.JobTask;
                console.log("tmpJob dataForBslistViewEstimasi ===>", tmpJob);
           
                for (var i = 0; i < tmpJob.length; i++) {
                    for (var j = 0; j < tmpJob[i].JobParts.length; j++) {
                        tmpJob[i].subTotal = tmpJob[i].Qty * tmpJob[i].RetailPrice;
                    }
                    if (tmpJob[i].isDeleted == 0) {
                        dataJobTaskPlanViewEstimasi.push({
                            TaskBPId: tmpJob[i].TaskBPId ? tmpJob[i].TaskBPId : i,
                            TaskName: tmpJob[i].TaskName,
                            BodyEstimationMinute: tmpJob[i].BodyEstimationMinute,
                            FIEstimationMinute: tmpJob[i].FIEstimationMinute,
                            PolishingEstimationMinute: tmpJob[i].PolishingEstimationMinute,
                            PuttyEstimationMinute: tmpJob[i].PutyEstimationMinute,                            
                            ReassemblyEstimationMinute: tmpJob[i].ReassemblyEstimationMinute,
                            SprayingEstimationMinute: tmpJob[i].SprayingEstimationMinute,
                            SurfacerEstimationMinute: tmpJob[i].SurfacerEstimationMinute,
                            Total: tmpJob[i].BodyEstimationMinute+tmpJob[i].FIEstimationMinute+tmpJob[i].PolishingEstimationMinute+tmpJob[i].PutyEstimationMinute+tmpJob[i].ReassemblyEstimationMinute+tmpJob[i].SprayingEstimationMinute+tmpJob[i].SurfacerEstimationMinute
                        });
                    }
                }
                $scope.gridEstimationViewEstimasi.data = dataJobTaskPlanViewEstimasi;
                $scope.getAvailablePartsServiceManualFromEstimasiBPViewEstimasi(tmpJob, 0, 0);
                $scope.gridWorkViewEstimasi = gridTempViewEstimasi;
                $scope.gridOriginalViewEstimasi = gridTempViewEstimasi;
            }
            $scope.sumAllPriceViewEstimasi();
            console.log("$scope.gridWorkViewEstimasi", $scope.gridWorkViewEstimasi);
            console.log("$scope.JobRequest", $scope.JobRequest);
            console.log("$scope.JobComplaint", $scope.JobComplaint);
            console.log("$scope.gridEstimationViewEstimasi.data", $scope.gridEstimationViewEstimasi.data);
        };

        $scope.gridEstimationViewEstimasi = {
            enableCellEditOnFocus: true,
            showColumnFooter: true,

            columnDefs: [{
                'name': 'Order Pekerjaan',
                'field': 'TaskName',
                enableCellEdit: false,
                footerCellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;">Time Per Stall</div>'
            }, {
                'name': 'Body',
                'field': 'BodyEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Putty',
                'field': 'PuttyEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Surfacer',
                'field': 'SurfacerEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Painting',
                'field': 'SprayingEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Polishing',
                'field': 'PolishingEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Re-Assembly',
                displayName: "Re-Assembly",
                'field': 'ReassemblyEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Final Inspection',
                'field': 'FIEstimationMinute',
                editableCellTemplate: '<input type="number" min="0" max="99999" ui-grid-editor ng-model="MODEL_COL_FIELD">',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                aggregationHideLabel: true

            }, {
                'name': 'Total',
                'field': 'Total',
                enableCellEdit: false,
                cellTemplate: '<div class="ui-grid-cell-contents" ng-model="MODEL_COL_FIELD">{{row.entity.BodyEstimationMinute + row.entity.PuttyEstimationMinute + row.entity.SurfacerEstimationMinute + row.entity.SprayingEstimationMinute +  row.entity.PolishingEstimationMinute + row.entity.ReassemblyEstimationMinute + row.entity.FIEstimationMinute }}</div>',
                aggregationType: uiGridConstants.aggregationTypes.sum,
                footerCellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;">{{grid.appScope.gridEstimationApi.grid.columns[8].getAggregationValue()}}</div>',
                aggregationHideLabel: true

            }],
            onRegisterApi: function(gridApi) {
                $scope.gridEstimationApi = gridApi;
                $scope.gridEstimationApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                        console.log("afterCellEdit rowEntity", rowEntity);
                        console.log("afterCellEdit colDef", colDef);
                        console.log("afterCellEdit newValue", newValue);
                        console.log("afterCellEdit oldValue", oldValue);
                        if (newValue == null || newValue == undefined) {
                            newValue = 0;
                            return newValue;
                            $scope.calculateforwork();
                        }
                        $scope.calculateforwork();
                    })
                    // $scope.gridEstimationApi.selection.on.rowSelectionChanged($scope, function (row) {
                    //     $scope.selectedRow = row.entity;
                    // });
                    // if ($scope.gridEstimationApi.selection.selectRow) {
                    //     $scope.gridEstimationApi.selection.selectRow($scope.gridEstimation.data[0]);
                    // }
            }
        };
        $scope.dissallowSemicolon = function(event){
            var patternRegex = /[\x3B]/i;
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        }

        $scope.ubahPartsCode = function(data){
            console.log('ubur ubur',data)
            // kata pa ari kl part yang ga pake partscode (manual), ga blh di kasi diskon pas release wo nya
            // hrs isi dr listodo dl partscode nya, baru kasi diskon di wo list
            if (data.PartsCode != null && data.PartsCode != undefined){
                if (data.PartsCode.length < 4){
                    $scope.ldModel.DiscountTypeId = -1;
                    $scope.ldModel.DiscountTypeListId = null;
                    $scope.ldModel.Discount = 0;
                    $scope.ldModel.PartsId = null; // kl mau harga nya enable waktu dia ubah partscode

                    // ========================= hilangkan tipe diskon material jika ganti part, hrs click "Cek" dl ====================== start
                    // for (var p=0; p<$scope.diskonData.length; p++){
                    //     if ($scope.diskonData[p].MasterId == 3){
                    //         $scope.diskonData.splice(p,1)
                    //     }
                    // }
                    // ========================= hilangkan tipe diskon material jika ganti part, hrs click "Cek" dl ====================== end
                }
            }
        }

        $scope.checkNameParts = function(data, model) {
            if ($scope.ldModel.IsOPB == null && data != null) {
                $scope.ldModel.RetailPrice = 0;
            } else {
                $scope.ldModel.RetailPrice = "";
            }

            if (model.MaterialTypeId == 1 && (model.PartsCode == null || model.PartsCode == undefined || model.PartsCode == '')) {
                model.OrderType = 3
                model.Type = 3

            }

        }



        $scope.listButtonSettingsAddress = { 
            view  : { enable: false}, 
            new   : { enable: false}, 
            edit  : { enable: false}, 
            delete: { enable: false}, 
            selectall: { enable: false} 
        };

        var gridActionLihatEstimasi = '<div class="ui-grid-cell-contents">' +
            // '<a href="#" uib-tooltip="Buat" tooltip-placement="bottom" ng-click="grid.appScope.actView(row.entity)" style="">Appointment</a>'+
            '<a href="#" ng-click="grid.appScope.lihatEstimasi(row.entity)"  uib-tooltip="Lihat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-id-card-o" style="padding:4px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>'

        $scope.listEstimasiYangBelumDigunakan = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            isRowSelectable: function(row) {
                if(row.entity.isUsedInAppointment === true) return false; // ini ga bisa di select
                return true; //yang lainnya bisa
            },
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15, 
            columnDefs: [
                { displayName: 'No. Estimasi', name: 'No. Estimasi', field: 'EstimationNo', visible: true, cellTemplate: '<p style="padding-top:7px">&nbsp; {{row.entity.EstimationNo}}<bsreqlabel ng-if="(row.entity.isUsedInAppointment == true && row.entity.UsedBy == 1) || (row.entity.isUsedInAppointment == true && row.entity.UsedBy == null)"></bsreqlabel><span style="color:red" ng-if="row.entity.UsedBy == 2">**</span></p>'},
                { displayName: 'Tanggal Estimasi', name: 'EstimationDate', field: 'EstimationDate', cellFilter: 'date:\"dd-MM-yyyy\"' },        
                { displayName: 'Pembayaran', name: 'Pembayaran', field: 'isCash', visible: true, cellTemplate:'<p ng-if="row.entity.isCash==1" style="padding-top:7px" >&nbsp; Cash</p><p ng-if="row.entity.isCash==0" style="padding-top:7px" >&nbsp; Insurance</p>'},
                { displayName: 'Pembuat', name: 'Pembuat', field: 'LastModifiedEmployeeName', visible: true, },
                { displayName: 'Status', name: 'Status', field: 'Status', width: 80,visible: true, cellTemplate:'<p ng-if="row.entity.Status==1" style="padding-top:7px" >&nbsp; Close</p><p ng-if="row.entity.Status==0" style="padding-top:7px" >&nbsp; Open</p><p ng-if="row.entity.Status==2" style="padding-top:7px" >&nbsp; Draft</p>'},
                { 
                    name: 'Action',
                    width: 120,
                    pinnedRight: true,
                    cellTemplate: gridActionLihatEstimasi 
                }, //jancok

            ],

            onRegisterApi: function(gridApi) {
                $scope.listEstimasiYangBelumDigunakan = gridApi;

                $scope.listEstimasiYangBelumDigunakan.selection.on.rowSelectionChanged($scope,function(row) {
                    $scope.selectedRows = $scope.listEstimasiYangBelumDigunakan.selection.getSelectedRows();
                    // console.log("selected=>",$scope.selectedRows);
                    if($scope.onSelectRows){
                        $scope.onSelectRowsEstimasiBP($scope.selectedRows);
                    }
                });
                $scope.listEstimasiYangBelumDigunakan.selection.on.rowSelectionChangedBatch($scope,function(row) {
                    $scope.selectedRows = $scope.listEstimasiYangBelumDigunakan.selection.getSelectedRows();
                    if($scope.onSelectRows){
                        $scope.onSelectRowsEstimasiBP($scope.selectedRows);
                    }
                });

            }
        };


        $scope.onSelectRowsEstimasiBP = function(rows){
            console.log("onSelectRowsEstimasiBP=>",rows);
            $scope.selectedCekEstimasi  = rows;

            if($scope.selectedCekEstimasi.length > 0){ //kalo ada isinya
              $scope.disableBtnPilihEstimasi = false;
            }else{ //kalo kosong
              $scope.disableBtnPilihEstimasi = true; 
            }
        }

        var tampungparts = [];
        var tampungtask = [];
        var taskAntiDuplicate = [];
        var partsAntiDuplicate = [];

        $scope.removeDuplicateTask = function(data){
            var dups = [];
            var arr = data.filter(function(el) {
                if (dups.indexOf(el.TaskBPId) == -1) {
                  dups.push(el.TaskBPId);              
                  return true;
                }else{
                  return false;
              
                }
              });
            return arr
        }

        $scope.removeDuplicateParts = function(data){
            var dups = [];
            var arr = data.filter(function(el) {
                if (dups.indexOf(el.PartsId) == -1) {
                  dups.push(el.PartsId);              
                  return true;
                }else{
                  return false;
              
                }
              });
            return arr
        }

        $scope.collapseGridworkAndEstimasiTaskList = function(gridWork, mDataByEstimasi){
            console.log('already data from bslist     ====>',gridWork);
            console.log('new data from estimasi data  ====>',mDataByEstimasi);

            countEstimasiMasuk += 1;


            for(var i in mDataByEstimasi.JobTask){
                mDataByEstimasi.JobTask[i].EstimationBPId = mDataByEstimasi.EstimationBPId;
                if(mDataByEstimasi.JobTask[i].JobParts.length > 0){
                    for(var j in mDataByEstimasi.JobTask[i].JobParts){
                        console.log('convertDPpercentToNominal | Price       ===>',mDataByEstimasi.JobTask[i].JobParts[j].Price);
                        console.log('convertDPpercentToNominal | qty         ===>',mDataByEstimasi.JobTask[i].JobParts[j].qty );
                        console.log('convertDPpercentToNominal | Qty         ===>',mDataByEstimasi.JobTask[i].JobParts[j].Qty );
                        console.log('convertDPpercentToNominal | Discount    ===>',mDataByEstimasi.JobTask[i].JobParts[j].Discount);
                        console.log('convertDPpercentToNominal | DownPayment ===>',mDataByEstimasi.JobTask[i].JobParts[j].DownPayment );
                        console.log('convertDPpercentToNominal | DPestimasi  ===>',mDataByEstimasi.JobTask[i].JobParts[j].DPestimasi  );
                        mDataByEstimasi.JobTask[i].JobParts[j].EstimationBPId = mDataByEstimasi.JobTask[i].EstimationBPId;
                        mDataByEstimasi.JobTask[i].JobParts[j].TaskBPId = mDataByEstimasi.JobTask[i].TaskBPId;
                        tampungparts.push(mDataByEstimasi.JobTask[i].JobParts[j]);
                    }
                }
            }



            

            for(var i in gridWork){
                gridWork[i].isDeleted = 0;
                if(typeof gridWork[i].PutyEstimationMinute){
                    gridWork[i].PutyEstimationMinute = gridWork[i].PuttyEstimationMinute;
                }
                for(var j in gridWork[i].JobParts){
                    gridWork[i].JobParts[j].TaskBPId =  gridWork[i].TaskBPId;
                    gridWork[i].JobParts[j].Price = gridWork[i].JobParts[j].RetailPrice;    
                    gridWork[i].JobParts[j].isDeleted = 0;
                    if(typeof gridWork[i].EstimationBPId != undefined){
                        gridWork[i].JobParts[j].EstimationBPId = gridWork[i].EstimationBPId;
                    }
                    tampungparts.push(gridWork[i].JobParts[j]);
                }
            }

            for(var i in mDataByEstimasi.JobTask){
                delete mDataByEstimasi.JobTask[i].JobParts
                delete mDataByEstimasi.JobTask[i].AEstimationBP_PartList
                tampungtask.push(mDataByEstimasi.JobTask[i]);
            }

            for(var i in gridWork){
                delete gridWork[i].JobParts;
                delete gridWork[i].AEstimationBP_PartList
                tampungtask.push(gridWork[i]);
            }

            partsAntiDuplicate = $scope.removeDuplicateParts(tampungparts);
            taskAntiDuplicate = $scope.removeDuplicateTask(tampungtask);

            for(var x in partsAntiDuplicate){
                for (var y in tampungparts){
                    if( partsAntiDuplicate[x].PartsId == tampungparts[y].PartsId && partsAntiDuplicate[x].TaskBPId == tampungparts[y].TaskBPId && partsAntiDuplicate[x].EstimationBPId != tampungparts[y].EstimationBPId ){
                        partsAntiDuplicate[x].Qty += tampungparts[y].Qty;
                        partsAntiDuplicate[x].qty += tampungparts[y].qty;
                    }
                }
            }

            for (var x in taskAntiDuplicate){
                taskAntiDuplicate[x].JobParts = [];
                for (var y in partsAntiDuplicate){

                    if(typeof partsAntiDuplicate[y].EstTaskBPId !== undefined){
                        //data dari estimasi
                        if(taskAntiDuplicate[x].EstTaskBPId == partsAntiDuplicate[y].EstTaskBPId && taskAntiDuplicate[x].TaskBPId == partsAntiDuplicate[y].TaskBPId){
                            taskAntiDuplicate[x].JobParts.push(partsAntiDuplicate[y]);
                        }else{
                            if(taskAntiDuplicate[x].TaskBPId == partsAntiDuplicate[y].TaskBPId){
                                taskAntiDuplicate[x].JobParts.push(partsAntiDuplicate[y]);
                            }
                        }
                    }else{
                        //data lama
                        if( taskAntiDuplicate[x].JobTaskId == partsAntiDuplicate[y].JobTaskId && taskAntiDuplicate[x].TaskBPId == partsAntiDuplicate[y].TaskBPId){
                            taskAntiDuplicate[x].JobParts.push(partsAntiDuplicate[y]);
                        }else{
                            if(taskAntiDuplicate[x].TaskBPId == partsAntiDuplicate[y].TaskBPId){
                                taskAntiDuplicate[x].JobParts.push(partsAntiDuplicate[y]);
                            }
                        }
                    }
                    
                }
            }

            console.log('tampungtask ===>',tampungtask);
            console.log('taskAntiDuplicate ===>',taskAntiDuplicate);
            console.log('tampungparts ===>',tampungparts);
            console.log('partsAntiDuplicate ===>',partsAntiDuplicate);
            console.log('result JOBTASK ==========================>',JSON.stringify(taskAntiDuplicate));
            return taskAntiDuplicate;
        }
        
        var tampungEstimasi = [{EstimationNo  : null, EstTaskLength : null, EstimationBPId: null }];
        $scope.jejeranDP = [];
        $scope.processedDataEstimasi = function(index){

            if($scope.selectedCekEstimasi[index].EstimationBPId !== 0){
                $scope.selectedCekEstimasi[index].EstimationBPId = $scope.selectedCekEstimasi[index].EstimationBPId;
                console.log('ini Estimasi belum WO ===> EstimationBPId => ' ,$scope.selectedCekEstimasi[index].EstimationBPId)
            }else{
                $scope.selectedCekEstimasi[index].EstimationBPId = $scope.selectedCekEstimasi[index].JobId;
                console.log('ini Estimasi sudah WO ===> JobId => ',$scope.selectedCekEstimasi[index].JobId)
            }


            AppointmentBpService.getDetailEstimasi($scope.selectedCekEstimasi[index].EstimationBPId, $scope.selectedCekEstimasi[index].PoliceNumber).then(function (res) {
            // AppointmentBpService.getDetailEstimasi($scope.selectedCekEstimasi[index].EstimationBPId).then(function (res) {
                $scope.mDataByEstimasi =  angular.copy(res.data.Result[0]);
                $scope.mDataByEstimasi.Km = String($scope.mDataByEstimasi.Km);
                $scope.mDataByEstimasi.Km = $scope.mDataByEstimasi.Km.replace(/\B(?=(\d{3})+(?!\d))/g, ',');

                tampungEstimasi.push({ 
                    EstimationNo  : $scope.mDataByEstimasi.EstimationNo, 
                    EstTaskLength : $scope.mDataByEstimasi.AEstimationBP_TaskList_TT.length, 
                    EstimationBPId: $scope.mDataByEstimasi.EstimationBPId
                });

                console.log('tampungEstimasi | processedDataEstimasi ===>',tampungEstimasi);


                $scope.jejeranDP = [];
                for (var i = 0; i < $scope.mDataByEstimasi.AEstimationBP_TaskList_TT.length; i++) {
                    $scope.jejeranDP.push({TaskBPId:$scope.mDataByEstimasi.AEstimationBP_TaskList_TT[i].TaskBPId, partsList:[]});
                }
                


                //ngisi bslist permintaan
                if($scope.JobRequest.length > 0){ //kalo udah ada isinya
                    for(var xxx in $scope.mDataByEstimasi.AEstimationBP_Request_TT){
                        $scope.JobRequest.push($scope.mDataByEstimasi.AEstimationBP_Request_TT[xxx]);
                    }
                }else{ //kalo kosong
                    $scope.JobRequest = $scope.mDataByEstimasi.AEstimationBP_Request_TT;
                }



                //buat dapetin diskon berdasarkan pembayaran (cash / asuransi)
                console.log('$scope.mData.isCash | kenyataan ====>',$scope.mData.isCash);
                if($scope.mData.isCash == undefined){
                    console.log('$scope.mData.isCash | undefined ====>',$scope.mData.isCash);
                    $scope.choosePembayaran($scope.mDataByEstimasi.isCash);
                    $scope.mData.isCash = $scope.mDataByEstimasi.isCash;
                    if($scope.mDataByEstimasi.isCash == 0){ // untuk dapetin nama asuransi + diskonnya
                        for (i in $scope.Asuransi){
                            if($scope.Asuransi[i].InsuranceId == $scope.mDataByEstimasi.InsuranceId){
                                $scope.mData.InsuranceName = $scope.Asuransi[i].InsuranceName;
                                $scope.selectInsurance($scope.Asuransi[i]);
                            }
                        }
                    }
                }else{
                    console.log('$scope.mData.isCash | ada isinya ====>',$scope.mData.isCash);
                }


                if($scope.inisudahTerdaftar = false){

                }
                
                //untuk dapetin Model, Tipe & Katashiki dari estimasi
                var tampunganVehicleTypeId = $scope.mDataByEstimasi.VehicleTypeId;
                for (var i = 0; i < $scope.modelData.length; i++) {
                    if ($scope.modelData[i].VehicleModelId == parseInt($scope.mDataByEstimasi.ModelCode)){
                        $scope.mData.VehicleModelName       = $scope.modelData[i].VehicleModelName;
                        $scope.mData.VehicleModelId         = $scope.modelData[i].VehicleModelId;
                        $scope.mDataDetail.VehicleModelName = $scope.modelData[i].VehicleModelName;
                        $scope.mDataDetail.VehicleModelId   = $scope.modelData[i].VehicleModelId;
                        console.log('model gue ===> ',parseInt($scope.mDataByEstimasi.ModelCode) +" == $scope.modelData "+ $scope.modelData[i].VehicleModelId );

                        WO.getVehicleTypeById(parseInt($scope.mDataByEstimasi.ModelCode)).then(function(res) {
                            // $scope.typeData = res.data.Result;
                            $scope.katashikiData = _.uniq(res.data.Result, 'KatashikiCode');

                            for (var i=0; i<$scope.katashikiData.length; i++){
                                $scope.katashikiData[i].NewDescription = $scope.mDataDetail.VehicleModelName + ' - ' + $scope.katashikiData[i].KatashikiCode
                            }

                            for (var i=0; i < $scope.katashikiData.length; i++) {
                                if ($scope.katashikiData[i].KatashikiCode == $scope.mDataByEstimasi.KatashikiCode) {
                                    $scope.mDataDetail.KatashikiCodex   = $scope.mDataByEstimasi.KatashikiCode;

                                }
                            }

                            if ($scope.mDataDetail.KatashikiCodex != null && $scope.mDataDetail.KatashikiCodex != undefined && $scope.mDataDetail.KatashikiCodex != '') {
                                AppointmentGrService.GetCVehicleTypeListByKatashiki($scope.mDataByEstimasi.KatashikiCode).then(function(res) {
                                    // $scope.typeData = res.data.Result[0];
                                    $scope.typeData = res.data.Result;

                                    for (var z = 0; z < $scope.typeData.length; z++) {
                                        $scope.typeData[z].NewDescription = $scope.typeData[z].Description + ' - ' + $scope.typeData[z].KatashikiCode + ' - ' + $scope.typeData[z].SuffixCode
                                    }

                                    for (var z = 0; z < $scope.typeData.length; z++) {
                                        if($scope.typeData[z].VehicleTypeId == tampunganVehicleTypeId){
                                            $scope.selectType($scope.typeData[z])
                                            console.log('Type Gue ===> ',tampunganVehicleTypeId +" == ModelOptions "+$scope.typeData[z].VehicleTypeId);
                                        }
                                    }

                                })
                            }
                        })
                    }
                }


                
                //penyesuaian dari estimasi [Tasklist & Partlist]
                $scope.mDataByEstimasi.JobTask = $scope.mDataByEstimasi.AEstimationBP_TaskList_TT;
                var TempJobParts = [];


                for (var i = 0; i < $scope.mDataByEstimasi.JobTask.length; i++) {
                        for (var j = 0; j < $scope.mDataByEstimasi.JobTask[i].AEstimationBP_PartList.length; j++) {
                            $scope.mDataByEstimasi.JobTask[i].AEstimationBP_PartList[j].Qty = $scope.mDataByEstimasi.JobTask[i].AEstimationBP_PartList[j].qty;
                            TempJobParts.push($scope.mDataByEstimasi.JobTask[i].AEstimationBP_PartList[j]);
                        }
                        $scope.mDataByEstimasi.JobTask[i].JobParts = [];
                        for (var k = 0; k < TempJobParts.length; k++) {
                            if($scope.mDataByEstimasi.JobTask[i].EstTaskBPId == TempJobParts[k].EstTaskBPId){
                                $scope.mDataByEstimasi.JobTask[i].JobParts.push(TempJobParts[k]);
                            }
                        }
                }


                var x = 0
                for(var m in $scope.mDataByEstimasi.JobTask){

                    if($scope.mDataByEstimasi.JobTask[m].TaskBPId == null){
                        x++;
                        $scope.mDataByEstimasi.JobTask[m].TaskBPId = x + "flagNonTaskList" + new Date().getTime();                        
                    }

                    $scope.mDataByEstimasi.JobTask[m].PaidBy = null;
                    // $scope.mDataByEstimasi.JobTask[m].Discount = $scope.DiscountTask;
                    $scope.mDataByEstimasi.JobTask[m].EstimationBPId = $scope.mDataByEstimasi.EstimationBPId;
                    delete $scope.mDataByEstimasi.JobTask[m].JobId;


                    if($scope.mDataByEstimasi.JobTask[m].JobParts.length > 0){
                        for(var n in $scope.mDataByEstimasi.JobTask[m].JobParts){

                            
                        
                            // $scope.mDataByEstimasi.JobTask[m].JobParts[n].DPestimasi = $scope.mDataByEstimasi.JobTask[m].JobParts[n].DownPayment;
                            $scope.mDataByEstimasi.JobTask[m].JobParts[n].PaidBy = null;
                            // $scope.mDataByEstimasi.JobTask[m].JobParts[n].Discount = $scope.DiscountParts;
                            $scope.mDataByEstimasi.JobTask[m].JobParts[n].RetailPriceMask = $scope.mDataByEstimasi.JobTask[m].JobParts[n].Price.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                            $scope.mDataByEstimasi.JobTask[m].JobParts[n].subTotalMask = ($scope.mDataByEstimasi.JobTask[m].JobParts[n].Price * $scope.mDataByEstimasi.JobTask[m].JobParts[n].qty).toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                            if($scope.mDataByEstimasi.JobTask[m].JobParts[n].ETA == null  ){
                                $scope.mDataByEstimasi.JobTask[m].JobParts[n].isAvailable = 1; // tersedia partsnya
                            }else{
                                $scope.mDataByEstimasi.JobTask[m].JobParts[n].isAvailable = 0; //tidak tersedia partsnya          
                            }
                            delete $scope.mDataByEstimasi.JobTask[m].JobParts[n].IsCustomDiscount;
                            delete $scope.mDataByEstimasi.JobTask[m].JobParts[n].JobId ;
    
                        }
                    }

                }
                //penyesuaian dari estimasi [Tasklist & Partlist]

                // =================================================================
                for(var i in $scope.mDataByEstimasi.JobTask){
                    if($scope.mDataByEstimasi.isCash == 1){ //apapun diskon dr estimasi BP, BUANG ganti pake diskon booking, kecuali asuransi | by BU 9-Jun-2020 
                        if ($scope.mDataByEstimasi.JobTask[i].PaidById != 30) {
                            $scope.mDataByEstimasi.JobTask[i].Discount = $scope.DiscountTask; // -1 tidak ada diskon | kalo ga ada diskon ambil dari master diskon booking
                        } else {
                            $scope.mDataByEstimasi.JobTask[i].Discount = 0
                        }
                        console.log('TaskName Disc From Master ===>', $scope.mDataByEstimasi.JobTask[i].TaskName+ ' | disType =>' + $scope.mDataByEstimasi.JobTask[i].DiscountTypeId + ' | discount => ' + $scope.mDataByEstimasi.JobTask[i].Discount);
                    }else{
                        console.log('TaskName Disc From Asuransi ===>', $scope.mDataByEstimasi.JobTask[i].TaskName+ ' | disType =>' + $scope.mDataByEstimasi.JobTask[i].DiscountTypeId + ' | discount => ' + $scope.mDataByEstimasi.JobTask[i].Discount);
                    }
                    $scope.mDataByEstimasi.JobTask[i].EstimationBPId = $scope.mDataByEstimasi.EstimationBPId;
                    $scope.mDataByEstimasi.JobTask[i].PaidBy = [];
                        for(var x in $scope.paymentData){
                            if($scope.mDataByEstimasi.JobTask[i].PaidById == $scope.paymentData[x].MasterId){
                                $scope.mDataByEstimasi.JobTask[i].PaidBy = $scope.paymentData[x].Name;
                            }
                        }
    
    
                    $scope.mDataByEstimasi.JobTask[i].EstimationBPId = $scope.mDataByEstimasi.EstimationBPId;
                    for(var j in $scope.mDataByEstimasi.JobTask[i].JobParts){
                        if($scope.mDataByEstimasi.isCash == 1){ //apapun diskon dr estimasi BP, BUANG ganti pake diskon booking, kecuali asuransi | by BU 9-Jun-2020 
                            if ($scope.mDataByEstimasi.JobTask[i].JobParts[j].PaidById != 30) {
                                $scope.mDataByEstimasi.JobTask[i].JobParts[j].Discount = $scope.DiscountParts; // -1 tidak ada diskon | kalo ga ada diskon ambil dari master diskon booking
                                $scope.mDataByEstimasi.JobTask[i].JobParts[j].DiskonForDisplay = $scope.DiscountParts + '%';
                            } else {
                                $scope.mDataByEstimasi.JobTask[i].JobParts[j].Discount = 0
                                $scope.mDataByEstimasi.JobTask[i].JobParts[j].DiskonForDisplay = 0 + '%';
                            }
                            
                            console.log('PartsName Disc From Master ===>', $scope.mDataByEstimasi.JobTask[i].JobParts[j].PartsName+ ' | disType =>' + $scope.mDataByEstimasi.JobTask[i].JobParts[j].DiscountTypeId + ' | discount => ' + $scope.mDataByEstimasi.JobTask[i].JobParts[j].Discount);
                        }else{
                            console.log('PartsName Disc From Asuransi ===>', $scope.mDataByEstimasi.JobTask[i].JobParts[j].PartsName+ ' | disType =>' + $scope.mDataByEstimasi.JobTask[i].JobParts[j].DiscountTypeId + ' | discount => ' + $scope.mDataByEstimasi.JobTask[i].JobParts[j].Discount);
                            $scope.mDataByEstimasi.JobTask[i].JobParts[j].DiskonForDisplay = $scope.mDataByEstimasi.JobTask[i].JobParts[j].Discount+ '%';

                        }
                        $scope.mDataByEstimasi.JobTask[i].JobParts[j].EstimationBPId = $scope.mDataByEstimasi.JobTask[i].EstimationBPId;
                        $scope.mDataByEstimasi.JobTask[i].JobParts[j].TaskBPId = $scope.mDataByEstimasi.JobTask[i].TaskBPId;
                        $scope.mDataByEstimasi.JobTask[i].JobParts[j].subTotal = $scope.mDataByEstimasi.JobTask[i].JobParts[j].Price * $scope.mDataByEstimasi.JobTask[i].JobParts[j].Qty;                  
                        $scope.mDataByEstimasi.JobTask[i].JobParts[j].subTotalForDisplay = $scope.mDataByEstimasi.JobTask[i].JobParts[j].subTotal - (($scope.mDataByEstimasi.JobTask[i].JobParts[j].subTotal) * $scope.mDataByEstimasi.JobTask[i].JobParts[j].Discount/100);
                        
    
                        for(var x in $scope.paymentData){
                            if($scope.mDataByEstimasi.JobTask[i].JobParts[j].PaidById == $scope.paymentData[x].MasterId){
                                $scope.mDataByEstimasi.JobTask[i].JobParts[j].PaidBy = $scope.paymentData[x].Name;
                                $scope.mDataByEstimasi.JobTask[i].JobParts[j].paidName = $scope.paymentData[x].Name;
                            }
                        }
    
                        for(var x in $scope.unitData){
                            if($scope.mDataByEstimasi.JobTask[i].JobParts[j].SatuanId == $scope.unitData[x].MasterId){
                                $scope.mDataByEstimasi.JobTask[i].JobParts[j].satuanName = $scope.unitData[x].Name;
                                $scope.mDataByEstimasi.JobTask[i].JobParts[j].Name = $scope.unitData[x].Name;
                            }
                        }
                    }
                }
    
                for(var i in $scope.gridWork){
                    $scope.gridWork[i].isDeleted = 0;
            
                        for(var x in $scope.paymentData){
                            if($scope.gridWork[i].PaidById == $scope.paymentData[x].MasterId){
                                $scope.gridWork[i].PaidBy = $scope.paymentData[x].Name;
                            }
                        }
    
                    for(var j in $scope.gridWork[i].JobParts){
                        $scope.gridWork[i].JobParts[j].Price = $scope.gridWork[i].JobParts[j].RetailPrice;    
                        $scope.gridWork[i].JobParts[j].isDeleted = 0;
                        if(typeof $scope.gridWork[i].EstimationBPId != undefined){
                            $scope.gridWork[i].JobParts[j].EstimationBPId = $scope.gridWork[i].EstimationBPId;
                        }
    
                        for(var x in $scope.paymentData){
                            if($scope.gridWork[i].JobParts[j].PaidById == $scope.paymentData[x].MasterId){
                                $scope.gridWork[i].JobParts[j].PaidBy = $scope.paymentData[x].Name;
                                $scope.gridWork[i].JobParts[j].paidName = $scope.paymentData[x].Name;
                            }
                        }
    
                        for(var x in $scope.unitData){
                            if($scope.gridWork[i].JobParts[j].SatuanId == $scope.unitData[x].MasterId){
                                $scope.gridWork[i].JobParts[j].satuanName = $scope.unitData[x].Name;
                                $scope.gridWork[i].JobParts[j].Name = $scope.unitData[x].Name;
                            }
                        }
                    }
                }
                // =================================================================


                //kalo data parent kosong masukin data dari estimasi
                if($scope.mData.WoCategoryId        == undefined){$scope.mData.WoCategoryId        = $scope.mDataByEstimasi.CategoryId}
                if($scope.mData.Km                  == undefined){$scope.mData.Km                  = $scope.mDataByEstimasi.Km}
                if($scope.mData.ContactPerson       == undefined){$scope.mData.ContactPerson       = $scope.mDataByEstimasi.ContactPerson}
                if($scope.mData.PhoneContactPerson1 == undefined){$scope.mData.PhoneContactPerson1 = $scope.mDataByEstimasi.PhoneContactPerson1}
                if($scope.mData.Email               == undefined){$scope.mData.Email               = $scope.mDataByEstimasi.Email}
                if($scope.mData.isCash              == undefined){$scope.mData.isCash              = $scope.mDataByEstimasi.isCash}
                
                
                //untuk dapetin tasklist
                dataJobTaskPlan = []; // jancok
                console.log('grid work pas klik estimasi ===>', $scope.gridWork);

                for( var j in $scope.gridWork){
                    $scope.gridWork[j].JobParts = $scope.gridWork[j].child;
                    for(var m in $scope.gridWork[j].JobParts){
                        $scope.gridWork[j].JobParts[m].Satuan = null; //dapetin satuan
                        for(var o in $scope.unitData){
                            if($scope.unitData[o].MasterId == $scope.gridWork[j].JobParts[m].SatuanId){
                                // $scope.gridWork[j].JobParts[m].Satuan.MasterId = $scope.unitData[o].MasterId;
                                $scope.gridWork[j].JobParts[m].Name = $scope.unitData[o].Name;
                                $scope.gridWork[j].JobParts[m].satuanName = $scope.unitData[o].Name;
                            }
                        }

                        $scope.gridWork[j].JobParts[m].PaidBy = null; //dapetin tipe pembayaran
                        for(var z in $scope.paymentData ){
                            if($scope.paymentData[z].MasterId == $scope.gridWork[j].JobParts[m].PaidById){
                                // $scope.gridWork[j].JobParts[m].PaidBy.MasterId = $scope.paymentData[z].MasterId;
                                // $scope.gridWork[j].JobParts[m].PaidBy.Name = $scope.paymentData[z].Name;
                                $scope.gridWork[j].JobParts[m].PaidBy = $scope.paymentData[z].Name;
                            }
                        }
                    }
                }


                var TaskTanpaDuplikat = $scope.collapseGridworkAndEstimasiTaskList($scope.gridWork, $scope.mDataByEstimasi);

                if(countEstimasiMasuk == jumlahEstimasiMasuk){
                    console.log('countEstimasiMasuk =>',countEstimasiMasuk +' | jumlahEstimasiMasuk => '+ jumlahEstimasiMasuk);
                    // console.log('estimasi ini masuk ===>',$scope.mDataByEstimasi.JobTask[0].JobParts.length);
                    $scope.mDataByEstimasi.JobTask =[];
                    $scope.mDataByEstimasi.JobTask = TaskTanpaDuplikat
                    $scope.mDataByEstimasi.AEstimationBP_TaskList_TT = [];
                    $scope.mDataByEstimasi.AEstimationBP_TaskList_TT = TaskTanpaDuplikat;
                    $scope.dataForBslist($scope.mDataByEstimasi);
                    countEstimasiMasuk = 0;
                    jumlahEstimasiMasuk = 0;
                }else{
                    console.log('countEstimasiMasuk =>',countEstimasiMasuk +' | jumlahEstimasiMasuk => '+ jumlahEstimasiMasuk);
                    // console.log('estimasi ini ga masuk ===>',$scope.mDataByEstimasi.JobTask[0].JobParts.length);
                }

                


            
                
            });

            $scope.btnBatalCekEstimasi();
        }


        
        



        // =====================CR4 Add Estimasi =======================


    }) // added by sss on 2017-10-10
    .filter('griddropdownStat', function() {
        return function(input, xthis) {
            // console.log("filter..");
            // console.log(input);
            // console.log(xthis);
            if (xthis !== undefined) {
                if (xthis.col !== undefined) {
                    var map = xthis.col.colDef.editDropdownOptionsArray;
                    var idField = xthis.col.colDef.editDropdownIdLabel;
                    var valueField = xthis.col.colDef.editDropdownValueLabel;
                    for (var i = 0; i < map.length; i++) {
                        if (map[i][idField] == input) {
                            return map[i][valueField];
                        }
                    }
                }
            }

            return '';
        };
    })
    .filter('griddropdownTask', function() {
        return function(input, xthis) {
            // console.log("filter..");
            // console.log(input);
            // console.log(xthis);
            if (xthis !== undefined) {
                if (xthis.col !== undefined) {
                    if (input !== null) {
                        return input.map(function(elem) {
                            return elem.TaskName;
                        }).join(", ");
                    }
                }
            }

            return '';
        };

    });