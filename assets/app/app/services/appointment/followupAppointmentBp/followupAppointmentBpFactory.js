angular.module('app')
    .factory('FollowUpBpService', function($http, $filter, CurrentUser) {
        var currUser = CurrentUser.user();
        console.log(currUser);
        return {
            // getData: function(firstDate,lastDate,filter) {
            //   if(firstDate != null || lastDate != null){
            //     console.log("getData bye datenya pertama",firstDate);
            //     console.log("getData bye datenya kedua",lastDate);
            //     var res=$http.get('/api/as/jobs/0/between/'+firstDate+'/'+lastDate);
            //   }else{
            //     if(filter == 1){
            //       var res=$http.get('/api/as/jobs/0/AppointOneDayBef');
            //     }else if(filter == 2){
            //       var res=$http.get('/api/as/jobs/0/AppointOneHourBef');
            //     }else{
            //       var res=$http.get('/api/as/jobs/0/AppointPlusH');
            //     }
            //   }
            //   console.log("getData biasa",filter);
            //   //res.data.Result = null;
            //   return res;
            // },
            UpdateJobAppointmentAddEstimasiId: function(jobId, arrayEstimasiId) {
                return $http.put('/api/as/AEstimationBP/UpdateJobAppointment?JobId=' + jobId, arrayEstimasiId);
            },
            
            getData: function(firstDate, lastDate, filter, dateNow, timeNow) {

                var tipeGet = "";
                if (filter == 1) {
                    tipeGet = "h"
                } else if (filter == 2) {
                    tipeGet = "j";
                } else if (filter == 3) {
                    tipeGet = "jn";
                } else if (filter == 7) {
                    tipeGet = "noshow";
                } else if (filter == 5) {
                    tipeGet = "history";
                } else if (filter == 6) {
                    tipeGet = "upcoming";
                } else if (filter == 4) {
                    tipeGet = "need-appr";
                }
                console.log("getData filter", filter);
                console.log("getData bye datenya pertama", firstDate);
                console.log("getData bye datenya kedua", lastDate);
                console.log("getData dateNow", dateNow);
                console.log("getData timeNow", timeNow);
                console.log("currUser", currUser);
                // if (currUser.RoleId == 1129 || currUser.RoleId == 1021) {
                //     var res = $http.get('/api/as/jobs/listFollowUpAppointment/0/' + tipeGet + '/' + firstDate + '/' + lastDate + '/' + dateNow + '/' + timeNow + '/' + currUser.RoleId);
                // } else {
                //     var res = $http.get('/api/as/jobs/listFollowUpAppointment/0/' + tipeGet + '/' + firstDate + '/' + lastDate + '/' + dateNow + '/' + timeNow );
                // }
                var res = $http.get('/api/as/jobs/listFollowUpAppointment/0/' + tipeGet + '/' + firstDate + '/' + lastDate + '/' + dateNow + '/' + timeNow);

                //   if (currUser.RoleId == 1129 ) {
                //   // if (currUser.RoleId == 1128 || currUser.RoleId == 1047) {
                //       if (firstDate != null || lastDate != null) {
                //           console.log("getData bye datenya pertama", firstDate);
                //           console.log("getData bye datenya kedua", lastDate);
                //           var res = $http.get('/api/as/jobs/0/between/' + firstDate + '/' + lastDate + '/' + currUser.RoleId);
                //       } else {
                //           if (filter == 1) {
                //               var res = $http.get('/api/as/jobs/0/AppointOneDayBef/' + currUser.RoleId);
                //           } else if (filter == 2) {
                //               var res = $http.get('/api/as/jobs/0/AppointOneHourBef/' + currUser.RoleId);
                //           } else {
                //               var res = $http.get('/api/as/jobs/0/AppointPlusH/' + currUser.RoleId);
                //           }
                //       }
                //   } else {
                //       if (firstDate != null || lastDate != null) {
                //           console.log("getData bye datenya pertama", firstDate);
                //           console.log("getData bye datenya kedua", lastDate);
                //           var res = $http.get('/api/as/jobs/0/between/' + firstDate + '/' + lastDate);
                //       } else {
                //           if (filter == 1) {
                //               var res = $http.get('/api/as/jobs/0/AppointOneDayBef');
                //           } else if (filter == 2) {
                //               var res = $http.get('/api/as/jobs/0/AppointOneHourBef');
                //           } else {
                //               var res = $http.get('/api/as/jobs/0/AppointPlusH');
                //           }
                //       }
                //   }
                console.log("getData biasa", filter);
                //res.data.Result = null;
                return res;
            },
            // cancel: function(job){
            //   return $http.put('/api/as/jobs/Cancel/true',[job.JobId]);
            // },
            cancel: function(job) {
                return $http.put('/api/as/jobs/Cancel/1', [{
                    jobId: job.JobId,
                    CancelReason: job.Reason
                }]);
            },
            onTime: function(filter, data) {
                console.log('ini filter', filter);
                console.log('ini datanya', data);
                if (filter == 1 || filter == 6) {
                    return $http.put('/api/as/jobs/AppointmentFollowUp/OneDay/1/' + [data.JobId]);
                } else if (filter == 2 || filter == 3) {
                    return $http.put('/api/as/jobs/AppointmentFollowUp/OneDay/0/' + [data.JobId]);
                }
            },
            getDataVehicle: function(key) {
                var res = $http.get('/api/crm/GetDataInfo/' + key);
                return res;
            },
            getDataParts: function(Key, servicetype, type) {
                console.log("keyyyyyy", Key);
                var res = $http.get('/api/as/StockAdjustment/GetStockAdjustmentDetailFromMaterial?PartsCode=' + Key + '&ServiceTypeId=' + servicetype + '&PartsClassId1=' + type);
                // console.log("/api/as/TaskLists?KatashikiCode="+Katashiki+"&Name="+Key);
                // console.log("resnya",res);
                return res;
            },
            getDataTask: function(Key, Katashiki, catg) {
                var res = $http.get('/api/as/TaskLists?KatashikiCode=' + Katashiki + '&Name=' + Key + '&TaskCategory=' + catg);
                // console.log("/api/as/TaskLists?KatashikiCode="+Katashiki+"&Name="+Key);
                // console.log("resnya",res);
                return res;
            },
            getDataGender: function() {
                var res = $http.get('/api/as/Gender/');
                return res;
            },
            getPayment: function() {
                var catId = 1008;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getUnitMeasurement: function() {
                var catId = 1;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getWoCategory: function() {
                var catId = 1012;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getVehicleTypeById: function(id) {
                var url = '/api/crm/GetCVehicleTypeById/' + id
                var res = $http.get(url);
                return res;
            },

            getTaskCategory: function() {
                var catId = 1010;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getWobyJobId: function(filter) {
                var res = $http.get('/api/as/Jobs/' + filter);
                return res;
            },
            getPreDiagnoseByJobId: function(id) {
                return $http.get('/api/as/GetPrediagnoses/job/' + id);
            },
            create: function(data) {
                if (data.AppointmentCreateDate == null || data.AppointmentCreateDate == undefined) {
                    data.AppointmentCreateDate = null
                } else {
                    data.AppointmentCreateDate = $filter('date')(data.AppointmentCreateDate, 'yyyy-MM-dd HH:mm:ss')
                }
                return $http.put('/api/as/jobs/reScheduleAppointment', [{
                    job: data,
                    //pid: role.pid,
                    Description: 'tes'
                }]);
                // return $http.post('/api/as/AppointmentGrService', [{
                //                                     Employee:data.EmployeeId,
                //                                     Initial:data.Initial,
                //                                     ShiftByte:5,
                //                                     People : {
                //                                       Name: data.Name,
                //                                       DateofBirth: data.DateofBirth
                //                                     }
                //                                     }]);
            },
            update: function(data) {

                // ilangin flag jika tasknya adalah nontasklist
                for(var x in data.JobTask){
                    if(typeof data.JobTask[x].TaskBPId == "string"){
                        data.JobTask[x].TaskBPId = null;
                    }
                }
                

			// if(data.disTask !== null || data.disTask !== undefined){
            //         console.log("oleh oleh oleh");
            //         for(var a = 0; a < data.JobTask.length; a++){
            //             if(data.JobTask[a].PaidById == 2277 || data.JobTask[a].PaidById == 30 || data.JobTask[a].PaidById == 31|| data.JobTask[a].PaidById == 32){

            //                 console.log("keatas dong");
            //                 data.JobTask[a].Discount = 0;
            //                 data.JobTask[a].DiscountTypeId = -1;
            //                 for(var b = 0; b < data.JobTask[a].JobParts.length; b++){
            //                    if(data.JobTask[a].JobParts[b].PaidById == 2277 || data.JobTask[a].JobParts[b].PaidById == 30 || data.JobTask[a].JobParts[b].PaidById == 31|| data.JobTask[a].JobParts[b].PaidById == 32){

            //                        data.JobTask[a].JobParts[b].Discount = 0;
            //                        data.JobTask[a].JobParts[b].DiscountTypeId = -1;
                                  
            //                    }else{
            //                        if(data.disParts !== null || data.disParts !== undefined && data.JobTask[a].JobParts.length > 0){
                                          
            //                               data.JobTask[a].JobParts[b].Discount = data.disParts;
            //                               data.JobTask[a].JobParts[b].DiscountTypeId = 10;
            //                        }
            //                    }
            //                 }

                            
            //             }else{
            //                 console.log("masuk bawah dong");
            //                 data.JobTask[a].Discount = data.disTask;
            //                 data.JobTask[a].DiscountTypeId = 10;
            //                 if(data.disParts !== null || data.disParts !== undefined && data.JobTask[a].JobParts.length > 0){
            //                     for(var b = 0; b < data.JobTask[a].JobParts.length; b++){
            //                         data.JobTask[a].JobParts[b].Discount = data.disParts;
            //                         data.JobTask[a].JobParts[b].DiscountTypeId = 10;
            //                     }
            //                 }
                        
            //             }
                       
            //         }
            //     }															
                console.log('FollowUpAppointmen BP || factory data====>',data);

                var res = $http.put('/api/as/Jobs/updateDetil/1', [{
                    JobId: data.JobId,
                    JobNo: 0,
                    OutletId: data.OutletId,
                    CalId: 1,
                    UserIdEstimasi: data.StallId,
                    // StallId: data.StallId,
                    // StallId:data.StallId,
                    ContactPerson: data.ContactPerson,
                    PhoneContactPerson1: data.PhoneContactPerson1,
                    PhoneContactPerson2: data.PhoneContactPerson2,
                    VehicleId: data.VehicleId,
                    PlanStart: data.PlanStart,
                    PlanFinish: data.PlanFinish,
                    CustRequest: data.CustRequest,
                    CustComplaint: data.CustComplaint,
                    TechnicianAction: 1,
                    JobSuggest: 1,
                    JobType: 0,
                    Status: 0,
                    JobTask: data.JobTask,
                    AppointmentVia: data.AppointmentVia,
                    AppointmentDate: data.AppointmentDate,
                    AppointmentTime: data.AppointmentTime,
                    AppointmentNo: data.AppointmentNo,
                    NewAppointmentRel: null,
                    isAppointment: 1,
                    isGr: 0,
                    Km: data.Km,
                    isCash: data.isCash,
                    isSpk: data.isSpk,
                    SpkNo: data.SpkNo,
                    InsuranceName: data.InsuranceName,
                    InsuranceId: data.InsuranceId,
                    isWOBase: 1,
                    WoCategoryId: data.WoCategoryId,
                    Process: null,
                    WoNo: 0,
                    IsEstimation: 0,
                    PoliceNumber: data.PoliceNumber,
                    VIN: data.VIN,
                    ModelType: data.detail.VehicleModelName,
                    KatashikiCode: data.KatashikiCode,
                    VehicleTypeId: data.VehicleTypeId,
                    // EstimationDate:"",
                    // EstimationNo:"",
                    PlanDateStart: data.PlanDateStart,
                    PlanDateFinish: data.PlanDateFinish,
                    JobDate: "",
                    FinalDP: data.totalDp,
                    AppointmentCreateDate: (data.AppointmentCreateDate == null || data.AppointmentCreateDate == undefined) ? null : $filter('date')(data.AppointmentCreateDate, 'yyyy-MM-dd HH:mm:ss'),
                }]);
                return res
            },
            updateApproval: function(data) {
                console.log("data di updateApproval", data);
                return $http.put('/api/as/RPAppointmentApprovalBPChangeDPTR', [{
                    OutletId: currUser.OrgId,
                    TransactionId: data.TransactionId,
                    JobId: data.JobId,
                    DPDefault: data.DPDefault,
                    DPRequested: data.DPRequested,
                    ApprovalCategoryId: 41,
                    seq: 1,
                    ApproverId: null,
                    RequesterId: null,
                    ApproverRoleId: data.ApproverRoleId,
                    RequestorRoleId: data.RequestorRoleId,
                    StatusApprovalId: data.NewStatusApprovalId,
                    VehicleTypeId: data.VehicleTypeId,
                    Discount: 0,
                    RequestReason: data.RequestReason,
                    RequestDate: data.RequestDate,
                    RejectReason: data.Reason,
                    ApproveRejectDate: new Date(),
                    StatusCode: 1,
                }]);
            },
            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
            getJobParts: function(jobId) {
                return $http.get('/api/as/jobs/listJobPartList/' + jobId + '/3/0');
            },
            sendNotif: function(data, recepient, Param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: "No. Appointment : " + data.AppointmentNo +
                        "<br> No. Polisi : " + data.LicensePlate,
                    RecepientId: recepient,
                    Param: Param
                }]);
            },
            getDataVIN: function(vin){
                var res = $http.get('/api/as/MasterVehicle_FPCFLCPLC/' + vin)
                return res;
            },
            listHistoryClaim: function(vin){
                var res = $http.get('/api/as/ToWass/ListHistoryClaim?VIN=' + vin)
                return res;
            },
            getTowass: function(vin) {
                var res = $http.get('/api/as/Towass/getFieldAction/1/' + vin);
                return res;
            }
        }
    });