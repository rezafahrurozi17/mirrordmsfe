angular.module('app')
    .controller('followupAppointmentGrController', function($rootScope, PreDiagnose, $scope, $q, $http, bsNotify, ASPricingEngine, CurrentUser, RepairSupportActivityGR, FollowUpGrService, FollowupService, AppointmentBpService, $timeout, AppointmentGrService, bsAlert, ngDialog, MrsList, AsbGrFactory, Parameter, $window, $filter, $state, CMaster, TabGrFactory, PrintRpt, WOBP, WO, WoHistoryGR, VehicleHistory, MasterDiscountFactory) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        var PPNPerc = 0;

        $scope.user = CurrentUser.user();
        $scope.boardName = 'asbGrFollowup';
        // ===========================================
        $scope.asb = {
            currentDate: new Date(),
            showMode: 'main',
            nopol: "D 1234 ABC",
            tipe: "Avanza",
            // NewChips:1,
        }
        $scope.asb.endDate = new Date();
        $scope.asb.endDate.setDate($scope.asb.endDate.getDate() + 7);
        var currentDate = new Date();
        // currentDate.setDate(currentDate.getDate() + 1);
        currentDate.setDate(currentDate.getDate()); // edit bisa hari ini Pak Dodi
        $scope.minDateASB = new Date(currentDate);
        // var showBoardAsbGr = function(vitem){
        //   AsbGrFactory.showBoard({container: 'asb_asbgr_view', currentChip: vitem, onPlot: contoh});
        // }
        var allBoardHeight = 500;
        $scope.startlongDay = '';
        $scope.endlongDay = '';

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            $scope.checkApprove();
            $scope.getDataModelForAppointment();
            $scope.checkIsKabeng();
            $scope.checkStall();
            $scope.getPPN()
        });

        $scope.getPPN = function (noNpwp) {
            if (noNpwp !== undefined && noNpwp !== null && noNpwp !== ''){
                noNpwp = noNpwp.toString();
                if (noNpwp.length === 20){
                    if (noNpwp !== '00.000.000.0-000.000'){
                        ASPricingEngine.getPPN(1, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    } else {
                        ASPricingEngine.getPPN(0, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    }
                } else {
                    ASPricingEngine.getPPN(0, 3).then(function (res) {
                        PPNPerc = res.data
                    });
                }

            } else {
                // param 1 adalah isnpwp, param 2 tipe ppn
                ASPricingEngine.getPPN(0, 3).then(function (res) {
                    PPNPerc = res.data
                });
            }
            
        };

        $scope.model_info_km = {}
        $scope.model_info_km.show = false;
        $scope.viewButtoninfokm = {save:{text:'Ok'}};

        $scope.Saveinfokm = function(){
            $scope.model_info_km.show = false;
        }
        $scope.Cancelinfokm = function(){
            $scope.model_info_km.show = false;
        }
        $scope.show_km_info = function() {
            $scope.model_info_km.show = true;
        }

        $scope.checkIsKabeng = function() {
            // if ($scope.user.RoleId == 1128 || $scope.user.RoleId == 1021) {
            $scope.typeFollowUp = [{
                    Id: 1,
                    Name: "H-1"
                },
                {
                    Id: 2,
                    Name: "J-N"
                },
                {
                    Id: 3,
                    Name: "J+N"
                },
                {
                    Id: 5,
                    Name: "History"
                },
                {
                    Id: 6,
                    Name: "Upcoming Appointment"
                },
                {
                    Id: 4,
                    Name: "Need Approval"
                }

            ];
            // }
        }

        // tambahan CR4
        $scope.DiskonPartsEndDate = null;
        $scope.DiskonTaskEndDate = null;
        $scope.asbDate = undefined;
        $scope.getDiscountFromMasterDiscountBooking = function(){
            MasterDiscountFactory.getDataAktif().then(
                function (res) {
                    console.log("res dis", res.data.Result);
                    for(var i = 0; i < res.data.Result.length; i++){
                        if(res.data.Result[i].Category == 2){
                            $scope.DiskonPartsEndDate = res.data.Result[i].DateTo;
                            $scope.DiscountParts = res.data.Result[i].Discount;
                            $scope.mData.disParts = res.data.Result[i].Discount;
                        }else if(res.data.Result[i].Category == 1){
                            $scope.DiskonTaskEndDate = res.data.Result[i].DateTo;
                            $scope.DiscountTask = res.data.Result[i].Discount;
                            $scope.mData.disTask = res.data.Result[i].Discount;
                        }
                    }

                    if(($scope.asbDate == undefined) || ($scope.asbDate.length == 0)){
                        
                        console.log('kalo asbnya undefined | [] ===>',$scope.asbDate);
                    }else{
                        if($scope.asbDate > $scope.DiskonTaskEndDate){
                            console.log('Tidak Masuk periode diskon booking Task');
                            $scope.DiscountTask = 0;
                            $scope.mData.disTask = 0;
                        }else{
                            console.log('masuk periode diskon booking Task');
                        }
                        if($scope.asbDate > $scope.DiskonPartsEndDate){
                            console.log('Tidak Masuk periode diskon booking Parts');
                            $scope.DiscountParts = 0;
                            $scope.mData.disParts = 0;
                        }else{
                            console.log('masuk periode diskon booking Parts');
                        }
                    }
                },
                function (err) { }
            );
            $scope.mData.disTask  = angular.copy($scope.DiscountTask);
            $scope.mData.disParts = angular.copy($scope.DiscountParts);
            console.log('diskon disTask ===> ',$scope.mData.disTask +' | diskon disParts ===> ' +$scope.mData.disParts);
        }
        // tambahan CR4

        $scope.choosePembayaran = function(data){
            console.log("data",data);
            // if(data == 1){

                $scope.getDiscountFromMasterDiscountBooking();
                
            // }else{
                // $scope.DiscountParts = 0;
                // $scope.mData.disParts = 0;
                // $scope.DiscountTask = 0;
                // $scope.mData.disTask = 0;
            // }
        }

        $scope.checkStall = function(){
            AppointmentGrService.getListStall().then(function(res){
                var xres = res.data.Result;
                var vstart = '';
                var vend = '';
                for(var i=0;  i<xres.length; i++){
                    var vitem = xres[i];
                    // === NEW CODE Before GO LIVE
                    if (!vitem.DefaultOpenTime) {
                        vitem.DefaultOpenTime = '08:00';
                    }
                    if (typeof vitem.DefaultOpenTime !== 'undefined') {

                        //cek defaultopen time menit nya 00 ga? kl ga 00 set jd 00 biar header nya menit nya 00
                        var cekDefaultOpenTime = vitem.DefaultOpenTime.split(':');
                        if (cekDefaultOpenTime[1].toString() != '00'){
                            vitem.DefaultOpenTime = cekDefaultOpenTime[0] + ':' + '00' + ':' + '00'
                        }

                        // if (vitem.DefaultOpenTime){
                        if (vstart === '') {
                            vstart = vitem.DefaultOpenTime;
                        } else {
                            if (vstart > vitem.DefaultOpenTime) {
                                vstart = vitem.DefaultOpenTime;
                            }
                        }
                        // }
                    }
                    if (vitem.DefaultCloseTime == undefined) {
                        vitem.DefaultCloseTime = '17:00';
                    }
                    if (typeof vitem.DefaultCloseTime !== 'undefined') {
                        if (vitem.DefaultCloseTime >= '23:59') {
                            vitem.DefaultCloseTime = '24:00';
                        }
                        if (vend === '') {
                            vend = vitem.DefaultCloseTime;
                        } else {
                            if (vend < vitem.DefaultCloseTime) {
                                vend = vitem.DefaultCloseTime;
                            }
                        }
                    }
                }
                if (vstart === '') {
                    vstart = '08:00';
                }
                if (vend === '') {
                    vend = '17:00';
                }
                var vxmin = JsBoard.Common.getMinute(vend);
                vend = JsBoard.Common.getTimeString(vxmin);
                // me.startHour = vstart;
                $scope.startlongDay = vstart;
                $scope.endlongDay = vend;
            });
        }

        var showBoardAsbGr = function(item) {
            console.log("item item", item);
            var vtime
            var tmphour
            var tmpMinute
            if (item !== undefined && item !== null) {
                vtime = item;
                tmphour = item.getHours();
                tmpMinute = item.getMinutes();
            } else {
                vtime = new Date();
                tmphour = vtime.getHours();
                tmpMinute = vtime.getMinutes();
            }

            vtime.setHours(tmphour);
            vtime.setMinutes(tmpMinute);
            vtime.setSeconds(0);
            $scope.currentItem = {
                mode: 'main',
                currentDate: $scope.currentDate,
                nopol: $scope.mDataDetail.LicensePlate,
                tipe: $scope.mDataDetail.VehicleModelName,
                durasi: $scope.tmpActRate,
                stallId: $scope.stallAsb,
                startTime: vtime,
                visible: $scope.visibleAsb,
                jobId: $scope.mData.JobId,
                preDiagnose: tmpPrediagnose,
                asbStatus: $scope.asb,
                longStart: $scope.startlongDay,
                longEnd:$scope.endlongDay,
                /*stallId :1,
                startDate :,
                endDate:,
                durasi:,*/
            }
            console.log('=============Showboard==============')
            console.log("actRe", $scope.tmpActRate);
            console.log("harusnya tanggal", $scope.asb.startDate);
            console.log("ini vtime", vtime);
            console.log("$scope.stallAsb", $scope.stallAsb);
            console.log("$scope.currentDate", $scope.currentDate);
            console.log("$scope.mData.JobId", $scope.mData.JobId);
            console.log("$scope.currentItem", $scope.currentItem);
            console.log('=============/Showboard/==============')
            $timeout(function() {
                AsbGrFactory.showBoard({
                    boardName: $scope.boardName,
                    container: 'asb_asbgr_viewFollow',
                    currentChip: $scope.currentItem,
                    onPlot: getDateEstimasi
                });

            }, 100);

        }
        var showTodayAppointmentBoard = function() {
            var vtime = new Date();
            vtime.setHours(9);
            vtime.setMinutes(30);
            vtime.setSeconds(0);
            $scope.currentItem = {
                mode: $scope.asb.showMode,
                currentDate: $scope.asb.currentDate,
                nopol: $scope.asb.nopol,
                tipe: $scope.asb.tipe,
                durasi: $scope.tmpActRate,
                stallId: 0,
                startTime: vtime,
                longStart: $scope.startlongDay,
                longEnd:$scope.endlongDay,
                /*stallId :1,
                startDate :,
                endDate:,
                durasi:,*/
            }
            console.log("actRe", $scope.tmpActRate);
            $timeout(function() {
                AsbGrFactory.showBoard({
                    boardName: 'todayAppointmentBoard',
                    container: 'Today_appointment_board',
                    currentChip: $scope.currentItem,
                    onPlot: getDateEstimasi
                });

            }, 100);

        }
        var allBoardHeight = 500;
        var vobj = {
            mode: $scope.asb.showMode,
            currentDate: $scope.asb.currentDate,
            nopol: $scope.asb.nopol,
            tipe: $scope.asb.tipe,
            /*stallId :1,
            startDate :,
            endDate:,
            durasi:,*/
        };
        $scope.reloadBoard = function(item) {
            var tmpMinDateASB = new Date();

            $scope.asbDate = item;


            if(item > $scope.DiskonTaskEndDate){
                console.log('Tidak Masuk periode diskon booking Task');
                $scope.DiscountTask = 0;
                $scope.mData.disTask = 0;
            }else{
                console.log('masuk periode diskon booking Task');
            }
            
            if(item > $scope.DiskonPartsEndDate){
                console.log('Tidak Masuk periode diskon booking Parts');
                $scope.DiscountParts = 0;
                $scope.mData.disParts = 0;
            }else{
                console.log('masuk periode diskon booking Parts');
            }


            tmpMinDateASB.setHours(0, 0, 0);
            if (item < tmpMinDateASB.getTime()) {
                console.log("ini nih bossss", tmpMinDateASB);
                console.log("item ini nih", item);
            } else {
                $scope.mData.PlanStart = null;
                $scope.mData.PlanFinish = null;
                $scope.mData.PlanDateStart = null;
                $scope.mData.AppointmentDate = null;
                $scope.mData.AppointmentTime = null;
                $scope.mData.TargetDateAppointment = null;
                $scope.mData.FixedDeliveryTime1 = null;
                $scope.mData.PlanDateFinish = null;
                $scope.asb.JobStallId = null;
                $scope.asb.JobStartTime = null;
                $scope.asb.JobEndTime = null;
                console.log("dari ng-change", item);
                console.log("$scope.mData.FullDate", $scope.mData.FullDate);
                var vtime = item;
                var tmphour
                var tmpMinute
                    // baris di bawah ini di tambahin validasinya,  kalau back dl supaya ga error
                if ($scope.mData.FullDate !== undefined && $scope.mData.FullDate !== null) {
                    console.log("$scope.mData.FullDate", $scope.mData.FullDate);
                    var tmpTime = $scope.mData.FullDate;
                    tmphour = tmpTime.getHours();
                    tmpMinute = tmpTime.getMinutes();
                } else {
                    tmphour = 9;
                    tmpMinute = 0;
                }
                vtime.setHours(tmphour);
                vtime.setMinutes(tmpMinute);
                vtime.setSeconds(0);
                $scope.currentItem = {
                    mode: 'main',
                    currentDate: $scope.asb.startDate,
                    nopol: $scope.mDataDetail.LicensePlate,
                    tipe: $scope.mDataDetail.VehicleModelName,
                    durasi: $scope.tmpActRate,
                    stallId: 0,
                    startTime: vtime,
                    visible: $scope.visibleAsb,
                    jobId: $scope.mData.JobId,
                    preDiagnose: tmpPrediagnose,
                    asbStatus: $scope.asb,
                    longStart: $scope.startlongDay,
                    longEnd:$scope.endlongDay,
                    // JobId: $scope.mData.JobId,
                    /*stallId :1,
                    startDate :,
                    endDate:,
                    durasi:,*/
                }
                console.log("actRe", $scope.tmpActRate);
                console.log("harusnya tanggal", $scope.asb.startDate);
                console.log("ini vtime", vtime);
                console.log("stallId", $scope.currentItem.stallId);
                $timeout(function() {
                    AsbGrFactory.showBoard({
                        boardName: $scope.boardName,
                        container: 'asb_asbgr_viewFollow',
                        currentChip: $scope.currentItem,
                        onPlot: getDateEstimasi
                    });

                }, 100);
            }
        }

        $scope.VehicleGasType = [
            { VehicleGasTypeId: 1, VehicleGasTypeName: "Bensin" },
            { VehicleGasTypeId: 2, VehicleGasTypeName: "Diesel" },
            { VehicleGasTypeId: 3, VehicleGasTypeName: "Hybrid" }
        ];
        var getDateEstimasi = function(data) {
            console.log('onplot data...', data);
            // baris di bawah ini di tambahin validasinya,  kalau back dl supaya ga error            
            if ((data.startTime === null) || (data.endTime === null) || (data.stallId == 0) || (data.PrediagStallId == 0)) {
                // jika startTime/EndTime null berarti belum ada plot, Data tanggal awal/akhir serta Tanggal Appointment harus dikosongkan maka harus dikosongkan
                if (data.PrediagScheduledTime !== null && data.PrediagStallId !== 0) {
                    var dt = new Date(data.PrediagScheduledTime);
                    var dtt = new Date(data.PrediagScheduledTime);
                    dt.setSeconds(0);
                    dtt.setSeconds(0);
                    var timeStart = dt.toTimeString();
                    var timeFinish = dtt.toTimeString();
                    timeStart = timeStart.split(' ');
                    var finaltimeFinish = timeFinish.split(' ');
                    finaltimeFinish = finaltimeFinish[0].split(':')
                    finaltimeFinish = (parseInt(finaltimeFinish[0]) + 1).toString() + ":" + finaltimeFinish[1] + ":" + finaltimeFinish[2];

                    var hourFinish = dtt.getHours();
                    var minuteFinish = dtt.getMinutes();
                    var minuteFinishplus = minuteFinish + 30;
                    var hourFinishplus = hourFinish + 1;
                    if (minuteFinishplus > 59) {
                        hourFinishplus = hourFinish + 1;
                        minuteFinishplus = minuteFinishplus - 60;
                    }
                    if (dtt.getHours() == 12) {
                        hourFinishplus = hourFinish + 1;
                    }
                    var finaltimeFinishDelivery = (hourFinishplus > 9 ? hourFinishplus : "0" + hourFinishplus) + ":" + minuteFinishplus + ":00";
                    console.log("timeStart", timeStart);
                    console.log("timeFinish", finaltimeFinish);
                    // ===============================
                    var yearFirst = dt.getFullYear();
                    var monthFirst = dt.getMonth() + 1;
                    var dayFirst = dt.getDate();
                    var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                    firstDate = $scope.changeFormatDate(dt);
                    // ===============================
                    var yearFinish = dtt.getFullYear();
                    var monthFinish = dtt.getMonth() + 1;
                    var dayFinish = dtt.getDate();
                    var FinishDate = yearFinish + '-' + monthFinish + '-' + dayFinish;
                    FinishDate = $scope.changeFormatDate(dtt);
                    // ===============================
                    // if(timeStart[0] !== null && timeFinish[0] !== null){
                    $scope.mData.StallId = data.PrediagStallId;
                    $scope.mData.FullDate = dt;
                    $scope.mData.PlanStart = timeStart[0];
                    $scope.mData.PlanFinish = finaltimeFinish;
                    $scope.mData.PlanDateStart = firstDate;
                    $scope.mData.AppointmentDate = firstDate;
                    $scope.mData.AppointmentTime = timeStart[0];
                    $scope.mData.TargetDateAppointment = FinishDate;
                    $scope.mData.FixedDeliveryTime1 = finaltimeFinishDelivery;
                    $scope.mData.PlanDateFinish = FinishDate;
                    $scope.mData.StatusPreDiagnose = data.StatusPreDiagnose;
                    $scope.mData.PrediagScheduledTime = data.PrediagScheduledTime;
                    tmpPrediagnose.ScheduledTime = data.PrediagScheduledTime;
                    $scope.mData.PrediagStallId = data.PrediagStallId;
                } else {
                    console.log("MAMAMMAMM");
                    // baris di bawah ini di tambahin validasinya,  kalau back dl supaya ga error
                    if (data.pdText !== undefined) {
                        if (tmpPrediagnose.PrediagStallId !== undefined) {
                            tmpPrediagnose.PrediagStallId = 0;
                        }
                    }
                    $scope.mData.StallId = 0;
                    $scope.mData.FullDate = null;
                    $scope.mData.PlanStart = null;
                    $scope.mData.PlanFinish = null;
                    $scope.mData.PlanDateStart = null;
                    //$scope.mData.AppointmentDate = null; 
                    delete $scope.mData.AppointmentDate;
                    //$scope.mData.AppointmentTime = null;
                    delete $scope.mData.AppointmentTime;
                    $scope.mData.TargetDateAppointment = null;
                    $scope.mData.FixedDeliveryTime1 = null;
                    $scope.mData.PlanDateFinish = null;
                    $scope.mData.StatusPreDiagnose = 0;
                    delete $scope.mData.PrediagScheduledTime;
                    delete $scope.mData.PrediagStallId;
                }
            } else {
                if ((data.PrediagStallId !== undefined && data.PrediagScheduledTime !== null) && $scope.gridWork.length == 0) {
                    var dt = new Date(data.PrediagScheduledTime);
                    var dtt = new Date(data.PrediagScheduledTime);
                    dt.setSeconds(0);
                    dtt.setSeconds(0);
                    var timeStart = dt.toTimeString();
                    var timeFinish = dtt.toTimeString();
                    timeStart = timeStart.split(' ');
                    var finaltimeFinish = timeFinish.split(' ');
                    finaltimeFinish = finaltimeFinish[0].split(':')
                    finaltimeFinish = (parseInt(finaltimeFinish[0]) + 1).toString() + ":" + finaltimeFinish[1] + ":" + finaltimeFinish[2];
                    console.log("timeStart", timeStart);
                    console.log("timeFinish", finaltimeFinish);
                    // =====
                    var hourFinish = dtt.getHours();
                    var minuteFinish = dtt.getMinutes();
                    var minuteFinishplus = minuteFinish + 30;
                    var hourFinishplus = hourFinish + 1;
                    if (minuteFinishplus > 59) {
                        hourFinishplus = hourFinish + 1;
                        minuteFinishplus = minuteFinishplus - 60;
                    }
                    if (dtt.getHours() == 12) {
                        hourFinishplus = hourFinish + 1;
                    }
                    var finaltimeFinishDelivery = (hourFinishplus > 9 ? hourFinishplus : "0" + hourFinishplus) + ":" + minuteFinishplus + ":00";
                    // ===============================
                    var yearFirst = dt.getFullYear();
                    var monthFirst = dt.getMonth() + 1;
                    var dayFirst = dt.getDate();
                    var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                    firstDate = $scope.changeFormatDate(dt);
                    // ===============================
                    var yearFinish = dtt.getFullYear();
                    var monthFinish = dtt.getMonth() + 1;
                    var dayFinish = dtt.getDate();
                    var FinishDate = yearFinish + '-' + monthFinish + '-' + dayFinish;
                    FinishDate = $scope.changeFormatDate(dtt);
                    // ===============================
                    // if(timeStart[0] !== null && timeFinish[0] !== null){
                    $scope.mData.StallId = data.PrediagStallId;
                    $scope.mData.FullDate = dt;
                    $scope.mData.PlanStart = timeStart[0];
                    $scope.mData.PlanFinish = finaltimeFinish;
                    $scope.mData.PlanDateStart = firstDate;
                    $scope.mData.AppointmentDate = firstDate;
                    $scope.mData.AppointmentTime = timeStart[0];
                    $scope.mData.TargetDateAppointment = FinishDate;
                    $scope.mData.FixedDeliveryTime1 = finaltimeFinishDelivery;
                    $scope.mData.PlanDateFinish = FinishDate;
                    $scope.mData.StatusPreDiagnose = data.StatusPreDiagnose;
                    $scope.mData.PrediagScheduledTime = data.PrediagScheduledTime;
                    tmpPrediagnose.ScheduledTime = data.PrediagScheduledTime;
                    $scope.mData.PrediagStallId = data.PrediagStallId;
                } else {
                    var info = AsbGrFactory.getInfoIsSplit($scope.boardName);
                    var dataSplitChip = AsbGrFactory.getChipData($scope.boardName);
                    console.log("PLOTING CAMPURAN");
                    if (info.sisaDurasi == 0) {
                        dataSplitChip = dataSplitChip.split(';');
                        console.log("datasplithchip", dataSplitChip);
                        if (dataSplitChip.length > 1) {
                            // var idxLastDataSplitChip = dataSplitChip[dataSplitChip.length - 1];
                            var tmpFirstDataSplitChip = dataSplitChip[0].split('|');
                            var tmpLastDataSplitChip = dataSplitChip[dataSplitChip.length - 1].split('|');
                            var dateLastSplitChip = new Date(tmpLastDataSplitChip[0]);
                            var timeLastSplitchip = tmpLastDataSplitChip[2];
                            timeLastSplitchip = timeLastSplitchip.split(":");

                            var menitPlus = parseInt(timeLastSplitchip[1]) + 30;
                            var jamPlus = parseInt(timeLastSplitchip[0]);
                            if (menitPlus > 59) {
                                jamPlus = jamPlus + 1;
                                menitPlus = menitPlus - 60;
                            }
                            if (jamPlus == 12) {
                                jamPlus = jamPlus + 1;
                            }
                            var dateStart1 = new Date(tmpFirstDataSplitChip[0] + " " + tmpFirstDataSplitChip[1]);
                            var dateFinish1 = new Date(tmpFirstDataSplitChip[0] + " " + tmpFirstDataSplitChip[2]);
                            var dateStart2 = new Date(tmpLastDataSplitChip[0] + " " + tmpLastDataSplitChip[1]);
                            var dateFinish2 = new Date(tmpLastDataSplitChip[0] + " " + tmpLastDataSplitChip[2]);

                            var finaltimeFinishDelivery = (jamPlus > 9 ? jamPlus : "0" + jamPlus) + ":" + (menitPlus == 0 ? "00" : menitPlus) + ":00";
                            console.log("menitPlus === > ", menitPlus);
                            console.log("jamPlus === > ", jamPlus);
                            console.log("dateStart1 === > ", dateStart1);
                            $scope.mData.AppointmentDateTmp = $scope.changeFormatDate(dateStart1);
                            $scope.mData.TargetDateAppointmentTemp = $scope.changeFormatDate(dateFinish2);
                            $scope.mData.FixedTmp = finaltimeFinishDelivery;
                            $scope.mData.StallId = tmpFirstDataSplitChip[3];
                            $scope.mData.FullDate = new Date(dateStart1);
                            $scope.mData.PlanStart = tmpFirstDataSplitChip[1];
                            $scope.mData.PlanFinish = tmpFirstDataSplitChip[2];
                            $scope.mData.PlanDateStart = $scope.changeFormatDate(dateStart1);
                            $scope.mData.PlanDateFinish = $scope.changeFormatDate(dateFinish2);
                            $scope.mData.AppointmentDate = $scope.changeFormatDate(dateStart1);
                            $scope.mData.AppointmentTime = tmpFirstDataSplitChip[1]
                            var strTmp = angular.copy(tmpFirstDataSplitChip[1]);
                            var resTimeJam = strTmp.slice(0, 5);
                            $scope.mData.AppointmentTimeTmp = resTimeJam;
                            $scope.mData.TargetDateAppointment = $scope.changeFormatDate(dateFinish2);
                            $scope.mData.FixedDeliveryTime1 = finaltimeFinishDelivery;
                            $scope.mData.StatusPreDiagnose = data.StatusPreDiagnose;
                            $scope.mData.PrediagScheduledTime = data.PrediagScheduledTime;
                            $scope.mData.PrediagStallId = data.PrediagStallId;
                            tmpPrediagnose.ScheduledTime = data.PrediagScheduledTime;

                        } else {
                            console.log("plotting stall");
                            var dt = new Date(data.startTime);
                            var dtt = new Date(data.endTime);
                            console.log("date", dt);
                            dt.setSeconds(0);
                            dtt.setSeconds(0);
                            var timeStart = dt.toTimeString();
                            var timeFinish = dtt.toTimeString();
                            timeStart = timeStart.split(' ');
                            timeFinish = timeFinish.split(' ');

                            var hourFinish = dtt.getHours();
                            var minuteFinish = dtt.getMinutes();
                            var minuteFinishplus = minuteFinish + 30;
                            var hourFinishplus = hourFinish;
                            if (minuteFinishplus > 59) {
                                hourFinishplus = hourFinish + 1;
                                minuteFinishplus = minuteFinishplus - 60;
                            }
                            if (dtt.getHours() == 12) {
                                hourFinishplus = hourFinish + 1;
                            }
                            // var finaltimeFinishDelivery = (hourFinishplus > 9 ? hourFinishplus : "0" + hourFinishplus) + ":" + minuteFinishplus + ":00";
                            var finaltimeFinishDelivery = (hourFinishplus > 9 ? hourFinishplus : "0" + hourFinishplus) + ":" + (minuteFinishplus == 0 ? "00" : minuteFinishplus) + ":00";

                            console.log("finaltimeFinishDelivery", finaltimeFinishDelivery);
                            var str = angular.copy(finaltimeFinishDelivery);
                            var resTimeTmp = str.slice(0, 5);
                            console.log('resTimeTmp', resTimeTmp);
                            $scope.mData.FixedTmp = resTimeTmp;
                            // ===============================
                            var yearFirst = dt.getFullYear();
                            var monthFirst = dt.getMonth() + 1;
                            var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                            var dayFirst = dt.getDate();
                            var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                            var firstDate = yearFirst + '/' + monthFirsts + '/' + dayFirsts;
                            var firstDateTemp = dayFirsts + '/' + monthFirsts + '/' + yearFirst;
                            // var yearFirst = dt.getFullYear();
                            // var monthFirst = dt.getMonth() + 1;
                            // var dayFirst = dt.getDate();
                            // var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                            // var firstDateTemp = dayFirst + '-' + monthFirst + '-' + yearFirst;
                            $scope.mData.AppointmentDateTmp = firstDateTemp;
                            firstDate = $scope.changeFormatDate(dt);
                            // ===============================
                            var yearFinish = dtt.getFullYear();
                            var monthFinish = dtt.getMonth() + 1;
                            var monthFinishs = monthFinish < 10 ? '0' + monthFinish : monthFinish;
                            var dayFinish = dtt.getDate();
                            var dayFinishs = dayFinish < 10 ? '0' + dayFinish : dayFinish;
                            var firstDate = yearFirst + '/' + monthFirsts + '/' + dayFirsts;
                            var firstDateTemp = dayFirsts + '/' + monthFirsts + '/' + yearFirst;
                            var FinishDateTemp = dayFinishs + '/' + monthFinishs + '/' + yearFinish;
                            // var yearFinish = dtt.getFullYear();
                            // var monthFinish = dtt.getMonth() + 1;
                            // var dayFinish = dtt.getDate();
                            // var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                            // var firstDateTemp = dayFirst + '-' + monthFirst + '-' + yearFirst;
                            $scope.mData.TargetDateAppointmentTemp = FinishDateTemp;
                            FinishDate = $scope.changeFormatDate(dtt);
                            // ===============================
                            // if(timeStart[0] !== null && timeFinish[0] !== null){
                            $scope.mData.StallId = data.stallId;
                            $scope.mData.FullDate = dt;
                            $scope.mData.PlanStart = timeStart[0];
                            $scope.mData.PlanFinish = timeFinish[0];
                            $scope.mData.PlanDateStart = firstDate;
                            $scope.mData.AppointmentDate = firstDate;
                            $scope.mData.AppointmentTime = timeStart[0];
                            console.log("timeStart[0]", timeStart[0]);
                            var strTmp = angular.copy(timeStart[0]);
                            var resTimeJam = strTmp.slice(0, 5);
                            $scope.mData.AppointmentTimeTmp = resTimeJam;
                            $scope.mData.TargetDateAppointment = FinishDate;
                            $scope.mData.FixedDeliveryTime1 = finaltimeFinishDelivery;
                            $scope.mData.PlanDateFinish = FinishDate;
                            $scope.mData.StatusPreDiagnose = data.StatusPreDiagnose;
                            $scope.mData.PrediagScheduledTime = data.PrediagScheduledTime;
                            $scope.mData.PrediagStallId = data.PrediagStallId;
                            tmpPrediagnose.ScheduledTime = data.PrediagScheduledTime;
                            // tmpPrediagnose.ScheduledTime = data.PrediagScheduledTime;
                            // tmpPrediagnose.StallId = data.PrediagStallId;
                            console.log('$scope.mData.TargetDateAppointment', $scope.mData.TargetDateAppointment, $scope.TargetDateAppointmentTemp);
                        }
                    }
                }
            }

            // }

            // }
        };
        //----------------------------------
        // Initialization
        //----------------------------------

        //----------------------------------
        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        $scope.tmpServiceRate = null;
        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.DateOptionsASB = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.DateFilterOptions1 = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.DateFilterOptions2 = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.modeDetail = null;
        var changeId = null;
        $scope.selectCategory = function(selected) {
            // if (selected.Id < 4) {
            $scope.filter.firstDate = null;
            $scope.filter.lastDate = null;
            // }
            $scope.grid.data = [];
            console.log("selected.Id : ", selected.Id);
            changeId = selected.Id;
            var currentDateOptionUpcoming = new Date();
            var currentDateOptionHistory = new Date();
            var maxDateUpcoming = new Date(currentDateOptionUpcoming.setDate(currentDateOptionUpcoming.getDate() + 2));
            var maxDateHistory = new Date();
            // var maxDateHistory = new Date(currentDateOptionHistory.setDate(currentDateOptionHistory.getDate() - 1));
            console.log("maxDateUpcoming", maxDateUpcoming);
            console.log("maxDateHistory", maxDateHistory);
            if (selected.Id == 4 || selected.Id == 5) {
                $scope.DateFilterOptions1.minDate = null;
                $scope.DateFilterOptions1.maxDate = maxDateHistory;
                $scope.DateFilterOptions2.minDate = null;
                $scope.DateFilterOptions2.maxDate = maxDateHistory;
            } else if (selected.Id == 6) {
                $scope.DateFilterOptions1.minDate = maxDateUpcoming;
                $scope.DateFilterOptions1.maxDate = null;
                $scope.DateFilterOptions2.minDate = maxDateUpcoming;
                $scope.DateFilterOptions2.maxDate = null;
            }

        };
        // added by sss on 2018-02-20
        $scope.startDateChange = function(selected) {
                console.log(selected.dt)
                if (changeId == 4 || changeId == 5) {
                    console.log("changeId == 4 | 5");
                    $scope.DateFilterOptions2.minDate = selected.dt;
                } else if (changeId == 6) {
                    console.log("changeId == 6");
                    $scope.DateFilterOptions2.minDate = selected.dt;
                    $scope.DateFilterOptions2.maxDate = null;
                }
            }
            // 

        $scope.asbTab = {
                currentDate: new Date(),
                showMode: 'main',
                nopol: "D 1234 ABC",
                tipe: "Avanza"
            }
            //$scope.asbTab.currentDate.setHours($scope.asbTab.currentDate.getHours()+1);
        console.log("$scope.asbTab.currentDate", $scope.asbTab.currentDate);
        console.log("$scope.dateOptions", $scope.dateOptions);
        $scope.infoTab = {
            today: new Date(),
            allAppointment: 0,
            noShowAppointment: 0,
        }
        $scope.asbTab.startDate = new Date();
        $scope.asbTab.endDate = new Date();
        $scope.asbTab.endDate.setDate($scope.asbTab.endDate.getDate() + 7);
        var allBoardHeight = 500;

        $scope.$watch('asb.showMode', function(newValue, oldValue) {
            if (newValue !== oldValue) {
                console.log('Changed! ' + newValue);
                $scope.asbTab.reloadBoard();
            }
        });

        // console.log("CurrentUser.user()", CurrentUser.user());

        var vmsgname = 'joblist';
        var voutletid = $scope.user.OrgId;
        var vgroupname = voutletid + '.' + vmsgname;
        var vappname = vmsgname;
        // try {
        //     $rootScope.ChatProxy.invoke('joinRoom', vgroupname);
        // } catch (exc) {
        //     $scope.showMsg = {
        //         message: 'Tidak dapat terhubung dengan SignalR server.',
        //         title: 'Koneksi SignalR gagal.',
        //         closeText: 'Close',
        //         exception: exc
        //     }
        //     ngDialog.openConfirm({
        //         width: 700,
        //         template: 'app/boards/factories/showMessage.html',
        //         plain: false,
        //         scope: $scope,
        //     })

        // };

        $scope.$on('SR-' + vappname, function(e, data) {
            console.log("notif-data", data);
            var vparams = data.Message.split('.');

            var vJobId = 0;
            if (typeof vparams[0] !== 'undefined') {
                var vJobId = vparams[0];
            }

            var vStatus = 0;
            if (typeof vparams[1] !== 'undefined') {
                var vStatus = vparams[1];
            }


            var jobid = vparams[0];
            var vcounter = vparams[1];
            var vJobList = ["0", "1", "2", "3", "4", "18", "20"];
            var xStatus = vStatus + '';

            if (vJobList.indexOf(xStatus) >= 0) {
                var vobj = {
                    mode: $scope.asbTab.showMode,
                    currentDate: $scope.asbTab.currentDate,
                    // nopol: $scope.asbTab.nopol,
                    // tipe: $scope.asbTab.tipe
                }
                showBoardTab(vobj);
            }

        });
        var timeActive = true;
        var minuteTick = function() {
            if (!timeActive) {
                console.log('time Active');
                return;
            }
            if (TabGrFactory.chipLoaded) {
                TabGrFactory.updateTimeLine();
                $timeout(function() {
                    minuteTick();
                }, 60000);
            } else {
                $timeout(function() {
                    minuteTick();
                }, 500);
            }
        };

        // var minuteTick = function(){
        //     if (!timeActive){
        //         console.log('time Active');
        //         return;
        //     }
        //     TabGrFactory.updateTimeLine();
        //     $timeout( function(){
        //         minuteTick();
        //     }, 60000);
        // }

        // $scope.$on('$viewContentLoaded', function() {
        //     //$scope.asbTab.reloadBoard = function(){
        //         $timeout( function(){
        //             var vobj = {
        //                 mode: $scope.asbTab.showMode,
        //                 currentDate: $scope.asbTab.currentDate,
        //                 // nopol: $scope.asbTab.nopol,
        //                 // tipe: $scope.asbTab.tipe
        //             }
        //             showBoardTab(vobj);

        //             // $timeout( function(){
        //             //     minuteTick();
        //             // }, 100);
        //             minuteTick();

        //         }, 100);
        //     //}
        // });

        $scope.$on('$destroy', function() {
            timeActive = false;
        });


        // $scope.asbTab.FullScreen = function(){
        //     // alert('full screen');
        //     var vobj = document.getElementById('asb_estimasi_view');
        //     vobj.style.cssText = 'position: absolute; left: 0; top: 0; width: 100%; height: 100%;';
        // }


        angular.element($window).bind('resize', function() {
            //AsbEstimasiFactory.resizeBoard();
            // AsbProduksiFactory.resizeBoard();

            // jpcbStopage.autoResizeStage();
            // jpcbStopage.redraw();
        });

        var showBoardTab = function(vitem) {
            vitem.infoTab = $scope.infoTab;
            TabGrFactory.showBoard({
                container: 'tab-board-gr',
                currentChip: vitem
            });

        };
        $rootScope.tabDoRefresh = function() {
                var vobj = {
                    mode: $scope.asbTab.showMode,
                    currentDate: $scope.asbTab.currentDate,
                    // nopol: $scope.asbTab.nopol,
                    // tipe: $scope.asbTab.tipe
                }
                showBoardTab(vobj);
            }
            //----------------------------------
        $scope.mData = null; //Model
        $scope.xData = {};
        $scope.xData.selected = [];
        $scope.filter = {};
        $scope.selectedRows = {};
        $scope.checkAsb = false;
        $scope.tabShowed = false;
        $scope.jobAvailable = false;
        $scope.partsAvailable = false;
        $scope.tmpActRate = 1;
        $scope.totalWork = 0;
        $scope.totalMaterialDiscounted = 0;
        $scope.subTotalMaterialSummary = 0;
        $scope.totalWorkDiscounted = 0;
        $scope.subTotalWorklSummary = 0;
        $scope.totalPPN = 0;
        $scope.totalEstimasi = 0;
        $scope.DisplayDiscountTask = 0;
        $scope.DisplayDiscountParts = 0;
        $scope.costMaterial = 0;
        $scope.summaryJoblist = 0;
        $scope.totalMaterial = 0;
        $scope.totalDp = 0;
        $scope.formApi = {};
        var DataPersonal = {};
        $scope.disT1 = true;
        $scope.show_modal = {
            show: false
        };
        $scope.show_modalApproval = {
            show: false
        };
        $scope.modelData = {};
        $scope.isReject = false;
        $scope.actBtnCaption = { approve: 'Approve', reject: 'Reject' };
        var Parts = [];
        var tmpPrediagnose = {};
        $scope.showPrediagnose = false;
        $scope.firstAppointmentDate = null;
        $scope.firstPlanStart = null;
        $scope.firstPlanFinish = null;
        $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
        $scope.modalMode = 'new';
        $scope.jobAvailable = false;
        $scope.partsAvailable = false;
        $scope.FlatRateAvailable = false;
        $scope.PriceAvailable = false;
        $scope.ComplaintCategory = [{
                ComplaintCatg: 'Body'
            },
            {
                ComplaintCatg: 'Body Electrical'
            },
            {
                ComplaintCatg: 'Brake'
            },
            { ComplaintCatg: 'Chassis' },
            {
                ComplaintCatg: 'Drive Train'
            },
            {
                ComplaintCatg: 'Engine'
            },
            {
                ComplaintCatg: 'Heater System & AC'
            },
            {
                ComplaintCatg: 'Restraint'
            },
            {
                ComplaintCatg: 'Steering'
            },
            {
                ComplaintCatg: 'Suspension and Axle'
            },
            {
                ComplaintCatg: 'Transmission'
            },
        ];
        $scope.JobRequest = [];
        $scope.JobComplaint = [];
        $scope.listButtonSettings = {
            new: {
                enable: true,
                icon: "fa fa-fw fa-car"
            },
            // view:{enable:false},
            // delete:{enable:false},
            // select:{enable:false},
            // selectall:{enable:false},
            // edit:{enable:false},
        };
        $scope.listCustomButtonSettings = {
            enable: true,
            icon: "fa fa-fw fa-car",
            func: function(row) {
                console.log("customButton", row);
            }
        };
        $scope.totalDpDefault = 0;
        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        var gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
            '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Lihat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-hide="grid.appScope.filter.typeCategory == 4" ng-click="grid.appScope.onTimeAppointment(row.entity)" uib-tooltip="Konfirmasi" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-check-circle-o" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-hide="grid.appScope.filter.typeCategory == 4" ng-click="grid.appScope.cancelAppointment(row.entity)" uib-tooltip="Batal" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-times-circle-o" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-hide="grid.appScope.filter.typeCategory == 4" ng-click="grid.appScope.actEditAppointment(row.entity)" uib-tooltip="Edit" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-hide="grid.appScope.filter.typeCategory == 4" ng-click="grid.appScope.actPrintAppointment(row.entity)" uib-tooltip="Print" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-print" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';
        $scope.checkApprove = function() {
            if ($scope.user.RoleId == 1128 || $scope.user.RoleId == 1174 || $scope.user.RoleId == 1021) {
                $scope.allowApprove = true;
            } else {
                $scope.allowApprove = false;
            }
        };
        $scope.actionButtonSettingsDetail = [{
                func: function(row, formScope) {

                    console.log('click simpan aja terus', $scope.actionButtonSettingsDetail)

                    if ($scope.loadingSimpan > 0) {
                        bsNotify.show({
                            size: 'small',
                            type: 'danger',
                            title: "Data sedang proses simpan..",
                            // timeout: 1000,
                        });
                        return false;
                    } else {
                        $scope.loadingSimpan++
                        $timeout(function() {
                            $scope.loadingSimpan = 0
                        }, 30000);

                        console.log("mData", $scope.mData);
                        console.log("mdata di row", row);

                        var tmpKm = angular.copy($scope.mData.Km).toString();
                        var backupKm = angular.copy($scope.mData.Km);
                        if (tmpKm.includes(",") == true) {
                            tmpKm = tmpKm.split(',');
                            if (tmpKm.length > 1) {
                                tmpKm = tmpKm.join('');
                            } else {
                                tmpKm = tmpKm[0];
                            }
                        }
                        tmpKm = parseInt(tmpKm);

                        if (tmpKm < $scope.KMTerakhir && $scope.KMTerakhir < 999999) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "KM tidak boleh lebih kecil dari " + $scope.KMTerakhir 
                            });
                            $scope.loadingSimpan = 0
                            return false;
                        }
                        
                        WoHistoryGR.getStatWO(row.JobId, row.Status).then(function(resux) {
                            if($scope.gridWork.length > 0){

                                for (var i = 0; i < $scope.gridWork.length; i++) {
                                    $scope.gridWork[i].DiscountTypeId = $scope.gridWork[i].typeDiskon;
                                    for(var j = 0; j < $scope.gridWork[i].child.length;j++){

                                        $scope.gridWork[i].child[j].DiscountTypeId = $scope.gridWork[i].child[j].typeDiskon;
                                    }														
                                }
                            }

                            console.log("resux", resux.data);
                            if (resux.data == 0){
                                var tmpGrid = [];
                                tmpGrid = angular.copy($scope.gridWork);
                                // if ($scope.gridWork.length > 0 && $scope.mData.AppointmentDate !== null && $scope.mData.ContactPerson !== null && $scope.mData.PhoneContactPerson1 !== null && $scope.mData.Category !== null && $scope.mData.Km !== null) {
                                if (($scope.mData.AppointmentDate !== null && $scope.mData.AppointmentDate !== undefined)  && ($scope.mData.ContactPerson !== null && $scope.mData.ContactPerson !== undefined) && ($scope.mData.PhoneContactPerson1 !== null && $scope.mData.PhoneContactPerson1) && ($scope.mData.Km !== null &&  $scope.mData.Km !== undefined)) {
                                    for (var i = 0; i < tmpGrid.length; i++) {
                                        // tmpGrid[i].DiscountTypeId = tmpGrid[i].typeDiskon;
                                        tmpGrid[i].JobParts = [];
                                        angular.forEach(tmpGrid[i].child, function(v, k) {
                                            tmpGrid[i].JobParts[k] = v;
                                            tmpGrid[i].JobParts[k].Price = tmpGrid[i].JobParts[k].RetailPrice;
                                            if (tmpGrid[i].JobParts[k].PaidById == 2277 || tmpGrid[i].JobParts[k].PaidById == 30 || tmpGrid[i].JobParts[k].PaidById == 31) {
                                                tmpGrid[i].JobParts[k].minimalDp = 0;
                                            }
                                            tmpGrid[i].JobParts[k].DPRequest = tmpGrid[i].JobParts[k].DownPayment;
                                            if (tmpGrid[i].JobParts[k].JobPartsId == undefined || tmpGrid[i].JobParts[k].JobPartsId == null) {
                                                tmpGrid[i].JobParts[k].JobPartsId = 0;
                                            }
                                            if (typeof tmpGrid[i].JobParts[k].PartsId == 'string') {
                                                if (tmpGrid[i].JobParts[k].PartsId.includes('#')) {
                                                    delete tmpGrid[i].JobParts[k].PartsId;
                                                } else {
                                                    tmpGrid[i].JobParts[k].PartsId = tmpGrid[i].JobParts[k].PartsId.split("&")[0];
                                                    tmpGrid[i].JobParts[k].PartsId = parseInt(tmpGrid[i].JobParts[k].PartsId);
                                                }

                                            }
                                            delete tmpGrid[i].JobParts[k].ETA;
                                            delete tmpGrid[i].JobParts[k].PaidBy;
                                            // delete tmpGrid[i].JobParts[k].Dp;
                                            delete tmpGrid[i].child;
                                        })
                                        if ($scope.mData.JobId !== undefined && $scope.mData.JobId !== null) {
                                            tmpGrid[i].JobId = $scope.mData.JobId;
                                        } else {
                                            tmpGrid[i].JobId = 0;
                                        }
                                        delete tmpGrid[i].PaidBy;
                                        delete tmpGrid[i].tmpTaskId;

                                    }
                                    console.log("=====>", tmpGrid);
                                    var tmpKm = angular.copy($scope.mData.Km);
                                    var backupKm = angular.copy($scope.mData.Km);
                                    if (tmpKm.toString().includes(",") == true) {
                                        tmpKm = tmpKm.split(',');
                                        if (tmpKm.length > 1) {
                                            tmpKm = tmpKm.join('');
                                        } else {
                                            tmpKm = tmpKm[0];
                                        }
                                    }
                                    tmpKm = parseInt(tmpKm);
                                    $scope.mData.Km = tmpKm;
                                    $scope.mData.JobTask = tmpGrid;
                                    $scope.mData.JobComplaint = $scope.JobComplaint;
                                    $scope.mData.JobRequest = $scope.JobRequest;
                                    // $scope.mDataDetail.LicensePlate.toUpperCase();
                                    $scope.mData.detail = $scope.mDataDetail;
                                    $scope.mData.PoliceNumber = $scope.mDataDetail.LicensePlate.toUpperCase();
                                    $scope.mData.VIN = $scope.mDataDetail.VIN;
                                    var data = $scope.mData;
                                    var tmpAppointmentDate = data.AppointmentDate;
                                    tmpAppointmentDate = new Date(tmpAppointmentDate);
                                    var finalDate
                                    var yyyy = tmpAppointmentDate.getFullYear().toString();
                                    var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                                    var dd = tmpAppointmentDate.getDate().toString();
                                    finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                                    $scope.mData.AppointmentDate = $scope.changeFormatDate($scope.mData.AppointmentDate);
                                    $scope.mData.PlanDateFinish = $scope.changeFormatDate($scope.mData.PlanDateFinish);
                                    $scope.mData.PlanDateStart = $scope.changeFormatDate($scope.mData.PlanDateStart);
                                    tmpPrediagnose.ScheduledTime = $filter('date')($scope.mData.PrediagScheduledTime, 'yyyy-MM-dd HH:mm:ss');
                                    tmpPrediagnose.StallId = $scope.mData.PrediagStallId;
                                    tmpPrediagnose.Status = $scope.mData.StatusPreDiagnose;
                                    // console.log("n", n);
                                    // ===============Data DP ================
                                    $scope.mData.totalDpDefault = $scope.totalDpDefault;
                                    $scope.mData.totalDp = $scope.totalDp;
                                    $scope.mData.FinalDP = $scope.totalDp;
                                    console.log("totalDpDefault: ", $scope.mData.totalDpDefault);
                                    console.log("totalDp: ", $scope.mData.totalDp);
                                    console.log("FinalDP: ", $scope.mData.FinalDP);
                                    // ===============Appointment Date========
                                    var tmpfirstPlanStart = angular.copy($scope.firstPlanStart);
                                    var tmpfirstPlanFinish = angular.copy($scope.firstPlanFinish);
                                    var tmpPlanStart = $scope.mData.PlanStart;
                                    var tmpPlanFinish = $scope.mData.PlanFinish;
                                    tmpfirstPlanStart = tmpfirstPlanStart.split(':');
                                    tmpfirstPlanFinish = tmpfirstPlanFinish.split(':');
                                    tmpPlanStart = tmpPlanStart.split(':');
                                    tmpPlanFinish = tmpPlanFinish.split(':');
                                    var today = new Date();

                                    // $scope.DateSebelumReschedule
                                    console.log('sebelum reschedule bos', $scope.DateSebelumReschedule.getFullYear(), $scope.DateSebelumReschedule.getMonth());
                                    var dataSetelahReschedule = finalDate.split('/');
                                    console.log('asem cok seuda resche', dataSetelahReschedule)


                                    var myFirstPlanStart = new Date($scope.DateSebelumReschedule.getFullYear(), $scope.DateSebelumReschedule.getMonth(), $scope.DateSebelumReschedule.getDate(), tmpfirstPlanStart[0], tmpfirstPlanStart[1], 0);
                                    var myFirstPlanFinish = new Date($scope.DateSebelumReschedule.getFullYear(), $scope.DateSebelumReschedule.getMonth(), $scope.DateSebelumReschedule.getDate(), tmpfirstPlanFinish[0], tmpfirstPlanFinish[1], 0);
                                    var myPlanStart = new Date(parseInt(dataSetelahReschedule[0]), (parseInt(dataSetelahReschedule[1]) - 1), parseInt(dataSetelahReschedule[2]), tmpPlanStart[0], tmpPlanStart[1], 0);
                                    var myPlanFinish = new Date(parseInt(dataSetelahReschedule[0]), (parseInt(dataSetelahReschedule[1]) - 1), parseInt(dataSetelahReschedule[2]), tmpPlanFinish[0], tmpPlanFinish[1], 0);

                                    // var myFirstPlanStart = new Date(today.getFullYear(), today.getMonth(), today.getDate(), tmpfirstPlanStart[0], tmpfirstPlanStart[1], 0);
                                    // var myFirstPlanFinish = new Date(today.getFullYear(), today.getMonth(), today.getDate(), tmpfirstPlanFinish[0], tmpfirstPlanFinish[1], 0);
                                    // var myPlanStart = new Date(today.getFullYear(), today.getMonth(), today.getDate(), tmpPlanStart[0], tmpPlanStart[1], 0);
                                    // var myPlanFinish = new Date(today.getFullYear(), today.getMonth(), today.getDate(), tmpPlanFinish[0], tmpPlanFinish[1], 0);

                                    console.log("$scope.mData ready to save", $scope.mData);
                                    console.log("$scope.totalDpDefault", $scope.totalDpDefault);
                                    console.log("$scope.totalDp", $scope.totalDp);
                                    console.log("tmpPrediagnose ready to save", tmpPrediagnose);

                                    if ($scope.mData.JobDate !== undefined && $scope.mData.JobDate !== null) {
                                        $scope.mData.JobDate = $scope.changeFormatDate($scope.mData.JobDate);
                                    } else {
                                        var dateJob = new Date();
                                        $scope.mData.JobDate = $scope.changeFormatDate(dateJob);
                                    }
                                    // =======================================
                                    if ($scope.totalDpDefault !== 0 && ($scope.totalDp < $scope.totalDpDefault)) {
                                        AppointmentGrService.checkApproval($scope.totalDp).then(function(res) {
                                            console.log("ressss checkApproval", res.data);
                                            var ApproverRoleID = res.data;
                                            if (res.data !== -1) {
                                                if (myFirstPlanStart.getTime() === myPlanStart.getTime() && myFirstPlanFinish.getTime() === myPlanFinish.getTime()) {
                                                    // if (myFirstPlanStart.getTime() === myPlanStart.getTime() && myFirstPlanFinish.getTime() === myPlanFinish.getTime() && new Date($scope.firstAppointmentDate) === new Date($scope.mData.AppointmentDate) ) {
                                                    console.log("ini edit doang harusnya");
                                                    $scope.mData.AppointmentDate = $scope.changeFormatDate($scope.mData.AppointmentDate);
                                                    $scope.mData.PlanDateFinish = $scope.changeFormatDate($scope.mData.PlanDateFinish);
                                                    $scope.mData.PlanDateStart = $scope.changeFormatDate($scope.mData.PlanDateStart);
                                                    FollowUpGrService.update($scope.mData).then(function(res) {
                                                        if (res.data == -1) {
                                                            bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                                            $scope.mData.Km = backupKm;
                                                            $scope.loadingSimpan = 0
                                                        } else if (res.data == -2) {
                                                            bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                                            $scope.mData.Km = backupKm;
                                                            $scope.loadingSimpan = 0
                                                        } else {
                                                            // AppointmentGrService.createPreadiagnose($scope.mData.JobId).then(function(res) {

                                                            // });
                                                            console.log("abis save", res);
                                                            // =====================
                                                            var tmpDataJob = angular.copy($scope.mData);
                                                            var tmpJobId = res.data.ResponseMessage;
                                                            tmpJobId = tmpJobId.split('#');
                                                            console.log("tmpJobId", tmpJobId[1]);
                                                            tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                                            tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                                            console.log("tmpJobId", tmpJobId[1]);
                                                            console.log("tmpPrediagnose", tmpPrediagnose);
                                                            console.log("tmpPrediagnose.PrediagnoseId", tmpPrediagnose.PrediagnoseId);
                                                            // =======================
                                                            $scope.saveApprovalDP(tmpDataJob);
                                                            // =======================
                                                            console.log("tmpPrediagnose", tmpPrediagnose);
                                                            tmpPrediagnose.JobId = parseInt(tmpJobId[1]);
                                                            // 
                                                            if (tmpPrediagnose.PrediagStallId !== undefined && tmpPrediagnose.PrediagStallId !== null) {
                                                                if (tmpPrediagnose.PrediagnoseId !== null && tmpPrediagnose.PrediagnoseId !== undefined) {
                                                                    AppointmentGrService.updatePrediagnose(tmpPrediagnose).then(function(res) {

                                                                    });
                                                                } else {
                                                                    AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                                                    });
                                                                };
                                                            }
                                                            console.log("abis save", res);
                                                            $scope.formApi.setMode('grid');
                                                            $scope.getData();
                                                        }
                                                    }, function(err) {
                                                        $scope.mData.Km = backupKm;
                                                    });
                                                } else {
                                                    console.log("Reschedule ini masuknya");
                                                    var RemoveVehicleType = delete $scope.mData.VehicleType;
                                                    console.log("$scope.mData", $scope.mData);
                                                    $scope.mData.AppointmentDate = $scope.changeFormatDate($scope.mData.AppointmentDate);
                                                    $scope.mData.PlanDateFinish = $scope.changeFormatDate($scope.mData.PlanDateFinish);
                                                    $scope.mData.PlanDateStart = $scope.changeFormatDate($scope.mData.PlanDateStart);
                                                    var splitChip = AsbGrFactory.getChipData($scope.boardName);
                                                    splitChip = splitChip.split(';');
                                                    var tmpJobSplitChip = [];
                                                    for (var i in splitChip) {
                                                        var tmpHasilSplit = splitChip[i].split('|');
                                                        tmpJobSplitChip.push({
                                                            JobSplitId: tmpHasilSplit[5],
                                                            PlanStart: tmpHasilSplit[1],
                                                            PlanFinish: tmpHasilSplit[2],
                                                            PlanDateStart: $scope.changeFormatDate(new Date(tmpHasilSplit[0])),
                                                            PlanDateFinish: $scope.changeFormatDate(new Date(tmpHasilSplit[0])),
                                                            StallId: tmpHasilSplit[3],
                                                            isSplitActive: 1,
                                                            JobId:$scope.mData.JobId
                                                        })
                                                    }
                                                    if (tmpJobSplitChip.length > 1) {
                                                        $scope.mData.JobListSplitChip = tmpJobSplitChip;
                                                    } else {
                                                        $scope.mData.JobListSplitChip = []
                                                    }
                                                    $scope.mData.Stall = null;
                                                    FollowUpGrService.updateReschedule($scope.mData).then(function(res) {
                                                        if (res.data == -1) {
                                                            bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                                            $scope.mData.Km = backupKm;
                                                            $scope.loadingSimpan = 0
                                                        } else if (res.data == -2) {
                                                            bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                                            $scope.mData.Km = backupKm;
                                                            $scope.loadingSimpan = 0
                                                        } else {
                                                            console.log("abis save", res);
                                                            // =====================
                                                            var tmpDataJob = angular.copy($scope.mData);
                                                            var tmpJobId = res.data.ResponseMessage;
                                                            tmpJobId = tmpJobId.split('#');
                                                            console.log("tmpJobId", tmpJobId[1]);
                                                            tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                                            tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                                            console.log("tmpJobId", tmpJobId[1]);
                                                            console.log("tmpPrediagnose", tmpPrediagnose);
                                                            console.log("tmpPrediagnose.PrediagnoseId", tmpPrediagnose.PrediagnoseId);
                                                            // =======================
                                                            // $scope.saveApprovalDP(tmpDataJob);
                                                            // =======================
                                                            console.log("tmpPrediagnose", tmpPrediagnose);
                                                            tmpPrediagnose.JobId = parseInt(tmpJobId[1]);

                                                            if (tmpPrediagnose.PrediagStallId !== undefined && tmpPrediagnose.PrediagStallId !== null) {
                                                                if (tmpPrediagnose.PrediagnoseId !== null && tmpPrediagnose.PrediagnoseId !== undefined) {
                                                                    AppointmentGrService.updatePrediagnose(tmpPrediagnose).then(function(res) {

                                                                    });
                                                                } else {
                                                                    AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                                                    });
                                                                };
                                                            }

                                                            var noAppointment = $scope.mData.AppointmentNo;
                                                            FollowUpGrService.getAppointmentDate(noAppointment).then(function(res) {
                                                                // var dataRes = res.data.Result;
                                                                console.log('Data Appointment Date ', res);
                                                                var hasil = res.data;

                                                                for (var i in hasil) {
                                                                    if (hasil[i].Status === 1) {
                                                                        $scope.tglApp = hasil[i].AppointmentDate;
                                                                        $scope.startApp = hasil[i].PlanStart;
                                                                    } else {
                                                                        $scope.tglRes = hasil[i].AppointmentDate;
                                                                        $scope.startRes = hasil[i].PlanStart;
                                                                    }
                                                                }

                                                                // APPOINTMENT DATE

                                                                var today = new Date($scope.tglApp);
                                                                var year = today.getFullYear();
                                                                var month = today.getMonth() + 1;
                                                                var day = today.getDate();
                                                                month = checkTime(month);
                                                                day = checkTime(day);

                                                                var timeApp = $scope.startApp.split(":");
                                                                var hh = timeApp[0];
                                                                var mm = timeApp[1];

                                                                var lastDateNya = day + '-' + month + '-' + year;
                                                                var timeDateNya = hh + ":" + mm;

                                                                var namaHari = {
                                                                    0: "Minggu",
                                                                    1: "Senin",
                                                                    2: "Selasa",
                                                                    3: "Rabu",
                                                                    4: "Kamis",
                                                                    5: "Jumat",
                                                                    6: "Sabtu"
                                                                };
                                                                var hariKe = today.getDay();
                                                                var hari = namaHari[hariKe];

                                                                // RESCHEDULE

                                                                var today1 = new Date($scope.tglRes);
                                                                var year1 = today1.getFullYear();
                                                                var month1 = today1.getMonth() + 1;
                                                                var day1 = today1.getDate();
                                                                month1 = checkTime(month1);
                                                                day1 = checkTime(day1);

                                                                var timeRes = $scope.startRes.split(":");
                                                                var hh1 = timeRes[0];
                                                                var mm1 = timeRes[1];

                                                                var lastDateNya1 = day1 + '-' + month1 + '-' + year1;
                                                                var timeDateNya1 = hh1 + ":" + mm1;

                                                                var hariKe1 = today1.getDay();
                                                                var hari1 = namaHari[hariKe1];

                                                                console.log('tglApp', $scope.tglApp + " " + $scope.startApp);
                                                                console.log('conv tglApp', hari + ", " + lastDateNya + " " + timeDateNya + " WIB");
                                                                console.log('tglRes', $scope.tglRes + " " + $scope.startRes);
                                                                console.log('conv tglRes', hari1 + ", " + lastDateNya1 + " " + timeDateNya1 + " WIB");

                                                                $scope.dateAppointment = hari + ", " + lastDateNya + " " + timeDateNya + " WIB";
                                                                $scope.dateReschedule = hari1 + ", " + lastDateNya1 + " " + timeDateNya1 + " WIB";

                                                                console.log('dateAppointment', $scope.dateAppointment);
                                                                console.log('dateReschedule', $scope.dateReschedule);

                                                                //kirim notifikasi
                                                                var DataNotif = {};
                                                                var messagetemp = "";
                                                                messagetemp = "No. Appointment : " + $scope.mData.AppointmentNo +
                                                                    " <br> No. Polisi : " + $scope.mData.PoliceNumber
                                                                    // +" <br> Periode Awal : "+ myFirstPlanStart 
                                                                    // +" <br> Periode Akhir : "+ myFirstPlanFinish;
                                                                    +
                                                                    " <br> Tanggal Appointment : " + $scope.dateAppointment +
                                                                    " <br> Tanggal Reschedule : " + $scope.dateReschedule;



                                                                DataNotif.Message = messagetemp;

                                                                console.log('DataNotif ', DataNotif);

                                                                FollowUpGrService.sendNotifRole(DataNotif, 1122, 16).then(
                                                                    function(res) {

                                                                    },
                                                                    function(err) {
                                                                        //console.log("err=>", err);
                                                                    });
                                                            });


                                                            $scope.formApi.setMode('grid');
                                                            $scope.getData();
                                                        }
                                                    }, function(err) {
                                                        $scope.mData.Km = backupKm;
                                                    });
                                                };
                                            } else {
                                                $scope.mData.Km = backupKm;
                                            }
                                        });
                                    } else {
                                        console.log('tes tipe data', myFirstPlanStart.getTime())
                                        if (myFirstPlanStart.getTime() === myPlanStart.getTime() && myFirstPlanFinish.getTime() === myPlanFinish.getTime()) {
                                            // if (myFirstPlanStart.getTime() === myPlanStart.getTime() && myFirstPlanFinish.getTime() === myPlanFinish.getTime() && new Date($scope.firstAppointmentDate) === new Date($scope.mData.AppointmentDate) ) {

                                            console.log("ini edit doang harusnya");
                                            $scope.mData.AppointmentDate = $scope.changeFormatDate($scope.mData.AppointmentDate);
                                            $scope.mData.PlanDateFinish = $scope.changeFormatDate($scope.mData.PlanDateFinish);
                                            $scope.mData.PlanDateStart = $scope.changeFormatDate($scope.mData.PlanDateStart);
                                            // if (AsbGrFactory.checkChipFinished($scope.boardName)) {
                                            // ==== COmmend DULU ====
                                            // var splitChip = AsbGrFactory.getChipData($scope.boardName);
                                            // splitChip = splitChip.split(';');
                                            var tmpJobSplitChip = [];
                                            // for (var i in splitChip) {
                                            //     var tmpHasilSplit = splitChip[i].split('|');
                                            //     tmpJobSplitChip.push({
                                            //         JobSplitId: tmpHasilSplit[5],
                                            //         PlanStart: tmpHasilSplit[1],
                                            //         PlanFinish: tmpHasilSplit[2],
                                            //         PlanDateStart: $scope.changeFormatDate(new Date(tmpHasilSplit[0])),
                                            //         PlanDateFinish: $scope.changeFormatDate(new Date(tmpHasilSplit[0])),
                                            //         StallId: tmpHasilSplit[3],
                                            //         isSplitActive: 1,
                                            //         JobId:$scope.mData.JobId
                                            //     })
                                            // }
                                            if (tmpJobSplitChip.length > 1) {
                                                $scope.mData.JobListSplitChip = tmpJobSplitChip;
                                            } else {
                                                $scope.mData.JobListSplitChip = []
                                            }
                                            console.log("ini edit doang harusnya", $scope.mData);

                                            FollowUpGrService.update($scope.mData).then(function(res) {
                                                if (res.data == -1) {
                                                    bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                                    $scope.mData.Km = backupKm;
                                                    $scope.loadingSimpan = 0
                                                } else if (res.data == -2) {
                                                    bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                                    $scope.mData.Km = backupKm;
                                                    $scope.loadingSimpan = 0
                                                } else {
                                                    // AppointmentGrService.createPreadiagnose($scope.mData.JobId).then(function(res) {

                                                    // });
                                                    console.log("abis save", res);
                                                    // =====================
                                                    var tmpDataJob = angular.copy($scope.mData);
                                                    var tmpJobId = res.data.ResponseMessage;
                                                    tmpJobId = tmpJobId.split('#');
                                                    console.log("tmpJobId", tmpJobId[1]);
                                                    tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                                    // tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                                    console.log("tmpJobId", tmpJobId[1]);
                                                    console.log("tmpPrediagnose", tmpPrediagnose);
                                                    console.log("tmpPrediagnose.PrediagnoseId", tmpPrediagnose.PrediagnoseId);
                                                    // =======================
                                                    // $scope.saveApprovalDP(tmpDataJob);
                                                    // =======================
                                                    console.log("tmpPrediagnose", tmpPrediagnose);
                                                    tmpPrediagnose.JobId = parseInt(tmpJobId[1]);
                                                    // 
                                                    if (tmpPrediagnose.PrediagStallId !== undefined && tmpPrediagnose.PrediagStallId !== null) {
                                                        if (tmpPrediagnose.PrediagnoseId !== null && tmpPrediagnose.PrediagnoseId !== undefined) {
                                                            AppointmentGrService.updatePrediagnose(tmpPrediagnose).then(function(res) {

                                                            });
                                                        } else {
                                                            AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                                            });
                                                        };
                                                    }
                                                    console.log("abis save", res);
                                                    $scope.formApi.setMode('grid');
                                                    $scope.getData();
                                                }
                                            }, function(err) {
                                                $scope.mData.Km = backupKm;
                                            });
                                        } else {
                                            console.log("Reschedule ini masuknya");
                                            var RemoveVehicleType = delete $scope.mData.VehicleType;
                                            console.log("$scope.mData", $scope.mData);
                                            $scope.mData.AppointmentDate = $scope.changeFormatDate($scope.mData.AppointmentDate);
                                            $scope.mData.PlanDateFinish = $scope.changeFormatDate($scope.mData.PlanDateFinish);
                                            $scope.mData.PlanDateStart = $scope.changeFormatDate($scope.mData.PlanDateStart);
                                            var splitChip = AsbGrFactory.getChipData($scope.boardName);
                                            splitChip = splitChip.split(';');
                                            var tmpJobSplitChip = [];
                                            for (var i in splitChip) {
                                                var tmpHasilSplit = splitChip[i].split('|');
                                                tmpJobSplitChip.push({
                                                    JobSplitId: tmpHasilSplit[5],
                                                    PlanStart: tmpHasilSplit[1],
                                                    PlanFinish: tmpHasilSplit[2],
                                                    PlanDateStart: $scope.changeFormatDate(new Date(tmpHasilSplit[0])),
                                                    PlanDateFinish: $scope.changeFormatDate(new Date(tmpHasilSplit[0])),
                                                    StallId: tmpHasilSplit[3],
                                                    isSplitActive: 1,
                                                    JobId:$scope.mData.JobId
                                                })
                                            }
                                            if (tmpJobSplitChip.length > 1) {
                                                $scope.mData.JobListSplitChip = tmpJobSplitChip;
                                            } else {
                                                $scope.mData.JobListSplitChip = []
                                            }
                                            $scope.mData.Stall = null;
                                            FollowUpGrService.updateReschedule($scope.mData).then(function(res) {
                                                if (res.data == -1) {
                                                    bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                                    $scope.mData.Km = backupKm;
                                                    $scope.loadingSimpan = 0
                                                } else if (res.data == -2) {
                                                    bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                                    $scope.mData.Km = backupKm;
                                                    $scope.loadingSimpan = 0
                                                } else {
                                                    console.log("abis save", res);
                                                    // =====================
                                                    var tmpDataJob = angular.copy($scope.mData);
                                                    var tmpJobId = res.data.ResponseMessage;
                                                    tmpJobId = tmpJobId.split('#');
                                                    console.log("tmpJobId", tmpJobId[1]);
                                                    tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                                    // tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                                    console.log("tmpJobId", tmpJobId[1]);
                                                    console.log("tmpPrediagnose", tmpPrediagnose);
                                                    console.log("tmpPrediagnose.PrediagnoseId", tmpPrediagnose.PrediagnoseId);
                                                    // =======================
                                                    $scope.saveApprovalDP(tmpDataJob);
                                                    // =======================
                                                    console.log("tmpPrediagnose", tmpPrediagnose);
                                                    tmpPrediagnose.JobId = parseInt(tmpJobId[1]);

                                                    if (tmpPrediagnose.PrediagStallId !== undefined && tmpPrediagnose.PrediagStallId !== null) {
                                                        if (tmpPrediagnose.PrediagnoseId !== null && tmpPrediagnose.PrediagnoseId !== undefined) {
                                                            AppointmentGrService.updatePrediagnose(tmpPrediagnose).then(function(res) {

                                                            });
                                                        } else {
                                                            AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                                            });
                                                        };
                                                    }

                                                    var noAppointment = $scope.mData.AppointmentNo;
                                                    FollowUpGrService.getAppointmentDate(noAppointment).then(function(res) {
                                                        // var dataRes = res.data.Result;
                                                        console.log('Data Appointment Date ', res);
                                                        var hasil = res.data;

                                                        for (var i in hasil) {
                                                            if (hasil[i].Status === 1) {
                                                                $scope.tglApp = hasil[i].AppointmentDate;
                                                                $scope.startApp = hasil[i].PlanStart;
                                                            } else {
                                                                $scope.tglRes = hasil[i].AppointmentDate;
                                                                $scope.startRes = hasil[i].PlanStart;
                                                            }
                                                        }

                                                        // APPOINTMENT DATE

                                                        var today = new Date($scope.tglApp);
                                                        var year = today.getFullYear();
                                                        var month = today.getMonth() + 1;
                                                        var day = today.getDate();
                                                        month = checkTime(month);
                                                        day = checkTime(day);

                                                        var timeApp = $scope.startApp.split(":");
                                                        var hh = timeApp[0];
                                                        var mm = timeApp[1];

                                                        var lastDateNya = day + '-' + month + '-' + year;
                                                        var timeDateNya = hh + ":" + mm;

                                                        var namaHari = {
                                                            0: "Minggu",
                                                            1: "Senin",
                                                            2: "Selasa",
                                                            3: "Rabu",
                                                            4: "Kamis",
                                                            5: "Jumat",
                                                            6: "Sabtu"
                                                        };
                                                        var hariKe = today.getDay();
                                                        var hari = namaHari[hariKe];

                                                        // RESCHEDULE

                                                        var today1 = new Date($scope.tglRes);
                                                        var year1 = today1.getFullYear();
                                                        var month1 = today1.getMonth() + 1;
                                                        var day1 = today1.getDate();
                                                        month1 = checkTime(month1);
                                                        day1 = checkTime(day1);

                                                        var timeRes = $scope.startRes.split(":");
                                                        var hh1 = timeRes[0];
                                                        var mm1 = timeRes[1];

                                                        var lastDateNya1 = day1 + '-' + month1 + '-' + year1;
                                                        var timeDateNya1 = hh1 + ":" + mm1;

                                                        var hariKe1 = today1.getDay();
                                                        var hari1 = namaHari[hariKe1];

                                                        console.log('tglApp', $scope.tglApp + " " + $scope.startApp);
                                                        console.log('conv tglApp', hari + ", " + lastDateNya + " " + timeDateNya + " WIB");
                                                        console.log('tglRes', $scope.tglRes + " " + $scope.startRes);
                                                        console.log('conv tglRes', hari1 + ", " + lastDateNya1 + " " + timeDateNya1 + " WIB");

                                                        $scope.dateAppointment = hari + ", " + lastDateNya + " " + timeDateNya + " WIB";
                                                        $scope.dateReschedule = hari1 + ", " + lastDateNya1 + " " + timeDateNya1 + " WIB";

                                                        console.log('dateAppointment', $scope.dateAppointment);
                                                        console.log('dateReschedule', $scope.dateReschedule);

                                                        //kirim notifikasi
                                                        var DataNotif = {};
                                                        var messagetemp = "";
                                                        messagetemp = "No. Appointment : " + $scope.mData.AppointmentNo +
                                                            " <br> No. Polisi : " + $scope.mData.PoliceNumber
                                                            // +" <br> Periode Awal : "+ myFirstPlanStart 
                                                            // +" <br> Periode Akhir : "+ myFirstPlanFinish;
                                                            +
                                                            " <br> Tanggal Appointment : " + $scope.dateAppointment +
                                                            " <br> Tanggal Reschedule : " + $scope.dateReschedule;



                                                        DataNotif.Message = messagetemp;

                                                        console.log('DataNotif ', DataNotif);

                                                        FollowUpGrService.sendNotifRole(DataNotif, 1122, 16).then(
                                                            function(res) {

                                                            },
                                                            function(err) {
                                                                //console.log("err=>", err);
                                                            });
                                                    });


                                                    $scope.formApi.setMode('grid');
                                                    $scope.getData();
                                                }
                                            }, function(err) {
                                                $scope.mData.Km = backupKm;
                                            });
                                        };
                                    }
                                } else {
                                    if(($scope.mData.AppointmentDate !== null || $scope.mData.AppointmentDate !== undefined)){
                                        bsAlert.warning("Silahkan Plotting Ulang", "silahkan cek kembali");
                                        $scope.loadingSimpan = 0
                                    }else{
                                        bsAlert.warning("Form masih ada yang belum terisi", "silahkan cek kembali");
                                        $scope.loadingSimpan = 0
                                    }
                                    
                                };
                            } else {
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Status Appointment sudah berubah, silahkan refresh kembali"
                                });
                                $scope.loadingSimpan = 0
                            }
                        })

                    }

                    
                    
                },
                title: 'Simpan',
                icon: 'fa fa-fw fa-save',
                //rightsBit: 1
            },
            {
                actionType: 'back', //Use 'Back Action' of bsForm
                title: 'Back',
                func: function(row, formScope) {

                    }
                    //icon: 'fa fa-fw fa-edit',
            },
        ]

        $scope.doCancelAppointment = function(item) {
            WoHistoryGR.getStatWO(item.JobId, item.Status).then(function(ress){
                if (ress.data == 0) {
                    console.log("doCancelAppointment Item", item);
                    $scope.totalDPAlert = 0;

                    FollowUpGrService.getJobParts(item.JobId).then(function(resu) {
                        console.log("form onSelectRows getJobParts cancel=>", resu.data.Result);
                        var data = resu.data.Result;
                        for (var i in data) {
                            $scope.totalDPAlert += data[i].FinalDP;
                            console.log('totalDPAlert alert', $scope.totalDPAlert);
                        }

                        if (item.Reason !== undefined && item.Reason !== "") {

                            if ($scope.totalDPAlert !== 0) {

                                var totaldp_withseparator = angular.copy($scope.totalDPAlert)
                                totaldp_withseparator = totaldp_withseparator.toString().replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, '.');

                                bsAlert.alert({
                                        title: "Apakah anda yakin?",
                                        text: "Cancel appointment dengan DP Rp. " + totaldp_withseparator + " ?",
                                        type: "question",
                                        showCancelButton: true,
                                        confirmButtonText: "Ya",
                                        cancelButtonText: "Tidak"
                                    },
                                    function() {
                                        if (item.IsAppointmentDP == 1) {
                                            // bsAlert.warning('Tidak bisa membatalkan appointment karena sudah ada DP yang dibayar')
                                            // $scope.show_modal = {
                                            //     show: false
                                            // };

                                            // bsAlert.warning('Silakan hubungi Administrasi Finance untuk laporan titipan dan pengembalian DP')
                                            // $scope.cancelService(item);

                                            bsAlert.alert({
                                                title: "Silakan hubungi Administrasi Finance untuk laporan titipan dan pengembalian DP",
                                                text: "",
                                                type: "warning",
                                                showCancelButton: false
                                            }, function(){
                                                $scope.cancelService(item);
                                            
                                            });
                                        }else{
                                            $scope.cancelService(item);
                                        }
                                    },
                                    function() {
                                        // $timeout(function() {
                                        //     $scope.show_modal = {
                                        //         show: false
                                        //     };
                                        // }, 100);
                                        // $timeout(function() {
                                        //     $scope.cancelAppointment($scope.modal_model);
                                        // }, 200);
                                    })
                            } else {
                                bsAlert.alert({
                                        title: "Apakah anda yakin??",
                                        text: "Untuk membatalkan appointment ini",
                                        type: "question",
                                        showCancelButton: true,
                                        confirmButtonText: "Ya",
                                        cancelButtonText: "Tidak"
                                    },
                                    function() {
                                        $scope.cancelService(item);
                                    },
                                    function() {

                                    })
                            }

                        } else {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Data masih belum terisi",
                                // content: error.join('<br>'),
                                // number: error.length
                            });
                            $scope.show_modal = {
                                show: false
                            };
                            $timeout(function() {
                                $scope.cancelAppointment($scope.modal_model);
                            }, 100);
                            
                        }
                    })
                    console.log('totalDPAlert', $scope.totalDPAlert);

                } else {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Ada Perubahan Status, Silahkan Refresh",
                        // content: error.join('<br>'),
                        // number: error.length
                    });
                }

            })
            

        };
        $scope.changeFormatDate = function(item) {
            var tmpItemDate = angular.copy(item);
            console.log("changeFormatDate item", item);
            tmpItemDate = new Date(tmpItemDate);
            var finalDate = '';
            var yyyy = tmpItemDate.getFullYear().toString();
            var mm = (tmpItemDate.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = tmpItemDate.getDate().toString();

            console.log("changeFormatDate finalDate", finalDate);
            return finalDate += yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
        }
        $scope.changeFormatDateStrip = function(item) {
            var tmpItemDate = angular.copy(item);
            console.log("changeFormatDate item", item);
            tmpItemDate = new Date(tmpItemDate);
            var finalDate = '';
            var yyyy = tmpItemDate.getFullYear().toString();
            var mm = (tmpItemDate.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = tmpItemDate.getDate().toString();

            console.log("changeFormatDate finalDate", finalDate);
            return finalDate += yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
        }
        $scope.doBackAppointment = function(item) {
                console.log('itemcancel', item);
            }
            // $scope.disView = false;
        $scope.doSaveApproval = function(item) {
            console.log("itemsave", item, $scope.gridFrontParts.data);
            var tmpPartsForApproval = [];
            var tmpPartsFront = $scope.gridFrontParts.data;
            for (var i in tmpPartsFront) {
                tmpPartsForApproval.push({
                    JobPartsId: tmpPartsFront[i].JobPartsId
                })
            }
            var tes = new Date();
            item.ApproveRejectDate = $scope.changeFormatDate(tes);
            console.log("tmpPartsForApproval", tmpPartsForApproval);
            FollowUpGrService.updateApproval(item, tmpPartsForApproval).then(
                function(res) {
                    $scope.getData();
                    $scope.show_modalApproval = {
                        show: false
                    };
                },
                function(err) {

                }
            );

        }

        $scope.buttonSettingTitle = {
            save: {
                text: 'Kirim'
            },
            cancel: {
                text: 'Kembali'
            }
        };

        $scope.saveApprovalDP = function(data) {
            // AppointmentGrService.getWobyJobId(data.JobId).then(function(res) {
            //     var tmpDataForApprovalParts = [];
            //     var tmpApproval = res.data.Result[0];
            //     console.log("sukses tmpApproval", tmpApproval);
            //     // var tmpDataForApprovalPart = tmpDataForApproval.JobParts;
            //     for (var k in tmpApproval.JobTask) {
            //         for (var i in tmpApproval.JobTask[k].JobParts) {
            //             if (tmpApproval.JobTask[k].JobParts[i].DownPayment < tmpApproval.JobTask[k].JobParts[i].minimalDp) {
            //                 console.log("mpApproval.JobTask[k].JobParts[i]", tmpApproval.JobTask[k].JobParts[i]);
            //                 tmpDataForApprovalParts.push({
            //                     JobId: data.JobId,
            //                     DPDefault: tmpApproval.JobTask[k].JobParts[i].minimalDp,
            //                     DPRequested: tmpApproval.JobTask[k].JobParts[i].DownPayment,
            //                     JobPartsId: tmpApproval.JobTask[k].JobParts[i].JobPartsId,
            //                     ApprovalCategoryId: 38,
            //                     ApproverId: null,
            //                     RequesterId: null,
            //                     ApproverRoleId: data.ApproverRoleId,
            //                     RequestorRoleId: $scope.user.RoleId,
            //                     RequestReason: "Request Pengurangan DP",
            //                     RequestDate: new Date(),
            //                     StatusApprovalId: 3,
            //                     StatusCode: 1,
            //                     VehicleTypeId: tmpApproval.VehicleTypeId
            //                 })
            //             }
            //         }
            //     }
            //     console.log("tmpDataForApprovalParts", tmpDataForApprovalParts);
            //     AppointmentGrService.postApprovalDp(tmpDataForApprovalParts).then(function(res) {
            //         console.log("sukses oostapproval");
            //     });
            // });
            var tmpDataForApprovalParts = [];
            var tmpApproval = data;
            for (var k in tmpApproval.JobTask) {
                for (var i in tmpApproval.JobTask[k].JobParts) {
                    if (tmpApproval.JobTask[k].JobParts[i].DownPayment < tmpApproval.JobTask[k].JobParts[i].minimalDp) {
                        console.log("mpApproval.JobTask[k].JobParts[i]", tmpApproval.JobTask[k].JobParts[i]);
                        tmpDataForApprovalParts.push({
                            JobId: tmpApproval.JobId,
                            DPDefault: tmpApproval.JobTask[k].JobParts[i].minimalDp,
                            DPRequested: tmpApproval.JobTask[k].JobParts[i].DownPayment,
                            JobPartsId: tmpApproval.JobTask[k].JobParts[i].JobPartsId,
                            ApprovalCategoryId: 38,
                            ApproverId: null,
                            RequesterId: null,
                            ApproverRoleId: tmpApproval.ApproverRoleId,
                            RequestorRoleId: $scope.user.RoleId,
                            RequestReason: "Request Pengurangan DP",
                            RequestDate: new Date(),
                            StatusApprovalId: 3,
                            StatusCode: 1,
                            VehicleTypeId: tmpApproval.VehicleTypeId
                        })
                    }
                }
            }
            console.log("tmpDataForApprovalParts", tmpDataForApprovalParts);
            AppointmentGrService.postApprovalDp(tmpDataForApprovalParts).then(function(res) {
                console.log("sukses oostapproval");
            });
        }
        $scope.doBack = function() {

            }
            // $scope.actView = function(data) {
            //   console.log("sukses", data);
            //   var Apppoint = angular.copy(data);
            //   $scope.mData = {};
            //   $scope.gridWork = [];
            //   $scope.JobComplaint = [];
            //   $scope.JobRequest = [];
            //   $scope.gridWork = $scope.gridWork;
            //   $scope.totalWork = 0;
            //   $scope.totalMaterial = 0;
            //   $scope.onShowDetail(data, 'view');
            //   AppointmentGrService.getDataVehicle(data.VehicleId).then(function(ress) {
            //     var dataDetail = ress.data.Result[0]
            //     $scope.mDataDetail = dataDetail;
            //     $scope.getAllParameter(dataDetail);
            //     console.log("$scope.mDataDetail", $scope.mDataDetail);
            //     console.log("$scope.mData", $scope.mData);
            //     AppointmentGrService.getDataTaskList($scope.mDataDetail.KatashikiCode).then(function(result) {
            //       console.log("result", result);
            //     });
            //   });
            //   MrsList.getDataModel().then(function(res) {
            //     console.log("result : ", res.data.Result);
            //     $scope.modelData = res.data.Result;
            //   });
            //   $scope.getEstimate(data.VehicleId);
            //   $scope.vehicleStatus = 0;
            //   $scope.checkAsb = false;
            //   $scope.formApi.setMode('view');
            //   $scope.mData = Apppoint;

        //   // $scope.disView = true;
        //   // $scope.formApi.setFormReadOnly('true');
        // };
        $scope.getDataModelForAppointment = function() {
            MrsList.getDataModel().then(function(res) {
                console.log("result : ", res.data.Result);
                $scope.modelData = res.data.Result;
            });
        }
        $scope.actEdit = function(data) {
            // console.log("sukses", data);
            // var Apppoint = angular.copy(data);
            // if ($scope.filter.typeCategory != 4) {
            $scope.mData = {};
            $scope.gridWork = [];
            var Parts = [];
            $scope.JobComplaint = [];
            $scope.JobRequest = [];
            $scope.gridWork = $scope.gridWork;
            $scope.totalWork = 0;
            $scope.costMaterial = 0;
            $scope.summaryJoblist = 0;
            $scope.totalMaterial = 0;
            $scope.totalDp = 0;
            $scope.onShowDetail(data, 'edit');
            $scope.modeDetail = 'edit';
            $scope.mDataVehicleHistory = [];
            $scope.MRS = {};

            // MrsList.getDataModel().then(function(res) {
            //     console.log("result : ", res.data.Result);
            //     $scope.modelData = res.data.Result;
            // });

            // $scope.getEstimate(data.VehicleId);
            $scope.checkAsb = false;
            console.log("$scope.$scope.formApi", $scope.formApi);
            $scope.formApi.setFormReadOnly(false);
            $scope.formApi.setMode('detail');
            // $scope.mData = Apppoint;
            console.log("mData Appointment", $scope.mData);
            $scope.actionButtonSettingsDetail[0].visible = true;
            $scope.actionButtonSettingsDetail[1].visible = true;
        };

        $scope.actView = function(data) {
            if (data.VIN != null || data.VIN != undefined || data.VIN != "") {
                FollowUpGrService.listHistoryClaim(data.VIN).then(function(res){
                    $scope.listClaim = res.data.data;
                    $scope.tempListClaim = [];
                    $scope.historyDate = new Date($scope.tempListClaim.claimDate)
                    console.log('cek list claim', res)
                    console.log('list claim', res.data.data)
                    console.log('list history claim', $scope.tempListClaim)
                    for (var i = 0; i < $scope.listClaim.historyClaimDealerList.length; i++) {
                        $scope.tempListClaim.push($scope.listClaim.historyClaimDealerList[i]);
                    }
                })

                FollowUpGrService.getTowass(data.VIN).then(function(res) {
                    console.log("data Towasssssss", res.data);
                    $scope.dataTowass = res.data.Result;
                });

            }
            
            $scope.mData = {};
            $scope.gridWork = [];
            $scope.JobComplaint = [];
            $scope.JobRequest = [];
            $scope.gridWork = $scope.gridWork;
            $scope.totalWork = 0;
            var Parts = [];
            $scope.totalMaterial = 0;
            $scope.totalDp = 0;
            $scope.mDataDetail = {};
            $scope.onShowDetail(data, 'view');
            $scope.modeDetail = 'view';
            $scope.mDataVehicleHistory = [];
            $scope.MRS = {};

            // MrsList.getDataModel().then(function(res) {
            //     console.log("result : ", res.data.Result);
            //     $scope.modelData = res.data.Result;
            // });
            // $scope.getEstimate(data.VehicleId);
            $scope.checkAsb = false;
            console.log("$scope.$scope.formApi", $scope.formApi);
            // $scope.mData = Apppoint;
            console.log("mData Appointment", $scope.mData);
            // $scope.disView = true;
            $scope.formApi.setFormReadOnly(true);
            $scope.formApi.setMode('detail');
            $scope.actionButtonSettingsDetail[0].visible = false;
            $scope.disableButton();
        };
        $scope.getEstimate = function(filter) {
            AppointmentGrService.getDataEstimasi(filter).then(function(res) {
                $scope.estimateData = res.data.Result;
                console.log('res==|||==>', res);
            })
        }
        $scope.bsAlertFuncCancel = function(prompt, item) {
            bsAlert.alert({
                    title: prompt,
                    text: item.AppointmentNo,
                    type: "warning",
                    showCancelButton: true
                },
                function() {
                    $scope.cancelService(item);
                },
                function() {

                }
            )
        };
        $scope.bsAlertFuncOnTime = function(prompt, item) {
            bsAlert.alert({
                    title: prompt,
                    text: item.AppointmentNo,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Sudah",
                    cancelButtonText: "Belum",
                },
                function() {
                    $scope.onTimeService(item);
                },
                function() {

                }
            )
        };
        $scope.onTimeAppointment = function(data) {
            if ($scope.filter.typeCategory < 4 || $scope.filter.typeCategory == 6) {
                if (data.Status == 0) {
                    if ((data.isFoOneDay == 0 && data.isFoOneHour == 0)) {
                        $scope.bsAlertFuncOnTime("Apakah Pelanggan sudah di Follow Up ?", data);
                    } else {
                        bsAlert.alert({
                                title: "Sudah Di Follow Up",
                                text: "",
                                type: "warning",
                                showCancelButton: false,
                            },
                            function() {

                            },
                            function() {

                            }
                        )
                    }
                } else {
                    bsAlert.alert({
                            title: "Tidak dapat follow Up dengan status " + data.Xstatus,
                            text: "",
                            type: "warning",
                            showCancelButton: false
                        },
                        function() {

                        },
                        function() {

                        }
                    )
                }
            } else {
                bsAlert.alert({
                        title: "Tidak dapat follow Up dibagian history",
                        text: "",
                        type: "warning",
                        showCancelButton: false
                    },
                    function() {

                    },
                    function() {

                    }
                )
            }

        };
        $scope.cancelAppointment = function(data) {
            console.log("data cancelAppointment", data);
            if (data.Status == 0) {
                $scope.modal_model = angular.copy(data);
                // $scope.bsAlertFuncCancel("Apakah anda yakin akan membatalkan Appointment?", data);
                $scope.show_modal = {
                    show: true
                };
                // $scope.bsAlertFuncCancel("Apakah anda yakin akan membatalkan Appointment?", data);
            } else {
                bsAlert.alert({
                        title: "Batal appointment hanya dapat dilakukan untuk status Outstanding",
                        text: "",
                        type: "warning",
                        showCancelButton: false
                    },
                    function() {

                    },
                    function() {

                    }
                )
            }
        };
        
        $scope.vinClaimType = [];
        $scope.actEditAppointment = function(data) {
            console.log('edit FU', data)
            if (data.VIN != null || data.VIN != undefined || data.VIN != "") {
                FollowUpGrService.listHistoryClaim(data.VIN).then(function(res){
                    $scope.listClaim = res.data.data;
                    $scope.tempListClaim = [];
                    console.log('cek list claim', res)
                    console.log('list claim', res.data.data)
                    console.log('list history claim', $scope.tempListClaim)
                    for (var i = 0; i < $scope.listClaim.historyClaimDealerList.length; i++) {
                        $scope.tempListClaim.push($scope.listClaim.historyClaimDealerList[i]);
                    }
                })
                
                FollowUpGrService.getTowass(data.VIN).then(function(res) {
                    console.log("data Towasssssss", res.data);
                    $scope.dataTowass = res.data.Result;
                });
            }
            
            WoHistoryGR.getStatWO(data.JobId, data.Status).then(function(ress){
                if (ress.data == 0) {
                    console.log("data====>", data);
                    $scope.loadingSimpan = 0

                    $scope.asbDate = data.AppointmentDate;
        
        
                    $scope.DateSebelumReschedule = angular.copy(data.AppointmentDate);
                    var tmpDateAppointment = new Date();
                    tmpDateAppointment.setHours(0);
                    tmpDateAppointment.setMinutes(0);
                    tmpDateAppointment.setSeconds(0);
                    tmpDateAppointment.setMilliseconds(0);
                    console.log(tmpDateAppointment, data.AppointmentDate);
                    if ((data.Status == 0 || data.Status == 2) && data.AppointmentDate >= tmpDateAppointment) {
                        $scope.actEdit(data);
                    } else {
                        if ((data.Status == 1 || data.Status == 2) && data.IsAppointmentDP == 1) {
                            $scope.actEdit(data);
                        } else {
                            bsAlert.alert({
                                title: "Edit appoinment tidak dapat dilakukan",
                                text: "",
                                type: "warning"
                                    // showCancelButton:true
                            },
                            function() {
        
                            },
                            function() {
        
                            }
                            )
                        } 
                    }
                } else {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Ada Perubahan Status, Silahkan Refresh",
                        // content: error.join('<br>'),
                        // number: error.length
                    });
                }

            }) 
        }
        $scope.onTimeService = function(data) {
            FollowUpGrService.onTime($scope.filter.typeCategory, data).then(function(res) {
                $scope.getData();
            });
        };
        $scope.actApprovel = function(data, type) {
            console.log("data====>", data);
            var tmpRow = {};
            tmpRow = angular.copy(data[0]);
            if (tmpRow.StatusApprovalId == 3) {
                tmpRow.isReject = type;
                if (type == 1) {
                    $scope.isReject = true;
                    $scope.buttonSettingTitle.save.text = "Reject";
                    tmpRow.NewStatusApprovalId = 2;
                } else {
                    $scope.isReject = false;
                    tmpRow.NewStatusApprovalId = 1;
                    $scope.buttonSettingTitle.save.text = "Approve";
                }
                $scope.modal_model = angular.copy(tmpRow);
                // $scope.bsAlertFuncCancel("Apakah anda yakin akan membatalkan Appointment?", data);
                $scope.show_modalApproval = {
                    show: true
                };
                $scope.dateOfApproval = new Date();
            } else if (tmpRow.StatusApprovalId == 2) {
                bsAlert.warning("Request Sudah di Reject", "");
            } else {
                bsAlert.warning("Request Sudah di Setujui", "");
            }
        };
        $scope.actPrintAppointment = function(data) {
            console.log('JobId cetakAppointment', data);
            // var data = $scope.mData;
            $scope.printAppoinmentGR = 'as/PrintAppointmentGr/' + data.JobId + '/' + $scope.user.OrgId + '/' + 1;
            $scope.cetakan($scope.printAppoinmentGR);
        }
        $scope.cetakan = function(data) {
            var pdfFile = null;
            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame 
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    } else {
                        printJS(pdfFile);
                    }
                } else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };
        $scope.estimateAsb = function() {
            if ($scope.gridWork.length > 0) {
                var totalW = 0;
                for (var i = 0; i < $scope.gridWork.length; i++) {
                    if ($scope.gridWork[i].ActualRate !== undefined) {
                        totalW += $scope.gridWork[i].ActualRate;
                    }
                }
                $scope.visibleAsb = 1;
                $scope.tmpActRate = totalW;
            } else {
                $scope.tmpActRate = 1;
                $scope.visibleAsb = 0;
            }
            $scope.checkAsb = true;
            $scope.asb.startDate = "";
            var currentDate;
            currentDate = new Date();
            currentDate.setDate(currentDate.getDate() + 1);
            currentDate.setDate(currentDate.getDate()); // edit bisa hari ini Pak Dodi
            $scope.minDateASB = new Date(currentDate);
            // $scope.currentDate = new Date(currentDate);
            // $scope.asb.startDate = new Date(currentDate);
            if ($scope.mData.AppointmentDate !== undefined && $scope.mData.AppointmentDate !== null) {
                console.log("Estimate  ASB $scope.mData.AppointmentDate", $scope.mData.AppointmentDate);
                var tmpDate = $scope.mData.AppointmentDate;
                // var tmpMonth = "";
                var tmpTime = $scope.mData.AppointmentTime;
                // if (tmpDate.getMonth() < 10) {
                //     tmpMonth = "0" + (tmpDate.getMonth() + 1);
                // } else {
                //     tmpMonth = tmpDate.getMonth();
                // }
                // var ini = tmpDate.getFullYear() + "/" + tmpMonth + "/" + tmpDate.getDate() + " " + tmpTime;
                var finalDate
                var yyyy = tmpDate.getFullYear().toString();
                var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                var dd = tmpDate.getDate().toString();
                finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + " " + tmpTime;
                var d = new Date(finalDate);
                $scope.asb.startDate = d;
                $scope.currentDate = d;
                // console.log("date yg diinginkan", ini);
            } else {
                $scope.asb.startDate = new Date(currentDate);
                $scope.currentDate = new Date(currentDate);
            }
            // ===================================
            var timeToDateStart = angular.copy($scope.mData.PlanDateStart ? $scope.mData.PlanDateStart : $scope.asb.startDate);
            var timeToDateFinish = angular.copy($scope.mData.PlanDateFinish ? $scope.mData.PlanDateFinish : $scope.asb.startDate);
            var timePlanStart = $scope.mData.PlanStart;
            var timePlanFinish = $scope.mData.PlanFinish;
            if (($scope.mData.PlanStart !== null && $scope.mData.PlanStart !== undefined) && $scope.mData.PlanFinish !== null && $scope.mData.PlanFinish !== undefined) {
                timePlanStart = timePlanStart.split(":");
                timePlanFinish = timePlanFinish.split(":");
                timeToDateStart = new Date(timeToDateStart.setHours(timePlanStart[0], timePlanStart[1], timePlanStart[2]));
                timeToDateFinish = new Date(timeToDateFinish.setHours(timePlanFinish[0], timePlanFinish[1], timePlanFinish[2]));
            } else {
                timeToDateStart = $scope.asb.startDate
                timeToDateFinish = $scope.asb.startDate
                    // timeToDateStart = $scope.mData.PlanStart;
                    // timeToDateFinish = $scope.mData.PlanFinish;
            }
            // ====================================
            $scope.asb.JobStallId = $scope.mData.StallId;
            $scope.asb.JobStartTime = timeToDateStart;
            $scope.asb.JobEndTime = timeToDateFinish;
            if ($scope.changeFormatDate(timeToDateStart) == $scope.changeFormatDate(timeToDateFinish)) {
                $scope.asb.NewChips = 1;
            } else {
                $scope.asb.NewChips = 0;
            }
            // if($scope.mData.JobId !== null && $scope.mData.JobId !== undefined){
            // $scope.asb.NewChips = 0;
            // }else{
            //     $scope.asb.NewChips = 1;
            // }
            $scope.asb.SplitChips = $scope.checkSplit($scope.mData.JobListSplitChip);
            // $scope.asb.currentDate = currentDate;
            // $scope.asb.JobStartTime = $scope.mData.PlanStart;
            // $scope.asb.JobEndTime = $scope.mData.PlanFinish;
            if (tmpPrediagnose.PrediagStallId !== undefined) {
                $scope.asb.PrediagStallId = tmpPrediagnose.PrediagStallId;
            } else {
                $scope.asb.PrediagStallId = $scope.mData.PrediagStallId
            }
            $scope.asb.preDiagnose = tmpPrediagnose;
            if (tmpPrediagnose.ScheduledTime !== undefined && tmpPrediagnose.ScheduledTime !== null) {
                $scope.asb.PrediagStartTime = tmpPrediagnose.ScheduledTime;
            } else {
                $scope.asb.PrediagStartTime = $scope.mData.PrediagScheduledTime;
            }

            // $scope.asb.PrediagEndTime = tmpPrediagnose.PrediagEndTime;
            // $scope.asb.startDate = $scope.mData.AppointmentDate;
            $scope.mData.FullDate = $scope.asb.startDate;
            $scope.stallAsb = $scope.mData.StallId;
            console.log("curDate", currentDate);
            console.log("$scope.asb.startDate", $scope.asb.startDate, tmpPrediagnose);
            showBoardAsbGr($scope.mData.FullDate);
        }
        $scope.backEstimate = function() {
            $scope.checkAsb = false;
        };
        $scope.cancelService = function(data) {
            FollowUpGrService.cancel(data).then(function(res) {
                $scope.getData();
                $scope.show_modal = {
                    show: false
                };

                bsAlert.alert({
                        title: "Cancel Appointment berhasil",
                        text: "",
                        // type: "warning",
                        type: "success",
                        showCancelButton: false
                    },
                    function() {

                    },
                    function() {

                    }
                )


                //kirim notifikasi batal Appointment
                console.log('data', data);
                // var DataNotif = {};
                // var messagetemp = "";
                // messagetemp =   "No. Appointment : " + $scope.mData.AppointmentNo
                //                 +"<br> No. Polisi : " + $scope.mData.PoliceNumber ;

                //  DataNotif.Message = messagetemp;
                FollowUpGrService.sendNotif(data, 1122, 20).then(
                    function(res) {

                    },
                    function(err) {
                        //console.log("err=>", err);
                    });
            });
        };
        $scope.checkSplit = function(data) {
            var tmpData = [];
            var objTemp = {}
            if (data.length > 0) {
                for (var i in data) {
                    var tmptgl = $scope.changeFormatDateStrip(data[i].PlanDateStart);
                    tmpData.push({
                        startTime: data[i].PlanStart,
                        endTime: data[i].PlanFinish,
                        stallId: data[i].StallId,
                        tmpDate: tmptgl,
                        JobSplitId: data[i].JobSplitId
                    });
                    // objTemp[tmptgl] = {
                    //         startTime: data[i].PlanStart,
                    //         endTime: data[i].PlanFinish,
                    //         stallId: data[i].StallId,
                    //         tmpDate:tmptgl
                    // }
                }
                // tmpData.push(objTemp);
            }
            console.log("tmpData", tmpData);
            // return objTemp
            return tmpData
        }
        var xres = []
        $scope.getAllParameter = function(item) {
            console.log("itemnya", item);
            DataPersonal = item;
            FollowUpGrService.getPayment().then(function(res) {
                // $scope.paymentData = res.data.Result;
                var tmpPayment = [];
                var tmpPaymentPart = [];
                var tmpPaymentIsNotWarranty = [];
                var tmpPaymentThereIstWarranty = [];
                _.map(res.data.Result,function(val){
                    if(val.Name !== 'Insurance'){
                        tmpPayment.push(val);
                        if (val.Name !== 'Warranty') {
                          tmpPaymentPart.push(val);
                        }else {
                          tmpPaymentThereIstWarranty.push(val);
                        }
                        if(val.Name !== 'Insurance' && val.Name !== 'Warranty'){
                            tmpPaymentIsNotWarranty.push(val);
                        }
                    }
                })
                $scope.paymentData = tmpPayment;
                $scope.paymentDataPart = tmpPaymentPart;
                $scope.paymentDataIsNotWarranty = tmpPaymentIsNotWarranty;
                $scope.paymentDataThereIstWarranty = tmpPaymentThereIstWarranty;
                console.log('gaada warratny', $scope.paymentDataIsNotWarranty);
                console.log('payment part', $scope.paymentDataPart);
                console.log('payment ada warranty', $scope.paymentDataThereIstWarranty);

                //validasi variable paymentData tanpa warranty 
                var tmpIdx = _.findIndex($scope.paymentData, { 'Name': 'Warranty' });
                $scope.somePaymentData = angular.copy($scope.paymentData);
                $scope.somePaymentData.splice(tmpIdx, 1);	

                if ($scope.modeDetail == 'view') { $scope.disableButton(); }
            });
            FollowUpGrService.getUnitMeasurement().then(function(res) {
                $scope.unitData = res.data.Result;
                if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                // $scope.unitDataGrid = [];
                // var tmpUnit = $scope.unitData;
                // for (var i = 0; i < tmpUnit.length; i++) {
                //     var temp = {};
                //     temp[tmpUnit[i].MasterId] = tmpUnit[i].Name;
                //     $scope.unitDataGrid.push(temp);
                //     if (!$scope.unitDataGrid[tmpUnit[i].MasterId]) {
                //         $scope.unitDataGrid[tmpUnit[i].MasterId] = tmpUnit[i].Name;
                //     }
                //     console.log("$scope.unitDataGrid", $scope.unitDataGrid);
                // }
                console.log("$scope.unitData=====>", $scope.unitData);
            });




            FollowUpGrService.getWoCategory().then(function(res) {
                console.log("reswo", res.data.Result);
                if ($scope.modeDetail == 'view') { $scope.disableButton(); }

                var temparrCategory = []
                for (var i=0; i<res.data.Result.length; i++) {
                    if (res.data.Result[i].Name != 'BP') {
                        temparrCategory.push(res.data.Result[i])
                    }
                }
                $scope.woCategory = temparrCategory;
                
                // $scope.woCategory = res.data.Result;
            });
            FollowUpGrService.getTaskCategory().then(function(res) {
                if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                $scope.notFlcFpc = [];
                $scope.notFpcPlc = [];
                $scope.notFlcPlc = [];
                $scope.notFlcPlcFpc = [];
                _.map(res.data.Result, function(val){
                    if (val.Name !== "FLC - Free Labor Claim" && val.Name !== "FPC - Free Parts Claim") {
                        $scope.notFlcFpc.push(val); 
                    }
                    if (val.Name !== "FPC - Free Parts Claim" && val.Name !== "PLC - Parts Labor Claim") {
                        $scope.notFpcPlc.push(val)  
                    }
                    if (val.Name !== "FLC - Free Labor Claim" && val.Name !== "PLC - Parts Labor Claim") {
                        $scope.notFlcPlc.push(val)  
                    }
                    if (val.Name !== "FLC - Free Labor Claim" && val.Name !== "PLC - Parts Labor Claim" && val.Name !== "FPC - Free Parts Claim") {
                        $scope.notFlcPlcFpc.push(val)  
                    }
                })
                console.log('ada flc', $scope.notFpcPlc)
                console.log('ada fpc', $scope.notFlcPlc)
                console.log('ada plc', $scope.notFlcFpc)
                console.log('gaada plc', $scope.notFlcPlcFpc)

                FollowUpGrService.getDataVIN($scope.mDataDetail.VIN).then(function(res){
                    console.log('cek input rangka', $scope.mDataDetail.VIN)
                    var vinMasterId = res.data.Result;
                    $scope.tempVIN = res.data.Result[0];
                    if (vinMasterId != null && $scope.tempVIN != null) {
                        if ($scope.tempVIN.DEC_Date != null && $scope.tempVIN.DEC_Date != undefined) {
                            $scope.tempVIN.DEC_Date = $filter('date')($scope.tempVIN.DEC_Date, 'dd/MM/yyyy')
                        }else{
                            $scope.tempVIN.DEC_Date = '-'
                        }
                    }
                    console.log('cek res rangka', $scope.tempVIN)
                    for(var i = 0; i < vinMasterId.length; i++){
                        console.log('cek data vin', $scope.tempVIN.MasterId)
                        console.log('cek data tempVIN', $scope.tempVIN.length)
                        if ($scope.mDataDetail.VIN != null || $scope.mDataDetail.VIN != "") {
                            if (vinMasterId[i].MasterId === 2678) {
                                $scope.taskCategory = $scope.notFlcFpc;
                            }else if(vinMasterId[i].MasterId === 2677){
                                $scope.taskCategory = $scope.notFpcPlc
                            }else if(vinMasterId[i].MasterId === 2675){
                                $scope.taskCategory = $scope.notFlcPlc
                            }else{
                                $scope.taskCategory = $scope.notFlcPlcFpc
                            }
                        }else{
                            $scope.taskCategory = $scope.notFlcPlcFpc
                        }
                    }
                            console.log('cek res input', $scope.tempVIN)
            })

            });
            FollowUpGrService.getDataGender().then(function(res) {
                if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                console.log("result gender : ", res.data.Result);
                var temp = res.data.Result;
                for (var i = 0; i < temp.length; i++) {
                    temp[i].value = temp[i].CustomerGenderId;
                    temp[i].text = (temp[i].CustomerGenderName == "Laki-Laki" ? "L" : "P");
                }
                console.log("tmp", temp);
                $scope.genderData = temp;
            });

            MrsList.getDataCusRole().then(function(res) {
                if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                console.log("result role : ", res.data.Result);
                $scope.RoleData = res.data.Result;
            });
            MrsList.getCCustomerCategory().then(function(res) {
                if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                $scope.CustomerTypeData = res.data.Result;
            });
            RepairSupportActivityGR.getT1().then(function(res) {
                if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                $scope.TFirst = res.data.Result;
                for (var i=0; i<res.data.Result.length; i++){
                    $scope.TFirst[i].xName = $scope.TFirst[i].T1Code + ' - ' + $scope.TFirst[i].Name;
                }
                //console.log("T1", $scope.T1);
            });
            // $scope.CustomerTypeData = [{CustomerTypeId:1,CustomerTypeDesc:"Individu"},{CustomerTypeId:2,CustomerTypeDesc:"Institusi"}]
            // MrsList.getDataCusType().then(function(res) {
            //   console.log("result type : ",res.data);
            //   $scope.CustomerTypeData = res.dafta.Result;
            // });
            // MrsList.getDataModel().then(function(res) {
            //   console.log("result : ",res.data.Result);
            //   $scope.modelData = res.data.Result;
            // });
            // if (item != undefined) {

            // }
            if (item !== undefined || item != null) {
                console.log('kok bisa kesini ??? padahal item na undefined');
                if (item.VIN != null) {
                    AppointmentGrService.getTowass(item.VIN).then(function(res) {
                        console.log("data Towasssssss", res.data);
                        $scope.dataTowass = res.data.Result;
                    });
                }
                if (item.ProvinceId !== undefined || item.ProvinceId !== null) {
                    // MrsList.getDataProvinceById(item.ProvinceId).then(function(res) {
                    //     if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                    //     console.log("result province : ", res.data.Result);
                    //     $scope.ProvinceData = res.data.Result;
                    // });
                    WOBP.getMLocationProvince().then(function(res) {
                        console.log("List Province====>", res.data.Result);
                        if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                        $scope.ProvinceData = res.data.Result;
                    })
                }
                if (item.CityRegencyId !== undefined || item.CityRegencyId !== null) {
                    // MrsList.getDataCityById(item.CityRegencyId).then(function(res) {
                    //     if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                    //     console.log("result city : ", res.data.Result);
                    //     $scope.CityRegencyData = res.data.Result;
                    // });
                    WOBP.getMLocationCityRegency(item.ProvinceId).then(function(resu) {
                        if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                        $scope.CityRegencyData = resu.data.Result;
                    });
                }
                if (item.DistrictId !== undefined || item.DistrictId !== null) {
                    // MrsList.getDataDistrictById(item.DistrictId).then(function(res) {
                    //     if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                    //     console.log("result district : ", res.data.Result);
                    //     $scope.DistrictData = res.data.Result;
                    // });
                    WOBP.getMLocationKecamatan(item.CityRegencyId).then(function(resu) {
                        if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                        $scope.DistrictData = resu.data.Result;
                    });
                }
                if (item.VillageId !== undefined || item.VillageId !== null) {
                    // MrsList.getDataVillageById(item.VillageId).then(function(res) {
                    //     if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                    //     console.log("result village : ", res.data.Result);
                    //     $scope.VillageData = res.data.Result;
                    // });
                    WOBP.getMLocationKelurahan(item.DistrictId).then(function(resu) {
                        if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                        $scope.VillageData = resu.data.Result;
                    });
                }
                if (item.GasTypeId !== null || item.GasTypeId !== undefined) {
                    $scope.mDataDetail.VehicleGasTypeId = item.GasTypeId;
                }

                WO.getDataPKS(item.LicensePlate).then(function(res) {
                    $scope.dataPKS = res.data.Result;
                    console.log("PKS", $scope.dataPKS);
                });
            }
            MrsList.getInvitationMedia().then(function(res) {
                if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                console.log("result media : ", res.data.Result);
                $scope.InvMediaData = res.data.Result;
            });
            MrsList.getMrsInvitation().then(function(res) {
                if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                console.log("result mrs invit : ", res.data.Result);
                $scope.MrsInvData = res.data.Result;
            });
            // ------- Model
            // AppointmentGrService.getDataModel().then(function(res) {
            //   $scope.modelData = res.data.Result;
            // });
        };

        function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        // $scope.getData = function() {
        //     if ($scope.filter.typeCategory < 4 && $scope.filter.typeCategory != null) {
        //         console.log("tipe", $scope.filter.typeCategory);
        //         var today = new Date();
        //         var h = today.getHours();
        //         var m = today.getMinutes();
        //         var s = today.getSeconds();
        //         var year = today.getFullYear();
        //         var month = today.getMonth() + 1;
        //         var dayLast = today.getDate();
        //         m = checkTime(m);
        //         s = checkTime(s);
        //         var lastDateNya = year + '-' + month + '-' + dayLast;
        //         var timeDateNya = h+'%20'+m+'%20'+s;
        //         return $q.resolve(
        //             // add a zero in front of numbers<10
        //             // =================
        //             FollowUpGrService.getData($scope.filter.firstDate, $scope.filter.lastDate, $scope.filter.typeCategory, lastDateNya, timeDateNya).then(function(res) {
        //                 xres = [];
        //                 if (res.data.Result.length > 0) {
        //                     console.log("res.data.Result", res.data.Result);
        //                     if ($scope.filter.typeCategory == 1) {
        //                         for (var i = 0; i < res.data.Result.length; i++) {
        //                             if (res.data.Result[i].Status == 0 && res.data.Result[i].isFoOneDay == 0) {
        //                                 xres.push(res.data.Result[i]);
        //                             }
        //                         }
        //                     } else if ($scope.filter.typeCategory == 2) {
        //                         for (var i = 0; i < res.data.Result.length; i++) {
        //                             if (res.data.Result[i].Status == 0 && res.data.Result[i].isFoOneHour == 0) {
        //                                 xres.push(res.data.Result[i]);
        //                             }
        //                         }
        //                     } else if ($scope.filter.typeCategory == 3) {
        //                         for (var i = 0; i < res.data.Result.length; i++) {
        //                             if (res.data.Result[i].Status == 0) {
        //                                 xres.push(res.data.Result[i]);
        //                             }
        //                         }
        //                     }
        //                     for (var i = 0; i < xres.length; i++) {
        //                         xres[i].Category = "GR"
        //                         if (xres[i].Status == 0 && xres[i].AppointmentNo !== null) {
        //                             xres[i].Xstatus = "Outstanding";
        //                         } else if (xres[i].Status == 0 && xres[i].AppointmentNo == null) {
        //                             xres[i].Xstatus = "Draft";
        //                         }
        //                         if ($scope.filter.typeCategory == 1) {
        //                             xres[i].TypeFollow = "H-1";
        //                         } else if ($scope.filter.typeCategory == 2) {
        //                             xres[i].TypeFollow = "J-N";
        //                         } else if ($scope.filter.typeCategory == 3) {
        //                             xres[i].TypeFollow = "J+1";
        //                         }
        //                     }
        //                 }
        //                 console.log("xresssss", xres);
        //                 var gridData = xres;
        //                 $scope.grid.data = gridData;
        //                 $scope.loading = false;
        //             })
        //         )
        //     } else if ($scope.filter.typeCategory >= 4 && $scope.filter.firstDate != null && $scope.filter.lastDate != null) {
        //         var a = new Date($scope.filter.firstDate);
        //         var yearFirst = a.getFullYear();
        //         var monthFirst = a.getMonth() + 1;
        //         var dayFirst = a.getDate();
        //         var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;

        //         var b = new Date($scope.filter.lastDate);
        //         var yearLast = b.getFullYear();
        //         var monthLast = b.getMonth() + 1;
        //         var dayLast = b.getDate();
        //         var lastDate = yearLast + '-' + monthLast + '-' + dayLast;
        //         // =================
        //         var today = new Date();
        //         var h = today.getHours();
        //         var m = today.getMinutes();
        //         var s = today.getSeconds();
        //         var year = today.getFullYear();
        //         var month = today.getMonth() + 1;
        //         var dayLast = today.getDate();
        //         m = checkTime(m);
        //         s = checkTime(s);
        //         var lastDateNya = year + '-' + month + '-' + dayLast;
        //         var timeDateNya = h+'%20'+m+'%20'+s;
        //         // add a zero in front of numbers<10
        //         // =================
        //         xres = [];
        //         //==================
        //         return $q.resolve(
        //             FollowUpGrService.getData(firstDate, lastDate, $scope.filter.typeCategory, lastDateNya, timeDateNya).then(function(res) {
        //                 xres = [];
        //                 if (res.data.Result.length > 0) {
        //                     for (var i = 0; i < res.data.Result.length; i++) {
        //                         // if (res.data.Result[i].Status == 0){
        //                         xres.push(res.data.Result[i]);
        //                         // }
        //                     }
        //                     for (var i = 0; i < xres.length; i++) {
        //                         if (xres[i].Status == 0 && xres[i].AppointmentNo !== null) {
        //                             xres[i].Xstatus = "Outstanding";
        //                         } else if (xres[i].Status == 0 && xres[i].AppointmentNo == null) {
        //                             xres[i].Xstatus = "Draft";
        //                         } else if (xres[i].Status == 1 && xres[i].NewAppointmentRel !== null) {
        //                             xres[i].Xstatus = "Reschedule";
        //                         } else if (xres[i].Status == 1) {
        //                             xres[i].Xstatus = "Cancel";
        //                         } else if (xres[i].Status == 2) {
        //                             xres[i].Xstatus = "No Show";
        //                         }
        //                         xres[i].TypeFollow = "All";
        //                         xres[i].Category = "GR"
        //                     }
        //                 }
        //                 var gridData = xres;
        //                 $scope.grid.data = gridData;
        //                 // var gridData = res.data.Result;
        //                 // $scope.grid.data = gridData;
        //                 console.log('res====>', gridData);
        //                 $scope.loading = false;
        //             })
        //         );
        //     } else {
        //         // bsNotify.show({
        //         //     size: 'big',
        //         //     type: 'danger',
        //         //     title: "Mohon Input Filter",
        //         //     // content: error.join('<br>'),
        //         //     // number: error.length
        //         // });
        //         bsAlert.alert({
        //             title: "Filter Belum Terisi",
        //             text: "Mohon Input Filter",
        //             type: "warning",
        //             showCancelButton: true
        //         });
        //         $scope.grid.data = [];
        //     }
        //     $scope.loading = false;
        // };
        // ======= New Get DATA Before Great War Ninja=================
        $scope.getData = function() {
            if ($scope.filter.typeCategory <= 4 && $scope.filter.typeCategory != null) {
                console.log("tipe", $scope.filter.typeCategory);
                var today = new Date();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                var year = today.getFullYear();
                var month = today.getMonth() + 1;
                var dayLast = today.getDate();
                m = checkTime(m);
                s = checkTime(s);
                var lastDateNya = year + '-' + month + '-' + dayLast;
                var timeDateNya = h + '%20' + m + '%20' + s;
                return $q.resolve(
                    FollowUpGrService.getData(lastDateNya, lastDateNya, $scope.filter.typeCategory, lastDateNya, timeDateNya).then(function(res) {
                        xres = [];
                        if (res.data.Result.length > 0) {
                            console.log("res.data.Result", res.data.Result);
                            if ($scope.filter.typeCategory == 1) {
                                for (var i = 0; i < res.data.Result.length; i++) {
                                    if (res.data.Result[i].Status == 0 || res.data.Result[i].Status == 1) {
                                        xres.push(res.data.Result[i]);
                                    }
                                }
                            } else if ($scope.filter.typeCategory == 2) {
                                for (var i = 0; i < res.data.Result.length; i++) {
                                    if (res.data.Result[i].Status == 0 || res.data.Result[i].Status == 1) {
                                        xres.push(res.data.Result[i]);
                                    }
                                }
                            } else if ($scope.filter.typeCategory == 3) {
                                for (var i = 0; i < res.data.Result.length; i++) {
                                    if (res.data.Result[i].Status == 0 || res.data.Result[i].Status == 2) {
                                        xres.push(res.data.Result[i]);
                                    }
                                }
                            } else if ($scope.filter.typeCategory == 4) {
                                for (var i = 0; i < res.data.Result.length; i++) {
                                    if (res.data.Result[i].Status == 0 || res.data.Result[i].Status == 1 || res.data.Result[i].Status == 2) {
                                        xres.push(res.data.Result[i]);
                                    }
                                }
                            }
                            if ($scope.filter.typeCategory == 4) {
                                for (var i = 0; i < xres.length; i++) {
                                    xres[i].Category = "GR"
                                    if (xres[i].StatusApprovalId == 1) {
                                        xres[i].Xstatus = "Approve";
                                    } else if (xres[i].StatusApprovalId == 2) {
                                        xres[i].Xstatus = "Reject";
                                    } else if (xres[i].StatusApprovalId == 3) {
                                        xres[i].Xstatus = "Need Approval";
                                    }
                                    if ($scope.filter.typeCategory == 1) {
                                        xres[i].TypeFollow = "H-1";
                                    } else if ($scope.filter.typeCategory == 2) {
                                        xres[i].TypeFollow = "J-N";
                                    } else if ($scope.filter.typeCategory == 3) {
                                        xres[i].TypeFollow = "J+N";
                                    } else if ($scope.filter.typeCategory == 4) {
                                        xres[i].TypeFollow = "Need Approval";
                                    }
                                }
                            } else {
                                for (var i = 0; i < xres.length; i++) {
                                    xres[i].Category = "GR"
                                    if (xres[i].Status == 0 && xres[i].AppointmentNo !== null) {
                                        if (xres[i].isFoOneDay == 1 || xres[i].isFoOneHour == 1) {
                                            xres[i].Xstatus = "On Schedule";
                                        } else if (xres[i].NewAppointmentRel !== null) {
                                            xres[i].Xstatus = "Reschedule";
                                        } else {
                                            xres[i].Xstatus = "Outstanding";
                                        }
                                    } else if (xres[i].Status == 0 && xres[i].AppointmentNo == null) {
                                        xres[i].Xstatus = "Draft";
                                    } else if (xres[i].Status == 1) {
                                        xres[i].Xstatus = "Cancel";
                                    } else if (xres[i].Status == 2) {
                                        xres[i].Xstatus = "No Show";
                                    }
                                    if ($scope.filter.typeCategory == 1) {
                                        xres[i].TypeFollow = "H-1";
                                    } else if ($scope.filter.typeCategory == 2) {
                                        xres[i].TypeFollow = "J-N";
                                    } else if ($scope.filter.typeCategory == 3) {
                                        xres[i].TypeFollow = "J+N";
                                    } else if ($scope.filter.typeCategory == 4) {
                                        xres[i].TypeFollow = "Need Approval";
                                    }
                                    // } else if ($scope.filter.typeCategory == 3) {
                                    //     xres[i].TypeFollow = "No Show";
                                    // }
                                }
                            }
                        }
                        console.log("xresssss", xres);
                        var gridData = xres;
                        $scope.grid.data = gridData;
                        $scope.gridFrontParts.data = [];
                        $scope.loading = false;
                    })
                )
            } else if ($scope.filter.typeCategory > 4 && $scope.filter.firstDate != null && $scope.filter.lastDate != null) {
                var a = new Date($scope.filter.firstDate);
                var yearFirst = a.getFullYear();
                var monthFirst = a.getMonth() + 1;
                var dayFirst = a.getDate();
                var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;

                var b = new Date($scope.filter.lastDate);
                var yearLast = b.getFullYear();
                var monthLast = b.getMonth() + 1;
                var dayLast = b.getDate();
                var lastDate = yearLast + '-' + monthLast + '-' + dayLast;
                // =================
                var today = new Date();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                var year = today.getFullYear();
                var month = today.getMonth() + 1;
                var dayLast = today.getDate();
                m = checkTime(m);
                s = checkTime(s);
                var lastDateNya = year + '-' + month + '-' + dayLast;
                var timeDateNya = h + '%20' + m + '%20' + s;
                xres = [];
                return $q.resolve(
                    FollowUpGrService.getData(firstDate, lastDate, $scope.filter.typeCategory, lastDateNya, timeDateNya).then(function(res) {
                        xres = [];
                        if (res.data.Result.length > 0) {
                            for (var i = 0; i < res.data.Result.length; i++) {
                                // if (res.data.Result[i].Status == 0){
                                xres.push(res.data.Result[i]);
                                // }
                            }
                            for (var i = 0; i < xres.length; i++) {
                                if (xres[i].Status == 0 && xres[i].AppointmentNo !== null) {
                                    if (xres[i].isFoOneDay == 1 || xres[i].isFoOneHour == 1) {
                                        xres[i].Xstatus = "On Schedule";
                                    } else if (xres[i].NewAppointmentRel !== null) {
                                        xres[i].Xstatus = "Reschedule";
                                    } else {
                                        xres[i].Xstatus = "Outstanding";
                                    }
                                } else if (xres[i].Status == 0 && xres[i].AppointmentNo == null) {
                                    xres[i].Xstatus = "Draft";
                                } else if (xres[i].Status == 1) {
                                    xres[i].Xstatus = "Cancel";
                                } else if (xres[i].Status == 2) {
                                    xres[i].Xstatus = "No Show";
                                } else if (xres[i].Status >= 3) {
                                    xres[i].Xstatus = "Show";
                                }
                                if ($scope.filter.typeCategory == 5) {
                                    xres[i].TypeFollow = "History";
                                } else if ($scope.filter.typeCategory == 6) {
                                    xres[i].TypeFollow = "Upcoming Appointment";
                                } else if ($scope.filter.typeCategory == 7) {
                                    xres[i].TypeFollow = "Need Approval";
                                }
                                xres[i].Category = "GR"
                            }
                        }
                        var gridData = xres;
                        $scope.grid.data = gridData;
                        $scope.gridFrontParts.data = [];
                        console.log('res====>', gridData);
                        $scope.loading = false;
                    })
                );
            } else {
                // bsNotify.show({
                //     size: 'big',
                //     type: 'danger',
                //     title: "Mohon Input Filter",
                //     // content: error.join('<br>'),
                //     // number: error.length
                // });
                bsAlert.alert({
                    title: "Filter Belum Terisi",
                    text: "Mohon Input Filter",
                    type: "warning",
                    showCancelButton: true
                });
                $scope.grid.data = [];
            }
            $scope.loading = false;
        };
        // ============================================================

        $scope.typeFollowUp = [{
                Id: 1,
                Name: "H-1"
            },
            {
                Id: 2,
                Name: "J-N"
            },
            {
                Id: 3,
                Name: "J+N"
            },
            {
                Id: 5,
                Name: "History"
            },
            {
                Id: 6,
                Name: "Upcoming Appointment"
            }

        ];
        // $scope.typeFollowUp = [{
        //         Id: 1,
        //         Name: "H-1"
        //     },
        //     {
        //         Id: 2,
        //         Name: "J-N"
        //     },
        //     {
        //         Id: 3,
        //         Name: "J+1"
        //     },
        //     {
        //         Id: 4,
        //         Name: "All"
        //     }
        // ];
        $scope.startMinOption = null;
        $scope.startMaxOption = null;
        $scope.endMinOption = null;
        $scope.endMaxOption = null;

        $scope.selectModel = function(selected) {
            console.log("selectModel selected : ", selected);
            // ini edit doang harusnya
            $scope.mDataDetail.VehicleTypeId      = "";
            $scope.mDataDetail.KatashikiCodex    = "";
            $scope.mDataDetail.VehicleTypeId = "";

            $scope.mDataDetail.VehicleModelName = selected.VehicleModelName;
            $scope.mData.ModelType = selected.VehicleModelName;
            $scope.mData.LicensePlate = $scope.mDataDetail.LicensePlate;
            // MrsList.getDataTypeByModel(selected.VehicleModelId).then(function(res) {
            WO.getVehicleTypeById(selected.VehicleModelId).then(function(res) {
                console.log("result : ", res);
                //$scope.typeData = res.data;
                // $scope.typeData = _.uniq(res.data.Result, 'KatashikiCode');

                $scope.katashikiData = _.uniq(res.data.Result, 'KatashikiCode');

                for (var i=0; i<$scope.katashikiData.length; i++){
                    $scope.katashikiData[i].NewDescription = selected.VehicleModelName + ' - ' + $scope.katashikiData[i].KatashikiCode
                }
            });
            $scope.mDataDetail.KatashikiCode = ""
                // $scope.selectedModel = {};
                // if(selected !== null) {
                //   $scope.selectedModel = selected;
                // }else {
                //   $scope.selectedModel = {};
                // }
        };

        $scope.selectKatashiki = function (selected) {
            $scope.mDataDetail.Description      = "";
            $scope.mDataDetail.VehicleTypeId = "";

            if(selected != null && selected != undefined){
                AppointmentGrService.GetCVehicleTypeListByKatashiki(selected.KatashikiCode).then(function(res) {
                    // $scope.typeData = res.data.Result[0];
                    $scope.typeData = res.data.Result;


                    for (var z = 0; z < $scope.typeData.length; z++) {
                        $scope.typeData[z].NewDescription = $scope.typeData[z].Description + ' - ' + $scope.typeData[z].KatashikiCode + ' - ' + $scope.typeData[z].SuffixCode
                    }

                    $scope.mDataDetail.KatashikiCode    = selected.KatashikiCode;
    
                })
            } else {
                $scope.mDataDetail.KatashikiCodex    = "";
                $scope.mDataDetail.KatashikiCode    = "";
                $scope.mDataDetail.Description   = "";
                $scope.mDataDetail.VehicleTypeId = "";
            }
            
        }

        $scope.getVehicleTypeById = function(vId) {

        };
        $scope.selectType = function(selected) {
            console.log("selected", selected);
            $scope.mDataDetail.KatashikiCode = selected.KatashikiCode;
            $scope.mDataDetail.Description = selected.Description;
            $scope.mDataDetail.VehicleTypeId = selected.VehicleTypeId;

            AppointmentGrService.getDataTaskList($scope.mDataDetail.KatashikiCode).then(function(result) {
                console.log("ketemu", result);
            });
            AppointmentGrService.getDataTaskListByKatashiki($scope.mDataDetail.KatashikiCode).then(function(restask) {
                taskList = restask.data.Result;
                var tmpTask = {};
                if (taskList.length > 0) {
                    for (var i = 0; i < taskList.length; i++) {
                        if (taskList[i].ServiceRate !== undefined && taskList[i].WarrantyRate !== undefined) {
                            tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                            tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                            $scope.PriceAvailable = true;
                        } else {
                            tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                            tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                        }
                        $scope.tmpServiceRate = tmpTask;
                    }
                } else {
                    $scope.tmpServiceRate = null;
                }
            });
        }
        var linkSaveCb = function(mode, model) {
            console.log("tmpPrediagnose linkSaveCb", tmpPrediagnose);
        };
        var linkBackCb = function(mode, model) {
            console.log("mode", mode);
            console.log("mode", model);
            // $scope.tabShowed = false;
        };
        $scope.preDiagnose = function() {
            $scope.showPrediagnose = true;
            $scope.dataPrediagnose = angular.copy(tmpPrediagnose);
        };
        $scope.cancelPreDiagnose = function() {
            $scope.showPrediagnose = false;
        };
        $scope.savePreDiagnose = function(data, mode) {
            console.log("savePreDiagnose mode", mode);
            console.log("savePreDiagnose data", data);
            tmpPrediagnose = data;
            $scope.showPrediagnose = false;
        };
        $scope.showTab = function() {
            $scope.tabShowed = true;
            // $scope.formApi.showLinkView(linkSaveCb, linkBackCb, 'true', 'true');
            $timeout(function() {
                var vobj = {
                    mode: $scope.asbTab.showMode,
                    currentDate: $scope.asbTab.currentDate,
                    // nopol: $scope.asbTab.nopol,
                    // tipe: $scope.asbTab.tipe
                }
                showBoardTab(vobj);

                // $timeout( function(){
                //     minuteTick();
                // }, 100);
                minuteTick();

            }, 100);
        };

        $scope.backGrid = function() {
            $scope.hideGridParts = false;
            $scope.tabShowed = false;
        };
        $scope.disableButton = function() {
            $("button[uib-tooltip='Lihat']").attr('ng-disabled', false).attr('skip-disable', true).removeAttr('disabled');
            $scope.sumAllPrice();
        }
        $scope.onShowDetail = function(data, mode) {
            console.log('data detail', data)
            $scope.restart_digit_odo = 0
            $scope.isKmHistory = 0
            $scope.nilaiKM = 0
            $scope.KMTerakhir = 0

            $scope.restart_digit_odo = 0;
            $scope.model_info_km.show = false;

            $scope.ShowEdit = true;
            $scope.EnableEdit = true;
            $scope.ShowEditInstitusi = true;
            $scope.EnableEditInstitusi = true;
            $scope.EnableEditContactPerson = true;
            $scope.hideGridParts = true;
            $scope.JobComplaint = [];
            $scope.JobRequest = [];
            $scope.gridWork = [];
            $scope.mDataDetail = {};
            var DiscountTaxB = [];
            var DiscountPartsB = [];
            $scope.DiscountParts = 0;
            $scope.DiscountTask = 0;
            $scope.totalDpDefault = 0;
            $scope.tmpActRateChange = 0;
            $scope.tmpActRate = 1;
            $scope.asb = {};
            $scope.asb = {
                NewChips: 1,
                showMode: 'main',
            }
            if (data.JobId != null) {
                FollowUpGrService.getWobyJobId(data.JobId).then(function(res) {
                    if (mode == 'view') {
                        console.log("disable, brooo", $scope.modeDetail);
                        $scope.disableButton();
                    }
                    console.log("resss getWobyJobId", res);
                    var dataWo = res.data.Result[0]
                    console.log('dataWo', dataWo);
                    console.log('dataWo.viN', dataWo.VIN);
                    
                    FollowUpGrService.getDataVIN(dataWo.VIN).then(function(res){
                        console.log('cek input rangka 1', dataWo.VIN)
                        var dataVIN = res.data.Result;
                        console.log('cek res input 1', $scope.tempVIN);
                        if (dataVIN.length > 0) {
                            if( dataVIN == "" ||  dataVIN == null ||  dataVIN == undefined){
                                var filtered = $scope.taskCategory.filter(function(item) { 
                                    return item.Name !== "FLC - Free Labor Claim" && item.Name !== "FPC - Free Parts Claim" && item.Name !== "PLC - Parts Labor Claim" ;  
                                 });
                                 console.log("filtered",filtered);
                                 $scope.taskCategory = filtered;
                                 console.log("$scope.taskCategory final",$scope.$scope.taskCategory);
                            }else{
                                for (var i = 0; i < dataVIN.length; i++) {
                                    if (dataVIN[i].ClaimType == "FPC - Free Parts Claim") {
                                        $scope.taskCategory = $scope.notFlcPlc
                                    }else if(dataVIN[i].ClaimType == "FLC - Free Labor Claim"){
                                        $scope.taskCategory = $scope.notFpcPlc
                                    }else if(dataVIN[i].ClaimType == "PLC - Parts Labor Claim"){
                                        $scope.taskCategory = $scope.notFlcFpc
                                    }else{
                                        $scope.taskCategory = filtered;
                                    }
                                }
                            }
                        }else{
                            $scope.taskCategory = $scope.notFlcPlcFpc
                        }
                    });

                    $scope.nilaiKM = angular.copy(dataWo.Km)

                    var tmpFixed = new Date(dataWo.FixedDeliveryTime1);
                    var hourFixed = tmpFixed.getHours();
                    var minuteFixed = tmpFixed.getMinutes();
                    dataWo.FixedDeliveryTime1 = (hourFixed > 9 ? hourFixed : "0" + hourFixed) + ":" + minuteFixed + ":00";
                    
                    $scope.mData = dataWo;
                    // console.log('$scope.mData.DPRequest ===>',$scope.mData.JobTask[0].JobParts[0].DPRequest);
                    // console.log('$scope.mData.DownPayment ===>',$scope.mData.JobTask[0].JobParts[0].DownPayment);
                    console.log('$scope.mData.isCash ===>',$scope.mData.isCash);
                    console.log('$scope.mData ===>',$scope.mData);
                    $scope.choosePembayaran($scope.mData.isCash);

                    for (var a = 0; a < dataWo.JobTask.length; a++) {
                        // $scope.DiscountTask = dataWo.JobTask[a].Discount;
                        DiscountTaxB.push(dataWo.JobTask[a].Discount);
                        var maxValue = _.max(DiscountTaxB);
                        for (var b = 0; b < dataWo.JobTask[a].JobParts.length; b++) {
                            // console.

                            // console.log("bgst woi", dataWo.JobTask[a].JobParts[b].Part.RetailPrice);
                            DiscountPartsB.push(dataWo.JobTask[a].JobParts[b].Discount);
                            

                            var maxValueParts = _.max(DiscountPartsB);
                        }
                    }
                    
                    $scope.DiscountTask = maxValue;
                    // $scope.DiscountParts = maxValueParts;
                    $scope.DiscountParts = maxValueParts == undefined || null ? 0 : maxValueParts;
                    // console.log("DiscountTaxB",DiscountTaxB);
                    // console.log("maxValue",maxValue);
                    // console.log("$scope.DiscountTask",$scope.DiscountTask);
                    // console.log("$scope.DiscountParts",$scope.DiscountParts);
                    var Ref = dataWo.AppointmentNo.replace(/\//g, '%20');
                    FollowUpGrService.getJobPartsOrder(Ref).then(function(resu) {
                        $scope.StatusPartsOrder = resu.data.Result;
                        console.log("$scope.StatusPartsOrder DisableQty", $scope.StatusPartsOrder);
                        _.map(dataWo.JobTask, function(resu1) {
                            _.map(resu1.JobParts, function(resu2) {
                                console.log("resu2", resu2);
                                
                                _.map($scope.StatusPartsOrder, function(resu3) {
                                    console.log("resu3", resu3);
                                    if (resu2.PartsId == resu3.PartId) {
                                        resu2.DisableQty = 1;
                                    } else {
                                        resu2.DisableQty = 0;
                                    };
                                });
                            });
                        });
                    });
                    console.log("dataWo DisableQty", dataWo);
                    console.log("$scope.mData.Km", $scope.mData.Km);
                    $scope.mData.Km = String($scope.mData.Km);
                    $scope.mData.Km = $scope.mData.Km.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                    $scope.JobRequest = dataWo.JobRequest;
                    $scope.JobComplaint = dataWo.JobComplaint;
                    $scope.firstAppointmentDate = angular.copy(dataWo.AppointmentDate);
                    $scope.firstPlanStart = angular.copy(dataWo.PlanStart);
                    $scope.firstPlanFinish = angular.copy(dataWo.PlanFinish);
                    console.log("firstAppointmentDate", $scope.firstAppointmentDate);
                    console.log("firstPlanStart", $scope.firstPlanStart);
                    console.log("firstPlanFinish", $scope.firstPlanFinish);

                    var tmpJob = dataWo.JobTask;
                    
                    // _.map(dataWo.JobTask, function(resu1) {
                    //     _.map(resu1.JobParts, function(resu2) {  
                    //         console.log("resnyda doang",resu2);  
                    //         console.log("resu2 bibi", resu2.RetailPrice);
                    //     });
                    // });
                    var subTotal = 0;
                    var RetailPrice = 0;
                    var Qty = 0;
                    var Discount = 0;
                    for (var i = 0; i < tmpJob.length; i++){
                        for (var j = 0; j < tmpJob[i].JobParts.length; j++){
                            console.log("biji",tmpJob[i].JobParts[j].Discount);
                            console.log("tmpJob[i].JobParts[j].RetailPrice",tmpJob[i].JobParts[j].Price)
                            Qty = tmpJob[i].JobParts[j].Qty;
                            RetailPrice = tmpJob[i].JobParts[j].Price;


                            //tambahan CR4  start ==> request okrivia (qty * harga parts ) - diskon
                            if(tmpJob[i].JobParts[j].PaidById == 2277){
                                tmpJob[i].JobParts[j].Discount = 0;
                                tmpJob[i].JobParts[j].DiskonForDisplay = '0%';
                                tmpJob[i].JobParts[j].NominalDiscount = 0;
                                tmpJob[i].JobParts[j].subTotalForDisplay = 0;
                                tmpJob[i].JobParts[j].subTotal = 0;
                            // }else if(tmpJob[i].JobParts[j].PaidById == 30 || tmpJob[i].JobParts[j].PaidById == 31 || tmpJob[i].JobParts[j].PaidById == 32 || tmpJob[i].JobParts[j].MaterialTypeId == 2){
                            // }else if(tmpJob[i].JobParts[j].PaidById == 30 || tmpJob[i].JobParts[j].MaterialTypeId == 2){
                            }else if(tmpJob[i].JobParts[j].PaidById == 30){
                                subTotal = tmpJob[i].JobParts[j].Price * tmpJob[i].JobParts[j].Qty;
                                Discount = 0
                                tmpJob[i].JobParts[j].Discount = 0;
                                tmpJob[i].JobParts[j].DiskonForDisplay = '0%';
                                tmpJob[i].JobParts[j].NominalDiscount = (Qty*RetailPrice)*(Discount/100);
                                tmpJob[i].JobParts[j].subTotalForDisplay = subTotal - (Qty*RetailPrice)*(Discount/100);
                                tmpJob[i].JobParts[j].subTotal = tmpJob[i].JobParts[j].Price * tmpJob[i].JobParts[j].Qty;
                            }else{
                                subTotal = tmpJob[i].JobParts[j].Price * tmpJob[i].JobParts[j].Qty;
                                Discount = tmpJob[i].JobParts[j].Discount;
                                tmpJob[i].JobParts[j].DiskonForDisplay = String(tmpJob[i].JobParts[j].Discount) +'%';
                                tmpJob[i].JobParts[j].NominalDiscount = (Qty*RetailPrice)*(Discount/100);
                                tmpJob[i].JobParts[j].subTotalForDisplay = subTotal - (Qty*RetailPrice)*(Discount/100);
                                tmpJob[i].JobParts[j].subTotal = tmpJob[i].JobParts[j].Price * tmpJob[i].JobParts[j].Qty;
                            }
                            // tambahan CR4 end


                            




                            console.log('DiskonForDisplay   ===>',tmpJob[i].JobParts[j].DiskonForDisplay);
                            console.log('NominalDiscount    ===>',tmpJob[i].JobParts[j].NominalDiscount);
                            console.log('subTotalForDisplay ===>',tmpJob[i].JobParts[j].subTotalForDisplay);
                            console.log('Qty,RetailPrice,subTotal,Discount ===>',Qty+'|'+RetailPrice+'|'+subTotal+'|'+Discount);
                        }
                    }

                    dataWo.JobTask = [];
                    dataWo.JobTask = tmpJob;

                    console.log('dataForBslist sentuhan CR4 ===>',dataWo);



                    $scope.dataForBslist(dataWo);
                    $scope.dataForPrediagnose(dataWo);
                    if (dataWo.VehicleId !== null) {
                        FollowUpGrService.getDataVehicle(dataWo.VehicleId).then(function(ress) {
                            // if($scope.modeDetail == 'view'){$scope.disableButton();}
                            $scope.mDataDetail = ress.data.Result[0];

                            $scope.tmpIsNonTAM = null;
                            if ($scope.mDataDetail.VehicleNonTAMId != null){
                                $scope.mDataDetail.KatashikiCode = $scope.mDataDetail.NonTamKatashikiCode
                                $scope.mDataDetail.ColorName = $scope.mDataDetail.NonTamColorName
                                $scope.mDataDetail.VehicleTypeDesc = $scope.mDataDetail.NonTamModelType
                                $scope.mDataDetail.VehicleModelName = $scope.mDataDetail.NonTamModelName
                                $scope.tmpIsNonTAM = $scope.mDataDetail.VehicleNonTAMId;
                            }
                            $scope.getAllParameter(ress.data.Result[0]);
                            console.log("onShowDetail masuknya", ress);
                            if (ress.data.Result.length <= 0) {
                                $scope.vehicleStatus = 1;
                            } else {
                                $scope.vehicleStatus = 0;
                                console.log("kalo CRM");
                            }
                            AppointmentGrService.getDataTaskList($scope.mDataDetail.KatashikiCode).then(function(result) {
                                console.log("result", result);
                                if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                            });
                            AppointmentGrService.getDataTaskListByKatashiki($scope.mDataDetail.KatashikiCode).then(function(restask) {
                                taskList = restask.data.Result;
                                console.log("resTask", taskList);
                                var tmpTask = {};
                                if (taskList.length > 0) {
                                    for (var i = 0; i < taskList.length; i++) {
                                        if (taskList[i].ServiceRate !== undefined && taskList[i].WarrantyRate !== undefined) {
                                            tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                                            tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                                            $scope.PriceAvailable = true;
                                        } else {
                                            tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                                            tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                                        }
                                        $scope.tmpServiceRate = tmpTask;
                                    }
                                } else {
                                    $scope.tmpServiceRate = null;
                                }
                            });
                            AppointmentGrService.getDataContactPerson($scope.mDataDetail.CustomerVehicleId).then(function(res) {
                                if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                                $scope.ContactPersonData = res.data.Result;
                            });

                            $scope.unDisable($scope.mDataDetail.VIN)
                        });
                        AppointmentGrService.getDataVehiclePSFU(dataWo.VehicleId).then(function(resu) {
                            console.log("getDataVehiclePSFU", resu.data.Result);
                            var dataQuestionAnswerPSFU = resu.data.Result[0];
                            $scope.mDataDetail.Suggest = dataQuestionAnswerPSFU.JobSuggest;
                            FollowupService.getQuestion().then(function(resu2) {
                                console.log("getQuestion", resu2.data.Result);
                                var dataQuestionAnswer = resu2.data.Result;
                                var arr = [];
                                _.map(dataQuestionAnswer, function(val, key) {
                                    var obj = {};
                                    if (dataQuestionAnswerPSFU.QuestionId1 == val.QuestionId) {
                                        obj.QuestionId = val.QuestionId;
                                        obj.Description = val.Description;
                                        obj.Answer = val.Answer;
                                        obj.Reason = dataQuestionAnswerPSFU.Reason1 ? dataQuestionAnswerPSFU.Reason1 : null;
                                        obj.ReasonStatus = dataQuestionAnswerPSFU.Reason1 ? false : true;
                                        obj.jawaban = dataQuestionAnswerPSFU.AnswerId1
                                        arr.push(obj);
                                    } else if (dataQuestionAnswerPSFU.QuestionId2 == val.QuestionId) {
                                        obj.QuestionId = val.QuestionId;
                                        obj.Description = val.Description;
                                        obj.Answer = val.Answer;
                                        obj.Reason = dataQuestionAnswerPSFU.Reason2 ? dataQuestionAnswerPSFU.Reason1 : null;
                                        obj.ReasonStatus = dataQuestionAnswerPSFU.Reason2 ? false : true;
                                        obj.jawaban = dataQuestionAnswerPSFU.AnswerId2
                                        arr.push(obj);
                                    } else if (dataQuestionAnswerPSFU.QuestionId3 == val.QuestionId) {
                                        obj.QuestionId = val.QuestionId;
                                        obj.Description = val.Description;
                                        obj.Answer = val.Answer;
                                        obj.Reason = dataQuestionAnswerPSFU.Reason3 ? dataQuestionAnswerPSFU.Reason3 : null;
                                        obj.ReasonStatus = dataQuestionAnswerPSFU.Reason3 ? false : true;
                                        obj.jawaban = dataQuestionAnswerPSFU.AnswerId3
                                        arr.push(obj);
                                    };
                                    // arr.push(obj);
                                });
                                console.log('arr >>>', arr);
                                $scope.Question = arr;
                                if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                            });
                        });
                    } else {
                        $scope.mDataDetail = {};
                        $scope.getAllParameter();
                        $scope.vehicleStatus = 1;
                        console.log("kalo bukan CRM", $scope.modelData);
                        $scope.mDataDetail.LicensePlate = dataWo.PoliceNumber.toUpperCase();
                        $scope.mDataDetail.VIN = dataWo.VIN;
                        $scope.mDataDetail.KatashikiCode = dataWo.KatashikiCode;
                        AppointmentGrService.getDataTaskListByKatashiki($scope.mDataDetail.KatashikiCode).then(function(restask) {
                            taskList = restask.data.Result;
                            console.log("resTask", taskList);
                            var tmpTask = {};
                            if (taskList.length > 0) {
                                for (var i = 0; i < taskList.length; i++) {
                                    if (taskList[i].ServiceRate !== undefined && taskList[i].WarrantyRate !== undefined) {
                                        tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                                        tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                                        $scope.PriceAvailable = true;
                                    } else {
                                        tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                                        tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                                    }
                                    $scope.tmpServiceRate = tmpTask;
                                }
                            } else {
                                $scope.tmpServiceRate = null;
                            }
                        });
                        _.map($scope.modelData, function(val) {
                            if (val.VehicleModelName === dataWo.ModelType || val.VehicleModelName === dataWo.VehicleTypeId) {
                                console.log('masuk kesini model val', val);
                                // $scope.mDataDetail.VehicleModelName = val.VehicleModelId;
                                $scope.mDataDetail.VehicleModelId = val.VehicleModelId;
                                $scope.mDataDetail.VehicleModelName = val.VehicleModelName;
                                console.log('masuk kesini model', $scope.mDataDetail);
                                // MrsList.getDataTypeByModel(val.VehicleModelId).then(function(res) {
                                WO.getVehicleTypeById(val.VehicleModelId).then(function(res) {
                                    if ($scope.modeDetail == 'view') { $scope.disableButton(); }
                                    console.log("result : ", res);
                                    //$scope.typeData = res.data;
                                    // $scope.typeData = _.uniq(res.data.Result, 'KatashikiCode');

                                    $scope.katashikiData = _.uniq(res.data.Result, 'KatashikiCode');

                                    for (var i=0; i<$scope.katashikiData.length; i++){
                                        $scope.katashikiData[i].NewDescription = $scope.mDataDetail.VehicleModelName + ' - ' + $scope.katashikiData[i].KatashikiCode
                                    }

                                    for (var i=0; i < $scope.katashikiData.length; i++) {
                                        if ($scope.katashikiData[i].KatashikiCode == dataWo.VehicleType.KatashikiCode) {
                                            $scope.mDataDetail.KatashikiCodex   = dataWo.VehicleType.KatashikiCode;

                                        }
                                    }

                                    if ($scope.mDataDetail.KatashikiCodex != null && $scope.mDataDetail.KatashikiCodex != undefined && $scope.mDataDetail.KatashikiCodex != '') {

                                        AppointmentGrService.GetCVehicleTypeListByKatashiki(dataWo.VehicleType.KatashikiCode).then(function(res) {
                                            // $scope.typeData = res.data.Result[0];
                                            $scope.typeData = res.data.Result;

                                            // _.map($scope.typeData, function(a) {
                                            //     if (a.KatashikiCode === dataWo.KatashikiCode) {
                                            //         console.log('masuk kesini type val', a);
                                            //         $scope.mDataDetail.VehicleTypeId = a.VehicleTypeId;
                                            //         $scope.mDataDetail.Description = a.Description;
                                            //         console.log('masuk kesini type', $scope.mDataDetail);
                                            //     }
                                            // })
                                            for (var z = 0; z < $scope.typeData.length; z++) {
                                                $scope.typeData[z].NewDescription = $scope.typeData[z].Description + ' - ' + $scope.typeData[z].KatashikiCode + ' - ' + $scope.typeData[z].SuffixCode
                                            }

                                            for (var z = 0; z < $scope.typeData.length; z++) {
                                                if($scope.typeData[z].VehicleTypeId == dataWo.VehicleType.VehicleTypeId){
                                                    $scope.selectType($scope.typeData[z])
                                                    console.log('Type Gue ===> ', dataWo.VehicleType.VehicleTypeId);
                                                }
                                            }
                                        })

                                    }

                                    
                                });
                            }
                        });
                        if(dataWo.ModelType != null){

                            $scope.mDataDetail.VehicleModelName = dataWo.ModelType;
                        }
                    }
                });
            }
        };
        $scope.unDisable = function(vin) {
            console.log("wanjay");
            $("#vehicleNya").find('*').removeAttr('disabled', '');

            $scope.MRS.category = 2;
            var category = 2;
            var VIN = vin;
            var startDate = '-';
            var endDate = '-';
            var param = 1;      // tab riwayat servis
            $scope.ServiceHistory(category, VIN, startDate, endDate, param);
        }
        $scope.mDataVehicleHistory = [];
        $scope.change = function(startDate, endDate, category, vin) {

            console.log("start Date 1", startDate);
            console.log("start Date 2", endDate);
            category = category.toString();
            var copyAngular = angular.copy(category);
            console.log("category", category);
            console.log("Copycategory", copyAngular);
            console.log("vin", vin);
            if (category == "" || category == null || category == undefined) {
                bsNotify.show({
                    size: 'big',
                    title: 'Mohon Isi Kategori Pekerjaan',
                    text: 'I will close in 2 seconds.',
                    timeout: 2000,
                    type: 'danger'
                });
            } else {

                if (startDate == undefined || startDate == "" || startDate == null || startDate == "Invalid Date") {
                    bsNotify.show({
                        size: 'big',
                        title: 'Mohon Isi periode servis',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                } else {

                    var tmpDate = startDate;
                    var tmpMonth = startDate;
                    console.log("tmpMonth", tmpMonth);
                    var tmpTime = $scope.startDate;
                    // if (tmpDate.getMonth() < 10) {
                    // tmpMonth = "0" + (tmpDate.getMonth() + 1);
                    // } else {
                    tmpMonth = (tmpDate.getMonth() + 1).toString();
                    // }
                    var ini = tmpDate.getFullYear() + "-" + (tmpMonth[1] ? tmpMonth : "0" + tmpMonth[0]) + "-" + tmpDate.getDate();
                    var d = new Date(ini);
                    console.log('data yang diinginkan', ini);
                    startDate = ini;
                };


                if (endDate == undefined || endDate == "" || endDate == null || endDate == "Invalid Date") {
                    bsNotify.show({
                        size: 'big',
                        title: 'Mohon Isi periode servis',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                } else {

                    var tmpDate2 = endDate;
                    var tmpMonth2 = endDate;
                    console.log("tmpMonth2", tmpMonth2);
                    var tmpTime2 = endDate;

                    // if (tmpDate2.getMonth() < 10) {
                    //     tmpMonth2 = "0" + (tmpDate2.getMonth() + 1);
                    // } else {
                    tmpMonth2 = (tmpDate2.getMonth() + 1).toString();
                    // }
                    var ini2 = tmpDate2.getFullYear() + "-" + (tmpMonth2[1] ? tmpMonth2 : "0" + tmpMonth2[0]) + "-" + tmpDate2.getDate();
                    var d2 = new Date(ini2);
                    console.log('data yang diinginkan 2', ini2);
                    endDate = ini2;
                };

                if (typeof vin !== "undefined") {
                    category = parseInt(category);
                    $scope.ServiceHistory(category, vin, startDate, endDate);
                } else {
                    bsNotify.show({
                        size: 'big',
                        title: 'VIN Tidak Ditemukan',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                };
            };
        };

        $scope.getUnique = function (array){
            var uniqueArray = [];
            
            // Loop through array values
            for(i=0; i < array.length; i++){
                if(uniqueArray.indexOf(array[i]) === -1) {
                    uniqueArray.push(array[i]);
                }
            }
            return uniqueArray;
        }

        $scope.ServiceHistory = function(category, vin, startDate, endDate, param) {
            VehicleHistory.getData(category, vin, startDate, endDate).then(
                function(res) {
                    if (res.data.length == 0) {
                        $scope.mDataVehicleHistory = [];
                        bsNotify.show({
                            size: 'big',
                            title: 'Data Riwayat Service Tidak Ditemukan',
                            text: 'I will close in 2 seconds.',
                            timeout: 2000,
                            type: 'danger'
                        });
                    } else {
                        res.data.sort(function(a, b) {
                            // Turn your strings into dates, and then subtract them
                            // to get a value that is either negative, positive, or zero.
                            return new Date(b.ServiceDate) - new Date(a.ServiceDate);
                        });
                        for(var i in res.data){
                            var tmpArray = [];
                            for(var j = 0; j<res.data[i].VehicleJobService.length; j++){
                                if(res.data[i].VehicleJobService[j].KTPNo == null || res.data[i].VehicleJobService[j].KTPNo == undefined){
                                    res.data[i].VehicleJobService[j].KTPNo = ''
                                }
                                if(!(res.data[i].VehicleJobService[j].KTPNo.includes('/'))){
                                    if(res.data[i].VehicleJobService[j].EmployeeName !== null){
                                        tmpArray.push(res.data[i].VehicleJobService[j].EmployeeName);
                                    }
                                }
                                
                            }
                            tmpArray = $scope.getUnique(tmpArray);
                            res.data[i].namaTeknisi = tmpArray.join(', ');
                        }
                        $scope.mDataVehicleHistory = res.data;

                        $scope.isKmHistory = $scope.mDataVehicleHistory.length;

                        if ($scope.isKmHistory >= 1) {

                            $scope.KMTerakhir = $scope.mDataVehicleHistory[0].Km;

                            $scope.restart_digit_odo = 0
                            WO.GetResetCounterOdometer(vin).then(function(res) {
                                $scope.restart_digit_odo = res.data
                            })

                        }

                        console.log("kesini ga?");
                        $scope.value = angular.copy($scope.mDataVehicleHistory);

                        $scope.MRS.category = category;
                        
                        if (param == 1) {       // tab riwayat servis

                            var dateEnd = $scope.mDataVehicleHistory[0].ServiceDate;
                            $scope.MRS.endDate = dateEnd;

                            var terakhir = $scope.mDataVehicleHistory.length;
                            $scope.MRS.startDate = $scope.mDataVehicleHistory[terakhir - 1].ServiceDate;

                        }
                        console.log('category', $scope.MRS.category);
                        console.log('startDate', $scope.MRS.startDate);
                        console.log('EndDate', $scope.MRS.endDate);
                    };

                    console.log("mDataVehicleHistory=> Followup ", $scope.mDataVehicleHistory);
                },
                function(err) {}
            );
        }

        $scope.dataForPrediagnose = function(data) {
            FollowUpGrService.getPreDiagnoseByJobId(data.JobId).then(function(res) {
                tmpPrediagnose = res.data;
                tmpPrediagnose.PrediagStallId = tmpPrediagnose.StallId;

                var co = ["PosisiShiftP", "PosisiShiftN", "PosisiShiftD", "PosisiShiftS", "PosisiShiftR", "PosisiShiftOne", "PosisiShiftTwo", "PosisiShiftThree", "PosisiShiftFour", "PosisiShiftLainnya"];

                var total = tmpPrediagnose.PositionShiftLever;
                var arrBin = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512];
                for (var i in arrBin) {
                    if (total & arrBin[i]) {
                        var xc = co[i];
                        tmpPrediagnose[xc] = arrBin[i];


                        // console.log(co[i]+' : nyala');
                    }
                }

                if ($scope.modeDetail == 'view') {
                    $scope.disableButton();
                }
                console.log("resss Prediagnosenya bosque", res.data);
            });
        }
        var Parts = [];
        $scope.dataForBslist = function(data) {
            gridTemp = []
            Parts.splice();
            Parts = [];
            if (data.JobTask.length > 0) {
                var tmpJob = data.JobTask;
                for (var i = 0; i < tmpJob.length; i++) {
                    console.log("tmpJob dataForBslist ===>", tmpJob);
                    // tadinya off
                    // for (var j = 0; j < tmpJob[i].JobParts.length; j++) {
                    //     console.log("tmpJob[i].JobParts[j]", tmpJob[i].JobParts[j]);
                    //     if (tmpJob[i].JobParts[j].PaidBy !== null) {
                    //         tmpJob[i].JobParts[j].PaidBy = tmpJob[i].JobParts[j].PaidBy.Name;
                    //         delete tmpJob[i].JobParts[j].PaidBy;
                    //     };
                    //     if (tmpJob[i].JobParts[j].Satuan !== null) {
                    //         tmpJob[i].JobParts[j].satuanName = tmpJob[i].JobParts[j].Satuan.Name;
                    //         delete tmpJob[i].JobParts[j].Satuan;
                    //     };
                    //     tmpJob[i].subTotal = tmpJob[i].Qty * tmpJob[i].RetailPrice;
                    //     // $scope.getAvailablePartsServiceManual(tmpJob[i].JobParts[j]);

                    //     // Parts.push(tmpJob[i].JobParts[j]);
                    // };
                    // tadinya off
                };
                // added by sss on 2017-11-03
                $scope.getAvailablePartsServiceManual(tmpJob, 0, 0);
                // 
                $scope.gridWork = gridTemp;
                $scope.gridOriginal = gridTemp;
                $scope.sumAllPrice();
                // $scope.onAfterSave();
                // console.log("totalw", $scope.totalWork);
            };
            // $scope.sumAllPrice();
            console.log("$scope.gridWork", $scope.gridWork);
            console.log("$scope.JobRequest", $scope.JobRequest);
            console.log("$scope.JobComplaint", $scope.JobComplaint);
        };
        $scope.countPrice = function() {
            // ini function ga kepake kaya nya abaikan perhitungan di function ini kecuali tau kapan di panggil nya
            var totalW = 0;
            var totalWnotWarranty = 0;
            $scope.adaWarranty = 0;
            $scope.jmlJobWarranty = $scope.gridWork.length;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                console.log("$scope.gridWork[i]", $scope.gridWork[i]);
                if ($scope.gridWork[i].Price !== undefined) {

                    // if ($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30 || $scope.gridWork[i].PaidById == 31 || $scope.gridWork[i].PaidById == 32){
                    if ($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                        totalW += 0
                    } else {
                        // totalW += $scope.gridWork[i].Summary;
                        totalW += Math.round($scope.gridWork[i].Price / 1.1);
                    }

                    if ($scope.gridWork[i].PaidById == 30){
                        $scope.adaWarranty++
                    } else {
                        totalWnotWarranty += Math.round($scope.gridWork[i].Price / 1.1);
                    }
                }
            }
            // bagi 1.1 di pindah ke atas
            // $scope.totalWork = totalW / (1.1);  //request pak dodi dibagi 1.1 13/02/2019
            // $scope.totalWorknotWarranty = totalWnotWarranty / (1.1)
            $scope.totalWork = totalW;  //request pak dodi dibagi 1.1 13/02/2019
            $scope.totalWorknotWarranty = totalWnotWarranty 
            console.log(totalW);
            // ============
            var totalM = 0;
            var totalMnotWarranty = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].RetailPrice !== undefined) {
                            // if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30 || $scope.gridWork[i].child[j].PaidById == 31 || $scope.gridWork[i].child[j].PaidById == 32){
                            if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30){
                                totalM += 0;
                            }else{
                                totalM += Math.round($scope.gridWork[i].child[j].RetailPrice / 1.1);
                            }

                            if ($scope.gridWork[i].child[j].PaidById == 30){

                            } else {
                                totalMnotWarranty += Math.round($scope.gridWork[i].child[j].RetailPrice / 1.1);
                            }
                            console.log("$scope.gridWork[i].child.Price", $scope.gridWork[i].child[j].RetailPrice);
                        }
                    }
                }
            }
            // bagi 1.1 di pindah ke atas
            // $scope.totalMaterial = totalM / (1.1);  //request pak dodi dibagi 1.1 13/02/2019
            // $scope.totalMaterialnotWarranty = totalMnotWarranty / (1.1);
            $scope.totalMaterial = totalM;  //request pak dodi dibagi 1.1 13/02/2019
            $scope.totalMaterialnotWarranty = totalMnotWarranty;

        };
        // $scope.tes = function(data){
        //   if(data !== undefined){
        //     console.log(data);
        //   }else{
        //     console.log("Error");
        //   }
        // }
        // $scope.hideGridParts = false;
        // $scope.onAfterCancelGrid = function(row, mode) {
        //   console.log('onaftercancel >>', row, mode);
        //   $scope.hideGridParts = false;
        // };
        // $scope.onBeforeSave = function() {
        //   console.log('onBeforeSave >>', row, mode);
        //   $scope.hideGridParts = false;
        // };
        $scope.gridFrontParts = {
            enableSorting: true,
            enableRowSelection: false,
            columnDefs: [{
                    name: "No. Material",
                    displayName: "No. Material",
                    field: 'PartsCode',
                    width: '18%'
                },
                {
                    name: "Nama Material",
                    displayName: "Nama Material",
                    field: 'PartsName',
                    width: '20%'
                },
                {
                    name: "Qty Order",
                    displayName: "Qty. Order",
                    field: 'qty',
                    width: '8%'
                },
                {
                    name: "ETA Part",
                    displayName: "ETA Part",
                    field: 'ETA',
                    cellFilter: dateFilter,
                    width: '18%'
                },
                {
                    name: "Nilai DP",
                    displayName: "Nilai DP",
                    field: "DownPayment",
                    width: '18%'
                },
                {
                    name: "Harga",
                    displayName: "Harga",
                    field: "RetailPrice",
                    width: '18%',
                    cellFilter: 'number : 0'
                },
                {
                    name: "Status DP",
                    displayName: "Status DP",
                    field: "StatusDP",
                    width: '18%'
                }
                // { name: "Keterangan" }
            ]
        };


        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
            paginationPageSizes: [10, 50, 100],
            // Neh... Lo kelupaan ini.. wkwkwk
            enableFiltering: true,
            // pageSize: 10,
            // widthForHorizontalScrolling: Math.floor(Math.random() * (120 - 30 + 1)) + 50,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 50,
            // pageSize: 10,
            columnDefs: [{
                    name: 'Tipe Follow Up',
                    field: 'TypeFollow',
                    width: '12%',

                },
                {
                    name: 'No. Appointment',
                    field: 'AppointmentNo',
                    width: '15%'
                },
                {
                    name: 'Tanggal',
                    field: 'AppointmentDate',
                    cellFilter: dateFilter,
                    width: '12%'
                },
                {
                    name: 'Jam',
                    field: 'AppointmentTime',
                    width: '12%'
                },
                {
                    name: 'No. Polisi',
                    field: 'LicensePlate',
                    width: '12%'
                },
                {
                    name: 'No. Rangka',
                    field: 'VIN',
                    width: '12%'
                },
                {
                    name: 'Nama Pelanggan',
                    field: 'Name',
                    width: '15%'
                },
                {
                    name: 'No. Telepon',
                    field: 'Phone',
                    width: '12%'
                },
                {
                    name: 'Status',
                    field: 'Xstatus',
                    width: '12%'
                },
                {
                    name: 'Kategori',
                    field: 'WoCategory',
                    width: '12%'
                },
                {
                    name: 'Action',
                    displayName: 'Follow Up',
                    width: 180,
                    pinnedRight: true,
                    cellTemplate: gridActionButtonTemplate
                }
            ],
            onRegisterApi: function(gridApi) {
                // set gridApi on $scope
                $scope.gridApiAppointment = gridApi;
                $scope.gridApiAppointment.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRows = $scope.gridApiAppointment.selection.getSelectedRows();
                    // console.log("selected=>",$scope.selectedRows);
                    if ($scope.onSelectRows) {
                        $scope.onSelectRows($scope.selectedRows);
                    }
                });
            }
        };
        // // =======Adding Custom Filter==============
        // $scope.gridCols=[];  //= angular.copy(scope.grid.columnDefs);
        // var x=-1;
        // for(var i=0;i<$scope.grid.columnDefs.length;i++){
        //   if($scope.grid.columnDefs[i].visible==undefined){
        //     x++;
        //     $scope.gridCols.push($scope.grid.columnDefs[i]);
        //     $scope.gridCols[x].idx = i;
        //   }
        //   console.log("$scope.gridCols",$scope.gridCols);
        //   console.log("$scope.grid.columnDefs[i]",$scope.grid.columnDefs[i]);
        // }
        // $scope.filterBtnLabel = $scope.gridCols[0].name;
        // $scope.filterBtnChange = function(col){
        //   console.log("col",col);
        //   $scope.filterBtnLabel = col.name;
        //   $scope.filterColIdx = col.idx+1;
        //   console.log("gridApiAppointment.grid.columns[filterColIdx].filters[0].term",$scope.gridApiAppointment.grid.columns[$scope.filterColIdx].filters[0].term)
        // }
        // $scope.filterGrid = function(item,param) {
        //     $scope.gridApiAppointment.grid.columns[param].filters[0].term = item
        // }
        // // ================================

        $scope.getTableHeight = function() {
            var rowHeight = 30; // your row height
            var headerHeight = 50; // your header height
            var filterHeight = 40; // your filter height
            var pageSize = $scope.grid.paginationPageSize;
            if ($scope.grid.paginationPageSize > $scope.grid.data.length) {
                pageSize = $scope.grid.data.length;
            }
            if (pageSize < 4) {
                pageSize = 3;
            }

            return {
                height: (pageSize * rowHeight + headerHeight) + 50 + "px"
            };
        };
        $scope.gridDetailGr = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: false,
            enableSelectAll: false,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'No Material',
                    field: 'NoMaterial',
                    width: 100
                },
                {
                    name: 'Nama Material',
                    field: 'NameMaterial',
                    width: 300
                },
                {
                    name: 'Qty Order',
                    field: 'Qty',
                    width: 100
                },
                {
                    name: 'ETA Part',
                    field: 'Eta',
                    width: 100
                },
                {
                    name: 'Keterangan',
                    field: 'Ket',
                    width: 300
                }
            ]
        };
        $scope.onAfterCancel = function() {
            $scope.jobAvailable = false;
        };
        //===============BS List==============
        $scope.gridWork = [];
        var tmpComplaint = [];
        var tmpRequest = [];
        $scope.listApi = {};
        $scope.listSelectedRows = [];
        $scope.lmModel = {};
        $scope.ldModel = {};
        $scope.typeMData = [{
                Id: 1,
                Name: "1"
            },
            {
                Id: 2,
                Name: "2"
            },
            {
                Id: 3,
                Name: "3"
            },
            {
                Id: 6,
                Name: "T"
            },
            // {
            //     Id: 7,
            //     Name: "C"
            // }
        ];
        $scope.addDetail = function(data, item, itemPrice, row) {
            console.log('clearDetail', $scope.listApi.clearDetail());
            $scope.listApi.clearDetail();
            row.FlatRate = data.FlatRate;
            row.catName = $scope.tmpCatg.Name;
            // row.ActualRate = data.FlatRate; // kok actualrate di timpa flatrate
            // row.ActualRate = data.ActualRate;
            if(data.standardactualrate !== 0 ){
                row.ActualRate = data.standardactualrate
            }else{
                row.ActualRate = data.FlatRate;
            }
            row.tmpTaskId = null;
            // row.cusType=$scope.tmpCus.Name
            if ($scope.tmpPaidById !== null) {
                row.PaidById = $scope.tmpPaidById;
                row.PaidBy = $scope.tmpPaidName;
            }
            console.log("itemPrice", itemPrice);
            if (itemPrice.length > 0) {
                for (var i = 0; i < itemPrice.length; i++) {
                    console.log("scope.tmpCatg.MasterId", $scope.tmpCatg.MasterId, $scope.tmpServiceRate);
                    // if($scope.tmpCatg.MasterId !== undefined && $scope.tmpCatg.MasterId !== null){
                    //     $scope.checkServiceRate(data.FlatRate,$scope.tmpCatg.MasterId,1);
                    // }

                    // if (itemPrice[i].ServiceRate !== undefined) {
                    //     var harganya = itemPrice[i].ServiceRate;
                    //     var harganyaa = data.FlatRate;
                    //     var summary = harganya * harganyaa;
                    //     var summaryy = parseInt(summary);
                    //     row.Summary = Math.round(summaryy);
                    //     // row.Fare = itemPrice[i].AdjustmentServiceRate;
                    //     row.Fare = itemPrice[i].ServiceRate;
                    // $scope.PriceAvailable = true;
                    // } else {
                    //     row.Fare = itemPrice[i].ServiceRate;
                    //     row.Summary = row.Fare;
                    // }
                    var sum = 0;
                    var tmpItem;
                    if ($scope.tmpCatg.MasterId == 59 || $scope.tmpCatg.MasterId == 60) {
                        if ($scope.tmpServiceRate !== null) {
                            if (data.FlatRate !== null) {
                                tmpItem = data.FlatRate.toString();
                                if (tmpItem.includes(".")) {
                                    tmpItem = tmpItem.split('.');
                                    if (tmpItem[tmpItem.length - 1] !== ".") {
                                        // item = parseInt(tmpItem);
                                        sum = $scope.tmpServiceRate.WarrantyRate * data.FlatRate;
                                        sum = Math.round(sum);
                                    }
                                } else {
                                    sum = $scope.tmpServiceRate.WarrantyRate * data.FlatRate;
                                    sum = Math.round(sum);
                                }
                            } else {
                                sum = $scope.tmpServiceRate.WarrantyRate;
                                sum = Math.round(sum);
                            }
                            $scope.PriceAvailable = true;
                            sum = Math.round(sum);
                            row.Summary = sum;
                            row.Fare = $scope.tmpServiceRate.WarrantyRate;
                            console.log("row WarrantyRate lewat $scope.addDetail", row);
                        }
                    } else {
                        if ($scope.tmpServiceRate !== null) {
                            if (data.FlatRate !== null) {
                                tmpItem = data.FlatRate.toString();
                                if (tmpItem.includes(".")) {
                                    tmpItem = tmpItem.split('.');
                                    if (tmpItem[tmpItem.length - 1] !== ".") {
                                        // data.FlatRate = parseInt(tmpItem);
                                        sum = $scope.tmpServiceRate.ServiceRate * data.FlatRate;
                                        sum = Math.round(sum);
                                    }
                                } else {
                                    sum = $scope.tmpServiceRate.ServiceRate * data.FlatRate;
                                    sum = Math.round(sum);
                                }
                            } else {
                                sum = $scope.tmpServiceRate.ServiceRate;
                                sum = Math.round(sum);
                            }
                            $scope.PriceAvailable = true;
                            sum = Math.round(sum);
                            row.Summary = sum;
                            row.Fare = $scope.tmpServiceRate.ServiceRate;
                            console.log("row ServiceRate lewat $scope.addDetail", row);
                        }
                    }
                    // row.ActualRate = data.FlatRate;
                    if(data.standardactualrate !== 0 ){
                        row.ActualRate = data.standardactualrate
                    }else{
                        row.ActualRate = data.FlatRate;
                    }
                    console.log("row terakhir lewat $scope.addDetail", row);
                }
            } else {
                $scope.PriceAvailable = false;
                row.Fare = "";
                row.Summary = "";
            }
            row.TaskId = data.TaskId;
            row.IsOPB = data.IsOPB ? data.IsOPB : null;
            var tmp = {}
                // $scope.listApi.addDetail(tmp,row)
            if (item.length > 0) {
                for (var i = 0; i < item.length; i++) {
                    // var tmpItem = item[i];
                    // $scope.getAvailablePartsService(item[i]);
                    // if ($scope.tmpPaidById !== null) {
                    //     item[i].PaidById = $scope.tmpPaidById;
                    //     item[i].paidName = $scope.tmpPaidName;
                    // }
                    // item[i].subTotal = item[i].Qty * item[i].RetailPrice;
                    // // console.log("item[i].subTotal",item[i].subTotal);
                    // tmp = item[i]
                    // $scope.listApi.addDetail(tmp, row);
                    $scope.getAvailablePartsService(item[i], row);
                }
            } else {
                $scope.listApi.addDetail(false, row);
            }
        };


        // tambahan CR4

        $scope.onShowDetailTaskList = function(row){
            console.log('onShowDetailTaskList ===>',onShowDetailTaskList);
            $scope.lmModel.ActualRate = row.ActualRate;
            $scope.lmModel.FlatRate = row.FlatRate;
            $scope.lmModel.Fare = row.Fare;
            $scope.lmModel.Summary = row.Summary;
            $scope.lmModel.PaidBy = row.PaidBy;
            $scope.lmModel.PaidById = row.PaidById;
            console.log("$scope.lmModel Detail",$scope.lmModel);
        }

        $scope.onBeforeEditJob = function(row) {
            // $scope.tmpLastParts = [];
            console.log('onBeforeEditJob ===>', row);
            console.log("PriceAvailable",$scope.PriceAvailable);
            // $scope.
            $scope.PriceAvailable = true;
              // $scope.lmModel.Fare = row.Fare;
              // $scope.lmModel.Summary = row.Summary;

            $scope.DataTempSelectedJob = angular.copy(row);
            //tambahan CR4
            $scope.lmModel.ActualRate = row.ActualRate;
            $scope.lmModel.FlatRate = row.FlatRate;
            $scope.lmModel.Fare = row.Fare;
            $scope.lmModel.Summary = row.Summary;
            $scope.lmModel.PaidBy = row.PaidBy;
            $scope.lmModel.PaidById = row.PaidById;
            // tambahan CR4

            if (row.JobTypeId == 59 || row.JobTypeId == 60) {
                $scope.disT1 = false;
                $scope.JobIsWarranty = 1
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }]; // kl pekerjaan twc / pwc ga blh pilih bahan
            } else {
                $scope.disT1 = true;
                $scope.JobIsWarranty = 0
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
            }
            if (row.TaskId != null && row.TaskId != undefined){
                $scope.FlatRateAvailable = true;
            }

            console.log("$scope.lmModel Edit",$scope.lmModel);
            $scope.listApi.addDetail(false, $scope.lmModel);
        }
        // tambahan CR4
        $scope.dataListSelected = [];
        $scope.onListSelectRows = function(rows) {
            console.log("form controller=>", rows);
            $scope.dataListSelected = rows;
        };

        $scope.onSelectRows = function(rows) {
            console.log("form onSelectRows=>", rows);
            if (rows.length != 0) {
                FollowUpGrService.getJobParts(rows[0].JobId).then(function(resu) {
                    console.log("form onSelectRows getJobParts=>", resu.data.Result);
                    var data = resu.data.Result;
                    var tmpData = [];
                    for (var i in data) {
                        // ===== untuk yg tampil hanya yg tidak tersedia
                        data[i].tmpETA = $scope.changeFormatDate(data[i].ETA);
                        if (data[i].tmpETA == "1900/01/01" || data[i].ETA == "Mon Jan 01 1900 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                            data[i].ETA = "-";
                        } else {
                            tmpData.push(data[i]);
                        }
                    }
                    // ===== untuk yg tampil hanya yg tidak tersedia
                    // $scope.gridFrontParts.data = data;
                    $scope.gridFrontParts.data = tmpData;
                })
            }
        };
        $scope.gridPartsDetail = {

            columnDefs: [
                { name: "No. Material", field: "PartsCode", width: '15%' },
                { name: "Nama Material", field: "PartsName", width: '20%' },
                { name: "Qty", field: "Qty", width: '6%' },
                { name: "Satuan", field: "satuanName", width: '8%' },
                // { name: "Satuan", field: "SatuanId", cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.unitDataGrid[row.entity.SatuanId]}}</div>' },
                { name: "Ketersedian", field: "Availbility", width: '12%' },
                { name: "Tipe", field: "OrderType", width: '6%' ,
                        cellTemplate: '<div style="text-align:center;margin-top: 5px;" ng-show="row.entity.OrderType == 6">T</div><div style="text-align:center;margin-top: 5px;" ng-show="row.entity.OrderType != 6">{{row.entity.OrderType}}</div>' 
                },
                { name: "ETA", displayName: "ETA", field: "ETA", cellFilter: dateFilter, width: '11%' },
                { name: "Harga", field: "RetailPrice", width: '10%' ,cellFilter: 'currency:"":0'},
                
                //replace CR4
                { name: "Diskon", field: "DiskonForDisplay", width: '10%' }, 
                { name: "SubTotal", field: "subTotalForDisplay", width: '10%',cellFilter: 'currency:"":0' }, 
                //Replace CR4

                // { name: "Discount", field: "Discount", width: '10%' },// ini sebelum CR4
                // { name: "SubTotal", field: "subTotal", width: '10%',cellFilter: 'currency:"":0'},// ini sebelum CR4

                { name: "Nilai Dp", displayName: "Nilai DP", displayName: "Nilai DP", field: "DownPayment", width: '10%' ,cellFilter: 'currency:"":0'},
                { name: "Status DP", displayName: "Status DP", field: "StatusDp", cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>', width: '7%' },
                { name: "Pembayaran", field: "PaidBy", width: '11%' },
                { name: "OPB", displayName: "OPB", field: "IsOPB", cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="null" disabled="disabled"></div>', width: '7%' }
            ]
        };
        $scope.gridWork = $scope.gridWork;
        $scope.sendWork = function(key, data) {
            console.log('sendWork ===>',data);
            var taskandParts = [];
            var taskList = {};
            AppointmentGrService.getDataPartsByTaskId(data.TaskId).then(function(res) {
                console.log("resabis ambil parts", res.data.Result);
                taskandParts = res.data.Result;

                //tambahan CR4  start ==> request okrivia (qty * harga parts ) - diskon
                for(i in taskandParts ){
                    taskandParts[i].Discount = $scope.DiscountParts;
                    taskandParts[i].DiskonForDisplay = String($scope.DiscountParts) + '%';
                    taskandParts[i].NominalDiscount = (taskandParts[i].Qty*taskandParts[i].RetailPrice)*($scope.DiscountParts/100);
                    taskandParts[i].subTotal = taskandParts[i].Qty*taskandParts[i].RetailPrice;
                    taskandParts[i].subTotalForDisplay = taskandParts[i].subTotal - taskandParts[i].NominalDiscount;
                }
                // tambahan CR4 end

                AppointmentGrService.getDataTaskListByKatashiki($scope.mDataDetail.KatashikiCode).then(function(restask) {
                    taskList = restask.data.Result;
                    console.log("taskList", taskList);
                    $scope.addDetail(data, taskandParts, taskList, $scope.lmModel)
                });
            });
        };
        $scope.sendParts = function(key, data) {
                var tmpMaterialId = angular.copy($scope.ldModel.MaterialTypeId);
                console.log("a $scope.ldModel.Parts", $scope.ldModel);
                $scope.ldModel = data;
                var tmp = {};
                tmp = data;
                $scope.ldModel.PartsId = data.PartsId;
                $scope.ldModel.PartsName = data.PartsName;
                $scope.ldModel.MaterialTypeId = tmpMaterialId;
                $scope.ldModel.SatuanId = data.UomId;
                $scope.ldModel.satuanName = data.Name;
                $scope.ldModel.RetailPrice = null;


                if ($scope.JobIsWarranty == 1) {
                    console.log('pekerjaan nya warranty nih parts nya jg harus', $scope.ldModel.PaidById);
                    _.map($scope.paymentData, function(val) {
                        console.log("valll tipe", val);
                        if (val.Name === "Warranty") {
                            $scope.ldModel.PaidById = val.MasterId;
                            $scope.ldModel.PaidBy = val.Name;
                            $scope.ldModel.DownPayment = 0;
                            $scope.ldModel.DPRequest = 0;
                            $scope.ldModel.minimalDp = 0;
                            $scope.disableDP = true;
                        }
                    })

                }else if($scope.JobIsWarranty == 2){
                    _.map($scope.paymentDataPart, function(val) {
                      console.log("valll tipe", val);
                      if (val.Name === "Warranty") {
                          $scope.ldModel.PaidById = val.MasterId;
                          $scope.ldModel.PaidBy = val.Name;
                          $scope.ldModel.DownPayment = 0;
                          $scope.ldModel.DPRequest = 0;
                          $scope.ldModel.minimalDp = 0;
                          $scope.disableDP = true;
                      }
                   })
                  }else if($scope.JobIsWarranty == 3){
                    _.map($scope.paymentDataThereIstWarranty, function(val) {
                      if (val.Name === "Warranty") {
                          $scope.ldModel.PaidById = val.MasterId;
                          $scope.ldModel.PaidBy = val.Name;
                          $scope.ldModel.DownPayment = 0;
                          $scope.ldModel.DPRequest = 0;
                          $scope.ldModel.minimalDp = 0;
                          $scope.disableDP = true;
                      }
                   })
                  }else if($scope.JobIsWarranty == 4){
                      _.map($scope.paymentDataThereIstWarranty, function(val) {
                        if (val.Name === "Warranty") {
                            $scope.ldModel.PaidById = val.MasterId;
                            $scope.ldModel.PaidBy = val.Name;
                            $scope.ldModel.DownPayment = 0;
                            $scope.ldModel.DPRequest = 0;
                            $scope.ldModel.minimalDp = 0;
                            $scope.disableDP = true;
                        }
                     })
                  } else {
                    console.log('pekerjaan nya bukan warranty');
                }

                // $scope.listApi.addDetail(tmp);
                console.log("key Parts", key);
                console.log("data Parts", data);
                console.log("b $scope.ldModel.Parts", $scope.ldModel);
            }
            //----------------------------------
            // Type Ahead
            //----------------------------------
        $scope.getWork = function(key) {
            var Katashiki = $scope.mDataDetail.KatashikiCode;
            var catg = $scope.tmpCatg.MasterId;
            console.log("$scope.mDataDetail.KatashikiCode", Katashiki);
            console.log("$scope.lmModel.JobType", catg);
            if (Katashiki != null && catg != null) {
                var ress = AppointmentGrService.getDataTask(key, Katashiki, catg).then(function(resTask) {
                    return resTask.data.Result;
                });
                console.log("ress", ress);
                return ress
            }
        };
        $scope.getParts = function(key) {
            var kategori = $scope.ldModel.MaterialTypeId;
            var isGr = 1;
            if (kategori !== undefined) {
                var ress = AppointmentGrService.getDataParts(key, isGr, kategori).then(function(resparts) {
                    console.log("respart follow up GR", resparts);
                    var tempResult = [];
                    // tempResult.push(resparts.data.Result[0]);
                    for (var i=0; i<resparts.data.Result.length; i++){
                        if(resparts.data.Result[i].PartsClassId1 == kategori){
                            tempResult.push(resparts.data.Result[i]);
                        }
                    }
                    console.log('isGr COYYY 0', tempResult);
                    return tempResult;
                });
                console.log("ress", ress);
                return ress
            }
        };

        $scope.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };
        $scope.noResults = true;
        $scope.onSelectWork = function($item, $model, $label) {
            // console.log("onSelectWork=>", idx);
            console.log('ausp');
            console.log("onSelectWork=>", $item);
            console.log("onSelectWork=>", $model);
            console.log("onSelectWork=>", $label);
            // $scope.mData.Work = $label;
            // console.log('materialArray', materialArray);
            // materialArray = [];
            // materialArray.push($item);
            //2678 = PLC ... 2675 = FPC
            if ($scope.lmModel.JobTypeId != 2678 || $scope.lmModel.JobTypeId != 2675) {
                $scope.ldModel.PaidById = 30;
                $scope.lmModel.PaidById = "";
            }

            if ($scope.lmModel.JobTypeId == 2675) {
                $scope.ldModel.PaidById = 30;
            }

            if ($item.FlatRate != null) {
                console.log("FlatRate", $item.FlatRate);
                $scope.FlatRateAvailable = true;
            } else {
                $scope.FlatRateAvailable = false;
            }
            $scope.sendWork($label, $item);
            console.log("modelnya", $scope.lmModel);
            // console.log('materialArray',materialArray);
        }
        $scope.onSelectParts = function($item, $model, $label) {
            // console.log("onSelectWork=>", idx);
            console.log("onSelectParts=>", $item);
            console.log("onSelectParts=>", $model);
            console.log("onSelectParts=>", $label);
            console.log("anita parts", $scope.gridPartsDetail);
            // ==== ARI SAID ======
            // for (var i=0; i<$scope.gridPartsDetail.data.length; i++){
            //     if ($label == $scope.gridPartsDetail.data[i].PartsCode){
            //         console.log('parts sudah ada');  
            //         $scope.ldModel.PartsCode = null;
            //         $scope.ldModel.PartsName = null;
            //         $item.PartsName = null;
            //         $item.PartsCode = null;
            //         bsNotify.show({
            //             size: 'big',
            //             title: 'Parts Sudah Ada!',
            //             text: 'I will close in 2 seconds.',
            //             timeout: 2000,
            //             type: 'danger'
            //         });

            //     }
            // }
            // $scope.mData.Work = $label;
            // console.log('materialArray', materialArray);
            // materialArray = [];
            // materialArray.push($item);
            if ($item.PartsName !== null) {
                $scope.partsAvailable = true;
            } else {
                $scope.partsAvailable = false;
            }
            $scope.sendParts($label, $item);
            console.log("modelnya", $item);
            // console.log('materialArray',materialArray);
        };
        $scope.onGotResult = function() {
            console.log("onGotResult=>");
        }
        $scope.selected = {};
        $scope.JobIsWarranty = 0;
        $scope.DataTempSelectedJob = [];
        $scope.selectTypeWork = function(data) {
            $scope.lmModel.JobTypeId = data.MasterId;
            console.log('dieu')
            console.log("selectTypeWork", data);
            if (data.MasterId == 59 || data.MasterId == 60) {

                var cek_jml_job_War = 0
                if ($scope.gridWork.length > 0) {
                    for (var i=0; i<$scope.gridWork.length; i++) {
                        if ($scope.gridWork[i].PaidById == 30) {
                            cek_jml_job_War++
                        }
                    }
                }

                if (cek_jml_job_War > 0) {
                    bsAlert.warning('Claim Warranty hanya untuk 1 Job Openo, jika ada penambahan silahkan gunakan fitur Add Openo')
                    var xdat = {}
                    xdat.MasterId = null
                    xdat.Name = null
                    $scope.selectTypeWork(xdat,null)
                    return;
                }
                
                $scope.disT1 = false;
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }]; // kl pekerjaan twc / pwc ga blh pilih bahan
                console.log('cek ldmodel', $scope.ldModel)
                $scope.ldModel.MaterialTypeId = null;
            } else {
                $scope.disT1 = true;
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
            }
            if ($scope.lmModel.JobTypeId != 60 && $scope.lmModel.JobTypeId != 59 && $scope.lmModel.JobTypeId != 2675 && $scope.lmModel.JobTypeId != 2678) {
                $scope.paymentData = $scope.somePaymentData;
            }else{
                $scope.paymentData = $scope.paymentDataThereIstWarranty;
            }
            $scope.tmpCatg = data;
            var tmpModel = {};
            var tmpCatgName = data.Name
            tmpModel.catName = data.Name;
            if (tmpCatgName == "TWC" || tmpCatgName == "PWC") {
                $scope.JobIsWarranty = 1;
                _.map($scope.paymentData, function(val) {
                    console.log("valll tipe", val);
                    if (val.Name === "Warranty") {
                        $scope.tmpPaidById = val.MasterId;
                        $scope.tmpPaidName = val.Name;
                        tmpModel.JobTypeId = data.MasterId;
                        tmpModel.PaidById = val.MasterId;
                        tmpModel.TaskName = null;
                        tmpModel.FlatRate = null;
                        tmpModel.Fare = null;
                        tmpModel.ActualRate = null;
                        tmpModel.Summary = null;
                        $scope.listApi.addDetail(false, tmpModel);
                    }
                })
            }else if (tmpCatgName == "FLC - Free Labor Claim") {
                $scope.JobIsWarranty = 2;
                if ($scope.lmModel.PaidById == 30) {
                  $scope.ldModel.PaidById = null;
                }
                _.map($scope.paymentDataPart, function(val){
                  if (val.Name !== "Warranty") {
                    console.log('cek val flc', val);
                    $scope.tmpPaidById = val.MasterId;
                    $scope.tmpPaidName = val.Name;
                    tmpModel.JobTypeId = data.MasterId;
                    tmpModel.PaidById = val.MasterId;
                    tmpModel.TaskName = null;
                    tmpModel.FlatRate = null;
                    tmpModel.Fare = null;
                    tmpModel.ActualRate = null;
                    tmpModel.Summary = null;
                    $scope.listApi.addDetail(false, tmpModel);
                  }
                })
                _.map($scope.paymentDataThereIstWarranty, function(val){
                  if (val.Name === "Warranty") {
                    console.log('cek val flc', val);
                    $scope.tmpPaidById = val.MasterId;
                    $scope.tmpPaidName = val.Name;
                    tmpModel.JobTypeId = data.MasterId;
                    tmpModel.PaidById = val.MasterId;
                    tmpModel.TaskName = null;
                    tmpModel.FlatRate = null;
                    tmpModel.Fare = null;
                    tmpModel.ActualRate = null;
                    tmpModel.Summary = null;
                    $scope.listApi.addDetail(false, tmpModel);
                  }
                })
                $scope.getDiscountFromMasterDiscountBooking();
              }else if(tmpCatgName == "FPC - Free Parts Claim"){
                $scope.JobIsWarranty = 3;
                if ($scope.ldModel.PaidById == 30) {
                  $scope.lmModel.PaidById = null;
                }
                $scope.lmModel.PaidById = '';
                // _.map($scope.paymentDataThereIstWarranty, function(val){
                //   if (val.Name === "Warranty") {
                //     console.log('cek val flc', val);
                //     $scope.tmpPaidById = val.MasterId;
                //     $scope.tmpPaidName = val.Name;
                //     $scope.lmModel.PaidById = '';
                //     tmpModel.JobTypeId = data.MasterId;
                //     tmpModel.PaidById = val.MasterId;
                //     tmpModel.TaskName = null;
                //     tmpModel.FlatRate = null;
                //     tmpModel.Fare = null;
                //     tmpModel.ActualRate = null;
                //     tmpModel.Summary = null;
                //     $scope.listApi.addDetail(false, tmpModel);
                //   }
                // })
                // _.map($scope.paymentDataIsNotWarranty, function(val){
                //   if (val.Name !== "Warranty") {
                //     console.log('cek val flc', val);
                //     $scope.tmpPaidById = val.MasterId;
                //     $scope.tmpPaidName = val.Name;
                //     tmpModel.JobTypeId = data.MasterId;
                //     tmpModel.PaidById = null;
                //     tmpModel.TaskName = null;
                //     tmpModel.FlatRate = null;
                //     tmpModel.Fare = null;
                //     tmpModel.ActualRate = null;
                //     tmpModel.Summary = null;
                //     $scope.listApi.addDetail(false, tmpModel);
                //   }
                // })

                tmpModel.JobTypeId = data.MasterId;
                $scope.tmpPaidById = null;
                $scope.lmModel.PaidById = null;
                tmpModel.PaidById = null;
                tmpModel.TaskName = null;
                tmpModel.FlatRate = null;
                tmpModel.Fare = null;
                tmpModel.ActualRate = null;
                tmpModel.Summary = null;
                $scope.listApi.addDetail(false, tmpModel);

            }else if(tmpCatgName == "PLC - Parts Labor Claim"){
                $scope.JobIsWarranty = 4;
                _.map($scope.paymentDataThereIstWarranty, function(val){
                    if (val.Name === "Warranty") {
                      console.log('cek val flc', val);
                      $scope.tmpPaidById = val.MasterId;
                      $scope.tmpPaidName = val.Name;
                      tmpModel.JobTypeId = data.MasterId;
                      tmpModel.PaidById = val.MasterId;
                      tmpModel.TaskName = null;
                      tmpModel.FlatRate = null;
                      tmpModel.Fare = null;
                      tmpModel.ActualRate = null;
                      tmpModel.Summary = null;
                      $scope.listApi.addDetail(false, tmpModel);
                    }
                
                  })
            } else {
                $scope.JobIsWarranty = 0;
                // $scope.paymentData = $scope.somePaymentData;
                tmpModel.JobTypeId = data.MasterId;
                $scope.tmpPaidById = null;
                $scope.lmModel.PaidById = null;
                tmpModel.PaidById = null;
                tmpModel.TaskName = null;
                tmpModel.FlatRate = null;
                tmpModel.Fare = null;
                tmpModel.ActualRate = null;
                tmpModel.Summary = null;
                $scope.listApi.addDetail(false, tmpModel);
                // $scope.listApi.addDetail(false, $scope.lmModel);
            }
            console.log("$scope.gridWork", $scope.gridWork);
            console.log("gridWork", $scope.lmModel);
        };

        $scope.CheckedVIN = function() {
            console.log('sini nih')
            if ($scope.mDataDetail.VIN != null || $scope.mDataDetail.VIN != "") {
                var count = $scope.mDataDetail.VIN;
                var countVIN = count.length;
                console.log('mdata vin', count.length)
                    if (countVIN >= 17) {
                        FollowUpGrService.getDataVIN($scope.mDataDetail.VIN).then(function(res){
                            console.log('cek input rangka', $scope.mDataDetail.VIN)
                            $scope.tempVIN = res.data.Result[0];
                            console.log('cek res input', $scope.tempVIN)
                                for(var i = 0; i < $scope.tempVIN.length; i++){
                                console.log('cek data vin', $scope.tempVIN[0].MasterId)
                                    if ($scope.mDataDetail.VIN != null || $scope.mDataDetail.VIN != "") {
                                        if ($scope.tempVIN[i].MasterId === 2678) {
                                            $scope.taskCategory = $scope.notFlcFpc;
                                        }else if($scope.tempVIN[i].MasterId === 2677){
                                            $scope.taskCategory = $scope.notFpcPlc
                                        }else if($scope.tempVIN[i].MasterId === 2675){
                                            $scope.taskCategory = $scope.notFlcPlc
                                        }else{
                                            $scope.taskCategory = $scope.notFlcPlcFpc
                                        }
                                    }else{
                                        $scope.taskCategory = $scope.notFlcPlcFpc
                                    }
                                }
                            }
                        )
                        FollowUpGrService.listHistoryClaim($scope.mDataDetail.VIN).then(function(res){
                            $scope.listClaim = res.data.data;
                            $scope.tempListClaim = [];
                            $scope.historyDate = new Date($scope.tempListClaim.claimDate)
                            console.log('cek list claim', res)
                            console.log('list claim', res.data.data)
                            console.log('list history claim', $scope.tempListClaim)
                            for (var i = 0; i < $scope.listClaim.historyClaimDealerList.length; i++) {
                                $scope.tempListClaim.push($scope.listClaim.historyClaimDealerList[i]);
                            }
                        })

                        FollowUpGrService.getTowass($scope.mDataDetail.VIN).then(function(res) {
                            console.log("data Towasssssss", res.data);
                            $scope.dataTowass = res.data.Result;
                        });

                    }else{
                        console.log('tanpa service')
                        $scope.taskCategory = $scope.notFlcPlcFpc
                    }
                }
        }

        $scope.selectTypeCust = function(data) {
            console.log("selectTypeCust", data);
            $scope.getDiscountFromMasterDiscountBooking();
            $scope.tmpCus = data;
            $scope.lmModel.Discount = $scope.DiscountTask;
            $scope.lmModel.PaidBy = data.Name;
            $scope.lmModel.PaidById = data.MasterId;
        }

        $scope.selectTypeParts = function(data) {
            $scope.ldModel.SatuanId = 4;
            $scope.DisTipeOrder = false;

            $scope.ldModel.PartsCode = null;
            $scope.ldModel.PartsId = null;
            if(data.MaterialTypeId == 1){
                $scope.ldModel.IsOPB = 0;
            }
            if ($scope.JobIsWarranty == 1) {
                console.log('pekerjaan nya warranty nih parts nya jg harus', $scope.ldModel.PaidById);
                _.map($scope.paymentData, function(val) {
                    console.log("valll tipe", val);
                    if (val.Name === "Warranty") {
                        $scope.ldModel.PaidById = val.MasterId;
                        $scope.ldModel.PaidBy = val.Name;
                        $scope.ldModel.DownPayment = 0;
                        $scope.ldModel.DPRequest = 0;
                        $scope.ldModel.minimalDp = 0;
                        $scope.disableDP = true;
                    }
                })

            }else if($scope.JobIsWarranty == 2){
                _.map($scope.paymentDataPart, function(val) {
                  console.log("valll tipe", val);
                  if (val.Name === "Warranty") {
                        $scope.ldModel.PaidById = val.MasterId;
                        $scope.ldModel.PaidBy = val.Name;
                        $scope.ldModel.DownPayment = 0;
                        $scope.ldModel.DPRequest = 0;
                        $scope.ldModel.minimalDp = 0;
                      $scope.disableDP = true;
                  }
               })
              }else if($scope.JobIsWarranty == 3){
                _.map($scope.paymentDataThereIstWarranty, function(val) {
                  if (val.Name === "Warranty") {
                      $scope.ldModel.PaidById = val.MasterId;
                      $scope.ldModel.DownPayment = 0;
                      $scope.ldModel.DPRequest = 0;
                      $scope.ldModel.minimalDp = 0;
                      $scope.disableDP = true;
                  }
               })
              }else if($scope.JobIsWarranty == 4){
                  _.map($scope.paymentDataThereIstWarranty, function(val) {
                    if (val.Name === "Warranty") {
                        $scope.ldModel.PaidById = val.MasterId;
                        $scope.ldModel.DownPayment = 0;
                        $scope.ldModel.DPRequest = 0;
                        $scope.ldModel.minimalDp = 0;
                        $scope.disableDP = true;
                    }
                 })
              } else {
                console.log('pekerjaan nya bukan warranty');
            }
        }

        $scope.selectTypeUnitParts = function(data) {
            console.log("selectTypeUnitParts", data);
            $scope.tmpUnit = data;
            $scope.ldModel.satuanName = data.Name;
            console.log("selectTypeUnitParts", $scope.ldModel);
        }
        $scope.selectTypePaidParts = function(data) {
            if($scope.gridPartsDetail.data.length > 0){
                console.log("selectTypePaidParts IF ===>", data);

                $scope.ldModel.PaidById = data.MasterId;
                $scope.getDiscountFromMasterDiscountBooking();
                
                for (var i = 0; i < $scope.gridPartsDetail.data.length; i++) {
                    if ( ($scope.ldModel.PartsCode !== null && $scope.ldModel.PartsCode !== undefined) && $scope.ldModel.PartsCode == $scope.gridPartsDetail.data[i].PartsCode && $scope.ldModel.PaidById == $scope.gridPartsDetail.data[i].PaidById ) {
                        console.log('parts sudah ada');
                        // bsNotify.show({
                        //     size: 'big',
                        //     title: 'Parts Sudah Ada!',
                        //     text: 'I will close in 2 seconds.',
                        //     timeout: 2000,
                        //     type: 'danger'
                        // });
                        bsAlert.warning('Parts Sudah Ada!','Silahkan ganti tipe pembayaran');
                        $scope.ldModel.PaidById = null;
                    }else{
                        console.log("selectTypePaidParts", data);
                        $scope.tmpPaidName = data;
                        $scope.ldModel.PaidBy = data.Name;
                        // if (data.Name == "Internal" || data.Name == "Warranty" || data.Name == " Free Service") { kata nya selain customer pembayaran nya di 0 in, jd di singkat != customer
                        if (data.Name != "Customer") {
                            $scope.ldModel.DownPayment = 0;
                            $scope.ldModel.DPRequest = 0;
                            $scope.ldModel.minimalDp = 0;
                            $scope.disableDP = true;
                            $scope.checkPrice($scope.ldModel.RetailPrice);

                            if ($scope.ldModel.PaidById == 30){
                                //cek kl warranty ga blh ada diskon nya
                                //copy aja rumus yg dr CR di atas
                                $scope.ldModel.Discount = 0;
                                $scope.ldModel.DiskonForDisplay = String(0) + '%';
                                $scope.ldModel.NominalDiscount = ($scope.ldModel.Qty* $scope.ldModel.RetailPrice)*(0/100);
                                $scope.ldModel.subTotalForDisplay = $scope.ldModel.subTotal - $scope.ldModel.NominalDiscount;
                            }
                        } else {
                            $scope.checkPrice($scope.ldModel.RetailPrice);
                            $scope.disableDP = false;
                        };
                    }
                }
            }else{
                console.log("selectTypePaidParts ELSE ===>", data);
                $scope.tmpPaidName = data;
                $scope.ldModel.PaidBy = data.Name;
                // if (data.Name == "Internal" || data.Name == "Warranty" || data.Name == " Free Service") { kata nya selain customer pembayaran nya di 0 in, jd di singkat != customer
                if (data.Name != "Customer") {
                    $scope.ldModel.DownPayment = 0;
                    $scope.ldModel.DPRequest = 0;
                    $scope.ldModel.minimalDp = 0;
                    $scope.disableDP = true;
                    $scope.checkPrice($scope.ldModel.RetailPrice);

                    if ($scope.ldModel.PaidById == 30){
                        //cek kl warranty ga blh ada diskon nya
                        //copy aja rumus yg dr CR di atas
                        $scope.ldModel.Discount = 0;
                        $scope.ldModel.DiskonForDisplay = String(0) + '%';
                        $scope.ldModel.NominalDiscount = ($scope.ldModel.Qty* $scope.ldModel.RetailPrice)*(0/100);
                        $scope.ldModel.subTotalForDisplay = $scope.ldModel.subTotal - $scope.ldModel.NominalDiscount;
                    }
                } else {
                    $scope.checkPrice($scope.ldModel.RetailPrice);
                    $scope.disableDP = false;
                };
            }
        }
        $scope.onAfterCancel = function() {
            $scope.jobAvailable = false;
            $scope.FlatRateAvailable = false;
            $scope.PriceAvailable = false;
        }
        $scope.onListSave = function(item) {
            console.log("save_modal=>", item);
        }
        $scope.onListCancel = function(item) {
            console.log("cancel_modal=>", item);
        }
        $scope.onBeforeNew = function() {
            $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
            $scope.cekJob = null;
            $scope.isRelease = false;
            $scope.FlatRateAvailable = false;
            $scope.PriceAvailable = false;
            $scope.partsAvailable = false;
            var tmpTask = {}
            tmpTask.ProcessId = 2;
            tmpTask.isOpe = 0;
            $scope.DataTempSelectedJob = [];
            $scope.listApi.addDetail(false, tmpTask);
            FollowUpGrService.getDataVIN($scope.mDataDetail.VIN).then(function(res){
                console.log('cek input rangka 1', $scope.mDataDetail.VIN)
                var dataVIN = res.data.Result;
                console.log('cek res input 1', $scope.tempVIN);
                if (dataVIN.length > 0) {
                    if( dataVIN == "" ||  dataVIN == null ||  dataVIN == undefined){
                        var filtered = $scope.taskCategory.filter(function(item) { 
                            return item.Name !== "FLC - Free Labor Claim" && item.Name !== "FPC - Free Parts Claim" && item.Name !== "PLC - Parts Labor Claim" ;  
                         });
                         console.log("filtered",filtered);
                         $scope.taskCategory = filtered;
                         console.log("$scope.taskCategory final",$scope.taskCategory);
                    }else{
                        for (var i = 0; i < dataVIN.length; i++) {
                            if (dataVIN[i].ClaimType == "FPC - Free Parts Claim") {
                                $scope.taskCategory = $scope.notFlcPlc
                            }else if(dataVIN[i].ClaimType == "FLC - Free Labor Claim"){
                                $scope.taskCategory = $scope.notFpcPlc
                            }else if(dataVIN[i].ClaimType == "PLC - Parts Labor Claim"){
                                $scope.taskCategory = $scope.notFlcFpc
                            }else{
                                $scope.taskCategory = filtered;
                            }
                        }
                    }
                }else{
                    $scope.taskCategory = $scope.notFlcPlcFpc
                }
            })
        };
        $scope.onNoResult = function() {
            console.log("gak", $scope.lmModel);
            // $scope.listApi.clearDetail();
             var jmlPart = 0;
             if ($scope.cekJob != null && $scope.cekJob != undefined){
                 AppointmentGrService.getDataPartsByTaskId($scope.cekJob).then(function(res) {
                     console.log("resabis ambil parts", res.data.Result);
                     jmlPart = res.data.Result.length;
                 });
             }
             if (jmlPart > 0){
                 $scope.listApi.clearDetail();
             }
            var row = $scope.lmModel;
            row.FlatRate = "";
            row.Fare = "";
            row.Summary = "";
            row.TaskId = null;
            row.tmpTaskId = null;
            row.ActualRate = null;
            row.catName = $scope.tmpCatg.Name;
            // row.cusType =$scope.tmpCus.Name;
            $scope.listApi.addDetail(false, row);
            console.log("uunnnnchhh", $scope.lmModel)
            $scope.jobAvailable = false;
            $scope.PriceAvailable = false;
            $scope.FlatRateAvailable = false;
        };
        $scope.onNoPartResult = function() {
            console.log("onGotResult=>");
            $scope.partsAvailable = false;
            $scope.ldModel.PartsName = null;
            $scope.ldModel.RetailPrice = null;
            $scope.ldModel.PercentDP = null;
            $scope.ldModel.ETA = null;
            $scope.ldModel.SatuanId = 4;
            $scope.ldModel.Availbility = null;
            $scope.ldModel.satuanName = null;
            // $scope.ldModel.paidName = null;
        }
        $scope.disableDP = false;
        // $scope.selectTypePaidParts = function(data) {
        //     console.log("selectTypePaidParts", data);
        //     $scope.tmpPaidName = data;
        //     $scope.ldModel.PaidBy = data.Name;
        //     console.log("selectTypePaidParts", $scope.ldModel);
        //     // if (data.Name == "Internal" || data.Name == "Warranty" || data.Name == " Free Service") {
        //     if (data.Name != "Customer") {
        //         $scope.ldModel.DownPayment = 0;
        //         $scope.disableDP = true;
        //     } else {
        //         $scope.checkPrice($scope.ldModel.RetailPrice);
        //         $scope.disableDP = false;
        //     };
        // };

        $scope.onSARtype = function(ActualRateTyping){
            console.log('ActualRateTyping ===>',ActualRateTyping);
            $scope.lmModel.ActualRate = ActualRateTyping;
            // $scope.lmModel.FlatRate = ActualRateTyping;
        }

        $scope.onBeforeSave = function(data, mode) {
            //tambahan CR4 - pas save data Task kemudian edit => field SAR & Pembayaran Kosong
            // data.ActualRate = $scope.lmModel.ActualRate; // jangan di timpa kek gini, actualrate jd ketimpa flatrate
            data.PaidBy = $scope.lmModel.PaidBy;
            data.PaidById = $scope.lmModel.PaidById;
            // tambahan CR4

            console.log("onBeforeSave data", data);
            console.log("onBeforeSave mode", mode);

            // if(data.PaidById == 28){
            if(data.PaidById != 2277 && data.PaidById != 30){
                data.Discount = $scope.DiscountTask;
            } else {
                data.Discount = 0;
            }

            var tmpData = angular.copy(data);
            var tmpGridWork = $scope.gridWork;
            var lengthGrid = tmpGridWork.length - 1;
            // if ($scope.tmpCus !== undefined) {
            //     tmpData.PaidBy = $scope.tmpCus.Name;
            // }
            _.map($scope.paymentData, function(val) {
                if (val.MasterId == tmpData.PaidById) {
                    tmpData.PaidBy = val.Name;
                }
            });
            // tmpData.PaidBy = $scope.tmpCus.Name;
            if (mode == 'new') {
                if (tmpData.tmpTaskId == null) {
                    if (tmpGridWork.length > 0) {
                        tmpData.tmpTaskId = tmpGridWork[lengthGrid].tmpTaskId + 1;
                    } else {
                        tmpData.tmpTaskId = 1;
                    }
                }
                if (data.child.length > 0) {
                    var tmpDataChild = angular.copy(data.child);
                    for (var i = 0; i < tmpDataChild.length; i++) {
                        delete data.child[i];
                    }
                    $scope.listApi.addDetail(tmpDataChild, tmpData);
                } else {
                    $scope.listApi.addDetail(false, tmpData);
                };


            }
        }

        var jejeranDiskonTask = [];
        var jejeranDiskonParts = [];
        $scope.newVarFLC = 0;
        $scope.displayDiscountAlgorithm = function(){
            console.log('jejeranDiskonTask ==>',jejeranDiskonTask);
            console.log('jejeranDiskonParts ==>',jejeranDiskonParts);
            if(jejeranDiskonTask.length > 1){
                var countTaskDiscount = 0;
                getSingleDiskonTask = [];
                for(var i in jejeranDiskonTask){
                    if(jejeranDiskonTask[i] == 0){
                        countTaskDiscount++;
                    }else{
                        getSingleDiskonTask.push(jejeranDiskonTask[i]);
                    }
                }
                if(countTaskDiscount == jejeranDiskonTask.length - 1 && getSingleDiskonTask.length == 1){
                    $scope.DisplayDiscountTask = getSingleDiskonTask[0]; // [0,0,0,5] get 5 for Discount
                }else{
                    if(countTaskDiscount == jejeranDiskonTask.length){
                        $scope.DisplayDiscountTask = jejeranDiskonTask[0]; //[0,0,0] get 0 for Discount
                    }else{
                        var countSameDiscount = 0;
                        for(var x in getSingleDiskonTask){
                            if(getSingleDiskonTask[0] == getSingleDiskonTask[x]){
                                countSameDiscount++;
                            }
                        }
                        if(countSameDiscount == getSingleDiskonTask.length){
                            $scope.DisplayDiscountTask = getSingleDiskonTask[0]; //[0,0,0,3,3] get 3 for Discount
                        }else{
                            $scope.DisplayDiscountTask = ' '; // [0,6,4,3,5] set blank For Discount
                        }
                    }
                }
            }else{
                $scope.DisplayDiscountTask = jejeranDiskonTask[0]; //[x] get x for Discount
            }


            if(jejeranDiskonParts.length > 1){
                var countPartsDiscount = 0;
                getSingleDiskonParts = [];
                for(var i in jejeranDiskonParts){
                    if(jejeranDiskonParts[i] == 0){
                        countPartsDiscount++;
                    }else{
                        getSingleDiskonParts.push(jejeranDiskonParts[i]);
                    }
                }
                if(countPartsDiscount == jejeranDiskonParts.length - 1 && getSingleDiskonParts.length == 1){
                    $scope.DisplayDiscountParts = getSingleDiskonParts[0]; // [0,0,0,5] get 5 for Discount
                }else{
                    if(countPartsDiscount == jejeranDiskonParts.length){
                        $scope.DisplayDiscountParts = jejeranDiskonParts[0]; //[0,0,0] get 0 for Discount
                    }else{
                        var countSameDiscount = 0;
                        for(var x in getSingleDiskonParts){
                            if(getSingleDiskonParts[0] == getSingleDiskonParts[x]){
                                countSameDiscount++;
                            }
                        }
                        if(countSameDiscount == getSingleDiskonParts.length){
                            $scope.DisplayDiscountParts = getSingleDiskonParts[0]; //[0,0,0,3,3] get 3 for discount
                        }else{
                            $scope.DisplayDiscountParts = ' '; // [0,6,4,3,5] set blank For Discount
                        }
                    }
                }
            }else{
                $scope.DisplayDiscountParts = jejeranDiskonParts[0];// get x for Discount
                $scope.DiscountParts = jejeranDiskonParts[0]
            }
                console.log('aneh ieu')
                var flagFLC = 0
                for(var i in $scope.gridWork){
                    if ($scope.gridWork[i].catName == "FLC - Free Labor Claim") {
                        console.log('$scope.gridWork[i].catName', $scope.gridWork[i].catName)
                        flagFLC++;
                    }
                }

                console.log('flagFLC', flagFLC)
                console.log('$scope.gridWork.length flc', flagFLC)
                if (flagFLC == $scope.gridWork.length) {
                    $scope.newVarFLC = 1;
                }

                var flag = 0;
                for(var i in $scope.gridWork){
                    console.log('$scope.gridWork[i] 123', $scope.gridWork[i])
                    if ($scope.gridWork[i].catName == "FPC - Free Parts Claim") {
                        flag++
                    }
                }
                console.log('flag disc 123', flag)
                console.log('$scope.gridWork.length 123', $scope.gridWork.length)
                if(flag == $scope.gridWork.length){
                    $scope.DiscountParts = 0
                }
                console.log('$scope.DiscountParts', $scope.DiscountParts)
                console.log('$scope.adaWarranty', $scope.adaWarranty)
                console.log('$scope.jmlJobWarranty', $scope.jmlJobWarranty)

        }
        console.log('$scope.newVarFLC =>', $scope.newVarFLC)
        
        $scope.onAfterSave = function(data, mode) {
            console.log("onAfterSave data", data);
            console.log("onAfterSave mode", mode);
            console.log("ini sih wkwwk", $scope.taskCategory);
            console.log('DataTempSelectedJob', $scope.DataTempSelectedJob);

            if (data.JobTypeId == 59 || data.JobTypeId == 60 || data.JobTypeId == 2686 || data.JobTypeId == 2675 || data.JobTypeId == 2677 || data.JobTypeId == 2678) {
                if (data.TaskId == null && data.TaskId == undefined) {
                    var tempJobList = data.JobTypeId;
                    var index = $scope.gridWork.indexOf(tempJobList);
                    $scope.gridWork.splice(index, 1);
                    bsAlert.warning("Input pekerjaan Warranty & Free Claim harus menggunakan Openo / Tasklist, silakan ubah pekerjaan terlebih dahulu") 
                }
            }

            if (data.TaskName != null && data.TaskName != undefined) {
                if (data.TaskName.includes('|')) {
                    var split_TaskName = data.TaskName.split('|')
                    var hasilSplitTaskName = split_TaskName[0].trim()
                }
            }

            console.log('cek hasil split taskName', hasilSplitTaskName);
            if (hasilSplitTaskName != '1000km') {
                if (data.catName == "PLC - Parts Labor Claim" || data.catName == "FPC - Free Parts Claim") {
                    for (var i = 0; i < $scope.gridWork.length; i++) {
                        $scope.tempPart = $scope.gridWork[i];
                    }
                    console.log('data part child', $scope.tempPart.child)
                    console.log('data part', $scope.tempPart)
                    if($scope.tempPart.child == undefined || $scope.tempPart.child == null || $scope.tempPart.child == "") {   
                        bsAlert.warning('Harus ada Part, dengan Pembayaran Warranty, Karena tipe Pekerjaan adalah ' + $scope.tempPart.catName)
                        $scope.gridWork = [];   
                    }else{
                        console.log('gaada parts gausah kesini')
                    }
                }
            }

            var NumberParent = 1;
            for (i in $scope.gridWork){
                $scope.gridWork[i].NumberParent = NumberParent+parseInt(i);
                for (j in $scope.gridWork[i].child){
                    $scope.gridWork[i].child[j].NumberChild = $scope.gridWork[i].NumberParent + "."  + parseInt(++j);
                }
            }
            // ======================
            var tmpErasedData = []
            var tmpDeleteData = []; //untuk kebutuhan tyre
            var tmpData = angular.copy(data);
            var countDeletedOrder = 0;
            if ($scope.DataTempSelectedJob != undefined) {
                if ($scope.DataTempSelectedJob.child != undefined && $scope.DataTempSelectedJob.child.length > 0) {
                    for (var i = 0; i < $scope.DataTempSelectedJob.child.length; i++) {
                        var idx = 0;
                        idx = _.findIndex(tmpData.child, { JobPartsId: $scope.DataTempSelectedJob.child[i].JobPartsId });
                        if(idx == -1 && $scope.DataTempSelectedJob.child[i].MaterialRequestStatusId == 4) {
                            tmpErasedData.push($scope.DataTempSelectedJob.child[i]);
                            countDeletedOrder++;
                        }else if(idx == -1){
                            tmpDeleteData.push($scope.DataTempSelectedJob.child[i]); //untuk kebutuhan tyre
                        }
                    }
                }
            }
            
            if(countDeletedOrder > 0){
                $scope.checkStatusPart($scope.mData.JobId, tmpErasedData, 4, tmpData.JobTaskId);
            }
            // ======================
            
            //Cek status part Tyre
            if(tmpDeleteData.length > 0){
                $scope.checkStatusPartTyre($scope.mData.JobId, tmpDeleteData, tmpData.JobTaskId);
            }
            
            
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        
                        // if($scope.gridWork[i].child[j].MaterialTypeId == 2){
                        // //     $scope.gridWork[i].child[j].Discount 
                        // // }else{
                        //     $scope.gridWork[i].child[j].Discount = 0;
                        // }else{
                            console.log("materialnya nih",$scope.DiscountParts);
                            // if($scope.gridWork[i].child[j].PaidById == 28){
                            // if($scope.gridWork[i].child[j].PaidById != 2277 && $scope.gridWork[i].child[j].PaidById != 30){
                            //     $scope.gridWork[i].child[j].Discount = $scope.DiscountParts;
                            // } else {
                            //     $scope.gridWork[i].child[j].Discount = 0;
                            // }

                            if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32){
                                $scope.gridWork[i].child[j].Discount = 0;
                            } else {
                                $scope.gridWork[i].child[j].Discount = $scope.DiscountParts;
                            }
                        // }
                    }
                }
            };

            // =============

            // =================================================== Total Work =======================================================================
            var totalW = 0;
            var totalWnotWarranty = 0;
            $scope.adaWarranty = 0;
            $scope.jmlJobWarranty = $scope.gridWork.length;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if($scope.gridWork[i].PaidById == 2277){
                    totalW += 0;
                }else{
                    if ($scope.gridWork[i].Summary !== undefined) {
                        if ($scope.gridWork[i].Summary == "") {
                            console.log("$scope.gridWork[i].Summary == kosong");
                            $scope.gridWork[i].Summary = $scope.gridWork[i].Fare * $scope.gridWork[i].FlatRate;
                        }
                        // if ($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                        if ($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                            totalW += 0
                        } else {
                            // totalW += Math.round($scope.gridWork[i].Summary / 1.1);
                            totalW += ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWork[i].Summary,
                                            // Discount: 5,
                                            // Qty: 2,
                                            Tipe: 2, 
                                            PPNPercentage: PPNPerc,
                                        });
                        }

                        if ($scope.gridWork[i].PaidById == 30){
                            $scope.adaWarranty++
                        } else {
                            // totalWnotWarranty += Math.round($scope.gridWork[i].Summary / 1.1);
                            totalWnotWarranty += ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWork[i].Summary,
                                                    // Discount: 5,
                                                    // Qty: 2,
                                                    Tipe: 2, 
                                                    PPNPercentage: PPNPerc,
                                                });
                        }
                    }
                    
                }
            };
           
            $scope.totalWork = totalW  //request pak dodi dibagi 1.1 13/02/2019
            $scope.totalWorknotWarranty = totalWnotWarranty
            // =================================================== Total Work =======================================================================




            var totalDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].DownPayment !== undefined) {
                            totalDp += $scope.gridWork[i].child[j].DownPayment;
                            console.log("$scope.gridWork[i].child.Price", $scope.gridWork[i].child[j].DownPayment);
                        }
                    }
                }
            }
            $scope.totalDp = totalDp;
            // ===============
            var totalDefaultDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].minimalDp !== undefined) {
                            totalDefaultDp += $scope.gridWork[i].child[j].minimalDp;
                            console.log("$scope.gridWork[i].child.totalDefaultDp", $scope.gridWork[i].child[j].minimalDp);
                        }
                    }
                }
            }
            $scope.totalDpDefault = totalDefaultDp;
            // ==============
          
            // =============
            $scope.tmpActRateChange = angular.copy($scope.tmpActRate);
            var totalActualRate = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].ActualRate !== undefined) {
                    totalActualRate += $scope.gridWork[i].ActualRate;
                }
            }
            $scope.tmpActRate = totalActualRate;
            console.log("$scope.tmpActRate", $scope.tmpActRate);
            console.log("$scope.tmpActRateChange", $scope.tmpActRateChange);

            // =================== CR4 Re-Calculate following CSV, req by Kevin =======================
            var totalMaterial_byKevin = 0;
            var totalMaterial_byKevinNotWarranty = 0;
            console.log('yangg aneh')
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        // if($scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32){
                            totalMaterial_byKevin += 0;
                        }else{

                            if($scope.gridWork[i].child[j].PaidById == 30 ){ 
                                totalMaterial_byKevin += 0;
                            }else{ 
                                // totalMaterial_byKevin += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                                totalMaterial_byKevin += ASPricingEngine.calculate({
                                                            InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                            // Discount: 5,
                                                            Qty: $scope.gridWork[i].child[j].Qty,
                                                            Tipe: 102, 
                                                            PPNPercentage: PPNPerc,
                                                        }); 
                            }

                            if ($scope.gridWork[i].child[j].PaidById == 30){

                            } else {
                                // totalMaterial_byKevinNotWarranty += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                                totalMaterial_byKevinNotWarranty += ASPricingEngine.calculate({
                                                                        InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                                        // Discount: 5,
                                                                        Qty: $scope.gridWork[i].child[j].Qty,
                                                                        Tipe: 102, 
                                                                        PPNPercentage: PPNPerc,
                                                                    }); 
                            }


                        }
                    }
                }
            };
            
            var totalMD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        // if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30 ){
                        if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32 ){
                            totalMD_byKevin += 0;
                        }else{
                            // totalMD_byKevin +=  Math.round(Math.round($scope.gridWork[i].child[j].RetailPrice /1.1 ) * ($scope.gridWork[i].child[j].Discount/100)) *  $scope.gridWork[i].child[j].Qty;
                            totalMD_byKevin +=  ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                    Discount: $scope.gridWork[i].child[j].Discount,
                                                    Qty: $scope.gridWork[i].child[j].Qty,
                                                    Tipe: 112, 
                                                    PPNPercentage: PPNPerc,
                                                }); 
                            
                        }
                    }
                }
            };

            // =================================================== Total Work Discount =======================================================================
            var totalWD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                // if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                if($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                    totalWD_byKevin += 0;
                }else{
                    // totalWD_byKevin +=  Math.round(Math.round($scope.gridWork[i].Summary /1.1 ) * ($scope.gridWork[i].Discount/100));
                    totalWD_byKevin +=  ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWork[i].Summary,
                                            Discount: $scope.gridWork[i].Discount,
                                            // Qty: 2,
                                            Tipe: 12, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                }
            };
            console.log('totalWD_byKevin ===>',totalWD_byKevin);
            // =================================================== Total Work Discount =======================================================================


            // =======================================================
            $scope.summaryJoblist = Math.round(($scope.totalWork + ($scope.totalMaterial - ($scope.totalMaterial * $scope.DiscountParts / 100))) + ((($scope.totalWork + ($scope.totalMaterial - ($scope.totalMaterial * $scope.DiscountParts / 100))) * 10) / 100));
            if ($scope.summaryJoblist >= 1000000) {
                $scope.costMaterial = 6000;
            } else if ($scope.summaryJoblist > 250000) {
                $scope.costMaterial = 3000;
            } else {
                $scope.costMaterial = 0;
            }
            // =======================================================

            $scope.totalMaterial           = totalMaterial_byKevin;
            $scope.totalMaterialnotWarranty = totalMaterial_byKevinNotWarranty;
            $scope.totalMaterialDiscounted = totalMD_byKevin;
            $scope.totalWorkDiscounted     = totalWD_byKevin;
            $scope.subTotalMaterialSummary = $scope.totalMaterial - $scope.totalMaterialDiscounted;
            $scope.subTotalWorkSummary     = $scope.totalWork - $scope.totalWorkDiscounted;
            // $scope.totalPPN                = Math.floor(($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) *0.1);  
            $scope.totalPPN                = ASPricingEngine.calculate({
                                                InputPrice: (($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * (1+PPNPerc/100)),
                                                // Discount: 0,
                                                // Qty: 0,
                                                Tipe: 33, 
                                                PPNPercentage: PPNPerc,
                                            });    
            $scope.totalEstimasi           = $scope.totalPPN + $scope.subTotalMaterialSummary + $scope.subTotalWorkSummary + $scope.costMaterial;
            // =================== CR4 Re-Calculate following CSV, req by Kevin =======================

            //nentuin diskon task & parts
            jejeranDiskonTask = [];
            jejeranDiskonParts = [];
            for(var i in $scope.gridWork){
                // if($scope.gridWork[i].PaidById == 30|| $scope.gridWork[i].PaidById == 31 || $scope.gridWork[i].PaidById == 32 || $scope.gridWork[i].PaidById == 2277){
                // if($scope.gridWork[i].PaidById == 30 || $scope.gridWork[i].PaidById == 2277){
                if($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                    var zeroDiscountTask = {Discount : 0}
                    jejeranDiskonTask.push(zeroDiscountTask.Discount);
                }else{
                    jejeranDiskonTask.push($scope.gridWork[i].Discount);
                }
                if($scope.gridWork[i].child.length > 0 ){
                    for(var j in $scope.gridWork[i].child){
                        // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 31 || $scope.gridWork[i].child[j].PaidById == 32 || $scope.gridWork[i].child[j].PaidById == 2277){
                        // if($scope.gridWork[i].child[j].PaidById == 30 || $scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32){
                            var zeroDiscountParts = {Discount : 0}
                            jejeranDiskonParts.push(zeroDiscountParts.Discount);
                        }else{
                            jejeranDiskonParts.push($scope.gridWork[i].child[j].Discount);
                        }

                        
                    }
                }
            }
            //nentuin diskon task & parts
            $scope.displayDiscountAlgorithm();



            // var DiscountTaxB = [];
            // var DiscountPartsB = [];
            // $scope.DiscountTask = 0;
            // $scope.DiscountParts = 0;
            // for(var a = 0; a < dataWo.JobTask.length; a++){
            //     // $scope.DiscountTask = dataWo.JobTask[a].Discount;
            //     DiscountTaxB.push(dataWo.JobTask[a].Discount);
            //     var maxValue = _.max(DiscountTaxB);
            //     for(var b = 0; b < dataWo.JobTask[a].JobParts.length; b++){
            //         DiscountPartsB.push(dataWo.JobTask[a].JobParts[b].Discount);
            //         var maxValueParts = _.max(DiscountPartsB);
            //     }
            // }
            // $scope.DiscountTask = maxValue;
            // $scope.DiscountParts = maxValueParts;

            
            if ($scope.tmpActRate !== $scope.tmpActRateChange) {
                $scope.mData.StallId = 0;
                $scope.mData.FullDate = null;
                $scope.mData.PlanStart = null;
                $scope.mData.PlanFinish = null;
                $scope.mData.PlanDateStart = null;
                //$scope.mData.AppointmentDate = null; 
                delete $scope.mData.AppointmentDate;
                //$scope.mData.AppointmentTime = null;
                delete $scope.mData.AppointmentTime;
                $scope.mData.TargetDateAppointment = null;
                $scope.mData.FixedDeliveryTime1 = null;
                $scope.mData.PlanDateFinish = null;
                $scope.mData.StatusPreDiagnose = 0;
                delete $scope.mData.PrediagScheduledTime;
                delete $scope.mData.PrediagStallId;
            }
            console.log("gridWork", $scope.gridWork);

            
        };
        $scope.onAfterDelete = function(data) {
            var NumberParent = 1;
            for (i in $scope.gridWork){
                $scope.gridWork[i].NumberParent = NumberParent+parseInt(i);
                for (j in $scope.gridWork[i].child){
                    $scope.gridWork[i].child[j].NumberChild = $scope.gridWork[i].NumberParent + "."  + parseInt(++j);
                }
            }
            // =======
            var deleteJob = function (data) {
                $scope.sumAllPrice();
                var countDeleted = 0;
                var countDeletedOrder = 0;
                var tmpDeletedArray = [];
                var tempData = angular.copy(data);
                for (var i = 0; i < data.length; i++) {
                    console.log('data deletedd', $scope.dataListSelected)
                    for (var j = 0; j < $scope.dataListSelected.length; j++) {
                        // ==== if separate deleted uncommend this f code
                        for (var z in $scope.dataListSelected[j].child) {
                            if($scope.dataListSelected[j].child[z].MaterialRequestStatusId == 4){
                                countDeletedOrder++;
                            }
                        }
                        

                    }
                }
                if(countDeletedOrder > 0) {
                    for (var f in $scope.dataListSelected) {
                        $scope.gridWork.push($scope.dataListSelected[f]);
                    }
                    bsAlert.warning("Material ini sedang dalam proses order", "Batalkan PO dahulu atau tunggu PO complete");
                }
                console.log("tmpDeletedArray", tmpDeletedArray);
            }
            // =======

            // =================================================== Total Work =======================================================================
            var totalW = 0;
            var totalWnotWarranty = 0;
            $scope.adaWarranty = 0;
            $scope.jmlJobWarranty = $scope.gridWork.length;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].Summary !== undefined) {
                    if ($scope.gridWork[i].Summary == "") {
                        $scope.gridWork[i].Summary = $scope.gridWork[i].Fare * $scope.gridWork[i].FlatRate;
                    }
                    
                        // if ($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                        if ($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                            totalW += 0
                        } else {
                            // totalW += Math.round($scope.gridWork[i].Summary / 1.1);
                            totalW += ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWork[i].Summary,
                                            // Discount: 5,
                                            // Qty: 2,
                                            Tipe: 2, 
                                            PPNPercentage: PPNPerc,
                                        });
                        }

                        if ($scope.gridWork[i].PaidById == 30){
                            $scope.adaWarranty++
                        } else {
                            // totalWnotWarranty += Math.round($scope.gridWork[i].Summary / 1.1);
                            totalWnotWarranty += ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWork[i].Summary,
                                                    // Discount: 5,
                                                    // Qty: 2,
                                                    Tipe: 2, 
                                                    PPNPercentage: PPNPerc,
                                                });
                        }
                    
                }
                
                
            }
            
            $scope.totalWork = totalW;  //request pak dodi dibagi 1.1 13/02/2019
            $scope.totalWorknotWarranty = totalWnotWarranty
            // =================================================== Total Work =======================================================================


            
            var totalDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].DownPayment !== undefined) {
                            totalDp += $scope.gridWork[i].child[j].DownPayment;
                            console.log("$scope.gridWork[i].child.Price", $scope.gridWork[i].child[j].DownPayment);
                        }
                    }
                }
            }
        
            $scope.totalDp = totalDp;

            var totalDefaultDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].minimalDp !== undefined) {
                            totalDefaultDp += $scope.gridWork[i].child[j].minimalDp;
                            console.log("$scope.gridWork[i].child.totalDefaultDp", $scope.gridWork[i].child[j].minimalDp);
                        }
                    }
                }
            }
            $scope.totalDpDefault = totalDefaultDp;
            // ==========
           

            // =================== CR4 Re-Calculate following CSV, req by Kevin =======================
            var totalMaterial_byKevin = 0;
            var totalMaterial_byKevinNotWarranty = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        // if($scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32){
                            totalMaterial_byKevin += 0;
                        }else{

                            if($scope.gridWork[i].child[j].PaidById == 30 ){ 
                                totalMaterial_byKevin += 0;
                            }else{ 
                                // totalMaterial_byKevin += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                                totalMaterial_byKevin += ASPricingEngine.calculate({
                                                            InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                            // Discount: 5,
                                                            Qty: $scope.gridWork[i].child[j].Qty,
                                                            Tipe: 102, 
                                                            PPNPercentage: PPNPerc,
                                                        }); 
                            }

                            if ($scope.gridWork[i].child[j].PaidById == 30){

                            } else {
                                // totalMaterial_byKevinNotWarranty += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                                totalMaterial_byKevinNotWarranty += ASPricingEngine.calculate({
                                                                        InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                                        // Discount: 5,
                                                                        Qty: $scope.gridWork[i].child[j].Qty,
                                                                        Tipe: 102, 
                                                                        PPNPercentage: PPNPerc,
                                                                    }); 
                            }


                        }
                    }
                }
            };
            
            var totalMD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        // if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30 ){
                        if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32){
                            totalMD_byKevin += 0;
                        }else{
                            // totalMD_byKevin +=  Math.round(Math.round($scope.gridWork[i].child[j].RetailPrice /1.1 ) * ($scope.gridWork[i].child[j].Discount/100)) *  $scope.gridWork[i].child[j].Qty;
                            totalMD_byKevin +=  ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                    Discount: $scope.gridWork[i].child[j].Discount,
                                                    Qty: $scope.gridWork[i].child[j].Qty,
                                                    Tipe: 112, 
                                                    PPNPercentage: PPNPerc,
                                                }); 
                            
                        }
                    }
                }
            };

            // =================================================== Total Work Discount =======================================================================
            var totalWD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                // if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                if($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                    totalWD_byKevin += 0;
                }else{
                    // totalWD_byKevin +=  Math.round(Math.round($scope.gridWork[i].Summary /1.1 ) * ($scope.gridWork[i].Discount/100));
                    totalWD_byKevin +=  ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWork[i].Summary,
                                            Discount: $scope.gridWork[i].Discount,
                                            // Qty: 2,
                                            Tipe: 12, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                }
            };
            console.log('totalWD_byKevin ===>',totalWD_byKevin);
            // =================================================== Total Work Discount =======================================================================


            // =======================================================
            $scope.summaryJoblist = Math.round(($scope.totalWork + ($scope.totalMaterial - ($scope.totalMaterial * $scope.DiscountParts / 100))) + ((($scope.totalWork + ($scope.totalMaterial - ($scope.totalMaterial * $scope.DiscountParts / 100))) * 10) / 100));
            if ($scope.summaryJoblist >= 1000000) {
                $scope.costMaterial = 6000;
            } else if ($scope.summaryJoblist > 250000) {
                $scope.costMaterial = 3000;
            } else {
                $scope.costMaterial = 0;
            }
            // =======================================================

            $scope.totalMaterial           = totalMaterial_byKevin;
            $scope.totalMaterialnotWarranty = totalMaterial_byKevinNotWarranty;
            $scope.totalMaterialDiscounted = totalMD_byKevin;
            $scope.totalWorkDiscounted     = totalWD_byKevin;
            $scope.subTotalMaterialSummary = $scope.totalMaterial - $scope.totalMaterialDiscounted;
            $scope.subTotalWorkSummary     = $scope.totalWork - $scope.totalWorkDiscounted;
            // $scope.totalPPN                = Math.floor(($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) *0.1);  
            $scope.totalPPN                = ASPricingEngine.calculate({
                                                InputPrice: (($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * (1+PPNPerc/100)),
                                                // Discount: 0,
                                                // Qty: 0,
                                                Tipe: 33, 
                                                PPNPercentage: PPNPerc,
                                            });      
            $scope.totalEstimasi           = $scope.totalPPN + $scope.subTotalMaterialSummary + $scope.subTotalWorkSummary + $scope.costMaterial;
            // =================== CR4 Re-Calculate following CSV, req by Kevin =======================

            //nentuin diskon task & parts
            jejeranDiskonTask = [];
            jejeranDiskonParts = [];
            for(var i in $scope.gridWork){
                // if($scope.gridWork[i].PaidById == 30|| $scope.gridWork[i].PaidById == 31 || $scope.gridWork[i].PaidById == 32 || $scope.gridWork[i].PaidById == 2277){
                // if($scope.gridWork[i].PaidById == 30|| $scope.gridWork[i].PaidById == 2277){
                if($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                    var zeroDiscountTask = {Discount : 0}
                    jejeranDiskonTask.push(zeroDiscountTask.Discount);
                }else{
                    jejeranDiskonTask.push($scope.gridWork[i].Discount);
                }
                if($scope.gridWork[i].child.length > 0 ){
                    for(var j in $scope.gridWork[i].child){
                        // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 31 || $scope.gridWork[i].child[j].PaidById == 32 || $scope.gridWork[i].child[j].PaidById == 2277){
                        // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32){
                            var zeroDiscountParts = {Discount : 0}
                            jejeranDiskonParts.push(zeroDiscountParts.Discount);
                        }else{
                            jejeranDiskonParts.push($scope.gridWork[i].child[j].Discount);
                        }

                        
                    }
                }
            }
            //nentuin diskon task & parts
            $scope.displayDiscountAlgorithm();

            


            if ($scope.tmpActRate !== $scope.tmpActRateChange) {
                $scope.mData.StallId = 0;
                $scope.mData.FullDate = null;
                $scope.mData.PlanStart = null;
                $scope.mData.PlanFinish = null;
                $scope.mData.PlanDateStart = null;
                //$scope.mData.AppointmentDate = null; 
                delete $scope.mData.AppointmentDate;
                //$scope.mData.AppointmentTime = null;
                delete $scope.mData.AppointmentTime;
                $scope.mData.TargetDateAppointment = null;
                $scope.mData.FixedDeliveryTime1 = null;
                $scope.mData.PlanDateFinish = null;
                $scope.mData.StatusPreDiagnose = 0;
                delete $scope.mData.PrediagScheduledTime;
                delete $scope.mData.PrediagStallId;
            }
            deleteJob(data);
        }
        $scope.checkStatusPart = function (id, dataTask, param, jobtaskId) {
            console.log('ID ====', id);
            console.log('DATA TASK =====', dataTask);
            var tmpArray = [];
            if (param === 1 || param === 2 || param === 3 || param === 4 || param === 5) {
                for (var i in dataTask) {
                    if (dataTask[i].JobPartsId !== undefined) {
                        tmpArray.push({
                            JobPartsId: dataTask[i].JobPartsId
                        })
                    }
                }
            }
            if(param == 4){
                var idxTask = _.findIndex($scope.gridWork, { JobTaskId: jobtaskId });
                if (idxTask > -1) {
                    for(var z in dataTask){
                        $scope.gridWork[idxTask].child.push(dataTask[z]);
                    }
                }
                bsAlert.warning("Material ini sedang dalam proses order", "Batalkan PO dahulu atau tunggu PO complete");
            }
        }
        //====================
        $scope.checkPrice = function(data) {
            console.log("RetailPrice nya ===>", data);
            console.log("$scope.ldModel.Qty nya ==>", $scope.ldModel.Qty);
            var sum = 0;
            var sumDp = 0;
            if (data !== undefined && data !== null) {
                if ($scope.ldModel.Qty !== null) {
                    var qty = $scope.ldModel.Qty;
                    var tmpDp = $scope.ldModel.nightmareIjal ? $scope.ldModel.nightmareIjal : 0;
                    if (qty !== null) {
                        sum = Math.round(qty * data);
                        sumDp = Math.round(qty * tmpDp);
                        $scope.ldModel.subTotal = sum;
                        $scope.ldModel.DownPayment = sumDp;
                        $scope.ldModel.DPRequest = sumDp;
                        $scope.ldModel.minimalDp = sumDp;
                        // $scope.ldModel.Discount = 0;

                        if ($scope.ldModel.PaidById != 28 && $scope.ldModel.PaidById != undefined){
                            $scope.ldModel.DownPayment = 0;
                            $scope.ldModel.DPRequest = 0;
                            $scope.ldModel.minimalDp = 0;
                            $scope.disableDP = true;
                        } else {
                            $scope.disableDP = false;
                        }


                        //tambahan CR4  start ==> request okrivia (qty * harga parts ) - diskon
                        // if($scope.ldModel.PaidById == 30 || $scope.ldModel.PaidById == 31 || $scope.ldModel.PaidById == 32  || $scope.ldModel.MaterialTypeId == 2 || $scope.ldModel.PaidById == 2277){
                        // if($scope.ldModel.PaidById == 30 || $scope.ldModel.MaterialTypeId == 2 || $scope.ldModel.PaidById == 2277){
                        if($scope.ldModel.PaidById == 30 || $scope.ldModel.PaidById == 2277){
                            $scope.ldModel.Discount = 0;
                            $scope.ldModel.DiskonForDisplay = 0+'%';
                            $scope.ldModel.NominalDiscount = 0;
                            $scope.ldModel.subTotalForDisplay = sum - $scope.ldModel.NominalDiscount;
                            $scope.ldModel.subTotal = sum;
                        }else{
                            $scope.ldModel.Discount = $scope.DiscountParts;
                            $scope.ldModel.DiskonForDisplay = String($scope.DiscountParts) + '%';
                            $scope.ldModel.NominalDiscount = (qty* $scope.ldModel.RetailPrice)*($scope.DiscountParts/100);
                            $scope.ldModel.subTotalForDisplay = sum - $scope.ldModel.NominalDiscount;
                            $scope.ldModel.subTotal = sum;
                        }
                        // tambahan CR4 end
                        if ($scope.ldModel.PaidById == 30){
                            //cek kl warranty ga blh ada diskon nya
                            //copy aja rumus yg dr CR di atas
                            $scope.ldModel.Discount = 0;
                            $scope.ldModel.DiskonForDisplay = String(0) + '%';
                            $scope.ldModel.NominalDiscount = (qty* $scope.ldModel.RetailPrice)*(0/100);
                            $scope.ldModel.subTotalForDisplay = sum - $scope.ldModel.NominalDiscount;
                        }

                    } else {
                        $scope.ldModel.subTotal = 0;
                        $scope.ldModel.Discount = 0;
                        $scope.ldModel.DownPayment = Math.round(tmpDp);
                        $scope.ldModel.DPRequest = Math.round(tmpDp);
                        $scope.ldModel.minimalDp = Math.round(tmpDp);
                    }
                }
            } else {
                $scope.ldModel.subTotal = 0;
                $scope.ldModel.Discount = 0;
                $scope.ldModel.DownPayment = 0;
                $scope.ldModel.DPRequest = 0;
                $scope.ldModel.minimalDp = 0;
            }
        };
        $scope.checkServiceRate = function(item, data) {
            console.log("item", item);
            console.log("item", data);
            var sum = 0;
            var row = {};
            var tmpItem;
            if (data == 59 || data == 60) {
                if ($scope.tmpServiceRate !== null) {
                    if (item !== null) {
                        tmpItem = item.toString();
                        if (tmpItem.includes(".")) {
                            tmpItem = tmpItem.split('.');
                            if (tmpItem[tmpItem.length - 1] !== ".") {
                                // item = parseInt(tmpItem);
                                sum = $scope.tmpServiceRate.WarrantyRate * item;
                                sum = Math.round(sum);
                            }
                        } else {
                            sum = $scope.tmpServiceRate.WarrantyRate * item;
                            sum = Math.round(sum);
                        }
                    } else {
                        sum = $scope.tmpServiceRate.WarrantyRate;
                        sum = Math.round(sum);
                    }
                    $scope.PriceAvailable = true;
                    sum = Math.round(sum);
                    row.Summary = sum;
                    row.Fare = $scope.tmpServiceRate.WarrantyRate;
                }
            } else {
                if ($scope.tmpServiceRate !== null) {
                    if (item !== null) {
                        tmpItem = item.toString();
                        if (tmpItem.includes(".")) {
                            tmpItem = tmpItem.split('.');
                            if (tmpItem[tmpItem.length - 1] !== ".") {
                                // item = parseInt(tmpItem);
                                sum = $scope.tmpServiceRate.ServiceRate * item;
                                sum = Math.round(sum);
                            }
                        } else {
                            sum = $scope.tmpServiceRate.ServiceRate * item;
                            sum = Math.round(sum);
                        }
                    } else {
                        sum = $scope.tmpServiceRate.ServiceRate;
                        sum = Math.round(sum);
                    }
                    $scope.PriceAvailable = true;
                    sum = Math.round(sum);
                    row.Summary = sum;
                    row.Fare = $scope.tmpServiceRate.ServiceRate;
                }
            }
            row.ActualRate = item;
            $scope.listApi.addDetail(false, row);
        }
        $scope.sumAllPrice = function() {
            var NumberParent = 1;
            for (i in $scope.gridWork){
                $scope.gridWork[i].NumberParent = NumberParent+parseInt(i);
                for (j in $scope.gridWork[i].child){
                    $scope.gridWork[i].child[j].NumberChild = $scope.gridWork[i].NumberParent + "."  + parseInt(++j);
                }
            }

            // =================================================== Total Work =======================================================================
            var totalW = 0;
            var totalWnotWarranty = 0;
            $scope.adaWarranty = 0;
            $scope.jmlJobWarranty = $scope.gridWork.length;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].Summary !== undefined) {
                    if ($scope.gridWork[i].Summary == "") {
                        $scope.gridWork[i].Summary = $scope.gridWork[i].Fare * $scope.gridWork[i].FlatRate;
                    }
                    
                        // if ($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                        if ($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                            totalW += 0
                        } else {
                            // totalW += Math.round($scope.gridWork[i].Summary / 1.1);
                            totalW += ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWork[i].Summary,
                                            // Discount: 5,
                                            // Qty: 2,
                                            Tipe: 2, 
                                            PPNPercentage: PPNPerc,
                                        });
                        }
                    
                        if ($scope.gridWork[i].PaidById == 30){
                            $scope.adaWarranty++
                        } else {
                            // totalWnotWarranty += Math.round($scope.gridWork[i].Summary / 1.1);
                            totalWnotWarranty += ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWork[i].Summary,
                                                    // Discount: 5,
                                                    // Qty: 2,
                                                    Tipe: 2, 
                                                    PPNPercentage: PPNPerc,
                                                });
                        }
                    
                }
            }
            console.log("totalW", $scope.totalWork);
            
            $scope.totalWork = totalW ;  //request pak dodi dibagi 1.1 13/02/2019
            $scope.totalWorknotWarranty = totalWnotWarranty
            // =================================================== Total Work =======================================================================


           
            // ==================================
            var totalDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].DownPayment !== undefined) {
                            totalDp += $scope.gridWork[i].child[j].DownPayment;
                            console.log("$scope.gridWork[i].child.Price", $scope.gridWork[i].child[j].DownPayment);
                        }
                    }
                }
            }
            $scope.totalDp = totalDp;
            // ==================================
            var totalDefaultDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].minimalDp !== undefined) {
                            totalDefaultDp += $scope.gridWork[i].child[j].minimalDp;
                            console.log("$scope.gridWork[i].child.totalDefaultDp", $scope.gridWork[i].child[j].minimalDp);
                        }
                    }
                }
            }
            $scope.totalDpDefault = totalDefaultDp;
            // ==================================
            var totalActualRate = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].ActualRate !== undefined) {
                    totalActualRate += $scope.gridWork[i].ActualRate;
                }
            }
            $scope.tmpActRate = totalActualRate;
            // ==================================
            
            

            // =================== CR4 Re-Calculate following CSV, req by Kevin =======================
            var totalMaterial_byKevin = 0;
            var totalMaterial_byKevinNotWarranty = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        // if($scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32){
                            totalMaterial_byKevin += 0;
                        }else{

                            if($scope.gridWork[i].child[j].PaidById == 30 ){ 
                                totalMaterial_byKevin += 0;
                            }else{ 
                                // totalMaterial_byKevin += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                                totalMaterial_byKevin += ASPricingEngine.calculate({
                                                            InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                            // Discount: 5,
                                                            Qty: $scope.gridWork[i].child[j].Qty,
                                                            Tipe: 102, 
                                                            PPNPercentage: PPNPerc,
                                                        }); 
                            }

                            if ($scope.gridWork[i].child[j].PaidById == 30){

                            } else {
                                // totalMaterial_byKevinNotWarranty += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                                totalMaterial_byKevinNotWarranty += ASPricingEngine.calculate({
                                                                        InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                                        // Discount: 5,
                                                                        Qty: $scope.gridWork[i].child[j].Qty,
                                                                        Tipe: 102, 
                                                                        PPNPercentage: PPNPerc,
                                                                    }); 
                            }


                        }
                    }
                }
            };
            
            var totalMD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        

                        // if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30 ){
                        if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32 ){
                            totalMD_byKevin += 0;
                        }else{
                            // totalMD_byKevin +=  Math.round(Math.round($scope.gridWork[i].child[j].RetailPrice /1.1 ) * ($scope.gridWork[i].child[j].Discount/100)) *  $scope.gridWork[i].child[j].Qty;
                            totalMD_byKevin +=  ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                    Discount: $scope.gridWork[i].child[j].Discount,
                                                    Qty: $scope.gridWork[i].child[j].Qty,
                                                    Tipe: 112, 
                                                    PPNPercentage: PPNPerc,
                                                }); 
                            
                        }
                    }
                }
            };

            // =================================================== Total Work Discount =======================================================================
            var totalWD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                // if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                if($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                    totalWD_byKevin += 0;
                }else{
                    // totalWD_byKevin +=  Math.round(Math.round($scope.gridWork[i].Summary /1.1 ) * ($scope.gridWork[i].Discount/100));
                    totalWD_byKevin +=  ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWork[i].Summary,
                                            Discount: $scope.gridWork[i].Discount,
                                            // Qty: 2,
                                            Tipe: 12, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                }
            };
            console.log('totalWD_byKevin ===>',totalWD_byKevin);
            // =================================================== Total Work Discount =======================================================================


            // =======================================================
            $scope.summaryJoblist = Math.round(($scope.totalWork + ($scope.totalMaterial - ($scope.totalMaterial * $scope.DiscountParts / 100))) + ((($scope.totalWork + ($scope.totalMaterial - ($scope.totalMaterial * $scope.DiscountParts / 100))) * 10) / 100));
            if ($scope.summaryJoblist >= 1000000) {
                $scope.costMaterial = 6000;
            } else if ($scope.summaryJoblist > 250000) {
                $scope.costMaterial = 3000;
            } else {
                $scope.costMaterial = 0;
            }
            // =======================================================

            $scope.totalMaterial           = totalMaterial_byKevin;
            $scope.totalMaterialnotWarranty = totalMaterial_byKevinNotWarranty;
            $scope.totalMaterialDiscounted = totalMD_byKevin;
            $scope.totalWorkDiscounted     = totalWD_byKevin;
            $scope.subTotalMaterialSummary = $scope.totalMaterial - $scope.totalMaterialDiscounted;
            $scope.subTotalWorkSummary     = $scope.totalWork - $scope.totalWorkDiscounted;
            // $scope.totalPPN                = Math.floor(($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) *0.1);
            $scope.totalPPN                = ASPricingEngine.calculate({
                                                InputPrice: (($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * (1+PPNPerc/100)),
                                                // Discount: 0,
                                                // Qty: 0,
                                                Tipe: 33, 
                                                PPNPercentage: PPNPerc,
                                            });    
            $scope.totalEstimasi           = $scope.totalPPN + $scope.subTotalMaterialSummary + $scope.subTotalWorkSummary + $scope.costMaterial;
            // =================== CR4 Re-Calculate following CSV, req by Kevin =======================
            //nentuin diskon task & parts
            jejeranDiskonTask = [];
            jejeranDiskonParts = [];
            for(var i in $scope.gridWork){
                // if($scope.gridWork[i].PaidById == 30|| $scope.gridWork[i].PaidById == 31 || $scope.gridWork[i].PaidById == 32 || $scope.gridWork[i].PaidById == 2277){
                // if($scope.gridWork[i].PaidById == 30|| $scope.gridWork[i].PaidById == 2277){
                if($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                    var zeroDiscountTask = {Discount : 0}
                    jejeranDiskonTask.push(zeroDiscountTask.Discount);
                }else{
                    jejeranDiskonTask.push($scope.gridWork[i].Discount);
                }
                if($scope.gridWork[i].child.length > 0 ){
                    for(var j in $scope.gridWork[i].child){
                        // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 31 || $scope.gridWork[i].child[j].PaidById == 32 || $scope.gridWork[i].child[j].PaidById == 2277){
                        // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32){
                            var zeroDiscountParts = {Discount : 0}
                            jejeranDiskonParts.push(zeroDiscountParts.Discount);
                        }else{
                            jejeranDiskonParts.push($scope.gridWork[i].child[j].Discount);
                        }

                        
                    }
                }
            }
            //nentuin diskon task & parts
            $scope.displayDiscountAlgorithm();
            
            console.log("$scope.gridWork", $scope.gridWork);
            console.log("gridWork", $scope.gridWork);
        };
        $scope.checkDp = function(valueDP, param){
            console.log('DP =====>', valueDP, param);
            if(param.nightmareIjal == 0 && param.Availbility == "Tidak Tersedia"){
                $scope.ldModel.minimalDp = valueDP;
            }
        }
        $scope.checkIsOPB = function(data, obj){
            $timeout(function(){
                if(obj.IsOPB == 1){
                    console.log("=====", data, obj)
                    $scope.ldModel.PartsCode = null;
                    $scope.ldModel.PartsId = null;
                    $scope.ldModel.PartId = null;
                    $scope.ldModel.PartsName = null
                    $scope.partsAvailable = false;
                }else{
                    $scope.partsAvailable = true;
                }
            },100)
        }
        $scope.checkAvailabilityParts = function(data) {
            console.log('xixi 123')
            console.log("data data data Parts", data);
            if (data.PartsId !== undefined) {
                AppointmentGrService.getAvailableParts(data.PartsId, 0, $scope.ldModel.Qty).then(function(res) {
                    // ====== validasi cek kl retailprice nya null ato 0 ga blh di pake ==================================================== start
                    if (res.data.Result[0].RetailPrice === 0 || res.data.Result[0].RetailPrice === null || res.data.Result[0].RetailPrice === undefined){
                        bsAlert.warning('Harga Retail Belum Tersedia. Harap hubungi Partsman/Petugas Gudang Bahan.');
                        return
                    }
                    // ====== validasi cek kl retailprice nya null ato 0 ga blh di pake ==================================================== end
                    console.log("Ressss abis availability", res.data);
                    var tmpAvailability = res.data.Result[0];
                    tmpAvailability.RetailPrice = Math.round(tmpAvailability.RetailPrice);
                    $scope.ldModel.RetailPrice = tmpAvailability.RetailPrice;


                    // ==================================
                    if ($scope.ldModel.PaidBy == "Internal" || $scope.ldModel.PaidBy == "Warranty" || $scope.ldModel.PaidBy == " Free Service" || $scope.JobIsWarranty == 1) {
                        // if ($scope.JobIsWarranty == 1){
                        // $scope.checkPrice(tmpAvailability.RetailPrice);
                        // }
                        $scope.ldModel.DownPayment = 0;
                        $scope.ldModel.DPRequest = 0;
                        $scope.ldModel.minimalDp = 0;
                    } else {
                        $scope.ldModel.minimalDp = Math.round(tmpAvailability.PriceDP);
                        $scope.ldModel.nightmareIjal = Math.round(tmpAvailability.PriceDP);
                        $scope.ldModel.DPRequest = Math.round(tmpAvailability.PriceDP);
                        $scope.ldModel.DownPayment = Math.round(tmpAvailability.PriceDP);
                    }
                    if ($scope.ldModel.Qty !== undefined && $scope.ldModel.Qty !== null) {
                        $scope.checkPrice(tmpAvailability.RetailPrice);
                    }
                    // ==================================
                    $scope.ldModel.RetailPrice = tmpAvailability.RetailPrice;
                    // $scope.ldModel.minimalDp = tmpAvailability.ValueDP;
                    // $scope.ldModel.minimalDp = tmpAvailability.PriceDP;
                    // $scope.ldModel.nightmareIjal = tmpAvailability.PriceDP;

                    if (tmpAvailability.isAvailable == 0) {
                        $scope.ldModel.Availbility = "Tidak Tersedia";
                        if (tmpAvailability.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                            $scope.ldModel.ETA = tmpAvailability.DefaultETA;
                        } else {
                            $scope.ldModel.ETA = tmpAvailability.DefaultETA;
                        }
                        console.log("tempe", tmpAvailability.PartsClassId3);

                        if(tmpAvailability.PartsClassId3 == 113 && tmpAvailability.PartsClassId1 == 1) {
                            $scope.ldModel.OrderType = 6;
                            data.OrderType = 6;
                        }
                        else {
                            $scope.ldModel.OrderType = 3;
                            $scope.ldModel.Type = 3;
                            data.OrderType = 3;
                            data.Type = 3;
                        }

                        $scope.DisTipeOrder = false;
                    } else if (tmpAvailability.isAvailable == 2) {
                        $scope.ldModel.Availbility = "Tersedia Sebagian";
                        if(tmpAvailability.PartsClassId3 == 113 && tmpAvailability.PartsClassId1 == 1) {
                            $scope.ldModel.OrderType = 6;
                        }
                    } else {
                        $scope.ldModel.Availbility = "Tersedia";
                        //kl tersedia dp nya 0 in dan disable
                        //$scope.ldModel.DownPayment = 0; //commented by Imron, karena tidak bisa Simpan
                    }
                });
            }
        }
        $scope.getAvailablePartsService = function(item, row) {
            console.log('wokwok')
            console.log('item part', item)
            console.log('row part', row)
                if (item.PartsId !== null) {
                    AppointmentGrService.getAvailableParts(item.PartsId,$scope.mData.JobId,item.Qty).then(function(res) {
                        var tmpRes = res.data.Result[0];
                        console.log("tmpRes", tmpRes);
                        tmpParts = tmpRes;
                        if (tmpParts !== null) {
                            console.log("have data RetailPrice");
                            item.RetailPrice = tmpParts.RetailPrice;
                            item.minimalDp = Math.round(tmpParts.PriceDP * item.Qty);
                            item.MaterialTypeId = item.PartsClassId1;
                            item.subTotal = item.Qty * tmpParts.RetailPrice;
                            item.DPRequest = Math.round(tmpParts.PriceDP * item.Qty);
                            item.DownPayment = Math.round(tmpParts.PriceDP * item.Qty);
                            item.nightmareIjal = tmpParts.PriceDP;
                            item.Discount = 0;
                            item.SatuanId = item.UomId;
                            for (var i in $scope.unitData) {
                                if ($scope.unitData[i].MasterId == item.SatuanId) {
                                    item.satuanName = $scope.unitData[i].Name;
                                }
                            }
                            if (row.catName == 'FPC - Free Parts Claim' || row.catName == 'PLC - Parts Labor Claim') {
                                console.log('imam sini')
                                for (var i in $scope.paymentDataThereIstWarranty) {
                                    console.log('paymentDataThereIstWarranty', $scope.paymentDataThereIstWarranty[i])
                                    if ($scope.paymentDataThereIstWarranty[i].Name == "Warranty") {
                                        item.PaidById = $scope.paymentDataThereIstWarranty[i].MasterId;
                                        item.PaidBy = $scope.paymentDataThereIstWarranty[i].Name;
                                        item.Discount = 0;
                                        item.DiscountTypeId = -1;
                                        item.DiskonForDisplay = item.Discount + '%';
                                    }
                                }
                            }else if(row.catName == 'FLC - Free Labor Claim'){
                                for (var i in $scope.paymentDataIsNotWarranty) {
                                    console.log('paymentDataIsNotWarranty', $scope.paymentDataIsNotWarranty[i])
                                    if ($scope.paymentDataIsNotWarranty[i].Name == "Customer") {
                                        item.PaidById = $scope.paymentDataIsNotWarranty[i].MasterId;
                                        item.PaidBy = $scope.paymentDataIsNotWarranty[i].Name;
                                    }
                                }
                            }else{
                                for (var i in $scope.paymentData) {
                                    if ($scope.paymentData[i].Name == "Customer") {
                                        item.PaidById = $scope.paymentData[i].MasterId;
                                        item.PaidBy = $scope.paymentData[i].Name;
                                    }
                                }
                            }
                            
                            if (tmpParts.isAvailable == 0) {
                                item.Availbility = "Tidak Tersedia";
                                if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                    item.ETA = tmpParts.DefaultETA;
                                } else {
                                    item.ETA = tmpParts.DefaultETA;
                                }

                                if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                    item.OrderType = 6;
                                }
                                else {
                                    item.OrderType = 3;
                                }
                            } else if (tmpParts.isAvailable == 2) {
                                item.Availbility = "Tersedia Sebagian";
                                if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                    item.OrderType = 6;
                                }
                            } else {
                                item.Availbility = "Tersedia";
                                //kl tersedia dp nya 0 in dan disable
                                //item.DownPayment = 0; //commented by Imron, karena tidak bisa Simpan
                            }
                        } else {
                            console.log("haven't data RetailPrice");
                        }
                        //Pengecekan untuk ngambil data service tipe pekerjaan FLC
                        if ($scope.tmpPaidById !== null) {
                            if (row.catName != 'FLC - Free Labor Claim') {
                                item.PaidById = $scope.tmpPaidById;
                                item.PaidBy = $scope.tmpPaidName;
                            }
                        }

                        tmp = item;
                        $scope.listApi.addDetail(tmp, row);
                        return item;
                    });
                } else {
                    if ($scope.tmpPaidById !== null) {
                        item.PaidById = $scope.tmpPaidById;
                        item.PaidBy = $scope.tmpPaidName;
                    }

                    if (row.catName == 'FPC - Free Parts Claim' || row.catName == 'PLC - Parts Labor Claim') {
                        console.log('imam sini')
                        for (var i in $scope.paymentDataThereIstWarranty) {
                            console.log('paymentDataThereIstWarranty', $scope.paymentDataThereIstWarranty[i])
                            if ($scope.paymentDataThereIstWarranty[i].Name == "Warranty") {
                                item.PaidById = $scope.paymentDataThereIstWarranty[i].MasterId;
                                item.PaidBy = $scope.paymentDataThereIstWarranty[i].Name;
                                item.Discount = 0;
                                item.DiscountTypeId = -1;
                                item.DiskonForDisplay = item.Discount + '%';
                            }
                        }
                    }else if(row.catName == 'FLC - Free Labor Claim'){
                        for (var i in $scope.paymentDataIsNotWarranty) {
                            console.log('paymentDataIsNotWarranty', $scope.paymentDataIsNotWarranty[i])
                            if ($scope.paymentDataIsNotWarranty[i].Name == "Customer") {
                                item.PaidById = $scope.paymentDataIsNotWarranty[i].MasterId;
                                item.PaidBy = $scope.paymentDataIsNotWarranty[i].Name;
                            }
                        }
                    }else{
                        for (var i in $scope.paymentData) {
                            if ($scope.paymentData[i].Name == "Customer") {
                                item.PaidById = $scope.paymentData[i].MasterId;
                                item.PaidBy = $scope.paymentData[i].Name;
                            }
                        }
                    }

                    item.MaterialTypeId = item.PartsClassId1;
                    item.RetailPrice = item.Price;
                    item.subTotal = item.Qty * item.Price;
                    item.DPRequest = Math.round(item.DPRequest * item.Qty);
                    item.DownPayment = Math.round(item.DownPayment * item.Qty);
                    item.nightmareIjal = item.minimalDp;
                    item.Discount = 0;
                    item.SatuanId = item.UomId;
                    for (var i in $scope.unitData) {
                        if ($scope.unitData[i].MasterId == item.SatuanId) {
                            item.satuanName = $scope.unitData[i].Name;
                        }
                    }
                    tmp = item;
                    $scope.listApi.addDetail(tmp, row);
                    return item;
                }
            }
            // $scope.getAvailablePartsServiceManual = function(item) {
            //     if (item.PartsId !== null) {
            //         AppointmentGrService.getAvailableParts(item.PartsId).then(function(res) {
            //             var tmpRes = res.data.Result[0];
            //             tmpParts = tmpRes;
            //             if (tmpParts !== null) {
            //                 console.log("have data RetailPrice");
            //                 item.RetailPrice = tmpParts.RetailPrice;
            //                 item.priceDp = tmpParts.PriceDP;
            //                 item.subTotal = item.Qty * item.RetailPrice;
            //                 // item.MaterialTypeId = item.PartsClassId1;
            //                 item.DPRequest = Math.round(item.DPRequest);
            //                 item.DownPayment = Math.round(item.DownPayment);
            //                 item.Discount = 0;
            //                 item.nightmareIjal = Math.round(item.minimalDp / item.Qty);
            //                 // item.SatuanId = item.UomId;
            //                 for (var i in $scope.unitData) {
            //                     if ($scope.unitData[i].MasterId == item.SatuanId) {
            //                         item.satuanName = $scope.unitData[i].Name;
            //                     }
            //                 }
            //                 for (var i in $scope.paymentData) {
            //                     console.log("$scope.paymentData[i].MasterId",$scope.paymentData[i].MasterId);
            //                     console.log("item.PaidById",item.PaidById);
            //                     if ($scope.paymentData[i].MasterId == item.PaidById) {
            //                         item.PaidById = $scope.paymentData[i].MasterId;
            //                         item.PaidBy = $scope.paymentData[i].Name;
            //                     }
            //                 }
            //                 if (tmpParts.isAvailable == 0) {
            //                     item.Availbility = "Tidak Tersedia";
            //                     if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
            //                         item.ETA = "";
            //                     } else {
            //                         item.ETA = tmpParts.DefaultETA;
            //                     }
            //                     item.Type = 3;
            //                 } else {
            //                     item.Availbility = "Tersedia";
            //                 }
            //             } else {
            //                 console.log("haven't data RetailPrice");
            //             }
            //             Parts.push(item);
            //             $scope.sumAllPrice();
            //             if ($scope.modeDetail == 'view') { $scope.disableButton(); }
            //             return item;
            //         });
            //     } else {
            //         item.RetailPrice = item.Price;
            //         item.subTotal = item.Qty * item.Price;
            //         item.DPRequest = Math.round(item.DPRequest);
            //         item.DownPayment = Math.round(item.DownPayment);
            //         item.MaterialTypeId = item.PartsClassId1;
            //         item.Discount = 0;
            //         item.SatuanId = item.UomId;
            //         item.nightmareIjal = Math.round(item.minimalDp / item.Qty);
            //         for (var i in $scope.unitData) {
            //             if ($scope.unitData[i].MasterId == item.SatuanId) {
            //                 item.satuanName = $scope.unitData[i].Name;
            //             }
            //         }
            //         for (var i in $scope.paymentData) {
            //             if ($scope.paymentData[i].MasterId == item.PaidById) {
            //                 item.PaidById = $scope.paymentData[i].MasterId;
            //                 item.PaidBy = $scope.paymentData[i].Name;
            //             }
            //         }
            //         tmp = item;
            //         Parts.push(item);
            //         $scope.sumAllPrice();
            //         return item;
            //     }
            // };
        $scope.getAvailablePartsServiceManual = function(item, x, y) {
            console.log("getAvailablePartsServiceManual", item);
            console.log("x : ", x);
            console.log("y : ", y);

            if (x > item.length - 1) {
                return item;
            }
            if (item[x].isDeleted !== 1) {
                if (item[x].JobParts[y] !== undefined && item[x].JobParts[y].PartsId !== null) {
                    var itemTemp = item[x].JobParts[y];
                        AppointmentGrService.getAvailableParts(itemTemp.PartsId, $scope.mData.JobId, itemTemp.Qty).then(function(res) {
                            var tmpParts = res.data.Result[0];
                            console.log('====> Hajime', tmpParts);
                            if (tmpParts !== null && tmpParts !== undefined) {
                                // if (itemTemp.isDeleted == 0){
                                    // console.log("have data RetailPrice",item,tmpRes);
                                    // item[x].JobParts[y].RetailPrice = tmpParts.RetailPrice;
                                    item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                                    // item[x].JobParts[y].minimalDp = tmpParts.PriceDP;
                                    item[x].JobParts[y].nightmareIjal = item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty;
                                    item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                    item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;

                                    // item.DiscountedPrice =  (normalPrice * item.Discount)/100;
                                    // if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                    //     item[x].JobParts[y].typeDiskon = -1;
                                    //     item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                    // } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                    //     item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
                                    //     item[x].JobParts[y].typeDiskon = 0;
                                    // }
                                    if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                        item[x].JobParts[y].typeDiskon = -1;
                                        item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                    } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                        item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                        // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
                                        item[x].JobParts[y].typeDiskon = 0;
                                    } else if ((item[x].JobParts[y].IsCustomDiscount == 1 || item[x].JobParts[y].IsCustomDiscount == 0) && item[x].JobParts[y].Discount == 0) {
                                        item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                        // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                                        item[x].JobParts[y].typeDiskon = 0;
                                    }
                                    // tambahan CR4
                                    else if(item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount != 0){
                                        item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                        item[x].JobParts[y].typeDiskon = item[x].JobParts[y].DiscountTypeId;
                                    }

                                    if (item[x].JobParts[y].PaidBy !== null) {
                                        item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy.Name;
                                        // delete item[x].JobParts[y].PaidBy;
                                    };
                                    if (item[x].JobParts[y].Satuan !== null) {
                                        item[x].JobParts[y].satuanName = item[x].JobParts[y].Satuan.Name;
                                        delete item[x].JobParts[y].Satuan;
                                    };
                                    if (tmpParts.isAvailable == 0) {
                                        item[x].JobParts[y].Availbility = "Tidak Tersedia";
                                        if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                            item[x].JobParts[y].ETA = tmpParts.DefaultETA;
                                        } else {
                                            item[x].JobParts[y].ETA = tmpParts.DefaultETA;
                                        }

                                        if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                            item[x].JobParts[y].OrderType = 6;
                                        }
                                        else {
                                            item[x].JobParts[y].OrderType = 3;
                                        }
                                    } else if (tmpParts.isAvailable == 2) {
                                        item[x].JobParts[y].Availbility = "Tersedia Sebagian";   
                                        if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                            item[x].JobParts[y].OrderType = 6;
                                        }     
                                    } else {
                                        item[x].JobParts[y].Availbility = "Tersedia";
                                        //kl tersedia dp nya 0 in dan disable
                                        //item[x].JobParts[y].DownPayment = 0; //commented by Imron, karena tidak bisa Simpan
                                    }
                                // }
                                
                            } else {
                                // if (itemTemp.isDeleted == 0){
                                    console.log("haven't data RetailPrice");
                                    item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                                    item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                                    item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                                    item[x].JobParts[y].nightmareIjal = Math.round(item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty);
                                    // if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                    //     item[x].JobParts[y].typeDiskon = -1;
                                    //     item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                                    // } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                    //     item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                                    //     item[x].JobParts[y].typeDiskon = 0;
                                    // }
                                    if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                        item[x].JobParts[y].typeDiskon = -1;
                                        item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                    } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                        item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                        // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
                                        item[x].JobParts[y].typeDiskon = 0;
                                    } else if ((item[x].JobParts[y].IsCustomDiscount == 1 || item[x].JobParts[y].IsCustomDiscount == 0) && item[x].JobParts[y].Discount == 0) {
                                        item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                        // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                                        item[x].JobParts[y].typeDiskon = 0;
                                    }
                                    // tambahan CR4
                                    else if(item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount != 0){
                                        item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                                        item[x].JobParts[y].typeDiskon = item[x].JobParts[y].DiscountTypeId;
                                    }
                                    if (item[x].JobParts[y].PaidBy !== null) {
                                        item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy.Name;
                                        // delete item[x].JobParts[y].PaidBy;
                                    };
                                    if (item[x].JobParts[y].Satuan !== null) {
                                        item[x].JobParts[y].satuanName = item[x].JobParts[y].Satuan.Name;
                                        delete item[x].JobParts[y].Satuan;
                                    };
                                // }
                            }
                            if (item[x].JobParts[y].Part !== null && item[x].JobParts[y].Part !== undefined) {
                                item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                                delete item[x].JobParts[y].Part;
                            }
                            if (item[x].JobParts[y].IsOPB !== null) {
                                item[x].JobParts[y].IsOPB = item[x].JobParts[y].IsOPB;
                            }
                            item[x].JobParts[y].PartsId = item[x].JobParts[y].PartsId + '&' + item[x].JobParts[y].PaidById;
                            if (itemTemp.isDeleted == 0){
                                Parts.push(item[x].JobParts[y]);
                                $scope.sumAllPrice();
                            }
                            
                            // return item;                    
    
                            if (x <= (item.length - 1)) {
                                if (y >= (item[x].JobParts.length - 1)) {
                                    if (item[x].Fare !== undefined) {
                                        var sum = item[x].Fare;
                                        var summ = item[x].FlatRate;
                                        var summaryy = sum * summ;
                                        var discountedPrice = (summaryy * item[x].Discount) / 100;
                                        var normalPrice = summaryy
                                        summaryy = parseInt(summaryy);
                                    }
                                    if (item[x].JobType == null) {
                                        item[x].JobType = { Name: "" };
                                    }
                                    if (item[x].PaidBy == null) {
                                        item[x].PaidBy = { Name: "" }
                                    }
                                    if (item[x].AdditionalTaskId == null) {
                                        item[x].isOpe = 0;
                                    } else {
                                        item[x].isOpe = 1;
                                    }
                                    if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                        item[x].typeDiskon = -1;
                                        item[x].DiscountedPrice = normalPrice;
                                    } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                        item[x].typeDiskon = 0;
                                        item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                    }
                                    item[x].typeDiskon = item[x].DiscountTypeId;
                                    gridTemp.push({
                                        ActualRate: item[x].ActualRate,
                                        AdditionalTaskId: item[x].AdditionalTaskId,
                                        isOpe: item[x].isOpe,
                                        Discount: item[x].Discount,
                                        JobTaskId: item[x].JobTaskId,
                                        JobTypeId: item[x].JobTypeId,
                                        DiscountedPrice: discountedPrice,
                                        typeDiskon: item[x].typeDiskon,
                                        NormalPrice: normalPrice,
                                        catName: item[x].JobType.Name,
                                        Fare: item[x].Fare,
                                        IsCustomDiscount: item[x].IsCustomDiscount,
                                        Summary: summaryy,
                                        PaidById: item[x].PaidById,
                                        JobId: item[x].JobId,
                                        TaskId: item[x].TaskId,
                                        TaskName: item[x].TaskName,
                                        FlatRate: item[x].FlatRate,
                                        ProcessId: item[x].ProcessId,
                                        UnitBy: item[x].UnitBy,
                                        TFirst1: item[x].TFirst1,
                                        PaidBy: item[x].PaidBy.Name,
                                        index: "$$" + x,
                                        child: Parts
                                    });
                                    $scope.sumAllPrice();
                                    Parts.splice();
                                    Parts = [];
                                    console.log("if 1 nilai x", x);
                                    $scope.getAvailablePartsServiceManual(item, x + 1, 0);
    
                                } else {
                                    $scope.getAvailablePartsServiceManual(item, x, y + 1);
                                }
                            }
                        });
                } else if (item[x].JobParts[y] !== undefined && (item[x].JobParts[y].PartsId == undefined || item[x].JobParts[y].PartsId == null)) {
                        item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                        item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                        item[x].JobParts[y].DownPayment = item[x].JobParts[y].DownPayment;
                        // ========================
                        console.log("haven't data RetailPrice");
                        item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                        item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                        item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                        item[x].JobParts[y].nightmareIjal = Math.round(item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty);
                        // if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                        //     item[x].JobParts[y].typeDiskon = -1;
                        //     item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                        // } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                        //     item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                        //     item[x].JobParts[y].typeDiskon = 0;
                        // }
                        if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                            item[x].JobParts[y].typeDiskon = -1;
                            item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                        } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                            item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                            // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
                            item[x].JobParts[y].typeDiskon = 0;
                        } else if ((item[x].JobParts[y].IsCustomDiscount == 1 || item[x].JobParts[y].IsCustomDiscount == 0) && item[x].JobParts[y].Discount == 0) {
                            item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                            // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                            item[x].JobParts[y].typeDiskon = 0;
                        }
                        // tambahan CR4
                        else if(item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount != 0){
                            item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                            item[x].JobParts[y].typeDiskon = item[x].JobParts[y].DiscountTypeId;
                        }
                        if (item[x].JobParts[y].PaidBy !== null) {
                            item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy.Name;
                            // delete item[x].JobParts[y].PaidBy;
                        };
                        if (item[x].JobParts[y].Part !== null && item[x].JobParts[y].Part !== undefined) {
                            item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                            delete item[x].JobParts[y].Part;
                        }
                        if (item[x].JobParts[y].Satuan !== null) {
                            item[x].JobParts[y].satuanName = item[x].JobParts[y].Satuan.Name;
                            delete item[x].JobParts[y].Satuan;
                        };
                        if (item[x].JobParts[y].IsOPB !== null) {
                            item[x].JobParts[y].IsOPB = item[x].JobParts[y].IsOPB;
                        }
                        if (item[x].JobParts[y].JobPartsId !== undefined && item[x].JobParts[y].JobPartsId !== null) {
                            item[x].JobParts[y].PartsId = "#" + item[x].JobParts[y].JobPartsId;
                        }
                        tmp = item[x].JobParts[y];
                    if (item[x].JobParts[y].isDeleted == 0){
                            Parts.push(item[x].JobParts[y]);
                            $scope.sumAllPrice();
                    }

                        if (x <= (item.length - 1)) {
                            if (y >= (item[x].JobParts.length - 1)) {
                                if (item[x].Fare !== undefined) {
                                    var sum = item[x].Fare;
                                    var summ = item[x].FlatRate;
                                    var summaryy = sum * summ;
                                    var discountedPrice = (summaryy * item[x].Discount) / 100;
                                    var normalPrice = summaryy
                                    summaryy = parseInt(summaryy);
                                }
                                if (item[x].JobType == null) {
                                    item[x].JobType = { Name: "" };
                                }
                                if (item[x].PaidBy == null) {
                                    item[x].PaidBy = { Name: "" }
                                }
                                if (item[x].AdditionalTaskId == null) {
                                    item[x].isOpe = 0;
                                } else {
                                    item[x].isOpe = 1;
                                }
                                if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                    item[x].typeDiskon = -1;
                                    item[x].DiscountedPrice = normalPrice;
                                } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                    item[x].typeDiskon = 0;
                                    item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                }

                                gridTemp.push({
                                    ActualRate: item[x].ActualRate,
                                    AdditionalTaskId: item[x].AdditionalTaskId,
                                    isOpe: item[x].isOpe,
                                    Discount: item[x].Discount,
                                    JobTaskId: item[x].JobTaskId,
                                    JobTypeId: item[x].JobTypeId,
                                    DiscountedPrice: discountedPrice,
                                    typeDiskon: item[x].typeDiskon,
                                    NormalPrice: normalPrice,
                                    catName: item[x].JobType.Name,
                                    Fare: item[x].Fare,
                                    IsCustomDiscount: item[x].IsCustomDiscount,
                                    Summary: summaryy,
                                    PaidById: item[x].PaidById,
                                    JobId: item[x].JobId,
                                    TaskId: item[x].TaskId,
                                    TaskName: item[x].TaskName,
                                    FlatRate: item[x].FlatRate,
                                    ProcessId: item[x].ProcessId,
                                    UnitBy: item[x].UnitBy,
                                    TFirst1: item[x].TFirst1,
                                    PaidBy: item[x].PaidBy.Name,
                                    index: "$$" + x,
                                    child: Parts
                                });
                                $scope.sumAllPrice();
                                Parts.splice();
                                Parts = [];
                                $scope.getAvailablePartsServiceManual(item, x + 1, 0);
                                console.log("if 2 nilai x", x);
                            } else {
                                $scope.getAvailablePartsServiceManual(item, x, y + 1);
                            }
                        }
                } else if (item[x].JobParts.length == 0) {
                    if (x <= (item.length - 1)) {
                        if (y >= (item[x].JobParts.length - 1)) {
                            if (item[x].Fare !== undefined) {
                                var sum = item[x].Fare;
                                var summ = item[x].FlatRate;
                                var summaryy = sum * summ;
                                var discountedPrice = (summaryy * item[x].Discount) / 100;
                                var normalPrice = summaryy
                                summaryy = parseInt(summaryy);
                            }
                            if (item[x].JobType == null) {
                                item[x].JobType = { Name: "" };
                            }
                            if (item[x].PaidBy == null) {
                                item[x].PaidBy = { Name: "" }
                            }
                            if (item[x].AdditionalTaskId == null) {
                                item[x].isOpe = 0;
                            } else {
                                item[x].isOpe = 1;
                            }
                            if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                item[x].typeDiskon = -1;
                                item[x].DiscountedPrice = normalPrice;
                            } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                item[x].typeDiskon = 0;
                                item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;

                            }
                            gridTemp.push({
                                ActualRate: item[x].ActualRate,
                                AdditionalTaskId: item[x].AdditionalTaskId,
                                isOpe: item[x].isOpe,
                                Discount: item[x].Discount,
                                JobTaskId: item[x].JobTaskId,
                                JobTypeId: item[x].JobTypeId,
                                DiscountedPrice: discountedPrice,
                                typeDiskon: item[x].typeDiskon,
                                NormalPrice: normalPrice,
                                catName: item[x].JobType.Name,
                                Fare: item[x].Fare,
                                IsCustomDiscount: item[x].IsCustomDiscount,
                                Summary: summaryy,
                                PaidById: item[x].PaidById,
                                JobId: item[x].JobId,
                                TaskId: item[x].TaskId,
                                TaskName: item[x].TaskName,
                                FlatRate: item[x].FlatRate,
                                ProcessId: item[x].ProcessId,
                                UnitBy: item[x].UnitBy,
                                TFirst1: item[x].TFirst1,
                                PaidBy: item[x].PaidBy.Name,
                                index: "$$" + x,
                                child: Parts
                            });
                            $scope.sumAllPrice();
                            Parts.splice();
                            Parts = [];
                            $scope.getAvailablePartsServiceManual(item, x + 1, 0);
                        } else {
                            $scope.getAvailablePartsServiceManual(item, x, y + 1);
                        }
                    }
                    // return item;                
                }

                if (x > item.length - 1) {
                    return item;
                }
            } else {
                $scope.getAvailablePartsServiceManual(item, x + 1, 0);
            }
            $scope.sumAllPrice();
        };
        // =================== CRM
        // $scope.ShowEdit = true;
        $scope.EnableEdit = true;
        $scope.ShowEditInstitusi = true;
        $scope.EnableEditInstitusi = true;

        $scope.EnableEditContactPerson = true;
        $scope.EditInfoUmum = function() {
            $scope.mDataDetailDefault = angular.copy($scope.mDataDetail);
            // AppointmentBpService.getLocation().then(function(res) {
            //     // console.log("List Province====>",res.data.Result);
            //     $scope.ProvinceData = res.data.Result;
            //     $scope.ShowEdit = false;
            //     $scope.EnableEdit = false;
            //     console.log("ProvinceData", $scope.ProvinceData);
            // })
            $scope.getAddressList();
            if ($scope.mDataDetail.CustomerVehicleId !== undefined) {
                $scope.getAddressList();
                console.log("$scope.mDataDetail.CustomerVehicleId", $scope.mDataDetail.CustomerVehicleId);
                AppointmentGrService.getDataContactPerson($scope.mDataDetail.CustomerVehicleId).then(function(res) {
                    // console.log("List Province====>",res.data.Result);
                    $scope.ContactPersonData = res.data.Result;
                    $scope.ShowEdit = false;
                    $scope.EnableEdit = false;
                    $scope.EnableEditContactPerson = false;
                    console.log("Data ContactPerson", $scope.ShowEdit);
                })
            };
        };
        $scope.BatalInfoUmum = function() {
            $scope.ShowEdit = true;
            $scope.EnableEdit = true;
            $scope.EnableEditContactPerson = true;
            $scope.mDataDetail = angular.copy($scope.mDataDetailDefault);
            // $scope.onBeforeEdit();
            // $scope.getAllParameter(DataPersonal.VehicleId);
            if ($scope.mDataDetail.ProvinceId !== undefined && $scope.mDataDetail.ProvinceId !== null) {
                WOBP.getMLocationProvince().then(function(res) {
                    console.log("List Province====>", res.data.Result);
                    $scope.ProvinceData = res.data.Result;

                    if ($scope.mDataDetail.CityRegencyId !== undefined && $scope.mDataDetail.CityRegencyId !== null) {
                        WOBP.getMLocationCityRegency($scope.mDataDetail.ProvinceId).then(function(resu) {
                            $scope.CityRegencyData = resu.data.Result;

                            if ($scope.mDataDetail.DistrictId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                WOBP.getMLocationKecamatan($scope.mDataDetail.CityRegencyId).then(function(resu) {
                                    $scope.DistrictData = resu.data.Result;

                                    if ($scope.mDataDetail.VillageId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                        WOBP.getMLocationKelurahan($scope.mDataDetail.DistrictId).then(function(resu) {
                                            $scope.VillageData = resu.data.Result;
                                        });
                                    };
                                });
                            };
                        });
                    };
                })
            };
        };

        $scope.CekPerbedaan = function() {
            console.log("$scope.mDataDetail A", $scope.mDataDetail);
            console.log("$scope.DataSebelumBerubahCRM B", $scope.DataSebelumBerubahCRM);
            $scope.datahasil = {};
            $scope.datahasilsudah = {};
            $scope.dataakhir = [];
            $scope.datasatu = [];
            $scope.datadua = [];
            var counter = 0;
            var keyofData = Object.keys($scope.DataSebelumBerubahCRM);

            for (var i in keyofData) {
                if ($scope.DataSebelumBerubahCRM[keyofData[i]] !== $scope.mDataDetail[keyofData[i]]) {
                    counter++;
                    $scope.datahasil[keyofData[i]] = $scope.mDataDetail[keyofData[i]];
                    $scope.datahasilsudah[keyofData[i]] = $scope.DataSebelumBerubahCRM[keyofData[i]];
                    // $scope.datasatu.push($scope.datahasil);
                    // $scope.datadua.push($scope.datahasilsudah);                    
                }
            }
            console.log('keyofData', keyofData);
            var fieldofData = Object.keys($scope.datahasilsudah);

            for (var i in fieldofData) {
                if ($scope.datahasilsudah[keyofData[i]] !== null) {
                    counter++;
                    $scope.datasatu.push($scope.datahasilsudah[fieldofData[i]]);

                }
            }

            for (var i = 0; i < fieldofData.length; i++) {
                var tmpDataAkhir = {};
                tmpDataAkhir.CustomerId = $scope.mDataDetail.CustomerId;
                tmpDataAkhir.ChangeTypeId = 2
                    tmpDataAkhir.Modul = 'Follow Up Appointment GR'
                    tmpDataAkhir.Attribute = fieldofData[i]
                    if (tmpDataAkhir.Attribute == "BirthDate"){
                        tmpDataAkhir.NewValue = $filter('date')($scope.datahasil[fieldofData[i]], 'yyyy-MM-dd');
                        tmpDataAkhir.OldValue = $filter('date')($scope.datahasilsudah[fieldofData[i]], 'yyyy-MM-dd');
                    } else {
                        tmpDataAkhir.NewValue = $scope.datahasil[fieldofData[i]];
                        tmpDataAkhir.OldValue = $scope.datahasilsudah[fieldofData[i]];
                        // tmpDataAkhir.NewValue = $scope.datahasilsudah[fieldofData[i]]
                        // tmpDataAkhir.OldValue = $scope.datahasil[fieldofData[i]]
                    }
                    // tmpDataAkhir.NewValue = $scope.datahasilsudah[fieldofData[i]],
                    // tmpDataAkhir.OldValue = $scope.datahasil[fieldofData[i]]
                console.log(' $scope.datadua', $scope.datadua);
                AppointmentGrService.sendToCRMHistory(tmpDataAkhir);
            }

        };

        $scope.UpdateInfoUmum = function(mDataDetail) {

            AppointmentGrService.getDataVehicle(mDataDetail.VehicleId).then(function(ress) {
                var dataDetail = ress.data.Result[0]
                $scope.DataSebelumBerubahCRM = dataDetail;
                console.log("data CRM A", $scope.DataSebelumBerubahCRM);
                $scope.CekPerbedaan();
            });

            // console.log("data sialan",DataPersonal);
            // if ($scope.mDataDetail.VehicleUserId !== undefined) {
            // AppointmentGrService.updateStatus(mDataDetail).then(function(res) {
            AppointmentBpService.putCustomerPersonal(DataPersonal, mDataDetail).then(function(res) {
                AppointmentBpService.putCustomerAddress(DataPersonal, mDataDetail, 1).then(function(res) {
                    // $scope.BatalInfoUmum();
                    console.log("save addres");
                    // console.log("$scope.mDataDetail",$scope.mDataDetail);
                    // console.log("DataPersonal",DataPersonal);
                    AppointmentGrService.putGastype($scope.mDataDetail).then(function(res) {
                        $scope.BatalInfoUmum();
                        console.log("update type Gas")
                    });
                });
            });
            // });
            // }
            // if ($scope.mDataDetail.VehicleUserId !== undefined) {
            //     AppointmentGrService.updateStatus(mDataDetail).then(
            //         function(res) {
            //             $scope.BatalInfoUmum();
            //         },
            //         function(err) {
            //             console.log("err=>", err);
            //         }
            //     );
            // }
        };
        $scope.EditInfoUmumInstitusi = function() {
            $scope.mDataDetailDefault = angular.copy($scope.mDataDetail);
            if ($scope.mDataDetail.CustomerVehicleId !== undefined) {
                $scope.getAddressList();
                console.log("$scope.mDataDetail.CustomerVehicleId", $scope.mDataDetail.CustomerVehicleId);
                AppointmentGrService.getDataContactPerson($scope.mDataDetail.CustomerVehicleId).then(function(res) {
                    // console.log("List Province====>",res.data.Result);
                    $scope.ContactPersonData = res.data.Result;
                    $scope.ShowEditInstitusi = false;
                    $scope.EnableEditInstitusi = false;
                    $scope.EnableEditContactPerson = false;
                    console.log("Data ContactPerson", $scope.ContactPersonData);
                })
            };
            // AppointmentBpService.getLocation().then(function(res) {
            //     // console.log("List Province====>",res.data.Result);
            //     $scope.ProvinceData = res.data.Result;
            //     $scope.ShowEditInstitusi = false;
            //     // $scope.EnableEditInstitusi = false;
            //     $scope.EnableEditContactPerson = false;
            //     console.log("ProvinceData", $scope.ProvinceData);
            // })
        };
        $scope.BatalInfoUmumInstitusi = function() {
            $scope.ShowEditInstitusi = true;
            $scope.EnableEditInstitusi = true;
            $scope.EnableEditContactPerson = true;
            $scope.mDataDetail = angular.copy($scope.mDataDetailDefault);
            // $scope.onBeforeEdit();
            // $scope.getAllParameter(DataPersonal.VehicleId);

            if ($scope.mDataDetail.ProvinceId !== undefined && $scope.mDataDetail.ProvinceId !== null) {
                WOBP.getMLocationProvince().then(function(res) {
                    console.log("List Province====>", res.data.Result);
                    $scope.ProvinceData = res.data.Result;

                    if ($scope.mDataDetail.CityRegencyId !== undefined && $scope.mDataDetail.CityRegencyId !== null) {
                        WOBP.getMLocationCityRegency($scope.mDataDetail.ProvinceId).then(function(resu) {
                            $scope.CityRegencyData = resu.data.Result;

                            if ($scope.mDataDetail.DistrictId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                WOBP.getMLocationKecamatan($scope.mDataDetail.CityRegencyId).then(function(resu) {
                                    $scope.DistrictData = resu.data.Result;

                                    if ($scope.mDataDetail.VillageId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                        WOBP.getMLocationKelurahan($scope.mDataDetail.DistrictId).then(function(resu) {
                                            $scope.VillageData = resu.data.Result;
                                        });
                                    };
                                });
                            };
                        });
                    };
                })
            };
        };

        $scope.UpdateInfoUmumInstitusi = function(mDataDetail) {
            // if ($scope.mDataDetail.VehicleUserId !== undefined) {
            //     // AppointmentGrService.updateStatus(mDataDetail).then(
            //     //     function(res) {
            //     //         $scope.BatalInfoUmumInstitusi();
            //     //     },
            //     //     function(err) {
            //     //         console.log("err=>", err);
            //     //     }
            //     // );
            // // }
            // AppointmentBpService.putCustomerAddress(DataPersonal, mDataDetail, 2).then(
            //     function(res) {
            //         $scope.BatalInfoUmumInstitusi();
            //         console.log("save addres")
            //     },
            //     function(err) {
            //         console.log("err=>", err);
            //     }
            // );
            // }
            if ($scope.mDataDetail.VehicleUserId !== undefined) {
                // AppointmentGrService.updateStatus(mDataDetail).then(function(res) {
                AppointmentBpService.putCustomerAddress(DataPersonal, mDataDetail, 2).then(function(res) {
                    $scope.BatalInfoUmumInstitusi();
                    console.log("save addres")
                });
                // });
            }
        }
        $scope.CustomerAddress = [];
        $scope.getAddressList = function() {
            WOBP.getMLocationProvince().then(function(res) {
                console.log("List Province====>", res.data.Result);
                // ProvinceId == row.ProvinceId
                $scope.ProvinceData = res.data.Result;
                // $scope.tempProvinsiData = angular.copy($scope.provinsiData);
            });
        };

        $scope.selectProvince = function(row) {
            console.log('row selectProvince', row);
            WOBP.getMLocationCityRegency(row.ProvinceId).then(function(resu) {
                $scope.CityRegencyData = resu.data.Result;

                $scope.mDataDetail.CityRegencyId = null
                $scope.mDataDetail.DistrictId = null
                $scope.mDataDetail.VillageId = null
                $scope.mDataDetail.PostalCode = null

                $scope.DistrictData = []
                $scope.VillageData = []
            });
            // $scope.selectedProvince = angular.copy(row);
        };
        $scope.selectRegency = function(row) {
            console.log('row selectRegency', row);
            WOBP.getMLocationKecamatan(row.CityRegencyId).then(function(resu) {
                $scope.DistrictData = resu.data.Result;

                $scope.mDataDetail.DistrictId = null
                $scope.mDataDetail.VillageId = null
                $scope.mDataDetail.PostalCode = null

                $scope.VillageData = []
            });
            // $scope.kecamatanData = row.MDistrict;
            // $scope.selectedRegency = angular.copy(row);
        };
        $scope.selectDistrict = function(row) {
            console.log('row selectDistrict', row);
            WOBP.getMLocationKelurahan(row.DistrictId).then(function(resu) {
                $scope.VillageData = resu.data.Result;

                $scope.mDataDetail.VillageId = null
                $scope.mDataDetail.PostalCode = null
            });
            // $scope.selectedDistrict = angular.copy(row);
        };
        $scope.selectVillage = function(row) {
            console.log('row selectVillage', row);
            // $scope.selecedtVillage = angular.copy(row);
            $scope.mDataDetail.PostalCode = row.PostalCode == 0 ? '-' : row.PostalCode;
        };
        $scope.dissallowSemicolon = function(event){
            var patternRegex = /[\x3B]/i;
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        }

        //Tyre
        $scope.checkStatusPartTyre = function (id, dataTask, jobtaskId){

            var tmpArray = [];

            for (var i in dataTask) {
                if (dataTask[i].JobPartsId !== undefined) {
                    tmpArray.push({
                        JobPartsId: dataTask[i].JobPartsId
                    })
                }
            }
            WoHistoryGR.getCheckDataPartTyre(id, tmpArray).then(
                function (res) {
                validasi = res.data;
                if(!validasi){
                    var idxTask = _.findIndex($scope.gridWork, { JobTaskId: jobtaskId });
                    if (idxTask > -1) {
                        for(var z in dataTask){
                            $scope.gridWork[idxTask].child.push(dataTask[z]);
                        }
                    }
                    bsAlert.warning("Parts Tyre tidak dapat dihapus,", 'karena PO tyre telah terbentuk');
                    $scope.sumAllPrice();
                }
            }, function (err) {
                var idxTask = _.findIndex($scope.gridWork, { JobTaskId: jobtaskId });
                if (idxTask > -1) {
                    for(var z in dataTask){
                        $scope.gridWork[idxTask].child.push(dataTask[z]);
                    }
                }
                $scope.sumAllPrice();
            });
        }

        $scope.ubahPartsCode = function(data){
            console.log('ubur ubur',data)
            // kata pa ari kl part yang ga pake partscode (manual), ga blh di kasi diskon pas release wo nya
            // hrs isi dr listodo dl partscode nya, baru kasi diskon di wo list
            if (data.PartsCode != null && data.PartsCode != undefined){
                if (data.PartsCode.length < 4){
                    $scope.ldModel.DiscountTypeId = -1;
                    $scope.ldModel.DiscountTypeListId = null;
                    $scope.ldModel.Discount = 0;
                    $scope.ldModel.PartsId = null; // kl mau harga nya enable waktu dia ubah partscode

                    // ========================= hilangkan tipe diskon material jika ganti part, hrs click "Cek" dl ====================== start
                    // for (var p=0; p<$scope.diskonData.length; p++){
                    //     if ($scope.diskonData[p].MasterId == 3){
                    //         $scope.diskonData.splice(p,1)
                    //     }
                    // }
                    // ========================= hilangkan tipe diskon material jika ganti part, hrs click "Cek" dl ====================== end
                }
            }
        }

        $scope.checkNameParts = function(data, model) {
            if ($scope.ldModel.IsOPB == null && data != null) {
                $scope.ldModel.RetailPrice = 0;
            } else {
                $scope.ldModel.RetailPrice = "";
            }

            if (model.MaterialTypeId == 1 && (model.PartsCode == null || model.PartsCode == undefined || model.PartsCode == '')) {
                model.OrderType = 3
                model.Type = 3

            }

        }

        // $scope.selectProvince = function(row) {
        //     $scope.CityRegencyData = row.MCityRegency;
        // }
        // $scope.selectRegency = function(row) {
        //     $scope.DistrictData = row.MDistrict;
        // }
        // $scope.selectDistrict = function(row) {
        //     $scope.VillageData = row.MVillage;
        // }
        // =======Adding Custom Filter==============
        $scope.gridCols = []; //= angular.copy(scope.grid.columnDefs);
        $scope.gridApiAppointment = {};
        var x = -1;
        for (var i = 0; i < $scope.grid.columnDefs.length; i++) {
            if ($scope.grid.columnDefs[i].visible == undefined && $scope.grid.columnDefs[i].name !== 'Action') {
                x++;
                $scope.gridCols.push($scope.grid.columnDefs[i]);
                $scope.gridCols[x].idx = i;
            }
            console.log("$scope.gridCols", $scope.gridCols);
            console.log("$scope.grid.columnDefs[i]", $scope.grid.columnDefs[i]);
        }
        $scope.filterBtnLabel = $scope.gridCols[0].name;
        $scope.filterBtnChange = function(col) {
            console.log("col", col);
            $scope.filterBtnLabel = col.name;
            $scope.filterColIdx = col.idx + 1;
        };
        // ====================================
        $scope.givePattern = function(item, event) {
            console.log("item", item);
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
                $scope.nilaiKM = item;
                if ($scope.nilaiKM.includes(",")) {
                    $scope.nilaiKM = $scope.nilaiKM.split(',');
                    if ($scope.nilaiKM.length > 1) {
                        $scope.nilaiKM = $scope.nilaiKM.join('');
                    } else {
                        $scope.nilaiKM = $scope.nilaiKM[0];
                    }
                }
                $scope.nilaiKM = parseInt($scope.nilaiKM);
                $scope.mData.Km = $scope.mData.Km.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                return;
            };
        };
    }); 
