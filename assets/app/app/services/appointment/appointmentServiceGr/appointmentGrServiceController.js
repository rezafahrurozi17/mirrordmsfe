angular.module('app')
    .controller('appointmentGrServiceController', function ($scope, $http, $q, bsNotify, ASPricingEngine, CurrentUser, FollowUpGrService, AppointmentGrService, FollowupService, $timeout, $filter, bsAlert, ngDialog, AsbGrFactory, MrsList, CMaster, AppointmentBpService, RepairSupportActivityGR, WO, WOBP, PrintRpt, VehicleHistory, MasterDiscountFactory) {

        var PPNPerc = 0;


        function escapeRegExp(str) {
            return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        };

        function replaceAmk(str, find, replacer) {
            console.log('replaceAmk', str, find, replacer);

            return str.replace(new RegExp(escapeRegExp(find), 'g'), replacer);
        };
        //----------------------------------
        // Start-Up
        //----------------------------------
        // $scope.asb = { showMode: 'main', NewChips : 1};
        //     // $scope.asb = {currentDate: new Date(), showMode: 'main', nopol: "D 1234 ABC", tipe: "Avanza"} 
        //     // console.log("$scope.currentDate",$scope.currentDate);
        // $scope.asb.endDate = new Date();
        // $scope.asb.endDate.setDate($scope.asb.endDate.getDate() + 7);
        // ========================
        var handleScroll = function(evt){
            if (!evt) evt = event;
            var direction = (evt.detail<0 || evt.wheelDelta>0) ? 1 : -1;
            // Use the value as you will
          };
        //   $('.ui-grid-viewport').on('DOMMouseScroll',handleScroll,false); // for Firefox
        //   $('.ui-grid-viewport').on('mousewheel',    handleScroll,false);
          $('.ui-grid-contents-wrapper > .ui-grid-render-container >.ng-isolate-scope >.ui-grid-render-container-body').scroll(function(){
              console.log("ihhh bener bener")
          });
        // ========================

        $scope.loadingSimpan = 0;

        $scope.disableDate = true;
        $scope.PartTersedia = true;
        $scope.undefinedPart = false;
        $scope.boardName = 'asbGrAppointment';
        $scope.startlongDay = '';
        $scope.endlongDay = '';
        $scope.disableBro = function(index) {
            $scope.disableDate = true;
            console.log('disableDate', $scope.disableDate, index);

            $scope.MRS.category = 2;
            var category = 2;
            var VIN = index;
            var startDate = '-';
            var endDate = '-';
            var param = 1;      // tab riwayat servis
            if (index != 1 && index != 0){
                $scope.ServiceHistory(category, VIN, startDate, endDate, param);
            }
        };
        $scope.DateOptionsHistory = {
            startingDay: 1,
            format: 'dd/MM/yyyy',
        };
        $scope.checkStall = function(){
            AppointmentGrService.getListStall().then(function(res){
                var xres = res.data.Result;
                var vstart = '';
                var vend = '';
                for(var i=0;  i<xres.length; i++){
                    var vitem = xres[i];
                    // === NEW CODE Before GO LIVE
                    if (!vitem.DefaultOpenTime) {
                        vitem.DefaultOpenTime = '08:00';
                    }
                    if (typeof vitem.DefaultOpenTime !== 'undefined') {

                        //cek defaultopen time menit nya 00 ga? kl ga 00 set jd 00 biar header nya menit nya 00
                        var cekDefaultOpenTime = vitem.DefaultOpenTime.split(':');
                        if (cekDefaultOpenTime[1].toString() != '00'){
                            vitem.DefaultOpenTime = cekDefaultOpenTime[0] + ':' + '00' + ':' + '00'
                        }

                        // if (vitem.DefaultOpenTime){
                        if (vstart === '') {
                            vstart = vitem.DefaultOpenTime;
                        } else {
                            if (vstart > vitem.DefaultOpenTime) {
                                vstart = vitem.DefaultOpenTime;
                            }
                        }
                        // }
                    }
                    if (vitem.DefaultCloseTime == undefined) {
                        vitem.DefaultCloseTime = '17:00';
                    }
                    if (typeof vitem.DefaultCloseTime !== 'undefined') {
                        if (vitem.DefaultCloseTime >= '23:59') {
                            vitem.DefaultCloseTime = '24:00';
                        }
                        if (vend === '') {
                            vend = vitem.DefaultCloseTime;
                        } else {
                            if (vend < vitem.DefaultCloseTime) {
                                vend = vitem.DefaultCloseTime;
                            }
                        }
                    }
                }
                if (vstart === '') {
                    vstart = '08:00';
                }
                if (vend === '') {
                    vend = '17:00';
                }
                var vxmin = JsBoard.Common.getMinute(vend);
                vend = JsBoard.Common.getTimeString(vxmin);
                // me.startHour = vstart;
                $scope.startlongDay = vstart;
                $scope.endlongDay = vend;
            });
        }
        $scope.mDataVehicleHistory = [];
        $scope.change = function (startDate, endDate, category, vin) {

            console.log("start Date 1", startDate);
            console.log("start Date 2", endDate);
            category = category.toString();
            var copyAngular = angular.copy(category);
            console.log("category", category);
            console.log("Copycategory", copyAngular);
            console.log("vin", vin);
            if (category == "" || category == null || category == undefined) {
                bsNotify.show({
                    size: 'big',
                    title: 'Mohon Isi Kategori Pekerjaan',
                    text: 'I will close in 2 seconds.',
                    timeout: 2000,
                    type: 'danger'
                });
            } else {

                if (startDate == undefined || startDate == "" || startDate == null || startDate == "Invalid Date") {
                    bsNotify.show({
                        size: 'big',
                        title: 'Mohon Isi periode servis',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                } else {

                    var tmpDate = startDate;
                    var tmpMonth = startDate;
                    console.log("tmpMonth", tmpMonth);
                    var tmpTime = $scope.startDate;
                    // if (tmpDate.getMonth() < 10) {
                    // tmpMonth = "0" + (tmpDate.getMonth() + 1);
                    // } else {
                    tmpMonth = (tmpDate.getMonth() + 1).toString();
                    // }
                    var ini = tmpDate.getFullYear() + "-" + (tmpMonth[1] ? tmpMonth : "0" + tmpMonth[0]) + "-" + tmpDate.getDate();
                    var d = new Date(ini);
                    console.log('data yang diinginkan', ini);
                    startDate = ini;
                };


                if (endDate == undefined || endDate == "" || endDate == null || endDate == "Invalid Date") {
                    bsNotify.show({
                        size: 'big',
                        title: 'Mohon Isi periode servis',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                } else {

                    var tmpDate2 = endDate;
                    var tmpMonth2 = endDate;
                    console.log("tmpMonth2", tmpMonth2);
                    var tmpTime2 = endDate;

                    // if (tmpDate2.getMonth() < 10) {
                    //     tmpMonth2 = "0" + (tmpDate2.getMonth() + 1);
                    // } else {
                    tmpMonth2 = (tmpDate2.getMonth() + 1).toString();
                    // }
                    var ini2 = tmpDate2.getFullYear() + "-" + (tmpMonth2[1] ? tmpMonth2 : "0" + tmpMonth2[0]) + "-" + tmpDate2.getDate();
                    var d2 = new Date(ini2);
                    console.log('data yang diinginkan 2', ini2);
                    endDate = ini2;
                };

                if (typeof vin !== "undefined") {
                    category = parseInt(category);
                    $scope.ServiceHistory(category, vin, startDate, endDate);
                } else {
                    bsNotify.show({
                        size: 'big',
                        title: 'VIN Tidak Ditemukan',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                };
            };
        };

        $scope.getUnique = function (array){
            var uniqueArray = [];
            
            // Loop through array values
            for(i=0; i < array.length; i++){
                if(uniqueArray.indexOf(array[i]) === -1) {
                    uniqueArray.push(array[i]);
                }
            }
            return uniqueArray;
        }

        $scope.ServiceHistory = function (category, vin, startDate, endDate, param) {
            VehicleHistory.getData(category, vin, startDate, endDate).then(
                function (res) {
                    if (res.data.length == 0) {
                        $scope.mDataVehicleHistory = [];
                        bsNotify.show({
                            size: 'big',
                            title: 'Data Riwayat Service Tidak Ditemukan',
                            text: 'I will close in 2 seconds.',
                            timeout: 2000,
                            type: 'danger'
                        });
                    } else {
                        res.data.sort(function (a, b) {
                            // Turn your strings into dates, and then subtract them
                            // to get a value that is either negative, positive, or zero.
                            return new Date(b.ServiceDate) - new Date(a.ServiceDate);
                        });
                        for(var i in res.data){
                            var tmpArray = [];
                            for(var j = 0; j<res.data[i].VehicleJobService.length; j++){
                                if(res.data[i].VehicleJobService[j].KTPNo == null || res.data[i].VehicleJobService[j].KTPNo == undefined){
                                    res.data[i].VehicleJobService[j].KTPNo = ''
                                }
                                if(!(res.data[i].VehicleJobService[j].KTPNo.includes('/'))){
                                    if(res.data[i].VehicleJobService[j].EmployeeName !== null){
                                        tmpArray.push(res.data[i].VehicleJobService[j].EmployeeName);
                                    }
                                }
                                
                            }
                            tmpArray = $scope.getUnique(tmpArray);
                            res.data[i].namaTeknisi = tmpArray.join(', ');
                        }
                        $scope.mDataVehicleHistory = res.data;
                        console.log("kesini ga?");
                        $scope.value = angular.copy($scope.mDataVehicleHistory);

                        $scope.MRS.category = category;
                        
                        if (param == 1) {       // tab riwayat servis

                            var dateEnd = $scope.mDataVehicleHistory[0].ServiceDate;
                            $scope.MRS.endDate = dateEnd;

                            var terakhir = $scope.mDataVehicleHistory.length;
                            $scope.MRS.startDate = $scope.mDataVehicleHistory[terakhir - 1].ServiceDate;

                        }
                        console.log('category', $scope.MRS.category);
                        console.log('startDate', $scope.MRS.startDate);
                        console.log('EndDate', $scope.MRS.endDate);
                    };

                    console.log("mDataVehicleHistory=> Followup ", $scope.mDataVehicleHistory);
                },
                function (err) { }
            );
        }

        $scope.activeTab = function(nahh) {
            console.log("nammhh", nahh);
        }
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.altInputFormats = ['M!/d!/yyyy'];

        var currentDate = new Date();
        var allBoardHeight = 500;
        currentDate.setDate(currentDate.getDate() + 1);
        $scope.minDateASB = new Date(currentDate);

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            console.log(" Last Push Appointment 2017-09-22 13:20:00");
            //showBoardAsbGr(vobj);
            $scope.filter.Type = 'Langsung';
            $scope.checkStall();
            $scope.getPPN()
        });

        $scope.getPPN = function (noNpwp) {
            if (noNpwp !== undefined && noNpwp !== null && noNpwp !== ''){
                noNpwp = noNpwp.toString();
                if (noNpwp.length === 20){
                    if (noNpwp !== '00.000.000.0-000.000'){
                        ASPricingEngine.getPPN(1, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    } else {
                        ASPricingEngine.getPPN(0, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    }
                } else {
                    ASPricingEngine.getPPN(0, 3).then(function (res) {
                        PPNPerc = res.data
                    });
                }

            } else {
                // param 1 adalah isnpwp, param 2 tipe ppn
                ASPricingEngine.getPPN(0, 3).then(function (res) {
                    PPNPerc = res.data
                });
            }
            
        };

        $scope.model_info_km = {}
        $scope.model_info_km.show = false;
        $scope.viewButtoninfokm = {save:{text:'Ok'}};

        $scope.Saveinfokm = function(){
            $scope.model_info_km.show = false;
        }
        $scope.Cancelinfokm = function(){
            $scope.model_info_km.show = false;
        }
        $scope.show_km_info = function() {
            $scope.model_info_km.show = true;
        }


        var showBoardAsbGr = function(item) {
            console.log("item item", item);
            var vtime
            var tmphour
            var tmpMinute
            if (item !== undefined && item !== null) {
                vtime = item;
                tmphour = item.getHours();
                tmpMinute = item.getMinutes();
            } else {
                vtime = new Date();
                tmphour = vtime.getHours();
                tmpMinute = vtime.getMinutes();
            }

            vtime.setHours(tmphour);
            vtime.setMinutes(tmpMinute);
            vtime.setSeconds(0);
            console.log("Show Board ASB vtime", vtime);
            $scope.currentItem = {
                mode: 'main',
                preDiagnose: tmpPrediagnose,
                currentDate: $scope.currentDate,
                nopol: $scope.mDataDetail.LicensePlate,
                tipe: $scope.mDataDetail.VehicleModelName,
                durasi: $scope.tmpActRate,
                stallId: $scope.stallAsb,
                startTime: vtime,
                visible: $scope.visibleAsb,
                asbStatus: $scope.asb,
                longStart: $scope.startlongDay,
                longEnd:$scope.endlongDay,
                // JobId: $scope.mData.JobId,
                /*stallId :1,
                startDate :,
                endDate:,
                durasi:,*/
            }
            console.log("actRe", $scope.tmpActRate);
            console.log("harusnya tanggal", $scope.asb.startDate);
            console.log("ini vtime", vtime);
            console.log("Status Visible ASB", $scope.visibleAsb);
            $timeout(function() {
                AsbGrFactory.showBoard({ boardName: $scope.boardName, container: 'asb_asbgr_view', currentChip: $scope.currentItem, onPlot: getDateEstimasi });

            }, 100);
        };
        $scope.reloadBoard = function(item) {
            $scope.asbDate = item;


            if($scope.asbDate > $scope.DiskonTaskEndDate){
                console.log('Tidak Masuk periode diskon booking Task');
                $scope.DiscountTask = 0;
                $scope.mData.disTask = 0;
            }else{
                console.log('masuk periode diskon booking Task');
            }
            if($scope.asbDate > $scope.DiskonPartsEndDate){
                console.log('Tidak Masuk periode diskon booking Parts');
                $scope.DiscountParts = 0;
                $scope.mData.disParts = 0;
            }else{
                console.log('masuk periode diskon booking Parts');
            }


            var tmpMinDateASB = new Date();
            tmpMinDateASB.setHours(0, 0, 0);
            if (item < tmpMinDateASB.getTime()) {
                console.log("ini nih bossss", tmpMinDateASB);
                console.log("item ini nih", item);
            } else {
                $scope.mData.PlanStart = null;
                $scope.mData.PlanFinish = null;
                $scope.mData.PlanDateStart = null;
                $scope.mData.AppointmentDate = null;
                $scope.mData.AppointmentTime = null;
                $scope.mData.TargetDateAppointment = null;
                $scope.mData.FixedDeliveryTime1 = null;
                $scope.mData.PlanDateFinish = null;
                console.log("dari ng-change", item);
                console.log("$scope.mData.FullDate", $scope.mData.FullDate);
                var vtime = item;
                var tmphour
                var tmpMinute
                if ($scope.mData.FullDate !== undefined && $scope.mData.FullDate !== null) {
                    console.log("$scope.mData.FullDate", $scope.mData.FullDate);
                    var tmpTime = $scope.mData.FullDate;
                    tmphour = tmpTime.getHours();
                    tmpMinute = tmpTime.getMinutes();
                } else {
                    tmphour = 9;
                    tmpMinute = 0;
                }
                vtime.setHours(tmphour);
                vtime.setMinutes(tmpMinute);
                vtime.setSeconds(0);
                $scope.currentItem = {
                    mode: 'main',
                    currentDate: $scope.asb.startDate,
                    nopol: $scope.mDataDetail.LicensePlate,
                    tipe: $scope.mDataDetail.VehicleModelName,
                    durasi: $scope.tmpActRate,
                    stallId: 0,
                    startTime: vtime,
                    visible: $scope.visibleAsb,
                    preDiagnose: tmpPrediagnose,
                    asbStatus: $scope.asb,
                    longStart: $scope.startlongDay,
                    longEnd:$scope.endlongDay,
                    // JobId: $scope.mData.JobId,
                    /*stallId :1,
                    startDate :,
                    endDate:,
                    durasi:,*/
                }
                console.log("actRe", $scope.tmpActRate);
                console.log("harusnya tanggal", $scope.asb.startDate);
                console.log("ini vtime", vtime);
                console.log("stallId", $scope.currentItem.stallId);
                $timeout(function() {
                    AsbGrFactory.showBoard({ boardName: $scope.boardName, container: 'asb_asbgr_view', currentChip: $scope.currentItem, onPlot: getDateEstimasi });

                }, 100);
            }
        };
        var getDateEstimasi = function(data) {
            console.log('onplot data...', data);
            if ((data.startTime === null) || (data.endTime === null)) {
                // jika startTime/EndTime null berarti belum ada plot, Data tanggal awal/akhir serta Tanggal Appointment harus dikosongkan maka harus dikosongkan
                if ((data.PrediagScheduledTime !== null && data.PrediagStallId !== 0) && $scope.gridWork.length == 0) {
                    console.log("Plotting CUMA PREDIAGNOSE");
                    var dt = new Date(data.PrediagScheduledTime);
                    var dtt = new Date(data.PrediagScheduledTime);
                    dt.setSeconds(0);
                    dtt.setSeconds(0);
                    var timeStart = dt.toTimeString();
                    var timeFinish = dtt.toTimeString();
                    timeStart = timeStart.split(' ');
                    var finaltimeFinish = timeFinish.split(' ');
                    finaltimeFinish = finaltimeFinish[0].split(':');
                    finaltimeFinish = (parseInt(finaltimeFinish[0]) + 1).toString() + ":" + finaltimeFinish[1] + ":" + finaltimeFinish[2]
                        // =============================
                    var hourFinish = dtt.getHours();
                    var minuteFinish = dtt.getMinutes();
                    var minuteFinishplus = minuteFinish + 30;
                    var hourFinishplus = hourFinish + 1;
                    if (minuteFinishplus > 59) {
                        hourFinishplus = hourFinish + 1;
                        minuteFinishplus = minuteFinishplus - 60;
                    }
                    if (dtt.getHours() == 12) {
                        hourFinishplus = hourFinish + 1;
                    }
                    var finaltimeFinishDelivery = (hourFinishplus > 9 ? hourFinishplus : "0" + hourFinishplus) + ":" + (minuteFinishplus == 0 ? "00" : minuteFinishplus) + ":00";

                    console.log("timeStart", timeStart);
                    console.log("finaltimeFinish", finaltimeFinish);
                    console.log("finaltimeFinishDelivery", finaltimeFinishDelivery);
                    var str = angular.copy(finaltimeFinishDelivery);
                    var resTimeTmp = str.slice(0, 5);
                    console.log('resTimeTmp', resTimeTmp);
                    $scope.mData.FixedTmp = resTimeTmp;
                    // ===============================
                    var yearFirst = dt.getFullYear();
                    var monthFirst = dt.getMonth() + 1;
                    var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                    var dayFirst = dt.getDate();
                    var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                    var firstDate = yearFirst + '/' + monthFirsts + '/' + dayFirsts;
                    var firstDateTemp = dayFirsts + '/' + monthFirsts + '/' + yearFirst;
                    $scope.mData.AppointmentDateTmp = firstDateTemp;
                    // ===============================
                    var yearFinish = dtt.getFullYear();
                    var monthFinish = dtt.getMonth() + 1;
                    var monthFinishs = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                    var dayFinish = dtt.getDate();
                    var dayFinishs = dayFinish < 10 ? '0' + dayFinish : dayFinish;
                    var FinishDate = yearFirst + '/' + monthFirsts + '/' + dayFirsts;
                    var FinishDateTemp = dayFirsts + '/' + monthFirsts + '/' + yearFirst;
                    $scope.mData.TargetDateAppointmentTemp = FinishDateTemp;
                    // ===============================
                    // if(timeStart[0] !== null && timeFinish[0] !== null){
                    $scope.mData.StallId = data.PrediagStallId;
                    $scope.mData.FullDate = dt;
                    $scope.mData.PlanStart = timeStart[0];
                    $scope.mData.PlanFinish = finaltimeFinish;
                    $scope.mData.PlanDateStart = firstDate;
                    $scope.mData.AppointmentDate = firstDate;
                    $scope.mData.AppointmentTime = timeStart[0];
                    console.log("timeStart[0]", timeStart[0]);
                    var strTmp = angular.copy(timeStart[0]);
                    var resTimeJam = strTmp.slice(0, 5);
                    $scope.mData.AppointmentTimeTmp = resTimeJam;
                    $scope.mData.TargetDateAppointment = FinishDate;
                    $scope.mData.FixedDeliveryTime1 = finaltimeFinishDelivery;
                    $scope.mData.PlanDateFinish = FinishDate;
                    $scope.mData.StatusPreDiagnose = data.StatusPreDiagnose;
                    $scope.mData.PrediagScheduledTime = data.PrediagScheduledTime;
                    $scope.mData.PrediagStallId = data.PrediagStallId;
                } else {
                    console.log("CLEAR VALUE PLOTTING");
                    $scope.mData.StallId = 0;
                    $scope.mData.FullDate = null;
                    $scope.mData.PlanStart = null;
                    $scope.mData.PlanFinish = null;
                    $scope.mData.PlanDateStart = null;
                    //$scope.mData.AppointmentDate = null; 
                    delete $scope.mData.AppointmentDate;
                    //$scope.mData.AppointmentTime = null;
                    delete $scope.mData.AppointmentTime;
                    $scope.mData.TargetDateAppointment = null;
                    $scope.mData.FixedDeliveryTime1 = null;
                    $scope.mData.PlanDateFinish = null;
                    $scope.mData.StatusPreDiagnose = 0;
                    delete $scope.mData.PrediagScheduledTime;
                    delete $scope.mData.PrediagStallId;
                }
                // delete tmpPrediagnose.ScheduledTime;
                // delete tmpPrediagnose.StallId;
            } else {
                if ((data.PrediagStallId !== undefined && data.PrediagScheduledTime !== null) && $scope.gridWork.length == 0) {
                    var dt = new Date(data.PrediagScheduledTime);
                    var dtt = new Date(data.PrediagScheduledTime);
                    dt.setSeconds(0);
                    dtt.setSeconds(0);
                    var timeStart = dt.toTimeString();
                    var timeFinish = dtt.toTimeString();
                    timeStart = timeStart.split(' ');
                    var finaltimeFinish = timeFinish.split(' ');
                    finaltimeFinish = finaltimeFinish[0].split(':')
                    finaltimeFinish = (parseInt(finaltimeFinish[0]) + 1).toString() + ":" + finaltimeFinish[1] + ":" + finaltimeFinish[2];
                    console.log("timeStart", timeStart);
                    console.log("timeFinish", finaltimeFinish);
                    // =====
                    var hourFinish = dtt.getHours();
                    var minuteFinish = dtt.getMinutes();
                    var minuteFinishplus = minuteFinish + 30;
                    var hourFinishplus = hourFinish + 1;
                    if (minuteFinishplus > 59) {
                        hourFinishplus = hourFinish + 1;
                        minuteFinishplus = minuteFinishplus - 60;
                    }
                    if (dtt.getHours() == 12) {
                        hourFinishplus = hourFinish + 1;
                    }
                    // var finaltimeFinishDelivery = (hourFinishplus > 9 ? hourFinishplus : "0" + hourFinishplus) + ":" + minuteFinishplus + ":00";
                    var finaltimeFinishDelivery = (hourFinishplus > 9 ? hourFinishplus : "0" + hourFinishplus) + ":" + (minuteFinishplus == 0 ? "00" : minuteFinishplus) + ":00";

                    console.log("finaltimeFinishDelivery", finaltimeFinishDelivery);
                    var str = angular.copy(finaltimeFinishDelivery);
                    var resTimeTmp = str.slice(0, 5);
                    console.log('resTimeTmp', resTimeTmp);
                    $scope.mData.FixedTmp = resTimeTmp;
                    // ===============================
                    var yearFirst = dt.getFullYear();
                    var monthFirst = dt.getMonth() + 1;
                    var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                    var dayFirst = dt.getDate();
                    var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                    var firstDate = yearFirst + '/' + monthFirsts + '/' + dayFirsts;
                    var firstDateTemp = dayFirsts + '/' + monthFirsts + '/' + yearFirst;
                    // var yearFirst = dt.getFullYear();
                    // var monthFirst = dt.getMonth() + 1;
                    // var dayFirst = dt.getDate();
                    // var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                    // var firstDateTemp = dayFirst + '-' + monthFirst + '-' + yearFirst;
                    $scope.mData.AppointmentDateTmp = firstDateTemp;
                    firstDate = $scope.changeFormatDate(dt);
                    // ===============================
                    var yearFinish = dtt.getFullYear();
                    var monthFinish = dtt.getMonth() + 1;
                    var monthFinishs = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                    var dayFinish = dtt.getDate();
                    var dayFinishs = dayFinish < 10 ? '0' + dayFinish : dayFinish;
                    var firstDate = yearFirst + '/' + monthFirsts + '/' + dayFirsts;
                    var firstDateTemp = dayFirsts + '/' + monthFirsts + '/' + yearFirst;
                    // var yearFinish = dtt.getFullYear();
                    // var monthFinish = dtt.getMonth() + 1;
                    // var dayFinish = dtt.getDate();
                    // var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                    // var firstDateTemp = dayFirst + '-' + monthFirst + '-' + yearFirst;
                    $scope.mData.TargetDateAppointmentTemp = FinishDateTemp;
                    FinishDate = $scope.changeFormatDate(dtt);
                    // ===============================
                    // if(timeStart[0] !== null && timeFinish[0] !== null){
                    $scope.mData.StallId = data.PrediagStallId;
                    $scope.mData.FullDate = dt;
                    $scope.mData.PlanStart = timeStart[0];
                    $scope.mData.PlanFinish = finaltimeFinish;
                    $scope.mData.PlanDateStart = firstDate;
                    $scope.mData.AppointmentDate = firstDate;
                    $scope.mData.AppointmentTime = timeStart[0];
                    console.log("timeStart[0]", timeStart[0]);
                    var strTmp = angular.copy(timeStart[0]);
                    var resTimeJam = strTmp.slice(0, 5);
                    $scope.mData.AppointmentTimeTmp = resTimeJam;
                    $scope.mData.TargetDateAppointment = FinishDate;
                    $scope.mData.FixedDeliveryTime1 = finaltimeFinishDelivery;
                    $scope.mData.PlanDateFinish = FinishDate;
                    $scope.mData.StatusPreDiagnose = data.StatusPreDiagnose;
                    $scope.mData.PrediagScheduledTime = data.PrediagScheduledTime;
                    tmpPrediagnose.ScheduledTime = data.PrediagScheduledTime;
                    $scope.mData.PrediagStallId = data.PrediagStallId;
                } else {
                    var info = AsbGrFactory.getInfoIsSplit($scope.boardName);
                    var dataSplitChip = AsbGrFactory.getChipData($scope.boardName);
                    console.log("PLOTING CAMPURAN");
                    if (info.sisaDurasi == 0) {
                        dataSplitChip = dataSplitChip.split(';');
                        console.log("datasplithchip", dataSplitChip);
                        if (dataSplitChip.length > 1) {
                            // var idxLastDataSplitChip = dataSplitChip[dataSplitChip.length - 1];
                            var tmpFirstDataSplitChip = dataSplitChip[0].split('|');
                            var tmpLastDataSplitChip = dataSplitChip[dataSplitChip.length - 1].split('|');
                            var dateLastSplitChip = new Date(tmpLastDataSplitChip[0]);
                            var timeLastSplitchip = tmpLastDataSplitChip[2];
                            timeLastSplitchip = timeLastSplitchip.split(":");

                            var menitPlus = parseInt(timeLastSplitchip[1]) + 30;
                            var jamPlus = parseInt(timeLastSplitchip[0]);
                            if (menitPlus > 59) {
                                jamPlus = jamPlus + 1;
                                menitPlus = menitPlus - 60;
                            }
                            if (jamPlus == 12) {
                                jamPlus = jamPlus + 1;
                            }
                            var dateStart1 = new Date(tmpFirstDataSplitChip[0] + " " + tmpFirstDataSplitChip[1]);
                            var dateFinish1 = new Date(tmpFirstDataSplitChip[0] + " " + tmpFirstDataSplitChip[2]);
                            var dateStart2 = new Date(tmpLastDataSplitChip[0] + " " + tmpLastDataSplitChip[1]);
                            var dateFinish2 = new Date(tmpLastDataSplitChip[0] + " " + tmpLastDataSplitChip[2]);

                            var finaltimeFinishDelivery = (jamPlus > 9 ? jamPlus : "0" + jamPlus) + ":" + (menitPlus == 0 ? "00" : menitPlus) + ":00";
                            console.log("menitPlus === > ", menitPlus);
                            console.log("jamPlus === > ", jamPlus);
                            console.log("dateStart1 === > ", dateStart1);
                            $scope.mData.AppointmentDateTmp = $scope.changeFormatDate(dateStart1);
                            $scope.mData.TargetDateAppointmentTemp = $scope.changeFormatDate(dateFinish2);
                            $scope.mData.FixedTmp = finaltimeFinishDelivery;
                            $scope.mData.StallId = tmpFirstDataSplitChip[3];
                            $scope.mData.FullDate = new Date(dateStart1);
                            $scope.mData.PlanStart = tmpFirstDataSplitChip[1];
                            $scope.mData.PlanFinish = tmpFirstDataSplitChip[2];
                            $scope.mData.PlanDateStart = $scope.changeFormatDate(dateStart1);
                            $scope.mData.PlanDateFinish = $scope.changeFormatDate(dateFinish2);
                            $scope.mData.AppointmentDate = $scope.changeFormatDate(dateStart1);
                            $scope.mData.AppointmentTime = tmpFirstDataSplitChip[1]
                            var strTmp = angular.copy(tmpFirstDataSplitChip[1]);
                            var resTimeJam = strTmp.slice(0, 5);
                            $scope.mData.AppointmentTimeTmp = resTimeJam;
                            $scope.mData.TargetDateAppointment = $scope.changeFormatDate(dateFinish2);
                            $scope.mData.FixedDeliveryTime1 = finaltimeFinishDelivery;
                            $scope.mData.StatusPreDiagnose = data.StatusPreDiagnose;
                            $scope.mData.PrediagScheduledTime = data.PrediagScheduledTime;
                            $scope.mData.PrediagStallId = data.PrediagStallId;
                            tmpPrediagnose.ScheduledTime = data.PrediagScheduledTime;
                            console.log("TargetDateAppointment =======>",$scope.mData.TargetDateAppointment);
                            console.log("PlanDateFinish =======>",$scope.mData.PlanDateFinish);
                            console.log("dateFinish1 =======>",dateFinish1);

                        } else {
                            console.log("plotting stall");
                            var dt = new Date(data.startTime);
                            var dtt = new Date(data.endTime);
                            console.log("date", dt);
                            dt.setSeconds(0);
                            dtt.setSeconds(0);
                            var timeStart = dt.toTimeString();
                            var timeFinish = dtt.toTimeString();
                            timeStart = timeStart.split(' ');
                            timeFinish = timeFinish.split(' ');

                            var hourFinish = dtt.getHours();
                            var minuteFinish = dtt.getMinutes();
                            var minuteFinishplus = minuteFinish + 30;
                            var hourFinishplus = hourFinish;
                            if (minuteFinishplus > 59) {
                                hourFinishplus = hourFinish + 1;
                                minuteFinishplus = minuteFinishplus - 60;
                            }
                            if (dtt.getHours() == 12) {
                                hourFinishplus = hourFinish + 1;
                            }
                            // var finaltimeFinishDelivery = (hourFinishplus > 9 ? hourFinishplus : "0" + hourFinishplus) + ":" + minuteFinishplus + ":00";
                            var finaltimeFinishDelivery = (hourFinishplus > 9 ? hourFinishplus : "0" + hourFinishplus) + ":" + (minuteFinishplus == 0 ? "00" : minuteFinishplus) + ":00";

                            console.log("finaltimeFinishDelivery", finaltimeFinishDelivery);
                            var str = angular.copy(finaltimeFinishDelivery);
                            var resTimeTmp = str.slice(0, 5);
                            console.log('resTimeTmp', resTimeTmp);
                            $scope.mData.FixedTmp = resTimeTmp;
                            // ===============================
                            var yearFirst = dt.getFullYear();
                            var monthFirst = dt.getMonth() + 1;
                            var monthFirsts = monthFirst < 10 ? '0' + monthFirst : monthFirst;
                            var dayFirst = dt.getDate();
                            var dayFirsts = dayFirst < 10 ? '0' + dayFirst : dayFirst;
                            var firstDate = yearFirst + '/' + monthFirsts + '/' + dayFirsts;
                            var firstDateTemp = dayFirsts + '/' + monthFirsts + '/' + yearFirst;
                            // var yearFirst = dt.getFullYear();
                            // var monthFirst = dt.getMonth() + 1;
                            // var dayFirst = dt.getDate();
                            // var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                            // var firstDateTemp = dayFirst + '-' + monthFirst + '-' + yearFirst;
                            $scope.mData.AppointmentDateTmp = firstDateTemp;
                            firstDate = $scope.changeFormatDate(dt);
                            // ===============================
                            var yearFinish = dtt.getFullYear();
                            var monthFinish = dtt.getMonth() + 1;
                            var monthFinishs = monthFinish < 10 ? '0' + monthFinish : monthFinish;
                            var dayFinish = dtt.getDate();
                            var dayFinishs = dayFinish < 10 ? '0' + dayFinish : dayFinish;
                            var firstDate = yearFirst + '/' + monthFirsts + '/' + dayFirsts;
                            var firstDateTemp = dayFirsts + '/' + monthFirsts + '/' + yearFirst;
                            var FinishDateTemp = dayFinishs + '/' + monthFinishs + '/' + yearFinish;
                            // var yearFinish = dtt.getFullYear();
                            // var monthFinish = dtt.getMonth() + 1;
                            // var dayFinish = dtt.getDate();
                            // var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                            // var firstDateTemp = dayFirst + '-' + monthFirst + '-' + yearFirst;
                            $scope.mData.TargetDateAppointmentTemp = FinishDateTemp;
                            FinishDate = $scope.changeFormatDate(dtt);
                            // ===============================
                            // if(timeStart[0] !== null && timeFinish[0] !== null){
                            $scope.mData.StallId = data.stallId;
                            $scope.mData.FullDate = dt;
                            $scope.mData.PlanStart = timeStart[0];
                            $scope.mData.PlanFinish = timeFinish[0];
                            $scope.mData.PlanDateStart = firstDate;
                            $scope.mData.AppointmentDate = firstDate;
                            $scope.mData.AppointmentTime = timeStart[0];
                            console.log("timeStart[0]", timeStart[0]);
                            var strTmp = angular.copy(timeStart[0]);
                            var resTimeJam = strTmp.slice(0, 5);
                            $scope.mData.AppointmentTimeTmp = resTimeJam;
                            $scope.mData.TargetDateAppointment = FinishDate;
                            $scope.mData.FixedDeliveryTime1 = finaltimeFinishDelivery;
                            $scope.mData.PlanDateFinish = FinishDate;
                            $scope.mData.StatusPreDiagnose = data.StatusPreDiagnose;
                            $scope.mData.PrediagScheduledTime = data.PrediagScheduledTime;
                            $scope.mData.PrediagStallId = data.PrediagStallId;
                            tmpPrediagnose.ScheduledTime = data.PrediagScheduledTime;
                            // tmpPrediagnose.ScheduledTime = data.PrediagScheduledTime;
                            // tmpPrediagnose.StallId = data.PrediagStallId;
                            console.log('$scope.mData.TargetDateAppointment', $scope.mData.TargetDateAppointment, $scope.TargetDateAppointmentTemp);
                        }
                    }
                }
            }

            // }

            // }
        };
        var det = new Date("October 13, 2014 11:13:00");
        var appd = new Date("December 12, 2015 11:13:00");
        var tmpPrediagnose = {};
        // var res=[{ 
        //           ToyotaId:12345678, 
        //           NoPolice:"B406DS", 
        //           Name:"John", 
        //           Address:"Jln Boulevard", 
        //           Telp:"021-987865", 
        //           Model:"Avanza", 
        //           Colour:"Hitam", 
        //           DateBuild:det, 
        //           RequestAppointmentDate:appd, 
        //           Source:"MRS" 
        //         }]
        var Parts = [];
        $scope.user = CurrentUser.user();
        $scope.mData = null; //Model
        $scope.xAppoint = {};
        $scope.xAppoint.selected = [];
        $scope.filter = {};
        $scope.noRef = 0;
        $scope.edit = false;
        $scope.show_modal = { show: false, email: false };
        $scope.showModalReason = false;
        $scope.modalMode = 'new';
        $scope.activeJustified = 0;
        $scope.ComplaintCategory = [
            { ComplaintCatg: 'Body' },
            { ComplaintCatg: 'Body Electrical' },
            { ComplaintCatg: 'Brake' },
            { ComplaintCatg: 'Chassis' },
            { ComplaintCatg: 'Drive Train' },
            { ComplaintCatg: 'Engine' },
            { ComplaintCatg: 'Heater System & AC' },
            { ComplaintCatg: 'Restraint' },
            { ComplaintCatg: 'Steering' },
            { ComplaintCatg: 'Suspension and Axle' },
            { ComplaintCatg: 'Transmission' },
        ];
        $scope.VehicleGasType = [
            { VehicleGasTypeId: 1, VehicleGasTypeName: "Bensin" },
            { VehicleGasTypeId: 2, VehicleGasTypeName: "Diesel" },
            { VehicleGasTypeId: 3, VehicleGasTypeName: "Hybrid" }
        ];
        $scope.jobAvailable = false;
        $scope.partsAvailable = false;
        $scope.FlatRateAvailable = false;
        $scope.PriceAvailable = false;
        $scope.JobRequest = [];
        $scope.JobComplaint = [];
        $scope.gridWork = [];
        $scope.listButtonSettings = { new: { enable: true, icon: "fa fa-fw fa-car" } };
        $scope.listCustomButtonSettings = {
            enable: true,
            icon: "fa fa-fw fa-car",
            func: function(row) {
                console.log("customButton", row);
            }
        };
        $scope.listApi = {};
        $scope.listRequestSelectedRows = [];
        $scope.listComplaintSelectedRows = [];
        $scope.listJoblistSelectedRows = [];
        $scope.lmComplaint = {};
        $scope.lmRequest = {};
        $scope.lmModel = {};
        $scope.ldModel = {};
        $scope.visibleAsb = 0;
        $scope.stallAsb = 0;
        $scope.typeMData = [{ Id: 1, Name: "1" }, { Id: 2, Name: "2" }, { Id: 3, Name: "3" }, { Id: 6, Name: "T" }]
        $scope.ismeridian = false;
        $scope.hstep = 1;
        $scope.mstep = 1;
        $scope.formApi = {};
        $scope.checkAsb = false;
        $scope.typeSearch = [{ Id: 0, Name: "No. Polisi" }, { Id: 1, Name: "Toyota ID" }];
        $scope.filter.typeSearch = 0;
        $scope.totalWork = 0;
        $scope.totalMaterialDiscounted = 0;
        $scope.subTotalMaterialSummary = 0;
        $scope.totalWorkDiscounted = 0;
        $scope.subTotalWorklSummary = 0;
        $scope.totalPPN = 0;
        $scope.totalEstimasi =0;
        $scope.costMaterial = 0;
        $scope.summaryJoblist = 0;
        $scope.totalDp = 0;
        $scope.totalDpDefault = 0;
        $scope.vehicleStatus = 0;
        $scope.totalMaterial = 0;
        // $scope.DiscountTask = 0;
        // $scope.DiscountParts = 0;
        $scope.tmpServiceRate = {};
        $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
        // $scope.mData.PlanStart = null;
        // $scope.mData.PlanFinish = null;
        // $scope.mData.PlanDateStart = null;
        // $scope.mData.AppointmentTime = null;
        // $scope.mData.FixedDeliveryTime1 = null;
        // $scope.mData.PlanDateFinish =null;
        //----------------------------------
        // Get Data
        //----------------------------------
        // Get Parameter Global Master
        var DataPersonal = {};

        $scope.dateOptionsBirth = {
            startingDay: 1,
            format: 'dd/MM/yyyy'
        }
        $scope.maxDateOption = {
            maxDate: new Date()
        }
        $scope.getAllParameter = function(item) {
            console.log("itemny", item);
            DataPersonal = item;
            AppointmentGrService.getPayment().then(function(res) {
                var tmpPayment = [];
                var tmpPaymentPart = [];
                var tmpPaymentIsNotWarranty = [];
                var tmpPaymentThereIstWarranty = [];
                _.map(res.data.Result,function(val){
                    if(val.Name !== 'Insurance'){
                        tmpPayment.push(val);
                        if (val.Name !== 'Warranty') {
                          tmpPaymentPart.push(val);
                        }else {
                          tmpPaymentThereIstWarranty.push(val);
                        }
                        if(val.Name !== 'Insurance' && val.Name !== 'Warranty'){
                            tmpPaymentIsNotWarranty.push(val);
                        }
                    }
                })
                $scope.paymentData = tmpPayment;
                $scope.paymentDataPart = tmpPaymentPart;
                $scope.paymentDataIsNotWarranty = tmpPaymentIsNotWarranty;
                $scope.paymentDataThereIstWarranty = tmpPaymentThereIstWarranty;
                console.log('gaada warratny', $scope.paymentDataIsNotWarranty);
                console.log('payment part', $scope.paymentDataPart);
                console.log('payment ada warranty', $scope.paymentDataThereIstWarranty);
                console.log('payment gaada warranty', $scope.paymentDataIsNotWarranty);

                //validasi variable paymentData tanpa warranty 
                var tmpIdx = _.findIndex($scope.paymentData, { 'Name': 'Warranty' });
                $scope.somePaymentData = angular.copy($scope.paymentData);
                $scope.somePaymentData.splice(tmpIdx, 1);	
            });
            AppointmentGrService.getUnitMeasurement().then(function(res) {
                $scope.unitData = res.data.Result;
                var tmpUnit = $scope.unitData;
                // for (var i = 0; i < tmpUnit.length; i++) {
                //     // var temp = {};
                //     // temp[tmpUnit[i].MasterId] = tmpUnit[i].Name;
                //     // $scope.unitDataGrid.push(temp);
                //     if (!$scope.unitDataGrid[tmpUnit[i].MasterId]) {
                //         $scope.unitDataGrid[tmpUnit[i].MasterId] = tmpUnit[i].Name;
                //     }
                // }
                // $scope.changeKey();
            });
            RepairSupportActivityGR.getT1().then(function(res) {
                $scope.TFirst = res.data.Result;
                for (var i=0; i<res.data.Result.length; i++){
                    $scope.TFirst[i].xName = $scope.TFirst[i].T1Code + ' - ' + $scope.TFirst[i].Name;
                }
                //console.log("T1", $scope.T1);
            });
            AppointmentGrService.getWoCategory().then(function(res) {
                console.log("reswo", res.data.Result);
                var resu = res.data.Result;
                // var data = _.sortBy(resu, 'Name');
                // console.log("data reswo", data);

                var temparrCategory = []
                for (var i=0; i<res.data.Result.length; i++) {
                    if (res.data.Result[i].Name != 'BP') {
                        temparrCategory.push(res.data.Result[i])
                    }
                }
                $scope.woCategory = temparrCategory;
                
                // $scope.woCategory = resu;
            });
            AppointmentGrService.getTaskCategory().then(function(res) {
                console.log('task categori ori', res)
                $scope.notFlcFpc = [];
                $scope.notFpcPlc = [];
                $scope.notFlcPlc = [];
                // $scope.notFlcPlcFpcEwc = [];
                $scope.notFlcPlcFpc = [];
                _.map(res.data.Result, function(val){
                    if (val.Name !== "FLC - Free Labor Claim" && val.Name !== "FPC - Free Parts Claim") {
                        $scope.notFlcFpc.push(val); 
                    }
                    if (val.Name !== "FPC - Free Parts Claim" && val.Name !== "PLC - Parts Labor Claim") {
                        $scope.notFpcPlc.push(val)  
                    }
                    if (val.Name !== "FLC - Free Labor Claim" && val.Name !== "PLC - Parts Labor Claim") {
                        $scope.notFlcPlc.push(val)  
                    }
                    // if (val.Name !== "FLC - Free Labor Claim" && val.Name !== "PLC - Parts Labor Claim" && val.Name !== "FPC - Free Parts Claim" && val.Name !== "EWC - Extended Warranty Claim") {
                    //     $scope.notFlcPlcFpcEwc.push(val)  
                    // }
                    if (val.Name !== "FLC - Free Labor Claim" && val.Name !== "PLC - Parts Labor Claim" && val.Name !== "FPC - Free Parts Claim") {
                        $scope.notFlcPlcFpc.push(val)  
                    }
                })

                console.log('ada flc', $scope.notFpcPlc)
                console.log('ada fpc', $scope.notFlcPlc)
                console.log('ada plc', $scope.notFlcFpc)
                // console.log('gaada plc', $scope.notFlcPlcFpcEwc)
            });
            AppointmentGrService.getDataGender().then(function(res) {
                console.log("result gender : ", res.data.Result);
                var temp = res.data.Result;
                for (var i = 0; i < temp.length; i++) {
                    temp[i].value = temp[i].CustomerGenderId;
                    temp[i].text = (temp[i].CustomerGenderName == "Laki-Laki" ? "L" : "P");
                }
                console.log("tmp", temp);
                $scope.genderData = temp;
            });

            MrsList.getDataCusRole().then(function(res) {
                console.log("result role : ", res.data.Result);
                $scope.RoleData = res.data.Result;
            });
            MrsList.getCCustomerCategory().then(function(res) {
                $scope.CustomerTypeData = res.data.Result;
            });
            // $scope.CustomerTypeData = [{CustomerTypeId:1,CustomerTypeDesc:"Individu"},{CustomerTypeId:2,CustomerTypeDesc:"Institusi"}]
            // MrsList.getDataCusType().then(function(res) {
            //   console.log("result type : ",res.data);
            //   $scope.CustomerTypeData = res.data.Result;
            // });
            // MrsList.getDataModel().then(function(res) {
            //   console.log("result : ",res.data.Result);
            //   $scope.modelData = res.data.Result;
            // });
            if (item !== undefined) {
                if (item.ProvinceId !== undefined && item.ProvinceId !== null) {
                    // MrsList.getDataProvinceById(item.ProvinceId).then(function(res) {
                    //     console.log("result province : ", res.data.Result);
                    //     $scope.ProvinceData = res.data.Result;
                    // });
                    WOBP.getMLocationProvince().then(function(res) {
                        console.log("List Province====>", res.data.Result);
                        $scope.ProvinceData = res.data.Result;
                    })
                };
                if (item.CityRegencyId !== undefined && item.CityRegencyId !== null) {
                    // MrsList.getDataCityById(item.CityRegencyId).then(function(res) {
                    //     console.log("result city : ", res.data.Result);
                    //     $scope.CityRegencyData = res.data.Result;
                    // });
                    WOBP.getMLocationCityRegency(item.ProvinceId).then(function(resu) {
                        $scope.CityRegencyData = resu.data.Result;
                    });
                };
                if (item.DistrictId !== undefined && item.DistrictId !== null) {
                    // MrsList.getDataDistrictById(item.DistrictId).then(function(res) {
                    //     console.log("result district : ", res.data.Result);
                    //     $scope.DistrictData = res.data.Result;
                    // });
                    WOBP.getMLocationKecamatan(item.CityRegencyId).then(function(resu) {
                        $scope.DistrictData = resu.data.Result;
                    });
                };
                if (item.VillageId !== undefined && item.DistrictId !== null) {
                    // MrsList.getDataVillageById(item.VillageId).then(function(res) {
                    //     console.log("result village : ", res.data.Result);
                    //     $scope.VillageData = res.data.Result;
                    // });
                    WOBP.getMLocationKelurahan(item.DistrictId).then(function(resu) {
                        $scope.VillageData = resu.data.Result;
                    });
                };
                if (item.GasTypeId !== null || item.GasTypeId !== undefined) {
                    $scope.mDataDetail.VehicleGasTypeId = item.GasTypeId;
                };
                if (item.VIN != null) {
                    AppointmentGrService.getTowass(item.VIN).then(function(res) {
                        console.log("data Towasssssss", res.data);
                        $scope.dataTowass = res.data.Result;

                        // var isLengthOpeNo = res.data.Result.length;
                        // if (isLengthOpeNo > 0) {

                        //     var category = 1;   // KATEGORI GR
                        //     var vin = item.VIN;
                        //     var startDate = '2001-01-1';
                        //     var endDate = new Date();

                        //     var d = endDate.getDate();
                        //     var m = endDate.getMonth() + 1;
                        //     var y = endDate.getFullYear();

                        //     endDate = y + '-' + m + '-' + d;
                        //     console.log('endDate ==>', endDate);

                        //     VehicleHistory.getData(category, vin, startDate, endDate).then(
                        //         function (resu) {

                        //                 console.log('DATA HISTORY TOWASS ====>', resu.data);
                        //                 $scope.mDataVehicleHistory = resu.data;
                        //                 var isLengthHistory = $scope.mDataVehicleHistory.length;

                        //                 if (isLengthHistory != 0) {
                        //                     console.log("panjang isLengthHistory", isLengthHistory);

                        //                     for (var k = 0; k < isLengthOpeNo; k++){
                        //                         console.log("k ==>", k);
                        //                         $scope.OpeNo = res.data.Result[k].OpeNo;
                        //                         console.log("$scope.OpeNo", $scope.OpeNo);

                        //                         for(var i=0; i < isLengthHistory; i++){

                        //                             console.log("i ==>", i);
                        //                             var isLengthVehicleJob = $scope.mDataVehicleHistory[i].VehicleJobService.length;
                        //                             console.log("panjang isLengthVehicleJob", isLengthVehicleJob);

                        //                             if (isLengthVehicleJob > 0) {

                        //                                 for (var j = 0; j < isLengthVehicleJob; j++) {

                        //                                     console.log("j ==>", j);
                        //                                     var jobdesc = $scope.mDataVehicleHistory[i].VehicleJobService[j].JobDescription

                        //                                     if (jobdesc.includes($scope.OpeNo)) {
                        //                                         $scope.dataTowass = [];
                        //                                         console.log('masuk includes ===>');
                        //                                         console.log("$scope.OpeNo", $scope.OpeNo);
                        //                                         console.log("jobdesc", jobdesc);

                        //                                     } else {
                        //                                         console.log('tidak masuk includes ===>');
                        //                                     }
                        //                                 }
                        //                             }

                        //                         }
                        //                     }

                        //                 } else {

                        //                     $scope.dataTowass = res.data.Result;
                        //                     console.log('$scope.dataTowass ELSE =>', $scope.dataTowass);
                        //                 }                            
                        //         },
                        //         function (err) { }
                        //     );
                        //     console.log('isLengthOpeNo ==>', isLengthOpeNo);
                        // }
                    });
                };
            }
            MrsList.getInvitationMedia().then(function(res) {
                console.log("result media : ", res.data.Result);
                $scope.InvMediaData = res.data.Result;
            });
            MrsList.getMrsInvitation().then(function(res) {
                console.log("result mrs invit : ", res.data.Result);
                $scope.MrsInvData = res.data.Result;
            });
            // ------- Model
            // AppointmentGrService.getDataModel().then(function(res) {
            //   $scope.modelData = res.data.Result;
            // });
        }

        $scope.tmpActRate = 1;
        $scope.tmpActRateChange = 0;
        $scope.estimateAsb = function() {
            $scope.hideSaveButton = true;
            if ($scope.gridWork.length > 0) {
                var totalW = 0;
                for (var i = 0; i < $scope.gridWork.length; i++) {
                    if ($scope.gridWork[i].ActualRate !== undefined) {
                        totalW += $scope.gridWork[i].ActualRate;
                    }
                }
                $scope.visibleAsb = 1;
                $scope.tmpActRate = totalW;
            } else {
                $scope.tmpActRate = 1;
                $scope.visibleAsb = 0;
            }
            $scope.checkAsb = true;
            $scope.asb.startDate = "";
            currentDate = new Date();
            currentDate.setDate(currentDate.getDate() + 1);
            // $scope.minDateASB = new Date(currentDate);
            $scope.currentDate = new Date(currentDate);
            // $scope.asb.startDate = new Date(currentDate);
            if (($scope.mData.AppointmentDate !== undefined && $scope.mData.AppointmentDate !== null) && ($scope.mData.PrediagStallId == null || $scope.mData.PrediagStallId == undefined)) {
                console.log("Estimate  ASB $scope.mData.AppointmentDate nya ada", $scope.mData.AppointmentDate);
                console.log("Estimate  ASB $scope.mData.AppointmentTime nya ada", $scope.mData.AppointmentTime);
                var tmpDate = $scope.mData.AppointmentDate;
                // var tmpMonth = "";
                var tmpTime = $scope.mData.AppointmentTime;
                // if (tmpDate.getMonth() < 10) {
                //     tmpMonth = "0" + (tmpDate.getMonth() + 1);
                // } else {
                //     tmpMonth = tmpDate.getMonth();
                // }
                // var ini = tmpDate.getFullYear() + "/" + tmpMonth + "/" + tmpDate.getDate() + " " + tmpTime;
                var finalDate
                var yyyy = tmpDate.getFullYear().toString();
                var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                var dd = tmpDate.getDate().toString();
                finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + " " + tmpTime;
                var d = new Date(finalDate);
                $scope.asb.startDate = d;
                $scope.currentDate = d;
                // console.log("date yg diinginkan", ini);
            } else {
                console.log("Estimate  ASB $scope.mData.AppointmentDate nya gk ada", $scope.mData.AppointmentDate);
                console.log("Estimate  ASB $scope.mData.AppointmentTime nya gk ada", $scope.mData.AppointmentTime);
                $scope.asb.startDate = new Date(currentDate);
                $scope.currentDate = new Date(currentDate);
            }
            // $scope.asb.startDate = $scope.mData.AppointmentDate;
            // ===============================
            var timeToDateStart = $scope.asb.startDate;
            var timeToDateFinish = "";
            if ($scope.mData.PlanDateFinish !== null && $scope.mData.PlanDateFinish !== undefined) {
                timeToDateFinish = new Date($scope.mData.PlanDateFinish);
            } else {
                timeToDateFinish = new Date($scope.asb.startDate);
            }
            var timePlanStart = $scope.mData.PlanStart;
            var timePlanFinish = $scope.mData.PlanFinish;
            if (($scope.mData.PlanStart !== null && $scope.mData.PlanStart !== undefined) && ($scope.mData.PlanFinish !== null && $scope.mData.PlanFinish !== undefined)) {
                timePlanStart = timePlanStart.split(":");
                timePlanFinish = timePlanFinish.split(":");
                timeToDateStart = new Date(timeToDateStart.setHours(timePlanStart[0], timePlanStart[1], timePlanStart[2]));
                timeToDateFinish = new Date(timeToDateFinish.setHours(timePlanFinish[0], timePlanFinish[1], timePlanFinish[2]));
                console.log("timeToDateStart ada PlanStart", timeToDateStart);
                console.log("timeToDateFinish ada PlanFinish", timeToDateFinish);
            } else {
                timeToDateStart = $scope.mData.PlanStart;
                timeToDateFinish = $scope.mData.PlanFinish;
            }
            // ===============================
            $scope.asb.JobStallId = $scope.mData.StallId;
            $scope.asb.JobStartTime = timeToDateStart;
            $scope.asb.JobEndTime = timeToDateFinish;
            if (tmpPrediagnose.StallId !== undefined) {
                $scope.asb.PrediagStallId = tmpPrediagnose.StallId;
            } else {
                $scope.asb.PrediagStallId = $scope.mData.PrediagStallId
            }
            $scope.asb.preDiagnose = tmpPrediagnose;
            if (tmpPrediagnose.ScheduledTime !== undefined && tmpPrediagnose.ScheduledTime !== null) {
                var dateDay = ""
                var dateMonth = ""
                var finalMonth = ""
                var finalDay = ""
                var dateYear = ""
                dateYear = $scope.minDateASB.getFullYear().toString();
                dateMonth = ($scope.minDateASB.getMonth()).toString(); // getMonth() is zero-based
                dateDay = $scope.minDateASB.getDate().toString();
                finalDay = (dateDay[1] ? dateDay : "0" + dateDay[0]);
                finalMonth = (dateMonth[1] ? dateMonth : "0" + dateMonth[0]);
                tmpPrediagnose.ScheduledTime.setFullYear(dateYear, finalMonth, finalDay);
                $scope.asb.PrediagStartTime = tmpPrediagnose.ScheduledTime;
            } else {
                $scope.asb.PrediagStartTime = $scope.mData.PrediagScheduledTime;
            }
            // $scope.asb.currentDate = currentDate;
            // $scope.asb.PrediagEndTime = $scope.mData.PrediagEndTime;
            // ==============================
            $scope.mData.FullDate = $scope.asb.startDate;
            $scope.stallAsb = $scope.mData.StallId;
            console.log("Estimate ASB curDate", currentDate);
            console.log("Estimate ASB $scope.asb.startDate", $scope.asb.startDate);
            console.log("Estimate ASB $scope.stallAsb", $scope.stallAsb);
            console.log("Estimate ASB $scope.mData", $scope.mData);
            console.log("Estimate ASB $scope.asb", $scope.asb);
            console.log("$scope.visibleAsb", $scope.visibleAsb);
            showBoardAsbGr($scope.mData.FullDate);
            _.map($scope.actionButtonSettingsDetail, function(val) {
                // if (val.title === 'Back') {
                val.visible = false;
                // }
            });
        };
        $scope.backEstimate = function() {
            $scope.checkAsb = false;
            _.map($scope.actionButtonSettingsDetail, function(val) {
                if (val.title !== 'Simpan & Kirim Email') {
                    val.visible = true;
                }
            });
            if ($scope.mDataDetail.Email == null) {
                $scope.disableEmail(false);
            } else {
                // $scope.disableEmail(true); // dikomen dl krn email gtw jln gtw ga
                $scope.disableEmail(false);

            };
            $scope.asb.startDate = [];
        };
        // $scope.unitDataGrid=[];
        $scope.unitDataGrid = {};
        $scope.changeKey = function() {
            var tmpUnit = $scope.unitData;
            for (var i = 0; i < tmpUnit.length; i++) {
                // var temp = {};
                // temp[tmpUnit[i].MasterId] = tmpUnit[i].Name;
                // $scope.unitDataGrid.push(temp);
                if (!$scope.unitDataGrid[tmpUnit[i].MasterId]) {
                    $scope.unitDataGrid[tmpUnit[i].MasterId] = tmpUnit[i].Name;
                }
            }
            console.log("unitDataGrid", $scope.unitDataGrid);
            console.log("unitDataGrid[2]", $scope.unitDataGrid[2]);

        }
        var gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
            // '<a href="#" uib-tooltip="Buat" tooltip-placement="bottom" ng-click="grid.appScope.actView(row.entity)" style="">Appointment</a>'+
            '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Buat" ng-hide="row.entity.StatusCode == 0" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-plus-circle" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.actCancel(row.entity)" uib-tooltip="Batal" ng-hide="row.entity.StatusCode == 0 || row.entity.StatusCode == undefined" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-times-circle-o" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.actReschedule(row.entity)" uib-tooltip="Reschedule" ng-hide="row.entity.StatusCode == 0 || row.entity.StatusCode == undefined" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';
        var gridLangsung = '<div class="ui-grid-cell-contents">' +
            '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Buat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-plus-circle" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';
        var lblSatuan = '';
        $scope.cekType = function(item) {
            console.log(item)
                // if(item == 'Langsung'){
                //   $scope.gridActionButtonTemplate ='<div class="ui-grid-cell-contents">' +
                //   '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Buat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-plus-circle" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>'+
                // '</div>'
                // }else{
                //   $scope.gridActionButtonTemplate =gridLain;
                // }
        };
        $scope.disableEmail = function(param) {
            console.log("disableEmail", param);
            _.map($scope.actionButtonSettingsDetail, function(val) {
                if (val.title === 'Simpan & Kirim Email') {
                    val.visible = param;
                }
            })
        };
        $scope.actView = function(data) {
            console.log('ini pas actView ');
            $scope.getPPN();
            $scope.tempListClaim = [];
            $scope.listClaim = [];

            $scope.loadingSimpan = 0
            $scope.restart_digit_odo = 0
            $scope.isKmHistory = 0
            $scope.nilaiKM = 0
            $scope.KMTerakhir = 0

            $scope.restart_digit_odo = 0;
            $scope.model_info_km.show = false;
            
            MasterDiscountFactory.getDataAktif().then(
                function (res) {
                    console.log("res dis | ", res.data.Result, res.data.Result.length);

                    $scope.MasterDiscountBooking = res.data.Result;
                    console.log('$scope.MasterDiscountBooking hobah', $scope.MasterDiscountBooking)

                        var flagcounttask = 0;
                        var flagcountparts = 0;
                        if($scope.MasterDiscountBooking.length > 0){

                            for(var i = 0; i < $scope.MasterDiscountBooking.length; i++){
                                if($scope.MasterDiscountBooking[i].Category == 2){
                                    $scope.DiskonPartsEndDate = $scope.MasterDiscountBooking[i].DateTo;
                                    $scope.DiscountParts = $scope.MasterDiscountBooking[i].Discount;
                                    $scope.mData.disParts = $scope.MasterDiscountBooking[i].Discount;
                                    flagcountparts++
                                }else if($scope.MasterDiscountBooking[i].Category == 1){
                                    $scope.DiskonTaskEndDate = $scope.MasterDiscountBooking[i].DateTo;
                                    $scope.DiscountTask = $scope.MasterDiscountBooking[i].Discount;
                                    $scope.mData.disTask = $scope.MasterDiscountBooking[i].Discount;
                                    flagcounttask++
                                }
                            }
                        }
                        else{

                        // $scope.DiskonPartsEndDate = $scope.MasterDiscountBooking[i].DateTo;
                        $scope.DiscountParts = 0;
                        $scope.mData.disParts = 0; 

                        // $scope.DiskonTaskEndDate = $scope.MasterDiscountBooking[i].DateTo;
                        $scope.DiscountTask = 0;
                        $scope.mData.disTask = 0;
                        }

                        if(flagcounttask == 0){
                            // $scope.DiskonTaskEndDate = $scope.MasterDiscountBooking[i].DateTo;
                            $scope.DiscountTask = 0;
                            $scope.mData.disTask = 0;
                        }

                        if(flagcountparts == 0){
                            
                            $scope.DiscountParts = 0;
                            $scope.mData.disParts = 0; 
                        }

                        if(($scope.asbDate == undefined) || ($scope.asbDate == [])){
                            
                            console.log('kalo asbnya undefined | [] ===>',$scope.asbDate);
                        }else{
                            if($scope.asbDate > $scope.DiskonTaskEndDate){
                                console.log('Tidak Masuk periode diskon booking Task');
                                $scope.DiscountTask = 0;
                                $scope.mData.disTask = 0;
                            }else{
                                console.log('masuk periode diskon booking Task');
                            }
                            if($scope.asbDate > $scope.DiskonPartsEndDate){
                                console.log('Tidak Masuk periode diskon booking Parts');
                                $scope.DiscountParts = 0;
                                $scope.mData.disParts = 0;
                            }else{
                                console.log('masuk periode diskon booking Parts');
                            }
                        }

                        
                   
                },
                function (err) { }
            );

            // if($scope.MasterDiscountBooking.length !== 2){
            //     $scope.bsAlertConfirm("Master Diskon Booking Belum di Input").then(function (res) {
            //         if (res) {
            //             console.log('ini kalo klik ok',res);
            //         }else{
            //             console.log("ini kalo klik cancle", res);
            //         }
            //     });
            // }else{

                $scope.isKmHistory = 0;
                $scope.KMTerakhir = 0;
                $scope.tmpIsNonTAM = data.VehicleNonTAMId;
                $scope.mDataVehicleHistory = [];
                $scope.MRS = {};
                var Parts = [];
                Parts.splice();
                $scope.ShowEdit = true;
                $scope.EnableEdit = true;
                $scope.ShowEditInstitusi = true;
                $scope.EnableEditInstitusi = true;
                $scope.EnableEditContactPerson = true;
                $scope.noRef = 0;
                var Apppoint = angular.copy(data);
                $scope.mData = {};
                gridTemp = [];
                $scope.JobComplaint = [];
                $scope.JobRequest = [];
                $scope.gridWork = gridTemp;
                $scope.totalWork = 0;
                $scope.costMaterial = 0;
                $scope.summaryJoblist = 0;
                $scope.totalDp = 0;
                $scope.totalMaterial = 0;
                tmpPrediagnose = {};
                $scope.asb = {};
                $scope.asb = { showMode: 'main', NewChips: 1 };
                $scope.asb.endDate = new Date();
                $scope.asb.endDate.setDate($scope.asb.endDate.getDate() + 7);
                AppointmentGrService.getDataVehicle(data.VehicleId).then(function(ress) {
                    var dataDetail = ress.data.Result[0]
                    console.log('cek data detai 1', dataDetail)
                    $scope.mDataDetail = dataDetail;

                    if ($scope.mDataDetail.VehicleNonTAMId != null){
                        $scope.mDataDetail.KatashikiCode = $scope.mDataDetail.NonTamKatashikiCode
                        $scope.mDataDetail.ColorName = $scope.mDataDetail.NonTamColorName
                        $scope.mDataDetail.VehicleTypeDesc = $scope.mDataDetail.NonTamModelType
                        $scope.mDataDetail.VehicleModelName = $scope.mDataDetail.NonTamModelName

                    }
                    AppointmentGrService.getDataTaskListByKatashiki($scope.mDataDetail.KatashikiCode).then(function(restask) {
                        taskList = restask.data.Result;
                        console.log("resTask", taskList);
                        var tmpTask = {};
                        if (taskList.length > 0) {
                            for (var i = 0; i < taskList.length; i++) {
                                // var summary = taskList[i].ServiceRate;
                                // var summaryy = parseInt(summary);
                                if (taskList[i].ServiceRate !== undefined && taskList[i].WarrantyRate !== undefined) {
                                    tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                                    tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                                    $scope.PriceAvailable = true;
                                } else {
                                    tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                                    tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                                }
                                // if (taskList[i].ServiceRate !== undefined) {
                                //     var summary = taskList[i].ServiceRate;
                                //     var summaryy = parseInt(summary);
                                //     tmpTask.ServiceRate = Math.round(summaryy);
                                //     // row.Fare = taskList[i].AdjustmentServiceRate;
                                //     tmpTask.Fare = taskList[i].ServiceRate;
                                //     $scope.PriceAvailable = true;
                                // } else {
                                //     tmpTask.Fare = taskList[i].ServiceRate;
                                //     tmpTask.Summary = tmpTask.Fare;
                                // }
                                $scope.tmpServiceRate = tmpTask;
                            }
                        } else {
                            $scope.tmpServiceRate = null;
                        }
                    });
                    $scope.getAllParameter(dataDetail);
                    console.log("$scope.mDataDetail", $scope.mDataDetail, $scope.tmpServiceRate);
                    console.log("$scope.mData", $scope.mData);
                    if (dataDetail.Email == null) {
                        $scope.disableEmail(false);
                    } else {
                        $scope.disableEmail(true);
                    };
                    // AppointmentGrService.getDataContactPerson($scope.mDataDetail.Cust).then(function(res){

                    // });
                    AppointmentGrService.getDataContactPerson($scope.mDataDetail.CustomerVehicleId).then(function(res) {
                        // console.log("List Province====>",res.data.Result);
                        $scope.ContactPersonData = res.data.Result;
                        // $scope.ShowEdit = false;
                        // $scope.EnableEditContactPerson = false;
                        // console.log("Data ContactPerson", $scope.ContactPersonData);
                        AppointmentGrService.CheckLastContactPerson($scope.mDataDetail.VehicleId, 1).then(function(res1) {
                            if (res1.data.length > 0) {
                                $scope.mData.ContactPerson = res1.data[0].ContactPerson;
                                $scope.mData.PhoneContactPerson1 = res1.data[0].PhoneContactPerson1;
                            }
                        })
                    })
                    AppointmentGrService.getDataTaskList($scope.mDataDetail.KatashikiCode).then(function(result) {
                        console.log("result getDataTaskList", result);
                    });

                    AppointmentGrService.getDataVIN($scope.mDataDetail.VIN).then(function(res){
                        console.log('cek input rangka gantengkuuu', $scope.mDataDetail.VIN)
                        var vinMasterId = res.data.Result;
                        $scope.tempVIN = res.data.Result[0];
                        if (vinMasterId != null && $scope.tempVIN != null) {
                            if ($scope.tempVIN.ProgramName != null && $scope.tempVIN.ProgramName != undefined && $scope.tempVIN.ProgramName != "") {
                                var tempProgramName = $scope.tempVIN.ProgramName;
                                if ($scope.tempVIN.ProgramName != null && $scope.tempVIN.ProgramName != undefined) {
                                    $scope.tempVIN.ProgramName = tempProgramName;
                                }else{
                                    $scope.tempVIN.ProgramName = '-'
                                } 
                            }else{
                                $scope.tempVIN.ProgramName = '-'
                            }
        
                            if ($scope.tempVIN.DEC_Date != null && $scope.tempVIN.DEC_Date != undefined && $scope.tempVIN.DEC_Date != "") {
                                if ($scope.tempVIN.DEC_Date != null && $scope.tempVIN.DEC_Date != undefined) {
                                    $scope.tempVIN.DEC_Date = $filter('date')($scope.tempVIN.DEC_Date, 'dd/MM/yyyy')
                                }else{
                                    $scope.tempVIN.DEC_Date = '-'
                                }
                            }else{
                                $scope.tempVIN.DEC_Date = '-'
                            }
        
                            console.log('cek res rangka', $scope.tempVIN)
                            // $scope.taskCategory = $scope.notFlcPlcFpcEwc;
                            for(var i = 0; i < vinMasterId.length; i++){
                                console.log('cek data vin', vinMasterId[i].MasterId)
                                console.log('cek data tempVIN', vinMasterId.length)
                                console.log('$scope.notFlcPlc', $scope.notFlcPlc)
                                if ($scope.mDataDetail.VIN != null && $scope.mDataDetail.VIN != "") {
                                    if (vinMasterId[i].ExtendedWarranty == 1) {
                                        if (vinMasterId[i].MasterId == 2678) {
                                            $scope.taskCategory = $scope.notFlcFpc;
                                        }else if(vinMasterId[i].MasterId == 2677){
                                            $scope.taskCategory = $scope.notFpcPlc;
                                        }else if(vinMasterId[i].MasterId == 2675){
                                            $scope.taskCategory = $scope.notFlcPlc;
                                        }else{
                                            $scope.tableListClear();
                                            $scope.taskCategory = $scope.notFlcPlcFpc;
                                        }
                                    }else{
                                        if (vinMasterId[i].MasterId == 2678) {
                                            var filtered = $scope.notFlcFpc.filter(function(item) { 
                                                return item.Name !== "EWC - Extended Warranty Claim";  
                                                });
                                                console.log("filtered",filtered);
                                                $scope.taskCategory = filtered;
                                        }else if(vinMasterId[i].MasterId == 2677){
                                            var filtered = $scope.notFpcPlc.filter(function(item) { 
                                                return item.Name !== "EWC - Extended Warranty Claim";  
                                                });
                                                console.log("filtered",filtered);
                                                $scope.taskCategory = filtered;
                                        }else if(vinMasterId[i].MasterId == 2675){
                                            var filtered = $scope.notFlcPlc.filter(function(item) { 
                                                return item.Name !== "EWC - Extended Warranty Claim";  
                                                });
                                                console.log("filtered",filtered);
                                                $scope.taskCategory = filtered;
                                        }else{
                                            $scope.tableListClear();
                                            $scope.taskCategory = $scope.notFlcPlcFpc;
                                        }      
                                    }
                                }else{
                                    $scope.tableListClear();
                                    $scope.taskCategory = $scope.notFlcPlcFpc;
                                }
                            }
                        }
                        
                            console.log('cek res input gantengkuuu', $scope.tempVIN)
                            console.log('$scope.taskCategory ', $scope.taskCategory )
                    })

                    AppointmentGrService.listHistoryClaim($scope.mDataDetail.VIN).then(function(res){
                        console.log('unyu 1234')
                        // res.data.data = $scope.ubahFormatTanggalIndo(res.data.data.historyClaimDealerList)
                        $scope.listClaim = res.data.data;
                        $scope.tempListClaim = [];
                        // $scope.historyDate = new Date($scope.tempListClaim.claimDate)
                        console.log('cek list claim', res)
                        console.log('list claim', res.data.data)
                        console.log('list history claim', $scope.tempListClaim)
                        for (var i = 0; i < $scope.listClaim.historyClaimDealerList.length; i++) {
                            $scope.tempListClaim.push($scope.listClaim.historyClaimDealerList[i]);
                        }
                    })

                });

                AppointmentGrService.getTowass($scope.mDataDetail.VIN).then(function(res) {
                    console.log("data Towasssssss", res.data);
                    $scope.dataTowass = res.data.Result;
                });
                
                AppointmentGrService.getDataVehiclePSFU(data.VehicleId).then(function(resu) {
                    console.log("getDataVehiclePSFU", resu.data.Result);
                    var dataQuestionAnswerPSFU = resu.data.Result[0];

                    //------- validasi tgl riwayat servis -----------------------
                    var minTglDEC = new Date(dataQuestionAnswerPSFU.DECDate);
                    console.log("minTglDEC", minTglDEC);
                    $scope.DateOptionsHistory.minDate = minTglDEC.setDate(minTglDEC.getDate() + 1);

                    var maxTglDEC = new Date();
                    console.log("maxTglDEC", maxTglDEC);
                    $scope.DateOptionsHistory.maxDate = maxTglDEC;
                    //------------------------------------------------------------

                    $scope.mDataDetail.Suggest = dataQuestionAnswerPSFU.JobSuggest;
                    FollowupService.getQuestion().then(function(resu2) {
                        console.log("getQuestion", resu2.data.Result);
                        var dataQuestionAnswer = resu2.data.Result;
                        var arr = [];
                        _.map(dataQuestionAnswer, function(val, key) {
                            var obj = {};
                            if (dataQuestionAnswerPSFU.QuestionId1 == val.QuestionId) {
                                obj.QuestionId = val.QuestionId;
                                obj.Description = val.Description;
                                obj.Answer = val.Answer;
                                obj.Reason = dataQuestionAnswerPSFU.Reason1 ? dataQuestionAnswerPSFU.Reason1 : null;
                                obj.ReasonStatus = dataQuestionAnswerPSFU.Reason1 ? false : true;
                                obj.jawaban = dataQuestionAnswerPSFU.AnswerId1
                                arr.push(obj);
                            } else if (dataQuestionAnswerPSFU.QuestionId2 == val.QuestionId) {
                                obj.QuestionId = val.QuestionId;
                                obj.Description = val.Description;
                                obj.Answer = val.Answer;
                                obj.Reason = dataQuestionAnswerPSFU.Reason2 ? dataQuestionAnswerPSFU.Reason1 : null;
                                obj.ReasonStatus = dataQuestionAnswerPSFU.Reason2 ? false : true;
                                obj.jawaban = dataQuestionAnswerPSFU.AnswerId2
                                arr.push(obj);
                            } else if (dataQuestionAnswerPSFU.QuestionId3 == val.QuestionId) {
                                obj.QuestionId = val.QuestionId;
                                obj.Description = val.Description;
                                obj.Answer = val.Answer;
                                obj.Reason = dataQuestionAnswerPSFU.Reason3 ? dataQuestionAnswerPSFU.Reason3 : null;
                                obj.ReasonStatus = dataQuestionAnswerPSFU.Reason3 ? false : true;
                                obj.jawaban = dataQuestionAnswerPSFU.AnswerId3
                                arr.push(obj);
                            };
                            // arr.push(obj);
                        });
                        console.log('arr >>>', arr);
                        $scope.Question = arr;
                    })

                    var vin = dataQuestionAnswerPSFU.VIN;
                    if (vin === null || vin === undefined || vin === "") {
                        console.log("Tidak ada VIN");

                    } else {

                        console.log('CEK SERVIS ====>');
                        //CEK SERVICE HISTORY
                        var category = 2;       // KATEGORI GR
                        var VIN = vin;
                        var startDate = '1900-01-1';
                        var endDate = new Date();
                        
                        var d = endDate.getDate();
                        var m = endDate.getMonth() + 1;
                        var y = endDate.getFullYear();

                        endDate = y + '-' + m + '-' + d;
                        console.log('endDate ==>',endDate);
                        $scope.CekServiceHistory(category, VIN, startDate, endDate);
                    }
                });
                MrsList.getDataModel().then(function(res) {
                    console.log("result : ", res.data.Result);
                    $scope.modelData = res.data.Result;
                });
                $scope.getEstimate(data.LicensePlate);
                $scope.vehicleStatus = 0;
                $scope.checkAsb = false;
                $scope.formApi.setMode('detail');
                $scope.mData = Apppoint;
                $scope.mData.startDate = null;
                $scope.mData.endDate = null;
                $scope.mData.categoryHisory = null;
                $scope.activeJustified = 0;
                $scope.AppointmentGrForm.$setPristine();
                $scope.AppointmentGrForm.$setUntouched();
                $("input[name='DateAppoint']").attr("disabled", "disabled");

            // }
        };
        $scope.actReschedule = function(item) {
            console.log('ini pas actReschedule ');

            if ($scope.filter.Type !== "Langsung") {
                $scope.show_modal.show = !$scope.show_modal.show;
                $scope.modal_model = angular.copy(item);
            }
        };
        // $scope.actCancel = function(item) {
        //     if ($scope.filter.Type !== "Langsung") {
        //         bsAlert.alert({
        //                 title: "Apakah anda yakin untuk membatalkan request appointment?",
        //                 text: "",
        //                 type: "warning",
        //                 showCancelButton: true
        //             },
        //             function() {
        //                 $scope.cancelAppointment(item);
        //             },
        //             function() {

        //             }
        //         )
        //     }
        // };
        // ==== Added 09-12-2017
        $scope.actCancel = function(item) {
            console.log('ini pas actCancel ')
            if ($scope.filter.Type !== "Langsung") {
                $scope.modal_model = angular.copy(item);
                $scope.showModalReason = true;
            }
        }
        $scope.doCancelAppointment = function(item) {
            console.log("doCancelAppointment Item", item);
            if (item.Reason !== undefined) {
                $scope.cancelAppointment(item);
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Data masih belum terisi",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            }
        };
        $scope.doBackAppointment = function(item) {
            console.log('itemcancel', item);
            $scope.showModalReason = false;
        }
        $scope.cancelAppointment = function(item) {
            console.log("itemnya", item);
            AppointmentGrService.cancelRequest(item).then(function() {
                $scope.showModalReason = false;
                $scope.getData(true);
            });
        };
        $scope.rescheduleSave = function(item) {
            var tmpItem = item.RequestAppointmentDateNew;
            var finalDate
            var yyyy = tmpItem.getFullYear().toString();
            var mm = (tmpItem.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = tmpItem.getDate().toString();
            finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
            item.RequestAppointmentDateNew = finalDate;
            AppointmentGrService.rescheduleRequest(item).then(function() {
                $scope.getData(true);
                $scope.show_modal.show = false;
            });
        };
        $scope.rescheduleCancel = function(item) {
                console.log('itemcancel', item);
                $scope.show_modal.show = false;
            }
            // ========Action Untuk BsForm Base========
            // $scope.customActionButtonSettings = [{
            //     title: 'Simpan Draft',
            //     icon: 'fa fa-fw fa-save',
            //     func: function(row, formScope) {},
            //     type: 'custom'
            // }]
        $scope.actionButtonSettingsDetail = [
            // {
            //     title: 'Simpan Draft',
            //     icon: 'fa fa-fw fa-save',
            //     func: function(row, formScope) {
            //         console.log("mData", $scope.mData);
            //         $scope.mData.JobTask = gridTemp;
            //         console.log("mData", $scope.mData);
            //         console.log("mDataDetail", $scope.mDataDetail);
            //         console.log('row kirim email >>', row);
            //         var tmpGrid = [];
            //         $scope.AppointmentNo = null;
            //         tmpGrid = angular.copy(gridTemp);
            //         if ((gridTemp.length > 0 || !(angular.equals({}, tmpPrediagnose))) && $scope.mData.AppointmentDate !== undefined && $scope.mData.ContactPerson !== undefined && $scope.mData.PhoneContactPerson1 !== undefined && $scope.mData.WoCategoryId !== undefined && $scope.mData.Km !== undefined && ($scope.mDataDetail.LicensePlate !== undefined && $scope.mDataDetail.LicensePlate !== null)) {
            //             var tmpKm = angular.copy($scope.mData.Km);
            //             var backupKm = angular.copy($scope.mData.Km);
            //             if (tmpKm.includes(",")) {
            //                 tmpKm = tmpKm.split(',');
            //                 if (tmpKm.length > 1) {
            //                     tmpKm = tmpKm.join('');
            //                 } else {
            //                     tmpKm = tmpKm[0];
            //                 }
            //             }
            //             tmpKm = parseInt(tmpKm);
            //             $scope.mData.Km = tmpKm;
            //             for (var i = 0; i < tmpGrid.length; i++) {
            //                 tmpGrid[i].JobParts = [];
            //                 angular.forEach(tmpGrid[i].child, function(v, k) {
            //                     tmpGrid[i].JobParts[k] = v;
            //                     tmpGrid[i].JobParts[k].Price = tmpGrid[i].JobParts[k].RetailPrice;
            //                     tmpGrid[i].JobParts[k].DPRequest = tmpGrid[i].JobParts[k].DownPayment;
            //                     delete tmpGrid[i].JobParts[k].ETA;
            //                     delete tmpGrid[i].JobParts[k].PaidBy;
            //                     // delete tmpGrid[i].JobParts[k].Dp;
            //                     delete tmpGrid[i].child;
            //                 })
            //                 if ($scope.mData.JobId !== undefined && $scope.mData.JobId !== null) {
            //                     tmpGrid[i].JobId = $scope.mData.JobId;
            //                 } else {
            //                     tmpGrid[i].JobId = 0;
            //                 }
            //                 delete tmpGrid[i].PaidBy;
            //                 delete tmpGrid[i].tmpTaskId;

            //             }
            //             console.log("=====>", tmpGrid);
            //             $scope.mData.JobTask = tmpGrid;
            //             $scope.mData.JobComplaint = $scope.JobComplaint;
            //             $scope.mData.JobRequest = $scope.JobRequest;
            //             $scope.mData.detail = $scope.mDataDetail;
            //             tmpPrediagnose.ScheduledTime = $filter('date')($scope.mData.PrediagScheduledTime, 'yyyy-MM-dd HH:mm:ss');
            //             tmpPrediagnose.StallId = $scope.mData.PrediagStallId;
            //             tmpPrediagnose.Status = $scope.mData.StatusPreDiagnose;
            //             var tmpData = angular.copy($scope.mData);
            //             var tmpAppointmentDate = tmpData.AppointmentDate;
            //             tmpAppointmentDate = new Date(tmpAppointmentDate);
            //             var finalDate
            //             var yyyy = tmpAppointmentDate.getFullYear().toString();
            //             var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based         
            //             var dd = tmpAppointmentDate.getDate().toString();
            //             finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
            //             $scope.mData.AppointmentDate = finalDate;
            //             AppointmentGrService.createAppointmentDraft($scope.mData).then(function(res) {
            //                 if (res.data == -1) {
            //                     bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mData.PoliceNumber + " sudah memiliki Appointment dengan tanggal " + $scope.mData.AppointmentDate, "silahkan cek kembali");
            //                     $scope.mData.Km = backupKm;
            //                 } else if (res.data == -2) {
            //                     bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
            //                     $scope.mData.Km = backupKm;
            //                 } else {
            //                     var tmpJobId = res.data.ResponseMessage;
            //                     tmpJobId = tmpJobId.split('#');
            //                     console.log("tmpJobId", tmpJobId[1]);
            //                     console.log("tmpPrediagnose", tmpPrediagnose);
            //                     tmpPrediagnose.JobId = tmpJobId[1];
            //                     AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

            //                     });
            //                     console.log("abis save", res);
            //                     $scope.filter.noPolice = null;
            //                     $scope.filter.noRangka = null;
            //                     $scope.formApi.setMode('grid');
            //                 }
            //             }, function(err) {
            //                 $scope.mData.Km = backupKm;
            //             });
            //         } else {
            //             bsAlert.warning("Form masih ada yang belum terisi", "silahkan cek kembali");
            //         };
            //     },
            //     type: 'custom' //for link
            // },
            {
                func: function(row, formScope) {
                    console.log("mData", $scope.mData);

                    var tmpKm = angular.copy($scope.mData.Km).toString();
                    var backupKm = angular.copy($scope.mData.Km);
                    if (tmpKm.includes(",") == true) {
                        tmpKm = tmpKm.split(',');
                        if (tmpKm.length > 1) {
                            tmpKm = tmpKm.join('');
                        } else {
                            tmpKm = tmpKm[0];
                        }
                    }
                    tmpKm = parseInt(tmpKm);

                    if (tmpKm < $scope.KMTerakhir && $scope.KMTerakhir < 999999) {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "KM tidak boleh lebih kecil dari " + $scope.KMTerakhir 
                        });
                        return false;
                    }

                    $scope.mData.JobTask = $scope.gridWork;
                    console.log("mData", $scope.mData);
                    console.log("mDataDetail", $scope.mDataDetail);
                    console.log('row kirim email >>', row);
                    
                    var tmpGrid = [];
                    $scope.AppointmentNo = null;
                    tmpGrid = angular.copy($scope.gridWork);
                    // if ((gridTemp.length > 0 || !(angular.equals({}, tmpPrediagnose))) && $scope.mData.AppointmentDate !== undefined && $scope.mData.ContactPerson !== undefined && $scope.mData.PhoneContactPerson1 !== undefined && $scope.mData.WoCategoryId !== undefined && $scope.mData.Km !== undefined && ($scope.mDataDetail.LicensePlate !== undefined && $scope.mDataDetail.LicensePlate !== null)) {
                    if ($scope.mData.AppointmentDate !== undefined && $scope.mData.ContactPerson !== undefined && $scope.mData.PhoneContactPerson1 !== undefined && $scope.mData.WoCategoryId !== undefined && $scope.mData.Km !== undefined && ($scope.mDataDetail.LicensePlate !== undefined && $scope.mDataDetail.LicensePlate !== null && $scope.mDataDetail.LicensePlate !== '')) {
                        if (AsbGrFactory.checkChipFinished($scope.boardName)) {
                            var splitChip = AsbGrFactory.getChipData($scope.boardName);
                            splitChip = splitChip.split(';');
                            var tmpJobSplitChip = [];
                            for (var i in splitChip) {
                                var tmpHasilSplit = splitChip[i].split('|');
                                tmpJobSplitChip.push({
                                    JobSplitId: 0,
                                    JobId: 0,
                                    PlanStart: tmpHasilSplit[1],
                                    PlanFinish: tmpHasilSplit[2],
                                    PlanDateStart: tmpHasilSplit[0],
                                    PlanDateFinish: tmpHasilSplit[0],
                                    StallId: tmpHasilSplit[3],
                                    isSplitActive: 1
                                })
                            }
                            if (tmpJobSplitChip.length > 1) {
                                $scope.mData.JobListSplitChip = tmpJobSplitChip;
                            } else {
                                $scope.mData.JobListSplitChip = []
                            }
                            console.log("splitChip", splitChip);
                            var tmpKm = angular.copy($scope.mData.Km);
                            var backupKm = angular.copy($scope.mData.Km);
                            if (tmpKm.includes(",")) {
                                tmpKm = tmpKm.split(',');
                                if (tmpKm.length > 1) {
                                    tmpKm = tmpKm.join('');
                                } else {
                                    tmpKm = tmpKm[0];
                                }
                            }
                            tmpKm = parseInt(tmpKm);
                            $scope.mData.KmNormal = tmpKm;
                            for (var i = 0; i < tmpGrid.length; i++) {
                                tmpGrid[i].JobParts = [];
                                angular.forEach(tmpGrid[i].child, function(v, k) {
                                    tmpGrid[i].JobParts[k] = v;
                                    if (tmpGrid[i].JobParts[k].PaidById == 2277 || tmpGrid[i].JobParts[k].PaidById == 30 || tmpGrid[i].JobParts[k].PaidById == 31) {
                                        tmpGrid[i].JobParts[k].minimalDp = 0;
                                    }
                                    // if(tmpGrid[i].JobParts[k].PartsId == null){
                                    //     delete tmpGrid[i].JobParts[k].PartsId
                                    // }
                                    // if(tmpGrid[i].JobParts[k].Parts == null){
                                    //     delete tmpGrid[i].JobParts[k].Parts
                                    // }
                                    tmpGrid[i].JobParts[k].Price = tmpGrid[i].JobParts[k].RetailPrice;
                                    tmpGrid[i].JobParts[k].DPRequest = tmpGrid[i].JobParts[k].DownPayment;
                                    delete tmpGrid[i].JobParts[k].ETA;
                                    delete tmpGrid[i].JobParts[k].PaidBy;
                                    // delete tmpGrid[i].JobParts[k].Dp;
                                    delete tmpGrid[i].child;
                                })
                                if ($scope.mData.JobId !== undefined && $scope.mData.JobId !== null) {
                                    tmpGrid[i].JobId = $scope.mData.JobId;
                                } else {
                                    tmpGrid[i].JobId = 0;
                                }
                                delete tmpGrid[i].PaidBy;
                                delete tmpGrid[i].tmpTaskId;

                            }
                            console.log("=====>", tmpGrid);
                            $scope.mData.JobTask = tmpGrid;
                            $scope.mData.JobComplaint = $scope.JobComplaint;
                            $scope.mData.JobRequest = $scope.JobRequest;
                            $scope.mData.detail = $scope.mDataDetail;
                            tmpPrediagnose.ScheduledTime = $filter('date')($scope.mData.PrediagScheduledTime, 'yyyy-MM-dd HH:mm:ss');
                            tmpPrediagnose.StallId = $scope.mData.PrediagStallId;
                            tmpPrediagnose.Status = $scope.mData.StatusPreDiagnose;
                            var tmpData = angular.copy($scope.mData);
                            var tmpAppointmentDate = tmpData.AppointmentDate;
                            tmpAppointmentDate = new Date(tmpAppointmentDate);
                            var finalDate
                            var yyyy = tmpAppointmentDate.getFullYear().toString();
                            var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                            var dd = tmpAppointmentDate.getDate().toString();
                            finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                            $scope.mData.AppointmentDate = finalDate;
                            $scope.mData.totalDpDefault = $scope.totalDpDefault;
                            $scope.mData.totalDp = $scope.totalDp;
                            $scope.mData.totalDpDefault = $scope.totalDpDefault;
                            console.log("$scope.mData dengan DP harusnya", $scope.mData);
                            if ($scope.mData.JobDate !== undefined && $scope.mData.JobDate !== null) {
                                if($scope.mData.JobDate == "NaN/NaN/NaN" || $scope.mData.JobDate == ""){
                                    $scope.mData.JobDate  = new Date();
                                    
                                }
                                $scope.mData.JobDate = $scope.changeFormatDate($scope.mData.JobDate);
                            } else {
                                var dateJob = new Date();
                                $scope.mData.JobDate = $scope.changeFormatDate(dateJob);
                            }
                            if ($scope.totalDpDefault !== 0 && ($scope.totalDp < $scope.totalDpDefault)) {
                                AppointmentGrService.checkApproval($scope.totalDp).then(function(res) {
                                    console.log("ressss checkApproval", res.data);
                                    var ApproverRoleID = res.data;
                                    if (res.data !== -1) {
                                        console.log('kondisi 1 | $scope.mData ===>',$scope.mData);
                                        AppointmentGrService.createAppointment($scope.mData).then(function(res) {
                                            if (res.data == -1) {
                                                bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                                $scope.mData.Km = backupKm;
                                            } else if (res.data == -2) {
                                                bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                                $scope.mData.Km = backupKm;
                                            } else {
                                                var tmpDataJob = angular.copy($scope.mData);
                                                var tmpJobId = res.data.ResponseMessage;
                                                tmpJobId = tmpJobId.split('#');
                                                console.log("tmpJobId", tmpJobId[1]);
                                                tmpJobId[1] = parseInt(tmpJobId[1])
                                                tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                                tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                                // =======================
                                                $scope.saveApprovalDP(tmpDataJob);
                                                // =======================
                                                if ($scope.mData.MRSId !== undefined) {
                                                    AppointmentGrService.updateMRS($scope.mData.MRSId).then(function(res) {

                                                    });
                                                }
                                                console.log("tmpPrediagnose", tmpPrediagnose);
                                                tmpPrediagnose.JobId = tmpJobId[1];
                                                if (tmpPrediagnose.fromAppointment !== undefined && tmpPrediagnose.StallId !== null) {
                                                    console.log("tmpPrediagnose.fromAppointment ada preDiagnose");
                                                    AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                                    });
                                                }
                                                AppointmentGrService.getNoAppointment(tmpJobId[1]).then(function(resu) {

                                                    console.log('getNoAppointment', resu.data);
                                                    // $scope.alertAfterSave(resu.data)
                                                    var dataNo = resu.data;
                                                    $scope.AppointmentNo = angular.copy(resu.data.AppointmentNo);
                                                    var dataNoApp = dataNo.AppointmentNo.replace(/\//g, '%20');
                                                    console.log('dataNoApp anitaaaa=>', dataNoApp);
                                                    AppointmentGrService.GetMaterialReqNo(dataNoApp).then(function(resu2) {
                                                        console.log("resu2", resu2.data);
                                                        $scope.dataMatNo;
                                                        if (resu2.data != null) {
                                                            $scope.dataMatNo = resu2.data[0].MaterialRequestNo;
                                                        } else {
                                                            $scope.dataMatNo = "-";
                                                        }
                                                    });
                                                    AppointmentGrService.publishPrintCetakan(tmpJobId[1], $scope.AppointmentNo).then(function(resu2) {

                                                        console.log('resu2 >>>', resu2.data);
                                                        $scope.AttachementURL = resu2.data;
                                                        $scope.getParameterEmail(row);
                                                        $scope.dataGridEmail.AttachmentURL = $scope.AttachementURL;
                                                        $scope.dataGridEmail.Email = $scope.mDataDetail.Email;
                                                        $scope.dataGridEmail.Name = row.Name;
                                                        $scope.dataGridEmail.LicensePlate = row.LicensePlate;
                                                        $scope.dataGridEmail.VIN = row.VIN;
                                                        $scope.show_modal.email = true;
                                                        $scope.filter.noPolice = null;
                                                        $scope.filter.noRangka = null;
                                                        $scope.alertAfterSave(dataNo, $scope.dataMatNo);
                                                    });

                                                    // var obj = {};
                                                    // obj.Email = $scope.mDataDetail.Email;
                                                    // obj.Name = row.Name;
                                                    // obj.LicensePlate = row.LicensePlate;

                                                    // $scope.gridEmail.data = $scope.dataGridEmail;
                                                    //show modal email

                                                });


                                                //kirim notifikasi Perubahan DP
                                                var DataNotif = {};
                                                var messagetemp = "";
                                                 messagetemp =   "No. Appointment : " + $scope.mData.AppointmentNo  
                                                                +" <br> No. Polisi : " + $scope.mData.PoliceNumber  
                                                                +" <br> DP Awal : "+ $scope.totalDpDefault 
                                                                +" <br> DP Akhir : "+ $scope.totalDp ;
                                                                
                                                

                                                DataNotif.Message = messagetemp;

                                                FollowUpGrService.sendNotif(DataNotif, 1128, 19).then(
                                                function(res) {

                                                },
                                                    function(err) {
                                                    //console.log("err=>", err);
                                                });

                                                console.log("abis save", res);
                                                // if (row.Category == undefined) {
                                                //     bsAlert.alert({
                                                //             title: "Pilih Category terlebih dahulu sebelum mengirim Email",
                                                //             // text: item.AppointmentNo,
                                                //             type: "warning",
                                                //             // showCancelButton: false
                                                //         },
                                                //         function() {},
                                                //         function() {}
                                                //     )
                                                // } else {

                                                // };
                                                // $scope.formApi.setMode('grid');
                                            }
                                        }, function(err) {
                                            $scope.mData.Km = backupKm;
                                        });
                                    } else {
                                        $scope.mData.Km = backupKm;
                                    }
                                });
                            } else {
                                console.log('kondisi 2 | $scope.mData ===>',$scope.mData);
                                AppointmentGrService.createAppointment($scope.mData).then(function(res) {
                                    if (res.data == -1) {
                                        bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                        $scope.mData.Km = backupKm;
                                    } else if (res.data == -2) {
                                        bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                        $scope.mData.Km = backupKm;
                                    } else {
                                        var tmpJobId = res.data.ResponseMessage;
                                        tmpJobId = tmpJobId.split('#');
                                        console.log("tmpJobId", tmpJobId[1]);
                                        console.log("tmpPrediagnose", tmpPrediagnose);
                                        tmpJobId[1] = parseInt(tmpJobId[1])
                                        tmpPrediagnose.JobId = parseInt(tmpJobId[1]);
                                        if ($scope.mData.MRSId !== undefined) {
                                            AppointmentGrService.updateMRS($scope.mData.MRSId).then(function(res) {

                                            });
                                        }
                                        AppointmentGrService.getNoAppointment(tmpJobId[1]).then(function(resu) {
                                            console.log('getNoAppointment', resu.data);
                                            // $scope.alertAfterSave(resu.data)
                                            var dataNo = resu.data;
                                            $scope.AppointmentNo = angular.copy(resu.data.AppointmentNo);
                                            var dataNoApp = dataNo.AppointmentNo.replace(/\//g, '%20');
                                            AppointmentGrService.GetMaterialReqNo(dataNoApp).then(function(resu2) {
                                                console.log("resu2", resu2.data);
                                                $scope.dataMatNo;
                                                if (resu2.data != null) {
                                                    $scope.dataMatNo = resu2.data[0].MaterialRequestNo;
                                                } else {
                                                    $scope.dataMatNo = "-";
                                                }
                                            });
                                            AppointmentGrService.publishPrintCetakan(tmpJobId[1], $scope.AppointmentNo).then(function(resu2) {
                                                console.log('resu2 >>>', resu2.data);
                                                $scope.AttachementURL = resu2.data;
                                                $scope.getParameterEmail(row);
                                                $scope.dataGridEmail.AttachmentURL = $scope.AttachementURL;
                                                $scope.dataGridEmail.Email = $scope.mDataDetail.Email;
                                                $scope.dataGridEmail.Name = row.Name;
                                                $scope.dataGridEmail.LicensePlate = row.LicensePlate;
                                                $scope.dataGridEmail.VIN = row.VIN;
                                                $scope.show_modal.email = true;
                                                $scope.filter.noPolice = null;
                                                $scope.filter.noRangka = null;
                                                $scope.alertAfterSave(dataNo, $scope.dataMatNo);
                                            });
                                        });
                                        if (tmpPrediagnose.fromAppointment !== undefined && tmpPrediagnose.StallId !== null) {
                                            console.log("tmpPrediagnose.fromAppointment ada preDiagnose");
                                            AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                            });
                                        }
                                    }
                                }, function(err) {
                                    $scope.mData.Km = backupKm;
                                });
                            }
                        } else {
                            console.log("Masih salah katanya");
                        };

                    } else {
                        bsAlert.warning("Form masih ada yang belum terisi", "silahkan cek kembali");
                    };
                },
                title: 'Simpan & Kirim Email',
                icon: '',
                // visible: $scope.disableEmail,
                //rightsBit: 1
            },
            {
                func: function(row, formScope) {
                    // console.log("row",row);

                    console.log('click simpan aja terus', $scope.actionButtonSettingsDetail)

                    if ($scope.loadingSimpan > 0) {
                        bsNotify.show({
                            size: 'small',
                            type: 'danger',
                            title: "Data sedang proses simpan..",
                            // timeout: 1000,
                        });
                        return false;
                    } else {
                        $scope.loadingSimpan++
                        $timeout(function() {
                            $scope.loadingSimpan = 0
                        }, 30000);

                        var tmpKm = angular.copy($scope.mData.Km).toString();
                        var backupKm = angular.copy($scope.mData.Km);
                        if (tmpKm.includes(",") == true) {
                            tmpKm = tmpKm.split(',');
                            if (tmpKm.length > 1) {
                                tmpKm = tmpKm.join('');
                            } else {
                                tmpKm = tmpKm[0];
                            }
                        }
                        tmpKm = parseInt(tmpKm);

                        if (tmpKm < $scope.KMTerakhir && $scope.KMTerakhir < 999999) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "KM tidak boleh lebih kecil dari " + $scope.KMTerakhir 
                            });
                            $scope.loadingSimpan = 0
                            return false;
                        }
                        
                        var tmpGrid = [];
                        tmpGrid = angular.copy($scope.gridWork);
                        console.log("mData", $scope.mData);
                        console.log(AsbGrFactory.checkChipFinished($scope.boardName))
                        // if ((gridTemp.length > 0 || !(angular.equals({}, tmpPrediagnose))) && $scope.mData.AppointmentDate !== undefined && $scope.mData.ContactPerson !== undefined && $scope.mData.PhoneContactPerson1 !== undefined && $scope.mData.WoCategoryId !== undefined && $scope.mData.Km !== undefined && ($scope.mDataDetail.LicensePlate !== undefined && $scope.mDataDetail.LicensePlate !== null)) {
                        if ($scope.mData.AppointmentDate !== undefined && $scope.mData.ContactPerson !== undefined && $scope.mData.PhoneContactPerson1 !== undefined && $scope.mData.WoCategoryId !== undefined && $scope.mData.Km !== undefined && ($scope.mDataDetail.LicensePlate !== undefined && $scope.mDataDetail.LicensePlate !== null && $scope.mDataDetail.LicensePlate !== '')) {
                            if (($scope.mData.PrediagScheduledTime !== null && $scope.mData.PrediagScheduledTime !== undefined) && $scope.gridWork.length == 0) {
                                console.log("hanya prediagnose");
                                var tmpKm = angular.copy($scope.mData.Km);
                                var backupKm = angular.copy($scope.mData.Km);
                                if (tmpKm.includes(",")) {
                                    tmpKm = tmpKm.split(',');
                                    if (tmpKm.length > 1) {
                                        tmpKm = tmpKm.join('');
                                    } else {
                                        tmpKm = tmpKm[0];
                                    }
                                }
                                tmpKm = parseInt(tmpKm);
                                $scope.mData.KmNormal = tmpKm;
                                for (var i = 0; i < tmpGrid.length; i++) {
                                    tmpGrid[i].JobParts = [];
                                    angular.forEach(tmpGrid[i].child, function(v, k) {
                                        tmpGrid[i].JobParts[k] = v;
                                        if (tmpGrid[i].JobParts[k].PaidById == 2277 || tmpGrid[i].JobParts[k].PaidById == 30 || tmpGrid[i].JobParts[k].PaidById == 31) {
                                            tmpGrid[i].JobParts[k].minimalDp = 0;
                                        }
                                        tmpGrid[i].JobParts[k].Price = tmpGrid[i].JobParts[k].RetailPrice;
                                        tmpGrid[i].JobParts[k].DPRequest = tmpGrid[i].JobParts[k].DownPayment;
                                        delete tmpGrid[i].JobParts[k].ETA;
                                        delete tmpGrid[i].JobParts[k].PaidBy;
                                        // delete tmpGrid[i].JobParts[k].Dp;
                                        delete tmpGrid[i].child;
                                    })
                                    if ($scope.mData.JobId !== undefined && $scope.mData.JobId !== null) {
                                        tmpGrid[i].JobId = $scope.mData.JobId;
                                    } else {
                                        tmpGrid[i].JobId = 0;
                                    }
                                    delete tmpGrid[i].PaidBy;

                                }
                                console.log("=====>", tmpGrid);
                                $scope.mData.JobTask = tmpGrid;
                                $scope.mData.JobComplaint = $scope.JobComplaint;
                                $scope.mData.JobRequest = $scope.JobRequest;
                                $scope.mData.detail = $scope.mDataDetail;
                                tmpPrediagnose.ScheduledTime = $filter('date')($scope.mData.PrediagScheduledTime, 'yyyy-MM-dd HH:mm:ss');
                                tmpPrediagnose.StallId = $scope.mData.PrediagStallId;
                                tmpPrediagnose.Status = $scope.mData.StatusPreDiagnose;

                                if ($scope.mData.PrediagStallId == null && tmpPrediagnose.fromAppointment == undefined && tmpPrediagnose.StallId == null) {
                                    $scope.mData.StatusPreDiagnose = null;
                                }
                                var tmpData = angular.copy($scope.mData);
                                var tmpAppointmentDate = tmpData.AppointmentDate;
                                tmpAppointmentDate = new Date(tmpAppointmentDate);
                                var finalDate
                                var yyyy = tmpAppointmentDate.getFullYear().toString();
                                var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                                var dd = tmpAppointmentDate.getDate().toString();
                                finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                                $scope.mData.AppointmentDate = finalDate;
                                $scope.mData.totalDpDefault = $scope.totalDpDefault;
                                $scope.mData.totalDp = $scope.totalDp;
                                $scope.mData.totalDpDefault = $scope.totalDpDefault;
                                console.log("$scope.mData dengan DP harusnya", $scope.mData);
                                console.log("$scope.mData tmpPrediagnose", tmpPrediagnose);
                                if ($scope.mData.JobDate !== undefined && $scope.mData.JobDate !== null) {
                                    if($scope.mData.JobDate == "NaN/NaN/NaN" || $scope.mData.JobDate == ""){
                                        $scope.mData.JobDate  = new Date();
                                        
                                    }
                                    $scope.mData.JobDate = $scope.changeFormatDate($scope.mData.JobDate);
                                } else {
                                    var dateJob = new Date();
                                    $scope.mData.JobDate = $scope.changeFormatDate(dateJob);
                                }

                                if ($scope.totalDpDefault !== 0 && ($scope.totalDp < $scope.totalDpDefault)) {
                                    AppointmentGrService.checkApproval($scope.totalDp).then(function(res) {
                                        console.log("ressss checkApproval", res.data);
                                        var ApproverRoleID = res.data;
                                        if (res.data !== -1) {
                                            console.log('kondisi 3 | $scope.mData ===>',$scope.mData);
                                            AppointmentGrService.createAppointment($scope.mData).then(function(res) {
                                                if (res.data == -1) {
                                                    bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                                    $scope.mData.Km = backupKm;
                                                    $scope.loadingSimpan = 0
                                                } else if (res.data == -2) {
                                                    bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                                    $scope.mData.Km = backupKm;
                                                    $scope.loadingSimpan = 0
                                                } else {
                                                    var tmpDataJob = angular.copy($scope.mData);
                                                    var tmpJobId = res.data.ResponseMessage;
                                                    tmpJobId = tmpJobId.split('#');
                                                    console.log("tmpJobId", tmpJobId[1]);
                                                    tmpJobId[1] = parseInt(tmpJobId[1])
                                                    tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                                    tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                                    // =======================
                                                    $scope.saveApprovalDP(tmpDataJob);
                                                    // =======================
                                                    if ($scope.mData.MRSId !== undefined) {
                                                        AppointmentGrService.updateMRS($scope.mData.MRSId).then(function(res) {

                                                        });
                                                    }
                                                    console.log("tmpPrediagnose", tmpPrediagnose);
                                                    tmpPrediagnose.JobId = tmpJobId[1];

                                                    AppointmentGrService.getNoAppointment(tmpJobId[1]).then(function(resu) {
                                                        console.log('getNoAppointment', resu.data);
                                                        var dataNo = resu.data;
                                                        var dataNoApp = dataNo.AppointmentNo.replace(/\//g, '%20');
                                                        AppointmentGrService.GetMaterialReqNo(dataNoApp).then(function(resu2) {
                                                            console.log("resu2", resu2.data);
                                                            var dataMatNo;
                                                            if (resu2.data != null) {
                                                                dataMatNo = resu2.data[0].MaterialRequestNo;
                                                            } else {
                                                                dataMatNo = "-";
                                                            }
                                                            $scope.alertAfterSave(dataNo, dataMatNo);
                                                        });
                                                    });
                                                    if (tmpPrediagnose.fromAppointment !== undefined && tmpPrediagnose.StallId !== null) {
                                                        console.log("tmpPrediagnose.fromAppointment ada preDiagnose");
                                                        AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                                        });
                                                    }

                                                    //kirim notifikasi Perubahan DP
                                                    var DataNotif = {};
                                                    var messagetemp = "";
                                                    messagetemp =   "No. Appointment : " + $scope.mData.AppointmentNo
                                                                    +" <br> No. Polisi : " + $scope.mData.PoliceNumber
                                                                    +" <br> DP Awal : "+ $scope.totalDpDefault
                                                                    +" <br> DP Akhir : "+ $scope.totalDp;
                                                                    
                                                    

                                                    DataNotif.Message = messagetemp;

                                                    FollowUpGrService.sendNotif(DataNotif, 1128, 19).then(
                                                    function(res) {

                                                    },
                                                        function(err) {
                                                        //console.log("err=>", err);
                                                    });


                                                    console.log("abis save", res);
                                                    // $scope.formApi.setMode('grid');
                                                }
                                            }, function(err) {
                                                $scope.mData.Km = backupKm;
                                            });
                                        } else {
                                            $scope.mData.Km = backupKm;
                                        }
                                    });
                                } else {
                                    console.log("Gak ada dp");
                                    console.log('kondisi 4 | $scope.mData ===>',$scope.mData);
                                    AppointmentGrService.createAppointment($scope.mData).then(function(res) {
                                        if (res.data == -1) {
                                            bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                            $scope.mData.Km = backupKm;
                                            $scope.loadingSimpan = 0
                                        } else if (res.data == -2) {
                                            bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                            $scope.mData.Km = backupKm;
                                            $scope.loadingSimpan = 0
                                        } else {
                                            var tmpJobId = res.data.ResponseMessage;
                                            tmpJobId = tmpJobId.split('#');
                                            console.log("tmpJobId", tmpJobId[1]);
                                            console.log("tmpPrediagnose", tmpPrediagnose);
                                            tmpJobId[1] = parseInt(tmpJobId[1])
                                            tmpPrediagnose.JobId = parseInt(tmpJobId[1]);
                                            if ($scope.mData.MRSId !== undefined) {
                                                AppointmentGrService.updateMRS($scope.mData.MRSId).then(function(res) {

                                                });
                                            }
                                            AppointmentGrService.getNoAppointment(tmpJobId[1]).then(function(resu) {
                                                console.log('getNoAppointment', resu.data);
                                                var dataNo = resu.data;
                                                var dataNoApp = dataNo.AppointmentNo.replace(/\//g, '%20');
                                                console.log("dataNoApp", dataNoApp);
                                                AppointmentGrService.GetMaterialReqNo(dataNoApp).then(function(resu2) {
                                                    console.log("resu2", resu2.data);
                                                    var dataMatNo;
                                                    if (resu2.data != null) {
                                                        dataMatNo = resu2.data[0].MaterialRequestNo;
                                                    } else {
                                                        dataMatNo = "-";
                                                    }
                                                    $scope.alertAfterSave(dataNo, dataMatNo);
                                                });
                                            });
                                            if (tmpPrediagnose.fromAppointment !== undefined && tmpPrediagnose.StallId !== null) {
                                                console.log("tmpPrediagnose.fromAppointment ada preDiagnose");
                                                AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                                });
                                            }
                                            console.log("abis save", res);
                                            // $scope.formApi.setMode('grid');
                                        }
                                    }, function(err) {
                                        $scope.mData.Km = backupKm;
                                    });
                                }
                            } else {
                                if (AsbGrFactory.checkChipFinished($scope.boardName) == 1) {
                                    var splitChip = AsbGrFactory.getChipData($scope.boardName);
                                    splitChip = splitChip.split(';');
                                    var tmpJobSplitChip = [];
                                    for (var i in splitChip) {
                                        var tmpHasilSplit = splitChip[i].split('|');
                                        tmpJobSplitChip.push({
                                            JobSplitId: 0,
                                            JobId: 0,
                                            PlanStart: tmpHasilSplit[1],
                                            PlanFinish: tmpHasilSplit[2],
                                            PlanDateStart: tmpHasilSplit[0],
                                            PlanDateFinish: tmpHasilSplit[0],
                                            StallId: tmpHasilSplit[3],
                                            isSplitActive: 1
                                        })
                                    }
                                    if (tmpJobSplitChip.length > 1) {
                                        $scope.mData.JobListSplitChip = tmpJobSplitChip;
                                    } else {
                                        $scope.mData.JobListSplitChip = []
                                    }
                                    var tmpKm = angular.copy($scope.mData.Km);
                                    var backupKm = angular.copy($scope.mData.Km);
                                    if (tmpKm.includes(",")) {
                                        tmpKm = tmpKm.split(',');
                                        if (tmpKm.length > 1) {
                                            tmpKm = tmpKm.join('');
                                        } else {
                                            tmpKm = tmpKm[0];
                                        }
                                    }
                                    tmpKm = parseInt(tmpKm);
                                    $scope.mData.KmNormal = tmpKm;
                                    for (var i = 0; i < tmpGrid.length; i++) {
                                        tmpGrid[i].JobParts = [];
                                        angular.forEach(tmpGrid[i].child, function(v, k) {
                                            tmpGrid[i].JobParts[k] = v;
                                            if (tmpGrid[i].JobParts[k].PaidById == 2277 || tmpGrid[i].JobParts[k].PaidById == 30 || tmpGrid[i].JobParts[k].PaidById == 31) {
                                                tmpGrid[i].JobParts[k].minimalDp = 0;
                                            }
                                            tmpGrid[i].JobParts[k].Price = tmpGrid[i].JobParts[k].RetailPrice;
                                            tmpGrid[i].JobParts[k].DPRequest = tmpGrid[i].JobParts[k].DownPayment;
                                            delete tmpGrid[i].JobParts[k].ETA;
                                            delete tmpGrid[i].JobParts[k].PaidBy;
                                            // delete tmpGrid[i].JobParts[k].Dp;
                                            delete tmpGrid[i].child;
                                        })
                                        if ($scope.mData.JobId !== undefined && $scope.mData.JobId !== null) {
                                            tmpGrid[i].JobId = $scope.mData.JobId;
                                        } else {
                                            tmpGrid[i].JobId = 0;
                                        }
                                        delete tmpGrid[i].PaidBy;

                                    }
                                    console.log("=====>", tmpGrid);
                                    $scope.mData.JobTask = tmpGrid;
                                    $scope.mData.JobComplaint = $scope.JobComplaint;
                                    $scope.mData.JobRequest = $scope.JobRequest;
                                    $scope.mData.detail = $scope.mDataDetail;
                                    tmpPrediagnose.ScheduledTime = $filter('date')($scope.mData.PrediagScheduledTime, 'yyyy-MM-dd HH:mm:ss');
                                    tmpPrediagnose.StallId = $scope.mData.PrediagStallId;
                                    tmpPrediagnose.Status = $scope.mData.StatusPreDiagnose;

                                    if ($scope.mData.PrediagStallId == null && tmpPrediagnose.fromAppointment == undefined && tmpPrediagnose.StallId == null) {
                                        $scope.mData.StatusPreDiagnose = null;
                                    }
                                    var tmpData = angular.copy($scope.mData);
                                    var tmpAppointmentDate = tmpData.AppointmentDate;
                                    tmpAppointmentDate = new Date(tmpAppointmentDate);
                                    var finalDate
                                    var yyyy = tmpAppointmentDate.getFullYear().toString();
                                    var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                                    var dd = tmpAppointmentDate.getDate().toString();
                                    finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                                    $scope.mData.AppointmentDate = finalDate;
                                    $scope.mData.totalDpDefault = $scope.totalDpDefault;
                                    $scope.mData.totalDp = $scope.totalDp;
                                    $scope.mData.totalDpDefault = $scope.totalDpDefault;
                                    console.log("$scope.mData dengan DP harusnya", $scope.mData);
                                    console.log("$scope.mData tmpPrediagnose", tmpPrediagnose);
                                    if ($scope.mData.JobDate !== undefined && $scope.mData.JobDate !== null) {
                                        if($scope.mData.JobDate == "NaN/NaN/NaN" || $scope.mData.JobDate == ""){
                                            $scope.mData.JobDate  = new Date();
                                            
                                        }
                                        $scope.mData.JobDate = $scope.changeFormatDate($scope.mData.JobDate);
                                    } else {
                                        var dateJob = new Date();
                                        $scope.mData.JobDate = $scope.changeFormatDate(dateJob);
                                    }

                                    if ($scope.totalDpDefault !== 0 && ($scope.totalDp < $scope.totalDpDefault)) {
                                        AppointmentGrService.checkApproval($scope.totalDp).then(function(res) {
                                            console.log("ressss checkApproval", res.data);
                                            var ApproverRoleID = res.data;
                                            if (res.data !== -1) {
                                                console.log('kondisi 5 | $scope.mData ===>',$scope.mData);
                                                AppointmentGrService.createAppointment($scope.mData).then(function(res) {
                                                    if (res.data == -1) {
                                                        bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                                        $scope.mData.Km = backupKm;
                                                        $scope.loadingSimpan = 0
                                                    } else if (res.data == -2) {
                                                        bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                                        $scope.mData.Km = backupKm;
                                                        $scope.loadingSimpan = 0
                                                    } else {
                                                        var tmpDataJob = angular.copy($scope.mData);
                                                        var tmpJobId = res.data.ResponseMessage;
                                                        tmpJobId = tmpJobId.split('#');
                                                        console.log("tmpJobId", tmpJobId[1]);
                                                        tmpJobId[1] = parseInt(tmpJobId[1])
                                                        tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                                        tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                                        // =======================
                                                        $scope.saveApprovalDP(tmpDataJob);
                                                        // =======================
                                                        console.log("tmpPrediagnose", tmpPrediagnose);
                                                        tmpPrediagnose.JobId = tmpJobId[1];
                                                        if ($scope.mData.MRSId !== undefined) {
                                                            AppointmentGrService.updateMRS($scope.mData.MRSId).then(function(res) {

                                                            });
                                                        }
                                                        AppointmentGrService.getNoAppointment(tmpJobId[1]).then(function(resu) {
                                                            console.log('getNoAppointment', resu.data);
                                                            var dataNo = resu.data;
                                                            var dataNoApp = dataNo.AppointmentNo.replace(/\//g, '%20');
                                                            AppointmentGrService.GetMaterialReqNo(dataNoApp).then(function(resu2) {
                                                                console.log("resu2", resu2.data);
                                                                var dataMatNo;
                                                                if (resu2.data != null) {
                                                                    dataMatNo = resu2.data[0].MaterialRequestNo;
                                                                } else {
                                                                    dataMatNo = "-";
                                                                }
                                                                $scope.alertAfterSave(dataNo, dataMatNo);
                                                            });
                                                        });
                                                        if (tmpPrediagnose.fromAppointment !== undefined && tmpPrediagnose.StallId !== null) {
                                                            console.log("tmpPrediagnose.fromAppointment ada preDiagnose");
                                                            AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                                            });
                                                        }

                                                        //kirim notifikasi Perubahan DP
                                                        var DataNotif = {};
                                                        var messagetemp = "";
                                                        messagetemp =   "No. Appointment : " + $scope.mData.AppointmentNo
                                                                    +" <br> No. Polisi : " + $scope.mData.PoliceNumber
                                                                    +" <br> DP Awal : "+ $scope.totalDpDefault
                                                                    +" <br> DP Akhir : "+ $scope.totalDp ;
                                                                        
                                                        

                                                        DataNotif.Message = messagetemp;

                                                        FollowUpGrService.sendNotif(DataNotif, 1128, 19).then(
                                                        function(res) {

                                                        },
                                                            function(err) {
                                                            //console.log("err=>", err);
                                                        });
                                                        console.log("abis save", res);
                                                    
                                                        // $scope.formApi.setMode('grid');
                                                    }
                                                }, function(err) {
                                                    $scope.mData.Km = backupKm;
                                                });
                                            } else {
                                                $scope.mData.Km = backupKm;
                                            }
                                        });
                                    } else {
                                        console.log("Gak ada dp");
                                        console.log('kondisi 6 | $scope.mData ===>',$scope.mData);
                                        AppointmentGrService.createAppointment($scope.mData).then(function(res) {
                                            if (res.data == -1) {
                                                bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                                $scope.mData.Km = backupKm;
                                                $scope.loadingSimpan = 0
                                            } else if (res.data == -2) {
                                                bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                                $scope.mData.Km = backupKm;
                                                $scope.loadingSimpan = 0
                                            } else {
                                                var tmpJobId = res.data.ResponseMessage;
                                                tmpJobId = tmpJobId.split('#');
                                                console.log("tmpJobId", tmpJobId[1]);
                                                console.log("tmpPrediagnose", tmpPrediagnose);
                                                tmpJobId[1] = parseInt(tmpJobId[1])
                                                tmpPrediagnose.JobId = parseInt(tmpJobId[1]);
                                                if ($scope.mData.MRSId !== undefined) {
                                                    AppointmentGrService.updateMRS($scope.mData.MRSId).then(function(res) {

                                                    });
                                                }
                                                AppointmentGrService.getNoAppointment(tmpJobId[1]).then(function(resu) {
                                                    console.log('getNoAppointment', resu.data);
                                                    var dataNo = resu.data;
                                                    var dataNoApp = dataNo.AppointmentNo.replace(/\//g, '%20');
                                                    console.log("dataNoApp", dataNoApp);
                                                    AppointmentGrService.GetMaterialReqNo(dataNoApp).then(function(resu2) {
                                                        console.log("resu2", resu2.data);
                                                        var dataMatNo;
                                                        if (resu2.data != null) {
                                                            dataMatNo = resu2.data[0].MaterialRequestNo;
                                                        } else {
                                                            dataMatNo = "-";
                                                        }
                                                        $scope.alertAfterSave(dataNo, dataMatNo);
                                                    });
                                                });
                                                if (tmpPrediagnose.fromAppointment !== undefined && tmpPrediagnose.StallId !== null) {
                                                    console.log("tmpPrediagnose.fromAppointment ada preDiagnose");
                                                    AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                                    });
                                                }
                                                console.log("abis save", res);

                                                
                                                // $scope.formApi.setMode('grid');
                                            }
                                        }, function(err) {
                                            $scope.mData.Km = backupKm;
                                        });
                                    }
                                } else {
                                    console.log("Masih salah katanya");
                                    bsAlert.warning("Plotting stall belum selesai", "silahkan cek kembali");
                                    $scope.loadingSimpan = 0
                                };
                            };
                        } else {
                            bsAlert.warning("Form masih ada yang belum terisi", "silahkan cek kembali");
                            $scope.loadingSimpan = 0
                        };
                    }

                    
                },
                title: 'Simpan',
                icon: 'fa fa-fw fa-save',
                //rightsBit: 1
            },
            {
                title: 'Simpan & Print',
                icon: 'fa fa-fw fa-print',
                func: function(row, formScope) {

                    console.log('click simpan aja terus', $scope.actionButtonSettingsDetail)

                    if ($scope.loadingSimpan > 0) {
                        bsNotify.show({
                            size: 'small',
                            type: 'danger',
                            title: "Data sedang proses simpan..",
                            // timeout: 1000,
                        });
                        return false;
                    } else {
                        $scope.loadingSimpan++
                        $timeout(function() {
                            $scope.loadingSimpan = 0
                        }, 30000);

                        var tmpGrid = [];
                        tmpGrid = angular.copy($scope.gridWork);
                        console.log('tmpGrid before save ===>',tmpGrid);
    
                        var tmpKm = angular.copy($scope.mData.Km).toString();
                        var backupKm = angular.copy($scope.mData.Km);
                        if (tmpKm.includes(",") == true) {
                            tmpKm = tmpKm.split(',');
                            if (tmpKm.length > 1) {
                                tmpKm = tmpKm.join('');
                            } else {
                                tmpKm = tmpKm[0];
                            }
                        }
                        tmpKm = parseInt(tmpKm);
    
                        if (tmpKm < $scope.KMTerakhir && $scope.KMTerakhir < 999999) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "KM tidak boleh lebih kecil dari " + $scope.KMTerakhir 
                            });
                            $scope.loadingSimpan = 0
                            return false;
                        }
    
                        // if ((gridTemp.length > 0 || !(angular.equals({}, tmpPrediagnose))) && $scope.mData.AppointmentDate !== undefined && $scope.mData.ContactPerson !== undefined && $scope.mData.PhoneContactPerson1 !== undefined && $scope.mData.WoCategoryId !== undefined && $scope.mData.Km !== undefined && ($scope.mDataDetail.LicensePlate !== undefined && $scope.mDataDetail.LicensePlate !== null)) {
                        if ($scope.mData.AppointmentDate !== undefined && $scope.mData.ContactPerson !== undefined && $scope.mData.PhoneContactPerson1 !== undefined && $scope.mData.WoCategoryId !== undefined && $scope.mData.Km !== undefined && ($scope.mDataDetail.LicensePlate !== undefined && $scope.mDataDetail.LicensePlate !== null && $scope.mDataDetail.LicensePlate !== '')) {
                            if (AsbGrFactory.checkChipFinished($scope.boardName)) {
                                var splitChip = AsbGrFactory.getChipData($scope.boardName);
                                splitChip = splitChip.split(';');
                                var tmpJobSplitChip = [];
                                for (var i in splitChip) {
                                    var tmpHasilSplit = splitChip[i].split('|');
                                    tmpJobSplitChip.push({
                                        JobSplitId: 0,
                                        JobId: 0,
                                        PlanStart: tmpHasilSplit[1],
                                        PlanFinish: tmpHasilSplit[2],
                                        PlanDateStart: tmpHasilSplit[0],
                                        PlanDateFinish: tmpHasilSplit[0],
                                        StallId: tmpHasilSplit[3],
                                        isSplitActive: 1
                                    })
                                }
                                if (tmpJobSplitChip.length > 1) {
                                    $scope.mData.JobListSplitChip = tmpJobSplitChip;
                                } else {
                                    $scope.mData.JobListSplitChip = []
                                }
                                console.log("splitChip", splitChip);
                                var tmpKm = angular.copy($scope.mData.Km);
                                var backupKm = angular.copy($scope.mData.Km);
                                if (tmpKm.toString().includes(",")) {
                                    tmpKm = tmpKm.toString().split(',');
                                    if (tmpKm.length > 1) {
                                        tmpKm = tmpKm.join('');
                                    } else {
                                        tmpKm = tmpKm[0];
                                    }
                                }
                                tmpKm = parseInt(tmpKm);
                                $scope.mData.KmNormal = tmpKm;
                                for (var i = 0; i < tmpGrid.length; i++) {
                                    tmpGrid[i].JobParts = [];
                                    angular.forEach(tmpGrid[i].child, function(v, k) {
                                        tmpGrid[i].JobParts[k] = v;
                                        if (tmpGrid[i].JobParts[k].PaidById == 2277 || tmpGrid[i].JobParts[k].PaidById == 30 || tmpGrid[i].JobParts[k].PaidById == 31) {
                                            tmpGrid[i].JobParts[k].minimalDp = 0;
                                        }
                                        tmpGrid[i].JobParts[k].Price = tmpGrid[i].JobParts[k].RetailPrice;
                                        tmpGrid[i].JobParts[k].DPRequest = tmpGrid[i].JobParts[k].DownPayment;
                                        delete tmpGrid[i].JobParts[k].ETA;
                                        delete tmpGrid[i].JobParts[k].PaidBy;
                                        // delete tmpGrid[i].JobParts[k].Dp;
                                        delete tmpGrid[i].child;
                                    })
                                    if ($scope.mData.JobId !== undefined && $scope.mData.JobId !== null) {
                                        tmpGrid[i].JobId = $scope.mData.JobId;
                                    } else {
                                        tmpGrid[i].JobId = 0;
                                    }
                                    delete tmpGrid[i].PaidBy;
    
                                }
    
                 
                                //pindahan dari factory
                                if($scope.mData.IsEstimation == undefined || $scope.mData.IsEstimation == null ){
                                    $scope.mData.JobId = 0;
                                }
    
                                for(var i in tmpGrid){
                                    // if(tmpGrid[i].PaidById == 2277 || tmpGrid[i].PaidById == 30 || tmpGrid[i].PaidById == 31 || tmpGrid[i].PaidById == 32){
                                    if(tmpGrid[i].PaidById == 2277 || tmpGrid[i].PaidById == 30){
                                        tmpGrid[i].Discount = 0;
                                        tmpGrid[i].DiscountTypeId = -1;
    
                                        for(var j in tmpGrid[i].JobParts){
                                            // if(tmpGrid[i].JobParts[j].PaidById == 2277 || tmpGrid[i].JobParts[j].PaidById == 30 || tmpGrid[i].JobParts[j].PaidById == 31 || tmpGrid[i].JobParts[j].PaidById == 32){
                                            if(tmpGrid[i].JobParts[j].PaidById == 2277 || tmpGrid[i].JobParts[j].PaidById == 30){
                                                tmpGrid[i].JobParts[j].Discount = 0;
                                                tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                               
                                            }else{
                                                // if(tmpGrid[i].JobParts[j].MaterialTypeId == 1){
        
                                                //    tmpGrid[i].JobParts[j].Discount = data.disParts;
                                                    tmpGrid[i].JobParts[j].DiscountTypeId = 10;
                                                // }
                                                // else{
                                                //     tmpGrid[i].JobParts[j].Discount = 0;
                                                //     tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                                // }
                                            }
                                        }
                                    }else{
                                        // tmpGrid[i].Discount = data.disTask;
                                        tmpGrid[i].DiscountTypeId = 10;
                                        for(var j in tmpGrid[i].JobParts){
                                            if(tmpGrid[i].JobParts[j].PaidById == 2277 || tmpGrid[i].JobParts[j].PaidById == 30){
                                                tmpGrid[i].JobParts[j].Discount = 0;
                                                tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                               
                                            }else{
                                                // if(tmpGrid[i].JobParts[j].MaterialTypeId == 1){
        
                                                //    tmpGrid[i].JobParts[j].Discount = data.disParts;
                                                    tmpGrid[i].JobParts[j].DiscountTypeId = 10;
                                                // }
                                                // else{
                                                //     tmpGrid[i].JobParts[j].Discount = 0;
                                                //     tmpGrid[i].JobParts[j].DiscountTypeId = -1;
                                                // }
                                            }
                                        }
                                        
                                    }
                                }
                                //pindahan dari factory
    
                                console.log("====asuuuu=>", tmpGrid);
                                $scope.mData.JobTask = tmpGrid;
                                $scope.mData.JobComplaint = $scope.JobComplaint;
                                $scope.mData.JobRequest = $scope.JobRequest;
                                $scope.mData.detail = $scope.mDataDetail;
                                tmpPrediagnose.ScheduledTime = $filter('date')($scope.mData.PrediagScheduledTime, 'yyyy-MM-dd HH:mm:ss');
                                tmpPrediagnose.StallId = $scope.mData.PrediagStallId;
                                tmpPrediagnose.Status = $scope.mData.StatusPreDiagnose;
    
                                if ($scope.mData.PrediagStallId == null && tmpPrediagnose.fromAppointment == undefined && tmpPrediagnose.StallId == null) {
                                    $scope.mData.StatusPreDiagnose = null;
                                }
    
    
                                var tmpData = angular.copy($scope.mData);
                                var tmpAppointmentDate = tmpData.AppointmentDate;
                                tmpAppointmentDate = new Date(tmpAppointmentDate);
                                var finalDate
                                var yyyy = tmpAppointmentDate.getFullYear().toString();
                                var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                                var dd = tmpAppointmentDate.getDate().toString();
                                finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                                $scope.mData.AppointmentDate = finalDate;
                                $scope.mData.totalDpDefault = $scope.totalDpDefault;
                                $scope.mData.totalDp = $scope.totalDp;
                                $scope.mData.totalDpDefault = $scope.totalDpDefault;
                                if ($scope.mData.JobDate !== undefined && $scope.mData.JobDate !== null) {
                                    if($scope.mData.JobDate == "NaN/NaN/NaN" || $scope.mData.JobDate == ""){
                                        $scope.mData.JobDate  = new Date();
                                        
                                    }
                                    $scope.mData.JobDate = $scope.changeFormatDate($scope.mData.JobDate);
                                } else {
                                    var dateJob = new Date();
                                    $scope.mData.JobDate = $scope.changeFormatDate(dateJob);
                                }
                                if ($scope.totalDpDefault !== 0 && ($scope.totalDp < $scope.totalDpDefault)) {
                                    AppointmentGrService.checkApproval($scope.totalDp).then(function(res) {
                                        console.log("ressss checkApproval", res.data);
                                        var ApproverRoleID = res.data;
                                        if (res.data !== -1) {
                                            console.log('kondisi 7 | $scope.mData ===>',JSON.stringify($scope.mData));
                                            AppointmentGrService.createAppointment($scope.mData).then(function(res) {
                                                if (res.data == -1) {
                                                    bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                                    $scope.mData.Km = backupKm;
                                                    $scope.loadingSimpan = 0
                                                } else if (res.data == -2) {
                                                    bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                                    $scope.mData.Km = backupKm;
                                                    $scope.loadingSimpan = 0
                                                } else {
                                                    var tmpDataJob = angular.copy($scope.mData);
                                                    var tmpJobId = res.data.ResponseMessage;
                                                    tmpJobId = tmpJobId.split('#');
                                                    console.log("tmpJobId", tmpJobId[1]);
                                                    tmpJobId[1] = parseInt(tmpJobId[1])
                                                    tmpDataJob.JobId = parseInt(tmpJobId[1]);
                                                    tmpDataJob.ApproverRoleId = parseInt(ApproverRoleID);
                                                    // =======================
                                                    $scope.saveApprovalDP(tmpDataJob);
                                                    $scope.cetakAppointment(tmpJobId[1]);
                                                    // =======================
                                                    if ($scope.mData.MRSId !== undefined) {
                                                        AppointmentGrService.updateMRS($scope.mData.MRSId).then(function(res) {
    
                                                        });
                                                    }
                                                    console.log("tmpPrediagnose", tmpPrediagnose);
                                                    tmpPrediagnose.JobId = tmpJobId[1];
    
                                                    AppointmentGrService.getNoAppointment(tmpJobId[1]).then(function(resu) {
                                                        console.log('getNoAppointment', resu.data);
                                                        var dataNo = resu.data;
                                                        var dataNoApp = dataNo.AppointmentNo.replace(/\//g, '%20');
    
                                                        console.log("dataNoApp", dataNoApp);
                                                        AppointmentGrService.GetMaterialReqNo(dataNoApp).then(function(resu2) {
                                                            console.log("resu2", resu2.data);
                                                            var dataMatNo;
                                                            if (resu2.data != null) {
                                                                dataMatNo = resu2.data[0].MaterialRequestNo;
                                                            } else {
                                                                dataMatNo = "-";
                                                            }
                                                            $scope.alertAfterSave(dataNo, dataMatNo);
                                                        });
                                                    });
                                                    if (tmpPrediagnose.fromAppointment !== undefined && tmpPrediagnose.StallId !== null) {
                                                        console.log("tmpPrediagnose.fromAppointment ada preDiagnose");
                                                        AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {
    
                                                        });
                                                    }
    
                                                    //kirim notifikasi Perubahan DP
                                                    var DataNotif = {};
                                                    var messagetemp = "";
                                                     messagetemp =   "No. Appointment : " + $scope.mData.AppointmentNo
                                                                    +"<br> No. Polisi : " + $scope.mData.PoliceNumber
                                                                    +"<br> DP Awal : "+ $scope.totalDpDefault
                                                                    +"<br> DP Akhir : "+ $scope.totalDp;
                                                                    
                                                    
    
                                                    DataNotif.Message = messagetemp;
    
                                                    FollowUpGrService.sendNotif(DataNotif, 1128, 19).then(
                                                    function(res) {
    
                                                    },
                                                        function(err) {
                                                        //console.log("err=>", err);
                                                    });
    
                                                    console.log("abis save", res);
                                                    $scope.getData(true);
                                                    $scope.formApi.setMode('grid');
                                                }
                                            }, function(err) {
                                                $scope.mData.Km = backupKm;
                                            });
                                        } else {
                                            $scope.mData.Km = backupKm;
                                        }
                                    });
                                } else {
                                    console.log("Gak ada dp");
                                    console.log('kondisi 8 | $scope.mData ===>',JSON.stringify($scope.mData));
                                    AppointmentGrService.createAppointment($scope.mData).then(function(res) {
                                        if (res.data == -1) {
                                            bsAlert.warning("Kendaraan dengan No. Polisi " + $scope.mDataDetail.LicensePlate + " sudah memiliki Appointment dengan tanggal " + finalDate, "silahkan cek kembali");
                                            $scope.mData.Km = backupKm;
                                            $scope.loadingSimpan = 0
                                        } else if (res.data == -2) {
                                            bsAlert.warning("Plotting Stall Gagal, chip bentrok dengan Kendaraan lain", "silahkan cek kembali");
                                            $scope.mData.Km = backupKm;
                                            $scope.loadingSimpan = 0
                                        } else {
                                            var tmpJobId = res.data.ResponseMessage;
                                            tmpJobId = tmpJobId.split('#');
                                            console.log("tmpJobId", tmpJobId[1]);
                                            console.log("tmpPrediagnose", tmpPrediagnose);
                                            tmpJobId[1] = parseInt(tmpJobId[1])
                                            tmpPrediagnose.JobId = parseInt(tmpJobId[1]);
                                            $scope.cetakAppointment(tmpJobId[1]);
                                            if ($scope.mData.MRSId !== undefined) {
                                                AppointmentGrService.updateMRS($scope.mData.MRSId).then(function(res) {
    
                                                });
                                            }
                                            AppointmentGrService.getNoAppointment(tmpJobId[1]).then(function(resu) {
                                                console.log('getNoAppointment', resu.data);
                                                var dataNo = resu.data;
                                                var dataNoApp = dataNo.AppointmentNo.replace(/\//g, '%20');
    
                                                console.log("dataNoApp", dataNoApp);
                                                AppointmentGrService.GetMaterialReqNo(dataNoApp).then(function(resu2) {
                                                    console.log("resu2", resu2.data);
                                                    var dataMatNo;
                                                    if (resu2.data != null) {
                                                        dataMatNo = resu2.data[0].MaterialRequestNo;
                                                    } else {
                                                        dataMatNo = "-";
                                                    }
                                                    $scope.alertAfterSave(dataNo, dataMatNo);
    
                                                });
                                            });
                                            if (tmpPrediagnose.fromAppointment !== undefined && tmpPrediagnose.StallId !== null) {
                                                console.log("tmpPrediagnose.fromAppointment ada preDiagnose");
                                                AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {
    
                                                });
                                            }
                                            console.log("abis save", res);
                                            $scope.getData(true);
    
                                            
    
                                            $scope.formApi.setMode('grid');
                                        }
                                    }, function(err) {
                                        $scope.mData.Km = backupKm;
                                    });
                                }

                            } else {
                                console.log("Masih salah katanya");
                                $scope.loadingSimpan = 0
    
                            };
                        } else {
                            bsAlert.warning("Form masih ada yang belum terisi", "silahkan cek kembali");
                            $scope.loadingSimpan = 0
                        };
                    }
                    
                },
                type: 'custom' //for link
            },
            {
                actionType: 'back', //Use 'Back Action' of bsForm
                title: 'Back',
                //                if($scope.visibleAsb) {
                //                    enable: 'false';
                //                }else{
                //                    enable: 'true';
                //                }
                //icon: 'fa fa-fw fa-edit',
            },
        ];
        // ===========;=============================

        $scope.alertAfterSave = function(item, item2) {
            bsAlert.alert({
                    title: "Data tersimpan",
                    text: "Dengan Nomor Appointment : " + item.AppointmentNo + " dan Nomor Material Request : " + item2,
                    type: "success",
                    showCancelButton: false
                },
                function() {
                    $scope.filter.noPolice = null;
                    $scope.filter.noRangka = null;
                    $scope.getData(true);
                    $scope.formApi.setMode('grid');
                    console.log('$scope.PartTersedia',$scope.PartTersedia);
                    console.log('$scope',$scope);
                    console.log('$scope.undefinedPart',$scope.undefinedPart);

                    //notif barang tidak Tersedia
                    if($scope.PartTersedia == false)
                    {
                          console.log("mData notif", $scope.mData);
                          var DataNotif = {};
                          var messagetemp = "";
                        //   for(var i = 0; i <= $scope.mData.JobTask[0].JobParts.length; i++)
                        //   {
                             
                        //   }
                         messagetemp =  "No. Material : " + $scope.mData.JobTask[0].JobParts[0].PartsCode  + " <br> " 
                                            +"Nama Material : " + $scope.mData.JobTask[0].JobParts[0].PartsName + " <br> " 
                                            +"QTY : "+ $scope.mData.JobTask[0].JobParts[0].Qty;
                          

                          DataNotif.Message = messagetemp;
                           //1122 = partsman GR
                           AppointmentGrService.sendNotif(DataNotif, 1122, 14).then(
                           function(res) {

                            },
                              function(err) {
                                                                                //console.log("err=>", err);
                              });
                    }
                     if($scope.undefinedPart == true)
                     {
                          console.log("mData notif", $scope.mData);
                          var DataNotif = {};
                           var messagetemp = "";
                          //for(var i = 0; i <= $scope.mData.JobTask[0].JobParts.length; i++)
                        //  {

                        //  }
                            messagetemp = "Kode Material : ? <br>"
                            +"Nama Material : " + $scope.mData.JobTask[0].JobParts[0].PartsName 
                            +"<br> Jumlah : "+ $scope.mData.JobTask[0].JobParts[0].Qty + ", ";

                          DataNotif.Message = messagetemp;
                          
                           //1122 = partsman GR
                           AppointmentGrService.sendNotif(DataNotif, 1122, 15).then(
                           function(res) {

                           },
                            function(err) {
                                //console.log("err=>", err);
                           });
                    }
                },
                function() {

                }
            )
        }


        $scope.bsAlertConfirm = function (title, text) {
            var defer = $q.defer();
            bsAlert.alert({
                title: title || 'Appointment Service BP',
                text: text || '',
                type: "warning",
                showCancelButton: false
            },
                function () {
                    defer.resolve(true);
                },
                function () {
                    defer.resolve(false);
                }
            )
            return defer.promise;
        };

        MasterDiscountFactory.getDataAktif().then(
            function (res) {
                console.log("res dis | 2", res.data.Result, res.data.Result.length);

                if(res.data.Result.length > 0){
                    $scope.MasterDiscountBooking = res.data.Result;
                    console.log('ini ada data master diskon bookingnya');
                }else{
                    $scope.MasterDiscountBooking = [];
                    console.log('ini ga data master diskon bookingnya');
                }
            },
            function (err) { }
        );

        $scope.bsAlertFunc = function(prompt) {
            bsAlert.alert({
                    title: prompt,
                    text: "",
                    type: "warning",
                    showCancelButton: true
                },
                function() {

                    // if($scope.MasterDiscountBooking.length !== 2){
                    //     $scope.bsAlertConfirm("Master Diskon Booking Belum di Input").then(function (res) {
                    //         if (res) {
                    //             console.log('ini kalo klik ok',res);
                    //         }else{
                    //             console.log("ini kalo klik cancle", res);
                    //         }
                    //     });
                    // }else{
                        $scope.vehicleStatus = 1;
                        $scope.getAllParameter();
                        MrsList.getDataModel().then(function(res) {
                            console.log("result : ", res.data.Result);
                            $scope.modelData = res.data.Result;
                        });
                        var filter = $scope.filter.noPolice.toUpperCase();
                        if (filter.includes("*")) {
                            filter = filter.split('*');
                            filter = filter[0];
                        }

                        $scope.restart_digit_odo = 0
                        $scope.isKmHistory = 0
                        $scope.nilaiKM = 0
                        $scope.KMTerakhir = 0

                        $scope.restart_digit_odo = 0;
                        $scope.model_info_km.show = false;

                        $scope.isKmHistory = 0;
                        $scope.KMTerakhir = 0;
                        $scope.tmpIsNonTAM = null;
                        $scope.mDataDetail = {};
                        $scope.mDataDetail.LicensePlate = filter;
                        $scope.stallAsb = 0;
                        gridTemp = [];
                        $scope.asb = {};
                        $scope.asb = { showMode: 'main', NewChips: 1 };
                        $scope.asb.endDate = new Date();
                        $scope.asb.endDate.setDate($scope.asb.endDate.getDate() + 7);
                        $scope.mData = {};
                        $scope.JobComplaint = [];
                        $scope.JobRequest = [];
                        tmpPrediagnose = {};
                        $scope.gridWork = gridTemp;
                        $scope.totalMaterial = 0;
                        $scope.totalWork = 0;
                        $scope.costMaterial = 0;
                        $scope.summaryJoblist = 0;
                        $scope.totalDp = 0;
                        $scope.checkAsb = false;
                        $scope.mData.AppointmentSourceId = 1;
                        $scope.asb.endDate = new Date();
                        $scope.asb.endDate.setDate($scope.asb.endDate.getDate() + 7);
                        $scope.mData.startDate = null;
                        $scope.mData.endDate = null;
                        $scope.mData.categoryHistory = null;
                        $scope.activeJustified = 0;
                        console.log("$scope.mDataDetail", $scope.mDataDetail);
                        console.log("$scope.stallAsb", $scope.stallAsb);
                        console.log("gridTemp", gridTemp);
                        console.log("$scope.asb", $scope.asb);
                        console.log("$scope.mData", $scope.mData);
                        console.log("$scope.JobComplaint", $scope.JobComplaint);
                        console.log("tmpPrediagnose", tmpPrediagnose);
                        // console.log("$scope.asb",$scope.asb);
                        $scope.AppointmentGrForm.$setPristine();
                        $scope.AppointmentGrForm.$setUntouched();
    
                        $scope.formApi.setMode('detail');
                        console.log("$scope.mDataDetail", $scope.mDataDetail);
                        console.log("$scope.stallAsb", $scope.stallAsb);
                        console.log("gridTemp", gridTemp);
                        console.log("$scope.asb", $scope.asb);
                        console.log("$scope.mData", $scope.mData);
                        console.log("$scope.JobComplaint", $scope.JobComplaint);
                        console.log("tmpPrediagnose", tmpPrediagnose);
                        $("input[name='DateAppoint']").attr("disabled", "disabled");
                        // $( "input[name='DateAppoint']" ).attr("disabled","disabled");
                        $scope.noRef = 0;
    
                        
                        $scope.getEstimate($scope.mDataDetail.LicensePlate);
                    // }

                    if($scope.MasterDiscountBooking.length == 0){
                        $scope.bsAlertConfirm("Master Diskon Booking Belum di Input").then(function (res) {
                            if (res) {
                                console.log('ini kalo klik ok',res);
                            }else{
                                console.log("ini kalo klik cancle", res);
                            }
                        });
                    }else{
                        $scope.vehicleStatus = 1;
                        $scope.getAllParameter();
                        MrsList.getDataModel().then(function(res) {
                            console.log("result : ", res.data.Result);
                            $scope.modelData = res.data.Result;
                        });
                        var filter = $scope.filter.noPolice.toUpperCase();
                        if (filter.includes("*")) {
                            filter = filter.split('*');
                            filter = filter[0];
                        }
                        $scope.tmpIsNonTAM = null;
                        $scope.mDataDetail = {};
                        $scope.mDataDetail.LicensePlate = filter;
                        $scope.stallAsb = 0;
                        gridTemp = [];
                        $scope.asb = {};
                        $scope.asb = { showMode: 'main', NewChips: 1 };
                        $scope.asb.endDate = new Date();
                        $scope.asb.endDate.setDate($scope.asb.endDate.getDate() + 7);
                        $scope.mData = {};
                        $scope.JobComplaint = [];
                        $scope.JobRequest = [];
                        tmpPrediagnose = {};
                        $scope.gridWork = gridTemp;
                        $scope.totalMaterial = 0;
                        $scope.totalWork = 0;
                        $scope.costMaterial = 0;
                        $scope.summaryJoblist = 0;
                        $scope.totalDp = 0;
                        $scope.checkAsb = false;
                        $scope.mData.AppointmentSourceId = 1;
                        $scope.asb.endDate = new Date();
                        $scope.asb.endDate.setDate($scope.asb.endDate.getDate() + 7);
                        $scope.mData.startDate = null;
                        $scope.mData.endDate = null;
                        $scope.mData.categoryHistory = null;
                        $scope.activeJustified = 0;
                        console.log("$scope.mDataDetail", $scope.mDataDetail);
                        console.log("$scope.stallAsb", $scope.stallAsb);
                        console.log("gridTemp", gridTemp);
                        console.log("$scope.asb", $scope.asb);
                        console.log("$scope.mData", $scope.mData);
                        console.log("$scope.JobComplaint", $scope.JobComplaint);
                        console.log("tmpPrediagnose", tmpPrediagnose);
                        // console.log("$scope.asb",$scope.asb);
                        $scope.AppointmentGrForm.$setPristine();
                        $scope.AppointmentGrForm.$setUntouched();

                        $scope.formApi.setMode('detail');
                        console.log("$scope.mDataDetail", $scope.mDataDetail);
                        console.log("$scope.stallAsb", $scope.stallAsb);
                        console.log("gridTemp", gridTemp);
                        console.log("$scope.asb", $scope.asb);
                        console.log("$scope.mData", $scope.mData);
                        console.log("$scope.JobComplaint", $scope.JobComplaint);
                        console.log("tmpPrediagnose", tmpPrediagnose);
                        $("input[name='DateAppoint']").attr("disabled", "disabled");
                        // $( "input[name='DateAppoint']" ).attr("disabled","disabled");
                        $scope.noRef = 0;

                        
                        $scope.getEstimate($scope.mDataDetail.LicensePlate);
                    }
                    
                },
                function() {

                }
            )
        };

        $scope.bsAlertConfirm = function (title, text) {
            var defer = $q.defer();
            bsAlert.alert({
                title: title || 'Appointment Service GR',
                text: text || '',
                type: "warning",
                showCancelButton: false
            },
                function () {
                    defer.resolve(true);
                },
                function () {
                    defer.resolve(false);
                }
            )
            return defer.promise;
        };

        var dataFinal = [];
        var gridTemp = [];
        var tmp = [];
        var materialArray = [];
        var typeSearch = "";
        var typeParam = "";
        $scope.DiscountParts = null;
        $scope.DiscountTask  = null;

        $scope.MasterDiscountBooking = [];
        $scope.getData = function(from_autocall) {
            $scope.loadingSimpan = 0
            $scope.mDataVehicleHistory = [];
            $scope.MRS = {};
            $scope.mDataDetail = {}
            $scope.loading = false;
            
            $scope.mData = {};
            
            MasterDiscountFactory.getDataAktif().then(
                function (res) {
                    console.log("res dis | 3", res.data.Result, res.data.Result.length);

                    $scope.MasterDiscountBooking = res.data.Result;
                    console.log('$scope.MasterDiscountBooking hobah 2', $scope.MasterDiscountBooking)

                        var flagcounttask = 0;
                        var flagcountparts = 0;
                        if($scope.MasterDiscountBooking.length > 0){

                            for(var i = 0; i < $scope.MasterDiscountBooking.length; i++){
                                if($scope.MasterDiscountBooking[i].Category == 2){
                                    $scope.DiskonPartsEndDate = $scope.MasterDiscountBooking[i].DateTo;
                                    $scope.DiscountParts = $scope.MasterDiscountBooking[i].Discount;
                                    $scope.mData.disParts = $scope.MasterDiscountBooking[i].Discount;
                                    flagcountparts++
                                }else if($scope.MasterDiscountBooking[i].Category == 1){
                                    $scope.DiskonTaskEndDate = $scope.MasterDiscountBooking[i].DateTo;
                                    $scope.DiscountTask = $scope.MasterDiscountBooking[i].Discount;
                                    $scope.mData.disTask = $scope.MasterDiscountBooking[i].Discount;
                                    flagcounttask++
                                }
                            }
                        }
                        else{

                        // $scope.DiskonPartsEndDate = $scope.MasterDiscountBooking[i].DateTo;
                        $scope.DiscountParts = 0;
                        $scope.mData.disParts = 0; 

                        // $scope.DiskonTaskEndDate = $scope.MasterDiscountBooking[i].DateTo;
                        $scope.DiscountTask = 0;
                        $scope.mData.disTask = 0;
                        }

                        if(flagcounttask == 0){
                            // $scope.DiskonTaskEndDate = $scope.MasterDiscountBooking[i].DateTo;
                            $scope.DiscountTask = 0;
                            $scope.mData.disTask = 0;
                        }

                        if(flagcountparts == 0){
                            
                            $scope.DiscountParts = 0;
                            $scope.mData.disParts = 0; 
                        }

                        if(($scope.asbDate == undefined) || ($scope.asbDate == [])){
                            
                            console.log('kalo asbnya undefined | [] ===>',$scope.asbDate);
                        }else{
                            if($scope.asbDate > $scope.DiskonTaskEndDate){
                                console.log('Tidak Masuk periode diskon booking Task');
                                $scope.DiscountTask = 0;
                                $scope.mData.disTask = 0;
                            }else{
                                console.log('masuk periode diskon booking Task');
                            }
                            if($scope.asbDate > $scope.DiskonPartsEndDate){
                                console.log('Tidak Masuk periode diskon booking Parts');
                                $scope.DiscountParts = 0;
                                $scope.mData.disParts = 0;
                            }else{
                                console.log('masuk periode diskon booking Parts');
                            }
                        }

                        
                   
                },
                function (err) { }
            );

            // ======================Fungsi Utama Masuk Ke Form Appointment GR =======================
            $scope.mData.disTask  = angular.copy($scope.DiscountTask);
            $scope.mData.disParts = angular.copy($scope.DiscountParts);
            console.log('diskon disTask ===> ',$scope.mData.disTask +' | diskon disParts ===> ' +$scope.mData.disParts);

            // console.log("no polisis panjang",$scope.filter.noPolice.length)
            if($scope.filter.noPolice !== null || $scope.filter.noRangka !== null || $scope.filter.Type == "Lain"){

                if ($scope.filter.Type == "Langsung" && $scope.filter.typeSearch == 0 && $scope.filter.noPolice !== "" && $scope.filter.noPolice !== undefined) {
                    if ($scope.filter.noPolice.length >= 2) {

                        console.log("filterno", $scope.filter.noPolice);
                        if ($scope.filter.noPolice.includes("*")) {
                            typeParam = 1;
                        } else {
                            typeParam = 0;
                        }
                        typeSearch = 0;
                        $scope.loading = true;
                        return $q.resolve(
                            AppointmentGrService.getData(typeSearch, $scope.filter.noPolice, typeParam).then(
                                function(res) {
                                    console.log("getData res=>", res);
                                    var gridData = res.data.Result;
                                    if (gridData.length > 0) {
                                        $scope.grid.data = gridData
                                    } else {
                                        $scope.grid.data = [];
                                        $scope.listClaim = [];
                                        $scope.tempListClaim = [];
                                        if (from_autocall != true) {
                                            $scope.bsAlertFunc("No. Polisi tidak ditemukan");
                                        }
                                        $scope.getPPN()
                                        $scope.disableEmail(false);
                                        $scope.loadingSimpan = 0
                                    }
                                    $scope.loading = false;
                                    return res;
                                },
                                function(err) {
                                    console.log("error=>", err);
                                    $scope.loading = false;
                                    return err;
                                }
                            )
                        )

                    } else {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Filter No. Polisi Harus Lebih Dari 2 Digit",
                            timeout: 2000
                        });
                    }
                } else if ($scope.filter.Type == "Langsung" && $scope.filter.typeSearch == 1 && $scope.filter.noRangka !== "" && $scope.filter.noRangka !== undefined ) {
                    if ($scope.filter.noRangka.length >= 3) {

                        console.log("filterno", $scope.filter.noRangka);
                        // if ($scope.filter.noPolice.includes("*")) {
                        //     typeParam = 1;
                        // } else {
                        typeParam = 0;
                        // }
                        typeSearch = 1;
                        $scope.loading = true;
                        return $q.resolve(

                            AppointmentGrService.getData(typeSearch, $scope.filter.noRangka, typeParam).then(
                                function(res) {
                                    var gridData = res.data.Result;
                                    if (gridData.length > 0) {
                                        $scope.grid.data = gridData
                                    } else {
                                        $scope.grid.data = [];
                                        $scope.bsAlertFunc("Toyota ID tidak ditemukan");
                                    }
                                    $scope.loading = false;
                                    return res;
                                },
                                function(err) {
                                    console.log("error=>", err);
                                    $scope.loading = false;
                                    return err;
                                }
                            )
                        )

                    } else {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Filter Toyota ID Harus Lebih Dari 3 Digit",
                            timeout: 2000
                        });
                    }
                } else if ($scope.filter.Type == "Lain") {
                    $scope.filter.noPolice = "";
                    gridData = [];
                    $scope.loading = true;
                    return $q.resolve(
                        AppointmentGrService.getDataLain().then(
                            function(res) {
                                console.log("res", res);
                                var gridData = res.data;
                                for (var i in gridData) {
                                    gridData[i].MRSId = gridData[i].Id;
                                    if (gridData[i].StatusCode == 0) {
                                        gridData[i].status = "Cancel";
                                    } else {
                                        gridData[i].status = "Outstanding";
                                    }
                                }
                                $scope.grid.data = gridData;
                                $scope.loading = false;
                                return res;
                            },
                            function(err) {
                                console.log("error=>", err);
                                $scope.loading = false;
                                return err;
                            }
                        )
                    )
                } else {
                    $scope.loading = false;
                    bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Mohon Input Filter",
                            // content: error.join('<br>'),
                            // number: error.length
                        }),
                        $scope.grid.data = [];
                    return $q.resolve()
                } 
            }
            // ======================Fungsi Utama Masuk Ke Form Appointment GR =======================
            
        }
        var linkSaveCb = function(mode, model) {
            tmpPrediagnose = model;
            console.log("tmpPrediagnose linkSaveCb", tmpPrediagnose);
        }
        var linkBackCb = function(mode, model) {
            console.log("mode", mode);
            console.log("mode", model);
        }
        $scope.preDiagnose = function() {
                tmpPrediagnose.fromAppointment = 1;
                console.log('tmpPrediagnose', tmpPrediagnose);
                console.log("Edit");
                $scope.formApi.showLinkView(linkSaveCb, linkBackCb, 'edit', tmpPrediagnose, 'true', 'true');
            }
            // =================== CRM
            // $scope.ShowEdit = true;
        $scope.EnableEdit = true;
        $scope.ShowEditInstitusi = true;
        $scope.EnableEditInstitusi = true;

        $scope.EnableEditContactPerson = true;
        $scope.EditInfoUmum = function() {
            $scope.mDataDetailDefault = angular.copy($scope.mDataDetail);
            // AppointmentBpService.getLocation().then(function(res) {
            //     // console.log("List Province====>",res.data.Result);
            //     $scope.ProvinceData = res.data.Result;
            //     $scope.ShowEdit = false;
            //     $scope.EnableEdit = false;
            //     console.log("ProvinceData", $scope.ProvinceData);
            // })
            if ($scope.mDataDetail.CustomerVehicleId !== undefined) {
                $scope.getAddressList();
                console.log("$scope.mDataDetail.CustomerVehicleId", $scope.mDataDetail.CustomerVehicleId);
                AppointmentGrService.getDataContactPerson($scope.mDataDetail.CustomerVehicleId).then(function(res) {
                    // console.log("List Province====>",res.data.Result);
                    $scope.ContactPersonData = res.data.Result;
                    $scope.ShowEdit = false;
                    $scope.EnableEdit = false;
                    $scope.EnableEditContactPerson = false;
                    console.log("Data ContactPerson", $scope.ShowEdit);
                })
            };
        };
        $scope.BatalInfoUmum = function() {
            $scope.ShowEdit = true;
            $scope.EnableEdit = true;
            $scope.EnableEditContactPerson = true;
            $scope.mDataDetail = angular.copy($scope.mDataDetailDefault);
            // $scope.onBeforeEdit();
            // $scope.getAllParameter(DataPersonal.VehicleId);
            if ($scope.mDataDetail.ProvinceId !== undefined && $scope.mDataDetail.ProvinceId !== null) {
                WOBP.getMLocationProvince().then(function(res) {
                    console.log("List Province====>", res.data.Result);
                    $scope.ProvinceData = res.data.Result;

                    if ($scope.mDataDetail.CityRegencyId !== undefined && $scope.mDataDetail.CityRegencyId !== null) {
                        WOBP.getMLocationCityRegency($scope.mDataDetail.ProvinceId).then(function(resu) {
                            $scope.CityRegencyData = resu.data.Result;

                            if ($scope.mDataDetail.DistrictId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                WOBP.getMLocationKecamatan($scope.mDataDetail.CityRegencyId).then(function(resu) {
                                    $scope.DistrictData = resu.data.Result;

                                    if ($scope.mDataDetail.VillageId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                        WOBP.getMLocationKelurahan($scope.mDataDetail.DistrictId).then(function(resu) {
                                            $scope.VillageData = resu.data.Result;
                                        });
                                    };
                                });
                            };
                        });
                    };
                })
            };
            
        };
        $scope.CekPerbedaan = function(){
            console.log("$scope.mDataDetail A", $scope.mDataDetail);
            console.log("$scope.DataSebelumBerubahCRM B", $scope.DataSebelumBerubahCRM);
            $scope.datahasil = {};
            $scope.datahasilsudah = {};
            $scope.dataakhir=[];
            $scope.datasatu=[];
            $scope.datadua=[];
            var counter = 0;
            var keyofData = Object.keys($scope.DataSebelumBerubahCRM);

            for(var i in keyofData){
                if($scope.DataSebelumBerubahCRM[keyofData[i]] !== $scope.mDataDetail[keyofData[i]]){
                    counter++;
                    $scope.datahasil[keyofData[i]] = $scope.mDataDetail[keyofData[i]];
                    $scope.datahasilsudah[keyofData[i]] = $scope.DataSebelumBerubahCRM[keyofData[i]];   
                    // $scope.datasatu.push($scope.datahasil);
                    // $scope.datadua.push($scope.datahasilsudah);                    
                }
            }         
            console.log('keyofData',keyofData);
            var fieldofData = Object.keys($scope.datahasilsudah);
            
            for(var i in fieldofData){
                if($scope.datahasilsudah[keyofData[i]] !== null){
                    counter++;
                    $scope.datasatu.push($scope.datahasilsudah[fieldofData[i]]);
                  
                }
            } 
            
            for (var i = 0; i < fieldofData.length; i++) {
                var tmpDataAkhir = {};
                tmpDataAkhir.CustomerId = $scope.mDataDetail.CustomerId;                             
                tmpDataAkhir.ChangeTypeId = 2
                tmpDataAkhir.Modul = 'Appointment GR'
                tmpDataAkhir.Attribute = fieldofData[i]
                if (tmpDataAkhir.Attribute == "BirthDate"){
                        tmpDataAkhir.NewValue = $filter('date')($scope.datahasil[fieldofData[i]], 'yyyy-MM-dd');
                        tmpDataAkhir.OldValue = $filter('date')($scope.datahasilsudah[fieldofData[i]], 'yyyy-MM-dd');
                    } else {
                        tmpDataAkhir.NewValue = $scope.datahasil[fieldofData[i]];
                        tmpDataAkhir.OldValue = $scope.datahasilsudah[fieldofData[i]];
                        // tmpDataAkhir.NewValue = $scope.datahasilsudah[fieldofData[i]]
                        // tmpDataAkhir.OldValue = $scope.datahasil[fieldofData[i]]
                    }
                // tmpDataAkhir.NewValue = $scope.datahasilsudah[fieldofData[i]],
                // tmpDataAkhir.OldValue = $scope.datahasil[fieldofData[i]]
                console.log(' $scope.datadua',$scope.datadua);
                AppointmentGrService.sendToCRMHistory(tmpDataAkhir);
            }
            

        };
        $scope.UpdateInfoUmum = function(mDataDetail) {

            AppointmentGrService.getDataVehicle(mDataDetail.VehicleId).then(function(ress) {
                var dataDetail = ress.data.Result[0]
                console.log('cek data detail 2', dataDetail)
                $scope.DataSebelumBerubahCRM = dataDetail;                 
                console.log("data CRM A", $scope.DataSebelumBerubahCRM);
                $scope.CekPerbedaan();
            });
            console.log("data sialan", DataPersonal);
            console.log("data sialan", mDataDetail);
            if (DataPersonal.CustomerGenderId == 0) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon isi data jenis kelamin",
                });
            } else {
                // if ($scope.mDataDetail.VehicleUserId !== undefined) {
                // AppointmentGrService.updateStatus(mDataDetail).then(function(res) {

                AppointmentBpService.putCustomerPersonal(DataPersonal, mDataDetail).then(function(res) {
                    AppointmentBpService.putCustomerAddress(DataPersonal, mDataDetail, 1).then(function(res) {
                        // $scope.BatalInfoUmum();
                        console.log("save addres");
                        // console.log("$scope.mDataDetail",$scope.mDataDetail);
                        // console.log("DataPersonal",DataPersonal);
                        AppointmentGrService.putGastype($scope.mDataDetail).then(function(res) {
                            $scope.ShowEdit = true;
                            $scope.EnableEdit = true;
                            $scope.EnableEditContactPerson = true;
                            console.log("update type Gas")
                        });
                    });
                });

                // });
                // }
                // if ($scope.mDataDetail.VehicleUserId !== undefined) {
                //     AppointmentGrService.updateStatus(mDataDetail).then(
                //         function(res) {
                //             $scope.BatalInfoUmum();
                //         },
                //         function(err) {
                //             console.log("err=>", err);
                //         }
                //     );
                // }

            }

        };
        $scope.EditInfoUmumInstitusi = function() {
            $scope.mDataDetailDefault = angular.copy($scope.mDataDetail);
            if ($scope.mDataDetail.CustomerVehicleId !== undefined) {
                $scope.getAddressList();
                console.log("$scope.mDataDetail.CustomerVehicleId", $scope.mDataDetail.CustomerVehicleId);
                AppointmentGrService.getDataContactPerson($scope.mDataDetail.CustomerVehicleId).then(function(res) {
                    // console.log("List Province====>",res.data.Result);
                    $scope.ContactPersonData = res.data.Result;
                    $scope.ShowEditInstitusi = false;
                    $scope.EnableEditInstitusi = false;
                    $scope.EnableEditContactPerson = false;
                    console.log("Data ContactPerson", $scope.ContactPersonData);
                })
            };
            // AppointmentBpService.getLocation().then(function(res) {
            //     // console.log("List Province====>",res.data.Result);
            //     $scope.ProvinceData = res.data.Result;
            //     $scope.ShowEditInstitusi = false;
            //     // $scope.EnableEditInstitusi = false;
            //     $scope.EnableEditContactPerson = false;
            //     console.log("ProvinceData", $scope.ProvinceData);
            // })
        };
        $scope.BatalInfoUmumInstitusi = function() {
            $scope.ShowEditInstitusi = true;
            $scope.EnableEditInstitusi = true;
            $scope.EnableEditContactPerson = true;
            $scope.mDataDetail = angular.copy($scope.mDataDetailDefault);
            // $scope.onBeforeEdit();
            // $scope.getAllParameter(DataPersonal.VehicleId);
            if ($scope.mDataDetail.ProvinceId !== undefined && $scope.mDataDetail.ProvinceId !== null) {
                WOBP.getMLocationProvince().then(function(res) {
                    console.log("List Province====>", res.data.Result);
                    $scope.ProvinceData = res.data.Result;

                    if ($scope.mDataDetail.CityRegencyId !== undefined && $scope.mDataDetail.CityRegencyId !== null) {
                        WOBP.getMLocationCityRegency($scope.mDataDetail.ProvinceId).then(function(resu) {
                            $scope.CityRegencyData = resu.data.Result;

                            if ($scope.mDataDetail.DistrictId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                WOBP.getMLocationKecamatan($scope.mDataDetail.CityRegencyId).then(function(resu) {
                                    $scope.DistrictData = resu.data.Result;

                                    if ($scope.mDataDetail.VillageId !== undefined && $scope.mDataDetail.DistrictId !== null) {
                                        WOBP.getMLocationKelurahan($scope.mDataDetail.DistrictId).then(function(resu) {
                                            $scope.VillageData = resu.data.Result;
                                        });
                                    };
                                });
                            };
                        });
                    };
                })
            };
            
        };

        $scope.UpdateInfoUmumInstitusi = function(mDataDetail) {
                // if ($scope.mDataDetail.VehicleUserId !== undefined) {
                //     // AppointmentGrService.updateStatus(mDataDetail).then(
                //     //     function(res) {
                //     //         $scope.BatalInfoUmumInstitusi();
                //     //     },
                //     //     function(err) {
                //     //         console.log("err=>", err);
                //     //     }
                //     // );
                // // }
                // AppointmentBpService.putCustomerAddress(DataPersonal, mDataDetail, 2).then(
                //     function(res) {
                //         $scope.BatalInfoUmumInstitusi();
                //         console.log("save addres")
                //     },
                //     function(err) {
                //         console.log("err=>", err);
                //     }
                // );
                // }
                if ($scope.mDataDetail.VehicleUserId !== undefined) {
                    // AppointmentGrService.updateStatus(mDataDetail).then(function(res) {
                    AppointmentBpService.putCustomerAddress(DataPersonal, mDataDetail, 2).then(function(res) {
                        // $scope.BatalInfoUmumInstitusi();
                        $scope.ShowEditInstitusi = true;
                        $scope.EnableEditInstitusi = true;
                        $scope.EnableEditContactPerson = true;
                        console.log("save addres")
                    });
                    // });
                }
            }
            // ====== Alamat Added by Fyberz===================
        $scope.CustomerAddress = [];
        $scope.getAddressList = function() {
            WOBP.getMLocationProvince().then(function(res) {
                console.log("List Province====>", res.data.Result);
                // ProvinceId == row.ProvinceId
                $scope.ProvinceData = res.data.Result;
                // $scope.tempProvinsiData = angular.copy($scope.provinsiData);
            })
        };

        $scope.selectProvince = function(row) {
            console.log('row selectProvince', row);
            WOBP.getMLocationCityRegency(row.ProvinceId).then(function(resu) {
                $scope.CityRegencyData = resu.data.Result;
                
                $scope.mDataDetail.CityRegencyId = null
                $scope.mDataDetail.DistrictId = null
                $scope.mDataDetail.VillageId = null
                $scope.mDataDetail.PostalCode = null

                $scope.DistrictData = []
                $scope.VillageData = []

            });
            // $scope.selectedProvince = angular.copy(row);
        };
        $scope.selectRegency = function(row) {
            console.log('row selectRegency', row);
            WOBP.getMLocationKecamatan(row.CityRegencyId).then(function(resu) {
                $scope.DistrictData = resu.data.Result;

                $scope.mDataDetail.DistrictId = null
                $scope.mDataDetail.VillageId = null
                $scope.mDataDetail.PostalCode = null

                $scope.VillageData = []

            });
            // $scope.kecamatanData = row.MDistrict;
            // $scope.selectedRegency = angular.copy(row);
        };
        $scope.selectDistrict = function(row) {
            console.log('row selectDistrict', row);
            WOBP.getMLocationKelurahan(row.DistrictId).then(function(resu) {
                $scope.VillageData = resu.data.Result;

                $scope.mDataDetail.VillageId = null
                $scope.mDataDetail.PostalCode = null
            });
            // $scope.selectedDistrict = angular.copy(row);
        };
        $scope.selectVillage = function(row) {
            console.log('row selectVillage', row);
            // $scope.selecedtVillage = angular.copy(row);
            $scope.mDataDetail.PostalCode = row.PostalCode == 0 ? '-' : row.PostalCode;
        };
        // ================================================

        // $scope.selectProvince = function(row) {
        //     $scope.CityRegencyData = row.MCityRegency;
        // }
        // $scope.selectRegency = function(row) {
        //     $scope.DistrictData = row.MDistrict;
        // }
        // $scope.selectDistrict = function(row) {
        //         $scope.VillageData = row.MVillage;
        //     }
        // ============================
        $scope.selectFilter = function(row) {
            if (row.Id !== undefined && row.Id == 0) {
                $scope.filter.noPolice = ""
            } else if (row.Id !== undefined && row.Id == 1) {
                $scope.filter.noRangka = ""
            }
        }
        $scope.DateOptions = {
            startingDay: 1,
            format: 'dd/MM/yyyy',
            //disableWeekend: 1
        };
        $scope.dateOptionsx = {
            startingDay: 1,
            format: 'dd/MM/yyyy',
            //disableWeekend: 1
        };
        $scope.DateOptionsASB = {
            startingDay: 1,
            format: 'dd/MM/yyyy',
            //disableWeekend: 1
        };

        $scope.minDate2 = new Date();
        // minDate2.setDate(minDate2.getDate() - 1);
        $scope.DateOptions2 = {
            startingDay: 1,
            format: 'dd/MM/yyyy',
            minDate: $scope.minDate2
        }

        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        $scope.onSelectRows = function(rows) {

            console.log("onSelectRows=>", rows);
        }
        $scope.clearOrderPekerjaan = function(){
            // $scope.gridWork = [];
            // gridTemp = [];
            // $scope.sumAllPrice();
            // $scope.stallAsb = 0;
            // // =================
            // $scope.mData.AppointmentDateTmp = null;
            // $scope.mData.AppointmentTimeTmp = null;
            // $scope.mData.TargetDateAppointmentTemp = null;
            // $scope.mData.FixedTmp = null;
            // $scope.mData.StallId = 0;
            // $scope.mData.FullDate = null;
            // $scope.mData.PlanStart = null;
            // $scope.mData.PlanFinish = null;
            // $scope.mData.PlanDateStart = null;
            // //$scope.mData.AppointmentDate = null; 
            // delete $scope.mData.AppointmentDate;
            // //$scope.mData.AppointmentTime = null;
            // delete $scope.mData.AppointmentTime;
            // $scope.mData.TargetDateAppointment = null;
            // $scope.mData.FixedDeliveryTime1 = null;
            // $scope.mData.PlanDateFinish = null;
            // $scope.mData.StatusPreDiagnose = 0;
            // delete $scope.mData.PrediagScheduledTime;
            // delete $scope.mData.PrediagStallId;
            // $scope.asb.NewChips = 1;
        }
        $scope.selectModel = function(selected) {
            if(selected != null && selected != undefined){
                console.log("selectModel selected : ", selected);
                $scope.mData.VehicleModelName       = selected.VehicleModelName;
                $scope.mData.LicensePlate           = $scope.mDataDetail.LicensePlate;
                $scope.mDataDetail.VehicleModelName = selected.VehicleModelName;
                $scope.mDataDetail.VehicleModelId   = selected.VehicleModelId;
                $scope.mDataDetail.KatashikiCode    = "";
                $scope.mDataDetail.Description      = "";
                $scope.mDataDetail.KatashikiCodex      = "";
                $scope.mDataDetail.VehicleTypeId      = "";



                WO.getVehicleTypeById(selected.VehicleModelId).then(function(res) {
                    console.log("result : ", res);
                    // $scope.typeData = res.data;
                    // $scope.typeData = _.uniq(res.data.Result, 'KatashikiCode');

                    $scope.katashikiData = _.uniq(res.data.Result, 'KatashikiCode');

                    for (var i=0; i<$scope.katashikiData.length; i++){
                        $scope.katashikiData[i].NewDescription = selected.VehicleModelName + ' - ' + $scope.katashikiData[i].KatashikiCode
                    }


                    // console.log("$scope.typeData : ", $scope.typeData);
                    console.log("$scope.katashikiData : ", $scope.katashikiData);


                });   
                $scope.clearOrderPekerjaan();
            }else{
                $scope.mData.VehicleModelName       = "";
                $scope.mDataDetail.VehicleModelName = "";
                $scope.mDataDetail.VehicleModelId   = "";
                $scope.mDataDetail.KatashikiCode    = "";
                $scope.mDataDetail.Description      = "";
                $scope.mDataDetail.KatashikiCodex    = "";
                $scope.mDataDetail.VehicleTypeId      = "";


            }

        };

        $scope.selectKatashiki = function (selected) {
            $scope.mDataDetail.Description      = "";
            $scope.mDataDetail.VehicleTypeId      = "";


            if(selected != null && selected != undefined){
                AppointmentGrService.GetCVehicleTypeListByKatashiki(selected.KatashikiCode).then(function(res) {
                    // $scope.typeData = res.data.Result[0];
                    $scope.typeData = res.data.Result;

                    for (var z = 0; z < $scope.typeData.length; z++) {
                        $scope.typeData[z].NewDescription = $scope.typeData[z].Description + ' - ' + $scope.typeData[z].KatashikiCode + ' - ' + $scope.typeData[z].SuffixCode
                    }
                    $scope.mDataDetail.KatashikiCode    = selected.KatashikiCode;
    
                })
            } else {
                $scope.mDataDetail.KatashikiCodex    = "";
                $scope.mDataDetail.KatashikiCode    = "";
                $scope.mDataDetail.Description   = "";
                $scope.mDataDetail.VehicleTypeId = "";
            }
            
        }

        $scope.selectType = function(selected) {
            console.log("selected", selected);
            if(selected != null && selected != undefined){
                $scope.mDataDetail.KatashikiCode = selected.KatashikiCode;
                $scope.mDataDetail.Description   = selected.Description;
                $scope.mDataDetail.VehicleTypeId = selected.VehicleTypeId;
                // AppointmentGrService.getDataTaskList($scope.mDataDetail.KatashikiCode).then(function(result) {
                //     console.log("ketemu", result);
                // });
                AppointmentGrService.getDataTaskListByKatashiki($scope.mDataDetail.KatashikiCode).then(function(restask) {
                    taskList = restask.data.Result;
                    var tmpTask = {};
                    // if (taskList.length > 0) {
                    //     for (var i = 0; i < taskList.length; i++) {
                    //         if (taskList[i].ServiceRate !== undefined) {
                    //             var summary = taskList[i].ServiceRate;
                    //             var summaryy = parseInt(summary);
                    //             tmpTask.Summary = Math.round(summaryy);
                    //             // row.Fare = taskList[i].AdjustmentServiceRate;
                    //             tmpTask.Fare = taskList[i].ServiceRate;
                    //             $scope.PriceAvailable = true;
                    //         } else {
                    //             tmpTask.Fare = taskList[i].ServiceRate;
                    //             tmpTask.Summary = tmpTask.Fare;
                    //         }
                    //         $scope.tmpServiceRate = tmpTask;
                    //     }
                    // } else {
                    //     $scope.tmpServiceRate = null;
                    // }

                    if (taskList.length > 0) {
                        for (var i = 0; i < taskList.length; i++) {
                            if (taskList[i].ServiceRate !== undefined && taskList[i].WarrantyRate !== undefined) {
                                tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                                tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                                $scope.PriceAvailable = true;
                            } else {
                                tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                                tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                            }
                            $scope.tmpServiceRate = tmpTask;
                        }
                    } else {
                        $scope.tmpServiceRate = null;
                    }
                    // $scope.clearOrderPekerjaan();
                });
            }else{
                $scope.mDataDetail.KatashikiCode = "";
                $scope.mDataDetail.Description   = "";
                $scope.mDataDetail.VehicleTypeId = "";
            }
        }
        $scope.getEstimate = function(filter) {
            AppointmentGrService.getDataEstimasi(filter).then(function(res) {
                $scope.estimateData = res.data.Result;
                console.log('res==|||==>', res);
            });

            WO.getDataPKS(filter).then(function(res) {
                $scope.dataPKS = res.data.Result;
                console.log("PKS", $scope.dataPKS);
            });
        }
        $scope.tes = function(dt) {
            console.log("dt", dt);
        }
        $scope.changeFormatDate = function(item) {
            var tmpAppointmentDate = item;
            console.log("changeFormatDate item", item);
            tmpAppointmentDate = new Date(tmpAppointmentDate);
            var finalDate
            var yyyy = tmpAppointmentDate.getFullYear().toString();
            var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based         
            var dd = tmpAppointmentDate.getDate().toString();
            finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
            console.log("changeFormatDate finalDate", finalDate);
            return finalDate;
        }
        $scope.dataForPrediagnose = function(data) {
            FollowUpGrService.getPreDiagnoseByJobId(data.JobId).then(function(res) {
                tmpPrediagnose = res.data;
                console.log("resss Prediagnosenya bosque", res.data);
            });
        };

        var jobEstimate = [];
        var countEstimate = 0;
        $scope.processedDataEstimasi = function(mDataByEstimasi){
            console.log('processedDataEstimasi | jobEstimate ===>',jobEstimate);
            jobEstimate = mDataByEstimasi.JobTask;

            countEstimate++;

            for(var x in jobEstimate){
                jobEstimate[x].Discount = $scope.DiscountTask;
                jobEstimate[x].tmpTaskId = jobEstimate[x].TaskId;
                for(var y in jobEstimate[x].JobParts){
                    jobEstimate[x].JobParts[y].PartId = jobEstimate[x].JobParts[y].PartsId;
                    jobEstimate[x].JobParts[y].Discount = $scope.DiscountParts;
                    jobEstimate[x].JobParts[y].subTotalForDisplay = jobEstimate[x].JobParts[y].Price * jobEstimate[x].JobParts[y].Qty - ((jobEstimate[x].JobParts[y].Price * jobEstimate[x].JobParts[y].Qty) * $scope.DiscountParts/100);
                    jobEstimate[x].JobParts[y].DiskonForDisplay = $scope.DiscountParts + '%';
                    jobEstimate[x].JobParts[y].IsEstimate = true;
                }
            }
        }

        $scope.selectEstimasi = function(item) {
                if (item != undefined) {
                    var tmpJobId = item.JobId;
                    console.log("itemnya", item.JobId);
                    AppointmentGrService.getWobyJobId(tmpJobId).then(function(res) {
                        var newData = res.data.Result[0];
                        // var getDataEstimasiDiscount = res.data.Result;
                    console.log('debug estimasinya start')
                        if (newData != undefined && newData != undefined) {
                            for (var i = 0; i < newData.JobTask.length; i++) {
                                console.log('newData.JobTask[i]', newData.JobTask[i])
                                if (newData.JobTask[i].PaidBy.MasterId == 30) {
                                    $scope.DiscountTask = 0;
                                    $scope.mData.disTask = 0;
                                }
                                for (var j = 0; j < newData.JobTask[i].JobParts.length; j++) {
                                    console.log('newData.JobTask[i].JobParts[j]', newData.JobTask[i].JobParts[j])
                                    if (newData.JobTask[i].JobParts[j].PaidById == 30) {
                                        $scope.DiscountParts = 0;
                                        $scope.mData.disParts = 0; 
                                    }
                                }
                            }
                        }
                        
                    console.log('debug estimasinya end')
                    $scope.tempVINEstimasi = newData.VIN;
                    console.log('$scope.tempVINEstimasi ini no rangka', $scope.tempVINEstimasi)
                    if ($scope.tempVINEstimasi != null || $scope.tempVINEstimasi != undefined || $scope.tempVINEstimasi != "") {
                        AppointmentGrService.getDataVIN($scope.tempVINEstimasi).then(function(res){
                            console.log('cek input rangka', $scope.tempVINEstimasi)
                            var vinMasterId = res.data.Result
                            $scope.tempVIN = res.data.Result[0]
                            console.log('res rangka yeu mah', vinMasterId)
                            for(var i = 0; i < vinMasterId.length; i++){
                                // if (vinMasterId[i].ExtendedWarranty == 1) {
                                if (vinMasterId[i].MasterId == 2678) {
                                    $scope.taskCategory = $scope.notFlcFpc;
                                }else if(vinMasterId[i].MasterId == 2677){
                                    $scope.taskCategory = $scope.notFpcPlc
                                }else if(vinMasterId[i].MasterId == 2675){
                                    $scope.taskCategory = $scope.notFlcPlc
                                }else{
                                    $scope.tableListClear();
                                    $scope.taskCategory = $scope.notFlcPlcFpc
                                }
                                // }else{
                                //     if (vinMasterId[i].MasterId == 2678) {
                                //         var filtered = $scope.notFlcFpc.filter(function(item) { 
                                //             return item.Name !== "EWC - Extended Warranty Claim";  
                                //          });
                                //          console.log("filtered",filtered);
                                //          $scope.taskCategory = filtered;
                                //     }else if(vinMasterId[i].MasterId == 2677){
                                //         var filtered = $scope.notFpcPlc.filter(function(item) { 
                                //             return item.Name !== "EWC - Extended Warranty Claim";  
                                //          });
                                //          console.log("filtered",filtered);
                                //          $scope.taskCategory = filtered;
                                //     }else if(vinMasterId[i].MasterId == 2675){
                                //         var filtered = $scope.notFlcPlc.filter(function(item) { 
                                //             return item.Name !== "EWC - Extended Warranty Claim";  
                                //          });
                                //          console.log("filtered",filtered);
                                //          $scope.taskCategory = filtered;
                                //     }else{
                                //         $scope.tableListClear();
                                //         $scope.taskCategory = $scope.notFlcPlcFpcEwc;
                                //     }     
                                // }
                            }

                            if ($scope.tempVIN.ProgramName != null && $scope.tempVIN.ProgramName != undefined && $scope.tempVIN.ProgramName != "") {
                                var tempProgramName = $scope.tempVIN.ProgramName;
                                if ($scope.tempVIN.ProgramName != null && $scope.tempVIN.ProgramName != undefined) {
                                    $scope.tempVIN.ProgramName = tempProgramName
                                }else{
                                    $scope.tempVIN.ProgramName = '-'
                                } 
                            }else{
                                $scope.tempVIN.ProgramName = '-'
                            }
    
                            if ($scope.tempVIN.DEC_Date != null && $scope.tempVIN.DEC_Date != undefined && $scope.tempVIN.DEC_Date != "") {
                                if ($scope.tempVIN.DEC_Date != null && $scope.tempVIN.DEC_Date != undefined) {
                                    $scope.tempVIN.DEC_Date = $filter('date')($scope.tempVIN.DEC_Date, 'dd/MM/yyyy')
                                }else{
                                    $scope.tempVIN.DEC_Date = '-'
                                }
                            }else{
                                $scope.tempVIN.DEC_Date = '-'
                            }
                        })
    
                        AppointmentGrService.listHistoryClaim($scope.tempVINEstimasi).then(function(res){
                            // res.data.data = $scope.ubahFormatTanggalIndo(res.data.data.historyClaimDealerList)
                            $scope.listClaim = res.data.data;
                            $scope.tempListClaim = [];
                            // $scope.historyDate = new Date($scope.tempListClaim.claimDate)
                            console.log('cek list claim', res)
                            console.log('list claim', res.data.data)
                            console.log('list history claim', $scope.tempListClaim)
                            for (var i = 0; i < $scope.listClaim.historyClaimDealerList.length; i++) {
                                $scope.tempListClaim.push($scope.listClaim.historyClaimDealerList[i]);
                            }
                        })
    
                        AppointmentGrService.getTowass($scope.tempVINEstimasi).then(function(res) {
                            console.log("data Towasssssss", res.data);
                            $scope.dataTowass = res.data.Result;
                        });
                }else{
                    $scope.tableListClear();
                    $scope.taskCategory = $scope.notFlcPlcFpc;
                }

                        console.log('ini cuk category', $scope.taskCategory);
                        console.log('data estimasi', newData)
                        console.log("new DATA dari Estimasi Appointment", newData);
                        // newData.AppointmentDate = "";
                        // newData.JobDate = "";
                        newData.AppointmentTime = "";
                        delete newData.AppointmentDate;
                        delete newData.StallId;
                        $scope.stallAsb = 0;

                        countEstimate = 0;
                        $scope.processedDataEstimasi(newData,countEstimate);
                        if(countEstimate == 1){
                            console.log('masuk processed');
                            newData.JobTask = [];
                            newData.JobTask = jobEstimate;
                            $scope.dataForBslist(newData);
                        }else{
                            console.log('ga masuk processed');
                        }

                        $scope.dataForPrediagnose(newData);
                        $scope.mData = newData;
                        $scope.JobRequest = newData.JobRequest;
                        $scope.JobComplaint = newData.JobComplaint;
                        $scope.mData.Estimate = item.JobId;
                        console.log("$scope.mData.Km", $scope.mData.Km);
                        $scope.mData.Km = String($scope.mData.Km);
                        $scope.mData.Km = $scope.mData.Km.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        $scope.sumAllPrice();
                        if (item.VehicleId == null) {
                            $scope.vehicleStatus = 1;
                            $scope.mDataDetail.LicensePlate = newData.PoliceNumber;
                            $scope.mDataDetail.VIN = newData.VIN;
                            $scope.mDataDetail.KatashikiCode = newData.KatashikiCode;

                            //untuk dapetin Model, Tipe & Katashiki dari estimasi
                            var tampunganVehicleTypeId = newData.VehicleTypeId;
                            for (var i = 0; i < $scope.modelData.length; i++) {
                                if ($scope.modelData[i].VehicleModelId == newData.VehicleType.VehicleModelId){
                                    $scope.mData.VehicleModelName       = $scope.modelData[i].VehicleModelName;
                                    $scope.mData.VehicleModelId         = $scope.modelData[i].VehicleModelId;
                                    $scope.mDataDetail.VehicleModelName = $scope.modelData[i].VehicleModelName;
                                    $scope.mDataDetail.VehicleModelId   = $scope.modelData[i].VehicleModelId;
                                    
                                    console.log('model gue ===> ',parseInt(newData.VehicleType.VehicleModelId) +" == $scope.modelData "+ $scope.modelData[i].VehicleModelId );

                                    WO.getVehicleTypeById(parseInt(newData.VehicleType.VehicleModelId)).then(function(res) {
                                        // $scope.typeData = res.data.Result;
                                        $scope.katashikiData = _.uniq(res.data.Result, 'KatashikiCode');

                                        for (var i=0; i<$scope.katashikiData.length; i++){
                                            $scope.katashikiData[i].NewDescription = $scope.mDataDetail.VehicleModelName + ' - ' + $scope.katashikiData[i].KatashikiCode
                                        }

                                        for (var i=0; i < $scope.katashikiData.length; i++) {
                                            if ($scope.katashikiData[i].KatashikiCode == newData.VehicleType.KatashikiCode) {
                                                $scope.mDataDetail.KatashikiCodex   = newData.VehicleType.KatashikiCode;

                                            }
                                        }

                                        if ($scope.mDataDetail.KatashikiCodex != null && $scope.mDataDetail.KatashikiCodex != undefined && $scope.mDataDetail.KatashikiCodex != '') {

                                            AppointmentGrService.GetCVehicleTypeListByKatashiki(newData.VehicleType.KatashikiCode).then(function(res) {
                                                // $scope.typeData = res.data.Result[0];
                                                $scope.typeData = res.data.Result;

                                                for (var z = 0; z < $scope.typeData.length; z++) {
                                                    $scope.typeData[z].NewDescription = $scope.typeData[z].Description + ' - ' + $scope.typeData[z].KatashikiCode + ' - ' + $scope.typeData[z].SuffixCode
                                                }

                                                for (var z = 0; z < $scope.typeData.length; z++) {
                                                    if($scope.typeData[z].VehicleTypeId == tampunganVehicleTypeId){
                                                        $scope.selectType($scope.typeData[z])
                                                        console.log('Type Gue ===> ',tampunganVehicleTypeId +" == ModelOptions "+$scope.typeData[z].VehicleTypeId);
                                                    }
                                                }
                                
                                            })
                                            
                                        }
                                        
                                    })


                                }
                            }
                            
                            // _.map($scope.modelData, function(val) {
                            //     if (val.VehicleModelName === newData.ModelType) {
                            //         console.log('masuk kesini model val', val);
                            //         $scope.mDataDetail.VehicleModelId = val.VehicleModelId;
                            //         console.log('masuk kesini model', $scope.mDataDetail);
                            //         MrsList.getDataTypeByModel(val.VehicleModelId).then(function(res) {
                            //             console.log("result : ", res);
                            //             $scope.typeData = res.data;
                            //             _.map($scope.typeData, function(a) {
                            //                 if (a.KatashikiCode === newData.KatashikiCode) {
                            //                     console.log('masuk kesini type val', a);
                            //                     $scope.mDataDetail.VehicleTypeId = a.VehicleTypeId;
                            //                     console.log('masuk kesini type', $scope.mDataDetail);
                            //                 }
                            //             })
                            //             AppointmentGrService.getDataTaskList($scope.mDataDetail.KatashikiCode).then(function(result) {
                            //                 console.log("resultgetDataTaskList Harga untuk Task", result);
                            //             });
                            //         });
                            //     }
                            // });
                        } else {
                            AppointmentGrService.getDataVehicle(item.VehicleId).then(function(res) {
                                $scope.getAllParameter(ress.data.Result[0]);
                                $scope.mDataDetail = ress.data.Result[0];
                                if (ress.data.Result.length <= 0) {
                                    $scope.vehicleStatus = 1;
                                } else {
                                    $scope.vehicleStatus = 0;
                                    console.log("kalo CRM");
                                }
                            });
                        }
                    });
                } else {
                    console.log("kosong lagi bos");
                    Parts = [];
                    $scope.mData = {};
                    gridTemp = [];
                    $scope.JobComplaint = [];
                    $scope.JobRequest = [];
                    $scope.gridWork = gridTemp;
                    $scope.totalWork = 0;
                    $scope.costMaterial = 0;
                    $scope.summaryJoblist = 0;
                    $scope.totalDp = 0;
                    $scope.totalMaterial = 0;
                    $scope.totalMaterialDiscounted = 0;
                    $scope.subTotalMaterialSummary = 0;
                    $scope.totalPPN = 0;
                    $scope.totalEstimasi = 0;
                }
            }
            // ===========Backup=====================
        var Parts = [];
        $scope.dataForBslist = function(data) {
            gridTemp = []
            Parts.splice();
            Parts = [];
            if (data.JobTask.length > 0) {
                var tmpJob = data.JobTask;
                for (var i = 0; i < tmpJob.length; i++) {
                    console.log("tmpJob", tmpJob);
                
                    for (var j = 0; j < tmpJob[i].JobParts.length; j++) {
                        if(tmpJob[i].JobParts[j].IsEstimate == true){
                            console.log("tmpJob[i].JobParts[j]", tmpJob[i].JobParts[j]);
                            if (tmpJob[i].JobParts[j].PaidBy !== null) {
                                tmpJob[i].JobParts[j].PaidBy = tmpJob[i].JobParts[j].PaidBy.Name;
                                // delete tmpJob[i].JobParts[j].PaidBy;
                            };
                            if (tmpJob[i].JobParts[j].Satuan !== null) {
                                tmpJob[i].JobParts[j].satuanName = tmpJob[i].JobParts[j].Satuan.Name;
                                delete tmpJob[i].JobParts[j].Satuan;
                            };
                            tmpJob[i].subTotal = tmpJob[i].Qty * tmpJob[i].RetailPrice;
                        }
                        
                    };
                };
                // added by sss on 2017-11-03
                $scope.getAvailablePartsServiceManual(tmpJob, 0, 0);
                // 
                $scope.gridWork = gridTemp;
                $scope.gridOriginal = gridTemp;
                // $scope.onAfterSave();
                // console.log("totalw", $scope.totalWork);
            };
            // $scope.sumAllPrice();
            console.log("$scope.gridWork", $scope.gridWork);
            console.log("$scope.JobRequest", $scope.JobRequest);
            console.log("$scope.JobComplaint", $scope.JobComplaint);
        };
        // $scope.dataForBslist = function(data){
        //     gridTemp = []
        //     var tmpTask = [];
        //     var tmpTaskPart = [];
        //     if (data.JobTask.length > 0) {
        //         var tmpJob = data.JobTask;
        //         for (var i = 0; i < tmpJob.length; i++) {
        //             // tmpTask.push(tmpJob[i]);
        //             if (tmpJob[i].Fare !== undefined) {
        //                 var sum = tmpJob[i].Fare;
        //                 var summ = tmpJob[i].FlatRate;
        //                 var summaryy = Math.ceil(sum * summ);
        //                 summaryy = parseInt(summaryy);
        //             }
        //             for(var j=0; j< tmpJob[i].JobParts.length; j++){
        //                 console.log("tmpJob[i].JobParts",tmpJob[i].JobParts[j]);
        //                 var tmpIni = tmpJob[i].JobParts[j];
        //                 AppointmentGrService.getAvailableParts(tmpJob[i].JobParts[j].PartsId).then(function(res) {
        //                     var tmpRes = res.data.Result[0];
        //                     // for(var i = 0; i < tmpJob.length; i++){
        //                         // for(var j=0; j< tmpJob[i].JobParts.length; j++){
        //                             if(tmpIni.PaidBy !== null){
        //                                 tmpIni.paidName = tmpIni.PaidBy.Name;
        //                                 delete tmpIni.PaidBy;
        //                             }
        //                             if(tmpIni.Satuan !== null){
        //                                 tmpIni.satuanName = tmpIni.Satuan.Name;
        //                                 delete tmpIni.Satuan;
        //                             }
        //                             tmpIni.subTotal = tmpIni.Qty * tmpIni.RetailPrice;
        //                             tmpIni.RetailPrice = tmpRes.RetailPrice;
        //                             tmpIni.Dp = tmpRes.ValueDP;
        //                             tmpIni.ETA = tmpRes.DefaultETA;
        //                             tmpIni.Type = 3;
        //                             if (tmpRes.isAvailable == 0) {
        //                                 tmpIni.Availbility = "Tidak Tersedia";
        //                             } else {
        //                                 tmpIni.Availbility = "Tersedia"
        //                             }
        //                             tmpTaskPart.push(tmpIni);
        //                             console.log("tmpTaskPart.push(tmpIni);",tmpIni);
        //                             $scope.sumAllPrice();
        //                             // break;
        //                         // }
        //                     // }
        //                 });
        //             }
        //             gridTemp.push({
        //                 ActualRate: tmpJob[i].ActualRate,
        //                 JobTaskId: tmpJob[i].JobTaskId,
        //                 JobTypeId: tmpJob[i].JobTypeId,
        //                 catName: tmpJob[i].JobType.Name,
        //                 Fare: tmpJob[i].Fare,
        //                 Summary: summaryy,
        //                 PaidById: tmpJob[i].PaidById,
        //                 JobId: tmpJob[i].JobId,
        //                 TaskId: tmpJob[i].JobTaskId,
        //                 TaskName: tmpJob[i].TaskName,
        //                 FlatRate: tmpJob[i].FlatRate,
        //                 ProcessId: tmpJob[i].ProcessId,
        //                 TFirst1: tmpJob[i].TFirst1,
        //                 PaidBy: tmpJob[i].PaidBy.Name,
        //                 child: tmpTaskPart
        //             });
        //         }
        //     }
        //     console.log("gridTemp getAvailablePartsService",gridTemp);
        //     $scope.gridWork = gridTemp;
        //     $scope.sumAllPrice();
        // }
        $scope.sumAllPrice = function() {


            var NumberParent = 1;
            for (i in $scope.gridWork){
                $scope.gridWork[i].NumberParent = NumberParent+parseInt(i);
                for (j in $scope.gridWork[i].child){
                    $scope.gridWork[i].child[j].NumberChild = $scope.gridWork[i].NumberParent + "."  + parseInt(++j);
                }
            }

            // =================================================== Total Work =======================================================================
            var totalW = 0;
            var totalWnotWarranty = 0;
            $scope.adaWarranty = 0;
            $scope.jmlJobWarranty = $scope.gridWork.length;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].Summary !== undefined) {
                    if ($scope.gridWork[i].Summary == "") {
                        $scope.gridWork[i].Summary = $scope.gridWork[i].Fare * $scope.gridWork[i].FlatRate;
                    }
                   
                        // if ($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                        if ($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                            totalW += 0
                        } else {
                            // totalW += Math.round($scope.gridWork[i].Summary / 1.1);
                            totalW += ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWork[i].Summary,
                                            // Discount: 5,
                                            // Qty: 2,
                                            Tipe: 2, 
                                            PPNPercentage: PPNPerc,
                                        });
                        }

                        if ($scope.gridWork[i].PaidById == 30){
                            $scope.adaWarranty++
                        } else {
                            // totalWnotWarranty += Math.round($scope.gridWork[i].Summary / 1.1);
                            totalWnotWarranty += ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWork[i].Summary,
                                                    // Discount: 5,
                                                    // Qty: 2,
                                                    Tipe: 2, 
                                                    PPNPercentage: PPNPerc,
                                                });
                        }
                    
                }
            }
            
            $scope.totalWork = totalW;  //request pak dodi dibagi 1.1 13/02/2019
            $scope.totalWorknotWarranty = totalWnotWarranty
            // =================================================== Total Work =======================================================================



            var totalDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].DownPayment !== undefined) {
                            totalDp += $scope.gridWork[i].child[j].DownPayment;
                            console.log("$scope.gridWork[i].child.Price", $scope.gridWork[i].child[j].DownPayment);
                        }
                    }
                }
            }
            $scope.totalDp = totalDp;
            // ===========================
            var totalDefaultDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].minimalDp !== undefined) {
                            totalDefaultDp += $scope.gridWork[i].child[j].minimalDp;
                            console.log("$scope.gridWork[i].child.Price", $scope.gridWork[i].child[j].minimalDp);
                        }
                    }
                }
            }
            $scope.totalDpDefault = totalDefaultDp;

           


            // =================================================== Total Material =======================================================================
            var totalMaterial_byKevin = 0;
            var totalMaterial_byKevinNotWarranty = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        // if($scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32 ){
                            totalMaterial_byKevin += 0;
                        }else{

                            if($scope.gridWork[i].child[j].PaidById == 30 ){ 
                                totalMaterial_byKevin += 0;
                            }else{ 
                                // totalMaterial_byKevin += Math.round(($scope.gridWork[i].child[j].RetailPrice/1.1)) * $scope.gridWork[i].child[j].Qty;
                                totalMaterial_byKevin += ASPricingEngine.calculate({
                                                            InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                            // Discount: 5,
                                                            Qty: $scope.gridWork[i].child[j].Qty,
                                                            Tipe: 102, 
                                                            PPNPercentage: PPNPerc,
                                                        }); 
                            }

                            if ($scope.gridWork[i].child[j].PaidById == 30){

                            } else {
                                // totalMaterial_byKevinNotWarranty += Math.round(($scope.gridWork[i].child[j].RetailPrice/1.1)) * $scope.gridWork[i].child[j].Qty;
                                totalMaterial_byKevinNotWarranty += ASPricingEngine.calculate({
                                                                        InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                                        // Discount: 5,
                                                                        Qty: $scope.gridWork[i].child[j].Qty,
                                                                        Tipe: 102, 
                                                                        PPNPercentage: PPNPerc,
                                                                    }); 
                            }


                        }
                    }
                }
            };
            // =================================================== Total Material =======================================================================
            

            // =================================================== Total Material Discount =======================================================================
            var totalMD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if($scope.gridWork[i].child[j].Discount == " "){
                            $scope.gridWork[i].child[j].Discount = 0;
                        }

                        // if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30 ){
                        if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32 ){
                            totalMD_byKevin += 0;
                        }else{
                            // totalMD_byKevin +=  Math.round(Math.round($scope.gridWork[i].child[j].RetailPrice /1.1 ) * ($scope.gridWork[i].child[j].Discount/100)) *  $scope.gridWork[i].child[j].Qty;
                            totalMD_byKevin +=  ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                    Discount: $scope.gridWork[i].child[j].Discount,
                                                    Qty: $scope.gridWork[i].child[j].Qty,
                                                    Tipe: 112, 
                                                    PPNPercentage: PPNPerc,
                                                }); 
                            
                        }
                    }
                }
            };

            // =================================================== Total Material Discount =======================================================================


            // =================================================== Total Work Discount =======================================================================
            var totalWD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                // if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                if($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                    totalWD_byKevin += 0;
                }else{
                    // totalWD_byKevin +=  Math.round(Math.round($scope.gridWork[i].Summary /1.1 ) * ($scope.gridWork[i].Discount/100));
                    totalWD_byKevin +=  ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWork[i].Summary,
                                            Discount: $scope.gridWork[i].Discount,
                                            // Qty: 2,
                                            Tipe: 12, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                }
            };
            // =================================================== Total Work Discount =======================================================================



            // $scope.summaryJoblist = Math.round(($scope.totalWork + ($scope.totalMaterial - ($scope.totalMaterial * $scope.DiscountParts / 100))) + ((($scope.totalWork + ($scope.totalMaterial - ($scope.totalMaterial * $scope.DiscountParts / 100))) * 10) / 100));
            if ($scope.totalEstimasi >= 1000000) {
                $scope.costMaterial = 6000;
            } else if ($scope.totalEstimasi > 250000) {
                $scope.costMaterial = 3000;
            } else {
                $scope.costMaterial = 0;
            }
            // =======================================================

            $scope.totalMaterial           = totalMaterial_byKevin;
            $scope.totalMaterialnotWarranty = totalMaterial_byKevinNotWarranty;
            $scope.totalMaterialDiscounted = totalMD_byKevin;
            $scope.totalWorkDiscounted     = totalWD_byKevin;
            $scope.subTotalMaterialSummary = $scope.totalMaterial - $scope.totalMaterialDiscounted;
            $scope.subTotalWorkSummary     = $scope.totalWork - $scope.totalWorkDiscounted;
            // $scope.totalPPN                = Math.floor(($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) *0.1);      
            $scope.totalPPN                = ASPricingEngine.calculate({
                                                InputPrice: (($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * (1+PPNPerc/100)),
                                                // Discount: 0,
                                                // Qty: 0,
                                                Tipe: 33, 
                                                PPNPercentage: PPNPerc,
                                            });    
            $scope.totalEstimasi           = $scope.totalPPN + $scope.subTotalMaterialSummary + $scope.subTotalWorkSummary + $scope.costMaterial;


            // =======================================================

            //nentuin diskon task & parts
            jejeranDiskonTask = [];
            jejeranDiskonParts = [];
            for(var i in $scope.gridWork){
                // if($scope.gridWork[i].PaidById == 30|| $scope.gridWork[i].PaidById == 31 || $scope.gridWork[i].PaidById == 32 || $scope.gridWork[i].PaidById == 2277){
                // if($scope.gridWork[i].PaidById == 30|| $scope.gridWork[i].PaidById == 2277){
                if($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                    var zeroDiscountTask = {Discount : 0}
                    jejeranDiskonTask.push(zeroDiscountTask.Discount);
                }else{
                    jejeranDiskonTask.push($scope.gridWork[i].Discount);
                }
                if($scope.gridWork[i].child.length > 0 ){
                    for(var j in $scope.gridWork[i].child){
                        // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 31 || $scope.gridWork[i].child[j].PaidById == 32 || $scope.gridWork[i].child[j].PaidById == 2277){
                        // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32){
                            var zeroDiscountParts = {Discount : 0}
                            jejeranDiskonParts.push(zeroDiscountParts.Discount);
                        }else{
                            jejeranDiskonParts.push($scope.gridWork[i].child[j].Discount);
                        }

                        
                    }
                }
            }
            //nentuin diskon task & parts
            $scope.displayDiscountAlgorithm();


            console.log("$scope.gridWork", $scope.gridWork);
            console.log("gridWork", $scope.gridWork);
        };


        var jejeranDiskonTask = [];
        var jejeranDiskonParts = [];
        $scope.displayDiscountAlgorithm = function(){
            console.log('jejeranDiskonTask ==>',jejeranDiskonTask);
            console.log('jejeranDiskonParts ==>',jejeranDiskonParts);
            if(jejeranDiskonTask.length > 1){
                var countTaskDiscount = 0;
                getSingleDiskonTask = [];
                for(var i in jejeranDiskonTask){
                    if(jejeranDiskonTask[i] == 0){
                        countTaskDiscount++;
                    }else{
                        getSingleDiskonTask.push(jejeranDiskonTask[i]);
                    }
                }
                console.log('jejeranDiskonTask 12', jejeranDiskonTask)
                console.log('getSingleDiskonTask 12', getSingleDiskonTask)
                if(countTaskDiscount == jejeranDiskonTask.length - 1 && getSingleDiskonTask.length == 1){
                    $scope.DisplayDiscountTask = getSingleDiskonTask[0]; // [0,0,0,5] get 5 for Discount
                }else{
                    console.log('jejeranDiskonTask 123', jejeranDiskonTask)
                    if(countTaskDiscount == jejeranDiskonTask.length){
                        $scope.DisplayDiscountTask = jejeranDiskonTask[0]; //[0,0,0] get 0 for Discount
                    }else{
                        var countSameDiscount = 0;
                        for(var x in getSingleDiskonTask){
                            if(getSingleDiskonTask[0] == getSingleDiskonTask[x]){
                                countSameDiscount++;
                            }
                        }
                        if(countSameDiscount == getSingleDiskonTask.length){
                            $scope.DisplayDiscountTask = getSingleDiskonTask[0]; //[0,0,0,3,3] get 3 for Discount
                        }else{
                            $scope.DisplayDiscountTask = ' '; // [0,6,4,3,5] set blank For Discount
                        }
                    }
                }
            }else{
                $scope.DisplayDiscountTask = jejeranDiskonTask[0]; //[x] get x for Discount
            }


            if(jejeranDiskonParts.length > 1){
                var countPartsDiscount = 0;
                getSingleDiskonParts = [];
                for(var i in jejeranDiskonParts){
                    if(jejeranDiskonParts[i] == 0){
                        countPartsDiscount++;
                    }else{
                        getSingleDiskonParts.push(jejeranDiskonParts[i]);
                    }
                }
                if(countPartsDiscount == jejeranDiskonParts.length - 1 && getSingleDiskonParts.length == 1){
                    $scope.DisplayDiscountParts = getSingleDiskonParts[0]; // [0,0,0,5] get 5 for Discount
                }else{
                    if(countPartsDiscount == jejeranDiskonParts.length){
                        $scope.DisplayDiscountParts = jejeranDiskonParts[0]; //[0,0,0] get 0 for Discount
                    }else{
                        var countSameDiscount = 0;
                        for(var x in getSingleDiskonParts){
                            if(getSingleDiskonParts[0] == getSingleDiskonParts[x]){
                                countSameDiscount++;
                            }
                        }
                        if(countSameDiscount == getSingleDiskonParts.length){
                            $scope.DisplayDiscountParts = getSingleDiskonParts[0]; //[0,0,0,3,3] get 3 for discount
                        }else{
                            $scope.DisplayDiscountParts = ' '; // [0,6,4,3,5] set blank For Discount
                        }
                    }
                }
            }else{
                
                $scope.DisplayDiscountParts = jejeranDiskonParts[0];// get x for Discount
                $scope.DiscountParts = jejeranDiskonParts[0]
                console.log('$scope.DisplayDiscountParts', $scope.DisplayDiscountParts)
            }

            var flagFLC = 0
            for(var i in $scope.gridWork){
                if ($scope.gridWork[i].catName == "FLC - Free Labor Claim") {
                    flagFLC++
                }
            }
            
            if (flagFLC == $scope.gridWork.length) {
                $scope.newVarFLC = 1;
            }

            var flag = 0;
            for(var i in $scope.gridWork){
                if ($scope.gridWork[i].catName == "FPC - Free Parts Claim") {
                    flag++
                }
            }
            console.log('flag disc 123', flag)
            console.log('$scope.gridWork.length 123', $scope.gridWork.length)
            if(flag == $scope.gridWork.length){
                $scope.DiscountParts = 0
            }
            console.log('$scope.DiscountParts', $scope.DiscountParts)
            console.log('$scope.adaWarranty', $scope.adaWarranty)
            console.log('$scope.jmlJobWarranty', $scope.jmlJobWarranty)
        }
        $scope.newVarFLC = 0;

        $scope.checkEstimasi = function(data) {
            console.log('data check estimasi', data)
            if (data == false) {
                Parts = [];
                $scope.mData.Estimate = ''
                $scope.mData = {};
                gridTemp = [];
                $scope.JobComplaint = [];
                $scope.JobRequest = [];
                $scope.gridWork = gridTemp;
                $scope.totalWork = 0;
                $scope.totalMaterial = 0;   
                $scope.totalMaterialDiscounted = 0;
                $scope.totalWorkDiscounted = 0;
                $scope.subTotalWorklSummary = 0;
                $scope.subTotalMaterialSummary = 0;
                $scope.totalPPN = 0;
                $scope.totalEstimasi = 0;
                $scope.totalDp = 0;
            }
        };
        $scope.vm = null;
        $scope.DateAppChange = function(dt) {

        };


        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'Toyota ID',
                    displayName: 'Toyota ID',
                    field: 'ToyotaId',
                    width: 100,
                    // width: '7%'
                },
                {
                    name: 'No. Polisi',
                    field: 'LicensePlate',
                    width: 100,
                    // width: '7%'
                },
                {
                    name: 'Nama Pelanggan',
                    field: 'Name',
                    width: 200,
                    // width: '9%'
                },
                {
                    name: 'Alamat',
                    field: 'Address',
                    width: 300,
                    // width: '9%'
                },
                {
                    name: 'No. Telepon',
                    field: 'Phone',
                    width: 130,
                    // width: '9%'
                },
                {
                    name: 'Model',
                    field: 'VehicleModelName',
                    width: 100,
                    // width: '9%'
                },
                {
                    name: 'Warna',
                    field: 'ColorName',
                    width: 140,
                    // width: '7%'
                },
                {
                    name: 'Tahun Rakit',
                    field: 'AssemblyYear',
                    width: 120,
                    // width: '7%'
                },
                {
                    name: 'Tgl. Appointment',
                    field: 'RequestAppointmentDate',
                    cellFilter: dateFilter,
                    width: 150,
                    // width: '7%'
                },

                {
                    name: 'status',
                    field: 'status',
                    width: 150,
                    // width: '7%'
                },
                {
                    name: 'Sumber Data',
                    field: 'AppointmentSourceName',
                    width: 130
                        // width: '9%'
                },
                {
                    name: 'Action',
                    // width: '8%',
                    width: 120,
                    // pinnedRight: true,
                    cellTemplate: gridActionButtonTemplate
                }
            ]
        };
        //===============BS List===========

        $scope.tableListClear = function(){
            $scope.tempVIN = null;
            $scope.tempListClaim = null;
        }

        $scope.onBeforeNew = function() {
            $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
            $scope.cekJob = null;
            $scope.isRelease = false;
            $scope.FlatRateAvailable = false;
            $scope.PriceAvailable = false; //tandain faisal
            $scope.partsAvailable = false;
            var tmpTask = {}
            tmpTask.isOpe = 0;
            tmpTask.ProcessId = 2;
            $scope.DataTempSelectedJob = [];
            $scope.listApi.addDetail(false, tmpTask);
            console.log('yuyuyuyuyuyu', $scope.taskCategory)
            AppointmentGrService.getDataVIN($scope.mDataDetail.VIN).then(function(res){
                console.log('cek input rangka 1', $scope.mDataDetail.VIN)
                var dataVIN = res.data.Result;
                console.log('cek res input 1', dataVIN);
                if (dataVIN.length > 0) {
                    if( dataVIN == "" ||  dataVIN == null ||  dataVIN == undefined){
                        var filtered = $scope.taskCategory.filter(function(item) { 
                        //     return item.Name !== "FLC - Free Labor Claim" && item.Name !== "FPC - Free Parts Claim" && item.Name !== "PLC - Parts Labor Claim" && item.Name !== "EWC - Extended Warranty Claim";  
                        //  });
                            return item.Name !== "FLC - Free Labor Claim" && item.Name !== "FPC - Free Parts Claim" && item.Name !== "PLC - Parts Labor Claim";  
                         });
                         console.log("filtered",filtered);
                         $scope.taskCategory = filtered;
                         console.log("$scope.taskCategory final",$scope.taskCategory);
                    }else{
                        for (var i = 0; i < dataVIN.length; i++) {
                            // if (dataVIN[i].ExtendedWarranty == 1) {
                                console.log('debug heula')
                                    if (dataVIN[i].ClaimType == "FPC - Free Parts Claim") {
                                        $scope.taskCategory = $scope.notFlcPlc
                                    }else if(dataVIN[i].ClaimType == "FLC - Free Labor Claim"){
                                        $scope.taskCategory = $scope.notFpcPlc
                                    }else if(dataVIN[i].ClaimType == "PLC - Parts Labor Claim"){
                                        $scope.taskCategory = $scope.notFlcFpc
                                    }else{
                                        $scope.taskCategory = $scope.notFlcPlcFpc ;
                                    }
                            // }else{
                            //     if (dataVIN[i].ClaimType == "FPC - Free Parts Claim") {
                            //         var filtered = $scope.notFlcPlc.filter(function(item) { 
                            //             return item.Name !== "EWC - Extended Warranty Claim";  
                            //          });
                            //          console.log("filtered",filtered);
                            //          $scope.taskCategory = filtered;
                            //     }else if(dataVIN[i].ClaimType == "FLC - Free Labor Claim"){
                            //         var filtered = $scope.notFpcPlc.filter(function(item) { 
                            //             return item.Name !== "EWC - Extended Warranty Claim";  
                            //          });
                            //          console.log("filtered",filtered);
                            //          $scope.taskCategory = filtered;
                            //     }else if(dataVIN[i].ClaimType == "PLC - Parts Labor Claim"){
                            //         var filtered = $scope.notFlcFpc.filter(function(item) { 
                            //             return item.Name !== "EWC - Extended Warranty Claim";  
                            //          });
                            //          console.log("filtered",filtered);
                            //          $scope.taskCategory = filtered;
                            //     }else{
                            //         $scope.tableListClear();
                            //         $scope.taskCategory = $scope.notFlcPlcFpcEwc;
                            //     }      
                            // }
                        }
                            console.log('debug heula geusan')
                    }
                }else{
                    $scope.tableListClear();
                    $scope.taskCategory = $scope.notFlcPlcFpc;
                }
            })
        };
        $scope.addDetail = function(data, item, itemPrice, row) {
            // debugger;
            console.log('row addDetail', row)
            console.log('clearDetail', $scope.listApi.clearDetail());
            $scope.listApi.clearDetail();
            row.FlatRate = data.FlatRate;
            row.catName = $scope.tmpCatg.Name;
            if(data.standardactualrate !== 0 ){
                row.ActualRate = data.standardactualrate
            }else{
                row.ActualRate = data.FlatRate;
            }
            row.tmpTaskId = null;
            // row.cusType=$scope.tmpCus.Name
            if ($scope.tmpPaidById !== null) {
                row.PaidById = $scope.tmpPaidById;
                row.PaidBy = $scope.tmpPaidName;
            }
            console.log("itemPrice", itemPrice);
            if (itemPrice.length > 0) {
                for (var i = 0; i < itemPrice.length; i++) {
                    console.log("scope.tmpCatg.MasterId", $scope.tmpCatg.MasterId, $scope.tmpServiceRate);
                    // if($scope.tmpCatg.MasterId !== undefined && $scope.tmpCatg.MasterId !== null){
                    //     $scope.checkServiceRate(data.FlatRate,$scope.tmpCatg.MasterId,1);
                    // }

                    // if (itemPrice[i].ServiceRate !== undefined) {
                    //     var harganya = itemPrice[i].ServiceRate;
                    //     var harganyaa = data.FlatRate;
                    //     var summary = harganya * harganyaa;
                    //     var summaryy = parseInt(summary);
                    //     row.Summary = Math.round(summaryy);
                    //     // row.Fare = itemPrice[i].AdjustmentServiceRate;
                    //     row.Fare = itemPrice[i].ServiceRate;
                    // $scope.PriceAvailable = true;
                    // } else {
                    //     row.Fare = itemPrice[i].ServiceRate;
                    //     row.Summary = row.Fare;
                    // }
                    var sum = 0;
                    var tmpItem;
                    if ($scope.tmpCatg.MasterId == 59 || $scope.tmpCatg.MasterId == 60 || $scope.tmpCatg.MasterId == 2686) {
                        if ($scope.tmpServiceRate !== null) {
                            if (data.FlatRate !== null) {
                                // * Commented for Some Reason
                                // tmpItem = data.FlatRate.toString();
                                // if (tmpItem.includes(".")) {
                                //     tmpItem = tmpItem.split('.');
                                //     if (tmpItem[tmpItem.length - 1] !== ".") {
                                // item = parseInt(tmpItem);
                                sum = $scope.tmpServiceRate.WarrantyRate * data.FlatRate;
                                sum = Math.round(sum);
                                // }
                                // } else {
                                sum = $scope.tmpServiceRate.WarrantyRate * data.FlatRate;
                                sum = Math.round(sum);
                                // }
                            } else {
                                sum = $scope.tmpServiceRate.WarrantyRate;
                                sum = Math.round(sum);
                            }
                            $scope.PriceAvailable = true;
                            sum = Math.round(sum);
                            row.Summary = sum;
                            row.Fare = $scope.tmpServiceRate.WarrantyRate;
                            console.log("row WarrantyRate lewat $scope.addDetail", row);
                        }
                    } else {
                        if ($scope.tmpServiceRate !== null) {
                            if (data.FlatRate !== null) {
                                // * Commented for Some Reason
                                // tmpItem = data.FlatRate.toString();
                                // if (tmpItem.includes(".")) {
                                //     tmpItem = tmpItem.split('.');
                                //     if (tmpItem[tmpItem.length - 1] !== ".") {
                                //         // data.FlatRate = parseInt(tmpItem);
                                //         sum = $scope.tmpServiceRate.ServiceRate * data.FlatRate;
                                //         sum = Math.round(sum);
                                //     }
                                // } else {
                                sum = $scope.tmpServiceRate.ServiceRate * data.FlatRate;
                                sum = Math.round(sum);
                                // }
                            } else {
                                sum = $scope.tmpServiceRate.ServiceRate;
                                sum = Math.round(sum);
                            }
                            //get harga
                            if (sum == 0 || sum == undefined) {
                                row.Fare = $scope.tmpServiceRate.ServiceRate;
                                $scope.PriceAvailable = false;
                            } else {
                                row.Fare = $scope.tmpServiceRate.ServiceRate;
                                $scope.PriceAvailable = true;
                                sum = Math.round(sum);
                                row.Summary = sum;
                            }

                            console.log("row ServiceRate lewat $scope.addDetail", row);
                        }
                    }
                    if(data.standardactualrate !== 0 ){
                        row.ActualRate = data.standardactualrate
                    }else{
                        row.ActualRate = data.FlatRate;
                    }
                    console.log("row terakhir lewat $scope.addDetail", row);
                }
            } else {
                $scope.PriceAvailable = false;
                row.Fare = "";
                row.Summary = "";
            }
            row.TaskId = data.TaskId;
            row.IsOPB = data.IsOPB ? data.IsOPB : null;
            var tmp = {}
                // $scope.listApi.addDetail(tmp,row)
            if (item.length > 0) {
                for (var i = 0; i < item.length; i++) {
                    // var tmpItem = item[i];
                    // $scope.getAvailablePartsService(item[i]);
                    // if ($scope.tmpPaidById !== null) {
                    //     item[i].PaidById = $scope.tmpPaidById;
                    //     item[i].paidName = $scope.tmpPaidName;
                    // }
                    // item[i].subTotal = item[i].Qty * item[i].RetailPrice;
                    // // console.log("item[i].subTotal",item[i].subTotal);
                    // tmp = item[i]
                    // $scope.listApi.addDetail(tmp, row);
                    $scope.getAvailablePartsService(item[i], row);
                }
            } else {
                $scope.listApi.addDetail(false, row);
            }
        };
        $scope.onListSelectRows = function(rows) {
            console.log("form controller=>", rows);
            $scope.dataListSelected = rows;
        };
        $scope.gridPartsDetail = {

            columnDefs: [
                { name: "No. Material", field: "PartsCode", width: '15%' },
                { name: "Nama Material", field: "PartsName", width: '20%' },
                { name: "Qty", field: "Qty", width: '6%' },
                { name: "Satuan", field: "satuanName", width: '8%' },
                // { name: "Satuan", field: "SatuanId", cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.unitDataGrid[row.entity.SatuanId]}}</div>' },
                { name: "Ketersediaan", field: "Availbility", width: '12%' },
                { name: "Tipe", field: "OrderType", width: '6%' ,
                        cellTemplate: '<div style="text-align:center;margin-top: 5px;" ng-show="row.entity.OrderType == 6">T</div><div style="text-align:center;margin-top: 5px;" ng-show="row.entity.OrderType != 6">{{row.entity.OrderType}}</div>' 
                },
                { name: "ETA", displayName: "ETA", field: "ETA", cellFilter: dateFilter, width: '11%' },
                { name: "Harga", field: "RetailPrice", width: '10%',cellFilter: 'currency:"":0' },
                
                //replace CR4
                { name: "Diskon", field: "DiskonForDisplay", width: '10%' }, 
                { name: "SubTotal", field: "subTotalForDisplay", width: '10%',cellFilter: 'currency:"":0' }, 
                //Replace CR4
                
                // { name: "Diskon", field: "Discount", width: '10%'},  // ini sebelum CR4 
                // { name: "SubTotal", field: "subTotal", width: '10%',cellFilter: 'currency:"":0' }, // ini sebelum CR4 
                
                { name: "Nilai Dp", displayName: "Nilai DP",cellFilter: 'currency:"":0', displayName: "Nilai DP", field: "DownPayment", width: '10%' },
                { name: "Status DP", displayName: "Status DP", field: "StatusDp", cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>', width: '7%' },
                { name: "Pembayaran", field: "PaidBy", width: '11%' },
                { name: "OPB", displayName: "OPB", field: "IsOPB", width: '7%', cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="null" disabled="disabled"></div>' }
            ]
        };
        // '<div class="ui-grid-cell-contents" style="text-align:center;padding-top:-10px;"><bscheckbox ng-model="MODEL_COL_FIELD" true-value="1" ng-disabled="true" false-value="0"></bscheckbox></div>'
        $scope.gridWork = gridTemp;
        $scope.sendWork = function(key, data) {
            var taskandParts = [];
            var taskList = {};
            AppointmentGrService.getDataPartsByTaskId(data.TaskId).then(function(res) {
                console.log("resabis ambil parts", res.data.Result);
                taskandParts = res.data.Result;

                //tambahan CR4  start ==> request okrivia (qty * harga parts ) - diskon
                for(i in taskandParts ){
                    taskandParts[i].Discount = $scope.DiscountParts;
                    taskandParts[i].DiskonForDisplay = String($scope.DiscountParts) + '%';
                    taskandParts[i].NominalDiscount = (taskandParts[i].Qty*taskandParts[i].RetailPrice)*($scope.DiscountParts/100);
                    taskandParts[i].subTotal = taskandParts[i].Qty*taskandParts[i].RetailPrice;
                    taskandParts[i].subTotalForDisplay = taskandParts[i].subTotal - taskandParts[i].NominalDiscount;
                }
                // tambahan CR4 end

                AppointmentGrService.getDataTaskListByKatashiki($scope.mDataDetail.KatashikiCode).then(function(restask) {
                    taskList = restask.data.Result;
                    console.log("taskList", taskList);
                    $scope.addDetail(data, taskandParts, taskList, $scope.lmModel)
                });
            });
        };
        $scope.sendParts = function(key, data) {
                var tmpMaterialId = angular.copy($scope.ldModel.MaterialTypeId);
                console.log("a $scope.ldModel.Parts", $scope.ldModel);
                $scope.ldModel = data;
                var tmp = {};
                tmp = data;
                $scope.ldModel.PartsId = data.PartsId;
                $scope.ldModel.PartsName = data.PartsName;
                $scope.ldModel.MaterialTypeId = tmpMaterialId;
                $scope.ldModel.SatuanId = data.UomId;
                $scope.ldModel.satuanName = data.Name;
                $scope.ldModel.RetailPrice = null;

                if ($scope.JobIsWarranty == 1){
                    console.log('pekerjaan nya warranty nih parts nya jg harus', $scope.ldModel.PaidById);
                    _.map($scope.paymentData, function(val) {
                        console.log("valll tipe", val);
                        if (val.Name === "Warranty") {
                            $scope.ldModel.PaidById = val.MasterId;
                            $scope.ldModel.DownPayment = 0;
                            $scope.ldModel.DPRequest = 0;
                            $scope.ldModel.minimalDp = 0;
                            $scope.disableDP = true;
                        }
                    })
    
                }else if($scope.JobIsWarranty == 2){
                    console.log('FLC PART', $scope.ldModel.PaidById);
                //     _.map($scope.paymentDataPart, function(val) {
                //       console.log("valll tipe", val);
                //       if (val.Name === "Warranty") {
                //           $scope.ldModel.PaidById = val.MasterId;
                //           $scope.ldModel.DownPayment = 0;
                //           $scope.ldModel.DPRequest = 0;
                //           $scope.ldModel.minimalDp = 0;
                //           $scope.disableDP = true;
                //       }
                //    })
                  }else if($scope.JobIsWarranty == 3){
                    console.log('fpc PART', $scope.ldModel.PaidById);
                    _.map($scope.paymentDataThereIstWarranty, function(val) {
                      if (val.Name === "Warranty") {
                          $scope.ldModel.PaidById = val.MasterId;
                          $scope.ldModel.DownPayment = 0;
                          $scope.ldModel.DPRequest = 0;
                          $scope.ldModel.minimalDp = 0;
                          $scope.disableDP = true;
                      }
                   })
                  }else if($scope.JobIsWarranty == 4){
                    console.log('PLC PART', $scope.ldModel.PaidById);
                      _.map($scope.paymentDataThereIstWarranty, function(val) {
                        if (val.Name === "Warranty") {
                            $scope.ldModel.PaidById = val.MasterId;
                            $scope.ldModel.DownPayment = 0;
                            $scope.ldModel.DPRequest = 0;
                            $scope.ldModel.minimalDp = 0;
                            $scope.disableDP = true;
                        }
                     })
                  } else {
                    console.log('pekerjaan nya bukan warranty');
                }

                // $scope.listApi.addDetail(tmp);
                console.log("key Parts", key);
                console.log("data Parts", data);
                console.log("b $scope.ldModel.Parts", $scope.ldModel);
            }
            //----------------------------------
            // Type Ahead
            //----------------------------------
        $scope.getWork = function(key) {
            var Katashiki = $scope.mDataDetail.KatashikiCode;
            var catg = $scope.tmpCatg.MasterId;
            var vehModelId = $scope.mDataDetail.VehicleModelId;
            console.log("$scope.mDataDetail.KatashikiCode", Katashiki);
            console.log("$scope.lmModel.JobType", catg);
            // if (catg == 2686) {
            //     catg = 59;
            // }
            if (Katashiki !== undefined) {
                if (Katashiki != null && catg != null && vehModelId != null) {
                    var ress = AppointmentGrService.getDataTask(key, Katashiki, catg, vehModelId).then(function(resTask) {
                        return resTask.data.Result;
                    });
                    console.log("ress", ress);
                    return ress
                }
            } else {
                return new Promise(function (resolve, reject) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Mohon Input Data Mobil Terlebih Dahulu",
                    });
                    resolve( null );
                })
            }
        };
        $scope.getParts = function(key) {
            var kategori = $scope.ldModel.MaterialTypeId;
            var isGr = 1;
            if (kategori !== undefined) {
                var ress = AppointmentGrService.getDataParts(key, isGr, kategori).then(function(resparts) {
                    console.log("respart GR", resparts);
                    var tempResult = [];
                    // tempResult.push(resparts.data.Result[0]);
                    for (var i=0; i<resparts.data.Result.length; i++){
                        // 
                        if(resparts.data.Result[i].PartsClassId1 == kategori){
                            tempResult.push(resparts.data.Result[i]);
                        }
                        
                    }
                    console.log('isGr COYYY 0', tempResult);
                    return tempResult;
                });
                console.log("ress", ress);
                return ress
            }
            // }
        };
        $scope.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };
        $scope.noResults = true;
        // var idx = gridTemp.length;
        $scope.onSelectWork = function($item, $model, $label) {
            // console.log("onSelectWork=>", idx);
            console.log("onSelectWork=> sin", $item);
            console.log("onSelectWork=> sini", $model);
            console.log("onSelectWork=> sini", $label);
            
            // if (newData != undefined && newData != undefined) {
            //     for (var i = 0; i < newData.JobTask.length; i++) {
            //         console.log('newData.JobTask[i]', newData.JobTask[i])
            //         if (newData.JobTask[i].PaidBy.MasterId == 30) {
            //             $scope.DiscountTask = 0;
            //             $scope.mData.disTask = 0;
            //         }
            //         for (var j = 0; j < newData.JobTask[i].JobParts.length; j++) {
            //             console.log('newData.JobTask[i].JobParts[j]', newData.JobTask[i].JobParts[j])
            //             if (newData.JobTask[i].JobParts[j].PaidById == 30) {
            //                 $scope.DiscountParts = 0;
            //                 $scope.mData.disParts = 0; 
            //             }
            //         }
            //     }
            // }

            if ($scope.lmModel.JobTypeId != 2678 || $scope.lmModel.JobTypeId != 2675) {
                $scope.ldModel.PaidById = 30;
                $scope.lmModel.PaidById = null;
            }

            if ($item.FlatRate != null) {
                console.log("FlatRate", $item.FlatRate);
                $scope.FlatRateAvailable = true;
            } else {
                $scope.FlatRateAvailable = false;
            }
            if($label.includes('|')){
                $label = $label.split('|');
                $label = $label[2].trim();
                if($label == 'OTHERS'){
                    $scope.FlatRateAvailable = false;
                }
            }
            $scope.sendWork($label, $item);
            console.log("modelnya", $scope.lmModel);
            // console.log('materialArray',materialArray);
        };
        $scope.onSelectParts = function($item, $model, $label) {
            // console.log("onSelectWork=>", idx);
            console.log("onSelectParts=>", $item);
            console.log("onSelectParts=>", $model);
            console.log("onSelectParts=>", $label);
            console.log("anita parts", $scope.gridPartsDetail);

            // ==== ARI SAID ======
            // for (var i=0; i<$scope.gridPartsDetail.data.length; i++){
            //     if ($label == $scope.gridPartsDetail.data[i].PartsCode){
            //         console.log('parts sudah ada');  
            //         $scope.ldModel.PartsCode = null;
            //         $scope.ldModel.PartsName = null;
            //         $item.PartsName = null;
            //         $item.PartsCode = null;
            //         bsNotify.show({
            //             size: 'big',
            //             title: 'Parts Sudah Ada!',
            //             text: 'I will close in 2 seconds.',
            //             timeout: 2000,
            //             type: 'danger'
            //         });

            //     }
            // }

            // $scope.mData.Work = $label;
            // console.log('materialArray', materialArray);
            // materialArray = [];
            // materialArray.push($item);
            if ($item.PartsName !== null) {
                $scope.partsAvailable = true;
            } else {
                $scope.partsAvailable = false;
            }
            $scope.sendParts($label, $item);
            console.log("modelnya", $item);
            // console.log('materialArray',materialArray);
        };
        $scope.onNoResult = function() {
            console.log("gak", $scope.lmModel);
            // $scope.listApi.clearDetail();
            var jmlPart = 0;
            if ($scope.cekJob != null && $scope.cekJob != undefined){
                AppointmentGrService.getDataPartsByTaskId($scope.cekJob).then(function(res) {
                    console.log("resabis ambil parts", res.data.Result);
                    jmlPart = res.data.Result.length;
                });
            }
            if (jmlPart > 0){
                $scope.listApi.clearDetail();
            }
            var row = $scope.lmModel;
            row.FlatRate = "";
            // if($scope.tmpServiceRate !== null){
            //     row.Fare = $scope.tmpServiceRate.Fare;
            //     row.Summary = $scope.tmpServiceRate.Summary;
            //     $scope.PriceAvailable = true;
            // }else{
            row.Fare = "";
            row.Summary = "";
            //     $scope.PriceAvailable = false;
            // }
            row.TaskId = null;
            row.tmpTaskId = null;
            row.ActualRate = null;
            row.catName = $scope.tmpCatg.Name;
            // row.cusType =$scope.tmpCus.Name;
            $scope.listApi.addDetail(false, row);
            console.log("uunnnnchhh", $scope.lmModel)
            $scope.jobAvailable = false;
            $scope.PriceAvailable = false;
            $scope.FlatRateAvailable = false;
        };
        $scope.onNoPartResult = function() {
            console.log("onGotResult=>");
            $scope.partsAvailable = false;
            $scope.ldModel.PartsName = null;
            $scope.ldModel.RetailPrice = null;
            $scope.ldModel.minimalDp = 0;
            $scope.ldModel.DownPayment = null;
            $scope.ldModel.ETA = null;
            $scope.ldModel.SatuanId = 4;
            $scope.ldModel.Availbility = null;
            $scope.ldModel.satuanName = null;
            $scope.ldModel.PartsId = null;
            $scope.ldModel.PartId = null;
            $scope.ldModel.TaskId = null;
            // $scope.ldModel.paidName = null;
        };
        $scope.onGotResult = function() {
            console.log("onGotResult=>");
        };
        $scope.selected = {};
        $scope.disT1 = true;
        $scope.JobIsWarranty = 0;
        $scope.selectTypeWork = function(data) {
            $scope.lmModel.JobTypeId = data.MasterId;
            console.log("selectTypeWork", data);
            console.log('pembayaran=>', $scope.lmModel.PaidById)
            if (data.MasterId == 59 || data.MasterId == 60) {
                var cek_jml_job_War = 0
                if ($scope.gridWork.length > 0) {
                    for (var i=0; i<$scope.gridWork.length; i++) {
                        if ($scope.gridWork[i].PaidById == 30) {
                            cek_jml_job_War++
                        }
                    }
                }

                if (cek_jml_job_War > 0) {
                    bsAlert.warning('Claim Warranty hanya untuk 1 Job Openo, jika ada penambahan silahkan gunakan fitur Add Openo')
                    var xdat = {}
                    xdat.MasterId = null
                    xdat.Name = null
                    $scope.selectTypeWork(xdat,null)
                    return;
                }
                
                $scope.disT1 = false;
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }]; // kl pekerjaan twc / pwc ga blh pilih bahan
                console.log('cek ldmodel', $scope.ldModel)
                $scope.ldModel.MaterialTypeId = null;
            }else if(data.MasterId == 2677 || data.MasterId == 2678 || data.MasterId == 2675 || data.MasterId == 2686){
                $scope.disT1 = true;
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }];
                $scope.ldModel.MaterialTypeId = null;
            } else {
                $scope.disT1 = true;
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
            }
            if ($scope.lmModel.JobTypeId != 60 && $scope.lmModel.JobTypeId != 59 && $scope.lmModel.JobTypeId != 2675 && $scope.lmModel.JobTypeId != 2678) {
                $scope.paymentData = $scope.somePaymentData;
            }else{
                $scope.paymentData = $scope.paymentDataThereIstWarranty;
            }
            $scope.tmpCatg = data;
            var tmpCatgName = data.Name
            var tmpModel = {};
            tmpModel.catName = data.Name;
            if (tmpCatgName == "TWC" || tmpCatgName == "PWC" || tmpCatgName == "EWC - Extended Warranty Claim") {
                $scope.JobIsWarranty = 1;
                _.map($scope.unitData,function(x){
                    if(x.Name == 'Hour'){
                        tmpModel.ProcessId = x.MasterId;
                    }
                });
                _.map($scope.paymentData, function(val) {
                    console.log("valll tipe", val);
                    if (val.Name === "Warranty") {
                        $scope.tmpPaidById = val.MasterId;
                        $scope.tmpPaidName = val.Name;
                        tmpModel.JobTypeId = data.MasterId;
                        tmpModel.PaidById = val.MasterId;
                        tmpModel.TaskName = null;
                        tmpModel.FlatRate = null;
                        tmpModel.Fare = null;
                        tmpModel.ActualRate = null;
                        tmpModel.Summary = null;
                        $scope.listApi.addDetail(false, tmpModel);
                    }
                })
                
            }else if (tmpCatgName == "FLC - Free Labor Claim") {
                $scope.JobIsWarranty = 2;
                _.map($scope.unitData,function(x){
                    if(x.Name == 'Hour'){
                        tmpModel.ProcessId = x.MasterId;
                    }
                })
                
                if ($scope.lmModel.PaidById == 30) {
                    $scope.ldModel.PaidById = null;
                }
                  _.map($scope.paymentDataThereIstWarranty, function(val){
                    if (val.Name === "Warranty") {
                      console.log('cek val flc', val);
                      $scope.tmpPaidById = val.MasterId;
                      $scope.tmpPaidName = val.Name;
                      tmpModel.JobTypeId = data.MasterId;
                      tmpModel.PaidById = val.MasterId;
                      tmpModel.TaskName = null;
                      tmpModel.FlatRate = null;
                      tmpModel.Fare = null;
                      tmpModel.ActualRate = null;
                      tmpModel.Summary = null;
                      $scope.listApi.addDetail(false, tmpModel);
                      $scope.disablePembayaran = true;
                    }
                  })
              }else if(tmpCatgName == "FPC - Free Parts Claim"){
                $scope.JobIsWarranty = 3;
                _.map($scope.unitData,function(x){
                    if(x.Name == 'Hour'){
                        tmpModel.ProcessId = x.MasterId;
                    }
                })
                if ($scope.ldModel.PaidById == 30) {
                  $scope.lmModel.PaidById = '';
                }
                $scope.lmModel.PaidById = '';
                _.map($scope.paymentDataThereIstWarranty, function(val){
                  if (val.Name === "Warranty") {
                    console.log('cek val part', val);
                    $scope.tmpPaidById = val.MasterId;
                    $scope.tmpPaidName = val.Name;
                    $scope.lmModel.PaidById = '';
                    tmpModel.JobTypeId = data.MasterId;
                    tmpModel.PaidById = val.MasterId;
                    tmpModel.TaskName = null;
                    tmpModel.FlatRate = null;
                    tmpModel.Fare = null;
                    tmpModel.ActualRate = null;
                    tmpModel.Summary = null;
                    $scope.listApi.addDetail(false, tmpModel);
                  }
                })
                _.map($scope.paymentDataIsNotWarranty, function(val){
                  if (val.Name !== "Warranty") {
                    console.log('cek val job', val);
                    console.log('masuk sini fpc');
                    $scope.tmpPaidById = null;
                    tmpModel.JobTypeId = data.MasterId;
                    tmpModel.PaidById = null;
                    tmpModel.TaskName = null;
                    tmpModel.FlatRate = null;
                    tmpModel.Fare = null;
                    tmpModel.ActualRate = null;
                    tmpModel.Summary = null;
                    $scope.listApi.addDetail(false, tmpModel);
                  }
                })
            }else if(tmpCatgName == "PLC - Parts Labor Claim"){
                $scope.JobIsWarranty = 4;
                _.map($scope.unitData,function(x){
                    if(x.Name == 'Hour'){
                        tmpModel.ProcessId = x.MasterId;
                    }
                })
                _.map($scope.paymentDataThereIstWarranty, function(val){
                    if (val.Name === "Warranty") {
                      console.log('cek val flc', val);
                      $scope.tmpPaidById = val.MasterId;
                      $scope.tmpPaidName = val.Name;
                      tmpModel.JobTypeId = data.MasterId;
                      tmpModel.PaidById = val.MasterId;
                      tmpModel.TaskName = null;
                      tmpModel.FlatRate = null;
                      tmpModel.Fare = null;
                      tmpModel.ActualRate = null;
                      tmpModel.Summary = null;
                      $scope.listApi.addDetail(false, tmpModel);
                    }
                
                  })
            } else {
                $scope.JobIsWarranty = 0;
                // $scope.paymentData = $scope.somePaymentData;
                tmpModel.JobTypeId = data.MasterId;
                $scope.tmpPaidById = null;
                $scope.lmModel.PaidById = null;
                $scope.ldModel.PaidById = null;
                tmpModel.PaidById = null;
                tmpModel.TaskName = null;
                tmpModel.FlatRate = null;
                tmpModel.Fare = null;
                tmpModel.ActualRate = null;
                tmpModel.Summary = null;
                $scope.listApi.addDetail(false, tmpModel);
                // $scope.listApi.addDetail(false, $scope.lmModel);
            }
            console.log("gridTemp", gridTemp);
            console.log("gridWork", $scope.lmModel);
        };

        $scope.CheckedVIN = function() {
            console.log('mklm')
            if ($scope.mDataDetail.VIN != null || $scope.mDataDetail.VIN != "") {
                var count = $scope.mDataDetail.VIN;
                console.log('mdata vin', count.length)
                    if (count.length <= 17) {
                        AppointmentGrService.getDataVIN($scope.mDataDetail.VIN).then(function(res){
                            console.log('cek input rangka', $scope.mDataDetail.VIN)
                            var vinMasterId = res.data.Result
                            $scope.tempVIN = res.data.Result[0]
                            if ($scope.tempVIN.ProgramName != null && $scope.tempVIN.ProgramName != undefined && $scope.tempVIN.ProgramName != "") {
                                var tempProgramName = $scope.tempVIN.ProgramName;
                                if ($scope.tempVIN.ProgramName != null && $scope.tempVIN.ProgramName != undefined) {
                                    $scope.tempVIN.ProgramName = tempProgramName
                                }else{
                                    $scope.tempVIN.ProgramName = '-'
                                } 
                            }else{
                                $scope.tempVIN.ProgramName = '-'
                            }

                            if ($scope.tempVIN.DEC_Date != null && $scope.tempVIN.DEC_Date != undefined && $scope.tempVIN.DEC_Date != "") {
                                if ($scope.tempVIN.DEC_Date != null && $scope.tempVIN.DEC_Date != undefined) {
                                    $scope.tempVIN.DEC_Date = $filter('date')($scope.tempVIN.DEC_Date, 'dd/MM/yyyy')
                                }else{
                                    $scope.tempVIN.DEC_Date = '-'
                                }
                            }else{
                                $scope.tempVIN.DEC_Date = '-'
                            }
                            
                            if ($scope.mDataDetail.VIN != null && $scope.mDataDetail.VIN != "") {
                                for(var i = 0; i < vinMasterId.length; i++){
                                    if (vinMasterId.length > 0) {
                                        // if (vinMasterId[i].ExtendedWarranty == 1) {
                                            if (vinMasterId[i].MasterId == 2678) {
                                                $scope.taskCategory = $scope.notFlcFpc;
                                            }else if(vinMasterId[i].MasterId == 2677){
                                                $scope.taskCategory = $scope.notFpcPlc
                                            }else if(vinMasterId[i].MasterId == 2675){
                                                $scope.taskCategory = $scope.notFlcPlc
                                            }else{
                                                $scope.tableListClear();
                                                $scope.taskCategory = $scope.notFlcPlcFpc
                                            }
                                        // }else{
                                        //     if (vinMasterId[i].MasterId == 2678) {
                                        //         var filtered = $scope.notFlcFpc.filter(function(item) { 
                                        //             return item.Name !== "EWC - Extended Warranty Claim";  
                                        //          });
                                        //          console.log("filtered",filtered);
                                        //          $scope.taskCategory = filtered;
                                        //     }else if(vinMasterId[i].MasterId == 2677){
                                        //         var filtered = $scope.notFpcPlc.filter(function(item) { 
                                        //             return item.Name !== "EWC - Extended Warranty Claim";  
                                        //          });
                                        //          console.log("filtered",filtered);
                                        //          $scope.taskCategory = filtered;
                                        //     }else if(vinMasterId[i].MasterId == 2675){
                                        //         var filtered = $scope.notFlcPlc.filter(function(item) { 
                                        //             return item.Name !== "EWC - Extended Warranty Claim";  
                                        //          });
                                        //          console.log("filtered",filtered);
                                        //          $scope.taskCategory = filtered;
                                        //     }else{
                                        //         $scope.tableListClear();
                                        //         $scope.taskCategory = $scope.notFlcPlcFpcEwc;
                                        //     }      
                                        // }
                                    }
                                }
                            }else{
                                $scope.tableListClear();
                                $scope.taskCategory = $scope.notFlcPlcFpc
                            }
                            
                                console.log('cek res input', $scope.tempVIN)
                                console.log('$scope.taskCategory ', $scope.taskCategory )
                        })

                        AppointmentGrService.listHistoryClaim($scope.mDataDetail.VIN).then(function(res){
                            // res.data.data = $scope.ubahFormatTanggalIndo(res.data.data.historyClaimDealerList)
                            $scope.listClaim = res.data.data;
                            $scope.tempListClaim = [];
                            // $scope.historyDate = new Date($scope.tempListClaim.claimDate)
                            console.log('cek list claim', res)
                            console.log('list claim', res.data.data)
                            console.log('list history claim', $scope.tempListClaim)
                            for (var i = 0; i < $scope.listClaim.historyClaimDealerList.length; i++) {
                                $scope.tempListClaim.push($scope.listClaim.historyClaimDealerList[i]);
                            }
                        })

                        AppointmentGrService.getTowass($scope.mDataDetail.VIN).then(function(res) {
                            console.log("data Towasssssss", res.data);
                            $scope.dataTowass = res.data.Result;
                        });

                    }else{
                        console.log('tanpa service')
                        $scope.taskCategory = $scope.notFlcPlcFpc
                    }
            }
        }

        $scope.selectTypeCust = function(data) {
            console.log("selectTypeCust", data);
            $scope.tmpCus = data;
            $scope.lmModel.Discount = $scope.DiscountTask;
            $scope.lmModel.PaidBy = data.Name;
        };
        $scope.selectTypeUnitParts = function(data) {
            console.log("selectTypeUnitParts", data);
            $scope.tmpUnit = data;
            $scope.ldModel.satuanName = data.Name;
            console.log("selectTypeUnitParts", $scope.ldModel);
        };
        $scope.disableDP = false;
        $scope.selectTypePaidParts = function(data) {
        $scope.tmpPaidName = data;
        $scope.ldModel.PaidBy = data.Name;
            if($scope.gridPartsDetail.data.length > 0){
                console.log("selectTypePaidParts IF ===>",data);
                console.log("$scope.ldModel",$scope.ldModel);
                console.log("$scope.gridPartsDetail",$scope.gridPartsDetail.data);
                for (var i = 0; i < $scope.gridPartsDetail.data.length; i++) {
                    // console.log("$scope.ldModel.PartsCode == $scope.gridPartsDetail.data[i].PartsCode",$scope.gridPartsDetail.data[j].PartsCode == $scope.gridPartsDetail.data[i].PartsCode);
                    // console.log("$scope.ldModel.PaidById == $scope.gridPartsDetail.data[i].PaidById",$scope.gridPartsDetail.data[j].PaidById == $scope.gridPartsDetail.data[i].PaidById);
                    console.log("$scope.gridPartsDetail.data[i].PaidById",$scope.gridPartsDetail.data[i].PaidById);
                    console.log("$scope.ldModel.PartsCode",$scope.ldModel.PartsCode);
                    console.log("$scope.ldModel.PaidById",$scope.ldModel.PaidById);
                    // console.log("$scope.gridPartsDetail.data[j].PaidById",$scope.gridPartsDetail.data[j].PaidById);
                    console.log("$scope.ldModel.PaidById",$scope.ldModel.PaidById);
                    //  
                    if (($scope.ldModel.PartsCode !== null && $scope.ldModel.PartsCode !== undefined) && $scope.ldModel.PartsCode == $scope.gridPartsDetail.data[i].PartsCode && data.MasterId == $scope.gridPartsDetail.data[i].PaidById ) {
                        console.log('parts sudah ada');
                        // bsNotify.show({
                        //     size: 'big',
                        //     title: 'Parts Sudah Ada!',
                        //     text: 'I will close in 2 seconds.',
                        //     timeout: 2000,
                        //     type: 'danger'
                        // });
                        bsAlert.warning('Parts Sudah Ada!','Silahkan ganti tipe pembayaran');
                        // $scope.ldModel.PaidById = null;
                        $scope.tmpPaidName = data.Name;
                        $scope.ldModel.PaidBy = data.Name;
                        $scope.ldModel.PaidById = data.MasterId;
                    }else{
                        console.log("selectTypePaidParts", data);
                        console.log('name selectTypePaidParts', data)
                        $scope.tmpPaidName = data.Name;
                        $scope.ldModel.PaidBy = data.Name;
                        $scope.ldModel.PaidById = data.MasterId;
                        console.log("selectTypePaidParts", $scope.ldModel);
                        // if (data.Name == "Internal" || data.Name == "Warranty" || data.Name == " Free Service") { kata nya selain customer pembayaran nya di 0 in, jd di singkat != customer
                            if (data.Name != "Customer") {
                                $scope.ldModel.DownPayment = 0;
                                $scope.ldModel.DPRequest = 0;
                                $scope.ldModel.minimalDp = 0;
                                $scope.disableDP = true;
                                $scope.checkPrice($scope.ldModel.RetailPrice);

                                if ($scope.ldModel.PaidById == 30){
                                    //cek kl warranty ga blh ada diskon nya
                                    //copy aja rumus yg dr CR di atas
                                    $scope.ldModel.Discount = 0;
                                    $scope.ldModel.DiskonForDisplay = String(0) + '%';
                                    $scope.ldModel.NominalDiscount = ($scope.ldModel.Qty* $scope.ldModel.RetailPrice)*(0/100);
                                    $scope.ldModel.subTotalForDisplay = $scope.ldModel.subTotal - $scope.ldModel.NominalDiscount;
                                }
                            }else {
                                console.log('pilih tipe pembayaran ==>',data.Name);
                                $scope.checkPrice($scope.ldModel.RetailPrice);
                                $scope.disableDP = false;
                                $scope.ldModel.Discount = $scope.DiscountParts;
                                $scope.ldModel.DiskonForDisplay = $scope.DiscountParts + '%';
                            };
                        }
                }
            }else{
                console.log("selectTypePaidParts ELSE ===>",data);

                // $scope.tmpPaidName = data.Name;
                // $scope.ldModel.PaidBy = data.Name;
                // $scope.ldModel.PaidById = data.MasterId;
                console.log("selectTypePaidParts", $scope.ldModel);
                // if (data.Name == "Internal" || data.Name == "Warranty" || data.Name == " Free Service") { kata nya selain customer pembayaran nya di 0 in, jd di singkat != customer
                if (data.Name != "Customer") {
                    $scope.ldModel.DownPayment = 0;
                    $scope.ldModel.DPRequest = 0;
                    $scope.ldModel.minimalDp = 0;
                    $scope.disableDP = true;
                    $scope.checkPrice($scope.ldModel.RetailPrice);

                    if ($scope.ldModel.PaidById == 30){
                        //cek kl warranty ga blh ada diskon nya
                        //copy aja rumus yg dr CR di atas
                        $scope.ldModel.Discount = 0;
                        $scope.ldModel.DiskonForDisplay = String(0) + '%';
                        $scope.ldModel.NominalDiscount = ($scope.ldModel.Qty* $scope.ldModel.RetailPrice)*(0/100);
                        $scope.ldModel.subTotalForDisplay = $scope.ldModel.subTotal - $scope.ldModel.NominalDiscount;
                    }
                } else {
                    $scope.checkPrice($scope.ldModel.RetailPrice);
                    $scope.disableDP = false;
                };
            }
        };

        $scope.selectTypeParts = function(data) {
            $scope.ldModel.SatuanId = 4;
            $scope.DisTipeOrder = false;

            $scope.ldModel.PartsCode = null;
            $scope.ldModel.PartsId = null;
            if(data.MaterialTypeId == 1){
                $scope.ldModel.IsOPB = 0;
            }
            if ($scope.JobIsWarranty == 1){
                console.log('pekerjaan nya warranty nih parts nya jg harus', $scope.ldModel.PaidById);
                _.map($scope.paymentData, function(val) {
                    console.log("valll tipe", val);
                    if (val.Name === "Warranty") {
                        $scope.ldModel.PaidById = val.MasterId;
                        $scope.ldModel.PaidBy = val.Name;
                        $scope.ldModel.DownPayment = 0;
                        $scope.ldModel.DPRequest = 0;
                        $scope.ldModel.minimalDp = 0;
                        $scope.disableDP = true;
                        console.log("BEDEBAH ====<",$scope.ldModel);
                    }
                })
                
            }else if($scope.JobIsWarranty == 2){
                //buat flc
                // _.map($scope.paymentDataPart, function(val){
                //   console.log("valll tipe", val);
                //   if (val.Name !== "Warranty") {
                //     $scope.ldModel.PaidById = val.MasterId;
                //     $scope.ldModel.PaidBy = val.Name;
                //     $scope.ldModel.DownPayment = 0;
                //     $scope.ldModel.DPRequest = 0;
                //     $scope.ldModel.minimalDp = 0;
                //     $scope.disableDP = true;
                //   }
                // })
              }else if($scope.JobIsWarranty == 3){
                _.map($scope.paymentDataThereIstWarranty, function(val){
                  if (val.Name === "Warranty") {
                    $scope.ldModel.PaidById = val.MasterId;
                    $scope.ldModel.PaidBy = val.Name;
                    $scope.ldModel.DownPayment = 0;
                    $scope.ldModel.DPRequest = 0;
                    $scope.ldModel.minimalDp = 0;
                    $scope.disableDP = true;
                  }
                })
              }else if($scope.JobIsWarranty == 4){
                _.map($scope.paymentDataThereIstWarranty, function(val){
                  if (val.Name === "Warranty") {
                    $scope.ldModel.PaidById = val.MasterId;
                    $scope.ldModel.PaidBy = val.Name;
                    $scope.ldModel.DownPayment = 0;
                    $scope.ldModel.DPRequest = 0;
                    $scope.ldModel.minimalDp = 0;
                    $scope.disableDP = true;
                  }
                })
              } else {
                console.log('pekerjaan nya bukan warranty');
            }
        }
        $scope.onListSave = function(item) {
            console.log("save_modal=>", item);
            // if(item[$scope.id]){ //Update
            //   //find the master Id
            //   var key = {};
            //   key[$scope.mId] = item[$scope.mId];
            //   var match = _.find($scope.contacts, key);
            //   var xitem = angular.copy(item);
            //   if(match){
            //       var index = _.indexOf($scope.contacts, _.find($scope.contacts, key));
            //       $scope.contacts.splice(index, 1, xitem);
            //   }
            // }else{
            //   $scope.contacts.push(item);
            // }
        };
        $scope.onListCancel = function(item) {
            console.log("cancel_modal=>", item);
        };
        $scope.onAfterCancel = function() {
            $scope.jobAvailable = false;
            $scope.FlatRateAvailable = false;
            $scope.PriceAvailable = false;
        };
        $scope.onShowModal = function(data, mode) {
            console.log("====", mode);
            console.log("=====", data);
            $scope.DataTempSelectedJob = angular.copy(data);
            // $scope.cekJob = data.TaskId karena datanya undefined, dan isi mode adalah data
            $scope.cekJob = data.TaskId
            if (data.JobTypeId == 59 || data.JobTypeId == 60 || data.JobTypeId == 2686) {
                $scope.disT1 = false;
                $scope.JobIsWarranty = 1
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }]; // kl pekerjaan twc / pwc ga blh pilih bahan
            }else if(data.JobTypeId == 2678){
                $scope.disT1 = false;
                $scope.JobIsWarranty = 4;
                // tmpTask.PaidById = ''
                $scope.disablePembayaran = true;
            }else if(data.JobTypeId == 2675){
                $scope.disT1 = false;
                $scope.JobIsWarranty = 3;
                // tmpTask.PaidById = ''
                $scope.disablePembayaran = true;
            }else if(data.JobTypeId == 2677){
                $scope.disT1 = false;
                $scope.JobIsWarranty = 2;
                // tmpTask.PaidById = ''
                $scope.disablePembayaran = true;
            }else {
                $scope.disT1 = true;
                $scope.JobIsWarranty = 0
                $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
            }

        };
        $scope.onBeforeSave = function(data, mode) {
            console.log("onBeforeSave data", data);
            console.log("onBeforeSave mode", mode);
            console.log("$scope.lmModel",$scope.lmModel);

            $scope.PriceAvailable = true;//tambahan CR4
            data.PaidBy = $scope.lmModel.PaidBy;
            // if(data.PaidById == 28){
            if(data.PaidById != 2277 && data.PaidById != 30){
                data.Discount = $scope.DiscountTask;
            }

            var tmpData = angular.copy(data);
            if(typeof tmpData.Discount == undefined){
                // if (tmpData.PaidById == 28){
                if (tmpData.PaidById != 2277 && tmpData.PaidById != 30){
                    tmpData.Discount = $scope.DiscountTask;
                } else {
                    tmpData.Discount = 0;
                }
            }
            var tmpGridWork = $scope.gridWork;
            var lengthGrid = tmpGridWork.length - 1;
            // if ($scope.tmpCus !== undefined) {
            //     tmpData.PaidBy = $scope.tmpCus.Name;
            // }
            // _.map($scope.paymentData,function(val){
            //     console.log('warranty 1', val)
            //     if(val.MasterId == $scope.tmpPaidById ){
            //         $scope.ldModel.PaidBy = val.Name;
            //         $scope.selectTypePaidParts(val);
            //     }
            // })

            tmpData.Summary = Math.round(tmpData.Fare * tmpData.FlatRate);
            // if (tmpData.PaidById == 28){
            if (tmpData.PaidById != 2277 && tmpData.PaidById != 30){
                tmpData.Discount = $scope.DiscountTask;
            } else {
                tmpData.Discount = 0;
            }

            // tmpData.PaidBy = $scope.tmpCus.Name;
            if (mode == 'new') {
                if (tmpData.tmpTaskId == null) {
                    if (tmpGridWork.length > 0) {
                        tmpData.tmpTaskId = tmpGridWork[lengthGrid].tmpTaskId + 1;
                    } else {
                        tmpData.tmpTaskId = 1;
                    }
                }
                if (data.child.length > 0) {

                    var tmpDataChild = angular.copy(data.child);
                    for (var i = 0; i < tmpDataChild.length; i++) {


                        if (tmpDataChild[i].PaidById == 2277 || tmpDataChild[i].PaidById == 30 || tmpDataChild[i].PaidById == 31) {
                            tmpDataChild[i].minimalDp = 0;
                        }
                        tmpDataChild[i].DPRequest = Math.round(tmpDataChild[i].DownPayment);
                        delete data.child[i];
                        
                    }
                    console.log("tmpDataChild",tmpDataChild);
                    $scope.listApi.addDetail(tmpDataChild, tmpData);
                } else {
                    $scope.listApi.addDetail(false, tmpData);
                };
            } else {
                if (data.child.length > 0) {
                    var tmpDataChild = angular.copy(data.child);
                    for (var i = 0; i < tmpDataChild.length; i++) {
                        if (tmpDataChild[i].PaidById == 2277 || tmpDataChild[i].PaidById == 30 || tmpDataChild[i].PaidById == 31) {
                            tmpDataChild[i].minimalDp = 0;
                        }
                        tmpDataChild[i].DPRequest = Math.round(tmpDataChild[i].DownPayment);
                        delete data.child[i];
                    }
                    $scope.listApi.addDetail(tmpDataChild, tmpData);
                } else {
                    $scope.listApi.addDetail(false, tmpData);
                };
            }
        }
        $scope.checkStatusPart = function (id, dataTask, param, jobtaskId) {
            console.log('ID ====', id);
            console.log('DATA TASK =====', dataTask);
            var tmpArray = [];
            if (param === 1 || param === 2 || param === 3 || param === 4 || param === 5) {
                for (var i in dataTask) {
                    if (dataTask[i].JobPartsId !== undefined) {
                        tmpArray.push({
                            JobPartsId: dataTask[i].JobPartsId
                        })
                    }
                }
            }
            if(param == 4){
                var idxTask = _.findIndex($scope.gridWork, { JobTaskId: jobtaskId });
                if (idxTask > -1) {
                    for(var z in dataTask){
                        $scope.gridWork[idxTask].child.push(dataTask[z]);
                    }
                }
                bsAlert.warning("Material ini sedang dalam proses order", "Batalkan PO dahulu atau tunggu PO complete");
            }
        }
        $scope.onAfterSave = function(data, mode) {
            console.log("after save data",data);

            if (data.catName == "PLC - Parts Labor Claim" || data.catName == "TWC" || data.catName == "PWC" || data.catName == "FLC - Free Labor Claim") {
                data.PaidById = $scope.lmModel.PaidById
                data.PaidBy = $scope.lmModel.PaidBy
            }

            if (data.JobTypeId == 59 || data.JobTypeId == 60 || data.JobTypeId == 2686 || data.JobTypeId == 2675 || data.JobTypeId == 2677 || data.JobTypeId == 2678) {
                if (data.TaskId == null && data.TaskId == undefined) {
                    var tempJobList = data.JobTypeId;
                    var index = $scope.gridWork.indexOf(tempJobList);
                    $scope.gridWork.splice(index, 1);
                    bsAlert.warning("Input pekerjaan Warranty & Free Claim harus menggunakan Openo / Tasklist, silakan ubah pekerjaan terlebih dahulu") 
                }
            }

            // if ($scope.gridWork[i].TaskName.includes('|')) {
                //                 var split_TaskName = $scope.gridWork[i].TaskName.split('|')
                //                 var hasilSplitTaskName = split_TaskName[0].trim()
                //                 if (hasilSplitTaskName == $scope.tempListClaim[j].openo && $scope.tempListClaim[j].statusDesc != null && $scope.tempListClaim[j].statusDesc != undefined && $scope.tempListClaim[j].statusDesc != "") {
                //                     bsAlert.warning('WO tidak dapat Release dikarenakan terdapat pekerjaan yang sudah pernah dilakukan Claim ke TOWASS, silahkan untuk ubah pekerjaan')
                //                     return false;
                //                 }
                //             }

            if (data.TaskName != null && data.TaskName != undefined) {
                if (data.TaskName.includes('|')) {
                    var split_TaskName = data.TaskName.split('|')
                    var hasilSplitTaskName = split_TaskName[0].trim()
                }
            }

            console.log('cek hasil split taskName', hasilSplitTaskName);
            if (hasilSplitTaskName != '1000km') {
                if (data.catName == "PLC - Parts Labor Claim" || data.catName == "FPC - Free Parts Claim") {
                    for (var i = 0; i < $scope.gridWork.length; i++) {
                        $scope.tempPart = $scope.gridWork[i];
                    }
                    console.log('data part child', $scope.tempPart.child)
                    console.log('data part', $scope.tempPart)
                    if($scope.tempPart.child == undefined || $scope.tempPart.child == null || $scope.tempPart.child == "") {   
                        bsAlert.warning('Harus ada Part, dengan Pembayaran Warranty, Karena tipe Pekerjaan adalah ' + $scope.tempPart.catName)
                        $scope.gridWork = [];   
                    }else{
                        console.log('gaada parts gausah kesini')
                    }
                }
            }
            
            console.log('jumlah gridwork', $scope.gridWork);
            // else{
                console.log('ada part selain flc dkk')  
                $scope.mData.disTask  = angular.copy($scope.DiscountTask);
                $scope.mData.disParts = angular.copy($scope.DiscountParts);

                console.log('gridTemp ===>',gridTemp);
                console.log("DiscountTask",$scope.DiscountTask);
                console.log("DiscountParts",$scope.DiscountParts);
                console.log('DataTempSelectedJob', $scope.DataTempSelectedJob);

                var NumberParent = 1;
                for (i in gridTemp){
                    gridTemp[i].NumberParent = NumberParent+parseInt(i);
                    for (j in gridTemp[i].child){
                        gridTemp[i].child[j].NumberChild = gridTemp[i].NumberParent + "."  + parseInt(++j);
                        
                    }
                }

                // ======================
                var tmpErasedData = []
                var tmpData = angular.copy(data);
                var countDeletedOrder = 0;
                if ($scope.DataTempSelectedJob != undefined) {
                    if ($scope.DataTempSelectedJob.child != undefined && $scope.DataTempSelectedJob.child.length > 0) {
                        for (var i = 0; i < $scope.DataTempSelectedJob.child.length; i++) {
                            var idx = 0;
                            idx = _.findIndex(tmpData.child, { JobPartsId: $scope.DataTempSelectedJob.child[i].JobPartsId });
                            if(idx == -1 && $scope.DataTempSelectedJob.child[i].MaterialRequestStatusId == 4) {
                                tmpErasedData.push($scope.DataTempSelectedJob.child[i]);
                                countDeletedOrder++;
                            }
                        }
                    }
                }
                
                if(countDeletedOrder > 0){
                    $scope.checkStatusPart($scope.mData.JobId, tmpErasedData, 4, tmpData.JobTaskId);
                }
                // ======================

                for (var i = 0; i < $scope.gridWork.length; i++) {
                    if ($scope.gridWork[i].child !== undefined) {
                        for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                            
                            // if($scope.gridWork[i].child[j].MaterialTypeId == 2){
                            // //     $scope.gridWork[i].child[j].Discount 
                            // // }else{
                            //     $scope.gridWork[i].child[j].Discount = 0;
                            // }else{
                                console.log("materialnya nih",$scope.DiscountParts);
                                // if($scope.gridWork[i].child[j].PaidById == 28){
                                if($scope.gridWork[i].child[j].PaidById != 2277 && $scope.gridWork[i].child[j].PaidById != 30){
                                    $scope.gridWork[i].child[j].Discount = $scope.DiscountParts;
                                } else {
                                    $scope.gridWork[i].child[j].Discount = 0;
                                }
                                
                            // }
                        }
                    }
                };
                
                console.log("onAfterSave data", data);
                console.log("onAfterSave mode", mode);
                console.log("ini sih wkwwk", $scope.taskCategory);

                // =================================================== Total Work =======================================================================
                var totalW = 0;
                var totalWnotWarranty = 0;
                $scope.adaWarranty = 0;
                $scope.jmlJobWarranty = $scope.gridWork.length;
                for (var i = 0; i < $scope.gridWork.length; i++) {
                    if ($scope.gridWork[i].Summary !== undefined) {
                        if ($scope.gridWork[i].Summary == "") {
                            console.log("$scope.gridWork[i].Summary == kosong");
                            $scope.gridWork[i].Summary = $scope.gridWork[i].Fare * $scope.gridWork[i].FlatRate;
                        }
                        // if ($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                        if ($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                            totalW += 0
                        } else {
                            // totalW += Math.round($scope.gridWork[i].Summary / 1.1);
                            totalW += ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWork[i].Summary,
                                            // Discount: 5,
                                            // Qty: 2,
                                            Tipe: 2, 
                                            PPNPercentage: PPNPerc,
                                        });
                        }

                        if ($scope.gridWork[i].PaidById == 30){
                            $scope.adaWarranty++
                        } else {
                            // totalWnotWarranty += Math.round($scope.gridWork[i].Summary / 1.1);
                            totalWnotWarranty += ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWork[i].Summary,
                                                    // Discount: 5,
                                                    // Qty: 2,
                                                    Tipe: 2, 
                                                    PPNPercentage: PPNPerc,
                                                });
                        }
                    }
                };
        
                $scope.totalWork = totalW;  //request pak dodi dibagi 1.1 13/02/2019
                $scope.totalWorknotWarranty = totalWnotWarranty
                // =================================================== Total Work =======================================================================


                


                var totalDp = 0;
                for (var i = 0; i < $scope.gridWork.length; i++) {
                    if ($scope.gridWork[i].child !== undefined) {
                        for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                            if ($scope.gridWork[i].child[j].DownPayment !== undefined) {
                                totalDp += $scope.gridWork[i].child[j].DownPayment;
                                console.log("$scope.gridWork[i].child.DownPayment", $scope.gridWork[i].child[j].DownPayment);
                            }
                        }
                    }
                }
                $scope.totalDp = totalDp;
                var totalDefaultDp = 0;
                for (var i = 0; i < $scope.gridWork.length; i++) {
                    if ($scope.gridWork[i].child !== undefined) {
                        for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                            if ($scope.gridWork[i].child[j].minimalDp !== undefined) {
                                totalDefaultDp += $scope.gridWork[i].child[j].minimalDp;
                                console.log("$scope.gridWork[i].child.minimalDp", $scope.gridWork[i].child[j].minimalDp);
                            }
                        }
                    }
                }
                $scope.totalDpDefault = totalDefaultDp;

            

                // =================== CR4 Re-Calculate following CSV, req by Kevin =======================
                var totalMaterial_byKevin = 0;
                var totalMaterial_byKevinNotWarranty = 0;
                for (var i = 0; i < $scope.gridWork.length; i++) {
                    if ($scope.gridWork[i].child !== undefined) {
                        for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                            // if($scope.gridWork[i].child[j].PaidById == 2277){
                            if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32){
                                totalMaterial_byKevin += 0;
                            }else{

                                if($scope.gridWork[i].child[j].PaidById == 30){ 
                                    totalMaterial_byKevin += 0;
                                }else{ 
                                    // totalMaterial_byKevin += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                                    totalMaterial_byKevin += ASPricingEngine.calculate({
                                                                InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                                // Discount: 5,
                                                                Qty: $scope.gridWork[i].child[j].Qty,
                                                                Tipe: 102, 
                                                                PPNPercentage: PPNPerc,
                                                            }); 
                                }

                                if ($scope.gridWork[i].child[j].PaidById == 30){

                                } else {
                                    // totalMaterial_byKevinNotWarranty += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                                    totalMaterial_byKevinNotWarranty += ASPricingEngine.calculate({
                                                                            InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                                            // Discount: 5,
                                                                            Qty: $scope.gridWork[i].child[j].Qty,
                                                                            Tipe: 102, 
                                                                            PPNPercentage: PPNPerc,
                                                                        }); 
                                }


                            }
                        }
                    }
                };
                
                var totalMD_byKevin = 0;
                for (var i = 0; i < $scope.gridWork.length; i++) {
                    if ($scope.gridWork[i].child !== undefined) {
                        for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                            if($scope.gridWork[i].child[j].Discount == " "){
                                $scope.gridWork[i].child[j].Discount = 0;
                            }

                            // if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30 ){
                            if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32 ){
                                totalMD_byKevin += 0;
                            }else{
                                // totalMD_byKevin +=  Math.round(Math.round($scope.gridWork[i].child[j].RetailPrice /1.1 ) * ($scope.gridWork[i].child[j].Discount/100)) *  $scope.gridWork[i].child[j].Qty;
                                totalMD_byKevin +=  ASPricingEngine.calculate({
                                                        InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                        Discount: $scope.gridWork[i].child[j].Discount,
                                                        Qty: $scope.gridWork[i].child[j].Qty,
                                                        Tipe: 112, 
                                                        PPNPercentage: PPNPerc,
                                                    }); 
                                
                            }
                        }
                    }
                };


                var totalWD_byKevin = 0;
                for (var i = 0; i < $scope.gridWork.length; i++) {
                    // if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                    if($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                        totalWD_byKevin += 0;
                    }else{
                        // totalWD_byKevin +=  Math.round(Math.round($scope.gridWork[i].Summary /1.1 ) * ($scope.gridWork[i].Discount/100));
                        totalWD_byKevin +=  ASPricingEngine.calculate({
                                                InputPrice: $scope.gridWork[i].Summary,
                                                Discount: $scope.gridWork[i].Discount,
                                                // Qty: 2,
                                                Tipe: 12, 
                                                PPNPercentage: PPNPerc,
                                            }); 
                    }
                };





                // =======================================================
                // $scope.summaryJoblist = Math.round(($scope.totalWork + ($scope.totalMaterial - ($scope.totalMaterial * $scope.DiscountParts / 100))) + ((($scope.totalWork + ($scope.totalMaterial - ($scope.totalMaterial * $scope.DiscountParts / 100))) * 10) / 100));
                if ($scope.totalEstimasi >= 1000000) {
                    $scope.costMaterial = 6000;
                } else if ($scope.totalEstimasi > 250000) {
                    $scope.costMaterial = 3000;
                } else {
                    $scope.costMaterial = 0;
                }
                // =======================================================

                $scope.totalMaterial           = totalMaterial_byKevin;
                $scope.totalMaterialnotWarranty = totalMaterial_byKevinNotWarranty;
                $scope.totalMaterialDiscounted = totalMD_byKevin;
                $scope.totalWorkDiscounted     = totalWD_byKevin;
                $scope.subTotalMaterialSummary = $scope.totalMaterial - $scope.totalMaterialDiscounted;
                $scope.subTotalWorkSummary     = $scope.totalWork - $scope.totalWorkDiscounted;
                // $scope.totalPPN                = Math.floor(($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) *0.1);      
                $scope.totalPPN                = ASPricingEngine.calculate({
                                                    InputPrice: (($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * (1+PPNPerc/100)),
                                                    // Discount: 0,
                                                    // Qty: 0,
                                                    Tipe: 33, 
                                                    PPNPercentage: PPNPerc,
                                                });    
                $scope.totalEstimasi           = $scope.totalPPN + $scope.subTotalMaterialSummary + $scope.subTotalWorkSummary + $scope.costMaterial;
                // =================== CR4 Re-Calculate following CSV, req by Kevin =======================



                console.log("$scope.costMaterial", $scope.costMaterial);

                var DiscountTaxB = [];
                var DiscountPartsB = [];
                // $scope.DiscountTask = 0;
                // $scope.DiscountParts = 0;
                // for (var a = 0; a < gridTemp.length; a++) {
                //     // DiscountTaxB.push(dataWo.JobTask[a].Discount);
                //     // var maxValue = _.max(DiscountTaxB);

                //     if (gridTemp[a].child !== undefined) {
                //         for (var b = 0; b < gridTemp[a].child.length; b++) {
                //             DiscountPartsB.push(gridTemp[a].child[b].Discount);
                //             var maxValueParts = _.max(DiscountPartsB);
                //         }
                //     }
                // }
                // // $scope.DiscountTask = maxValue;
                // $scope.DiscountParts = maxValueParts;
                // ===========================

                $scope.tmpActRateChange = angular.copy($scope.tmpActRate);
                var totalW = 0;
                for (var i = 0; i < $scope.gridWork.length; i++) {
                    if ($scope.gridWork[i].ActualRate !== undefined) {
                        totalW += $scope.gridWork[i].ActualRate;
                    }
                }
                $scope.tmpActRate = totalW;
                if ($scope.tmpActRate !== $scope.tmpActRateChange) {
                    $scope.mData.StallId = 0;
                    $scope.mData.FullDate = null;
                    $scope.mData.PlanStart = null;
                    $scope.mData.PlanFinish = null;
                    $scope.mData.PlanDateStart = null;
                    //$scope.mData.AppointmentDate = null; 
                    delete $scope.mData.AppointmentDate;
                    //$scope.mData.AppointmentTime = null;
                    delete $scope.mData.AppointmentTime;
                    $scope.mData.TargetDateAppointment = null;
                    $scope.mData.FixedDeliveryTime1 = null;
                    $scope.mData.PlanDateFinish = null;
                    $scope.mData.StatusPreDiagnose = 0;
                    delete $scope.mData.PrediagScheduledTime;
                    delete $scope.mData.PrediagStallId;
                    $scope.asb.NewChips = 1;
                }

                
                //if($scope.gridWork.child[0].PartsCode == "" || $scope.gridWork.child[0].PartsCode == null)
                if($scope.ldModel.PartsCode == "" || $scope.ldModel.PartsCode == null || $scope.ldModel.PartsCode == undefined)
                {
                $scope.undefinedPart = true;
                }


                console.log("gridTemp", gridTemp);
                console.log("gridWork", $scope.gridWork);
                console.log("totalDefaultDp", totalDefaultDp);
                console.log("totalDp", totalDp);

                //nentuin diskon task & parts
                jejeranDiskonTask = [];
                jejeranDiskonParts = [];
                for(var i in $scope.gridWork){
                    // if($scope.gridWork[i].PaidById == 30|| $scope.gridWork[i].PaidById == 31 || $scope.gridWork[i].PaidById == 32 || $scope.gridWork[i].PaidById == 2277){
                    // if($scope.gridWork[i].PaidById == 30|| $scope.gridWork[i].PaidById == 2277){
                    if($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                        var zeroDiscountTask = {Discount : 0}
                        jejeranDiskonTask.push(zeroDiscountTask.Discount);
                    }else{
                        jejeranDiskonTask.push($scope.gridWork[i].Discount);
                    }
                    if($scope.gridWork[i].child.length > 0 ){
                        for(var j in $scope.gridWork[i].child){
                            // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 31 || $scope.gridWork[i].child[j].PaidById == 32 || $scope.gridWork[i].child[j].PaidById == 2277){
                            // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 2277){
                            if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32){
                                var zeroDiscountParts = {Discount : 0}
                                jejeranDiskonParts.push(zeroDiscountParts.Discount);
                            }else{
                                jejeranDiskonParts.push($scope.gridWork[i].child[j].Discount);
                            }

                            
                        }
                    }
                }
                
                
                // var filtered = $scope.gridWork.filter(function(item) { 
                //     return item.catName != "FPC - Free Parts Claim";  
                // });
                // if(data.catName == "FPC - Free Parts Claim"){
                //     // kalo FPC pembayaran pasti warranty makannya discountnya pasti 0 , dari mba merry & pak nor
                    
                //     console.log("filtered",filtered);
                //     if(filtered == 0){
                        
                //         $scope.DiscountParts = 0;
                //     }
                // }
                //nentuin diskon task & parts
                
                $scope.displayDiscountAlgorithm();
            // };
        }

            


        $scope.onAfterDelete = function(data) {

            var deleteJob = function (data) {
                $scope.sumAllPrice();
                var countDeleted = 0;
                var countDeletedOrder = 0;
                var tmpDeletedArray = [];
                var tempData = angular.copy(data);
                for (var i = 0; i < data.length; i++) {
                    console.log('data deletedd', $scope.dataListSelected)
                    for (var j = 0; j < $scope.dataListSelected.length; j++) {
                        // ==== if separate deleted uncommend this f code
                        for (var z in $scope.dataListSelected[j].child) {
                            if($scope.dataListSelected[j].child[z].MaterialRequestStatusId == 4){
                                countDeletedOrder++;
                            }
                        }
                        

                    }
                }
                if(countDeletedOrder > 0) {
                    for (var f in $scope.dataListSelected) {
                        $scope.gridWork.push($scope.dataListSelected[f]);
                    }
                    bsAlert.warning("Material ini sedang dalam proses order", "Batalkan PO dahulu atau tunggu PO complete");
                }
                console.log("tmpDeletedArray", tmpDeletedArray);
            }
            
            var NumberParent = 1;
            for (i in $scope.gridWork){
                $scope.gridWork[i].NumberParent = NumberParent+parseInt(i);
                for (j in $scope.gridWork[i].child){
                    $scope.gridWork[i].child[j].NumberChild = $scope.gridWork[i].NumberParent + "."  + parseInt(++j);
                }
            }

            // =================================================== Total Work =======================================================================
            var totalW = 0;
            var totalWnotWarranty = 0;
            $scope.adaWarranty = 0;
            $scope.jmlJobWarranty = $scope.gridWork.length;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].Summary !== undefined) {
                    if ($scope.gridWork[i].Summary == "") {
                        $scope.gridWork[i].Summary = $scope.gridWork[i].Fare * $scope.gridWork[i].FlatRate;
                    }
                   
                        // if ($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                        if ($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                            totalW += 0
                        } else {
                            // totalW += Math.round($scope.gridWork[i].Summary / 1.1);
                            totalW += ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWork[i].Summary,
                                            // Discount: 5,
                                            // Qty: 2,
                                            Tipe: 2, 
                                            PPNPercentage: PPNPerc,
                                        });
                        }

                        if ($scope.gridWork[i].PaidById == 30){
                            $scope.adaWarranty++
                        } else {
                            // totalWnotWarranty += Math.round($scope.gridWork[i].Summary / 1.1);
                            totalWnotWarranty += ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWork[i].Summary,
                                                    // Discount: 5,
                                                    // Qty: 2,
                                                    Tipe: 2, 
                                                    PPNPercentage: PPNPerc,
                                                });
                        }
                    
                }
            };
           
            $scope.totalWork = totalW;  //request pak dodi dibagi 1.1 13/02/2019
            $scope.totalWorknotWarranty = totalWnotWarranty
            // =================================================== Total Work =======================================================================


            // ============
            
            // ============
            var totalDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].DownPayment !== undefined) {
                            totalDp += $scope.gridWork[i].child[j].DownPayment;
                            console.log("$scope.gridWork[i].child.Price", $scope.gridWork[i].child[j].DownPayment);
                        }
                    }
                }
            }
            // ==========
            $scope.totalDp = totalDp;
            var totalDefaultDp = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if ($scope.gridWork[i].child[j].minimalDp !== undefined) {
                            totalDefaultDp += $scope.gridWork[i].child[j].minimalDp;
                            console.log("$scope.gridWork[i].child.Price", $scope.gridWork[i].child[j].minimalDp);
                        }
                    }
                }
            }
            $scope.totalDpDefault = totalDefaultDp;
            // =========
          
            // =================== CR4 Re-Calculate following CSV, req by Kevin =======================
            var totalMaterial_byKevin = 0;
            var totalMaterial_byKevinNotWarranty = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        // if($scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32){
                            totalMaterial_byKevin += 0;
                        }else{

                            if($scope.gridWork[i].child[j].PaidById == 30 ){ 
                                totalMaterial_byKevin += 0;
                            }else{ 
                                // totalMaterial_byKevin += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                                totalMaterial_byKevin += ASPricingEngine.calculate({
                                                            InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                            // Discount: 5,
                                                            Qty: $scope.gridWork[i].child[j].Qty,
                                                            Tipe: 102, 
                                                            PPNPercentage: PPNPerc,
                                                        }); 
                            }

                            if ($scope.gridWork[i].child[j].PaidById == 30){

                            } else {
                                // totalMaterial_byKevinNotWarranty += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                                totalMaterial_byKevinNotWarranty += ASPricingEngine.calculate({
                                                                        InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                                        // Discount: 5,
                                                                        Qty: $scope.gridWork[i].child[j].Qty,
                                                                        Tipe: 102, 
                                                                        PPNPercentage: PPNPerc,
                                                                    }); 
                            }


                        }
                    }
                }
            };
            
            var totalMD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if($scope.gridWork[i].child[j].Discount == " "){
                            $scope.gridWork[i].child[j].Discount = 0;
                        }

                        // if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30 ){
                        if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32 ){
                            totalMD_byKevin += 0;
                        }else{
                            // totalMD_byKevin +=  Math.round(Math.round($scope.gridWork[i].child[j].RetailPrice /1.1 ) * ($scope.gridWork[i].child[j].Discount/100)) *  $scope.gridWork[i].child[j].Qty;
                            totalMD_byKevin +=  ASPricingEngine.calculate({
                                                    InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                    Discount: $scope.gridWork[i].child[j].Discount,
                                                    Qty: $scope.gridWork[i].child[j].Qty,
                                                    Tipe: 112, 
                                                    PPNPercentage: PPNPerc,
                                                }); 
                            
                        }
                    }
                }
            };

            var totalWD_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                // if($scope.gridWork[i].PaidById == 2277 || $scope.gridWork[i].PaidById == 30){
                if($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                    totalWD_byKevin += 0;
                }else{
                    // totalWD_byKevin +=  Math.round(Math.round($scope.gridWork[i].Summary /1.1 ) * ($scope.gridWork[i].Discount/100));
                    totalWD_byKevin +=  ASPricingEngine.calculate({
                                            InputPrice: $scope.gridWork[i].Summary,
                                            Discount: $scope.gridWork[i].Discount,
                                            // Qty: 2,
                                            Tipe: 12, 
                                            PPNPercentage: PPNPerc,
                                        }); 
                }
            };

            // $scope.summaryJoblist = Math.round(($scope.totalWork + ($scope.totalMaterial - ($scope.totalMaterial * $scope.DiscountParts / 100))) + ((($scope.totalWork + ($scope.totalMaterial - ($scope.totalMaterial * $scope.DiscountParts / 100))) * 10) / 100));
            if ($scope.totalEstimasi >= 1000000) {
                $scope.costMaterial = 6000;
            } else if ($scope.totalEstimasi > 250000) {
                $scope.costMaterial = 3000;
            } else {
                $scope.costMaterial = 0;
            }
            // =======================================================

            $scope.totalMaterial           = totalMaterial_byKevin;
            $scope.totalMaterialnotWarranty = totalMaterial_byKevinNotWarranty;
            $scope.totalMaterialDiscounted = totalMD_byKevin;
            $scope.totalWorkDiscounted     = totalWD_byKevin;
            $scope.subTotalMaterialSummary = $scope.totalMaterial - $scope.totalMaterialDiscounted;
            $scope.subTotalWorkSummary     = $scope.totalWork - $scope.totalWorkDiscounted;
            // $scope.totalPPN                = Math.floor(($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) *0.1);      
            $scope.totalPPN                = ASPricingEngine.calculate({
                                                    InputPrice: (($scope.subTotalMaterialSummary + $scope.subTotalWorkSummary) * (1+PPNPerc/100)),
                                                    // Discount: 0,
                                                    // Qty: 0,
                                                    Tipe: 33, 
                                                    PPNPercentage: PPNPerc,
                                                });    
            $scope.totalEstimasi           = $scope.totalPPN + $scope.subTotalMaterialSummary + $scope.subTotalWorkSummary + $scope.costMaterial;


            // =================== CR4 Re-Calculate following CSV, req by Kevin =======================

            $scope.tmpActRateChange = angular.copy($scope.tmpActRate);
            var totalW = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].ActualRate !== undefined) {
                    totalW += $scope.gridWork[i].ActualRate;
                }
            }
            $scope.tmpActRate = totalW;
            if ($scope.tmpActRate !== $scope.tmpActRateChange) {
                $scope.mData.StallId = 0;
                $scope.mData.FullDate = null;
                $scope.mData.PlanStart = null;
                $scope.mData.PlanFinish = null;
                $scope.mData.PlanDateStart = null;
                //$scope.mData.AppointmentDate = null; 
                delete $scope.mData.AppointmentDate;
                //$scope.mData.AppointmentTime = null;
                delete $scope.mData.AppointmentTime;
                $scope.mData.TargetDateAppointment = null;
                $scope.mData.FixedDeliveryTime1 = null;
                $scope.mData.PlanDateFinish = null;
                $scope.mData.StatusPreDiagnose = 0;
                delete $scope.mData.PrediagScheduledTime;
                delete $scope.mData.PrediagStallId;
                $scope.asb.NewChips = 1;
            }


            //nentuin diskon task & parts
            jejeranDiskonTask = [];
            jejeranDiskonParts = [];
            for(var i in $scope.gridWork){
                // if($scope.gridWork[i].PaidById == 30|| $scope.gridWork[i].PaidById == 31 || $scope.gridWork[i].PaidById == 32 || $scope.gridWork[i].PaidById == 2277){
                // if($scope.gridWork[i].PaidById == 30|| $scope.gridWork[i].PaidById == 2277){
                if($scope.gridWork[i].PaidById != 28 && $scope.gridWork[i].PaidById != 2458 && $scope.gridWork[i].PaidById != 32){
                    var zeroDiscountTask = {Discount : 0}
                    jejeranDiskonTask.push(zeroDiscountTask.Discount);
                }else{
                    jejeranDiskonTask.push($scope.gridWork[i].Discount);
                }
                if($scope.gridWork[i].child.length > 0 ){
                    for(var j in $scope.gridWork[i].child){
                        // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 31 || $scope.gridWork[i].child[j].PaidById == 32 || $scope.gridWork[i].child[j].PaidById == 2277){
                        // if($scope.gridWork[i].child[j].PaidById == 30|| $scope.gridWork[i].child[j].PaidById == 2277){
                        if($scope.gridWork[i].child[j].PaidById != 28 && $scope.gridWork[i].child[j].PaidById != 2458 && $scope.gridWork[i].child[j].PaidById != 32){
                            var zeroDiscountParts = {Discount : 0}
                            jejeranDiskonParts.push(zeroDiscountParts.Discount);
                        }else{
                            jejeranDiskonParts.push($scope.gridWork[i].child[j].Discount);
                        }

                        
                    }
                }
            }
            //nentuin diskon task & parts
            $scope.displayDiscountAlgorithm();
            deleteJob(data);

        };
        // $scope.onSelectUser = function(item, model, label){
        //     console.log("onSelectUser=>",item,model,label);
        // }
        //====================
        $scope.checkPrice = function(data) {
            console.log("RetailPrice nya ===>", data);
            console.log("$scope.ldModel.Qty nya ==>", $scope.ldModel.Qty);
            console.log("$scope.ldModel.Qty nya2 ==>", $scope.ldModel);
            var sum = 0;
            var sumDp = 0;
            if (data !== undefined && data !== null) {
                if ($scope.ldModel.Qty !== null) {
                    var qty = $scope.ldModel.Qty;
                    var tmpDp = $scope.ldModel.nightmareIjal ? $scope.ldModel.nightmareIjal : 0;
                    if (qty !== null) {
                        sum = Math.round(qty * data);
                        sumDp = Math.round(qty * tmpDp);
                        $scope.ldModel.subTotal = sum;
                        $scope.ldModel.DownPayment = sumDp;
                        $scope.ldModel.DPRequest = sumDp;
                        $scope.ldModel.minimalDp = sumDp;
                        // $scope.ldModel.Discount = 0;

                        if ($scope.ldModel.PaidById != 28 && $scope.ldModel.PaidById != undefined){
                            $scope.ldModel.DownPayment = 0;
                            $scope.ldModel.DPRequest = 0;
                            $scope.ldModel.minimalDp = 0;
                            $scope.disableDP = true;
                        } else {
                            $scope.disableDP = false;
                        }

                        //tambahan CR4  start ==> request okrivia (qty * harga parts ) - diskon
                        // if($scope.ldModel.PaidById == 30 || $scope.ldModel.PaidById == 31 || $scope.ldModel.PaidById == 32 || $scope.ldModel.MaterialTypeId == 2){
                        // if($scope.ldModel.PaidById == 30 || $scope.ldModel.PaidById == 2277 || $scope.ldModel.MaterialTypeId == 2){
                        if($scope.ldModel.PaidById == 30 || $scope.ldModel.PaidById == 2277){
                            $scope.ldModel.Discount = 0;
                            $scope.ldModel.DiskonForDisplay = 0+'%';
                            $scope.ldModel.NominalDiscount = 0;
                            $scope.ldModel.subTotalForDisplay = sum - $scope.ldModel.NominalDiscount;
                            $scope.ldModel.subTotal = sum;
                        }else{
                            $scope.ldModel.Discount = $scope.DiscountParts;
                            $scope.ldModel.DiskonForDisplay = $scope.DiscountParts+'%';
                            $scope.ldModel.NominalDiscount = (qty*$scope.ldModel.RetailPrice)*($scope.DiscountParts/100);
                            $scope.ldModel.subTotalForDisplay = sum - $scope.ldModel.NominalDiscount;
                            $scope.ldModel.subTotal = sum;
                        }
                        // tambahan CR4 end
                        if ($scope.ldModel.PaidById == 30){
                            //cek kl warranty ga blh ada diskon nya
                            //copy aja rumus yg dr CR di atas
                            $scope.ldModel.Discount = 0;
                            $scope.ldModel.DiskonForDisplay = String(0) + '%';
                            $scope.ldModel.NominalDiscount = (qty* $scope.ldModel.RetailPrice)*(0/100);
                            $scope.ldModel.subTotalForDisplay = sum - $scope.ldModel.NominalDiscount;
                        }
                    } else {
                        $scope.ldModel.subTotal = 0;
                        $scope.ldModel.Discount = 0;
                        $scope.ldModel.DownPayment = Math.round(tmpDp);
                        $scope.ldModel.DPRequest = Math.round(tmpDp);
                        $scope.ldModel.minimalDp = Math.round(tmpDp);
                    }
                }
            } else {
                $scope.ldModel.subTotal = 0;
                $scope.ldModel.Discount = 0;
                $scope.ldModel.DownPayment = 0;
                $scope.ldModel.DPRequest = 0;
                $scope.ldModel.minimalDp = 0;
            }
        };
        $scope.checkServiceRate = function(item, data, tipe) {
                console.log("item", item);
                console.log("item", data);
                var sum = 0;
                var row = {};
                var tmpItem;
                if (data == 59 || data == 60 || data == 2686) {
                    if ($scope.tmpServiceRate !== null) {
                        if (item !== null) {
                            tmpItem = item.toString();
                            if (tmpItem.includes(".")) {
                                tmpItem = tmpItem.split('.');
                                if (tmpItem[tmpItem.length - 1] !== ".") {
                                    // item = parseInt(tmpItem);
                                    sum = $scope.tmpServiceRate.WarrantyRate * item;
                                    sum = Math.round(sum);
                                }
                            } else {
                                sum = $scope.tmpServiceRate.WarrantyRate * item;
                                sum = Math.round(sum);
                            }
                        } else {
                            sum = $scope.tmpServiceRate.WarrantyRate;
                            sum = Math.round(sum);
                        }
                        $scope.PriceAvailable = true;
                        sum = Math.round(sum);
                        row.Summary = sum;
                        row.Fare = $scope.tmpServiceRate.WarrantyRate;
                        console.log("row WarrantyRate", row);
                    }
                } else {
                    if ($scope.tmpServiceRate !== null) {
                        if (item !== null) {
                            tmpItem = item.toString();
                            if (tmpItem.includes(".")) {
                                tmpItem = tmpItem.split('.');
                                if (tmpItem[tmpItem.length - 1] !== ".") {
                                    // item = parseInt(tmpItem);
                                    sum = $scope.tmpServiceRate.ServiceRate * item;
                                    sum = Math.round(sum);
                                }
                            } else {
                                sum = $scope.tmpServiceRate.ServiceRate * item;
                                sum = Math.round(sum);
                            }
                        } else {
                            sum = $scope.tmpServiceRate.ServiceRate;
                            sum = Math.round(sum);
                        }
                        $scope.PriceAvailable = true;
                        sum = Math.round(sum);
                        row.Summary = sum;
                        row.Fare = $scope.tmpServiceRate.ServiceRate;
                        console.log("row ServiceRate", row);
                    }
                }
                row.ActualRate = item;
                console.log("row terakhir", row);
                $scope.listApi.addDetail(false, row);
            }
            // $scope.checkAvailabilityParts = function(data) {
            //     console.log("data data data Parts", data);
            //     if (data.PartsId !== undefined) {
            //         AppointmentGrService.getAvailableParts(data.PartsId).then(function(res) {
            //             console.log("Ressss abis availability", res.data);
            //             var tmpAvailability = res.data.Result[0];
            //             $scope.ldModel.RetailPrice = tmpAvailability.RetailPrice;
            //             $scope.ldModel.minimalDp = tmpAvailability.ValueDP;
            //             $scope.ldModel.ETA = tmpAvailability.DefaultETA;
            //             $scope.ldModel.Type = 3;
            //             if (tmpAvailability.isAvailable == 0) {
            //                 $scope.ldModel.Availbility = "Tidak Tersedia";
            //             } else {
            //                 $scope.ldModel.Availbility = "Tersedia";
            //             }
            //         });
            //     }
            // };
        $scope.checkDp = function(valueDP, param){
            console.log('DP =====>', valueDP, param);
            if(param.nightmareIjal == 0 && param.Availbility == "Tidak Tersedia"){
                $scope.ldModel.minimalDp = valueDP;
            }
        }
        $scope.checkIsOPB = function(data, obj){
            $timeout(function(){
                if(obj.IsOPB == 1){
                    console.log("=====", data, obj)
                    $scope.ldModel.PartsCode = null;
                    $scope.ldModel.PartsId = null;
                    $scope.ldModel.PartId = null;
                    $scope.ldModel.PartsName = null
                    $scope.partsAvailable = false;
                }else{
                    $scope.partsAvailable = true;
                }
            },100)
        }
        $scope.checkAvailabilityParts = function(data) {
            console.log("data data data Parts", data);
            if (data.PartsId !== undefined) {
                AppointmentGrService.getAvailableParts(data.PartsId, 0, $scope.ldModel.Qty).then(function(res) {
                    console.log('3')
                    // ====== validasi cek kl retailprice nya null ato 0 ga blh di pake ==================================================== start
                    if (res.data.Result[0].RetailPrice === 0 || res.data.Result[0].RetailPrice === null || res.data.Result[0].RetailPrice === undefined){
                        bsAlert.warning('Harga Retail Belum Tersedia. Harap hubungi Partsman/Petugas Gudang Bahan.');
                        return
                    }
                    // ====== validasi cek kl retailprice nya null ato 0 ga blh di pake ==================================================== end
                    console.log("Ressss abis availability", res.data);
                    var tmpAvailability = res.data.Result[0];
                    tmpAvailability.RetailPrice = Math.round(tmpAvailability.RetailPrice);
                    $scope.ldModel.RetailPrice = tmpAvailability.RetailPrice;
                    
                    console.log('any', $scope.tmpPaidById)
                    // console.log('warranty 1', val)
                    _.map($scope.paymentData,function(val){
                        if(data.PaidById == 30){
                            if(val.MasterId == data.PaidById ){
                                $scope.ldModel.PaidBy = val.Name;
                                $scope.selectTypePaidParts(val);
                            }else{
                                if(val.MasterId == $scope.tmpPaidById && $scope.JobIsWarranty != 2){
                                    $scope.ldModel.PaidBy = val.Name;
                                    return $scope.selectTypePaidParts(val);
                                }
                            }
                        }
                    })
                    
                        
                    
                    if ($scope.ldModel.PaidBy == "Internal" || $scope.ldModel.PaidBy == "Warranty" || $scope.ldModel.PaidBy == " Free Service" || $scope.JobIsWarranty == 1 || $scope.JobIsWarranty == 2 || $scope.JobIsWarranty == 3 || $scope.JobIsWarranty == 4) {
                        $scope.ldModel.DownPayment = 0;
                        $scope.ldModel.DPRequest = 0;
                        $scope.ldModel.minimalDp = 0;
                    } else {
                        $scope.ldModel.minimalDp = Math.round(tmpAvailability.PriceDP);
                        $scope.ldModel.nightmareIjal = Math.round(tmpAvailability.PriceDP);
                        $scope.ldModel.DPRequest = Math.round(tmpAvailability.PriceDP);
                        $scope.ldModel.DownPayment = Math.round(tmpAvailability.PriceDP);
                    }
                    if ($scope.ldModel.Qty !== undefined && $scope.ldModel.Qty !== null) {
                        $scope.checkPrice(tmpAvailability.RetailPrice);
                    }
                    // $scope.ldModel.minimalDp = tmpAvailability.ValueDP;

                    switch (tmpAvailability.isAvailable) {
                        case 0:
                            $scope.PartTersedia = false;
                            $scope.ldModel.Availbility = "Tidak Tersedia";
                            if (tmpAvailability.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                $scope.ldModel.ETA = tmpAvailability.DefaultETA;
                            } else {
                                $scope.ldModel.ETA = tmpAvailability.DefaultETA;
                            }
                            if(tmpAvailability.PartsClassId3 == 113 && tmpAvailability.PartsClassId1 == 1) {
                                $scope.ldModel.OrderType = 6;
                                data.OrderType = 6;
                            }
                            else {
                                $scope.ldModel.OrderType = 3;
                                $scope.ldModel.Type = 3;
                                data.OrderType = 3;
                                data.Type = 3;
                            }
                            $scope.DisTipeOrder = false;
                            break;
                        case 1:
                            $scope.ldModel.Availbility = "Tersedia";
                            $scope.DisTipeOrder = true;
                            //kl tersedia dp nya 0 in dan disable
                            $scope.ldModel.DownPayment = 0;
                            break;
                        case 2:
                            $scope.ldModel.Availbility = "Tersedia Sebagian";
                            $scope.DisTipeOrder = true;
                            if(tmpAvailability.PartsClassId3 == 113 && tmpAvailability.PartsClassId1 == 1) {
                                $scope.ldModel.OrderType = 6;
                            }
                            break;
                    }
                });
            }
        }

        $scope.getAvailablePartsService = function(item, row) {
            console.log('1 item', item)
            console.log('1 row', row)
            if (item.PartsId !== null) {
                console.log('imam 2')
                AppointmentGrService.getAvailableParts(item.PartsId,$scope.mData.JobId,item.Qty).then(function(res) {
                    console.log('1');
                    var tmpRes = res.data.Result[0];
                    console.log("tmpRes", tmpRes);
                    tmpParts = tmpRes;
                    if (tmpParts !== null) {
                        console.log("have data RetailPrice");
                        item.RetailPrice = tmpParts.RetailPrice;
                        item.MaterialTypeId = item.PartsClassId1;
                        item.subTotal = item.Qty * tmpParts.RetailPrice;
                        item.DPRequest = Math.round(tmpParts.PriceDP * item.Qty);
                        item.DownPayment = Math.round(tmpParts.PriceDP * item.Qty);
                        item.minimalDp = Math.round(tmpParts.PriceDP * item.Qty);
                        item.nightmareIjal = item.minimalDp;
                        item.Discount = 0;
                        item.SatuanId = item.UomId;

                        for (var i in $scope.unitData) {
                            if ($scope.unitData[i].MasterId == item.SatuanId) {
                                item.satuanName = $scope.unitData[i].Name;
                            }
                        }
                        
                        if (row.catName == 'FPC - Free Parts Claim' || row.catName == 'PLC - Parts Labor Claim') {
                            console.log('imam sini')
                            for (var i in $scope.paymentDataThereIstWarranty) {
                                console.log('paymentDataThereIstWarranty', $scope.paymentDataThereIstWarranty[i])
                                if ($scope.paymentDataThereIstWarranty[i].Name == "Warranty") {
                                    item.PaidById = $scope.paymentDataThereIstWarranty[i].MasterId;
                                    item.PaidBy = $scope.paymentDataThereIstWarranty[i].Name;
                                    item.Discount = 0;
                                    item.DiscountTypeId = -1;
                                    item.DiskonForDisplay = item.Discount + '%';
                                }
                            }
                        }else if(row.catName == 'FLC - Free Labor Claim'){
                            console.log('flc sini yow')
                            for (var i in $scope.paymentDataIsNotWarranty) {
                                console.log('paymentDataIsNotWarranty', $scope.paymentDataIsNotWarranty[i])
                                    if ($scope.paymentDataIsNotWarranty[i].Name == "Customer") {
                                        item.PaidById = $scope.paymentDataIsNotWarranty[i].MasterId;
                                        item.PaidBy = $scope.paymentDataIsNotWarranty[i].Name;
                                    }
                            }
                            console.log('item.paidby FLC', item.PaidBy);
                        }else{
                            for (var i in $scope.paymentData) {
                                if (row.PaidById == 30){
                                    if (_.isEqual($scope.paymentData[i].Name, "Warranty")) {
                                        item.PaidById = $scope.paymentData[i].MasterId;
                                        item.PaidBy = $scope.paymentData[i].Name;
                                    }
                                }else{
                                    for (var i in $scope.paymentData) {
                                        if ($scope.paymentData[i].Name == "Customer") {
                                            item.PaidById = $scope.paymentData[i].MasterId;
                                            item.PaidBy = $scope.paymentData[i].Name;
                                        }
                                    }
                                } 
                            }
                        }

                        if (tmpParts.isAvailable == 0) {
                            item.Availbility = "Tidak Tersedia";
                            if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                item.ETA = "";
                            } else {
                                item.ETA = tmpParts.DefaultETA;
                            }

                            if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                item.OrderType = 6;
                            }
                            else {
                                item.OrderType = 3;
                            }
                        }else if(tmpParts.isAvailable == 2){
                            item.Availbility = "Tersedia Sebagian";
                            if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                item.OrderType = 6;
                            }
                        } else {
                            item.Availbility = "Tersedia";
                            //kl tersedia dp nya 0 in dan disable
                            item.DownPayment = 0;
                        }
                    } else {
                        console.log("haven't data RetailPrice");
                    }
                    //Pengecekan untuk ngambil data service tipe pekerjaan FLC
                    if ($scope.tmpPaidById !== null) {
                        if (row.catName != 'FLC - Free Labor Claim') {
                            item.PaidById = $scope.tmpPaidById;
                            item.PaidBy = $scope.tmpPaidName;
                        }
                    }
                    
                    tmp = item;
                    $scope.listApi.addDetail(tmp, row);
                    return item;
                });
            } else {
                console.log('imam 1')
                if ($scope.tmpPaidById !== null && $scope.tmpPaidById !== undefined) {
                    item.PaidById = $scope.tmpPaidById;
                    item.PaidBy = $scope.tmpPaidName;
                }
                if (row.catName == 'FPC - Free Parts Claim' || row.catName == 'PLC - Parts Labor Claim') {
                    console.log('imam sini')
                    for (var i in $scope.paymentDataThereIstWarranty) {
                        console.log('paymentDataThereIstWarranty', $scope.paymentDataThereIstWarranty[i])
                        if ($scope.paymentDataThereIstWarranty[i].Name == "Warranty") {
                            item.PaidById = $scope.paymentDataThereIstWarranty[i].MasterId;
                            item.PaidBy = $scope.paymentDataThereIstWarranty[i].Name;
                            item.Discount = 0;
                            item.DiscountTypeId = -1;
                            item.DiskonForDisplay = item.Discount + '%';
                        }
                    }
                }else if(row.catName == 'FLC - Free Labor Claim'){
                    for (var i in $scope.paymentDataIsNotWarranty) {
                        console.log('paymentDataIsNotWarranty', $scope.paymentDataIsNotWarranty[i])
                        if ($scope.paymentDataIsNotWarranty[i].Name == "Customer") {
                            item.PaidById = $scope.paymentDataIsNotWarranty[i].MasterId;
                            item.PaidBy = $scope.paymentDataIsNotWarranty[i].Name;
                        }
                    }
                }else{
                    for (var i in $scope.paymentData) {
                        if ($scope.paymentData[i].Name == "Customer") {
                            item.PaidById = $scope.paymentData[i].MasterId;
                            item.PaidBy = $scope.paymentData[i].Name;
                        }
                    }
                }
                item.MaterialTypeId = item.PartsClassId1;
                item.RetailPrice = item.Price;
                item.subTotal = item.Qty * item.Price;
                item.DPRequest = Math.round(item.DPRequest * item.Qty);
                item.DownPayment = Math.round(item.DownPayment * item.Qty);
                item.nightmareIjal = item.minimalDp;
                item.Discount = 0;
                item.SatuanId = item.UomId;
                for (var i in $scope.unitData) {
                    if ($scope.unitData[i].MasterId == item.SatuanId) {
                        item.satuanName = $scope.unitData[i].Name;
                    }
                }
                tmp = item;
                $scope.listApi.addDetail(tmp, row);
                return item;
            }
            
        }
        $scope.getAvailablePartsServiceManual = function(item, x, y) {
            console.log("getAvailablePartsServiceManual", item);
            console.log("x : ", x);
            console.log("y : ", y);

            if (x > item.length - 1) {
                return item;
            }
            if (item[x].isDeleted !== 1) {
                if (item[x].JobParts[y] !== undefined && item[x].JobParts[y].PartsId !== null) {
                    var itemTemp = item[x].JobParts[y];
                    AppointmentGrService.getAvailableParts(itemTemp.PartsId).then(function(res) {
                        console.log('2');
                        var tmpParts = res.data.Result[0];
                        if (tmpParts !== null && tmpParts !== undefined) {
                            // console.log("have data RetailPrice",item,tmpRes);
                            item[x].JobParts[y].RetailPrice = tmpParts.RetailPrice;
                            // item[x].JobParts[y].minimalDp = tmpParts.PriceDP;
                            item[x].JobParts[y].nightmareIjal = item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty;
                            item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                            item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;

                            // item.DiscountedPrice =  (normalPrice * item.Discount)/100;
                            if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                item[x].JobParts[y].typeDiskon = -1;
                                item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                            } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
                                item[x].JobParts[y].typeDiskon = 0;
                            }
                            // if (item[x].JobParts[y].PaidBy !== null) {
                            //     item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy.Name;
                            //     // delete item[x].JobParts[y].PaidBy;
                            // };
                            // if (item[x].JobParts[y].Satuan !== null) {
                            //     item[x].JobParts[y].satuanName = item[x].JobParts[y].Satuan.Name;
                            //     // delete item[x].JobParts[y].Satuan;
                            // };
                            if (tmpParts.isAvailable == 0) {
                                item[x].JobParts[y].Availbility = "Tidak Tersedia";
                                if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                    item[x].JobParts[y].ETA = "";
                                } else {
                                    item[x].JobParts[y].ETA = tmpParts.DefaultETA;
                                }

                                if(tmpParts.PartsClassId3 == 113 && tmpParts.PartsClassId1 == 1) {
                                    item[x].JobParts[y].OrderType = 6;
                                }
                                else {
                                    item[x].JobParts[y].OrderType = 3;
                                }
                            } else {
                                item[x].JobParts[y].Availbility = "Tersedia";
                                //kl tersedia dp nya 0 in dan disable
                                item[x].JobParts[y].DownPayment = 0;
                            }
                        } else {
                            console.log("haven't data RetailPrice");
                            item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                            item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                            item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                            item[x].JobParts[y].nightmareIjal = Math.round(item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty);
                            if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                item[x].JobParts[y].typeDiskon = -1;
                                item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                            } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                                item[x].JobParts[y].typeDiskon = 0;
                            }
                            // if (item[x].JobParts[y].PaidBy !== null) {
                            //     item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy.Name;
                            //     // delete item[x].JobParts[y].PaidBy;
                            // };
                            // if (item[x].JobParts[y].Satuan !== null) {
                            //     item[x].JobParts[y].satuanName = item[x].JobParts[y].Satuan.Name;
                            //     // delete item[x].JobParts[y].Satuan;
                            // };
                        }
                        if (item[x].JobParts[y].PartsId !== null) {
                            item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                            delete item[x].JobParts[y].Part;
                        }
                        if (item[x].JobParts[y].IsOPB !== null) {
                            item[x].JobParts[y].IsOPB = item[x].JobParts[y].IsOPB;
                        }
                        Parts.push(item[x].JobParts[y]);
                        $scope.sumAllPrice();
                        // return item;                    

                        if (x <= (item.length - 1)) {
                            if (y >= (item[x].JobParts.length - 1)) {
                                if (item[x].Fare !== undefined) {
                                    var sum = item[x].Fare;
                                    var summ = item[x].FlatRate;
                                    var summaryy = Math.ceil(sum * summ);
                                    var discountedPrice = (summaryy * item[x].Discount) / 100;
                                    var normalPrice = summaryy
                                    summaryy = parseInt(summaryy);
                                }
                                if (item[x].JobType == null) {
                                    item[x].JobType = { Name: "" };
                                }
                                // if (item[x].PaidBy == null) {
                                //     item[x].PaidBy = { Name: "" }
                                // }
                                if (item[x].AdditionalTaskId == null) {
                                    item[x].isOpe = 0;
                                } else {
                                    item[x].isOpe = 1;
                                }
                                if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                    item[x].typeDiskon = -1;
                                    item[x].DiscountedPrice = normalPrice;
                                } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                    item[x].typeDiskon = 0;
                                    item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                }
                                gridTemp.push({
                                    ActualRate: item[x].ActualRate,
                                    AdditionalTaskId: item[x].AdditionalTaskId,
                                    isOpe: item[x].isOpe,
                                    Discount: item[x].Discount,
                                    JobTaskId: item[x].JobTaskId,
                                    JobTypeId: item[x].JobTypeId,
                                    DiscountedPrice: discountedPrice,
                                    typeDiskon: item[x].typeDiskon,
                                    NormalPrice: normalPrice,
                                    catName: item[x].JobType.Name,
                                    Fare: item[x].Fare,
                                    IsCustomDiscount: item[x].IsCustomDiscount,
                                    Summary: summaryy,
                                    PaidById: item[x].PaidById,
                                    JobId: item[x].JobId,
                                    TaskId: item[x].TaskId,
                                    TaskName: item[x].TaskName,
                                    FlatRate: item[x].FlatRate,
                                    ProcessId: item[x].ProcessId,
                                    TFirst1: item[x].TFirst1,
                                    PaidBy: item[x].PaidBy.Name,
                                    index: "$$" + x,
                                    child: Parts
                                });
                                $scope.sumAllPrice();
                                Parts.splice();
                                Parts = [];
                                console.log("if 1 nilai x", x);
                                $scope.getAvailablePartsServiceManual(item, x + 1, 0);

                            } else {
                                $scope.getAvailablePartsServiceManual(item, x, y + 1);
                            }
                        }
                    });
                } else if (item[x].JobParts[y] !== undefined && (item[x].JobParts[y].PartsId == undefined || item[x].JobParts[y].PartsId == null)) {
                    item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                    item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                    item[x].JobParts[y].DownPayment = item[x].JobParts[y].DownPayment;
                    // if (item[x].JobParts[y].PaidBy !== null) {
                    //     item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy.Name;
                    //     // delete item[x].JobParts[y].PaidBy;
                    // };
                    // if (item[x].JobParts[y].Satuan !== null) {
                    //     item[x].JobParts[y].satuanName = item[x].JobParts[y].Satuan.Name;
                    //     // delete item[x].JobParts[y].Satuan;
                    // };
                    tmp = item[x].JobParts[y];
                    if (item[x].JobParts[y].IsOPB !== null) {
                        item[x].JobParts[y].IsOPB = item[x].JobParts[y].IsOPB;
                    }
                    Parts.push(item[x].JobParts[y]);
                    $scope.sumAllPrice();

                    if (x <= (item.length - 1)) {
                        if (y >= (item[x].JobParts.length - 1)) {
                            if (item[x].Fare !== undefined) {
                                var sum = item[x].Fare;
                                var summ = item[x].FlatRate;
                                var summaryy = Math.ceil(sum * summ);
                                var discountedPrice = (summaryy * item[x].Discount) / 100;
                                var normalPrice = summaryy
                                summaryy = parseInt(summaryy);
                            }
                            if (item[x].JobType == null) {
                                item[x].JobType = { Name: "" };
                            }
                            // if (item[x].PaidBy == null) {
                            //     item[x].PaidBy = { Name: "" }
                            // }
                            if (item[x].AdditionalTaskId == null) {
                                item[x].isOpe = 0;
                            } else {
                                item[x].isOpe = 1;
                            }
                            if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                item[x].typeDiskon = -1;
                                item[x].DiscountedPrice = normalPrice;
                            } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                item[x].typeDiskon = 0;
                                item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                            }

                            gridTemp.push({
                                ActualRate: item[x].ActualRate,
                                AdditionalTaskId: item[x].AdditionalTaskId,
                                isOpe: item[x].isOpe,
                                Discount: item[x].Discount,
                                JobTaskId: item[x].JobTaskId,
                                JobTypeId: item[x].JobTypeId,
                                DiscountedPrice: discountedPrice,
                                typeDiskon: item[x].typeDiskon,
                                NormalPrice: normalPrice,
                                catName: item[x].JobType.Name,
                                Fare: item[x].Fare,
                                IsCustomDiscount: item[x].IsCustomDiscount,
                                Summary: summaryy,
                                PaidById: item[x].PaidById,
                                JobId: item[x].JobId,
                                TaskId: item[x].TaskId,
                                TaskName: item[x].TaskName,
                                FlatRate: item[x].FlatRate,
                                ProcessId: item[x].ProcessId,
                                TFirst1: item[x].TFirst1,
                                PaidBy: item[x].PaidBy.Name,
                                index: "$$" + x,
                                child: Parts
                            });
                            $scope.sumAllPrice();
                            Parts.splice();
                            Parts = [];
                            $scope.getAvailablePartsServiceManual(item, x + 1, 0);
                            console.log("if 2 nilai x", x);
                        } else {
                            $scope.getAvailablePartsServiceManual(item, x, y + 1);
                        }
                    }
                } else if (item[x].JobParts.length == 0) {
                    if (x <= (item.length - 1)) {
                        if (y >= (item[x].JobParts.length - 1)) {
                            if (item[x].Fare !== undefined) {
                                var sum = item[x].Fare;
                                var summ = item[x].FlatRate;
                                var summaryy = Math.ceil(sum * summ);
                                var discountedPrice = (summaryy * item[x].Discount) / 100;
                                var normalPrice = summaryy
                                summaryy = parseInt(summaryy);
                            }
                            if (item[x].JobType == null) {
                                item[x].JobType = { Name: "" };
                            }
                            // if (item[x].PaidBy == null) {
                            //     item[x].PaidBy = { Name: "" }
                            // }
                            if (item[x].AdditionalTaskId == null) {
                                item[x].isOpe = 0;
                            } else {
                                item[x].isOpe = 1;
                            }
                            if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                item[x].typeDiskon = -1;
                                item[x].DiscountedPrice = normalPrice;
                            } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                item[x].typeDiskon = 0;
                                item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;

                            }
                            gridTemp.push({
                                ActualRate: item[x].ActualRate,
                                AdditionalTaskId: item[x].AdditionalTaskId,
                                isOpe: item[x].isOpe,
                                Discount: item[x].Discount,
                                JobTaskId: item[x].JobTaskId,
                                JobTypeId: item[x].JobTypeId,
                                DiscountedPrice: discountedPrice,
                                typeDiskon: item[x].typeDiskon,
                                NormalPrice: normalPrice,
                                catName: item[x].JobType.Name,
                                Fare: item[x].Fare,
                                IsCustomDiscount: item[x].IsCustomDiscount,
                                Summary: summaryy,
                                PaidById: item[x].PaidById,
                                JobId: item[x].JobId,
                                TaskId: item[x].TaskId,
                                TaskName: item[x].TaskName,
                                FlatRate: item[x].FlatRate,
                                ProcessId: item[x].ProcessId,
                                TFirst1: item[x].TFirst1,
                                PaidBy: item[x].PaidBy.Name,
                                index: "$$" + x,
                                child: Parts
                            });
                            $scope.sumAllPrice();
                            Parts.splice();
                            Parts = [];
                            $scope.getAvailablePartsServiceManual(item, x + 1, 0);
                        } else {
                            $scope.getAvailablePartsServiceManual(item, x, y + 1);
                        }
                    }
                    // return item;                
                }

                if (x > item.length - 1) {
                    return item;
                }
            } else {
                $scope.getAvailablePartsServiceManual(item, x + 1, 0);
            }
        };

        $scope.dataGridEmail = {};
        // $scope.gridEmail = {
        //     columnDefs: [{
        //         name: 'No Polisi',
        //         field: 'LicensePlate'
        //     }, {
        //         name: 'Nama Pelanggan',
        //         field: 'Name'
        //     }, {
        //         name: 'Email',
        //         field: 'Email'
        //     }]
        // };

        $scope.sendEmail = function(row) {
            console.log('row mode sendEmail', row);
            var arr = [];
            AppointmentGrService.sendEmail(row).then(function(resu) {
                console.log('resu sendEmail', resu.data);
                $scope.formApi.setMode('grid');
                $scope.show_modal.email = false;
            });
            // var nextDate = new Date();
            // nextDate.setHours(24, 0, 0, 0);
            // console.log('nextDate', nextDate);
            // _.map(row.Grid, function(val) {
            //     console.log('val row.grid >>', val);
            //     var obj = {};
            //     obj.Sender = Sender;
            //     obj.SendTo = val.Email;
            //     obj.Message = EmailMessage;
            //     obj.InsertTime = new Date();
            // obj.AsHtml = false;
            // obj.RetryCount = 0;
            // obj.Status = null;
            //     arr.push(obj);
            // });
            // FollowupService.sendSMS(arr).then(function(resu) {
            //     $scope.dataGrid.SMSMessage = "";
            //     console.log('smsText', resu.data);
            // });
        };
        $scope.saveApprovalDP = function(data) {
            AppointmentGrService.getWobyJobId(data.JobId).then(function(res) {
                var tmpDataForApprovalParts = [];
                var tmpApproval = res.data.Result[0];
                console.log("sukses tmpApproval", tmpApproval);
                // var tmpDataForApprovalPart = tmpDataForApproval.JobParts;
                for (var k in tmpApproval.JobTask) {
                    for (var i in tmpApproval.JobTask[k].JobParts) {
                        if (tmpApproval.JobTask[k].JobParts[i].DownPayment < tmpApproval.JobTask[k].JobParts[i].minimalDp) {
                            console.log("mpApproval.JobTask[k].JobParts[i]", tmpApproval.JobTask[k].JobParts[i]);
                            tmpDataForApprovalParts.push({
                                JobId: data.JobId,
                                DPDefault: tmpApproval.JobTask[k].JobParts[i].minimalDp,
                                DPRequested: tmpApproval.JobTask[k].JobParts[i].DownPayment,
                                JobPartsId: tmpApproval.JobTask[k].JobParts[i].JobPartsId,
                                ApprovalCategoryId: 38,
                                ApproverId: null,
                                RequesterId: null,
                                ApproverRoleId: data.ApproverRoleId,
                                RequestorRoleId: $scope.user.RoleId,
                                RequestReason: "Request Pengurangan DP",
                                RequestDate: new Date(),
                                StatusApprovalId: 3,
                                StatusCode: 1,
                                VehicleTypeId: tmpApproval.VehicleTypeId
                            })
                        }
                    }
                }
                console.log("tmpDataForApprovalParts", tmpDataForApprovalParts);
                AppointmentGrService.postApprovalDp(tmpDataForApprovalParts).then(function(res) {
                    console.log("sukses oostapproval");
                });
            });
        }
        $scope.cancelEmail = function(row, mode) {
            console.log('row mode cancel', row, mode);
            selectedRow = [];
            // $scope.gridEmail.data = [];
            $scope.dataGridEmail = [];
            // $scope.dataGridEmail.EmailMessage = '';
            $scope.dataGridEmail.EmailMessage = '<p>Testing Template ' + $scope.coba2 + '</p>'
        };
        $scope.coba2 = 'Faisal Email Provider';
        $scope.buttonEmail = {
            save: {
                text: 'Kirim',
                icon: 'fa fa-fw fa fa-envelope',
                func: function() {
                    // _.forEach($scope.dataGridEmail.Grid, function(e) {
                    //     e.emailCount--;
                    // });
                    console.log('$scope.dataGrid email', $scope.dataGridEmail);
                    // $scope.grid.data = $scope.dataGrid;
                }
            }
        };

        $scope.getParameterEmail = function(row) {
            console.log('getParameterEmail >>>', row);
            $scope.dataFromAppointment = row;
            AppointmentGrService.getParamEmail().then(function(resu) {
                console.log('resu >>>', resu.data);
                var paramEmail = angular.copy(resu.data.Result);

                _.map($scope.woCategory, function(val) {
                    console.log('val ><><>', val);
                    if (val.MasterId == row.WoCategoryId) {
                        // console.log('val ><><>', val);
                        $scope.valueCategory = val;
                        // $scope.dataGridEmail.Subject = 'Appointment {{valueCategory.Name}} - {{$scope.dataFromAppointment.LicensePlate}}';
                    };
                });
                var a = new Date(row.AppointmentDate);
                var yearFirst = a.getFullYear();
                var monthFirst = a.getMonth() + 1;
                var dayFirst = a.getDate();
                $scope.TanggalAppointment = yearFirst + '-' + monthFirst + '-' + dayFirst;
                // $scope.LicensePlateAppointment = row.LicensePlate;
                // $scope.dataGridEmail.Sender = 'dms-noreply@toyota.astra.co.id';
                // $scope.dataGridEmail.EmailMessage = resu.obj.Message;
                _.map(paramEmail, function(val, key) {
                    if (key == 0) {
                        $scope.dataGridEmail.Sender = val.Name;
                    } else if (key == 1) {
                        $scope.dataGridEmail.Subject = val.Name + ' ' + $scope.valueCategory.Name + ' - ' + $scope.dataFromAppointment.LicensePlate;
                    } else {
                        var string = val.Name.split('|');
                        console.log('String >>>', string);
                        var arr = [];
                        _.map(string, function(val, key) {
                            if (key === 0) {
                                arr[key] = val + $scope.AppointmentNo;
                            } else if (key == 1) {
                                arr[key] = val + $scope.TanggalAppointment;
                            } else if (key == 2) {
                                arr[key] = val + row.LicensePlate;
                            } else if (key == 3) {
                                arr[key] = val + row.Name;
                            } else {
                                arr[key] = val;
                            }
                        });
                        $scope.dataGridEmail.EmailMessage = arr.toString();
                    }
                });
            });
        };

        $scope.summernoteOptions2 = {
            dialogsInBody: true,
            dialogsFade: false,
            height: 300
        };

        $scope.cetakAppointment = function(JobId) {
            console.log('JobId cetakAppointment', JobId);
            var data = $scope.mData;
            $scope.printAppoinmentGR = 'as/PrintAppointmentGr/' + JobId + '/' + $scope.user.OrgId + '/' + 1;
            $scope.cetakan($scope.printAppoinmentGR);
        };

        $scope.cetakan = function(data) {
            var pdfFile = null;

            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame 
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    }
                    else {
                        printJS(pdfFile);
                    }
                }
                else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };


        $scope.onBeforeSaveJobRequest = function (data,mode){
            console.log("dataaa on save", data);
            console.log("mode on save", mode);
            console.log('lreq',$scope.lmRequest)
            console.log('litem',$scope.lmItem)
            console.log('$scope.JobRequest',$scope.JobRequest)
            // $scope.JobRequest = [];
            // $scope.JobRequest.push(data);
        }

        $scope.onBeforeSaveJobComplaint =  function (data,mode){
            console.log("dataaa on save", data);
            console.log("mode on save", mode);
            console.log('lreq',$scope.lmRequest)
            console.log('litem',$scope.lmItem)
            console.log('$scope.JobRequest',$scope.JobRequest)
            // $scope.JobComplaint = [];
            // $scope.JobComplaint.push(data);
        }


        $scope.ubahPartsCode = function(data){
            console.log('ubur ubur',data)
            // kata pa ari kl part yang ga pake partscode (manual), ga blh di kasi diskon pas release wo nya
            // hrs isi dr listodo dl partscode nya, baru kasi diskon di wo list
            if (data.PartsCode != null && data.PartsCode != undefined){
                if (data.PartsCode.length < 4){
                    $scope.ldModel.DiscountTypeId = -1;
                    $scope.ldModel.DiscountTypeListId = null;
                    $scope.ldModel.Discount = 0;
                    $scope.ldModel.PartsId = null; // kl mau harga nya enable waktu dia ubah partscode

                    // ========================= hilangkan tipe diskon material jika ganti part, hrs click "Cek" dl ====================== start
                    // for (var p=0; p<$scope.diskonData.length; p++){
                    //     if ($scope.diskonData[p].MasterId == 3){
                    //         $scope.diskonData.splice(p,1)
                    //     }
                    // }
                    // ========================= hilangkan tipe diskon material jika ganti part, hrs click "Cek" dl ====================== end
                }
            }
        }

        $scope.checkNameParts = function(data, model) {
            if ($scope.ldModel.IsOPB == null && data != null) {
                $scope.ldModel.RetailPrice = 0;
            } else {
                $scope.ldModel.RetailPrice = "";
            }

            if (model.MaterialTypeId == 1 && (model.PartsCode == null || model.PartsCode == undefined || model.PartsCode == '')) {
                model.OrderType = 3
                model.Type = 3

            }

        }


        // ===========================
        // Allow Pattern
        // ===========================
        $scope.allowPattern = function(event, type, item) {
            console.log("event", event);
            // if(event.charCode == 13){
            //     $scope.getData();
            // }
            var patternRegex
            if (type == 1) {
                patternRegex = /\d/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /\d|[a-z]|[*]/i; //ALPHANUMERIC ONLY
                if (item.includes("*")) {
                    event.preventDefault();
                    return false;
                }
            } else if (type == 3) {
                patternRegex = /\d|[a-z]/i;
            }
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };

        $scope.allowPatternFilter = function(event, type, item) {
            console.log("event", event);
            if (event.charCode == 13) {
                $scope.getData(true);
            }
            var patternRegex
            if (type == 1) {
                patternRegex = /\d/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /\d|[a-z]|[*]/i; //ALPHANUMERIC ONLY
                if (item.includes("*")) {
                    event.preventDefault();
                    return false;
                }
            } else if (type == 3) {
                patternRegex = /\d|[a-z]/i;
            }
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };

        $scope.givePattern = function(item, event) {
            console.log("item", item);
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
                $scope.nilaiKM = item;
                if ($scope.nilaiKM.includes(",")) {
                    $scope.nilaiKM = $scope.nilaiKM.split(',');
                    if ($scope.nilaiKM.length > 1) {
                        $scope.nilaiKM = $scope.nilaiKM.join('');
                    } else {
                        $scope.nilaiKM = $scope.nilaiKM[0];
                    }
                }
                $scope.nilaiKM = parseInt($scope.nilaiKM);
                $scope.mData.Km = $scope.mData.Km.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                return;
            }
        };

        $scope.dissallowSemicolon = function(event){
            var patternRegex = /[\x3B]/i;
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        }

        $scope.ubahFormatTanggalIndo = function(data){
            console.log('tanggal indo', data)
            for (var i = 0; i < data.length; i++) {
                var tgl = 1;
                var bulan = 'Januari';
                var tahun = 1900;
                if (data[i].claimDate != null && data[i].claimDate != undefined) {
                    tgl = data[i].claimDate.getDate()
                    tahun = data[i].claimDate.getFullYear()
                    bulan = data[i].claimDate.getMonth()
                    switch (bulan) {
                        case 0:
                            bulan = 'Januari'
                            break;
                        case 1:
                            bulan = 'Februari'
                            break;
                        case 2:
                            bulan = 'Maret'
                            break;
                        case 3:
                            bulan = 'April'
                            break;
                        case 4:
                            bulan = 'Mei'
                            break;
                        case 5:
                            bulan = 'Juni'
                            break;
                        case 6:
                            bulan = 'Juli'
                            break;
                        case 7:
                            bulan = 'Agustus'
                            break;
                        case 8:
                            bulan = 'September'
                            break;
                        case 9:
                            bulan = 'Oktober'
                            break;
                        case 10:
                            bulan = 'November'
                            break;
                        case 11:
                            bulan = 'Desember'
                            break;
                        default: 
                            bulan = 'Januari'
                            break;
                    }
                    data[i].claimDateIndo = tgl + ' - ' + bulan + ' - ' + tahun
                }
            }
            return data;
        }

        $scope.CekServiceHistory = function (category, vin, startDate, endDate) {
            VehicleHistory.getData(category, vin, startDate, endDate).then(
                function (res) {

                    for(var i in res.data){
                        var tmpArray = [];
                        for(var j = 0; j<res.data[i].VehicleJobService.length; j++){
                            if(res.data[i].VehicleJobService[j].KTPNo == null || res.data[i].VehicleJobService[j].KTPNo == undefined){
                                res.data[i].VehicleJobService[j].KTPNo = ''
                            }
                            if(!(res.data[i].VehicleJobService[j].KTPNo.includes('/'))){
                                if(res.data[i].VehicleJobService[j].EmployeeName !== null){
                                    tmpArray.push(res.data[i].VehicleJobService[j].EmployeeName);
                                }
                            }
                            
                        }
                        tmpArray = $scope.getUnique(tmpArray);
                        res.data[i].namaTeknisi = tmpArray.join(', ');
                    }

                    console.log('DATA HISTORY ====>',res.data);
                    $scope.mDataVehicleHistory = res.data;
                    $scope.isKmHistory = $scope.mDataVehicleHistory.length;

                    if ($scope.isKmHistory >= 1) {

                        $scope.KMTerakhir = $scope.mDataVehicleHistory[0].Km;

                        $scope.restart_digit_odo = 0
                        WO.GetResetCounterOdometer(vin).then(function(res) {
                            $scope.restart_digit_odo = res.data
                        })

                    }

                    console.log('Data History', $scope.isKmHistory);
                    console.log("kesini ga?");

                    console.log("mDataVehicleHistory=> CEK SERVICE TERAKHIR ", $scope.mDataVehicleHistory);
                },
                function (err) { }
            );
        }
    });