angular.module('app')
    .factory('AppointmentGrService', function($http, CurrentUser) {
        var currUser = CurrentUser.user();
        console.log(currUser);
        return {
            getData: function(type, filter, param) {
                if (type == 0) {
                    if (filter.includes("*")) {
                        filter = filter.split('*');
                        filter = filter[0];
                    }
                    var res = $http.get('/api/crm/GetCustomerListFilterByLicenePlate/' + filter + '/-/1/' + param);
                    console.log("filterFactory", filter);
                } else {
                    var res = $http.get('/api/crm/GetCustomerListFilterByLicenePlate/-/' + filter + '/1/' + param);
                    console.log("filterFactory", filter);
                }

                return res;
            },
            // getDataTask: function(filter){
            //   var res=$http.get('/api/as/TaskLists?KatashikiCode=ACV30R-JEMNKD&Name='+filter+'&TaskCategory=54');
            //   // console.log("/api/as/TaskLists?KatashikiCode=ACV30R-JEMNKD&Name=",filter);
            //   // console.log("resnya",res);
            //   return res;
            // },
            changeFormatDate: function(item){
                var tmpAppointmentDate = item;
                console.log("changeFormatDate item", item);
                tmpAppointmentDate = new Date(tmpAppointmentDate);
                var finalDate
                var yyyy = tmpAppointmentDate.getFullYear().toString();
                var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                var dd = tmpAppointmentDate.getDate().toString();
                finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                console.log("changeFormatDate finalDate", finalDate);
                return finalDate;
            },
            getJobSuggest: function(JobID) {
                var res = $http.get('/api/as/JobSuggestion/' + JobID + '');
                // console.log('resnya employee=>',res);
                return res;
            },
            getListStall: function(){
                var res = $http.get('/api/as/Boards/Stall/'+1);
                return res
            },
            getTowass: function(vin) {
                var res = $http.get('/api/as/Towass/getFieldAction/1/' + vin);
                return res;
            },
            getDataContactPerson: function(item) {
                console.log("item Tarik", item);
                return $http.get('/api/crm/GetVehicleUser/' + item);
            },
            getNoAppointment: function(item) {
                return $http.get('/api/as/jobs/getAppointmentNo/' + item);
            },
            getDataVehicle: function(key) {
                var res = $http.get('/api/crm/GetDataInfo/' + key);
                return res;
            },
            getVehicleTypeById: function(id) {
                var url = '/api/crm/GetCVehicleTypeById/' + id
                var res = $http.get(url);
                return res;
            },
            getDataVehiclePSFU: function(key) {
                var res = $http.get('/api/crm/GetVehicleListById/' + key);
                return res;
            },
            getDataTask: function(Key, Katashiki, catg, vehicleModelId) {
                var res = $http.get('/api/as/TaskLists?KatashikiCode=' + Katashiki + '&Name=' + Key + '&TaskCategory=' + catg + '&VehicleModelId=' + vehicleModelId);
                // var res=$http.get('/api/as/TaskLists?KatashikiCode=ASV50R-JETGKD&Name='+Key+'&TaskCategory='+catg);
                console.log('/api/as/TaskLists?KatashikiCode=' + Katashiki + '&Name=' + Key + '&TaskCategory=' + catg + '&VehicleModelId=' + vehicleModelId);
                // console.log("resnya",res);
                return res;
            },
            getDataPartsByTaskId: function(key) {
                var res = $http.get('/api/as/TaskLists/Parts?TaskId=' + key);
                return res
            },
            getAvailableParts: function(item, jobid, qtyinput) {
                // /api/as/jobs/getAvailableParts/{partid}
                // var res = $http.get('/api/as/jobs/getPartsPriceAndDefaultETDETA/' + item);
                if(jobid === undefined || jobid == null){
                    jobid = 0 ;
                }
                var res = $http.get('/api/as/jobs/getAvailablePartsAppointment/' + item + '/1?JobId='+jobid + '&Qty='+qtyinput);
                return res
            },
            getDataTaskListByKatashiki: function(key) {
                var res = $http.get('/api/as/TaskLists/TasklistSA?KatashikiCode=' + key);
                return res
            },
            getDataParts: function(Key, servicetype, type) {
                console.log("keyyyyyy", Key);
                var res = $http.get('/api/as/StockAdjustment/GetStockAdjustmentDetailFromMaterial?PartsCode=' + Key + '&ServiceTypeId=' + servicetype + '&PartsClassId1=' + type);
                // console.log("/api/as/TaskLists?KatashikiCode="+Katashiki+"&Name="+Key);
                // console.log("resnya",res);
                return res;
            },
            getDataModel: function() {
                var res = $http.get('/api/param/VehicleModels?start=1&limit=1000');
                return res;
            },
            getDataTaskList: function(item) {
                var res = $http.get('/api/as/TaskLists/TasklistSA?KatashikiCode=' + item);
                return res
            },
            getDataLain: function() {
                var res = $http.get('/api/as/AppointmentRequests/1');
                return res;
            },
            getDataEstimasi: function(filter) {
                // ====== yang ini service by filter nopolisi tapi tetep ajah kosong 
                // var res=$http.get('/api/as/jobs/EstimationList/1/NoPolisi/'+filter);
                // ====== yang ini service dengan filter vehicle id =============
                // var res = $http.get('/api/as/jobs/getEstimation/' + filter);
                // ====== yang ini service dari get data estimasi di menu estimasi
                var res = $http.get('/api/as/jobs/getListEstimationbyPoliceNumberRev/PoliceNumber/' + filter + '?isGR=1');
                console.log('resnya pause=>', res);
                return res;
            },
            // ============== Global Master =====
            getPayment: function() {
                var catId = 1008;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getUnitMeasurement: function() {
                var catId = 1;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getWoCategory: function() {
                var catId = 1007;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },

            getTaskCategory: function() {
                var catId = 1010;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getWobyJobId: function(filter) {
                var res = $http.get('/api/as/Jobs/' + filter);
                return res;
            },
            getDataGender: function() {
                var res = $http.get('/api/as/Gender/');
                return res;
            },
            updateStatus: function(data) {
                // api/crm/PutUserStatus/{CustomerVehicleId}/{VehicleUserId}
                return $http.put('/api/crm/PutUserStatus/' + data.CustomerVehicleId + '/' + data.VehicleUserId);
            },
            //===========================
            checkApproval: function(data) {
                console.log("data checkApproval", data);
                return $http.put('/api/as/CheckingApproval/', [{
                    DPRequested: data,
                    ApprovalCategoryId: 38,
                    RoleId: currUser.RoleId
                }]);
            },
            postApproval: function(data) {
                // ApproverRoleId
                console.log("postApproval", data);
                return $http.post('/api/as/PostApprovalRequestDP/1', [{
                    JobId: data.JobId,
                    DPDefault: data.totalDpDefault,
                    DPRequested: data.totalDp,
                    ApprovalCategoryId: 38,
                    ApproverId: null,
                    RequesterId: null,
                    ApproverRoleId: data.ApproverRoleId,
                    RequestorRoleId: currUser.RoleId,
                    RequestReason: "Request Pengurangan DP",
                    RequestDate: new Date(),
                    StatusApprovalId: 3,
                    StatusCode: 1,
                    VehicleTypeId: data.VehicleTypeId
                }]);
            },
            postApprovalDp: function(data) {
                // ApproverRoleId
                console.log("postApproval", data);
                return $http.post('/api/as/PostApprovalRequestDP/1', data);
            },
            //===========================
            // cancelRequest: function(data) {
            //     return $http.delete('/api/as/AppointmentRequests/', { data: data, headers: { 'Content-Type': 'application/json' } });
            // },
            cancelRequest: function(data) {
                return $http.put('/api/as/CancelAppointmentRequests', [{
                    Id: data.Id,
                    Reason: data.Reason
                }]);
            },
            rescheduleRequest: function(data) {
                return $http.put('/api/as/AppointmentRequests', [{
                    Id: data.Id,
                    VehicleId: data.VehicleId,
                    RequestAppointmentDate: data.RequestAppointmentDateNew,
                    isGr: data.isGr,
                    OutletId: data.OutletId
                }]);
            },
            createAppointment: function(data) {
                console.log("datan sebelum kirim", data);
                // coding ini dipindah ke controller
                // if(data.IsEstimation == undefined || data.IsEstimation == null ){
                //     data.JobId = 0;
                // }
                // if(data.disTask !== null || data.disTask !== undefined){
                //     console.log("oleh oleh oleh");
                //     for(var a = 0; a < data.JobTask.length; a++){
                //         if(data.JobTask[a].PaidById == 2277 || data.JobTask[a].PaidById == 30 || data.JobTask[a].PaidById == 31 || data.JobTask[a].PaidById == 32){

                //             console.log("keatas dong");
                //             data.JobTask[a].Discount = 0;
                //             data.JobTask[a].DiscountTypeId = -1;
                //             for(var b = 0; b < data.JobTask[a].JobParts.length; b++){
                //                if(data.JobTask[a].JobParts[b].PaidById == 2277 || data.JobTask[a].JobParts[b].PaidById == 30 || data.JobTask[a].JobParts[b].PaidById == 31 || data.JobTask[a].JobParts[b].PaidById == 32){

                //                    data.JobTask[a].JobParts[b].Discount = 0;
                //                    data.JobTask[a].JobParts[b].DiscountTypeId = -1;
                                  
                //                }else{
                //                    if(data.disParts !== null || data.disParts !== undefined && data.JobTask[a].JobParts.length > 0){
                //                           if(data.JobTask[a].JobParts[b].MaterialTypeId == 1){

                //                               data.JobTask[a].JobParts[b].Discount = data.disParts;
                //                               data.JobTask[a].JobParts[b].DiscountTypeId = 10;
                //                           }else{

                //                               data.JobTask[a].JobParts[b].Discount = 0;
                //                               data.JobTask[a].JobParts[b].DiscountTypeId = -1;
                //                           }
                //                    }
                //                }
                //             }

                            
                //         }else{
                //             console.log("masuk bawah dong");
                //             data.JobTask[a].Discount = data.disTask;
                //             data.JobTask[a].DiscountTypeId = 10;
                //             if(data.disParts !== null || data.disParts !== undefined && data.JobTask[a].JobParts.length > 0){
                //                 for(var b = 0; b < data.JobTask[a].JobParts.length; b++){
                //                     if(data.JobTask[a].JobParts[b].MaterialTypeId == 1){

                //                         data.JobTask[a].JobParts[b].Discount = data.disParts;
                //                         data.JobTask[a].JobParts[b].DiscountTypeId = 10;
                //                     }else{

                //                         data.JobTask[a].JobParts[b].Discount = 0;
                //                         data.JobTask[a].JobParts[b].DiscountTypeId = -1;
                //                     }
                //                 }
                //             }
                        
                //         }
                       
                //     }
                // }

                // coding ini dipindah ke controller

                var res = $http.put('/api/as/Jobs/updateDetil/1?createNo=1', [{
                    JobId: data.JobId,
                    JobNo: 0,
                    OutletId: data.OutletId,
                    CalId: 1,
                    AppointmentVia: data.AppointmentSourceId,
                    StallId: data.StallId,
                    ContactPerson: data.ContactPerson,
                    PhoneContactPerson1: data.PhoneContactPerson1,
                    Email: data.Email,
                    VehicleId: data.VehicleId,
                    PlanStart: data.PlanStart,
                    PlanFinish: data.PlanFinish,
                    CustRequest: data.CustRequest,
                    CustComplaint: data.CustComplaint,
                    TechnicianAction: 1,
                    JobSuggest: 1,
                    JobType: 0,
                    Status: 0,
                    JobTask: data.JobTask,
                    JobComplaint: data.JobComplaint,
                    JobRequest: data.JobRequest,
                    AppointmentDate: data.PlanDateStart,
                    AppointmentTime: data.AppointmentTime,
                    FixedDeliveryTime1: data.FixedDeliveryTime1,
                    AppointmentNo: null,
                    NewAppointmentRel: null,
                    isAppointment: 1,
                    isEstimation: 0,
                    isGr: 1,
                    FinalDP: data.totalDp,
                    Km: data.KmNormal,
                    isCash: 1, //ini harunya tipe pembayarannya cash, karna di UI Appointment Service GR tidak ada pilihan pembayaran.
                    isSpk: 0,
                    SpkNo: "-",
                    InsuranceName: "-",
                    InsuranceId: 0,
                    isWOBase: 1,
                    WoCategoryId: data.WoCategoryId,
                    Process: null,
                    WoNo: 0,
                    PoliceNumber: data.detail.LicensePlate,
                    VIN: data.detail.VIN,
                    ModelType: (data.VehicleModelName?data.VehicleModelName:data.ModelType),
                    KatashikiCode: data.detail.KatashikiCode,
                    VehicleTypeId: data.detail.VehicleTypeId,
                    UserIdApp: currUser.UserIdApp,
                    // EstimationDate:"",
                    // EstimationNo:"",
                    PlanDateStart: data.PlanDateStart,
                    PlanDateFinish: data.PlanDateFinish,
                    StatusPreDiagnose: data.StatusPreDiagnose,
                    JobDate: data.JobDate,
                    JobListSplitChip : data.JobListSplitChip
                }]);
                console.log('data simpan ini', res)
                return res
            },
            createAppointmentDraft: function(data) {
                console.log("datan sebelum kirim", data);
                if(data.IsEstimation == undefined || data.IsEstimation == null ){
                    data.JobId = 0;
                }
                var res = $http.put('/api/as/Jobs/updateDetil/1', [{
                    JobId: data.JobId,
                    JobNo: 0,
                    OutletId: data.OutletId,
                    CalId: 1,
                    AppointmentVia: data.AppointmentSourceId,
                    StallId: data.StallId,
                    ContactPerson: data.ContactPerson,
                    PhoneContactPerson1: data.PhoneContactPerson1,
                    Email: data.Email,
                    VehicleId: data.VehicleId,
                    PlanStart: data.PlanStart,
                    PlanFinish: data.PlanFinish,
                    CustRequest: data.CustRequest,
                    CustComplaint: data.CustComplaint,
                    TechnicianAction: 1,
                    JobSuggest: 1,
                    JobType: 0,
                    Status: 0,
                    JobTask: data.JobTask,
                    JobComplaint: data.JobComplaint,
                    FixedDeliveryTime1: data.FixedDeliveryTime1,
                    JobRequest: data.JobRequest,
                    AppointmentDate: data.PlanDateStart,
                    AppointmentTime: data.AppointmentTime,
                    AppointmentNo: null,
                    NewAppointmentRel: null,
                    isAppointment: 1,
                    isGr: 1,
                    FinalDP: data.totalDp,
                    Km: data.KmNormal,
                    isCash: 0,
                    isSpk: 0,
                    SpkNo: "-",
                    InsuranceName: "-",
                    InsuranceId: 0,
                    isWOBase: 1,
                    WoCategoryId: data.WoCategoryId,
                    Process: null,
                    WoNo: 0,
                    IsEstimation: 0,
                    PoliceNumber: data.detail.LicensePlate,
                    VIN: data.detail.VIN,
                    ModelType: data.VehicleModelName,
                    KatashikiCode: data.detail.KatashikiCode,
                    VehicleTypeId: data.detail.VehicleTypeId,
                    UserIdApp: currUser.UserIdApp,
                    // EstimationDate:"",
                    // EstimationNo:"",
                    PlanDateStart: data.PlanDateStart,
                    PlanDateFinish: data.PlanDateFinish,
                    StatusPreDiagnose: data.StatusPreDiagnose,
                    JobDate: data.JobDate,
                }]);
                return res
            },
            createPreadiagnose: function(mData) {
                // var tmpItemDate = angular.copy(mData.FirstIndication);
                // console.log("changeFormatDate item",tmpItemDate);
                // tmpItemDate = new Date(tmpItemDate);
                // console.log("tmpItemDate",tmpItemDate);
                // var finalDate = '';
                // var yyyy = tmpItemDate.getFullYear().toString();
                // var mm = (tmpItemDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                // var dd = (tmpItemDate.getDate()).toString();
                
                // // finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                // finalDate = yyyy + '-' + mm + '-' + dd;
                finalDate = this.changeFormatDate(mData.FirstIndication);
                console.log("finalDate",finalDate)
                console.log("tahu >>", mData);
                return $http.post('/api/as/PostPrediagnoses', [{
                    JobId: mData.JobId,
                    FirstIndication: finalDate,
                    FrequencyIncident: mData.FrequencyIncident,
                    Mil: mData.Mil,
                    ConditionIndication: mData.ConditionIndication,
                    MachineCondition: mData.MachineCondition,
                    MachineConditionTxt: mData.MachineConditionTxt,
                    PositionShiftLever: mData.PositionShiftLever,
                    PositionShiftLeverTxt: mData.PositionShiftLeverTxt,
                    VehicleSpeed: mData.VehicleSpeed,
                    RPM: mData.RPM,
                    VehicleLoads: mData.VehicleLoads,
                    PassengerNumber: mData.PassengerNumber,
                    ClassificationProblemR: mData.ClassificationProblemR,
                    ClassificationProblemRTxt: mData.ClassificationProblemRTxt,
                    Traffic: mData.Traffic,
                    TrafficTxt: mData.TrafficTxt,
                    WeatherConditions: mData.WeatherConditions,
                    WeatherConditionsTxt: mData.WeatherConditionsTxt,
                    CustomerFinalConfirmation: mData.CustomerFinalConfirmation,
                    ClassificationProblemM: mData.ClassificationProblemM,
                    ClassificationProblemMTxt: mData.ClassificationProblemMTxt,
                    BlowerSpeed: mData.BlowerSpeed,
                    TempSetting: mData.TempSetting,

                    SittingPosition: mData.SittingPosition,
                    TechnicalSupport: mData.TechnicalSupport,
                    PreDiagnoseTxt: mData.PreDiagnoseTxt,
                    JobDetail: mData.JobDetail,

                    notes: mData.notes,
                    IsNeedTest: mData.IsNeedTest,
                    FinalConfirmation: mData.FinalConfirmation,
                    CustomerConfirmation: mData.CustomerConfirmation,
                    LastModifiedUserId: mData.LastModifiedUserId,
                    LastModifiedDate: mData.LastModifiedDate,
                    // ComplaintCatId : mData.ComplaintCatId,
                    // DetailJSON : mData.DetailJSON,
                    ScheduledTime: mData.ScheduledTime,
                    StallId: mData.StallId,
                    OutletId: mData.OutletId
                }]);
            },
            updatePrediagnose: function(mData) {
                return $http.put('/api/as/PutPrediagnoses', [{
                    PrediagnoseId: mData.PrediagnoseId,
                    JobId: mData.JobId,
                    FirstIndication: mData.FirstIndication,
                    FrequencyIncident: mData.FrequencyIncident,
                    Mil: mData.Mil,
                    ConditionIndication: mData.ConditionIndication,
                    MachineCondition: mData.MachineCondition,
                    MachineConditionTxt: mData.MachineConditionTxt,
                    PositionShiftLever: mData.PositionShiftLever,
                    PositionShiftLeverTxt: mData.PositionShiftLeverTxt,
                    VehicleSpeed: mData.VehicleSpeed,
                    RPM: mData.RPM,
                    VehicleLoads: mData.VehicleLoads,
                    PassengerNumber: mData.PassengerNumber,
                    ClassificationProblemR: mData.ClassificationProblemR,
                    ClassificationProblemRTxt: mData.ClassificationProblemRTxt,
                    Traffic: mData.Traffic,
                    TrafficTxt: mData.TrafficTxt,
                    WeatherConditions: mData.WeatherConditions,
                    WeatherConditionsTxt: mData.WeatherConditionsTxt,
                    CustomerFinalConfirmation: mData.CustomerFinalConfirmation,
                    ClassificationProblemM: mData.ClassificationProblemM,
                    ClassificationProblemMTxt: mData.ClassificationProblemMTxt,
                    BlowerSpeed: mData.BlowerSpeed,
                    TempSetting: mData.TempSetting,

                    SittingPosition: mData.SittingPosition,
                    TechnicalSupport: mData.TechnicalSupport,
                    PreDiagnoseTxt: mData.PreDiagnoseTxt,
                    JobDetail: mData.JobDetail,

                    notes: mData.notes,
                    IsNeedTest: mData.IsNeedTest,
                    FinalConfirmation: mData.FinalConfirmation,
                    CustomerConfirmation: mData.CustomerConfirmation,
                    LastModifiedUserId: mData.LastModifiedUserId,
                    LastModifiedDate: mData.LastModifiedDate,
                    // ComplaintCatId : mData.ComplaintCatId,
                    // DetailJSON : mData.DetailJSON,
                    ScheduledTime: mData.ScheduledTime,
                    StallId: mData.StallId,
                    OutletId: mData.OutletId
                }]);
            },
            create: function(data) {
                console.log("datan sebelum kirim", data);
                var res = $http.post('/api/as/Jobs/postAppointment', [{
                    JobId: 0,
                    JobNo: 0,
                    OutletId: data.OutletId,
                    CalId: 1,
                    StallId: data.StallId,
                    ContactPerson: data.ContactPerson,
                    PhoneContactPerson1: data.PhoneContactPerson1,
                    Email: data.Email,
                    VehicleId: data.VehicleId,
                    PlanStart: data.PlanStart,
                    PlanFinish: data.PlanFinish,
                    CustRequest: data.CustRequest,
                    CustComplaint: data.CustComplaint,
                    TechnicianAction: 1,
                    JobSuggest: 1,
                    JobType: 0,
                    Status: 0,
                    JobTask: data.JobTask,
                    JobComplaint: data.JobComplaint,
                    JobRequest: data.JobRequest,
                    AppointmentDate: data.AppointmentDate,
                    AppointmentTime: data.AppointmentTime,
                    AppointmentNo: null,
                    NewAppointmentRel: null,
                    isAppointment: 1,
                    isGr: 1,
                    FixedDeliveryTime1: data.FixedDeliveryTime1,
                    Km: data.KmNormal,
                    isCash: 0,
                    isSpk: 0,
                    SpkNo: "-",
                    InsuranceId: 0,
                    InsuranceName: "-",
                    isWOBase: 1,
                    WoCategoryId: data.Category,
                    Process: null,
                    WoNo: 0,
                    IsEstimation: 0,
                    PoliceNumber: data.LicensePlate,
                    VIN: data.VIN,
                    ModelType: data.VehicleModelName,
                    KatashikiCode: data.KatashikiCode,
                    UserIdApp: currUser.UserIdApp,
                    // EstimationDate:"",
                    // EstimationNo:"",
                    PlanDateStart: data.PlanDateStart,
                    PlanDateFinish: data.PlanDateFinish,
                    JobDate: data.JobDate,
                }]);
                return res
            },
            update: function(data) {
                return $http.post('/api/as/Jobs/postAppointment', [{
                    CalId: 0,
                    StallId: 0,
                    VehicleId: 0,
                    CustRequest: data.Request,
                    CustComplaint: data.Complaint,
                    TechnicianAction: "",
                    JobSuggest: "",
                    Status: 0,
                    JobTask: data.gridTemp
                }]);
                console.log("post");
                // return $http.put('/api/fw/Role', [{
                //                                     Id: role.Id,
                //                                     //pid: role.pid,
                //                                     Name: role.Name,
                //                                     Description: role.Description}]);
            },
            delete: function(id) {
                return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
            getParamEmail: function() {
                // var obj = {};
                // obj.Message = '<p>Terima kasih atas booking service yang telah dibuat di bengkel kami. Berikut adalah informasi booking service :</p><p>No. Appointment :{{AppointmentNo}} </p><p>Tanggal Appointment : {{TanggalAppointment}} </p><p>No. Polisi :{{mData.LicensePlate }}</p><p>Nama Pemilik : {{mData.Name}}</p><p><br></p><p>Terima Kasih</p>'
                // return obj;
                var catId = 2029;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya paramEmail=>', res);
                return res;
            },
            publishPrintCetakan: function(id,noappointment) {
                var tmpNoappointment = noappointment.split('/');
                var tmpresult = tmpNoappointment.join();
                // return $http.get('/api/as/PrintAppointmentGrForAttachment/' + id + '/' + currUser.OrgId + '/' + 1)
                //api/as/PrintAppointmentGrForAttachment/{jobId}/{outletId}/{isGr}/{AppNo}
                return $http.get('/api/as/PrintAppointmentGrForAttachment/'+id+'/'+currUser.OrgId+'/'+1+'/'+tmpresult)
            },
            sendEmail: function(data) {
                return $http.post('/api/as/PostUmailQueue/', [{
                    // QueueId: data.QueueId,
                    OutletId: currUser.OrgId,
                    WorkerId: null,
                    Sender: data.Sender,
                    SendTo: data.Email,
                    SendCc: null,
                    SendBcc: null,
                    Subject: data.Subject,
                    AsHtml: 1,
                    Message: data.EmailMessage,
                    Attachment: data.AttachmentURL,
                    RetryCount: 0,
                    Status: 1,
                    InsertTime: new Date(),
                    ExecuteTime: null,
                    FetchTime: null,
                    FinisihTime: null
                }])
            },
            GetMaterialReqNo: function(ref) {
                return $http.get('/api/as/getMaterialReqNo/' + ref);
            },
            updateMRS: function(id) {
                return $http.put('/api/as/UpdateAfterPostAppointment/' + id);
            },
            putGastype: function(data) {
                console.log("data", data);
                return $http.put('/api/crm/PutVehicleAppoint', [{
                    GasTypeId: data.VehicleGasTypeId,
                    VehicleId: data.VehicleId,
                    LicensePlate: data.LicensePlate
                }])
            },
            GetCVehicleTypeListByKatashiki: function(KatashikiCode) {
                // api/crm/GetCVehicleTypeListByKatashiki/{KatashikiCode}
                return $http.get('/api/crm/GetCVehicleTypeListByKatashiki/' + KatashikiCode)
            },
            sendNotif: function(data, recepient, param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param : param
                }]);
            },
            sendToCRMHistory: function(data) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/crm/InsertCHistoryDataChanged/', [{
                    CustomerId : data.CustomerId,
                    ChangeTypeId : data.ChangeTypeId,
                    Modul : data.Modul,
                    Attribute : data.Attribute,
                    NewValue : data.NewValue,
                    OldValue : data.OldValue
                }]);
            },
            CheckLastContactPerson: function(filter, flag) {
                // api/as/CheckLastContactPerson/{filter}/{flag}
                // flag 1 kirim vehicleid
                // flag 2 kirim nopol

                return $http.get('/api/as/CheckLastContactPerson/' + filter + '/' + flag);
            },
            getDataVIN: function(vin){
                var res = $http.get('/api/as/MasterVehicle_FPCFLCPLC/' + vin)
                return res;
            },
            listHistoryClaim: function(vin){
                var res = $http.get('/api/as/ToWass/ListHistoryClaim?VIN=' + vin)
                return res;
            }            
        }
    });