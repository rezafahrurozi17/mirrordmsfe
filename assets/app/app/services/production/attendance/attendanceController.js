angular.module('app')
    .controller('AttendancesController', function($scope, $http, CurrentUser, Attendances,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mData = null; //Model
    $scope.xRole={selected:[]};
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.ismeridian = false;
    $scope.hstep = 1;
    $scope.mstep = 1;
    

    $scope.Alasan2 = [
    {
        Alasantext : "IJ - Ijin"
    },
    {
        Alasantext : "CT - Cuti"
    }
    ];
    var res = [
    {
        Kode : 'F120',
        Initial : 'LC',
        Name : 'Lifa Christian',
        Jabatan : 'Teknisi BP',
        Posisi : 'BP',
        Group : 'GGWP',
        Tanggal : '2017-01-18',
        Hadir: true,
        Availability: false,
        JamDatang : '09:00',
        JamPulang : '21:00',
        Shift : '1',
        Alasan : 'IJ - Ijin',
        Catatan : 'Nope',
        Stall : 'EM 02'
    },
    {
        Kode : 'B2200',
        Initial : 'CG',
        Name : 'WHAT',
        Jabatan : 'Teknisi CR',
        Posisi : 'BP',
        Group : 'GG',
        Tanggal : '2017-02-18',
        Hadir: false,
        Availability: true,
        JamDatang : '09:00',
        JamPulang : '21:00',
        Shift : '1',
        Alasan : 'IJ - Ijin',
        Catatan : 'Nope',
        Stall : 'EM 02'
    }
    ];

    var gridData = [];
    $scope.getData = function() {
        $scope.grid.data = res;
      // // console.log("nih", $scope.VM );
      // Towing.getData()
      //   .then(

      //     function(res) {
      //       gridData = [];
      //       //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
      //       $scope.grid.data = res.data;
      //       // console.log("role=>", res.data.Result);
      //       //console.log("grid data=>",$scope.grid.data);
      //       //$scope.roleData = res.data;
      //       $scope.loading = false;
      //       console.log("test",res);
      //     },
      //     function(err) {
      //       console.log("err=>", err);
      //     }
      //   );
    }
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Kode',    field:'Kode'},
            { name:'Initial', field:'Initial' },
            { name:'Nama',  field: 'Name' },
            { name:'Jabatan',  field: 'Jabatan' },
            { name:'Posisi',  field: 'Posisi' },
            { name:'Group', field:'Group'},
            { name:'Tanggal', field:'Tanggal'},
            { name:'Hadir', field:'Hadir', type: 'boolean', cellTemplate : '<input type="checkbox" ng-checked="row.entity.Hadir"/>'},
            { name:'Availability', field:'Availability', cellTemplate : '<input type="checkbox" ng-checked="row.entity.Availability"/>'},
            { name:'Jam Datang', field:'JamDatang'},
            { name:'Jam Pulang', field:'JamPulang'},
            { name:'Shift', field:'Shift'},
            { name:'Alasan', field:'Alasan'},
            { name:'Catatan', field:'Catatan'}
        ]
    };
});
