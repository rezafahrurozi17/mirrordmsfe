angular.module('app')
    .factory('RepairSupportActivity', function($http, CurrentUser) {
        function stringToDate(str) {

            var date = new Date(str);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            return yyyy + '-' + mm + '-' + dd;
        }

        function getToday() {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            var today = yyyy + '-' + mm + '-' + dd;
            return today;
        }
        var currentUser = CurrentUser.user;
        return {
            getData: function(startDate, stopDate, status, NoWo) {
                console.log("start date di fac", startDate);
                console.log("stop date di fac", stopDate);
                // var res=$http.get('/api/as/JobTaskProgressBPs/listProblemFinding/'+startDate+'/'+stopDate+'/WoNo/'+NoWo+'/status/'+status+'');
                var res = $http.get('/api/as/JobTaskProgressBPs/listProblemFinding/problemDate/' + startDate + '/' + stopDate + '/WoNo/' + NoWo + '/status/' + status + '');
                //console.log('res=>',res);
                return res;
            },
            putRSA: function(Model) {
                    // FinalId,
                    console.log("Vin", Model.VIN);

                    return $http.put('/api/as/JobTasks/ProblemFindingBp/' + Model.IdJobTaskProgress + '', {
                        // MarketInpactRank: Model.MarketInpactRank,
                        // TechnicalRank: Model.TechnicalRank,
                        // TransmissionType: Model.TransmissionType,
                        // StatusFailedPart: Model.StatusFailedPart,
                        // PartDelivery: Model.PartDelivery,
                        // NoLTIfRepeatRepair: Model.NoLTIfRepeatRepair,
                        // NoLTIfInvolvedPBT: Model.NoLTIfInvolvedPBT,
                        // ProblemCanBeDuplicate: Model.ProblemCanBeDuplicate,
                        // NumberOfCases: Model.NumberOfCases,
                        // InquiryTitle: Model.InquiryTitle,
                        // MethodToAct: Model.MethodToAct,
                        // ResultAction: Model.ResultAction,
                        // RequestToDCC: Model.RequestToDCC,
                        // IsSolvedProblem: Model.IsSolvedProblem,
                        // IsEscalationProblem: Model.IsEscalationProblem,
                        // ReportForm: Model.BentukLaporan,
                        // ConsultationDCCCatg: Model.ConsultationDCCCatg,
                        // IsFIR: Model.IsFIR,
                        // CatgComponent:Model.CatgComponent,
                        // SubCatgComponent:Model.SubCatgComponent,
                        // TroubleDesc:Model.TroubleDesc
                        MarketInpactRank: Model.MarketInpactRank,
                        TechnicalRank: Model.TechnicalRank,
                        TransmissionType: Model.TransmissionType,
                        StatusFailedPart: Model.StatusFailedPart,
                        PartDelivery: Model.PartDelivery,
                        NoLTIfRepeatRepair: Model.NoLTIfRepeatRepair,
                        NoLTIfInvolvedPBT: Model.NoLTIfInvolvedPBT,
                        ProblemCanBeDuplicate: Model.ProblemCanBeDuplicate,
                        NumberOfCases: Model.NumberOfCases,
                        InquiryTitle: Model.InquiryTitle,
                        MethodToAct: Model.MethodToAct,
                        ResultAction: Model.ResultAction,
                        RequestToDCC: Model.RequestToDCC,
                        IsSolvedProblem: Model.IsSolvedProblem,
                        IsEscalationProblem: Model.IsEscalationProblem,
                        ReportForm: Model.BentukLaporan,
                        ConsultationDCCCatg: Model.ConsultationDCCCatg,
                        IsFIR: Model.IsFIR,
                        CatgComponent: Model.CatgComponent,
                        Component: Model.SubCatgComponent,
                        TroubleDesc: Model.TroubleDesc,
                        RSANo: Model.rsaDocNo,
                        DccNo: Model.dccDocNo,
                        LTNo: Model.ltDocNo,
                        TfirstTahun_Pembuatan__c: Model.AssemblyYear,
                        TfirstDCCorLT: Model.BentukLaporan,
                        TfirstPenggunaan_Kendaraan__c: Model.CarUser,
                        TfirstSub_Kategori_Problem__c: Model.CategorySubProblem,
                        TfirstPenyebab_Masalah__c: Model.CausedT2,
                        TfirstPemeriksaan_dan_Langkah_Investigasi__c: Model.ConditionT1,
                        TfirstDelivery_Date__c: stringToDate(Model.DECDate),
                        TfirstRepair_Date__c: stringToDate(Model.DateStart),
                        TfirstTanggal_Komplain_Pertama__c: stringToDate(Model.DateofFirstComplain),
                        TfirstNomor_Mesin__c: Model.EngineNo,
                        TfirstTipe_Mesin__c: Model.EngineType,
                        TfirstTanggal_Masuk_Kendaraan__c: stringToDate(Model.GateInPushTime),
                        TfirstKode_Dealer_Ext__c: Model.GroupDealerCode,
                        TfirstSubject: Model.InquiryTitle,
                        TfirstKeluhan_Pelanggan__c: Model.JobComplaint,
                        TfirstKode_Tipe_Ext__c: Model.KatashikiCode,
                        TfirstOdometer__c: Model.Km,
                        TfirstTipe_Report__c: Model.OFP,
                        TfirstTanggal_Produksi_Kendaraan__c: stringToDate(Model.PLOD),
                        TfirstPengadaan_Part_Tanggal_Pesan_Tersedia__c: stringToDate(Model.PartDeliveryAvailable),
                        TfirstTanggal_Selesai_Perbaikan__c: stringToDate(Model.PlanDateFinish),
                        TfirstNomor_Polisi__c: Model.PoliceNumber,
                        TfirstTanggal_Internal_PBT__c: stringToDate(Model.ProblemFoundedDate),
                        TfirstTanggal_Kirim_PBT_ke_ATL__c: stringToDate(Model.ProblemFoundedDate),
                        TfirstKode_T1__c: Model.T1,
                        TfirstKode_T2__c: Model.T2,
                        TfirstNomor_Rangka_VIN__c: Model.VIN,
                        TfirstKode_Model_Ext__c: Model.VehicleModelCode,
                        TfirstRTJ_atau_Tidak__c: Model.isRTJ,
                        RespondDesciption: Model.DeskripsiRespon
                    });
                }
                // ,
                // create: function(mData) {
                //   return $http.post('/api/as/Towings', [{
                //                                       PoliceNumber: mData.PoliceNumber,
                //                                       VehicleModelId: mData.VehicleModelId,
                //                                       Color: mData.Color,
                //                                       EntryDate: mData.EntryDate,
                //                                       EntryTime: mData.EntryTime,
                //                                       OwnerName: mData.OwnerName,
                //                                       Phone: mData.Phone,
                //                                       Detail: mData.Detail
                //                                     }]);
                // },
                // update: function(mData){
                //   return $http.put('/api/as/Towings', [{
                //                                       TowingId : mData.TowingId,
                //                                       PoliceNumber: mData.PoliceNumber,
                //                                       VehicleModelId: mData.VehicleModelId,
                //                                       Color: mData.Color,
                //                                       EntryDate: mData.EntryDate,
                //                                       EntryTime: mData.EntryTime,
                //                                       OwnerName: mData.OwnerName,
                //                                       Phone: mData.Phone,
                //                                       Detail: mData.Detail}]);
                // }
                //,
                // delete: function(id) {
                //   return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
                // },
        }
    });