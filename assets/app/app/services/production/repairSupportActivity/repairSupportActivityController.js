angular.module('app')
    .controller('RepairSupportActivityController', function($scope, $http, CurrentUser,
        RepairSupportActivityGR, GeneralParameter, RepairProcessGR, AppointmentGrService, ngDialog, $timeout, bsNotify, RepairSupportActivity, bsAlert) {
        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
            $scope.mData.Status = 0;
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        console.log('$ $scope.user', $scope.user)
        $scope.mData = {}; //Model
        $scope.xRole = {
            selected: []
        };
        $scope.showme = false;
        $scope.showme2 = false;
        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.ismeridian = false;
        $scope.hstep = 1;
        $scope.mstep = 1;
        $scope.ShowWo = false;

        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };

        $scope.FIR = [{
                Id: 1,
                FIR: "Ya"
            },
            {
                Id: 0,
                FIR: "Tidak"
            }
        ];

        $scope.Status = [{
                StatusId: 0,
                Statustext: "Unsolved"
            },
            {
                StatusId: 1,
                Statustext: "Solved"
            }
        ];

        $scope.ClickWO = function(value, JobID) {
            if (value == 1) {
                //console.log("jobid", JobID);
                $scope.ShowWo = true;
                $scope.showme = true;
                RepairProcessGR.getDataDetail(JobID)
                    .then(

                        function(res) {
                            $scope.DataWo = res.data.Result;
                            console.log("data wo", $scope.DataWo);

                            for (i = 0; i < $scope.DataWo.length; i++) {
                                $scope.DataWOTask = $scope.DataWo[i].JobTask;
                                _.map($scope.DataWOTask, function(valTask) {
                                    _.map(valTask.JobParts, function(valParts) {

                                        AppointmentGrService.getAvailableParts(valParts.PartsId, 0, valParts.Qty)
                                            .then(function(resxx) {
                                                var tmpPart = resxx.data.Result[0];
                                                valParts.isAvailable = tmpPart.isAvailable;
                                                if (tmpPart.isAvailable == 0) {
                                                    valParts.Availbility = "Tidak Tersedia";
                                                } else {
                                                    valParts.Availbility = "Tersedia";
                                                }
                                                // $scope.tempFilter2 = tmpPart.PartsId;

                                                // function checkFilterPart(data) {
                                                //     return data.PartsId == $scope.tempFilter2;
                                                // }
                                                // var result = $scope.DataTempParts.filter(checkFilterPart)[0];
                                                // valParts.Availbility = tmpPart.Availbility;
                                                // $scope.DataParts.push(valParts);
                                            });

                                    })
                                });

                                for (a = 0; a < $scope.DataWOTask.length; a++) {
                                    $scope.DataMaterial = $scope.DataWOTask[i].JobParts;
                                    //console.log("data wo parts", $scope.DataMaterial);
                                }
                                //console.log("Data wo task", $scope.DataWOTask);
                            }
                            //console.log("data Wo Detail", $scope.DataWo);
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
            } else {
                $scope.showme = false;
                $scope.ShowWo = false;
            }
        }

        $scope.Kembali = function() {
            $scope.showme = false;
            $scope.ShowWo = false;
        }

        $scope.close = function(Text) {
            $scope.showme = false;
            $scope.showme2 = false;
            $scope.mData = {};
        }

        var gridData = [];
        $scope.getData = function() {
            //console.log("status", status);
            var startDate = $scope.filterData.StartDate;
            var stopDate = $scope.filterData.StopDate;
            var status = $scope.filterData.Status;
            var NoWo = $scope.filterData.KodeWO;


            var fNoWo = "-";
            if (NoWo == undefined || NoWo == "" || NoWo == null) {
                fNoWo = "-"
            } else {
                fNoWo = NoWo
            };

            var a = new Date(startDate);
            var yearFirst = a.getFullYear();
            var monthFirst = a.getMonth() + 1;
            var dayFirst = a.getDate();
            var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
            //console.log("first date", firstDate)

            var b = new Date(stopDate);
            var yearLast = b.getFullYear();
            var monthLast = b.getMonth() + 1;
            var dayLast = b.getDate();
            var lastDate = yearLast + '-' + monthLast + '-' + dayLast;
            //console.log("stop date", lastDate);

            if (status == undefined || firstDate == 'NaN-NaN-NaN' || lastDate == 'NaN-NaN-NaN') {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Filter",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            } else {
                fNoWo = fNoWo.replace(/\//g, ' ');
                var XfNoWo = encodeURIComponent(fNoWo);
                RepairSupportActivity.getData(firstDate, lastDate, status, XfNoWo)
                    .then(

                        function(res) {
                            $scope.xData = res.data;
                            console.log("data inti", $scope.xData);

                            $scope.xData.forEach(function(element) {
                                console.log('element', element);
                                RepairProcessGR.getDataDetailRSA(element.JobTaskBp.JobId)
                                    .then(

                                        function(res) {
                                            $scope.DataWo = res.data.Result;
                                            console.log("data wo", $scope.DataWo);
                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                        }
                                    );
                            });
                            if ($scope.xData.length == 0){
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Data Tidak Ditemukan",
                                    timeout : 3000
                                });
                            }
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );


            }
        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }
        $scope.selectRole = function(rows) {
            //console.log("onSelectRows=>", rows);
            $timeout(function() {
                $scope.$broadcast('show-errors-check-validity');
            });
        }
        $scope.onSelectRows = function(rows) {
                //console.log("onSelectRows=>", rows);
            }
            //----------------------------------
            // Grid Setup
            //----------------------------------

        function UpdateTfirstLocal(Model) {
            console.log("ari sudah lelah", Model);

            bsAlert.alert({
                    title: "Data berhasil disimpan",
                    text: "",
                    type: "success"
                },
                function() {
                    RepairSupportActivity.putRSA(Model).then(

                        function(res) {
                            //console.log("hasil simpan",res);
                            $scope.getData();
                            $scope.mData = {};
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
                },
                function() {}
            )

            $scope.showme = false;
            $scope.showme2 = false;

        }

        $scope.simpanRSA = function(Model) {
            console.log("mData", Model);
            Model["rsaDocNo"] = "";
            Model["dccDocNo"] = "";
            Model["ltDocNo"] = "";
            console.log("model dikirim : ", Model);
            console.log("mdata", $scope.mData);

            RepairSupportActivityGR.postRSATfirst(Model).then(function(res) {
                    console.log("test liad rsa 12", res.data);
                    if (res.data.ResponseCode == 100){
                        var pesan = res.data.ResponseMessage.split('#')
                        bsNotify.show({
                          size: 'big',
                          type: 'danger',
                          title: pesan[1],
                        });
                    } else {
                        //console.log("test liad rsa", JSON.parse(res.data.msg));
                        var resParsed = JSON.parse(res.data.msg);
                        if (Array.isArray(resParsed)) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: resParsed[0].message,
                                // content: error.join('<br>'),
                                // number: error.length
                            });
                        } else {
                            if (resParsed.success) {
                                Model["rsaDocNo"] = res.data.noDoc;

                                if (Model.BentukLaporan == "1") {
                                    RepairSupportActivityGR.postDCCTfirst(Model).then(function(res) {
                                            console.log("test liad dcc", res.data);

                                            var resParsed = JSON.parse(res.data.msg);
                                            if (Array.isArray(resParsed)) {
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: resParsed[0].message,
                                                    // content: error.join('<br>'),
                                                    // number: error.length
                                                });
                                            } else {

                                                if (resParsed.success) {
                                                    Model["dccDocNo"] = res.data.noDoc;

                                                    UpdateTfirstLocal(Model);
                                                }

                                            }
                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                        }
                                    );
                                } else if (Model.BentukLaporan == "0") {
                                    RepairSupportActivityGR.postLTETfirst(Model).then(function(res) {
                                            console.log("test liad lte", res.data);

                                            var resParsed = JSON.parse(res.data.msg);
                                            if (Array.isArray(resParsed)) {
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: resParsed[0].message,
                                                    // content: error.join('<br>'),
                                                    // number: error.length
                                                });
                                            } else {

                                                if (resParsed.success) {
                                                    Model["ltDocNo"] = res.data.noDoc;

                                                    UpdateTfirstLocal(Model);
                                                }

                                            }
                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                        }
                                    );
                                } else {

                                    UpdateTfirstLocal(Model);
                                }

                            }

                        }
                    }
                    

                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.choseData = [];
        // ,IdJobTaskProgress
        $scope.checkRequired = function(param) {
            if (param == 0) {
                $scope.mData.CategorySubProblem = null
                $('div[name="slctCategorySubProblem"]').removeClass("ng-dirty");
            }
        }

        $scope.getDataProblemList = function(Id, data, parts, ResponseDesc, TroubleDesc, objX, JobId) {

            // $scope.DataTempParts = parts;
            $scope.DataParts = [];
            // for(var x = 0 ; x< $scope.DataTempParts.length ; x++ ){
            // var item = $scope.DataTempParts;
            // AppointmentGrService.getAvailableParts(item[x].PartsId,0,item[x].Qty)
            // .then(function(res) {
            //     var tmpPart = res.data.Result[0];
            //     if(tmpPart.isAvailable == 0){
            //     tmpPart.Availbility = "Tidak Tersedia";
            //     }else{
            //     tmpPart.Availbility = "Tersedia";
            //     }
            //     $scope.tempFilter2 = tmpPart.PartsId;
            //     function checkFilterPart(data)
            //     {
            //     return data.PartsId == $scope.tempFilter2;
            //     }
            //     var result = $scope.DataTempParts.filter(checkFilterPart)[0]; 
            //     result.isAvailable = tmpPart.isAvailable;
            //     result.Availbility = tmpPart.Availbility;
            //     $scope.DataParts.push(result);

            // });
            // }
            console.log("data job parts", parts);
            console.log("data job task", data);
            console.log("yang dipassing", objX);
            console.log("des res", ResponseDesc);
            console.log("des mas", TroubleDesc)
            $scope.showme = true;
            $scope.showme2 = true;
            $scope.DataProblemList = Id;
            console.log("DataProblemList", $scope.DataProblemList);
            if ($scope.choseData.length > 0) {

            } else {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].TaskName.includes("Replace")){
                        var arrTaskName = data[i].TaskName.split('-');
                        console.log('arrTaskName', arrTaskName);
                        data[i].NamaTask = arrTaskName[0];
                        data[i].WorkType = arrTaskName[1];
                    } else {
                        var arrTaskName = data[i].TaskName.split('-');
                        console.log('arrTaskName', arrTaskName);
                        data[i].NamaTask = arrTaskName[0];
                        data[i].WorkType = arrTaskName[1] + "-" + arrTaskName[2];
                    }
                    $scope.choseData.push(data[i]);
                }
            };
            $scope.chosedatafinal = objX;
            $scope.statusFilter = objX.IsSolvedProblem;
            console.log("status filter", $scope.statusFilter);
            console.log("chosedata", $scope.choseData);
            $scope.getJob();
            // $scope.getParts(parts);
            $scope.getMarket();
            $scope.getTransmision();
            $scope.getTechnicalRank();
            $scope.getFailPart();
            $scope.getEngine();
            $scope.getKategoriKomponen();
            $scope.getKomponen();
            $scope.getKategoriProblem();
            $scope.getSubKategoriProblem();
            $scope.getT1();
            $scope.getT2();
            $scope.getDataCompanyGroupDealer();

            // $scope.mData.NoLTIfRepeatRepair = parseInt(objX.JobTask.JobTaskProgress[0].NoLTIfRepeatRepair);
            // $scope.mData.NoLTIfInvolvedPBT = parseInt(objX.JobTask.JobTaskProgress[0].NoLTIfInvolvedPBT);
            objX.TfirstKode_T1__c = objX.TfirstKode_T1__c == null ? objX.JobTask.TFirst1 : objX.TfirstKode_T1__c;
            objX.TfirstKode_T2__c = objX.TfirstKode_T2__c == null ? objX.JobTask.TFirst2 : objX.TfirstKode_T2__c;

            $scope.mData.CatgComponent = objX.CatgComponent;
            $scope.mData.SubCatgComponent = objX.Component;
            $scope.mData.T1 = parseInt(objX.TfirstKode_T1__c);
            $scope.mData.ConditionT1 = objX.TfirstPemeriksaan_dan_Langkah_Investigasi__c;
            $scope.mData.T2 = parseInt(objX.TfirstKode_T2__c);
            $scope.mData.CausedT2 = objX.TfirstPenyebab_Masalah__c;
            $scope.mData.MarketInpactRank = objX.MarketInpactRank;
            $scope.mData.TechnicalRank = objX.TechnicalRank;
            $scope.mData.TransmissionType = objX.TransmissionType;
            $scope.mData.EngineType = parseInt(objX.TfirstTipe_Mesin__c);
            $scope.mData.StatusFailedPart = objX.StatusFailedPart;
            $scope.mData.PartDelivery = objX.PartDelivery;
            $scope.mData.PartDeliveryAvailable = objX.TfirstPengadaan_Part_Tanggal_Pesan_Tersedia__c;
            $scope.mData.DateofFirstComplain = objX.TfirstTanggal_Komplain_Pertama__c;
            $scope.mData.NoLTIfRepeatRepair = objX.NoLTIfRepeatRepair;
            $scope.mData.NoLTIfInvolvedPBT = objX.NoLTIfInvolvedPBT;
            $scope.mData.ProblemCanBeDuplicate = objX.ProblemCanBeDuplicate;
            $scope.mData.NumberOfCases = parseInt(objX.NumberOfCases);
            $scope.mData.InquiryTitle = objX.InquiryTitle;
            $scope.mData.MethodToAct = objX.MethodToAct;
            $scope.mData.ResultAction = objX.ResultAction;
            $scope.mData.RequestToDCC = objX.RequestToDCC;
            $scope.mData.CarUser = objX.TfirstPenggunaan_Kendaraan__c;
            $scope.mData.IsSolvedProblem = objX.IsSolvedProblem;
            $scope.mData.IsEscalationProblem = objX.IsEscalationProblem;
            $scope.mData.BentukLaporan = objX.TfirstDCCorLT;
            $scope.mData.ConsultationDCCCatg = objX.ConsultationDCCCatg;
            $scope.mData.CategorySubProblem = parseInt(objX.TfirstSub_Kategori_Problem__c);
            $scope.mData.IsFIR = objX.IsFIR;
            $scope.mData.TroubleDesc = TroubleDesc;
            $scope.mData.DeskripsiRespon = ResponseDesc;
            // $scope.mData.DeskripsiRespon = objX.RespondDesciption == null ? objX.ResponseDesc : objX.RespondDesciption;

            AppointmentGrService.getDataVehiclePSFU(objX.JobTaskBp.Job.VehicleId).then(function(res) {
                    //console.log("crm data", res);
                    var hasil = res.data.Result[0];
                    console.log('cek', hasil.VIN);
                    $scope.mData["EngineNo"] = hasil.EngineNo;
                    $scope.mData["VIN"] = hasil.VIN;
                    $scope.mData["DECDate"] = hasil.DECDate;
                    $scope.mData["PLOD"] = hasil.PLOD;
                    $scope.mData["AssemblyYear"] = hasil.AssemblyYear;
                    $scope.mData["VehicleModelCode"] = hasil.MVehicleTypeColor.MVehicleType.MVehicleModel.VehicleModelCode;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );


            RepairProcessGR.getDataDetail(JobId).then(function(res) {
                    //console.log("crm data", res);
                    var tempJobData = res.data.Result;
                    $scope.groupdisplay = res.data.Result[0];
                    console.log('abccccc', res.data.Result , tempJobData, $scope.groupdisplay);
                    _.map(tempJobData, function(valJob) {
                        _.map(valJob.JobTask, function(valTask) {
                            _.map(valTask.JobParts, function(valParts) {
                                // $scope.DataTempParts = valParts;
                                AppointmentGrService.getAvailableParts(valParts.PartsId, 0, valParts.Qty)
                                    .then(function(resxx) {
                                        var tmpPart = resxx.data.Result[0];
                                        valParts.isAvailable = tmpPart.isAvailable;
                                        if (tmpPart.isAvailable == 0) {
                                            valParts.Ketersediaan = "Tidak Tersedia";
                                        } else {
                                            valParts.Ketersediaan = "Tersedia";
                                        }
                                        // $scope.tempFilter2 = tmpPart.PartsId;

                                        // function checkFilterPart(data) {
                                        //     return data.PartsId == $scope.tempFilter2;
                                        // }
                                        // var result = $scope.DataTempParts.filter(checkFilterPart)[0];
                                        // valParts.Ketersediaan = tmpPart.Ketersediaan;
                                        $scope.DataParts.push(valParts);
                                    });

                                // $scope.DataParts.push(valParts);
                            })
                        })
                    });
                    // for (i = 0; i < $scope.DataWo.length; i++) {
                    //   $scope.DataWOTask = $scope.DataWo[i].JobTask;

                    for (var i = 0; i < res.data.Result.length; i++) {
                        console.log('res.data.Result1', res.data.Result)
                        for (var a = 0; a < res.data.Result[i].JobTask.length; a++) {
                            // for(var b =0; b < res.data.Result[i].JobTask[a].JobParts.length; b++){
                            // $scope.DataParts = res.data.Result[i].JobTask[a].JobParts;
                            // $scope.dataDetail2 = res.data.Result[i];
                            // console.log("data parts ee", $scope.dataDetail
                            
                            $scope.groupBPIDnew = [];
                            var posisiAkhir = $scope.dataDetail2.JobTaskBp.length - 1;
                            if ($scope.dataDetail2.JobTaskBp[posisiAkhir].GroupBP1.GroupName != null) {
                                $scope.groupBPIDnew.push($scope.dataDetail2.JobTaskBp[posisiAkhir].GroupBP1.GroupName);
                            }
                            if ($scope.dataDetail2.JobTaskBp[posisiAkhir].GroupBP2.GroupName != null) {
                                $scope.groupBPIDnew.push($scope.dataDetail2.JobTaskBp[posisiAkhir].GroupBP2.GroupName);
                            }
                            if ($scope.dataDetail2.JobTaskBp[posisiAkhir].GroupBP3.GroupName != null) {
                                $scope.groupBPIDnew.push($scope.dataDetail2.JobTaskBp[posisiAkhir].GroupBP3.GroupName);
                            }
                            if ($scope.dataDetail2.JobTaskBp[posisiAkhir].GroupBP4.GroupName != null) {
                                $scope.groupBPIDnew.push($scope.dataDetail2.JobTaskBp[posisiAkhir].GroupBP4.GroupName);
                            }
                            if ($scope.dataDetail2.JobTaskBp[posisiAkhir].GroupBP5.GroupName != null) {
                                $scope.groupBPIDnew.push($scope.dataDetail2.JobTaskBp[posisiAkhir].GroupBP5.GroupName);
                            }
                            if ($scope.dataDetail2.JobTaskBp[posisiAkhir].GroupBP6.GroupName != null) {
                                $scope.groupBPIDnew.push($scope.dataDetail2.JobTaskBp[posisiAkhir].GroupBP6.GroupName);
                            }
                            if ($scope.dataDetail2.JobTaskBp[posisiAkhir].GroupBP7.GroupName != null) {
                                $scope.groupBPIDnew.push($scope.dataDetail2.JobTaskBp[posisiAkhir].GroupBP7.GroupName);
                            }

                            var unique = Array.from(new Set($scope.groupBPIDnew.map(function(item) { return item; })));

                            console.log('nih group', unique)
                            $scope.parsingGroup = '';

                            for (var i = 0; i < unique.length; i++) {
                                $scope.parsingGroup = $scope.parsingGroup + unique[i] + ', '
                            }
                            $scope.parsingGroup = $scope.parsingGroup.substring(0, $scope.parsingGroup.length - 2);
                            console.log('parsingan fix', $scope.parsingGroup)
                                // }
                        }
                    }

                    // for (var i = 0; i < res.data.Result.length; i++) {
                    //     for (var a = 0; a < res.data.Result[i].JobTask.length; a++) {
                    //         // for(var b =0; b < res.data.Result[i].JobTask[a].JobParts.length; b++){
                    //         $scope.DataParts = res.data.Result[i].JobTask[a].JobParts;
                    //         console.log("data parts", $scope.DataParts);
                    //         // }
                    //     }
                    // }

                },
                function(err) {
                    console.log("err=>", err);
                }
            );

            $scope.mData["IdJobTaskProgress"] = objX.Id;
            $scope.mData["KatashikiCode"] = objX.JobTaskBp.Job.KatashikiCode;
            $scope.mData["PoliceNumber"] = objX.JobTaskBp.Job.PoliceNumber;
            $scope.mData["Km"] = objX.JobTaskBp.Job.Km;
            $scope.mData["ProblemFoundedDate"] = objX.ProblemFoundedDate;
            $scope.mData["isRTJ"] = objX.JobTaskBp.JobTypeId == 58 ? "Ya" : "Tidak";
            $scope.mData["GateInPushTime"] = objX.JobTaskBp.Job.GateInPushTime;
            $scope.mData["PlanDateFinish"] = objX.JobTaskBp.Job.PlanDateFinish;
            $scope.mData["DateStart"] = objX.ProblemFoundedDate;


            if (Array.isArray(objX.JobTaskBp.Job)) {
                if (objX.JobTaskBp.Job.length > 0) {
                    var stringJobComplaint = "<ul>";
                    objX.JobTaskBp.Job.forEach(function(element) {
                        //console.log(element);
                        stringJobComplaint = stringJobComplaint + "<li>" + element + "</li>";
                    });
                    stringJobComplaint = stringJobComplaint + "</ul>";
                    $scope.mData["JobComplaint"] = stringJobComplaint;

                } else {
                    $scope.mData["JobComplaint"] = "";
                }
            } else {
                $scope.mData["JobComplaint"] = "";
            }

            if (Array.isArray(objX.JobTaskBp.JobParts)) {
                if (objX.JobTaskBp.JobParts.length > 0) {
                    var OFPCode = "";
                    for (var i = 0; i < objX.JobTaskBp.JobParts.length; i++) {
                        if (objX.JobTaskBp.JobParts[i].IsOfp) {
                            OFPCode = objX.JobTaskBp.JobParts[i].Part.PartsCode;
                            break;
                        }
                    };

                    $scope.mData["OFP"] = OFPCode;

                } else {
                    $scope.mData["OFP"] = "";
                }
            } else {
                $scope.mData["OFP"] = "";
            }

        };

        $scope.getDataCompanyGroupDealer = function() {
            GeneralParameter.getDataCompanyGroupDealer()
                .then(

                    function(res) {
                        $scope.mData["GroupDealerCode"] = res.data[0].GroupDealerCode;
                        //console.log("group dealer", res.data[0].GroupDealerCode);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        }

        $scope.getKategoriProblem = function() {
            RepairSupportActivityGR.getKategoriProblem()
                .then(

                    function(res) {
                        $scope.KategoriProblem = res.data.Result;
                        //console.log("KategoriKomponen", $scope.KategoriKomponen);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        }

        $scope.getSubKategoriProblem = function() {
            RepairSupportActivityGR.getSubKategoriProblem()
                .then(

                    function(res) {
                        //console.log("res ", res);
                        $scope.SubKategoriProblem = res.data.Result;
                        //console.log("KategoriKomponen", $scope.KategoriKomponen);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        }

        $scope.getKategoriKomponen = function() {
            RepairSupportActivityGR.getKategoriKomponen()
                .then(

                    function(res) {
                        $scope.KategoriKomponen = res.data.Result;
                        //console.log("KategoriKomponen", $scope.KategoriKomponen);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        }

        $scope.getKomponen = function() {
            RepairSupportActivityGR.getKomponen()
                .then(

                    function(res) {
                        $scope.Komponen = res.data.Result;
                        //console.log("Komponen", $scope.Komponen);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        }

        $scope.getT1 = function() {
            RepairSupportActivityGR.getT1()
                .then(

                    function(res) {
                        $scope.T1 = res.data.Result;
                        //console.log("T1", $scope.T1);
                        for (var i = 0; i < res.data.Result.length; i++) {
                            $scope.T1[i].xName = $scope.T1[i].T1Code + ' - ' + $scope.T1[i].Name;
                        }
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        }

        $scope.getT2 = function() {
            RepairSupportActivityGR.getT2()
                .then(

                    function(res) {
                        $scope.T2 = res.data.Result;
                        //console.log("T2", $scope.T2);
                        for (var i = 0; i < res.data.Result.length; i++) {
                            $scope.T2[i].xName = $scope.T2[i].T2Code + ' - ' + $scope.T2[i].Name;
                        }
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        }

        $scope.getMarket = function() {

            // document.write(today);
            // $scope.PauseResaon = [];
            RepairSupportActivityGR.getMarketImpact()
                .then(

                    function(res) {
                        $scope.MarketImpact = res.data.Result;
                        console.log("MarketImpact", $scope.MarketImpact);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

            // var newDataWoList = angular.copy($scope.DataWoList);

        }

        $scope.getTechnicalRank = function() {

            // document.write(today);
            // $scope.PauseResaon = [];
            RepairSupportActivityGR.getTechnicalRank()
                .then(

                    function(res) {
                        $scope.TechnicalRank = res.data.Result;
                        console.log("TechnicalRank", $scope.TechnicalRank);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

            // var newDataWoList = angular.copy($scope.DataWoList);

        }

        $scope.getTransmision = function() {

            // document.write(today);
            // $scope.PauseResaon = [];
            RepairSupportActivityGR.getTransmisionType()
                .then(

                    function(res) {
                        $scope.TransmissionType = res.data.Result;
                        //console.log("TransmissionType", $scope.TransmissionType);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

            // var newDataWoList = angular.copy($scope.DataWoList);

        }

        $scope.getEngine = function() {

            // document.write(today);
            // $scope.PauseResaon = [];
            RepairSupportActivityGR.getEngineType()
                .then(

                    function(res) {
                        $scope.EngineType = res.data.Result;
                        console.log("EngineType", $scope.EngineType);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

            // var newDataWoList = angular.copy($scope.DataWoList);

        }

        $scope.getFailPart = function() {

            // document.write(today);
            // $scope.PauseResaon = [];
            RepairSupportActivityGR.getFailPart()
                .then(

                    function(res) {
                        $scope.StatusFailed = res.data.Result;
                        //console.log("Fail", $scope.StatusFailed);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

            // var newDataWoList = angular.copy($scope.DataWoList);

        }

        $scope.getJob = function() {
            $scope.gridJob.data = $scope.choseData;
        }

        $scope.gridJob = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            // data: $scope.choseData,
            showGridFooter: true,
            columnDefs: [{
                    field: 'JobTaskId',
                    name: 'JobTaskId',
                    visible: false
                },
                {
                    field: 'NamaTask',
                    name: 'Nama Pekerjaan'
                },
                {
                    field: 'WorkType',
                    name: 'WorkType'
                }

            ]
        };

        // $scope.getParts = function(parts) {
        //   //console.log("kakak", parts);
        //   if (typeof parts != undefined) {
        //     $scope.gridParts.data = parts;
        //   }
        // }

        $scope.gridWODetail = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            data: 'DataMaterial',
            showGridFooter: true,
            columnDefs: [{
                    field: 'ModelCode',
                    name: 'Nomor Material'
                },
                {
                    field: 'PartsName',
                    name: 'Nama Material'
                },
                {
                    field: 'Status',
                    name: 'Status'
                },
                {
                    field: 'ETA',
                    name: 'ETA'
                },
                {
                    field: 'Qty',
                    name: 'Permintaan QTY'
                },
                {
                    field: 'QtyGI',
                    name: 'Isu QTY'
                },
                {
                    field: 'Satuan.Name',
                    name: 'Satuan'
                }
            ]
        };

        $scope.gridParts = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            data: 'DataParts',
            showGridFooter: true,
            columnDefs: [
                // { field:'PartsCode', name:'Kode Parts'},
                {
                    field: 'PartsName',
                    name: 'Nama Parts'
                },
                {
                    field: 'Qty',
                    name: 'QTY'
                },
                {
                    field: 'Ketersediaan',
                    name: 'Ketersediaan'
                }
            ]
        };

    });