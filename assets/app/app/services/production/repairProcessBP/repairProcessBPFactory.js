angular.module('app')
    .factory('RepairProcessBP', function($http, CurrentUser, $q) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(NoPolice, NoWO, Employee) {
                // console.log("employee in fac", Employee);
                // var res=$http.get('/api/as/jobs/WoList/FirstDate/'+dateNow+'/LastDate/'+dateNow+'');
                // var res=$http.get('/api/as/jobs/WoList/FirstDate/'+dateNow+'/LastDate/'+dateNow+'/0');
                // var res=$http.get('/api/as/jobs/EstimationList/1/NoPolisi/'+NoPolice+'/idTeknisi/-/WoNo/'+NoWO);
                // var res = $http.get('/api/as/jobs/EstimationList/0/NoPolisi/' + NoPolice + '/idTeknisi/-/WoNo/' + NoWO + '/Date/-');
                var WoNo = NoWO.replace(/\//g, '%20');

                var res = $http.get('/api/as/jobs/EstimationList/0/NoPolisi/' + NoPolice + '/idTeknisi/-/WoNo/' + WoNo + '/Date/-');


                // console.log('resnya=>',res);
                return res;
            },

            getDataDetail: function(jobId) {
                // var res=$http.get('/api/as/jobs/WoList/FirstDate/'+dateNow+'/LastDate/'+dateNow+'');
                var res = $http.get('/api/as/JobTaskBPs/Job/' + jobId + '');
                // var res=$http.get('/api/as/Jobs/'+jobId+'');
                // console.log('resnya=>',res);
                return res;
            },

            putClock: function(Status, jobId, chip, mData, JobTaskIdQC) {

                console.log("model", mData);
                console.log("JobTaskIdQC", JobTaskIdQC);
                var chipTaskBpQcDto = {
                    ChipIsPassQc: mData.ChipIsPassQc,
                    ChipQcTechnicianId: mData.ChipQcTechnicianId,
                    ChipNotPassQcReason: mData.ChipNotPassQcReason,
                    ChipIsOtherReason: mData.ChipIsOtherReason,
                    ChipOtherReason: mData.ChipOtherReason,
                    ChipAdditionNote: mData.ChipAdditionNote,
                    RedoToChipKe: mData.RedoToChipKe
                };
                var TechnicalProblemDto = {
                    PauseReasonId: mData.PauseReasonId,
                    PauseReasonDesc: mData.PauseReasonDesc
                };
                var JobTask = JobTaskIdQC;
                var route = '/api/as/JobTaskBP/job/' + jobId + '/Clock/' + Status + '/chip/' + chip + '';
                return $http.put(route, [{
                    chipTaskBpQcDto: chipTaskBpQcDto,
                    TechnicalProblemDto: TechnicalProblemDto,
                    JobTask: JobTask
                }]);
            },

            // putLolosQC: function(Status,jobId,chip,mData,JobTaskIdQC){

            //   console.log("model", mData);
            //   console.log("Status", Status);
            //   console.log("jobId", jobId);
            //   console.log("chip", chip);
            //   console.log("JobTaskIdQC", JobTaskIdQC);
            //   if(chip != 6){
            //     console.log("Status tidak sama dgn 6");
            //     var chipTaskBpQcDto = {
            //                 ChipIsPassQc:mData.ChipIsPassQc,
            //                 ChipQcTechnicianId:mData.ChipQcTechnicianId,
            //                 ChipNotPassQcReason:mData.ChipNotPassQcReason,
            //                 ChipIsOtherReason:mData.ChipIsOtherReason,
            //                 ChipOtherReason :mData.ChipOtherReason,
            //                 ChipAdditionNote:mData.ChipAdditionNote,
            //                 RedoToChipKe:chip+1
            //               };
            //     var TechnicalProblemDto = {
            //                 PauseReasonId: mData.PauseReasonId,
            //                 PauseReasonDesc: mData.PauseReasonDesc
            //                       };
            //     var JobTask = JobTaskIdQC;
            //      var route = '/api/as/JobTaskBP/job/'+jobId+'/Clock/'+Status+'/chip/'+chip+'';
            //     return $http.put(route,[{chipTaskBpQcDto,TechnicalProblemDto,JobTask}]);
            // },

            putClockProblem: function(Status, jobId, chip, mData) {

                console.log("model", mData);
                var chipTaskBpQcDto = {};
                var TechnicalProblemDto = {
                    PauseReasonId: mData.PauseReasonId,
                    PauseReasonDesc: mData.PauseReasonDesc,
                    IsNeedRSA: mData.IsNeedRSA,
                    IsNeedAdditionalTime: mData.IsNeedAdditionalTime,
                    IsOwnDo: mData.IsOwnDo,
                    Duration: mData.Duration,
                    TroubleDesc: mData.TroubleDesc,
                    ResponseDesc: mData.ResponseDesc,
                    TfirstKode_T1__c: mData.T1,
                    TfirstKode_T2__c: mData.T2
                };
                var route = '/api/as/JobTaskBP/job/' + jobId + '/Clock/pause/chip/' + chip + '';
                return $http.put(route, [{
                    chipTaskBpQcDto: chipTaskBpQcDto,
                    TechnicalProblemDto: TechnicalProblemDto
                }]);
            },


            getPauseReason: function() {
                var catId = 1009;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },

            putFinalInspection: function(Data, Model, JobTaskIdFI, foremanid) {
                // console.log("model nih", model);
                var jobInspection = {
                    Id: Data.Id,
                    JobId: Data.JobId,
                    Status: Model.Status,
                    Remarks: Model.Remarks,
                };
                var routePutFI = '/api/as/JobInspections';
                var jobTask = JobTaskIdFI;
                console.log("Job INspection", jobInspection);
                console.log("JobTask", JobTaskIdFI);
                return $http.put(routePutFI, [{
                    jobInspection: jobInspection,
                    jobTask: jobTask,
                    OldPartKeepPlace: Model.OldPartKeepPlace,
                    isFixItRight: Model.isFixItRight,
                    RedoToChipKe: Model.RedoToChipKe,
                    ForemanId : foremanid
                }]);
            },

            putFinalInspectionLolos: function(Data, Model, JobTaskIdFI, datadetailmodel, foremanid) {
                console.log("model nih", Model);
                var jobInspection = {
                    Id: Data.Id,
                    JobId: Data.JobId,
                    Status: Model.Status,
                    Remarks: Model.Remarks
                };
                var routePutFI = '/api/as/JobInspections';
                var jobTask = JobTaskIdFI;
                console.log("Job INspection", jobInspection);
                console.log("JobTask", JobTaskIdFI);
                return $http.put(routePutFI, [{
                    jobInspection: jobInspection,
                    jobTask: jobTask,
                    OldPartKeepPlace: Model.OldPartKeepPlace,
                    isFixItRight: Model.isFixItRight,
                    RedoToChipKe: Model.RedoToChipKe,
                    JobTaskBpId: datadetailmodel[0].JobTaskBpId,
                    ForemanId : foremanid
                }]);
            },


            getListSA: function() {
                // var catId = 2019;
                var res = $http.get('/api/as/EmployeeRoles/5');
                console.log('Data SA=>', res);
                return res;
            },

            getDataQC: function(outletId) {
                var res = $http.get('/api/as/EmployeeRoles/' + outletId);
                console.log('resnya employee=>', res);
                return res;
            },

            getDataTeknisi: function() {
                var res = $http.get('/api/as/GroupBP/GetGroupBp');
                console.log('resnya employee=>', res);
                return res;
            },

            getDataTeknisiById: function(Jobid) {
                var res = $http.get('/api/as/GroupBP/GetForemanById/' + Jobid);
                console.log('resnya getDataTeknisiById', res);
                return res;
            },


            getDataDefect: function(key) {

                var res = $http.get('/api/as/DefectAll');
                console.log('resnya employee=>', res);
                return res;
            },

            UpdateFIforBP: function(JobId) {
                return $http.put('/api/as/UpdateFIforBP/' + JobId);
                // var res=$http.post('/api/as/JobInspections/job/'+JobId+'');
                // return res;
            },

            getDataDefectByJobId: function(JobId, CurrentChip) {

                // var res=$http.get('/api/as/JobDefects/JobId/'+JobId+'');
                var res = $http.get('/api/as/JobDefects/JobId/' + JobId + '/' + CurrentChip);
                // console.log('resnya employee=>',res);
                return res;
            },
            // postDefect: function(Data, IdDefect, AChip, JObTaskBP, JobTaskId) {
            //     console.log("IdDefect", IdDefect);
            //     console.log("JObTaskBP", JObTaskBP);
            //     console.log("jobTaskId", JobTaskId);
            //     var FinalJobTask = [];
            //     for (var i = 0; i < JobTaskId.length; i++) {
            //         return $http.post('/api/as/JobDefects', [{
            //             DefectId: IdDefect,
            //             Qty: Data.JumlahDefect,
            //             JobTaskBpId: JObTaskBP[0].JobTaskBpId,
            //             CurrentChip: AChip,
            //             RedoToChip: Data.RedoToChipKe,
            //             JobId: JObTaskBP[0].JobId,
            //             JobTaskId: JobTaskId[i]
            //         }]);
            //     }
            // },
            postDefect: function(dataGrid) {
                console.log("dataGrid", dataGrid);
                
                var data = [];
                for (var i=0; i<dataGrid.length; i++){
                    var objData = {};
                    objData.JobDefectId = dataGrid[i].JobDefectId,
                    objData.DefectId = dataGrid[i].DefectId,
                    objData.Qty = dataGrid[i].Qty,
                    objData.JobTaskBpId = dataGrid[i].JobTaskBpId,
                    objData.CurrentChip = dataGrid[i].CurrentChip,
                    objData.RedoToChip = dataGrid[i].RedoToChip,
                    objData.JobId = dataGrid[i].JobId,
                    objData.JobTaskId = dataGrid[i].JobTaskId
                    objData.Process = dataGrid[i].Proses

                    data.push(objData);
                }
                return $http.post('/api/as/JobDefects', data);
            },
            putDefect: function(Data) {
                return $http.put('/api/as/JobDefects/' + Data.JobDefectId + '/qty/' + Data.JumlahDefect + '');
            },

            deleteDefect: function(JobIdDefect, JobId) {
                return $http.delete('/api/as/JobDefects/' + JobIdDefect + '/job/' + JobId);
            },

            getWACBp: function(JobId) {

                var res = $http.get('/api/as/JobWACPBs/getByJob/' + JobId);
                // console.log('resnya employee=>',res);
                return res;
            },
            sendNotif: function(data, recepient, param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotification', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param: param,
                    Ref: data.Ref
                }]);
            },

            sendNotifPause: function(NoWO, recepient, param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotification', [{
                    Message: NoWO,
                    RecepientId: recepient,
                    Param: param
                }]);
            },

            sendNotifRole: function(data, recepient, param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param: param
                }]);
            },
            CreateOplFromClockOn: function(JobId, groupid) {

                var res = $http.post('/api/as/CreateOPLTeknisi/' + JobId + '/' + groupid);
                // console.log('resnya employee=>',res);
                return res;
            },
            GetStatusJobFI: function(JobId) {
                var res = $http.get('/api/as/Jobs/GetStatusJobFI/' + JobId);
                // console.log('resnya employee=>',res);
                return res;
            },
            getStatWO: function(JobId, status) {
                return $http.get('/api/as/Jobs/GetStatusJob/' + JobId + '/' + status);
            },
            CheckIsJobCancelWO: function (jobId) {
                console.log("data di ReqCancelWO", jobId);
                return $http.get('/api/as/CheckIsJobCancelWO/'+jobId);
            },
            putCheckStatusCLockOn: function(JobId , StallId, ChipType  ) {
                console.log("check clock on", JobId, StallId, ChipType);

                return $http.put('/api/as/CheckOtherClockOnBP', [{
                    JobId : JobId ,
                    StallId :StallId,
                    ChipType :ChipType
                }]);
            },
            putCheckCLockOnBentrok: function(JobId , StallId, ClockOn, ChipDuration, ChipType  ) {
                console.log("check clock on", JobId, StallId, ClockOn, ChipDuration, ChipType);

                return $http.put('/api/as/CheckClockOnBP', [{
                    JobId : JobId ,
                    StallId :StallId,
                    ClockOn :ClockOn,
                    ChipDuration :ChipDuration,
                    ChipType :ChipType
                }]);
            },

            GetJobTasks: function (jobId) {
                console.log("data di GetJobTasks", jobId);
                // api/as/GetJobTasks/{JobId}
                return $http.get('/api/as/GetJobTasks/'+jobId);
            },

            getListStall: function(param){
                var res = $http.get('/api/as/Boards/BP/Stall/'+param);
                return res
            },

        }
    });