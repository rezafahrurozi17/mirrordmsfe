angular.module('app')
    .controller('RepairProcessBPController', function($scope, $http, bsAlert, PrintRpt, CurrentUser, WO, AppointmentGrService, WoHistoryBP, RepairProcessBP, RepairProcessGR, RepairSupportActivityGR, $timeout, ngDialog, bsNotify, JpcbGrViewFactory, $filter, GeneralMaster, WOBP) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        console.log("user", $scope.user);
        $scope.mData = []; //Model
        $scope.xRole = { selected: [] };
        $scope.selectedRows = []; //Hold Grid selectedRows
        $scope.mFilter = null;
        $scope.modelCheckNameDefect = [];
        $scope.DamageVehicle = [];
        WOBP.getDamage().then(function(res) {
                $scope.DamageVehicle = res.data.Result;
                _.map($scope.DamageVehicle, function(val) {
                    val.Id = val.MasterId;
                })
                $scope.grid.columnDefs[2].editDropdownOptionsArray = $scope.DamageVehicle;
                console.log("$scope.dramagaaaa", $scope.DamageVehicle);
            })
            //----------------------------------
            // Get Data
            //----------------------------------
        $scope.showDetail = false;
        $scope.show = false;
        $scope.duration = false;
        $scope.material = false;
        // $scope.Fi = false;
        // $scope.ButtonLolosFi = false;
        // $scope.ButtonTidakLolosFi = false;
        $scope.ButtonLolosQC = false;
        $scope.ButtonTidakLolosQC = false;
        $scope.showClockON = false;

        $scope.showClockOff = false;

        $scope.showPause = false;

        $scope.StatusFixed = false;
        $scope.StatusNotFixed = false;
        $scope.showPetugasQc = false;
        $scope.btnFI = true;
        $scope.btnQC = true;
        $scope.showFI = false;

        $scope.ismeridian = false;
        $scope.hstep = 1;
        $scope.mstep = 1;
        var currentDate;
        currentDate = new Date();
        currentDate.setDate(currentDate.getDate() + 1);
        $scope.mindatesuggestion = new Date(currentDate);
        console.log("$scope.mindatesuggestion", $scope.mindatesuggestion);
        // ==========================
        $scope.DateOptions = { // ini huruf gede kecil nya ngefek.. yg atas ga dihapus takut kepake
            startingDay: 1,
            format: 'dd/MM/yyyy',
            //disableWeekend: 1
        };

        $scope.slider = {
            value: 0,
            options: {
                step: 3.125,
                hidePointerLabels: true,
                noSwitching: true,
                minRange: 1,
                floor: 0,
                ceil: 100,
                showSelectionBar: true,
                // translate: function(value) {
                //     return value + '%';
                // }
                translate: function(value, id, label) {
                    if (value == 0) {
                        return value = 'E'
                    } else if (value == 100) {
                        return value = 'F'
                    }
                }
            }
        };
        $scope.refreshSlider = function() {
            $timeout(function() {
                $scope.$broadcast('rzSliderForceRender');
            });
        };
        $scope.data = {};
        $scope.dataGridIdentity = [];
        $scope.CreateGrids = function() {
            $scope.data = {};
            $scope.dataGridIdentity = [];
            for (var i in $scope.headerWAC) {
                if ($scope.headerWAC[i].GroupName !== 'body') {
                    var studentData = "gridData" + $scope.headerWAC[i].GroupName;
                    $scope.dataGridIdentity.push(studentData);
                    $scope.data[studentData] = [];
                    // defining the grid control
                    $scope.data[studentData] = {
                        enableRowSelection: false,
                        enableFullRowSelection: false,
                        columnDefs: [{
                            name: 'Description',
                            field: 'name'
                        }, {
                            name: 'Action',
                            field: 'ItemStatus',
                            width: '35%',
                            cellTemplate: $scope.ItemStatusTemplate
                        }],
                        // onRegisterApi: function (gridApi) {
                        //     $scope.gridExtApi = gridApi;

                        //     $scope.gridExtApi.selection.on.rowSelectionChanged($scope, function (row) {
                        //         $scope.selectedRow = row.entity;
                        //     });
                        //     if ($scope.gridExtApi.selection.selectRow) {
                        //         $scope.gridExtApi.selection.selectRow($scope.gridExt.data[0]);
                        //     }
                        // }
                    };
                    $scope.data[studentData].data = [];
                }
            }
            console.log($scope.data);

        };

        $scope.selectedFilter = function(data){
            console.log('data >>', data);

            if (data.FilterId == 1) {
                $scope.mData.KodeWO = "";
            } else {
                $scope.mData.NomorPolisi = "";
            };
        };


        $scope.selectedFilterGroup = function(data){
            RepairProcessBP.getListStall(data.GroupId).then(function(res){
                console.log(res)
                $scope.dataStallbyGroup = res.data.Result
            })
        }

        // $scope.disabledOther = true;
        $scope.getDataDefault = function() {

            var Employee = "-";
            var NoPol = "-";
            var fNoWo = "-";

            RepairProcessBP.getData(NoPol, fNoWo, Employee)
                .then(

                    function(res) {
                        console.log("EmployeeFinal", Employee);
                        // $scope.DataModel = res.data;
                        // if(EmployeeFinal !== "-"){
                        //   var data = res.data;

                        //   var evens = _.remove(data, function(n) {
                        //     for(var i = 0; i < n.JobTask.length; i++){
                        //        for(var a = 0; a < n.JobTask[i].JobTaskTechnician.length; a++){
                        //           return n.JobTask[i].JobTaskTechnician[a].MProfileTechnicianId == EmployeeFinal;
                        //         }
                        //     }
                        //   });
                        //   $scope.DataModel = angular.copy(evens);
                        //   console.log("Data Setelah Remove", $scope.DataModel);
                        // }else{
                        $scope.DataModel = res.data;
                        //   console.log("masuk else");
                        // }

                        var newDataModel = angular.copy($scope.DataModel);
                        var cc = {};
                        console.log("newDataModel", newDataModel);

                        for (var j = 1; j < 8; j++) {
                            for (var k = 4; k < 13; k++) {
                                var x = j + '-' + k;
                                cc[j + '-' + k] = [];
                                for (var i = 0; i < newDataModel.length; i++) {
                                    // var test = cc[j+'-'+k].map(function(item){
                                    var test = newDataModel.map(function(item) {
                                        var hasil = item;
                                        // console.log("wew", hasil);
                                        if (hasil.Status == 4 || hasil.Status == 5 || hasil.Status == 7 || hasil.Status == 8 || hasil.Status == 9 || hasil.Status == 10 || hasil.Status == 11 || hasil.Status == 12) {
                                            hasil.color = "green"
                                        } else if (hasil.Status == 6) {

                                            var webgriz = item.JobTaskBp.map(function(final) {


                                                    if (final.JobTaskProgressBP.length == 0) {
                                                        if (hasil.Status == 4 || hasil.Status == 5 || hasil.Status == 7 || hasil.Status == 8 || hasil.Status == 9 || hasil.Status == 10 || hasil.Status == 11 || hasil.Status == 12) {
                                                            hasil.color = "green";
                                                        } else {
                                                            hasil.color = "yellow";
                                                        }
                                                    } else {
                                                        if (final.JobTaskProgressBP[final.JobTaskProgressBP.length - 1].PauseReasonId == 45) {
                                                            // if(final.JobTaskProgressBP[final.JobTaskProgressBP.length+1].PauseReasonId == 45){
                                                            if (final.JobTaskProgressBP.some(function(wow) {
                                                                    console.log("wow", wow);
                                                                    return wow.PauseReasonId == 45;
                                                                })) {
                                                                hasil.color = "red";
                                                            }
                                                            // else{
                                                            //   hasil.color = "yellow";
                                                            // }
                                                        } else {
                                                            hasil.color = "yellow";
                                                        }
                                                    }

                                                })
                                                // hasil.color = "yellow"
                                        } else {
                                            hasil.color = "red"
                                        }

                                        return hasil;
                                    });
                                    console.log("Test", test);
                                    if (newDataModel[i].ActiveChip == j && newDataModel[i].Status == k) {
                                        cc[j + '-' + k].push(newDataModel[i]);
                                        $scope.DataList = cc;

                                        _.map($scope.DataList, function(item) {
                                            var hasil = item;
                                            console.log("hasil ", hasil);

                                            console.log("hasil Plan Finish ", hasil.PlanFinish);

                                            var jmltask = 0;
                                            var adaJobTWC = 0;
                                            var adaJobPWC = 0;
                                            var adaMenungguParts = 0;
                                            // MaterialRequestStatusId: 1 => kalau MaterialRequestStatusId = 1 berarti ada yang di tunggu(menunggu status part info)
                                            if (hasil.JobTask != undefined || hasil.JobTask != null) {
                                                // jmltask = hasil.JobTask.length;

                                                for (var i = 0; i < hasil.JobTask.length; i++) {
                                                    if (hasil.JobTask[i].isDeleted == 0) {
                                                        jmltask++;
                                                    };

                                                    if (hasil.JobTask[i].JobType != null && hasil.JobTask[i].JobType != undefined) {
                                                        if (hasil.JobTask[i].JobType.Name == 'TWC' && hasil.JobTask[i].JobType.Name != null && hasil.JobTask[i].JobType.Name != undefined) {
                                                            adaJobTWC = 1;
                                                        }
                                                        if (hasil.JobTask[i].JobType.Name == 'PWC' && hasil.JobTask[i].JobType.Name != null && hasil.JobTask[i].JobType.Name != undefined) {
                                                            adaJobPWC = 1;
                                                        }
                                                    };

                                                    if (hasil.JobTask[i].JobParts != null && hasil.JobTask[i].JobParts != undefined) {
                                                        for (var j = 0; j < hasil.JobTask[i].JobParts.length; j++) {
                                                            if (hasil.JobTask[i].JobParts[j].MaterialRequestStatusId != null && hasil.JobTask[i].JobParts[j].MaterialRequestStatusId != undefined) {
                                                                if (hasil.JobTask[i].JobParts[j].MaterialRequestStatusId == 1) {
                                                                    adaMenungguParts = 1;
                                                                }
                                                            }
                                                        }
                                                    };
                                                }
                                            }

                                            // $scope.DataWoList.jmltask = jmltask;
                                            // $scope.DataWoList.adaJobTWC = adaJobTWC;
                                            // $scope.DataWoList.adaJobPWC = adaJobPWC;
                                            item.jmltask = jmltask;
                                            item.adaJobTWC = adaJobTWC;
                                            item.adaJobPWC = adaJobPWC;
                                            item.adaMenungguParts = adaMenungguParts;
                                            console.log("jmltask >", jmltask);
                                        });

                                        console.log("$scope.DataList", $scope.DataList);
                                    } else {
                                        // console.log("laper");
                                    }

                                }
                            }
                        }
                        // console.log("Data Di Dalam Table ==>",$scope.DataList);

                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

            // RepairProcessBP.getData(NoPol,fNoWo,Employee)
            //   .then(

            //     function(res) {
            //       $scope.DataModel = res.data;

            //       // var evens = _.remove($scope.DataModel, function(n) {
            //       //   for(var i = 0; i < n.JobTaskBp.length; i++){
            //       //     // console.log("job Task", n.JobTaskBp[i]);
            //       //     // if(n.JobTaskBp[i].JobTaskBpTechnician.length !== 0){

            //       //      // for(var a = 0; a < n.JobTaskBp[i].JobTaskBpTechnician.length; a++){
            //       //         // console.log("JOb Teknisi", n.JobTaskBp[i]);
            //       //         return n.JobTaskBp[i].BodyEstimationMinute == null && n.JobTaskBp[i].PutyEstimationMinute == null && n.JobTaskBp[i].SurfacerEstimationMinute == null && n.JobTaskBp[i].SprayingEstimationMinute == null && n.JobTaskBp[i].PolishingEstimationMinute == null && n.JobTaskBp[i].ReassemblyEstimationMinute == null && n.JobTaskBp[i].FIEstimationMinute == null;
            //       //       // }
            //       //     // }
            //       //   }
            //       // });


            //       console.log("$scope.DataModel",$scope.DataModel);
            //       var newDataModel = angular.copy($scope.DataModel);
            //       var cc={};
            //       console.log("data model", newDataModel);
            //       for (var j = 1;  j < 8; j++) {
            //           for (var k = 4 ; k < 13; k++) {

            //                   var x = j+'-'+k;
            //                   // console.log("===>",x);
            //                   cc[j+'-'+k] = [];
            //                   // $scope.DataList = [];
            //               for (var i = 0 ; i < newDataModel.length; i++) {
            //                 // var test = cc[j+'-'+k].map(function(item){
            //                 console.log("newDataModel", newDataModel);
            //                 var test = newDataModel.map(function(item){
            //                  var hasil = item;
            //                   console.log("wew", hasil);
            //                     if(hasil.Status == 4 || hasil.Status == 5 || hasil.Status == 7 || hasil.Status == 8 || hasil.Status == 9 || hasil.Status == 10 || hasil.Status == 11 || hasil.Status == 12){
            //                       hasil.color = "green";
            //                     }else if(hasil.Status == 6){

            //                       var webgriz = item.JobTaskBp.map(function(final){

            //                       // console.log("ini dia",final.JobTaskProgressBP[final.JobTaskProgressBP.length-1].PauseReasonId);
            //                         // ini yg lama sebelum dirubah jadi final.JobTaskProgressBP[0].PauseReasonId == 45
            //                         if(final.JobTaskProgressBP[final.JobTaskProgressBP.length-1].PauseReasonId == 45){
            //                         // if(final.JobTaskProgressBP[0].PauseReasonId == 45){
            //                             if(final.JobTaskProgressBP.some(function(wow){
            //                               console.log("wow",wow);
            //                               return wow.PauseReasonId == 45;
            //                             })){
            //                               hasil.color = "red";
            //                             }
            //                             // else{
            //                             //   hasil.color = "yellow";
            //                             // }
            //                         }else{
            //                           hasil.color = "yellow";
            //                         }

            //                       })
            //                       // hasil.color = "yellow"
            //                     }else{
            //                       hasil.color = "red"
            //                     }

            //                   return hasil;
            //                 });

            //                   if (newDataModel[i].ActiveChip == j && newDataModel[i].Status == k) {

            //                           cc[j+'-'+k].push(newDataModel[i]);
            //                               $scope.DataList = cc;

            //                   }

            //               }
            //           }
            //       }
            //       // console.log("Data Di Dalam Table ==>",$scope.DataList);

            //     },
            //     function(err) {
            //       console.log("err=>", err);
            //     }
            //   );

        }
        var NopoliceF;
        var NoWof;
        var groupId;
        $scope.chip6 = true;
        $scope.getData = function(NoPolice, NoWO, Employee) {
            console.log("NoPolice", NoPolice);
            console.log("NoWO", NoWO);
            console.log("employee", Employee);

            var NoPol = "-";
            var fNoWo = "-";
            if (NoPolice == undefined || NoPolice == "" || NoPolice == null) {
                NoPol = "-"
            } else {
                NoPol = NoPolice;
                NopoliceF = NoPolice;
            };

            if (NoWO == undefined || NoWO == "" || NoWO == null) {
                fNoWo = "-"
            } else {
                fNoWo = NoWO;
                NoWof = NoWO;
            };

            if (Employee == undefined || Employee == "" || Employee == null) {
                EmployeeFinal = "-"
            } else {
                EmployeeFinal = Employee;
                groupId = Employee;
            };


            if (EmployeeFinal == "-") {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Pilih Group"
                });
            } else {

                RepairProcessBP.getData(NoPol, fNoWo, EmployeeFinal)
                    .then(

                        function(res) {
                            console.log("EmployeeFinal", EmployeeFinal);
                            // $scope.DataModel = res.data;
                            // if(EmployeeFinal !== "-"){
                            var data = res.data;

                            var evens = _.remove(data, function(n) {
                                for (var i = 0; i < n.JobTaskBp.length; i++) {
                                    // for(var a = 0; a < n.JobTaskBp[i].JobTaskTechnician.length; a++){
                                    // var flag = 0;
                                    if (n.JobTaskBp[i].Chip1GroupId == EmployeeFinal) {
                                        return n.JobTaskBp[i].Chip1GroupId == EmployeeFinal;
                                    } else if (n.JobTaskBp[i].Chip2GroupId == EmployeeFinal) {
                                        return n.JobTaskBp[i].Chip2GroupId == EmployeeFinal;
                                    } else if (n.JobTaskBp[i].Chip3GroupId == EmployeeFinal) {
                                        return n.JobTaskBp[i].Chip3GroupId == EmployeeFinal;
                                    } else if (n.JobTaskBp[i].Chip4GroupId == EmployeeFinal) {
                                        return n.JobTaskBp[i].Chip4GroupId == EmployeeFinal;
                                    } else if (n.JobTaskBp[i].Chip5GroupId == EmployeeFinal) {
                                        return n.JobTaskBp[i].Chip5GroupId == EmployeeFinal;
                                    } else if (n.JobTaskBp[i].Chip6GroupId == EmployeeFinal) {
                                        return n.JobTaskBp[i].Chip6GroupId == EmployeeFinal;
                                    } else if (n.JobTaskBp[i].Chip7GroupId == EmployeeFinal) {
                                        return n.JobTaskBp[i].Chip7GroupId == EmployeeFinal;
                                    } else {
                                        console.log("gak ada data di", n.JobId);
                                        // $scope.DataModel = [];
                                        // return false;
                                    }
                                    // }
                                }
                            });
                            $scope.DataModel = angular.copy(evens);
                            console.log("Data Setelah Remove", evens);
                            // }else{
                            //   $scope.DataModel = res.data;
                            //   console.log("masuk else");
                            // }

                            var newDataModel = angular.copy($scope.DataModel);
                            var cc = {};
                            console.log("newDataModel", newDataModel);

                            // for(var z = 0; z < newDataModel.length; z++){
                            //   for(var a = 0; a < newDataModel[z].JobTaskBp.length; a++){
                            //     if(newDataModel[z].JobTaskBp[a].Chip6GroupId == EmployeeFinal){
                            //       $scope.chip6 = false;
                            //       console.log("chip 6 ada", newDataModel[z].JobId);
                            //     }


                            //   }
                            // }

                            if (newDataModel.length == 0) {
                                $scope.DataList = [];
                            } else {
                                // var StatusArr = [4, 5, 6, 7, 8, 9, 10, 11, 12, 25];
                                // for (var Stall = 1; Stall <= 7; Stall++) {
                                //     for (var Status = 0; Status <= 6; Status++) {
                                //         cc[Stall + '-' + k] = [];

                                //         _.map(newDataModel, function(value, key) {
                                //             _.map(value.JobTaskBp, function(itemChip, keyChip) {

                                //             });

                                //             if (hasil.Status == 4 || hasil.Status == 5 || hasil.Status == 7 || hasil.Status == 8 || hasil.Status == 9 || hasil.Status == 10 || hasil.Status == 11 || hasil.Status == 12) {
                                //                 hasil.color = "green";
                                //             } else if (hasil.Status == 6) {
                                //                 _.map(hasil.JobTaskBp, function(value2, key2) {

                                //                 });
                                //             } else {
                                //                 hasil.color = "red";
                                //             }

                                //         });
                                //     };
                                // };

                                for (var j = 1; j < 8; j++) {
                                    for (var k = 4; k <= 25; k++) { //dulunya '< 13'
                                        if ((k >= 4 && k <= 12) || (k == 25)) {
                                            var x = j + '-' + k;
                                            cc[j + '-' + k] = [];
                                            for (var i = 0; i < newDataModel.length; i++) {
                                                // var test = cc[j+'-'+k].map(function(item){
                                                var test = newDataModel.map(function(item) {
                                                    var hasil = item;

                                                    // console.log('item bong', item)
                                                    var posisi = 0;
                                                    for (var i = 0; i < item.JobTaskBp.length; i++) {
                                                        if (item.JobTaskBp[i].isOldChip == 0) {
                                                            posisi = i;
                                                        }
                                                    }
                                                    item.NewChip = item.JobTaskBp[posisi];

                                                    //============================= start listing nama teknisi per chip ============================================= start
                                                    for (var pp=0; pp<$scope.dataStallbyGroup.length; pp++){
                                                        if ($scope.dataStallbyGroup[pp].StallId == item.NewChip.Chip1StallId){
                                                            var tek1 = []
                                                            if ($scope.dataStallbyGroup[pp].Initial1 != null){
                                                                tek1.push($scope.dataStallbyGroup[pp].Initial1)
                                                            }
                                                            if ($scope.dataStallbyGroup[pp].Initial2 != null){
                                                                tek1.push($scope.dataStallbyGroup[pp].Initial2)
                                                            }
                                                            if ($scope.dataStallbyGroup[pp].Initial3 != null){
                                                                tek1.push($scope.dataStallbyGroup[pp].Initial3)
                                                            }
                                                            item.NewChip.TeknisiChip1 = tek1.join(', ');
                                                        }

                                                        if ($scope.dataStallbyGroup[pp].StallId == item.NewChip.Chip2StallId){
                                                            var tek2 = []
                                                            if ($scope.dataStallbyGroup[pp].Initial1 != null){
                                                                tek2.push($scope.dataStallbyGroup[pp].Initial1)
                                                            }
                                                            if ($scope.dataStallbyGroup[pp].Initial2 != null){
                                                                tek2.push($scope.dataStallbyGroup[pp].Initial2)
                                                            }
                                                            if ($scope.dataStallbyGroup[pp].Initial3 != null){
                                                                tek2.push($scope.dataStallbyGroup[pp].Initial3)
                                                            }
                                                            item.NewChip.TeknisiChip2 = tek2.join(', ');
                                                        }

                                                        if ($scope.dataStallbyGroup[pp].StallId == item.NewChip.Chip3StallId){
                                                            var tek3 = []
                                                            if ($scope.dataStallbyGroup[pp].Initial1 != null){
                                                                tek3.push($scope.dataStallbyGroup[pp].Initial1)
                                                            }
                                                            if ($scope.dataStallbyGroup[pp].Initial2 != null){
                                                                tek3.push($scope.dataStallbyGroup[pp].Initial2)
                                                            }
                                                            if ($scope.dataStallbyGroup[pp].Initial3 != null){
                                                                tek3.push($scope.dataStallbyGroup[pp].Initial3)
                                                            }
                                                            item.NewChip.TeknisiChip3 = tek3.join(', ');
                                                        }

                                                        if ($scope.dataStallbyGroup[pp].StallId == item.NewChip.Chip4StallId){
                                                            var tek4 = []
                                                            if ($scope.dataStallbyGroup[pp].Initial1 != null){
                                                                tek4.push($scope.dataStallbyGroup[pp].Initial1)
                                                            }
                                                            if ($scope.dataStallbyGroup[pp].Initial2 != null){
                                                                tek4.push($scope.dataStallbyGroup[pp].Initial2)
                                                            }
                                                            if ($scope.dataStallbyGroup[pp].Initial3 != null){
                                                                tek4.push($scope.dataStallbyGroup[pp].Initial3)
                                                            }
                                                            item.NewChip.TeknisiChip4 = tek4.join(', ');
                                                        }

                                                        if ($scope.dataStallbyGroup[pp].StallId == item.NewChip.Chip5StallId){
                                                            var tek5 = []
                                                            if ($scope.dataStallbyGroup[pp].Initial1 != null){
                                                                tek5.push($scope.dataStallbyGroup[pp].Initial1)
                                                            }
                                                            if ($scope.dataStallbyGroup[pp].Initial2 != null){
                                                                tek5.push($scope.dataStallbyGroup[pp].Initial2)
                                                            }
                                                            if ($scope.dataStallbyGroup[pp].Initial3 != null){
                                                                tek5.push($scope.dataStallbyGroup[pp].Initial3)
                                                            }
                                                            item.NewChip.TeknisiChip5 = tek5.join(', ');
                                                        }

                                                        if ($scope.dataStallbyGroup[pp].StallId == item.NewChip.Chip6StallId){
                                                            var tek6 = []
                                                            if ($scope.dataStallbyGroup[pp].Initial1 != null){
                                                                tek6.push($scope.dataStallbyGroup[pp].Initial1)
                                                            }
                                                            if ($scope.dataStallbyGroup[pp].Initial2 != null){
                                                                tek6.push($scope.dataStallbyGroup[pp].Initial2)
                                                            }
                                                            if ($scope.dataStallbyGroup[pp].Initial3 != null){
                                                                tek6.push($scope.dataStallbyGroup[pp].Initial3)
                                                            }
                                                            item.NewChip.TeknisiChip6 = tek6.join(', ');
                                                        }

                                                        if ($scope.dataStallbyGroup[pp].StallId == item.NewChip.Chip7StallId){
                                                            var tek7 = []
                                                            if ($scope.dataStallbyGroup[pp].Initial1 != null){
                                                                tek7.push($scope.dataStallbyGroup[pp].Initial1)
                                                            }
                                                            if ($scope.dataStallbyGroup[pp].Initial2 != null){
                                                                tek7.push($scope.dataStallbyGroup[pp].Initial2)
                                                            }
                                                            if ($scope.dataStallbyGroup[pp].Initial3 != null){
                                                                tek7.push($scope.dataStallbyGroup[pp].Initial3)
                                                            }
                                                            item.NewChip.TeknisiChip7 = tek7.join(', ');
                                                        }
                                                    }
                                                    //============================= start listing nama teknisi per chip ============================================= end

                                                    // if (hasil.Status == 25) {
                                                    //     console.log("hasil >>", hasil);
                                                    // };
                                                    // console.log("wew", hasil);
                                                    if (hasil.Status == 4 || hasil.Status == 5 || hasil.Status == 7 || hasil.Status == 8 || hasil.Status == 9 || hasil.Status == 10 || hasil.Status == 11 || hasil.Status == 12) {
                                                        hasil.color = "green";
                                                    } else if (hasil.Status == 6 || hasil.Status == 25) {
                                                        var webgriz = item.JobTaskBp.map(function(final) {
                                                            if (final.JobTaskProgressBP.length == 0) {
                                                                if (hasil.Status == 4 || hasil.Status == 5 || hasil.Status == 7 || hasil.Status == 8 || hasil.Status == 9 || hasil.Status == 10 || hasil.Status == 11 || hasil.Status == 12) {
                                                                    hasil.color = "green";
                                                                } else {
                                                                    hasil.color = "yellow";
                                                                }
                                                            } else {
                                                                if (final.JobTaskProgressBP[final.JobTaskProgressBP.length - 1].PauseReasonId == 45) {
                                                                    // if(final.JobTaskProgressBP[final.JobTaskProgressBP.length+1].PauseReasonId == 45){
                                                                    if (final.JobTaskProgressBP.some(function(wow) {
                                                                            // console.log("wow 45", wow);
                                                                            return wow.PauseReasonId == 45;
                                                                        })) {
                                                                        hasil.color = "red";
                                                                    }
                                                                    // else{
                                                                    //   hasil.color = "yellow";
                                                                    // }
                                                                } else {
                                                                    hasil.color = "yellow";
                                                                };
                                                            };

                                                        });
                                                        // hasil.color = "yellow"
                                                    } else {
                                                        hasil.color = "red";
                                                    };

                                                    return hasil;
                                                });
                                                // console.log("Test", test);
                                                if (newDataModel[i].ActiveChip == j && (newDataModel[i].Status == k)) {
                                                    cc[j + '-' + k].push(newDataModel[i]);
                                                    $scope.DataList = cc;
                                                    _.map($scope.DataList, function(val) {
                                                        _.map(val, function(item) {
                                                            var hasil = item;
                                                            console.log("hasil ", hasil);

                                                            console.log("hasil Plan Finish ", hasil.PlanFinish);

                                                            var jmltask = 0;
                                                            var adaJobTWC = 0;
                                                            var adaJobPWC = 0;
                                                            var adaMenungguParts = 0;
                                                            // MaterialRequestStatusId: 1 => kalau MaterialRequestStatusId = 1 berarti ada yang di tunggu(menunggu status part info)
                                                            if (hasil.JobTask != undefined || hasil.JobTask != null) {
                                                                // jmltask = hasil.JobTask.length;


                                                                for (var i = 0; i < hasil.JobTask.length; i++) {
                                                                    if (hasil.JobTask[i].isDeleted == 0) {
                                                                        jmltask++;
                                                                    };

                                                                    if (hasil.JobTask[i].JobType != null && hasil.JobTask[i].JobType != undefined) {
                                                                        if (hasil.JobTask[i].JobType.Name == 'TWC' && hasil.JobTask[i].JobType.Name != null && hasil.JobTask[i].JobType.Name != undefined) {
                                                                            adaJobTWC = 1;
                                                                        }
                                                                        if (hasil.JobTask[i].JobType.Name == 'PWC' && hasil.JobTask[i].JobType.Name != null && hasil.JobTask[i].JobType.Name != undefined) {
                                                                            adaJobPWC = 1;
                                                                        }
                                                                    }

                                                                    if (hasil.JobTask[i].JobParts != null && hasil.JobTask[i].JobParts != undefined) {
                                                                        for (var j = 0; j < hasil.JobTask[i].JobParts.length; j++) {
                                                                            if (hasil.JobTask[i].JobParts[j].MaterialRequestStatusId != null && hasil.JobTask[i].JobParts[j].MaterialRequestStatusId != undefined) {
                                                                                if (hasil.JobTask[i].JobParts[j].MaterialRequestStatusId == 1) {
                                                                                    adaMenungguParts = 1;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            // $scope.DataWoList.jmltask = jmltask;
                                                            // $scope.DataWoList.adaJobTWC = adaJobTWC;
                                                            // $scope.DataWoList.adaJobPWC = adaJobPWC;
                                                            item.jmltask = jmltask;
                                                            item.adaJobTWC = adaJobTWC;
                                                            item.adaJobPWC = adaJobPWC;
                                                            item.adaMenungguParts = adaMenungguParts;
                                                            console.log("jmltask", jmltask);
                                                        });
                                                    });
                                                    console.log("$scope.DataList >", $scope.DataList);
                                                } else {
                                                    // console.log("$Do Nothing !");
                                                }
                                                console.log("$scope.DataList", newDataModel.length);

                                            }
                                        }
                                    }
                                }
                            }
                            // console.log("Data Di Dalam Table ==>",$scope.DataList);

                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );

            }
        }



        // $scope.DataPetugasQC = [
        //     {
        //         id : 1,
        //         Name:"Budi"
        //     },
        //     {
        //         id : 2,
        //         Name:"Amin"
        //     }

        // ];

        $scope.xJobData = [{
                JobName: "Bumper Kiri",
                Type: "Replace"
            },
            {
                JobName: "Fender Kiri",
                Type: "Replace"
            }
        ];


        $scope.getListRSA = function() {

            RepairProcessBP.getListSA()
                .then(

                    function(res) {
                        console.log("data RSA", res.data);
                        // $scope.PauseReasonId = res.data.Result;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        }

        $scope.CountShift = function() {
            // console.log("model", $scope.mData);
            // console.log("model", $scope.mData.CategoryJobSuggestKeamanan);
            $scope.mData.SuggestionCatg =
                ($scope.mData.CategoryJobSuggestKeamanan == undefined ? 0 : $scope.mData.CategoryJobSuggestKeamanan) +
                ($scope.mData.CategoryJobSuggestPeraturan == undefined ? 0 : $scope.mData.CategoryJobSuggestPeraturan) +
                ($scope.mData.CategoryJobSuggestKenyamanan == undefined ? 0 : $scope.mData.CategoryJobSuggestKenyamanan) +
                ($scope.mData.CategoryJobSuggestEfisiensi == undefined ? 0 : $scope.mData.CategoryJobSuggestEfisiensi);
            console.log("jajaj", $scope.mData.SuggestionCatg);
            // // $scope.mData.SuggestionCatg = $scope.mData.CategoryJobSuggestKeamanan + $scope.mData.CategoryJobSuggestPeraturan + $scope.mData.CategoryJobSuggestKenyamanan + $scope.mData.CategoryJobSuggestEfisiensi;
        }

        $scope.SimpanJobSuggest = function(Model) {
            // Model.SuggestionDate = Model.SuggestionDate.getFullYear() + '-' + (Model.SuggestionDate.getMonth() + 1) + '-' + Model.SuggestionDate.getDate();

            if (_.isNull($scope.mData.Suggestion) || $scope.mData.Suggestion == "" || _.isUndefined($scope.mData.Suggestion) || $scope.mData.Suggestion == "Tidak ada saran") {

                RepairProcessGR.postJobSuggest($scope.xchoseData, Model).then(
                    function(res) {
                        // $scope.getData();
                        // $scope.getDataDetail($scope.JbId);
                        // $scope.mData=[];
                        // $scope.xDataWoList

                        RepairProcessGR.putJobSuggestCRM($scope.VinData[0].VIN, Model).then(
                            function(res) {
                                // $scope.getData();
                                // $scope.getDataDetail($scope.JbId);
                                // $scope.mData=[];
                                // $scope.xDataWoList


                                RepairProcessGR.putJobSuggestCT($scope.VinData[0].VIN, Model).then(
                                    function(res) {
                                        // $scope.getData();

                                        RepairProcessGR.putTeknisiNote($scope.xchoseData, $scope.mData.TechnicianNotes)
                                            .then(

                                                function(res) {
                                                    console.log("suksess");
                                                    $scope.choseData($scope.xchoseData);
                                                    $scope.mData = [];
                                                },
                                                function(err) {
                                                    console.log("err=>", err);
                                                }
                                            );

                                        // $scope.xDataWoList

                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                );

                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );

                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

            } else {
                if (_.isNull($scope.mData.SuggestionDate) || $scope.mData.SuggestionDate == "" || _.isUndefined($scope.mData.SuggestionDate)) {
                    bsNotify.show({

                        size: 'big',
                        timeout: 2000,
                        type: 'danger',
                        title: "Mohon isi job suggest date"
                    });
                } else {
                    RepairProcessGR.postJobSuggest($scope.xchoseData, Model).then(
                        function(res) {
                            // $scope.getData();
                            // $scope.getDataDetail($scope.JbId);
                            // $scope.mData=[];
                            // $scope.xDataWoList

                            RepairProcessGR.putJobSuggestCRM($scope.VinData[0].VIN, Model).then(
                                function(res) {
                                    // $scope.getData();
                                    // $scope.getDataDetail($scope.JbId);
                                    // $scope.mData=[];
                                    // $scope.xDataWoList


                                    RepairProcessGR.putJobSuggestCT($scope.VinData[0].VIN, Model).then(
                                        function(res) {
                                            // $scope.getData();

                                            RepairProcessGR.putTeknisiNote($scope.xchoseData, $scope.mData.TechnicianNotes)
                                                .then(

                                                    function(res) {
                                                        console.log("suksess");

                                                        $scope.choseData($scope.xchoseData);
                                                        $scope.mData = [];
                                                    },
                                                    function(err) {
                                                        console.log("err=>", err);
                                                    }
                                                );

                                            // $scope.xDataWoList

                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                        }
                                    );

                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );

                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
                }
            }

        }

        $scope.getDataPause = function() {

            // document.write(today);
            $scope.PauseResaon = [];
            RepairProcessBP.getPauseReason()
                .then(

                    function(res) {
                        $scope.PauseReasonId = res.data.Result;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );


        }


        $scope.FilterType = [{
                FilterId: 1,
                FilterText: 'No. Polisi'
            },
            {
                FilterId: 2,
                FilterText: 'No. WO'
            }
        ];

        $scope.MaterialYa = function(MaterialValue) {
            if (MaterialValue == 1) {
                $scope.material = true;
            } else if (MaterialValue == 2) {
                $scope.material = false;
            }
        }

        $scope.MaterialTidak = function() {
            $scope.material = false;
        }

        $scope.NeedTime = function(TimeValue) {
            if (TimeValue == 1) {
                $scope.duration = true;
            } else if (TimeValue == 2) {
                $scope.duration = false;
            }
        }

        var copygridDefect = [];

        $scope.showAlasanLolosQC = function(Button) {
            
            if (Button == 1) {
                // lolos

                if ($scope.gridDefect.data.length >= 0){
                    copygridDefect = angular.copy($scope.gridDefect.data)
                } else {
                    copygridDefect = [];
                }

                $scope.show = false;
                $scope.showPetugasQc = true;
                $scope.ButtonLolosQC = true;
                $scope.ButtonTidakLolosQC = false;

                delete $scope.mData.RedoToChipKe;
                delete $scope.mData.FreDoJob;
                delete $scope.mData.ChipNotPassQcReason;
                delete $scope.mData.ChipIsOtherReason;
                delete $scope.mData.ChipOtherReason;
                delete $scope.mData.ChipAdditionNote;
                delete $scope.mData.NamePekerjaanDefect;
                delete $scope.mData.NameDefect;
                delete $scope.mData.JumlahDefect;
                $scope.gridDefect.data = [];
                $scope.dataPekerjaanDefect = [];



            } else if (Button == 2) {
                // tidak lolos
                $scope.show = true;
                $scope.showPetugasQc = true;
                $scope.ButtonLolosQC = false;
                $scope.ButtonTidakLolosQC = true;

                $scope.mData.RedoToChipKe = null;
                $scope.mData.FreDoJob = null;
                $scope.mData.ChipNotPassQcReason = null;
                $scope.mData.ChipIsOtherReason = null;
                $scope.mData.ChipOtherReason = null;
                $scope.mData.ChipAdditionNote = null;
                $scope.mData.NamePekerjaanDefect = null;
                $scope.mData.NameDefect = null;
                $scope.mData.JumlahDefect = null;
                if ($scope.gridDefect.data == undefined || $scope.gridDefect.data == null){
                    $scope.gridDefect.data = [];
                }
                if (copygridDefect.length > 0){
                    $scope.gridDefect.data = angular.copy(copygridDefect)
                }
                JobTaskIdQC = []
            }
        }

        $scope.showAlasanTidakLolosQC = function(Button) {
            $scope.show = true;
            if (Button == 1) {
                $scope.ButtonLolosQC = false;
                $scope.ButtonTidakLolosQC = true;
            } else if (Button == 2) {
                $scope.ButtonLolosQC = false;
                $scope.ButtonTidakLolosQC = false;
            }
        }

        $scope.StatusFixedBtn = function(value) {
            if (value == 1) {
                $scope.StatusFixed = true;
                $scope.StatusNotFixed = false;
            } else if (value == 2) {
                $scope.StatusNotFixed = true;
                $scope.StatusFixed = false;
            }
        }

        $scope.clockOn = function(JobId, ActiveChip, Model) {
            RepairProcessBP.CheckIsJobCancelWO(JobId).then(function (resultC) {
                if (resultC.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                    });
                } else {
                    console.log("Job Id", JobId);
                    console.log("Active Chip", ActiveChip);
                    console.log('cek data', $scope.DataList);
                    $scope.clickBerlebih = true;
                    // var JumlahChipClockOn = $scope.DataList[ActiveChip + '-' + 5].length + $scope.DataList[ActiveChip + '-' + 7].length;
                    var JumlahChipClockOn = 0;
                    // SA.JobTaskBp[0].Chip4GroupId !== mData.EmployeeId
                    var posisiOldChip4 = 0;
                    var posisiOldChip5 = 0;
                    var posisiOldChip7 = 0;
        
                    //buat cari stall id chip yg active
                    var stallIdActive = 0;
                    angular.forEach($scope.DataList[ActiveChip + '-' + 4], function(val, k) {
                        if (val.JobId == JobId) {
                            for (var i = 0; i < val.JobTaskBp.length; i++) {
                                if (val.JobTaskBp[i].isOldChip == 0) {
                                    posisiOldChip4 = i;
                                }
                            }
        
                            stallIdActive = val.JobTaskBp[posisiOldChip4]['Chip' + ActiveChip + 'StallId'];
                        }
                    });
        
                    if ($scope.DataList[ActiveChip + '-' + 5].length > 0) {
                        angular.forEach($scope.DataList[ActiveChip + '-' + 5], function(value, key) {
                            // this.push(key + ': ' + value);
                            console.log('asem1', value)
                            for (var i = 0; i < value.JobTaskBp.length; i++) {
                                if (value.JobTaskBp[i].isOldChip == 0) {
                                    posisiOldChip5 = i;
                                }
                            }
                            var stallOnList = 0;
                            stallOnList = value.JobTaskBp[posisiOldChip5]['Chip' + ActiveChip + 'StallId'];
                            console.log('param 0 ', stallOnList);
                            console.log('param 1', value.JobTaskBp[posisiOldChip5]['Chip' + ActiveChip + 'GroupId'])
                            console.log('param 2', $scope.mData.EmployeeId)
                            if (value.JobTaskBp[posisiOldChip5]['Chip' + ActiveChip + 'GroupId'] == $scope.mData.EmployeeId) {
                                if (stallIdActive == stallOnList) {
                                    JumlahChipClockOn = 1;
                                }
                            }
        
                        });
                    }
        
                    if ($scope.DataList[ActiveChip + '-' + 7].length > 0) {
                        angular.forEach($scope.DataList[ActiveChip + '-' + 7], function(value, key) {
                            // this.push(key + ': ' + value);
                            console.log('asem2', value)
                            for (var i = 0; i < value.JobTaskBp.length; i++) {
                                if (value.JobTaskBp[i].isOldChip == 0) {
                                    posisiOldChip7 = i;
                                }
                            }
                            var stallOnList = 0;
                            stallOnList = value.JobTaskBp[posisiOldChip5]['Chip' + ActiveChip + 'StallId'];
                            console.log('param 0 ', stallOnList);
        
                            if (value.JobTaskBp[posisiOldChip7]['Chip' + ActiveChip + 'GroupId'] == $scope.mData.EmployeeId) {
                                if (stallIdActive == stallOnList) {
                                    JumlahChipClockOn = 1;
                                }
                            }
        
                        });
                    }
        
                    console.log('JumlahChipClockOn', JumlahChipClockOn);
        
        
                    var pos4 = 0;
                    var cekChipDispatchForClock = 0;
                    var cekChipStallForClock = -2;
                    var Durasi = 0;
        
                    if ($scope.DataList[ActiveChip + '-' + 4].length > 0) {
                        angular.forEach($scope.DataList[ActiveChip + '-' + 4], function(value, key) {
                            // this.push(key + ': ' + value);
                            console.log('asem3', value)
                            if (value.JobId == JobId) {
                                for (var i = 0; i < value.JobTaskBp.length; i++) {
                                    if (value.JobTaskBp[i].isOldChip == 0) {
                                        pos4 = i;
                                    }
                                }
                                $scope.groupBPid = value.JobTaskBp[pos4]['Chip' + ActiveChip + 'GroupId']
        
                                for (var i = 0; i < value.JobTaskBp.length; i++) {
                                    if (value.JobTaskBp[i].isOldChip == 0) {
                                        // Chip1Status
                                        cekChipDispatchForClock = value.JobTaskBp[i].isDispatched; // cek dl uda dispatch belum
                                        cekChipStallForClock = value.JobTaskBp[i]['Chip' + ActiveChip + 'StallId']; //cek juga uda di plot belum
                                        console.log('masuk ke cek status1', cekChipDispatchForClock)
                                        console.log('masuk ke cek status2', value.JobTaskBp[i]['Chip' + ActiveChip + 'Status'])
        
                                        if (ActiveChip == 1) {
                                            Durasi = value.JobTaskBp[i].BodyEstimationMinute;
                                        } else if (ActiveChip == 2) {
                                            Durasi = value.JobTaskBp[i].PutyEstimationMinute;
                                        } else if (ActiveChip == 3) {
                                            Durasi = value.JobTaskBp[i].SurfacerEstimationMinute;
                                        } else if (ActiveChip == 4) {
                                            Durasi = value.JobTaskBp[i].SprayingEstimationMinute;
                                        } else if (ActiveChip == 5) {
                                            Durasi = value.JobTaskBp[i].PolishingEstimationMinute;
                                        } else if (ActiveChip == 6) {
                                            Durasi = value.JobTaskBp[i].ReassemblyEstimationMinute;
                                        } else if (ActiveChip == 7) {
                                            Durasi = value.JobTaskBp[i].FIEstimationMinute;
                                        }
                                    }
                                }
        
                            }
        
        
        
                        });
                    }
                    console.log('group nya', $scope.groupBPid);
        
                    var cekJamClockOnVSPlanBoard = 0;
                    angular.forEach($scope.DataList[ActiveChip + '-' + 4], function(value, key) {
                        if (JobId == value.JobId) {
                            var actualNOW = new Date();
                            var lastTaskId = value.JobTaskBp.length - 1;
                            if (actualNOW >= value.JobTaskBp[lastTaskId]['Chip' + ActiveChip + 'PlanStart']) {
                                cekJamClockOnVSPlanBoard = 0;
                                console.log('ok boleh clock on')
                            } else {
                                cekJamClockOnVSPlanBoard = 1;
                                console.log('blm blh clock on blm sesuai plan')
                            }
        
                        }
                    });
        
                    RepairProcessBP.putCheckStatusCLockOn(JobId, cekChipStallForClock, ActiveChip).then( //ini buat cek chip nya kalau di clock on, ada yg lg clock on lg ga di stall itu
                        function(res) {
                            console.log('ini res check clock on', res)
                            var hsl = res.data.split('#');
        
                            if (hsl[0] == '0') {
                            // if (res.data == 0) {
                                // JobId , StallId, ClockOn, ChipDuration, ChipType  //clockon adalah jam clock on nya
                                var tempJamClockon = new Date();
                                var jamClockon = tempJamClockon.getFullYear() + '-' + (tempJamClockon.getMonth() + 1) + '-' + tempJamClockon.getDate() + ' ' + tempJamClockon.getHours() + ':' + tempJamClockon.getMinutes() + ':' + tempJamClockon.getSeconds();
                                RepairProcessBP.putCheckCLockOnBentrok(JobId, cekChipStallForClock, jamClockon, Durasi, ActiveChip).then( //ini buat cek chip nya kalau di clock on kan maju, nanti bentrok ga gt.
                                    function(resx) {
                                        console.log('ini res check clock on', resx)
                                        var hasil = resx.data.split('#')
        
                                        if (hasil[0] == '0') {
                                        // if (resx.data == 0) {
        
                                            if (JumlahChipClockOn > 0) {
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "Proses ini ada pekerjaan yang sedang berjalan."
                                                });
                                                $scope.clickBerlebih = false;
                                            } else if (cekChipDispatchForClock != 1 || cekChipStallForClock <= 0) {
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "Proses belum dispatch / belum di plot, silahkan cek ulang"
                                                });
                                                $scope.clickBerlebih = false;
                                            } else {
                                                bsAlert.alert({
                                                        title: "Anda yakin untuk clock on?",
                                                        text: "",
                                                        type: "question",
                                                        showCancelButton: true
                                                    },
                                                    function() {

                                                        RepairProcessBP.GetJobTasks(JobId).then(function (resultJ) {

                                                            var ada_job_dihapus = 0;
                                                            var ada_job_ditambah = 0;
                                                            if (resultJ.data.length > $scope.copy_dataTask.length){
                                                                ada_job_ditambah++
                                                            }
                                                            for (var q=0; q<$scope.copy_dataTask.length; q++){
                                                                for (var d=0; d<resultJ.data.length; d++){
                                                                    if ($scope.copy_dataTask[q].JobTaskId == resultJ.data[d].JobTaskId){
                                                                        if (resultJ.data[d].isDeleted == 1 && $scope.copy_dataTask[q].isDeleted == 0){
                                                                            // berarti ada yg di delete tp si foreman blm refresh
                                                                            ada_job_dihapus++
                                                                        }
                                                                    }
                                                                }
                                                            }
                                    
                                                            if (ada_job_dihapus > 0 || ada_job_ditambah > 0){
                                                                if (ada_job_ditambah > 0){
                                                                    bsNotify.show({
                                                                        size: 'big',
                                                                        type: 'danger',
                                                                        // title: "Ada penambahan pekerjaan, silahkan ploting chip pekerjaan baru terlebih dahulu.",
                                                                        title: "Terdapat penambahan pekerjaan baru oleh SA, konfirmasi PTM untuk melakukan ploting ulang",

                                                                    });
                                                                }
                                    
                                                                if (ada_job_dihapus > 0){
                                                                    bsNotify.show({
                                                                        size: 'big',
                                                                        type: 'danger',
                                                                        // title: "Ada pengurangan pekerjaan, silahkan refresh data terlebih dahulu.",
                                                                        title: "Terdapat pengurangan pekerjaan baru oleh SA, konfirmasi PTM untuk melakukan ploting ulang",

                                                                    });
                                                                }

                                                                $scope.clickBerlebih = false;
                                                                ngDialog.closeAll();
                                                                $scope.choseData(JobId);
                                                                $scope.close();
                                                                return
                                                            } else {
                                                                $scope.doClockOn(JobId, ActiveChip, Model);
                                                            }
                                                        })
                                                    },
                                                    function() {
                                                        $scope.clickBerlebih = false;
        
                                                    }
                                                )
        
                                                // ngDialog.openConfirm({
                                                //     template: '\
                                                //          <p>Anda yakin untuk clock on</p>\
                                                //          <div class="ngdialog-buttons">\
                                                //            <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog()">Tidak</button>\
                                                //            <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm()">Ya</button>\
                                                //          </div>',
                                                //     plain: true,
                                                //     controller: 'RepairProcessBPController',
                                                // }).then(function(value) {
                                                //     $scope.doClockOn(JobId, ActiveChip, Model);
                                                // }, function(value) {
        
                                                //     //Do something
                                                //     //console.log("else",value);
                                                // });
        
                                            }
        
                                        } else {
                                            bsNotify.show({
                                                size: 'big',
                                                type: 'danger',
                                                title: "Proses ini akan menumpuk dengan chip No. Polisi " + hasil[1] + "."
                                                // title: "Proses ini ada pekerjaan yang sedang berjalan)."
                                            });
                                            $scope.clickBerlebih = false;
                                        }
        
        
                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                        $scope.clickBerlebih = false;
                                    }
                                );
        
                            } else {
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Proses ini ada pekerjaan yang sedang berjalan (No. Polisi " + hsl[1] + ")."
                                });
                                $scope.clickBerlebih = false;
                            }
        
                        },
                        function(err) {
                            console.log("err=>", err);
                            $scope.clickBerlebih = false;
                        }
                    );
                        
                    
                }
            });
            
        }

        $scope.doClockOn = function(JobId, ActiveChip, Model) {
            // console.log("clockOn");
            RepairProcessBP.getStatWO($scope.xDataWoList[0].JobId,$scope.xDataWoList[0].Status).then(function(result5){
                if (result5.data == 1) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                    });
                    $scope.choseData(JobId);
                    $scope.close();
                    ngDialog.closeAll();
                    $scope.clickBerlebih = false;
                } else {
                    RepairProcessBP.putClock(1, JobId, ActiveChip, Model).then(
                        function(res) {
                            RepairProcessBP.CreateOplFromClockOn(JobId, $scope.groupBPid).then(function(res) {
                                console.log('buat OPL nih', res)
                            })
                            $scope.choseData(JobId);
                            $scope.close();
                        },
                        function(err) {
                            console.log("err=>", err);
                            $scope.clickBerlebih = false;
                        }
                    );
                    ngDialog.closeAll();
                    // return Model.Status
                }
            })
            
        }

        $scope.clockOff = function(JobId, ActiveChip, Model) {
            RepairProcessBP.CheckIsJobCancelWO(JobId).then(function (resultC) {
                if (resultC.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                    });
                } else {
                    console.log("Job Id", JobId);
                    console.log("Active Chip", ActiveChip);
                    $scope.clickBerlebih = true;
        
                    bsAlert.alert({
                            title: "Anda yakin untuk clock off?",
                            text: "",
                            type: "question",
                            showCancelButton: true
                        },
                        function() {
                            RepairProcessBP.GetJobTasks(JobId).then(function (resultJ) {

                                var ada_job_dihapus = 0;
                                var ada_job_ditambah = 0;
                                if (resultJ.data.length > $scope.copy_dataTask.length){
                                    ada_job_ditambah++
                                }
                                for (var q=0; q<$scope.copy_dataTask.length; q++){
                                    for (var d=0; d<resultJ.data.length; d++){
                                        if ($scope.copy_dataTask[q].JobTaskId == resultJ.data[d].JobTaskId){
                                            if (resultJ.data[d].isDeleted == 1 && $scope.copy_dataTask[q].isDeleted == 0){
                                                // berarti ada yg di delete tp si foreman blm refresh
                                                ada_job_dihapus++
                                            }
                                        }
                                    }
                                }
        
                                if (ada_job_dihapus > 0 || ada_job_ditambah > 0){
                                    if (ada_job_ditambah > 0){
                                        bsNotify.show({
                                            size: 'big',
                                            type: 'danger',
                                            // title: "Ada penambahan pekerjaan, silahkan ploting chip pekerjaan baru terlebih dahulu.",
                                            title: "Terdapat penambahan pekerjaan baru oleh SA, konfirmasi PTM untuk melakukan ploting ulang",

                                        });
                                    }
        
                                    if (ada_job_dihapus > 0){
                                        bsNotify.show({
                                            size: 'big',
                                            type: 'danger',
                                            // title: "Ada pengurangan pekerjaan, silahkan refresh data terlebih dahulu.",
                                            title: "Terdapat pengurangan pekerjaan baru oleh SA, konfirmasi PTM untuk melakukan ploting ulang",

                                        });
                                    }

                                    $scope.clickBerlebih = false;
                                    ngDialog.closeAll();
                                    $scope.choseData(JobId);
                                    $scope.close();
                                    return
                                } else {
                                    $scope.doClockOff(JobId, ActiveChip, Model);
        
                                    //kirim notifikasi clock off
                                    var DataNotif = {};
                                    var messagetemp = "";
                                    messagetemp = Model.PoliceNumber;
                
                
                                    DataNotif.Message = messagetemp;
                                    RepairProcessBP.sendNotifRole(DataNotif, 1032, 30).then(
                                        function(res) {
                
                                        },
                                        function(err) {
                                            //console.log("err=>", err);
                                    });
                                }
                            })
                            
                        },
                        function() {
                            $scope.clickBerlebih = false;
        
                        }
                    )
        
        
                    // ngDialog.openConfirm({
                    //     template: '\
                    //          <p>Anda yakin untuk clock off</p>\
                    //          <div class="ngdialog-buttons">\
                    //            <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog()">Tidak</button>\
                    //            <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm()">Ya</button>\
                    //          </div>',
                    //     plain: true,
                    //     controller: 'RepairProcessBPController',
                    // }).then(function(value) {
                    //     $scope.doClockOff(JobId, ActiveChip, Model);
                    // }, function(value) {
        
                    // });
                }
            });
            
        }

        $scope.doClockOff = function(JobId, ActiveChip, Model) {
            RepairProcessBP.getStatWO($scope.xDataWoList[0].JobId,$scope.xDataWoList[0].Status).then(function(result5){
                if (result5.data == 1) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                    });
                    $scope.choseData(JobId);
                    $scope.close();
                    ngDialog.closeAll();
                    $scope.clickBerlebih = false;
                } else {
                    RepairProcessBP.putClock(4, JobId, ActiveChip, Model).then(
                        function(res) {
                            $scope.choseData(JobId);
                            $scope.close();
                        },
                        function(err) {
                            console.log("err=>", err);
                            $scope.clickBerlebih = false;
                        }
                    );
                    ngDialog.closeAll();
                }
            })
            
        }

        //Pause Area////////////////////////////////////////////////////////
        $scope.pausearea = true;
        $scope.pause = function(JobId) {
            RepairProcessBP.CheckIsJobCancelWO(JobId).then(function (resultC) {
                if (resultC.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                    });
                } else {
                    $scope.pausearea = false;
                    // $scope.showPause = true;
                    // $scope.getDataPause();
                }
            });
            
        }

        $scope.problemfinding = function() {
            $scope.showPause = true;


            RepairProcessGR.getDataJobTask($scope.xchoseData).then(
                function(res) {
                    $scope.DataPF = res.data.Result;
                    $scope.dataJobParts = [];
                    for (var i = 0; i < $scope.DataPF.length; i++) {
                        for (var j = 0; j < $scope.DataPF[i].JobParts.length; j++) {
                            $scope.dataJobParts.push($scope.DataPF[i].JobParts[j]);
                        }
                        // $scope.dataJobParts = $scope.DataPF[i].JobParts;
                    }
                    console.log("Data pf", $scope.DataPF);
                },
                function(err) {
                    console.log("err=>", err);
                }
            );

        }


        var JobTaskIdDtl = [];

        $scope.ChoseDataDtl = function(JobTaskId, $index) {
            console.log("JobTaskId", JobTaskId, "idx", $index);
            var a = JobTaskIdDtl.indexOf(JobTaskId);
            if (a > -1) {
                JobTaskIdDtl.splice(a, 1);
                console.log("nih di if 1", JobTaskIdDtl);
            } else {
                JobTaskIdDtl.push(JobTaskId);
                console.log("nih else", JobTaskIdDtl);
            }
            // if(JobTaskIdDtl.length == 0){
            //   $scope.btnFI = true;
            // }else{
            //   $scope.btnFI = false;
            // }
        }

        $scope.clockPause = function(JobId, ActiveChip, Model) {
            console.log("ModelPause", Model, ActiveChip);
            console.log("$scope.DataDetailModel[0].Job.WoNo", $scope.DataDetailModel[0].Job.WoNo);

            if ($scope.mData.PauseReasonId == null || $scope.mData.PauseReasonId == '' || $scope.mData.PauseReasonId == undefined) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Pilih Alasan Pause"
                });
            } else {
                // if($scope.mData.SuggestionDate == undefined || $scope.mData.SuggestionDate == null || $scope.mData.Suggestion == null || $scope.mData.Suggestion == undefined ){
                //       bsNotify.show(
                //         {
                //           size: 'big',
                //           type: 'danger',
                //           title: "Mohon isi Suggestion"
                //         }
                //       );
                // }else{

                var msg_title = 'Anda yakin untuk pause?'
                var msg_text = ''

                if ($scope.mData.PauseReasonId == 43) {
                    msg_title = 'Apakah Anda yakin memilih Pause Rawat Jalan ?'
                    msg_text = 'Pause Rawat Jalan membutuhkan Pelunasan Biaya ke kasir sebelum mobil dapat melakukan Gate Out.'
                } else {
                    msg_title = 'Anda yakin untuk pause?'
                    msg_text = ''
                }

                bsAlert.alert({
                        // title: "Anda yakin untuk pause?",
                        title: msg_title,
                        // text: "",
                        text: msg_text,
                        type: "question",
                        showCancelButton: true
                    },
                    function() {

                        RepairProcessBP.getStatWO($scope.xDataWoList[0].JobId,$scope.xDataWoList[0].Status).then(function(result5){
                            if (result5.data == 1) {
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                                });
                                $scope.choseData(JobId);
                                $scope.close();
                                ngDialog.closeAll();
                                $scope.clickBerlebih = false;
                            } else {
                                RepairProcessBP.GetJobTasks(JobId).then(function (resultJ) {

                                    var ada_job_dihapus = 0;
                                    var ada_job_ditambah = 0;
                                    if (resultJ.data.length > $scope.copy_dataTask.length){
                                        ada_job_ditambah++
                                    }
                                    for (var q=0; q<$scope.copy_dataTask.length; q++){
                                        for (var d=0; d<resultJ.data.length; d++){
                                            if ($scope.copy_dataTask[q].JobTaskId == resultJ.data[d].JobTaskId){
                                                if (resultJ.data[d].isDeleted == 1 && $scope.copy_dataTask[q].isDeleted == 0){
                                                    // berarti ada yg di delete tp si foreman blm refresh
                                                    ada_job_dihapus++
                                                }
                                            }
                                        }
                                    }
            
                                    if (ada_job_dihapus > 0 || ada_job_ditambah > 0){
                                        if (ada_job_ditambah > 0){
                                            bsNotify.show({
                                                size: 'big',
                                                type: 'danger',
                                                // title: "Ada penambahan pekerjaan, silahkan ploting chip pekerjaan baru terlebih dahulu.",
                                                title: "Terdapat penambahan pekerjaan baru oleh SA, konfirmasi PTM untuk melakukan ploting ulang",
        
                                            });
                                        }
            
                                        if (ada_job_dihapus > 0){
                                            bsNotify.show({
                                                size: 'big',
                                                type: 'danger',
                                                // title: "Ada pengurangan pekerjaan, silahkan refresh data terlebih dahulu.",
                                                title: "Terdapat pengurangan pekerjaan baru oleh SA, konfirmasi PTM untuk melakukan ploting ulang",
        
                                            });
                                        }
        
                                        $scope.clickBerlebih = false;
                                        ngDialog.closeAll();
                                        $scope.choseData(JobId);
                                        $scope.close();
                                        return
                                    } else {
                                        RepairProcessBP.putClock(2, JobId, ActiveChip, Model).then(
                                            function(res) {
                
                                                Model.ResponseDesc = "";
                                                Model.T2 = "";
                                                Model.T1 = "";
                                                Model.TroubleDesc = "";
                                                Model.Duration = "";
                                                $scope.duration = false;
                                                Model.IsNeedAdditionalTime = false;
                                                Model.IsOwnDo = false;
                                                Model.IsNeedRSA = false;
                                                Model.PauseReasonDesc = "";
                                                Model.PauseReasonId = "";
                                                $scope.showPause = false;
                                                $scope.pausearea = true;
                                                // $scope.SimpanJobSuggest(Model);
                
                                                bsNotify.show({
                                                    title: "Update",
                                                    content: "Data berhasil disimpan",
                                                    type: 'success'
                                                });
                
                                                $scope.choseData(JobId);
                                                $scope.close();
                
                                                RepairProcessBP.sendNotifPause($scope.DataDetailModel[0].Job.WoNo, $scope.IdSA, 12).then(
                                                    function(res) {
                                                        console.log('Suksess simpan');
                
                                                    },
                                                    function(err) {
                                                        console.log("err=>", err);
                                                    }
                                                );
                
                                            },
                                            function(err) {
                                                console.log("err=>", err);
                                            }
                                        );
                                    }
                                })
                            }
                        })

                        
                        
                        
                    },
                    function() {

                    }
                )


                // ngDialog.openConfirm({
                //     template: '\
                //          <p>Anda yakin untuk pause?</p>\
                //          <div class="ngdialog-buttons">\
                //            <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog()">Tidak</button>\
                //            <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm()">Ya</button>\
                //          </div>',
                //     plain: true,
                //     controller: 'RepairProcessBPController',
                // }).then(function(value) {
                //     // $scope.doClockOn(JobId, ActiveChip, Model);                    
                //     RepairProcessBP.putClock(2, JobId, ActiveChip, Model).then(
                //         function(res) {

                //             Model.ResponseDesc = "";
                //             Model.T2 = "";
                //             Model.T1 = "";
                //             Model.TroubleDesc = "";
                //             Model.Duration = "";
                //             $scope.duration = false;
                //             Model.IsNeedAdditionalTime = false;
                //             Model.IsOwnDo = false;
                //             Model.IsNeedRSA = false;
                //             Model.PauseReasonDesc = "";
                //             Model.PauseReasonId = "";
                //             $scope.showPause = false;
                //             $scope.pausearea = true;
                //             // $scope.SimpanJobSuggest(Model);
                //             $scope.choseData(JobId);
                //             $scope.close();

                //         },
                //         function(err) {
                //             console.log("err=>", err);
                //         }
                //     );

                // }, function(value) {

                //     //Do something
                //     //console.log("else",value);
                // });



                // }
            }
        }

        $scope.clockPauseTec = function(JobId, ActiveChip, Model) {
            console.log("ModelPause", Model, ActiveChip);
            console.log("$scope.DataDetailModel[0].Job.WoNo", $scope.DataDetailModel[0].Job.WoNo);

            var odel = angular.copy(Model);
            if (JobTaskIdDtl.length == 0) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Pilih Task"
                });
            } else {
                RepairProcessBP.getStatWO($scope.xDataWoList[0].JobId,$scope.xDataWoList[0].Status).then(function(result5){
                    if (result5.data == 1) {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                        });
                        $scope.choseData(JobId);
                        $scope.close();
                        ngDialog.closeAll();
                        $scope.clickBerlebih = false;
                    } else {
                        RepairProcessBP.GetJobTasks(JobId).then(function (resultJ) {

                            var ada_job_dihapus = 0;
                            var ada_job_ditambah = 0;
                            if (resultJ.data.length > $scope.copy_dataTask.length){
                                ada_job_ditambah++
                            }
                            for (var q=0; q<$scope.copy_dataTask.length; q++){
                                for (var d=0; d<resultJ.data.length; d++){
                                    if ($scope.copy_dataTask[q].JobTaskId == resultJ.data[d].JobTaskId){
                                        if (resultJ.data[d].isDeleted == 1 && $scope.copy_dataTask[q].isDeleted == 0){
                                            // berarti ada yg di delete tp si foreman blm refresh
                                            ada_job_dihapus++
                                        }
                                    }
                                }
                            }
        
                            if (ada_job_dihapus > 0 || ada_job_ditambah > 0){
                                if (ada_job_ditambah > 0){
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        // title: "Ada penambahan pekerjaan, silahkan ploting chip pekerjaan baru terlebih dahulu.",
                                        title: "Terdapat penambahan pekerjaan baru oleh SA, konfirmasi PTM untuk melakukan ploting ulang",
                                    });
                                }
        
                                if (ada_job_dihapus > 0){
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        // title: "Ada pengurangan pekerjaan, silahkan refresh data terlebih dahulu.",
                                        title: "Terdapat pengurangan pekerjaan baru oleh SA, konfirmasi PTM untuk melakukan ploting ulang",
        
                                    });
                                }
        
                                $scope.clickBerlebih = false;
                                ngDialog.closeAll();
                                $scope.choseData(JobId);
                                $scope.close();
                                return
                            } else {
                                RepairProcessBP.putClockProblem(2, JobId, ActiveChip, Model, JobTaskIdDtl).then(function(res) {
                                        $scope.duration = false;
                                        Model.IsNeedAdditionalTime = false;
                                        Model.IsOwnDo = false;
                                        Model.IsNeedRSA = false;
                                        $scope.choseData(JobId);
                                        $scope.showPause = false;
                                        console.log("$scope.DataDetailModel[0].Job.WoNo", $scope.DataDetailModel[0].Job.WoNo);
                
                                        RepairProcessBP.sendNotifPause($scope.DataDetailModel[0].Job.WoNo, $scope.IdSA, 12).then(
                                            function(res) {
                                                console.log('Suksess simpan');
                
                                            },
                                            function(err) {
                                                console.log("err=>", err);
                                            }
                                        );
                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                );
                            }
                        })
                    }
                })
                
                
            }
        }

        $scope.clockOnAfterPause = function(JobId, ActiveChip, Model) {
            RepairProcessBP.CheckIsJobCancelWO(JobId).then(function (resultC) {
                if (resultC.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                    });
                } else {
                    $scope.clickBerlebih = true;
                    console.log('cek data', $scope.DataList);
                    var PoliceNumber = "";
                    var IdSa = "";

                    RepairProcessBP.getStatWO($scope.xDataWoList[0].JobId,$scope.xDataWoList[0].Status).then(function(result5){
                        if (result5.data == 1) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                            });
                            $scope.choseData(JobId);
                            $scope.close();
                            ngDialog.closeAll();
                            $scope.clickBerlebih = false;
                        } else {
                            RepairProcessBP.GetJobTasks(JobId).then(function (resultJ) {

                                var ada_job_dihapus = 0;
                                var ada_job_ditambah = 0;
                                if (resultJ.data.length > $scope.copy_dataTask.length){
                                    ada_job_ditambah++
                                }
                                for (var q=0; q<$scope.copy_dataTask.length; q++){
                                    for (var d=0; d<resultJ.data.length; d++){
                                        if ($scope.copy_dataTask[q].JobTaskId == resultJ.data[d].JobTaskId){
                                            if (resultJ.data[d].isDeleted == 1 && $scope.copy_dataTask[q].isDeleted == 0){
                                                // berarti ada yg di delete tp si foreman blm refresh
                                                ada_job_dihapus++
                                            }
                                        }
                                    }
                                }
        
                                if (ada_job_dihapus > 0 || ada_job_ditambah > 0){
                                    if (ada_job_ditambah > 0){
                                        bsNotify.show({
                                            size: 'big',
                                            type: 'danger',
                                            // title: "Ada penambahan pekerjaan, silahkan ploting chip pekerjaan baru terlebih dahulu.",
                                            title: "Terdapat penambahan pekerjaan baru oleh SA, konfirmasi PTM untuk melakukan ploting ulang",
        
                                        });
                                    }
        
                                    if (ada_job_dihapus > 0){
                                        bsNotify.show({
                                            size: 'big',
                                            type: 'danger',
                                            // title: "Ada pengurangan pekerjaan, silahkan refresh data terlebih dahulu.",
                                            title: "Terdapat pengurangan pekerjaan baru oleh SA, konfirmasi PTM untuk melakukan ploting ulang",
        
                                        });
                                    }
        
                                    $scope.clickBerlebih = false;
                                    ngDialog.closeAll();
                                    $scope.choseData(JobId);
                                    $scope.close();
                                    return
                                } else {
                                    // var JumlahChipClockOn = $scope.DataList[ActiveChip + '-' + 5].length + $scope.DataList[ActiveChip + '-' + 7].length;
                                    // console.log('JumlahChipClockOn', JumlahChipClockOn);
                        
                                    var JumlahChipClockOn = 0;
                                    // SA.JobTaskBp[0].Chip4GroupId !== mData.EmployeeId
                                    var posisiOldChip4 = 0;
                                    var posisiOldChip5 = 0; // buat cek ada yang clock on ga
                                    var posisiOldChip7 = 0; // buat cek ada yg clock after pause ga
                        
                                    //buat cari stall id chip yg active
                                    var stallIdActive = 0;
                                    angular.forEach($scope.DataList[ActiveChip + '-' + 4], function(val, k) {
                                        if (val.JobId == JobId) {
                                            for (var i = 0; i < val.JobTaskBp.length; i++) {
                                                if (val.JobTaskBp[i].isOldChip == 0) {
                                                    posisiOldChip4 = i;
                                                }
                                            }
                        
                                            stallIdActive = val.JobTaskBp[posisiOldChip4]['Chip' + ActiveChip + 'StallId'];
                                        }
                                    });
                        
                        
                                    if ($scope.DataList[ActiveChip + '-' + 5].length > 0) {
                                        angular.forEach($scope.DataList[ActiveChip + '-' + 5], function(value, key) {
                                            // this.push(key + ': ' + value);
                                            console.log('asem', value)
                                            for (var i = 0; i < value.JobTaskBp.length; i++) {
                                                if (value.JobTaskBp[i].isOldChip == 0) {
                                                    posisiOldChip5 = i;
                                                }
                                            }
                        
                                            var stallOnList = 0;
                                            stallOnList = value.JobTaskBp[posisiOldChip5]['Chip' + ActiveChip + 'StallId'];
                                            console.log('param 0 ', stallOnList);
                                            console.log('param 1', value.JobTaskBp[posisiOldChip5]['Chip' + ActiveChip + 'GroupId'])
                                            console.log('param 2', $scope.mData.EmployeeId)
                                            if (value.JobTaskBp[posisiOldChip5]['Chip' + ActiveChip + 'GroupId'] == $scope.mData.EmployeeId) {
                                                if (stallIdActive == stallOnList) {
                                                    JumlahChipClockOn = 1;
                                                }
                                            }
                        
                                        });
                                    }
                        
                                    if ($scope.DataList[ActiveChip + '-' + 7].length > 0) {
                                        angular.forEach($scope.DataList[ActiveChip + '-' + 7], function(value, key) {
                                            // this.push(key + ': ' + value);
                                            console.log('asem', value)
                                            for (var i = 0; i < value.JobTaskBp.length; i++) {
                                                if (value.JobTaskBp[i].isOldChip == 0) {
                                                    posisiOldChip7 = i;
                                                }
                                            }
                        
                                            PoliceNumber = $scope.DataList[ActiveChip + '-' + 7][0].PoliceNumber;
                                            IdSa = $scope.DataList[ActiveChip + '-' + 7][0].UserIdWORelease;
                        
                                            if (value.JobTaskBp[posisiOldChip7]['Chip' + ActiveChip + 'GroupId'] == $scope.mData.EmployeeId) {
                                                JumlahChipClockOn = 1;
                                            }
                        
                                        });
                                    }
                        
                                    console.log('JumlahChipClockOn', JumlahChipClockOn);
                        
                                    var pos6 = 0;
                                    var cekChipDispatchForClock = 0;
                                    var cekChipStallForClock = -2;
                                    if ($scope.DataList[ActiveChip + '-' + 6].length > 0) {
                                        angular.forEach($scope.DataList[ActiveChip + '-' + 6], function(value, key) {
                                            // this.push(key + ': ' + value);
                                            console.log('asem3', value)
                                            if (value.JobId == JobId) {
                                                for (var i = 0; i < value.JobTaskBp.length; i++) {
                                                    if (value.JobTaskBp[i].isOldChip == 0) {
                                                        pos6 = i;
                                                    }
                                                }
                                                $scope.groupBPid = value.JobTaskBp[pos6]['Chip' + ActiveChip + 'GroupId']
                        
                                            }
                        
                                            for (var i = 0; i < value.JobTaskBp.length; i++) {
                                                if (value.JobTaskBp[i].isOldChip == 0) {
                                                    // Chip1Status
                                                    cekChipDispatchForClock = value.JobTaskBp[i].isDispatched; // cek dl uda dispatch belum
                                                    cekChipStallForClock = value.JobTaskBp[i]['Chip' + ActiveChip + 'StallId']; //cek juga uda di plot belum
                                                    console.log('masuk ke cek status1', cekChipDispatchForClock)
                                                    console.log('masuk ke cek status2', value.JobTaskBp[i]['Chip' + ActiveChip + 'Status'])
                                                }
                                            }
                        
                                        });
                                    }
                                    console.log('group nya', $scope.groupBPid);
                        
                                    if (JumlahChipClockOn > 0) {
                                        bsNotify.show({
                                            size: 'big',
                                            type: 'danger',
                                            title: "Proses ini ada pekerjaan yang sedang berjalan."
                                        });
                                        $scope.clickBerlebih = false;
                                    } else if (cekChipDispatchForClock != 1 || cekChipStallForClock <= 0) {
                                        bsNotify.show({
                                            size: 'big',
                                            type: 'danger',
                                            title: "Proses belum dispatch / belum di plot, silahkan cek ulang"
                                        });
                                        $scope.clickBerlebih = false;
                                    } else {
                                        RepairProcessBP.putClock(3, JobId, ActiveChip, Model).then(
                                            function(res) {
                                                $scope.choseData(JobId);
                                                $scope.close();
                                                ngDialog.closeAll();
                                            },
                                            function(err) {
                                                console.log("err=>", err);
                                                $scope.clickBerlebih = false;
                                            }
                                        );
                                        // alert ('loh kok clock on after pause')
                                    }
                        
                                    //notif jika lanjut clockoff after pause tunggu part
                                    if (Model.PauseReasonId == 38) {
                                        //kirim notifikasi
                                        var DataNotif = {};
                                        var messagetemp = "";
                                        messagetemp = PoliceNumber;
                        
                        
                                        DataNotif.Message = messagetemp;
                                        RepairProcessGR.sendNotif(DataNotif, IdSa, 18).then(
                                            function(res) {
                        
                                            },
                                            function(err) {
                                                //console.log("err=>", err);
                                            });
                                    }
                                    //notif jika lanjut clockoff after pause approve customer            
                                    if ($scope.xDataWoList[0].PauseReasonId == 39) {
                                        //kirim notifikasi
                                        var DataNotif = {};
                                        var messagetemp = "";
                                        messagetemp = PoliceNumber;
                        
                        
                                        DataNotif.Message = messagetemp;
                                        RepairProcessGR.sendNotif(DataNotif, IdSa, 19).then(
                                            function(res) {
                        
                                            },
                                            function(err) {
                                                //console.log("err=>", err);
                                            });
                                    }
                                }
                            })
        
                        }
                    })

                    
        
                    
                }
            });
            

        }


        //Akhir Pause Area////////////////////////////////////////////////////////
        $scope.WoDetail = function(JobID) {
            $scope.arrTaskParts = [];
            RepairProcessGR.getDataDetail(JobID)
                .then(

                    function(res) {

                        $scope.DataWo = res.data.Result;
                        // console.log("$scope.DataWo", $scope.DataWo[0].Status);

                        // ===========================

                        for (var z in $scope.DataWo) {
                            for (var j in res.data.Result[z].JobTask) {
                                if (res.data.Result[z].JobTask[j].isDeleted == 0) {
                                    $scope.arrTaskParts.push(res.data.Result[z].JobTask[j]);
                                }
                            };
                            console.log("$scope.arrTaskParts==========>", $scope.arrTaskParts);
                        }

                        for (i = 0; i < $scope.DataWo.length; i++) {
                            $scope.DataWOTask = $scope.DataWo[i].JobTask;

                            for (a = 0; a < $scope.DataWOTask.length; a++) {
                                $scope.DataMaterial = $scope.DataWOTask[i].JobParts;
                                console.log("data wo parts", $scope.DataMaterial);
                            }
                            console.log("Data wo task", $scope.DataWOTask);
                        }
                        //console.log("data Wo Detail", $scope.DataWo);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        }


        $scope.show_modalTask = { show: false };
        $scope.modal_modelTask = [];
        $scope.modalModeTask = 'new';
        $scope.addPekerjaan = function() {
            $scope.show_modalTask = { show: true };
        }

        $scope.doAddPekerjaan = function(Data) {

            console.log("this is add perkerjaan", Data);
            Data.Ref = $scope.mData.PoliceNumber;
            // Data.NewMessage = "Mohon untuk tambah pekerjaan " + Data.Message + " dan Plotting Ulang Chip."
            RepairProcessBP.sendNotif(Data, $scope.IdSA, 13).then(
                function(res) {
                    console.log('Suksess simpan');

                    RepairProcessGR.updateFlagForAddTask($scope.JbId).then(
                        function(res) {
                            console.log('Suksess update');
                            $scope.show_modalTask = { show: false };
                            // $scope.getDataDetail($scope.JbId);
                            ngDialog.closeAll();
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );

                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }

        $scope.rescheduleCancel = function() {
            console.log('itemcancel');
        }

        $scope.show_modal2 = { show: false };
        $scope.modal_model = [];
        $scope.modalMode = 'new';
        $scope.addMaterial = function() {
            // console.log("data wo", dataDetailJob[0]);
            $scope.show_modal2 = { show: true };
        }


        $scope.doAddMaterial = function(Data) {
            // console.log("this is add Material");
            console.log("this is add Material", Data);
            Data.Ref = $scope.mData.PoliceNumber;

            // console.log("ModelData",ModelData);
            // Data.NewMessage = "Mohon untuk tambah parts " + Data.Message;
            RepairProcessBP.sendNotif(Data, $scope.IdSA, 14).then(
                function(res) {
                    console.log('Suksess simpan');

                    RepairProcessGR.updateFlagForAddTask($scope.JbId).then(
                        function(res) {
                            console.log('Suksess update');
                            $scope.show_modal2 = { show: false };
                            // $scope.getDataDetail($scope.JbId);
                            ngDialog.closeAll();
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );

                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }





        var Pausedata = [];
        $scope.choseData = function(status, jobId, dataJob, StallId, groupId, ChipProsesKe) {
            RepairProcessBP.GetStatusJobFI(jobId).then(function(resxxx){
                if (resxxx.data == 1){
                    // notif ada pekerjaan baru belum ploting / dispatch
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Ada pekerjaan yang belum ploting / dispatch"
                    });
                } else {
                    // boleh lanjut, karena uda ploting / dispatch.. tapi cek dl uda sama belum status nya
                    RepairProcessBP.getStatWO(jobId,status).then(function(resuxxx){
                        if(resuxxx.data == 1){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Ada pekerjaan baru silahkan refresh"
                            });
                            $scope.close()
                        } else {
                            $scope.doubleClickQCFinish = 0
                            copygridDefect = [];
                            $scope.mData.ChipIsPassQc = undefined;
                            $scope.clickBerlebih = false;
                            $scope.JbId = jobId;
                            console.log("groupId", groupId);
                            console.log("stallId", StallId);
                            console.log("Job Id", jobId);
                            console.log('datamodel cari chip ada di proses ke brp', ChipProsesKe)
                            $scope.showDetail = true;
                            $scope.xchoseData = jobId;
                            $scope.dataJob = dataJob;
                            $scope.pausearea = true;
                            // $scope.DataMaterial = dataJob.Material;

                            RepairProcessGR.getDataDetail(jobId)
                            .then(

                                function(res) {

                                    if (res.data.Result[0].ActiveChip != ChipProsesKe){
                                        bsNotify.show({
                                            size: 'big',
                                            type: 'danger',
                                            title: "Ada perubahan pada menu lain, silahkan refresh terlebih dahulu.",
                                        });
                                        ngDialog.closeAll();
                                        // $scope.choseData(JobId);
                                        $scope.close();
                                        return
                                    }
                                    $scope.xDataWoList = res.data.Result;

                                    $scope.copy_dataTask = angular.copy(res.data.Result[0].JobTask)

                                    // console.log("ale euy", $scope.xDataWoList[0].Status);
                                    $scope.DataWarranty = [];
                                    delete $scope.mData.OFP
                                    delete $scope.mData.Note
                                    delete $scope.mData.T2

                                    for (var i = 0; i < $scope.xDataWoList.length; i++) {
                                        for (var a = 0; a < $scope.xDataWoList[i].JobTask.length; a++) { //looping sebanyak berapa task
                
                                            if (($scope.xDataWoList[i].JobTask[a].JobTypeId == 59 || $scope.xDataWoList[i].JobTask[a].JobTypeId == 60) || $scope.xDataWoList[i].JobTask[a].PaidById == 30) {
                                                if ($scope.xDataWoList[i].JobTask[a].isDeleted == 0){
                                                    $scope.DataWarranty.push($scope.xDataWoList[i].JobTask[a]);
                                                }
                                                console.log("$scope.DataWarranty", $scope.DataWarranty);
                                            } else {
                                                var countParts = $scope.xDataWoList[i].JobTask[a].JobParts.length - 1;
                                                var flagPart = 0;
                                                _.map($scope.xDataWoList[i].JobTask[a].JobParts, function(ax, k) {
                                                    // console.log('Parts ><>', ax);
                                                    if (ax.PaidById == 30) {
                                                        flagPart = 1;
                
                                                        if (countParts == k) {
                                                            if (flagPart == 1) {
                                                                if ($scope.xDataWoList[i].JobTask[a].isDeleted == 0){
                                                                    $scope.DataWarranty.push($scope.xDataWoList[i].JobTask[a]);
                                                                }
                                                            };
                                                            
                                                        };
                                                    };
                                                    
                                                });
                                                
                                            }
                                        }
                                    }
                                    var xJobParts = res.data.Result;
                                    // $scope.Job = $scope.xDataWoList;
                                    for (var i = 0; i < xJobParts.length; i++) {
                                        for (var j = 0; j < xJobParts[i].JobTask.length; j++) {
                                            $scope.fJobParts = xJobParts[i].JobTask[j].JobParts;
                                        }
                                    }
                                    for (var i = 0; i < xJobParts.length; i++) {
                                        $scope.fJobTask = xJobParts[i].JobTask;
                                    }

                                    console.log("JobTask", $scope.fJobTask);
                                    console.log("JobParts", $scope.fJobParts);


                                    var Count = 0;
                                    var cc = [];
                                    for (i = 0; i < $scope.xDataWoList.length; i++) {
                                        var z = $scope.xDataWoList[i];
                                        console.log("Data Wo", z);
                                        for (j = 0; j < $scope.xDataWoList[i].JobTask.length; j++) {
                                            Count++
                                            if ($scope.xDataWoList[i].Status > 7) {
                                                cc.push($scope.xDataWoList[i].JobTask[j]);
                                                console.log("data JbTsk", cc.length);
                                                console.log("count", Count);
                                            } else {
                                                console.log("aghus", $scope.xDataWoList[i].Status);
                                            }

                                        }

                                        if (cc.length == Count || cc.Status != 11 || cc.Status != 12) {
                                            $scope.showFI = true;
                                            console.log("masuk if", cc.length);

                                            RepairProcessBP.getDataTeknisiById(jobId).then(
                                                function(res) {
                                                    $scope.DataTeknisi = res.data;
                                                    // di timpa name sama id nya pakai $scope.user karena permintaan ingin sesuai yg login
                                                    // $scope.DataTeknisi.EmployeeName = $scope.user.EmployeeName;
                                                    // $scope.DataTeknisi.EmployeeId = $scope.user.EmployeeId;
                                                    $scope.DataTeknisi = $scope.user;
                                                    console.log("Data DataTeknisi", $scope.DataTeknisi);
                                                },
                                                function(err) {
                                                    console.log("err=>", err);
                                                }
                                            );


                                            RepairProcessBP.getDataDefectByJobId(jobId, ChipProsesKe)
                                                .then(

                                                    function(res) {
                                                        $scope.gridDefectFi.data = res.data.Result;
                                                        console.log("Data Defect", $scope.gridDefectFi.data);
                                                    },
                                                    function(err) {
                                                        console.log("err=>", err);
                                                    }
                                                );

                                            RepairProcessGR.getFinalInspectionById(jobId).then(
                                                function(res) {
                                                    if (res.data.ResponseCode !== 44) {
                                                        $scope.dataSaveFi = res.data;

                                                        var dataJBTsk = [];
                                                        angular.forEach($scope.dataSaveFi.Job.JobTask, function(row) {
                                                            dataJBTsk.push(row);
                                                        });
                                                        $scope.DataFI = dataJBTsk;
                                                        console.log("Data FI", $scope.DataFI);
                                                    }

                                                },
                                                function(err) {
                                                    console.log("err=>", err);
                                                }
                                            );
                                            // console.log("masuk else", Count);
                                        } else {
                                            $scope.showFI = false;
                                            console.log("hasil else", cc.length);
                                            console.log("masuk else", Count);
                                        }

                                    }

                                    choosedatanext(status, jobId, dataJob, StallId, groupId, ChipProsesKe)

                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );

                            choosedatanext = function(status, jobId, dataJob, StallId, groupId, ChipProsesKe) {
                                $scope.getListRSA();

                                RepairProcessBP.getDataDetail(jobId)
                                    .then(

                                        function(res) {
                                            $scope.DataDetailModel = res.data;

                                            // ==================buat nampilin jam mulai sampai jam selesai===================
                                            var jobtaskbpid = -1;
                                            var dataDetailModelTerpilih = 0;
                                            for (var i = 0; i < $scope.DataDetailModel.length; i++) {
                                                console.log('ini masuk')
                                                if ($scope.DataDetailModel[i].JobTaskBpId > jobtaskbpid) {
                                                    jobtaskbpid = $scope.DataDetailModel[i].JobTaskBpId;
                                                    dataDetailModelTerpilih = i;
                                                }
                                            }
                                            var chipKePlanStart = 'Chip' + ChipProsesKe + 'PlanStart';
                                            var chipKePlanFinish = 'Chip' + ChipProsesKe + 'PlanFinish';
                                            var chipKeExtensionMinute = 'Chip' + ChipProsesKe + 'ExtensionMinute';
                                            var chipKeExtendDateFinish = 'Chip' + ChipProsesKe + 'ExtendDateFinish';
                                            $scope.JamMulaiBaseOnJPCB = $scope.DataDetailModel[dataDetailModelTerpilih][chipKePlanStart];
                                            if ($scope.DataDetailModel[dataDetailModelTerpilih][chipKeExtensionMinute] != null) {
                                                if ($scope.DataDetailModel[dataDetailModelTerpilih][chipKeExtensionMinute] > 0) {
                                                    $scope.JamSelesaiBaseOnJPCB = $scope.DataDetailModel[dataDetailModelTerpilih][chipKeExtendDateFinish];
                                                } else {
                                                    $scope.JamSelesaiBaseOnJPCB = $scope.DataDetailModel[dataDetailModelTerpilih][chipKePlanFinish];
                                                }

                                            } else {
                                                $scope.JamSelesaiBaseOnJPCB = $scope.DataDetailModel[dataDetailModelTerpilih][chipKePlanFinish];
                                            }
                                            console.log('jam mulai susah', $scope.JamMulaiBaseOnJPCB)
                                            console.log('jam selesai susah', $scope.JamSelesaiBaseOnJPCB)

                                            $scope.tampilanPlanStartDouble = 0
                                            if ($scope.JamMulaiBaseOnJPCB != null && $scope.JamMulaiBaseOnJPCB != undefined) {
                                                if ($scope.JamMulaiBaseOnJPCB.toString().includes(':')) {
                                                    $scope.tampilanPlanStartDouble = 1
                                                } else {
                                                    $scope.tampilanPlanStartDouble = 0
                                                }
                                            }

                                            //get data forman
                                            RepairProcessBP.getDataTeknisiById(jobId).then(
                                                function(res) {
                                                    $scope.DataTeknisi = res.data;
                                                    // di timpa name sama id nya pakai $scope.user karena permintaan ingin sesuai yg login
                                                    // $scope.DataTeknisi.EmployeeName = $scope.user.EmployeeName;
                                                    // $scope.DataTeknisi.EmployeeId = $scope.user.EmployeeId;
                                                    $scope.DataTeknisi = $scope.user;
                                                    console.log("Data DataTeknisi", $scope.DataTeknisi);
                                                },
                                                function(err) {
                                                    console.log("err=>", err);
                                                }
                                            );
                                            // ==================buat nampilin jam mulai sampai jam selesai===================


                                            console.log("Data Detail", $scope.DataDetailModel);

                                            // Plan Start
                                            var dateClock = new Date($scope.DataDetailModel[0].Chip1PlanStart);

                                            var dd1 = dateClock.getDate();
                                            var day1 = dd1 < 10 ? '0' + dd1 : dd1;
                                            var mm1 = dateClock.getMonth() + 1;
                                            var month1 = mm1 < 10 ? '0' + mm1 : mm1;
                                            var yyyy1 = dateClock.getFullYear();

                                            var hh1 = dateClock.getHours();
                                            var hour1 = hh1 < 10 ? '0' + hh1 : hh1;
                                            var mn1 = dateClock.getMinutes();
                                            var minute1 = mn1 < 10 ? '0' + mn1 : mn1;
                                            // var ss1 = dateClock.getSeconds();
                                            // var second1 = ss1 < 10 ? '0' + ss1 : ss1;

                                            // var realTime = hour1 + ":" + minute1 + ":" + second1;
                                            var realDateStart = day1 + "/" + month1 + "/" + yyyy1;
                                            var realTime = hour1 + ":" + minute1;
                                            $scope.RealTimeStart = realDateStart + " " + realTime;

                                            // Plan Finish
                                            var dateClock1 = new Date($scope.DataDetailModel[0].Chip7PlanFinish);

                                            var dd12 = dateClock1.getDate();
                                            var day12 = dd12 < 10 ? '0' + dd12 : dd12;
                                            var mm12 = dateClock1.getMonth() + 1;
                                            var month12 = mm12 < 10 ? '0' + mm12 : mm12;
                                            var yyyy12 = dateClock1.getFullYear();

                                            var hh12 = dateClock1.getHours();
                                            var hour12 = hh12 < 10 ? '0' + hh12 : hh12;
                                            var mn12 = dateClock1.getMinutes();
                                            var minute12 = mn12 < 10 ? '0' + mn12 : mn12;
                                            // var ss12 = dateClock1.getSeconds();
                                            // var second12 = ss12 < 10 ? '0' + ss12 : ss12;

                                            // var realTime2 = hour12 + ":" + minute12 + ":" + second12;
                                            var realDateStart2 = day12 + "/" + month12 + "/" + yyyy12;
                                            var realTime2 = hour12 + ":" + minute12;
                                            $scope.RealTimeFinish = realDateStart2 + " " + realTime2;

                                            //-janji penyerahan sementara kata pa ian plan finish tambah 30 mnt. 15/okt/2018n
                                            var JanjiPenyerahanSplit = $scope.RealTimeFinish.split(':');
                                            var JanjiPenyerahanJam = parseInt(JanjiPenyerahanSplit[0]);
                                            var JanjiPenyerahanMenit = parseInt(JanjiPenyerahanSplit[1]);

                                            JanjiPenyerahanMenit = JanjiPenyerahanMenit + 30;
                                            if (JanjiPenyerahanMenit == 60) {
                                                JanjiPenyerahanMenit = 0;
                                                JanjiPenyerahanJam = JanjiPenyerahanJam + 1;
                                            } else if (JanjiPenyerahanMenit > 60) {
                                                JanjiPenyerahanMenit = JanjiPenyerahanMenit - 60;
                                                JanjiPenyerahanJam = JanjiPenyerahanJam + 1;
                                            }

                                            var JanjiJam = JanjiPenyerahanJam < 10 ? '0' + JanjiPenyerahanJam : JanjiPenyerahanJam;
                                            var JanjiMenit = JanjiPenyerahanMenit < 10 ? '0' + JanjiPenyerahanMenit : JanjiPenyerahanMenit;

                                            // $scope.JanjiPenyerahan = JanjiJam.toString() + ':' + JanjiMenit.toString();

                                            console.log("StallId", $scope.DataDetailModel[0].Job.StallId);
                                            var datadetail = res.data[0];
                                            var dataRedo = [{
                                                    Redoid: 1,
                                                    Name: "Body"
                                                },
                                                {
                                                    Redoid: 2,
                                                    Name: "Putty"
                                                },
                                                {
                                                    Redoid: 3,
                                                    Name: "Surfacer"
                                                },
                                                {
                                                    Redoid: 4,
                                                    Name: "Spraying"
                                                },
                                                {
                                                    Redoid: 5,
                                                    Name: "Polishing"
                                                },
                                                {
                                                    Redoid: 6,
                                                    Name: "Re-Assembly"
                                                }
                                            ];

                                            var arr = [];
                                            _.map(dataRedo, function(n, key) {
                                                console.log("n inih", n, key);
                                                if (key == 0) {
                                                    if (datadetail.BodyEstimationMinute == null || datadetail.BodyEstimationMinute == 0) {
                                                        // console.log("nihhh BodyEstimationMinute" , n);
                                                        delete n;
                                                    } else {

                                                        if ($scope.DataDetailModel[0].Job.ActiveChip >= 1) {
                                                            arr.push(n);
                                                        }

                                                    }
                                                }
                                                if (key == 1) {
                                                    if (datadetail.PutyEstimationMinute == null || datadetail.PutyEstimationMinute == 0) {
                                                        // console.log("nihhh PutyEstimationMinute" , n);
                                                        delete n;
                                                    } else {
                                                        if ($scope.DataDetailModel[0].Job.ActiveChip >= 2) {
                                                            arr.push(n);
                                                        }
                                                    }
                                                }
                                                if (key == 2) {
                                                    if (datadetail.SurfacerEstimationMinute == null || datadetail.SurfacerEstimationMinute == 0) {
                                                        // console.log("nihhh SurfacerEstimationMinute" , n);
                                                        delete n;
                                                    } else {
                                                        if ($scope.DataDetailModel[0].Job.ActiveChip >= 3) {
                                                            arr.push(n);
                                                        }
                                                    }
                                                }
                                                if (key == 3) {
                                                    if (datadetail.SprayingEstimationMinute == null || datadetail.SprayingEstimationMinute == 0) {
                                                        // console.log("nihhh SprayingEstimationMinute" , n);
                                                        delete n;
                                                    } else {
                                                        if ($scope.DataDetailModel[0].Job.ActiveChip >= 4) {
                                                            arr.push(n);
                                                        }
                                                    }
                                                }
                                                if (key == 4) {
                                                    if (datadetail.PolishingEstimationMinute == null || datadetail.PolishingEstimationMinute == 0) {
                                                        // console.log("nihhh SurfacerEstimationMinute" , n);
                                                        delete n;
                                                    } else {
                                                        if ($scope.DataDetailModel[0].Job.ActiveChip >= 5) {
                                                            arr.push(n);
                                                        }
                                                    }
                                                }
                                                if (key == 5) {
                                                    if (datadetail.ReassemblyEstimationMinute == null || datadetail.ReassemblyEstimationMinute == 0) {
                                                        // console.log("nihhh SurfacerEstimationMinute" , n);
                                                        delete n;
                                                    } else {
                                                        if ($scope.DataDetailModel[0].Job.ActiveChip >= 6) {
                                                            arr.push(n);
                                                        }
                                                    }
                                                }
                                            });

                                            console.log("$scope.dataRedo", dataRedo);
                                            console.log("$scope.arr", arr);
                                            console.log("Data Detail X >>", $scope.DataDetailModel);

                                            $scope.ProcesRoDo = arr;

                                            RepairSupportActivityGR.getT1()
                                                .then(

                                                    function(res) {
                                                        $scope.T1 = res.data.Result;
                                                        console.log("T1", $scope.T1);
                                                        for (var i = 0; i < res.data.Result.length; i++) {
                                                            $scope.T1[i].xName = $scope.T1[i].T1Code + ' - ' + $scope.T1[i].Name;
                                                        }
                                                    },
                                                    function(err) {
                                                        console.log("err=>", err);
                                                    }
                                                );

                                            RepairSupportActivityGR.getT2()
                                                .then(

                                                    function(res) {
                                                        $scope.T2 = res.data.Result;
                                                        console.log("T2", $scope.T2);
                                                        for (var i = 0; i < res.data.Result.length; i++) {
                                                            $scope.T2[i].xName = $scope.T2[i].T2Code + ' - ' + $scope.T2[i].Name;
                                                        }
                                                    },
                                                    function(err) {
                                                        console.log("err=>", err);
                                                    }
                                                );

                                            $scope.IdSA = res.data[0].Job.UserIdWORelease;
                                            console.log('cari sa disini boy', res.data[0].Job.UserIdWORelease);


                                            RepairProcessGR.getVinFromCRM($scope.DataDetailModel[0].Job.VehicleId).then(
                                                function(res) {
                                                    Vin = res.data.Result;
                                                    $scope.VinData = Vin;
                                                    console.log("Vin", Vin);
                                                },
                                                function(err) {
                                                    console.log("err=>", err);
                                                }
                                            );


                                            RepairProcessGR.getTeknisiNote($scope.xchoseData).then(
                                                function(res) {
                                                    var TeknisiNote = res.data.Result;
                                                    console.log("Teknisi Note", TeknisiNote);
                                                    $scope.mData.TechnicianNotes = TeknisiNote[0].TechnicianNotes;
                                                },
                                                function(err) {
                                                    console.log("err=>", err);
                                                }
                                            );


                                            $scope.getDataPause();

                                            // RepairProcessGR.getDataWac($scope.xchoseData).then(
                                            //     function(res) {
                                            //         var dataWac = res.data;
                                            //         $scope.finalDataWac = dataWac;

                                            //         console.log("Data WAC", dataWac);
                                            //         $scope.mData.OtherDescription = dataWac.Other;
                                            //         $scope.mData.moneyAmount = dataWac.MoneyAmount;
                                            //         $scope.mData.BanSerep = dataWac.isSpareTire;
                                            //         $scope.mData.CdorKaset = dataWac.isCD;
                                            //         $scope.mData.Payung = dataWac.isUmbrella;
                                            //         $scope.mData.Dongkrak = dataWac.isJack;
                                            //         $scope.mData.STNK = dataWac.isVehicleRegistrationNumber;
                                            //         $scope.mData.P3k = dataWac.isP3k;
                                            //         $scope.mData.KunciSteer = dataWac.isSteerLock;
                                            //         $scope.mData.ToolSet = dataWac.isToolSet;
                                            //         $scope.mData.ClipKarpet = dataWac.isCarpetClip;
                                            //         $scope.mData.BukuService = dataWac.isServiceBook;
                                            //         $scope.mData.AlarmCondition = dataWac.isAlarmConditionOk;
                                            //         $scope.mData.AcCondition = dataWac.isAcOk;
                                            //         $scope.mData.PowerWindow = dataWac.isPowerWindowOk;
                                            //         $scope.mData.CarType = dataWac.isPowerWindowOk;
                                            //         // $scope.mData.TechnicianNotes = TeknisiNote[0].TechnicianNotes;
                                            //     },
                                            //     function(err) {
                                            //         console.log("err=>", err);
                                            //     }
                                            // );
                                            // $scope.dataForWAC(dataJob);



                                            RepairProcessGR.getJobSuggest($scope.xchoseData).then(
                                                function(res) {
                                                    $scope.GetDataJobSuggest = res.data.Result;
                                                    console.log("Data JObSUggest", $scope.GetDataJobSuggest);

                                                    // if($scope.GetDataJobSuggest[0].SuggestionCatg == null || $scope.GetDataJobSuggest[0].SuggestionCatg == undefined || $scope.GetDataJobSuggest[0].SuggestionCatg == 0){
                                                    //   $scope.mData.SuggestionCatg = 0;
                                                    // }else{
                                                    $scope.mData.SuggestionCatg = $scope.GetDataJobSuggest[0].SuggestionCatg;
                                                    // }
                                                    var co = ["CategoryJobSuggestKeamanan", "CategoryJobSuggestPeraturan", "CategoryJobSuggestKenyamanan", "CategoryJobSuggestEfisiensi"];

                                                    var total = $scope.mData.SuggestionCatg;
                                                    var arrBin = [1, 2, 4, 8];
                                                    for (var i in arrBin) {
                                                        if (total & arrBin[i]) {
                                                            var xc = co[i];
                                                            $scope.mData[xc] = arrBin[i];
                                                        }
                                                    }

                                                    if ($scope.GetDataJobSuggest[0].Suggestion == null) {
                                                        $scope.mData.Suggestion = "Tidak ada saran";
                                                    } else {
                                                        $scope.mData.Suggestion = $scope.GetDataJobSuggest[0].Suggestion;
                                                    }

                                                    // console.log("total", total);
                                                    // console.log("$scope.GetDataJobSuggest[0].Suggestion", $scope.GetDataJobSuggest[0].Suggestion);
                                                    // console.log("$scope.GetDataJobSuggest[0].SuggestionCatg", $scope.GetDataJobSuggest[0].SuggestionCatg);
                                                    // console.log("$scope.GetDataJobSuggest[0].SuggestionDate", $scope.GetDataJobSuggest[0].SuggestionDate);
                                                    console.log('psfu asem1', $scope.xchoseData)
                                                    console.log('psfu asem2', $scope.xDataWoList)
                                                        // AppointmentGrService.getDataVehiclePSFU($scope.xDataWoList[0].VehicleId).then(function(resu) {
                                                    AppointmentGrService.getDataVehiclePSFU($scope.DataDetailModel[0].Job.VehicleId).then(function(resu) {
                                                        var dataQuestionAnswerPSFU = resu.data.Result[0];
                                                        console.log("data Suggest dari CRM", dataQuestionAnswerPSFU);

                                                        // if (dataQuestionAnswerPSFU.JobSuggest == null) {
                                                        //     $scope.mData.Suggestion = "Tidak ada saran";
                                                        // } else {
                                                        //     $scope.mData.Suggestion = dataQuestionAnswerPSFU.JobSuggest;
                                                        // }
                                                        // $scope.mData.Suggestion = dataQuestionAnswerPSFU.JobSuggest;
                                                    });

                                                    // $scope.GetDataJobSuggest[0].SuggestionDate = null;

                                                    var tanggalSerba1 = $scope.GetDataJobSuggest[0].SuggestionDate.getDate() + '-' + $scope.GetDataJobSuggest[0].SuggestionDate.getMonth() + 1 + '-' + $scope.GetDataJobSuggest[0].SuggestionDate.getFullYear();
                                                    console.log('tgl serba 1', tanggalSerba1); //cek apa tanggal nya uda ke pernah di set apa blm, kalau belom pasti 1-jan-0001
                                                    if ($scope.GetDataJobSuggest[0].SuggestionDate == null ||
                                                        $scope.GetDataJobSuggest[0].SuggestionDate == undefined ||
                                                        $scope.GetDataJobSuggest[0].SuggestionDate == '' ||
                                                        $scope.GetDataJobSuggest[0].SuggestionDate == 'Invalid Date' ||
                                                        tanggalSerba1 == '1-01-1') {
                                                        $scope.GetDataJobSuggest[0].SuggestionDate = null;
                                                    }
                                                    $scope.mData.SuggestionDate = $scope.GetDataJobSuggest[0].SuggestionDate;

                                                    // if ($scope.GetDataJobSuggest[0].SuggestionDate == 'Mon Jan 01 0001 00:00:00 GMT+0700 (SE Asia Standard Time)' ||
                                                    //     $scope.GetDataJobSuggest[0].SuggestionDate == 'Mon Jan 01 0001 00:00:00 GMT+0707 (Western Indonesia Time)') {

                                                    //     var today = new Date();
                                                    //     var dd = today.getDate();
                                                    //     var mm = today.getMonth() + 1; //January is 0!
                                                    //     var yyyy = today.getFullYear();
                                                    //     today = yyyy + '/' + mm + '/' + dd;
                                                    //     $scope.mData.SuggestionDate = today;
                                                    //     console.log("tanggal", $scope.mData.SuggestionDate);
                                                    // } else {

                                                    //     $scope.mData.SuggestionDate = $scope.GetDataJobSuggest[0].SuggestionDate;
                                                    // }
                                                },
                                                function(err) {
                                                    console.log("err=>", err);
                                                }
                                            );

                                            



                                            //board
                                            var isBoardLoaded = false;
                                            $scope.dateOptions = {
                                                startingDay: 1,
                                                format: 'dd/MM/yyyy',
                                                // disableWeekend: 1
                                            };
                                            $scope.DateOptions = { // ini huruf gede kecil nya ngefek.. yg atas ga dihapus takut kepake
                                                startingDay: 1,
                                                format: 'dd/MM/yyyy',
                                                //disableWeekend: 1
                                            };

                                            var boardName = 'repairprocessbpboard';
                                            $scope.repairprocessbpboard = { currentDate: new Date() }
                                            $scope.repairprocessbpboard.startDate = new Date();

                                            var timeActive = true;
                                            var minuteTick = function() {
                                                if (!timeActive) {
                                                    return;
                                                }
                                                //if ()
                                                JpcbGrViewFactory.updateTimeLine(boardName);
                                                $timeout(function() {
                                                    minuteTick();
                                                }, 60000);
                                            }
                                            $scope.$on('$destroy', function() {
                                                timeActive = false;
                                            });

                                            var showBoardRepairProcessBp = function() {
                                                // if($scope.DataDetailModel[0].Job.StallId == undefined || $scope.DataDetailModel[0].Job.StallId == null){
                                                if (StallId == undefined || StallId == null) {
                                                    console.log("Stall id undefined", $scope.DataDetailModel[0].Job.StallId);
                                                } else {

                                                    var today2 = new Date();
                                                    var dd = today2.getDate().toString();
                                                    var mm = (today2.getMonth() + 1).toString(); //January is 0!
                                                    var yyyy = today2.getFullYear().toString();
                                                    today2 = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);

                                                    console.log("+$scope.DataDetailModel[0].Job.StallId+", $scope.DataDetailModel[0].Job.StallId);
                                                    console.log("+$scope.DataDetailModel xxxx+", $scope.DataDetailModel[0]['Chip' + ChipProsesKe + 'GroupId']);
                                                    $scope.groupidFac = $scope.DataDetailModel[0]['Chip' + ChipProsesKe + 'GroupId'];
                                                    var vitem = {
                                                        // mode: $scope.jpcbgrview.showMode,
                                                        currentDate: $scope.repairprocessbpboard.currentDate,
                                                        // stallUrl: '/api/as/Boards/Stall/'+$scope.xDataWoList[0].StallId+'',
                                                        // stallUrl: '/api/as/Stall/'+$scope.DataDetailModel[0].Job.StallId+'',
                                                        stallUrl: '/api/as/StallBP/' + StallId + '',
                                                        chipUrl: '/api/as/Boards/BP/JobList/' + $scope.xchoseData + '/' + groupId + '/' + today2 + '/' + today2,
                                                        // stallUrl: '/api/as/Boards/jpcb/bydate/'+$filter('date')($scope.repairprocessbpboard.currentDate, 'yyyy-MM-dd')+'/0/17',
                                                        nopol: $scope.mData.PoliceNumber,
                                                        groupid: $scope.DataDetailModel[0]['Chip' + ChipProsesKe + 'GroupId']
                                                            // tipe: $scope.jpcbgrview.tipe
                                                    }
                                                    console.log("showBoardRepairProcessBp");
                                                    JpcbGrViewFactory.showBoard(boardName, { container: 'repairprocessbpboard', currentChip: vitem });
                                                }
                                            }

                                            $scope.repairprocessbpboard.reloadBoard = function() {
                                                $timeout(function() {
                                                    var vobj = {
                                                        mode: $scope.repairprocessbpboard.showMode,
                                                        currentDate: $scope.repairprocessbpboard.currentDate,
                                                        nopol: $scope.repairprocessbpboard.nopol,
                                                        tipe: $scope.repairprocessbpboard.tipe
                                                    }
                                                    showBoardRepairProcessBp(vobj);

                                                    $timeout(function() {
                                                        minuteTick();
                                                    }, 100);

                                                }, 100);
                                            }

                                            var velement = document.getElementById('repairProcess_bp_panel');
                                            var erd = elementResizeDetectorMaker();
                                            erd.listenTo(velement, function(element) {
                                                if (!isBoardLoaded) {
                                                    isBoardLoaded = true;
                                                    showBoardRepairProcessBp();
                                                    minuteTick();
                                                    // untuk menampilkan timeline
                                                    // $timeout( function(){
                                                    //     minuteTick();
                                                    // }, 1000);
                                                }
                                                vboard = JpcbGrViewFactory.getBoard(boardName);
                                                if (vboard) {
                                                    vboard.autoResizeStage();
                                                    vboard.redraw();
                                                }

                                            });

                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                        }
                                    );
                                
                            }
                            
                        }
                    })
                }
            });
            
        }

        $scope.onTypeSuggest = function(item) {
            if (item == "") {
                //console.log('onTypeSuggest 2', item);
                $scope.mData.SuggestionDate = null;
                $scope.mData.CategoryJobSuggestKeamanan = 0;
                $scope.mData.CategoryJobSuggestPeraturan = 0;
                $scope.mData.CategoryJobSuggestKenyamanan = 0;
                $scope.mData.CategoryJobSuggestEfisiensi = 0;
            } else {
                //console.log('onTypeSuggest 3', item);
            }

        };

        $scope.closeData = function() {
            $scope.showDetail = false;
            $scope.show = false;
        }

        $scope.closeProblemFinding = function(textProblem) {
            $scope.showDetail = false;
            $scope.show = false;
            $scope.material = false;
            $scope.duration = false;

            textProblem.DescriptionProblem = "";
            textProblem.NamaParts = "";
            textProblem.QTY = "";
            textProblem.DeskripsiRespon = "";
            textProblem.T2ProblemFinding = "";
            textProblem.T1ProblemFinding = "";
            textProblem.Durasi = "";

            textProblem.AlertSa = false;
            textProblem.AlertPTH = false;
            textProblem.MaterialYa = false
            textProblem.NeedTime = false;
            textProblem.Confirm = false;
            textProblem.RSA = false;
        }

        $scope.TestDrive = function() {
            console.log("test drive", $scope.JbId);
            RepairProcessBP.CheckIsJobCancelWO($scope.JbId).then(function (resultC) {
                if (resultC.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                    });
                } else {
                    RepairProcessGR.TestDrive($scope.JbId).then(function(res) {
                            console.log("Test drive success");
                            if (res.data == -1) {
                                alert('Kendaraan Sedang TestDrive');
                            } else {
                                bsAlert.alert({
                                    title: "Test drive success ",
                                    text: "Success",
                                    type: "warning",
                                    showCancelButton: false
                                });
                                $scope.close();
                            };
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
                }
            });

            
        };

        // $scope.Group = [];
        $scope.getDataTeknisi = function() {

            RepairProcessBP.getDataTeknisi()
                .then(

                    function(res) {
                        $scope.EmployeeId = res.data.Result;
                        console.log("Data Teknisi", res.data);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        }



        var datajbTaskForPutFi = [];
        $scope.getFIById = function(status, JobId, dataJobTask) {
            RepairProcessBP.GetStatusJobFI(JobId).then(function(resxxx){
                if (resxxx.data == 1){
                    // notif ada pekerjaan baru belum ploting / dispatch
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Ada pekerjaan yang belum ploting / dispatch"
                    });
                } else {
                    // boleh lanjut, karena uda ploting / dispatch.. tapi cek dl uda sama belum status nya
                    RepairProcessBP.getStatWO(JobId,status).then(function(resuxxx){
                        if(resuxxx.data == 1){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Ada pekerjaan baru silahkan refresh"
                            });
                            $scope.close()
                        } else {
                            console.log("data job task", dataJobTask);
                            console.log("job id", JobId);
                            $scope.mData.RedoToChipKe = null;
                            $scope.mData.FreDoJob = null;
                            $scope.btnFI = true;

                            $scope.JbIdFi = JobId;
                            datajbTaskForPutFi = dataJobTask;
                            RepairProcessGR.getFinalInspectionById(JobId)
                                .then(

                                    function(res) {
                                        console.log("res ini", res.data);

                                        if (res.data.ResponseCode == 44) {
                                            RepairProcessGR.postFinalInspection(JobId).then(
                                                function(res) {

                                                    var dataJBTsk = [];
                                                    angular.forEach(dataJobTask, function(row) {
                                                        dataJBTsk.push(row);
                                                    });

                                                    RepairProcessGR.getFinalInspectionById(JobId).then(
                                                        function(res) {
                                                            $scope.DataFI = dataJBTsk;
                                                            $scope.dataSaveFi = res.data;
                                                        },
                                                        function(err) {
                                                            console.log("err=>", err);
                                                        }
                                                    );
                                                    console.log("succes insert");

                                                },
                                                function(err) {
                                                    console.log("err=>", err);
                                                }
                                            );
                                            console.log("masuk Post");
                                        } else {

                                            // buat update waktu clock on fi kalau sudah pernah ada
                                            RepairProcessBP.UpdateFIforBP(JobId).then(
                                                function(res) {
                                                    
                                                },
                                                function(err) {
                                                    console.log("err=>", err);
                                                }
                                            );

                                            if ($scope.xDataWoList[0].Revision != 0) {
                                                var dataJBTsk = [];
                                                angular.forEach(dataJobTask, function(row) {
                                                    // if ($scope.xDataWoList[0].Revision == row.Revision) {  // dicomment dl biar muncul aja lah semua job nya, ga usah yg defect doang hehe
                                                        dataJBTsk.push(row);
                                                        console.log("Model Yang Di Push nih", dataJBTsk);
                                                    // }
                                                });

                                                RepairProcessGR.getFinalInspectionById(JobId).then(
                                                    function(res) {
                                                        $scope.DataFI = dataJBTsk;
                                                        $scope.dataSaveFi = res.data;
                                                    },
                                                    function(err) {
                                                        console.log("err=>", err);
                                                    }
                                                );
                                            } else {
                                                var dataJBTsk = [];
                                                angular.forEach(dataJobTask, function(row) {
                                                    dataJBTsk.push(row);
                                                });
                                                RepairProcessBP.getDataTeknisiById(JobId).then(
                                                    function(res) {
                                                        $scope.DataTeknisi = res.data;
                                                        // di timpa name sama id nya pakai $scope.user karena permintaan ingin sesuai yg login
                                                        // $scope.DataTeknisi.EmployeeName = $scope.user.EmployeeName;
                                                        // $scope.DataTeknisi.EmployeeId = $scope.user.EmployeeId;
                                                        $scope.DataTeknisi = $scope.user;
                                                        console.log("Data DataTeknisi", $scope.DataTeknisi);
                                                    },
                                                    function(err) {
                                                        console.log("err=>", err);
                                                    }
                                                );

                                                RepairProcessGR.getFinalInspectionById(JobId).then(
                                                    function(res) {
                                                        $scope.DataFI = dataJBTsk;
                                                        $scope.dataSaveFi = res.data;
                                                        console.log("Data FI", $scope.DataFI);
                                                    },
                                                    function(err) {
                                                        console.log("err=>", err);
                                                    }
                                                );

                                            }
                                        }
                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                );
                        }
                    });
                }
            });
            
        }

        var JobTaskIdFI = [];
        $scope.ChoseDataFI = function(JobTaskId, $index) {
            console.log("JobTaskId", JobTaskId, "idx", $index);
            var a = JobTaskIdFI.indexOf(JobTaskId);
            if (a > -1) {
                JobTaskIdFI.splice(a, 1);
                console.log("nih di if 1", JobTaskIdFI);
            } else {
                JobTaskIdFI.push(JobTaskId);
                console.log("nih else", JobTaskIdFI);
            }
            if (JobTaskIdFI.length == 0) {
                $scope.btnFI = true;
            } else {
                $scope.btnFI = false;
            }
        }


        $scope.PutFI = function(Data, Model) {

            RepairProcessBP.CheckIsJobCancelWO($scope.JbId).then(function (resultC) {
                if (resultC.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",

                    })

                } else {
                    console.log("data", Data);
                    console.log("Model", Model);
                    console.log("jobtaskid", JobTaskIdFI);

                    if ($scope.DataWarranty.length != 0){
                        // $scope.kirimDataWarranty(Data, Model) // karena di redo
                    }

                    RepairProcessBP.putFinalInspection(Data, Model, JobTaskIdFI, $scope.DataTeknisi.EmployeeId).then(
                        function(res) {
                            // $scope.getFIById(Data.JobId);
                            var dataJBTsk = [];
                            // angular.forEach(dataJobTask,function(row){
                            //     dataJBTsk.push(row);
                            // });
                            angular.forEach(datajbTaskForPutFi, function(row) {
                                dataJBTsk.push(row);
                            });
                            RepairProcessGR.getFinalInspectionById(Data.Job.JobId).then(
                                function(res) {
                                    $scope.DataFI = dataJBTsk;
                                    $scope.dataSaveFi = res.data;
                                    $scope.close();
                                    console.log("Data FI", $scope.DataFI);
                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );

                            if (JobTaskIdFI.length > 0) {
                                //kirim notif juga di FI ada re do
                                var DataNotif = {};
                                var messagetemp = "";
                                messagetemp = Model.PoliceNumber;


                                DataNotif.Message = messagetemp;
                                RepairProcessBP.sendNotifRole(DataNotif, 1032, 31).then(
                                    function(res) {},
                                    function(err) {
                                        //console.log("err=>", err);
                                    });
                            }
                            // $scope.getDataDetail(Data.JobId);
                            JobTaskIdFI = [];
                            $scope.close();
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
                    RepairProcessGR.postJobSuggest($scope.xchoseData, Model).then(
                        function(res) {});

                }

            })
            
            
        }

        $scope.PutFILolos = function(Data, Model) {
            RepairProcessBP.CheckIsJobCancelWO($scope.JbId).then(function (resultC) {
                if (resultC.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                    });
                } else {
                    console.log("data", Data);
                    console.log("Model", Model);
                    console.log("jobtaskid", JobTaskIdFI);
                    console.log("$scope.DataDetailModel", $scope.DataDetailModel);
                    // $scope.PrintProductionBpQC(Data.JobId);

                    if ($scope.DataWarranty.length != 0){
                        $scope.kirimDataWarranty(Data, Model)
                    } else {
                        RepairProcessBP.putFinalInspectionLolos(Data, Model, JobTaskIdFI, $scope.DataDetailModel, $scope.DataTeknisi.EmployeeId).then(
                            function(res) {
                                // $scope.getFIById(Data.JobId);
                                $scope.close();
                                // var dataJBTsk = [];
                                // angular.forEach(dataJobTask,function(row){
                                //     dataJBTsk.push(row);
                                // });
                                // RepairProcessGR.getFinalInspectionById(JobId).then(
                                //     function(res) {
                                //         $scope.DataFI = dataJBTsk;
                                //         $scope.dataSaveFi = res.data;
                                //       console.log("Data FI", $scope.DataFI);
                                //     },
                                //     function(err) {
                                //         console.log("err=>", err);
                                //     }
                                // );
            
                                // $scope.getDataDetail(Data.JobId);
                                // JobTaskIdFI = [];
            
                                $scope.PrintProductionBpQC(Data.JobId);
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
            
                        RepairProcessGR.postJobSuggest($scope.xchoseData, Model).then(
                            function(res) {});
                    }
        
                    
                }
            });
            
        }

        $scope.kirimDataWarranty = function (Data, Model){

            var ol = [];
            var ol2 = [];
            var countMistake = 0;
            var countMistakeOFP = 0;
            //console.log("ol", ol.length);
            //console.log("$scope.DataWarranty.length", $scope.DataWarranty.length);
            if(Model.T2 == undefined || Model.OFP == undefined || Model.Note == undefined){
                bsNotify.show({
                    title: 'Mohon isi OFP(minimal 10 karakter), T2 beserta keterangannya',
                    text: 'I will close in 2 seconds.',
                    size: 'big',
                    timeout: 2000,
                    type: 'danger'
                }); 
            }else{
                ol = Object.keys(Model.Note);
                ol2 = Object.keys(Model.OFP);
                ol3 = Object.keys(Model.T2);
                for(var xNote in ol){
                    console.log("Model[ol[xNote]] ==>",Model.Note[ol[xNote]],xNote,ol);
                    if(Model.Note[ol[xNote]] == '' || Model.Note[ol[xNote]] == null){
                        countMistake += 1;
                    }
                }
                for(var xOfp in ol2){
                    console.log("Model[ol2[xOfp]] ==>",Model.OFP[ol2[xOfp]],xOfp,ol2);
                    if(Model.OFP[ol2[xOfp]] == '' || Model.OFP[ol2[xOfp]] == null || Model.OFP[ol2[xOfp]] == undefined){
                        countMistake += 1;
                    } else {
                        var tes = Model.OFP[ol2[xOfp]]
                        if (Model.OFP[ol2[xOfp]].includes('-') || Model.OFP[ol2[xOfp]].includes(' ')){
                            var tmp = Model.OFP[ol2[xOfp]].replace(/ |-/g, "");
                            Model.OFP[ol2[xOfp]] = tmp;
                        }
                        if(Model.OFP[ol2[xOfp]].length < 10 ){
                            countMistakeOFP += 1;
                        }
                    }
                }
                for(var xt2 in ol3){
                    console.log("Model[ol3[xt2]] ==>",Model.T2[ol3[xt2]],xt2,ol3);
                    if(Model.T2[ol3[xt2]] == '' || Model.T2[ol3[xt2]] == null){
                        countMistake += 1;
                    }
                }
            }
            console.log("salah anjay=====>",countMistake);
            if(countMistake > 0){
                bsNotify.show({
                    title: 'Mohon isi OFP, T2 beserta keterangannya',
                    text: 'I will close in 2 seconds.',
                    size: 'big',
                    timeout: 2000,
                    type: 'danger'
                }); 
                return false;
            }
            if(countMistakeOFP > 0){
                bsNotify.show({
                    title: 'Kode OFP minimal 10 karakter (Hanya angka dan huruf)',
                    text: 'I will close in 2 seconds.',
                    size: 'big',
                    // timeout: 2000,
                    type: 'danger'
                }); 
                return false;
            }
            var FinalSimpan = [];

            
            var dataTerpilihT2 = [];
            for (var i=0; i<$scope.T2.length; i++){
                for (var j=0; j<ol3.length; j++){
                    if ($scope.T2[i].xName == Model.T2[j]){
                        dataTerpilihT2.push($scope.T2[i]);
                    }
                }
            }
            for (var c in Model.Note) {
                FinalSimpan.push({
                    JobTaskId: $scope.DataWarranty[c].JobTaskId,
                    // T2: DataT2Save[c].T2Code,
                    // T2: dataTerpilihT2[c].T2Code,
                    T2: Model.T2[c],
                    notes: Model.Note[c],
                    OFP : Model.OFP[c]
                });

            }

            if ($scope.DataWarranty.length == ol.length && $scope.DataWarranty.length == ol2.length) {
                    
                for (var i=0; i<FinalSimpan.length; i++){
                    FinalSimpan[i].notes = FinalSimpan[i].notes.toString().toUpperCase();
                    FinalSimpan[i].OFP = FinalSimpan[i].OFP.toString().toUpperCase().replace(/[^a-zA-Z0-9 ]/g, '');
                }

                var obj = {}
                var data_obj = []

                for (var i=0; i<FinalSimpan.length; i++){
                    obj = {}

                    obj.OFP = angular.copy(FinalSimpan[i].OFP)
                    data_obj.push(obj)

                }

                RepairProcessGR.CekOFP(data_obj).then(function(res12) {
                    var hasel = '';
                    var yangmana = '';

                    if (res12.data.includes('#')) {
                        hasel = res12.data.split('#')[0]
                        yangmana = res12.data.split('#')[1]
                    } else {
                        hasel = res12.data
                    }

                    if (hasel == '666') {
                        bsNotify.show({
                            title: 'Kode Part yang di input pada kolom OFP tidak sesuai dengan master Part TOYOTA, silahkan input kode part yang benar & tepat.',
                            text: '',
                            size: 'big',
                            type: 'danger'
                        });

                    } else if (hasel == '1') {

                        RepairProcessGR.putT2(FinalSimpan).then(function(res) {
                            console.log('res t2 put', res)



                            RepairProcessBP.putFinalInspectionLolos(Data, Model, JobTaskIdFI, $scope.DataDetailModel, $scope.DataTeknisi.EmployeeId).then(
                                function(res) {
                                    // $scope.getFIById(Data.JobId);
                                    $scope.close();
                                    // var dataJBTsk = [];
                                    // angular.forEach(dataJobTask,function(row){
                                    //     dataJBTsk.push(row);
                                    // });
                                    // RepairProcessGR.getFinalInspectionById(JobId).then(
                                    //     function(res) {
                                    //         $scope.DataFI = dataJBTsk;
                                    //         $scope.dataSaveFi = res.data;
                                    //       console.log("Data FI", $scope.DataFI);
                                    //     },
                                    //     function(err) {
                                    //         console.log("err=>", err);
                                    //     }
                                    // );
                
                                    // $scope.getDataDetail(Data.JobId);
                                    // JobTaskIdFI = [];
                
                                    $scope.PrintProductionBpQC(Data.JobId);
                                },
                                function(err) {
                                    console.log("err=>", err);
                                }
                            );
                
                            RepairProcessGR.postJobSuggest($scope.xchoseData, Model).then(
                                function(res) {});
                        })

                    }

                })

                

            } else {
                bsNotify.show({
                    title: 'Mohon isi T2 beserta keterangannya',
                    text: 'I will close in 2 seconds.',
                    size: 'big',
                    timeout: 2000,
                    type: 'danger'
                });
            }

        }

        $scope.PrintProductionBpQC = function(jobid) {
            var cetak = 'as/PrintProductionBpQC/' + jobid + '/' + $scope.user.OrgId + '/' + 0;
            $scope.cetakan(cetak);
        };
        $scope.cetakan = function(data) {
            var pdfFile = null;

            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame 
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    } else {
                        printJS(pdfFile);
                    }
                } else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };



        $scope.Batal = function(model, Type) {
            if (Type == 1) {
                model.JobSuggest = "";
                model.CategoryJobSuggestKeamanan = false;
                model.CategoryJobSuggestPeraturan = false;
                model.CategoryJobSuggestKenyamanan = false;
                model.CategoryJobSuggestEfisiensi = false;
                model.NextJob = "";
            } else if (Type == 2) {
                $scope.showPause = false;
                model.DeskripsiRespon = "";
                model.T2 = "";
                model.T1 = "";
                model.Deskripsi = "";
                model.Durasi = "";
                $scope.duration = false;
                model.NeedTime = false;
                model.Question = false;
                model.ReqRSA = false;
                model.KeteranganPause = "";
                model.PauseResaon = "";
            } else if (Type == 3) {
                model.DeskripsiRespon = "";
                model.T2 = "";
                model.T1 = "";
                model.Deskripsi = "";
                model.Durasi = "";
                $scope.duration = false;
                model.NeedTime = false;
                model.Question = false;
                model.ReqRSA = false;
                model.KeteranganPause = "";
                model.PauseResaon = "";
            }
        }

        $scope.close = function() {
            $scope.showPetugasQc = false;
            $scope.showDetail = false;
            $scope.show = false;
            // $scope.getDataDefault();

            console.log("NO police", NopoliceF);
            console.log("Nowo", NoWof);
            console.log("groupId", groupId);
            $scope.getData(NopoliceF, NoWof, groupId);
            // $scope.mData = [];

            $scope.DataDetailModel[0].Job.StallId = null;
            $scope.mData.CategoryJobSuggestKeamanan = false;
            $scope.mData.CategoryJobSuggestPeraturan = false;
            $scope.mData.CategoryJobSuggestKenyamanan = false;
            $scope.mData.CategoryJobSuggestEfisiensi = false;
            $scope.showPause = false;
            //Agar Tombol Lolos QC & Tombol Tidak Lolos QC kembali tidak berwarna ketika ditekan batal
            $scope.ButtonLolosQC = false;
            $scope.ButtonTidakLolosQC = false;

        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }

        //----------------------------------
        // Bagian Problem Finding Material
        // nama grid =  $scope.gridOption;
        //----------------------------------

        var res = [{
            KodeParts: 1,
            NamaParts: 'Body',
            QTY: 20
        }];

        var gridData = [];

        var customCellTemplate = '<div><select style="height:40px;" ui-grid-edit-dropdown ng-model=\"MODEL_COL_FIELD\" ng-change="grid.appScope.onDropdownChange(row.entity)" ng-options=\"field[editDropdownIdLabel] as field[editDropdownValueLabel] CUSTOM_FILTERS for field in editDropdownOptionsArray\" style="height:30px;"></select></div>';

        // added by sss on 2017-10-10

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: false,
            // added by sss on 2017-10-11
            // rowHeight: 30,
            //
            // multiSelect: true,
            // enableSelectAll: true,
            rowHeight: 40,
            enableCellEdit: true,
            enableCellEditOnFocus: true,
            // showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'JobWACExtId', field: 'JobWACExtId', width: '7%', visible: false },
                { name: 'Nama Item', field: 'ItemName', enableCellEdit: false },
                {
                    name: 'Status Kerusakan',
                    // width: '15%',
                    field: 'Status',
                    // editableCellTemplate: 'ui-grid/dropdownEditor',
                    editableCellTemplate: customCellTemplate,
                    editDropdownValueLabel: 'Name',
                    editDropdownIdLabel: 'Id',
                    editDropdownOptionsArray: $scope.DamageVehicle,
                    cellFilter: "griddropdownStat:this"
                },
            ]
        };

        $scope.getDataMaterial = function() {
            $scope.gridOptions.data = res;

            // $scope.grid.data = res;
            // // console.log("nih", $scope.VM );
            // Towing.getData()
            //   .then(

            //     function(res) {
            //       gridData = [];
            //       //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
            //       $scope.grid.data = res.data;
            //       // console.log("role=>", res.data.Result);
            //       //console.log("grid data=>",$scope.grid.data);
            //       //$scope.roleData = res.data;
            //       $scope.loading = false;
            //       console.log("test",res);
            //     },
            //     function(err) {
            //       console.log("err=>", err);
            //     }
            //   );
        }

        $scope.addMaterialList = function(DataMaterial) {
            // console.log("mData.NamaParts",DataMaterial);
            // if(DataMaterial.NamaParts == '' || DataMaterial.QTY == ''){
            //     var error = [];
            //     if(DataMaterial.NamaParts == ''){error.push("Nama Parts kosong");};
            //     if(DataMaterial.QTY == ''){error.push("QTY kosong");};
            //     bsNotify.show(
            //         {
            //             size: 'big',
            //             type: 'danger',
            //             title: "Nama Tidak Boleh kosong",
            //             content: error.join('<br>'),
            //             number: error.length
            //         }
            //     );
            // }else{
            var xDataMaterial = DataMaterial;

            var n = $scope.gridOptions.data.length + 1;

            res.push({
                "KodeParts": n,
                "NamaParts": xDataMaterial.NamaParts,
                "QTY": xDataMaterial.QTY
            });
            DataMaterial.NamaParts = "";
            DataMaterial.QTY = "";
            // }

        };

        $scope.deleteMaterial = function() {
            console.log("in yang harus didelete");
            // $scope.gridOptions.data.splice($scope.selectedRows.KodeParts,$scope.selectedRows.length);
        }

        //----------------------------------
        // Akhir Bagian Problem Finding Material
        //----------------------------------
        var mstId = null;
        $scope.filter = { vType: null };
        $scope.areasArray = [];

        GeneralMaster.getData(2030).then(
            function(res) {
                $scope.VTypeData = res.data.Result;
                console.log("VTypeData", $scope.VTypeData);
                // console.log("$scope.UomData : ",$scope.UomData);
                return res.data;
            }
        );

        RepairProcessGR.getAllTypePoints().then(
            function(res) {
                // console.log(res);
                $scope.imageWACArr = res.data.Result;
                console.log("$scope.imageWACArr : ", $scope.imageWACArr);
                $scope.loading = false;
                return res.data;
            },
            function(err) {
                console.log("err=>", err);
            }
        );
        $scope.statusObj = { status: 1 };
        var dotsData = { 2221: [], 2222: [], 2223: [], 2224: [], 2225: [], 2226: [], 2227: [] };
        $scope.imageSel = null;

        function getImgAndPoints(mstId) {
            switch (mstId) {
                case 2221:
                    $scope.imageSel = "../images/wacImages/Type A.png";
                    break;
                case 2222:
                    $scope.imageSel = "../images/wacImages/Type B.png";
                    break;
                case 2223:
                    $scope.imageSel = "../images/wacImages/Type C.png";
                    break;
                case 2224:
                    $scope.imageSel = "../images/wacImages/Type D.png";
                    break;
                case 2225:
                    $scope.imageSel = "../images/wacImages/Type E.png";
                    break;
                case 2226:
                    $scope.imageSel = "../images/wacImages/Type F.png";
                    break;
                case 2227:
                    $scope.imageSel = "../images/wacImages/Type G.png";
            }
            var temp = _.find($scope.imageWACArr, function(o) {
                return o.TypeId == mstId;
            });

            return JSON.parse(temp.Points);
        };
        $scope.clickWACcount = 0;

        function setDataGrid(data) {
            console.log('data setDataGrid', data);
            $scope.grid.data = angular.copy(data);
        }
        $scope.chgChk = function() {
            if (!$scope.disableWACExt) {
                // console.log("false check..");
                // console.log("mstId : ",mstId);
                // var chAreas = document.getElementsByClassName('choosen-area');
                // for (var i in chAreas) {
                //     console.log(chAreas[i]);
                //     if (typeof chAreas[i].classList !== 'undefined') {
                //         chAreas[i].classList.remove('choosen-area');
                //     }
                // }
                $('.choosen-area').removeClass('choosen-area');
                $scope.grid.data = [];
                dotsData[mstId] = [];
            }
            $scope.disableWACExt = !$scope.disableWACExt;
        }
        $scope.dataForWAC = function(data) {
            $('#layoutWac').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
            console.log("======================>", data);
            if ($scope.clickWACcount == 0) {
                $scope.getWac();
                $scope.refreshSlider();

                RepairProcessGR.getDataWac(data.JobId).then(function(resu) {
                    var dataWAC = resu.data;
                    console.log('dataWAC', dataWAC);
                    $scope.dataItemWACBP = dataWAC;
                    $scope.mDataforWac = {};
                    $scope.mDataforWac.OtherDescription = dataWAC.Other;
                    $scope.mDataforWac.moneyAmount = dataWAC.MoneyAmount;
                    $scope.mDataforWac.BanSerep = dataWAC.isSpareTire;
                    $scope.mDataforWac.CdorKaset = dataWAC.isCD;
                    $scope.mDataforWac.Payung = dataWAC.isUmbrella;
                    $scope.mDataforWac.Dongkrak = dataWAC.isJack;
                    $scope.mDataforWac.STNK = dataWAC.isVehicleRegistrationNumber;
                    $scope.mDataforWac.P3k = dataWAC.isP3k;
                    $scope.mDataforWac.KunciSteer = dataWAC.isSteerLock;
                    $scope.mDataforWac.ToolSet = dataWAC.isToolSet;
                    $scope.mDataforWac.ClipKarpet = dataWAC.isCarpetClip;
                    $scope.mDataforWac.BukuService = dataWAC.isServiceBook;
                    $scope.mDataforWac.AlarmCondition = dataWAC.isAlarmConditionOk;
                    $scope.mDataforWac.AcCondition = dataWAC.isAcOk;
                    $scope.mDataforWac.PowerWindow = dataWAC.isPowerWindowOk;
                    $scope.mDataforWac.CarType = dataWAC.isPowerWindowOk;
                    $scope.mDataforWac.Fuel = dataWAC.Fuel;
                    $scope.mDataforWac.Km = data.Km
                        // $scope.mData.MoneyAmount = dataWAC.MoneyAmount;
                    $scope.slider.value = dataWAC.Fuel;
                });

                RepairProcessGR.getProbPoints(data.JobId, null).then(
                    function(res) {
                        $scope.areasArray = [];
                        $scope.areasArray.splice();
                        console.log("RepairProcessGR.getProbPoints", res.data);

                        if (res.data.Result.length > 0) {
                            mstId = angular.copy(res.data.Result[0].TypeId);
                            console.log("ini MstId", mstId);
                            $scope.filter.vType = mstId;
                            console.log("probPoints", res.data.Result);
                            console.log("$scope.areasArray1", $scope.areasArray);
                            $scope.areasArray = getImgAndPoints(mstId);
                            console.log("$scope.areasArray2", $scope.areasArray);
                            dotsData[mstId] = [];
                            for (var i in res.data.Result) {
                                var getTemp = [];
                                getTemp = JSON.parse(res.data.Result[i]["Points"]);
                                var xtemp = {};
                                console.log('gettemp', getTemp);

                                for (var j in getTemp) {
                                    console.log('gettempJstat', getTemp[j]);
                                    var xtemp = {};
                                    xtemp = angular.copy(getTemp[j]);
                                    delete xtemp['status'];
                                    console.log("xtemp : ", xtemp);
                                    console.log("scope.areasarray3 : ", $scope.areasArray);
                                    if (typeof _.find($scope.areasArray, xtemp) !== 'undefined') {
                                        console.log("merah");
                                        console.log("find..", xtemp);
                                        console.log("scope.areasarray4 : ", $scope.areasArray);
                                        var idx = _.findIndex($scope.areasArray, xtemp);
                                        console.log('idx', idx);
                                        xtemp.cssClass = "choosen-area";
                                        $scope.areasArray[idx] = xtemp;

                                        dotsData[mstId].push({
                                            "ItemId": xtemp.areaid,
                                            "ItemName": xtemp.name,
                                            "Points": xtemp,
                                            "Status": getTemp[j].status
                                        });
                                    }
                                }

                            }
                            setDataGrid(dotsData[mstId]);
                        }
                    },
                    function(err) {
                        $scope.areasArray = getImgAndPoints(mstId);
                    }
                );
                $scope.clickWACcount++;
            } else {
                console.log("di click mulu, rese but sih luuuuuuuuuuuuuuuuu");
            }

            console.log("$scope.mDataforWac =================>", $scope.mDataforWac);
        }
        $scope.chooseVType = function(selected) {
            // dotsData = {2221:[],2222:[],2223:[],2224:[],2225:[],2226:[],2227:[]};
            // mstId = selected.MasterId;
            mstId = selected;
            WOBP.getDamage().then(function(res) {
                _.map(res.data.Result, function(val) {
                    val.Id = val.MasterId;
                });
                $scope.grid.columnDefs[2].editDropdownOptionsArray = res.data.Result;
            });
            $scope.grid.data = angular.copy(dotsData[mstId]);

            WO.getProbPoints(1221, mstId).then(
                function(res) {
                    $scope.areasArray = getImgAndPoints(mstId);
                    console.log("$scope.areasArray", $scope.areasArray);
                    if (res.data.Result.length > 0) {
                        for (var i in res.data.Result) {
                            var getTemp = JSON.parse(res.data.Result[i]["Points"]);
                            for (var j in getTemp) {
                                var xtemp = angular.copy(getTemp[j]);
                                delete xtemp['status'];
                                console.log("xtemp : ", xtemp);
                                if (typeof _.find($scope.areasArray, xtemp) !== 'undefined') {
                                    console.log("find..", xtemp);
                                    var idx = _.findIndex($scope.areasArray, xtemp);
                                    xtemp.cssClass = "choosen-area";
                                    $scope.areasArray[idx] = xtemp;

                                    dotsData[mstId].push({ "ItemId": xtemp.areaid, "ItemName": xtemp.name, "Points": xtemp, "Status": getTemp[j].status });
                                }
                            }
                        }
                        setDataGrid(dotsData[mstId]);
                    }
                },
                function(err) {
                    $scope.areasArray = getImgAndPoints(mstId);
                }
            );
        };

        var sel;
        $scope.doClick = function(ev, boxId, areas, area, stat, selection) {
            // console.log(boxId);
            // console.log(dotsData[mstId]);
            var temp = null;
            for (var i in dotsData[mstId]) {
                if (dotsData[mstId][i].Points) {
                    if (dotsData[mstId][i].Points.areaid == boxId.areaid) {
                        temp = dotsData[mstId][i].Status;
                    }
                }
            }
            console.log("temp : ", temp);
            if (selection.hasClass("choosen-area")) {
                if (sel != undefined) {
                    sel.empty();
                }
                $name = $(selection).empty().append($("<div><span class=\"select-area-field-label border-thin name-area-id-" + area.areaid + "\">" + (temp == 1 ? "Penyok" : (temp == 2 ? "Baret" : "Rusak")) + "</span></div>"));
                sel = selection;
            }
        };
        //----------------------------------
        // Bagian QC Defect List
        // nama grid = $scope.gridDefect
        //----------------------------------

        // var DataDefectTrue = [
        //     {
        //         IdDefect : 1,
        //         NameDefect : "Seeds",
        //         JumlahDefect : "11"
        //     },
        //     {
        //         IdDefect : 2,
        //         NameDefect : "Pin Hole",
        //         JumlahDefect : "5"
        //     }
        // ];

        var JobTaskIdQC = []

        $scope.ChoseDataQC = function(JobTaskId, $index) {
                console.log("JobTaskId", JobTaskId, "idx", $index);
                var a = JobTaskIdQC.indexOf(JobTaskId);
                if (a > -1) {
                    JobTaskIdQC.splice(a, 1);
                    console.log("nih di if 1", JobTaskIdQC);
                } else {
                    JobTaskIdQC.push(JobTaskId);
                    console.log("nih else", JobTaskIdQC);
                }
                $scope.dataPekerjaanDefect = [];
                for (var i=0; i<JobTaskIdQC.length; i++){
                    var objPekerjaan = {};
                    for (var j=0; j<$scope.DataFI.length; j++){
                        if ($scope.DataFI[j].JobTaskId == JobTaskIdQC[i]){
                            objPekerjaan.Id = $scope.DataFI[j].JobTaskId;
                            objPekerjaan.Name = $scope.DataFI[j].TaskName;
                            $scope.dataPekerjaanDefect.push(objPekerjaan);
                        }
                    }
                    
                }
                if (JobTaskIdQC.length == 0) {
                    $scope.btnQC = true;
                } else {
                    $scope.btnQC = false;
                }
            }
            // $scope.ChoseDataQC = function(JobTaskId){
            //   // console.log("JobTaskId", JobTaskId);
            //   // // if(parseInt(JobTaskId) !=0 && parseInt(JobTaskId) !=-1){
            //   //   if(JobTaskId != null ||JobTaskId != false || JobTaskId != true){
            //   //     JobTaskIdQC.push(JobTaskId);
            //   //     console.log("job tas niiih bukan 0 dan bukan -1", JobTaskIdQC);
            //   //   }
            // }

        $scope.simpanQC = function(Model, JobId, ActiveChip) {
            RepairProcessBP.CheckIsJobCancelWO(JobId).then(function (resultC) {
                if (resultC.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                    });
                } else {
                    // RepairProcessBP.getStatWO($scope.xDataWoList[0].JobId,$scope.xDataWoList[0].Status).then(function(result5){
                    RepairProcessBP.getStatWO($scope.xDataWoList[0].JobId,9).then(function(result5){ // di kasi 9 karena kl dia mau qc dia click tab qc yang uda pasti update jadi 9

                        if (result5.data == 1) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                            });
                            $scope.choseData(JobId);
                            $scope.close();
                            ngDialog.closeAll();
                            $scope.clickBerlebih = false;
                        } else {
                            console.log('arr cari redo kemana', $scope.ProcesRoDo);
                            console.log("hahahha", JobId);
                            console.log("model", Model);
                            if ((Model.RedoToChipKe == undefined || Model.RedoToChipKe == "" || Model.RedoToChipKe == null) || (Model.ChipQcTechnicianId == undefined || Model.ChipQcTechnicianId == "" || Model.ChipQcTechnicianId == null) || (JobTaskIdQC.length == 0) || ($scope.gridDefect.data.length == 0)) {
                                if (Model.RedoToChipKe == undefined || Model.RedoToChipKe == "" || Model.RedoToChipKe == null){
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Input Data 'Re Do Ke Proses'"
                                    });
                                }
                                if (Model.ChipQcTechnicianId == undefined || Model.ChipQcTechnicianId == "" || Model.ChipQcTechnicianId == null){
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Input Data 'Petugas QC'"
                                    });
                                }
                                if (JobTaskIdQC.length == 0){
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Input Data 'Kembali Ke Pekerjaan dan Data Defect'"
                                    });
                                }
                                if ($scope.gridDefect.data.length == 0){
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Input Data 'Kembali Ke Pekerjaan dan Data Defect'"
                                    });
                                }
                                
                            } else {
                                var xJobtaskbpid = 0;
                                for (var j=0; j<$scope.DataDetailModel.length; j++){
                                    if ($scope.DataDetailModel[j].isOldChip == 0){
                                        xJobtaskbpid = $scope.DataDetailModel[j].JobTaskBpId
                                    }
                                }
                                // sort dl data di gridnya
                                var SortedData = $scope.gridDefect.data;
                                SortedData = SortedData.sort(function(a, b){
                                    var dataA= a.JobTaskId, dataB=b.JobTaskId
                                    return dataA-dataB //sort by date ascending
                                })
                                var countProses = 0;
                                var xSortedData = angular.copy(SortedData);
                                for (var i=0; i<xSortedData.length; i++){
                                    if (i == 0){
                                        xSortedData[i].Proses = 1;
                                    } else {
                                        if (xSortedData[i].JobTaskId == xSortedData[i-1].JobTaskId) {
                                            xSortedData[i].Proses = xSortedData[i-1].Proses + 1
                                        } else {
                                            xSortedData[i].Proses = 1
                                        }
                                    }
                                    xSortedData[i].JobTaskBpId = xJobtaskbpid;
                                    xSortedData[i].CurrentChip = AChip;
                                    xSortedData[i].RedoToChip = $scope.mData.RedoToChipKe
                                    if (xSortedData[i].JobDefectId == null || xSortedData[i].JobDefectId == undefined){
                                        xSortedData[i].JobDefectId = 0;
                                    } else {
                                        xSortedData[i].JobDefectId = xSortedData[i].JobDefectId;
                                    }
                
                                }
                                console.log('data tabel defect', xSortedData);
                                // console.log($scope.mData, AChip, $scope.DataDetailModel)
                                RepairProcessBP.postDefect(xSortedData).then(
                                    function(res) {
                                        // $scope.choseData(JobId);
                                        $scope.mData.NameDefect = "";
                                        $scope.mData.JumlahDefect = "";
                                        $scope.modelCheckNameDefect = [];
                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                );
                
                                
                                RepairProcessBP.putClock(6, JobId, ActiveChip, Model, JobTaskIdQC).then(
                                    function(res) {
                                        $scope.close();
                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                );
                            }
        
                        }
                    })
                    
                }
            });
            
        }

        $scope.doubleClickQCFinish = 0
        $scope.simpanLolosQC = function(Model, JobId, ActiveChip) {
            if ($scope.doubleClickQCFinish == 0){
                $scope.doubleClickQCFinish = 1
                RepairProcessBP.CheckIsJobCancelWO(JobId).then(function (resultC) {
                    if (resultC.data == 666){
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                        });
                        $scope.doubleClickQCFinish = 0
                    } else {
                        console.log("hahahha", JobId);
                        console.log("model", Model);
                        if ((Model.ChipQcTechnicianId == undefined || Model.ChipQcTechnicianId == "" || Model.ChipQcTechnicianId == null)) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Mohon Input Data"
                                    // ,
                                    // content: error.join('<br>'),
                                    // number: error.length
                            });
                            $scope.doubleClickQCFinish = 0
                        } else {
                            // RepairProcessBP.getStatWO($scope.xDataWoList[0].JobId,$scope.xDataWoList[0].Status).then(function(result5){
                            RepairProcessBP.getStatWO($scope.xDataWoList[0].JobId,9).then(function(result5){ // di kasi 9 karena kl dia mau qc dia click tab qc yang uda pasti update jadi 9

                                if (result5.data == 1) {
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                                    });
                                    $scope.choseData(JobId);
                                    $scope.close();
                                    ngDialog.closeAll();
                                    $scope.clickBerlebih = false;
                                    $scope.doubleClickQCFinish = 0
                                } else {
                                    RepairProcessBP.putClock(6, JobId, ActiveChip, Model, JobTaskIdQC).then(
                                        function(res) {
                                            $scope.close();
                                            $scope.doubleClickQCFinish = 0
                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                            $scope.doubleClickQCFinish = 0
                                        }
                                    );
                                }
                            })
                            
                        }
                    }
                },
                function(err) {
                    console.log("err=>", err);
                    $scope.doubleClickQCFinish = 0
                });
            }
            
            
        }

        // $scope.DataPetugasQC = [];
        // $scope.DataDefect = [];
        var AChip = {};
        $scope.clockOnQC = function(JobId, ActiveChip, Model, JobTask) {
            RepairProcessBP.CheckIsJobCancelWO(JobId).then(function (resultC) {
                if (resultC.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                    });
                } else {
                    RepairProcessBP.getStatWO($scope.xDataWoList[0].JobId,$scope.xDataWoList[0].Status).then(function(result5){
                        if (result5.data == 1) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                            });
                            $scope.choseData(JobId);
                            $scope.close();
                            ngDialog.closeAll();
                            $scope.clickBerlebih = false;
                        } else {
                            var jamActual = new Date();
                            console.log('activechip', ActiveChip)
                            console.log('Model', Model)
                            console.log('JobTask', JobTask)
                            console.log('plan jam selesai chip', $scope.JamSelesaiBaseOnJPCB)
                            console.log('actual jam ', jamActual)
                            console.log('ini buat detail data', $scope.DataDetailModel)
                
                            var Extend = 0;
                            // var planFinishx = null;
                            // for (var i=0; i<$scope.DataDetailModel.length; i++){
                            //     if ($scope.DataDetailModel[i].isOldChip == 0){
                            //         Extend = $scope.DataDetailModel[i]['Chip' + ActiveChip + 'ExtensionMinute'];
                            //         planFinishx = $scope.DataDetailModel[i]['Chip' + ActiveChip + 'PlanFinish']; 
                            //     }
                            // }
                
                            // var newDate = angular.copy(planFinishx)
                            // console.log("newDate 1", newDate);
                            // newDate.setSeconds(0);
                
                            // // var total = 0;
                            // // for (var i = 0; i < $scope.gridTimeWorkApi.grid.columns.length; i++) {
                            // //     var a = $scope.gridTimeWorkApi.grid.columns[i].getAggregationValue();
                            // //     // $scope.arrWorkTime.push(a + ' minutes');
                            // //     if (a != null || a !== undefined) {
                            // //         $scope.arrWorkTime.push(a + ' minutes');
                            // //         $scope.getCalculatedTimeWork.push(a);
                            // //         total = total + a;
                            // //     };
                            // // };
                
                            // function roundToTwo(num) {
                            //     return +(Math.round(num + "e+2") + "e-2");
                            // };
                
                            // // katanya 1 hari jam kerja nya 7 jam (1/nov/2018), kalau mau 8 jam ganti aja (7 * 60) jadi 8 * 60
                            // var days = Math.floor(Math.abs(Extend / (8 * 60)));
                            // var hours = Math.abs((Extend % (8 * 60)) / 60);
                            // hours = hours-days;
                            // newDate.setDate(newDate.getDate()+ days);
                            // newDate.setHours(newDate.getHours() + roundToTwo(hours));
                            // console.log('ini semoga uda tambah ext bener', newDate)
                
                
                            var tahunS = $scope.JamSelesaiBaseOnJPCB.getFullYear();
                            var bulanS = $scope.JamSelesaiBaseOnJPCB.getMonth();
                            var tanggalS = $scope.JamSelesaiBaseOnJPCB.getDate();
                            var jamS = $scope.JamSelesaiBaseOnJPCB.getHours();
                            var menitS = $scope.JamSelesaiBaseOnJPCB.getMinutes();
                            var menitWithExtend = menitS + Extend;
                
                            var finalJamSelesai = new Date(tahunS, bulanS, tanggalS, jamS, menitWithExtend, 0);
                
                            console.log('final jam selesai with extend', finalJamSelesai);
                
                            if (jamActual > finalJamSelesai) {
                                console.log('dah kelebihan'); // kalau melewati plan finish + extend ga boleh qc
                                $scope.cekQC = true;
                                bsNotify.show({
                                    type: 'danger',
                                    title: "Jam actual telah melebihi plan finish, silahkan lakukan EXTEND",
                                    timeout: 5000
                                });
                            } else {
                                console.log('boleh kok') // kalau ga melewati plan finish + extend boleh qc
                                $scope.cekQC = false;
                                // $scope.mData.ChipIsPassQc = null; // dipindah ke choosedata
                                $scope.mData.ChipQcTechnicianId = null;
                                // $scope.mData.RedoToChipKe = null;
                                // $scope.mData.FreDoJob = null;
                                // $scope.mData.ChipNotPassQcReason = null;
                                // $scope.mData.ChipIsOtherReason = null;
                                // $scope.mData.ChipOtherReason = null;
                                // $scope.mData.ChipAdditionNote = null;
                                // $scope.mData.NameDefect = null;
                                // $scope.mData.JumlahDefect = null;
                                // $scope.mData.qty = null;
                
                
                                AChip = ActiveChip;
                                $scope.DataFI = JobTask;
                                var outletId = $scope.user.OutletId;
                                RepairProcessBP.getDataQC(outletId)
                                    .then(
                
                                        function(res) {
                                            $scope.DataPetugasQC = res.data;
                                            console.log("Data QC", res.data);
                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                        }
                                    );
                
                                RepairProcessBP.getDataDefectByJobId(JobId, AChip)
                                    .then(
                
                                        function(res) {
                                            $scope.modelCheckNameDefect = [];
                                            // $scope.gridDefect.data = res.data.Result;
                
                                            $scope.gridDefect.data = [];
                                            for (var q=0; q<res.data.Result.length; q++){
                                                if(res.data.Result[q].CurrentChip == AChip){ // kata pa dodi dan pa yo kl defect dr proses lain ga usah di tampilin
                                                    $scope.gridDefect.data.push(res.data.Result[q])
                                                }
                                            }
                
                                            if (copygridDefect.length > 0){
                                                $scope.gridDefect.data = angular.copy(copygridDefect)
                                            }
                
                                            console.log("list Defect", $scope.DataDefect);
                                            console.log("Data Defect tabel", $scope.gridDefect.data);
                                            console.log('data pekerjaan', $scope.DataFI)
                                            for (var j=0; j<$scope.gridDefect.data.length; j++){
                                                for (var k=0; k<$scope.DataFI.length; k++){
                                                    if ($scope.gridDefect.data[j].JobTaskId == $scope.DataFI[k].JobTaskId){
                                                        $scope.gridDefect.data[j].Defect.JobName = $scope.DataFI[k].TaskName;
                                                    }
                                                }
                                            }
                                            // for (var i = 0; i < res.data.Result.length; i++) {
                                            for (var i = 0; i < $scope.gridDefect.data.length; i++) {
                                                $scope.modelCheckNameDefect.push($scope.gridDefect.data[i].DefectId);
                                                $scope.gridDefect.data[i].Defect.JobTaskId = $scope.gridDefect.data[i].JobTaskId
                                            }
                                            console.log('$scope.modelCheckNameDefect', $scope.modelCheckNameDefect);
                                        },
                                        function(err) {
                                            console.log("err=>", err);
                                        }
                                    );
                
                                RepairProcessBP.putClock(5, JobId, ActiveChip, Model).then(
                                    function(res) {
                                        // $scope.choseData(JobId); // ini di komen karena jam di tab qc jadi ilang.. kalau ada error uncomment aja, tambaihn chipProsesKe nya
                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                );
                
                                RepairProcessBP.getDataDefect().then(
                                    function(res) {
                                        $scope.dataDefect = res.data;
                                        console.log("$scope.dataDefect", $scope.dataDefect);
                                    },
                                    function(err) {
                                        console.log("err=>", err);
                                    }
                                )
                            }
        
                        }
                    })

                    
                }
            });
            

        }

        $scope.noResults = false;
        $scope.selected = {};
        // RepairProcessBP.getDataDefect().then(
        //     function(res){
        //       $scope.dataDefect = res.data;
        //       console.log("$scope.dataDefect",$scope.dataDefect);
        //     },
        //     function(err) {
        //       console.log("err=>", err);
        //     }
        // );

        $scope.getDefectTypeAhead = function() {
            RepairProcessBP.getDataDefect().then(
                function(res) {
                    $scope.dataDefect = res.data;
                    console.log("$scope.dataDefect", $scope.dataDefect);
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            // var ress = RepairProcessBP.getDataDefect(key).then(function(res) {
            //            return res.data;
            //          });
            // console.log("ress",ress);
            // return ress
            // }
        };

        $scope.addDefectList = function(Data) {
            console.log("data yang disave", Data);
            if ((Data.JumlahDefect == undefined || Data.JumlahDefect == "" || Data.JumlahDefect == null) || (Data.NameDefect == undefined || Data.NameDefect == "" || Data.NameDefect == null) || (JobTaskIdQC.length == 0)) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Data Defect"
                });

            // } else if ($scope.modelCheckNameDefect.includes($scope.mData.NameDefect)) {
            //     // } else if ($scope.gridDefect.data.find(x => x.DefectId == $scope.mData.NameDefect)) {
            //     bsNotify.show({
            //         type: 'danger',
            //         title: "Nama Defect tidak boleh sama",
            //         timeout: 5000
            //     });
            // } else {
            } else {

                var tesDefectSama = 0;
                if ($scope.gridDefect.data.length > 0){
                    for (var k=0; k<$scope.gridDefect.data.length; k++){
                        if ($scope.gridDefect.data[k].Defect.Id == $scope.mData.NameDefect && $scope.gridDefect.data[k].Defect.JobTaskId == $scope.mData.NamePekerjaanDefect){
                            tesDefectSama++;
                        } 
                    }
                }
                if (tesDefectSama > 0){
                    bsNotify.show({
                        type: 'danger',
                        title: "Nama Defect tidak boleh sama",
                        timeout: 5000
                    });
                } else {
                    var TambahDataDefect = {};
                    TambahDataDefect.DefectId = $scope.mData.NameDefect;
                    TambahDataDefect.JobTaskId = $scope.mData.NamePekerjaanDefect;
                    TambahDataDefect.JobId = $scope.DataDetailModel[0].JobId;
                    TambahDataDefect.Defect={};
                    for (var i=0; i<$scope.dataPekerjaanDefect.length; i++){
                        if ($scope.dataPekerjaanDefect[i].Id == $scope.mData.NamePekerjaanDefect){
                            TambahDataDefect.Defect.JobName = $scope.dataPekerjaanDefect[i].Name
                            TambahDataDefect.Defect.JobTaskId = $scope.dataPekerjaanDefect[i].Id
                        }
                    }
                    for (var j=0; j<$scope.dataDefect.length; j++){
                        if ($scope.dataDefect[j].Id == $scope.mData.NameDefect){
                            TambahDataDefect.Defect.Name = $scope.dataDefect[j].Name
                            TambahDataDefect.Defect.Id = $scope.dataDefect[j].Id
                        }
                    }
                    TambahDataDefect.Qty = $scope.mData.JumlahDefect;
    
                    $scope.gridDefect.data.push(TambahDataDefect);
                    copygridDefect = angular.copy($scope.gridDefect.data)
                    console.log('datagrid', $scope.gridDefect.data)
                }
                








                // sabar di comment dl nanti di panggil pas simpan di akhir aja
                // RepairProcessBP.postDefect(Data, $scope.mData.NameDefect, AChip, $scope.DataDetailModel, JobTaskIdQC).then(
                //     function(res) {
                //         // $scope.choseData(JobId);
                //         $scope.mData.NameDefect = "";
                //         $scope.mData.JumlahDefect = "";
                //         $scope.modelCheckNameDefect = [];
                //         RepairProcessBP.getDataDefectByJobId($scope.xchoseData, AChip)
                //             .then(
                //                 function(res) {
                //                     $scope.gridDefect.data = res.data.Result;
                //                     console.log("simpan Berhasil");
                //                     for (var i = 0; i < res.data.Result.length; i++) {
                //                         $scope.modelCheckNameDefect.push($scope.gridDefect.data[i].DefectId);
                //                     }
                //                     console.log('$scope.modelCheckNameDefect', $scope.modelCheckNameDefect);
                //                 },
                //                 function(err) {
                //                     console.log("err=>", err);
                //                 }
                //             );
                //     },
                //     function(err) {
                //         console.log("err=>", err);
                //     }
                // );
            }
        }

        var IdDefect = {};
        $scope.onSelectWork = function($item, $model, $label) {
            // console.log("onSelectWork=>", idx);
            console.log("onSelectWorkitem=>", $item);
            IdDefect = $item.Id;
            console.log("IdDefect", IdDefect);
            console.log("onSelectWorkmodel=>", $model);
            console.log("onSelectWorklabel=>", $label);
            // $scope.mData.Work = $label;
            // console.log('materialArray', materialArray);
            // materialArray = [];
            // materialArray.push($item);
            // if($item.FR != null){
            //   $scope.jobAvailable = true;
            // }else{
            //   $scope.jobAvailable = false;
            // }
            // $scope.sendWork($label,$item);
            // console.log("modelnya",$scope.lmModel);
            // console.log('materialArray',materialArray);
        }
        $scope.onNoResult = function() {
            console.log("Data Kosong");
        }

        $scope.onGotResult = function() {
            console.log("onGotResult=>");
        }

        $scope.deleteDefectList = function() {

        }

        // ===========================
        // Allow Pattern
        // ===========================
        $scope.allowPattern = function (event, type, item) {
            var patternRegex
            if (type == 1) {
                patternRegex = /\d/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /\d|[a-z]|[*]/i; //ALPHANUMERIC ONLY
            };
            console.log("event", event);
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (event.key === '*'){
                event.preventDefault();
                return false;
            }
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            };
        };

        // $scope.onSelectRowsDef = function(rows){
        //     console.log("onSelectRowsDef=>",rows);
        // }

        var actionAfterBilling = '<div class="ui-grid-cell-contents">' +
            '<a href="#" ng-click="grid.appScope.$parent.UpdateDefect(row.entity)" uib-tooltip="Ubah Defect" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-pencil fa-lg" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.$parent.DeleteDefect(row.entity)" ng-show="row.entity.JobDefectId == undefined || row.entity.JobDefectId == null" uib-tooltip="Hapus Defect" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-trash fa-lg" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';

        $scope.show_modal = { show: false };
        $scope.modal_model = [];

        $scope.modalMode = 'new';

        // var DataDefChose = {};
        $scope.UpdateDefect = function(data) {
            console.log("data yang dipilih dari defect", data);
            $scope.dataDefectEdit = data;
            // $scope.dataCancel = res.data.Result;
            // DataDefChose = data;
            $scope.show_modal = { show: true };
            $scope.mData.qty = data.Qty;
            $scope.mData.JobDefectId = data.JobDefectId;
        }

        $scope.updateDefectFinal = function(data) {
            console.log("dak teh", data);
            for (var i=0; i<$scope.gridDefect.data.length; i++){
                if ($scope.dataDefectEdit.JobTaskId == $scope.gridDefect.data[i].JobTaskId && $scope.dataDefectEdit.DefectId == $scope.gridDefect.data[i].DefectId){
                    $scope.gridDefect.data[i].Qty = angular.copy(data.JumlahDefect)
                }
            }
            copygridDefect = angular.copy($scope.gridDefect.data);
            $scope.show_modal = { show: false };
            data.JumlahDefect = null;

            // RepairProcessBP.putDefect(data).then(
            //     function(res) {
            //         $scope.modelCheckNameDefect = [];
            //         RepairProcessBP.getDataDefectByJobId($scope.xchoseData, AChip)
            //             .then(

            //                 function(res) {
            //                     $scope.gridDefect.data = res.data.Result;
            //                     $scope.mData.JumlahDefect = null;
            //                     $scope.show_modal = { show: false };
            //                     // console.log("Data Defect", $scope.DataDefect);
            //                     for (var i = 0; i < res.data.Result.length; i++) {
            //                         $scope.modelCheckNameDefect.push($scope.gridDefect.data[i].DefectId);
            //                     }
            //                     console.log('$scope.modelCheckNameDefect', $scope.modelCheckNameDefect);
            //                 },
            //                 function(err) {
            //                     console.log("err=>", err);
            //                 }
            //             );
            //         // $scope.choseData(JobId);
            //     },
            //     function(err) {
            //         console.log("err=>", err);
            //     }
            // );
        }

        $scope.DeleteDefect = function(data) {
            var JobDefectId = data.JobDefectId;
            var JobId = data.JobId;
            var JobTaskId = data.JobTaskId;
            var DefectId = data.DefectId;

            bsAlert.alert({
                    title: "Apakah Anda yakin ingin menghapus data ?",
                    text: "",
                    type: "question",
                    showCancelButton: true,
                    confirmButtonText: "Ya",
                    cancelButtonText: "Tidak",
                },
                function() {
                    for (var i=0; i<$scope.gridDefect.data.length; i++){
                        if (JobTaskId == $scope.gridDefect.data[i].JobTaskId && DefectId == $scope.gridDefect.data[i].DefectId){
                            $scope.gridDefect.data.splice(i,1);
                        }
                        // fruits.splice(0, 1);
                    }
                    copygridDefect = angular.copy($scope.gridDefect.data);
                    // sabar ini hapus defect hrs nya buat kl belom pernah di simpan aja. kl uda di simpan hrs nya ga blh hapus
                    // RepairProcessBP.deleteDefect(JobDefectId, JobId).then(
                    //     function(res) {

                    //         bsAlert.success("Data berhasil dihapus");
                    //         $scope.modelCheckNameDefect = [];
                    //         RepairProcessBP.getDataDefectByJobId($scope.xchoseData, AChip).then(
                    //             function(res) {
                    //                 $scope.gridDefect.data = res.data.Result;
                    //                 $scope.mData.JumlahDefect = null;
                    //                 $scope.show_modal = { show: false };
                    //                 // console.log("Data Defect", $scope.DataDefect);
                    //                 for (var i = 0; i < res.data.Result.length; i++) {
                    //                     $scope.modelCheckNameDefect.push($scope.gridDefect.data[i].DefectId);
                    //                 }
                    //                 console.log('$scope.modelCheckNameDefect', $scope.modelCheckNameDefect);
                    //             },
                    //             function(err) {
                    //                 console.log("err=>", err);
                    //             }
                    //         );
                    //     },
                    //     function(err) {
                    //         console.log("err=>", err);
                    //     }
                    // );
                },
                function() {}
            )
        }

        $scope.gridDefect = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            showGridFooter: true,
            columnDefs: [
                { field: 'DefectId', name: 'Id Defect', visible: false },
                { field: 'JobDefectId', name: 'Job Id Defect', visible: false },
                { field: 'JobId', name: 'Job Id', visible: false },
                { field: 'Defect.JobName', name: 'Nama Pekerjaan', minWidth: 200, Width: 250 },
                { field: 'Defect.Name', name: 'Nama Defect', minWidth: 200, Width: 250 },
                { field: 'Qty', name: 'Jumlah Defect', minWidth: 200, width: 250 },
                { name: 'Action', cellTemplate: actionAfterBilling, minWidth: 200, width: 250 }
            ]
        };

        $scope.gridDefectFi = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            showGridFooter: true,
            columnDefs: [
                { field: 'DefectId', name: 'Id Defect', visible: false },
                { field: 'Defect.Name', name: 'Nama Defect', minWidth: 300, Width: 400 },
                { field: 'Qty', name: 'Jumlah Defect', minWidth: 300, width: 400 },
            ]
        };

        $scope.gridDefect.onRegisterApi = function(gridApi) {
            // set gridApi on $scope
            $scope.gridApi = gridApi;

            $scope.gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                // console.log("selected=>",$scope.selectedRows);
                if ($scope.onSelectRowsDef) {
                    $scope.onSelectRowsDef($scope.selectedRows);
                }
            });
            $scope.gridApi.selection.on.rowSelectionChangedBatch($scope, function(row) {
                $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                if ($scope.onSelectRowsDef) {
                    $scope.onSelectRowsDef($scope.selectedRows);
                }
            });
        }

        //----------------------------------
        // Akhir Bagian QC Defect List
        //----------------------------------

        //----------------------------------
        // Grid Setup
        //----------------------------------

        $scope.gridOptions = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            showGridFooter: true,
            columnDefs: [
                { field: 'KodeParts', name: 'KodeParts', visible: false },
                { field: 'NamaParts', name: 'NamaParts', minWidth: 200, Width: 250 },
                { field: 'QTY', name: 'QTY', minWidth: 200, Width: 250 }
            ]
        };

        $scope.gridOptions.onRegisterApi = function(gridApi) {
            // set gridApi on $scope
            $scope.gridApi = gridApi;

            $scope.gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                // console.log("selected=>",$scope.selectedRows);
                if ($scope.onSelectRows) {
                    $scope.onSelectRows($scope.selectedRows);
                }
            });
            $scope.gridApi.selection.on.rowSelectionChangedBatch($scope, function(row) {
                $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                if ($scope.onSelectRows) {
                    $scope.onSelectRows($scope.selectedRows);
                }
            });
        }

        //grid Final Inspection

        var DataFinalInspection = [{
            Id: 1,
            NamaPekerjaan: "Perbaikan",
            TipePerbaikan: "Mesin"
        }];

        $scope.getFinalInspection = function() {
            $scope.gridFinalInspection.data = DataFinalInspection;
            // $scope.grid.data = res;
            // // console.log("nih", $scope.VM );
            // Towing.getData()
            //   .then(

            //     function(res) {
            //       gridData = [];
            //       //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
            //       $scope.grid.data = res.data;
            //       // console.log("role=>", res.data.Result);
            //       //console.log("grid data=>",$scope.grid.data);
            //       //$scope.roleData = res.data;
            //       $scope.loading = false;
            //       console.log("test",res);
            //     },
            //     function(err) {
            //       console.log("err=>", err);
            //     }
            //   );
        }

        $scope.gridFinalInspection = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            showGridFooter: true,
            columnDefs: [
                { field: 'Id', name: 'id', visible: false },
                { field: 'NamaPekerjaan', name: 'Nama Warranty' },
                { field: 'TipePerbaikan', name: 'Open No' },
                { field: 'TipePerbaikan', name: 'T1' },
                { field: 'TipePerbaikan', name: 'T2' },
                { field: 'TipePerbaikan', name: 'OFP' },
                { field: 'TipePerbaikan', name: 'Keterangan' }
                // {
                //   field: 'T1', name: 'T1',
                //   editableCellTemplate: 'ui-grid/dropdownEditor',
                //   editDropdownValueLabel: 'T1',
                //   editDropdownIdLabel: 'T1',
                //   editDropdownOptionsArray: [
                //        { T1: 'Bemper' },
                //        { T1: 'Jok' },
                //      ]
                // },
                // { field: 'T2', name: 'T2',
                //   editableCellTemplate: 'ui-grid/dropdownEditor',
                //   editDropdownValueLabel: 'T2',
                //   editDropdownIdLabel: 'T2',
                //   editDropdownOptionsArray: [
                //        { T2: 'Bemper' },
                //        { T2: 'Jok' },
                //      ]
                // }
            ]
        };

        //grid final inspection re do ke job
        var DataRedoFinal = [{
            RedoId: 1,
            NamaPekerjaan: "Fender Kiri",
            TiperPerbaikan: "Replace"
        }];

        $scope.getRedoFinal = function() {
            $scope.gridRedoFinal.data = DataRedoFinal;
            // $scope.grid.data = res;
            // // console.log("nih", $scope.VM );
            // Towing.getData()
            //   .then(

            //     function(res) {
            //       gridData = [];
            //       //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
            //       $scope.grid.data = res.data;
            //       // console.log("role=>", res.data.Result);
            //       //console.log("grid data=>",$scope.grid.data);
            //       //$scope.roleData = res.data;
            //       $scope.loading = false;
            //       console.log("test",res);
            //     },
            //     function(err) {
            //       console.log("err=>", err);
            //     }
            //   );
        }

        $scope.gridRedoFinal = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            showGridFooter: true,
            columnDefs: [
                { field: 'RedoId', name: 'RedoId', visible: false },
                { field: 'NamaPekerjaan', name: 'Nama Pekerjaan' },
                { field: 'TiperPerbaikan', name: 'Nama Defect' }
            ]
        };

        //Re do ke job QC
        // var DataRedoQC = [
        //     {
        //         RedoId : 1,
        //         NamaPekerjaan : "Fender Kanan",
        //         TiperPerbaikan : "Replace"
        //     }
        // ];

        // $scope.getRedoQC = function(){
        //     $scope.gridRedoQC.data = DataRedoQC;
        //     // $scope.grid.data = res;
        //     // // console.log("nih", $scope.VM );
        //     // Towing.getData()
        //     //   .then(

        //     //     function(res) {
        //     //       gridData = [];
        //     //       //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
        //     //       $scope.grid.data = res.data;
        //     //       // console.log("role=>", res.data.Result);
        //     //       //console.log("grid data=>",$scope.grid.data);
        //     //       //$scope.roleData = res.data;
        //     //       $scope.loading = false;
        //     //       console.log("test",res);
        //     //     },
        //     //     function(err) {
        //     //       console.log("err=>", err);
        //     //     }
        //     //   );
        // }

        // $scope.gridRedoQC = {
        //     enableFiltering: true,
        //     enableSorting: true,
        //     multiSelect: true,
        //     enableRowSelection: true,
        //     enableSelectAll: true,
        //     // selectionRowHeaderWidth: 35,
        //     // rowHeight: 35,
        //     showGridFooter:true,
        //     columnDefs: [
        //       { field:'RedoId', name:'RedoId', visible :false},
        //       { field: 'NamaPekerjaan', name: 'Nama Pekerjaan'},
        //       { field: 'TiperPerbaikan', name:'Nama Defect'}
        //     ]
        // };

        //Grid Job di Problem Finding
        var DataJobProbFinding = [{
                JobId: 1,
                NamaPekerjaan: "Bemper Kiri",
                TiperPerbaikan: "Replace"
            },
            {
                JobId: 2,
                NamaPekerjaan: "Fender Kiri",
                TiperPerbaikan: "Replace"
            }

        ];

        $scope.getJobProbFinding = function() {
            $scope.gridJobProbFinding.data = DataJobProbFinding;
            // $scope.grid.data = res;
            // // console.log("nih", $scope.VM );
            // Towing.getData()
            //   .then(

            //     function(res) {
            //       gridData = [];
            //       //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
            //       $scope.grid.data = res.data;
            //       // console.log("role=>", res.data.Result);
            //       //console.log("grid data=>",$scope.grid.data);
            //       //$scope.roleData = res.data;
            //       $scope.loading = false;
            //       console.log("test",res);
            //     },
            //     function(err) {
            //       console.log("err=>", err);
            //     }
            //   );
        }

        $scope.gridJobProbFinding = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            showGridFooter: true,
            columnDefs: [
                { field: 'JobId', name: 'JobId', visible: false },
                { field: 'NamaPekerjaan', name: 'Nama Pekerjaan' },
                { field: 'TiperPerbaikan', name: 'Nama Defect' }
            ]
        };

        $scope.gridparts = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            // $scope.dataJobParts
            data: 'dataJobParts',
            showGridFooter: true,
            columnDefs: [
                { field: 'Part.PartsCode', name: 'Kode Parts' },
                { field: 'PartsName', name: 'Nama Parts' },
                { field: 'Qty', name: 'Qty' }
            ],
            onRegisterApi: function(gridApi) {
                // set gridApi on $scope
                $scope.gridApi = gridApi;
                $scope.gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                    // console.log("selected=>",$scope.selectedRows);
                    if ($scope.onSelectRows) {
                        $scope.onSelectRows($scope.selectedRows);
                    }
                });
            }
        };


        $scope.gridWODetailParts = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            data: 'DataMaterial',
            showGridFooter: true,
            columnDefs: [
                { field: 'ModelCode', name: 'Nomor Material' },
                { field: 'PartsName', name: 'Nama Material' },
                { field: 'Status', name: 'Status' },
                { field: 'ETA', name: 'ETA' },
                { field: 'Qty', name: 'Permintaan QTY' },
                { field: 'QtyGI', name: 'Isu QTY' },
                { field: 'Satuan.Name', name: 'Satuan' }
            ]
        };

        $scope.gridWoDetail = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            data: 'fJobParts',
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            showGridFooter: true,
            columnDefs: [
                { field: 'NomorMaterial', name: 'Nomor Material' },
                { field: 'PartsName', name: 'Nama Material' },
                { field: 'Status', name: 'Status' },
                { field: 'ETA', name: 'ETA' },
                { field: 'QtyPrepicking', name: 'Permintaan QTY' },
                { field: 'QtyReserved', name: 'Isu QTY' },
                { field: 'Satuan.Name', name: 'Satuan' }
            ]
        };

        $scope.getWac = function() {

            WOBP.getDataWAC().then(function(resu) {
                console.log('cek data WAC', resu.data);
                var tempdata = [];
                for (var i in resu.data) {
                    if (resu.data[i].StatusCode != 0 & resu.data[i].StatusCode != null) {
                        tempdata.push(resu.data[i]);

                    }
                }
                $scope.headerWAC = tempdata;
                $scope.CreateGrids();
            });

            RepairProcessBP.getWACBp($scope.xchoseData)
                .then(

                    function(res) {


                        $scope.dataItemWACBP = res.data.Result;
                        console.log("data WacBP", $scope.dataItemWACBP);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );

        }
        $scope.mDataWAC = [];

        $scope.tabClickWAC = function(param, name) {
            $scope.gridName = name;
            var jobid = $scope.xchoseData;
            $scope.getDataExt(param, name, jobid);
        };
        $scope.getDataExt = function(id, gridName, jobid) {
            // if (gridName.includes(' ')) {
            //     gridName = gridName.replace(" ", "");
            // }
            var gridIdentity = 'gridData' + gridName;
            console.log('id header', id, gridName, jobid, gridIdentity)
            WOBP.getDataWACItem(jobid, id).then(function(resu) {
                console.log('item wac', resu.data.Result);
                if ($scope.data[gridIdentity].data.length == 0) {
                    var arr = [];
                    angular.forEach(resu.data.Result, function(e, k) {
                        var obj = {};

                        obj.no = k + 1;
                        obj.JobWacItemId = e.ItemId;
                        obj.name = e.ItemName;
                        obj.ItemStatus = 1;
                        obj.OtherDescription = null;
                        obj.ItemStatus = e.ItemStatus;
                        arr.push(obj);
                    });
                    $scope.mDataWAC[gridIdentity] = arr;
                    $scope.data[gridIdentity].data = $scope.mDataWAC[gridIdentity];
                }
                console.log("$scope.mDataWAC", $scope.mDataWAC);
                console.log("$scope.data[gridIdentity].data", $scope.data[gridIdentity].data);
                console.log("$scope.dataGridIdentity", $scope.dataGridIdentity);
            })
        };
        $scope.tabClick = function(Id) {
            console.log("Id", Id);

            var dataItemWAC = $scope.dataItemWACBP;
            WOBP.getItemWAC(Id).then(function(resu) {


                if (Id == 1) {
                    $scope.chooseVType();
                } else if (Id == 2 && $scope.gridExt.data.length == 0) {
                    $scope.dataExt = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataExt, function(e, k) {
                        var obj = {};
                        _.map(dataItemWAC, function(a) {
                            if (e.ItemId == a.JobWacItemId) {
                                obj.no = k + 1;
                                obj.JobWacItemId = e.ItemId;
                                obj.name = e.ItemName;
                                obj.ItemStatus = a.ItemStatus;
                                obj.OtherDescription = a.OtherDescription;
                            };
                        });

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Ext = arr;
                    $scope.gridExt.data = $scope.mDataWAC.Ext;
                } else if (Id == 4 && $scope.gridInt.data.length == 0) {
                    $scope.dataInt = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataInt, function(e, k) {
                        var obj = {};

                        _.map(dataItemWAC, function(a) {
                            if (e.ItemId == a.JobWacItemId) {
                                obj.no = k + 1;
                                obj.JobWacItemId = e.ItemId;
                                obj.name = e.ItemName;
                                obj.ItemStatus = a.ItemStatus;
                                obj.OtherDescription = a.OtherDescription;
                            };
                        });

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Int = arr;
                    $scope.gridInt.data = $scope.mDataWAC.Int;
                } else if (Id == 1003 && $scope.gridElec.data.length == 0) {
                    $scope.dataElec = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataElec, function(e, k) {
                        var obj = {};
                        _.map(dataItemWAC, function(a) {
                            if (e.ItemId == a.JobWacItemId) {
                                obj.no = k + 1;
                                obj.JobWacItemId = e.ItemId;
                                obj.name = e.ItemName;
                                obj.ItemStatus = a.ItemStatus;
                                obj.OtherDescription = a.OtherDescription;
                            };
                        });

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Elec = arr;
                    $scope.gridElec.data = $scope.mDataWAC.Elec;
                } else if (Id == 1004 && $scope.gridEquip.data.length == 0) {
                    $scope.dataEquip = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataEquip, function(e, k) {
                        var obj = {};

                        _.map(dataItemWAC, function(a) {
                            if (e.ItemId == a.JobWacItemId) {
                                obj.no = k + 1;
                                obj.JobWacItemId = e.ItemId;
                                obj.name = e.ItemName;
                                obj.ItemStatus = a.ItemStatus;
                                obj.OtherDescription = a.OtherDescription;
                            };
                        });

                        arr.push(obj);
                    });
                    $scope.mDataWAC.Equip = arr;
                    $scope.gridEquip.data = $scope.mDataWAC.Equip;
                } else if (Id == 1005 && $scope.gridDoc.data.length == 0) {
                    $scope.dataDoc = resu.data.Result;
                    var arr = [];
                    angular.forEach($scope.dataDoc, function(e, k) {
                        var obj = {};
                        _.map(dataItemWAC, function(a) {
                            if (e.ItemId == a.JobWacItemId) {
                                obj.no = k + 1;
                                obj.JobWacItemId = e.ItemId;
                                obj.name = e.ItemName;
                                obj.ItemStatus = a.ItemStatus;
                                obj.OtherDescription = a.OtherDescription;
                            };
                        });
                        arr.push(obj);
                    });
                    console.log('grid dataDoc arr', arr);
                    $scope.mDataWAC.Doc = arr;
                    $scope.gridDoc.data = $scope.mDataWAC.Doc;
                }
                // else if (Id == 1006 && $scope.gridOther.length == 0) {
                //     $scope.dataOther = resu.data.Result;
                //     console.log('grid other ', $scope.dataOther[0]);
                //     var arr = [];
                //     angular.forEach($scope.dataOther, function(e, k) {
                //         var obj = {};

                //         obj.no = k + 1;
                //         obj.JobWacItemId = e.ItemId;
                //         obj.name = e.ItemName;
                //         obj.ItemStatus = null;
                //         obj.OtherDescription = '';
                //         arr.push(obj);
                //     });
                //     // $scope.mDataWAC.Oth = arr;
                //     $scope.gridOther = arr;
                //     console.log('grid other arr', $scope.gridOther);
                // }
                else if (Id == 1007 && $scope.gridEquipSafe.data.length == 0) {
                    $scope.dataOther = resu.data.Result;
                    console.log('grid other ', $scope.dataOther[0]);
                    var arr = [];
                    angular.forEach($scope.dataOther, function(e, k) {
                        var obj = {};

                        _.map(dataItemWAC, function(a) {
                            if (e.ItemId == a.JobWacItemId) {
                                obj.no = k + 1;
                                obj.JobWacItemId = e.ItemId;
                                obj.name = e.ItemName;
                                obj.ItemStatus = a.ItemStatus;
                                obj.OtherDescription = a.OtherDescription;
                            };
                        });
                        arr.push(obj);
                    });
                    $scope.mDataWAC.Oth = arr;
                    $scope.gridEquipSafe.data = $scope.mDataWAC.Oth;
                    console.log('grid other arr', $scope.gridEquipSafe.data);
                } else if (Id == 1008 && $scope.gridPersonal.data.length == 0) {
                    $scope.dataOther = resu.data.Result;
                    console.log('grid other ', $scope.dataOther[0]);
                    var arr = [];
                    angular.forEach($scope.dataOther, function(e, k) {
                        var obj = {};

                        _.map(dataItemWAC, function(a) {
                            if (e.ItemId == a.JobWacItemId) {
                                obj.no = k + 1;
                                obj.JobWacItemId = e.ItemId;
                                obj.name = e.ItemName;
                                obj.ItemStatus = a.ItemStatus;
                                obj.OtherDescription = a.OtherDescription;
                            };
                        });
                        arr.push(obj);
                    });
                    $scope.mDataWAC.Oth = arr;
                    $scope.gridPersonal.data = $scope.mDataWAC.Oth;
                    console.log('grid other arr', $scope.gridPersonal.data);
                }
            });
        }


        //WAC LIST > Exterior --------------------------------
        $scope.ItemStatusTemplate = '<div class="btn-group ui-grid-cell-contents"><input ng-model="row.entity.ItemStatus" ng-disabled="true" type="radio" ng-value="1" style="width:20px">&nbsp;Baik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input ng-model="row.entity.ItemStatus" type="radio" ng-value="2" style="width:20px">&nbsp;Rusak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input ng-model="row.entity.ItemStatus" type="radio" ng-value="3" style="width:20px">&nbsp;Tidak Ada</div>';

        $scope.gridExt = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                field: 'ItemStatus',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridExtApi = gridApi;

                $scope.gridExtApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridExtApi.selection.selectRow) {
                    $scope.gridExtApi.selection.selectRow($scope.gridExt.data[0]);
                }
            }
        };

        //WAC LIST > Interior --------------------------------
        $scope.gridInt = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridIntApi = gridApi;

                $scope.gridIntApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridIntApi.selection.selectRow) {
                    $scope.gridIntApi.selection.selectRow($scope.gridInt.data[0]);
                }
            }
        };
        //WAC LIST > Body ------------------------------------

        $scope.coorX = null;
        $scope.coorY = null;
        $scope.drawData = [];
        $scope.showPopup = false;
        $scope.openPopup = function(e) {
            ////
            $scope.showPopup = true;
            var left = e.offsetX;
            var top = e.offsetY;
            $scope.coorX = angular.copy(left) + 'px';
            $scope.coorY = angular.copy(top) + 'px';
            console.log('event X', $scope.coorX);
            console.log('event Y', $scope.coorY);
            $scope.popoverStyle = {
                'position': 'absolute',
                'background': '#fff',
                'border': '1px solid #999',
                'padding': '10px',
                'width': 'auto',
                'box-shadow': '0 0 10px rgba(0, 0, 0, .5)',
                'left': $scope.coorX,
                'top': $scope.coorY
            };
            $scope.drawData = [{
                x: $scope.coorX,
                y: $scope.coorY
            }];
            console.log('scope.drawData', $scope.drawData);
        };

        $scope.closePopover = function(param) {
            $scope.paramClosePopover = param;
            $scope.showPopup = false;
            console.log('scope.paramClosePopover', $scope.paramClosePopover);
        };


        //WAC LIST > Exterior --------------------------------
        $scope.ItemStatusTemplate = '<div class="btn-group ui-grid-cell-contents"><input ng-model="row.entity.ItemStatus" type="radio" ng-value="1" ng-disabled = "true" skip-enable style="width:20px">&nbsp;Baik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input ng-model="row.entity.ItemStatus" type="radio" ng-value="2"  ng-disabled = "true" skip-enable style="width:20px">&nbsp;Rusak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input ng-model="row.entity.ItemStatus" type="radio" ng-value="3"  ng-disabled = "true" skip-enable style="width:20px">&nbsp;Tidak Ada</div>';

        $scope.ItemStatus = null;
        $scope.gridExt = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                field: 'ItemStatus',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridExtApi = gridApi;

                $scope.gridExtApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridExtApi.selection.selectRow) {
                    $scope.gridExtApi.selection.selectRow($scope.gridExt.data[0]);
                }
            }
        };
        //WAC LIST > Interior --------------------------------
        $scope.gridInt = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridIntApi = gridApi;

                $scope.gridIntApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridIntApi.selection.selectRow) {
                    $scope.gridIntApi.selection.selectRow($scope.gridInt.data[0]);
                }
            }
        };

        //WAC LIST > Electricity --------------------------------
        $scope.gridElec = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridElecApi = gridApi;

                $scope.gridElecApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridElecApi.selection.selectRow) {
                    $scope.gridElecApi.selection.selectRow($scope.gridElec.data[0]);
                }
            }
        };

        //WAC LIST > Equipment --------------------------------
        $scope.gridEquip = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridEquipApi = gridApi;

                $scope.gridEquipApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridEquipApi.selection.selectRow) {
                    $scope.gridEquipApi.selection.selectRow($scope.gridEquip.data[0]);
                }
            }
        };

        //WAC LIST > Document --------------------------------
        $scope.gridDoc = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridDocApi = gridApi;

                $scope.gridDocApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridDocApi.selection.selectRow) {
                    $scope.gridDocApi.selection.selectRow($scope.gridDoc.data[0]);
                }
            }
        };
        //WAC LIST > Other ----------------------------------
        $scope.summernoteOptions2 = {
            dialogsInBody: true,
            dialogsFade: false,
            height: 300
        };
        //WAC LIST > Equipment Safety --------------------------------
        $scope.gridEquipSafe = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridEquipSafeApi = gridApi;

                $scope.gridEquipSafeApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridEquipSafeApi.selection.selectRow) {
                    $scope.gridEquipSafeApi.selection.selectRow($scope.gridEquipSafe.data[0]);
                }
            }
        };
        //WAC LIST > Personal Item --------------------------------
        $scope.gridPersonal = {
            enableRowSelection: false,
            enableFullRowSelection: true,
            columnDefs: [{
                name: 'Description',
                field: 'name'
            }, {
                name: 'Action',
                width: '30%',
                cellTemplate: $scope.ItemStatusTemplate
            }],
            onRegisterApi: function(gridApi) {
                $scope.gridPersonalApi = gridApi;

                $scope.gridPersonalApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRow = row.entity;
                });
                if ($scope.gridPersonalApi.selection.selectRow) {
                    $scope.gridPersonalApi.selection.selectRow($scope.gridPersonal.data[0]);
                }
            }
        };

    });