angular.module("app")
    .factory("RepairProcessGR", function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        return {
            getData: function(NoPolice, NoWO, Employee, date) {
                console.log("date", date);
                var finalDate;
                if (date != '-') {
                    var yyyy = date.getFullYear().toString();
                    var mm = (date.getMonth() + 1).toString(); // getMonth() is zero-based         
                    var dd = date.getDate().toString();
                    finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
                } else {
                    finalDate = date;
                };
                // var res=$http.get('/api/as/jobs/WoList/FirstDate/'+dateNow+'/LastDate/'+dateNow+'');
                // var res=$http.get('/api/as/jobs/WoList/FirstDate/'+dateNow+'/LastDate/'+dateNow+'/1');
                var res = $http.get('/api/as/jobs/EstimationList/1/NoPolisi/' + NoPolice + '/idTeknisi/' + Employee + '/WoNo/' + NoWO + '/Date/' + finalDate);
                // var res = $http.get('/api/as/jobs/EstimationList/1/NoPolisi/-/idTeknisi/-/WoNo/-/Date/-');
                // console.log('resnya=>',res);
                return res;
            },
            //from sss
            getAllTypePoints: function() {
                return $http.get('/api/as/TypePoints');
            },
            checkAvailabilityForTechnician: function(id) {
                return $http.get('/api/as/CheckTechClockOn/' + id);
            },
            NewCheckTechClockOn: function(data) {
                return $http.put('/api/as/NewCheckTechClockOn/', data);
            },
            getProbPoints: function(JobId, TypeId) {
                if(TypeId == null){
                    TypeId = 0;
                }
                return $http.get('/api/as/JobWACExts/Job/' + JobId + '/' + TypeId);
            },

            getPrediagnose: function(JobId) {
                return $http.get('/api/as/GetPrediagnosesJobId/' + JobId);
            },
            //end from sss
            getDataDetail: function(JbId) {
                console.log("JbId", JbId);
                var res = $http.get("/api/as/Jobs/" + JbId);
                // console.log('resnya=>',res);
                return res;
            },
            getDataDetailRSA: function(JbId) {
                console.log("JbId", JbId);
                var res = $http.get("/api/as/Jobs/" + JbId);
                // console.log('resnya=>',res);
                return res;
            },

            // , [{}]
            putClockJobBase: function(Status, Model, JobTask, actualTime, stallid) {
                console.log('jobtask bundle', JobTask)
                console.log('actualTime ', actualTime)
                return $http.put('/api/as/JobTasks/Clock/' + Status, [{
                    JobTaskId: Model.JobTaskId,
                    JobId: Model.JobId,
                    TaskId: Model.TaskId,
                    EmployeeId: Model.EmployeeId,
                    Employee: Model.Employee,
                    TaskName: Model.TaskName,
                    FR: Model.FR,
                    ActualRate: Model.ActualRate,
                    Fare: Model.Fare,
                    FlatRate: Model.FlatRate,
                    PlanStart: Model.PlanStart,
                    PlanFinish: Model.PlanFinish,
                    ActualTime: actualTime,
                    Status: Model.Status,
                    LastModifiedUserId: Model.LastModifiedUserId,
                    LastModifiedDate: Model.LastModifiedDate,
                    ProcessId: Model.ProcessId,
                    TFirst1: Model.TFirst1,
                    // Satuan: Model.Satuan,
                    isTypeR: Model.isTypeR,
                    isTypeX: Model.isTypeX,
                    PaidById: Model.PaidById,
                    JobTypeId: Model.JobTypeId,
                    Revision: Model.Revision,
                    JobTaskArray: JobTask,
                    IsCustomDiscount : Model.IsCustomDiscount,
                    Discount : Model.Discount,
                    DiscountTypeId : Model.DiscountTypeId,
                    StallId: stallid,
                }]);
            },

            putClockWoBase: function(Status, Model, actualTime, stallid) {
                console.log("nih status", Status);
                console.log("model", Model);
                console.log('actualTime ', actualTime)

                var newModelArray = [];
                _.map(Model, function(res) {
                    var obj = {};
                    if (res.isDeleted == 0) {
                        obj.JobTaskId = res.JobTaskId;
                        obj.JobId = res.JobId;
                        obj.TaskId = res.TaskId;
                        obj.EmployeeId = res.EmployeeId;
                        obj.Employee = res.Employee;
                        obj.TaskName = res.TaskName;
                        obj.ActualRate = res.ActualRate;
                        obj.FR = res.FR;
                        obj.Fare = res.Fare;
                        obj.FlatRate = res.FlatRate;
                        obj.PlanStart = res.PlanStart;
                        obj.PlanFinish = res.PlanFinish;
                        obj.ActualTime = actualTime;
                        obj.Status = res.Status;
                        obj.TFirst1 = res.TFirst1;
                        obj.LastModifiedUserId = res.LastModifiedUserId;
                        obj.LastModifiedDate = res.LastModifiedDate;
                        obj.ProcessId = res.ProcessId;
                        obj.isTypeR = res.isTypeR;
                        obj.isTypeX = res.isTypeX;
                        obj.PaidById = res.PaidById;
                        obj.Revision = res.Revision;
                        obj.JobTypeId = res.JobTypeId;
                        obj.IsCustomDiscount = res.IsCustomDiscount;
                        obj.Discount = res.Discount;
                        obj.DiscountTypeId = res.DiscountTypeId;
                        obj.StallId = stallid;
                        newModelArray.push(obj);
                    }
                });

                // var ModelArray = Model.map(function(res) {
                //     if (res.isDeleted == 0) { //kalau misal error waktu clock-on, coba hapusin ini if nya
                //         return {
                //             JobTaskId: res.JobTaskId,
                //             JobId: res.JobId,
                //             TaskId: res.TaskId,
                //             EmployeeId: res.EmployeeId,
                //             Employee: res.Employee,
                //             TaskName: res.TaskName,
                //             ActualRate: res.ActualRate,
                //             FR: res.FR,
                //             Fare: res.Fare,
                //             FlatRate: res.FlatRate,
                //             PlanStart: res.PlanStart,
                //             PlanFinish: res.PlanFinish,
                //             ActualTime: actualTime,
                //             Status: res.Status,
                //             TFirst1: res.TFirst1,
                //             LastModifiedUserId: res.LastModifiedUserId,
                //             LastModifiedDate: res.LastModifiedDate,
                //             ProcessId: res.ProcessId,
                //             // Satuan: res.Satuan,
                //             isTypeR: res.isTypeR,
                //             isTypeX: res.isTypeX,
                //             PaidById: res.PaidById,
                //             Revision: res.Revision,
                //             JobTypeId: res.JobTypeId
                //         };
                //     };
                // });
                var ModelArray = newModelArray;
                console.log("ModelArray", ModelArray);
                var route = "/api/as/JobTasks/Clock/" + Status;
                console.log('routenya', route);
                return $http.put(route, ModelArray);
            },
            getDataJobTask: function(JbId) {
                console.log("JbId", JbId);
                var res = $http.get('/api/as/JobTasks/Job/' + JbId);
                // console.log('resnya=>',res);
                return res;
            },
            getPauseReason: function() {
                var catId = 1009;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },

            // clockPause : function(model,dataJobTask,JobTaskIdDtl){
            //   // var JobTaskProgressDetil = JobTaskIdDtl;
            //   // console.log("fuck", dataJobTask);
            //   var finalDatajobTask = [];
            //   for(var a = 0; a < dataJobTask.length; a++){
            //     finalDatajobTask.push(dataJobTask[a]);
            //     console.log("finalDatajobTask", finalDatajobTask);
            //   }

            //   return $http.put('/api/as/JobTasks/Clock/pause',
            //                             [{
            //                               "JobTask":[
            //                                   {
            //                                   JobTaskId : finalDatajobTask.JobTaskId,
            //                                   JobId : finalDatajobTask.JobId,
            //                                   TaskId : finalDatajobTask.TaskId,
            //                                   EmployeeId : finalDatajobTask.EmployeeId,
            //                                   Employee : finalDatajobTask.Employee,
            //                                   TaskName : finalDatajobTask.TaskName,
            //                                   FR : finalDatajobTask.FR,
            //                                   Fare : finalDatajobTask.Fare,
            //                                   PlanStart : finalDatajobTask.PlanStart,
            //                                   PlanFinish : finalDatajobTask.PlanFinish,
            //                                   Status : finalDatajobTask.Status,
            //                                   LastModifiedUserId : finalDatajobTask.LastModifiedUserId,
            //                                   LastModifiedDate :finalDatajobTask.LastModifiedDate,
            //                                   ProcessId : finalDatajobTask.ProcessId,
            //                                   Satuan : finalDatajobTask.Satuan,
            //                                   isTypeR : finalDatajobTask.isTypeR,
            //                                   isTypeX : finalDatajobTask.isTypeX,
            //                                   PaidById : finalDatajobTask.PaidById,
            //                                   Revision : finalDatajobTask.Revision
            //                                 }
            //                               ],
            //                               "TechnicalProblemDto":{
            //                                   PauseReasonId : model.PauseReasonId,
            //                                   PauseReasonDesc : model.PauseReasonDesc,
            //                                   IsNeedRSA : model.IsNeedRSA,
            //                                   IsNeedAdditionalTime : model.IsNeedAdditionalTime,
            //                                   IsOwnDo : model.IsOwnDo,
            //                                   Duration : model.Duration,
            //                                   TroubleDesc : model.TroubleDesc,
            //                                   ResponseDesc : model.ResponseDesc
            //                               },
            //                               "JobTaskProgressDetil":JobTaskIdDtl
            //                             }]);

            // },
            clockPause: function(model, dataJobTask, JobTaskIdDtl, jobpartsfinal, JobTaskIdJobBASE, stallid) {
                console.log("hahahahha ini dia", jobpartsfinal);
                var jobparts = [];
                if (_.isUndefined(jobpartsfinal)) {
                    jobparts = [];
                } else {
                    for (var i = 0; i < jobpartsfinal.length; i++) {
                        jobparts.push(jobpartsfinal[i].JobPartsId);
                    }
                }
                console.log("JobParts", jobparts);

                var JobTask = [];
                _.map(dataJobTask, function(res) {
                // var JobTask = dataJobTask.map(function(res) {
                    var obj = {};
                    if (res.isDeleted == 0){
                        // return {
                            obj.JobTaskId= res.JobTaskId;
                            obj.JobId= res.JobId;
                            obj.TaskId= res.TaskId;
                            obj.EmployeeId= res.EmployeeId;
                            obj.Employee= res.Employee;
                            obj.TaskName= res.TaskName;
                            obj.FR= res.FR;
                            obj.Fare= res.Fare;
                            obj.FlatRate= res.FlatRate;
                            obj.ActualRate= res.ActualRate;
                            obj.JobTypeId= res.JobTypeId;
                            obj.PlanStart= res.PlanStart;
                            obj.PlanFinish= res.PlanFinish;
                            obj.Status= res.Status;
                            obj.LastModifiedUserId= res.LastModifiedUserId;
                            obj.LastModifiedDate= res.LastModifiedDate;
                            obj.ProcessId= res.ProcessId;
                            obj.Satuan= res.Satuan;
                            obj.isTypeR= res.isTypeR;
                            obj.isTypeX= res.isTypeX;
                            obj.PaidById= res.PaidById;
                            obj.Revision= res.Revision;
                            obj.TFirst1= model.T1;
                            obj.TFirst2= model.T2;
                            obj.IsCustomDiscount= res.IsCustomDiscount;
                            obj.Discount= res.Discount;
                            obj.DiscountTypeId = res.DiscountTypeId;
                            obj.StallId = stallid;

                                // JobParts : jobpartsfinal
                            JobTask.push(obj);
                        // };
                    }
                    
                });
                var routePause = '/api/as/JobTasks/Clock/pause';
                var TechnicalProblemDto = {
                    PauseReasonId: model.PauseReasonId,
                    PauseReasonDesc: model.PauseReasonDesc,
                    IsNeedRSA: model.IsNeedRSA,
                    IsNeedAdditionalTime: model.IsNeedAdditionalTime,
                    IsOwnDo: model.IsOwnDo,
                    Duration: model.Duration,
                    TroubleDesc: model.TroubleDesc,
                    ResponseDesc: model.ResponseDesc
                };
                var JobTaskProgressDetil = JobTaskIdDtl;
                var JobTaskProgressDetilPart = jobparts;
                return $http.put(routePause, [{
                    JobTask: JobTask,
                    TechnicalProblemDto: TechnicalProblemDto,
                    JobTaskProgressDetil: JobTaskProgressDetil,
                    JobTaskProgressDetilPart: JobTaskProgressDetilPart,
                    JobTaskId: JobTaskIdJobBASE
                }]);
            },
            // clockJobPause : function(model,dataJobTask){
            //   return $http.put('/api/as/JobTasks/Clock/pause',
            //                                             [
            //                                                   {
            //                                                     JobTaskId : dataJobTask.JobTaskId,
            //                                                     JobId : dataJobTask.JobId,
            //                                                     TaskId : dataJobTask.TaskId,
            //                                                     EmployeeId : dataJobTask.EmployeeId,
            //                                                     Employee : dataJobTask.Employee,
            //                                                     TaskName : dataJobTask.TaskName,
            //                                                     FR : dataJobTask.FR,
            //                                                     Fare : dataJobTask.Fare,
            //                                                     PlanStart : dataJobTask.PlanStart,
            //                                                     PlanFinish : dataJobTask.PlanFinish,
            //                                                     Status : dataJobTask.Status,
            //                                                     LastModifiedUserId : dataJobTask.LastModifiedUserId,
            //                                                     LastModifiedDate :dataJobTask.LastModifiedDate,
            //                                                     ProcessId : dataJobTask.ProcessId,
            //                                                     Satuan : dataJobTask.Satuan,
            //                                                     isTypeR : dataJobTask.isTypeR,
            //                                                     isTypeX : dataJobTask.isTypeX,
            //                                                     PaidById : dataJobTask.PaidById,
            //                                                     Revision : dataJobTask.Revision
            //                                                   },
            //                                                   {
            //                                                     PauseReasonId : model.PauseReasonId,
            //                                                     PauseReasonDesc : model.PauseReasonDesc,
            //                                                     IsNeedRSA : model.IsNeedRSA,
            //                                                     IsNeedAdditionalTime : model.IsNeedAdditionalTime,
            //                                                     IsOwnDo : model.IsOwnDo,
            //                                                     Duration : model.Duration,
            //                                                     TroubleDesc : model.TroubleDesc,
            //                                                     ResponseDesc : model.ResponseDesc
            //                                                   }
            //                                             ]);
            // },
            clockJobPause: function(model, dataJobTask, JobTaskIdDtl, JobTaskIdJobBASE, stallid) {
                // console.log("model nih", model);
                var JobTask = [{
                    JobTaskId: dataJobTask.JobTaskId,
                    JobId: dataJobTask.JobId,
                    TaskId: dataJobTask.TaskId,
                    EmployeeId: dataJobTask.EmployeeId,
                    Employee: dataJobTask.Employee,
                    TaskName: dataJobTask.TaskName,
                    FR: dataJobTask.FR,
                    FlatRate: dataJobTask.FlatRate,
                    ActualRate: dataJobTask.ActualRate,
                    JobTypeId: dataJobTask.JobTypeId,
                    Fare: dataJobTask.Fare,
                    PlanStart: dataJobTask.PlanStart,
                    PlanFinish: dataJobTask.PlanFinish,
                    Status: dataJobTask.Status,
                    LastModifiedUserId: dataJobTask.LastModifiedUserId,
                    LastModifiedDate: dataJobTask.LastModifiedDate,
                    ProcessId: dataJobTask.ProcessId,
                    Satuan: dataJobTask.Satuan,
                    isTypeR: dataJobTask.isTypeR,
                    isTypeX: dataJobTask.isTypeX,
                    PaidById: dataJobTask.PaidById,
                    Revision: dataJobTask.Revision,
                    TFirst1: model.T1,
                    TFirst2: model.T2,
                    IsCustomDiscount: dataJobTask.IsCustomDiscount,
                    Discount: dataJobTask.Discount,
                    DiscountTypeId: dataJobTask.DiscountTypeId,
                    StallId: stallid,

                }];
                var routePause = '/api/as/JobTasks/Clock/pause';
                var TechnicalProblemDto = {
                    PauseReasonId: model.PauseReasonId,
                    PauseReasonDesc: model.PauseReasonDesc,
                    IsNeedRSA: model.IsNeedRSA,
                    IsNeedAdditionalTime: model.IsNeedAdditionalTime,
                    IsOwnDo: model.IsOwnDo,
                    Duration: model.Duration,
                    TroubleDesc: model.TroubleDesc,
                    ResponseDesc: model.ResponseDesc
                };
                var JobTaskProgressDetil = JobTaskIdDtl;
                // console.log("hahahah", TechnicalProblemDto);
                return $http.put(routePause, [{
                    JobTask: JobTask,
                    TechnicalProblemDto: TechnicalProblemDto,
                    JobTaskProgressDetil: JobTaskProgressDetil,
                    JobTaskId: JobTaskIdJobBASE.JobTaskId
                }]);
            },

            getFinalInspectionById: function(JobId) {
                var res = $http.get('/api/as/JobInspections/job/' + JobId + '');
                // console.log('resnya=>',res);
                return res;
            },

            postFinalInspection: function(JobId) {
                return $http.post('/api/as/JobInspections/job/' + JobId);
                // var res=$http.post('/api/as/JobInspections/job/'+JobId+'');
                // return res;
            },

            putFinalInspection: function(Data, Model, JobTaskIdFI, actualTime) {
                // console.log("model nih", model);
                // var jobInspection = {
                //                       Id :Data.Id,
                //                       JobId :Data.JobId,
                //                       Status :parseInt(Model.Status),
                //                       Remarks :Model.Remarks
                //                     };
                var routePutFI = '/api/as/JobInspections';
                // var jobTask = JobTaskIdFI;
                // console.log("Job INspection",jobInspection);
                // console.log("JobTask",JobTaskIdFI);
                // var test= {
                //   jobInspection: jobInspection,
                //   jobTask : jobTask,
                //   // OldPartKeepPlace : Model.OldPartKeepPlace,
                //   OldPartKeepPlace : 'gituu',
                //   isFixItRight : 1
                // };

                // console.log("terkirim", test);
                return $http.put(routePutFI, [{
                    jobInspection: {
                        Id: Data.Id,
                        JobTaskId: Data.JobTaskId,
                        JobId: Data.JobId,
                        Status: Model.StatusInspeksi,
                        // Status: Model.Status,
                        Remarks: Model.Remarks
                    },
                    jobTask: JobTaskIdFI,
                    OldPartKeepPlace: Model.OldPartKeepPlace,
                    isFixItRight: Model.isFixItRight,
                    IsFieldAction: Data.IsFieldAction,
                    Catatan: Model.Catatan,
                    IsChecked: parseInt(Data.IsChecked),
                    IsRepaired: parseInt(Data.IsRepaired),
                    ActualTime: actualTime,
                }]);
            },

            putT2: function(Data) {
                console.log("Data", Data);
                // console.log("model fac", Model);

                return $http.put('/api/as/JobTasks/updateJobTaskT2', Data);
            },

            postJobSuggest: function(JobId, Model) {
                console.log("jobid fac", JobId);
                console.log("model fac", Model);
                var SuggestionDate = '';
                if (Model.SuggestionDate == null || Model.SuggestionDate == undefined) {
                    SuggestionDate = null;
                } else {
                    var yearx = Model.SuggestionDate.getFullYear();
                    var monthx = Model.SuggestionDate.getMonth() + 1;
                    var datex = Model.SuggestionDate.getDate();
                    SuggestionDate = yearx.toString() + '-' + monthx.toString() + '-' + datex.toString();
                }

                return $http.put('/api/as/JobSuggestion', [{
                    JobId: JobId,
                    Suggestion: Model.Suggestion,
                    SuggestionCatg: Model.SuggestionCatg,
                    // SuggestionDate: Model.SuggestionDate
                    SuggestionDate: SuggestionDate
                }]);
            },

            putJobSuggestCRM: function(VIN, Model) {
                console.log("Vin CRM", VIN);
                console.log("model CRM", Model);

                return $http.put('/api/crm/PutVehicleListJobSuggest', [{
                    // VehicleId : VehicleId,
                    JobSuggest: Model.Suggestion,
                    VIN: VIN
                }]);
            },

            putJobSuggestCT: function(VIN, Model) {
                console.log("VIN CT", VIN);
                console.log("model CT", Model);

                return $http.put('/api/ct/PutVehicleListJobSuggest', [{
                    // VehicleId : VehicleId,
                    JobSuggest: Model.Suggestion,
                    VIN: VIN
                }]);
            },

            getDataTeknisi: function() {
                // var res=$http.get('/api/as/EmployeeRoles/2');Mprofile_Employee_LastPosition
                var res = $http.get('/api/as/Mprofile_Employee_LastPosition/TGR');
                // var res = $http.get('/api/as/Mprofile_Employee/TGR');
                // console.log('resnya employee=>',res);
                return res;
            },

            getEmployeeById: function(Dig) {
                console.log("dig", Dig);
                var res = $http.get('/api/as/Employees/100122');
                // console.log('resnya employee=>',res);
                return res;
            },

            TestDrive: function(JobId) {
                return $http.post('/api/as/Gates/PostFrom/2/' + JobId + '');
            },

            getJobSuggest: function(JobID) {
                var res = $http.get('/api/as/JobSuggestion/' + JobID + '');
                // console.log('resnya employee=>',res);
                return res;
            },

            getTeknisiTask: function(JobTaskID) {
                var res = $http.get('/api/as/JobTaskTechniciansByTaskId/' + JobTaskID);
                console.log('JOb Task Dari Teknisi=>', res);
                return res;
            },

            getVinFromCRM: function(vin) {
                var res = $http.get('/api/crm/GetVehicleListById/' + vin + '');
                console.log('Get Data VIN=>', res);
                return res;
            },

            getTeknisiNote: function(JobId) {
                var res = $http.get('/api/as/Jobs/getTechNote/' + JobId);
                console.log('Get Data NoteTeknisi=>', res);
                return res;
            },

            putTeknisiNote: function(JobId, Model) {
                console.log("jobid fac", JobId);
                console.log("model fac", Model);

                return $http.put('/api/as/jobs/putTechNote/' + JobId + '', [Model]);
            },
            getDataWac: function(JobId) {
                // console.log("JbId",JbId);
                var res = $http.get('/api/as/JobWACs/Job/' + JobId);
                // console.log('resnya=>',res);
                return res;
            },

            CheckIsJobCancelWO: function (jobId) {
                console.log("data di ReqCancelWO", jobId);
                return $http.get('/api/as/CheckIsJobCancelWO/'+jobId);
            },

            GetJobTasks: function (jobId) {
                console.log("data di GetJobTasks", jobId);
                // api/as/GetJobTasks/{JobId}
                return $http.get('/api/as/GetJobTasks/'+jobId);
            },

            addTask: function(Model, recepient, param) {
                console.log("model", recepient);

                return $http.post('/api/as/SendNotification', [{
                    Message: Model.Message,
                    RecepientId: recepient,
                    Param: param,
                    Ref: Model.Ref
                }]);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                // return $http.post('/api/as/lappet', [{
                //     Message: Model.Message,
                //     userIdSA: IdSA
                // }]);
            },

            updateFlagForAddTask: function(Model) {
                console.log("data", Model);

                return $http.put('/api/as/setFlreqjob/' + Model, [{
                    Flreqjob: 1
                }]);

            },

            GetDataBoard: function(currentDate, StallId) {
                return $http.get('/api/as/Boards/jpcb/bydate/' + currentDate + '/0/22/' + StallId);
            },
            sendNotify: function(data) {
                console.log("data notif", data.PoliceNumber);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotification', [{
                    Message: 'Ada Laporan Problem Finding ' + data.PoliceNumber + '/232323565', // id sementara hardcode
                    RecepientId: 1122, //role id partsman GR
                    Param: 37
                }]);
            },
            sendNotif: function(data, recepient, param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotification', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param: param,
                    Ref: data.Ref

                }]);
            },
            sendNotifRole: function(data, recepient, param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param: param
                }]);
            },

            CekOFP: function(data) {
                // api/as/CekOFP/
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.put('/api/as/CekOFP/', data);
            },

            

            GetStatusJobFromProduction: function(data) {
                var res = $http.get('/api/as/Jobs/GetStatusJobFromProduction/' + data.JobId + '/' + data.Status);
                return res;
            },
            checkGateIn: function(policeNo, jobId) {
                var res = $http.get('/api/as/JobInspections/GateInCheck/' + policeNo + '/' + jobId)
                return res;
            },
            getStatWO: function (JobId, status) {
                return $http.get('/api/as/Jobs/GetStatusJob/' + JobId + '/' + status);
            },
            CheckPrediagnoseGate: function (JobId, PoliceNo) {
                return $http.get('/api/as/CheckPrediagnoseGate/' + JobId + '/' + PoliceNo);
            },
            CheckKendaraanGateInOut: function(JobId,PoliceNo,flag) {
                //Flag 1 : Cek Dari OPL
                //Flag 2 : Cek Dari Final Inspection Test Drive
                //Flag 3 : Cek Dari Production Rawat Jalan
                //Flag 4 : Cek Dari Prediagnose Test Drive
                // belum gatein 666#TestDrive 666#OPL 666#RawatJalan 666#Prediagnose
                // belum gateout 999#TestDrive999#OPL 999#RawatJalan 999#Prediagnose
                var res = $http.get('/api/as/CheckKendaraanGateInOut/' + JobId + '/' + PoliceNo + '/' + flag);
                return res;
            },
            CekStatusClockOn: function (JobId, StallId) {
                // api/as/JobTasks/CekStatusClockOn/{JobId}/{StallId}
                return $http.get('/api/as/JobTasks/CekStatusClockOn/' + JobId + '/' + StallId);
            },


        };
    });