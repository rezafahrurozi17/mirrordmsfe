angular.module('app')
    .controller('RepairProcessGRController', function($scope, $q, $http, CurrentUser, Role, WOBP, RepairProcessGR, RepairSupportActivityGR, AppointmentGrService, $timeout, ngDialog, bsNotify, bsAlert, JpcbGrViewFactory, $filter, GeneralMaster, PreDiagnose, PreDiagnoseFirstCheck, PreDiagnoseDTC, WO) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------

        $scope.IdSA = null;
        $scope.user = CurrentUser.user();
        //console.log("user", $scope.user);
        $scope.mData = {}; //Model
        $scope.mFilter = null; //Model Untuk Filter
        $scope.xRole = { selected: [] };
        $scope.ShowRepair = false;
        $scope.JobShow = false;
        $scope.DataProblem = false;
        $scope.open = false;
        // $scope.color = {'background-color': 'white'};
        $scope.showPause = false;
        $scope.duration = false;
        $scope.StatusFixed = false;
        $scope.StatusNotFixed = false;
        $scope.ShowBtnKembali = false;
        $scope.btnFI = true;
        $scope.DamageVehicle = [];
        $scope.lmModelDTC = {};

        // var dayNow = dateNow.getDate();

        //$scope.color = 'lightgray';
        $scope.showFi = false;
        $scope.conditionData = [{ Id: 0, Name: "Not Ok" }, { Id: 1, Name: "Ok" }];
        // $scope.slider = {
        //     value: 0,
        //     options: {
        //         floor: 0,
        //         ceil: 100,
        //         showSelectionBar: true,
        //         translate: function(value) {
        //             return value + '%';
        //         }
        //     }
        // };
        WOBP.getDamage().then(function(res) {
            $scope.DamageVehicle = res.data.Result;
            _.map($scope.DamageVehicle, function(val) {
                val.Id = val.MasterId;
            })
            $scope.gridWac.columnDefs[2].editDropdownOptionsArray = $scope.DamageVehicle;
            console.log("$scope.dramagaaaa", $scope.DamageVehicle);
        })
        $scope.slider = {
            value: 0,
            options: {
                step: 3.125,
                hidePointerLabels: true,
                noSwitching: true,
                minRange: 1,
                floor: 0,
                ceil: 100,
                showSelectionBar: true,
                disabled: true,
                // translate: function(value) {
                //     return value + '%';
                // }
                translate: function(value, id, label) {
                    if (value == 0) {
                        return value = 'E'
                    } else if (value == 100) {
                        return value = 'F'
                    }
                }
            }
        };

        $scope.refreshSlider = function() {
            $timeout(function() {
                $scope.$broadcast('rzSliderForceRender');
            });
        };
        var currentDate;
        currentDate = new Date();
        currentDate.setDate(currentDate.getDate() + 1);
        $scope.mindatesuggestion = new Date(currentDate);
        $scope.dateToString = function(val) {
            var d = new Date(val);
            var n = d.getDate() < 10 ? '0' + d.getDate().toString() : d.getDate().toString();
            var e = d.getFullYear().toString();
            var m = d.getMonth() < 10 ? '0' + d.getMonth().toString() : d.getMonth().toString();
            return e + '-' + m + '-' + n;
        }

        $scope.allowPattern = function (event, type, item) {
            var patternRegex
            if (type == 1) {
                patternRegex = /\d/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /\d|[a-z]|[*]/i; //ALPHANUMERIC ONLY
            };
            console.log("event", event);
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (event.key === '*'){
                event.preventDefault();
                return false;
            }
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            };
        };

        $scope.allowPatternFilter = function(event, type, item) {
            //console.log("event", event);
            //console.log("item", item);

            if (event.charCode == 13) {
                var NoPol = "-";
                var fNoWo = "-";
                var Employee = "-";
                var FilterDate = "-";
                if (item.PoliceNumber == undefined || item.PoliceNumber == "" || item.PoliceNumber == null) {
                    NoPol = "-"
                } else {
                    NoPol = item.PoliceNumber
                };

                if (item.NoWo == undefined || item.NoWo == "" || item.NoWo == null) {
                    fNoWo = "-"
                } else {
                    fNoWo = item.NoWo
                };

                if (item.EmployeeId == undefined || item.EmployeeId == "" || item.EmployeeId == null) {
                    Employee = "-"
                } else {
                    Employee = item.EmployeeId
                };
                if (item.FilterDate == undefined || item.FilterDate == "" || item.FilterDate == null) {
                    FilterDate = "-"
                } else {
                    FilterDate = item.FilterDate
                };


                $scope.getData(NoPol, fNoWo, Employee, FilterDate);
            }

            var patternRegex
            if (type == 1) {
                patternRegex = /\d/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /\d|[a-z]|[*]/i; //ALPHANUMERIC ONLY
                // if (item.includes("*")) {
                //     event.preventDefault();
                //     return false;
                // }
            } else if (type == 3) {
                patternRegex = /\d|[a-z]/i;
            }
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }

        };
        $scope.getDataDefault = function() {
            var dateNow = new Date();
            var finalTime = ((dateNow.getHours() * 60) + dateNow.getMinutes());

            var yearNow = dateNow.getFullYear();
            var monthNow = dateNow.getMonth() + 1;
            var dayNow = dateNow.getDate();

            //console.log("finalTime", finalTime);
            //console.log("date", dateNow);
            var NoPol = "-";
            var fNoWo = "-";
            var Employee = "-";
            var FilterDate = "-";
            RepairProcessGR.getData(NoPol, fNoWo, Employee, FilterDate)
                .then(
                    function(res) {
                        $scope.DataWoList = res.data;
                        var CountWaitProd = [];
                        var CountProd = [];
                        var CountWaitFI = [];
                        if ($scope.DataWoList.length == 0) {

                            $scope.CountProdFinal = 0;
                            $scope.CountInProdFinal = 0;
                            $scope.CountWaitFiFinal = 0;
                        } else {
                            // var arr22 = [];
                            // _.map($scope.DataWoList, function(a) {
                            //     if (a.Status == 22) {
                            //         arr22.push(a);
                            //     }
                            // });

                            // if (arr22.length > 0) {
                            //     //console.log('arr22', arr22);
                            // }

                            for (var i = 0; i < $scope.DataWoList.length; i++) {
                                if (CountWaitFI.length == 0) {
                                    $scope.CountWaitFiFinal = 0;
                                }

                                if (CountProd.length == 0) {
                                    $scope.CountInProdFinal = 0;
                                }

                                if (CountWaitProd.length == 0) {
                                    $scope.CountProdFinal = 0;
                                }

                                if ($scope.DataWoList[i].Status == 4 || $scope.DataWoList[i].Status == 22 && $scope.DataWoList[i].StallId >= 0 && $scope.DataWoList[i].StallId !== null) {
                                    CountWaitProd.push($scope.DataWoList[i]);
                                    if (CountWaitProd.length == 0) {
                                        $scope.CountProdFinal = 0;
                                    } else {
                                        $scope.CountProdFinal = CountWaitProd.length;
                                    }
                                    //console.log("CountWaitProd", CountWaitProd);
                                    //console.log("$scope.CountProdFinal", $scope.CountProdFinal);
                                } else if (($scope.DataWoList[i].Status == 5 && $scope.DataWoList[i].StallId >= 0 && $scope.DataWoList[i].StallId !== null) || ($scope.DataWoList[i].Status == 6 && $scope.DataWoList[i].StallId >= 0 && $scope.DataWoList[i].StallId !== null) || ($scope.DataWoList[i].Status == 7 && $scope.DataWoList[i].StallId >= 0 && $scope.DataWoList[i].StallId !== null)) {
                                    CountProd.push($scope.DataWoList[i]);
                                    if (CountProd.length == 0) {
                                        $scope.CountInProdFinal = 0;
                                    } else {
                                        $scope.CountInProdFinal = CountProd.length;
                                    }
                                    //console.log("CountInProdFinal", $scope.CountInProdFinal);
                                } else if (($scope.DataWoList[i].Status == 8 && $scope.DataWoList[i].StallId >= 0 && $scope.DataWoList[i].StallId !== null) || ($scope.DataWoList[i].Status == 11 && $scope.DataWoList[i].StallId >= 0 && $scope.DataWoList[i].StallId !== null)) {
                                    CountWaitFI.push($scope.DataWoList[i]);
                                    //console.log("CountWaitFI.length", CountWaitFI.length);
                                    if (CountWaitFI.length == 0) {
                                        $scope.CountWaitFiFinal = 0;
                                    } else {
                                        $scope.CountWaitFiFinal = CountWaitFI.length;
                                    }
                                    //console.log("$scope.CountWaitFiFinal", $scope.CountWaitFiFinal);
                                }
                            }
                        }
                        console.log("data sesungguhnya", res.data);
                        $scope.DataWoList = res.data.map(function(item) {
                            var hasil = item;
                            console.log("hasil ", hasil);

                            console.log("hasil Plan Finish ", hasil.PlanFinish);

                            var jmltask = 0;
                            var adaJobTWC = 0;
                            var adaJobPWC = 0;
                            var adaMenungguParts = 0;
                            // MaterialRequestStatusId: 1 => kalau MaterialRequestStatusId = 1 berarti ada yang di tunggu(menunggu status part info)
                            if (hasil.JobTask != undefined || hasil.JobTask != null) {
                                jmltask = hasil.JobTask.length;

                                for (var i = 0; i < hasil.JobTask.length; i++) {
                                    if (hasil.JobTask[i].JobType != null && hasil.JobTask[i].JobType != undefined) {
                                        if (hasil.JobTask[i].JobType.Name == 'TWC' && hasil.JobTask[i].JobType.Name != null && hasil.JobTask[i].JobType.Name != undefined) {
                                            adaJobTWC = 1;
                                        }
                                        if (hasil.JobTask[i].JobType.Name == 'PWC' && hasil.JobTask[i].JobType.Name != null && hasil.JobTask[i].JobType.Name != undefined) {
                                            adaJobPWC = 1;
                                        }
                                    }

                                    if (hasil.JobTask[i].JobParts != null && hasil.JobTask[i].JobParts != undefined) {
                                        for (var j = 0; j < hasil.JobTask[i].JobParts.length; j++) {
                                            if (hasil.JobTask[i].JobParts[j].MaterialRequestStatusId != null && hasil.JobTask[i].JobParts[j].MaterialRequestStatusId != undefined) {
                                                if (hasil.JobTask[i].JobParts[j].MaterialRequestStatusId == 1) {
                                                    adaMenungguParts = 1;
                                                }
                                            }
                                        }

                                    }
                                }

                            }

                            // $scope.DataWoList.jmltask = jmltask;
                            // $scope.DataWoList.adaJobTWC = adaJobTWC;
                            // $scope.DataWoList.adaJobPWC = adaJobPWC;
                            item.jmltask = jmltask;
                            item.adaJobTWC = adaJobTWC;
                            item.adaJobPWC = adaJobPWC;
                            item.adaMenungguParts = adaMenungguParts;

                            // else if(){

                            // }

                            if ((item.isAppointment == 0 && item.Status == 4) || (item.isAppointment == 0 && item.Status == 22) || (item.isAppointment == 0 && item.Status == 8) || (item.isAppointment == 0 && item.Status == 11)) {
                                var datePlan = new Date(item.PlanDateFinish);

                                // //console.log('datePlan',datePlan);
                                if (item.PlanFinish !== null) {
                                    var yearFirst = datePlan.getFullYear();
                                    var monthFirst = datePlan.getMonth() + 1;
                                    var dayFirst = datePlan.getDate();

                                    var Jam = item.PlanFinish;
                                    // //console.log('Jam',Jam);
                                    var JamFinal = parseInt(Jam.slice(0, 2));
                                    // //console.log("JamFinal",JamFinal);
                                    var Menit = item.PlanFinish;
                                    var MenitFinal = parseInt(Menit.slice(3, 5));
                                    var FinalAppTime = (JamFinal * 60) + MenitFinal;
                                    var TempMenit = Menit.split(':');
                                    var splitJam = TempMenit[0];
                                    var splitMenit = TempMenit[1];
                                    var splitDetik = TempMenit[2];

                                    var a = parseInt(splitMenit) + 30;
                                    var b = splitJam + ":" + a.toString() + ":" + splitDetik
                                    $scope.JamPenyerahan = b;

                                    // console.log("Jam Penyerahan : ", b);
                                    // console.log("Menit : ", Menit);
                                    // console.log("splitJam : ", splitJam, "splitMenit : ", splitMenit);
                                    //console.log("dayNow >= dayFirst", dayNow, dayFirst, monthNow, monthFirst, yearNow, yearFirst, finalTime, FinalAppTime, item.PoliceNumber);
                                    if (dayNow <= dayFirst && monthNow <= monthFirst && yearNow <= yearFirst && finalTime < FinalAppTime) {
                                        // if(){
                                        hasil.color = "green";
                                        // }else{
                                        //     hasil.color = "red";
                                        // }
                                    } else {
                                        hasil.color = "red";
                                    }
                                } else {

                                    hasil.color = "red";
                                }
                            } else if ((item.isAppointment == 1 && item.Status == 4) || (item.isAppointment == 1 && item.Status == 22) || (item.isAppointment == 1 && item.Status == 8) || (item.isAppointment == 1 && item.Status == 11)) {
                                var datePlan = new Date(item.PlanDateFinish);

                                // //console.log('datePlan',datePlan);
                                if (item.PlanFinish !== null) {
                                    var yearFirst = datePlan.getFullYear();
                                    var monthFirst = datePlan.getMonth() + 1;
                                    var dayFirst = datePlan.getDate();

                                    var Jam = item.PlanFinish;
                                    // //console.log('Jam',Jam);
                                    var JamFinal = parseInt(Jam.slice(0, 2));
                                    // //console.log("JamFinal",JamFinal);
                                    var Menit = item.PlanFinish;
                                    var MenitFinal = parseInt(Menit.slice(3, 5));
                                    var FinalAppTime = (JamFinal * 60) + MenitFinal;
                                    var TempMenit = Menit.split(':');
                                    var splitJam = TempMenit[0];
                                    var splitMenit = TempMenit[1];
                                    var splitDetik = TempMenit[2];

                                    var a = parseInt(splitMenit) + 30;
                                    var b = splitJam + ":" + a.toString() + ":" + splitDetik
                                    $scope.JamPenyerahan = b;

                                    console.log("Jam Penyerahan : ", b);
                                    console.log("Menit : ", Menit);
                                    console.log("splitJam : ", splitJam, "splitMenit : ", splitMenit);
                                    //console.log("dayNow >= dayFirst", dayNow, dayFirst, monthNow, monthFirst, yearNow, yearFirst, finalTime, FinalAppTime, item.PoliceNumber);
                                    if (dayNow <= dayFirst && monthNow <= monthFirst && yearNow <= yearFirst && finalTime < FinalAppTime) {
                                        // if(){
                                        hasil.color = "blue";
                                        // }else{
                                        //     hasil.color = "red";
                                        // }
                                    } else {
                                        hasil.color = "red";
                                    }
                                } else {

                                    hasil.color = "red";
                                }
                            } else if (item.Status == 6 || item.Status == 25) {
                                hasil.color = "red";
                            } else if (item.Status == 5 || item.Status == 7) {
                                var datePlan = new Date(item.PlanDateFinish);

                                // //console.log('datePlan',datePlan);
                                if (item.PlanFinish !== null) {
                                    var yearFirst = datePlan.getFullYear();
                                    var monthFirst = datePlan.getMonth() + 1;
                                    var dayFirst = datePlan.getDate();

                                    var Jam = item.PlanFinish;
                                    // //console.log('Jam',Jam);
                                    var JamFinal = parseInt(Jam.slice(0, 2));
                                    // //console.log("JamFinal",JamFinal);
                                    var Menit = item.PlanFinish;
                                    var MenitFinal = parseInt(Menit.slice(3, 5));
                                    var FinalAppTime = (JamFinal * 60) + MenitFinal;
                                    var TempMenit = Menit.split(':');
                                    var splitJam = TempMenit[0];
                                    var splitMenit = TempMenit[1];
                                    var splitDetik = TempMenit[2];

                                    var a = parseInt(splitMenit) + 30;
                                    var b = splitJam + ":" + a.toString() + ":" + splitDetik
                                    $scope.JamPenyerahan = b;

                                    console.log("Jam Penyerahan : ", b);
                                    console.log("Menit : ", Menit);
                                    console.log("splitJam : ", splitJam, "splitMenit : ", splitMenit);
                                    //console.log("dayNow >= dayFirst", dayNow, dayFirst, monthNow, monthFirst, yearNow, yearFirst, finalTime, FinalAppTime, item.PoliceNumber);
                                    if (dayNow <= dayFirst && monthNow <= monthFirst && yearNow <= yearFirst && finalTime < FinalAppTime) {
                                        // if(){
                                        hasil.color = "yellow";
                                        // }else{
                                        //     hasil.color = "red";
                                        // }
                                    } else {
                                        hasil.color = "red";
                                    }
                                } else {

                                    hasil.color = "red";
                                }
                            } else {
                                hasil.color = "white";
                            }
                            return hasil;

                            // if(item.JobTask.every(function(x){
                            //   //console.log("ini Task",x);
                            //   return x.Status == 1 || x.Status == 4;
                            // })){
                            //   hasil.color = "yellow";
                            //   // hasil.color = "green";

                            // }else if(item.JobTask.some(function(x){
                            //   // //console.log("ini Task",x);
                            //   return x.Status == 2;
                            // })){

                            //     var webgriz = item.JobTask.map(function(final){
                            //       var JobtaskProgressF = final.JobTaskProgress;
                            //       //console.log("JobtaskProgressF",JobtaskProgressF);

                            //       var DataCount = final.JobTaskProgress.length;
                            //       //console.log("DataCount",DataCount);

                            //       var test = _.sortBy(JobtaskProgressF, ['ProblemFoundedDate'], ['asc']);
                            //       if (test[DataCount - 1].PauseReasonId == 45) {
                            //           hasil.color = 'red';
                            //       } else {
                            //           hasil.color = 'yellow';
                            //       }

                            //     })

                            // }else{
                            //   hasil.color = "green";

                            // }

                            // return hasil;
                        });
                        //console.log("ini nih", $scope.DataWoList);
                    },
                    function(err) {
                        //console.log("err=>", err);
                    }
                );
        }

        $scope.getData = function(NoPolice, NoWO, EmployeeId, FilterDate) {
            console.log("NoPolice", NoPolice);
            console.log("NoWO", NoWO);
            console.log("EmployeeId", EmployeeId);
            console.log("FilterDate", FilterDate);
            var dateNow = new Date();
            var finalTime = ((dateNow.getHours() * 60) + dateNow.getMinutes());

            var yearNow = dateNow.getFullYear();
            var monthNow = dateNow.getMonth() + 1;
            var dayNow = dateNow.getDate();

            var NoPol = "-";
            var fNoWo = "-";
            var Employee = "-";
            var filterDate = "-";
            if (NoPolice == undefined || NoPolice == "" || NoPolice == null) {
                NoPol = "-"
            } else {
                NoPol = NoPolice
            };

            if (NoWO == undefined || NoWO == "" || NoWO == null) {
                fNoWo = "-"
            } else {
                fNoWo = NoWO
            };

            if (EmployeeId == undefined || EmployeeId == "" || EmployeeId == null) {
                Employee = "-"
            } else {
                Employee = EmployeeId
            };

            if (FilterDate == undefined || FilterDate == "" || FilterDate == null) {
                filterDate = "-"
            } else {
                filterDate = FilterDate
            };

            console.log("NoPol", NoPol);
            console.log("fNoWo", fNoWo);
            console.log("Employee", Employee);
            console.log("filterDate", filterDate);
            RepairProcessGR.getData(NoPol, fNoWo, Employee, filterDate)
                .then(
                    function(res) {
                        // if (Employee !== "-") {
                        //     var data = res.data;

                        //     var evens = _.remove(data, function(n) {
                        //         for (var i = 0; i < n.JobTask.length; i++) {
                        //             // //console.log("job Task", n.JobTask[i]);
                        //             // if(n.JobTask[i].JobTaskTechnician.length !== 0){

                        //             for (var a = 0; a < n.JobTask[i].JobTaskTechnician.length; a++) {
                        //                 // //console.log("JOb Teknisi", n.JobTask[i]);
                        //                 return n.JobTask[i].JobTaskTechnician[a].MProfileTechnicianId == Employee;
                        //             }
                        //             // }
                        //         }
                        //     });
                        //     $scope.DataWoList = angular.copy(evens);
                        //     //console.log("Data Setelah Remove", evens);
                        // } else {
                        //     $scope.DataWoList = res.data;
                        //     console.log("$scope.DataWoList", $scope.DataWoList);
                        // }

                        var dataRepairProcessGR = res.data;
                        console.log("dataRepairProcessGR", dataRepairProcessGR);
                        // var isFiltered = false;
                        // if(Employee != "-" || NoPol != "-" || fNoWo != "-" || filterDate != "-"){
                        //     isFiltered = true;
                        // }

                        // for(var i = 0; i < res.data.length; i++){
                        //     var isNoPol = false;
                        //     var isWO = false;
                        //     var isTanggal = false;    
                        //     var isEmployee = false;

                        //     if(Employee != "-"){
                        //         for(var j = 0; j < res.data[i].JobTask.length; j++){
                        //             for(var k = 0; k < res.data[i].JobTask[j].JobTaskTechnician.length; k++){
                        //                 if(res.data[i].JobTask[j].JobTaskTechnician[k].MProfileTechnicianId == Employee){
                        //                     isEmployee = true;
                        //                 }
                        //             }
                        //         }
                        //     }
                        //     if(NoPol != "-"){
                        //         if(res.data[i].PoliceNumber != null){                                                                                                        
                        //             if(res.data[i].PoliceNumber.toLowerCase().indexOf(NoPol.toLowerCase()) != -1){                                                                            
                        //                 isNoPol = true;                                        
                        //             }
                        //         }                                
                        //     }
                        //     if(fNoWo != "-"){
                        //         if(res.data[i].WoNo != null){                                                                       
                        //             if(res.data[i].WoNo.toLowerCase().indexOf(fNoWo.toLowerCase()) != -1){
                        //                 isWO = true;

                        //             }
                        //         }                                
                        //     }
                        //     if(filterDate != "-"){
                        //         if(res.data[i].JobDate != null){                                                                          
                        //             if($scope.dateToString(res.data[i].JobDate) == $scope.dateToString(filterDate)){
                        //                 isTanggal = true;                                      
                        //             }
                        //         }                                
                        //     }
                        //     if(isFiltered){                                
                        //         if(isEmployee || isNoPol || isWO || isTanggal){                                  
                        //             dataRepairProcessGR.push(res.data[i]);
                        //             console.log(res.data[i].PoliceNumber, res.data[i]);
                        //         }                                
                        //     }                            
                        //     else {                                
                        //         dataRepairProcessGR.push(res.data[i]);
                        //     }                                                       
                        // }                        
                        $scope.DataWoList = dataRepairProcessGR;

                        var CountWaitProd = [];
                        var CountProd = [];
                        var CountWaitFI = [];
                        //console.log("$scope.DataWoList.length", $scope.DataWoList.length);
                        if ($scope.DataWoList.length == 0) {
                            $scope.CountProdFinal = 0;
                            $scope.CountInProdFinal = 0;
                            $scope.CountWaitFiFinal = 0;
                        } else {
                            // var arr22 = [];
                            // _.map($scope.DataWoList, function(a) {
                            //     if (a.Status == 22) {
                            //         arr22.push(a);
                            //     }
                            // });

                            // if (arr22.length > 0) {
                            //     //console.log('arr22', arr22);
                            // };

                            for (var i = 0; i < $scope.DataWoList.length; i++) {
                                if ($scope.DataWoList[i].Status == 4 && $scope.DataWoList[i].StallId >= 0) {
                                    CountWaitProd.push($scope.DataWoList[i]);
                                    if ($scope.DataWoList[i].length == 0) {
                                        $scope.CountProdFinal = 0;
                                        //console.log("ini kosong");
                                    } else {
                                        $scope.CountProdFinal = CountWaitProd.length;
                                    }
                                    // //console.log("CountWaitProd",CountWaitProd);
                                    //console.log("CountWaitProd", CountWaitProd.length);
                                } else if (($scope.DataWoList[i].Status == 5 && $scope.DataWoList[i].StallId >= 0) || ($scope.DataWoList[i].Status == 6 && $scope.DataWoList[i].StallId >= 0) || ($scope.DataWoList[i].Status == 7 && $scope.DataWoList[i].StallId >= 0)) {

                                    CountProd.push($scope.DataWoList[i]);
                                    if ($scope.DataWoList[i].length == 0) {
                                        $scope.CountInProdFinal = 0;
                                    } else {
                                        $scope.CountInProdFinal = CountProd.length;
                                    }
                                } else if (($scope.DataWoList[i].Status == 8 && $scope.DataWoList[i].StallId >= 0) || ($scope.DataWoList[i].Status == 11 && $scope.DataWoList[i].StallId >= 0)) {
                                    // if(item.Status == 8 || item.Status == 11){
                                    CountWaitFI.push($scope.DataWoList[i]);
                                    if ($scope.DataWoList[i].length == 0) {
                                        $scope.CountWaitFiFinal = 0;
                                    } else {
                                        $scope.CountWaitFiFinal = CountWaitFI.length;
                                    }
                                }

                            }
                        }
                        $scope.DataWoList.map(function(item) {
                            var hasil = item;
                            // //console.log("ini job",item);

                            // if(item.JobTask.every(function(x){
                            //   // //console.log("ini Task",x);
                            //   return x.Status == 1 || x.Status == 4;
                            // })){
                            //   hasil.color = "green";

                            // }else if(item.JobTask.some(function(x){
                            //   // //console.log("ini Task",x);
                            //   return x.Status == 2;
                            // })){
                            //     var webgriz = item.JobTask.map(function(final){
                            //       var JobtaskProgressF = final.JobTaskProgress;
                            //       //console.log("JobtaskProgressF",JobtaskProgressF);

                            //       var DataCount = final.JobTaskProgress.length;
                            //       //console.log("DataCount",DataCount);

                            //       var test = _.sortBy(JobtaskProgressF, ['ProblemFoundedDate'], ['asc']);
                            //       if (test[DataCount - 1].PauseReasonId == 45) {
                            //           hasil.color = 'red';
                            //       } else {
                            //           hasil.color = 'yellow';
                            //       }
                            //       // if(final.JobTaskProgress[final.JobTaskProgress.length-1].PauseReasonId == 45){
                            //       //     if(final.JobTaskProgress.some(function(wow){
                            //       //       //console.log("wow",wow);
                            //       //       return wow.PauseReasonId == 45;
                            //       //     })){
                            //       //       hasil.color = "red";
                            //       //     }
                            //       // }else{
                            //       //   hasil.color = "yellow";
                            //       // }

                            //     })
                            // }else{
                            //   hasil.color = "green";

                            // }
                            // if ((item.isAppointment == 0 && item.Status == 4) || (item.isAppointment == 0 && item.Status == 8) || (item.isAppointment == 0 && item.Status == 11)) {
                            //     hasil.color = "green";
                            // } else if ((item.isAppointment == 1 && item.Status == 4) || (item.isAppointment == 1 && item.Status == 8) || (item.isAppointment == 1 && item.Status == 11)) {
                            //     hasil.color = "blue";
                            // } else if (item.Status == 6) {
                            //     hasil.color = "red";
                            // } else if (item.Status == 5 || item.Status == 7) {
                            //     hasil.color = "yellow";
                            // } else {
                            //     hasil.color = "white";
                            // }

                            if ((item.isAppointment == 0 && item.Status == 4) || (item.isAppointment == 0 && item.Status == 22) || (item.isAppointment == 0 && item.Status == 8) || (item.isAppointment == 0 && item.Status == 11)) {
                                var datePlan = new Date(item.PlanDateFinish);

                                // //console.log('datePlan',datePlan);
                                if (item.PlanFinish !== null) {
                                    var yearFirst = datePlan.getFullYear();
                                    var monthFirst = datePlan.getMonth() + 1;
                                    var dayFirst = datePlan.getDate();

                                    var Jam = item.PlanFinish;
                                    // //console.log('Jam',Jam);
                                    var JamFinal = parseInt(Jam.slice(0, 2));
                                    // //console.log("JamFinal",JamFinal);
                                    var Menit = item.PlanFinish;
                                    var MenitFinal = parseInt(Menit.slice(3, 5));
                                    var FinalAppTime = (JamFinal * 60) + MenitFinal;
                                    console.log("MenitFinal 4 : ", MenitFinal);
                                    //console.log("dayNow >= dayFirst", dayNow, dayFirst, monthNow, monthFirst, yearNow, yearFirst, finalTime, FinalAppTime, item.PoliceNumber);
                                    if (dayNow <= dayFirst && monthNow <= monthFirst && yearNow <= yearFirst && finalTime < FinalAppTime) {
                                        // if(){
                                        hasil.color = "green";
                                        // }else{
                                        //     hasil.color = "red";
                                        // }
                                    } else {
                                        hasil.color = "red";
                                    }
                                } else {

                                    hasil.color = "red";
                                }
                            } else if ((item.isAppointment == 1 && item.Status == 4) || (item.isAppointment == 1 && item.Status == 22) || (item.isAppointment == 1 && item.Status == 8) || (item.isAppointment == 1 && item.Status == 11)) {
                                var datePlan = new Date(item.PlanDateFinish);

                                // //console.log('datePlan',datePlan);
                                if (item.PlanFinish !== null) {
                                    var yearFirst = datePlan.getFullYear();
                                    var monthFirst = datePlan.getMonth() + 1;
                                    var dayFirst = datePlan.getDate();

                                    var Jam = item.PlanFinish;
                                    // //console.log('Jam',Jam);
                                    var JamFinal = parseInt(Jam.slice(0, 2));
                                    // //console.log("JamFinal",JamFinal);
                                    var Menit = item.PlanFinish;
                                    var MenitFinal = parseInt(Menit.slice(3, 5));
                                    var FinalAppTime = (JamFinal * 60) + MenitFinal;
                                    console.log("MenitFinal 5 : ", MenitFinal);
                                    //console.log("dayNow >= dayFirst", dayNow, dayFirst, monthNow, monthFirst, yearNow, yearFirst, finalTime, FinalAppTime, item.PoliceNumber);
                                    if (dayNow <= dayFirst && monthNow <= monthFirst && yearNow <= yearFirst && finalTime < FinalAppTime) {
                                        // if(){
                                        hasil.color = "blue";
                                        // }else{
                                        //     hasil.color = "red";
                                        // }
                                    } else {
                                        hasil.color = "red";
                                    }
                                } else {

                                    hasil.color = "red";
                                }
                            } else if (item.Status == 6 || item.Status == 25) {
                                hasil.color = "red";
                            } else if (item.Status == 5 || item.Status == 7) {
                                var datePlan = new Date(item.PlanDateFinish);

                                // //console.log('datePlan',datePlan);
                                if (item.PlanFinish !== null) {
                                    var yearFirst = datePlan.getFullYear();
                                    var monthFirst = datePlan.getMonth() + 1;
                                    var dayFirst = datePlan.getDate();

                                    var Jam = item.PlanFinish;
                                    // //console.log('Jam',Jam);
                                    var JamFinal = parseInt(Jam.slice(0, 2));
                                    // //console.log("JamFinal",JamFinal);
                                    var Menit = item.PlanFinish;
                                    var MenitFinal = parseInt(Menit.slice(3, 5));
                                    var FinalAppTime = (JamFinal * 60) + MenitFinal;
                                    console.log("MenitFinal 6 : ", MenitFinal);
                                    //console.log("dayNow >= dayFirst", dayNow, dayFirst, monthNow, monthFirst, yearNow, yearFirst, finalTime, FinalAppTime, item.PoliceNumber);
                                    if (dayNow <= dayFirst && monthNow <= monthFirst && yearNow <= yearFirst && finalTime < FinalAppTime) {
                                        // if(){
                                        hasil.color = "yellow";
                                        // }else{
                                        //     hasil.color = "red";
                                        // }
                                    } else {
                                        hasil.color = "red";
                                    }
                                } else {

                                    hasil.color = "red";
                                }
                            } else {
                                hasil.color = "white";
                            }
                            return hasil;
                        });
                        //console.log("data sesungguhnya", $scope.DataWoList);
                    },
                    function(err) {
                        //console.log("err=>", err);
                    }
                );
            // }

        }

        $scope.dataTeknisi = function() {
            // $scope.EmployeeId = [];
            RepairProcessGR.getDataTeknisi()
                .then(

                    function(res) {
                        $scope.EmployeeId = res.data.Result;
                        //console.log("Data Teknisi2", res.data);
                    },
                    function(err) {
                        //console.log("err=>", err);
                    }
                );

        }

        $scope.selectFilter = function(row) {
            if (row.ChoseFilterId != undefined && row.ChoseFilterId == 1) {
                $scope.mData.NoWo = "";
            } else if (row.ChoseFilterId != undefined && row.ChoseFilterId == 2) {
                $scope.mData.PoliceNumber = "";
            }
        }

        $scope.getDataPause = function() {

            // document.write(today);
            $scope.PauseResaon = [];
            RepairProcessGR.getPauseReason()
                .then(

                    function(res) {
                        $scope.PauseReasonId = res.data.Result;
                    },
                    function(err) {
                        //console.log("err=>", err);
                    }
                );

            RepairProcessGR.getDataJobTask($scope.JbId).then(
                function(res) {
                    $scope.DataPF = res.data.Result;
                    //console.log("Data pf", $scope.DataPF);
                },
                function(err) {
                    //console.log("err=>", err);
                }
            );

            // var newDataWoList = angular.copy($scope.DataWoList);

        }

        $scope.Kembali = function() {
            $scope.ShowRepair = false;
            $scope.DataProblem = false;
            $scope.showPause = false;
            $scope.ShowBtnKembali = false;
            $scope.btnClockPause = true;
            console.log('status pause disabled ga1', $scope.btnClockPause)
            $scope.btnClockOff = true;
            $scope.btnClockOffJob = false;

            $scope.btnJobSuggest = true;
            $scope.getDataDefault();
            $scope.mData = {};
            //$scope.filter = [];

            $scope.imageSel = null;
            // $scope.mData.CategoryJobSuggestKeamanan = false;
            // $scope.mData.CategoryJobSuggestPeraturan = false;
            // $scope.mData.CategoryJobSuggestKenyamanan = false;
            // $scope.mData.CategoryJobSuggestEfisiensi = false;
            // $scope.mData.IsRepaired = 2;
            // $scope.mData.Status = 2;
            // $scope.mData.Remarks = "";
            // $scope.mData.isFixItRight = 2;
            // $scope.mData.OldPartKeepPlace = "";
        }

        $scope.show_modal = { show: false };
        $scope.modal_model = [];

        $scope.listView = {
            new: { enable: false },
            view: { enable: false },
            delete: { enable: false },
            select: { enable: false },
            selectall: { enable: false },
            edit: { enable: false },
        }

        $scope.getDataPrediagnose = function() {
            RepairProcessGR.getPrediagnose($scope.JbId)
                .then(
                    function(res) {
                        // //console.log("data jobid", res.data.Result);
                        PreDiagnoseFirstCheck.GetDataFirstCheck(res.data.Result[0].PrediagnoseId)
                            .then(
                                function(res) {
                                    $scope.DataPemeriksaanAwal = res.data.Result;
                                    //console.log("First Check", $scope.DataPemeriksaanAwal);
                                    // $scope.loading=false;
                                },
                                function(err) {
                                    //console.log("err=>", err);
                                }
                            );
                        PreDiagnoseDTC.GetDataDTC(res.data.Result[0].PrediagnoseId)
                            .then(
                                function(res) {
                                    $scope.DataDTC = res.data.Result;
                                    //console.log("DTC", $scope.DataDTC);
                                    // $scope.loading=false;
                                },
                                function(err) {
                                    //console.log("err=>", err);
                                }
                            );

                        PreDiagnose.getDataDetail(res.data.Result[0].PrediagnoseId)
                            .then(
                                function(res) {
                                    //console.log("data PreDiagnose by jobid", res.data);

                                    // $rootScope.theme = 'ngdialog-theme-plain custom-width';
                                    ngDialog.openConfirm({
                                        template: 'app/services/production/repairProcessGR/repairProcessGR_Prediagnose.html',
                                        // controller: 'WOController',
                                        className: 'ngdialog-theme-plain custom-width',
                                        scope: $scope
                                    });

                                    $scope.mData = res.data;


                                    //untuk bagian Posisi Shift Lever
                                    var co = ["PosisiShiftP", "PosisiShiftN", "PosisiShiftD", "PosisiShiftS", "PosisiShiftR", "PosisiShiftOne", "PosisiShiftTwo", "PosisiShiftThree", "PosisiShiftFour", "PosisiShiftLainnya"];

                                    var total = $scope.mData.PositionShiftLever;
                                    var arrBin = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512];
                                    for (var i in arrBin) {
                                        if (total & arrBin[i]) {
                                            var xc = co[i];
                                            $scope.mData[xc] = arrBin[i];


                                            // //console.log(co[i]+' : nyala');
                                        }
                                    }
                                    //akhir bagian posisi shift lever

                                    //untuk bagian posisi Problem R
                                    var CheckBxProblemR = ["KlasifikasiMasalahDKota", "KlasifikasiMasalahLKota", "KlasifikasiMasalahTol", "KlasifikasiMasalahBelok", "KlasifikasiMasalahLurus", "KlasifikasiMasalahDatar", "KlasifikasiMasalahTanjakan", "KlasifikasiMasalahTurunan", "KlasifikasiMasalahAkselerasi", "KlasifikasiMasalahPengereman", "KlasifikasiMasalahLainnya"];

                                    var TotalProblemR = $scope.mData.ClassificationProblemR;
                                    var arrBinProblemR = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024];
                                    for (var a in arrBinProblemR) {
                                        if (TotalProblemR & arrBinProblemR[a]) {
                                            var resProblemR = CheckBxProblemR[a];
                                            $scope.mData[resProblemR] = arrBinProblemR[a];
                                        }
                                    }
                                    //akhir untuk bagian posisi Problem R

                                    //untuk bagian Lalu lintas
                                    var CheckBxTraffic = ["LaluLintasMct", "LaluLintasLancar", "LaluLintasLainnya"];

                                    var TotalTraffic = $scope.mData.Traffic;
                                    var arrBinTraffic = [1, 2, 4];

                                    for (var Tr in arrBinTraffic) {
                                        if (TotalTraffic & arrBinTraffic[Tr]) {
                                            var resTraffic = CheckBxTraffic[Tr];
                                            $scope.mData[resTraffic] = arrBinTraffic[Tr];
                                        }
                                    }
                                    //akhir bagian Lalu lintas

                                    //untuk bagian Kondisi Cuaca
                                    var CheckBxWeather = ["KondisiCuacaCerah", "KondisiCuacaBerawan", "KondisiCuacaHujan", "KondisiCuacaPanas", "KondisiCuacaLembab"];

                                    var TotalWeather = $scope.mData.WeatherConditions;
                                    var arrBinWeather = [1, 2, 4, 8, 16];

                                    for (var j in arrBinWeather) {
                                        if (TotalWeather & arrBinWeather[j]) {
                                            var resWeather = CheckBxWeather[j];
                                            $scope.mData[resWeather] = arrBinWeather[j];

                                            // //console.log(CheckBxWeather[j]+' : Nyala');
                                        }
                                    }
                                    //akhir bagian kondisi cuaca

                                    //klasifikasi masalah pada kendaraan
                                    var CheckProblemM = ["Engine", "Suspension", "Rem", "Lainnya"];

                                    var TotalProblemM = $scope.mData.ClassificationProblemM;
                                    var arrProblemM = [1, 2, 4, 8];

                                    for (var c in arrProblemM) {
                                        if (TotalProblemM & arrProblemM[c]) {
                                            var resProblemM = CheckProblemM[c];
                                            $scope.mData[resProblemM] = arrProblemM[c];
                                        }
                                    }
                                    //akhir klasifikasi masalah pada kendaraan

                                    //untuk bagian Posisi DUduk
                                    var CheckBxPosition = ["Auto", "Posisi1", "Posisi2", "Posisi3", "Posisi4", "Posisi5"];

                                    var TotalPosition = $scope.mData.SittingPosition;
                                    var arrBinPosition = [1, 2, 4, 8, 16, 32];

                                    for (var j in arrBinPosition) {
                                        if (TotalPosition & arrBinPosition[j]) {
                                            var resPosition = CheckBxPosition[j];
                                            $scope.mData[resPosition] = arrBinPosition[j];

                                            // //console.log(CheckBxWeather[j]+' : Nyala');
                                        }
                                    }
                                    //akhir bagian Posisi DUduk
                                },
                                function(err) {
                                    //console.log("err=>", err);
                                }
                            );
                    },
                    function(err) {
                        //console.log("err=>", err);
                    }
                );
            $timeout(function() {
                $('.containeramk').width(500);
                $('.containeramk').height(165);
            }, 500);
        }

        $scope.Cancel = function() {
            //console.log("back");
        }

        var Vin = {};
        $scope.btnClockOn = true;
        $scope.hideBtnClokON = false;
        $scope.btnClockPause = true;
        console.log('status pause disabled ga2', $scope.btnClockPause)

        $scope.btnClockOff = true;
        $scope.btnClockOffJob = false;

        $scope.DataWarranty = [];

        var mstId = null;


        GeneralMaster.getData(2030).then(
            function(res) {
                $scope.VTypeData = res.data.Result;
                //console.log("VTypeData", $scope.VTypeData);
                // //console.log("$scope.UomData : ",$scope.UomData);
                return res.data;
            }
        );

        RepairProcessGR.getAllTypePoints().then(
            function(res) {
                // //console.log(res);
                $scope.imageWACArr = res.data.Result;
                //console.log("$scope.imageWACArr : ", $scope.imageWACArr);
                $scope.loading = false;
                return res.data;
            },
            function(err) {
                //console.log("err=>", err);
            }
        );

        var dataDetailJob = [];
        var jobtaskFinalData = [];
        var T1Final;
        $scope.getDataDetail = function(JobId, SelectedJobTask) {
            $scope.disabletestdrive = false;
            $scope.JbId = JobId;
            $scope.ShowRepair = true;
            $scope.ShowBtnKembali = true;
            $scope.DataWarranty = [];
            $scope.mData.StatusInspeksi = null;
            if (SelectedJobTask == 1) {
                $scope.mDataJob.JobTaskF = null;
            }
            var selectedID = [];

            RepairProcessGR.getPauseReason()
                .then(

                    function(res) {
                        $scope.PauseReasonId = res.data.Result;
                    },
                    function(err) {
                        //console.log("err=>", err);
                    }
                );

            RepairProcessGR.getDataDetail(JobId).then(function(res) {
                $scope.copy_dataTask = angular.copy(res.data.Result[0].JobTask)
                var udaPernahFI = 0;
                    if (res.data.Result[0].JobListSplitChip != null){
                        if (res.data.Result[0].JobListSplitChip.length > 0){
                            // cek uda ada yang pernah lolos FI belum pekerjaannya
                            // kl uda ada, ambil yang belum nya aja
                            for (var i=0; i<res.data.Result[0].JobListSplitChip.length; i++){
                                if (res.data.Result[0].JobListSplitChip[i].isFI == 1){
                                    udaPernahFI++;
                                }
                            }
                        }
                    }
                    if (udaPernahFI > 0){
                        for (var i=0; i<res.data.Result[0].JobTask.length; i++){
                            if (res.data.Result[0].JobTask[i].IsFI == 1){
                                res.data.Result[0].JobTask.splice(i,1);
                                // krn kl uda di splice urutan array nya mundur menggantikan posisi array yg di splice
                                i--;
                            }
                        }
                    }
                    $scope.mData = res.data.Result[0];
                    $scope.mData.Km = String($scope.mData.Km);
                    $scope.mData.Km = $scope.mData.Km.replace(/\B(?=(\d{3})+(?!\d))/g, ',');

                    RepairSupportActivityGR.getT1()
                        .then(

                            function(res) {
                                $scope.T1 = res.data.Result;
                                T1Final = res.data.Result;
                                for (var i = 0; i < res.data.Result.length; i++) {
                                    $scope.T1[i].xName = $scope.T1[i].T1Code + ' - ' + $scope.T1[i].Name;
                                    T1Final[i].xName = T1Final[i].T1Code + ' - ' + T1Final[i].Name;

                                    // console.log('$scope.T1[i].xName ==>', $scope.T1[i].xName);
                                    // console.log('T1Final[i].xName ==>', T1Final[i].xName);
                                }
                                // console.log("T1", $scope.T1);
                            },
                            function(err) {
                                //console.log("err=>", err);
                            }
                        );

                    RepairSupportActivityGR.getT2()
                        .then(

                            function(res) {
                                $scope.T2 = res.data.Result;
                                for (var i = 0; i < res.data.Result.length; i++) {
                                    $scope.T2[i].T2Code = $scope.T2[i].T2Code.trim();
                                    $scope.T2[i].xName = $scope.T2[i].T2Code + ' - ' + $scope.T2[i].Name;
                                }
                                //console.log("T2", $scope.T2);
                            },
                            function(err) {
                                //console.log("err=>", err);
                            }
                        );

                    $scope.xDataWoList = res.data.Result;
                    
                    // =============== cek kl ada split, stallid mana yg active, dan kl bs jng yg di stoppage =========================== start
                    $scope.xDataWoList[0].stallIdSplit = null;
                        if ($scope.xDataWoList[0].JobListSplitChip.length > 0){
                            var loopJobListSplitChip = $scope.xDataWoList[0].JobListSplitChip
                            for (var w=0; w < loopJobListSplitChip.length; w++){
                                if (loopJobListSplitChip[w].isSplitActive == 1 && loopJobListSplitChip[w].isFI != 1){
                                    $scope.xDataWoList[0].stallIdSplit = loopJobListSplitChip[w].StallId;
                                    break; // di break biar dpt yg apling pertama aja, kl slh y break na hapus
                                }
                            }
                        }
                    if ($scope.xDataWoList[0].stallIdSplit == null){
                        $scope.xDataWoList[0].stallIdSplit = $scope.xDataWoList[0].StallId
                    }
                    // =============== cek kl ada split, stallid mana yg active, dan kl bs jng yg di stoppage =========================== end

                    $scope.IdSA = $scope.xDataWoList[0].UserIdWORelease;
                    console.log('tatatata', $scope.xDataWoList);
                    $scope.mData.CustComplaint = $scope.xDataWoList[0].JobComplaint;
                    console.log("Complaint Data =>", $scope.mData.CustComplaint);
                    var arrValidTask = [];

                    _.map($scope.xDataWoList, function(val) {
                        val.PlanStart = val.PlanStart.substring(0, 5);
                        val.PlanFinish = val.PlanFinish.substring(0, 5);

                        _.map(val.JobTask, function(xval) {
                            if (xval.isDeleted == 0) {
                                arrValidTask.push(xval);
                            }
                        });

                    });
                    $scope.JanjiPenyerahan = '';
                    //janji penyerahan sementara kata pa ian plan finish tambah 30 mnt. 15/okt/2018
                    var JanjiPenyerahanSplit = $scope.xDataWoList[0].PlanFinish.split(':');
                    var JanjiPenyerahanJam = parseInt(JanjiPenyerahanSplit[0]);
                    var JanjiPenyerahanMenit = parseInt(JanjiPenyerahanSplit[1]);
                    var JanjiPenyerahanDetik = parseInt(JanjiPenyerahanSplit[2]);

                    console.log('jam', JanjiPenyerahanJam, ' menit', JanjiPenyerahanMenit, ' detik', JanjiPenyerahanDetik);

                    JanjiPenyerahanMenit = JanjiPenyerahanMenit + 30;
                    if (JanjiPenyerahanMenit == 60) {
                        JanjiPenyerahanMenit = 0;
                        JanjiPenyerahanJam = JanjiPenyerahanJam + 1;
                    } else if (JanjiPenyerahanMenit > 60) {
                        JanjiPenyerahanMenit = JanjiPenyerahanMenit - 60;
                        JanjiPenyerahanJam = JanjiPenyerahanJam + 1;
                    }

                    if (JanjiPenyerahanJam < 10) {
                        JanjiPenyerahanJam = '0' + JanjiPenyerahanJam.toString();
                    } else {
                        JanjiPenyerahanJam = JanjiPenyerahanJam.toString();
                    }

                    if (JanjiPenyerahanMenit < 10) {
                        JanjiPenyerahanMenit = '0' + JanjiPenyerahanMenit.toString();
                    } else {
                        JanjiPenyerahanMenit = JanjiPenyerahanMenit.toString();
                    }

                    if (JanjiPenyerahanDetik < 10) {
                        JanjiPenyerahanDetik = '0' + JanjiPenyerahanDetik.toString();
                    } else {
                        JanjiPenyerahanDetik = JanjiPenyerahanDetik.toString();
                    }

                    $scope.JanjiPenyerahan = JanjiPenyerahanJam + ':' + JanjiPenyerahanMenit;
                    // $scope.JanjiPenyerahan = JanjiPenyerahanJam + ':' + JanjiPenyerahanMenit + ':' + JanjiPenyerahanDetik;
                    console.log('janjipenyerahan', $scope.JanjiPenyerahan);

                    // if ($scope.xDataWoList[0].FixedDeliveryTime2 != $scope.xDataWoList[0].FixedDeliveryTime1){
                    //     $scope.JanjiPenyerahan = $scope.xDataWoList[0].FixedDeliveryTime2.toString();
                    //     $scope.JanjiPenyerahan = $scope.JanjiPenyerahan.substring(15,25);
                    //     console.log('jam uy', $scope.JanjiPenyerahan);
                    // } else {
                    //     $scope.JanjiPenyerahan = $scope.xDataWoList[0].FixedDeliveryTime1.toString();
                    //     $scope.JanjiPenyerahan = $scope.JanjiPenyerahan.substring(15,25);
                    //     console.log('jam uy', $scope.JanjiPenyerahan);
                    // }
                    console.log('$scope.xDataWoList', $scope.xDataWoList);

                    // jobtaskFinalData = angular.copy(res.data.Result);
                    for (var i = 0; i < $scope.xDataWoList.length; i++) {
                        arrValidTask
                        $scope.SelectJobTask = arrValidTask;
                        // $scope.SelectJobTask = $scope.xDataWoList[i].JobTask;

                        for (var a = 0; a < $scope.xDataWoList[i].JobTask.length; a++) {

                            for (var b = 0; b < $scope.xDataWoList[i].JobTask[a].JobTaskProgress.length; b++) {

                                if ($scope.xDataWoList[i].isWOBase == 1) {
                                    //console.log("data WoBase");
                                    var Jobtaskccd = $scope.xDataWoList[i].JobTask[a].JobTaskProgress;

                                    var JobTaskRemove = _.remove(Jobtaskccd, function(n) {
                                        return n.ProblemFoundedDate !== null;
                                    });
                                    //console.log("JobTaskRemove", JobTaskRemove);
                                    // Jobtaskccd.sort();
                                    JobTaskRemove.sort(function(a, b) {
                                        return a.Id - b.Id;
                                    });

                                    var countdata = JobTaskRemove.length;
                                    //console.log("countdata", countdata);
                                    if (countdata !== 0) {

                                        if (JobTaskRemove[countdata - 1].PauseReasonDesc !== undefined) {
                                            $scope.mData.PauseReasonId = JobTaskRemove[countdata - 1].PauseReasonId;
                                            $scope.mData.PauseReasonDesc = JobTaskRemove[countdata - 1].PauseReasonDesc;
                                        } else {
                                            $scope.mData.PauseReasonId = "";
                                            $scope.mData.PauseReasonDesc = "";
                                        }

                                    } else {
                                        //console.log("ga ada data");
                                    }

                                } else {
                                    //console.log("data JobBase");
                                }

                            }

                        }
                    }

                    // dataDetailJob = res.data.Result;
                    //console.log("data job", $scope.xDataWoList);

                    for (var i = 0; i < $scope.xDataWoList.length; i++) {
                        var ListJobTaskId = [];

                        //console.log("data wo", $scope.xDataWoList[i]);
                        $scope.dataJobParts = [];
                        for (var a = 0; a < $scope.xDataWoList[i].JobTask.length; a++) {
                            console.log("data JObTask", $scope.xDataWoList[i].JobTask[a]);

                            $scope.dataJobParts = $scope.dataJobParts.concat($scope.xDataWoList[i].JobTask[a].JobParts);
                            console.log("data $scope.dataJobParts", $scope.dataJobParts);

                            for (var z = 0; z < $scope.xDataWoList[i].JobTask[a].JobTaskTechnician.length; z++) {
                                // $scope.JobTaskTeknisi = $scope.xDataWoList[i].JobTask[a].JobTaskTechnician[z];
                                if (ListJobTaskId.length == 0){
                                    ListJobTaskId.push($scope.xDataWoList[i].JobTask[a].JobTaskTechnician[z].JobTaskId);


                                    var FinalTeknisi = [];
                                    RepairProcessGR.getTeknisiTask($scope.xDataWoList[i].JobTask[a].JobTaskTechnician[z].JobTaskId).then(
                                        function(res) {
    
                                            for (var i=0; i<res.data.length; i++){
                                                FinalTeknisi.push(res.data[i]);
        
                                                // $scope.JobTaskTeknisi = FinalTeknisi;
        
                                                //console.log("job Teksnisi", $scope.JobTaskTeknisi);
                                                // selectedID.push($scope.JobTaskTeknisi["0"].JobTaskId);
                                                //console.log("selectedID", selectedID);
                                            }
                                            // FinalTeknisi.push(res.data[0]);
                                            // console.log("data Teknisi3", res.data);
    
                                            $scope.JobTaskTeknisi = FinalTeknisi;

                                            $scope.NamaTeknisiNew = ''
                                            for (var i=0; i<$scope.JobTaskTeknisi.length; i++){
                                                $scope.NamaTeknisiNew = $scope.NamaTeknisiNew + $scope.JobTaskTeknisi[i].EmployeeName + ', '
                                            }
                                            $scope.NamaTeknisiNew = $scope.NamaTeknisiNew.substring(0, $scope.NamaTeknisiNew.length - 2);
                                            
    
                                            console.log("job Teksnisi", $scope.JobTaskTeknisi);
                                            selectedID.push($scope.JobTaskTeknisi["0"].JobTaskId);
                                            // //console.log("selectedID", selectedID);
                                            
                                        },
                                        function(err) {
                                            //console.log("err=>", err);
                                        }
                                    );
                                } else {
                                    var valid = 0;
                                    for (var k=0;k<ListJobTaskId.length;k++){
                                        if (ListJobTaskId[k] == $scope.xDataWoList[i].JobTask[a].JobTaskTechnician[z].JobTaskId){
                                            valid++;
                                        }
                                    }
                                    if (valid == 0){
                                        ListJobTaskId.push($scope.xDataWoList[i].JobTask[a].JobTaskTechnician[z].JobTaskId);


                                        var FinalTeknisi = [];
                                        RepairProcessGR.getTeknisiTask($scope.xDataWoList[i].JobTask[a].JobTaskTechnician[z].JobTaskId).then(
                                            function(res) {
        
                                                for (var i=0; i<res.data.length; i++){
                                                    FinalTeknisi.push(res.data[i]);
            
                                                    // $scope.JobTaskTeknisi = FinalTeknisi;
            
                                                    //console.log("job Teksnisi", $scope.JobTaskTeknisi);
                                                    // selectedID.push($scope.JobTaskTeknisi["0"].JobTaskId);
                                                    //console.log("selectedID", selectedID);
                                                }
                                                // FinalTeknisi.push(res.data[0]);
                                                // console.log("data Teknisi3", res.data);
        
                                                $scope.JobTaskTeknisi = FinalTeknisi;
                                                $scope.NamaTeknisiNew = ''
                                                for (var i=0; i<$scope.JobTaskTeknisi.length; i++){
                                                    $scope.NamaTeknisiNew = $scope.NamaTeknisiNew + $scope.JobTaskTeknisi[i].EmployeeName + ', '
                                                }
                                                $scope.NamaTeknisiNew = $scope.NamaTeknisiNew.substring(0, $scope.NamaTeknisiNew.length - 2);
        
                                                console.log("job Teksnisi", $scope.JobTaskTeknisi);
                                                selectedID.push($scope.JobTaskTeknisi["0"].JobTaskId);
                                                // //console.log("selectedID", selectedID);
                                                
                                            },
                                            function(err) {
                                                //console.log("err=>", err);
                                            }
                                        );
                                    }
                                }
                                
                            }

                        }

                    }

                    var lc98 = 0;
                    var lc99 = 0;
                    // var count = 0;
                    for (var i = 0; i < $scope.xDataWoList.length; i++) {
                        //console.log("revisi utama", $scope.xDataWoList[i].Revision);
                        //console.log("$scope.xDataWoList", $scope.xDataWoList[i].JobTask);

                        for (var a = 0; a < $scope.xDataWoList[i].JobTask.length; a++) { //looping sebanyak berapa task
                            //console.log("REvisi", $scope.xDataWoList[i].JobTask[a].Revision);
                            //console.log("Status", $scope.xDataWoList[i].JobTask[a].Status);
                            /*
                            //console.log("FinalTeknisi", FinalTeknisi);
                            //console.log("FinalTeknisi4", FinalTeknisi);
                            //console.log("FinalTeknisi1", FinalTeknisi["0"]);
                            //console.log("FinalTeknisi2", FinalTeknisi["0"].JobTaskId);*/

                            if (($scope.xDataWoList[i].JobTask[a].JobTypeId == 59 || $scope.xDataWoList[i].JobTask[a].JobTypeId == 60) || $scope.xDataWoList[i].JobTask[a].PaidById == 30) {
                                // $scope.DataWarranty = $scope.xDataWoList[i].JobTask;
                                if ($scope.xDataWoList[i].JobTask[a].isDeleted == 0){
                                    $scope.DataWarranty.push($scope.xDataWoList[i].JobTask[a]);
                                }
                                console.log("$scope.DataWarranty", $scope.DataWarranty);
                            } else {
                                var countParts = $scope.xDataWoList[i].JobTask[a].JobParts.length - 1;
                                var flagPart = 0;
                                _.map($scope.xDataWoList[i].JobTask[a].JobParts, function(ax, k) {
                                    console.log('Parts ><>', ax);
                                    if (ax.PaidById == 30) {
                                        flagPart = 1;

                                        if (countParts == k) {
                                            if (flagPart == 1) {
                                                console.log('$scope.xDataWoList[i].JobTask[a]', $scope.xDataWoList[i].JobTask[a]);
                                                if ($scope.xDataWoList[i].JobTask[a].isDeleted == 0){
                                                    $scope.DataWarranty.push($scope.xDataWoList[i].JobTask[a]);
                                                }
                                            };
                                            //  else {
                                            //     $scope.DataWarranty = [];
                                            // };
                                        };
                                    };
                                    // else {

                                    // }

                                    // if (countParts == k) {
                                    //     if (flagPart == 1) {
                                    //         $scope.DataWarranty.push($scope.xDataWoList[i].JobTask[a]);
                                    //     } else {
                                    //         $scope.DataWarranty = [];
                                    //     };
                                    // };
                                });
                                // $scope.DataWarranty = [];
                            }

                            // --------------------- coba di ganti kl wo base liat na status ajoblist nya aja, jng di liat per task ----------------------- start
                            // if ($scope.xDataWoList[i].JobTask[a].Status == 2) {
                            //     $scope.hideBtnClokON = true;
                            // } else {
                            //     $scope.hideBtnClokON = false;
                            // }

                            if ($scope.xDataWoList[i].Status == 6){
                                $scope.hideBtnClokON = true;
                            } else {
                                $scope.hideBtnClokON = false;
                            }

                            // --------------------- coba di ganti kl wo base liat na status ajoblist nya aja, jng di liat per task ----------------------- end

                            if ((($scope.xDataWoList[i].Revision >= $scope.xDataWoList[i].JobTask[a].Revision &&
                                        $scope.xDataWoList[i].JobTask[a].Status == 0 &&
                                        $scope.xDataWoList[i].IsStopPage == 0 && $scope.xDataWoList[i].JobTask[a].isDeleted == 0) ||
                                    ($scope.xDataWoList[i].Revision >= $scope.xDataWoList[i].JobTask[a].Revision &&
                                        $scope.xDataWoList[i].JobTask[a].Status == 2 &&
                                        $scope.xDataWoList[i].IsStopPage == 0 && $scope.xDataWoList[i].JobTask[a].isDeleted == 0) ||
                                    ($scope.xDataWoList[i].Revision >= $scope.xDataWoList[i].JobTask[a].Revision &&
                                        $scope.xDataWoList[i].JobTask[a].Status == null &&
                                        $scope.xDataWoList[i].IsStopPage == 0 && $scope.xDataWoList[i].JobTask[a].isDeleted == 0)) >= 1) {
                                // console.log('penyebab keanehan0')

                                $scope.btnClockOn = false;
                                lc98 = 1;
                            } else {
                                // console.log('penyebab keanehan1')
                                $scope.btnClockOn = true;

                            }

                            // if ((($scope.xDataWoList[i].Revision == $scope.xDataWoList[i].JobTask[a].Revision && $scope.xDataWoList[i].JobTask[a].Status == 1 && $scope.xDataWoList[i].IsStopPage == 0) || ($scope.xDataWoList[i].Revision == $scope.xDataWoList[i].JobTask[a].Revision && $scope.xDataWoList[i].JobTask[a].Status == 3 && $scope.xDataWoList[i].IsStopPage == 0)) >= 1) {
                            if ((($scope.xDataWoList[i].Revision >= $scope.xDataWoList[i].JobTask[a].Revision && $scope.xDataWoList[i].JobTask[a].Status == 1 && $scope.xDataWoList[i].IsStopPage == 0) || ($scope.xDataWoList[i].Revision >= $scope.xDataWoList[i].JobTask[a].Revision && $scope.xDataWoList[i].JobTask[a].Status == 3 && $scope.xDataWoList[i].IsStopPage == 0)) >= 1) {
                                $scope.btnClockPause = false;
                                console.log('status pause disabled ga3', $scope.btnClockPause)

                                $scope.btnClockOff = false;
                                var jobtaskidx = 0;
                                //console.log("selectedID" + selectedID["0"]);
                                /*
                                for(var x = 0; x < $scope.SelectJobTask.length; x++){
                                    if($scope.JobTaskTeknisi[0].JobTaskId == $scope.SelectJobTask[x.toString()].JobTaskId){
                                      jobtaskidx = x;
                                    }
                                }*/
                                $scope.mData.JobTaskFWO = $scope.SelectJobTask[jobtaskidx];
                                lc99 = 1;
                            } else {
                                $scope.btnClockPause = true;
                                console.log('status pause disabled ga4', $scope.btnClockPause)

                                $scope.btnClockOff = true;
                            }

                            if (lc98 == 1) {
                                // console.log('penyebab keanehan2')
                                $scope.btnClockOn = false;
                            }

                            if (lc99 == 1) {
                                $scope.btnClockPause = false;
                                console.log('status pause disabled ga5', $scope.btnClockPause)

                                $scope.btnClockOff = false;
                                $scope.btnClockOffJob = false;

                                // if ($scope.xDataWoList[i].isWOBase == 0) {

                                // }
                            }
                            // || $scope.xDataWoList[i].JobTask[a].Status == 3
                            console.log('asem1', [a], $scope.xDataWoList[i].JobTask[a].Status);
                            console.log('asem2', [a], $scope.xDataWoList[i].JobTask[a].IsStopPage);
                            console.log('asem3', [a], $scope.xDataWoList[i].JobTask[a]);
                            console.log('asem4', [a], SelectedJobTask);
                            if (SelectedJobTask != 1 && SelectedJobTask != undefined) {
                                if (SelectedJobTask.JobTaskId == $scope.xDataWoList[i].JobTask[a].JobTaskId) {
                                    if ($scope.xDataWoList[i].JobTask[a].Status == 4 || $scope.xDataWoList[i].JobTask[a].Status == 2 || $scope.xDataWoList[i].JobTask[a].Status < 1 || $scope.xDataWoList[i].JobTask[a].Status == null || $scope.xDataWoList[i].JobTask[a].IsStopPage == 1) {
                                        $scope.btnClockOffJob = true;
                                        console.log('status pause disabled ga6', $scope.btnClockOffJob)
                                    }


                                    if ($scope.xDataWoList[i].JobTask[a].JobTaskProgress.length > 0) {
                                        var posisiProgress = $scope.xDataWoList[i].JobTask[a].JobTaskProgress.length - 1;
                                        if ($scope.xDataWoList[i].JobTask[a].JobTaskProgress[posisiProgress].ProblemFoundedDate != null || $scope.xDataWoList[i].JobTask[a].JobTaskProgress[posisiProgress].ProblemFoundedDate != undefined) {
                                            $scope.mData.PauseReasonId = $scope.xDataWoList[i].JobTask[a].JobTaskProgress[posisiProgress].PauseReasonId;
                                            $scope.mData.PauseReasonDesc = $scope.xDataWoList[i].JobTask[a].JobTaskProgress[posisiProgress].PauseReasonDesc;
                                        } else {
                                            $scope.mData.PauseReasonId = $scope.xDataWoList[i].JobTask[a].JobTaskProgress[posisiProgress].PauseReasonId;
                                            $scope.mData.PauseReasonDesc = $scope.xDataWoList[i].JobTask[a].JobTaskProgress[posisiProgress].PauseReasonDesc;
                                        }
                                    } else {
                                        $scope.mData.PauseReasonId = "";
                                        $scope.mData.PauseReasonDesc = "";
                                    }



                                }


                            }
                            $scope.mData.JobTaskFWO = $scope.SelectJobTask[0];
                        }
                    }
                    //console.log("btn clock ON", $scope.btnClockOn);
                    //console.log("btn Clock Pause", $scope.btnClockPause);
                    //console.log("btn clock off", $scope.btnClockOff);

                    RepairProcessGR.getVinFromCRM($scope.xDataWoList[0].VehicleId).then(
                        function(res) {
                            Vin = res.data.Result;
                            $scope.VinData = Vin;
                            //console.log("Vin", Vin);
                        },
                        function(err) {
                            //console.log("err=>", err);
                        }
                    );

                    RepairProcessGR.getTeknisiNote($scope.JbId).then(
                        function(res) {
                            var TeknisiNote = res.data.Result;
                            //console.log("Teknisi Note", TeknisiNote);
                            $scope.mData.TechnicianNotes = TeknisiNote[0].TechnicianNotes;
                        },
                        function(err) {
                            //console.log("err=>", err);
                        }
                    );
                    // $scope.xDataWoList[0].JobId
                    // ========= Comment
                    // RepairProcessGR.getDataWac($scope.xDataWoList[0].JobId).then(
                    //     function(res) {
                    //         var dataWac = res.data;
                    //         $scope.finalDataWac = dataWac;

                    //         //console.log("Data WAC", dataWac);
                    //         $scope.mData.OtherDescription = dataWac.Other;
                    //         $scope.mData.moneyAmount = dataWac.MoneyAmount;
                    //         $scope.mData.BanSerep = dataWac.isSpareTire;
                    //         $scope.mData.CdorKaset = dataWac.isCD;
                    //         $scope.mData.Payung = dataWac.isUmbrella;
                    //         $scope.mData.Dongkrak = dataWac.isJack;
                    //         $scope.mData.STNK = dataWac.isVehicleRegistrationNumber;
                    //         $scope.mData.P3k = dataWac.isP3k;
                    //         $scope.mData.KunciSteer = dataWac.isSteerLock;
                    //         $scope.mData.ToolSet = dataWac.isToolSet;
                    //         $scope.mData.ClipKarpet = dataWac.isCarpetClip;
                    //         $scope.mData.BukuService = dataWac.isServiceBook;
                    //         $scope.mData.AlarmCondition = dataWac.isAlarmConditionOk;
                    //         $scope.mData.AcCondition = dataWac.isAcOk;
                    //         $scope.mData.PowerWindow = dataWac.isPowerWindowOk;
                    //         $scope.mData.CarType = dataWac.isPowerWindowOk;
                    //         $scope.slider.value = dataWac.Fuel;
                    //         // $scope.mData.TechnicianNotes = TeknisiNote[0].TechnicianNotes;
                    //     },
                    //     function(err) {
                    //         //console.log("err=>", err);
                    //     }
                    // );



                    //console.log("Data Yang Dipilih=>", $scope.xDataWoList);
                    //console.log("stall id", $scope.xDataWoList[0].StallId);


                    RepairProcessGR.getJobSuggest($scope.xDataWoList[0].JobId).then(
                        function(res) {
                            $scope.GetDataJobSuggest = res.data.Result;
                            //console.log("Data JObSUggest", $scope.GetDataJobSuggest);

                            // if($scope.GetDataJobSuggest[0].SuggestionCatg == null || $scope.GetDataJobSuggest[0].SuggestionCatg == undefined || $scope.GetDataJobSuggest[0].SuggestionCatg == 0){
                            //   $scope.mData.SuggestionCatg = 0;
                            // }else{
                            $scope.mData.SuggestionCatg = $scope.GetDataJobSuggest[0].SuggestionCatg;
                            // }
                            var co = ["CategoryJobSuggestKeamanan", "CategoryJobSuggestPeraturan", "CategoryJobSuggestKenyamanan", "CategoryJobSuggestEfisiensi"];

                            var total = $scope.mData.SuggestionCatg;
                            var arrBin = [1, 2, 4, 8];
                            for (var i in arrBin) {
                                if (total & arrBin[i]) {
                                    var xc = co[i];
                                    $scope.mData[xc] = arrBin[i];
                                }
                            }

                            if ($scope.GetDataJobSuggest[0].Suggestion == null) {
                                $scope.mData.Suggestion = "Tidak ada saran";
                            } else {
                                $scope.mData.Suggestion = $scope.GetDataJobSuggest[0].Suggestion;
                            }

                            // //console.log("total", total);
                            //console.log("$scope.GetDataJobSuggest[0].Suggestion", $scope.GetDataJobSuggest[0].Suggestion);
                            //console.log("$scope.GetDataJobSuggest[0].SuggestionCatg", $scope.GetDataJobSuggest[0].SuggestionCatg);
                            //console.log("$scope.GetDataJobSuggest[0].SuggestionDate", $scope.GetDataJobSuggest[0].SuggestionDate);

                            AppointmentGrService.getDataVehiclePSFU($scope.xDataWoList[0].VehicleId).then(function(resu) {
                                var dataQuestionAnswerPSFU = resu.data.Result[0];
                                console.log("data Suggest dari CRM", dataQuestionAnswerPSFU);

                                // if (dataQuestionAnswerPSFU.JobSuggest == null) {
                                //     $scope.mData.Suggestion = "Tidak ada saran";
                                // } else {
                                //     $scope.mData.Suggestion = dataQuestionAnswerPSFU.JobSuggest;
                                // }
                            });
                            console.log('date job suggest', $scope.GetDataJobSuggest[0].SuggestionDate)
                            var tanggalSerba1 = $scope.GetDataJobSuggest[0].SuggestionDate.getDate() + '-' + $scope.GetDataJobSuggest[0].SuggestionDate.getMonth() + 1 + '-' + $scope.GetDataJobSuggest[0].SuggestionDate.getFullYear();
                            console.log('tgl serba 1', tanggalSerba1); //cek apa tanggal nya uda ke pernah di set apa blm, kalau belom pasti 1-jan-0001

                            if ($scope.GetDataJobSuggest[0].SuggestionDate == null ||
                                $scope.GetDataJobSuggest[0].SuggestionDate == undefined ||
                                $scope.GetDataJobSuggest[0].SuggestionDate == '' ||
                                $scope.GetDataJobSuggest[0].SuggestionDate == 'Invalid Date' ||
                                tanggalSerba1 == '1-01-1') {
                                $scope.GetDataJobSuggest[0].SuggestionDate = null;
                            }
                            $scope.mData.SuggestionDate = $scope.GetDataJobSuggest[0].SuggestionDate;
                            // if ($scope.GetDataJobSuggest[0].SuggestionDate == 'Mon Jan 01 0001 00:00:00 GMT+0700 (SE Asia Standard Time)' ||
                            //     $scope.GetDataJobSuggest[0].SuggestionDate == 'Mon Jan 01 0001 00:00:00 GMT+0707 (Western Indonesia Time)') {

                            //     var today = new Date();
                            //     var dd = today.getDate();
                            //     var mm = today.getMonth() + 1; //January is 0!
                            //     var yyyy = today.getFullYear();
                            //     today = yyyy + '/' + mm + '/' + dd;
                            //     $scope.mData.SuggestionDate = today;
                            //     //console.log("tanggal", $scope.mData.SuggestionDate);
                            // } else {

                            //     $scope.mData.SuggestionDate = $scope.GetDataJobSuggest[0].SuggestionDate;
                            // }
                        },
                        function(err) {
                            //console.log("err=>", err);
                        }
                    );

                    for (var i = 0; i < $scope.xDataWoList.length; i++) {
                        var EmpF = $scope.xDataWoList[i];
                        for (var a = 0; a < EmpF.JobTask.length; a++) {
                            var Dig = EmpF.JobTask[a].EmployeeId;
                            //console.log("kenapa jan", Dig);
                            RepairProcessGR.getEmployeeById(Dig).then(
                                function(res) {
                                    $scope.EmployeeFinalId = res.data;
                                    //console.log("berhasil", res.data);
                                },
                                function(err) {
                                    //console.log("err=>", err);
                                }
                            );

                        }
                    }


                    var Count = 0;
                    var cc = [];
                    for (i = 0; i < $scope.xDataWoList.length; i++) {
                        var z = $scope.xDataWoList[i];
                        //console.log("hahah", z);
                        for (j = 0; j < $scope.xDataWoList[i].JobTask.length; j++) {
                            if ($scope.xDataWoList[i].JobTask[j].isDeleted == 0) {
                                Count++
                                if ($scope.xDataWoList[i].JobTask[j].Status == 4) {
                                    cc.push($scope.xDataWoList[i].JobTask[j]);
                                    //console.log("data JbTsk", cc.length);
                                    //console.log("count", Count);

                                }
                            }

                        }

                        if (cc.length == Count) {
                            if ($scope.user.RoleId == 1033 || $scope.user.RoleId == 1) {
                                $scope.showFi = true;
                            } else {
                                $scope.showFi = false;
                            }
                        } else {
                            $scope.showFi = false;
                        }

                    }

                    //board


                    var isBoardLoaded = false;
                    $scope.DateOptions = {
                        startingDay: 1,
                        format: 'dd/MM/yyyy',
                        // disableWeekend: 1
                    };

                    var boardName = 'repairprocessgrboard';
                    $scope.repairprocessgrboard = { currentDate: new Date() }
                    $scope.repairprocessgrboard.startDate = new Date();

                    var timeActive = true;
                    var minuteTick = function() {
                        if (!timeActive) {
                            return;
                        }
                        //if ()
                        JpcbGrViewFactory.updateTimeLine(boardName);
                        $timeout(function() {
                            minuteTick();
                        }, 60000);
                    }

                    $scope.$on('$destroy', function() {
                        timeActive = false;
                    });

                    var showBoardRepairProcessGr = function() {
                        if ($scope.xDataWoList[0].StallId == undefined) {
                            //console.log("Stall id undefined", $scope.xDataWoList[0].StallId);
                        } else {

                            //console.log("+$scope.xDataWoList[0].StallId+", $scope.xDataWoList[0].StallId);
                            var stallIdActive = 0;
                            var dateNow = new Date();
                            var dateNowNoHour = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate(), 0, 0, 0)
                            var dataChipToday = [];

                            if ($scope.xDataWoList[0].JobListSplitChip != null){
                                if ($scope.xDataWoList[0].JobListSplitChip.length > 0){
                                    for (var i=0; i<$scope.xDataWoList[0].JobListSplitChip.length; i++){
                                        if (dateNowNoHour.getTime() == $scope.xDataWoList[0].JobListSplitChip[i].PlanDateStart.getTime()){
                                            dataChipToday.push($scope.xDataWoList[0].JobListSplitChip[i]);
                                        }
                                    }
                                    
                                    if (dataChipToday.length > 0){
                                        var planstartChip = null;
                                        var selisihActualVSNow = null;
                                        for (var j=0; j<dataChipToday.length; j++){
                                            var splitJamChip = dataChipToday[j].PlanStart.split(':');
                                            planstartChip = new Date(dataChipToday[j].PlanDateStart.getFullYear(), dataChipToday[j].PlanDateStart.getMonth(), dataChipToday[j].PlanDateStart.getDate(), splitJamChip[0], splitJamChip[1], 0);
    
                                            
                                            if ((planstartChip.getTime() - dateNow.getTime()) > 0){
                                                if (selisihActualVSNow == null){
                                                    stallIdActive = dataChipToday[j].StallId;
                                                    selisihActualVSNow = planstartChip.getTime() - dateNow.getTime();
                                                    if (dataChipToday[j].isFI != 1){
                                                        stallIdActive = dataChipToday[j].StallId
                                                    }
                                                } else {
                                                    if ((planstartChip.getTime() - dateNow.getTime()) < selisihActualVSNow){
                                                        selisihActualVSNow = planstartChip.getTime() - dateNow.getTime();
                                                        stallIdActive = dataChipToday[j].StallId;
                                                        if (dataChipToday[j].isFI != 1){
                                                            stallIdActive = dataChipToday[j].StallId
                                                        }
                                                    }
                                                }
    
                                                
                                            } else {
                                                // ini kalau semua nya uda kelewat dari jam actual
                                                for (var k=0; k<dataChipToday.length; k++){
                                                    if (dataChipToday[k].isSplitActive == 1){
                                                        stallIdActive = dataChipToday[k].StallId
                                                        if (dataChipToday[j].isFI != 1){
                                                            stallIdActive = dataChipToday[j].StallId
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        stallIdActive = $scope.xDataWoList[0].StallId;
                                    }
                                } else {
                                    stallIdActive = $scope.xDataWoList[0].StallId;
                                }
                                
                            } else {
                                stallIdActive = $scope.xDataWoList[0].StallId;
                            }
                            
                            console.log('list chip today', dataChipToday);
                            var vitem = {
                                    // mode: $scope.jpcbgrview.showMode,
                                    currentDate: $scope.repairprocessgrboard.currentDate,
                                    // stallUrl: '/api/as/Boards/Stall/'+$scope.xDataWoList[0].StallId+'',
                                    // stallUrl: '/api/as/Stall/' + $scope.xDataWoList[0].StallId + '', // di komen 16 april 2019
                                    stallUrl: '/api/as/Stall/' + stallIdActive + '',
                                    // chipUrl: '/api/as/Boards/jpcb/bydate/' + $filter('date')($scope.repairprocessgrboard.currentDate, 'yyyy-MM-dd') + '/0/22/' + $scope.xDataWoList[0].StallId, // di komen 16 april 2019
                                    chipUrl: '/api/as/Boards/jpcb/bydate/' + $filter('date')($scope.repairprocessgrboard.currentDate, 'yyyy-MM-dd') + '/0/22/' + stallIdActive,
                                    nopol: $scope.mData.PoliceNumber
                                        // tipe: $scope.jpcbgrview.tipe
                                }
                                //console.log("showBoardRepairProcessGr");
                            $scope.GetDataBoard();
                            JpcbGrViewFactory.showBoard(boardName, { container: 'repairprocessgrboard', currentChip: vitem });
                        }
                    }

                    $scope.statusClock2 = 'off';
                    $scope.GetDataBoard = function() {

                        var stallIdActive = 0;
                            var dateNow = new Date();
                            var dateNowNoHour = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate(), 0, 0, 0)
                            var dataChipToday = [];

                            if ($scope.xDataWoList[0].JobListSplitChip != null){
                                if ($scope.xDataWoList[0].JobListSplitChip.length > 0){
                                    for (var i=0; i<$scope.xDataWoList[0].JobListSplitChip.length; i++){
                                        if (dateNowNoHour.getTime() == $scope.xDataWoList[0].JobListSplitChip[i].PlanDateStart.getTime()){
                                            dataChipToday.push($scope.xDataWoList[0].JobListSplitChip[i]);
                                        }
                                    }
                                    
                                    if (dataChipToday.length > 0){
                                        var planstartChip = null;
                                        var selisihActualVSNow = null;
                                        for (var j=0; j<dataChipToday.length; j++){
                                            var splitJamChip = dataChipToday[j].PlanStart.split(':');
                                            planstartChip = new Date(dataChipToday[j].PlanDateStart.getFullYear(), dataChipToday[j].PlanDateStart.getMonth(), dataChipToday[j].PlanDateStart.getDate(), splitJamChip[0], splitJamChip[1], 0);
    
                                            
                                            if ((planstartChip.getTime() - dateNow.getTime()) > 0){
                                                if (selisihActualVSNow == null){
                                                    stallIdActive = dataChipToday[j].StallId;
                                                    selisihActualVSNow = planstartChip.getTime() - dateNow.getTime();
                                                    if (dataChipToday[j].isFI != 1){
                                                        stallIdActive = dataChipToday[j].StallId
                                                    }
                                                } else {
                                                    if ((planstartChip.getTime() - dateNow.getTime()) < selisihActualVSNow){
                                                        selisihActualVSNow = planstartChip.getTime() - dateNow.getTime();
                                                        stallIdActive = dataChipToday[j].StallId;
                                                        if (dataChipToday[j].isFI != 1){
                                                            stallIdActive = dataChipToday[j].StallId
                                                        }
                                                    }
                                                }
    
                                                
                                            } else {
                                                // ini kalau semua nya uda kelewat dari jam actual
                                                for (var k=0; k<dataChipToday.length; k++){
                                                    if (dataChipToday[k].isSplitActive == 1){
                                                        stallIdActive = dataChipToday[k].StallId
                                                        if (dataChipToday[j].isFI != 1){
                                                            stallIdActive = dataChipToday[j].StallId
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        stallIdActive = $scope.xDataWoList[0].StallId;
                                    }
                                } else {
                                    stallIdActive = $scope.xDataWoList[0].StallId;
                                }
                            } else {
                                stallIdActive = $scope.xDataWoList[0].StallId;
                            }
                            
                            console.log('list chip today', dataChipToday);

                        // console.log('kapan masuk sini', $filter('date')($scope.repairprocessgrboard.currentDate, 'yyyy-MM-dd'), $scope.xDataWoList[0].StallId);
                        // RepairProcessGR.GetDataBoard($filter('date')($scope.repairprocessgrboard.currentDate, 'yyyy-MM-dd'), $scope.xDataWoList[0].StallId).then( //di komen 16 April 2019
                        RepairProcessGR.GetDataBoard($filter('date')($scope.repairprocessgrboard.currentDate, 'yyyy-MM-dd'), stallIdActive).then(
                            function(res) {
                                console.log('Data board per stall?', res.data.Result);
                                // for (var i = 0; i <= res.data.Result.length; i++) {
                                //     if (res.data.Result[i].Status == 5) {
                                //         // alert('Tidak Bisa Memulai Clock On, Clock Off Unit Sebelumnya');
                                //         $scope.statusClock2 = 'on';
                                //     }
                                // }


                                if ($scope.xDataWoList[0].Status == 5) {
                                    // console.log('penyebab keanehan3')

                                    $scope.statusClock2 = 'on';
                                    $scope.btnClockOn = true;
                                    $scope.btnClockOff = false;
                                }


                                // if (res.data.Result.length > 0) {
                                //     _.map(res.data.Result, function(a) {
                                //         if (a.Status == 5) {
                                //             // console.log('penyebab keanehan3')

                                //             $scope.statusClock2 = 'on';
                                //             $scope.btnClockOn = true;
                                //             $scope.btnClockOff = false;
                                //         }
                                //     });
                                // }

                            },
                            function(err) {
                                //console.log("err=>", err);
                            }
                        );
                    }

                    $scope.repairprocessgrboard.reloadBoard = function() {
                        $timeout(function() {
                            var vobj = {
                                mode: $scope.repairprocessgrboard.showMode,
                                currentDate: $scope.repairprocessgrboard.currentDate,
                                nopol: $scope.repairprocessgrboard.nopol,
                                tipe: $scope.repairprocessgrboard.tipe
                            }
                            showBoardRepairProcessGr(vobj);

                            $timeout(function() {
                                minuteTick();
                            }, 100);

                        }, 100);
                    }

                    var velement = document.getElementById('repairProcess_gr_panel');
                    var erd = elementResizeDetectorMaker();
                    erd.listenTo(velement, function(element) {
                        if (!isBoardLoaded) {
                            isBoardLoaded = true;
                            showBoardRepairProcessGr();
                            minuteTick();
                            // untuk menampilkan timeline
                            // $timeout( function(){
                            //     minuteTick();
                            // }, 1000);
                        }
                        vboard = JpcbGrViewFactory.getBoard(boardName);
                        if (vboard) {
                            vboard.autoResizeStage();
                            vboard.redraw();
                        }

                    });



                    //board

                    // var Count = 0;
                    // for(i = 0; i < $scope.xDataWoList.length; i++){
                    //   for(j = 0; j < $scope.xDataWoList[i].JobTask.length; j++){
                    //     var a = $scope.xDataWoList[i].JobTask[j];
                    //     //console.log("tidak",a);
                    //     if(a.Status == 4){
                    //       Count++
                    //     if(Count == $scope.xDataWoList[i].JobTask[j].length){
                    //       $scope.showFi = true;
                    //       //console.log("hahah",$scope.showFi);
                    //     }else{
                    //       $scope.showFi = false;
                    //       //console.log("Masuk Else",$scope.showFi);
                    //     }
                    //     }
                    //   }
                    // }

                },
                function(err) {
                    //console.log("err=>", err);
                }
            );
        }

        $scope.SimpanEnd = function(model, dataJobTask, JobId) {
            //console.log("model", model);
            //console.log("dataJobTask", dataJobTask);
            //console.log("JobId", JobId);
            //console.log('scope.mdata.pausereasonid', $scope.mData.PauseReasonId)
            // $scope.SimpanJobSuggest($scope.mData);
            // $scope.SimpanNote();
            // if (_.isNull($scope.mData.PauseReasonId) || _.isEmpty($scope.mData.PauseReasonId) || _.isUndefined($scope.mData.PauseReasonId)) {

            RepairProcessGR.getStatWO($scope.xDataWoList[0].JobId, $scope.xDataWoList[0].Status).then(function(resux2) { 
                if (resux2.data == 1){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                    });
                } else {
                    if (($scope.mData.PauseReasonId == null) || ($scope.mData.PauseReasonId == '') || ($scope.mData.PauseReasonId == undefined)) {
                        bsNotify.show({
                            title: 'Mohon Pilih Alasan Pause',
                            text: 'I will close in 2 seconds.',
                            size: 'big',
                            timeout: 2000,
                            type: 'danger'
                        });
        
                        // return;
                    } else {
                        if ($scope.mData.PauseReasonId == 43) {
                            // kl rawat jalan ceck dl di gate nya
                            RepairProcessGR.CheckKendaraanGateInOut($scope.JbId, $scope.xDataWoList[0].PoliceNumber,3).then(function (resultnya) {
                                var splitRes = resultnya.data.split('#')
                                    
                                if (splitRes[0] == 999){
                                    bsAlert.warning("Kendaraan sudah berada di list Gate Out security untuk aktivitas " + splitRes[1]);
                                    return;
                                } else if (splitRes[0] == 666){
                                    bsAlert.warning("Kendaraan sedang berada di luar Cabang/Bengkel untuk aktivitas " + splitRes[1]);
                                    return;
                                } else {

                                    bsAlert.alert({
                                        title: "Apakah Anda yakin memilih Pause Rawat Jalan ?",
                                        text: "Pause Rawat Jalan membutuhkan Pelunasan Biaya ke kasir sebelum mobil dapat melakukan Gate Out.",
                                        type: "question",
                                        showCancelButton: true
                                    },
                                    function() {
                                        console.log("yes nih");
                                        $scope.savePauseWo(model, dataJobTask, JobId);
                                    },
                                    function() {
                                        console.log("ga jadi nih");

                                    }
                                    );

                                    // $scope.savePauseWo(model, dataJobTask, JobId);
                                }
                            })
        
                        } else {
                            $scope.savePauseWo(model, dataJobTask, JobId);
                        }
                    }
                }

            })
            
        }

        $scope.SimpanEndWithoutPause = function() {
            //console.log("ini simpan tanpa pause");
            // $scope.mData.Suggestion == null || $scope.mData.Suggestion == '' || $scope.mData.Suggestion == undefined ||
            if (_.isNull($scope.mData.Suggestion) || $scope.mData.Suggestion == "" || _.isUndefined($scope.mData.Suggestion) || $scope.mData.Suggestion == "Tidak ada saran") {

                $scope.SimpanJobSuggest($scope.mData);

            } else {
                if (_.isNull($scope.mData.SuggestionDate) || $scope.mData.SuggestionDate == "" || _.isUndefined($scope.mData.SuggestionDate)) {
                    bsNotify.show({

                        size: 'big',
                        timeout: 2000,
                        type: 'danger',
                        title: "Mohon isi job suggest date"
                    });

                    return;
                }
                $scope.SimpanJobSuggest($scope.mData);
            }
        }

        $scope.listApi = {};
        $scope.listApiDTC = {};
        $scope.showBtnTambah = false;
        $scope.WoDetail = function(JobID) {

            RepairProcessGR.getDataDetail(JobID).then(function(res) {
                    $scope.DataWo = res.data.Result;
                    //console.log("$scope.DataWo[0].Status", $scope.DataWo[0].Status);
                    // if($scope.DataWo[0].Status == 6){
                    //     $scope.showBtnTambah = false;
                    // }else{
                    //     $scope.showBtnTambah = true;
                    // }
                    var dataTaskList = $scope.DataWo[0].JobTask;
                    _.map(dataTaskList, function(valTask) {
                        if (valTask.isDeleted == 0) {
                            _.map(valTask.JobParts, function(valParts) {
                                if (valParts.isDeleted == 0){
                                    AppointmentGrService.getAvailableParts(valParts.PartsId, JobID, valParts.Qty).then(function(resu) {
                                        var dataPart = resu.data.Result[0];
                                        console.log('dataPart 22', dataPart)
                                        if (dataPart !== null && dataPart !== undefined) {
                                            if (dataPart.isAvailable == 0) {
                                                valParts.Availability = "Tidak Tersedia"
                                                if (dataPart.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                                    valParts.ETA = "";
                                                } else {
                                                    valParts.ETA = dataPart.DefaultETA;
                                                }
                                                valParts.Type = 3;
                                            } else {
                                                valParts.Availability = "Tersedia";
                                            };
                                        };
                                        console.log('valParts 22', valParts)
                                    });
                                }
                            });
                        }
                    });
                    console.log("data Wo dataTaskList", dataTaskList);

                    $scope.DataWOTask = [];
                    _.map($scope.DataWo[0].JobTask, function(val) {
                        if (val.isDeleted == 0) {
                            $scope.DataWOTask.push(val);
                        }
                    })
                    for (var i=0; i<$scope.DataWOTask.length; i++){
                        for (var j=0; j<$scope.DataWOTask[i].JobParts.length; j++){
                            if ($scope.DataWOTask[i].JobParts[j].isDeleted == 1){
                                $scope.DataWOTask[i].JobParts.splice(j,1);
                                j=-1;
                            }
                        }
                    }
                    console.log('dududuhun', $scope.DataWOTask)


                    // for (var i = 0; i < $scope.DataWo.length; i++) {
                    //     $scope.DataWOTask = $scope.DataWo[i].JobTask;
                    //     // console.log("data wo tas", $scope.DataWOTask);
                    //     for (var a = 0; a < $scope.DataWo[i].JobTask.length; a++) {
                    //         for (var b = 0; b < $scope.DataWo[i].JobTask[a].JobParts.length; b++) {
                    //             $scope.DataMaterialFromP = $scope.DataWo[i].JobTask[a].JobParts[b];
                    //             // console.log("data wo parts", $scope.DataMaterialFromP);
                    //         }
                    //     }
                    // };
                    // console.log("data Wo dataTaskList", $scope.DataWo);
                },
                function(err) {
                    //console.log("err=>", err);
                }
            );
        }


        $scope.gridWODetail = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,

            data: 'DataMaterialFromP',
            showGridFooter: true,
            columnDefs: [{
                    field: 'ModelCode',
                    name: 'Nomor Material'
                },
                {
                    field: 'PartsName',
                    name: 'Nama Material'
                },
                {
                    field: 'Status',
                    name: 'Status'
                },
                {
                    field: 'ETA',
                    name: 'ETA'
                },
                {
                    field: 'Qty',
                    name: 'Permintaan Qty'
                },
                {
                    field: 'QtyGI',
                    name: 'Issue Qty'
                },
                {
                    field: 'Satuan.Name',
                    name: 'Satuan'
                }
            ]
        };


        // $scope.SimpanNote = function(){

        //   RepairProcessGR.putTeknisiNote($scope.JbId, $scope.mData.TechnicianNotes)
        //     .then(

        //       function(res) {
        //         //console.log("suksess");
        //       },
        //       function(err) {
        //         //console.log("err=>", err);
        //       }
        //   );
        //   // //console.log("faisal merasa merdeka");
        // }


        $scope.TestDrive = function() {
            RepairProcessGR.CheckIsJobCancelWO($scope.JbId).then(function (resultC) {
                if (resultC.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                    });
                } else {

                    RepairProcessGR.getStatWO($scope.JbId, 11).then(function(resux2) { // di kasi 11 karena pasti uda click tab FI jadi stat na 11
                        if (resux2.data == 1){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Kendaraan sudah Final Inspection"
                            });
                        } else {
                            RepairProcessGR.CheckKendaraanGateInOut($scope.JbId, $scope.xDataWoList[0].PoliceNumber,2).then(function (resultnya) {
                                var splitRes = resultnya.data.split('#')
                                    
                                if (splitRes[0] == 999){
                                    bsAlert.warning("Kendaraan sudah berada di list Gate Out security untuk aktivitas " + splitRes[1]);
                                    return;
                                } else if (splitRes[0] == 666){
                                    bsAlert.warning("Kendaraan sedang berada di luar Cabang/Bengkel untuk aktivitas " + splitRes[1]);
                                    return;
                                } else {
                                    //console.log("test drive", $scope.JbId);
                                    $scope.disabletestdrive = true;
        
                                    RepairProcessGR.TestDrive($scope.JbId).then(function(res) {
                                            //console.log("test drive success");
                                            if (res.data == -1) {
                                                alert('Kendaraan Sedang TestDrive');
                                                $scope.disabletestdrive = false;
                                            } else {
                                                setTimeout(function() { $scope.disabletestdrive = false; }, 30000);
                                                bsAlert.alert({
                                                        title: "Test Drive Berhasil",
                                                        text: "",
                                                        type: "success"
                                                    },
                                                    function() {},
                                                    function() {}
                                                )
                                            };
                                        },
                                        function(err) {
                                            $scope.disabletestdrive = false;
                                            //console.log("err=>", err);
                                        }
                                    );
                                }
                            })

                        }
                    })
                    

                }

            });
            
        };
        //----------------------------------
        $scope.search0 = function(item) {
            if (item.Status == 22 || item.Status == 4) {
                return true;
            }
            return false;
        };

        $scope.search = function(item) {
            if ((item.Status >= 5 && item.Status <= 7) || item.Status == 25) {
                return true;
            }
            return false;
        }


        $scope.search2 = function(item) {
            if (item.Status == 8 || item.Status == 11) {
                return true;
            }
            return false;
        }

        $scope.StatusFixedBtn = function(value) {
            if (value == 1) {
                $scope.StatusFixed = true;
                $scope.StatusNotFixed = false;
            } else if (value == 2) {
                $scope.StatusNotFixed = true;
                $scope.StatusFixed = false;
            }
        }

        $scope.mData.IsChecked = 0;
        $scope.mData.IsRepaired = 0;
        $scope.StatusFixedFA = false;
        $scope.ShowFA = function(value) {
            if (value == 1) {
                $scope.StatusFixedFA = true;
            } else if (value == 2) {
                $scope.StatusFixedFA = false;
            }
        }

        $scope.IsNeedAdditionalTime = function(TimeValue) {
            if (TimeValue == 1) {
                $scope.duration = true;
            } else if (TimeValue == 2) {
                $scope.duration = false;
            }
        }

        $scope.Batal = function(model, Type) {
            if (Type == 1) {
                model.JobSuggest = "";
                model.CategoryJobSuggestKeamanan = false;
                model.CategoryJobSuggestPeraturan = false;
                model.CategoryJobSuggestKenyamanan = false;
                model.CategoryJobSuggestEfisiensi = false;
                model.NextJob = "";
            } else if (Type == 2) {
                $scope.showPause = false;
                model.ResponseDesc = "";
                model.T2 = "";
                model.T1 = "";
                model.TroubleDesc = "";
                model.Duration = "";
                $scope.duration = false;
                model.IsNeedAdditionalTime = false;
                model.IsOwnDo = false;
                model.IsNeedRSA = false;
                model.PauseReasonDesc = "";
                model.PauseResaon = "";
            }
        }

        $scope.show_modalTask = { show: false };
        $scope.modal_modelTask = [];
        $scope.modalModeTask = 'new';
        $scope.addPekerjaan = function() {
            $scope.modal_modelTask = [];
            $scope.show_modalTask = { show: true };
        }

        $scope.doAddPekerjaan = function(Data) {

            console.log("this is add perkerjaan", $scope.mData, $scope.mData.PoliceNumber);
            // Data.Message = $scope.mData.PoliceNumber;
            Data.Ref = $scope.mData.PoliceNumber;
            // Data.MessageNew = "Mohon untuk tambah pekerjaan " + Data.Message + " dan Plotting Ulang Chip."
            RepairProcessGR.sendNotif(Data, $scope.IdSA, 13).then(
                function(res) {
                    //console.log('Suksess simpan');

                    RepairProcessGR.updateFlagForAddTask($scope.JbId).then(
                        function(res) {
                            //console.log('Suksess update');
                            $scope.show_modalTask = { show: false };
                            $scope.getDataDetail($scope.JbId);
                            ngDialog.closeAll();
                        },
                        function(err) {
                            //console.log("err=>", err);
                        }
                    );

                },
                function(err) {
                    //console.log("err=>", err);
                }
            );
        }

        $scope.show_modal = { show: false };
        $scope.modal_model = [];
        $scope.modalMode = 'new';
        $scope.addMaterial = function() {
            //console.log("data wo", dataDetailJob[0]);
            $scope.show_modal = { show: true };
        }


        $scope.doAddMaterial = function(Data) {
            // //console.log("this is add Material");
            console.log("this is add Material", Data);
            // //console.log("ModelData",ModelData);
            Data.Ref = $scope.mData.PoliceNumber;
            // Data.NewMessage = "Mohon untuk tambah parts " + Data.Message;
            RepairProcessGR.addTask(Data, $scope.IdSA, 14).then(
                function(res) {
                    //console.log('Suksess simpan');

                    RepairProcessGR.updateFlagForAddTask($scope.JbId).then(
                        function(res) {
                            //console.log('Suksess update');
                            $scope.show_modal = { show: false };
                            $scope.getDataDetail($scope.JbId);
                            ngDialog.closeAll();

                        },
                        function(err) {
                            //console.log("err=>", err);
                        }
                    );

                },
                function(err) {
                    //console.log("err=>", err);
                }
            );
        }

        $scope.rescheduleCancel = function() {
            //console.log('itemcancel');
        }

        $scope.checkAvailableTech = function(ModelDta, JobId) {
            //console.log("ModelDta", ModelDta);
            //console.log("JobId", JobId);
            var tmpTechId = "";
            // var returnVal = 0;
            if (ModelDta.length > 0) {
                // for(var i in ModelDta){
                //     ModelDta[i].JobTaskTechnician[0].
                // }

                tmpTechId = ModelDta[0].JobTaskTechnician[0].MProfileTechnicianId;
                return RepairProcessGR.checkAvailabilityForTechnician(tmpTechId).success(function(res) {
                    //console.log("resnya", res.data);
                    returnVal = res.data;
                    return returnVal;
                });
                // //console.log('returnValue', returnValue);

                // if (returnValue.$$state.value == 0) {
                //     return true;
                //     // returnVal = true;
                // } else {
                //     bsNotify.show({
                //         title: 'Teknisi sedang dalam proses Clock On Silahkan Teknisi Clock Off terlebih dahulu',
                //         text: 'Silahkan Teknisi Clock Off terlebih dahulu',
                //         size: 'big',
                //         timeout: 5000,
                //         type: 'danger'
                //     });
                //     return false;
                // }
            }
            // return returnVal;
        }
        $scope.clockOnWo = function(ModelDta, JobId, statuschip) {

            RepairProcessGR.GetJobTasks(JobId).then(function (resultJ) {
                var ada_job_dihapus = 0;
                var ada_job_ditambah = 0;
                if (resultJ.data.length > $scope.copy_dataTask.length){
                    ada_job_ditambah++
                }
                for (var q=0; q<$scope.copy_dataTask.length; q++){
                    for (var d=0; d<resultJ.data.length; d++){
                        if ($scope.copy_dataTask[q].JobTaskId == resultJ.data[d].JobTaskId){
                            if (resultJ.data[d].isDeleted == 1 && $scope.copy_dataTask[q].isDeleted == 0){
                                // berarti ada yg di delete tp si foreman blm refresh
                                ada_job_dihapus++
                            }
                        }
                    }
                }

                if (ada_job_dihapus > 0 || ada_job_ditambah > 0){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Terdapat update data pada menu lain, silahkan klik kembali lalu ulangi proses ini.",
                    });
                    return
                } else {
                    RepairProcessGR.CheckIsJobCancelWO(JobId).then(function (resultC) {
                        if (resultC.data == 666){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                            });
                        } else {
                            RepairProcessGR.CheckPrediagnoseGate(JobId, $scope.xDataWoList[0].PoliceNumber).then(function (result3) {
                                if (result3.data == 666){
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Kendaraan belum Gate In dari Prediagnose",
                                    });
                                    return;
                                } else if (result3.data == 999){
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Kendaraan sedang menunggu Gate Out proses Prediagnose",
                                    });
                                    return;
                                } else {
                                    RepairProcessGR.getStatWO($scope.xDataWoList[0].JobId, $scope.xDataWoList[0].Status).then(function(resux2) { 
                                        if (resux2.data == 1){
                                            bsNotify.show({
                                                size: 'big',
                                                type: 'danger',
                                                title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                                            });
                                        } else {
                                            RepairProcessGR.CekStatusClockOn($scope.xDataWoList[0].JobId, $scope.xDataWoList[0].stallIdSplit).then(function(resux3) { 
                                                if (resux3.data !== '0'){
                                                    var sem = resux3.data.split('#');
                                                    var nopolOn = sem[1]
                                                    bsNotify.show({
                                                        size: 'big',
                                                        type: 'danger',
                                                        title: "Nopol " + nopolOn + " sedang clock on pada stall ini. Silahkan selesaikan dan clock off nopol " + nopolOn + " dahulu."
                                                    });
                                                } else {
                                                    console.log('modeldata oi', ModelDta);
                                                    console.log('jobid oi', JobId);
                                                    console.log('statuschip oi', statuschip);
                                        
                                                    ModelDta = ModelDta.sort(function(a, b){
                                                        var dataA=new Date(a.isDeleted), dataB=new Date(b.isDeleted)
                                                        return dataB-dataA //sort by date ascending
                                                    })
                                                    // $scope.GetDataBoard();
                                                    //console.log('ModelDta', ModelDta);
                                                    // //console.log($scope.statusClock2);
                                                    // //console.log('billi', $scope.checkAvailableTech(ModelDta, JobId));
                                                    // var checkAvaTech = $scope.checkAvailableTech(ModelDta, JobId);
                                                    // //console.log('checkAvaTech', checkAvaTech);
                                                    // //console.log('checkAvaTech.$$state.value', checkAvaTech.$$state.value);
                                        
                                                    function cekTech(id, dataLength, countLength) {
                                                        return RepairProcessGR.checkAvailabilityForTechnician(id).then(function(res) {
                                                            var returnVal = res.data;
                                                            if (returnVal == 0) {
                                                                if ($scope.statusClock2 == 'on') {
                                                                    bsNotify.show({
                                                                        title: 'Mohon Clock Off Unit Sebelumnya',
                                                                        text: 'Mohon Clock Off Unit Sebelumnya',
                                                                        size: 'big',
                                                                        timeout: 2000,
                                                                        type: 'danger'
                                                                    });
                                                                    return 0;
                                                                } else {
                                                                    if (dataLength == countLength) {
                                                                        // return 1;
                                                                        $scope.doClockOnWo(ModelDta, JobId);
                                                                        // console.log('uweiii')
                                                                    }
                                                                }
                                                            } else {
                                                                bsNotify.show({
                                                                    title: 'Teknisi sedang dalam proses Clock On Silahkan Teknisi Clock Off terlebih dahulu',
                                                                    text: 'Silahkan Teknisi Clock Off terlebih dahulu',
                                                                    size: 'big',
                                                                    timeout: 5000,
                                                                    type: 'danger'
                                                                });
                                                                return 0;
                                                            };
                                                        });
                                                    }
                                        
                                                    function validasi(data) {
                                                        var dataLength = data.length;
                                                        var countLength = 0;
                                                        $scope.idteknisiaktif = []
                                                        _.map(data, function(val) {
                                                            countLength++;
                                                            if (val.isDeleted == 0) {
                                                                if (val.JobTaskTechnician.length == 0){
                                                                    bsNotify.show({
                                                                        type: 'danger',
                                                                        title: "Mohon isi teknisi terlebih dahulu",
                                                                        text: 'I will close in 2 seconds.',
                                                                        size: 'big',
                                                                        timeout: 3000
                                                                    });
                                                                    return;
                                                                }
                                                                _.map(val.JobTaskTechnician, function(valTech) {
                                                                    var obj = {}
                                                                    obj.Id = 0;
                                                                    if (valTech.isActive == 1){
                                                                        // cekTech(valTech.MProfileTechnicianId, dataLength, countLength);
                                                                        obj.Id = valTech.MProfileTechnicianId
                                                                        $scope.idteknisiaktif.push(obj)
                                                                    }
                                                                    // console.log('cektech', valx)
                                                                });
                                                            };
                                                        });
                                                    }
                                        
                                                    if (ModelDta.length > 0) {
                                                        var JobTeknisinih = [];
                                                        for (var i = 0; i < $scope.xDataWoList.length; i++) {
                                                            for (var a = 0; a < $scope.xDataWoList[i].JobTask.length; a++) {
                                                                //console.log("data JObTask", $scope.xDataWoList[i].JobTask[a]);
                                                                if ($scope.xDataWoList[i].JobTask[a].isDeleted == 0) {
                                                                    for (var z = 0; z < $scope.xDataWoList[i].JobTask[a].JobTaskTechnician.length; z++) {
                                                                        JobTeknisinih = $scope.xDataWoList[i].JobTask[a].JobTaskTechnician[z];
                                                                    };
                                                                };
                                                            };
                                                        };
                                        
                                                        if ((JobTeknisinih.length == 0) || (statuschip == 4)) {
                                                            bsNotify.show({
                                                                type: 'danger',
                                                                title: "Mohon Lakukan Job Dispatch Terlebih Dahulu",
                                                                text: 'I will close in 2 seconds.',
                                                                size: 'big',
                                                                timeout: 2000
                                                            });
                                                        } else {
                                                            // var deferred = $q.defer();
                                                            // console.log('deferred', deferred)
                                                            // var validation = validasi(ModelDta);
                                                            validasi(ModelDta);
                                                            RepairProcessGR.NewCheckTechClockOn($scope.idteknisiaktif).then(function(resss) {
                                                                if (resss.data == 0){
                                                                    $scope.doClockOnWo(ModelDta, JobId);
                                                                } else {
                                                                    for (var i=0; i<resss.data.length; i++){
                                                                        var dato = resss.data[i]
                                                                        bsNotify.show({
                                                                            title: 'Teknisi '+ dato.TechName + ' sedang proses Clock On No Polisi ' + dato.PoliceNumber + '. Silahkan Melakukan Clock Off terlebih dahulu.',
                                                                            text: 'Silahkan Teknisi Clock Off terlebih dahulu',
                                                                            size: 'big',
                                                                            type: 'danger'
                                                                        });
                                                                    }
                                                                    
                                                                }
                                                            })
                                                            // console.log('validation', validation);
                                                            // if (validation) {
                                                            //     // $scope.doClockOnWo(ModelDta, JobId);
                                                            //     bsNotify.show({
                                                            //         type: 'danger',
                                                            //         title: "Harusnya Uda bisa Clock-On",
                                                            //         text: 'I will close in 2 seconds.',
                                                            //         size: 'big',
                                                            //         timeout: 2000
                                                            //     });
                                                            // } else {
                                                            //     bsNotify.show({
                                                            //         type: 'danger',
                                                            //         title: "Harusnya Uda bisa Clock-On, tapi asynchronous",
                                                            //         text: 'I will close in 2 seconds.',
                                                            //         size: 'big',
                                                            //         timeout: 2000
                                                            //     });
                                                            // }
                                                            // //     _.map(ModelDta, function(val) {
                                                            //         if (val.isDeleted == 0) {
                                                            //             _.map(val.JobTaskTechnician, function(valTech) {
                                                            //                 RepairProcessGR.checkAvailabilityForTechnician(valTech.MProfileTechnicianId).then(function(res) {
                                                            //                     var returnVal = res.data;
                                                            //                     if (returnVal == 0) {
                                                            //                         if ($scope.statusClock2 == 'on') {
                                                            //                             bsNotify.show({
                                                            //                                 title: 'Mohon Clock Off Unit Sebelumnya',
                                                            //                                 text: 'Mohon Clock Off Unit Sebelumnya',
                                                            //                                 size: 'big',
                                                            //                                 timeout: 2000,
                                                            //                                 type: 'danger'
                                                            //                             });
                                                            //                         };
                                                            //                     } else {
                                                            //                         bsNotify.show({
                                                            //                             title: 'Teknisi sedang dalam proses Clock On Silahkan Teknisi Clock Off terlebih dahulu',
                                                            //                             text: 'Silahkan Teknisi Clock Off terlebih dahulu',
                                                            //                             size: 'big',
                                                            //                             timeout: 5000,
                                                            //                             type: 'danger'
                                                            //                         });
                                                            //                     };
                                                            //                 });
                                                            //             });
                                                            //         };
                                                            //     });
                                                        };
                                                    };
                                        
                                        
                                                    // gk dipake lagi
                                                    // if (ModelDta.length > 0) {
                                                    //     if (ModelDta[0].JobTaskTechnician.length > 0) {
                                                    //         var tmpTechId = ModelDta[0].JobTaskTechnician[0].MProfileTechnicianId;
                                                    //         RepairProcessGR.checkAvailabilityForTechnician(tmpTechId).then(function(res) {
                                                    //             var returnVal = res.data;
                                                    //             //console.log("returnVal", returnVal);
                                                    //             if (returnVal == 0) {
                                                    //                 //console.log('nasuk kesini gk sih oiii!');
                                                    //                 if ($scope.statusClock2 == 'on') {
                                                    //                     bsNotify.show({
                                                    //                         title: 'Mohon Clock Off Unit Sebelumnya',
                                                    //                         text: 'Mohon Clock Off Unit Sebelumnya',
                                                    //                         size: 'big',
                                                    //                         timeout: 2000,
                                                    //                         type: 'danger'
                                                    //                     });
                                                    //                 } else {
                                                    //                     //console.log("job id nih ", JobId);
                                                    //                     var JobTeknisinih = [];
                                                    //                     for (var i = 0; i < $scope.xDataWoList.length; i++) {
                                        
                                                    //                         //console.log("data wo", $scope.xDataWoList[i]);
                                        
                                                    //                         for (var a = 0; a < $scope.xDataWoList[i].JobTask.length; a++) {
                                                    //                             //console.log("data JObTask", $scope.xDataWoList[i].JobTask[a]);
                                                    //                             if ($scope.xDataWoList[i].JobTask[a].isDeleted == 0) {
                                                    //                                 for (var z = 0; z < $scope.xDataWoList[i].JobTask[a].JobTaskTechnician.length; z++) {
                                                    //                                     JobTeknisinih = $scope.xDataWoList[i].JobTask[a].JobTaskTechnician[z];
                                                    //                                     //console.log("job Teksnisi", JobTeknisinih);
                                                    //                                 }
                                                    //                             }
                                        
                                                    //                         }
                                        
                                                    //                     }
                                                    //                     //console.log("jobTeknisi", JobTeknisinih);
                                                    //                     if ((JobTeknisinih.length == 0) || (statuschip == 4)) {
                                                    //                         // //console.log("gagal");
                                                    //                         bsNotify.show({
                                                    //                             type: 'danger',
                                                    //                             title: "Mohon Lakukan Job Dispatch Terlebih Dahulu",
                                                    //                             text: 'I will close in 2 seconds.',
                                                    //                             size: 'big',
                                                    //                             timeout: 2000
                                                    //                         });
                                                    //                     } else {
                                                    //                         $scope.doClockOnWo(ModelDta, JobId);
                                        
                                        
                                                    //                         // ngDialog.openConfirm ({
                                                    //                         // template:'\
                                                    //                         //              <p>Anda yakin untuk clock on</p>\
                                                    //                         //              <div class="ngdialog-buttons">\
                                                    //                         //                <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog()">Tidak</button>\
                                                    //                         //                <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm()">Ya</button>\
                                                    //                         //              </div>',
                                                    //                         //           plain: true,
                                                    //                         //           controller: 'RepairProcessGRController',
                                                    //                         // }).then(function (value) {
                                                    //                         // }, function (value) {
                                                    //                         //Do something
                                                    //                         ////console.log("else",value);
                                                    //                         // });
                                                    //                     }
                                        
                                                    //                 }
                                                    //             } else {
                                                    //                 //console.log('nasuk na kesini gk sih oiii!');
                                                    //                 bsNotify.show({
                                                    //                     title: 'Teknisi sedang dalam proses Clock On Silahkan Teknisi Clock Off terlebih dahulu',
                                                    //                     text: 'Silahkan Teknisi Clock Off terlebih dahulu',
                                                    //                     size: 'big',
                                                    //                     timeout: 5000,
                                                    //                     type: 'danger'
                                                    //                 });
                                                    //             };
                                                    //         });
                                                    //     } else {
                                                    //         bsNotify.show({
                                                    //             title: 'Data ini belum di Dispatch atau data teknisi salah satu pekerjaan masih kosong',
                                                    //             text: 'Silahkan Dispatch ulang dan pilih teknisi',
                                                    //             size: 'big',
                                                    //             timeout: 5000,
                                                    //             type: 'danger'
                                                    //         });
                                                    //     }
                                                    // };
        
                                                }
                                            })
                                            
                                        }
                                    })
        
                                }
        
                            })
        
                            
        
                        }
        
                    });

                }


            })

            
            
            
        };

        $scope.doClockOnWo = function(ModelDta, JobId) {
            RepairProcessGR.GetStatusJobFromProduction($scope.xDataWoList[0]).then(function(resxx) {
                if (resxx.data == 1) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Chip masih berada di Job Stoppage, silahkan plotting kembali",
                    });
                } else {
                    console.log("clockOnWO gelondongan", ModelDta);
                    $scope.getActualTimeForClock();


                    // ------------------------------------------ validasi clock on minimal 15 mnt sebelom plan (ga blh kecepetan), dan ga blh di hari berikutnya (max harus di hari yg sama) ---------------------------------------- start
                    if ($scope.xDataWoList[0].JobListSplitChip.length > 0){
                        // ada split, cek dl mana yg aktif dan ga ada di stopage, dan isfi tidak 1
                        var data_split = $scope.xDataWoList[0].JobListSplitChip
                        for (var i=0; i<data_split.length; i++){
                            if (data_split[i].isSplitActive == 1 && data_split[i].isFI != 1 && data_split[i].IsInJobStopageGR != 1){
                                var thn_planS = data_split[i].PlanDateStart.getFullYear()
                                var bln_planS = data_split[i].PlanDateStart.getMonth()
                                var tgl_planS = data_split[i].PlanDateStart.getDate()
                                var jam_planS = data_split[i].PlanStart.split(':')[0]
                                var mnt_planS = data_split[i].PlanStart.split(':')[1]
                               
                                var onlyDate_planS = new Date(thn_planS, bln_planS, tgl_planS, 0, 0, 0)
                                var date_time_planS = new Date(thn_planS, bln_planS, tgl_planS, jam_planS, mnt_planS, 0)

                                var date_time_ClockOns = new Date()
                                var onlyDate_ClockOns = new Date(date_time_ClockOns.getFullYear(), date_time_ClockOns.getMonth(), date_time_ClockOns.getDate(), 0,0,0)

                                console.log('hiyaaa',date_time_planS.getTime())
                                console.log('hiyaaa',date_time_ClockOns.getTime())

                                console.log('hiyaaa',onlyDate_planS.getTime())
                                console.log('hiyaaa',onlyDate_ClockOns.getTime())

                                if (date_time_planS.getTime() > date_time_ClockOns.getTime()){
                                    // cek dl, kata nya clock on ga blh kecepetan. minimal 15mnt sebelum plan
                                    if ((date_time_ClockOns.getTime()+900000) >= date_time_planS.getTime()){
                                        // boleh clock on karena di range 15 mnt sebelom plan
                                        var xx = 'a'
                                        break;
                                    } else {
                                        bsAlert.warning("Waktu Clock On Minimal 15 Menit Sebelum Jam Plan atau Maksimal Di Akhir Hari Yang Sama Setelah Jam Plan. Harap hubungi PTM untuk melakukan Ploting Ulang");
                                        return;
                                    }
                                } else if (date_time_planS.getTime() < date_time_ClockOns.getTime()) {
                                    if (onlyDate_planS.getTime() < onlyDate_ClockOns.getTime()){
                                        // ga blh clock on nih, chip nya ada di hari kemarin2
                                        bsAlert.warning("Waktu Clock On Minimal 15 Menit Sebelum Jam Plan atau Maksimal Di Akhir Hari Yang Sama Setelah Jam Plan. Harap hubungi PTM untuk melakukan Ploting Ulang");
                                        return;
                                    } else {
                                        // boleh aja clock on, hrs nya ini chip di hari yg sama tapi memang jam nya kelewat
                                        var xx = 'a'
                                        break;
                                    }
                                }

                              
                            }
                        }

                    } else {
                        // kaga ada split na nih
                        var thn_planS = $scope.xDataWoList[0].PlanDateStart.getFullYear()
                        var bln_planS = $scope.xDataWoList[0].PlanDateStart.getMonth()
                        var tgl_planS = $scope.xDataWoList[0].PlanDateStart.getDate()
                        var jam_planS = $scope.xDataWoList[0].PlanStart.split(':')[0]
                        var mnt_planS = $scope.xDataWoList[0].PlanStart.split(':')[1]

                        var onlyDate_planS = new Date(thn_planS, bln_planS, tgl_planS, 0, 0, 0)
                        var date_time_planS = new Date(thn_planS, bln_planS, tgl_planS, jam_planS, mnt_planS, 0)

                        var date_time_ClockOns = new Date()
                        var onlyDate_ClockOns = new Date(date_time_ClockOns.getFullYear(), date_time_ClockOns.getMonth(), date_time_ClockOns.getDate(), 0,0,0)

                        console.log('hiyaaa',date_time_planS.getTime())
                        console.log('hiyaaa',date_time_ClockOns.getTime())

                        console.log('hiyaaa',onlyDate_planS.getTime())
                        console.log('hiyaaa',onlyDate_ClockOns.getTime())

                        if (date_time_planS.getTime() > date_time_ClockOns.getTime()){
                            // cek dl, kata nya clock on ga blh kecepetan. minimal 15mnt sebelum plan
                            if ((date_time_ClockOns.getTime()+900000) >= date_time_planS.getTime()){
                                // boleh clock on karena di range 15 mnt sebelom plan
                                var xx = 'a'
                            } else {
                                bsAlert.warning("Waktu Clock On Minimal 15 Menit Sebelum Jam Plan atau Maksimal Di Akhir Hari Yang Sama Setelah Jam Plan. Harap hubungi PTM untuk melakukan Ploting Ulang");
                                return;
                            }
                        } else if (date_time_planS.getTime() < date_time_ClockOns.getTime()) {
                            if (onlyDate_planS.getTime() < onlyDate_ClockOns.getTime()){
                                // ga blh clock on nih, chip nya ada di hari kemarin2
                                bsAlert.warning("Waktu Clock On Minimal 15 Menit Sebelum Jam Plan atau Maksimal Di Akhir Hari Yang Sama Setelah Jam Plan. Harap hubungi PTM untuk melakukan Ploting Ulang");
                                return;
                            } else {
                                // boleh aja clock on, hrs nya ini chip di hari yg sama tapi memang jam nya kelewat
                                var xx = 'a'
                            }
                        }

                    }
                    // ------------------------------------------ validasi clock on minimal 15 mnt sebelom plan (ga blh kecepetan), dan ga blh di hari berikutnya (max harus di hari yg sama) ---------------------------------------- end

        
                    if ($scope.xDataWoList[0].Revision != 0) {
        
                        var Model = [];
                        angular.forEach(ModelDta, function(row) {
                            if (row.isDeleted == 0) {
                                if ($scope.xDataWoList[0].Revision >= row.Revision) {
                                    Model.push(row);
                                    //console.log("Model Yang Di Push", Model);
                                }
                            }
                        });
        
                        RepairProcessGR.putClockWoBase(1, Model, $scope.actualFullTime, $scope.xDataWoList[0].stallIdSplit).then(
                            function(res) {
                                bsNotify.show({
                                    title: 'Clock On Berhasil',
                                    text: 'I will close in 2 seconds.',
                                    size: 'big',
                                    timeout: 2000,
                                    type: 'success'
                                });
                                $scope.getDataDetail(JobId);
                            },
                            function(err) {
                                //console.log("err=>", err);
                            }
                        );
                        ngDialog.closeAll();
        
                    } else {
        
                        RepairProcessGR.putClockWoBase(1, ModelDta, $scope.actualFullTime, $scope.xDataWoList[0].stallIdSplit).then(
                            function(res) {
                                bsNotify.show({
                                    title: 'Clock On Berhasil',
                                    text: 'I will close in 2 seconds.',
                                    size: 'big',
                                    timeout: 2000,
                                    type: 'success'
                                });
                                $scope.getDataDetail(JobId);
                            },
                            function(err) {
                                //console.log("err=>", err);
                            }
                        );
                        ngDialog.closeAll();
        
                    }
                }
            })
            
        }


        $scope.clockOnWoAfterPause = function(ModelDta, JobId) {
            RepairProcessGR.GetJobTasks(JobId).then(function (resultJ) {
                var ada_job_dihapus = 0;
                var ada_job_ditambah = 0;
                if (resultJ.data.length > $scope.copy_dataTask.length){
                    ada_job_ditambah++
                }
                for (var q=0; q<$scope.copy_dataTask.length; q++){
                    for (var d=0; d<resultJ.data.length; d++){
                        if ($scope.copy_dataTask[q].JobTaskId == resultJ.data[d].JobTaskId){
                            if (resultJ.data[d].isDeleted == 1 && $scope.copy_dataTask[q].isDeleted == 0){
                                // berarti ada yg di delete tp si foreman blm refresh
                                ada_job_dihapus++
                            }
                        }
                    }
                }

                if (ada_job_dihapus > 0 || ada_job_ditambah > 0){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Terdapat update data pada menu lain, silahkan klik kembali lalu ulangi proses ini.",
                    });
                    return
                } else {
                    RepairProcessGR.CheckIsJobCancelWO(JobId).then(function (resultC) {
                        if (resultC.data == 666){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                            });
                        } else {
                            RepairProcessGR.getStatWO($scope.xDataWoList[0].JobId, $scope.xDataWoList[0].Status).then(function(resux2) { 
                                if (resux2.data == 1){
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                                    });
                                } else {
                                    RepairProcessGR.CekStatusClockOn($scope.xDataWoList[0].JobId, $scope.xDataWoList[0].stallIdSplit).then(function(resux3) { 
                                        if (resux3.data !== '0'){
                                            var sem = resux3.data.split('#');
                                            var nopolOn = sem[1]
                                            bsNotify.show({
                                                size: 'big',
                                                type: 'danger',
                                                title: "Nopol " + nopolOn + " sedang clock on pada stall ini. Silahkan selesaikan dan clock off nopol " + nopolOn + " dahulu."
                                            });
                                        } else {
                                            //console.log("job id nih ", JobId);
                                            // ngDialog.openConfirm ({
                                            // template:'\
                                            //              <p>Anda yakin untuk clock on</p>\
                                            //              <div class="ngdialog-buttons">\
                                            //                <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog()">Tidak</button>\
                                            //                <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm()">Ya</button>\
                                            //              </div>',
                                            //           plain: true,
                                            //           controller: 'RepairProcessGRController',
                                            // }).then(function (value) {
                                            console.log('cek model nya', ModelDta)
                                            var counterIsStoppage = 0;
                                            if ($scope.xDataWoList[0].JobListSplitChip != null){
                                                if ($scope.xDataWoList[0].JobListSplitChip.length > 0){
                                                    for (var i=0; i<$scope.xDataWoList[0].JobListSplitChip.length; i++){
                                                        if ($scope.xDataWoList[0].JobListSplitChip[i].IsInJobStopageGR == 1){
                                                            counterIsStoppage++;
                                                        }
                                                    }
                                                } else {
                                                    counterIsStoppage = $scope.xDataWoList[0].IsStopPage;
                                                }
                                            } else {
                                                counterIsStoppage = $scope.xDataWoList[0].IsStopPage;
                                            }
                                
                                            if (counterIsStoppage == 0){
                                                $scope.doClockOnWoAfterPause(ModelDta, JobId);
                                                console.log('lah kok bisa');
                                            } else {
                                                bsNotify.show({
                                                    title: 'Pekerjaan sedang dalam Job Stoppage, silahkan pindahkan ke stall',
                                                    text: '',
                                                    size: 'big',
                                                    // timeout: 3000,
                                                    type: 'danger'
                                                });
                                            }
                                            // }, function (value) {
                                            //Do something
                                            ////console.log("else",value);
                                            // });
        
                                        }
                                    })
                                    
                                }
                            })
        
                        }
        
                    });

                }


            })

            
            
            
        }

        $scope.doClockOnWoAfterPause = function(ModelDta, JobId) {
            RepairProcessGR.GetStatusJobFromProduction($scope.xDataWoList[0]).then(function(resxx) {
                if (resxx.data == 1) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Chip masih berada di Job Stoppage, silahkan plotting kembali",
                    });
                } else {
                    console.log("ModelDta doClockOnWoAfterPause", ModelDta);
                    $scope.getActualTimeForClock();

                    if ($scope.xDataWoList[0].Revision != 0) {

                        var Model = [];
                        angular.forEach(ModelDta, function(row) {
                            if (row.isDeleted == 0) {
                                if ($scope.xDataWoList[0].Revision >= row.Revision) {
                                    Model.push(row);
                                    //console.log("Model Yang Di Push", Model);
                                }
                            }
                            
                        });

                        RepairProcessGR.putClockWoBase(3, Model, $scope.actualFullTime, $scope.xDataWoList[0].stallIdSplit).then(
                            function(res) {
                                bsNotify.show({
                                    title: 'Clock On Berhasil',
                                    text: 'I will close in 2 seconds.',
                                    size: 'big',
                                    timeout: 2000,
                                    type: 'success'
                                });
                                $scope.getDataDetail(JobId);
                            },
                            function(err) {
                                //console.log("err=>", err);
                            }
                        );
                        ngDialog.closeAll();

                    } else {

                        RepairProcessGR.putClockWoBase(3, ModelDta, $scope.actualFullTime, $scope.xDataWoList[0].stallIdSplit).then(
                            function(res) {
                                bsNotify.show({
                                    title: 'Clock On Berhasil',
                                    text: 'I will close in 2 seconds.',
                                    size: 'big',
                                    timeout: 2000,
                                    type: 'success'
                                        // size: 'big'
                                        // size: 'big',
                                        // type: 'danger',
                                        // title: "Mohon isi hasil inspeksi dan tanggal job selanjutnya",
                                        // timer : 3000
                                });
                                $scope.getDataDetail(JobId);
                            },
                            function(err) {
                                //console.log("err=>", err);
                            }
                        );
                        ngDialog.closeAll();

                    }

                    //notif jika lanjut clockoff after pause tunggu part
                    if ($scope.xDataWoList[0].PauseReasonId == 38) {
                        //kirim notifikasi
                        var DataNotif = {};
                        var messagetemp = "";
                        messagetemp = $scope.xDataWoList[0].PoliceNumber;


                        DataNotif.Message = messagetemp;
                        RepairProcessGR.sendNotif(DataNotif, $scope.xDataWoList[0].UserIdWORelease, 18).then(
                            function(res) {

                            },
                            function(err) {
                                //console.log("err=>", err);
                            });
                    }
                    //notif jika lanjut clockoff after pause approve customer            
                    if ($scope.xDataWoList[0].PauseReasonId == 39) {
                        //kirim notifikasi
                        var DataNotif = {};
                        var messagetemp = "";
                        messagetemp = $scope.xDataWoList[0].PoliceNumber;


                        DataNotif.Message = messagetemp;
                        RepairProcessGR.sendNotif(DataNotif, $scope.xDataWoList[0].UserIdWORelease, 19).then(
                            function(res) {

                            },
                            function(err) {
                                //console.log("err=>", err);
                            });
                    }
                }
            })
            
        }

        $scope.clockOn = function(Model, JobTask) {
            console.log('Model oi', Model);
            RepairProcessGR.CheckIsJobCancelWO($scope.xDataWoList[0].JobId).then(function (resultC) {
                if (resultC.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                    });
                } else {
                    RepairProcessGR.CheckPrediagnoseGate($scope.xDataWoList[0].JobId, $scope.xDataWoList[0].PoliceNumber).then(function (result3) {
                        if (result3.data == 666){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Kendaraan belum Gate In dari Prediagnose",
                            });
                            return;
                        } else if (result3.data == 999){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Kendaraan sedang menunggu Gate Out proses Prediagnose",
                            });
                            return;
                        } else {
                            RepairProcessGR.getStatWO($scope.xDataWoList[0].JobId, $scope.xDataWoList[0].Status).then(function(resux2) { 
                                if (resux2.data == 1){
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                                    });
                                } else {
                                    RepairProcessGR.CekStatusClockOn($scope.xDataWoList[0].JobId, $scope.xDataWoList[0].stallIdSplit).then(function(resux3) { 
                                        if (resux3.data !== '0'){
                                            var sem = resux3.data.split('#');
                                            var nopolOn = sem[1]
                                            bsNotify.show({
                                                size: 'big',
                                                type: 'danger',
                                                title: "Nopol " + nopolOn + " sedang clock on pada stall ini. Silahkan selesaikan dan clock off nopol " + nopolOn + " dahulu."
                                            });
                                        } else {
                                            $scope.JobTaskForClockOn = JobTask;
                
                                            var JobTeknisinih = [];
                                            for (var i = 0; i < $scope.xDataWoList.length; i++) {
                                
                                                //console.log("data wo", $scope.xDataWoList[i]);
                                
                                                for (var a = 0; a < $scope.xDataWoList[i].JobTask.length; a++) {
                                                    //console.log("data JObTask", $scope.xDataWoList[i].JobTask[a]);
                                
                                                    for (var z = 0; z < $scope.xDataWoList[i].JobTask[a].JobTaskTechnician.length; z++) {
                                                        JobTeknisinih = $scope.xDataWoList[i].JobTask[a].JobTaskTechnician[z];
                                                        //console.log("job Teksnisi", JobTeknisinih);
                                                    }
                                
                                                }
                                
                                            }
                                            //console.log("jobTeknisi", JobTeknisinih);
                                            if (JobTeknisinih.length == 0) {
                                                // //console.log("gagal");
                                                // bsNotify.show(
                                                //                 {
                                                //                     size: 'big',
                                                //                     type: 'danger',
                                                //                     title: "Mohon Lakukan Job Dispatch Terlebih Dahulu"
                                                //                     // ,
                                                //                     // content: error.join('<br>'),
                                                //                     // number: error.length
                                                //                 }
                                                //             );
                                                bsNotify.show({
                                                    title: 'Mohon Lakukan Job Dispatch Terlebih Dahulu',
                                                    text: 'I will close in 2 seconds.',
                                                    size: 'big',
                                                    timeout: 2000,
                                                    type: 'danger'
                                                });
                                            } else {
                                                // ngDialog.openConfirm ({
                                                // template:'\
                                                //              <p>Anda yakin untuk clock on</p>\
                                                //              <div class="ngdialog-buttons">\
                                                //                <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog()">Tidak</button>\
                                                //                <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm()">Ya</button>\
                                                //              </div>',
                                                //           plain: true,
                                                //           controller: 'RepairProcessGRController',
                                                // }).then(function (value) {
                                                $scope.doClockOn(Model, $scope.JobTaskForClockOn);
                                                // }, function (value) {
                                
                                                //Do something
                                                ////console.log("else",value);
                                                // });
                                            }
                                        }
                                    })

                                    
                                }
                
                            })
                            
                        }
                    })
                    
                }

            });
            
            
        }

        $scope.doClockOn = function(Model, JobTaskForClockOn) {
            //console.log("clockOn");
            RepairProcessGR.GetStatusJobFromProduction($scope.xDataWoList[0]).then(function(resxx) {
                if (resxx.data == 1) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Chip masih berada di Job Stoppage, silahkan plotting kembali",
                    });
                } else {
                    $scope.getActualTimeForClock();

                    // ------------------------------------------ validasi clock on minimal 15 mnt sebelom plan (ga blh kecepetan), dan ga blh di hari berikutnya (max harus di hari yg sama) ---------------------------------------- start
                    if ($scope.xDataWoList[0].JobListSplitChip.length > 0){
                        // ada split, cek dl mana yg aktif dan ga ada di stopage, dan isfi tidak 1
                        var data_split = $scope.xDataWoList[0].JobListSplitChip
                        for (var i=0; i<data_split.length; i++){
                            if (data_split[i].isSplitActive == 1 && data_split[i].isFI != 1 && data_split[i].IsInJobStopageGR != 1){
                                var thn_planS = data_split[i].PlanDateStart.getFullYear()
                                var bln_planS = data_split[i].PlanDateStart.getMonth()
                                var tgl_planS = data_split[i].PlanDateStart.getDate()
                                var jam_planS = data_split[i].PlanStart.split(':')[0]
                                var mnt_planS = data_split[i].PlanStart.split(':')[1]
                               
                                var onlyDate_planS = new Date(thn_planS, bln_planS, tgl_planS, 0, 0, 0)
                                var date_time_planS = new Date(thn_planS, bln_planS, tgl_planS, jam_planS, mnt_planS, 0)
        
                                var date_time_ClockOns = new Date()
                                var onlyDate_ClockOns = new Date(date_time_ClockOns.getFullYear(), date_time_ClockOns.getMonth(), date_time_ClockOns.getDate(), 0,0,0)
        
                                console.log('hiyaaa',date_time_planS.getTime())
                                console.log('hiyaaa',date_time_ClockOns.getTime())
        
                                console.log('hiyaaa',onlyDate_planS.getTime())
                                console.log('hiyaaa',onlyDate_ClockOns.getTime())
        
                                if (date_time_planS.getTime() > date_time_ClockOns.getTime()){
                                    // cek dl, kata nya clock on ga blh kecepetan. minimal 15mnt sebelum plan
                                    if ((date_time_ClockOns.getTime()+900000) >= date_time_planS.getTime()){
                                        // boleh clock on karena di range 15 mnt sebelom plan
                                        var xx = 'a'
                                        break;
                                    } else {
                                        bsAlert.warning("Waktu Clock On Minimal 15 Menit Sebelum Jam Plan atau Maksimal Di Akhir Hari Yang Sama Setelah Jam Plan. Harap hubungi PTM untuk melakukan Ploting Ulang");
                                        return;
                                    }
                                } else if (date_time_planS.getTime() < date_time_ClockOns.getTime()) {
                                    if (onlyDate_planS.getTime() < onlyDate_ClockOns.getTime()){
                                        // ga blh clock on nih, chip nya ada di hari kemarin2
                                        bsAlert.warning("Waktu Clock On Minimal 15 Menit Sebelum Jam Plan atau Maksimal Di Akhir Hari Yang Sama Setelah Jam Plan. Harap hubungi PTM untuk melakukan Ploting Ulang");
                                        return;
                                    } else {
                                        // boleh aja clock on, hrs nya ini chip di hari yg sama tapi memang jam nya kelewat
                                        var xx = 'a'
                                        break;
                                    }
                                }
        
                              
                            }
                        }
        
                    } else {
                        // kaga ada split na nih
                        var thn_planS = $scope.xDataWoList[0].PlanDateStart.getFullYear()
                        var bln_planS = $scope.xDataWoList[0].PlanDateStart.getMonth()
                        var tgl_planS = $scope.xDataWoList[0].PlanDateStart.getDate()
                        var jam_planS = $scope.xDataWoList[0].PlanStart.split(':')[0]
                        var mnt_planS = $scope.xDataWoList[0].PlanStart.split(':')[1]
        
                        var onlyDate_planS = new Date(thn_planS, bln_planS, tgl_planS, 0, 0, 0)
                        var date_time_planS = new Date(thn_planS, bln_planS, tgl_planS, jam_planS, mnt_planS, 0)
        
                        var date_time_ClockOns = new Date()
                        var onlyDate_ClockOns = new Date(date_time_ClockOns.getFullYear(), date_time_ClockOns.getMonth(), date_time_ClockOns.getDate(), 0,0,0)
        
                        console.log('hiyaaa',date_time_planS.getTime())
                        console.log('hiyaaa',date_time_ClockOns.getTime())
        
                        console.log('hiyaaa',onlyDate_planS.getTime())
                        console.log('hiyaaa',onlyDate_ClockOns.getTime())
        
                        if (date_time_planS.getTime() > date_time_ClockOns.getTime()){
                            // cek dl, kata nya clock on ga blh kecepetan. minimal 15mnt sebelum plan
                            if ((date_time_ClockOns.getTime()+900000) >= date_time_planS.getTime()){
                                // boleh clock on karena di range 15 mnt sebelom plan
                                var xx = 'a'
                            } else {
                                bsAlert.warning("Waktu Clock On Minimal 15 Menit Sebelum Jam Plan atau Maksimal Di Akhir Hari Yang Sama Setelah Jam Plan. Harap hubungi PTM untuk melakukan Ploting Ulang");
                                return;
                            }
                        } else if (date_time_planS.getTime() < date_time_ClockOns.getTime()) {
                            if (onlyDate_planS.getTime() < onlyDate_ClockOns.getTime()){
                                // ga blh clock on nih, chip nya ada di hari kemarin2
                                bsAlert.warning("Waktu Clock On Minimal 15 Menit Sebelum Jam Plan atau Maksimal Di Akhir Hari Yang Sama Setelah Jam Plan. Harap hubungi PTM untuk melakukan Ploting Ulang");
                                return;
                            } else {
                                // boleh aja clock on, hrs nya ini chip di hari yg sama tapi memang jam nya kelewat
                                var xx = 'a'
                            }
                        }
        
                    }
                    // ------------------------------------------ validasi clock on minimal 15 mnt sebelom plan (ga blh kecepetan), dan ga blh di hari berikutnya (max harus di hari yg sama) ---------------------------------------- end
        
                    RepairProcessGR.putClockJobBase(1, Model, JobTaskForClockOn, $scope.actualFullTime, $scope.xDataWoList[0].stallIdSplit).then(
                        function(res) {
                            bsNotify.show({
                                title: 'Clock On Berhasil',
                                text: 'I will close in 2 seconds.',
                                size: 'big',
                                timeout: 2000,
                                type: 'success'
                            });
                            $scope.getDataDetail(Model.JobId, Model);
                        },
                        function(err) {
                            //console.log("err=>", err);
                        }
                    );
                    ngDialog.closeAll();
                    // return Model.Status
                }

            })
            
        }

        $scope.clockOnAfterPause = function(Model, JobTask) {
            RepairProcessGR.CheckIsJobCancelWO($scope.xDataWoList[0].JobId).then(function (resultC) {
                if (resultC.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                    });
                } else {
                    RepairProcessGR.getStatWO($scope.xDataWoList[0].JobId, $scope.xDataWoList[0].Status).then(function(resux2) { 
                        if (resux2.data == 1){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                            });
                        } else {
                            RepairProcessGR.CekStatusClockOn($scope.xDataWoList[0].JobId, $scope.xDataWoList[0].stallIdSplit).then(function(resux3) { 
                                if (resux3.data !== '0'){
                                    var sem = resux3.data.split('#');
                                    var nopolOn = sem[1]
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Nopol " + nopolOn + " sedang clock on pada stall ini. Silahkan selesaikan dan clock off nopol " + nopolOn + " dahulu."
                                    });
                                } else {
                                    $scope.JobTaskForClockOnAfter = JobTask;
                                    var JobTeknisinih = [];
                                    for (var i = 0; i < $scope.xDataWoList.length; i++) {
                        
                                        //console.log("data wo", $scope.xDataWoList[i]);
                        
                                        for (var a = 0; a < $scope.xDataWoList[i].JobTask.length; a++) {
                                            //console.log("data JObTask", $scope.xDataWoList[i].JobTask[a]);
                        
                                            for (var z = 0; z < $scope.xDataWoList[i].JobTask[a].JobTaskTechnician.length; z++) {
                                                JobTeknisinih = $scope.xDataWoList[i].JobTask[a].JobTaskTechnician[z];
                                                //console.log("job Teksnisi", JobTeknisinih);
                                            }
                        
                                        }
                        
                                    }
                                    //console.log("jobTeknisi", JobTeknisinih);
                                    // if(JobTeknisinih.length == 0){
                                    //   // //console.log("gagal");
                                    //           bsNotify.show(
                                    //                           {
                                    //                               size: 'big',
                                    //                               type: 'danger',
                                    //                               title: "Mohon Lakukan Job Dispatch Terlebih Dahulu"
                                    //                               // ,
                                    //                               // content: error.join('<br>'),
                                    //                               // number: error.length
                                    //                           }
                                    //                       );
                                    // }else{
                                    // ngDialog.openConfirm ({
                                    // template:'\
                                    //              <p>Anda yakin untuk clock on</p>\
                                    //              <div class="ngdialog-buttons">\
                                    //                <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog()">Tidak</button>\
                                    //                <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm()">Ya</button>\
                                    //              </div>',
                                    //           plain: true,
                                    //           controller: 'RepairProcessGRController',
                                    // }).then(function (value) {
                        
                                        console.log('cek model nya', Model)
                                        var counterIsStoppage = 0;
                                        if ($scope.xDataWoList[0].JobListSplitChip != null){
                                            if ($scope.xDataWoList[0].JobListSplitChip.length > 0){
                                                for (var i=0; i<$scope.xDataWoList[0].JobListSplitChip.length; i++){
                                                    if ($scope.xDataWoList[0].JobListSplitChip[i].IsInJobStopageGR == 1){
                                                        counterIsStoppage++;
                                                    }
                                                }
                                            } else {
                                                counterIsStoppage = $scope.xDataWoList[0].IsStopPage;
                                            }
                                        } else {
                                            counterIsStoppage = $scope.xDataWoList[0].IsStopPage;
                                        }
                            
                                        if (counterIsStoppage == 0){
                                            $scope.doClockOnAfterPause(Model, $scope.JobTaskForClockOnAfter);
                        
                                            // console.log('lah kok bisa');
                                        } else {
                                            bsNotify.show({
                                                title: 'Pekerjaan sedang dalam Job Stoppage, silahkan pindahkan ke stall',
                                                text: '',
                                                size: 'big',
                                                // timeout: 3000,
                                                type: 'danger'
                                            });
                                        }
                                    // $scope.doClockOnAfterPause(Model, $scope.JobTaskForClockOnAfter);
                        
                                    // }, function (value) {
                        
                                    //Do something
                                    ////console.log("else",value);
                                    // });
                                    // }
                                }
                            })
                            
                        }
                    })

                }

            });
            
            
        }

        $scope.doClockOnAfterPause = function(Model, JobTaskForClockOnAfter) {
            //console.log("clockOn");
            $scope.getActualTimeForClock();
            RepairProcessGR.putClockJobBase(3, Model, JobTaskForClockOnAfter, $scope.actualFullTime,  $scope.xDataWoList[0].stallIdSplit).then(
                function(res) {
                    bsNotify.show({
                        title: 'Clock On Berhasil',
                        text: 'I will close in 2 seconds.',
                        size: 'big',
                        timeout: 2000,
                        type: 'success'
                    });

                    // $scope.mDataJob.JobTaskF = null;
                    $scope.btnClockOffJob = false;
                    $scope.getDataDetail(Model.JobId, Model);
                },
                function(err) {
                    //console.log("err=>", err);
                }
            );
            ngDialog.closeAll();
            // return Model.Status
        }

        $scope.clockOffWo = function(ModelDta, JobId) {

            RepairProcessGR.GetJobTasks(JobId).then(function (resultJ) {
                var ada_job_dihapus = 0;
                var ada_job_ditambah = 0;
                if (resultJ.data.length > $scope.copy_dataTask.length){
                    ada_job_ditambah++
                }
                for (var q=0; q<$scope.copy_dataTask.length; q++){
                    for (var d=0; d<resultJ.data.length; d++){
                        if ($scope.copy_dataTask[q].JobTaskId == resultJ.data[d].JobTaskId){
                            if (resultJ.data[d].isDeleted == 1 && $scope.copy_dataTask[q].isDeleted == 0){
                                // berarti ada yg di delete tp si foreman blm refresh
                                ada_job_dihapus++
                            }
                        }
                    }
                }

                if (ada_job_dihapus > 0 || ada_job_ditambah > 0){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Terdapat update data pada menu lain, silahkan klik kembali lalu ulangi proses ini.",
                    });
                    return
                } else {
                    RepairProcessGR.CheckIsJobCancelWO(JobId).then(function (resultC) {
                        if (resultC.data == 666){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                            });
                        } else {
                            RepairProcessGR.getStatWO($scope.xDataWoList[0].JobId, $scope.xDataWoList[0].Status).then(function(resux2) { 
                                if (resux2.data == 1){
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                                    });
                                } else {
                                    var JobTeknisinih = [];
                                    var cekJobTeknisiKosong = 0;
                                    for (var i = 0; i < $scope.xDataWoList.length; i++) {
                        
                                        //console.log("data wo", $scope.xDataWoList[i]);
                        
                                        for (var a = 0; a < $scope.xDataWoList[i].JobTask.length; a++) {
                                            //console.log("data JObTask", $scope.xDataWoList[i].JobTask[a]);
                        
                                            for (var z = 0; z < $scope.xDataWoList[i].JobTask[a].JobTaskTechnician.length; z++) {
                                                JobTeknisinih = $scope.xDataWoList[i].JobTask[a].JobTaskTechnician[z];
                                                //console.log("job Teksnisi", JobTeknisinih);
                                            }
                        
                                        }
                        
                                    }
                                    function validasi(data) {
                                        var dataLength = data.length;
                                        var countLength = 0;
                                        _.map(data, function(val) {
                                            countLength++;
                                            if (val.isDeleted == 0) {
                                                if (val.JobTaskTechnician.length == 0){
                                                    bsNotify.show({
                                                        type: 'danger',
                                                        title: "Mohon isi teknisi terlebih dahulu",
                                                        text: 'I will close in 2 seconds.',
                                                        size: 'big',
                                                        timeout: 3000
                                                    });
                                                    cekJobTeknisiKosong = 1;
                                                    return true;
                                                }
                                                // _.map(val.JobTaskTechnician, function(valTech) {
                                                //     cekTech(valTech.MProfileTechnicianId, dataLength, countLength);
                                                //     // console.log('cektech', valx)
                                                // });
                                            };
                                        });
                                    }
                                    validasi(ModelDta);
                                    if (cekJobTeknisiKosong == 1){
                                        return true;
                                    } else {
                                        //console.log("jobTeknisi", JobTeknisinih);
                                        if (JobTeknisinih.length == 0) {
                                            // //console.log("gagal");
                                            // bsNotify.show(
                                            //                 {
                                            //                     size: 'big',
                                            //                     type: 'danger',
                                            //                     title: "Mohon Lakukan Job Dispatch Terlebih Dahulu"
                                            //                     // ,
                                            //                     // content: error.join('<br>'),
                                            //                     // number: error.length
                                            //                 }
                                            //             );
                                            bsNotify.show({
                                                title: 'Mohon Lakukan JOb Dispatch Terlebih Dahulu',
                                                text: 'I will close in 2 seconds.',
                                                size: 'big',
                                                timeout: 2000,
                                                type: 'danger'
                                            });
                                        } else {
                                            // ngDialog.openConfirm ({
                                            // template:'\
                                            //              <p>Anda yakin untuk clock off</p>\
                                            //              <div class="ngdialog-buttons">\
                                            //                <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog()">Tidak</button>\
                                            //                <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm()">Ya</button>\
                                            //              </div>',
                                            //           plain: true,
                                            //           controller: 'RepairProcessGRController',
                                            // }).then(function (value) {
                                            $scope.doClockOffWo(ModelDta, JobId);
                                            // }, function (value) {
                        
                                            //Do something
                                            ////console.log("else",value);
                                            // });
                                        }
                                    }
                                }
                            })
                        }
        
                    });

                }


            })

           
            
            
            

            
        }

        $scope.doClockOffWo = function(ModelDta, JobId) {
            RepairProcessGR.GetStatusJobFromProduction($scope.xDataWoList[0]).then(function(resxx) {
                if (resxx.data == 1) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Chip masih berada di Job Stoppage, silahkan plotting kembali",
                    });
                } else {
                    // //console.log("clockoff",Model);
                    // RepairProcessGR.putClockWoBase(4,Model).then(
                    //     function(res){
                    //         $scope.getDataDetail(JobId);
                    //     },
                    //     function(err) {
                    //       //console.log("err=>", err);
                    //     }
                    // );
                    // ngDialog.closeAll();
                    $scope.getActualTimeForClock();
                    if ($scope.xDataWoList[0].Revision != 0) {

                        var Model = [];
                        angular.forEach(ModelDta, function(row) {
                            if (row.isDeleted == 0){
                                if ($scope.xDataWoList[0].Revision >= row.Revision) {
                                    Model.push(row);
                                    //console.log("Model Yang Di Push", Model);
                                }
                            }
                        });

                        RepairProcessGR.putClockWoBase(4, Model, $scope.actualFullTime, $scope.xDataWoList[0].stallIdSplit).then(
                            function(res) {
                                bsNotify.show({
                                    title: 'Clock Off Berhasil',
                                    text: 'I will close in 2 seconds.',
                                    size: 'big',
                                    timeout: 2000,
                                    type: 'success'
                                });
                                $scope.getDataDetail(JobId);
                                $scope.Kembali();

                                //kirim notifikasi clock off
                                var DataNotif = {};
                                var messagetemp = "";
                                messagetemp = $scope.mData.PoliceNumber;


                                DataNotif.Message = messagetemp;
                                RepairProcessGR.sendNotifRole(DataNotif, 1031, 30).then(
                                    function(res) {

                                    },
                                    function(err) {
                                        //console.log("err=>", err);
                                    });
                            },
                            function(err) {
                                //console.log("err=>", err);
                            }
                        );
                        ngDialog.closeAll();

                    } else {

                        RepairProcessGR.putClockWoBase(4, ModelDta, $scope.actualFullTime, $scope.xDataWoList[0].stallIdSplit).then(
                            function(res) {
                                bsNotify.show({
                                    title: 'Clock Off Berhasil',
                                    text: 'I will close in 2 seconds.',
                                    size: 'big',
                                    timeout: 2000,
                                    type: 'success'
                                });
                                $scope.getDataDetail(JobId);
                                $scope.Kembali();


                                //kirim notifikasi clock off
                                var DataNotif = {};
                                var messagetemp = "";
                                messagetemp = $scope.mData.PoliceNumber;

                                DataNotif.Message = messagetemp;
                                RepairProcessGR.sendNotifRole(DataNotif, 1031, 30).then(
                                    function(res) {

                                    },
                                    function(err) {
                                        //console.log("err=>", err);
                                    });
                            },
                            function(err) {
                                //console.log("err=>", err);
                            }
                        );
                        ngDialog.closeAll();

                    }
                }
            })
            
        }

        // Status,JobTskId,JobId,TaskId,EmployeeId,Employee,TaskName,FR,Fare,PlanStart,PlanFinish,LastModifiedUserId,LastModifiedDate,ProcessId,Satuan,isTypeR,isTypeX,PaidById,Revision, Model
        $scope.clockOff = function(Model, JobTask) {
            RepairProcessGR.CheckIsJobCancelWO($scope.xDataWoList[0].JobId).then(function (resultC) {
                if (resultC.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                    });
                } else {
                    RepairProcessGR.getStatWO($scope.xDataWoList[0].JobId, $scope.xDataWoList[0].Status).then(function(resux2) { 
                        if (resux2.data == 1){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                            });
                        } else {
                            console.log('model clock off per job', Model);
                            $scope.JobTaskForClockOff = JobTask;
                            console.log('jobtask buat clock off', $scope.JobTaskForClockOff);
                            var JobTeknisinih = [];
                            for (var i = 0; i < $scope.xDataWoList.length; i++) {
                
                                //console.log("data wo", $scope.xDataWoList[i]);
                
                                for (var a = 0; a < $scope.xDataWoList[i].JobTask.length; a++) {
                                    //console.log("data JObTask", $scope.xDataWoList[i].JobTask[a]);
                
                                    for (var z = 0; z < $scope.xDataWoList[i].JobTask[a].JobTaskTechnician.length; z++) {
                                        JobTeknisinih = $scope.xDataWoList[i].JobTask[a].JobTaskTechnician[z];
                                        //console.log("job Teksnisi", JobTeknisinih);
                                    }
                
                                }
                
                            }
                            //console.log("jobTeknisi", JobTeknisinih);
                            if (JobTeknisinih.length == 0) {
                                // //console.log("gagal");
                                // bsNotify.show(
                                //                 {
                                //                     size: 'big',
                                //                     type: 'danger',
                                //                     title: "Mohon Lakukan Job Dispatch Terlebih Dahulu"
                                //                     // ,
                                //                     // content: error.join('<br>'),
                                //                     // number: error.length
                                //                 }
                                //             );
                                bsNotify.show({
                                    title: 'Mohon Lakukan Job Dispatch Terlebih Dahulu',
                                    text: 'I will close in 2 seconds.',
                                    size: 'big',
                                    timeout: 2000,
                                    type: 'danger'
                                });
                            } else {
                                // //console.log("nih jir", TaskId);
                                //console.log("model", Model);
                                // //console.log("Status", Status, "Job task Id", JobTskId,"Job Id", JobId, "Task Id", TaskId, "Employee Id", EmployeeId, "Employee", Employee , "TaskName" , TaskName, "FR", FR, "Fare", Fare , "PlanStart", PlanStart , "PlanFinish" , PlanFinish , "LastModifiedUserId" , LastModifiedUserId , "LastModifiedDate" , LastModifiedDate , "ProcessId" , ProcessId , "Satuan" , Satuan , "isTypeR" , isTypeR , "isTypeX" , isTypeX , "PaidById" , PaidById , "Revision" , Revision);
                                // '+Status+','+JobTskId+','+JobId+','+TaskId+','+EmployeeId+','+Employee+','+TaskName+','+FR+','+Fare+','+PlanStart+','+PlanFinish+','+LastModifiedUserId+','+LastModifiedDate+','+ProcessId+','+Satuan+','+isTypeR+','+isTypeX+','+PaidById+','+Revision+'
                                // var something = ngDialog.openConfirm ({
                                // template:'\
                                //              <p>Anda yakin untuk clock off</p>\
                                //              <div class="ngdialog-buttons">\
                                //                <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog()">Tidak</button>\
                                //                <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm()">Ya</button>\
                                //              </div>',
                                //           plain: true,
                                //           controller: 'RepairProcessGRController',
                                // }).then(function (value) {
                                // perform delete operation
                                ////console.log("then",value);
                                $scope.doClockOff(Model, $scope.JobTaskForClockOff);
                                // }, function (value) {
                                //Do something
                                ////console.log("else",value);
                                // });
                
                                // //console.log();
                            }
                        }
                    })
                }

            });
            
            
        }

        $scope.Jst = [];
        $scope.showFi2 = false;
        $scope.getFIById = function(JobId, dataJobTask) {
            console.log('FI JobId', JobId)
            console.log('FI dataJobTask', dataJobTask)
            for(var x in dataJobTask){
                if (dataJobTask[x].isDeleted == 0) {
                    $scope.xDataWoListT2 = dataJobTask[x].JobType.Name;
                }
            }
            console.log('name T2', $scope.xDataWoListT2)
            $scope.showFi2 = false;
            console.log('parampampa', $scope.xDataWoList)
            RepairProcessGR.CheckIsJobCancelWO(JobId).then(function (resultC) {
                if (resultC.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                    });
                } else {
                    RepairProcessGR.getStatWO(JobId, $scope.xDataWoList[0].Status).then(function(resux2) { 
                        if (resux2.data == 1){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Kendaraan sudah Final Inspection"
                            });
                        } else {
                            var JobForeman;
                            console.log("mData", $scope.mData);
                            // $scope.mData.Status = 1; // dikomen dl krn ga jls knp jadi 1 status wo nya
                            if ($scope.mData.StatusInspeksi == null){
                                $scope.mData.StatusInspeksi = 1; // set default status inspeksi..
                            }
                            $scope.StatusFixed = true;
                            $scope.mData.IsFieldAction = 0;
                            $scope.mData.isFixItRight = 0;
                            for (var i = 0; i < $scope.xDataWoList.length; i++) {
                
                                //console.log("data wo", $scope.xDataWoList[i]);
                                JobForeman = $scope.xDataWoList[i].MProfileForeman;
                                //console.log("JobForeman", JobForeman);
                
                            }
                
                            if (JobForeman == null) {
                
                                // bsNotify.show(
                                //                 {
                                //                     size: 'big',
                                //                     type: 'danger',
                                //                     title: "Mohon Isi Nama Foreman"
                                //                     // ,
                                //                     // content: error.join('<br>'),
                                //                     // number: error.length
                                //                 }
                                //             );
                                bsNotify.show({
                                    title: 'Mohon Isi Nama Foreman',
                                    text: 'I will close in 2 seconds.',
                                    size: 'big',
                                    timeout: 2000,
                                    type: 'danger'
                                });
                                $scope.showFi2 = false;
                            } else {
                                $scope.showFi2 = true;
                                //console.log("data job task", dataJobTask);
                                //console.log("job id", JobId);
                                $scope.Jst = dataJobTask;
                                $scope.JbIdFi = JobId;
                
                                RepairProcessGR.getJobSuggest(JobId).then(
                                    function(res) {
                                        $scope.chooseJobSuggest = res.data.Result;
                                        //console.log("$scope.chooseJobSuggest", $scope.chooseJobSuggest);
                                    });
                
                
                                //Get Prdiagnose di final incpection
                                // $scope.getDataPrediagnose();
                                //get Field Action
                                if ($scope.mData.VehicleId !== undefined) {
                                    AppointmentGrService.getDataVehiclePSFU($scope.mData.VehicleId).then(function(resu) {
                                        console.log("cobain dulu", resu.data.Result);
                                        WO.getTowass(resu.data.Result[0].VIN).then(function(res) {
                                            //console.log("Data TOwass", $scope.mDataCrm.Vehicle.VIN);
                                            $scope.GetDataTowass = res.data.Result;
                                            console.log("Data TOwass", $scope.GetDataTowass);
                                        }, function(err) {
                                            console.log("err=>", err);
                                        })
                                    });
                                }
                
                                RepairProcessGR.getFinalInspectionById(JobId)
                                    .then(
                
                                        function(res) {
                                            //console.log("res ini", res.data);
                
                                            if (res.data.ResponseCode == 44) {
                                                RepairProcessGR.postFinalInspection(JobId).then(
                                                    function(res) {
                                                        $scope.xDataWoList[0].Status = 11; //di set 11 karena service postFinalInspection update status jd 11
                                                        var dataJBTsk = [];
                                                        angular.forEach(dataJobTask, function(row) {
                                                            // dataJBTsk.push(row);
                                                            if (row.isDeleted == 0) {
                                                                if ($scope.xDataWoList[0].Revision >= row.Revision) {
                                                                    dataJBTsk.push(row);
                                                                    //console.log("Model Yang Di Push nih", dataJBTsk);
                                                                }
                                                            }
                                                        });
                
                                                        RepairProcessGR.getFinalInspectionById(JobId).then(
                                                            function(res) {
                                                                $scope.DataFI = dataJBTsk;
                                                                $scope.dataSaveFi = res.data;
                                                                console.log("lc", $scope.DataFI);
                
                                                            },
                                                            function(err) {
                                                                //console.log("err=>", err);
                                                            }
                                                        );
                                                        //console.log("succes insert");
                                                    },
                                                    function(err) {
                                                        //console.log("err=>", err);
                                                    }
                                                );
                                                //console.log("masuk Post");
                                            } else {
                                                RepairProcessGR.postFinalInspection(JobId).then(function(res) {
                                                    console.log('update status jadi 11 doang')
                                                    $scope.xDataWoList[0].Status = 11; //di set 11 karena service postFinalInspection update status jd 11
                                                });
                                                //console.log("masuk get data Fi");
                                                if ($scope.xDataWoList[0].Revision != 0) {
                                                    var dataJBTsk = [];
                                                    angular.forEach(dataJobTask, function(row) {
                                                        if (row.isDeleted == 0) {
                                                            if ($scope.xDataWoList[0].Revision >= row.Revision) {
                                                                dataJBTsk.push(row);
                                                                //console.log("Model Yang Di Push nih", dataJBTsk);
                                                            }
                                                        }
                                                    });
                
                                                    RepairProcessGR.getFinalInspectionById(JobId).then(
                                                        function(res) {
                                                            $scope.DataFI = dataJBTsk;
                                                            $scope.dataSaveFi = res.data;
                                                            console.log("lc", $scope.DataFI);
                
                                                        },
                                                        function(err) {
                                                            //console.log("err=>", err);
                                                        }
                                                    );
                                                } else {
                                                    var dataJBTsk = [];
                                                    angular.forEach(dataJobTask, function(row) {
                                                        if (row.isDeleted == 0) {
                                                            dataJBTsk.push(row);
                                                        }
                                                    });
                                                    RepairProcessGR.getFinalInspectionById(JobId).then(
                                                        function(res) {
                                                            $scope.DataFI = dataJBTsk;
                                                            $scope.dataSaveFi = res.data;
                                                            console.log("lc", $scope.DataFI);
                                                        },
                                                        function(err) {
                                                            //console.log("err=>", err);
                                                        }
                                                    );
                                                }
                                            }
                                        },
                                        function(err) {
                                            //console.log("err=>", err);
                                        }
                                    );
                
                                // bsNotify.show({
                                //     title: "Berhasil",
                                //     content: "Data berhasil disimpan",
                                //     type: "success"
                                // });
                            }
                        }
                    })
                }

            });
            
            
        }


        $scope.selectT1 = function(Data) {
            //console.log("data T1", Data);
        }

        var DataT2Save = [];
        var DtT2;
        $scope.selectT2 = function(Data) {
            //console.log("data T2", Data);
            DtT2 = Data;
            //console.log("DtT2", DtT2);
            if (Data !== undefined) {

                // var a = DataT2Save.indexOf(Data.T2Code);
                // if (a > -1){
                //     DataT2Save.splice(a, 1);
                //     //console.log("nih di if 1", DataT2Save);
                // }else {
                //     DataT2Save.push(Data);
                //     //console.log("nih else", DataT2Save);
                // }

                DataT2Save.push(Data);
                // //console.log("nih else", DataT2Save);
            }
            // else{
            //   DataT2Save = [];
            // }


        }


        $scope.SimpanT2 = function(Data) {

            //console.log("mData", Data);

            //console.log("$scope.DataWarranty", $scope.DataWarranty);
            DataT2Save.push(DtT2);
            //console.log("DataT2Save", DataT2Save);
            // if(Data !== undefined){
            //   DataT2Save.push(Data);
            // }else{
            //   DataT2Save = [];
            // }

            // var FinalSimpan = [];

            // for(var c in Data.Note){

            //         FinalSimpan.push({
            //           JobTaskId : $scope.DataWarranty[c].JobTaskId,
            //           T2 : DataT2Save[c].T2Code,
            //           notes : Data.Note[c]
            //         });

            // }
            // var ol = Object.keys(Data.Note);
            // //console.log("ol", ol.length);
            // //console.log("$scope.DataWarranty.length",$scope.DataWarranty.length);
            // if($scope.DataWarranty.length == ol.length){

            //     RepairProcessGR.putT2(FinalSimpan).then(
            //         function(res) {
            //           //console.log("Berhasil");
            //         },
            //         function(err) {
            //             //console.log("err=>", err);
            //         }
            //     );

            // }else{
            //    bsNotify.show(
            //     {
            //       title: 'Mohon isi T2 beserta keterangannya',
            //       text: 'I will close in 2 seconds.',
            //       timer: 2000,
            //       type: 'danger'
            //     });
            // }

        }

        $scope.PutFI = function(Data, Model) {
            console.log("data PutFI", Data);
            console.log("Model PutFI", Model);
            console.log("Model PutFI", Model.JobTask[0].JobType.MasterId);
            console.log("jobtaskid", JobTaskIdFI);
            console.log("$scope.chooseJobSuggest", $scope.chooseJobSuggest);
            console.log("$scope.JbId", $scope.JbId);
            for (var i = 0; i < Model.JobTask.length; i++) {
                if (Model.JobTask[i].isDeleted == 0) {
                    $scope.typeId = Model.JobTask[i].JobType.MasterId;
                }
            }
            console.log('$scope.typeId', $scope.typeId)
                
            $scope.typePaidBy = Model.JobTask[0].PaidById;
            RepairProcessGR.CheckIsJobCancelWO($scope.JbId).then(function (resultC) {
                if (resultC.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                    });
                } else {
                    RepairProcessGR.CheckPrediagnoseGate($scope.JbId, Data.Job.PoliceNumber).then(function (result3) {
                        if (result3.data == 666){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Kendaraan belum Gate In dari Prediagnose",
                            });
                            return;
                        } else if (result3.data == 999){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Kendaraan sedang menunggu Gate Out proses Prediagnose",
                            });
                            return;
                        } else {
                            RepairProcessGR.checkGateIn(Data.Job.PoliceNumber, $scope.JbId).then(
                                function(resdu) {
                
                                    if (resdu.data == -1){
                                        bsNotify.show({
                                            size: 'big',
                                            title: 'Tidak Bisa FI, Kendaraan Belum Gate In Ke Production',
                                            text: '',
                                            type: 'danger'
                                        });
                                    } else {
                                        RepairProcessGR.getStatWO($scope.JbId, 11).then(function(resux2) { // di kasi 11 karena pasti uda click tab FI jadi stat na 11
                                            if (resux2.data == 1){
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "Kendaraan sudah Final Inspection"
                                                });
                                            } else {
                                                $scope.getActualTimeForClock();
                                                if ($scope.DataWarranty.length == 0) {
                                                    console.log('$scope.DataWarranty', $scope.DataWarranty)
                                                    if ($scope.chooseJobSuggest !== 0) { 
                                                        if ($scope.mData.Remarks == null) {
                                                            console.log('inspeksi 1')
                                                            bsNotify.show({
                                                                title: 'Mohon isi hasil inspeksi',
                                                                text: 'I will close in 2 seconds.',
                                                                size: 'big',
                                                                timeout: 2000,
                                                                type: 'danger'
                                                                    // size: 'big'
                                                                    // size: 'big',
                                                                    // type: 'danger',
                                                                    // title: "Mohon isi hasil inspeksi dan tanggal job selanjutnya",
                                                                    // timer : 3000
                                                            });
                                                        } else {
                                                            RepairProcessGR.putFinalInspection(Data, Model, JobTaskIdFI, $scope.actualFullTime).then(
                                    
                                                                function(res) {
                                                                    // $scope.getFIById(Data.JobId);
                                    
                                                                    if ($scope.xDataWoList[0].Revision != 0) {
                                                                        var dataJBTsk = [];
                                                                        angular.forEach($scope.Jst, function(row) {
                                                                            if (row.isDeleted == 0) {
                                                                                if ($scope.xDataWoList[0].Revision >= row.Revision) {
                                                                                    dataJBTsk.push(row);
                                                                                    console.log("Model Yang Di Push nih", dataJBTsk);
                                                                                } else {
                                                                                    //console.log("else model yang dipush");
                                                                                }
                                                                            }
                                                                        });
                                    
                                                                        RepairProcessGR.getFinalInspectionById(Data.JobId).then(
                                                                            function(res) {
                                                                                $scope.DataFI = dataJBTsk;
                                                                                $scope.dataSaveFi = res.data;
                                                                                console.log("lc", $scope.DataFI);
                                    
                                                                            },
                                                                            function(err) {
                                                                                //console.log("err=>", err);
                                                                            },
                                                                            bsNotify.show({
                                                                                title: "Berhasil",
                                                                                content: "Data berhasil disimpan",
                                                                                type: "success"
                                                                            })
                                                                        );
                                    
                                                                    } else {
                                                                        var dataJBTsk = [];
                                                                        angular.forEach($scope.Jst, function(row) {
                                                                            if (row.isDeleted == 0) {
                                                                                dataJBTsk.push(row);
                                                                            }
                                                                        });
                                                                        RepairProcessGR.getFinalInspectionById(Data.JobId).then(
                                                                            function(res) {
                                                                                $scope.DataFI = dataJBTsk;
                                                                                $scope.dataSaveFi = res.data;
                                                                                console.log("lc", $scope.DataFI);
                                                                            },
                                                                            function(err) {
                                                                                //console.log("err=>", err);
                                                                            },
                                                                            bsNotify.show({
                                                                                title: "Berhasil",
                                                                                content: "Data berhasil disimpan",
                                                                                type: "success"
                                                                            })
                                                                        );
                                                                    }
                                    
                                                                    RepairProcessGR.postJobSuggest($scope.JbId, Model).then(
                                                                        function(res) {
                                                                            RepairProcessGR.putJobSuggestCRM($scope.VinData[0].VIN, Model).then(
                                                                                function(res) {
                                    
                                                                                    // RepairProcessGR.putJobSuggestCT($scope.VinData[0].VIN,$scope.mData).then(
                                                                                    //       function(res){
                                                                                    //         //console.log("suksess put Job Suggest");
                                                                                    //       },
                                                                                    //       function(err) {
                                                                                    //               //console.log("err=>", err);
                                                                                    //       }
                                                                                    // );
                                    
                                                                                },
                                                                                function(err) {
                                                                                    //console.log("err=>", err);
                                                                                }
                                                                            );
                                    
                                                                        },
                                                                        function(err) {
                                                                            //console.log("err=>", err);
                                                                        },
                                                                        bsNotify.show({
                                                                            title: "Berhasil",
                                                                            content: "Data berhasil disimpan",
                                                                            type: "success"
                                                                        })
                                                                    );
                                    
                                                                    // $scope.SimpanJobSuggest(Model);
                                                                    $scope.getDataDetail(Data.JobId);
                                                                    $scope.getDataDefault();
                                    
                                    
                                                                    if (JobTaskIdFI.length > 0) {
                                                                        //kirim notif juga di FI ada re do
                                                                        var DataNotif = {};
                                                                        var messagetemp = "";
                                                                        messagetemp = Data.PoliceNumber;
                                    
                                    
                                                                        DataNotif.Message = messagetemp;
                                                                        RepairProcessGR.sendNotifRole(DataNotif, 1031, 31).then(
                                                                            function(res) {
                                    
                                                                            },
                                                                            function(err) {
                                                                                //console.log("err=>", err);
                                                                            });
                                                                    }
                                    
                                                                    $scope.StatusFixed = false;
                                                                    $scope.ShowRepair = false;
                                                                    $scope.StatusNotFixed = false;
                                                                    $scope.ShowBtnKembali = false;
                                                                    JobTaskIdFI = [];
                                                                    $scope.mData = {};
                                                                },
                                                                function(err) {
                                                                    //console.log("err=>", err);
                                                                },
                                                                bsNotify.show({
                                                                    title: "Berhasil",
                                                                    content: "Data berhasil disimpan",
                                                                    type: "success"
                                                                })
                                                            );
                                                        }
                                    
                                                    } else {
                                                        if ($scope.mData.Remarks == null) {
                                                            console.log('inspeksi 2')
                                                            bsNotify.show({
                                                                title: 'Mohon isi hasil inspeksi',
                                                                text: 'I will close in 2 seconds.',
                                                                size: 'big',
                                                                timeout: 2000,
                                                                type: 'danger'
                                                                    // size: 'big',
                                                                    // type: 'danger',
                                                                    // title: "Mohon isi hasil inspeksi"
                                                            });
                                                        } else {
                                                            RepairProcessGR.putFinalInspection(Data, Model, JobTaskIdFI, $scope.actualFullTime).then(
                                                                function(res) {
                                                                    // $scope.getFIById(Data.JobId);
                                                                    if ($scope.xDataWoList[0].Revision != 0) {
                                                                        var dataJBTsk = [];
                                                                        angular.forEach($scope.Jst, function(row) {
                                                                            if (row.isDeleted == 0) {
                                                                                if ($scope.xDataWoList[0].Revision >= row.Revision) {
                                                                                    dataJBTsk.push(row);
                                                                                    //console.log("Model Yang Di Push nih", dataJBTsk);
                                                                                } else {
                                                                                    //console.log("else model yang dipush");
                                                                                }
                                                                            }
                                                                        });
                                                                        RepairProcessGR.getFinalInspectionById(Data.JobId).then(
                                                                            function(res) {
                                                                                $scope.DataFI = dataJBTsk;
                                                                                $scope.dataSaveFi = res.data;
                                                                                console.log("lc", $scope.DataFI);
                                    
                                                                            },
                                                                            function(err) {
                                                                                //console.log("err=>", err);
                                                                            },
                                                                            bsNotify.show({
                                                                                title: "Berhasil",
                                                                                content: "Data berhasil disimpan",
                                                                                type: "success"
                                                                            })
                                                                        );
                                    
                                                                    } else {
                                                                        var dataJBTsk = [];
                                                                        angular.forEach($scope.Jst, function(row) {
                                                                            if (row.isDeleted == 0) {
                                                                                dataJBTsk.push(row);
                                                                            }
                                                                        });
                                                                        RepairProcessGR.getFinalInspectionById(Data.JobId).then(
                                                                            function(res) {
                                                                                $scope.DataFI = dataJBTsk;
                                                                                $scope.dataSaveFi = res.data;
                                                                                console.log("lc", $scope.DataFI);
                                                                            },
                                                                            function(err) {
                                                                                //console.log("err=>", err);
                                                                            },
                                                                            bsNotify.show({
                                                                                title: "Berhasil",
                                                                                content: "Data berhasil disimpan",
                                                                                type: "success"
                                                                            })
                                                                        );
                                                                    }
                                                                    $scope.getDataDetail(Data.JobId);
                                                                    $scope.getDataDefault();
                                                                    $scope.StatusFixed = false;
                                                                    $scope.ShowRepair = false;
                                                                    $scope.StatusNotFixed = false;
                                                                    $scope.ShowBtnKembali = false;
                                                                    JobTaskIdFI = [];
                                                                    $scope.mData = {};
                                                                },
                                                                function(err) {
                                                                    //console.log("err=>", err);
                                                                },
                                                                bsNotify.show({
                                                                    title: "Berhasil",
                                                                    content: "Data berhasil disimpan",
                                                                    type: "success"
                                                                })
                                                            );
                                                        }
                                                    }

                                                } else {
                                                    // ====
                                                    var ol = [];
                                                    var ol2 = [];
                                                    var ol3 = [];
                                                    var countMistake = 0;
                                                    var countMistakeOFP = 0;
                                                    //console.log("ol", ol.length);
                                                    //console.log("$scope.DataWarranty.length", $scope.DataWarranty.length);
                                                    console.log("MODELT2 1", Model.T2)
                                                    if($scope.typeId != 2675 && $scope.typeId != 2677 && $scope.typeId != 2678){
                                                        countMistake = 0;
                                                        countMistakeOFP = 0;
                                                    }else if((($scope.typeId != 2675 && $scope.typeId != 2677 && $scope.typeId != 2678) && (Model.T2 == undefined && Model.OFP == undefined && Model.Note == undefined)) || ($scope.typeId == 59 || $scope.typeId == 60) && (Model.T2 == undefined || Model.OFP == undefined || Model.Note == undefined)){
                                                        bsNotify.show({
                                                            title: 'Mohon isi OFP(minimal 10 karakter), T2 beserta keterangannya',
                                                            text: 'I will close in 2 seconds.',
                                                            size: 'big',
                                                            timeout: 2000,
                                                            type: 'danger'
                                                        }); 
                                                    }else{
                                                        if ($scope.typeId == 2675 || $scope.typeId == 2677 || $scope.typeId == 2678) {
                                                            ol = Model.Note;
                                                            ol2 = Model.OFP;
                                                            ol3 = Model.T2;
                                                        }else{
                                                            ol = Object.keys(Model.Note);
                                                            ol2 = Object.keys(Model.OFP);
                                                            ol3 = Object.keys(Model.T2);

                                                            for(var xNote in ol){
                                                                console.log("Model[ol[xNote]] ==>",Model.Note[ol[xNote]],xNote,ol);
                                                                if(Model.Note[ol[xNote]] == '' || Model.Note[ol[xNote]] == null){
                                                                    countMistake += 1;
                                                                }
                                                            }
                                                            for(var xOfp in ol2){
                                                                console.log("Model[ol2[xOfp]] ==>",Model.OFP[ol2[xOfp]],xOfp,ol2);
                                                                if(Model.OFP[ol2[xOfp]] == '' || Model.OFP[ol2[xOfp]] == null || Model.OFP[ol2[xOfp]] == undefined){
                                                                    countMistake += 1;
                                                                } else {
                                                                    var tes = Model.OFP[ol2[xOfp]]
                                                                    if (Model.OFP[ol2[xOfp]].includes('-') || Model.OFP[ol2[xOfp]].includes(' ')){
                                                                        var tmp = Model.OFP[ol2[xOfp]].replace(/ |-/g, "");
                                                                        Model.OFP[ol2[xOfp]] = tmp;
                                                                    }
                                                                    if(Model.OFP[ol2[xOfp]].length < 10 ){
                                                                        countMistakeOFP += 1;
                                                                    }
                                                                }
                                                                
                                                            }
                                                            for(var xt2 in ol3){
                                                                console.log("Model[ol3[xt2]] ==>",Model.T2[ol3[xt2]],xt2,ol3);
                                                                if(Model.T2[ol3[xt2]] == '' || Model.T2[ol3[xt2]] == null){
                                                                    countMistake += 1;
                                                                }
                                                            }
                                                        }
                                                        
                                                    }
                                                    console.log("MODELT2 2", Model.T2)
                                                    console.log("ini cek harusnya 0 tapi",countMistake);
                                                    if(countMistake > 0){
                                                        bsNotify.show({
                                                            title: 'Mohon isi OFP, T2 beserta keterangannya',
                                                            text: 'I will close in 2 seconds.',
                                                            size: 'big',
                                                            timeout: 2000,
                                                            type: 'danger'
                                                        }); 
                                                        return false;
                                                    }
                                                    if(countMistakeOFP > 0){
                                                        bsNotify.show({
                                                            title: 'Kode OFP minimal 10 karakter (Hanya angka dan huruf)',
                                                            text: 'I will close in 2 seconds.',
                                                            size: 'big',
                                                            // timeout: 2000,
                                                            type: 'danger'
                                                        }); 
                                                        return false;
                                                    }
                                                    var FinalSimpan = [];
                                    
                                                    var dataTerpilihT2 = [];
                                                    if (($scope.T2 != null && $scope.T2 != undefined) && (ol3 != null && ol3 != undefined)) {
                                                        for (var i=0; i<$scope.T2.length; i++){
                                                            for (var j=0; j<ol3.length; j++){
                                                                if ($scope.T2[i].xName == Model.T2[j]){
                                                                    dataTerpilihT2.push($scope.T2[i]);
                                                                }
                                                            }
                                                        }
                                                    }else {
                                                        dataTerpilihT2.push($scope.T2)
                                                    }
                                                    for (var c in Model.Note) {
                                                        FinalSimpan.push({
                                                            JobTaskId: $scope.DataWarranty[c].JobTaskId,
                                                            // T2: DataT2Save[c].T2Code,
                                                            // T2: dataTerpilihT2[c].T2Code,
                                                            T2: Model.T2[c],
                                                            notes: Model.Note[c],
                                                            OFP : Model.OFP[c]
                                                        });
                                    
                                                    }
                                                    // return false;
                                                    console.log('$scope.DataWarranty cuco', $scope.DataWarranty)
                                                    console.log('$scope.DataWarranty length', $scope.DataWarranty.length)
                                                    console.log('ol cuco', ol)
                                                    console.log('ol2 cuco', ol2)
                                                    console.log('ol3 cuco', ol3)
                                                    if ((($scope.typeId == 2675 || $scope.typeId == 2677 || $scope.typeId == 2678) && ol == undefined && ol2 == undefined && ol3 == null) || ($scope.DataWarranty.length == ol.length && $scope.DataWarranty.length == ol2.length) || (Model.T2 != null && Model.T2 != "")) {
                                                        console.log('una inu una ini')
                                                        for (var i=0; i<FinalSimpan.length; i++){
                                                            FinalSimpan[i].notes = FinalSimpan[i].notes.toString().toUpperCase();
                                                            FinalSimpan[i].OFP = FinalSimpan[i].OFP.toString().toUpperCase().replace(/[^a-zA-Z0-9 ]/g, '');
                                                        }

                                                        var obj = {}
                                                        var data_obj = []

                                                        for (var i=0; i<FinalSimpan.length; i++){
                                                            obj = {}

                                                            obj.OFP = angular.copy(FinalSimpan[i].OFP)
                                                            data_obj.push(obj)

                                                        }

                                                        RepairProcessGR.CekOFP(data_obj).then(function(res12) {
                                                            var hasel = '';
                                                            var yangmana = '';

                                                            if (res12.data.includes('#')) {
                                                                hasel = res12.data.split('#')[0]
                                                                yangmana = res12.data.split('#')[1]
                                                            } else {
                                                                hasel = res12.data
                                                            }

                                                            if (hasel == '666') {
                                                                bsNotify.show({
                                                                    title: 'Kode Part yang di input pada kolom OFP tidak sesuai dengan master Part TOYOTA, silahkan input kode part yang benar & tepat.',
                                                                    text: '',
                                                                    size: 'big',
                                                                    type: 'danger'
                                                                });

                                                            } else if (hasel == '1') {
                                                                RepairProcessGR.putT2(FinalSimpan).then(
                                                                    function(res) {
                                                                        //console.log("Berhasil");
                                                                        bsNotify.show({
                                                                            title: "Berhasil",
                                                                            content: "Data berhasil disimpan",
                                                                            type: "success"
                                                                        })
                                                                        if ($scope.chooseJobSuggest !== 0) {
                                                                            console.log('inspeksi 3')
                                                                            if ($scope.mData.Remarks == null) {
                                                                                console.log('inspeksi 3')
                                                                                bsNotify.show({
                                                                                    title: 'Mohon isi hasil inspeksi',
                                                                                    text: 'I will close in 2 seconds.',
                                                                                    size: 'big',
                                                                                    timeout: 2000,
                                                                                    type: 'danger'
                                                                                });
                                                                            } else {
                                                                                RepairProcessGR.putFinalInspection(Data, Model, JobTaskIdFI, $scope.actualFullTime).then(
                                                                                    function(res) {
                                                                                        // $scope.getFIById(Data.JobId);
                                            
                                                                                        if ($scope.xDataWoList[0].Revision != 0) {
                                                                                            var dataJBTsk = [];
                                                                                            angular.forEach($scope.Jst, function(row) {
                                                                                                if (row.isDeleted == 0) {
                                                                                                    if ($scope.xDataWoList[0].Revision >= row.Revision) {
                                                                                                        dataJBTsk.push(row);
                                                                                                        //console.log("Model Yang Di Push nih", dataJBTsk);
                                                                                                    } else {
                                                                                                        //console.log("else model yang dipush");
                                                                                                    }
                                                                                                }
                                                                                            });
                                            
                                                                                            RepairProcessGR.getFinalInspectionById(Data.JobId).then(
                                                                                                function(res) {
                                                                                                    $scope.DataFI = dataJBTsk;
                                                                                                    $scope.dataSaveFi = res.data;
                                                                                                    console.log("lc", $scope.DataFI);
                                            
                                                                                                },
                                                                                                function(err) {
                                                                                                    //console.log("err=>", err);
                                                                                                },
                                                                                                bsNotify.show({
                                                                                                    title: "Berhasil",
                                                                                                    content: "Data berhasil disimpan",
                                                                                                    type: "success"
                                                                                                })
                                                                                            );
                                            
                                                                                        } else {
                                                                                            var dataJBTsk = [];
                                                                                            angular.forEach($scope.Jst, function(row) {
                                                                                                if (row.isDeleted == 0) {
                                                                                                    dataJBTsk.push(row);
                                                                                                }
                                                                                            });
                                                                                            RepairProcessGR.getFinalInspectionById(Data.JobId).then(
                                                                                                function(res) {
                                                                                                    $scope.DataFI = dataJBTsk;
                                                                                                    $scope.dataSaveFi = res.data;
                                                                                                    console.log("lc", $scope.DataFI);
                                                                                                },
                                                                                                function(err) {
                                                                                                    //console.log("err=>", err);
                                                                                                },
                                                                                                bsNotify.show({
                                                                                                    title: "Berhasil",
                                                                                                    content: "Data berhasil disimpan",
                                                                                                    type: "success"
                                                                                                })
                                                                                            );
                                                                                        }
                                            
                                                                                        RepairProcessGR.postJobSuggest($scope.JbId, $scope.mData).then(
                                                                                            function(res) {
                                                                                                RepairProcessGR.putJobSuggestCRM($scope.VinData[0].VIN, $scope.mData).then(
                                                                                                    function(res) {
                                            
                                                                                                        // RepairProcessGR.putJobSuggestCT($scope.VinData[0].VIN,$scope.mData).then(
                                                                                                        //       function(res){
                                                                                                        //         //console.log("suksess put Job Suggest");
                                                                                                        //       },
                                                                                                        //       function(err) {
                                                                                                        //               //console.log("err=>", err);
                                                                                                        //       }
                                                                                                        // );
                                            
                                                                                                    },
                                                                                                    function(err) {
                                                                                                        //console.log("err=>", err);
                                                                                                    },
                                                                                                    bsNotify.show({
                                                                                                        title: "Berhasil",
                                                                                                        content: "Data berhasil disimpan",
                                                                                                        type: "success"
                                                                                                    })
                                                                                                );
                                            
                                                                                            },
                                                                                            function(err) {
                                                                                                //console.log("err=>", err);
                                                                                            },
                                            
                                                                                            bsNotify.show({
                                                                                                title: "Berhasil",
                                                                                                content: "Data berhasil disimpan",
                                                                                                type: "success"
                                                                                            })
                                                                                        );
                                            
                                                                                        // $scope.SimpanJobSuggest(Model);
                                            
                                                                                        $scope.getDataDetail(Data.JobId);
                                                                                        $scope.getDataDefault();
                                                                                        $scope.StatusFixed = false;
                                                                                        $scope.ShowRepair = false;
                                                                                        $scope.StatusNotFixed = false;
                                                                                        $scope.ShowBtnKembali = false;
                                                                                        JobTaskIdFI = [];
                                                                                        $scope.mData = {};
                                                                                    },
                                                                                    function(err) {
                                                                                        //console.log("err=>", err);
                                                                                    },
                                                                                    bsNotify.show({
                                                                                        title: "Berhasil",
                                                                                        content: "Data berhasil disimpan",
                                                                                        type: "success"
                                                                                    })
                                                                                );
                                                                            }
                                            
                                                                        } else {
                                                                            if ($scope.mData.Remarks == null) {
                                                                                console.log('inspeksi 4')
                                                                                bsNotify.show({
                                            
                                                                                    title: 'Mohon isi hasil inspeksi',
                                                                                    text: 'I will close in 2 seconds.',
                                            
                                                                                    size: 'big',
                                                                                    timeout: 2000,
                                                                                    type: 'danger'
                                                                                });
                                                                            } else {
                                                                                RepairProcessGR.putFinalInspection(Data, Model, JobTaskIdFI, $scope.actualFullTime).then(
                                                                                    function(res) {
                                                                                        // $scope.getFIById(Data.JobId);
                                            
                                                                                        if ($scope.xDataWoList[0].Revision != 0) {
                                                                                            var dataJBTsk = [];
                                                                                            angular.forEach($scope.Jst, function(row) {
                                                                                                if (row.isDeleted == 0) {
                                                                                                    if ($scope.xDataWoList[0].Revision >= row.Revision) {
                                                                                                        dataJBTsk.push(row);
                                                                                                        //console.log("Model Yang Di Push nih", dataJBTsk);
                                                                                                    } else {
                                                                                                        //console.log("else model yang dipush");
                                                                                                    }
                                                                                                }
                                                                                            });
                                            
                                                                                            RepairProcessGR.getFinalInspectionById(Data.JobId).then(
                                                                                                function(res) {
                                                                                                    $scope.DataFI = dataJBTsk;
                                                                                                    $scope.dataSaveFi = res.data;
                                                                                                    console.log("lc", $scope.DataFI);
                                            
                                                                                                },
                                                                                                function(err) {
                                                                                                    //console.log("err=>", err);
                                                                                                },
                                                                                                bsNotify.show({
                                                                                                    title: "Berhasil",
                                                                                                    content: "Data berhasil disimpan",
                                                                                                    type: "success"
                                                                                                })
                                                                                            );
                                            
                                                                                        } else {
                                                                                            var dataJBTsk = [];
                                                                                            angular.forEach($scope.Jst, function(row) {
                                                                                                if (row.isDeleted == 0) {
                                                                                                    dataJBTsk.push(row);
                                                                                                }
                                                                                            });
                                                                                            RepairProcessGR.getFinalInspectionById(Data.JobId).then(
                                                                                                function(res) {
                                                                                                    $scope.DataFI = dataJBTsk;
                                                                                                    $scope.dataSaveFi = res.data;
                                                                                                    console.log("lc", $scope.DataFI);
                                            
                                                                                                },
                                                                                                function(err) {
                                                                                                    //console.log("err=>", err);
                                                                                                },
                                                                                                bsNotify.show({
                                                                                                    title: "Berhasil",
                                                                                                    content: "Data berhasil disimpan",
                                                                                                    type: "success"
                                                                                                })
                                                                                            );
                                                                                        }
                                                                                        $scope.getDataDetail(Data.JobId);
                                                                                        $scope.getDataDefault();
                                                                                        $scope.StatusFixed = false;
                                                                                        $scope.ShowRepair = false;
                                                                                        $scope.StatusNotFixed = false;
                                                                                        $scope.ShowBtnKembali = false;
                                                                                        JobTaskIdFI = [];
                                                                                        $scope.mData = {};
                                                                                    },
                                                                                    function(err) {
                                                                                        //console.log("err=>", err);
                                                                                    },
                                                                                    bsNotify.show({
                                                                                        title: "Berhasil",
                                                                                        content: "Data berhasil disimpan",
                                                                                        type: "success"
                                                                                    })
                                                                                );
                                                                            }
                                                                        }
                                            
                                                                    },
                                                                    function(err) {
                                                                        //console.log("err=>", err);
                                                                    },
                                                                    bsNotify.show({
                                                                        title: "Berhasil",
                                                                        content: "Data berhasil disimpan",
                                                                        type: "success"
                                                                    })
                                                                );

                                                            }

                                                        })
                                                    } else {
                                                        bsNotify.show({
                                                            title: 'Mohon isi T2 beserta keterangannya',
                                                            text: 'I will close in 2 seconds.',
                                                            size: 'big',
                                                            timeout: 2000,
                                                            type: 'danger'
                                                        });
                                                    }
                                    
                                                }
                                            }
                                        })
                                        
                                    }
                
                                }
                            );
                            
                        }

                    })

                    

                }

            });


            

        }

        var JobTaskIdFI = [];

        $scope.ChoseDataFI = function(JobTaskId, $index) {
            //console.log("JobTaskId", JobTaskId, "idx", $index);
            var a = JobTaskIdFI.indexOf(JobTaskId);
            if (a > -1) {
                JobTaskIdFI.splice(a, 1);
                //console.log("nih di if 1", JobTaskIdFI);
            } else {
                JobTaskIdFI.push(JobTaskId);
                //console.log("nih else", JobTaskIdFI);
            }
            if (JobTaskIdFI.length == 0) {
                $scope.btnFI = true;
            } else {
                $scope.btnFI = false;
            }
        }

        $scope.doClockOff = function(Model, JobTaskForClockOff) {
            //console.log("clockoff", Model);
            $scope.getActualTimeForClock();
            RepairProcessGR.putClockJobBase(4, Model, JobTaskForClockOff, $scope.actualFullTime, $scope.xDataWoList[0].stallIdSplit).then(
                function(res) {
                    bsNotify.show({
                        title: 'Clock Off Berhasil',
                        text: 'I will close in 2 seconds.',
                        size: 'big',
                        timeout: 2000,
                        type: 'success'
                    });
                    $scope.getDataDetail(Model.JobId);
                    $scope.Kembali();


                    //kirim notifikasi clock off
                    var DataNotif = {};
                    var messagetemp = "";
                    messagetemp = Model.PoliceNumber;


                    DataNotif.Message = messagetemp;
                    RepairProcessGR.sendNotifRole(DataNotif, 1031, 30).then(
                        function(res) {

                        },
                        function(err) {
                            //console.log("err=>", err);
                        });
                },
                function(err) {
                    //console.log("err=>", err);
                }
            );
            ngDialog.closeAll();
            // return Model.Status
        }

        $scope.pauseWo = function() {
            // //console.log("Data Job Task", dataJobTask , "JobId" , JobId);
            var JobTeknisinih = [];
            for (var i = 0; i < $scope.xDataWoList.length; i++) {

                //console.log("data wo", $scope.xDataWoList[i]);

                for (var a = 0; a < $scope.xDataWoList[i].JobTask.length; a++) {
                    //console.log("data JObTask", $scope.xDataWoList[i].JobTask[a]);

                    for (var z = 0; z < $scope.xDataWoList[i].JobTask[a].JobTaskTechnician.length; z++) {
                        JobTeknisinih = $scope.xDataWoList[i].JobTask[a].JobTaskTechnician[z];
                        //console.log("job Teksnisi", JobTeknisinih);
                    }

                }

            }
            //console.log("jobTeknisi", JobTeknisinih);
            // if(JobTeknisinih.length == 0){
            //   // //console.log("gagal");
            //           bsNotify.show(
            //                           {
            //                               size: 'big',
            //                               type: 'danger',
            //                               title: "Mohon Lakukan Job Dispatch Terlebih Dahulu"
            //                               // ,
            //                               // content: error.join('<br>'),
            //                               // number: error.length
            //                           }
            //                       );
            // }else{
            $scope.showPause = true;
            $scope.getDataPause();
            // }
        }

        $scope.btnJobSuggest = true;
        $scope.BeforePause = function(Model, JobId) {
            RepairProcessGR.GetJobTasks(JobId).then(function (resultJ) {
                var ada_job_dihapus = 0;
                var ada_job_ditambah = 0;
                if (resultJ.data.length > $scope.copy_dataTask.length){
                    ada_job_ditambah++
                }
                for (var q=0; q<$scope.copy_dataTask.length; q++){
                    for (var d=0; d<resultJ.data.length; d++){
                        if ($scope.copy_dataTask[q].JobTaskId == resultJ.data[d].JobTaskId){
                            if (resultJ.data[d].isDeleted == 1 && $scope.copy_dataTask[q].isDeleted == 0){
                                // berarti ada yg di delete tp si foreman blm refresh
                                ada_job_dihapus++
                            }
                        }
                    }
                }

                if (ada_job_dihapus > 0 || ada_job_ditambah > 0){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Terdapat update data pada menu lain, silahkan klik kembali lalu ulangi proses ini.",
                    });
                    return
                } else {
                    RepairProcessGR.CheckIsJobCancelWO(JobId).then(function (resultC) {
                        if (resultC.data == 666){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "WO kendaran ini sedang dalam pengajuan approval cancel WO.",
                            });
                        } else {
                            RepairProcessGR.getStatWO($scope.xDataWoList[0].JobId, $scope.xDataWoList[0].Status).then(function(resux2) { 
                                if (resux2.data == 1){
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Status Kendaraan Sudah Berganti, Silahkan Refresh"
                                    });
                                } else {
                                    //console.log('Job ID', JobId);
                                    console.log('M aja ', Model);
                                    $scope.JobBasedSelectedJob = angular.copy(Model);
                
                
                                    var JobTeknisinih = [];
                                    for (var i = 0; i < $scope.xDataWoList.length; i++) {
                
                                        //console.log("data wo", $scope.xDataWoList[i]);
                
                                        for (var a = 0; a < $scope.xDataWoList[i].JobTask.length; a++) {
                                            //console.log("data JObTask", $scope.xDataWoList[i].JobTask[a]);
                
                                            for (var z = 0; z < $scope.xDataWoList[i].JobTask[a].JobTaskTechnician.length; z++) {
                                                JobTeknisinih = $scope.xDataWoList[i].JobTask[a].JobTaskTechnician[z];
                                                //console.log("job Teksnisi", JobTeknisinih);
                                            }
                
                                        }
                
                                    }
                
                                    if (JobTeknisinih.length == 0) {
                                        // //console.log("gagal");
                                        // bsNotify.show(
                                        //                 {
                                        //                     size: 'big',
                                        //                     type: 'danger',
                                        //                     title: "Mohon Lakukan Job Dispatch Terlebih Dahulu"
                                        //                     // ,
                                        //                     // content: error.join('<br>'),
                                        //                     // number: error.length
                                        //                 }
                                        //             );
                                        bsNotify.show({
                                            title: 'Mohon Lakukan Job Dispatch Terlebih Dahulu',
                                            text: 'I will close in 2 seconds.',
                                            size: 'big',
                                            timeout: 2000,
                                            type: 'danger'
                                                // size: 'big'
                                                // size: 'big',
                                                // type: 'danger',
                                                // title: "Mohon isi hasil inspeksi dan tanggal job selanjutnya",
                                                // timer : 3000
                                        });
                                    } else {
                                        // ngDialog.openConfirm ({
                                        //             template:'\
                                        //                          <p>Anda yakin untuk clock Pause</p>\
                                        //                          <div class="ngdialog-buttons">\
                                        //                            <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog()">Tidak</button>\
                                        //                            <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm()">Ya</button>\
                                        //                          </div>',
                                        //                       plain: true,
                                        //                       controller: 'RepairProcessGRController',
                                        //             }).then(function (value) {
                                        // perform delete operation
                                        ////console.log("then",value);
                                        // $scope.doPause(Model, JobId); //ini di komen karena kalau mencet icon pause maunya cuma ngaktifin alasan pause
                                        $scope.ModelForPause = Model;
                                        $scope.JobIdForPause = JobId;
                                        $scope.doBtnEnable();
                                        // }, function (value) {
                                        //             //Do something
                                        //             ////console.log("else",value);
                                        // });
                                    }
                                }
                            })
        
                        }
        
                    });

                }


            })

            
            
            
        }

        $scope.doPause = function(Model, JobId) {
            //console.log("Pause", Model);
            if (($scope.mData.PauseReasonId == null) || ($scope.mData.PauseReasonId == '') || ($scope.mData.PauseReasonId == undefined)) {
                bsNotify.show({
                    title: 'Mohon Pilih Alasan Pause',
                    text: 'I will close in 2 seconds.',
                    size: 'big',
                    timeout: 2000,
                    type: 'danger'
                });
            } else {
                RepairProcessGR.putClockWoBase(2, Model).then(
                    function(res) {
                        bsNotify.show({
                            title: 'Pause Berhasil',
                            text: 'I will close in 2 seconds.',
                            size: 'big',
                            timeout: 2000,
                            type: 'success'
                        });
                        $scope.getDataDetail(JobId);
                    },
                    function(err) {
                        //console.log("err=>", err);
                    }
                );
            }

            ngDialog.closeAll();
            // return Model.Status
        }

        $scope.doBtnEnable = function() {
            $scope.btnJobSuggest = false;
            $scope.btnClockOff = true;
            $scope.btnClockOffJob = true;
        }


        var JobTaskIdDtl = [];

        $scope.ChoseDataDtl = function(JobTaskId, $index) {
            //console.log("JobTaskId", JobTaskId, "idx", $index);
            var a = JobTaskIdDtl.indexOf(JobTaskId);
            if (a > -1) {
                JobTaskIdDtl.splice(a, 1);
                //console.log("nih di if 1", JobTaskIdDtl);
            } else {
                JobTaskIdDtl.push(JobTaskId);
                //console.log("nih else", JobTaskIdDtl);
            }
            // if(JobTaskIdDtl.length == 0){
            //   $scope.btnFI = true;
            // }else{
            //   $scope.btnFI = false;
            // }
        }

        $scope.savePauseWo = function(model, dataJobTask, JobId) {
            console.log('pause nih model', model)
            console.log('pause nih datajobtask', dataJobTask)
            console.log('data yang mau di pause', $scope.JobBasedSelectedJob.JobTaskId)
            RepairProcessGR.sendNotify(model).then(function(res) {
                Console.log('apaa');
            });
            var JobTaskIdJobBASE = $scope.JobBasedSelectedJob.JobTaskId;
            // if(JobTaskIdDtl.length == 0){

            //         bsNotify.show(
            //                         {
            //                             size: 'big',
            //                             type: 'danger',
            //                             title: "Mohon Pilih Pekerjaan"
            //                             // ,
            //                             // content: error.join('<br>'),
            //                             // number: error.length
            //                         }
            //                     );
            // }else{
            if ($scope.xDataWoList[0].Revision != 0) {

                var dataJBTsk = [];
                angular.forEach(dataJobTask, function(row) {
                    if ($scope.xDataWoList[0].Revision >= row.Revision) {
                        dataJBTsk.push(row);
                        //console.log("Model Yang Di Push", dataJBTsk);
                    }
                });

                RepairProcessGR.clockPause(model, dataJBTsk, JobTaskIdDtl, jobpartsfinal, JobTaskIdJobBASE, $scope.xDataWoList[0].stallIdSplit).then(
                    function(res) {
                        model.ResponseDesc = "";
                        model.T2 = "";
                        model.T1 = "";
                        model.TroubleDesc = "";
                        model.Duration = "";
                        $scope.duration = false;
                        model.IsNeedAdditionalTime = "";
                        model.IsOwnDo = "";
                        model.IsNeedRSA = "";
                        model.PauseReasonDesc = "";
                        model.PauseResaon = "";
                        model.PauseReasonId = "";
                        $scope.showPause = false;
                        $scope.mData = {};
                        jobpartsfinal = [];
                        JobTaskIdDtl = [];
                        jobpartsfinal = [];
                        $scope.btnJobSuggest = true;
                        bsNotify.show({
                            title: 'Pause Berhasil',
                            text: 'I will close in 3 seconds.',

                            size: 'big',
                            timeout: 3000,
                            type: 'success'
                                // size: 'big'
                                // size: 'big',
                                // type: 'danger',
                                // title: "Mohon isi hasil inspeksi dan tanggal job selanjutnya",
                                // timer : 3000
                        });
                        console.log('nii nih si ase1', $scope.ModelForPause)
                        $scope.getDataDetail($scope.JbId, $scope.ModelForPause);
                        // $scope.mData=[];
                        // $scope.SimpanJobSuggest(model);
                        // $scope.getDataDetail(JobId);

                    },
                    function(err) {
                        //console.log("err=>", err);
                    }
                );

            } else {

                //console.log("Model", model, "Data Job Task", dataJobTask, "Job id", JobId);
                RepairProcessGR.clockPause(model, dataJobTask, JobTaskIdDtl, jobpartsfinal, JobTaskIdJobBASE, $scope.xDataWoList[0].stallIdSplit).then(
                    function(res) {
                        model.ResponseDesc = "";
                        model.T2 = "";
                        model.T1 = "";
                        model.TroubleDesc = "";
                        model.Duration = "";
                        $scope.duration = false;
                        model.PauseReasonId = "";
                        model.IsNeedAdditionalTime = "";
                        model.IsOwnDo = "";
                        model.IsNeedRSA = "";
                        model.PauseReasonDesc = "";
                        model.PauseResaon = "";
                        $scope.showPause = false;
                        $scope.mData = {};
                        $scope.btnJobSuggest = true;
                        JobTaskIdDtl = [];
                        jobpartsfinal = [];
                        bsNotify.show({
                            title: 'Pause Berhasil',
                            text: 'I will close in 3 seconds.',

                            size: 'big',
                            timeout: 3000,
                            type: 'success'
                                // size: 'big'
                                // size: 'big',
                                // type: 'danger',
                                // title: "Mohon isi hasil inspeksi dan tanggal job selanjutnya",
                                // timer : 3000
                        });
                        console.log('nii nih si ase2', $scope.ModelForPause)

                        $scope.getDataDetail($scope.JbId, $scope.ModelForPause);

                        // $scope.SimpanJobSuggest(model);
                        // $scope.getDataDetail(JobId);

                    },
                    function(err) {
                        //console.log("err=>", err);
                    }
                );
            }
            // }
        }

        $scope.pause = function() {
            var JobTeknisinih = [];
            for (var i = 0; i < $scope.xDataWoList.length; i++) {

                //console.log("data wo", $scope.xDataWoList[i]);

                for (var a = 0; a < $scope.xDataWoList[i].JobTask.length; a++) {
                    //console.log("data JObTask", $scope.xDataWoList[i].JobTask[a]);

                    for (var z = 0; z < $scope.xDataWoList[i].JobTask[a].JobTaskTechnician.length; z++) {
                        JobTeknisinih = $scope.xDataWoList[i].JobTask[a].JobTaskTechnician[z];
                        //console.log("job Teksnisi", JobTeknisinih);
                    }

                }

            }
            //console.log("jobTeknisi", JobTeknisinih);
            // if(JobTeknisinih.length == 0){
            //   // //console.log("gagal");
            //           bsNotify.show(
            //                           {
            //                               size: 'big',
            //                               type: 'danger',
            //                               title: "Mohon Lakukan Job Dispatch Terlebih Dahulu"
            //                               // ,
            //                               // content: error.join('<br>'),
            //                               // number: error.length
            //                           }
            //                       );
            // }else{
            $scope.showPause = true;
            $scope.getDataPause();
            // }
        }

        $scope.savePause = function(model, dataJobTask, JobId) {
            RepairProcessGR.clockJobPause(model, dataJobTask, JobTaskIdDtl, $scope.JobBasedSelectedJob, $scope.xDataWoList[0].stallIdSplit).then(
                function(res) {
                    $scope.getDataDetail(JobId);
                    model.ResponseDesc = "";
                    model.T2 = "";
                    model.T1 = "";
                    model.TroubleDesc = "";
                    model.Duration = "";
                    $scope.duration = false;
                    model.IsNeedAdditionalTime = false;
                    model.IsOwnDo = false;
                    JobTaskIdDtl = [];
                    model.IsNeedRSA = false;
                    model.PauseReasonDesc = "";
                    model.PauseResaon = "";
                    $scope.showPause = false;
                },
                function(err) {
                    //console.log("err=>", err);
                }
            );
        }

        $scope.actualFullTime = '';
        $scope.getActualTimeForClock = function() {
            var actualFullDate = new Date();
            var actualHour = actualFullDate.getHours();
            var actualMinute = actualFullDate.getMinutes();
            var actualSecond = actualFullDate.getSeconds();
            var actualFullTime = actualHour.toString() + ':' + actualMinute.toString() + ':' + actualSecond.toString();
            console.log('actual boyyyysss', actualFullTime);
            $scope.actualFullTime = actualFullTime;
            // return actualFullTime;
        }

        $scope.CountShift = function() {
            // //console.log("model", $scope.mData);
            console.log("model 1", $scope.mData.CategoryJobSuggestKeamanan);
            console.log("model 2", $scope.mData.CategoryJobSuggestPeraturan);
            console.log("model 3", $scope.mData.CategoryJobSuggestKenyamanan);
            console.log("model 4", $scope.mData.CategoryJobSuggestEfisiensi);
            $scope.mData.SuggestionCatg =
                ($scope.mData.CategoryJobSuggestKeamanan == undefined ? 0 : $scope.mData.CategoryJobSuggestKeamanan) +
                ($scope.mData.CategoryJobSuggestPeraturan == undefined ? 0 : $scope.mData.CategoryJobSuggestPeraturan) +
                ($scope.mData.CategoryJobSuggestKenyamanan == undefined ? 0 : $scope.mData.CategoryJobSuggestKenyamanan) +
                ($scope.mData.CategoryJobSuggestEfisiensi == undefined ? 0 : $scope.mData.CategoryJobSuggestEfisiensi);
            //console.log("Hasil COunt Suggestion Catgeory", $scope.mData.SuggestionCatg);
            // // $scope.mData.SuggestionCatg = $scope.mData.CategoryJobSuggestKeamanan + $scope.mData.CategoryJobSuggestPeraturan + $scope.mData.CategoryJobSuggestKenyamanan + $scope.mData.CategoryJobSuggestEfisiensi;
        }

        $scope.SimpanJobSuggest = function(Model) {

            //console.log("model", Model);

            RepairProcessGR.postJobSuggest($scope.JbId, Model).then(
                function(res) {
                    // $scope.getData();
                    // $scope.getDataDetail($scope.JbId);
                    // $scope.mData=[];
                    // $scope.xDataWoList

                    RepairProcessGR.putJobSuggestCRM($scope.VinData[0].VIN, Model).then(
                        function(res) {
                            // $scope.getData();
                            // $scope.getDataDetail($scope.JbId);
                            // $scope.mData=[];
                            // $scope.xDataWoList


                            // RepairProcessGR.putJobSuggestCT($scope.VinData[0].VIN,Model).then(
                            //       function(res){

                            RepairProcessGR.putTeknisiNote($scope.JbId, $scope.mData.TechnicianNotes)
                                .then(

                                    function(res) {
                                        //console.log("suksess");

                                        $scope.getDataDetail($scope.JbId);
                                        // sini notif simpan job suggest bill
                                        bsNotify.show({
                                            title: 'Data Berhasil Disimpan',
                                            text: 'I will close in 2 seconds.',
                                            size: 'big',
                                            timeout: 3000,
                                            type: 'success'
                                        });
                                        $scope.mData = {};
                                    },
                                    function(err) {
                                        //console.log("err=>", err);
                                    }
                                );

                            //       },
                            //       function(err) {
                            //               //console.log("err=>", err);
                            //       }
                            // );

                        },
                        function(err) {
                            //console.log("err=>", err);
                        }
                    );

                },
                function(err) {
                    //console.log("err=>", err);
                }
            );

        }

        //----------------------------------
        //Filter Type antari kode polisi atau kode WO
        $scope.ChoseFilter = [{
                ChoseFilterId: 1,
                ChoseFilter: "No WO"
            },
            {
                ChoseFilterId: 2,
                ChoseFilter: "No polisi"
            }
        ];
        //----------------------------------

        $scope.ShowJob = function(value) {
            if (value == 1) {
                $scope.JobShow = true;
            } else {
                $scope.JobShow = false;
            }
        }

        $scope.mDataJob = {};
        // data,JbTskId
        $scope.ChoseProblem = function(selected) {
                console.log("selected choose prob", selected);

                if (selected.Status == 4 || selected.Status == 2 || selected.Status < 1 || selected.Status == null || selected.IsStopPage == 1) {
                    $scope.btnClockOffJob = true;
                } else {
                    $scope.btnClockOffJob = false;
                }
                $scope.JbTskId = $scope.mDataJob.JobTaskF;
                // $scope.DataMaterial = [];
                // //console.log("material",$scope.mData.JobTaskF);
                // $scope.DataMaterial = data.JobParts;
                // $scope.DataChoseProblem = data;
                //console.log("$scope.JbTskId", $scope.JbTskId);
                //console.log("$scope.xDataWoList Choose", jobtaskFinalData);
                if ($scope.JbTskId !== undefined) {

                    // =================start selected=================
                    // var JobTaskProgressRemove = _.remove(selected.JobTaskProgress, function(l) {
                    //     return l.ProblemFoundedDate !== null;
                    // });
                    // //console.log("JobTaskProgressRemove", JobTaskProgressRemove);
                    // JobTaskProgressRemove.sort(function(a, b) {
                    //     return a.Id - b.Id;
                    // });

                    // var countdata = JobTaskProgressRemove.length;
                    //console.log("countdata", countdata);
                    // if (countdata !== 0) {

                    //     if (JobTaskProgressRemove[countdata - 1].PauseReasonDesc !== undefined) {
                    //         $scope.mData.PauseReasonDesc = JobTaskProgressRemove[countdata - 1].PauseReasonDesc;
                    //         $scope.mData.PauseReasonId = JobTaskProgressRemove[countdata - 1].PauseReasonId;
                    //     } else {
                    //         $scope.mData.PauseReasonDesc = "";
                    //         $scope.mData.PauseReasonId = "";
                    //     }

                    // }

                    // =============================================end selected=========

                    if (selected.JobTaskProgress.length > 0) {
                        var posisiTaskProgress = selected.JobTaskProgress.length - 1;
                        console.log('posisiTaskProgress', posisiTaskProgress) // jobtaskprogress nambah terus, jadi ambil posisi terakhir buat cari tau status. kalau pause dapetin alasan dan desc nya
                        if (selected.JobTaskProgress[posisiTaskProgress].ProblemFoundedDate != null || selected.JobTaskProgress[posisiTaskProgress].ProblemFoundedDate != undefined) {
                            $scope.mData.PauseReasonId = selected.JobTaskProgress[posisiTaskProgress].PauseReasonId;
                            $scope.mData.PauseReasonDesc = selected.JobTaskProgress[posisiTaskProgress].PauseReasonDesc;
                            console.log('masuk ada pause', $scope.mData.PauseReasonDesc)
                        } else {
                            $scope.mData.PauseReasonId = "";
                            $scope.mData.PauseReasonDesc = "";
                            console.log('ga msk 1')
                        }
                    } else {
                        $scope.mData.PauseReasonId = "";
                        $scope.mData.PauseReasonDesc = "";
                        console.log('ga msk 2')

                    }


                    // for(var i = 0; i < jobtaskFinalData.length; i++){
                    //   for(var a = 0; a < jobtaskFinalData[i].JobTask.length; a++){

                    //     var JobTaskRemove = _.remove(jobtaskFinalData[i].JobTask, function(n) {
                    //               return n.JobTaskId == $scope.JbTskId;
                    //     });
                    //     //console.log("JobTaskRemove",JobTaskRemove);

                    //     for(var k = 0; k < jobtaskFinalData[i].JobTask[a].JobTaskProgress.length; k++){

                    //         // if(JobTaskRemove[0].JobTaskProgress !== undefined){

                    //           var JobTaskProgressRemove = _.remove(JobTaskRemove[0].JobTaskProgress, function(l) {
                    //                     return l.ProblemFoundedDate !== null;
                    //           });
                    //           //console.log("JobTaskProgressRemove",JobTaskProgressRemove);

                    //         // }
                    //     }

                    //   }
                    // }

                }
                RepairProcessGR.getTeknisiTask($scope.mDataJob.JobTaskF).then(
                    function(res) {
                        $scope.DataTeknisiTask = res.data;
                        //console.log("data Teknisi1", $scope.DataTeknisiTask);
                        $scope.DataProblem = true;
                    },
                    function(err) {
                        //console.log("err=>", err);
                    }
                );

                // $scope.color = {'background-color': 'red'};
                // //console.log("Data chose problem", $scope.DataChoseProblem);
            }
            //----------------------------------
            //----------------------------------

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }


        $scope.gridWODetail = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            data: 'DataMaterial',
            showGridFooter: true,
            columnDefs: [
                { field: 'ModelCode', name: 'Nomor Material' },
                { field: 'PartsName', name: 'Nama Material' },
                { field: 'Status', name: 'Status' },
                { field: 'ETA', name: 'ETA' },
                { field: 'Qty', name: 'Permintaan Qty' },
                { field: 'QtyGI', name: 'Issue Qty' },
                { field: 'Satuan.Name', name: 'Satuan' }
            ]
        };


        $scope.gridWarranty = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            enableCellEditOnFocus: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            data: 'DataWarranty',
            showGridFooter: true,
            columnDefs: [
                { field: 'TaskName', name: 'Nama Pekerjaan Warranty', enableCellEdit: false },
                { field: 'OFP', name: 'OFP' },
                // { field: 'TFirst1', name:'T1'},
                // { field: 'TFirst2', name:'T2'},
                {
                    field: 'TFirst1',
                    name: 'T1',
                    // editableCellTemplate: 'ui-grid/dropdownEditor',
                    // editDropdownValueLabel: 'Name',
                    // editDropdownIdLabel: 'T1Id',
                    // enableCellEdit: true,
                    // editDropdownOptionsArray: $scope.T1,
                },
                {
                    field: 'T2',
                    name: 'T2',
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownValueLabel: 'T2',
                    editDropdownIdLabel: 'T2',
                    editDropdownOptionsArray: [{
                            T2: 'Bemper'
                        },
                        {
                            T2: 'Jok'
                        },
                        {
                            T2: 'Knalpot'
                        }
                    ]
                },
                { field: 'Keterangan', name: 'Keterangan' }
            ]
        };


        $scope.gridparts = {
            enableFiltering: true,
            enableSorting: true,
            multiSelect: true,
            enableRowSelection: true,
            enableSelectAll: true,
            // selectionRowHeaderWidth: 35,
            // rowHeight: 35,
            // $scope.dataJobParts
            data: 'dataJobParts',
            showGridFooter: true,
            columnDefs: [
                { field: 'Part.PartsCode', name: 'Kode Parts' },
                { field: 'PartsName', name: 'Nama Parts' },
                { field: 'Qty', name: 'Qty' }
            ],
            onRegisterApi: function(gridApi) {
                // set gridApi on $scope
                $scope.gridApi = gridApi;
                $scope.gridApi.selection.on.rowSelectionChanged($scope, function(row) {
                    $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                    // //console.log("selected=>",$scope.selectedRows);
                    if ($scope.onSelectRows) {
                        $scope.onSelectRows($scope.selectedRows);
                    }
                });
            }
        };

        $scope.selectRole = function(rows) {
            //console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        var jobpartsfinal;
        $scope.onSelectRows = function(rows) {
            // var a = JobTaskIdDtl.indexOf(JobTaskId);
            // if (a > -1){
            //     JobTaskIdDtl.splice(a, 1);
            //     //console.log("nih di if 1", JobTaskIdDtl);
            // }else {
            //     JobTaskIdDtl.push(JobTaskId);
            //     //console.log("nih else", JobTaskIdDtl);
            // }

            //console.log("onSelectRows=>", rows);
            jobpartsfinal = rows;
            //console.log("jobpartsfinal", jobpartsfinal);
        }



        /// FROM BG STEV
        $scope.user = CurrentUser.user();
        $scope.mTrialCanvas = null; //Model
        $scope.cTrialCanvas = null; //Collection
        $scope.xTrialCanvas = {};
        $scope.xTrialCanvas.selected = [];

        $scope.inputMap = false;
        $scope.formApi = {};
        $scope.areaApi = {};
        $scope.filter = { vType: null };

        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.areasArray = [];
        $scope.show_modal = { wac: false };
        $scope.groupWACData = [];
        $scope.jobWACData = [];
        $scope.imageSel = null;
        $scope.imageWACArr = [];

        // GeneralMaster.getData(2030).then(
        //     function(res) {
        //         $scope.VTypeData = res.data.Result;
        //         // //console.log("$scope.UomData : ",$scope.UomData);
        //         return res.data;
        //     }
        // );

        $scope.selectItemWAC = function(selected) {
            WO.getWACBPbyItemId(selected.ItemId).then(
                function(res) {
                    //console.log(res.data.Result);
                    $scope.jobWACData = res.data.Result;
                },
                function(err) {
                    //console.log("err=>", err);
                }
            )
        };


        $scope.statusObj = { status: 1 };
        var dotsData = { 2221: [], 2222: [], 2223: [], 2224: [], 2225: [], 2226: [], 2227: [] };

        function getImgAndPoints(mstId) {
            switch (mstId) {
                case 2221:
                    $scope.imageSel = "../images/wacImages/Type A.png";
                    break;
                case 2222:
                    $scope.imageSel = "../images/wacImages/Type B.png";
                    break;
                case 2223:
                    $scope.imageSel = "../images/wacImages/Type C.png";
                    break;
                case 2224:
                    $scope.imageSel = "../images/wacImages/Type D.png";
                    break;
                case 2225:
                    $scope.imageSel = "../images/wacImages/Type E.png";
                    break;
                case 2226:
                    $scope.imageSel = "../images/wacImages/Type F.png";
                    break;
                case 2227:
                    $scope.imageSel = "../images/wacImages/Type G.png";
            }
            var temp = _.find($scope.imageWACArr, function(o) {
                return o.TypeId == mstId;
            });

            return JSON.parse(temp.Points);
        };


        $scope.chooseVType = function(selected) {
            WOBP.getDamage().then(function(res) {
                $scope.DamageVehicle = res.data.Result;
                _.map($scope.DamageVehicle, function(val) {
                    val.Id = val.MasterId;
                })
                $scope.gridWac.columnDefs[2].editDropdownOptionsArray = $scope.DamageVehicle;
                console.log("$scope.dramagaaaa", $scope.DamageVehicle);
            })
            $('#dataWAC').find('*').attr('ng-disabled', true).attr('skip-enable', true).attr('disabled', 'disabled');
            RepairProcessGR.getProbPoints($scope.mData.JobId, null).then(
                function(res) {
                    $scope.areasArray = [];
                    $scope.areasArray.splice();
                    console.log("RepairProcessGR.getProbPoints", res.data);
                    console.log("================>", $scope.imageWACArr);
                    mstId = angular.copy(res.data.Result[0].TypeId);
                    console.log("ini MstId", mstId);
                    $scope.filter.vType = mstId;
                    console.log("probPoints", res.data.Result);
                    // $scope.chooseVType(mstId);
                    console.log("$scope.areasArray1", $scope.areasArray);
                    $scope.areasArray = getImgAndPoints(mstId);
                    console.log("$scope.areasArray2", $scope.areasArray);
                    dotsData[mstId] = [];
                    if (res.data.Result.length > 0) {
                        for (var i in res.data.Result) {
                            var getTemp = [];
                            getTemp = JSON.parse(res.data.Result[i]["Points"]);
                            var xtemp = {};
                            console.log('gettemp', getTemp);

                            // if(getTemp.length == 0){
                            //     console.log('cek xtemp kosong', xtemp);
                            //     console.log('areas array css', $scope.areasArray);
                            //     for (var i=0; i<$scope.areasArray.length; i++){
                            //         $scope.areasArray[i].cssClass = 'unchoosen-area';
                            //     }
                            //     // if($('.ngAreas-element').hasClass("choosen-area")){
                            //     //     $('.ngAreas-element').removeClass('choosen-area');
                            //     // }
                            // }


                            for (var j in getTemp) {
                                console.log('gettempJstat', getTemp[j]);
                                var xtemp = {};
                                xtemp = angular.copy(getTemp[j]);
                                delete xtemp['status'];
                                console.log("xtemp : ", xtemp);
                                console.log("scope.areasarray3 : ", $scope.areasArray);
                                if (typeof _.find($scope.areasArray, xtemp) !== 'undefined') {
                                    console.log("merah");
                                    console.log("find..", xtemp);
                                    console.log("scope.areasarray4 : ", $scope.areasArray);
                                    var idx = _.findIndex($scope.areasArray, xtemp);
                                    console.log('idx', idx);
                                    xtemp.cssClass = "choosen-area";
                                    $scope.areasArray[idx] = xtemp;

                                    dotsData[mstId].push({ "ItemId": xtemp.areaid, "ItemName": xtemp.name, "Points": xtemp, "Status": getTemp[j].status });
                                }
                            }

                        }
                        setDataGrid(dotsData[mstId]);
                    }
                },
                function(err) {
                    $scope.areasArray = getImgAndPoints(mstId);
                }
            );
            RepairProcessGR.getDataWac($scope.mData.JobId).then(
                function(res) {
                    var dataWac = res.data;
                    $scope.finalDataWac = dataWac;

                    //console.log("Data WAC", dataWac);
                    $scope.mData.OtherDescription = dataWac.Other;
                    $scope.mData.moneyAmount = dataWac.MoneyAmount;
                    $scope.mData.BanSerep = dataWac.isSpareTire;
                    $scope.mData.CdorKaset = dataWac.isCD;
                    $scope.mData.Payung = dataWac.isUmbrella;
                    $scope.mData.Dongkrak = dataWac.isJack;
                    $scope.mData.STNK = dataWac.isVehicleRegistrationNumber;
                    $scope.mData.P3k = dataWac.isP3k;
                    $scope.mData.KunciSteer = dataWac.isSteerLock;
                    $scope.mData.ToolSet = dataWac.isToolSet;
                    $scope.mData.ClipKarpet = dataWac.isCarpetClip;
                    $scope.mData.BukuService = dataWac.isServiceBook;
                    $scope.mData.AlarmCondition = dataWac.isAlarmConditionOk;
                    $scope.mData.AcCondition = dataWac.isAcOk;
                    $scope.mData.PowerWindow = dataWac.isPowerWindowOk;
                    $scope.mData.CarType = dataWac.isPowerWindowOk;
                    $scope.slider.value = dataWac.Fuel;
                    console.log("$scope ========>", $scope.mData);
                    // $scope.mData.TechnicianNotes = TeknisiNote[0].TechnicianNotes;
                },
                function(err) {
                    //console.log("err=>", err);
                }
            );
            $scope.refreshSlider();
        }

        function setDataGrid(data) {
            console.log('data setDataGrid', data);
            $scope.gridWac.data = angular.copy(data);
        }
        $scope.onDropdownChange = function(ent) {
            console.log(ent);
            var temp = _.findIndex(dotsData[mstId], { "ItemId": ent.ItemId, "ItemName": ent.ItemName, "Points": ent.Points });
            if (temp > -1) {
                dotsData[mstId][temp].Status = ent.Status;
            }
        }
        var customCellTemplate = '<div><select style="height:40px;" ui-grid-edit-dropdown ng-model=\"MODEL_COL_FIELD\" ng-change="grid.appScope.onDropdownChange(row.entity)" ng-options=\"field[editDropdownIdLabel] as field[editDropdownValueLabel] CUSTOM_FILTERS for field in editDropdownOptionsArray\"></select></div>'
        $scope.gridWac = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'JobWACId', field: 'JobWACId', width: '7%', visible: false },
                { name: 'Nama Item', field: 'ItemName' },
                {
                    name: 'Status Kerusakan',
                    field: 'Status',
                    // editableCellTemplate: 'ui-grid/dropdownEditor',
                    editableCellTemplate: customCellTemplate,
                    editDropdownValueLabel: 'Name',
                    editDropdownIdLabel: 'Id',
                    editDropdownOptionsArray: $scope.DamageVehicle,
                    // editDropdownOptionsArray: [
                    //     { 'Id': 1, 'Name': 'Penyok' },
                    //     { 'Id': 2, 'Name': 'Baret' },
                    //     { 'Id': 3, 'Name': 'Hilang' },
                    // ],
                    // editDropdownOptionsArray: [
                    //     { 'Id': 1, 'Name': 'Penyok' }, { 'Id': 2, 'Name': 'Baret' }, { 'Id': 3, 'Name': 'Rusak' }, { 'Id': 4, 'Name': 'Hilang' }
                    // ],
                    cellFilter: "griddropdown:this"
                },
            ]
        };

        $scope.onChangeAreas = function(ev, boxId, areas, area) {
            //console.log(areas);
            $scope.logDots = JSON.stringify(areas);
            $scope.$apply();
        }

        $scope.onAddArea = function(ev, boxId, areas, area) {
            //console.log(areas);
            $scope.logDots = JSON.stringify(areas);
            $scope.$apply();
        }

        $scope.onRemoveArea = function(ev, boxId, areas, area) {
            //console.log(areas);
            $scope.logDots = JSON.stringify(areas);
            $scope.$apply();
        }

        var sel;
        $scope.doClick = function(ev, boxId, areas, area, stat, selection) {
            // //console.log(boxId);
            // //console.log(dotsData[mstId]);
            var temp = null;
            for (var i in dotsData[mstId]) {
                if (dotsData[mstId][i].Points) {
                    if (dotsData[mstId][i].Points.areaid == boxId.areaid) {
                        temp = dotsData[mstId][i].Status;
                    }
                }
            }
            //console.log("temp : ", temp);
            // if (selection.hasClass("choosen-area")) {
            //     if (sel != undefined) {
            //         sel.empty();
            //     }
            //     $name = $(selection).empty().append($("<div><span class=\"select-area-field-label border-thin name-area-id-" + area.areaid + "\">" + (temp == 1 ? "Penyok" : (temp == 2 ? "Baret" : (temp == 3 ? "Rusak" : "Hilang"))) + "</span></div>"));
            //     sel = selection;
            // }
        };
        $scope.mData.Suggestion = "";
        $scope.disNoType = true;
        $scope.onTypeSuggest = function(item) {
            if (item == "") {
                //console.log('onTypeSuggest 2', item);
                $scope.mData.SuggestionDate = null;
                $scope.mData.CategoryJobSuggestKeamanan = 0;
                $scope.mData.CategoryJobSuggestPeraturan = 0;
                $scope.mData.CategoryJobSuggestKenyamanan = 0;
                $scope.mData.CategoryJobSuggestEfisiensi = 0;
            } else {
                //console.log('onTypeSuggest 3', item);
            }

        };

        // $scope.grid = {
        //     enableSorting: true,
        //     enableRowSelection: true,
        //     multiSelect: true,
        //     enableSelectAll: true,
        //     //showTreeExpandNoChildren: true,
        //     // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        //     // paginationPageSize: 15,
        //     columnDefs: [
        //         { name: 'JobWACId', field: 'JobWACId', width: '7%', visible: false },
        //         { name: 'Nama Item', field: 'ItemName' },
        //         {
        //             name: 'Status Kerusakan',
        //             field: 'Status',
        //             editableCellTemplate: 'ui-grid/dropdownEditor',
        //             editDropdownValueLabel: 'Name',
        //             editDropdownIdLabel: 'Id',
        //             editDropdownOptionsArray: [
        //                 { 'Id': 1, 'Name': 'Penyok' },
        //                 { 'Id': 2, 'Name': 'Baret' },
        //                 { 'Id': 3, 'Name': 'Hilang' }
        //             ],
        //             cellFilter: "griddropdown:this"
        //         },
        //     ]
        // };

        // }).filter('griddropdown', function() {
        //         return function(input, xthis) {
        //             //console.log("filter..");
        //             //console.log(input);
        //             //console.log(xthis);
        //             if (xthis !== undefined) {
        //                 if (xthis.col !== undefined) {
        //                     var map = xthis.col.colDef.editDropdownOptionsArray;
        //                     var idField = xthis.col.colDef.editDropdownIdLabel;
        //                     var valueField = xthis.col.colDef.editDropdownValueLabel;
        //                     for (var i = 0; i < map.length; i++) {
        //                         if (map[i][idField] == input) {
        //                             return map[i][valueField];
        //                         }
        //                     }
        //                 }
        //             }

        //             return '';
        //         };
    }).filter('griddropdown', function() {
        return function(input, xthis) {
            // console.log("filter..");
            // console.log(input);
            // console.log(xthis);
            if (xthis !== undefined) {
                if (xthis.col !== undefined) {
                    var map = xthis.col.colDef.editDropdownOptionsArray;
                    var idField = xthis.col.colDef.editDropdownIdLabel;
                    var valueField = xthis.col.colDef.editDropdownValueLabel;
                    for (var i = 0; i < map.length; i++) {
                        if (map[i][idField] == input) {
                            return map[i][valueField];
                        }
                    }
                }
            }
            return '';
        };
    });