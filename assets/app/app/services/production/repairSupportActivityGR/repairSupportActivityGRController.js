angular.module('app')
  .controller('RepairSupportActivityGRController', function($scope, $http, CurrentUser,
    RepairSupportActivityGR, GeneralParameter, RepairProcessGR, AppointmentGrService, ngDialog, $timeout, bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------

    $scope.$on('$viewContentLoaded', function() {
      $scope.loading = true;
      $scope.gridData = [];
      $scope.mData.Status = 0;
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mData = {}; //Model
    $scope.xRole = {
      selected: []
    };
    $scope.showme = false;
    $scope.showme2 = false;
    $scope.isGridDisabled = true;
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.ismeridian = false;
    $scope.hstep = 1;
    $scope.mstep = 1;
    $scope.ShowWo = false;

    var dateFormat = 'dd/MM/yyyy';
    var dateFilter = 'date:"dd/MM/yyyy"';
    $scope.DateOptions = {
      startingDay: 1,
      format: dateFormat,
      //disableWeekend: 1
    };

    $scope.FIR = [{
        Id: 1,
        FIR: "Ya"
      },
      {
        Id: 0,
        FIR: "Tidak"
      }
    ];

    $scope.Status = [{
        StatusId: 0,
        Statustext: "Unsolved"
      },
      {
        StatusId: 1,
        Statustext: "Solved"
      }
    ];
    var Parts = [];
    $scope.dataForBslist = function(data) {
      Parts.splice();
      Parts = [];
      if (data.JobTask.length > 0) {
          var tmpJob = data.JobTask;
          $scope.getAvailablePartsServiceManual(tmpJob, 0, 0);
      };
  };
    $scope.ClickWO = function(value, JobID) {
      $scope.gridWork = [];
      if (value == 1) {
        //console.log("jobid", JobID);
        $scope.ShowWo = true;
        $scope.showme = true;
        RepairProcessGR.getDataDetail(JobID)
          .then(

            function(res) {
              $scope.DataWo = res.data.Result;

              for (i = 0; i < $scope.DataWo.length; i++) {
                $scope.dataForBslist($scope.DataWo[i]);
                // for(var z in $scope.DataWo[i].JobTask){
                //   _.map($scope.DataWo[i].JobTask[z].JobParts,function(val){
                //     if(val.Part !== undefined && val.Part !== null){
                //       val.PartsCode = val.Part.PartsCode;
                //       if(val.PartsId !== null && val.PartsId !== undefined){
                //           AppointmentGrService.getAvailableParts(val.PartsId,0,val.Qty).then(function(res) {
                //             var tmpPart = res.data.Result[0];
                //             var tmpRes = {}
                //             if(tmpPart.isAvailable == 0){
                //               tmpPart.Availbility = "Tidak Tersedia";
                //             }else{
                //               tmpPart.Availbility = "Tersedia";
                //             }
                //             tmpRes.isAvailable = tmpPart.isAvailable;
                //             tmpRes.Availbility = tmpPart.Availbility;
                //             // $scope.DataParts.push(item[x]);
                //             // $scope.getAvailablePartsService(item,x+1);
                //             return tmpRes
                //           });
                //       }
                //     }else{
                //       val.PartsCode = "-"
                //     }
                //   })
                // }
                $scope.DataWOTask = $scope.DataWo[i].JobTask;

                for (a = 0; a < $scope.DataWOTask.length; a++) {
                  $scope.DataMaterial = $scope.DataWOTask[i].JobParts;
                  //console.log("data wo parts", $scope.DataMaterial);
                }
                //console.log("Data wo task", $scope.DataWOTask);
              }
              //console.log("data Wo Detail", $scope.DataWo);
            },
            function(err) {
              console.log("err=>", err);
            }
          );
      } else {
        $scope.showme = false;
        $scope.ShowWo = false;
      }
    }

    $scope.Kembali = function() {
      $scope.showme = false;
      $scope.ShowWo = false;
    }

    $scope.close = function(Text) {
      $scope.showme = false;
      $scope.showme2 = false;
      $scope.mData = {};
    }

    var gridData = [];
    $scope.getData = function() {
      //console.log("status", status);
      var startDate = $scope.filterData.StartDate;
      var stopDate = $scope.filterData.StopDate;
      var status = $scope.filterData.Status;
      var NoWo = $scope.filterData.KodeWO;


      var fNoWo = "-";
      if (NoWo == undefined || NoWo == "" || NoWo == null) {
        fNoWo = "-"
      } else {
        fNoWo = NoWo
      };

      var a = new Date(startDate);
      var yearFirst = a.getFullYear();
      var monthFirst = a.getMonth() + 1;
      var dayFirst = a.getDate();
      var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
      //console.log("first date", firstDate)

      var b = new Date(stopDate);
      var yearLast = b.getFullYear();
      var monthLast = b.getMonth() + 1;
      var dayLast = b.getDate();
      var lastDate = yearLast + '-' + monthLast + '-' + dayLast;
      //console.log("stop date", lastDate);

      if (status == undefined || firstDate == 'NaN-NaN-NaN' || lastDate == 'NaN-NaN-NaN') {
        bsNotify.show({
          size: 'big',
          type: 'danger',
          title: "Mohon Input Filter",
          // content: error.join('<br>'),
          // number: error.length
        });
      } else {
        fNoWo = fNoWo.replace(/\//g,' ');
        var XfNoWo = encodeURIComponent(fNoWo);
        RepairSupportActivityGR.getData(firstDate, lastDate, status, XfNoWo)
          .then(

            function(res) {
              $scope.xData = res.data;
              //foreach xdata cari employee name nya 
              $scope.xData.forEach(function(element) {
                //console.log(element);
                
              RepairProcessGR.getDataDetail(element.JobTask.Job.JobId)
              .then(function(res){
                //var all = $scope.xData.filter(x=> x.JobTask.Job.JobId ==  res.data.Result[0].JobId  ) ;
                $scope.filterSearch1 = res.data.Result[0].JobId;
                function checkJobId(data)
                {
                    return data.JobTask.Job.JobId ==$scope.filterSearch1;
                }
                var all = $scope.xData.filter(checkJobId);
                  
                all.forEach(function(element) {
                      element.JobTask.Job.UserSa = res.data.Result[0].UserSa  ; 
                  });
                  
                },
                function(err)
                {
                  console.log("err=>", err);
                }


              );
              });
              console.log("data inti",$scope.xData);
              if ($scope.xData.length == 0){
                bsNotify.show({
                  size: 'big',
                  type: 'danger',
                  title: "Data Tidak Ditemukan",
                  timeout: 3000
                });
              }
            },
            function(err) {
              console.log("err=>", err);
            }
          );
      }
    }

    function roleFlattenAndSetLevel(node, lvl) {
      for (var i = 0; i < node.length; i++) {
        node[i].$$treeLevel = lvl;
        gridData.push(node[i]);
        if (node[i].child.length > 0) {
          roleFlattenAndSetLevel(node[i].child, lvl + 1)
        } else {

        }
      }
      return gridData;
    }
    $scope.selectRole = function(rows) {
      //console.log("onSelectRows=>", rows);
      $timeout(function() {
        $scope.$broadcast('show-errors-check-validity');
      });
    }
    $scope.onSelectRows = function(rows) {
        //console.log("onSelectRows=>", rows);
      }
      //----------------------------------
      // Grid Setup
      //----------------------------------

    function UpdateTfirstLocal(Model) {
      console.log("ari sudah lelah", Model);
      RepairSupportActivityGR.putRSA(Model).then(

        function(res) {
          //console.log("hasil simpan",res);
          $scope.getData();
          $scope.mData = {};
        },
        function(err) {
          console.log("err=>", err);
        }
      );
      $scope.showme = false;
      $scope.showme2 = false;

    }

    $scope.simpanRSA = function(Model) {
      console.log("mData",Model);
      Model["rsaDocNo"] = "";
      Model["dccDocNo"] = "";
      Model["ltDocNo"] = "";
      //console.log("model dikirim : ", Model);
      //console.log("mdata", $scope.mData);

      RepairSupportActivityGR.postRSATfirst(Model).then(function(res) {
          console.log("test liad rsa", res.data);
          if (res.data.ResponseCode == 100){
            var pesan = res.data.ResponseMessage.split('#')
            bsNotify.show({
              size: 'big',
              type: 'danger',
              title: pesan[1],
            });
          } else {
            //console.log("test liad rsa", JSON.parse(res.data.msg));
            var resParsed = JSON.parse(res.data.msg);
            if (Array.isArray(resParsed)) {
              bsNotify.show({
                size: 'big',
                type: 'danger',
                title: resParsed[0].message,
                // content: error.join('<br>'),
                // number: error.length
              });
            } else {
              if (resParsed.success) {
                Model["rsaDocNo"] = res.data.noDoc;

                if (Model.BentukLaporan == 1) {
                  RepairSupportActivityGR.postDCCTfirst(Model).then(function(res) {
                      console.log("test liad dcc", res.data);

                      var resParsed = JSON.parse(res.data.msg);
                      if (Array.isArray(resParsed)) {
                        bsNotify.show({
                          size: 'big',
                          type: 'danger',
                          title: resParsed[0].message,
                          // content: error.join('<br>'),
                          // number: error.length
                        });
                      } else {

                        if (resParsed.success) {
                          Model["dccDocNo"] = res.data.noDoc;

                          UpdateTfirstLocal(Model);
                        }

                      }
                    },
                    function(err) {
                      console.log("err=>", err);
                    }
                  );
                } else if (Model.BentukLaporan == 0) {
                  RepairSupportActivityGR.postLTETfirst(Model).then(function(res) {
                      console.log("test liad lte", res.data);

                      var resParsed = JSON.parse(res.data.msg);
                      if (Array.isArray(resParsed)) {
                        bsNotify.show({
                          size: 'big',
                          type: 'danger',
                          title: resParsed[0].message,
                          // content: error.join('<br>'),
                          // number: error.length
                        });
                      } else {

                        if (resParsed.success) {
                          Model["ltDocNo"] = res.data.noDoc;

                          UpdateTfirstLocal(Model);
                        }

                      }
                    },
                    function(err) {
                      console.log("err=>", err);
                    }
                  );
                } else {

                  UpdateTfirstLocal(Model);
                }

              }

            }

          }
          

        },
        function(err) {
          console.log("err=>", err);
        }
      );
    }
    $scope.choseData = {};
    // ,IdJobTaskProgress
    $scope.checkRequired = function (param) {
      console.log('km',param, $scope.mData.CategorySubProblem , $scope.mData.BentukLaporan)
      if (param == 0) {
        $scope.mData.CategorySubProblem = null;
        $scope.mData.ConsultationDCCCatg = null;
        $('div[name="slctCategorySubProblem"]').removeClass("ng-dirty");
      }
    }
    $scope.getDataProblemList = function(Id, data, parts, ResponseDesc, TroubleDesc, objX, JobId) {
      $scope.DataTempParts = parts;
      $scope.DataParts =[];
      for(var x = 0 ; x< $scope.DataTempParts.length ; x++ ){
        var item = $scope.DataTempParts;
        AppointmentGrService.getAvailableParts(item[x].PartsId,0,item[x].Qty)
        .then(function(res) {
          var tmpPart = res.data.Result[0];
          if(tmpPart.isAvailable == 0){
            tmpPart.Availbility = "Tidak Tersedia";
          }else{
            tmpPart.Availbility = "Tersedia";
          }
          $scope.tempFilter2 = tmpPart.PartsId;
          function checkFilterPart(data)
          {
            return data.PartsId == $scope.tempFilter2;
          }
          var result = $scope.DataTempParts.filter(checkFilterPart)[0]; 
          result.isAvailable = tmpPart.isAvailable;
          result.Availbility = tmpPart.Availbility;
          $scope.DataParts.push(result);
        
        });
      } 
      console.log("yang dipassing", objX);
      console.log("des res", ResponseDesc);
      console.log("des mas", TroubleDesc)
      $scope.showme = true;
      $scope.showme2 = true;
      $scope.DataProblemList = Id;
      console.log("DataProblemList", $scope.DataProblemList);
      $scope.choseData = data;
      $scope.chosedatafinal = objX;
      $scope.statusFilter = objX.IsSolvedProblem;
      console.log("status filter", $scope.statusFilter);
      console.log("chosedata", $scope.choseData);
      RepairProcessGR.getTeknisiTask($scope.choseData.JobTaskId).then(
        function(res) {
          console.log('nama teknisi',res.data)
          $scope.NamaTeknisi = '';
            for (var i=0; i<res.data.length; i++){
              $scope.NamaTeknisi =  $scope.NamaTeknisi + res.data[i].EmployeeName + ', ';
            }
            $scope.NamaTeknisi = $scope.NamaTeknisi.substring(0, $scope.NamaTeknisi.length-2);
            console.log('scope.nama teknisi', $scope.NamaTeknisi)
            // $scope.NamaTeknisi = res.data[0].EmployeeName;
        },
        function(err) {
        }
    );
      $scope.getJob();
      // $scope.getParts(parts);
      $scope.getMarket();
      $scope.getTransmision();
      $scope.getTechnicalRank();
      $scope.getFailPart();
      $scope.getEngine();
      $scope.getKategoriKomponen();
      $scope.getKomponen();
      $scope.getKategoriProblem();
      $scope.getSubKategoriProblem();
      $scope.getT1();
      $scope.getT2();
      $scope.getDataCompanyGroupDealer();

      // $scope.mData.NoLTIfRepeatRepair = parseInt(objX.JobTask.JobTaskProgress[0].NoLTIfRepeatRepair);
      // $scope.mData.NoLTIfInvolvedPBT = parseInt(objX.JobTask.JobTaskProgress[0].NoLTIfInvolvedPBT);
      objX.TfirstKode_T1__c = objX.TfirstKode_T1__c == null ? objX.JobTask.TFirst1 : objX.TfirstKode_T1__c;
      objX.TfirstKode_T2__c = objX.TfirstKode_T2__c == null ? objX.JobTask.TFirst2 : objX.TfirstKode_T2__c;
      $scope.mData.CatgComponent = objX.CatgComponent;
      $scope.mData.SubCatgComponent = objX.Component;
      $scope.mData.T1 = parseInt(objX.TfirstKode_T1__c);
      $scope.mData.ConditionT1 = objX.TfirstPemeriksaan_dan_Langkah_Investigasi__c;
      $scope.mData.T2 = parseInt(objX.TfirstKode_T2__c);
      $scope.mData.CausedT2 = objX.TfirstPenyebab_Masalah__c;
      $scope.mData.MarketInpactRank = objX.MarketInpactRank;
      $scope.mData.TechnicalRank = objX.TechnicalRank;
      $scope.mData.TransmissionType = objX.TransmissionType;
      $scope.mData.EngineType = parseInt(objX.TfirstTipe_Mesin__c);
      $scope.mData.StatusFailedPart = objX.StatusFailedPart;
      $scope.mData.PartDelivery = objX.PartDelivery;
      $scope.mData.PartDeliveryAvailable = objX.TfirstPengadaan_Part_Tanggal_Pesan_Tersedia__c;
      $scope.mData.DateofFirstComplain = objX.TfirstTanggal_Komplain_Pertama__c;
      $scope.mData.NoLTIfRepeatRepair = objX.NoLTIfRepeatRepair;
      $scope.mData.NoLTIfInvolvedPBT = objX.NoLTIfInvolvedPBT;
      $scope.mData.ProblemCanBeDuplicate = objX.ProblemCanBeDuplicate;
      $scope.mData.NumberOfCases = parseInt(objX.NumberOfCases);
      $scope.mData.InquiryTitle = objX.InquiryTitle;
      $scope.mData.MethodToAct = objX.MethodToAct;
      $scope.mData.ResultAction = objX.ResultAction;
      $scope.mData.RequestToDCC = objX.RequestToDCC;
      $scope.mData.CarUser = objX.TfirstPenggunaan_Kendaraan__c;
      $scope.mData.IsSolvedProblem = objX.IsSolvedProblem;
      $scope.mData.IsEscalationProblem = objX.IsEscalationProblem;
      $scope.mData.BentukLaporan = objX.TfirstDCCorLT;
      $scope.mData.ConsultationDCCCatg = objX.ConsultationDCCCatg;
      $scope.mData.CategorySubProblem = parseInt(objX.TfirstSub_Kategori_Problem__c);
      $scope.mData.IsFIR = objX.IsFIR;

      $scope.mData.TroubleDesc = TroubleDesc;
      $scope.mData.DeskripsiRespon = ResponseDesc;

      AppointmentGrService.getDataVehiclePSFU(objX.JobTask.Job.VehicleId)
        .then(

          function(res) {
            //console.log("crm data", res);
            var hasil = res.data.Result[0];
            $scope.mData["EngineNo"] = hasil.EngineNo;
            $scope.mData["VIN"] = hasil.VIN;
            $scope.mData["DECDate"] = hasil.DECDate;
            $scope.mData["PLOD"] = hasil.PLOD;
            $scope.mData["AssemblyYear"] = hasil.AssemblyYear;
            $scope.mData["VehicleModelCode"] = hasil.MVehicleTypeColor.MVehicleType.MVehicleModel.VehicleModelCode;

          },
          function(err) {
            console.log("err=>", err);
          }
        );
        


        RepairProcessGR.getDataDetail(JobId)
          .then(function(res) {
              //console.log("crm data", res);

              // for (i = 0; i < $scope.DataWo.length; i++) {
              //   $scope.DataWOTask = $scope.DataWo[i].JobTask;

              //   for (a = 0; a < $scope.DataWOTask.length; a++) {
              //     $scope.DataMaterial = $scope.DataWOTask[i].JobParts;
              //     //console.log("data wo parts", $scope.DataMaterial);
              //   }
              //   //console.log("Data wo task", $scope.DataWOTask);
              // }
              
              
              // for(var i = 0; i < res.data.Result.length; i++){
              //   for(var a = 0; a < res.data.Result[i].JobTask.length; a++){
              //     // for(var b =0; b < res.data.Result[i].JobTask[a].JobParts.length; b++){
              //       // $scope.DataParts = res.data.Result[i].JobTask[a].JobParts;
              //       $scope.DataParts=[];
              //       $scope.getAvailablePartsService(res.data.Result[i].JobTask[a].JobParts,0)
              //       // console.log("data parts",$scope.DataParts);
              //     // }
              //   }
              // }

            },
            function(err) {
              console.log("err=>", err);
            }
          );

      $scope.mData["IdJobTaskProgress"] = objX.Id;
      $scope.mData["KatashikiCode"] = objX.JobTask.Job.KatashikiCode;
      $scope.mData["PoliceNumber"] = objX.JobTask.Job.PoliceNumber;
      $scope.mData["Km"] = objX.JobTask.Job.Km;
      $scope.mData["ProblemFoundedDate"] = objX.ProblemFoundedDate;
      $scope.mData["isRTJ"] = objX.JobTask.JobTypeId == 58 ? "Ya" : "Tidak";
      $scope.mData["GateInPushTime"] = objX.JobTask.Job.GateInPushTime;
      $scope.mData["PlanDateFinish"] = objX.JobTask.Job.PlanDateFinish;
      $scope.mData["DateStart"] = objX.DateStart;


      if (Array.isArray(objX.JobTask.Job)) {
        if (objX.JobTask.Job.length > 0) {
          var stringJobComplaint = "<ul>";
          objX.JobTask.Job.forEach(function(element) {
            //console.log(element);
            stringJobComplaint = stringJobComplaint + "<li>" + element + "</li>";
          });
          stringJobComplaint = stringJobComplaint + "</ul>";
          $scope.mData["JobComplaint"] = stringJobComplaint;

        } else {
          $scope.mData["JobComplaint"] = "";
        }
      } else {
        $scope.mData["JobComplaint"] = "";
      }

      if (Array.isArray(objX.JobTask.JobParts)) {
        if (objX.JobTask.JobParts.length > 0) {
          var OFPCode = "";
          for (var i = 0; i < objX.JobTask.JobParts.length; i++) {
            if (objX.JobTask.JobParts[i].IsOfp) {
              OFPCode = objX.JobTask.JobParts[i].Part.PartsCode;
              break;
            }
          };

          $scope.mData["OFP"] = OFPCode;

        } else {
          $scope.mData["OFP"] = "";
        }
      } else {
        $scope.mData["OFP"] = "";
      }

    };



    /////////////////////////////////////////////
    $scope.getAvailablePartsServiceManual = function(item, x, y) {
      console.log("getAvailablePartsServiceManual", item);
      console.log("x : ", x);
      console.log("y : ", y);
      // $scope.mData.MProfileTechnicianId.EmployeeName = item.JobTaskTechnician.MProfileTechnicianId.EmployeeName;

      if (x > item.length - 1) {
          return item;
      }
      if (item[x].isDeleted !== 1) {
          console.log("item[x]", item[x]);

          if (item[x].JobParts[y] !== undefined && item[x].JobParts[y].PartsId !== null) {
              var itemTemp = item[x].JobParts[y];
              AppointmentGrService.getAvailableParts(itemTemp.PartsId, item[x].JobId).then(function(res) {
                  var tmpParts = res.data.Result[0];

                  if (tmpParts !== null && tmpParts !== undefined) {
                      // console.log("have data RetailPrice",item,tmpRes);
                      item[x].JobParts[y].RetailPrice = tmpParts.RetailPrice;
                      // item[x].JobParts[y].minimalDp = tmpParts.PriceDP;
                      item[x].JobParts[y].nightmareIjal = item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty;
                      item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                      item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                      // item.DiscountedPrice =  (normalPrice * item.Discount)/100;
                      if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                          item[x].JobParts[y].typeDiskon = -1;
                          item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                      } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                          item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                          // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                          item[x].JobParts[y].typeDiskon = 0;
                      }
                      if (tmpParts.isAvailable == 0) {
                          item[x].JobParts[y].Availbility = "Tidak Tersedia";
                          if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                              item[x].JobParts[y].ETA = "";
                          } else {
                              item[x].JobParts[y].ETA = tmpParts.DefaultETA;
                          }
                          item[x].JobParts[y].Type = 3;
                      } else {
                          item[x].JobParts[y].Availbility = "Tersedia";
                      }
                  } else {
                      console.log("haven't data RetailPrice");
                      item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                      item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                      item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                      item[x].JobParts[y].nightmareIjal = Math.round(item[x].JobParts[y].minimalDp / item[x].JobParts[y].Qty);
                      if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                          item[x].JobParts[y].typeDiskon = -1;
                          item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                      } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                          // item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                          item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].subTotal - ((item[x].JobParts[y].subTotal * item[x].JobParts[y].Discount) / 100);
                          item[x].JobParts[y].typeDiskon = 0;
                      }
                  }
                  if (item[x].JobParts[y].Part !== null && item[x].JobParts[y].Part !== undefined) {
                      item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                      delete item[x].JobParts[y].Part;
                  }
                  if (item[x].JobParts[y].PaidBy != undefined && item[x].JobParts[y].PaidBy != null) {
                      item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy;
                  }
                  // if (item[x].JobParts[y].PaidBy !== null) {
                  //     item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy.Name;
                  //     // delete item[x].JobParts[y].PaidBy;
                  // };
                  if (item[x].JobParts[y].IsOPB !== null) {
                      item[x].JobParts[y].isOPB = item[x].JobParts[y].IsOPB;
                  }
                  Parts.push(item[x].JobParts[y]);
                  // return item;

                  if (x <= (item.length - 1)) {
                      if (y >= (item[x].JobParts.length - 1)) {
                          if (item[x].Fare !== undefined) {
                              var sum = item[x].Fare;
                              var summ = item[x].FlatRate;
                              var summaryy = sum * summ;
                              var discountedPrice = (summaryy * item[x].Discount) / 100;
                              var normalPrice = summaryy
                              summaryy = parseInt(summaryy);
                          }
                          if (item[x].JobType == null) {
                              item[x].JobType = { Name: "" };
                          }
                          if (item[x].PaidBy == null) {
                              item[x].PaidBy = { Name: "" }
                          }
                          if (item[x].AdditionalTaskId == null) {
                              item[x].isOpe = 0;
                          } else {
                              item[x].isOpe = 1;
                          }
                          if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                              item[x].typeDiskon = -1;
                              item[x].DiscountedPrice = normalPrice;
                          } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                              item[x].typeDiskon = 0;
                              // item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                              item[x].DiscountedPrice = normalPrice - ((normalPrice * item[x].Discount) / 100);
                          }
                          $scope.gridWork.push({
                              // gridTemp.push({
                              ActualRate: item[x].ActualRate,
                              AdditionalTaskId: item[x].AdditionalTaskId,
                              isOpe: item[x].isOpe,
                              Discount: item[x].Discount,
                              JobTaskId: item[x].JobTaskId,
                              JobTypeId: item[x].JobTypeId,
                              DiscountedPrice: item[x].DiscountedPrice,
                              typeDiskon: item[x].typeDiskon,
                              NormalPrice: normalPrice,
                              catName: item[x].JobType.Name,
                              Fare: item[x].Fare,
                              IsCustomDiscount: item[x].IsCustomDiscount,
                              Summary: summaryy,
                              PaidById: item[x].PaidById,
                              JobId: item[x].JobId,
                              TaskId: item[x].TaskId,
                              TaskName: item[x].TaskName,
                              FlatRate: item[x].FlatRate,
                              ProcessId: item[x].ProcessId,
                              TFirst1: item[x].TFirst1,
                              PaidBy: item[x].PaidBy.Name,
                              index: "$$" + x,
                              child: Parts
                          });
      
                          Parts.splice();
                          Parts = [];
                          console.log("if 1 nilai x", x);
                          $scope.getAvailablePartsServiceManual(item, x + 1, 0);

                      } else {
                          $scope.getAvailablePartsServiceManual(item, x, y + 1);
                      }
                  }
              });
          } else if (item[x].JobParts[y] !== undefined && (item[x].JobParts[y].PartsId == undefined || item[x].JobParts[y].PartsId == null)) {
              item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
              item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
              item[x].JobParts[y].DownPayment = item[x].JobParts[y].DownPayment;
              if (item[x].JobParts[y].PaidBy != undefined || item[x].JobParts[y].PaidBy != null) {
                  item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy;
              }
              if (item[x].JobParts[y].Part !== null && item[x].JobParts[y].Part !== undefined) {
                  item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                  delete item[x].JobParts[y].Part;
              }
              // if (item[x].JobParts[y].PaidBy !== null) {
              //     item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy.Name;
              //     // delete item[x].JobParts[y].PaidBy;
              // };
              tmp = item[x].JobParts[y];
              if (item[x].JobParts[y].IsOPB !== null) {
                  item[x].JobParts[y].isOPB = item[x].JobParts[y].IsOPB;
              }
              Parts.push(item[x].JobParts[y]);

              if (x <= (item.length - 1)) {
                  if (y >= (item[x].JobParts.length - 1)) {
                      if (item[x].Fare !== undefined) {
                          var sum = item[x].Fare;
                          var summ = item[x].FlatRate;
                          var summaryy = sum * summ;
                          var discountedPrice = (summaryy * item[x].Discount) / 100;
                          var normalPrice = summaryy
                          summaryy = parseInt(summaryy);
                      }
                      if (item[x].JobType == null) {
                          item[x].JobType = { Name: "" };
                      }
                      if (item[x].PaidBy == null) {
                          item[x].PaidBy = { Name: "" }
                      }
                      if (item[x].AdditionalTaskId == null) {
                          item[x].isOpe = 0;
                      } else {
                          item[x].isOpe = 1;
                      }
                      if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                          item[x].typeDiskon = -1;
                          item[x].DiscountedPrice = normalPrice;
                      } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                          item[x].typeDiskon = 0;
                          // item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                          item[x].DiscountedPrice = normalPrice - ((normalPrice * item[x].Discount) / 100);
                      }

                      // gridTemp.push({
                      $scope.gridWork.push({
                          ActualRate: item[x].ActualRate,
                          AdditionalTaskId: item[x].AdditionalTaskId,
                          isOpe: item[x].isOpe,
                          Discount: item[x].Discount,
                          JobTaskId: item[x].JobTaskId,
                          JobTypeId: item[x].JobTypeId,
                          DiscountedPrice: item[x].DiscountedPrice,
                          typeDiskon: item[x].typeDiskon,
                          NormalPrice: normalPrice,
                          catName: item[x].JobType.Name,
                          Fare: item[x].Fare,
                          IsCustomDiscount: item[x].IsCustomDiscount,
                          Summary: summaryy,
                          PaidById: item[x].PaidById,
                          JobId: item[x].JobId,
                          TaskId: item[x].TaskId,
                          TaskName: item[x].TaskName,
                          FlatRate: item[x].FlatRate,
                          ProcessId: item[x].ProcessId,
                          TFirst1: item[x].TFirst1,
                          PaidBy: item[x].PaidBy.Name,
                          index: "$$" + x,
                          child: Parts
                      });
  
                      Parts.splice();
                      Parts = [];
                      $scope.getAvailablePartsServiceManual(item, x + 1, 0);
                      console.log("if 2 nilai x", x);
                  } else {
                      $scope.getAvailablePartsServiceManual(item, x, y + 1);
                  }
              }
          } else if (item[x].JobParts.length == 0) {
              if (x <= (item.length - 1)) {
                  if (y >= (item[x].JobParts.length - 1)) {
                      if (item[x].Fare !== undefined) {
                          var sum = item[x].Fare;
                          var summ = item[x].FlatRate;
                          var summaryy = sum * summ;
                          var discountedPrice = (summaryy * item[x].Discount) / 100;
                          var normalPrice = summaryy
                          summaryy = parseInt(summaryy);
                      }
                      if (item[x].JobType == null) {
                          item[x].JobType = { Name: "" };
                      }
                      if (item[x].PaidBy == null) {
                          item[x].PaidBy = { Name: "" }
                      }
                      if (item[x].AdditionalTaskId == null) {
                          item[x].isOpe = 0;
                      } else {
                          item[x].isOpe = 1;
                      }
                      if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                          item[x].typeDiskon = -1;
                          item[x].DiscountedPrice = normalPrice;
                      } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                          item[x].typeDiskon = 0;
                          // item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                          item[x].DiscountedPrice = normalPrice - ((normalPrice * item[x].Discount) / 100);

                      }
                      // gridTemp.push({
                      $scope.gridWork.push({
                          ActualRate: item[x].ActualRate,
                          AdditionalTaskId: item[x].AdditionalTaskId,
                          isOpe: item[x].isOpe,
                          Discount: item[x].Discount,
                          JobTaskId: item[x].JobTaskId,
                          JobTypeId: item[x].JobTypeId,
                          DiscountedPrice: item[x].DiscountedPrice,
                          typeDiskon: item[x].typeDiskon,
                          NormalPrice: normalPrice,
                          catName: item[x].JobType.Name,
                          Fare: item[x].Fare,
                          IsCustomDiscount: item[x].IsCustomDiscount,
                          Summary: summaryy,
                          PaidById: item[x].PaidById,
                          JobId: item[x].JobId,
                          TaskId: item[x].TaskId,
                          TaskName: item[x].TaskName,
                          FlatRate: item[x].FlatRate,
                          ProcessId: item[x].ProcessId,
                          TFirst1: item[x].TFirst1,
                          PaidBy: item[x].PaidBy.Name,
                          index: "$$" + x,
                          child: Parts
                      });
  
                      Parts.splice();
                      Parts = [];
                      $scope.getAvailablePartsServiceManual(item, x + 1, 0);
                  } else {
                      $scope.getAvailablePartsServiceManual(item, x, y + 1);
                  };
              };
              // return item;
          };

          if (x > item.length - 1) {
              return item;
          };
      } else {
          $scope.getAvailablePartsServiceManual(item, x + 1, 0);
      }
  };
    $scope.getAvailablePartsService = function(item,x){
      if (x > item.length - 1) {
          return item;
      }
      console.log("itemmmm",item)
      if(item[x].PartsId !== null && item[x].PartsId !== undefined ){
        AppointmentGrService.getAvailableParts(item[x].PartsId,0,item[x].Qty).then(function(res) {
              var tmpPart = res.data.Result[0];
              if(tmpPart.isAvailable == 0){
                tmpPart.Availbility = "Tidak Tersedia";
              }else{
                tmpPart.Availbility = "Tersedia";
              }
              item[x].isAvailable = tmpPart.isAvailable;
              item[x].Availbility = tmpPart.Availbility;
              $scope.DataParts.push(item[x]);
              $scope.getAvailablePartsService(item,x+1);
        });
      }
      console.log("$scope.DataParts",$scope.DataParts);
    }
    $scope.getDataCompanyGroupDealer = function() {
      GeneralParameter.getDataCompanyGroupDealer()
        .then(

          function(res) {
            $scope.mData["GroupDealerCode"] = res.data[0].GroupDealerCode;
            //console.log("group dealer", res.data[0].GroupDealerCode);
          },
          function(err) {
            console.log("err=>", err);
          }
        );
    }

    $scope.getKategoriProblem = function() {
      RepairSupportActivityGR.getSubKategoriProblem()
        .then(

          function(res) {
            $scope.KategoriProblem = res.data.Result;
            //console.log("KategoriKomponen", $scope.KategoriKomponen);
          },
          function(err) {
            console.log("err=>", err);
          }
        );
    }

    $scope.getSubKategoriProblem = function() {
      RepairSupportActivityGR.getSubKategoriProblem()
        .then(

          function(res) {
            //console.log("res ", res);
            $scope.SubKategoriProblem = res.data.Result;
            //console.log("KategoriKomponen", $scope.KategoriKomponen);
          },
          function(err) {
            console.log("err=>", err);
          }
        );
    }

    $scope.getKategoriKomponen = function() {
      RepairSupportActivityGR.getKategoriKomponen()
        .then(

          function(res) {
            $scope.KategoriKomponen = res.data.Result;
            console.log("KategoriKomponen", $scope.KategoriKomponen);
          },
          function(err) {
            console.log("err=>", err);
          }
        );
    }

    $scope.getKomponen = function() {
      RepairSupportActivityGR.getKomponen()
        .then(

          function(res) {
            $scope.Komponen = res.data.Result;
            console.log("Komponen", $scope.Komponen);
          },
          function(err) {
            console.log("err=>", err);
          }
        );
    }

    $scope.getT1 = function() {
      RepairSupportActivityGR.getT1()
        .then(

          function(res) {
            _.map(res.data.Result,function(val){
              val.Id = val.T1Id
            })
            $scope.T1 = res.data.Result;
            // console.log("T1 ===>", $scope.T1);
            for (var i = 0; i < res.data.Result.length; i++) {
              $scope.T1[i].xName = $scope.T1[i].T1Code + ' - ' + $scope.T1[i].Name;
            }
          },
          function(err) {
            console.log("err=>", err);
          }
        );
    }

    $scope.getT2 = function() {
      RepairSupportActivityGR.getT2()
        .then(

          function(res) {
            $scope.T2 = res.data.Result;
            //console.log("T2", $scope.T2);
            for (var i = 0; i < res.data.Result.length; i++) {
              $scope.T2[i].xName = $scope.T2[i].T2Code + ' - ' + $scope.T2[i].Name;
            }
          },
          function(err) {
            console.log("err=>", err);
          }
        );
    }

    $scope.getMarket = function() {

      // document.write(today);
      // $scope.PauseResaon = [];
      RepairSupportActivityGR.getMarketImpact()
        .then(

          function(res) {
            $scope.MarketImpact = res.data.Result;
            console.log("MarketImpact", $scope.MarketImpact);
          },
          function(err) {
            console.log("err=>", err);
          }
        );

      // var newDataWoList = angular.copy($scope.DataWoList);

    }

    $scope.getTechnicalRank = function() {

      // document.write(today);
      // $scope.PauseResaon = [];
      RepairSupportActivityGR.getTechnicalRank()
        .then(

          function(res) {
            $scope.TechnicalRank = res.data.Result;
            console.log("TechnicalRank", $scope.TechnicalRank);
          },
          function(err) {
            console.log("err=>", err);
          }
        );

      // var newDataWoList = angular.copy($scope.DataWoList);

    }

    $scope.getTransmision = function() {

      // document.write(today);
      // $scope.PauseResaon = [];
      RepairSupportActivityGR.getTransmisionType()
        .then(

          function(res) {
            $scope.TransmissionType = res.data.Result;
            //console.log("TransmissionType", $scope.TransmissionType);
          },
          function(err) {
            console.log("err=>", err);
          }
        );

      // var newDataWoList = angular.copy($scope.DataWoList);

    }

    $scope.getEngine = function() {

      // document.write(today);
      // $scope.PauseResaon = [];
      RepairSupportActivityGR.getEngineType()
        .then(

          function(res) {
            $scope.EngineType = res.data.Result;
            console.log("EngineType", $scope.EngineType);
          },
          function(err) {
            console.log("err=>", err);
          }
        );

      // var newDataWoList = angular.copy($scope.DataWoList);

    }

    $scope.getFailPart = function() {

      // document.write(today);
      // $scope.PauseResaon = [];
      RepairSupportActivityGR.getFailPart()
        .then(

          function(res) {
            $scope.StatusFailed = res.data.Result;
            //console.log("Fail", $scope.StatusFailed);
          },
          function(err) {
            console.log("err=>", err);
          }
        );

      // var newDataWoList = angular.copy($scope.DataWoList);

    }

    $scope.getJob = function() {
      var fDataJob = [];
      fDataJob.push($scope.choseData);
      //console.log("FdataJob", fDataJob);
      $scope.gridJob.data = fDataJob;
    }

    $scope.gridJob = {
      enableFiltering: false,
      enableSorting: false,
      multiSelect: false,
      enableRowSelection: false,
      enableSelectAll: false,
      // selectionRowHeaderWidth: 35,
      // rowHeight: 35,
      // data: $scope.choseData,
      showGridFooter: true,
      columnDefs: [{
          field: 'JobTaskId',
          name: 'JobTaskId',
          visible: false
        },
        {
          field: 'TaskName',
          name: 'Nama Pekerjaan',
          cellEditableCondition: false
        }
      ]
    };

    // $scope.getParts = function(parts) {
    //   //console.log("kakak", parts);
    //   if (typeof parts != undefined) {
    //     $scope.gridParts.data = parts;
    //   }
    // }

    $scope.gridWODetail = {
      enableFiltering: true,
      enableSorting: true,
      multiSelect: true,
      enableRowSelection: true,
      enableSelectAll: true,
      // selectionRowHeaderWidth: 35,
      // rowHeight: 35,
      data: 'DataMaterial',
      showGridFooter: true,
      columnDefs: [{
          field: 'ModelCode',
          name: 'Nomor Material'
        },
        {
          field: 'PartsName',
          name: 'Nama Material'
        },
        {
          field: 'Status',
          name: 'Status'
        },
        {
          field: 'ETA',
          name: 'ETA'
        },
        {
          field: 'Qty',
          name: 'Permintaan QTY'
        },
        {
          field: 'QtyGI',
          name: 'Isu QTY'
        },
        {
          field: 'Satuan.Name',
          name: 'Satuan'
        }
      ]
    };

    $scope.gridParts = {
      enableFiltering: true,
      enableSorting: true,
      multiSelect: true,
      enableRowSelection: true,
      enableSelectAll: true,
      // selectionRowHeaderWidth: 35,
      // rowHeight: 35,
      data :'DataParts',
      showGridFooter: true,
      columnDefs: [
        // { field:'PartsCode', name:'Kode Parts'},
        {
          field: 'PartsName',
          name: 'Nama Parts'
        },
        {
          field: 'Qty',
          name: 'QTY'
        },
        {
          field: 'Availbility',
          name: 'Ketersediaan'
        }
      ]
    };

  });
