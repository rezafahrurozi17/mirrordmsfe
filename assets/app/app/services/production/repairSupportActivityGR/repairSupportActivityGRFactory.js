angular.module('app')
    .factory('RepairSupportActivityGR', function($http, CurrentUser) {

        function stringToDate(str) {

            var date = new Date(str);
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //January is 0!

            var yyyy = date.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            return yyyy + '-' + mm + '-' + dd;
        }

        function getToday() {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            var today = yyyy + '-' + mm + '-' + dd;
            return today;
        }

        var currentUser = CurrentUser.user;
        return {

            // getDataDetail: function(startDate, stopDate, status, NoWo) {
            //   var res = $http.get('/api/as/JobTaskProgresses/135');
            //   console.log('res detail', res)
            //   return res;
            // },

            getData: function(startDate, stopDate, status, NoWo) {
                var res = $http.get('/api/as/JobTasks/ProblemFinding/problemDate/' + startDate + '/' + stopDate + '/WoNo/' + NoWo + '/status/' + status + '');
                return res;
            },

            putRSA: function(Model) {
                /*console.log("local TFirsts :", {
                  MarketInpactRank: Model.MarketInpactRank,
                  TechnicalRank: Model.TechnicalRank,
                  TransmissionType: Model.TransmissionType,
                  StatusFailedPart: Model.StatusFailedPart,
                  PartDelivery: Model.PartDelivery,
                  NoLTIfRepeatRepair: Model.NoLTIfRepeatRepair,
                  NoLTIfInvolvedPBT: Model.NoLTIfInvolvedPBT,
                  ProblemCanBeDuplicate: Model.ProblemCanBeDuplicate,
                  NumberOfCases: Model.NumberOfCases,
                  InquiryTitle: Model.InquiryTitle,
                  MethodToAct: Model.MethodToAct,
                  ResultAction: Model.ResultAction,
                  RequestToDCC: Model.RequestToDCC,
                  IsSolvedProblem: Model.IsSolvedProblem,
                  IsEscalationProblem: Model.IsEscalationProblem,
                  ReportForm: Model.IsEscalationProblem,
                  ConsultationDCCCatg: Model.ConsultationDCCCatg,
                  IsFIR: Model.IsFIR,
                  CatgComponent: Model.CatgComponent,
                  SubCatgComponent: Model.SubCatgComponent,
                  TroubleDesc: Model.TroubleDesc,
                  RSANo: Model.rsaDocNo,
                  DccNo: Model.dccDocNo,
                  LTNo: Model.ltDocNo,
                  TfirstTahun_Pembuatan__c: Model.AssemblyYear,
                  TfirstDCCorLT : Model.BentukLaporan,
                  TfirstPenggunaan_Kendaraan__c: Model.CarUser,
                  TfirstSub_Kategori_Problem__c: Model.CategorySubProblem,
                  TfirstPenyebab_Masalah__c: Model.CausedT2,
                  TfirstPemeriksaan_dan_Langkah_Investigasi__c: Model.ConditionT1,
                  TfirstDelivery_Date__c: stringToDate(Model.DECDate),
                  TfirstRepair_Date__c: stringToDate(Model.DateStart),
                  TfirstTanggal_Komplain_Pertama__c: stringToDate(Model.DateofFirstComplain),
                  TfirstNomor_Mesin__c: Model.EngineNo,
                  TfirstTipe_Mesin__c: Model.EngineType,
                  TfirstTanggal_Masuk_Kendaraan__c: stringToDate(Model.GateInPushTime),
                  TfirstKode_Dealer_Ext__c: Model.GroupDealerCode,
                  TfirstSubject: Model.InquiryTitle,
                  TfirstKeluhan_Pelanggan__c : Model.JobComplaint,
                  TfirstKode_Tipe_Ext__c : Model.KatashikiCode,
                  TfirstOdometer__c: Model.Km,
                  TfirstTipe_Report__c: Model.OFP,
                  TfirstTanggal_Produksi_Kendaraan__c: stringToDate(Model.PLOD),
                  TfirstPengadaan_Part_Tanggal_Pesan_Tersedia__c: stringToDate(Model.PartDeliveryAvailable),
                  TfirstTanggal_Selesai_Perbaikan__c: stringToDate(Model.PlanDateFinish),
                  TfirstNomor_Polisi__c: Model.PoliceNumber,
                  TfirstTanggal_Internal_PBT__c: stringToDate(Model.ProblemFoundedDate),
                  TfirstTanggal_Kirim_PBT_ke_ATL__c: stringToDate(Model.ProblemFoundedDate),
                  TfirstKode_T1__c: Model.T1,
                  TfirstKode_T2__c: Model.T2,
                  TfirstNomor_Rangka_VIN__c: Model.VIN,
                  TfirstKode_Model_Ext__c: Model.VehicleModelCode,
                  TfirstRTJ_atau_Tidak__c: Model.isRTJ
                });*/

                return $http.put('/api/as/JobTasks/ProblemFinding/' + Model.IdJobTaskProgress + '', {
                    MarketInpactRank: Model.MarketInpactRank,
                    TechnicalRank: Model.TechnicalRank,
                    TransmissionType: Model.TransmissionType,
                    StatusFailedPart: Model.StatusFailedPart,
                    PartDelivery: stringToDate(Model.PartDelivery),
                    NoLTIfRepeatRepair: Model.NoLTIfRepeatRepair,
                    NoLTIfInvolvedPBT: Model.NoLTIfInvolvedPBT,
                    ProblemCanBeDuplicate: Model.ProblemCanBeDuplicate,
                    NumberOfCases: Model.NumberOfCases,
                    InquiryTitle: Model.InquiryTitle,
                    MethodToAct: Model.MethodToAct,
                    ResultAction: Model.ResultAction,
                    RequestToDCC: Model.RequestToDCC,
                    IsSolvedProblem: Model.IsSolvedProblem,
                    IsEscalationProblem: Model.IsEscalationProblem,
                    ReportForm: Model.BentukLaporan,
                    ConsultationDCCCatg: Model.ConsultationDCCCatg,
                    IsFIR: Model.IsFIR,
                    CatgComponent: Model.CatgComponent,
                    Component: Model.SubCatgComponent,
                    TroubleDesc: Model.TroubleDesc,
                    RSANo: Model.rsaDocNo,
                    DccNo: Model.dccDocNo,
                    LTNo: Model.ltDocNo,
                    TfirstTahun_Pembuatan__c: Model.AssemblyYear,
                    TfirstDCCorLT: Model.BentukLaporan,
                    TfirstPenggunaan_Kendaraan__c: Model.CarUser,
                    TfirstSub_Kategori_Problem__c: Model.CategorySubProblem,
                    TfirstPenyebab_Masalah__c: Model.CausedT2,
                    TfirstPemeriksaan_dan_Langkah_Investigasi__c: Model.ConditionT1,
                    TfirstDelivery_Date__c: stringToDate(Model.DECDate),
                    TfirstRepair_Date__c: stringToDate(Model.DateStart),
                    TfirstTanggal_Komplain_Pertama__c: stringToDate(Model.DateofFirstComplain),
                    TfirstNomor_Mesin__c: Model.EngineNo,
                    TfirstTipe_Mesin__c: Model.EngineType,
                    TfirstTanggal_Masuk_Kendaraan__c: stringToDate(Model.GateInPushTime),
                    TfirstKode_Dealer_Ext__c: Model.GroupDealerCode,
                    TfirstSubject: Model.InquiryTitle,
                    TfirstKeluhan_Pelanggan__c: Model.JobComplaint,
                    TfirstKode_Tipe_Ext__c: Model.KatashikiCode,
                    TfirstOdometer__c: Model.Km,
                    TfirstTipe_Report__c: Model.OFP,
                    TfirstTanggal_Produksi_Kendaraan__c: stringToDate(Model.PLOD),
                    TfirstPengadaan_Part_Tanggal_Pesan_Tersedia__c: stringToDate(Model.PartDeliveryAvailable),
                    TfirstTanggal_Selesai_Perbaikan__c: stringToDate(Model.PlanDateFinish),
                    TfirstNomor_Polisi__c: Model.PoliceNumber,
                    TfirstTanggal_Internal_PBT__c: stringToDate(Model.ProblemFoundedDate),
                    TfirstTanggal_Kirim_PBT_ke_ATL__c: stringToDate(Model.ProblemFoundedDate),
                    TfirstKode_T1__c: Model.T1,
                    TfirstKode_T2__c: Model.T2,
                    TfirstNomor_Rangka_VIN__c: Model.VIN,
                    TfirstKode_Model_Ext__c: Model.VehicleModelCode,
                    TfirstRTJ_atau_Tidak__c: Model.isRTJ,
                    RespondDesciption: Model.DeskripsiRespon
                });
            },

            postRSATfirst: function(model) {
                var today = getToday();
                var Fir;
                // var dateNow = new Date();
                // var tglDiterimaTam_c = new Date(dateNow.valueOf() + dateNow.getTimezoneOffset() * 60000);
                var date = new Date();
                var milSec = date.getMilliseconds();
                var sec = date.getSeconds().toString();
                var hour = date.getHours().toString();
                var minute = date.getMinutes().toString();
                var yyyy = date.getFullYear().toString();
                var mm = (date.getMonth() + 1).toString();
                var dd = date.getDate().toString();
                var tglDiterimaTam_c = yyyy+'-'+(mm[1] ? mm : "0" + mm[0])+'-'+(dd[1] ? dd : "0" + dd[0])+'T'+(hour[1] ? hour : "0" + hour[0])+':'+(minute[1] ? minute : "0" + minute[0])+":"+(sec[1] ? sec : "0" + sec[0])+"."+milSec+"+"+"07:00";

                if (model.IsFIR == 1) {
                    Fir = "FIR";
                } else {
                    Fir = "Non FIR";
                }
                console.log("fir", Fir);
                var obj = {
                    "Kode_Dealer_Ext__c": model.GroupDealerCode,
                    "Kode_Model_Ext__c": model.VehicleModelCode,
                    "Kode_Tipe_Ext__c": model.KatashikiCode,
                    "Kode_Model_Text_Area__c": model.KatashikiCode,
                    // "FIR_Non_FIR__c": model.IsFIR,
                    "FIR_Non_FIR__c": Fir,
                    "Kategori_Komponen__c": model.CatgComponent,
                    "Kategori_Problem__c": model.ConsultationDCCCatg,
                    "Komponen__c": model.SubCatgComponent,
                    "Sub_Kategori_Problem__c": model.CategorySubProblem,
                    "RTJ_atau_Tidak__c": model.isRTJ,
                    "Subject": model.InquiryTitle,
                    "Judul_Dokumen__c": model.InquiryTitle,
                    "Nomor_Polisi__c": model.PoliceNumber,
                    "Nomor_Rangka_VIN__c": model.VIN,
                    "Penyebab_Masalah__c": model.CausedT2,
                    "Perbaikan_Yang_Dilakukan__c": model.MethodToAct,
                    "Odometer__c": model.Km,
                    "Tanggal_Masuk_Kendaraan__c": stringToDate(model.GateInPushTime),
                    "Tanggal_Internal_PBT__c": stringToDate(model.ProblemFoundedDate),
                    "Tanggal_Kirim_PBT_ke_ATL__c": stringToDate(model.ProblemFoundedDate),
                    "Tanggal_Respon_PBT_Oleh_ATL__c": today,
                    "Tanggal_Selesai_Perbaikan__c": stringToDate(model.PlanDateFinish),
                    "Pengadaan_Part_Tanggal_Pesan__c": stringToDate(model.PartDelivery),
                    "Pengadaan_Part_Tanggal_Pesan_Tersedia__c": stringToDate(model.PartDeliveryAvailable),
                    "Tanggal_Diterima_TAM__c": tglDiterimaTam_c,
                    "INQ_DMS_ID__c": "",
                    "Module_Type__c": "TL Internal PBT",
                    "Origin": "DMS",
                    "Status": "New"
                };
                console.log("obj rsa :", obj);

                return $http.post('/api/as/TFirsts/RSA', obj);
            },

            postDCCTfirst: function(model) {
                var today = getToday();

                var obj = {
                    "Kode_Dealer_Ext__c": model.GroupDealerCode,
                    "Kode_Model_Ext__c": model.VehicleModelCode,
                    "Kode_Tipe_Ext__c": model.KatashikiCode,
                    "Kode_Model_Text_Area__c": model.KatashikiCode,
                    "Kategori_Komponen__c": model.CatgComponent,
                    "Kategori_Problem__c": model.ConsultationDCCCatg,
                    "Komponen__c": model.SubCatgComponent,
                    "Sub_Kategori_Problem__c": model.CategorySubProblem,
                    "Subject": model.InquiryTitle,
                    "Judul_Dokumen__c": model.InquiryTitle,
                    "Nomor_Mesin__c": model.EngineNo,
                    "Nomor_Polisi__c": model.PoliceNumber,
                    "Nomor_Rangka_VIN__c": model.VIN,
                    "Hasil__c": model.ResultAction,
                    "Keluhan_Pelanggan__c": model.JobComplaint,
                    "Penggunaan_Kendaraan__c": model.CarUser,
                    "Perbaikan_Yang_Dilakukan__c": model.MethodToAct,
                    "Request_ke_DCC__c": model.RequestToDCC,
                    "Odometer__c": model.Km,
                    "Delivery_Date__c": stringToDate(model.DECDate),
                    "Tanggal_Masuk_Kendaraan__c": stringToDate(model.GateInPushTime),
                    "Tanggal_Komplain_Pertama__c": stringToDate(model.DateofFirstComplain),
                    "Repair_Date__c": stringToDate(model.DateStart),
                    "Tanggal_Selesai_Perbaikan__c": stringToDate(model.PlanDateFinish),
                    "INQ_DMS_ID__c": "",
                    "Module_Type__c": "DCC",
                    "Origin": "DMS",
                    "Status": "Draft"
                };
                console.log("obj dcc :", obj);

                return $http.post('/api/as/TFirsts/DCC', obj);
            },

            postLTETfirst: function(model) {
                var today = getToday();

                // var dateNow = new Date();
                // var tglDiterimaTam_c = new Date(dateNow.valueOf() + dateNow.getTimezoneOffset() * 60000);
                var date = new Date();
                var milSec = date.getMilliseconds();
                var sec = date.getSeconds().toString();
                var hour = date.getHours().toString();
                var minute = date.getMinutes().toString();
                var yyyy = date.getFullYear().toString();
                var mm = (date.getMonth() + 1).toString();
                var dd = date.getDate().toString();
                var tglDiterimaTam_c = yyyy+'-'+(mm[1] ? mm : "0" + mm[0])+'-'+(dd[1] ? dd : "0" + dd[0])+'T'+(hour[1] ? hour : "0" + hour[0])+':'+(minute[1] ? minute : "0" + minute[0])+":"+(sec[1] ? sec : "0" + sec[0])+"."+milSec+"+"+"07:00";

                var obj = {
                    "Kode_Dealer_Ext__c": model.GroupDealerCode,
                    "Kode_Model_Ext__c": model.VehicleModelCode,
                    "Kode_Tipe_Ext__c": model.KatashikiCode,
                    "No_LT_Jika_Melibatkan_PBT_Ext__c": model.NoLTIfInvolvedPBT,
                    "No_LT_Jika_Repeat_Repair_Ext__c": model.NoLTIfRepeatRepair,
                    "Original_Failed_Parts_Ext__c": "Part1",
                    "Kategori_Komponen__c": model.CatgComponent,
                    "Kode_T1__c": model.T1,
                    "Kode_T2__c": model.T2,
                    "Komponen__c": model.SubCatgComponent,
                    "Market_Impact_Rank__c": model.MarketInpactRank,
                    "Problem_can_be_duplicated__c": model.ProblemCanBeDuplicate,
                    "Status_Failed_Part__c": model.StatusFailedPart,
                    "Technical_Rank__c": model.TechnicalRank,
                    "Tipe_Report__c": model.OFP,
                    "Name": model.InquiryTitle,
                    "Judul_Laporan__c": model.InquiryTitle,
                    "Nomor_Mesin__c": model.EngineNo,
                    "no_polisi__c": model.PoliceNumber,
                    "Nomor_Rangka_VIN__c": model.VIN,
                    "Tahun_Pembuatan__c": model.AssemblyYear,
                    "Tipe_Mesin__c": model.EngineType,
                    "Tipe_Transmisi__c": model.TransmissionType,
                    "Hasil_Perbaikan__c": model.ResultAction,
                    "Keluhan_Pelanggan__c": model.JobComplaint,
                    "Metode_Perbaikan__c": model.MethodToAct,
                    "Pemeriksaan_dan_Langkah_Investigasi__c": model.ConditionT1,
                    "Penyebab_Masalah__c": model.CausedT2,
                    "Jumlah_Case__c": model.NumberOfCases,
                    "Odometer__c": model.Km,
                    "Tanggal_Part_Dikirim__c": model.PartDelivery,
                    "Tanggal_Produksi_Kendaraan__c": stringToDate(model.PLOD),
                    "Tanggal_Komplain_Pertama__c": stringToDate(model.DateofFirstComplain),
                    "Tanggal_Selesai_Perbaikan__c": stringToDate(model.PlanDateFinish),
                    "Tanggal_Diterima_TAM__c": tglDiterimaTam_c,
                    "LT_DMS_ID__c": "",
                    "Source_Origin__c": "DMS",
                    "Status_Dokumen__c": "Draft"
                };
                console.log("obj lt :", obj);

                return $http.post('/api/as/TFirsts/LT', obj);
            },

            getMarketImpact: function() {
                var catId = 2013;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                // console.log('resnya pause=>',res);
                return res;
            },

            getTechnicalRank: function() {
                var catId = 2016;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                // console.log('resnya pause=>',res);
                return res;
            },

            getTransmisionType: function() {
                var catId = 2017;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                // console.log('resnya pause=>',res);
                return res;
            },

            getEngineType: function() {
                var catId = 2018;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                // console.log('resnya pause=>',res);
                return res;
            },

            getFailPart: function() {
                var catId = 2019;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                // console.log('resnya pause=>',res);
                return res;
            },

            getKategoriKomponen: function() {
                var catId = 2021;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                // console.log('resnya pause=>',res);
                return res;
            },

            getKomponen: function() {
                var catId = 2022;
                // var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                var res = $http.get('/api/as/MTFIRSTComponent');
                // console.log('resnya pause=>',res);
                return res;
            },

            getKategoriProblem: function() {
                var catId = 2025;
                // var res = $http.get('/api/as/GlobalMaster/Sub/' + catId);
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                // console.log('resnya pause=>',res);
                return res;
            },

            getSubKategoriProblem: function() {
                var catId = 2026;
                // var res = $http.get('/api/as/GlobalMaster/Sub/' + catId);
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                // console.log('resnya pause=>',res);
                return res;
            },

            getT1: function() {
                // var catId = 2023;
                var res = $http.get('/api/as/GetT1');
                // console.log('resnya pause=>',res);
                return res;
            },

            getT2: function() {
                // var catId = 2024;
                // var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                var res = $http.get('/api/as/GetT2');
                // console.log('resnya pause=>',res);
                return res;
            },
            getTeknisiTask: function(JobTaskID) {
                var res = $http.get('/api/as/JobTaskTechniciansByTaskId/' + JobTaskID);
                console.log('JOb Task Dari Teknisi=>', res);
                return res;
            }

        }
    });