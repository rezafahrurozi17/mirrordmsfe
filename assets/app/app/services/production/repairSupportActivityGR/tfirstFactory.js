angular.module('app')
  .factory('TFirst', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;

    var postAuthTfirst = $http({
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      /*transformRequest: function(obj) {
        console.log("transform", obj);
        var str = [];
        for (var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        console.log("transform result", str.join("&"));
        return str.join("&");
      },
      data: {
        grant_type: "password",
        client_id: "3MVG9e2mBbZnmM6mryoG7xARUIbrQAhlb1hjpaeJJ2w2AI2IFQfW_iodaxFZqdYQuE4BEhnc6tPlAklo5qi4T",
        client_secret: "5583664426233030818",
        username: "dms.integration@tfirst.co.id.sb",
        password: "Password1234"
      },*/
      url: 'https://test.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9e2mBbZnmM6mryoG7xARUIbrQAhlb1hjpaeJJ2w2AI2IFQfW_iodaxFZqdYQuE4BEhnc6tPlAklo5qi4T&client_secret=5583664426233030818&username=dms.integration@tfirst.co.id.sb&password=Password1234'
    });/*.then(function successCallback(response) {
      // this callback will be called asynchronously
      // when the response is available
      console.log("sukses", response);
      return response.token_type + " " + response.access_token;
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
      console.log("failed", response);
      return "";
    });*/

    function checkTokkenValid(tokken){
      var res = "";
      console.log("tokken", tokken);

      if(typeof tokken === 'object' && tokken.hasOwnProperty('access_token') && tokken.hasOwnProperty('token_type')){
        res = tokken.token_type + " " + tokken.access_token;
      }

      console.log("res : ",res);

      return res;
    }

    return {

      postDCC: function() {
        var tokken = postAuthTfirst();

        var auth = checkTokkenValid(tokken);

        console.log("auth ",auth);

        return {};
      }
    }
  });
