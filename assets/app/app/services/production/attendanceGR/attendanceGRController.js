angular.module('app')
    .controller('AttendanceGRController', function($scope, $http, CurrentUser, AttendanceGR,$timeout,bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mData = null; //Model
    $scope.xRole={selected:[]};
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.ismeridian = false;
    $scope.hstep = 1;
    $scope.mstep = 1;
    $scope.filter = [];
    $scope.Alasan2 = [
    {
        Alasantext : "IJ - Ijin"
    },
    {
        Alasantext : "CT - Cuti"
    }
    ];
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var det = new Date("October 13, 2014 11:13:00");
    $scope.DateOptions = {
        // maxDate: new Date(2016, 9, 29),
        // minDate: new Date(),
        startingDay: 1,
        format: dateFormat
    };
    var res = [
    {
        Kode : 'F120',
        Initial : 'LC',
        Name : 'Lifa Christian',
        Jabatan : 'Teknisi BP',
        Posisi : 'BP',
        Group : 'GGWP',
        Tanggal : det,
        Hadir: 1,
        Availability: 0,
        JamDatang : '09:00',
        JamPulang : '21:00',
        Shift : '1',
        Alasan : '',
        Catatan : '',
        Stall : 'EM 02'
    },
    {
        Kode : 'B2200',
        Initial : 'CG',
        Name : 'WHAT',
        Jabatan : 'Teknisi CR',
        Posisi : 'BP',
        Group : 'GG',
        Tanggal : det,
        Hadir: 0,
        Availability: 0,
        JamDatang : '09:00',
        JamPulang : '21:00',
        Shift : '1',
        Alasan : 'IJ - Ijin',
        Catatan : 'Nope',
        Stall : 'EM 02'
    }
    ];

    $scope.radioptions = [
                        {text:"Hadir",value:"Hadir"},
                        {text: "Tidak Hadir",value: "Absen"}
                      ];
    var gridData = [];
    $scope.ismeridian = false;
    // $scope.mytime = new Date();
    $scope.hstep = 1;
    $scope.mstep = 1;
    $scope.getData = function() {
        console.log('=====>',$scope.filter.dt);
        if($scope.filter.dt !== undefined){
            // if($scope.filter.name !== undefined){
                $scope.grid.data = res;
            // }
        }else{
              bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "Mohon Input Filter",
                // content: error.join('<br>'),
                // number: error.length
            });
        }
    $scope.onShowDetail = function(data,mode){
        if(data.Hadir !== 0){
            $scope.mData.Hadir = "1";
            $scope.mData.Alasan = "";
        }
        // var dt = new Date($scope.filter.dt);
        // console.log("dt",dt);
        // $scope.filter.dt = dt;
        console.log("ini mData",$scope.mData.Alasan);
        console.log("ini modenya",mode);
        console.log("ini datanya",data);
    }
    $scope.checkRadio = function(data){
        if(data == "1"){
            $scope.mData.Alasan = "";
            $scope.mData.Catatan="";
        }
    }

    $scope.fFrom = function(){
    var d = new Date($scope.mData.TimeThru);
    d.getHours();
    d.getMinutes();
    $scope.maxFrom = d;
    if($scope.mData.TimeFrom > $scope.mData.TimeThru){
            $scope.mData.TimeFrom =  $scope.mData.TimeThru;
        }
    }
    $scope.fThrough =function(){
        var z = new Date($scope.mData.TimeFrom);
        z.getHours();
        z.getMinutes();
        $scope.minThru = z;
        if($scope.mData.TimeThru < $scope.mData.TimeFrom){
            $scope.mData.TimeThru = $scope.mData.TimeFrom;
        }
    }
      // // console.log("nih", $scope.VM );
      // Towing.getData()
      //   .then(

      //     function(res) {
      //       gridData = [];
      //       //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
      //       $scope.grid.data = res.data;
      //       // console.log("role=>", res.data.Result);
      //       //console.log("grid data=>",$scope.grid.data);
      //       //$scope.roleData = res.data;
      //       $scope.loading = false;
      //       console.log("test",res);
      //     },
      //     function(err) {
      //       console.log("err=>", err);
      //     }
      //   );
    }
    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    // var actionTemplate =   '<div class="ui-grid-cell-contents">'+
    //                         '<a href="#" ng-click="grid.appScope.kehadiran"'+
    //                         '<button class="ui icon inverted grey button ng-scope" ng-click="grid.appScope.deleteWork(grid.renderContainers.body.visibleRowCache.indexOf(row),row.entity)" style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px" tabindex="0"> <i class="fa fa-fw fa-lg fa-trash-o"></i> </button>'+
    //                         '</div>';
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'Nama',  field: 'Name' },
            { name:'Jabatan',  field: 'Jabatan' },
            { name:'Tanggal', field:'Tanggal', cellFilter: dateFilter},
            { name:'Jam Datang', field:'JamDatang'},
            { name:'Jam Pulang', field:'JamPulang'},
            { name:'Hadir', field:'Hadir', type: 'boolean', cellTemplate : '<input type="checkbox" ng-disabled="true" ng-checked="row.entity.Hadir"/>'},
            { name:'Availability', field:'Availability', cellTemplate : '<input type="checkbox" ng-disabled="true" ng-checked="row.entity.Availability"/>'},
            { name:'Action', field:'Action'},
        ]
    };
});
