angular.module('app')
    .controller('QueueCategoryController', function($scope, $http, CurrentUser, QueueCategory,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mData = null; //Model
    $scope.xRole={selected:[]};

    //----------------------------------
    // Get Data
    //----------------------------------

    var gridData = [];
    $scope.getData = function() {
        
        QueueCategory.getData()
        .then(
            function(res){
                gridData = [];
                //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
                for (var i = 0;i<res.data.Result.length; i++) {
                    if (res.data.Result[i].Status == 1) {
                        res.data.Result[i].Xstatus = "Active";
                    } else if(res.data.Result[i].Status == 0) {
                        res.data.Result[i].Xstatus = "Not Active";
                    } else {
                        res.data.Result[i].Xstatus = " - ";
                    }
                }
                $scope.grid.data = res.data.Result;
                console.log("role=>",res.data.Result);
                //console.log("grid data=>",$scope.grid.data);
                //$scope.roleData = res.data;

                $scope.loading=false;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }

    function roleFlattenAndSetLevel(node,lvl){
        for(var i=0;i<node.length;i++){
            node[i].$$treeLevel = lvl;
            gridData.push(node[i]);
            if(node[i].child.length>0){
                roleFlattenAndSetLevel(node[i].child,lvl+1)
            }else{

            }
        }
        return gridData;
    }
    $scope.selectRole = function(rows){
        console.log("onSelectRows=>",rows);
        $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
    }
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'QCategoryId',    field:'QCategoryId', width:'7%', visible:false },
            { name:'name', field:'Name' },
            { name:'Auto Call Weight',  field: 'AutoCallWeight' },
            { name:'Status',  field: 'Xstatus' },
        ]
    };
});
