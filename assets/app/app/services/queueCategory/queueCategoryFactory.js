angular.module('app')
  .factory('QueueCategory', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    console.log(currentUser);
    return {
      getData: function() {
        var res=$http.get('/api/as/QueueCategories');
        console.log('res=>',res);
        //res.data.Result = null;
        return res;
      },
      create: function(mData) {
        return $http.post('/api/as/QueueCategories', [{
                                            Name: mData.Name,
                                            AutoCallWeight: mData.AutoCallWeight,
                                            Status: mData.Status
                                            }]);
      },
      update: function(mData){
        return $http.put('/api/as/QueueCategories', [{
                                            QCategoryId : mData.QCategoryId,
                                            Name: mData.Name,
                                            AutoCallWeight: mData.AutoCallWeight,
                                            Status: mData.Status}]);
      },
      delete: function(QCategoryId) {
        return $http.delete('/api/as/QueueCategories',{data:QCategoryId,headers: {'Content-Type': 'application/json'}});
      }
    }
  });
