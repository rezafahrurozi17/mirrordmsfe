angular.module('app')
  .factory('GeneralFactory', function($http) {
    console.log('Service CompletenessData Loaded...');
    var serverURL = 'http://localhost:8080/api/';
    // var currentUser = CurrentUser.user();
    // console.log(currentUser);
    return {
            getDataDealer: function() {
            var res=$http.get(serverURL + 'Dealer/');
            return res;},
            getDataCabang: function() {
            var res=$http.get(serverURL + 'Cabang/');
            return res;},
            getDataSatellite: function() {
            var res=$http.get(serverURL + 'Satellite/');
            return res;},            
            getDataAreaTAM: function() {
            var res=$http.get(serverURL + 'AreaTAM/');
            return res;},
            getDataAreaDealer: function() {
            var res=$http.get(serverURL + 'AreaDealer/');
            return res;},
            getDataAreaCabang: function() {
            var res=$http.get(serverURL + 'AreaCabang/');
            return res;},  
            getTipeMaterial: function() {
            var res=$http.get(serverURL + 'TipeMaterial/');
            return res;},            
            getGlobal: function(data) {
            var res=$http.get(serverURL + 'getGlobal/'+ data.id);
            return res;},       
            getGrupBP: function() {
            var res=$http.get(serverURL + 'getGrupBP/');
            return res;},   
            getPaymentMethod: function() {
            var res=$http.get(serverURL + 'getPaymentMethod/');
            return res;},   
            getInsurance: function(data) {
            var res=$http.get(serverURL + 'getInsurance/'+ data.Cabang);
            return res;}, 
            getClaimReason: function(data) {
            var res=$http.get(serverURL + 'getClaimReason');
            return res;},   
            getVendor: function(data) {
            var res=$http.get(serverURL + 'getVendor');
            return res;},      
    }
  });
