angular.module('app')
    .controller('WIPReportController', function($scope, $http, $stateParams, CurrentUser, WIPReportFactory) {
        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var periode = new Date();
        $scope.maxDate = new Date(periode.getFullYear(), periode.getMonth(), periode.getDate());
        $scope.Show_Periode = true;
        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat,

            // mode:"'month'",
        };
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
        });
        $scope.filter = { Dealer: null, Cabang: null, DateStart: periode, DateEnd: periode, TipePencarianWO: 1, TipePIC: 1, PIC: null };
        $scope.tipePencarianWO = { data: [{ text: "Tgl Cetak WO", value: 1 }, { text: "Tgl Billing WO", value: 2 }, { text: "Open WO", value: 3 }] };
        $scope.tipePIC = { data: [{ text: "SA", value: 1 }, { text: "Teknisi", value: 2 }] };
        $scope.role = $stateParams.role;
        console.log('role', $scope.role);
        if ($scope.role == "GR") {
            var datakategori = { 'kategori': 'General Repair', };
            console.log('General Repair');
        } else {
            var datakategori = { 'kategori': 'Body And Paint', };
            console.log('Body And Paint');
        }

        // //----------------------------------
        // // Get Data
        // //----------------------------------
        // var gridData = [];

        WIPReportFactory.getDataDealer().then(function(response) {
            console.log('Response BEDealer:', response.data);
            $scope.dataDealer = response.data;
            $scope.filter.Dealer = response.data[0].dealerCode;
        })
        WIPReportFactory.getDataCabang().then(function(response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabang = response.data;
            $scope.DisabledCabang();
            $scope.filter.Cabang = response.data[0].cabangID;
        })

        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length > 1) {
                $scope.Disabled = false;
                console.log('Cabang Enabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            } else {
                $scope.Disabled = true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };

        WIPReportFactory.getDataPIC('teknisi ' + $scope.role).then(function(response) {
            console.log('Response BEPIC:', response.data);
            // $scope.dataSA = response.data;
            $scope.dataTeknisi = response.data; // hmm bisa ketukar sa dan teknisi
        })

        WIPReportFactory.getDataPIC('service advisor ' + $scope.role).then(function(response) {
            console.log('Response BEPIC:', response.data);
            // $scope.dataTeknisi = response.data;
            $scope.dataSA = response.data; // hmm bisa ketukar sa dan teknisi
        })

        $scope.DisabledTAM = function() {
            if ($scope.userlogin != 'TAM') {
                return true;
                console.log("DisabledTAM TRUE");
            } else {
                return false;
                console.log("DisabledTAM FALSE");
            }
        };

        $scope.changeTipePencarianWO = function(value) {
            // $timeout(function(){
            //console.log('value', value);
            //console.log('$scope.filter===>', $scope.filter);
            if (value == 3 || value == '3') {

                var d = new Date();
                $scope.filter.DateStart = d;
                $scope.filter.DateThru = d;
                $scope.filter.DateEnd = d;
                $scope.filter.TipePencarianWODisable = true;
                $scope.Show_Periode = false;

            } else {

                $scope.filter.TipePencarianWODisable = false;
                $scope.Show_Periode = true;
            }
            // });
        }

        $scope.changeTipePIC = function() {
            // $timeout(function(){
            console.log('tipe pic', $scope.filter.TipePIC);
            if ($scope.filter.TipePIC == 1) {

                console.log("INI SA");
            } else if ($scope.filter.TipePIC == 2) {
                console.log("INI Teknisi");
            }
            // });
        }
        $scope.getData = function() {
            if ($scope.role == "GR") $scope.getSummaryGR();
            else $scope.getSummaryBP();
            console.log('get data');
        }
        $scope.getReport = function() {
            if ($scope.role == "GR") $scope.getReportGR();
            else $scope.getReportBP();
            console.log('get data');
        }

        function format1(n, currency) {
            console.log('format1', n, currency);
            return currency == null ? 0 : currency + " " + n.toFixed(0).replace(/./g, function(c, i, a) {
                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
            });
        }
        $scope.getSummaryGR = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataFilter = {

                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tipe': $scope.filter.TipePencarianWO,
                'PIC': $scope.filter.TipePIC,
                'picid': $scope.filter.PIC
            };

            WIPReportFactory.getSummaryGR(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);

                $scope.ServiceTotalUnit = response.data[0].unitBS;
                $scope.ARTotalUnit = response.data[0].unitAR;
                $scope.TotalUnit = $scope.ServiceTotalUnit + $scope.ARTotalUnit;
                $scope.ServiceTotalAmount = response.data[0].amountBS == null ? 0 : response.data[0].amountBS;
                console.log('ServiceTotalAmount:', $scope.ServiceTotalAmount);
                $scope.ServiceTotalAmount = format1($scope.ServiceTotalAmount, '');
                $scope.ARTotalAmount = response.data[0].amountAR == null ? 0 : response.data[0].amountAR;
                console.log('ARTotalAmount:', $scope.ARTotalAmount);
                $scope.ARTotalAmount = format1($scope.ARTotalAmount, '');

                //$scope.TotalAmount = $scope.ServiceTotalAmount+$scope.ARTotalAmount;
                $scope.TotalAmount = response.data[0].amountBS + response.data[0].amountAR;
                console.log('TotalAmount:', $scope.TotalAmount);
                $scope.TotalAmount = format1($scope.TotalAmount, '');

            })
        }
        $scope.getReportGR = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();

            var dataReport = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tipe': $scope.filter.TipePencarianWO,
                'PIC': $scope.filter.TipePIC,
                'picid': $scope.filter.PIC
            };

            WIPReportFactory.getExportGR(dataReport);
        }

        $scope.getSummaryBP = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataFilter = {

                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tipe': $scope.filter.TipePencarianWO,
                'PIC': $scope.filter.TipePIC,
                'picid': $scope.filter.PIC
            };
            console.log('filter', dataFilter);
            WIPReportFactory.getSummaryBP(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);

                $scope.ServiceTotalUnit = response.data[0].unitBS;
                $scope.ARTotalUnit = response.data[0].unitAR;
                $scope.TotalUnit = $scope.ServiceTotalUnit + $scope.ARTotalUnit;
                $scope.ServiceTotalAmount = response.data[0].amountBS == null ? 0 : response.data[0].amountBS;
                console.log('ServiceTotalAmount:', $scope.ServiceTotalAmount);
                $scope.ServiceTotalAmount = format1($scope.ServiceTotalAmount, '');
                $scope.ARTotalAmount = response.data[0].amountAR == null ? 0 : response.data[0].amountAR;
                console.log('ARTotalAmount:', $scope.ARTotalAmount);
                $scope.ARTotalAmount = format1($scope.ARTotalAmount, '');


                $scope.TotalAmount = response.data[0].amountBS + response.data[0].amountAR;
                $scope.TotalAmount = format1($scope.TotalAmount, '');
                console.log('TotalAmount', $scope.TotalAmount);
            })
        }
        $scope.getReportBP = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();

            var dataReport = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tipe': $scope.filter.TipePencarianWO,
                'PIC': $scope.filter.TipePIC,
                'picid': $scope.filter.PIC
            };

            WIPReportFactory.getExportBP(dataReport);
        }
    });