angular.module('app')
  .factory('WIPReportFactory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = ''; //'http://localhost:8080';
    return {
            getDataDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},
            getDataCabang: function() {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            return res;},
            getDataPIC: function(data) {
            console.log('data pic',data);
            var res=$http.get(serverURL + '/api/rpt/pic/'+ currentUser.OrgId +'/'+ data);
            return res;},
            getDataSA: function(data) {
            var res=$http.get(serverURL + '/api/rpt/SA/'+ data.kategori + currentUser.OrgId);
            return res;}, 
            getSummaryBP: function(data) {
              console.log('Data Report:', data);

              var res = $http.get(serverURL + '/api/rpt/AS_WIPReport_GRBPCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.tipe +'/'+ data.PIC +'/'+ data.picid +'/BP' );
                return res;
            },
            getExportBP: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_WIPReport_GRBPCAB?dealer='+ data.dealer +'&cabang='+ data.cabang  +'&tipe='+ data.tipe +'&periode_awal='+ data.periodeawal +'&periode_akhir='+ data.periodeakhir +'&pic='+ data.PIC +'&picid='+ data.picid +'&kategori=BP');
            },
            getSummaryGR: function(data) {
              console.log('Data Report:', data);

              var res = $http.get(serverURL + '/api/rpt/AS_WIPReport_GRBPCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.tipe +'/'+ data.PIC +'/'+ data.picid +'/GR');
                return res;
            },
            getExportGR: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_WIPReport_GRBPCAB?dealer='+ data.dealer +'&cabang='+ data.cabang  +'&tipe='+ data.tipe +'&periode_awal='+ data.periodeawal +'&periode_akhir='+ data.periodeakhir +'&pic='+ data.PIC +'&picid='+ data.picid +'&kategori=GR');
            }
    }
  });
