angular.module('app')
  .factory('CompletenessDataFactory', function($http, CurrentUser,$q) {
    console.log('Service CompletenessData Loaded...');
    var serverURL = ''; //http://localhost:8080/'
    var currentUser = CurrentUser.user();
    // console.log(currentUser);
    return {
            getDataDealer: function(areaId) {
            // var res=$http.get(serverURL + '/api/rpt/NewDealer/'+ areaId);
            var res=$http.get(serverURL + '/api/as/NewDealer/'+ areaId);
            return res;},

            getDataCabang: function(dealerID, areaId) {
            // var res=$http.get(serverURL + '/api/rpt/NewCabang/'+ dealerID);
            var res=$http.get(serverURL + '/api/as/NewCabang/'+ dealerID + '/' + areaId);
            return res;},

            getDataArea: function() {
            // var res=$http.get(serverURL + '/api/rpt/AreaTAM/');
            var res=$http.get(serverURL + '/api/as/AreaTAM/');
            return res;},

            getSummaryFilter: function(data) {
              console.log('Data Filter:', data);
              // console.log('periode',moment(data.periode).format('YYYY-MM-DD'))
              var res = $http.get(serverURL + '/api/rpt/C_CustomerCompleteness/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode +'/'+ data.area +'/'+ data.jenispelanggan +'/'+ data.jenis );
              return res;},

            getSummaryExport: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/C_CustomerCompleteness?area='+ data.area +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periode +'&jenis_pelanggan='+ data.jenispelanggan);
              // var res = $http.get(serverURL + 'CCustomerCompleteness?area='+ data.area +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periode +'&jenis_pelanggan='+ data.jenispel);
              // return res;
            }

    }
  });
