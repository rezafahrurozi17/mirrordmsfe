angular.module('app')
    .controller('CompletenessDataController', function($scope, $http, CurrentUser, CompletenessDataFactory,$timeout,$q,bsAlert,QueueCategory,$q) {
    console.log('CompletenessData COMP');
// $scope.inputan5 = "Individu"
// $scope.inputan6 = "summary"
// $scope.inputan1 = "TAM"
// $scope.inputan2 = "TAM000000"
$scope.tanggal=null;
// $scope.disabledGetReport = true;
$scope.CompletenessDataForm={};
$scope.dataJenisPelanggan=[
    {jenispelangganID:"Individu",jenispelangganName:"Individu"},
    {jenispelangganID:"Perusahaan",jenispelangganName:"Perusahaan"},
    {jenispelangganID:"Yayasan",jenispelangganName:"Yayasan"},
    {jenispelangganID:"Pemerintahan",jenispelangganName:"Pemerintahan"}];

$scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
        // mode:"'month'",
        disableWeekend: 1
    };

      
      
    //   CompletenessDataFactory.getDataAreaTAM().then(function(response) {
      CompletenessDataFactory.getDataArea().then(function(response) {          
            console.log('Response AreaTAM:', response.data);
            $scope.dataAreaTAM = response.data;
            $scope.CompletenessDataForm.area = null;
            $scope.CompletenessDataForm.dealer = null;
            $scope.CompletenessDataForm.cabang = null;
            $scope.CompletenessDataForm.jenispelanggan = null;
            $scope.CompletenessDataForm.tanggal = new Date();
        })
      $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
    
    $scope.resetKolom = function() {
        $scope.DataDiri = ''
        $scope.DataInstitusi = ''
        $scope.DataKeluarga = ''
        $scope.DataKendaraan = ''
        $scope.DataPekerjaan = ''
        $scope.DataPenanggungJawab = ''
        $scope.DataPreferensi = ''
        $scope.JumlahIndividu = ''
        $scope.JumlahInstitusi = ''
        $scope.KelengkapanDataIndividu = '' 
    }

      $scope.getData = function(){
        $scope.resetKolom()
        var $tanggal=$scope.CompletenessDataForm.tanggal;
        console.log('tgl',$scope.CompletenessDataForm.tanggal);
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        if ($scope.CompletenessDataForm.area == null ||$scope.CompletenessDataForm.area == undefined){
            $scope.CompletenessDataForm.area = '-';
        } 
        if ($scope.CompletenessDataForm.dealer == null || $scope.CompletenessDataForm.dealer == undefined || $scope.CompletenessDataForm.dealer == ""){
            $scope.CompletenessDataForm.dealer = '-';
        } 
        if ($scope.CompletenessDataForm.cabang == null ||$scope.CompletenessDataForm.cabang == undefined ||$scope.CompletenessDataForm.cabang == ""){
            $scope.CompletenessDataForm.cabang = '-';
        }  
        if ($scope.CompletenessDataForm.jenispelanggan == null || $scope.CompletenessDataForm.jenispelanggan == undefined||$scope.CompletenessDataForm.jenispelanggan == ""){
            $scope.CompletenessDataForm.jenispelanggan = '-';
        }  
        var dataFilter = {

            'jenispelanggan': $scope.CompletenessDataForm.jenispelanggan,
            'jenis': "summary",
            'dealer': $scope.CompletenessDataForm.dealer,
            'cabang': $scope.CompletenessDataForm.cabang,
            'periode': $filtertanggal,
            'area': $scope.CompletenessDataForm.area,};
            console.log('dtf', dataFilter);

          CompletenessDataFactory.getSummaryFilter(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            if (response.data.length > 0){
                $scope.DataDiri = response.data[0].dataDiri;
                $scope.DataInstitusi = response.data[0].dataInstitusi;
                $scope.DataKeluarga = response.data[0].dataKeluarga;
                $scope.DataKendaraan = response.data[0].dataKendaraan;
                $scope.DataPekerjaan = response.data[0].dataPekerjaan;
                $scope.DataPenanggungJawab = response.data[0].dataPenanggungJawab;
                $scope.DataPreferensi = response.data[0].dataPreferensi;
                $scope.JumlahIndividu = response.data[0].jumlahIndividu;
                console.log('Individu', response.data[0].jumlahIndividu);
                $scope.JumlahInstitusi = response.data[0].jumlahInstitusi;
                $scope.KelengkapanDataIndividu = response.data[0].kelengkapanDataIndividu;
            }
            
          })
        //   console.log ('inidiaaa=====>',$scope.CompletenessDataForm);
      }

      $scope.getReport = function(){
        // if($scope.CompletenessDataForm.area != null||$scope.CompletenessDataForm.dealer != null||$scope.CompletenessDataForm.cabang != null||$scope.CompletenessDataForm.jenispelanggan != null || $scope.CompletenessDataForm.tanggal != null){
        // }
        // $scope.disabledGetReport = false;
        var $tanggal=$scope.CompletenessDataForm.tanggal;
        var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
        if ($scope.CompletenessDataForm.area == null ||$scope.CompletenessDataForm.area == undefined){
            $scope.CompletenessDataForm.area = '-';
        } 
        if ($scope.CompletenessDataForm.dealer == null || $scope.CompletenessDataForm.dealer == undefined || $scope.CompletenessDataForm.dealer == ""){
            $scope.CompletenessDataForm.dealer = '-';
        } 
        if ($scope.CompletenessDataForm.cabang == null ||$scope.CompletenessDataForm.cabang == undefined ||$scope.CompletenessDataForm.cabang == ""){
            $scope.CompletenessDataForm.cabang = '-';
        }  
        if ($scope.CompletenessDataForm.jenispelanggan == null || $scope.CompletenessDataForm.jenispelanggan == undefined||$scope.CompletenessDataForm.jenispelanggan == ""){
            $scope.CompletenessDataForm.jenispelanggan = '-';
        }  
        var dataReport = {
            'jenispelanggan': $scope.CompletenessDataForm.jenispelanggan,
            'dealer': $scope.CompletenessDataForm.dealer,
            'cabang': $scope.CompletenessDataForm.cabang,
            'periode':$filtertanggal,
            'area': $scope.CompletenessDataForm.area,};

        CompletenessDataFactory.getSummaryExport(dataReport);
    }

    $scope.areaId = function(selected){
        $scope.CompletenessDataForm.dealer = null;
        $scope.dataDealer = [];
        $scope.CompletenessDataForm.cabang = null;
        $scope.dataCabang = [];
        if (selected != undefined){
            console.log('value area', selected.areaId);
            CompletenessDataFactory.getDataDealer(selected.areaId).then(function (response) {
                console.log('Response BEDealer:', response.data);
                $scope.dataDealer = response.data;
                $scope.dealer=response.data[0].dealerCode;
              })
        }
    }

    $scope.dealerCode = function(selected){
        $scope.CompletenessDataForm.cabang = null;
        $scope.dataCabang = [];
        if (selected != undefined){
            console.log('value dealer', selected.dealerID);
            if ($scope.CompletenessDataForm.area == null || $scope.CompletenessDataForm.area == undefined){
                $scope.CompletenessDataForm.area = 0
            }
            CompletenessDataFactory.getDataCabang(selected.dealerID, $scope.CompletenessDataForm.area).then(function (response) {
                console.log('Response BECabang:', response.data);
                $scope.dataCabang = response.data;
                $scope.DisabledCabang();
                $scope.cabang=response.data[0].cabangID;
              })
        }
    }
    
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
    });
    // //----------------------------------
    // // Initialization
    // //----------------------------------
    // $scope.user = CurrentUser.user();
    // $scope.filter = {Dealer:null, Cabang:null, DateStart:null, DateThru:null, TipePencarianWO:1, TipePIC:1, PIC:null};
    // $scope.tipePencarianWO = {data:[{text:"Tgl Cetak WO",value:1},{text:"Tgl Billing WO",value:2},{text:"Open WO",value:3}]};
    // $scope.tipePIC = {data:[{text:"SA",value:1},{text:"Teknisi",value:2}]};
    // //----------------------------------
    // // Get Data
    // //----------------------------------
    // var gridData = [];
    
    // $scope.getDealer = [
    // {DealerId:1,DealerName:"Dealer001"},
    // {DealerId:2,DealerName:"Dealer002"}
    // ];

    // $scope.getCabang = [
    // {cabangID:1,CabangName:"Cabang001"},
    // {cabangID:2,CabangName:"Cabang002"}
    // ];

    // $scope.getSA = [
    // {SAId:1,SAName:"SA001"},
    // {SAId:2,SAName:"SA002"}
    // ];

    // $scope.getTeknisi = [
    // {TeknisiId:1,TeknisiName:"Teknisi001"},
    // {TeknisiId:2,TeknisiName:"Teknisi002"}
    // ];


    // $scope.getData = function() {
    //     console.log("getData=>",$scope.filter);
    // }

 
});
