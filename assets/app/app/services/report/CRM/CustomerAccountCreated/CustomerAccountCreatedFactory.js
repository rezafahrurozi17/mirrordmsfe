angular.module('app')
  .factory('CustomerAccountCreatedFactory', function($http ,$httpParamSerializer, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = ''; //'http://localhost:8080';
    return {
             getDataDealer: function() {
              var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
              return res;},
            getDataCabang: function() {
              var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
              return res;},
            getSummary: function(data) {
              console.log('Data Report:', data);

              var res = $http.get(serverURL + '/api/rpt/C_CustomerReport/'+ data.dealer +'/'+ data.cabang +'/'+ data.no_polisi +'/'+ data.advance +'/'+ data.filter1 +'/'+ data.value1a +'/'+ data.value1b +'/'+ data.filter2 +'/'+ data.value2a +'/'+ data.value2b +'/'+ data.filter3 +'/'+ data.value3a +'/'+ data.value3b);
                return res;
            },
            getVModel: function() {
              var res =  $http.get('/api/ct/GetCVehicleModel/');
              // var resUsr =  $http.get('/api/as/VehicleUser');
              // console.log("resSvc Model factory=>", res);
              return res;
            },
            getVehicleType: function(VehicleModelId) {
              var res =  $http.get('/api/ct/GetCVehicleTypeById/'+ VehicleModelId);
              // var resUsr =  $http.get('/api/as/VehicleUser');
              // console.log("resSvc Type factory=>", res);
              return res;
            },
            
            getExport: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/C_CustomerReport?dealer='+ data.dealer +'&cabang='+ data.cabang +'&no_polisi='+  data.no_polisi +'&advance='+ data.advance +'&filter1='+ data.filter1 +'&value1a='+ data.value1a +'&value1b='+ data.value1b +'&filter2='+ data.filter2 +  '&value2a='+ data.value2a +'&value2b='+ data.value2b +'&filter3='+ data.filter3 +'&value3a='+ data.value3a +'&value3b='+ data.value3b);
            },

            getCustomerVehicleList: function(nopol) {
              return $http.get('/api/crm/GetCustomerListFilter/' + '-' + '/' + nopol + '/' + '-' + '/' + '-' + '/' + '-' + '/' + '-' + '/' + '-' + '/' + '-');
            },
    }
  });
