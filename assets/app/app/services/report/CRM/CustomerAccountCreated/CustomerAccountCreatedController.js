angular.module('app')
    .controller('CustomerAccountCreatedController', function($scope,$http, CurrentUser, CustomerAccountCreatedFactory  ,$timeout,$q,bsAlert,QueueCategory,$q, $filter) {
    console.log('Customer Account Created Controller');
     //----------------------------------
    // Initialization
    //----------------------------------
    $scope.VehicleModelId = [];

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.dataSum = true;
    });
    $scope.init = function(){
    $scope.status = true;
    }
  
    $scope.changeStatus = function(){
    $scope.status = !$scope.status;
    }
    var periode=new Date();
    $scope.fModelKendaraan =[];
    $scope.advFilter1=[
        {filterId:0,filterName:""},
        {filterId:1,filterName:"Jenis Pelanggan"},
        {filterId:2,filterName:"Umur"},
        {filterId:3,filterName:"Jenis Kelamin"},
        {filterId:4,filterName:"Status Pernikahan"},
        {filterId:5,filterName:"Model Kendaraan"},
        {filterId:6,filterName:"Tipe Kendaraan"},
        {filterId:7,filterName:"Periode Pembelian"},
        {filterId:8,filterName:"Periode Servis"},
        {filterId:9,filterName:"Masa Berlaku STNK"}
    ];
    $scope.advFilter2=[];
    $scope.advFilter3=[];
    $scope.fJenisPelanggan=[
        {jenis:"Individu"},
        {jenis:"Perusahaan"},
        {jenis:"Yayasan"},
        {jenis:"Pemerintahan"}
    ];
    $scope.filter = {Dealer:null, Cabang:null, inputa1:null };
    $scope.filter.adv1=0;
    $scope.filter.adv2=0;
    $scope.filter.adv3=0;
    $scope.f1 = {}
    $scope.f2 = {}
    $scope.f3 = {}
    var gridData = [];
    console.log('filter ', $scope.filter1, $scope.filter2, $scope.filter3);
    CustomerAccountCreatedFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
      })
    CustomerAccountCreatedFactory.getDataCabang().then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
    $scope.DisabledCabang = function() {
        if ($scope.dataCabang.length > 1){
            $scope.Disabled = false;
            console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
        }
        else{
            $scope.Disabled =true;
            console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
        }
    };
    if ($scope.filter.inputa1 = null){
        $scope.filter.inputa1 = '-';
    }
    $scope.getReport = function(){
        $scope.status = true;
          var dataReport = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'no_polisi': $scope.filter.inputa1,
            'filter1':$scope.filter.adv1,
            'filter2':$scope.filter.adv2,
            'filter3':$scope.filter.adv3,
            'advance':$scope.status,
            'value1a':'-',
            'value1b':'-',
            'value2a':'-',
            'value2b':'-',
            'value3a':'-',
            'value3b':'-'
        };

        switch ($scope.filter.adv1){
            case '1': dataReport.value1a=$scope.f1.jenispelanggan;break;
            case '2': 
                dataReport.value1a=$scope.f1.umur1;
                dataReport.value1b=$scope.f1.umur2;break;
            case '3': dataReport.value1a=$scope.f1.jeniskelamin;break;
            case '4': dataReport.value1a=$scope.f1.statuspernikahan;break;
            case '5': dataReport.value1a=$scope.f1.modelkendaraan;break;
            case '6': dataReport.value1a=$scope.f1.tipekendaraan;break;
            case '7': 
                var $tanggalbeli1=$scope.f1.pembelian_from;
                var $filtertgl1=$tanggalbeli1.getFullYear()+'-'+($tanggalbeli1.getMonth()+1)+'-'+$tanggalbeli1.getDate();
                var $tanggalbeli2=$scope.f1.pembelian_to;
                var $filtertgl2=$tanggalbeli2.getFullYear()+'-'+($tanggalbeli2.getMonth()+1)+'-'+$tanggalbeli2.getDate();
                dataReport.value1a=$filtertgl1;
                dataReport.value1b=$filtertgl2;break;
            case '8': 
                var $tanggal1=$scope.f1.servis_from;
                var $filtertgl1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
                var $tanggal2=$scope.f1.servis_to;
                var $filtertgl2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
                dataReport.value1a=$filtertgl1;
                dataReport.value1b=$filtertgl2;break;
            case '9': 
                var $tanggal1=$scope.f1.masaberlakuSTNK;
                var $filtertgl1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();                    
                dataReport.value1a=$filtertgl1;break;
        }
        switch ($scope.filter.adv2){
            case '1': dataReport.value2a=$scope.f2.jenispelanggan;break;
            case '2': 
                dataReport.value2a=$scope.f2.umur1;
                dataReport.value2b=$scope.f2.umur2;break;
            case '3': dataReport.value2a=$scope.f2.jeniskelamin;break;
            case '4': dataReport.value2a=$scope.f2.statuspernikahan;break;
            case '5': dataReport.value2a=$scope.f2.modelkendaraan;break;
            case '6': dataReport.value2a=$scope.f2.tipekendaraan;break;
            case '7': 
                var $tanggalbeli1=$scope.f2.pembelian_from;
                var $filtertgl1=$tanggalbeli1.getFullYear()+'-'+($tanggalbeli1.getMonth()+1)+'-'+$tanggalbeli1.getDate();
                var $tanggalbeli2=$scope.f2.pembelian_to;
                var $filtertgl2=$tanggalbeli2.getFullYear()+'-'+($tanggalbeli2.getMonth()+1)+'-'+$tanggalbeli2.getDate();
                dataReport.value2a=$filtertgl1;
                dataReport.value2b=$filtertgl2;break;
            case '8': 
                var $tanggal1=$scope.f2.servis_from;
                var $filtertgl1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
                var $tanggal2=$scope.f2.servis_to;
                var $filtertgl2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
                dataReport.value2a=$filtertgl1;
                dataReport.value2b=$filtertgl2;break;
            case '9': 
                var $tanggal1=$scope.f2.masaberlakuSTNK;
                var $filtertgl1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();                    
                dataReport.value2a=$filtertgl1;break;
        }
        switch ($scope.filter.adv3){
            case '1': dataFilter.value3a=$scope.f3.jenispelanggan;break;
            case '2': 
                dataReport.value3a=$scope.f3.umur1;
                dataReport.value3b=$scope.f3.umur2;break;
            case '3': dataReport.value3a=$scope.f3.jeniskelamin;break;
            case '4': dataReport.value3a=$scope.f3.statuspernikahan;break;
            case '5': dataReport.value3a=$scope.f3.modelkendaraan;break;
            case '6': dataReport.value3a=$scope.f3.tipekendaraan;break;
            case '7': 
                var $tanggalbeli1=$scope.f3.pembelian_from;
                var $filtertgl1=$tanggalbeli1.getFullYear()+'-'+($tanggalbeli1.getMonth()+1)+'-'+$tanggalbeli1.getDate();
                var $tanggalbeli2=$scope.f3.pembelian_to;
                var $filtertgl2=$tanggalbeli2.getFullYear()+'-'+($tanggalbeli2.getMonth()+1)+'-'+$tanggalbeli2.getDate();
                dataReport.value3a=$filtertgl1;
                dataReport.value3b=$filtertgl2;break;
            case '8': 
                var $tanggal1=$scope.f3.servis_from;
                var $filtertgl1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
                var $tanggal2=$scope.f3.servis_to;
                var $filtertgl2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
                dataReport.value3a=$filtertgl1;
                dataReport.value3b=$filtertgl2;break;
            case '9': 
                var $tanggal1=$scope.f3.masaberlakuSTNK;
                var $filtertgl1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();                    
                dataReport.value3a=$filtertgl1;break;
        }
                if (dataReport.dealer == null || dataReport.dealer == undefined){
                    dataReport.dealer = '-'
                }
                if (dataReport.cabang == null || dataReport.cabang == undefined){
                    dataReport.cabang = '-'
                }
                if (dataReport.no_polisi == null || dataReport.no_polisi == undefined || dataReport.no_polisi == ""){
                    dataReport.no_polisi = '-'
                }
                if (dataReport.filter1 == null || dataReport.filter1 == undefined){
                    dataReport.filter1 = '-'
                }
                if (dataReport.filter2 == null || dataReport.filter2 == undefined){
                    dataReport.filter2 = '-'
                }
                if (dataReport.filter3 == null || dataReport.filter3 == undefined){
                    dataReport.filter3 = '-'
                }
                if (dataReport.advance == null || dataReport.advance == undefined){
                    dataReport.advance = '-'
                }
                if (dataReport.value1a == null || dataReport.value1a == undefined){
                    dataReport.value1a = '-'
                }
                if (dataReport.value1b == null || dataReport.value1b == undefined){
                    dataReport.value1b = '-'
                }
                if (dataReport.value2a == null || dataReport.value2a == undefined){
                    dataReport.value2a = '-'
                }
                if (dataReport.value2b == null || dataReport.value2b == undefined){
                    dataReport.value2b = '-'
                }
                if (dataReport.value3a == null || dataReport.value3a == undefined){
                    dataReport.value3a = '-'
                }
                if (dataReport.value3b == null || dataReport.value3b == undefined){
                    dataReport.value3b = '-'
                }
                
            CustomerAccountCreatedFactory.getExport(dataReport);
    }
        $scope.getData = function(){
        $scope.status = false;
        $scope.TotalToyotaID = '';
        $scope.TotalKendaraan = '';
        $scope.TotalNonToyotaID = '';
            var dataFilter = {
                        
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'no_polisi': $scope.filter.inputa1,
                'filter1':$scope.filter.adv1,
                'filter2':$scope.filter.adv2,
                'filter3':$scope.filter.adv3,
                'advance':$scope.status,
                'value1a':null,
                'value1b':null,
                'value2a':null,
                'value2b':null,
                'value3a':null,
                'value3b':null
            };
            console.log("$scope.f1 ====",$scope.f1);
            console.log("$scope.f2 ====",$scope.f2);
            console.log("$scope.f3 ====",$scope.f3);

            switch ($scope.filter.adv1){
                case '1': dataFilter.value1a=$scope.f1.jenispelanggan;break;
                case '2': 
                    dataFilter.value1a=$scope.f1.umur1;
                    dataFilter.value1b=$scope.f1.umur2;break;
                case '3': dataFilter.value1a=$scope.f1.jeniskelamin;break;
                case '4': dataFilter.value1a=$scope.f1.statuspernikahan;break;
                case '5': dataFilter.value1a=$scope.f1.modelkendaraan;break;
                case '6': dataFilter.value1a=$scope.f1.tipekendaraan;break;
                case '7': 
                    var $tanggalbeli1=$scope.f1.pembelian_from;
                    var $filtertgl1=$tanggalbeli1.getFullYear()+'-'+($tanggalbeli1.getMonth()+1)+'-'+$tanggalbeli1.getDate();
                    var $tanggalbeli2=$scope.f1.pembelian_to;
                    var $filtertgl2=$tanggalbeli2.getFullYear()+'-'+($tanggalbeli2.getMonth()+1)+'-'+$tanggalbeli2.getDate();
                    dataFilter.value1a=$filtertgl1;
                    dataFilter.value1b=$filtertgl2;break;
                case '8': 
                    var $tanggal1=$scope.f1.servis_from;
                    var $filtertgl1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
                    var $tanggal2=$scope.f1.servis_to;
                    var $filtertgl2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
                    dataFilter.value1a=$filtertgl1;
                    dataFilter.value1b=$filtertgl2;break;
                case '9': 
                    var $tanggal1=$scope.f1.masaberlakuSTNK;
                    var $filtertgl1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();                    
                    dataFilter.value1a=$filtertgl1;break;
            }
            switch ($scope.filter.adv2){
                case '1': dataFilter.value2a=$scope.f2.jenispelanggan;break;
                case '2': 
                    dataFilter.value2a=$scope.f2.umur1;
                    dataFilter.value2b=$scope.f2.umur2;break;
                case '3': dataFilter.value2a=$scope.f2.jeniskelamin;break;
                case '4': dataFilter.value2a=$scope.f2.statuspernikahan;break;
                case '5': dataFilter.value2a=$scope.f2.modelkendaraan;break;
                case '6': dataFilter.value2a=$scope.f2.tipekendaraan;break;
                case '7': 
                    var $tanggalbeli1=$scope.f2.pembelian_from;
                    var $filtertgl1=$tanggalbeli1.getFullYear()+'-'+($tanggalbeli1.getMonth()+1)+'-'+$tanggalbeli1.getDate();
                    var $tanggalbeli2=$scope.f2.pembelian_to;
                    var $filtertgl2=$tanggalbeli2.getFullYear()+'-'+($tanggalbeli2.getMonth()+1)+'-'+$tanggalbeli2.getDate();
                    dataFilter.value2a=$filtertgl1;
                    dataFilter.value2b=$filtertgl2;break;
                case '8': 
                    var $tanggal1=$scope.f2.servis_from;
                    var $filtertgl1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
                    var $tanggal2=$scope.f2.servis_to;
                    var $filtertgl2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
                    dataFilter.value2a=$filtertgl1;
                    dataFilter.value2b=$filtertgl2;break;
                case '9': 
                    var $tanggal1=$scope.f2.masaberlakuSTNK;
                    var $filtertgl1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();                    
                    dataFilter.value2a=$filtertgl1;break;
            }
            switch ($scope.filter.adv3){
                case '1': dataFilter.value3a=$scope.f3.jenispelanggan;break;
                case '2': 
                    dataFilter.value3a=$scope.f3.umur1;
                    dataFilter.value3b=$scope.f3.umur2;break;
                case '3': dataFilter.value3a=$scope.f3.jeniskelamin;break;
                case '4': dataFilter.value3a=$scope.f3.statuspernikahan;break;
                case '5': dataFilter.value3a=$scope.f3.modelkendaraan;break;
                case '6': dataFilter.value3a=$scope.f3.tipekendaraan;break;
                case '7': 
                    var $tanggalbeli1=$scope.f3.pembelian_from;
                    var $filtertgl1=$tanggalbeli1.getFullYear()+'-'+($tanggalbeli1.getMonth()+1)+'-'+$tanggalbeli1.getDate();
                    var $tanggalbeli2=$scope.f3.pembelian_to;
                    var $filtertgl2=$tanggalbeli2.getFullYear()+'-'+($tanggalbeli2.getMonth()+1)+'-'+$tanggalbeli2.getDate();
                    dataFilter.value3a=$filtertgl1;
                    dataFilter.value3b=$filtertgl2;break;
                case '8': 
                    var $tanggal1=$scope.f3.servis_from;
                    var $filtertgl1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
                    var $tanggal2=$scope.f3.servis_to;
                    var $filtertgl2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
                    dataFilter.value3a=$filtertgl1;
                    dataFilter.value3b=$filtertgl2;break;
                case '9': 
                    var $tanggal1=$scope.f3.masaberlakuSTNK;
                    var $filtertgl1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();                    
                    dataFilter.value3a=$filtertgl1;break;
            }
                
                if (dataFilter.dealer == null || dataFilter.dealer == undefined){
                    dataFilter.dealer = '-'
                }
                if (dataFilter.cabang == null || dataFilter.cabang == undefined){
                    dataFilter.cabang = '-'
                }
                if (dataFilter.no_polisi == null || dataFilter.no_polisi == undefined || dataFilter.no_polisi == ""){
                    dataFilter.no_polisi = '-'
                    $scope.dataSum = true
                } else {
                    CustomerAccountCreatedFactory.getCustomerVehicleList(dataFilter.no_polisi).then(function (response) {
                        $scope.datasumGet = []
                        for (var i=0 ; i<response.data.Result.length ; i++) {
                            if (response.data.Result[i].StatusCode == 1 && response.data.Result[i].CurrentStatus == 1) {
                                $scope.datasumGet = response.data.Result[i]
                            }
                        }
                        var dataGrid = []
                        var objectGrid = {}
                        if ($scope.datasumGet.length !== 0) {
                            if ($scope.datasumGet.CustomerList.CustomerTypeId == 1 || $scope.datasumGet.CustomerList.CustomerTypeId == 3) {
                                for (var i = 0; i<$scope.datasumGet.CustomerList.CustomerListPersonal.length; i++) {
                                    if ($scope.datasumGet.CustomerList.CustomerListPersonal[i].StatusCode == 1) {
                                        objectGrid.Name = $scope.datasumGet.CustomerList.CustomerListPersonal[i].CustomerName
                                        objectGrid.TipePelanggan = 'Individu'
                                        objectGrid.HP = $scope.datasumGet.CustomerList.CustomerListPersonal[i].Handphone1
                                    }
                                }
                                for  (var j = 0; j<$scope.datasumGet.CustomerList.CustomerAddress.length; j++) {
                                    if ($scope.datasumGet.CustomerList.CustomerAddress[j].StatusCode == 1) {
                                        objectGrid.Alamat = $scope.datasumGet.CustomerList.CustomerAddress[j].Address
                                        objectGrid.Tel = $scope.datasumGet.CustomerList.CustomerAddress[j].Phone1
                                        if ($scope.datasumGet.CustomerList.CustomerAddress[j].MainAddress == true) {
                                            objectGrid.Alamat = $scope.datasumGet.CustomerList.CustomerAddress[j].Address
                                            objectGrid.Tel = $scope.datasumGet.CustomerList.CustomerAddress[j].Phone1
                                        }
                                    }
                                }
                            } else {
                                if ($scope.datasumGet.CustomerList.CustomerTypeId !== null) {
                                    for (var i = 0; i<$scope.datasumGet.CustomerList.CustomerListInstitution.length; i++) {
                                        if ($scope.datasumGet.CustomerList.CustomerListInstitution[i].StatusCode == 1) {
                                            objectGrid.Name = $scope.datasumGet.CustomerList.CustomerListInstitution[i].Name
                                            objectGrid.TipePelanggan = 'Institusi'
                                            objectGrid.HP = $scope.datasumGet.CustomerList.CustomerListInstitution[i].PICHp
                                        }
                                    }
                                    for  (var j = 0; j<$scope.datasumGet.CustomerList.CustomerAddress.length; j++) {
                                        if ($scope.datasumGet.CustomerList.CustomerAddress[j].StatusCode == 1) {
                                            objectGrid.Alamat = $scope.datasumGet.CustomerList.CustomerAddress[j].Address
                                            objectGrid.Tel = $scope.datasumGet.CustomerList.CustomerAddress[j].Phone1
                                            if ($scope.datasumGet.CustomerList.CustomerAddress[j].MainAddress == true) {
                                                objectGrid.Alamat = $scope.datasumGet.CustomerList.CustomerAddress[j].Address
                                                objectGrid.Tel = $scope.datasumGet.CustomerList.CustomerAddress[j].Phone1
                                            }
                                        }
                                    }
                                }
                            }
                            if ($scope.datasumGet.VehicleList.isNonTAM == 0) {
                                objectGrid.Model = $scope.datasumGet.VehicleList.MVehicleTypeColor.MVehicleType.MVehicleModel.VehicleModelName
                                objectGrid.Type = $scope.datasumGet.VehicleList.MVehicleTypeColor.MVehicleType.Description
                                objectGrid.LicensePlate = $scope.datasumGet.VehicleList.LicensePlate
                                objectGrid.NoRangka = $scope.datasumGet.VehicleList.VIN
                                objectGrid.STNK =  $filter('date')($scope.datasumGet.VehicleList.STNKDate, 'yyyy-MM-dd');
                                //objectGrid.CabangBeli = $scope.datasumGet.CustomerList.CustomerListPersonal[i].
                                objectGrid.TanggalBeli =  $filter('date')($scope.datasumGet.VehicleList.DECDate, 'yyyy-MM-dd');
                                //objectGrid.ServiceTerakhir = $scope.datasumGet.CustomerList.CustomerListPersonal[i].
                            } else {
                                objectGrid.Model = $scope.datasumGet.VehicleList.VehicleListNonTAM[0].ModelName
                                objectGrid.Type = $scope.datasumGet.VehicleList.VehicleListNonTAM[0].ModelType
                                objectGrid.LicensePlate = $scope.datasumGet.VehicleList.LicensePlate
                                objectGrid.NoRangka = $scope.datasumGet.VehicleList.VIN
                                objectGrid.STNK = $filter('date')($scope.datasumGet.VehicleList.STNKDate, 'yyyy-MM-dd');
                                //objectGrid.CabangBeli = $scope.datasumGet.CustomerList.CustomerListPersonal[i].
                                objectGrid.TanggalBeli = $filter('date')($scope.datasumGet.VehicleList.DECDate, 'yyyy-MM-dd');
                                //objectGrid.ServiceTerakhir = $scope.datasumGet.CustomerList.CustomerListPersonal[i].
                            }
                            dataGrid.push(objectGrid)
                        }
                        $scope.gridSummary.data = dataGrid
                    });
                    $scope.dataSum = false
                }
                if (dataFilter.filter1 == null || dataFilter.filter1 == undefined){
                    dataFilter.filter1 = '-'
                }
                if (dataFilter.filter2 == null || dataFilter.filter2 == undefined){
                    dataFilter.filter2 = '-'
                }
                if (dataFilter.filter3 == null || dataFilter.filter3 == undefined){
                    dataFilter.filter3 = '-'
                }
                if (dataFilter.advance == null || dataFilter.advance == undefined){
                    dataFilter.advance = '-'
                }
                if (dataFilter.value1a == null || dataFilter.value1a == undefined){
                    dataFilter.value1a = '-'
                }
                if (dataFilter.value1b == null || dataFilter.value1b == undefined){
                    dataFilter.value1b = '-'
                }
                if (dataFilter.value2a == null || dataFilter.value2a == undefined){
                    dataFilter.value2a = '-'
                }
                if (dataFilter.value2b == null || dataFilter.value2b == undefined){
                    dataFilter.value2b = '-'
                }
                if (dataFilter.value3a == null || dataFilter.value3a == undefined){
                    dataFilter.value3a = '-'
                }
                if (dataFilter.value3b == null || dataFilter.value3b == undefined){
                    dataFilter.value3b = '-'
                }

            CustomerAccountCreatedFactory.getSummary(dataFilter).then(function (response) {
                console.log('Response BE Filter:', response.data); 
                $scope.TotalToyotaID = response.data[0].denganToyotaID;
                $scope.TotalKendaraan = response.data[0].totalKendaraan;
                $scope.TotalNonToyotaID = response.data[0].tanpaToyotaID;

            });
            // var summary= {
            //     'TotalToyotaID': $scope.TotalToyotaID,
            //     'TotalKendaraan': $scope.TotalKendaraan,
            //     'cabangSummary': $scope.Cabang
            // };

        }  
        $scope.changeOption = function(param, type){
            console.log('prama',param);
            // $scope.checkArray(param, type);
            if(param == '0' && type == 1){
                $scope.filter.adv2 = 0;
                $scope.filter.adv3 = 0;
            }else if(param == '0' && type == 2){
                $scope.filter.adv3 = 0;
            }
            // if($scope.filter.adv1 == $scope.filter.adv2 ){
            //     $scope.filter.adv2 = 0;
            //     $scope.filter.adv3 = 0;
            // }
            if((param == '5'|| param == '6') &&  $scope.fModelKendaraan.length  == 0 ){
                // panggil service
                CustomerAccountCreatedFactory.getVModel().then(function (response){
                    console.log('responsenya model==>', response.data);
                    _.map(response.data.Result,function(val){
                        val.modelId = val.VehicleModelId;
                        val.modelName = val.VehicleModelName;
                    })
                    $scope.fModelKendaraan = response.data.Result;
                })
            }
        }
        $scope.selectedModel = function(paramId, type){
            CustomerAccountCreatedFactory.getVehicleType(paramId).then(function(response){
                _.map(response.data.Result,function(val){
                    val.tipeId = val.VehicleTypeId;
                    val.tipeName = val.Description;
                    val.tipeName2 =  val.KatashikiCode;
                    val.tipeName3 = val.SuffixCode;
                })
                if(type == 1){
                    $scope.fTipeKendaraan1 = response.data.Result;
                }else if(type == 2){
                    $scope.fTipeKendaraan2 = response.data.Result;
                }else{
                    $scope.fTipeKendaraan3 = response.data.Result;
                }
            });
        }
        $scope.checkArray = function(param, type){
            var tmpArray  = angular.copy($scope.advFilter1);
            if(type == 1){
                $scope.advFilter2 = [];
                $scope.advFilter3 = [];
                for(var i in tmpArray){
                    if(tmpArray[i].filterId !== parseInt(param)){
                        $scope.advFilter2.push(tmpArray[i]);
                    }
                }
            }else if(type == 2){
                $scope.advFilter3 = [];
                for(var i in $scope.advFilter2){
                    if($scope.advFilter2[i].filterId !== parseInt(param)){
                        $scope.advFilter3.push($scope.advFilter2[i]);
                    }
                }
            }
        }


        $scope.gridSummary = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            enableFiltering: true,
            paginationPageSizes: [250, 500, 1000],
            paginationPageSize: 250,
            onRegisterApi: function(gridApi) {
                $scope.gridApiSummary = gridApi;
            },
            columnDefs: [{
                    name: 'Nama',
                    field: 'Name',
                    width: '10%'
                },
                {
                    name: 'Tipe Pelanggan',
                    field: 'TipePelanggan',
                    width: '10%'
                },
                {
                    name: 'Model',
                    field: 'Model',
                    width: '10%'
                },
                {
                    name: 'Type',
                    field: 'Type',
                    width: '10%'
                },
                {
                    name: 'No.Polisi',
                    displayName: 'No. Polisi',
                    field: 'LicensePlate',
                    width: '10%'
                },
                {
                    name: 'No. Rangka',
                    field: 'NoRangka',
                    width: '10%'
                },
                {
                    name: 'Masa Berlaku STNK',
                    field: 'STNK',
                    width: '10%'
                },
                // {
                //     name: 'Cabang Beli',
                //     field: 'CabangBeli',
                //     width: '10%'
                // },
                {
                    name: 'Tanggal Beli',
                    field: 'TanggalBeli',
                    width: '10%'
                },
                // {
                //     name: 'Service Terakhir',
                //     field: 'ServiceTerakhir',
                //     width: '10%'
                // },
                {
                    name: 'Alamat',
                    field: 'Alamat',
                    width: '25%'
                },
                {
                    name: 'Telepon',
                    field: 'Tel',
                    width: '10%'
                },
                {
                    name: 'No. HP',
                    field: 'HP',
                    width: '10%'
                },
            ]
            
        };


        $scope.getTableHeightgridSummary = function() {
            var rowHeight = 30; // your row height
            var headerHeight = 50; // your header height
            var filterHeight = 40; // your filter height
            var pageSizegridSummary = $scope.gridGateIn.paginationPageSize;

            if ($scope.gridGateIn.paginationPageSize > $scope.gridGateIn.data.length) {
                pageSizegridSummary = $scope.gridGateIn.data.length;
            }
            if (pageSizegridSummary < 4) {
                pageSizegridSummary = 3;
            }

            return {
                height: (pageSizegridSummary * rowHeight + headerHeight) + 50 + "px"
            };

        };

});

    
    
  

