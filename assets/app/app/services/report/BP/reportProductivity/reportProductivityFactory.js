angular.module('app')
  .factory('ReportBPProductivity', function($http, CurrentUser,$q) {
    console.log('Service Productivity BP Loaded...');
    var serverURL ='';// 'http://localhost:8080';
    var currentUser = CurrentUser.user();
    return {
            getDataDealer: function() {
              var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
              return res;},
            getDataCabang: function() {
              var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
              return res;},
            getDataCabangHONew: function(data) {
              console.log('Data Get Cabang:', data);
              var res=$http.get(serverURL + '/api/rpt/GetCabangHONew/'+ data.areaId + '/' +  data.DealerId + '/' + data.kategori);
              return res;},
            getDataSatellite: function() {
            var res=$http.get(serverURL + '/api/rpt/Satellite/');
            return res;},            
            getDataAreaTAM: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaTAM/');
            return res;},
            getDataAreaDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaDealer/');
            return res;},
            getDataAreaDealerHO: function(data) {
              var res=$http.get(serverURL + '/api/rpt/AreaDealerHONew/'+ data.isGR );
              return res;},
            getSummaryHOTP: function(data) {
              console.log('Data Report:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_TechnicianProductivity_BPHO/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.tamarea +'/'+ data.dealerarea);
                return res;
            },

            getExportHOTP: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_TechnicianProductivity_BPHO?TamArea='+ data.tamarea +'&DealerArea='+ data.dealerarea +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir +'&kategori='+ data.kategori +'&Jenis='+ data.jenis +'&satelite='+ data.satellite);
            },
            getSummaryHOCBP: function(data) {
              console.log('Data Report:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_CombiBoothProductivity_BPHO/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.tamarea +'/'+ data.dealerarea +'/'+ data.satellite +'/'+ data.kategori);
                return res;
            },

            getExportHOCBP: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_CombiBoothProductivity_BPHO?TamArea='+ data.tamarea +'&DealerArea='+ data.dealerarea +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir +'&kategori='+ data.kategori +'&satelite='+ data.satellite);
            },
            getSummaryHOPS: function(data) {
              console.log('Data Report:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_ProductivityStall_BPHO/'+ data.tamarea +'/'+ data.dealer +'/'+ data.dealerarea +'/'+ data.cabang +'/'+ data.kategori +'/'+ data.periodeawal +'/'+ data.periodeakhir);
                return res;
            },

            getExportHOPS: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_ProductivityStall_BPHO?tamarea='+ data.tamarea +'&dealer='+ data.dealer +'&DealerArea='+ data.dealerarea +'&cabang='+ data.cabang +'&kategori='+ data.kategori +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir +'&satelite='+ data.satellite);
            },

  // =================================================================== CABANG ===============================
            getSummaryCabangTP: function(data) {
              console.log('Data Report:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_TechnicianProductivity_BPCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.dealerarea);
                return res;
            },
            getSummaryCabangCBP: function(data) {              
              console.log('Data Report:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_CombiBoothProductivity_BPCAB/'+ data.dealer +'/'+ data.dealerarea +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir);
                return res;
            },
            getExportCabangTP: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_TechnicianProductivity_BPCAB?dealerarea='+ data.dealerarea +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir);
            },
            
            getExportCabangCBP: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_CombiBoothProductivity_BPCAB?dealerarea='+ data.dealerarea +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir);
            },
            getSummaryCabangPS: function(data) {
              console.log('Data Report:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_ProductivityStall_BPCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.dealerarea +'/'+ data.satellite +'/'+ data.kategori);
                return res;
            },
            getExportCabangPS: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_ProductivityStall_BPCAB?DealerArea='+ data.dealerarea +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir +'&kategori='+ data.kategori +'&satelite='+ data.satellite);
            }
    }
  });
