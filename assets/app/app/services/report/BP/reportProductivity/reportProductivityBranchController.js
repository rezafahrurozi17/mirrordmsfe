angular.module('app')
    .controller('ReportProductivityBPController', function($scope,$stateParams, $http, CurrentUser, ReportBPProductivity,$timeout  ) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
        });
        // //----------------------------------
        // // Initialization
        // //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.roleId = $scope.user.RoleId;
        $scope.role=$stateParams.role;
        console.log('role',$scope.role);
        // $scope.filter = {Dealer:null, Cabang:null, KategoriWorkshop:"Seminar", DateStart:null, DateThru:null, satellite:null};
        // //----------------------------------
        // // Get Data
        // //----------------------------------
        
        $scope.formatDate = function(date) {
            var dateOut = new Date(date);
            return dateOut;
        };
        var dateFormat2='dd/MM/yyyy';
        var dateFormat = 'MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var periode=new Date();
        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat2,
            // mode:"'month'",
        };
        $scope.dateOptions2 = {
            startingDay: 1,
            format: dateFormat2,
            // mode:"'month'",
        };
        // //----------------------------------
        // // Get Data
        // //----------------------------------
        $scope.dataDealer = [];
        $scope.dataCabang = [];
        $scope.dataAreaTAM = [];
        $scope.dataAreaDealer = [];
        $scope.dataSatellite = [];
        $scope.filter = {Dealer:null,DealerId:null, Cabang:null, kategori:"Body and Paint", DateStart:periode,DateEnd:periode, satellite:null, dealerarea:0,tamarea:0,isGR:0 };
        ReportBPProductivity.getDataDealer().then(function(response) {
            console.log('Response BEDealer:', response.data);
            $scope.dataDealer = response.data;
            $scope.filter.Dealer=response.data[0].dealerCode;
            $scope.filter.DealerId=response.data[0].dealerID;
            ReportBPProductivity.getDataAreaDealerHO($scope.filter).then(function (response) {
                console.log('ayamtest',response.data)
                $scope.dataGroupDealerHO = response.data
                $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
                $scope.areaId($scope.dataGroupDealerHO[0])
            })
        })
        ReportBPProductivity.getDataCabang().then(function(response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabang = response.data;
            $scope.DisabledCabang();
            $scope.filter.Cabang=response.data[0].cabangID;
        })
        ReportBPProductivity.getDataAreaTAM().then(function(response) {
            console.log('Response AreaTAM:', response.data);
            $scope.dataAreaTAM = response.data;
        })
        ReportBPProductivity.getDataAreaDealer().then(function(response) {
            console.log('Response AreaDealer:', response.data);
            $scope.dataAreaDealer = response.data;
        })
        ReportBPProductivity.getDataAreaDealerHO($scope.filter).then(function(response) {
            console.log('Response AreaDealerHO:', response.data);
            $scope.dataAreaDealerHO = response.data;
            $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
            $scope.areaId($scope.dataAreaDealerHO[0])
        })
        ReportBPProductivity.getDataSatellite().then(function(response) {
            console.log('Response AreaSatellite:', response.data);
            $scope.dataSatellite = response.data;
        })
    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
            $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };


        $scope.areaId = function(row) {
          // $scope.DataDealer = row
            $scope.DataDealer = Object.assign(row,{'kategori': $scope.filter.kategori})
            if($scope.userlogin != 'TAM'){
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
            }else{
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
            }
            console.log('row dealerid', row,$scope.DataDealer);
            ReportBPProductivity.getDataCabangHONew($scope.DataDealer).then(function (response) {
                console.log('Response BECabang:', response.data);
                $scope.dataCabangHO = response.data;
                $scope.DisabledCabang();
                $scope.filter.CabangHO = null
                console.log('kambing',$scope.filter)
                $timeout(function() {
                    $scope.filter.CabangHO = response.data[0].cabangID;
                    console.log('kambing123',$scope.filter.CabangHO)
                }, 100);
            })
        };

        $scope.dealerCode = function(row){
            row = Object.assign(row,{'isGR':$scope.filter.isGR})
            $scope.filter.DealerId = row.dealerID
            ReportBPProductivity.getDataAreaDealerHO(row).then(function (response) {
                console.log('ayam',response.data)
                console.log('rusa',$scope.dataGroupDealer)
                $scope.dataGroupDealer = response.data
                $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
                $scope.areaId($scope.dataGroupDealer[0])
                console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
            })
        }

        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
        $scope.getData = function() {
            if ($scope.role == "Branch") $scope.getSummaryCabang();
            else $scope.getSummaryHO();
            console.log('get data');
        };

        // ============================================================================= HO =======================    
        $scope.getSummaryHO = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataFilter = {
                'kategori': "BP",
                'jenis': "summary",
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tamarea': $scope.filter.tamarea,
                'satellite': $scope.filter.satellite,
                'dealerarea': $scope.filter.dealerarea,
            };

            ReportBPProductivity.getSummaryHOTP(dataFilter).then(function(response) {
                console.log('Response BE TP:', response.data);
                // Technician Productivity
                $scope.HariKerja = response.data[0].harikerja;
                $scope.HariKerja = Math.round($scope.HariKerja * 100) / 100
                $scope.JT = response.data[0].jt; // jamTersedia
                $scope.JT = Math.round($scope.JT * 100) / 100
                $scope.JF = response.data[0].jf; // jamTerjual
                $scope.JF = Math.round($scope.JF * 100) / 100
                $scope.JA = response.data[0].ja; //jamTerpakai
                $scope.JA = Math.round($scope.JA * 100) / 100
                $scope.UE = response.data[0].unitEntry;
                $scope.UE = Math.round($scope.UE * 100) / 100
                $scope.SA = response.data[0].sa;
                $scope.SA = Math.round($scope.SA * 100) / 100

                // // $scope.RUM = response.data[0].teknisi > 0 ? response.data[0].unitEntry/(response.data[0].teknisi*12)*100 : 0;
                // $scope.RUM = response.data[0].sa > 0 ? response.data[0].ue/(response.data[0].sa) : 0;
                // // $scope.RUM = Math.round($scope.RUM * 100) / 100
                // $scope.RUM = parseFloat($scope.RUM).toFixed(2);
                // $scope.TE = response.data[0].ja > 0 ? response.data[0].jf/response.data[0].ja*100 : 0;
                // // $scope.TE = Math.round($scope.TE * 100) / 100
                // $scope.TE = parseFloat($scope.TE).toFixed(2);
                // $scope.OP = response.data[0].jt > 0 ? response.data[0].jf/response.data[0].jt*100 : 0;
                // // $scope.OP = Math.round($scope.OP * 100) / 100
                // $scope.OP = parseFloat($scope.OP).toFixed(2);
                // $scope.LU = response.data[0].jt > 0 ? response.data[0].ja/response.data[0].jt*100 : 0;
                // // $scope.LU = Math.round($scope.LU * 100) / 100
                // $scope.LU = parseFloat($scope.LU).toFixed(2);

                $scope.RUM = parseFloat(response.data[0].rumbp).toFixed(2)
                $scope.TE = parseFloat(response.data[0].tebp).toFixed(2)
                $scope.OP = parseFloat(response.data[0].opbp).toFixed(2)
                $scope.LU = parseFloat(response.data[0].lubp).toFixed(2)

            })

            ReportBPProductivity.getSummaryHOCBP(dataFilter).then(function(response) {
                console.log('Response BE CBP:', response.data);

                $scope.ProductivityCombiBoothReguler = response.data[0].cbRegular;
                $scope.ProductivityCombiBoothReguler = Math.round($scope.ProductivityCombiBoothReguler * 100) / 100
                $scope.TotalStallCombiBoothReguler = response.data[0].totalStallReguler;
                $scope.TotalStallCombiBoothReguler = Math.round($scope.TotalStallCombiBoothReguler * 100) / 100
                $scope.ProductivityCombiBoothTPS = response.data[0].cbtps;
                $scope.ProductivityCombiBoothTPS = Math.round($scope.ProductivityCombiBoothTPS * 100) / 100
                $scope.TotalStallCombiBoothTPS = response.data[0].totalStallTPS;
                $scope.TotalStallCombiBoothTPS = Math.round($scope.TotalStallCombiBoothTPS * 100) / 100
            })
            ReportBPProductivity.getSummaryHOPS(dataFilter).then(function(response) {
                console.log('Response BE PS:', response.data);

                $scope.RPS = response.data[0].unitReguler;
                $scope.RPS = Math.round($scope.RPS * 100) / 100 
                $scope.TotalStallReguler = response.data[0].totalReguler;
                $scope.TotalStallReguler = Math.round($scope.TotalStallReguler * 100) / 100
                $scope.TargetRPSReguler = response.data[0].targetUnitReg;
                $scope.TargetRPSReguler = Math.round($scope.TargetRPSReguler * 100) / 100
                $scope.RPSTPS = response.data[0].unitTPS;
                $scope.RPSTPS = Math.round($scope.RPSTPS * 100) / 100
                $scope.TotalStallTPS = response.data[0].totalTPS;
                $scope.TotalStallTPS = Math.round($scope.TotalStallTPS * 100) / 100
                $scope.TargetRPSTPS = response.data[0].targetUnitTPS;
                $scope.TargetRPSTPS = Math.round($scope.TargetRPSTPS * 100) / 100
            })
        }

        $scope.getReportHOTP = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '/' + ($tanggal.getMonth() + 1) + '/' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '/' + ($tanggalakhir.getMonth() + 1) + '/' + $tanggalakhir.getDate();
            var dataReport = {
                'kategori': "BP",
                'jenis': "export",
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tamarea': $scope.filter.tamarea,
                'satellite': $scope.filter.satellite,
                'dealerarea': $scope.filter.dealerarea,
            };

            ReportBPProductivity.getExportHOTP(dataReport);
        }
        $scope.getReportHOCBP = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '/' + ($tanggal.getMonth() + 1) + '/' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '/' + ($tanggalakhir.getMonth() + 1) + '/' + $tanggalakhir.getDate();
            var dataReport = {
                'kategori': "BP",
                'jenis': "export",
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tamarea': $scope.filter.tamarea,
                'satellite': $scope.filter.satellite,
                'dealerarea': $scope.filter.dealerarea,
            };

            ReportBPProductivity.getExportHOCBP(dataReport);
        }
        $scope.getReportHOPS = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '/' + ($tanggal.getMonth() + 1) + '/' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '/' + ($tanggalakhir.getMonth() + 1) + '/' + $tanggalakhir.getDate();
            var dataReport = {
                'kategori': "BP",
                'jenis': "export",
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tamarea': $scope.filter.tamarea,
                'satellite': $scope.filter.satellite,
                'dealerarea': $scope.filter.dealerarea,
            };

            ReportBPProductivity.getExportHOPS(dataReport);
        }


        // ========================================================================================== HO =================
        $scope.getSummaryCabang = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataFilter = {
                'kategori': "BP",
                'jenis': "summary",
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'satellite': $scope.filter.satellite,
                'dealerarea': $scope.filter.dealerarea,
            };

            ReportBPProductivity.getSummaryCabangTP(dataFilter).then(function(response) {
                console.log('Response BE TP:', response.data);
                // Technician Productivity
                $scope.HariKerja = response.data[0].harikerja;
                $scope.HariKerja = Math.round($scope.HariKerja * 100) / 100
                $scope.JT = response.data[0].jt;
                $scope.JT = Math.round($scope.JT * 100) / 100
                $scope.JF = response.data[0].jf;
                $scope.JF = Math.round($scope.JF * 100) / 100
                $scope.JA = response.data[0].ja;
                $scope.JA = Math.round($scope.JA * 100) / 100
                $scope.UE = response.data[0].ue;
                $scope.UE = Math.round($scope.UE * 100) / 100

                // $scope.RUM = response.data[0].sa > 0 ? response.data[0].ue/(response.data[0].sa) : 0;
                $scope.RUM = response.data[0].sa > 0 ? response.data[0].ue/(response.data[0].sa*12) : 0;
                // $scope.RUM = Math.round($scope.RUM * 100) / 100
                $scope.RUM = parseFloat($scope.RUM).toFixed(2);
                $scope.TE = response.data[0].ja > 0 ? response.data[0].jf/((response.data[0].ja * 100) / 100)*100 : 0;
                // $scope.TE = Math.round($scope.TE * 100) / 100
                $scope.TE = parseFloat($scope.TE).toFixed(2);
                $scope.OP = response.data[0].jt > 0 ? $scope.JF/response.data[0].jt*100 : 0;
                // $scope.OP = Math.round($scope.OP * 100) / 100
                $scope.OP = parseFloat($scope.OP).toFixed(2);
                $scope.LU = response.data[0].jt > 0 ? response.data[0].ja/response.data[0].jt*100 : 0;
                // $scope.LU = parseFloat(($scope.LU * 100) / 100).toFixed(2);
                $scope.LU = parseFloat($scope.LU).toFixed(2);
            })

            ReportBPProductivity.getSummaryCabangCBP(dataFilter).then(function(response) {
                console.log('Response BE CBP:', response.data);

                $scope.ProductivityCombiBoothReguler = response.data[0].cbRegular;
                $scope.ProductivityCombiBoothReguler = Math.round($scope.ProductivityCombiBoothReguler * 100) / 100
                $scope.TotalStallCombiBoothReguler = response.data[0].totalStallCBReguler;
                $scope.TotalStallCombiBoothReguler = Math.round($scope.TotalStallCombiBoothReguler * 100) / 100
                $scope.ProductivityCombiBoothTPS = response.data[0].cbtps;
                $scope.ProductivityCombiBoothTPS = Math.round($scope.ProductivityCombiBoothTPS * 100) / 100
                $scope.TotalStallCombiBoothTPS = response.data[0].totalStallCBTPS;
                $scope.TotalStallCombiBoothTPS = Math.round($scope.TotalStallCombiBoothTPS * 100) / 100
            })

            ReportBPProductivity.getSummaryCabangPS(dataFilter).then(function(response) {
                console.log('Response BE PS:', response.data);

                $scope.RPS = response.data[0].rpsReguler; 
                $scope.RPS = Math.round($scope.RPS * 100) / 100
                $scope.TotalStallReguler = response.data[0].totalStallReguler;
                $scope.TotalStallReguler = Math.round($scope.TotalStallReguler * 100) / 100
                $scope.TargetRPSReguler = response.data[0].targetUnitReg;
                $scope.TargetRPSReguler = Math.round($scope.TargetRPSReguler * 100) / 100
                $scope.RPSTPS = response.data[0].rpsStallTPS; 
                $scope.RPSTPS = Math.round($scope.RPSTPS * 100) / 100
                $scope.TotalStallTPS = response.data[0].totalStallTPS;
                $scope.TotalStallTPS = Math.round($scope.TotalStallTPS * 100) / 100
                $scope.TargetRPSTPS = response.data[0].targetUnitTPS;
                $scope.TargetRPSTPS = Math.round($scope.TargetRPSTPS * 100) / 100
            })
        }

        $scope.getReportCabangTP = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '/' + ($tanggal.getMonth() + 1) + '/' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '/' + ($tanggalakhir.getMonth() + 1) + '/' + $tanggalakhir.getDate();
            var dataReport = {
                'kategori': "BP",
                'jenis': "export",
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'satellite': $scope.filter.satellite,
                'dealerarea': $scope.filter.dealerarea,
            };

            ReportBPProductivity.getExportCabangTP(dataReport);
        }
        $scope.getReportCabangCBP = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '/' + ($tanggal.getMonth() + 1) + '/' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '/' + ($tanggalakhir.getMonth() + 1) + '/' + $tanggalakhir.getDate();
            var dataReport = {
                'kategori': "BP",
                'jenis': "export",
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tamarea': $scope.filter.tamarea,
                'satellite': $scope.filter.satellite,
                'dealerarea': $scope.filter.dealerarea,
            };

            ReportBPProductivity.getExportCabangCBP(dataReport);
        }
        $scope.getReportCabangPS = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '/' + ($tanggal.getMonth() + 1) + '/' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '/' + ($tanggalakhir.getMonth() + 1) + '/' + $tanggalakhir.getDate();
            var dataReport = {
                'kategori': "BP",
                'jenis': "export",
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tamarea': $scope.filter.tamarea,
                'satellite': $scope.filter.satellite,
                'dealerarea': $scope.filter.dealerarea,
            };

            ReportBPProductivity.getExportCabangPS(dataReport);
        }

        // $scope.getReportHO = function(){
        //       var $tanggal=$scope.filter.DateStart;
        //       var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        //     var $tanggalakhir=$scope.filter.DateEnd;
        //     var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
        //       var dataReport = {
        //         'kategori': $scope.filter.kategori,
        //         'jenis': "export",
        //         'dealer': $scope.filter.Dealer,
        //         'cabang': $scope.filter.Cabang,
        //         'periodeakhir': $filtertanggalakhir,
        //         'periodeawal': $filtertanggal,
        //         'tamarea': $scope.filter.tamarea,
        //         'satellite': $scope.filter.satellite,
        //         'dealerarea': $scope.filter.dealerarea,};

        //     ReportBPProductivity.getExportHO(dataReport);
        // }
        // var gridData = [];

        // $scope.getDealer = [
        // {DealerId:1,DealerName:"Dealer001"},
        // {DealerId:2,DealerName:"Dealer002"}
        // ];

        // $scope.getCabang = [
        // {cabangID:1,CabangName:"Cabang001"},
        // {cabangID:2,CabangName:"Cabang002"}
        // ];

        // $scope.getsatellite = [
        // {satelliteId:1,satelliteName:"satellite001"},
        // {satelliteId:2,satelliteName:"satellite002"}
        // ];

        // $scope.getTamArea = [
        // {TamAreaId:1,TamAreaName:"TAM Area 001"},
        // {TamAreaId:2,TamAreaName:"TAM Area 001"}
        // ];

        //  $scope.getGroupDealerArea = [
        // {GroupDealerAreaId:1,GroupDealerAreaName:"Group Dealer Area 1"},
        // {GroupDealerAreaId:2,GroupDealerAreaName:"Group Dealer Area 2"}
        // ];

        // $scope.getData = function() {
        //     console.log("getData=>",$scope.filter);
        // }
    });