angular.module('app')
  .factory('reportOPLTeknisiFactory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = 'https://dms-sit.toyota.astra.co.id'; //'http://localhost:8080';
    return {
            getDataVendorOpl: function () {
                var res = $http.get('/api/as/AS_GetDataVendorOPL');
                //console.log('res=>',res);
                //res.data.Result = null;
                return res;
            },
            getDataDealer: function() {
            var res=$http.get('/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},
            getDataCabang: function() {
            var res=$http.get('/api/rpt/Cabang/'+ currentUser.OrgId);
            return res;},
            getDataPIC: function(data) {
            console.log('data pic',data);
            var res=$http.get('/api/rpt/pic/'+ currentUser.OrgId +'/'+ data);
            return res;},
            getDataSA: function(data) {
            var res=$http.get('/api/rpt/SA/'+ data.kategori + currentUser.OrgId);
            return res;}, 
            getSummaryBP: function(data) {
              console.log('Data Summary:', data);
              // var res = $http.get('/api/rpt/AS_WIPReport_GRBPCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.tipe +'/'+ data.PIC +'/'+ data.picid +'/BP' );
              var res = $http.get('/api/rpt/AS_OPLTEKNISI_BP/' +  data.kategoriDate + '/' + data.dealer + '/' + data.cabang + '/' + data.employeeId + '/' + data.kategoriNama + '/' + data.periodeawal + '/' + data.periodeakhir);
                return res;
            },
            getExportBP: function(data) {
                console.log('Data Export:', data);
                // window.open('/api/rpt/AS_WIPReport_GRBPCAB?dealer='+ data.dealer +'&cabang='+ data.cabang  +'&tipe='+ data.tipe +'&periode_awal='+ data.periodeawal +'&periode_akhir='+ data.periodeakhir +'&pic='+ data.PIC +'&picid='+ data.picid +'&kategori=BP');
                window.open('/api/rpt/AS_OPLTEKNISI_BP?IsWO='+ data.kategoriDate +'&GroupDealerCode='+ data.dealer  +'&OutletId='+ data.cabang +'&EmployeeId='+ data.employeeId +'&IsSA='+ data.kategoriNama +'&StartDate='+ data.periodeawal +'&EndDate='+ data.periodeakhir);
            },
            getSummaryGR: function(data) {
              console.log('Data Summary:', data);
              // var res = $http.get('/api/rpt/AS_WIPReport_GRBPCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.tipe +'/'+ data.PIC +'/'+ data.picid +'/BP' );
              var res = $http.get('/api/rpt/AS_OPLTEKNISI_GR/' +  data.kategoriDate + '/' + data.dealer + '/' + data.cabang + '/' + data.employeeId + '/' + data.kategoriNama + '/' + data.periodeawal + '/' + data.periodeakhir);
                return res;
            },
            getExportGR: function(data) {
              console.log('Data Export:', data);
              // window.open('/api/rpt/AS_WIPReport_GRBPCAB?dealer='+ data.dealer +'&cabang='+ data.cabang  +'&tipe='+ data.tipe +'&periode_awal='+ data.periodeawal +'&periode_akhir='+ data.periodeakhir +'&pic='+ data.PIC +'&picid='+ data.picid +'&kategori=BP');
              window.open('/api/rpt/AS_OPLTEKNISI_GR?IsWO='+ data.kategoriDate +'&GroupDealerCode='+ data.dealer  +'&OutletId='+ data.cabang +'&EmployeeId='+ data.employeeId +'&IsSA='+ data.kategoriNama +'&StartDate='+ data.periodeawal +'&EndDate='+ data.periodeakhir);
            }
    }
  });
