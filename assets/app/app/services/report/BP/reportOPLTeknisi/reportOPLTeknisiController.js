angular.module('app')
    .controller('reportOPLTeknisiController', function($scope, bsNotify, $http, $stateParams, CurrentUser, reportOPLTeknisiFactory) {
        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var periode = new Date();
        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat,

            // mode:"'month'",
        };
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
        });

        

        $scope.summary = {};
        $scope.filter = {DateStart:periode, DateEnd:periode, kategori:"Body and Paint",namaVendor:undefined,namaSA:undefined};


        var today = new Date();
        $scope.maxDate = new Date(today.getFullYear(),today.getMonth(), today.getDate());

        $scope.filterCategoryName =  { data: [
            { text: "Nama SA", value: 1 },
            { text: "Nama Group / Vendor", value: 2 },
        ] };

        $scope.filterCategoryDate =  { data: [
            { text: "Tanggal WO", value: 3 }, 
            { text: "Tanggal OPL/Billing", value: 4 }, 
        ] };
        $scope.dataVendor = [];
        reportOPLTeknisiFactory.getDataVendorOpl().then(function(response) {
            console.log('Response data vendor opl:', response.data.Result);
            $scope.dataVendor[0] = {VendorId:0 , Name:"ALL"};
            for(var a in response.data.Result){
                $scope.dataVendor.push(response.data.Result[a]);
            }
        })
       
        $scope.selectedFilterCategoryName = "Nama SA";
        $scope.selectedFilterCategoryDate = "Tanggal WO";
    
        $scope.changeCategoryFilterName = function(selected){
            $scope.filter.categoryName = parseInt(selected);
            console.log('changeCategoryFilterName ===>',selected);
            if(selected == 1){
                console.log('changeCategoryFilterName by Nama ===>',selected);
                $scope.selectedFilterCategoryName = 'Nama SA';
                // for(var a in $scope.dataSA){
                    // if($scope.dataSA[a].employeeid == 0){
                        $scope.filter.namaSA = 0;
                //     }
                // }
            }else if(selected == 2){
                console.log('changeCategoryFilterName by Nama ===>',selected);
                $scope.selectedFilterCategoryName = 'Nama Group / Vendor';
                
                // for(var a in $scope.dataVendor){
                    // if($scope.dataVendor[a].employeeid == 0){
                        $scope.filter.namaVendor = 0;
                //     }
                // }
            }
        }


        $scope.changeCategoryFilterDate = function(selected){
            $scope.filter.categoryDate = parseInt(selected);
            if(selected == 3){
                console.log('changeCategoryFilterDate by Tanggal ===>',selected);
                $scope.selectedFilterCategoryDate = "Tanggal WO";
            }else if(selected == 4){
                console.log('changeCategoryFilterDate by Tanggal ===>',selected);
                $scope.selectedFilterCategoryDate = "Tanggal OPL/Billing";
            }
        }

        $scope.onSelectSA = function (selected) {
            console.log('onSelectSA ===>',selected);
            console.log('filter.namaSA ->', $scope.filter.namaSA);
        }


        $scope.onSelectVendor = function (selected) {
            console.log('onSelectVendor ===>',selected);
            console.log('filter.namaVendor ->', $scope.filter.namaVendor);
        }



        $scope.changeCategoryFilterName('1');
        $scope.changeCategoryFilterDate('3');

        // //----------------------------------
        // // Get Data
        // //----------------------------------
        // var gridData = [];

        reportOPLTeknisiFactory.getDataDealer().then(function(response) {
            console.log('Response BEDealer:', response.data);
            $scope.dataDealer = response.data;
            $scope.filter.Dealer = response.data[0].dealerCode;
        })
        reportOPLTeknisiFactory.getDataCabang().then(function(response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabang = response.data;
            $scope.DisabledCabang();
            $scope.filter.Cabang = response.data[0].cabangID;
        })

        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length > 1) {
                $scope.Disabled = false;
                console.log('Cabang Enabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            } else {
                $scope.Disabled = true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };

        reportOPLTeknisiFactory.getDataPIC('teknisi ' + $scope.role).then(function(response) {
            console.log('Response BEPIC:', response.data);
            // $scope.dataSA = response.data;
            $scope.dataTeknisi = response.data; // hmm bisa ketukar sa dan teknisi

        });

        reportOPLTeknisiFactory.getDataPIC('service advisor ' + $scope.role).then(function(response) {
            console.log('Response BEPIC:', response.data);
            // $scope.dataTeknisi = response.data;
            $scope.dataSA = response.data; // hmm bisa ketukar sa dan teknisi
        });

        $scope.DisabledTAM = function() {
            if ($scope.userlogin != 'TAM') {
                return true;
                console.log("DisabledTAM TRUE");
            } else {
                return false;
                console.log("DisabledTAM FALSE");
            }
        };

        $scope.changeTipePencarianWO = function(value) {
            // $timeout(function(){
            //console.log('value', value);
            //console.log('$scope.filter===>', $scope.filter);
            if (value == 3 || value == '3') {

                var d = new Date();
                $scope.filter.DateStart = d;
                $scope.filter.DateThru = d;
                $scope.filter.DateEnd = d;
                $scope.filter.TipePencarianWODisable = true;

            } else {

                $scope.filter.TipePencarianWODisable = false;
            }
            // });
        }

        $scope.changeTipePIC = function() {
            // $timeout(function(){
            console.log('tipe pic', $scope.filter.TipePIC);
            if ($scope.filter.TipePIC == 1) {

                console.log("INI SA");
            } else if ($scope.filter.TipePIC == 2) {
                console.log("INI Teknisi");
            }
            // });
        }




        $scope.getData = function() {
            console.log('filterr | getData ===>',$scope.filter)

            if($scope.filter.categoryName == 1){ //Nama SA
                console.log('if 167');
                console.log('nama sa ->', $scope.filter.namaSA);
                if($scope.filter.namaSA === undefined){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Nama SA harus diisi",
                    });
                    console.log('namaSA ga valid');
                }
            }else if($scope.filter.categoryName == 2){ //Nama Vendor
                console.log('if 177');
                if($scope.filter.namaVendor == undefined){
                    console.log('if 179');
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Nama Vendor harus diisi",
                    });
                    console.log('namaVendor ga valid');
                }
            }if($scope.filter.DateEnd == undefined){
                console.log('if 188');
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Tanggal Akhir Periode harus diisi",
                });
                console.log('DateEnd ga valid');
            }else if($scope.filter.DateStart == undefined){
                console.log('if 196');
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Tanggal Awal Periode harus diisi",
                });
                console.log('DateStart ga valid');
            }else{
                console.log('if 204');
                console.log('customValidate valid');
                console.log('role ->', $scope.role);
                if ($scope.role == "GR") $scope.getSummaryGR();
                else $scope.getSummaryBP();
            }
            // console.log('valid nih');
            // if ($scope.role == "GR") $scope.getSummaryGR();
            // else $scope.getSummaryBP();
        }




        $scope.getReport = function() {
            console.log('summary  ===>',$scope.summary);
            if($scope.filter.DateEnd == undefined){
                console.log('if 188');
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Tanggal Akhir Periode harus diisi",
                });
                console.log('DateEnd ga valid');
            }else if($scope.filter.DateStart == undefined){
                console.log('if 196');
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Tanggal Awal Periode harus diisi",
                });
                console.log('DateStart ga valid');
            }else{
                $scope.getReportBP();
                console.log('get data');
            }
        }

        function format1(n, currency) {
            console.log('format1', n, currency);
            return currency == null ? 0 : currency + " " + n.toFixed(0).replace(/./g, function(c, i, a) {
                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
            });
        }
       

        // $scope.getSummaryGR = function() {
        //     var $tanggal = $scope.filter.DateStart;
        //     var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
        //     var $tanggalakhir = $scope.filter.DateEnd;
        //     var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
        //     var dataFilter = {

        //         'dealer': $scope.filter.Dealer,
        //         'cabang': $scope.filter.Cabang,
        //         'periodeakhir': $filtertanggalakhir,
        //         'periodeawal': $filtertanggal,
        //         'tipe': $scope.filter.TipePencarianWO,
        //         'PIC': $scope.filter.TipePIC,
        //         'picid': $scope.filter.PIC
        //     };

        //     reportOPLTeknisiFactory.getSummaryGR(dataFilter).then(function(response) {
        //         console.log('Response BE Filter:', response.data);

        //         $scope.ServiceTotalUnit = response.data[0].unitBS;
        //         $scope.ARTotalUnit = response.data[0].unitAR;
        //         $scope.TotalUnit = $scope.ServiceTotalUnit + $scope.ARTotalUnit;
        //         $scope.ServiceTotalAmount = response.data[0].amountBS == null ? 0 : response.data[0].amountBS;
        //         console.log('ServiceTotalAmount:', $scope.ServiceTotalAmount);
        //         $scope.ServiceTotalAmount = format1($scope.ServiceTotalAmount, '');
        //         $scope.ARTotalAmount = response.data[0].amountAR == null ? 0 : response.data[0].amountAR;
        //         console.log('ARTotalAmount:', $scope.ARTotalAmount);
        //         $scope.ARTotalAmount = format1($scope.ARTotalAmount, '');

        //         //$scope.TotalAmount = $scope.ServiceTotalAmount+$scope.ARTotalAmount;
        //         $scope.TotalAmount = response.data[0].amountBS + response.data[0].amountAR;
        //         console.log('TotalAmount:', $scope.TotalAmount);
        //         $scope.TotalAmount = format1($scope.TotalAmount, '');

        //     })
        // }

        $scope.getSummaryGR = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataFilter = null;
            if($scope.filter.categoryName == 1) { 
                dataFilter = {
                    'kategoriDate': $scope.filter.categoryDate,
                    'dealer': $scope.filter.Dealer,
                    'cabang': $scope.filter.Cabang,
                    'employeeId': $scope.filter.namaSA, 
                    'kategoriNama': $scope.filter.categoryName,
                    'periodeakhir': $filtertanggalakhir,
                    'periodeawal': $filtertanggal
                };
            }
            else {
                dataFilter = {
                    'kategoriDate': $scope.filter.categoryDate,
                    'dealer': $scope.filter.Dealer,
                    'cabang': $scope.filter.Cabang,
                    'employeeId': $scope.filter.namaVendor, 
                    'kategoriNama': $scope.filter.categoryName,
                    'periodeakhir': $filtertanggalakhir,
                    'periodeawal': $filtertanggal
                };
            }
            console.log('obj data filter summary GR', dataFilter);
            reportOPLTeknisiFactory.getSummaryGR(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);
                $scope.summary.totalOPLbyWO = format1(response.data[0].total_OPL_byWO,'');
                $scope.summary.totalAmountOPLbyJobs = format1(response.data[0].total_OPL_ByJobs,'')
                $scope.summary.totalAmountOPLinternal = format1(response.data[0].total_OPL_teknisi,'')
            })
        }

        $scope.getReportGR = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataReport = null;
            if($scope.filter.categoryName == 1) { 
                dataReport = {
                    'kategoriDate': $scope.filter.categoryDate,
                    'dealer': $scope.filter.Dealer,
                    'cabang': $scope.filter.Cabang,
                    'employeeId': $scope.filter.namaSA, 
                    'kategoriNama': $scope.filter.categoryName,
                    'periodeakhir': $filtertanggalakhir,
                    'periodeawal': $filtertanggal
                };
            }
            else {
                dataReport = {
                    'kategoriDate': $scope.filter.categoryDate,
                    'dealer': $scope.filter.Dealer,
                    'cabang': $scope.filter.Cabang,
                    'employeeId': $scope.filter.namaVendor, 
                    'kategoriNama': $scope.filter.categoryName,
                    'periodeakhir': $filtertanggalakhir,
                    'periodeawal': $filtertanggal
                };
            }
            console.log('obj data report export', dataReport);
            reportOPLTeknisiFactory.getExportGR(dataReport);
        }
        // $scope.getReportGR = function() {
        //     var $tanggal = $scope.filter.DateStart;
        //     var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
        //     var $tanggalakhir = $scope.filter.DateEnd;
        //     var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();

        //     var dataReport = {
        //         'dealer': $scope.filter.Dealer,
        //         'cabang': $scope.filter.Cabang,
        //         'periodeakhir': $filtertanggalakhir,
        //         'periodeawal': $filtertanggal,
        //         'tipe': $scope.filter.TipePencarianWO,
        //         'PIC': $scope.filter.TipePIC,
        //         'picid': $scope.filter.PIC
        //     };

        //     reportOPLTeknisiFactory.getExportGR(dataReport);
        // }

        $scope.getSummaryBP = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataFilter = null;
            if($scope.filter.categoryName == 1) { 
                dataFilter = {
                    'kategoriDate': $scope.filter.categoryDate,
                    'dealer': $scope.filter.Dealer,
                    'cabang': $scope.filter.Cabang,
                    'employeeId': $scope.filter.namaSA, 
                    'kategoriNama': $scope.filter.categoryName,
                    'periodeakhir': $filtertanggalakhir,
                    'periodeawal': $filtertanggal
                };
            }
            else {
                dataFilter = {
                    'kategoriDate': $scope.filter.categoryDate,
                    'dealer': $scope.filter.Dealer,
                    'cabang': $scope.filter.Cabang,
                    'employeeId': $scope.filter.namaVendor, 
                    'kategoriNama': $scope.filter.categoryName,
                    'periodeakhir': $filtertanggalakhir,
                    'periodeawal': $filtertanggal
                };
            }

            console.log('obj data filter summary BP', dataFilter);
            reportOPLTeknisiFactory.getSummaryBP(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);

                // $scope.ServiceTotalUnit = response.data[0].unitBS;
                // $scope.ARTotalUnit = response.data[0].unitAR;
                // $scope.TotalUnit = $scope.ServiceTotalUnit + $scope.ARTotalUnit;
                // $scope.ServiceTotalAmount = response.data[0].amountBS == null ? 0 : response.data[0].amountBS;
                // console.log('ServiceTotalAmount:', $scope.ServiceTotalAmount);
                // $scope.ServiceTotalAmount = format1($scope.ServiceTotalAmount, '');
                // $scope.ARTotalAmount = response.data[0].amountAR == null ? 0 : response.data[0].amountAR;
                // console.log('ARTotalAmount:', $scope.ARTotalAmount);
                // $scope.ARTotalAmount = format1($scope.ARTotalAmount, '');


                // $scope.TotalAmount = response.data[0].amountBS + response.data[0].amountAR;
                // $scope.TotalAmount = format1($scope.TotalAmount, '');
                // console.log('TotalAmount', $scope.TotalAmount);
                var totalamount = 0
                totalamount = parseInt(response.data[0].total_OPL_teknisi)

                $scope.summary.totalOPLbyWO = format1(response.data[0].total_OPL_byWO,'');
                $scope.summary.totalAmountOPLbyJobs = format1(response.data[0].total_OPL_ByJobs,'')
                $scope.summary.totalAmountOPLinternal = format1(totalamount,'')
                
            })
        }
        $scope.getReportBP = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataReport = null;
            if($scope.filter.categoryName == 1) { 
                dataReport = {
                    'kategoriDate': $scope.filter.categoryDate,
                    'dealer': $scope.filter.Dealer,
                    'cabang': $scope.filter.Cabang,
                    'employeeId': $scope.filter.namaSA, 
                    'kategoriNama': $scope.filter.categoryName,
                    'periodeakhir': $filtertanggalakhir,
                    'periodeawal': $filtertanggal
                };
            }
            else {
                dataReport = {
                    'kategoriDate': $scope.filter.categoryDate,
                    'dealer': $scope.filter.Dealer,
                    'cabang': $scope.filter.Cabang,
                    'employeeId': $scope.filter.namaVendor, 
                    'kategoriNama': $scope.filter.categoryName,
                    'periodeakhir': $filtertanggalakhir,
                    'periodeawal': $filtertanggal
                };
            }
            console.log('obj data report export', dataReport);
            reportOPLTeknisiFactory.getExportBP(dataReport);
        }
    });