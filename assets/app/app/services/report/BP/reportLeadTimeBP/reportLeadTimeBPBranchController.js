angular.module('app')
    .controller('ReportLeadTimeBPController', function($scope, $stateParams, $http, CurrentUser, ReportLeadTimeBP, bsNotify,$timeout) {
        console.log('Report LeadTime BP');
        //----------------------------------
        // Start-Up

        // console.log('report LeadTime BP', $item.RoleId);
        var dateFormat2 = 'dd/MM/yyyy';
        var dateFormat = 'MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var periode = new Date();
        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat,
            // mode:"'month'",
        };
        $scope.dateOptions2 = {
            startingDay: 1,
            format: dateFormat2,
            // mode:"'month'",
        };
        $scope.dateOptionsS = {
            startingDay: 1,
            format: dateFormat2,

            // mode:"'month'",
        };
        $scope.dateOptionsE = {
            startingDay: 1,
            format: dateFormat2,

            // mode:"'month'",
        };

        var changeFormatTime = function (data) {
            console.log('data ===',data);
            var mins_num = parseFloat(data, 10); // float anjay
            var hours   = Math.floor(mins_num / 60);
            var minutes = Math.floor((mins_num - ((hours * 3600)) / 60));
            var seconds = Math.floor((mins_num * 60) - (hours * 3600) - (minutes * 60));

            // nambah 0 anjay
            if (hours   < 10) {hours   = "0"+hours;}
            if (minutes < 10) {minutes = "0"+minutes;}
            if (seconds < 10) {seconds = "0"+seconds;}
            console.log('mins_num ===',mins_num);

            return hours+':'+minutes+':'+seconds;
        }


        //===============validasi date start and date end===========
        var today = new Date();
        $scope.maxDateS = new Date(today.getFullYear(), today.getMonth(), today.getDate());
        $scope.maxDateE = new Date(today.getFullYear(), today.getMonth(), today.getDate());
        $scope.minDateE = new Date(today.getFullYear(), today.getMonth(), today.getDate());


        $scope.changeStartDate = function(tgl) {
            var today = new Date();
            var dayOne = new Date($scope.filter.DateStart);
            var dayTwo = new Date($scope.filter.DateEnd);

            $scope.dateOptionsE.minDate = new Date(dayOne.getFullYear(), dayOne.getMonth(), dayOne.getDate());
            if (tgl == null || tgl == undefined) {
                $scope.filter.DateEnd = null;
            } else {
                if ($scope.filter.DateStart < $scope.filter.DateEnd) {

                } else {
                    if (dayOne > today) {
                        $scope.filter.DateStart = today;
                        $scope.filter.DateEnd = $scope.filter.DateStart;
                    } else {
                        $scope.filter.DateEnd = $scope.filter.DateStart;
                    }
                }
            }
        }

        //=============== End of validasi date start and date end===========

        $scope.user = CurrentUser.user();
        $scope.roleId = $scope.user.RoleId;
        $scope.role = $stateParams.role;
        console.log('role', $scope.role);
        $scope.dataDealer = [];
        $scope.dataCabang = [];
        $scope.dataAreaTAM = [];
        $scope.dataAreaDealer = [];
        $scope.dataSatellite = [];
        $scope.filter = { Dealer: null,DealerId: null, Cabang: null, kategori: "Body and Paint", DateStart: periode, DateEnd: periode, satellite: null, dealerarea: 0, tamarea: 0,isGR:0 };
        ReportLeadTimeBP.getDataDealer().then(function(response) {
            console.log('Response BEDealer:', response.data);
            $scope.dataDealer = response.data;
            $scope.filter.Dealer = response.data[0].dealerCode;
            $scope.filter.DealerId=response.data[0].dealerID;
            ReportLeadTimeBP.getDataAreaDealerHO($scope.filter).then(function (response) {
                console.log('ayamtest',response.data)
                $scope.dataGroupDealerHO = response.data
                $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
                $scope.areaId($scope.dataGroupDealerHO[0])
            })
        })
        ReportLeadTimeBP.getDataCabang($scope.filter).then(function(response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabang = response.data;
            $scope.DisabledCabang();
            $scope.filter.Cabang = response.data[0].cabangID;
        })
        ReportLeadTimeBP.getDataAreaTAM().then(function(response) {
            console.log('Response AreaTAM:', response.data);
            $scope.dataAreaTAM = response.data;
        })
        ReportLeadTimeBP.getDataAreaDealer().then(function(response) {
            console.log('Response AreaDealer:', response.data);
            $scope.dataAreaDealer = response.data;
        })
        ReportLeadTimeBP.getDataAreaDealerHO($scope.filter).then(function(response) {
            console.log('Response AreaDealerHO:', response.data);
            $scope.dataAreaDealerHO = response.data;
            $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
            $scope.areaId($scope.dataAreaDealerHO[0])
        })
        ReportLeadTimeBP.getDataSatellite().then(function(response) {
            console.log('Response AreaSatellite:', response.data);
            $scope.dataSatellite = response.data;
        })
        var currentUser = CurrentUser.user();
        console.log('LOGIN :', currentUser.OrgId)
        $scope.userlogin = currentUser.OrgCode.substring(0, 3);
        console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
        $scope.DisabledDealer = function() {
            if ($scope.userlogin == 'TAM') {
                return true;
                console.log("DisabledDealer TRUE");
            } else {
                return false;
                console.log("DisabledDealer FALSE");
            }
        };
        $scope.DisabledTAM = function() {
            if ($scope.userlogin != 'TAM') {
                return true;
                console.log("DisabledTAM TRUE");
            } else {
                return false;
                console.log("DisabledTAM FALSE");
            }
        };

        

        $scope.areaId = function(row) {
          // $scope.DataDealer = row
            $scope.DataDealer = Object.assign(row,{'kategori': $scope.filter.kategori})
            if($scope.userlogin != 'TAM'){
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
            }else{
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
            }
            console.log('row dealerid', row,$scope.DataDealer);
            ReportLeadTimeBP.getDataCabangHONew($scope.DataDealer).then(function (response) {
                console.log('Response BECabang:', response.data);
                $scope.dataCabangHO = response.data;
                $scope.DisabledCabang();
                $scope.filter.CabangHO = null
                console.log('kambing',$scope.filter)
                $timeout(function() {
                    $scope.filter.CabangHO = response.data[0].cabangID;
                    console.log('kambing123',$scope.filter.CabangHO)
                }, 100);
            })
        };

        $scope.dealerCode = function(row){
            row = Object.assign(row,{'isGR':$scope.filter.isGR})
            $scope.filter.DealerId = row.dealerID
            ReportLeadTimeBP.getDataAreaDealerHO(row).then(function (response) {
                console.log('ayam',response.data)
                console.log('rusa',$scope.dataGroupDealer)
                $scope.dataGroupDealer = response.data
                $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
                $scope.areaId($scope.dataGroupDealer[0])
                console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
            })
        }

        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length > 1) {
                $scope.Disabled = false;
                console.log('Cabang Enabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            } else {
                $scope.Disabled = true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };

        function FormatTime(number) {
            if (number < 10 && number >= 0) {
                number = '0' + number;
            } else if (number > -10 && number < 0) {
                number = number * -1;
                number = '-0' + number;
            } else {
                number = number;
            }
            return number;
        }
        // =================================================================== BP HO ====================
        $scope.getData = function() {
            if ($scope.role == "Branch") $scope.getSummaryBPCabang();
            else $scope.getSummaryBPHO();
            console.log('get data');
        }
        $scope.getSummaryBPHO = function() {
            var $tanggal1 = $scope.filter.DateStart;
            var $tanggal2 = $scope.filter.DateEnd;
            var $filtertanggal1 = $tanggal1.getFullYear() + '-' + ($tanggal1.getMonth() + 1) + '-' + $tanggal1.getDate();
            var $filtertanggal2 = $tanggal2.getFullYear() + '-' + ($tanggal2.getMonth() + 1) + '-' + $tanggal2.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'tamarea': $scope.filter.tamarea,
                'dealerarea': $scope.filter.dealerarea,
                'kategori': "BP",
                'periode_awal': $filtertanggal1,
                'periode_akhir': $filtertanggal2,
                'satellite': $scope.filter.satellite,
                'Jenis': 'summary'
            };

            if (((dataFilter.satellite == null) || (dataFilter.satellite == undefined)) ||
                ((dataFilter.cabang == null) || (dataFilter.cabang == undefined) || (dataFilter.cabang == ''))) {
                console.log('get summary HO', dataFilter);
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "error",
                    content: "Isi semua Field"
                });
            } else {



                ReportLeadTimeBP.getSummaryBPHO(dataFilter).then(function(response) {
                    console.log('Response BE Filter:', response.data);
                    // $scope.dataSummary = response.data;
                    var hasil = response.data[0];
                    $scope.sumTotalData = hasil.totalData;
                    $scope.sumTotalData = Math.round($scope.sumTotalData * 100) / 100
                    $scope.sumValid = hasil.dataValid;
                    $scope.sumValid = Math.round($scope.sumValid * 100) / 100
                    $scope.sumCombi = hasil.totalCombiBooth;
                    $scope.sumCombi = Math.round($scope.sumCombi * 100) / 100
                    $scope.sumOTD1 = hasil.otD1;
                    $scope.sumOTD1 = Math.round($scope.sumOTD1 * 100) / 100
                    $scope.sumOTD1Rate = hasil.otD1Percent;
                    $scope.sumOTD1Rate = Math.round($scope.sumOTD1Rate * 100) / 100
                    $scope.sumOTD2 = hasil.otD2;
                    $scope.sumOTD2 = Math.round($scope.sumOTD2 * 100) / 100
                    $scope.sumOTD2Rate = hasil.otD2Percent;
                    $scope.sumOTD2Rate = Math.round($scope.sumOTD2Rate * 100) / 100
                    $scope.sumDTN = hasil.dtn;
                    $scope.sumDTN = Math.round($scope.sumDTN * 100) / 100
                })
                ReportLeadTimeBP.getSummaryBPHOLain(dataFilter).then(function(response) {
                    console.log('Response BE Filter:', response.data);

                    $scope.estTPSProses = changeFormatTime(0);
                    $scope.estTPSStagnasi = changeFormatTime(0);
                    $scope.IATPSProses = changeFormatTime(0);
                    $scope.IATPSStagnasi = changeFormatTime(0);
                    $scope.productionTPSProses = changeFormatTime(0);
                    $scope.productionTPSStagnasi = changeFormatTime(0);
                    $scope.totalTPSProses = changeFormatTime(0);
                    $scope.totalTPSStagnasi = changeFormatTime(0);

                    $scope.estLightProses = changeFormatTime(0);
                    $scope.estLightStagnasi = changeFormatTime(0);
                    $scope.IALightProses = changeFormatTime(0);
                    $scope.IALightStagnasi = changeFormatTime(0);
                    $scope.productionLightProses = changeFormatTime(0);
                    $scope.productionLightStagnasi = changeFormatTime(0);
                    $scope.totalLightProses = changeFormatTime(0);
                    $scope.totalLightStagnasi = changeFormatTime(0);

                    $scope.estMediumProses = changeFormatTime(0);
                    $scope.estMediumStagnasi = changeFormatTime(0);
                    $scope.IAMediumProses = changeFormatTime(0);
                    $scope.IAMediumStagnasi = changeFormatTime(0);
                    $scope.productionMediumProses = changeFormatTime(0);
                    $scope.productionMediumStagnasi = changeFormatTime(0);
                    $scope.totalMediumProses = changeFormatTime(0);
                    $scope.totalMediumStagnasi = changeFormatTime(0);   

                    $scope.estHeavyProses = changeFormatTime(0);
                    $scope.estHeavyStagnasi = changeFormatTime(0);
                    $scope.IAHeavyProses = changeFormatTime(0);
                    $scope.IAHeavyStagnasi = changeFormatTime(0);
                    $scope.productionHeavyProses = changeFormatTime(0);
                    $scope.productionHeavyStagnasi = changeFormatTime(0);
                    $scope.totalHeavyProses = changeFormatTime(0);
                    $scope.totalHeavyStagnasi = changeFormatTime(0);
                    
                    for (var i = 0; i < response.data.length; i++) {
                        res = response.data[i];

                        switch (res.jenisPekerjaan) {
                            case 'TPS':
                                console.log('result', i, res);
                                console.log('HASIL FORMAT', FormatTime(~~(res.estimasiLTP / 3600)));
                                // $scope.estTPSProses = FormatTime(~~(res.estimasiLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTP) % 60));
                                // $scope.estTPSStagnasi = FormatTime(~~(res.estimasiLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTS) % 60));
                                // $scope.IATPSProses = FormatTime(~~(res.insuranceApprovalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTP) % 60));
                                // $scope.IATPSStagnasi = FormatTime(~~(res.insuranceApprovalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTS) % 60));
                                // $scope.productionTPSProses = FormatTime(~~(res.productionLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTP) % 60));
                                // $scope.productionTPSStagnasi = FormatTime(~~(res.productionLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTS) % 60));
                                // $scope.totalTPSProses = FormatTime(~~(res.totalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTP) % 60));
                                // $scope.totalTPSStagnasi = FormatTime(~~(res.totalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTS) % 60));
                                $scope.estTPSProses = changeFormatTime(res.estimasiLTP);
                                $scope.estTPSStagnasi = changeFormatTime(res.estimasiLTS);
                                $scope.IATPSProses = changeFormatTime(res.insuranceApprovalLTP);
                                $scope.IATPSStagnasi = changeFormatTime(res.insuranceApprovalLTS);
                                $scope.productionTPSProses = changeFormatTime(res.productionLTP);
                                $scope.productionTPSStagnasi = changeFormatTime(res.productionLTS);
                                $scope.totalTPSProses = changeFormatTime(res.totalLTP);
                                $scope.totalTPSStagnasi = changeFormatTime(res.totalLTS);

                                break;
                            case 'Light':
                                console.log('result', i, res);
                                // $scope.estLightProses = FormatTime(~~(res.estimasiLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTP) % 60));
                                // $scope.estLightStagnasi = FormatTime(~~(res.estimasiLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTS) % 60));
                                // $scope.IALightProses = FormatTime(~~(res.insuranceApprovalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTP) % 60));
                                // $scope.IALightStagnasi = FormatTime(~~(res.insuranceApprovalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTS) % 60));
                                // $scope.productionLightProses = FormatTime(~~(res.productionLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTP) % 60));
                                // $scope.productionLightStagnasi = FormatTime(~~(res.productionLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTS) % 60));
                                // $scope.totalLightProses = FormatTime(~~(res.totalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTP) % 60));
                                // $scope.totalLightStagnasi = FormatTime(~~(res.totalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTS) % 60));

                                $scope.estLightProses = changeFormatTime(res.estimasiLTP);
                                $scope.estLightStagnasi = changeFormatTime(res.estimasiLTS);
                                $scope.IALightProses = changeFormatTime(res.insuranceApprovalLTP);
                                $scope.IALightStagnasi = changeFormatTime(res.insuranceApprovalLTS);
                                $scope.productionLightProses = changeFormatTime(res.productionLTP);
                                $scope.productionLightStagnasi = changeFormatTime(res.productionLTS);
                                $scope.totalLightProses = changeFormatTime(res.totalLTP);
                                $scope.totalLightStagnasi = changeFormatTime(res.totalLTS);

                                break;
                            case 'Medium':
                                console.log('result', i, res);
                                // $scope.estMediumProses = FormatTime(~~(res.estimasiLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTP) % 60));
                                // $scope.estMediumStagnasi = FormatTime(~~(res.estimasiLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTS) % 60));
                                // $scope.IAMediumProses = FormatTime(~~(res.insuranceApprovalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTP) % 60));
                                // $scope.IAMediumStagnasi = FormatTime(~~(res.insuranceApprovalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTS) % 60));
                                // $scope.productionMediumProses = FormatTime(~~(res.productionLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTP) % 60));
                                // $scope.productionMediumStagnasi = FormatTime(~~(res.productionLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTS) % 60));
                                // $scope.totalMediumProses = FormatTime(~~(res.totalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTP) % 60));
                                // $scope.totalMediumStagnasi = FormatTime(~~(res.totalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTS) % 60));

                                $scope.estMediumProses = changeFormatTime(res.estimasiLTP);
                                $scope.estMediumStagnasi = changeFormatTime(res.estimasiLTS);
                                $scope.IAMediumProses = changeFormatTime(res.insuranceApprovalLTP);
                                $scope.IAMediumStagnasi = changeFormatTime(res.insuranceApprovalLTS);
                                $scope.productionMediumProses = changeFormatTime(res.productionLTP);
                                $scope.productionMediumStagnasi = changeFormatTime(res.productionLTS);
                                $scope.totalMediumProses = changeFormatTime(res.totalLTP);
                                $scope.totalMediumStagnasi = changeFormatTime(res.totalLTS);

                                break;
                            case 'Heavy':
                                console.log('result', i, res);
                                // $scope.estHeavyProses = FormatTime(~~(res.estimasiLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTP) % 60));
                                // $scope.estHeavyStagnasi = FormatTime(~~(res.estimasiLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTS) % 60));
                                // $scope.IAHeavyProses = FormatTime(~~(res.insuranceApprovalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTP) % 60));
                                // $scope.IAHeavyStagnasi = FormatTime(~~(res.insuranceApprovalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTS) % 60));
                                // $scope.productionHeavyProses = FormatTime(~~(res.productionLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTP) % 60));
                                // $scope.productionHeavyStagnasi = FormatTime(~~(res.productionLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTS) % 60));
                                // $scope.totalHeavyProses = FormatTime(~~(res.totalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTP) % 60));
                                // $scope.totalHeavyStagnasi = FormatTime(~~(res.totalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTS) % 60));

                                $scope.estHeavyProses = changeFormatTime(res.estimasiLTP);
                                $scope.estHeavyStagnasi = changeFormatTime(res.estimasiLTS);
                                $scope.IAHeavyProses = changeFormatTime(res.insuranceApprovalLTP);
                                $scope.IAHeavyStagnasi = changeFormatTime(res.insuranceApprovalLTS);
                                $scope.productionHeavyProses = changeFormatTime(res.productionLTP);
                                $scope.productionHeavyStagnasi = changeFormatTime(res.productionLTS);
                                $scope.totalHeavyProses = changeFormatTime(res.totalLTP);
                                $scope.totalHeavyStagnasi = changeFormatTime(res.totalLTS);

                                break;
                        }
                    }
                })
            };
        }
        $scope.getReportBPHO = function() {
            var $tanggal1 = $scope.filter.DateStart;
            var $tanggal2 = $scope.filter.DateEnd;
            var $filtertanggal1 = $tanggal1.getFullYear() + '/' + ($tanggal1.getMonth() + 1) + '/' + $tanggal1.getDate();
            var $filtertanggal2 = $tanggal2.getFullYear() + '/' + ($tanggal2.getMonth() + 1) + '/' + $tanggal2.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'tamarea': $scope.filter.tamarea,
                'dealerarea': $scope.filter.dealerarea,
                'kategori': "BP",
                'periode_awal': $filtertanggal1,
                'periode_akhir': $filtertanggal2,
                'satellite': $scope.filter.satellite,
                'Jenis': 'summary'
            };

            if (($scope.sumTotalData == undefined) || ($scope.sumValid == undefined) || ($scope.sumCombi == undefined)) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "error",
                    content: "can not export due to empty summary"
                });
            } else {
                ReportLeadTimeBP.getExportBPHO(dataFilter);
            };
        }

        // ============================================================================== BP CABANG ====================
        $scope.getSummaryBPCabang = function() {
            var $tanggal1 = $scope.filter.DateStart;
            var $tanggal2 = $scope.filter.DateEnd;
            var $filtertanggal1 = $tanggal1.getFullYear() + '-' + ($tanggal1.getMonth() + 1) + '-' + $tanggal1.getDate();
            var $filtertanggal2 = $tanggal2.getFullYear() + '-' + ($tanggal2.getMonth() + 1) + '-' + $tanggal2.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'kategori': "BP",
                'periode_awal': $filtertanggal1,
                'periode_akhir': $filtertanggal2,
                'satelite': $scope.filter.satellite
            };

            ReportLeadTimeBP.getSummaryBPCabang(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);
                // $scope.dataSummary = response.data;
                var hasil = response.data[0];
                $scope.sumTotalData = hasil.totalData;
                $scope.sumTotalData = Math.round($scope.sumTotalData * 100) / 100;
                $scope.sumValid = hasil.dataValid;
                $scope.sumValid = Math.round($scope.sumValid * 100) / 100;
                $scope.sumCombi = hasil.totalCombiBooth;
                $scope.sumCombi = Math.round($scope.sumCombi * 100) / 100;
                $scope.sumOTD1 = hasil.otD1;
                $scope.sumOTD1 = Math.round($scope.sumOTD1 * 100) / 100;
                $scope.sumOTD1Rate = hasil.otD1Percent;
                $scope.sumOTD1Rate = Math.round($scope.sumOTD1Rate * 100) / 100;
                $scope.sumOTD2 = hasil.otD2;
                $scope.sumOTD2 = Math.round($scope.sumOTD2 * 100) / 100;
                $scope.sumOTD2Rate = hasil.otD2Percent;
                $scope.sumOTD2Rate = Math.round($scope.sumOTD2Rate * 100) / 100;
                $scope.sumDTN = hasil.dtn;
            })
            ReportLeadTimeBP.getSummaryBPCabangLain(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);
                // $scope.dataSummary = response.data;
                $scope.estTPSProses = changeFormatTime(0);
                $scope.estTPSStagnasi = changeFormatTime(0);
                $scope.IATPSProses = changeFormatTime(0);
                $scope.IATPSStagnasi = changeFormatTime(0);
                $scope.productionTPSProses = changeFormatTime(0);
                $scope.productionTPSStagnasi = changeFormatTime(0);
                $scope.totalTPSProses = changeFormatTime(0);
                $scope.totalTPSStagnasi = changeFormatTime(0);

                $scope.estLightProses = changeFormatTime(0);
                $scope.estLightStagnasi = changeFormatTime(0);
                $scope.IALightProses = changeFormatTime(0);
                $scope.IALightStagnasi = changeFormatTime(0);
                $scope.productionLightProses = changeFormatTime(0);
                $scope.productionLightStagnasi = changeFormatTime(0);
                $scope.totalLightProses = changeFormatTime(0);
                $scope.totalLightStagnasi = changeFormatTime(0);

                $scope.estMediumProses = changeFormatTime(0);
                $scope.estMediumStagnasi = changeFormatTime(0);
                $scope.IAMediumProses = changeFormatTime(0);
                $scope.IAMediumStagnasi = changeFormatTime(0);
                $scope.productionMediumProses = changeFormatTime(0);
                $scope.productionMediumStagnasi = changeFormatTime(0);
                $scope.totalMediumProses = changeFormatTime(0);
                $scope.totalMediumStagnasi = changeFormatTime(0);   

                $scope.estHeavyProses = changeFormatTime(0);
                $scope.estHeavyStagnasi = changeFormatTime(0);
                $scope.IAHeavyProses = changeFormatTime(0);
                $scope.IAHeavyStagnasi = changeFormatTime(0);
                $scope.productionHeavyProses = changeFormatTime(0);
                $scope.productionHeavyStagnasi = changeFormatTime(0);
                $scope.totalHeavyProses = changeFormatTime(0);
                $scope.totalHeavyStagnasi = changeFormatTime(0);
                
                if (response.data.length > 0) {
                    for (var i = 0; i < response.data.length; i++) {
                        res = response.data[i];
                        console.log('result', i, res);
                        switch (res.jenisPekerjaan) {
                            case 'TPS':
                                console.log('result', i, res);
                                // $scope.estTPSProses = FormatTime(~~(res.estimasiLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTP) % 60));
                                // $scope.estTPSStagnasi = FormatTime(~~(res.estimasiLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTS) % 60));
                                // $scope.IATPSProses = FormatTime(~~(res.insuranceApprovalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTP) % 60));
                                // $scope.IATPSStagnasi = FormatTime(~~(res.insuranceApprovalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTS) % 60));
                                // $scope.productionTPSProses = FormatTime(~~(res.productionLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTP) % 60));
                                // $scope.productionTPSStagnasi = FormatTime(~~(res.productionLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTS) % 60));
                                // $scope.totalTPSProses = FormatTime(~~(res.totalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTP) % 60));
                                // $scope.totalTPSStagnasi = FormatTime(~~(res.totalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTS) % 60));

                                $scope.estTPSProses = changeFormatTime(res.estimasiLTP);
                                $scope.estTPSStagnasi = changeFormatTime(res.estimasiLTS);
                                $scope.IATPSProses = changeFormatTime(res.insuranceApprovalLTP);
                                $scope.IATPSStagnasi = changeFormatTime(res.insuranceApprovalLTS);
                                $scope.productionTPSProses = changeFormatTime(res.productionLTP);
                                $scope.productionTPSStagnasi = changeFormatTime(res.productionLTS);
                                $scope.totalTPSProses = changeFormatTime(res.totalLTP);
                                $scope.totalTPSStagnasi = changeFormatTime(res.totalLTS);
                                break;
                            case 'Light':
                                console.log('result', i, res);
                                // $scope.estLightProses = FormatTime(~~(res.estimasiLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTP) % 60));
                                // $scope.estLightStagnasi = FormatTime(~~(res.estimasiLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTS) % 60));
                                // $scope.IALightProses = FormatTime(~~(res.insuranceApprovalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTP) % 60));
                                // $scope.IALightStagnasi = FormatTime(~~(res.insuranceApprovalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTS) % 60));
                                // $scope.productionLightProses = FormatTime(~~(res.productionLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTP) % 60));
                                // $scope.productionLightStagnasi = FormatTime(~~(res.productionLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTS) % 60));
                                // $scope.totalLightProses = FormatTime(~~(res.totalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTP) % 60));
                                // $scope.totalLightStagnasi = FormatTime(~~(res.totalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTS) % 60));

                                $scope.estLightProses = changeFormatTime(res.estimasiLTP);
                                $scope.estLightStagnasi = changeFormatTime(res.estimasiLTS);
                                $scope.IALightProses = changeFormatTime(res.insuranceApprovalLTP);
                                $scope.IALightStagnasi = changeFormatTime(res.insuranceApprovalLTS);
                                $scope.productionLightProses = changeFormatTime(res.productionLTP);
                                $scope.productionLightStagnasi = changeFormatTime(res.productionLTS);
                                $scope.totalLightProses = changeFormatTime(res.totalLTP);
                                $scope.totalLightStagnasi = changeFormatTime(res.totalLTS);
                                break;
                            case 'Medium':
                                console.log('result', i, res);
                                // $scope.estMediumProses = FormatTime(~~(res.estimasiLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTP) % 60));
                                // $scope.estMediumStagnasi = FormatTime(~~(res.estimasiLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTS) % 60));
                                // $scope.IAMediumProses = FormatTime(~~(res.insuranceApprovalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTP) % 60));
                                // $scope.IAMediumStagnasi = FormatTime(~~(res.insuranceApprovalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTS) % 60));
                                // $scope.productionMediumProses = FormatTime(~~(res.productionLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTP) % 60));
                                // $scope.productionMediumStagnasi = FormatTime(~~(res.productionLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTS) % 60));
                                // $scope.totalMediumProses = FormatTime(~~(res.totalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTP) % 60));
                                // $scope.totalMediumStagnasi = FormatTime(~~(res.totalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTS) % 60));

                                $scope.estMediumProses = changeFormatTime(res.estimasiLTP);
                                $scope.estMediumStagnasi = changeFormatTime(res.estimasiLTS);
                                $scope.IAMediumProses = changeFormatTime(res.insuranceApprovalLTP);
                                $scope.IAMediumStagnasi = changeFormatTime(res.insuranceApprovalLTS);
                                $scope.productionMediumProses = changeFormatTime(res.productionLTP);
                                $scope.productionMediumStagnasi = changeFormatTime(res.productionLTS);
                                $scope.totalMediumProses = changeFormatTime(res.totalLTP);
                                $scope.totalMediumStagnasi = changeFormatTime(res.totalLTS);
                                break;
                            case 'Heavy':
                                console.log('result', i, res);
                                // $scope.estHeavyProses = FormatTime(~~(res.estimasiLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTP) % 60));
                                // $scope.estHeavyStagnasi = FormatTime(~~(res.estimasiLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.estimasiLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.estimasiLTS) % 60));
                                // $scope.IAHeavyProses = FormatTime(~~(res.insuranceApprovalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTP) % 60));
                                // $scope.IAHeavyStagnasi = FormatTime(~~(res.insuranceApprovalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.insuranceApprovalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.insuranceApprovalLTS) % 60));
                                // $scope.productionHeavyProses = FormatTime(~~(res.productionLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTP) % 60));
                                // $scope.productionHeavyStagnasi = FormatTime(~~(res.productionLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.productionLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.productionLTS) % 60));
                                // $scope.totalHeavyProses = FormatTime(~~(res.totalLTP / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTP) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTP) % 60));
                                // $scope.totalHeavyStagnasi = FormatTime(~~(res.totalLTS / 3600)) + ':' + FormatTime(~~((Math.abs(res.totalLTS) % 3600) / 60)) + ':' + FormatTime((Math.abs(res.totalLTS) % 60));

                                $scope.estHeavyProses = changeFormatTime(res.estimasiLTP);
                                $scope.estHeavyStagnasi = changeFormatTime(res.estimasiLTS);
                                $scope.IAHeavyProses = changeFormatTime(res.insuranceApprovalLTP);
                                $scope.IAHeavyStagnasi = changeFormatTime(res.insuranceApprovalLTS);
                                $scope.productionHeavyProses = changeFormatTime(res.productionLTP);
                                $scope.productionHeavyStagnasi = changeFormatTime(res.productionLTS);
                                $scope.totalHeavyProses = changeFormatTime(res.totalLTP);
                                $scope.totalHeavyStagnasi = changeFormatTime(res.totalLTS);

                                break;
                        }
                    }
                } else {
                    $scope.estTPSProses = null;
                    $scope.estTPSStagnasi = null;
                    $scope.IATPSProses = null;
                    $scope.IATPSStagnasi = null;
                    $scope.productionTPSProses = null;
                    $scope.productionTPSStagnasi = null;
                    $scope.totalTPSProses = null;
                    $scope.totalTPSStagnasi = null;
                    $scope.estLightProses = null;
                    $scope.estLightStagnasi = null;
                    $scope.IALightProses = null;
                    $scope.IALightStagnasi = null;
                    $scope.productionLightProses = null;
                    $scope.productionLightStagnasi = null;
                    $scope.totalLightProses = null;
                    $scope.totalLightStagnasi = null;
                    $scope.estMediumProses = null;
                    $scope.estMediumStagnasi = null;
                    $scope.IAMediumProses = null;
                    $scope.IAMediumStagnasi = null;
                    $scope.productionMediumProses = null;
                    $scope.productionMediumStagnasi = null;
                    $scope.totalMediumProses = null;
                    $scope.totalMediumStagnasi = null;
                    $scope.estHeavyProses = null;
                    $scope.estHeavyStagnasi = null;
                    $scope.IAHeavyProses = null;
                    $scope.IAHeavyStagnasi = null;
                    $scope.productionHeavyProses = null;
                    $scope.productionHeavyStagnasi = null;
                    $scope.totalHeavyProses = null;
                    $scope.totalHeavyStagnasi = null;
                }

            })
        }
        $scope.getReportBPCabang = function() {
            var $tanggal1 = $scope.filter.DateStart;
            var $tanggal2 = $scope.filter.DateEnd;
            var $filtertanggal1 = $tanggal1.getFullYear() + '/' + ($tanggal1.getMonth() + 1) + '/' + $tanggal1.getDate();
            var $filtertanggal2 = $tanggal2.getFullYear() + '/' + ($tanggal2.getMonth() + 1) + '/' + $tanggal2.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'kategori': "BP",
                'periode_awal': $filtertanggal1,
                'periode_akhir': $filtertanggal2,
                'satelite': $scope.filter.satellite
            };

            ReportLeadTimeBP.getExportBPCabang(dataFilter);
        }

        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;

        });
        // //----------------------------------
        // // Initialization
        // //----------------------------------
        // $scope.user = CurrentUser.user();
        // $scope.loading=false;
        // $scope.formApi = {};
        // $scope.backToMain = true;
        // $scope.roleId = 1;

        // //----------------------------------
        // // Get Data
        // //----------------------------------

        // var gridData = [];
        // $scope.getData = function() {
        //     $scope.loading=true;
        //     return $q.resolve(
        //         ReportLeadTimeBP.getData().then(
        //           function(res){
        //             console.log("getData res=>",res);
        //             $scope.grid.data = res;
        //             $scope.loading=false;
        //             return res;
        //           },
        //           function(err){
        //             console.log("error=>",err);
        //             $scope.loading=false;
        //             return err;
        //           }
        //         )
        //     )
        // }

        // $scope.getDealer = [
        // {DealerId:1,DealerName:"Dealer001"},
        // {DealerId:2,DealerName:"Dealer002"}
        // ];

        // $scope.getCabang = [
        // {cabangID:1,CabangName:"Cabang001"},
        // {cabangID:2,CabangName:"Cabang002"}
        // ];

        //    $scope.getTamArea = [
        // {TamAreaId:1,TamAreaName:"TAM Area 001"},
        // {TamAreaId:2,TamAreaName:"TAM Area 001"}
        // ];


        // $scope.getGroupDealer = [
        // {GroupDealerId:1,GroupDealerName:"Group Dealer 001"},
        // {GroupDealerId:2,GroupDealerName:"Group Dealer 002"}
        // ];

        //  $scope.getGroupDealerArea = [
        // {GroupDealerAreaId:1,GroupDealerAreaName:"Group Dealer Area 1"},
        // {GroupDealerAreaId:2,GroupDealerAreaName:"Group Dealer Area 2"}
        // ];

        //  $scope.getSatellite = [
        // {SatelliteId:1,SatelliteName:"Satellite001"},
        // {SatelliteId:2,SatelliteName:"Satellite002"}
        // ];


    });
