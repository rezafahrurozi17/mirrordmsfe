angular.module('app')
  .factory('ReportLeadTimeBP', function($http, CurrentUser,$q) {
     var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL ='';// 'http://localhost:8080';
    return {
      getDataDealer: function() {
      var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
      return res;},
      // getDataCabang: function() {
      // var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
      // return res;},
      getDataCabang: function(data) {
        var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId + '/' + data.kategori);
        return res;},
      getDataCabangHONew: function(data) {
        console.log('Data Get Cabang:', data);
        var res=$http.get(serverURL + '/api/rpt/GetCabangHONew/'+ data.areaId + '/' +  data.DealerId + '/' + data.kategori);
        return res;},
      getDataSatellite: function() {
      var res=$http.get(serverURL + '/api/rpt/Satellite/');
      return res;},
      getDataAreaTAM: function() {
      var res=$http.get(serverURL + '/api/rpt/AreaTAM/');
      return res;},
      getDataAreaDealer: function() {
      var res=$http.get(serverURL + '/api/rpt/AreaDealer/');
      return res;},
      getDataAreaDealerHO: function(data) {
      var res=$http.get(serverURL + '/api/rpt/AreaDealerHONew/'+ data.isGR );
      return res;},
      getData: function() {
        var res=$http.get('/api/fw/Role');
        console.log('data=>',res);
        //---------
        return $q.resolve(data);
      },
      getSummaryBPHO: function(data) {
              console.log('Data Filter:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_LeadTime_BPHO/'+ data.tamarea +'/'+ data.dealer +'/'+ data.dealerarea +'/'+ data.cabang +'/'+ data.satellite +'/'+ data.kategori +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/Summary');
              return res;},
      getSummaryBPHOLain: function(data) {
              console.log('Data Filter:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_LeadTime_BPHO/'+ data.tamarea +'/'+ data.dealer +'/'+ data.dealerarea +'/'+ data.cabang +'/'+ data.satellite +'/'+ data.kategori +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/Lainnya');
              return res;},
      getExportBPHO: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_LeadTime_BPHO?tamarea='+ data.tamarea +'&dealerarea='+ data.dealerarea +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode_awal='+ data.periode_awal +'&periode_akhir='+ data.periode_akhir +'&kat_workshop='+ data.kategori +'&satelite='+ data.satellite);
            },
      getSummaryBPCabang: function(data) {
              console.log('Data Filter:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_LeadTime_BPCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/Summary');
              return res;},
      getSummaryBPCabangLain: function(data) {
              console.log('Data Filter:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_LeadTime_BPCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/Lainnya');
              return res;},
      getExportBPCabang: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_LeadTime_BPCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&kat_workshop='+ data.kategori +'&periode_awal='+ data.periode_awal +'&periode_akhir='+ data.periode_akhir);
            }
    }
  });
