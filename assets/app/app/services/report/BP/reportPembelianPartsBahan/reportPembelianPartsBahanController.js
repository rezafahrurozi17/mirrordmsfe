angular.module('app')
    .controller('reportPembelianPartsBahanController', function($scope, bsNotify, $http, $stateParams, CurrentUser, reportPembelianPartsBahanFactory) {
        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var periode = new Date();
        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat,

            // mode:"'month'",
        };
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
        });

        $scope.generateKategori = function(outletId){
            if(outletId < 2000000){
                return "General Repair"
            }else if(outletId >= 2000000){
                return "Body and Paint"
            }
        }


        $scope.user = CurrentUser.user();
        console.log('$scope.user ===>',$scope.user);
        $scope.summary = {};
        $scope.filter = {
            Dealer:null, 
            Cabang:null, 
            kategori: $scope.generateKategori($scope.user.OutletId),
            DateStart:periode,
            DateEnd:periode,
            typePO : null,
            // referencePO : null,
            // statusPO : null,
        };

        $scope.filterCategoryDate =  { data: [
            { text: "Tanggal PO", value: 1 }, 
            { text: "Tanggal Invoice", value: 2 }, 
        ] };   
        // var filtered = $scope.filterCategoryDate.data.filter(function(item) { 
        //    return item.value == 1;  
        // });

        $scope.filter.categoryDate = 1;
        console.log("$scope.filter",$scope.filter);

        $scope.changeCategoryFilterDate = function(selected){

            if(selected == 1){
                console.log('changeKategoryFilter by WO ===>',selected);
                $scope.selectedFilterCategory = 'Tanggal PO';
            }else{
                console.log('changeKategoryFilter by OPL ===>',selected);
                $scope.selectedFilterCategory = "Tanggal Invoice";
            }
            console.log("$scope.filter.categoryDate",$scope.filter.categoryDate)
        }

        
        $scope.dataTipePO = [
            { text: "All", value: 0 }, 
            { text: "PO TAM Tipe Order 1", value: 1 }, 
            { text: "PO TAM Tipe Order 2", value: 2 }, 
            { text: "PO TAM Tipe Order 3", value: 3 },
            { text: "PO TAM Tipe Order T", value: 6 },
            { text: "PO TAM Campaign 3 & C", value: 7 },
            { text: "PO Non TAM", value: 4 },
            { text: "PO Sublet", value: 5 },
        ];

        $scope.dataVendor = [
            { text: "All", value: 0 }, 
            { text: "TAM", value: 1 }, 
            { text: "Non TAM", value: 2 }
        ];

        // $scope.dataReferensiPO = [
        //     { text: "All", value: 0 }, 
        //     { text: "SOP-Appointment", value: 1 }, 
        //     { text: "SOP-WO", value: 2 }, 
        //     { text: "SOP-Sales Order", value: 3 },
        //     { text: "Non SOP", value: 4 },
        // ];

        // $scope.dataStatusPO = [
        //     { text: "All", value: 0 }, 
        //     { text: "Draft", value: 1 }, 
        //     { text: "Request Approval", value: 2 }, 
        //     { text: "Order ", value: 3 },
        //     { text: "Partial ", value: 4 },
        //     { text: "Completed ", value: 5 },
        //     { text: "Cancelled ", value: 6 },
        // ];

        


        $scope.onSelectTypePO = function (selected) {
            if(selected !== undefined){
                console.log('onSelectTypePO ===>',selected)
                $scope.filter.typePO = selected.value;
            }else{
                $scope.filter.typePO = null;
            }
        }
        $scope.onSelectVendor = function (selected) {
            if(selected !== undefined){
                $scope.filter.vendor = selected.value;
            }else{
                $scope.filter.vendor = null;
            }
        }
        // $scope.onSelectReferencePO = function (selected) {
        //     if(selected !== undefined){
        //         console.log('onSelectReferencePO ===>',selected)
        //         $scope.filter.referencePO = selected.value;
        //     }else{
        //         $scope.filter.referencePO = null;
        //     }
        // }

        // $scope.onSelectStatusPO = function (selected) {
        //     if(selected !== undefined){
        //         console.log('onSelectStatusPO ===>',selected)
        //         $scope.filter.statusPO = selected.value;
        //     }else{
        //         $scope.filter.statusPO = null;
        //     }
        // }

        // $scope.changeCategoryFilter = function(selected){
        //     if(selected == 3){
        //         console.log('changeKategoryFilter by Nama ===>',selected);
        //         $scope.selectedFilterCategory = 'Nama SA';
        //     }else if(selected == 4){
        //         console.log('changeKategoryFilter by Nama ===>',selected);
        //         $scope.selectedFilterCategory = 'Nama Group / Vendor';
        //     }else{
        //         console.log('changeKategoryFilter by tanggal ===>',selected);
        //         $scope.selectedFilterCategory = "Tanggal WO";
        //     }
        // }

        $scope.onSelectTypePO($scope.dataTipePO[0]);
        $scope.onSelectVendor($scope.dataVendor[0]);
        // $scope.onSelectReferencePO($scope.dataTipePO[0]);
        // $scope.onSelectStatusPO($scope.dataTipePO[0]);

        // //----------------------------------
        // // Get Data
        // //----------------------------------
        // var gridData = [];

        reportPembelianPartsBahanFactory.getDataDealer().then(function(response) {
            console.log('Response BEDealer:', response.data);
            $scope.dataDealer = response.data;
            $scope.filter.Dealer = response.data[0].dealerCode;
        })
        reportPembelianPartsBahanFactory.getDataCabang().then(function(response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabang = response.data;
            $scope.DisabledCabang();
            $scope.filter.Cabang = response.data[0].cabangID;
        })

        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length > 1) {
                $scope.Disabled = false;
                console.log('Cabang Enabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            } else {
                $scope.Disabled = true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };

        // reportPembelianPartsBahanFactory.getDataPIC('teknisi ' + $scope.role).then(function(response) {
        //     console.log('Response BEPIC:', response.data);
        //     // $scope.dataSA = response.data;
        //     $scope.dataTeknisi = response.data; // hmm bisa ketukar sa dan teknisi
        // })

        // reportPembelianPartsBahanFactory.getDataPIC('service advisor ' + $scope.role).then(function(response) {
        //     console.log('Response BEPIC:', response.data);
        //     // $scope.dataTeknisi = response.data;
        //     $scope.dataSA = response.data; // hmm bisa ketukar sa dan teknisi
        // })

        $scope.DisabledTAM = function() {
            if ($scope.userlogin != 'TAM') {
                return true;
                console.log("DisabledTAM TRUE");
            } else {
                return false;
                console.log("DisabledTAM FALSE");
            }
        };

        // $scope.changeTipePencarianWO = function(value) {
        //     // $timeout(function(){
        //     //console.log('value', value);
        //     //console.log('$scope.filter===>', $scope.filter);
        //     if (value == 3 || value == '3') {

        //         var d = new Date();
        //         $scope.filter.DateStart = d;
        //         $scope.filter.DateThru = d;
        //         $scope.filter.DateEnd = d;
        //         $scope.filter.TipePencarianWODisable = true;

        //     } else {

        //         $scope.filter.TipePencarianWODisable = false;
        //     }
        //     // });
        // }

        // $scope.changeTipePIC = function() {
        //     // $timeout(function(){
        //     console.log('tipe pic', $scope.filter.TipePIC);
        //     if ($scope.filter.TipePIC == 1) {

        //         console.log("INI SA");
        //     } else if ($scope.filter.TipePIC == 2) {
        //         console.log("INI Teknisi");
        //     }
        //     // });
        // }
        $scope.getData = function() {
            console.log('filter ===>',$scope.filter);

            //validasi tanngal

            if($scope.filter.typePO == undefined){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Tipe PO belum dipilih",
                });
            }else if($scope.filter.vendor == undefined){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Vendor Filter belum dipilih",
                });
             }else if($scope.filter.DateEnd == undefined){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Tanggal Akhir Filter harus diisi",
                    });
            }else if($scope.filter.DateStart == undefined){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Tanggal Awal Filter harus diisi",
                });
            } else {
                console.log('get data');
                if ($scope.role == "GR") $scope.getSummaryGR();
                else $scope.getSummaryBP();
            }

        }
        $scope.getReport = function() {
            console.log('summary ===>',$scope.summary);

            if ($scope.role == "GR") $scope.getReportGR();
            else $scope.getReportBP();
            console.log('get data');
        }

        function format1(n, currency) {
            console.log('format1', n, currency);
            return currency == null ? 0 : currency + " " + n.toFixed(0).replace(/./g, function(c, i, a) {
                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
            });
        }
        $scope.getSummaryGR = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataFilter = {

                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'tipepo': $scope.filter.typePO,
                'vendor': $scope.filter.vendor,
                'startdate': $filtertanggal,
                'enddate': $filtertanggalakhir
            };

            console.log('OBJ =>', dataFilter)

            reportPembelianPartsBahanFactory.getSummaryGR(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);

                $scope.totalParts = response.data[0].total_pembelian_Parts == null ? 0 : response.data[0].total_pembelian_Parts;
                $scope.totalBahan = response.data[0].total_pembelian_Bahan == null ? 0 : response.data[0].total_pembelian_Bahan;
                $scope.totalPartsBahan = response.data[0].total_pembelian == null ? 0 : response.data[0].total_pembelian;

                $scope.totalAmountBahan = response.data[0].total_amount_Bahan == null ? 0 : response.data[0].total_amount_Bahan;
                $scope.totalAmountBahan = format1($scope.totalAmountBahan, '');

                $scope.totalAmountParts = response.data[0].total_amount_Parts == null ? 0 : response.data[0].total_amount_Parts;
                $scope.totalAmountParts = format1($scope.totalAmountParts, '');

                $scope.totalAmountPartsBahan = response.data[0].total_amount == null ? 0 : response.data[0].total_amount;
                $scope.totalAmountPartsBahan = format1($scope.totalAmountPartsBahan, '');
            })
        }
        $scope.getReportGR = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();

            var dataReport = {
                'GroupDealerCode': $scope.filter.Dealer,
                'OutletId': $scope.filter.Cabang,
                'POType': $scope.filter.typePO,
                'Vendor': $scope.filter.vendor,
                'StartDate': $filtertanggal,
                'EndDate': $filtertanggalakhir
            };

            reportPembelianPartsBahanFactory.getExportGR(dataReport);
        }

        $scope.getSummaryBP = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataFilter = {

                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'tipepo': $scope.filter.typePO,
                'vendor': $scope.filter.vendor,
                'startdate': $filtertanggal,
                'enddate': $filtertanggalakhir,
                'flag' : $scope.filter.categoryDate
            };
            console.log('filter', dataFilter);
            reportPembelianPartsBahanFactory.getSummaryBP(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);

                $scope.totalParts = response.data[0].total_pembelian_Parts == null ? 0 : response.data[0].total_pembelian_Parts;
                $scope.totalBahan = response.data[0].total_pembelian_Bahan == null ? 0 : response.data[0].total_pembelian_Bahan;
                $scope.totalPartsBahan = response.data[0].total_pembelian == null ? 0 : response.data[0].total_pembelian;

                $scope.totalAmountBahan = response.data[0].total_amount_Bahan == null ? 0 : response.data[0].total_amount_Bahan;
                $scope.totalAmountBahan = format1($scope.totalAmountBahan, '');

                $scope.totalAmountParts = response.data[0].total_amount_Parts == null ? 0 : response.data[0].total_amount_Parts;
                $scope.totalAmountParts = format1($scope.totalAmountParts, '');

                $scope.totalAmountPartsBahan = response.data[0].total_amount == null ? 0 : response.data[0].total_amount;
                $scope.totalAmountPartsBahan = format1($scope.totalAmountPartsBahan, '');
            })
        }
        $scope.getReportBP = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();

            var dataReport = {
                'GroupDealerCode': $scope.filter.Dealer,
                'OutletId': $scope.filter.Cabang,
                'POType': $scope.filter.typePO,
                'Vendor': $scope.filter.vendor,
                'StartDate': $filtertanggal,
                'EndDate': $filtertanggalakhir,
                'flag' : $scope.filter.categoryDate
            };

            reportPembelianPartsBahanFactory.getExportBP(dataReport);
        }
    });