angular.module('app')
  .factory('reportPembelianPartsBahanFactory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = ''; //'http://localhost:8080';
    return {
            getDataDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},
            getDataCabang: function() {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            return res;},
            getDataPIC: function(data) {
            console.log('data pic',data);
            var res=$http.get(serverURL + '/api/rpt/pic/'+ currentUser.OrgId +'/'+ data);
            return res;},
            getDataSA: function(data) {
            var res=$http.get(serverURL + '/api/rpt/SA/'+ data.kategori + currentUser.OrgId);
            return res;}, 
            getSummaryBP: function(data) {
              console.log('Data Report:', data);

              var res = $http.get(serverURL + '/api/rpt/AS_PartsDanBahan/'+ data.dealer +'/'+ data.cabang +'/'+ data.tipepo +'/'+ data.vendor +'/'+ data.startdate +'/'+ data.enddate +'/'+ data.flag);
                return res;
            },
            getExportBP: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_PartsDanBahan?GroupDealerCode='+ data.GroupDealerCode +'&OutletId='+ data.OutletId  +'&Type_PO='+ data.POType +'&Vendor='+ data.Vendor +'&StartDate='+ data.StartDate +'&EndDate='+ data.EndDate+'&Dataflag='+data.flag);
            },
            getSummaryGR: function(data) {
              console.log('Data Report:', data);

              var res = $http.get(serverURL + '/api/rpt/AS_PartsDanBahan/'+ data.dealer +'/'+ data.cabang +'/'+ data.tipepo +'/'+ data.vendor +'/'+ data.startdate +'/'+ data.enddate +'/'+ data.flag);
                return res;
            },
            getExportGR: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_PartsDanBahan?GroupDealerCode='+ data.GroupDealerCode +'&OutletId='+ data.OutletId  +'&Type_PO='+ data.POType +'&Vendor='+ data.Vendor +'&StartDate='+ data.StartDate +'&EndDate='+ data.EndDate+'&Dataflag='+data.flag);
            }
    }
  });
