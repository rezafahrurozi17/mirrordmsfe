angular.module('app')
    .controller('ReportKPIBulananBengkelBPController', function ($scope, $http, CurrentUser, KPILaporanBulananBengkelBPFactory) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = false;
        });
        $scope.user = CurrentUser.user();
        $scope.roleId = $scope.user.RoleId;
        $scope.formatDate = function (date) {
            var dateOut = new Date(date);
            return dateOut;
        };
        var dateFormat = 'MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        // var periode = new Date();
		var today = new Date();
		today.setDate(1);
		today.setHours(-1);
		var monthperiode = (today.getMonth() + 1).toString();
		var dayPeriode = today.getDate().toString();
		var yearPeriode = today.getFullYear().toString();
		$scope.periode = yearPeriode + '/' + (monthperiode[1] ? monthperiode : "0" + monthperiode[0]) + '/' + dayPeriode;
		$scope.dateOptions = {
			startingDay: 1,
			format: dateFormat,
			maxDate : $scope.periode
		};
        $scope.dataDealer = [];
        $scope.dataCabang = [];

        function format1(n, currency) {
			return currency + " " + n.toFixed(0).replace(/./g, function (c, i, a) {
				return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
			});
        }
        var convertDecimal = function(item){
			var num = item
			var with2Decimals = num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]
            var totalValue = parseFloat(with2Decimals);
            console.log("=====",item,totalValue)
			return totalValue;
		}
        
        $scope.filter = { Dealer: null, Cabang: null, kategori: "Body and Paint", periode: $scope.periode, listrikkwh: 0, amount25: 0, amount26: 0, amount27: 0, amount28: 0, amount30: 0, amount31: 0, amount32: 0, amount33: 0, amount34: 0, amount37: 0, amount38: 0, amount40: 0, amount41: 0, amount42: 0, amount44: 0, amount67: 0, };
        KPILaporanBulananBengkelBPFactory.getDataDealer().then(function (response) {
            console.log('Response BEDealer:', response.data);
            $scope.dataDealer = response.data;
            $scope.filter.Dealer = response.data[0].dealerCode;
        })
        KPILaporanBulananBengkelBPFactory.getDataCabang().then(function (response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabang = response.data;
            $scope.DisabledCabang();
            $scope.filter.Cabang = response.data[0].cabangID;
        })
        $scope.DisabledCabang = function () {
            if ($scope.dataCabang.length > 1) {
                $scope.Disabled = false;
                console.log('Cabang Enabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else {
                $scope.Disabled = true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
        $scope.getData = function () {
            console.log('get data');
        }
        $scope.allowPattern = function(item, event,param) {
            console.log("item", item);
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
				if(param == 0){
					$scope.filter.SLC = $scope.filter.SLC.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
				}else if(param == 1){
					$scope.filter.SDE = $scope.filter.SDE.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
				}
                
                return;
            }
        };
        $scope.simpan = function () {
            var $tanggal1 = $scope.filter.periode;
            var $periode = $tanggal1.getFullYear() + '-' + ($tanggal1.getMonth() + 1) + '-' + $tanggal1.getDate();
            $scope.filter.SLC = $scope.filter.SLC.replace(/,/g, '');
			$scope.filter.SDE = $scope.filter.SDE.replace(/,/g, '');
            var dataFilter = {
                'cabang': $scope.filter.Cabang,
                'periode': $periode,
                'listrik': $scope.filter.listrikkwh,
                'amount25': $scope.filter.amount25,
                'amount26': $scope.filter.amount26,
                'amount27': $scope.filter.amount27,
                'amount28': $scope.filter.amount28,
                'amount30': $scope.filter.amount30,
                'amount31': $scope.filter.amount31,
                'amount32': $scope.filter.amount32,
                'amount33': $scope.filter.amount33,
                'amount34': $scope.filter.amount34,
                'amount37': $scope.filter.amount37,
                'amount38': $scope.filter.amount38,
                'amount40': $scope.filter.amount40,
                'amount41': $scope.filter.amount41,
                'amount42': $scope.filter.amount42,
                'amount44': $scope.filter.amount44,
                'SLC': $scope.filter.SLC,
                'SDE': $scope.filter.SDE
            };

            KPILaporanBulananBengkelBPFactory.save(dataFilter).then(function (response) {
                console.log('Response BE Filter:', response.data);
                // $scope.dataSummary = response.data;
                if (response.data.length > 0) {
                    var hasil = response.data[0];
                    $scope.filter.listrikkwh = hasil.listrik;
                    $scope.filter.amount25 = hasil.amount25;
                    $scope.filter.amount26 = hasil.amount26;
                    $scope.filter.amount27 = hasil.amount27;
                    $scope.filter.amount28 = hasil.amount28;
                    $scope.filter.amount29 = hasil.amount25 + hasil.amount26 + hasil.amount27 + hasil.amount28;
                    $scope.filter.amount30 = hasil.amount30;
                    $scope.filter.amount31 = hasil.amount31;
                    $scope.filter.amount32 = hasil.amount32;
                    $scope.filter.amount33 = hasil.amount33;
                    $scope.filter.amount34 = hasil.amount34;
                    $scope.filter.amount35 = hasil.amount30 + hasil.amount31 + hasil.amount32 + hasil.amount33 + hasil.amount34;
                    $scope.filter.amount36 = $scope.filter.amount29 + $scope.filter.amount35;
                    $scope.filter.amount37 = hasil.amount37;
                    $scope.filter.amount38 = hasil.amount38;
                    $scope.filter.amount39 = hasil.amount37 + hasil.amount38;
                    $scope.filter.amount40 = hasil.amount40;
                    $scope.filter.amount41 = hasil.amount41;
                    $scope.filter.amount42 = hasil.amount42;
                    $scope.filter.amount43 = hasil.amount40 + hasil.amount41 + hasil.amount42;
                    $scope.filter.amount44 = hasil.amount44;
                    $scope.filter.SLC = hasil.slc;
                    $scope.filter.SDE = hasil.sde;
                } else {
                    $scope.filter.listrikkwh = 0;

                    $scope.filter.amount25 = 0;
                    $scope.filter.amount26 = 0;
                    $scope.filter.amount27 = 0;
                    $scope.filter.amount28 = 0;
                    $scope.filter.amount29 = 0;
                    $scope.filter.amount30 = 0;
                    $scope.filter.amount31 = 0;
                    $scope.filter.amount32 = 0;
                    $scope.filter.amount33 = 0;
                    $scope.filter.amount34 = 0;
                    $scope.filter.amount35 = 0;
                    $scope.filter.amount36 = 0;
                    $scope.filter.amount37 = 0;
                    $scope.filter.amount38 = 0;
                    $scope.filter.amount39 = 0;
                    $scope.filter.amount40 = 0;
                    $scope.filter.amount41 = 0;
                    $scope.filter.amount42 = 0;
                    $scope.filter.amount43 = 0;
                    $scope.filter.amount44 = 0;
                    $scope.filter.SLC = 0;
                    $scope.filter.SDE = 0;
                }
            })
        }
        $scope.tampilkan = function () {
            var $tanggal1 = $scope.filter.periode;
            var $periode = $tanggal1.getFullYear() + '-' + ($tanggal1.getMonth() + 1) + '-' + $tanggal1.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periode': $periode//,
                // 'listrik': 0,
                // 'amount25':0,
                // 'amount26':0,
                // 'amount27':0,
                // 'amount28':0,
                // 'amount30':0,
                // 'amount31':0,
                // 'amount32':0,
                // 'amount33':0,
                // 'amount34':0,
                // 'amount37':0,
                // 'amount38':0,
                // 'amount40':0,
                // 'amount41':0,
                // 'amount42':0,
                // 'amount44':0

            };

            KPILaporanBulananBengkelBPFactory.tampilkan(dataFilter).then(function (response) {
                console.log('Response BE Filter:', response.data);
                // $scope.dataSummary = response.data;
                if (response.data.length > 0) {
                    var hasil = response.data[0];
                    $scope.filter.listrikkwh = hasil.listrik;

                    $scope.filter.amount25 = hasil.amount25;
                    $scope.filter.amount26 = hasil.amount26;
                    $scope.filter.amount27 = hasil.amount27;
                    $scope.filter.amount28 = hasil.amount28;
                    $scope.filter.amount29 = hasil.amount25 + hasil.amount26 + hasil.amount27 + hasil.amount28;
                    $scope.filter.amount30 = hasil.amount30;
                    $scope.filter.amount31 = hasil.amount31;
                    $scope.filter.amount32 = hasil.amount32;
                    $scope.filter.amount33 = hasil.amount33;
                    $scope.filter.amount34 = hasil.amount34;
                    $scope.filter.amount35 = hasil.amount30 + hasil.amount31 + hasil.amount32 + hasil.amount33 + hasil.amount34;
                    $scope.filter.amount36 = $scope.filter.amount29 + $scope.filter.amount35;
                    $scope.filter.amount37 = hasil.amount37;
                    $scope.filter.amount38 = hasil.amount38;
                    $scope.filter.amount39 = hasil.amount37 + hasil.amount38;
                    $scope.filter.amount40 = hasil.amount40;
                    $scope.filter.amount41 = hasil.amount41;
                    $scope.filter.amount42 = hasil.amount42;
                    $scope.filter.amount43 = hasil.amount40 + hasil.amount41 + hasil.amount42;
                    $scope.filter.amount44 = hasil.amount44;
                    var ServiceCost = hasil.serviceCost;
                    $scope.filter.ServiceCost = format1(ServiceCost, '');
                    console.log('$scope.filter.ServiceCost', $scope.filter.ServiceCost);
                    var ServiceRevenue = hasil.serviceRevenue;
                    $scope.filter.ServiceRevenue = format1(ServiceRevenue, '');
                    console.log('$scope.filter.ServiceRevenue', $scope.filter.ServiceRevenue);
                    
                    $scope.filter.absorption_rate = convertDecimal((ServiceRevenue - ServiceCost) / hasil.sde * 100);
                    
                    console.log('$scope.filter.absorption_rate', $scope.filter.absorption_rate);

                    $scope.filter.SLC = hasil.slc;
                    $scope.filter.SLC = format1($scope.filter.SLC, '');
                    $scope.filter.SDE = hasil.sde;
                    $scope.filter.SDE = format1($scope.filter.SDE, '');
                } else {
                    $scope.filter.listrikkwh = 0;

                    $scope.filter.amount25 = 0;
                    $scope.filter.amount26 = 0;
                    $scope.filter.amount27 = 0;
                    $scope.filter.amount28 = 0;
                    $scope.filter.amount29 = 0;
                    $scope.filter.amount30 = 0;
                    $scope.filter.amount31 = 0;
                    $scope.filter.amount32 = 0;
                    $scope.filter.amount33 = 0;
                    $scope.filter.amount34 = 0;
                    $scope.filter.amount35 = 0;
                    $scope.filter.amount36 = 0;
                    $scope.filter.amount37 = 0;
                    $scope.filter.amount38 = 0;
                    $scope.filter.amount39 = 0;
                    $scope.filter.amount40 = 0;
                    $scope.filter.amount41 = 0;
                    $scope.filter.amount42 = 0;
                    $scope.filter.amount43 = 0;
                    $scope.filter.amount44 = 0;
                    $scope.filter.SLC = 0;
                    $scope.filter.SDE = 0;
                    $scope.filter.serviceCost = 0;
                    $scope.filter.serviceRevenue = 0;
                    
                    $scope.filter.absorption_rate = 0;

                    $scope.filter.SLC = hasil.slc;
                    $scope.filter.SDE = hasil.sde;
                }

            })

        }
        $scope.getReport = function () {
            var $tanggal = $scope.filter.periode;
            var $filtertanggal = $tanggal.getFullYear() + '/' + ($tanggal.getMonth() + 1) + '/' + $tanggal.getDate();
            var dataReport = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'kategori': $scope.filter.kategori,
                'periode': $filtertanggal
            };

            KPILaporanBulananBengkelBPFactory.getExport(dataReport);
        }
    });
