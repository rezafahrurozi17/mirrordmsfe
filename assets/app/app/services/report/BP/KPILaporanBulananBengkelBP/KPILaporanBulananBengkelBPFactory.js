angular.module('app')
  .factory('KPILaporanBulananBengkelBPFactory', function ($http, CurrentUser, $q) {
    console.log('Service GR Bulanan KPILaporanBulananBengkelGRFactory Loaded...');
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = '';//http://localhost:8080';
    return {
      getDataDealer: function () {
        var res = $http.get(serverURL + '/api/rpt/Dealer/' + currentUser.OrgId);
        return res;
      },
      getDataCabang: function () {
        var res = $http.get(serverURL + '/api/rpt/Cabang/' + currentUser.OrgId);
        return res;
      },
      getData: function () {
        var res = $http.get('/api/fw/Role');
        console.log('data=>', res);
        //---------
        return $q.resolve(data);
      },
      save: function (data) {

        console.log('Data Filter:', data);
        var res = $http.get(serverURL + '/api/rpt/AS_KPILaporanBulananBengkel_BPCAB/' + data.cabang + '/' + data.periode + '/' + data.listrik + '/' +
          data.amount25 + '/' + data.amount26 + '/' + data.amount27 + '/' + data.amount28 + '/' + data.amount30 + '/' + data.amount31 + '/' + data.amount32 + '/' + data.amount33 + '/' + data.amount34 + '/' +
          data.amount37 + '/' + data.amount38 + '/' + data.amount40 + '/' + data.amount41 + '/' + data.amount42 + '/' + data.amount44 + '/' + data.SLC + '/' + data.SDE + '/' + 'save');
        return res;
      },
      tampilkan: function (data) {

        console.log('Data Filter:', data);
        var res = $http.get(serverURL + '/api/rpt/AS_KPILaporanBulananBengkel_BPCAB2/' + data.cabang + '/' + data.periode + '/BP');
        return res;
      },
      getExport: function (data) {
        console.log('Data Report:', data);
        // window.open(serverURL + 'AS_KPILaporanBulananBengkel_BPCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periode +'&kategoriWorkshop='+ data.kategori);
        window.open(serverURL + '/api/rpt/AS_KPILaporanBulananBengkel_BPCAB?dealer=' + data.dealer + '&cabang=' + data.cabang + '&periode=' + data.periode + '&kategoriWorkshop=' + data.kategori);
      }
    }
  });
