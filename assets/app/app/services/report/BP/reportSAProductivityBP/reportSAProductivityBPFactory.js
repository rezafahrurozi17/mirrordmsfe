angular.module('app')
  .factory('ReportSAProductivityBP', function($http, CurrentUser,$q) {
     console.log('Service SAProductivity BP Loaded...');
    var serverURL = ''; //'http://localhost:8080'
    var currentUser = CurrentUser.user();
    return {
            getDataDealer: function() {
              var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
              return res;},
            // getDataCabang: function() {
            //   var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            //   return res;},
            getDataCabang: function(data) {
              var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId + '/' + data.kategori);
              return res;},
            getDataCabangHONew: function(data) {
              console.log('Data Get Cabang:', data);
              var res=$http.get(serverURL + '/api/rpt/GetCabangHONew/'+ data.areaId + '/' +  data.DealerId  + '/' + data.kategori);
              return res;},
            getDataSatellite: function() {
            var res=$http.get(serverURL + '/api/rpt/Satellite/');
            return res;},
            getDataAreaTAM: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaTAM/');
            return res;},
            getDataAreaDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaDealer/');
            return res;},
            getDataAreaDealerHO: function(data) {
            var res=$http.get(serverURL + '/api/rpt/AreaDealerHONew/'+ data.isGR );
            return res;},
            getDataSA: function() {
              var res=$http.get(serverURL + '/api/rpt/SA/BP/'+ currentUser.OrgId);
              return res;},

            // ========================================== HO ======================
            getSummaryUnitHO: function(data) {
              console.log('Data Report:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_SAProductivityByUnit_BPHO/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.tamarea +'/'+ data.dealerarea +'/'+ data.satelite +'/'+ data.kategori +'/'+ data.jenis);
                return res;
            },
            getExportUnitHO: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_SAProductivityByUnit_BPHO?dealer='+ data.dealer +'&cabang='+ data.cabang +'&Jenis='+ data.jenis +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir +'&TamArea='+ data.tamarea +'&DealerArea='+ data.dealerarea +'&kategori='+ data.kategori +'&satelite='+ data.satelite);
            },
            getSummaryAmountHO: function(data) {
              console.log('Data Report:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_SAProductivityAmount_GRHO/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.tamarea +'/'+ data.dealerarea +'/'+ data.kategori +'/summary');
                return res;
            },
            getExportAmountHO: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_SAProductivityByAmount_BPHO?dealer='+ data.dealer +'&cabang='+ data.cabang +'&Jenis='+ data.jenis +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir +'&TamArea='+ data.tamarea +'&DealerArea='+ data.dealerarea +'&kategori='+ data.kategori +'&satelite='+ data.satelite);
            },


            // ============================== CABANG ====================================

            getSummaryUnitCabang: function(data) {
              console.log('Data Report:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_SAProductivityByUnit_BPCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.satelite +'/'+ data.sa);
                return res;
            },
            getSummaryAmountCabang: function(data) {
              console.log('Data Report:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_SAProductivityByAmount_BPCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.satelite +'/'+ data.sa);
                return res;
            },
            getExportUnitCabang: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_SAProductivityByUnit_BPCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir +'&satelite='+ data.satelite +'&sa='+ data.sa);
            },
            getExportAmountCabang: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_SAProductivityByAmount_BPCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir +'&satelite='+ data.satelite +'&sa='+ data.sa);
            }
    }
  });
