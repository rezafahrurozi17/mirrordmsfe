angular.module('app')
    .controller('ReportSAProductivityBPController', function($scope, $http, $stateParams,CurrentUser, ReportSAProductivityBP, bsNotify,$timeout ) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
        });
        $scope.user = CurrentUser.user();
        $scope.roleId = $scope.user.RoleId;
        $scope.role=$stateParams.role;
        console.log('role',$scope.role);
        // //----------------------------------
        // // Initialization
        // //----------------------------------
        // $scope.user = CurrentUser.user();
        // $scope.filter = {Dealer:null, Cabang:null, KategoriWorkshop:"Seminar", DateStart:null, DateThru:null, Satellite:null};
        // //----------------------------------
        // // Get Data
        // //----------------------------------

        $scope.formatDate = function(date) {
            var dateOut = new Date(date);
            return dateOut;
        };
        var dateFormat = 'MM/yyyy';
        var dateFormat2 = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var periode=new Date();
        $scope.maxDate = new Date(periode.getFullYear(), periode.getMonth(), periode.getDate());
        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat2,
            // mode:"'month'",
        };
        $scope.dateOptions2 = {
            startingDay: 1,
            format: dateFormat2,
            // mode:"'month'",
        };
        // //----------------------------------
        // // Get Data
        // //----------------------------------
        $scope.dataDealer = [];
        $scope.dataCabang = [];
        $scope.dataAreaTAM = [];
        $scope.dataAreaDealer = [];
        $scope.dataSatellite = [];
        $scope.filter = {Dealer:null,DealerId:null, Cabang:null, kategori:"Body and Paint", DateStart:periode,DateEnd:periode, Satellite:null, dealerarea:0,tamarea:0,isGR:0 };
        ReportSAProductivityBP.getDataDealer().then(function(response) {
            console.log('Response BEDealer:', response.data);
            $scope.dataDealer = response.data;
            $scope.filter.Dealer=response.data[0].dealerCode;
            $scope.filter.DealerId=response.data[0].dealerID;
            ReportSAProductivityBP.getDataAreaDealerHO($scope.filter).then(function (response) {
                console.log('ayamtest',response.data)
                $scope.dataGroupDealerHO = response.data
                $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
                $scope.areaId($scope.dataGroupDealerHO[0])
            })
        })
        ReportSAProductivityBP.getDataCabang($scope.filter).then(function(response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabang = response.data;
            $scope.DisabledCabang();
            $scope.filter.Cabang=response.data[0].cabangID;
        })
        ReportSAProductivityBP.getDataAreaTAM().then(function(response) {
            console.log('Response AreaTAM:', response.data);
            $scope.dataAreaTAM = response.data;
        })
        ReportSAProductivityBP.getDataAreaDealer().then(function(response) {
            console.log('Response AreaDealer:', response.data);
            $scope.dataAreaDealer = response.data;
        })
        ReportSAProductivityBP.getDataAreaDealerHO($scope.filter).then(function(response) {
            console.log('Response AreaDealerHO:', response.data);
            $scope.dataAreaDealerHO = response.data;
            $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
            $scope.areaId($scope.dataAreaDealerHO[0])
        })
        ReportSAProductivityBP.getDataSatellite().then(function(response) {
            console.log('Response AreaSatellite:', response.data);
            $scope.dataSatellite = response.data;
        })
        ReportSAProductivityBP.getDataSA().then(function (response) {
            console.log('Response SA:', response.data);
            $scope.dataSA = response.data;
          })
    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
        $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };

        
        $scope.areaId = function(row) {
            // $scope.DataDealer = row
            $scope.DataDealer = Object.assign(row,{'kategori': $scope.filter.kategori})
            if($scope.userlogin != 'TAM'){
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
            }else{
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
            }
            console.log('row dealerid', row,$scope.DataDealer);
            ReportSAProductivityBP.getDataCabangHONew($scope.DataDealer).then(function (response) {
                console.log('Response BECabang:', response.data);
                $scope.dataCabangHO = response.data;
                $scope.DisabledCabang();
                $scope.filter.CabangHO = null
                console.log('kambing',$scope.filter)
                $timeout(function() {
                    $scope.filter.CabangHO = response.data[0].cabangID;
                    console.log('kambing123',$scope.filter.CabangHO)
                }, 100);
            })
        };

        $scope.dealerCode = function(row){
            row = Object.assign(row,{'isGR':$scope.filter.isGR})
            $scope.filter.DealerId = row.dealerID
            ReportSAProductivityBP.getDataAreaDealerHO(row).then(function (response) {
                console.log('ayam',response.data)
                console.log('rusa',$scope.dataGroupDealer)
                $scope.dataGroupDealer = response.data
                $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
                $scope.areaId($scope.dataGroupDealer[0])
                console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
            })
        }

        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };

function format1(n, currency) {
    return currency + " " + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}

        $scope.getData = function() {
            if ($scope.role == "Branch") $scope.getSummaryCabang();
            else $scope.getSummaryHO();
            console.log('get data');
        }
        $scope.getSummaryHO = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataFilter = {
                'kategori': "BP",
                'jenis': "summary",
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tamarea': $scope.filter.tamarea,
                'satelite': $scope.filter.satellite,
                'dealerarea': $scope.filter.dealerarea,
            };

            ReportSAProductivityBP.getSummaryUnitHO(dataFilter).then(function(response) {
                console.log('Response BE Filter Unit:', response.data);
                // $scope.dataSummary = response.data;

                $scope.unitEntry = response.data[0].unitEntry;
                $scope.unitEntry = Math.round($scope.unitEntry * 100) / 100;
                $scope.unitEntry = format1($scope.unitEntry,'');
                $scope.totalSA = response.data[0].totalSA;
                $scope.totalSA = Math.round($scope.totalSA * 100) / 100;
                $scope.totalSA = format1($scope.totalSA,'');
                $scope.unitSA = response.data[0].unitSA;
                $scope.unitSA = Math.round($scope.unitSA * 100) / 100;
                $scope.otdRate = response.data[0].otdRate;
                $scope.otdRate = Math.round($scope.otdRate * 100) / 100;
                $scope.deliveryRate = response.data[0].deliveryRate;
                $scope.deliveryRate = Math.round($scope.deliveryRate * 100) / 100;
                });

            ReportSAProductivityBP.getSummaryAmountHO(dataFilter).then(function (response) {
                console.log('Response BE Filter Amount:', response.data);
                //$scope.jasa = response.data[0].jasa / 1000;
                $scope.jasa = response.data[0].jasa;
                $scope.jasa = Math.round($scope.jasa * 100) / 100;
                $scope.jasa = format1($scope.jasa,'');
                //$scope.part = response.data[0].parts / 1000;
                $scope.part = response.data[0].parts;
                $scope.part = Math.round($scope.part * 100) / 100;
                $scope.part = format1($scope.part,'');
                //$scope.bahan = response.data[0].bahan / 1000;
                $scope.bahan = response.data[0].bahan;
                $scope.bahan = Math.round($scope.bahan * 100) / 100;
                $scope.bahan = format1($scope.bahan,'');
                //$scope.opl = response.data[0].opl / 1000;
                $scope.opl = response.data[0].opl;
                $scope.opl = Math.round($scope.opl * 100) / 100
                $scope.opl = format1($scope.opl,'');
                //$scope.opb = response.data[0].opb / 1000;
                $scope.opb = response.data[0].opb;
                $scope.opb = Math.round($scope.opb * 100) / 100;
                $scope.opb = format1($scope.opb,'');
                //$scope.totalRevenue = (response.data[0].jasa + response.data[0].parts + response.data[0].bahan + response.data[0].opl + response.data[0].opb) / 1000;
                //$scope.totalRevenue = (response.data[0].jasa + response.data[0].parts + response.data[0].bahan + response.data[0].opl + response.data[0].opb);
                $scope.totalRevenue = response.data[0].totalRevenue;
                $scope.totalRevenue = Math.round($scope.totalRevenue * 100) / 100;
                $scope.totalRevenue = format1($scope.totalRevenue,'');
            });
        }

        $scope.getReportUnitHO = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataReport = {
                'kategori': "BP",
                'jenis': "export",
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tamarea': $scope.filter.tamarea,
                'satelite': $scope.filter.satellite,
                'dealerarea': $scope.filter.dealerarea,
            };

            ReportSAProductivityBP.getExportUnitHO(dataReport);
        }

        $scope.getReportAmountHO = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataReport = {
                'kategori': "BP",
                'jenis': "export",
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tamarea': $scope.filter.tamarea,
                'satelite': $scope.filter.satellite,
                'dealerarea': $scope.filter.dealerarea,
            };

            ReportSAProductivityBP.getExportAmountHO(dataReport);
        }

        $scope.getSummaryCabang = function() {
            if($scope.filter.SA == undefined  ){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Terdapat Data Kosong Harus Diisi",
                });
            }else{
                var $tanggal = $scope.filter.DateStart;
                var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
                var $tanggalakhir = $scope.filter.DateEnd;
                var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
                var dataFilter = {
                    'kategori': "BP",
                    'jenis': "summary",
                    'dealer': $scope.filter.Dealer,
                    'cabang': $scope.filter.Cabang,
                    'periodeakhir': $filtertanggalakhir,
                    'periodeawal': $filtertanggal,
                    'tamarea': $scope.filter.tamarea,
                    'satelite': $scope.filter.satellite,
                    'dealerarea': $scope.filter.dealerarea,
                    'sa': $scope.filter.SA,
                };
            }

            ReportSAProductivityBP.getSummaryUnitCabang(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);
                // $scope.dataSummary = response.data;

                $scope.unitEntry = response.data[0].unitEntry;
                $scope.unitEntry = Math.round($scope.unitEntry * 100) / 100
                $scope.unitEntry = format1($scope.unitEntry,'');
                $scope.totalSA = response.data[0].unitEntryTotalSA;
                $scope.totalSA = Math.round($scope.totalSA * 100) / 100
                $scope.totalSA = format1($scope.totalSA,'');
                $scope.unitSA = response.data[0].unitEntryUnitSADay;
                $scope.unitSA = Math.round($scope.unitSA * 100) / 100
                // $scope.unitSA = format1($scope.unitSA,'');
                $scope.otdRate = response.data[0].unitEntryOTDRate;
                $scope.otdRate = Math.round($scope.otdRate * 100) / 100
                //$scope.otdRate = format1($scope.otdRate,'');
                $scope.deliveryRate = response.data[0].unitEntryDeliveryRate;
                $scope.deliveryRate = Math.round($scope.deliveryRate * 100) / 100
                //$scope.deliveryRate = format1($scope.deliveryRate,'');
            })
            ReportSAProductivityBP.getSummaryAmountCabang(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);
                // $scope.dataSummary = response.data;
                //$scope.jasa = response.data[0].saJasa / 1000;
                $scope.jasa = response.data[0].saJasa;
                $scope.jasa = Math.round($scope.jasa * 100) / 100
                $scope.jasa = format1($scope.jasa,'');
                //$scope.part = response.data[0].saPart / 1000;
                $scope.part = response.data[0].saPart;
                $scope.part = Math.round($scope.part * 100) / 100
                $scope.part = format1($scope.part,'');
                //$scope.bahan = response.data[0].saBahan / 1000;
                $scope.bahan = response.data[0].saBahan;
                $scope.bahan = Math.round($scope.bahan * 100) / 100
                $scope.bahan = format1($scope.bahan,'');
                //$scope.opl = response.data[0].saopl / 1000;
                $scope.opl = response.data[0].saopl;
                $scope.opl = Math.round($scope.opl * 100) / 100
                $scope.opl = format1($scope.opl,'');
                //$scope.opb = response.data[0].saopb / 1000;
                $scope.opb = response.data[0].saopb;
                $scope.opb = Math.round($scope.opb * 100) / 100
                $scope.opb = format1($scope.opb,'');
                //$scope.totalRevenue = (response.data[0].saJasa + response.data[0].saPart + response.data[0].saBahan + response.data[0].saopl + response.data[0].saopb) / 1000;
                //$scope.totalRevenue = (response.data[0].saJasa + response.data[0].saPart + response.data[0].saBahan + response.data[0].saopl + response.data[0].saopb);
                $scope.totalRevenue = response.data[0].totalRevenue;
                $scope.totalRevenue = Math.round($scope.totalRevenue * 100) / 100;
                $scope.totalRevenue = format1($scope.totalRevenue,'');
            })
        }

        $scope.getReportUnitCabang = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataReport = {
                'kategori': "BP",
                'jenis': "export",
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tamarea': $scope.filter.tamarea,
                'satelite': $scope.filter.satellite,
                'dealerarea': $scope.filter.dealerarea,
                'sa': $scope.filter.SA,
            };
            if($scope.filter.SA == undefined  ){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Terdapat Data Kosong Harus Diisi",
                });
            }else{
                ReportSAProductivityBP.getExportUnitCabang(dataReport);
            }
        }
        $scope.getReportAmountCabang = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataReport = {
                'kategori': "BP",
                'jenis': "export",
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tamarea': $scope.filter.tamarea,
                'satelite': $scope.filter.satellite,
                'dealerarea': $scope.filter.dealerarea,
                'sa': $scope.filter.SA,
            };
            if($scope.filter.SA == undefined  ){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Terdapat Data Kosong Harus Diisi",
                });
            }else{
                ReportSAProductivityBP.getExportAmountCabang(dataReport);
            }
        }

        // var gridData = [];

        // $scope.getDealer = [
        // {DealerId:1,DealerName:"Dealer001"},
        // {DealerId:2,DealerName:"Dealer002"}
        // ];

        // $scope.getCabang = [
        // {cabangID:1,CabangName:"Cabang001"},
        // {cabangID:2,CabangName:"Cabang002"}
        // ];

        // $scope.getSatellite = [
        // {SatelliteId:1,SatelliteName:"Satellite001"},
        // {SatelliteId:2,SatelliteName:"Satellite002"}
        // ];

        // $scope.getData = function() {
        //     console.log("getData=>",$scope.filter);
        // }
    });
