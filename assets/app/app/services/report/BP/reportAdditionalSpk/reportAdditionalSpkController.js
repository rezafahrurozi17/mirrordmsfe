angular.module('app')
    .controller('ReportAdditionalSpkController', function($scope, $http, CurrentUser, ReportAdditionalSpk ) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        // $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.dateOptionsS = {
        startingDay: 1,
        format: dateFormat,
        
        // mode:"'month'",
        //disableWeekend: 1
    };
    $scope.dateOptionsE = {
        startingDay: 1,
        format: dateFormat,
        
        // mode:"'month'",
        //disableWeekend: 1
    };


     //===============validasi date start and date end===========
     var today = new Date();
     $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(), today.getDate());  
     $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());      
     $scope.minDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());          


     $scope.changeStartDate = function(tgl){
         var today = new Date();
         var dayOne = new Date($scope.filter.DateStart);
         var dayTwo = new Date($scope.filter.DateEnd);
         
         $scope.dateOptionsE.minDate = new Date(dayOne.getFullYear(),dayOne.getMonth(), dayOne.getDate());
         if (tgl == null || tgl == undefined){
             $scope.filter.DateEnd = null;
         } else {
             if ($scope.filter.DateStart < $scope.filter.DateEnd){

             } else {
                 if (dayOne > today){
                     $scope.filter.DateStart = today;
                     $scope.filter.DateEnd = $scope.filter.DateStart;                    
                 } else {
                     $scope.filter.DateEnd = $scope.filter.DateStart;
                 }
             }
         }
     }

     $scope.changeEndDate = function(tgl){
         var today = new Date();
         var dayOne = new Date($scope.filter.DateStart);
         var dayTwo = new Date($scope.filter.DateEnd);
 
         if (dayTwo > today){
             $scope.dateOptionsE.minDate = $scope.filter.DateStart;
             $scope.filter.DateEnd = today;
         }
     }

     //=============== End of validasi date start and date end===========






    if ($scope.roleId==1128 || $scope.roleId==1129) {
        $scope.filter = {kategori:"Body and Paint"};
    } else {
        $scope.filter = {kategori:"Body and Paint"};    
    }

    $scope.getDealer = [];
    $scope.getCabang = [];
    $scope.getSatellite = [];
    $scope.filter = {Dealer:null, Cabang:null, kategori:"Body and Paint", DateStart:periode,DateEnd:periode, Satellite:null, dealerarea:0,tamarea:0 };
    ReportAdditionalSpk.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.getDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
    })
    ReportAdditionalSpk.getDataCabang().then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.getCabang = response.data;
        // $scope.DisabledCabang();
        // $scope.filter.Cabang=response.data[0].cabangID;
        $scope.filter.Cabang=$scope.user.OrgId;     
        $scope.Disabled=true;   
    })
    ReportAdditionalSpk.getDataSatellite().then(function (response) {
        console.log('Response BESatellite:', response.data);
        $scope.getSatellite = response.data;
    })
    $scope.DisabledCabang = function() {
            if ($scope.getCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.getCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.getCabang.length + ' ' + $scope.Disabled);
            }
        };
    $scope.getData = function() {
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'kategori_workshop': $scope.filter.kategori,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,            
            'satellite': $scope.filter.Satellite
        };

        ReportAdditionalSpk.getSummaryFilter(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;
            var hasil=response.data[0];
            $scope.totalAdditional = hasil.totalAdditional;
            $scope.totalAdditional = Math.round($scope.totalAdditional * 100) / 100
            $scope.totalSPK = hasil.totalSPK;
            $scope.totalSPK = Math.round($scope.totalSPK * 100) / 100
            if($scope.totalAdditional == 0){
                $scope.persentase = parseFloat(100.00).toFixed(2);
            }else{
                $scope.persentase = hasil.totalSPK > 0 ? ((hasil.totalSPK - hasil.totalAdditional)/hasil.totalAdditional)*100 : 0;
                $scope.persentase = Math.round($scope.persentase * 100) / 100 
            }

        })
    }

    $scope.getReport = function(){
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'kategori_workshop': $scope.filter.kategori,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,            
            'satellite': $scope.filter.Satellite
        };

        ReportAdditionalSpk.getSummaryExport(dataFilter);
    }
   
    
});
