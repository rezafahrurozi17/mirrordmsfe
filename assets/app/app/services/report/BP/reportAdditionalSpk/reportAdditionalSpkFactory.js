angular.module('app')
  .factory('ReportAdditionalSpk', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    var serverURL = ''; //'http://localhost:8080'
    console.log(currentUser);
    return {
      getDataDealer: function() {
        var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
        return res;},
      getDataCabang: function() {
        var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
        return res;},
      getDataSatellite: function() {
            var res=$http.get(serverURL + '/api/rpt/Satellite/');
            return res;},
      getSummaryFilter: function(data) {
        console.log('Data Filter:', data);
        // console.log('periode',moment(data.periode).format('YYYY-MM-DD'))
        var res = $http.get('/api/rpt/AS_AdditionalSPK_BPCAB/'+ data.dealer +'/'+ data.cabang  +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/'+ data.satellite +'/'+ data.kategori_workshop);        
        return res;
      },
      getSummaryExport: function(data) {
        console.log('Data Report:', data);
        window.open(serverURL + '/api/rpt/AS_AdditionalSPK_BPCAB?dealer='+ data.dealer +'&cabang='+ data.cabang  +'&kategori='+ data.kategori_workshop +'&datestart='+ data.periode_awal +'&dateend='+ data.periode_akhir +'&satelite='+ data.satellite);
        // var res = $http.get(serverURL + 'CCustomerCompleteness?area='+ data.area +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periode +'&jenis_pelanggan='+ data.jenispel);
        // return res;
      }

    }
  });
