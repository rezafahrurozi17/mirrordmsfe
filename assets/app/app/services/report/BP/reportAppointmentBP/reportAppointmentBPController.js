angular.module('app')
    .controller('ReportAppointmentBPController', function($scope,$stateParams, $http, CurrentUser, ReportAppointmentBP,$timeout  ) {
        //----------------------------------
        // Start-Up

        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
        });
        $scope.user = CurrentUser.user();
        $scope.roleId = $scope.user.RoleId;
        $scope.role=$stateParams.role;
        console.log('role',$scope.role);
        $scope.formatDate = function(date) {
            var dateOut = new Date(date);
            return dateOut;
        };
        var dateFormat = 'MM/yyyy';
        var dateFormat2 = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var periode=new Date();
        $scope.dateOptionsS = {
            startingDay: 1,
            format: dateFormat2,

            // mode:"'month'",
        };
        $scope.dateOptionsE = {
            startingDay: 1,
            format: dateFormat2,

            // mode:"'month'",
        };
        $scope.dateOptions2 = {
            startingDay: 1,
            format: dateFormat2,

            // mode:"'month'",
        };


        //===============validasi date start and date end===========
        var today = new Date();
        $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(), today.getDate());
        $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());
        $scope.minDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());


        $scope.changeStartDate = function(tgl){
            var today = new Date();
            var dayOne = new Date($scope.filter.DateStart);
            var dayTwo = new Date($scope.filter.DateEnd);

            $scope.dateOptionsE.minDate = new Date(dayOne.getFullYear(),dayOne.getMonth(), dayOne.getDate());
            if (tgl == null || tgl == undefined){
                $scope.filter.DateEnd = null;
            } else {
                if ($scope.filter.DateStart < $scope.filter.DateEnd){

                } else {
                    if (dayOne > today){
                        $scope.filter.DateStart = today;
                        $scope.filter.DateEnd = $scope.filter.DateStart;
                    } else {
                        $scope.filter.DateEnd = $scope.filter.DateStart;
                    }
                }
            }
        }

        $scope.changeEndDate = function(tgl){
            var today = new Date();
            var dayOne = new Date($scope.filter.DateStart);
            var dayTwo = new Date($scope.filter.DateEnd);

            if (dayTwo > today){
                $scope.dateOptionsE.minDate = $scope.filter.DateStart;
                $scope.filter.DateEnd = today;
            } else if (dayTwo < dayOne) {
                $scope.filter.DateEnd = $scope.filter.DateStart;

            }
        }

    //=============== End of validasi date start and date end===========

        $scope.dataDealer = [];
        $scope.dataCabang = [];
        $scope.dataAreaTAM = [];
        $scope.dataAreaDealer = [];
        $scope.dataSatellite = [];
        $scope.filter = {Dealer:null,DealerId:null, Cabang:null, kategori:"Body and Paint", DateStart:periode,DateEnd:periode, satellite:null, dealerarea:0,tamarea:0,isGR:0 };
        ReportAppointmentBP.getDataDealer().then(function(response) {
            console.log('Response BEDealer:', response.data);
            $scope.dataDealer = response.data;
            $scope.filter.Dealer=response.data[0].dealerCode;
            $scope.filter.DealerId=response.data[0].dealerID;
            ReportAppointmentBP.getDataAreaDealerHO($scope.filter).then(function (response) {
            console.log('ayamtest',response.data)
            $scope.dataGroupDealerHO = response.data
            $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
            $scope.areaId($scope.dataGroupDealerHO[0])
            })
        })
        ReportAppointmentBP.getDataCabang($scope.filter).then(function(response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabang = response.data;
            $scope.DisabledCabang();
            $scope.filter.Cabang=response.data[0].cabangID;
        })
        ReportAppointmentBP.getDataAreaTAM().then(function(response) {
            console.log('Response AreaTAM:', response.data);
            $scope.dataAreaTAM = response.data;
        })
        ReportAppointmentBP.getDataAreaDealer().then(function(response) {
            console.log('Response AreaDealer:', response.data);
            $scope.dataAreaDealer = response.data;
        })
        ReportAppointmentBP.getDataAreaDealerHO($scope.filter).then(function(response) {
            console.log('Response AreaDealerHO:', response.data);
            $scope.dataAreaDealerHO = response.data;
            $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
            $scope.areaId($scope.dataAreaDealerHO[0])
        })
        ReportAppointmentBP.getDataSatellite().then(function(response) {
            console.log('Response satellite:', response.data);
            $scope.dataSatellite = response.data;
        })
    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
            $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };


        $scope.areaId = function(row) {
          // $scope.DataDealer = row
            $scope.DataDealer = Object.assign(row,{'kategori': $scope.filter.kategori})
            if($scope.userlogin != 'TAM'){
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
            }else{
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
            }
            console.log('row dealerid', row,$scope.DataDealer);
            ReportAppointmentBP.getDataCabangHONew($scope.DataDealer).then(function (response) {
                console.log('Response BECabang:', response.data);
                $scope.dataCabangHO = response.data;
                $scope.DisabledCabang();
                $scope.filter.CabangHO = null
                console.log('kambing',$scope.filter)
                $timeout(function() {
                    $scope.filter.CabangHO = response.data[0].cabangID;
                    console.log('kambing123',$scope.filter.CabangHO)
                }, 100);
            })
        };

        $scope.dealerCode = function(row){
            row = Object.assign(row,{'isGR':$scope.filter.isGR})
            $scope.filter.DealerId = row.dealerID
            ReportAppointmentBP.getDataAreaDealerHO(row).then(function (response) {
                console.log('ayam',response.data)
                console.log('rusa',$scope.dataGroupDealer)
                $scope.dataGroupDealer = response.data
                $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
                $scope.areaId($scope.dataGroupDealer[0])
                console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
            })
        }

        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
        $scope.getData = function() {
            if ($scope.role == "Branch") $scope.getSummaryCabang();
            else $scope.getSummaryHO();
            console.log('get data');
        }
         $scope.getSummaryCabang = function(){
        var $tanggal=$scope.filter.DateStart;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
        var dataFilter = {
            'kategori': "BP",
            'jenis': "summary",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'PIC': "1",
            'tamarea': $scope.filter.tamarea,
            'satellite': $scope.filter.satellite,
            'dealerarea': $scope.filter.dealerarea,};

          ReportAppointmentBP.getSummaryCabang(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);

            $scope.TotalUnitEntry = response.data[0].totalUnitEntry;
            $scope.TotalUnitEntry = Math.round($scope.TotalUnitEntry * 100) / 100
            $scope.CustomerBatal = response.data[0].customerCancel;
            $scope.CustomerBatal = Math.round($scope.CustomerBatal * 100) / 100
            $scope.CustomerAppointment = response.data[0].customerAppointment;
            $scope.CustomerAppointment = Math.round($scope.CustomerAppointment * 100) / 100
            $scope.CustomerAppointmentShow = response.data[0].customerAppointmentShow;
            $scope.CustomerAppointmentShow = Math.round($scope.CustomerAppointmentShow * 100) / 100
            $scope.CustomerReschedule = response.data[0].customerReschedule;
            $scope.CustomerReschedule = Math.round($scope.CustomerReschedule * 100) / 100
            $scope.CustomerAppointmentResch = (response.data[0].customerAppointment + response.data[0].customerReschedule)
            $scope.CustomerAppointmentResch = Math.round($scope.CustomerAppointmentResch * 100) / 100

            $scope.AppointmentRate = response.data[0].totalUnitEntry>0 ? ($scope.CustomerAppointmentShow / $scope.TotalUnitEntry)*100 : 0;
            $scope.AppointmentRate = Math.round($scope.AppointmentRate * 100) / 100
            $scope.NoShowRate = response.data[0].customerAppointment>0 ? ($scope.CustomerBatal / $scope.CustomerAppointment)*100 : 0;
            $scope.NoShowRate = Math.round($scope.NoShowRate * 100) / 100
            $scope.ShowRate = response.data[0].customerAppointment>0 ? ($scope.CustomerAppointmentShow / $scope.CustomerAppointment)*100 : 0;
            $scope.ShowRate = Math.round($scope.ShowRate * 100) / 100
            $scope.RescheduleRate = response.data[0].customerAppointment>0 ? ($scope.CustomerReschedule / ($scope.CustomerAppointment + $scope.CustomerReschedule))*100: 0;
            $scope.RescheduleRate = Math.round($scope.RescheduleRate * 100) / 100
          })
          console.log('result',$scope)
      }


      $scope.getSummaryHO = function(){
        var $tanggal=$scope.filter.DateStart;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
        var dataFilter = {
            'kategori': "BP",
            'jenis': "summary",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'satellite': $scope.filter.satellite,
            'dealerarea': $scope.filter.dealerarea,};

          ReportAppointmentBP.getSummaryHO(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;

            $scope.TotalUnitEntry = response.data[0].unitEntry;
            $scope.TotalUnitEntry = Math.round($scope.TotalUnitEntry * 100) / 100
            $scope.CustomerBatal = response.data[0].customerCancel;
            $scope.CustomerBatal = Math.round($scope.CustomerBatal * 100) / 100
            $scope.CustomerAppointment = response.data[0].customerAppointment;
            $scope.CustomerAppointment = Math.round($scope.CustomerAppointment * 100) / 100
            $scope.CustomerAppointmentShow = response.data[0].customerAppointmentShow;
            $scope.CustomerAppointmentShow = Math.round($scope.CustomerAppointmentShow * 100) / 100
            $scope.CustomerReschedule = response.data[0].customerReschedule;
            $scope.CustomerReschedule = Math.round($scope.CustomerReschedule * 100) / 100
            $scope.CustomerAppointmentResch = (response.data[0].customerAppointment + response.data[0].customerReschedule)
            $scope.CustomerAppointmentResch = Math.round($scope.CustomerAppointmentResch * 100) / 100

            $scope.AppointmentRate = response.data[0].unitEntry>0 ? (response.data[0].customerAppointmentShow / response.data[0].unitEntry)*100 : 0;
            $scope.AppointmentRate = Math.round($scope.AppointmentRate * 100) / 100
            $scope.NoShowRate = response.data[0].customerAppointment>0 ? (response.data[0].customerCancel / response.data[0].customerAppointment)*100 : 0;
            $scope.NoShowRate = Math.round($scope.NoShowRate * 100) / 100
            $scope.ShowRate = response.data[0].customerAppointment>0 ? (response.data[0].customerAppointmentShow / response.data[0].customerAppointment)*100 : 0;
            $scope.ShowRate = Math.round($scope.ShowRate * 100) / 100
            $scope.RescheduleRate = response.data[0].customerAppointment>0 ? (response.data[0].customerReschedule / ($scope.CustomerAppointment + $scope.CustomerReschedule))*100 : 0;
            $scope.RescheduleRate = Math.round($scope.RescheduleRate * 100) / 100
          })
      }

      $scope.getReport = function(){
        if ($scope.role == "Branch") $scope.getReportCabang();
        else $scope.getReportHO();
      }
      $scope.getReportCabang = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
          var dataReport = {
            'kategori': "BP",
            'jenis': "export",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'PIC': "1",
            'tamarea': $scope.filter.tamarea,
            'satellite': "43",
            'dealerarea': $scope.filter.dealerarea,};

        ReportAppointmentBP.getExportCabang(dataReport);
    }
    $scope.getReportHO = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
          var dataReport = {
            'kategori': "BP",
            'jenis': "export",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'satellite': $scope.filter.satellite,
            'dealerarea': $scope.filter.dealerarea,};

        ReportAppointmentBP.getExportHO(dataReport);
    }

    });
