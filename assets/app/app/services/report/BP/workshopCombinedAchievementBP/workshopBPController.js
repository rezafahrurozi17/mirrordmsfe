angular.module('app')
    .controller('WorkshopBPController', function($scope, $http, CurrentUser, WorkshopBPFactory) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        // $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;

    $scope.dataDealer = [];
    $scope.dataCabang = [];

    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    var dateFormat='MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
        
        // mode:"'month'",
        disableWeekend: 1
    };
$scope.filter = {Dealer:null, Cabang:null, kategori:"Body and Paint", DateStart:periode, Satellite:null, dealerarea:0,tamarea:0, pcgs:0,ocgs:0, opl:0,opb:0, labour:0,overhead:0, depreciation:0,totalsga:0 };
      WorkshopBPFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
      })
      WorkshopBPFactory.getDataCabang().then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
    $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
        $scope.getData = function(){
        }
    $scope.simpan = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
          
            var dataFilter = {                
                'cabang': $scope.filter.Cabang,
                'periode': $filtertanggal,
                'pcgs': $scope.filter.pcgs,
                'ocgs':$scope.filter.ocgs,
                'opl':$scope.filter.opl,
                'opb':$scope.filter.opb,
                'labour':$scope.filter.labour,
                'overhead':$scope.filter.overhead,
                'depreciation':$scope.filter.depreciation,
                'totalsga':$scope.filter.totalsga
                
            };

            WorkshopBPFactory.save(dataFilter).then(function(response) {
                console.log('Simpan:', response);
                // $scope.dataSummary = response.data;
                var hasil = response.data[0];
                if (response.data[0] !== undefined) {
                    $scope.filter.pcgs=hasil.pcgs;
                    $scope.filter.ocgs=hasil.ocgs;
                    $scope.filter.opl=hasil.opl;
                    $scope.filter.opb=hasil.opb;
                    $scope.filter.labour=hasil.labour;
                    $scope.filter.overhead=hasil.overhead;
                    $scope.filter.depreciation=hasil.depreciation;
                    $scope.filter.totalsga=hasil.totalsga;
                }
                else{
                    $scope.filter.pcgs=0;
                    $scope.filter.ocgs=0;
                    $scope.filter.opl=0;
                    $scope.filter.opb=0;
                    $scope.filter.labour=0;
                    $scope.filter.overhead=0;
                    $scope.filter.depreciation=0;
                    $scope.filter.totalsga=0;
                }

            })
        }
        $scope.tampilkan = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
          
            var dataFilter = {                
                'cabang': $scope.filter.Cabang,
                'periode': $filtertanggal,
                'pcgs': 0,
                'ocgs':0,
                'opl':0,
                'opb':0,
                'labour':0,
                'overhead':0,
                'depreciation':0,
                'totalsga':0
            };

            WorkshopBPFactory.save(dataFilter).then(function(response) {
                console.log('Tampilkan:', response.data);
                // $scope.dataSummary = response.data;
                var hasil = response.data[0];
                if (response.data[0] !== undefined) {
                    var tmpKey = Object.keys(hasil);
                    var angka = 0;
                    for(var i = 1; i < tmpKey.length; i++){
                        console.log("=====",hasil[tmpKey[i]])
                        angka += hasil[tmpKey[i]];
                    }
                    console.log('aaa', angka, tmpKey);
                    $scope.filter.pcgs=hasil.pcgs;
                    $scope.filter.ocgs=hasil.ocgs;
                    $scope.filter.opl=hasil.opl;
                    $scope.filter.opb=hasil.opb;
                    $scope.filter.labour=hasil.labour;
                    $scope.filter.overhead=hasil.overhead;
                    $scope.filter.depreciation=hasil.depreciation;
                    // $scope.filter.totalsga=hasil.totalsga;
                    $scope.filter.totalsga=angka;
                }
                else{
                    $scope.filter.pcgs=0;
                    $scope.filter.ocgs=0;
                    $scope.filter.opl=0;
                    $scope.filter.opb=0;
                    $scope.filter.labour=0;
                    $scope.filter.overhead=0;
                    $scope.filter.depreciation=0;
                    $scope.filter.totalsga=0;
                }
                
            })

        }

    $scope.getReport = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
          
          var dataReport = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periode': $filtertanggal,
            'kategori': $scope.filter.kategori};

        WorkshopBPFactory.getSummaryExport(dataReport);
    }
    
    
});
