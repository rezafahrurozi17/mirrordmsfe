angular.module('app')
  .factory('WorkshopBPFactory', function($http,CurrentUser,$q) {
    console.log('Service WorkshopBP Loaded...');
    var serverURL = ''; //http://localhost:8080'
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    return {
            getDataDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},

            getDataCabang: function() {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            return res;},

            getData: function() {
              var res=$http.get('/api/fw/Role');        
              console.log('data=>',res);
              //---------
              return $q.resolve(data);
            },

            save: function(data) {
              console.log('Data Filter:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_WorkshopCombinedAchievmentReport_BPCAB/'+ data.cabang +'/'+ data.periode +'/'+ data.pcgs  +'/'+ data.ocgs +'/'+ data.opl+'/'+ data.opb +'/'+ data.labour  +'/'+ data.overhead +'/'+ data.depreciation +'/'+ data.totalsga);
              return res;},
            getSummaryExport: function(data) {
              console.log('Data Report:', data);
              // window.open(serverURL + 'A_CabGRWorkshopCombinedAchievement?dealer='+ data.dealer +'&cabang='+ data.cabang +'&kategori='+ data.kategori +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir);
              window.open(serverURL + '/api/rpt/AS_WorkshopCombinedAchievmentReport_BPCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&kategori='+ data.kategori +'&datestart='+ data.periode );
              // var res = $http.get(serverURL + 'CCustomerCompleteness?area='+ data.area +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periode +'&jenis_pelanggan='+ data.jenispel);
              // return res;
            }
   
    }
  });
