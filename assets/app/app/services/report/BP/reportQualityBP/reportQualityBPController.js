angular.module('app')
    .controller('ReportQualityBPController', function($scope, $http, CurrentUser, ReportQualityBP, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
        });
        // var $item = angular.fromJson($stateParams.tab.item);
        // $scope.roleId = $item.RoleId;
        $scope.user = CurrentUser.user();
        $scope.roleId = $scope.user.roleId;

        $scope.formatDate = function(date) {
            var dateOut = new Date(date);
            return dateOut;
        };
        var dateFormat2='dd/MM/yyyy';
        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var periode=new Date();
        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat,

            // mode:"'month'",
        };
        // //----------------------------------
        // // Get Data
        // //----------------------------------
        $scope.dataDealer = [];
        $scope.dataCabang = [];
        $scope.dataAreaTAM = [];
        $scope.dataAreaDealer = [];
        $scope.dataSatellite = [];
        $scope.dataKategoriPerbaikan = [];
        $scope.filter = {Dealer:null, Cabang:null, kategori:"Body and Paint", DateStart:periode,DateEnd:periode, satellite:null, dealerarea:0,tamarea:0,JenisPembayaran:null,KategoriPerbaikan:null,Group:null };
        ReportQualityBP.getDataDealer().then(function(response) {
            console.log('Response BEDealer:', response.data);
            $scope.dataDealer = response.data;
            $scope.filter.Dealer=response.data[0].dealerCode;
        })
        ReportQualityBP.getDataCabang().then(function(response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabang = response.data;
            $scope.DisabledCabang();
            $scope.filter.Cabang=response.data[0].cabangID;
        })
        ReportQualityBP.getDataAreaTAM().then(function(response) {
            console.log('Response AreaTAM:', response.data);
            $scope.dataAreaTAM = response.data;
        })
        ReportQualityBP.getDataAreaDealer().then(function(response) {
            console.log('Response AreaDealer:', response.data);
            $scope.dataAreaDealer = response.data;
        })
        ReportQualityBP.getDataSatellite().then(function(response) {
            console.log('Response satellite:', response.data);
            $scope.dataSatellite = response.data;
        })
        ReportQualityBP.getGlobal({ id: 1012 }).then(function(response) {
            console.log('Response global:', response.data);
            $scope.dataGlobal = response.data;
        })
        // ReportQualityBP.getGroup({ id: 1012 }).then(function(response) {
        ReportQualityBP.getGroup().then(function(response) {
            console.log('Response Group:', response.data);
            $scope.dataGroup = response.data;
        })
        // ReportQualityBP.getKategoriPembayaran({ id: 1012 }).then(function(response) {
        //     console.log('Response KategoriPembayaran:', response.data);
        //     $scope.dataKategoriPembayaran = response.data;
        // })
        $scope.dataKategoriPembayaran = [{name:"ALL", value:2}, {name:"Insurance", value:0}, {name:"Cash", value:1}];
        console.log('pembayaran',$scope.dataKategoriPembayaran);

        ReportQualityBP.getKategoriPerbaikan({ id: 1012 }).then(function(response) {
            console.log('Response KategoriPerbaikan:', response.data);
            $scope.dataKategoriPerbaikan = response.data;
        })
        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
        $scope.getData = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataFilter = {
                // 'kategori': $scope.filter.kategori,
                'kategori': 'BP',
                'jenis': "summary",
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'kategoriPembayaran': $scope.filter.JenisPembayaran,
                'kategoriperbaikan': $scope.filter.KategoriPerbaikan,
                'group': $scope.filter.Group,
                'satelite' : $scope.filter.satellite
            };

            if (($scope.filter.Cabang == null || $scope.filter.DateStart == null || $scope.filter.DateEnd == null || $scope.filter.satellite == null || $scope.filter.KategoriPerbaikan == null || $scope.filter.Group == null || $scope.filter.JenisPembayaran == null) ||
                ($scope.filter.Cabang == undefined || $scope.filter.DateStart == undefined || $scope.filter.DateEnd == undefined || $scope.filter.satellite == undefined || $scope.filter.KategoriPerbaikan == undefined || $scope.filter.Group == undefined || $scope.filter.JenisPembayaran == undefined) || 
                ($scope.filter.DateStart == '' || $scope.filter.DateEnd == '')) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Isi data bertanda *",
                        // content: error.join('<br>'),
                        // number: error.length
                    });
            } else {
                ReportQualityBP.getSummary(dataFilter).then(function(response) {
                    console.log('Response BE Filter:', response.data);
                    // $scope.redoBody=0;
                    // $scope.redoPutty=0;
                    // $scope.redoSurfacer=0;
                    // $scope.redoPainting=0;
                    // $scope.redoPolishing=0;
                    // $scope.redoReassembly=0;
                    // $scope.ratioPointDefect=0;
                    // $scope.ratioReturn=0;
                    // $scope.Accuration=0;

                    // $scope.percenBody = 0;
                    // $scope.percenFinalInspection = 0;
                    // $scope.percenPainting = 0;
                    // $scope.percenPolishing = 0;
                    // $scope.percenPutty = 0;
                    // $scope.percenReasembly = 0;
                    // $scope.percenSurfacer = 0;
                    // $scope.reDoFInalInspection = 0;
                    // $scope.reDoPainting = 0;
                    // $scope.reDoPolishing = 0;
                    // $scope.reDoReassembly = 0;
                    // $scope.reDoSurfacer = 0;
                    // $scope.redoBody = 0;
                    // $scope.redoPutty = 0;
                    // $scope.totalBody = 0;
                    // $scope.totalFInalInspection = 0;
                    // $scope.totalPainting = 0;
                    // $scope.totalPolishing = 0;
                    // $scope.totalPutty = 0;
                    // $scope.totalReassembly = 0;
                    // $scope.totalSurfacer = 0;

                    if (response.data[0] !== undefined) {
                        // $scope.redoBody=response.data[0].reDoBody;
                        // $scope.redoBody = Math.round($scope.redoBody * 100) / 100
                        // $scope.redoPutty=response.data[0].reDoPutty;
                        // $scope.redoPutty = Math.round($scope.redoPutty * 100) / 100
                        // $scope.redoSurfacer=response.data[0].reDoSurfacer;
                        // $scope.redoSurfacer = Math.round($scope.redoSurfacer * 100) / 100
                        // $scope.redoPainting=response.data[0].reDoPainting;
                        // $scope.redoPainting = Math.round($scope.redoPainting * 100) / 100
                        // $scope.redoPolishing=response.data[0].reDoPolishing;
                        // $scope.redoPolishing = Math.round($scope.redoPolishing * 100) / 100
                        // $scope.redoReassembly=response.data[0].reDoReassembly;
                        // $scope.redoReassembly = Math.round($scope.redoReassembly * 100) / 100
                        // $scope.ratioPointDefect=response.data[0].paintDefectRatio;
                        // $scope.ratioPointDefect = Math.round($scope.ratioPointDefect * 100) / 100
                        // $scope.ratioReturn=response.data[0].returnRatioJob;
                        // $scope.ratioReturn = Math.round($scope.ratioReturn * 100) / 100
                        // $scope.Accuration=response.data[0].accuration;
                        // $scope.Accuration = Math.round($scope.Accuration * 100) / 100

                        $scope.percenBody = response.data[0].percenBody.toFixed(2);
                        $scope.percenFinalInspection = response.data[0].percenFinalInspection.toFixed(2);
                        $scope.percenPainting = response.data[0].percenPainting.toFixed(2);
                        $scope.percenPolishing = response.data[0].percenPolishing.toFixed(2);
                        $scope.percenPutty = response.data[0].percenPutty.toFixed(2);
                        $scope.percenReasembly = response.data[0].percenReasembly.toFixed(2);
                        $scope.percenSurfacer = response.data[0].percenSurfacer.toFixed(2);
                        $scope.reDoFInalInspection = response.data[0].reDoFInalInspection;
                        $scope.reDoPainting = response.data[0].reDoPainting;
                        $scope.reDoPolishing = response.data[0].reDoPolishing;
                        $scope.reDoReassembly = response.data[0].reDoReassembly;
                        $scope.reDoSurfacer = response.data[0].reDoSurfacer;
                        $scope.redoBody = response.data[0].redoBody;
                        $scope.redoPutty = response.data[0].redoPutty;
                        $scope.totalBody = response.data[0].totalBody;
                        $scope.totalFInalInspection = response.data[0].totalFInalInspection;
                        $scope.totalPainting = response.data[0].totalPainting;
                        $scope.totalPolishing = response.data[0].totalPolishing;
                        $scope.totalPutty = response.data[0].totalPutty;
                        $scope.totalReassembly = response.data[0].totalReassembly;
                        $scope.totalSurfacer = response.data[0].totalSurfacer;
                    } else {
                        $scope.percenBody = 0;
                        $scope.percenFinalInspection = 0;
                        $scope.percenPainting = 0;
                        $scope.percenPolishing = 0;
                        $scope.percenPutty = 0;
                        $scope.percenReasembly = 0;
                        $scope.percenSurfacer = 0;
                        $scope.reDoFInalInspection = 0;
                        $scope.reDoPainting = 0;
                        $scope.reDoPolishing = 0;
                        $scope.reDoReassembly = 0;
                        $scope.reDoSurfacer = 0;
                        $scope.redoBody = 0;
                        $scope.redoPutty = 0;
                        $scope.totalBody = 0;
                        $scope.totalFInalInspection = 0;
                        $scope.totalPainting = 0;
                        $scope.totalPolishing = 0;
                        $scope.totalPutty = 0;
                        $scope.totalReassembly = 0;
                        $scope.totalSurfacer = 0;
                    }
                })

            } 
             
            
        }


        $scope.getReport = function() {
            if (
                // $scope.redoBody== null ||
                // $scope.redoPutty== null ||
                // $scope.redoSurfacer== null ||
                // $scope.redoPainting== null ||
                // $scope.redoPolishing== null ||
                // $scope.redoReassembly== null ||
                // $scope.ratioPointDefect== null ||
                // $scope.ratioReturn== null ||
                // $scope.Accuration== null || 
                
                // $scope.redoBody== undefined ||
                // $scope.redoPutty== undefined ||
                // $scope.redoSurfacer== undefined ||
                // $scope.redoPainting== undefined ||
                // $scope.redoPolishing== undefined ||
                // $scope.redoReassembly== undefined ||
                // $scope.ratioPointDefect== undefined ||
                // $scope.ratioReturn== undefined ||
                // $scope.Accuration== undefined 

                $scope.percenBody == null ||
                $scope.percenFinalInspection == null ||
                $scope.percenPainting == null ||
                $scope.percenPolishing == null ||
                $scope.percenPutty == null ||
                $scope.percenReasembly == null ||
                $scope.percenSurfacer == null ||
                $scope.reDoFInalInspection == null ||
                $scope.reDoPainting == null ||
                $scope.reDoPolishing == null ||
                $scope.reDoReassembly == null ||
                $scope.reDoSurfacer == null ||
                $scope.redoBody == null ||
                $scope.redoPutty == null ||
                $scope.totalBody == null ||
                $scope.totalFInalInspection == null ||
                $scope.totalPainting == null ||
                $scope.totalPolishing == null ||
                $scope.totalPutty == null ||
                $scope.totalReassembly == null ||
                $scope.totalSurfacer == null ||

                $scope.percenBody == undefined ||
                $scope.percenFinalInspection == undefined ||
                $scope.percenPainting == undefined ||
                $scope.percenPolishing == undefined ||
                $scope.percenPutty == undefined ||
                $scope.percenReasembly == undefined ||
                $scope.percenSurfacer == undefined ||
                $scope.reDoFInalInspection == undefined ||
                $scope.reDoPainting == undefined ||
                $scope.reDoPolishing == undefined ||
                $scope.reDoReassembly == undefined ||
                $scope.reDoSurfacer == undefined ||
                $scope.redoBody == undefined ||
                $scope.redoPutty == undefined ||
                $scope.totalBody == undefined ||
                $scope.totalFInalInspection == undefined ||
                $scope.totalPainting == undefined ||
                $scope.totalPolishing == undefined ||
                $scope.totalPutty == undefined ||
                $scope.totalReassembly == undefined ||
                $scope.totalSurfacer == undefined
            ){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Cari Data Terlebih Dahulu",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            } else {
                var $tanggal = $scope.filter.DateStart;
                var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
                var $tanggalakhir = $scope.filter.DateEnd;
                var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
                var dataReport = {
                    // 'kategori': $scope.filter.kategori,
                    'kategori': 'BP',
                    'jenis': "export",
                    'dealer': $scope.filter.Dealer,
                    'cabang': $scope.filter.Cabang,
                    'periodeakhir': $filtertanggalakhir,
                    'periodeawal': $filtertanggal,
                    'kategoriPembayaran': $scope.filter.JenisPembayaran,
                    'kategoriperbaikan': $scope.filter.KategoriPerbaikan,
                    'group': $scope.filter.Group,
                    'satelite' : $scope.filter.satellite
                };

            ReportQualityBP.getExport(dataReport);
            }


            
        }

    $scope.dateOptions2S = {
        startingDay: 1,
        format: dateFormat2,
        // mode:"'month'",
    };
    $scope.dateOptions2E = {
        startingDay: 1,
        format: dateFormat2,
        // mode:"'month'",
    };


    //===============validasi date start and date end===========
    var today = new Date();
    $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(),today.getDate());  
    $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(),today.getDate()); 
    $scope.minDateE = new Date(today.getFullYear(),today.getMonth(),today.getDate());                         


    $scope.changeStartDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.DateStart);
        var dayTwo = new Date($scope.filter.DateEnd);
         
        $scope.dateOptions2E.minDate = $scope.filter.DateStart;

        if (tgl == null || tgl == undefined){
            $scope.filter.DateEnd = null;
        } else {
            if ($scope.filter.DateStart < $scope.filter.DateEnd){
                if((dayTwo.getMonth() - dayOne.getMonth()) > 2 || dayOne.getFullYear() != dayTwo.getFullYear()){
                $scope.filter.DateEnd = $scope.filter.DateStart; 
                $scope.dateOptions2E.maxDate = new Date(dayOne.getFullYear(),dayOne.getMonth(),dayOne.getDate());
                if ($scope.dateOptions2E.maxDate > today) {
                    $scope.dateOptions2E.maxDate = new Date(today.getFullYear(),today.getMonth(),today.getDate());                    
                }                                   
                }

            } else {
                if (dayOne > today){
                    $scope.filter.DateStart = today;
                    $scope.filter.DateEnd = $scope.filter.DateStart;                    
                } else {
                $scope.filter.DateEnd = $scope.filter.DateStart;
                $scope.dateOptions2E.maxDate = new Date(dayOne.getFullYear(),dayOne.getMonth(),dayOne.getDate());
                if ($scope.dateOptions2E.maxDate > today) {
                    $scope.dateOptions2E.maxDate = new Date(today.getFullYear(),today.getMonth(),today.getDate());                    
                }    
            }                                                
            }
        }
    }

    $scope.changeEndDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.DateStart);
        var dayTwo = new Date($scope.filter.DateEnd);

        if (dayTwo > today){
            $scope.dateOptions2E.minDate = $scope.filter.DateStart;
            $scope.filter.DateEnd = today;
        }
    }

    //=============== End of validasi date start and date end===========

        // $scope.getGroup = [
        // {GroupId:1,GroupName:"Group001"},
        // {GroupId:2,GroupName:"Group002"}
        // ];

        //
        // $scope.getSatellite = [
        // {SatelliteId:1,SatelliteName:"Satellite001"},
        // {SatelliteId:2,SatelliteName:"Satellite002"}
        // ];

        // $scope.getKategoriPerbaikan = [
        // {KategoriPerbaikanId:1,KategoriPerbaikanName:"KategoriPerbaikan001"},
        // {KategoriPerbaikanId:2,KategoriPerbaikanName:"KategoriPerbaikan002"}
        // ];

        // $scope.getJenisPembayaran = [
        // {JenisPembayaranId:1,JenisPembayaranName:"JenisPembayaran001"},
        // {JenisPembayaranId:2,JenisPembayaranName:"JenisPembayaran002"}
        // ];

    });
