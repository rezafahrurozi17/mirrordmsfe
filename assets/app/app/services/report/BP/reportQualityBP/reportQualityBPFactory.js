angular.module('app')
  .factory('ReportQualityBP', function($http, CurrentUser,$q) {
     var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = '';//http://localhost:8080';
    return {
            getDataDealer: function() {
              var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
              return res;},
            getDataCabang: function() {
              var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
              return res;},
            getDataSatellite: function() {
            var res=$http.get(serverURL + '/api/rpt/Satellite/');
            return res;},            
            getDataAreaTAM: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaTAM/');
            return res;},
            getDataAreaDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaDealer/');
            return res;},
            getGlobal: function(data) {
            var res=$http.get(serverURL + '/api/rpt/getGlobal/'+ data.id);
            return res;},
            getGroup: function(data) {
            var res=$http.get(serverURL + '/api/rpt/getGrupBP/');
            return res;},  
            getKategoriPembayaran: function(data) {
            var res=$http.get(serverURL + '/api/rpt/getPaymentMethod/');
            return res;},  
            getKategoriPerbaikan: function(data) {
            var res=$http.get(serverURL + '/api/rpt/KategoriPerbaikan/');
            return res;},     
            getSummary: function(data) {
              console.log('Data Report:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_Quality_BPCAB/'+ data.dealer +'/'+ data.cabang  +'/'+ data.satelite +'/'+ data.kategoriperbaikan +'/'+ data.kategori +'/'+  data.kategoriPembayaran +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.group);
                return res;
            },
            getExport: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_Quality_BPCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&kategoriPerbaikan='+ data.kategoriperbaikan +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir +'&kategoriWorkshop='+ data.kategori +'&kategoriPembayaran='+ data.kategoriPembayaran +'&group='+ data.group +'&jenis=export&satelite=' + data.satelite);
                //window.open(serverURL + '/api/rpt/AS_Quality_BPCAB?dealer=' + data.dealer + '&outletcode=' + data.cabang + '&satelite=' + data.satelite + '&kategoriPerbaikan=' + data.kategoriperbaikan + '&kat_workshop=' + data.kategori + '&kategoriPembayaran=' + data.kategoriPembayaran + '&datestart=' + data.periodeawal + '&dateend=' + data.periodeakhir + '&group=' + data.group + '&jenis=export');
                //window.open(serverURL + '/api/rpt/AS_Quality_BPCAB?kategori_perbaikan=' + data.kategoriperbaikan + '&dealer=' + data.dealer + '&cabang=' + data.cabang + '&datestart=' + data.periodeawal + '&dateend=' + data.periodeakhir + '&group=' + data.group + '&kategori_workshop=' + data.kategori + '&jenis=export' + '&kategoriPembayaran=' + data.kategoriPembayaran + '&satelite=' + data.satelite );
                 }
        }
  });
