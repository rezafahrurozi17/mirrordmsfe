angular.module('app')
  .factory('ReportBPPengMaterialBahanFactory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = '';//http://localhost:8080';
    console.log("service yang diload")
    return {
      getDataDealer: function() {
            console.log(serverURL);
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},
      getDataCabang: function() {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            return res;},
      getKategoriPerbaikan: function(data) {
            var res=$http.get(serverURL + '/api/rpt/KategoriPerbaikan/');
            return res;},     
      getData: function() {
        var res=$http.get('/api/fw/Role');        
        console.log('data=>',res);
        //---------
        return $q.resolve(data);
      },
      // getSummary: function(data) {
      //         console.log('Data Filter:', data);
      //         var res = $http.get(serverURL + 'ACabBPQuality/'+ data.dealer +'/'+ data.cabang +'/'+ data.satellite +'/'+ data.KategoriPerbaikan +'/'+ data.kategori +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/'+ data.group);
      //         return res;},
      getExport: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_PenggunaanMaterialBahan_BPCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periode_awal +'&dateend='+ data.periode_akhir +'&kategori='+ data.kategori +'&area='+ data.area);
            }
    }
  });
