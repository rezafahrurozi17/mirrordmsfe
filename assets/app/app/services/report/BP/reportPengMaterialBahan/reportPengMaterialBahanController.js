angular.module('app')
    .controller('ReportPengMaterialBahanController', function($scope, $http, CurrentUser, ReportBPPengMaterialBahanFactory  ) {
        //----------------------------------
        // Start-Up

        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
        });
        $scope.user = CurrentUser.user();
        $scope.roleId = $scope.user.RoleId;
        $scope.formatDate = function(date) {
            var dateOut = new Date(date);
            return dateOut;
        };
        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var periode=new Date();
        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat,
            // mode:"'month'",
        };
        $scope.dataDealer = [];
        $scope.dataCabang = [];
        $scope.dataKategoriPerbaikan = [];
        $scope.filter = {Dealer:null, Cabang:null, kategori:"Body and Paint", DateStart:periode,DateEnd:periode, KategoriPerbaikan:null, area:0 };
        ReportBPPengMaterialBahanFactory.getDataDealer().then(function(response) {
            console.log('Response BEDealer:', response.data);
            $scope.dataDealer = response.data;
            $scope.filter.Dealer=response.data[0].dealerCode;
        })
        ReportBPPengMaterialBahanFactory.getDataCabang().then(function(response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabang = response.data;
            $scope.DisabledCabang();
            $scope.filter.Cabang=response.data[0].cabangID;
        })
        ReportBPPengMaterialBahanFactory.getKategoriPerbaikan().then(function(response) {
            console.log('Response AreaSatellite:', response.data);
            $scope.dataKategoriPerbaikan = response.data;
        })
        $scope.getData = function() {
        }
        // $scope.getSummary = function() {
        //     var $tanggal1=$scope.filter.DateStart;
        //     var $tanggal2=$scope.filter.DateEnd;
        //     var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        //     var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        //     var dataFilter = {
        //         'dealer': $scope.filter.Dealer,
        //         'cabang': $scope.filter.Cabang,
        //         'kategori': $scope.filter.kategori,
        //         'KategoriPerbaikan': $scope.filter.KategoriPerbaikan,
        //         'satellite': $scope.filter.satellite, 
        //         'periode_awal': $filtertanggal1,
        //         'periode_akhir': $filtertanggal2,            
        //         'group': $scope.filter.Group};

        //       ReportQualityBP.getSummary(dataFilter).then(function (response) {
        //         console.log('Response BE Filter:', response.data);
        //         // $scope.dataSummary = response.data;
        //         var hasil=response.data[0];
        //         $scope.redoBody = 1;
        //         $scope.redoPutty = 2;
        //         $scope.redoSurfacer = 3;
        //         $scope.redoPainting = 4;
        //         $scope.redoPolishing = 5;
        //         $scope.redoReassembly = 6;
        //         $scope.ratioPointDefect = 7;
        //         $scope.ratioReturn = 8;
        //         $scope.Accuration = 9;
        //       })
        //   }
        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
        $scope.getReport = function() {
                var $tanggal1 = $scope.filter.DateStart;
                var $tanggal2 = $scope.filter.DateEnd;
                var $filtertanggal1 = $tanggal1.getFullYear() + '/' + ($tanggal1.getMonth() + 1) + '/' + $tanggal1.getDate();
                var $filtertanggal2 = $tanggal2.getFullYear() + '/' + ($tanggal2.getMonth() + 1) + '/' + $tanggal2.getDate();
                var dataFilter = {
                    'dealer': $scope.filter.Dealer,
                    'cabang': $scope.filter.Cabang,
                    'kategori': $scope.filter.KategoriPerbaikan,
                    'periode_awal': $filtertanggal1,
                    'periode_akhir': $filtertanggal2,
                    'area': $scope.filter.area
                };

                console.log(ReportBPPengMaterialBahanFactory);

                ReportBPPengMaterialBahanFactory.getExport(dataFilter);
            }
            // //----------------------------------
            // // Initialization
            // //----------------------------------
            // $scope.user = CurrentUser.user();
            // $scope.loading=false;
            // $scope.formApi = {};
            // $scope.backToMain = true;
            // //----------------------------------
            // // Get Data
            // //----------------------------------
            // var gridData = [];
            
        //  $scope.getDealer = [
        // {DealerId:1,DealerName:"Dealer001"},
        // {DealerId:2,DealerName:"Dealer002"}
        // ];

        // $scope.getCabang = [
        // {cabangID:1,CabangName:"Cabang001"},
        // {cabangID:2,CabangName:"Cabang002"}
        // ];

        //  $scope.dataPerbaikan = [
        // {perbaikan:"All"},{perbaikan:"TPS"},{perbaikan:"Light"},{perbaikan:"Medium"},{perbaikan:"Heavy"}
        // ];

    });