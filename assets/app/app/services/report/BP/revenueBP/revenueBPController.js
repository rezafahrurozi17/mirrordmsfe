angular.module('app')
    .controller('RevenueBPController', function($scope, $http,$stateParams, CurrentUser , RevenueBPFactory, bsNotify,$timeout ) {
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
        });
        $scope.user = CurrentUser.user();
        $scope.roleId = $scope.user.RoleId;
        $scope.role=$stateParams.role;
        console.log('role',$scope.role);
        $scope.formatDate = function(date) {
            var dateOut = new Date(date);
            return dateOut;
        };
        var dateFormat = 'MM/yyyy';
        var dateFormat2 = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var periode=new Date();
        $scope.dateOptionsS = {
            startingDay: 1,
            format: dateFormat2,

            // mode:"'month'",
        };
        $scope.dateOptionsE = {
            startingDay: 1,
            format: dateFormat2,

            // mode:"'month'",
        };
        $scope.dateOptions2 = {
            startingDay: 1,
            format: dateFormat2,

            // mode:"'month'",
        };


        //===============validasi date start and date end===========
        var today = new Date();
        $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(), today.getDate());
        $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());
        $scope.minDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());


        $scope.changeStartDate = function(tgl){
            var today = new Date();
            var dayOne = new Date($scope.filter.DateStart);
            var dayTwo = new Date($scope.filter.DateEnd);

            $scope.dateOptionsE.minDate = new Date(dayOne.getFullYear(),dayOne.getMonth(), dayOne.getDate());
            if (tgl == null || tgl == undefined){
                $scope.filter.DateEnd = null;
            } else {
                if ($scope.filter.DateStart < $scope.filter.DateEnd){

                } else {
                    if (dayOne > today){
                        $scope.filter.DateStart = today;
                        $scope.filter.DateEnd = $scope.filter.DateStart;
                    } else {
                        $scope.filter.DateEnd = $scope.filter.DateStart;
                    }
                }
            }
        }

        $scope.changeEndDate = function(tgl){
            var today = new Date();
            var dayOne = new Date($scope.filter.DateStart);
            var dayTwo = new Date($scope.filter.DateEnd);

            if (dayTwo > today){
                $scope.dateOptionsE.minDate = $scope.filter.DateStart;
                $scope.filter.DateEnd = today;
            }
        }

        //=============== End of validasi date start and date end===========











        $scope.dataDealer = [];
        $scope.dataCabang = [];
        $scope.dataAreaTAM = [];
        $scope.dataAreaDealer = [];
        $scope.getSatellite = [];
        $scope.filter = {Dealer:null,DealerId:null, Cabang:null, kategori:"Body and Paint", DateStart:periode,DateEnd:periode, Satellite:null, DealerArea:0,tamArea:0,isGR:0 };
        RevenueBPFactory.getDataDealer().then(function(response) {
            console.log('Response BEDealer:', response.data);
            $scope.dataDealer = response.data;
            $scope.filter.Dealer=response.data[0].dealerCode;
            $scope.filter.DealerId=response.data[0].dealerID;
            RevenueBPFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
                console.log('ayamtest',response.data)
                $scope.dataGroupDealerHO = response.data
                $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
                $scope.areaId($scope.dataGroupDealerHO[0])
            })
        })
        RevenueBPFactory.getDataCabang($scope.filter).then(function(response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabang = response.data;
            $scope.DisabledCabang();
            $scope.filter.Cabang=response.data[0].cabangID;
        })
        RevenueBPFactory.getDataAreaTAM().then(function(response) {
            console.log('Response AreaTAM:', response.data);
            $scope.dataAreaTAM = response.data;
        })
        RevenueBPFactory.getDataAreaDealer().then(function(response) {
            console.log('Response AreaDealer:', response.data);
            $scope.dataAreaDealer = response.data;
        })
        RevenueBPFactory.getDataAreaDealerHO($scope.filter).then(function(response) {
            console.log('Response AreaDealerHO:', response.data);
            $scope.dataAreaDealerHO = response.data;
            $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
            $scope.areaId($scope.dataAreaDealerHO[0])
        })
        RevenueBPFactory.getDataSatellite().then(function(response) {
            console.log('Response BESatellite:', response.data);
            $scope.dataSatellite = response.data;
        })
    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
            $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };

        
        $scope.areaId = function(row) {
            // $scope.DataDealer = row
            $scope.DataDealer = Object.assign(row,{'kategori': $scope.filter.kategori})
            if($scope.userlogin != 'TAM'){
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
            }else{
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
            }
            console.log('row dealerid', row,$scope.DataDealer);
            RevenueBPFactory.getDataCabangHONew($scope.DataDealer).then(function (response) {
                console.log('Response BECabang:', response.data);
                $scope.dataCabangHO = response.data;
                $scope.DisabledCabang();
                $scope.filter.CabangHO = null
                console.log('kambing',$scope.filter)
                $timeout(function() {
                    $scope.filter.CabangHO = response.data[0].cabangID;
                    console.log('kambing123',$scope.filter.CabangHO)
                }, 100);
            })
        };

        $scope.dealerCode = function(row){
            row = Object.assign(row,{'isGR':$scope.filter.isGR})
            $scope.filter.DealerId = row.dealerID
            RevenueBPFactory.getDataAreaDealerHO(row).then(function (response) {
                console.log('ayam',response.data)
                console.log('rusa',$scope.dataGroupDealer)
                $scope.dataGroupDealer = response.data
                $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
                $scope.areaId($scope.dataGroupDealer[0])
                console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
            })
        }

        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };

    function format1(n, currency) {
        return currency + " " + n.toFixed(0).replace(/./g, function(c, i, a) {
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
        });
    }

    function format2(n, currency) {
        return currency + " " + n.toFixed(2).replace(/./g, function(c, i, a) {
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
        });
    }
        $scope.getData = function() {
            if ($scope.role == "Branch") $scope.getSummaryBranch();
            else $scope.getSummaryHO();
            console.log('get data');
        }
        $scope.getSummaryBranch = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'kategori': $scope.filter.kategori
            };

            RevenueBPFactory.getSummaryCabang(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);
                var hasil = response.data[0];
                $scope.CPUSRevenueAvg=0;
                $scope.CPUSEMRevenueAvg=0;
                $scope.CPUSSBERevenueAvg=0;
                $scope.CPUSGRRevenueAvg=0;
                $scope.CPUSRegRevenueAvg=0;
                $scope.CPUSTPSRevenueAvg=0;
                $scope.NoCPUSRevenueAvg=0;
                if (response.data[0] != undefined) {
                    $scope.CPUSTotalUnit = hasil.cpusUnit;
                    $scope.CPUSTotalUnit = Math.round($scope.CPUSTotalUnit * 100) / 100
                    $scope.CPUSJasa = hasil.cpusJasa / 1000;
                    $scope.CPUSJasa = Math.round($scope.CPUSJasa * 100) / 100
                    $scope.CPUSJasa = format1($scope.CPUSJasa,'');
                    $scope.CPUSPart = hasil.cpusParts / 1000;
                    $scope.CPUSPart = Math.round($scope.CPUSPart * 100) / 100
                    $scope.CPUSPart = format1($scope.CPUSPart,'');
                    $scope.CPUSBahan = hasil.cpusBahan / 1000;
                    $scope.CPUSBahan = Math.round($scope.CPUSBahan * 100) / 100
                    $scope.CPUSBahan = format1($scope.CPUSBahan,'');
                    $scope.CPUSOPL = hasil.cpusopl / 1000;
                    $scope.CPUSOPL = Math.round($scope.CPUSOPL * 100) / 100
                    $scope.CPUSOPL = format1($scope.CPUSOPL,'');
                    $scope.CPUSOPB = hasil.cpusopb / 1000;
                    $scope.CPUSOPB = Math.round($scope.CPUSOPB * 100) / 100
                    $scope.CPUSOPB = format1($scope.CPUSOPB,'');
                    $scope.CPUSRevenue = hasil.cpusRevenue / 1000;
                    $scope.CPUSRevenue = Math.round($scope.CPUSRevenue * 100) / 100
                    $scope.CPUSRevenue = format1($scope.CPUSRevenue,'');
                    if (hasil.cpusUnit>0) $scope.CPUSRevenueAvg = (hasil.cpusRevenue / hasil.cpusUnit)  / 1000;
                    $scope.CPUSRevenueAvg = Math.round($scope.CPUSRevenueAvg * 100) / 100
                    $scope.CPUSRevenueAvg = format2($scope.CPUSRevenueAvg,'');
                    $scope.CPUSRegUnit = hasil.cpusRegUnit;
                    $scope.CPUSRegUnit = Math.round($scope.CPUSRegUnit * 100) / 100
                    $scope.CPUSRegRevenue = hasil.cpusRegRevenue / 1000;
                    $scope.CPUSRegRevenue = Math.round($scope.CPUSRegRevenue * 100) / 100
                    $scope.CPUSRegRevenue = format1($scope.CPUSRegRevenue,'');
                    if (hasil.cpusRegUnit>0) $scope.CPUSRegRevenueAvg = (hasil.cpusRegRevenue / hasil.cpusRegUnit) / 1000;
                    $scope.CPUSRegRevenueAvg = Math.round($scope.CPUSRegRevenueAvg * 100) / 100
                    $scope.CPUSRegRevenueAvg = format2($scope.CPUSRegRevenueAvg,'');
                    $scope.CPUSTPSUnit = hasil.cpustpsUnit;
                    $scope.CPUSTPSUnit = Math.round($scope.CPUSTPSUnit * 100) / 100
                    $scope.CPUSTPSRevenue = hasil.cpustpsRevenue / 1000;
                    $scope.CPUSTPSRevenue = Math.round($scope.CPUSTPSRevenue * 100) / 100
                    $scope.CPUSTPSRevenue = format1($scope.CPUSTPSRevenue,'');
                    if (hasil.cpustpsUnit>0) $scope.CPUSTPSRevenueAvg = (hasil.cpustpsRevenue / hasil.cpustpsUnit) / 1000;
                    $scope.CPUSTPSRevenueAvg = Math.round($scope.CPUSTPSRevenueAvg * 100) / 100
                    $scope.CPUSTPSRevenueAvg = format2($scope.CPUSTPSRevenueAvg,'');

                    $scope.NoCPUSUnit=hasil.nonCPUSUnit;
                    $scope.NoCPUSUnit = Math.round($scope.NoCPUSUnit * 100) / 100
                    $scope.NoCPUSPart = hasil.nonCPUSParts / 1000;
                    $scope.NoCPUSPart = Math.round($scope.NoCPUSPart * 100) / 100
                    $scope.NoCPUSPart = format1($scope.NoCPUSPart,'');
                    $scope.NoCPUSOPB = hasil.nonCPUSOPB / 1000;
                    $scope.NoCPUSOPB = Math.round($scope.NoCPUSOPB * 100) / 100
                    $scope.NoCPUSOPB = format1($scope.NoCPUSOPB,'');
                    $scope.NoCPUSRevenue = hasil.nonCPUSRevenue / 1000;
                    $scope.NoCPUSRevenue = Math.round($scope.NoCPUSRevenue * 100) / 100
                    $scope.NoCPUSRevenue = format1($scope.NoCPUSRevenue,'');
                    $scope.NoCPUSJasa = hasil.nonCPUSJasa / 1000;
                    $scope.NoCPUSJasa = Math.round($scope.NoCPUSJasa * 100) / 100
                    $scope.NoCPUSJasa = format1($scope.NoCPUSJasa,'');
                    $scope.NoCPUSBahan = hasil.nonCPUSBahan / 1000;
                    $scope.NoCPUSBahan = Math.round($scope.NoCPUSBahan * 100) / 100
                    $scope.NoCPUSBahan = format1($scope.NoCPUSBahan,'');
                    $scope.NoCPUSOPL = hasil.nonCPUSOPL / 1000;
                    $scope.NoCPUSOPL = Math.round($scope.NoCPUSOPL * 100) / 100
                    $scope.NoCPUSOPL = format1($scope.NoCPUSOPL,'');
                    if (hasil.nonCPUSUnit>0) $scope.NoCPUSRevenueAvg=(hasil.nonCPUSRevenue/hasil.nonCPUSUnit) / 1000;
                    $scope.NoCPUSRevenueAvg = Math.round($scope.NoCPUSRevenueAvg * 100) / 100
                    $scope.NoCPUSRevenueAvg = format2($scope.NoCPUSRevenueAvg,'');
                }
                else
                {
                    $scope.CPUSTotalUnit = 0;
                    $scope.CPUSJasa = 0;
                    $scope.CPUSPart = 0;
                    $scope.CPUSBahan = 0;
                    $scope.CPUSOPL = 0;
                    $scope.CPUSOPB = 0;
                    $scope.CPUSRevenue = 0;
                    $scope.CPUSRevenueAvg = 0;
                    $scope.CPUSRegUnit = 0;
                    $scope.CPUSRegRevenue = 0;
                    $scope.CPUSRegRevenueAvg = 0;
                    $scope.CPUSTPSUnit = 0;
                    $scope.CPUSTPSRevenue = 0;
                    $scope.CPUSTPSRevenueAvg = 0;
                    // $scope.NoCPUSUnit=hasil.;
                    $scope.NoCPUSPart = 0;
                    $scope.NoCPUSOPB = 0;
                    $scope.NoCPUSRevenue = 0;
                    $scope.NoCPUSJasa = 0;
                    $scope.NoCPUSBahan = 0;
                    $scope.NoCPUSOPL = 0;
                    // $scope.NoCPUSRevenueAvg=hasil.nonCPUSRevenue/hasil.nonCPUSUnit;
                }
            })
        }
        $scope.getSummaryHO = function() {
            if(($scope.filter.DealerArea == undefined || $scope.filter.Cabang == undefined || $scope.filter.Satellite == undefined || $scope.filter.DateStart == undefined || $scope.filter.DateEnd == undefined)
                ||
               ($scope.filter.DealerArea == null || $scope.filter.Cabang == null || $scope.filter.Satellite == null || $scope.filter.DateStart == null || $scope.filter.DateEnd == null)){
                bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "error",
                        content: "Isi Semua Field Bertanda *"
                      });
               } else {
                var $tanggal = $scope.filter.DateStart;
                var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();
                var $tanggalakhir = $scope.filter.DateEnd;
                var $filtertanggalakhir = $tanggalakhir.getFullYear() + '-' + ($tanggalakhir.getMonth() + 1) + '-' + $tanggalakhir.getDate();
                var dataFilter = {
                    'kategori': $scope.filter.kategori,
                    'dealer': $scope.filter.Dealer,
                    'cabang': $scope.filter.CabangHO,
                    'periodeakhir': $filtertanggalakhir,
                    'periodeawal': $filtertanggal,
                    'tamarea': $scope.filter.tamArea,
                    'satellite': $scope.filter.Satellite,
                    'dealerarea': $scope.filter.DealerArea
                };

                console.log("cek data aja", dataFilter);
                // if (((dataFilter.satellite == null) || (dataFilter.satellite == undefined) || (dataFilter.satellite == ""))
                //     || ((dataFilter.dealer == null) || (dataFilter.dealer == undefined) || (dataFilter.dealer == ""))
                //     || ((dataFilter.cabang == null) || (dataFilter.cabang == undefined) || (dataFilter.cabang == ""))
                //     || ((dataFilter.dealerarea == null) || (dataFilter.dealerarea == undefined) || (dataFilter.dealerarea == ""))
                //     || ((dataFilter.periodeawal == null) || (dataFilter.periodeawal == undefined) || (dataFilter.periodeawal == ""))){
                //     // || ((dataFilter.periodeakhir == null) || (dataFilter.periodeakhir == undefined) || (dataFilter.periodeakhir == ""))){
                //   bsNotify.show({
                //     size: 'big',
                //     type: 'danger',
                //     title: "error",
                //     content: "Isi semua Field"
                //   });
                // } else {

                    RevenueBPFactory.getSummaryHO(dataFilter).then(function(response) {
                        console.log('Response BE Filter:', response.data);
                        // $scope.dataSummary = response.data;
                            var hasil = response.data[0];
                        $scope.CPUSRevenueAvg=0;
                        $scope.CPUSEMRevenueAvg=0;
                        $scope.CPUSSBERevenueAvg=0;
                        $scope.CPUSGRRevenueAvg=0;
                        $scope.NoCPUSRevenueAvg=0;
                        $scope.CPUSRegRevenueAvg=0;
                        $scope.CPUSTPSRevenueAvg=0
                        if (response.data[0] !== undefined) {
                            $scope.CPUSTotalUnit = hasil.cpusUnit;
                            $scope.CPUSTotalUnit = Math.round($scope.CPUSTotalUnit * 100) / 100
                            $scope.CPUSJasa = hasil.cpusJasa / 1000;
                            $scope.CPUSJasa = Math.round($scope.CPUSJasa * 100) / 100
                            $scope.CPUSJasa = format1($scope.CPUSJasa,'');
                            $scope.CPUSPart = hasil.cpusPart / 1000;
                            $scope.CPUSPart = Math.round($scope.CPUSPart * 100) / 100
                            $scope.CPUSPart = format1($scope.CPUSPart,'');
                            $scope.CPUSBahan = hasil.cpusBahan / 1000;
                            $scope.CPUSBahan = Math.round($scope.CPUSBahan * 100) / 100
                            $scope.CPUSBahan = format1($scope.CPUSBahan,'');
                            $scope.CPUSOPL = hasil.cpusOPL / 1000;
                            $scope.CPUSOPL = Math.round($scope.CPUSOPL * 100) / 100
                            $scope.CPUSOPL = format1($scope.CPUSOPL,'');
                            $scope.CPUSOPB = hasil.cpusOPB / 1000;
                            $scope.CPUSOPB = Math.round($scope.CPUSOPB * 100) / 100
                            $scope.CPUSOPB = format1($scope.CPUSOPB,'');
                            $scope.CPUSRevenue = hasil.cpusRevenue / 1000;
                            $scope.CPUSRevenue = Math.round($scope.CPUSRevenue * 100) / 100
                            $scope.CPUSRevenue = format1($scope.CPUSRevenue,'');
                            if (hasil.cpusUnit>0) $scope.CPUSRevenueAvg = (hasil.cpusRevenue / hasil.cpusUnit) / 1000;
                            $scope.CPUSRevenueAvg = Math.round($scope.CPUSRevenueAvg * 100) / 100
                            $scope.CPUSRevenueAvg = format1($scope.CPUSRevenueAvg,'');
                            $scope.CPUSRegUnit = hasil.cpusRegUnit;
                            $scope.CPUSRegUnit = Math.round($scope.CPUSRegUnit * 100) / 100
                            $scope.CPUSRegRevenue = hasil.reg_Revenue / 1000;
                            $scope.CPUSRegRevenue = Math.round($scope.CPUSRegRevenue * 100) / 100
                            $scope.CPUSRegRevenue = format1($scope.CPUSRegRevenue,'');
                            if (hasil.reg_Unit>0) $scope.CPUSRegRevenueAvg = (hasil.reg_Revenue / hasil.reg_Unit) / 1000;
                            $scope.CPUSRegRevenueAvg = Math.round($scope.CPUSRegRevenueAvg * 100) / 100
                            $scope.CPUSRegRevenueAvg = format1($scope.CPUSRegRevenueAvg,'');
                            $scope.CPUSTPSUnit = hasil.tpS_Unit;
                            $scope.CPUSTPSUnit = Math.round($scope.CPUSTPSUnit * 100) / 100
                            $scope.CPUSTPSRevenue = hasil.tpS_Revenue / 1000;
                            $scope.CPUSTPSRevenue = Math.round($scope.CPUSTPSRevenue * 100) / 100
                            $scope.CPUSTPSRevenue = format1($scope.CPUSTPSRevenue,'');
                            if (hasil.tpS_Unit>0) $scope.CPUSTPSRevenueAvg = (hasil.tpS_Revenue / hasil.tpS_Unit) / 1000;
                            $scope.CPUSTPSRevenueAvg = Math.round($scope.CPUSTPSRevenueAvg * 100) / 100
                            $scope.CPUSTPSRevenueAvg = format1($scope.CPUSTPSRevenueAvg,'');

                            $scope.NoCPUSUnit=hasil.nonCPUSUnit;
                            $scope.NoCPUSUnit = Math.round($scope.NoCPUSUnit * 100) / 100
                            $scope.NoCPUSPart = hasil.nonCPUSParts / 1000;
                            $scope.NoCPUSPart = Math.round($scope.NoCPUSPart * 100) / 100
                            $scope.NoCPUSPart = format1($scope.NoCPUSPart,'');
                            $scope.NoCPUSOPB = hasil.nonCPUSOPB / 1000;
                            $scope.NoCPUSOPB = Math.round($scope.NoCPUSOPB * 100) / 100
                            $scope.NoCPUSOPB = format1($scope.NoCPUSOPB,'');
                            $scope.NoCPUSRevenue = hasil.nonCPUSRevenue / 1000;
                            $scope.NoCPUSRevenue = Math.round($scope.NoCPUSRevenue * 100) / 100
                            $scope.NoCPUSRevenue = format1($scope.NoCPUSRevenue,'');
                            $scope.NoCPUSJasa = hasil.nonCPUSJasa / 1000;
                            $scope.NoCPUSJasa = Math.round($scope.NoCPUSJasa * 100) / 100
                            $scope.NoCPUSJasa = format1($scope.NoCPUSJasa,'');
                            $scope.NoCPUSBahan = hasil.nonCPUSBahan / 1000;
                            $scope.NoCPUSBahan = Math.round($scope.NoCPUSBahan * 100) / 100
                            $scope.NoCPUSBahan = format1($scope.NoCPUSBahan,'');
                            $scope.NoCPUSOPL = hasil.nonCPUSOPL / 1000;
                            $scope.NoCPUSOPL = Math.round($scope.NoCPUSOPL * 100) / 100
                            $scope.NoCPUSOPL = format1($scope.NoCPUSOPL,'');
                            if (hasil.nonCPUSUnit>0) $scope.NoCPUSRevenueAvg=(hasil.nonCPUSRevenue/hasil.nonCPUSUnit) / 1000;
                            $scope.NoCPUSRevenueAvg = Math.round($scope.NoCPUSRevenueAvg * 100) / 100
                            $scope.NoCPUSRevenueAvg = format1($scope.NoCPUSRevenueAvg,'');
                            console.log('cpus part',$scope.CPUSPart,hasil.cpusPart);


                        }
                        else
                        {
                            $scope.CPUSTotalUnit = 0;
                            $scope.CPUSJasa = 0;
                            $scope.CPUSPart = 0;
                            $scope.CPUSBahan = 0;
                            $scope.CPUSOPL = 0;
                            $scope.CPUSOPB = 0;
                            $scope.CPUSRevenue = 0;
                            $scope.CPUSRevenueAvg = 0;
                            $scope.CPUSRegUnit = 0;
                            $scope.CPUSRegRevenue = 0;
                            $scope.CPUSRegRevenueAvg = 0;
                            $scope.CPUSTPSUnit = 0;
                            $scope.CPUSTPSRevenue = 0;
                            $scope.CPUSTPSRevenueAvg = 0;

                            // $scope.NoCPUSUnit=hasil.;
                            $scope.NoCPUSPart = 0;
                            $scope.NoCPUSOPB = 0;
                            $scope.NoCPUSRevenue = 0;
                            $scope.NoCPUSJasa = 0;
                            $scope.NoCPUSBahan = 0;
                            $scope.NoCPUSOPL = 0;
                        }
                    })
               }

        //   }
        }
        $scope.getReport = function() {
            if ($scope.role == "Branch") $scope.getReportBranch();
            else $scope.getReportHO();
        }
        $scope.getReportHO = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '/' + ($tanggal.getMonth() + 1) + '/' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '/' + ($tanggalakhir.getMonth() + 1) + '/' + $tanggalakhir.getDate();
            var dataReport = {
                'kategori': $scope.filter.kategori,
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'tamarea': $scope.filter.tamArea,
                'satellite': $scope.filter.Satellite,
                'dealerarea': $scope.filter.DealerArea
            };

            RevenueBPFactory.getExportHO(dataReport);
        }
        $scope.getReportBranch = function() {
            var $tanggal = $scope.filter.DateStart;
            var $filtertanggal = $tanggal.getFullYear() + '/' + ($tanggal.getMonth() + 1) + '/' + $tanggal.getDate();
            var $tanggalakhir = $scope.filter.DateEnd;
            var $filtertanggalakhir = $tanggalakhir.getFullYear() + '/' + ($tanggalakhir.getMonth() + 1) + '/' + $tanggalakhir.getDate();
            var dataReport = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'kategori': $scope.filter.kategori
            };

            RevenueBPFactory.getExportCabang(dataReport);
        }

        // $scope.allowPatternFilter = function(event, type, item) {
        //     console.log("event", event);
        //     var patternRegex
        //     if (type == 1) {
        //         patternRegex = /\d|\\/i; //NUMERIC ONLY
        //     }
        //     var keyCode = event.which || event.keyCode;
        //     var keyCodeChar = String.fromCharCode(keyCode);
        //     if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
        //         event.preventDefault();
        //         return false;
        //     }
        // };

    });
