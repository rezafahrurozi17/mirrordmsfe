angular.module('app')
    .controller('PsfuBPController', function($scope, $stateParams, $http, CurrentUser, PsfuBPFactory,$timeout ) {
        console.log('PSFUBP COMP');
        $scope.user = CurrentUser.user();
        $scope.roleId = $scope.user.RoleId;
        $scope.role=$stateParams.role;
        console.log('role',$scope.role);
        var dateFormat = 'MM/yyyy';
        var dateFormat2 = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var periode=new Date();
        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat,
            // mode:"'month'",
        };
        $scope.dateOptions2S = {
            startingDay: 1,
            format: dateFormat2,
        };
        $scope.dateOptions2E = {
            startingDay: 1,
            format: dateFormat2,
        };


        //===============validasi date start and date end===========
        var today = new Date();
        $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(), today.getDate());
        $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());
        $scope.minDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());


        $scope.changeStartDate = function(tgl){
            var today = new Date();
            var dayOne = new Date($scope.filter.DateStart);
            var dayTwo = new Date($scope.filter.DateEnd);

            $scope.dateOptions2E.minDate = new Date(dayOne.getFullYear(),dayOne.getMonth(), dayOne.getDate());
            if (tgl == null || tgl == undefined){
                $scope.filter.DateEnd = null;
            } else {
                if ($scope.filter.DateStart < $scope.filter.DateEnd){

                }else {
                  if (dayOne > today){
                      $scope.filter.DateStart = today;
                      console.log('masuk if today', today);
                      $scope.filter.DateEnd = $scope.filter.DateStart;
                  } else {
                      $scope.filter.DateEnd = $scope.filter.DateStart;
                  }
                }
            }

        }

        $scope.changeEndDate = function(tgl){
            var today = new Date();
            var dayOne = new Date($scope.filter.DateStart);
            var dayTwo = new Date($scope.filter.DateEnd);

            if (dayTwo > today){
                $scope.dateOptions2E.minDate = $scope.filter.DateStart;
                $scope.filter.DateEnd = today;
            } else if (dayTwo < $scope.filter.DateStart) {
                $scope.filter.DateEnd = $scope.filter.DateStart;
            }
        }
        //=============== End of validasi date start and date end===========

        var currentUser = CurrentUser.user();
        console.log('LOGIN :', currentUser.OrgId)
        $scope.userlogin = currentUser.OrgCode.substring(0, 3);
        console.log('Dealer :', currentUser.OrgCode.substring(0, 3))

        $scope.filter = {Dealer:null,DealerId:null, Cabang:currentUser.OrgId, kategori:"Body and Paint", DateStart:periode,DateEnd:periode, satelite:null, dealerarea:0,tamarea:0,isGR:0 };

        $scope.dataDealer = [];
        $scope.dataCabang = [];
        $scope.dataAreaTAM = [];
        $scope.dataAreaDealer = [];
        $scope.dataSA = [];

        PsfuBPFactory.getDataDealer().then(function(response) {
            console.log('Response BEDealer:', response.data);
            $scope.dataDealer = response.data;
            $scope.filter.Dealer=response.data[0].dealerCode;
            $scope.filter.DealerId=response.data[0].dealerID;
            PsfuBPFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
                console.log('ayamtest',response.data)
                $scope.dataGroupDealerHO = response.data
                $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
                $scope.areaId($scope.dataGroupDealerHO[0])
            })
        })
        PsfuBPFactory.getDataCabang().then(function(response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabang = response.data;
            $scope.DisabledCabang();
            $scope.filter.Cabang=response.data[0].cabangID;
            $scope.filter.Cabang=$scope.user.OrgId;
        })
        PsfuBPFactory.getDataAreaTAM().then(function(response) {
            console.log('Response AreaTAM:', response.data);
            $scope.dataAreaTAM = response.data;
        })
        PsfuBPFactory.getDataAreaDealer().then(function(response) {
            console.log('Response AreaDealer:', response.data);
            $scope.dataAreaDealer = response.data;
        })
        PsfuBPFactory.getDataAreaDealerHO($scope.filter).then(function(response) {
            console.log('Response AreaDealerHO:', response.data);
            $scope.dataAreaDealerHO = response.data;
            $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
            $scope.areaId($scope.dataAreaDealerHO[0])
        })
        PsfuBPFactory.getDataSatellite().then(function (response) {
            console.log('Response satellite:', response.data);
            $scope.dataSatellite = response.data;
        })
    
    $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };

        


        $scope.areaId = function(row) {
          // $scope.DataDealer = row
            $scope.DataDealer = Object.assign(row,{'kategori': $scope.filter.kategori})
            if($scope.userlogin != 'TAM'){
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
            }else{
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
            }
            console.log('row dealerid', row,$scope.DataDealer);
            PsfuBPFactory.getDataCabangHONew($scope.DataDealer).then(function (response) {
                console.log('Response BECabang:', response.data);
                $scope.dataCabangHO = response.data;
                $scope.DisabledCabang();
                $scope.filter.CabangHO = null
                console.log('kambing',$scope.filter)
                $timeout(function() {
                    $scope.filter.CabangHO = response.data[0].cabangID;
                    console.log('kambing123',$scope.filter.CabangHO)
                }, 100);
            })
        };

        $scope.dealerCode = function(row){
            row = Object.assign(row,{'isGR':$scope.filter.isGR})
            $scope.filter.DealerId = row.dealerID
            PsfuBPFactory.getDataAreaDealerHO(row).then(function (response) {
                console.log('ayam',response.data)
                console.log('rusa',$scope.dataGroupDealer)
                $scope.dataGroupDealer = response.data
                $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
                $scope.areaId($scope.dataGroupDealer[0])
                console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
            })
        }

        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
        // PsfuBPFactory.getDataSA().then(function(response) {
        //     console.log('Response SA:', response.data);
        //     $scope.dataSA = response.data;
        // })
        

        $scope.getData = function(){
            if ($scope.role == "Branch") $scope.getSummaryBPCabang();
            else $scope.getSummaryBPHO();
            console.log('get data');
        }

        // =================================================================== BP HO ====================
        $scope.getSummaryBPHO = function() {
            var $tanggal1 = $scope.filter.DateStart;
            var $tanggal2 = $scope.filter.DateEnd;
            var $filtertanggal1 = $tanggal1.getFullYear() + '-' + ($tanggal1.getMonth() + 1) + '-' + $tanggal1.getDate();
            var $filtertanggal2 = $tanggal2.getFullYear() + '-' + ($tanggal2.getMonth() + 1) + '-' + $tanggal2.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'tamarea': $scope.filter.tamarea,
                'dealerarea': $scope.filter.dealerarea,
                'kategori': "BP",
                'periode_awal': $filtertanggal1,
                'periode_akhir': $filtertanggal2,
                'Jenis': 'summary',
                'satelite':$scope.filter.satellite
            };

            PsfuBPFactory.getSummaryBPHO(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);
                // $scope.dataSummary = response.data;
                var hasil = response.data[0];
                $scope.unitYangHarusFU = hasil.jumlahYangHarusFollowUp;
                $scope.unitYangHarusFU = Math.round($scope.unitYangHarusFU * 100) / 100
                $scope.unitBerhasilFU = hasil.jumlahTerFollowUp;
                $scope.unitBerhasilFU = Math.round($scope.unitBerhasilFU * 100) / 100
                $scope.Q1 = hasil.jumlahQ1;
                $scope.Q1 = Math.round($scope.Q1 * 100) / 100
                $scope.Q2 = hasil.jumlahQ2;
                $scope.Q2 = Math.round($scope.Q2 * 100) / 100
                $scope.Q3 = hasil.jumlahQ3;
                $scope.Q3 = Math.round($scope.Q3 * 100) / 100
                $scope.Q1Rate = hasil.q1Rate;
                $scope.Q1Rate = Math.round($scope.Q1Rate * 100) / 100
                $scope.Q2Rate = hasil.q2Rate;
                $scope.Q2Rate = Math.round($scope.Q2Rate * 100) / 100
                $scope.Q3Rate = hasil.q3Rate;
                $scope.Q3Rate = Math.round($scope.Q3Rate * 100) / 100
                $scope.firRate = hasil.jumlahTerFollowUp > 0 ? (hasil.jumlahTerFollowUp-(hasil.jumlahQ1 + hasil.jumlahQ2 + hasil.jumlahQ3)) / hasil.jumlahTerFollowUp *100 : 0;
                $scope.firRate = Math.round($scope.firRate * 100) / 100
            })
        }
        $scope.getReportBPHO = function() {
            var $tanggal1 = $scope.filter.DateStart;
            var $tanggal2 = $scope.filter.DateEnd;
            var $filtertanggal1 = $tanggal1.getFullYear() + '/' + ($tanggal1.getMonth() + 1) + '/' + $tanggal1.getDate();
            var $filtertanggal2 = $tanggal2.getFullYear() + '/' + ($tanggal2.getMonth() + 1) + '/' + $tanggal2.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'tamarea': $scope.filter.tamarea,
                'dealerarea': $scope.filter.dealerarea,
                'kategori': "BP",
                'periode_awal': $filtertanggal1,
                'periode_akhir': $filtertanggal2,
                'satelite':$scope.filter.satellite,
                'Jenis': 'summary'
            };

            PsfuBPFactory.getExportBPHO(dataFilter);
        }

        // ============================================================================== BP CABANG ====================
        $scope.getSummaryBPCabang = function() {
            var $tanggal1 = $scope.filter.DateStart;
            var $tanggal2 = $scope.filter.DateEnd;
            var $filtertanggal1 = $tanggal1.getFullYear() + '-' + ($tanggal1.getMonth() + 1) + '-' + $tanggal1.getDate();
            var $filtertanggal2 = $tanggal2.getFullYear() + '-' + ($tanggal2.getMonth() + 1) + '-' + $tanggal2.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'kategori': "BP",
                'periode_awal': $filtertanggal1,
                'periode_akhir': $filtertanggal2,
                'satelite': $scope.filter.satelite
            };

            PsfuBPFactory.getSummaryBPCabang(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);
                // $scope.dataSummary = response.data;
                var hasil = response.data[0];
                $scope.unitYangHarusFU = hasil.jumlahYangHarusFollowUp;
                $scope.unitYangHarusFU = Math.round($scope.unitYangHarusFU * 100) / 100
                $scope.unitBerhasilFU = hasil.jumlahTerFollowUp;
                $scope.unitBerhasilFU = Math.round($scope.unitBerhasilFU * 100) / 100
                $scope.Q1 = hasil.jumlahQ1;
                $scope.Q1 = Math.round($scope.Q1 * 100) / 100
                $scope.Q2 = hasil.jumlahQ2;
                $scope.Q2 = Math.round($scope.Q2 * 100) / 100
                $scope.Q3 = hasil.jumlahQ3;
                $scope.Q3 = Math.round($scope.Q3 * 100) / 100
                $scope.Q1Rate = hasil.q1Rate;
                $scope.Q1Rate = Math.round($scope.Q1Rate * 100) / 100
                $scope.Q2Rate = hasil.q2Rate;
                $scope.Q2Rate = Math.round($scope.Q2Rate * 100) / 100
                $scope.Q3Rate = hasil.q3Rate;
                $scope.Q3Rate = Math.round($scope.Q3Rate * 100) / 100
                $scope.firRate = hasil.jumlahTerFollowUp > 0 ? (hasil.jumlahTerFollowUp-(hasil.jumlahQ1 + hasil.jumlahQ2 + hasil.jumlahQ3)) / hasil.jumlahTerFollowUp *100 : 0;
                $scope.firRate = Math.round($scope.firRate * 100) / 100
            })
        }
        $scope.getReportBPCabang = function() {
            var $tanggal1 = $scope.filter.DateStart;
            var $tanggal2 = $scope.filter.DateEnd;
            var $filtertanggal1 = $tanggal1.getFullYear() + '/' + ($tanggal1.getMonth() + 1) + '/' + $tanggal1.getDate();
            var $filtertanggal2 = $tanggal2.getFullYear() + '/' + ($tanggal2.getMonth() + 1) + '/' + $tanggal2.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'kategori': "BP",
                'periode_awal': $filtertanggal1,
                'periode_akhir': $filtertanggal2,
                'satelite': $scope.filter.satelite
            };

            PsfuBPFactory.getExportBPCabang(dataFilter);
        }

        //----------------------------------
        //  // Start-Up
        //  //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
        });
        //  //----------------------------------
        //  // Initialization
        //  //----------------------------------
        //  $scope.user = CurrentUser.user();
        //  $scope.loading=false;
        //  $scope.formApi = {};
        //  $scope.backToMain = true;
        //  $scope.roleId = 0;// 0 = HO | 1 = Branch


        //  $scope.onSelectUser = function(item, model, label){
        //      console.log("onSelectUser=>",item,model,label);
        //  }

    

    //=============== End of validasi date start and date end===========

    });
