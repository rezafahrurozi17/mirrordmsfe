angular.module('app')
  .factory('PsfuBPFactory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = '';//http://localhost:8080';
    return {
      getDataDealer: function() {
        var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
        return res;},
      getDataCabang: function(data) {
        var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId + '/' + data.kategori);
        return res;},
      getDataCabang: function() {
        var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
        return res;},
      getDataCabangHONew: function(data) {
        console.log('Data Get Cabang:', data);
        var res=$http.get(serverURL + '/api/rpt/GetCabangHONew/'+ data.areaId + '/' +  data.DealerId + '/' + data.kategori);
        return res;},
      getDataAreaTAM: function() {
      var res=$http.get(serverURL + '/api/rpt/AreaTAM/');
      return res;},
      getDataAreaDealer: function() {
      var res=$http.get(serverURL + '/api/rpt/AreaDealer/');
      return res;},
      getDataAreaDealerHO: function(data) {
      var res=$http.get(serverURL + '/api/rpt/AreaDealerHONew/'+ data.isGR );
      return res;},
      // getDataSA: function(data) {
      // var res=$http.get(serverURL + '/api/rpt/SA/');
      // return res;},
      getDataSatellite: function() {
            var res=$http.get(serverURL + '/api/rpt/Satellite/');
            return res;},
      getData: function() {
        var res=$http.get('/api/fw/Role');
        console.log('data=>',res);
        //---------
        return $q.resolve(data);
      },
      getSummaryBPHO: function(data) {
              console.log('Data Filter:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_KPIPSFU_BPHO/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/'+ data.tamarea +'/'+ data.dealerarea +'/'+ data.satelite);
              // var res = $http.get(serverURL + '/api/rpt/AS_KPIPSFU_BPHO/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/'+ data.tamarea +'/'+ data.dealerarea +'/'+ data.satelite);
              return res;
            },
      getExportBPHO: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_KPIPSFU_BPHO?TamArea='+ data.tamarea +'&DealerArea='+ data.dealerarea +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periode_awal +'&dateend='+ data.periode_akhir +'&satelite='+ data.satelite);
            },
      getSummaryBPCabang: function(data) {
              console.log('Data Filter:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_KPIPSFU_BPCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/'+ data.kategori +'/'+ data.satelite);
              return res;},
      getExportBPCabang: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_KPIPSFU_BPCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periode_awal +'&dateend='+ data.periode_akhir +'&kategori='+ data.kategori +'&satelite='+ data.satelite);
            },
      getSummaryGRHO: function(data) {
              console.log('Data Filter:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_KPIPSFU_GRHO/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/'+ data.tamarea +'/'+ data.dealerarea +'/'+ data.kategori);
              return res;},
      getExportGRHO: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_KPIPSFU_GRHO?TamArea='+ data.tamarea +'&DealerArea='+ data.dealerarea +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periode_awal +'&dateend='+ data.periode_akhir +'&kategori='+ data.kategori);
            },
      getSummaryGRCabang: function(data) {
              console.log('Data Filter:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_KPIPSFU_GRCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/'+ data.kategori +'/'+ data.sa);
              return res;},
      getExporGRCabang: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_KPIPSFU_GRCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periode_awal +'&dateend='+ data.periode_akhir +'&kategori='+ data.kategori +'&sa='+ data.sa);
            }
    }
  });
