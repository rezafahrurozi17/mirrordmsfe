angular.module('app')
    .controller('ReportCustomerInBPController', function($scope,$stateParams, $http, CurrentUser, ReportCustomerInBP ,$timeout,$q,bsAlert,$q, bsNotify) {
    //----------------------------------
    // Start-Up

    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
    });


    // //----------------------------------
    // // Initialization
    // //----------------------------------
    //
    //
    // $scope.formApi = {};
    // $scope.backToMain = true;
    $scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;
    $scope.role=$stateParams.role;
    console.log('role',$scope.role);
    $scope.formname=$stateParams.formname;
    console.log('formname',$scope.formname);
    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    var dateFormat2='dd/MM/yyyy';
    var dateFormat='MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat2,
        // mode:"'month'",
    };
    $scope.dateOptions2 = {
        startingDay: 1,
        format: dateFormat2,
        // mode:"'month'",
    };
    // //----------------------------------
    // // Get Data
    // //----------------------------------
    $scope.filter = {Dealer:null,DealerId:null, Cabang:null, kategori:"Body and Paint", DateStart:periode,DateEnd:periode, satellite:null, dealerarea:0,tamarea:0,isGR:0 };
    ReportCustomerInBP.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
        $scope.filter.DealerId=response.data[0].dealerID;
        ReportCustomerInBP.getDataAreaDealerHO($scope.filter).then(function (response) {
          console.log('ayamtest',response.data)
          $scope.dataGroupDealerHO = response.data
          $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
          $scope.GroupDealerAreaId($scope.dataGroupDealerHO[0])
        })
      })
    ReportCustomerInBP.getDataCabang($scope.filter).then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
    ReportCustomerInBP.getDataAreaTAM().then(function (response) {
        console.log('Response AreaTAM:', response.data);
        $scope.dataAreaTAM = response.data;
      })
    ReportCustomerInBP.getDataAreaDealer().then(function (response) {
        console.log('Response AreaDealer:', response.data);
        $scope.dataAreaDealer = response.data;
      })
    ReportCustomerInBP.getDataAreaDealerHO($scope.filter).then(function (response) {
        console.log('Response AreaDealerHO:', response.data);
        $scope.dataAreaDealerHO = response.data;
        $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
        $scope.GroupDealerAreaId($scope.dataAreaDealerHO[0])
      })
    ReportCustomerInBP.getDataSatellite().then(function (response) {
        console.log('Response satellite:', response.data);
        $scope.dataSatellite = response.data;
      })
    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
            $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };

     $scope.getData = function() {
            if ($scope.role == "Branch")
                $scope.getSummaryCabang();
             else
                $scope.getSummaryHO();
            console.log('get data');
        }

      // $scope.filter.dealerarea = null

      $scope.GroupDealerAreaId = function(row) {
        // $scope.DataDealer = row
        $scope.DataDealer = Object.assign(row,{'kategori': $scope.filter.kategori})
        if($scope.userlogin != 'TAM'){
          $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
        }else{
          $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
        }
        console.log('row dealerid', row,$scope.DataDealer);
        ReportCustomerInBP.getDataCabangHONew($scope.DataDealer).then(function (response) {
          console.log('Response BECabang:', response.data);
          $scope.dataCabangHO = response.data;
          $scope.DisabledCabang();
          $scope.filter.CabangHO = null
          console.log('kambing',$scope.filter)
          $timeout(function() {
            $scope.filter.CabangHO = response.data[0].cabangID;
            console.log('kambing123',$scope.filter.CabangHO)
          }, 100);
        })
      };

      $scope.dealerCode = function(row){
        row = Object.assign(row,{'isGR':$scope.filter.isGR})
        $scope.filter.DealerId = row.dealerID
        ReportCustomerInBP.getDataAreaDealerHO(row).then(function (response) {
            console.log('ayam',response.data)
            console.log('rusa',$scope.dataGroupDealer)
            $scope.dataGroupDealer = response.data
            $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
            $scope.GroupDealerAreaId($scope.dataGroupDealer[0])
            console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
        })
      }

      $scope.DisabledCabang = function() {
        if ($scope.dataCabang.length>1){
          $scope.Disabled =false;
          console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
        }else{
          $scope.Disabled =true;
          console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
        }
      };  
        
      $scope.getSummaryHO = function(){
        var $tanggal=$scope.filter.DateStart;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
        var dataFilter = {
            'kategori': $scope.filter.kategori,
            'jenis': "summary",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'satelite': $scope.filter.satellite,
            'dealerarea': $scope.filter.dealerarea};

          ReportCustomerInBP.getSummaryHO(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;

            if(response.data[0].allWo == null || response.data[0].allWo == undefined ){
              response.data[0].allWo = 0;
            }
            if(response.data[0].cash == null || response.data[0].cash == undefined ){
              response.data[0].cash = 0;
            }
            if(response.data[0].cashPercent == null || response.data[0].cashPercent == undefined ){
              response.data[0].cashPercent = 0;
            }
            if(response.data[0].customerCancel == null || response.data[0].customerCancel == undefined ){
              response.data[0].customerCancel = 0;
            }
            if(response.data[0].customerIn == null || response.data[0].customerIn == undefined ){
              response.data[0].customerIn = 0;
            }
            if(response.data[0].heavy == null || response.data[0].heavy == undefined ){
              response.data[0].heavy = 0;
            }
            if(response.data[0].insurance == null || response.data[0].insurance == undefined ){
              response.data[0].insurance = 0;
            }
            if(response.data[0].insurancePercent == null || response.data[0].insurancePercent == undefined ){
              response.data[0].insurancePercent = 0;
            }
            if(response.data[0].light == null || response.data[0].light == undefined ){
              response.data[0].light = 0;
            }
            if(response.data[0].medium == null || response.data[0].medium == undefined ){
              response.data[0].medium = 0;
            }
            if(response.data[0].outletSatelite == null || response.data[0].outletSatelite == undefined ){
              response.data[0].outletSatelite = 0;
            }
            if(response.data[0].tps == null || response.data[0].tps == undefined ){
              response.data[0].tps = 0;
            }
            if(response.data[0].unitCenter == null || response.data[0].unitCenter == undefined ){
              response.data[0].unitCenter = 0;
            }
            if(response.data[0].unitEntry == null || response.data[0].allWo == undefined ){
              response.data[0].unitEntry = 0;
            }
            $scope.CustomerReleaseWO = response.data[0].allWo;
            $scope.CustomerReleaseWO = Math.round($scope.CustomerReleaseWO * 100) / 100
            $scope.TotalUnitEntry = response.data[0].unitEntry;
            $scope.TotalUnitEntry = Math.round($scope.TotalUnitEntry * 100) / 100
            $scope.UnitSatelitte = response.data[0].outletSatelite;
            $scope.UnitSatelitte = Math.round($scope.UnitSatelitte * 100) / 100
            $scope.UnitCenter = response.data[0].unitCenter;
            $scope.UnitCenter = Math.round($scope.UnitCenter * 100) / 100
            $scope.CustomerIn = response.data[0].customerIn;
            $scope.CustomerIn = Math.round($scope.CustomerIn * 100) / 100
            $scope.Cancel = response.data[0].customerCancel;
            $scope.Cancel = Math.round($scope.Cancel * 100) / 100
            // $scope.PotentialLoss = response.data[0].customerIn > 0 ? (response.data[0].customerCancel / response.data[0].customerIn)*100 : 0;
            // $scope.PotentialLoss = Math.round($scope.PotentialLoss * 100) / 100
            if(response.data[0].allWo == 0){
              $scope.PotentialLoss = 0
            }else{
              $scope.PotentialLoss = response.data[0].customerIn > 0 ? (response.data[0].customerCancel / response.data[0].allWo)*100 : 0;
              $scope.PotentialLoss = Math.round($scope.PotentialLoss * 100) / 100
            }

            $scope.Cash1 = response.data[0].cash;
            $scope.Cash1 = Math.round($scope.Cash1 * 100) / 100
            $scope.Cash2 = response.data[0].cashPercent;
            $scope.Cash2 = Math.round($scope.Cash2 * 100) / 100
            $scope.Insurance1 = response.data[0].insurance;
            $scope.Insurance1 = Math.round($scope.Insurance1 * 100) / 100
            $scope.Insurance2 = response.data[0].insurancePercent;
            $scope.Insurance2 = Math.round($scope.Insurance2 * 100) / 100

            $scope.TPS = response.data[0].tps;
            $scope.TPS = Math.round($scope.TPS * 100) / 100
            $scope.Light = response.data[0].light;
            $scope.Light = Math.round($scope.Light * 100) / 100
            $scope.Medium = response.data[0].medium;
            $scope.Medium = Math.round($scope.Medium * 100) / 100
            $scope.Heavy = response.data[0].heavy;
            $scope.Heavy = Math.round($scope.Heavy * 100) / 100
          })
      }
      $scope.getSummaryCabang = function(){
        var $tanggal=$scope.filter.DateStart;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'satelite': $scope.filter.satellite};

          ReportCustomerInBP.getSummaryCabang(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;
            $scope.CustomerReleaseWO = response.data[0].allWo;
            $scope.CustomerReleaseWO = Math.round($scope.CustomerReleaseWO * 100) / 100
            // $scope.TotalUnitEntry = response.data[0].TotalUnitEntry;
            // $scope.TotalUnitEntry = Math.round($scope.TotalUnitEntry * 100) / 100
            $scope.UnitSatelitte = response.data[0].unitSatelite;
            $scope.UnitSatelitte = Math.round($scope.UnitSatelitte * 100) / 100
            $scope.UnitCenter = response.data[0].unitCenter;
            $scope.UnitCenter = Math.round($scope.UnitCenter * 100) / 100
            $scope.CustomerIn = response.data[0].customerIn;
            $scope.CustomerIn = Math.round($scope.CustomerIn * 100) / 100
            $scope.Cancel = response.data[0].cancel;
            $scope.Cancel = Math.round($scope.Cancel * 100) / 100
            if(response.data[0].allWo == 0){
              $scope.PotentialLoss = 0
            }else{
              $scope.PotentialLoss = response.data[0].customerIn > 0 ? (response.data[0].cancel / response.data[0].allWo)*100 : 0;
              $scope.PotentialLoss = Math.round($scope.PotentialLoss * 100) / 100
            }
            $scope.Cash1 = response.data[0].cashA;
            $scope.Cash1 = Math.round($scope.Cash1 * 100) / 100
            $scope.Cash2 = response.data[0].cashB;
            $scope.Cash2 = Math.round($scope.Cash2 * 100) / 100
            $scope.Insurance1 = response.data[0].insuranceA;
            $scope.Insurance1 = Math.round($scope.Insurance1 * 100) / 100
            $scope.Insurance2 = response.data[0].insuranceB;
            $scope.Insurance2 = Math.round($scope.Insurance2 * 100) / 100

            $scope.TPS = response.data[0].tps;
            $scope.TPS = Math.round($scope.TPS * 100) / 100
            $scope.Light = response.data[0].light;
            $scope.Light = Math.round($scope.Light * 100) / 100
            $scope.Medium = response.data[0].medium;
            $scope.Medium = Math.round($scope.Medium * 100) / 100
            $scope.Heavy = response.data[0].heavy;
            $scope.Heavy = Math.round($scope.Heavy * 100) / 100
          })
      }
      $scope.getReportHO = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
          var dataReport = {
            'kategori': $scope.filter.kategori,
            'jenis': "export",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'satelite': $scope.filter.satellite,
            'dealerarea': $scope.filter.dealerarea,};

        ReportCustomerInBP.getExportHO(dataReport);
    }
    $scope.getReportCabang = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
          var dataReport = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'satelite': $scope.filter.satellite};

            if(($scope.CustomerReleaseWO == undefined) || ($scope.UnitSatelitte == undefined)
              || ($scope.UnitCenter == undefined) || ($scope.CustomerIn == undefined)
              || ($scope.Cancel == undefined) || ($scope.PotentialLoss == undefined)){
                bsNotify.show({
                  size: 'big',
                  type: 'danger',
                  title: "error",
                  content: "can not export due to empty summary"
                });

            }else{
              ReportCustomerInBP.getExportCabang(dataReport);
            };

    }

});
