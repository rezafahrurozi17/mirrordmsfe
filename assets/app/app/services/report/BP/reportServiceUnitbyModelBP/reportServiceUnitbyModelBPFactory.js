angular.module('app')
  .factory('ReportServiceUnitbyModelBP', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = ''; //'http://localhost:8080';
    return {
      getDataDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},
      getDataCabang: function() {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            return res;},
      getExport: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_KPIServiceUnitByModel_BPCAB?dealer='+ data.dealer +'&cabang='+ data.cabang  +'&kat_workshop='+ data.kategori +'&periode='+ data.periode);
              // var res = $http.get(serverURL + 'CCustomerCompleteness?area='+ data.area +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periode +'&jenis_pelanggan='+ data.jenispel);
              // return res;
            }
    }
  });
