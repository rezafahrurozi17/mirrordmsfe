angular.module('app')
    .controller('ReportServiceUnitbyModelBPController', function($scope, $http, CurrentUser, ReportServiceUnitbyModelBP  ) {
        
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
        });
        $scope.user = CurrentUser.user();
        $scope.roleId = $scope.user.RoleId;

        $scope.formatDate = function(date) {
            var dateOut = new Date(date);
            return dateOut;
        };
        var dateFormat = 'MM/yyyy';
        var dateFilter = 'date:"MM/yyyy"';
        var periode=new Date();
        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat,

            // mode:"'month'",
        };
        // //----------------------------------
        // // Get Data
        // //----------------------------------
        $scope.dataDealer = [];
        $scope.dataCabang = [];
        $scope.filter = {Dealer:null, Cabang:null, kategori:"Body and Paint", MonthYear:periode, Satellite:null, dealerarea:0,tamarea:0 };
        ReportServiceUnitbyModelBP.getDataDealer().then(function(response) {
            console.log('Response BEDealer:', response.data);
            $scope.dataDealer = response.data;
            $scope.filter.Dealer=response.data[0].dealerCode;
        })
        ReportServiceUnitbyModelBP.getDataCabang().then(function(response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabang = response.data;
            $scope.DisabledCabang();
            $scope.filter.Cabang=response.data[0].cabangID;
        })
        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
        $scope.getData = function() {
            
        }

        $scope.getReport = function() {
            var $tanggal = $scope.filter.MonthYear;
            var $filtertanggal = $tanggal.getFullYear() + '-' + ($tanggal.getMonth() + 1) + '-' + $tanggal.getDate();

            var dataReport = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periode': $filtertanggal,
                // 'kategori': $scope.filter.kategori
                'kategori': "BP"
            };

            ReportServiceUnitbyModelBP.getExport(dataReport);
        }
    });