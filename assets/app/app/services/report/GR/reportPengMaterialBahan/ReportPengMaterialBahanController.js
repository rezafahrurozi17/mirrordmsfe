angular.module('app')
    .controller('ReportPengMaterialBahanGRController', function($scope, $http, CurrentUser, ReportPengMaterialBahanFactory,$timeout,$q, bsNotify) {
    //----------------------------------
    // Start-Up
    
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.grid = {};
        $scope.loading=false;
    });
    $scope.formatDate = function(date){
        var dateOut = new Date(date);
        return dateOut;
    };
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
	var periode=new Date();
     $scope.SdateOptions = {
        startingDay: 1,
        format: dateFormat,
        // mode:"'month'",
    };
    $scope.EdateOptions = {
        startingDay: 1,
        format: dateFormat,
        // mode:"'month'",
    };

     //===============validasi date start and date end===========
     var today = new Date();
     $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(), today.getDate());  
     $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());      
     $scope.minDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());          
 
 
     $scope.changeStartDate = function(tgl){
         var today = new Date();
         var dayOne = new Date($scope.filter.DateStart);
         var dayTwo = new Date($scope.filter.DateEnd);
         
         $scope.EdateOptions.minDate = new Date(dayOne.getFullYear(),dayOne.getMonth(), dayOne.getDate());
         if (tgl == null || tgl == undefined){
             $scope.filter.DateEnd = null;
         } else {
             if ($scope.filter.DateStart < $scope.filter.DateEnd){
 
             } else {
                 if (dayOne > today){
                     $scope.filter.DateStart = today;
                     $scope.filter.DateEnd = $scope.filter.DateStart;                    
                 } else {
                     $scope.filter.DateEnd = $scope.filter.DateStart;
                 }
             }
         }
     }

     $scope.changeEndDate = function(tgl){
      var today = new Date();
      var dayOne = new Date($scope.filter.DateStart);
      var dayTwo = new Date($scope.filter.DateEnd);

      if (dayTwo > today){
          $scope.EdateOptions.minDate = $scope.filter.DateStart;
          $scope.filter.DateEnd = today;
      }
  }
 
     //=============== End of validasi date start and date end===========

	$scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;
    $scope.filter = {Dealer:null, Cabang:null, DateStart:periode, DateEnd:periode, tamarea:0,dealerarea:0,kategori:"General Repair"};
    ReportPengMaterialBahanFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
      })
    ReportPengMaterialBahanFactory.getDataCabang().then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
    ReportPengMaterialBahanFactory.getKategoriPerbaikan().then(function (response) {
        console.log('Response BEKategoriPerbaikan:', response.data);
        $scope.dataKategoriPerbaikan = response.data;
      })
    // //----------------------------------
    // // Initialization
    // //----------------------------------
    // $scope.user = CurrentUser.user();
    // $scope.loading=false;
    // $scope.formApi = {};
    // $scope.backToMain = true;
    // //----------------------------------
    // // Get Data
    // //----------------------------------
    $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
    $scope.getData = function() {

    }

    var gridData = [];
    $scope.getReport = function() {
        var tempDate1 = new Date($scope.filter.DateStart);
        var tempDate2 = new Date($scope.filter.DateEnd);      
        var monthCount = 0;
        while((tempDate1.getMonth()+''+tempDate1.getFullYear()) != (tempDate2.getMonth()+''+tempDate2.getFullYear())) {
            monthCount++;
            tempDate1.setMonth(tempDate1.getMonth()+1);
        }
        console.log('hasil bulan', monthCount);
        if (monthCount >= 3){
            bsNotify.show(
              {
                  title: "Mandatory",
                  content: "Maksimal Range Tanggal Adjustment 3 bulan",
                  type: 'danger'
              }
            ); 
        } else {
            var $tanggal=$scope.filter.DateStart;
            var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
            var $tanggalakhir=$scope.filter.DateEnd;
            var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periode_akhir': $filtertanggalakhir,
                'periode_awal': $filtertanggal,
            };

             ReportPengMaterialBahanFactory.getExport(dataFilter);
        }

        
    }
        // $scope.loading=true;
        // return $q.resolve(
        //     ReportPengMaterialBahanFactory.getData().then(
        //       function(res){
        //         console.log("getData res=>",res);
        //         $scope.grid.data = res;
        //         $scope.loading=false;
        //         return res;
        //       },
        //       function(err){
        //         console.log("error=>",err);
        //         $scope.loading=false;
        //         return err;
        //       }
        //     )
        // )



     $scope.getDealer = [
    {DealerId:1,DealerName:"Dealer001"},
    {DealerId:2,DealerName:"Dealer002"}
    ];

    $scope.getCabang = [
    {cabangID:1,CabangName:"Cabang001"},
    {cabangID:2,CabangName:"Cabang002"}
    ];

     $scope.getKategori = [
    {KategoriId:1,KategoriName:"Kategori Perbaikan 001"},
    {KategoriId:2,KategoriName:"Kategori Perbaikan 002"}
    ];
    
});
