angular.module('app')
  .factory('ReportServiceUnitbyModelGR', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = ''; //'http://localhost:8080'
    return {
            getDataDealer: function() {
                  var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
                  return res;},
            getDataCabang: function() {
                  var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
                  return res;},
            getExport: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_KPIServiceUnitByModel_GRCAB?dealer='+ data.dealer +'&cabang='+ data.cabang  +'&kat_workshop='+ data.kategori+'&periode='+ data.periode );
            }
      }
  });
