angular.module('app')
    .controller('ReportServiceUnitbyModelGRController', function($scope, $http, CurrentUser, ReportServiceUnitbyModelGR ) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        // $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;

    var dateFormat='MM/yyyy';
    var dateFilter='date:"MM/yyyy"';
    var periode=new Date();
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat
    };
    ReportServiceUnitbyModelGR.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
      })
    ReportServiceUnitbyModelGR.getDataCabang().then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
    // if ($scope.roleId==1128 || $scope.roleId==1129) {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", kategori:'General Repair'  };
    // } else {
        $scope.filter = {Dealer:null, Cabang:null, kategori:'General Repair', MonthYear:periode  };
    // }
    $scope.getData = function(){
    }
    
    $scope.getReport = function(){
        var $tanggal=$scope.filter.MonthYear;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'kategori': $scope.filter.kategori,
            'periode': $filtertanggal};

        ReportServiceUnitbyModelGR.getExport(dataFilter);
    }

});
