angular.module('app')
  .factory('ReportSAProductivityGRFactory', function($http, CurrentUser,$q) {
    console.log('Service SAProductivity GR Loaded...');
    var currentUser = CurrentUser.user();
    // console.log(currentUser);
    var serverURL = ''; //http://localhost:8080'
    return {
            getDataDealer: function() {
                  var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
                  return res;},
            // getDataCabang: function() {
            //       var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            //       return res;},
            getDataCabang: function(data) {
                  var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId + '/' + data.kategori);
                  return res;},
            getDataCabangHONew: function(data) {
              console.log('Data Get Cabang:', data);
              var res=$http.get(serverURL + '/api/rpt/GetCabangHONew/'+ data.areaId + '/' +  data.DealerId + '/' + data.kategori);
              return res;},
            getDataAreaTAM: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaTAM/');
            return res;},
            getDataAreaDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaDealer/');
            return res;},
            getDataAreaDealerHO: function(data) {
            var res=$http.get(serverURL + '/api/rpt/AreaDealerHONew/'+ data.isGR );
            return res;},
            getDataSA: function() {
            var res=$http.get(serverURL + '/api/rpt/SA/General Repair/'+ currentUser.OrgId);
            return res;},

            getExportHOByUnit: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_SAProductivityUnit_GRHO?dealer='+ data.dealer +'&cabang='+ data.cabang +'&Jenis='+ data.jenis +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir +'&TamArea='+ data.tamarea +'&DealerArea='+ data.dealerarea +'&kategori='+ data.kategori);
            },
            getExportHOByAmount: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_SAProductivityAmount_GRHO?dealer='+ data.dealer +'&cabang='+ data.cabang +'&Jenis='+ data.jenis +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir +'&TamArea='+ data.tamarea +'&DealerArea='+ data.dealerarea +'&kategori='+ data.kategori);
            },
            getSummaryHOUnit: function(data) {
              console.log('Data Report:', data);
            // unit {dealer}/{outletcode}/{datestart}/{dateend}/{TAMArea}/{GroupDealerArea}/{kategori}/{Jenis}
              var res = $http.get(serverURL + '/api/rpt/AS_SAProductivityUnit_GRHO/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.tamarea +'/'+ data.dealerarea +'/'+ data.kategori +'/summary');
                return res;
            },
            getSummaryHOAmount: function(data) {
              console.log('Data Report:', data);
              // amount {dealer}/{outletcode}/{datestart}/{dateend}/{TAMArea}/{GroupDealerArea}/{kategori}/{Jenis}
              var res = $http.get(serverURL + '/api/rpt/AS_SAProductivityAmount_GRHO/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.tamarea +'/'+ data.dealerarea +'/'+ data.kategori +'/summary');
                return res;
            },
            getExportCabangByUnit: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_SAProductivityByUnit_GRCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir +'&sa='+ data.sa);
            },
            getExportCabangByAmount: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_SAProductivityByAmount_GRCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir +'&sa='+ data.sa);
            },
            getSummaryCabangUnit: function(data) {
              console.log('Data Report:', data);
            // unit {dealer}/{outletcode}/{datestart}/{dateend}/{TAMArea}/{GroupDealerArea}/{kategori}/{Jenis}
              var res = $http.get(serverURL + '/api/rpt/AS_SAProductivityByUnit_GRCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.sa);
                return res;
            },
            getSummaryCabangAmount: function(data) {
              console.log('Data Report:', data);
              // amount {dealer}/{outletcode}/{datestart}/{dateend}/{TAMArea}/{GroupDealerArea}/{kategori}/{Jenis}
              var res = $http.get(serverURL + '/api/rpt/AS_SAProductivityByAmount_GRCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.sa);
                return res;
            },
      }
  });
