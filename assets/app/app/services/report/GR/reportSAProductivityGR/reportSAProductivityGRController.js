angular.module('app')
    .controller('ReportSAProductivityGRController', function($scope, $stateParams,$http, CurrentUser, ReportSAProductivityGRFactory,bsNotify,$timeout  ) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        // $scope.$on('$viewContentLoaded', function() {
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
        });

        var tmpDate = new Date();
        $scope.maxDate = new Date(tmpDate.getFullYear(), tmpDate.getMonth(), tmpDate.getDate());
        $scope.user = CurrentUser.user();
        $scope.roleId = $scope.user.RoleId;
        $scope.role=$stateParams.role;
        console.log('role',$scope.role);
        $scope.formatDate = function(date) {
            var dateOut = new Date(date);
            return dateOut;
        };
        var dateFormat2 = 'dd/MM/yyyy';
        var dateFormat = 'MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var periode=new Date();
        $scope.dateOptions = {
            startingDay: 1,
            format: dateFormat2,

            // mode:"'month'",
        };
        $scope.dateOptions2 = {
            startingDay: 1,
            format: dateFormat2,

            // mode:"'month'",
        };
        $scope.dataDealer = [];
        $scope.dataCabang = [];
        $scope.dataAreaTAM = [];
        $scope.dataAreaDealer = [];
        $scope.filter = {Dealer:null,DealerId:null, Cabang:null, DateStart:periode, DateEnd:periode, tamarea:0,dealerarea:0,kategori:"General Repair",isGR:1};
        $scope.getData = function(){
            if ($scope.role == "Branch") $scope.getSummaryCabang();
            else $scope.getSummaryHO();
            console.log('get data');

        };

            ReportSAProductivityGRFactory.getDataDealer().then(function (response) {
                console.log('Response BEDealer:', response.data);
                $scope.dataDealer = response.data;
                $scope.filter.Dealer=response.data[0].dealerCode;
                $scope.filter.DealerId=response.data[0].dealerID;
                ReportSAProductivityGRFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
                  console.log('ayamtest',response.data)
                  $scope.dataGroupDealerHO = response.data
                  $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
                  $scope.areaId($scope.dataGroupDealerHO[0])
                })
              })
            ReportSAProductivityGRFactory.getDataCabang($scope.filter).then(function (response) {
                console.log('Response BECabang:', response.data);
                $scope.dataCabang = response.data;
                $scope.DisabledCabang();
                $scope.filter.Cabang=response.data[0].cabangID;
              })
            ReportSAProductivityGRFactory.getDataAreaTAM().then(function (response) {
                console.log('Response AreaTAM:', response.data);
                $scope.dataAreaTAM = response.data;
              })
            ReportSAProductivityGRFactory.getDataAreaDealer().then(function (response) {
                console.log('Response AreaDealer:', response.data);
                $scope.dataAreaDealer = response.data;
              })
            ReportSAProductivityGRFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
                console.log('Response AreaDealerHO:', response.data);
                $scope.dataAreaDealerHO = response.data;
                $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
                $scope.areaId($scope.dataAreaDealerHO[0])
              })
            ReportSAProductivityGRFactory.getDataSA().then(function (response) {
                console.log('Response SA:', response.data);
                $scope.dataSA = response.data;
              })
    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
            $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };

        // $scope.filter.dealerarea = null

        $scope.areaId = function(row) {
          // $scope.DataDealer = row
          $scope.DataDealer = Object.assign(row,{'kategori': $scope.filter.kategori})
          if($scope.userlogin != 'TAM'){
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
          }else{
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
          }
          console.log('row dealerid', row,$scope.DataDealer);
          ReportSAProductivityGRFactory.getDataCabangHONew($scope.DataDealer).then(function (response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabangHO = response.data;
            $scope.DisabledCabang();
            $scope.filter.CabangHO = null
            console.log('kambing',$scope.filter)
            $timeout(function() {
              $scope.filter.CabangHO = response.data[0].cabangID;
              console.log('kambing123',$scope.filter.CabangHO)
            }, 100);
          })
        };

        $scope.dealerCode = function(row){
          row = Object.assign(row,{'isGR':$scope.filter.isGR})
          $scope.filter.DealerId = row.dealerID
          ReportSAProductivityGRFactory.getDataAreaDealerHO(row).then(function (response) {
              console.log('ayam',response.data)
              console.log('rusa',$scope.dataGroupDealer)
              $scope.dataGroupDealer = response.data
              $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
              $scope.areaId($scope.dataGroupDealer[0])
              console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
          })
        }
        
        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };

function format1(n, currency) {
    return currency + " " + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}
        $scope.getSummaryHO = function(){
            var $tanggal=$scope.filter.DateStart;
            var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
            var $tanggalakhir=$scope.filter.DateEnd;
            var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
            var dataFilter = {
              'kategori': $scope.filter.kategori,
              'dealer': $scope.filter.Dealer,
              'cabang': $scope.filter.CabangHO,
              'periodeakhir': $filtertanggalakhir,
              'periodeawal': $filtertanggal,
              'tamarea': $scope.filter.tamarea,
              'dealerarea': $scope.filter.dealerarea,};

            ReportSAProductivityGRFactory.getSummaryHOAmount(dataFilter).then(function (response) {
              console.log('Response BE Filter:', response.data);
              // $scope.dataSummary = response.data;
              //$scope.jasa = response.data[0].jasa / 1000;
              $scope.jasa = response.data[0].jasa;
              $scope.jasa = Math.round($scope.jasa * 100) / 100;
              $scope.jasa = format1($scope.jasa,'');
              //$scope.parts = response.data[0].parts / 1000;
              $scope.parts = response.data[0].parts;
              $scope.parts = Math.round($scope.parts * 100) / 100;
              $scope.parts = format1($scope.parts,'');
              //$scope.bahan = response.data[0].bahan / 1000;
              $scope.bahan = response.data[0].bahan;
              $scope.bahan = Math.round($scope.bahan * 100) / 100;
              $scope.bahan = format1($scope.bahan,'');
              //$scope.opb = response.data[0].opb / 1000;
              $scope.opb = response.data[0].opb;
              $scope.opb = Math.round($scope.opb * 100) / 100;
              $scope.opb = format1($scope.opb,'');
              //$scope.opl = response.data[0].opl / 1000;
              $scope.opl = response.data[0].opl;
              $scope.opl = Math.round($scope.opl * 100) / 100;
              $scope.opl = format1($scope.opl,'');
              //$scope.totalrevenue = (response.data[0].jasa + response.data[0].parts + response.data[0].bahan + response.data[0].opb + response.data[0].opl) / 1000;
              //$scope.totalrevenue = (response.data[0].jasa + response.data[0].parts + response.data[0].bahan + response.data[0].opb + response.data[0].opl);
              $scope.totalrevenue = response.data[0].totalrevenue;
              $scope.totalrevenue = Math.round($scope.totalrevenue * 100) / 100;
              $scope.totalrevenue = format1($scope.totalrevenue,'');
              // console.log('Format Jasa',new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format($scope.jasa));
              console.log('Format Jasa',format1($scope.jasa,''));
            });

            $scope.OTDRate=0;
            $scope.unitSADAY=0;
            $scope.deliveryRate=0;
            ReportSAProductivityGRFactory.getSummaryHOUnit(dataFilter).then(function (response) {
              console.log('Response BE Filter:', response.data);
              // $scope.dataSummary = response.data;
              $scope.unitEntry = response.data[0].unitEntry;
              $scope.unitEntry = Math.round($scope.unitEntry * 100) / 100
              $scope.totalSA = response.data[0].totalSA;
              $scope.totalSA = Math.round($scope.totalSA * 100) / 100;
              if (response.data[0].totalSA>0 && response.data[0].hari>0)
                //$scope.unitSADAY = (response.data[0].unitEntry/response.data[0].totalSA)/response.data[0].hari;
                $scope.unitSADAY = response.data[0].unitSADAY;
              $scope.unitSADAY = Math.round($scope.unitSADAY * 100) / 100;
              // if (response.data[0].unitEntry>0)
              // {
              //   $scope.OTDRate = response.data[0].otd/response.data[0].unitEntry;
              //   //$scope.deliveryRate = response.data[0].delivery/response.data[0].unitEntry;
              //   $scope.deliveryRate = response.data[0].deliveryRate;
              // }
              $scope.OTDRate = response.data[0].otdRate;
              $scope.OTDRate = Math.round($scope.OTDRate * 100) / 100;
              $scope.deliveryRate = response.data[0].deliveryRate;
              $scope.deliveryRate = Math.round($scope.deliveryRate * 100) / 100;
            });
          }

      $scope.getReportHOByUnit = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
          var $tanggalakhir=$scope.filter.DateEnd;
          var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
          var dataReport = {
            'kategori': $scope.filter.kategori,
            'jenis': "export",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'dealerarea': $scope.filter.dealerarea,};
            

        ReportSAProductivityGRFactory.getExportHOByUnit(dataReport);
    }

    $scope.getReportHOByAmount = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
          var $tanggalakhir=$scope.filter.DateEnd;
          var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
          var dataReport = {
            'kategori': $scope.filter.kategori,
            'jenis': "export",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'dealerarea': $scope.filter.dealerarea,};

        ReportSAProductivityGRFactory.getExportHOByAmount(dataReport);
    }
// =========================================================================  CABANG ===============================
     $scope.getSummaryCabang = function(){
      if($scope.filter.SA == undefined  ){
        bsNotify.show({
            size: 'big',
            type: 'danger',
            title: "Terdapat Data Kosong Harus Diisi",
        });
      }else{
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
          var $tanggalakhir=$scope.filter.DateEnd;
          var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
          var dataFilter = {
              'kategori': $scope.filter.kategori,
              'dealer': $scope.filter.Dealer,
              'cabang': $scope.filter.Cabang,
              'periodeakhir': $filtertanggalakhir,
              'periodeawal': $filtertanggal,
              'sa': $scope.filter.SA,};

            ReportSAProductivityGRFactory.getSummaryCabangAmount(dataFilter).then(function (response) {
              console.log('Response BE Filter:', response.data);
              // $scope.dataSummary = response.data;
              $scope.jasa = response.data[0].saJasa;
              $scope.jasa = Math.round($scope.jasa * 100) / 100
              $scope.jasa = format1($scope.jasa,'');
              $scope.parts = response.data[0].saPart;
              $scope.parts = Math.round($scope.parts * 100) / 100
              $scope.parts = format1($scope.parts,'');
              $scope.bahan = response.data[0].saBahan;
              $scope.bahan = Math.round($scope.bahan * 100) / 100
              $scope.bahan = format1($scope.bahan,'');
              $scope.opb = response.data[0].saopb;
              $scope.opb = Math.round($scope.opb * 100) / 100
              $scope.opb = format1($scope.opb,'');
              $scope.opl = response.data[0].saopl;
              $scope.opl = Math.round($scope.opl * 100) / 100
              $scope.opl = format1($scope.opl,'');
              //$scope.totalrevenue = response.data[0].saJasa + response.data[0].saPart + response.data[0].saBahan + response.data[0].saopb + response.data[0].saopl;
              $scope.totalrevenue = response.data[0].totalrevenue;
              $scope.totalrevenue = Math.round($scope.totalrevenue * 100) / 100;
              $scope.totalrevenue = format1($scope.totalrevenue,'');
            })
            ReportSAProductivityGRFactory.getSummaryCabangUnit(dataFilter).then(function (response) {
              console.log('Response BE Filter:', response.data);
              // $scope.dataSummary = response.data;
              $scope.unitEntry = response.data[0].unitEntry;
              $scope.unitEntry = Math.round($scope.unitEntry * 100) / 100;
              $scope.totalSA = response.data[0].unitEntryTotalSA;
              $scope.totalSA = Math.round($scope.totalSA * 100) / 100;
              $scope.unitSADAY = response.data[0].unitEntryUnitSADay;
              $scope.unitSADAY = Math.round($scope.unitSADAY * 100) / 100;
              $scope.OTDRate = response.data[0].unitEntryOTDRate;
              $scope.OTDRate = Math.round($scope.OTDRate * 100) / 100;
              $scope.deliveryRate = response.data[0].unitEntryDeliveryRate;
              $scope.deliveryRate = Math.round($scope.deliveryRate * 100) / 100;

            })
          }
      } 

      $scope.getReportCabangByUnit = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
          var $tanggalakhir=$scope.filter.DateEnd;
          var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
          var dataReport = {
            'kategori': $scope.filter.kategori,
            'jenis': "export",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'sa': $scope.filter.SA,};
        
        if($scope.filter.SA == undefined  ){
            bsNotify.show({
            size: 'big',
            type: 'danger',
            title: "Terdapat Data Kosong Harus Diisi",
            });
        }else{    
          ReportSAProductivityGRFactory.getExportCabangByUnit(dataReport);
        }
    }

    $scope.getReportCabangByAmount = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
          var $tanggalakhir=$scope.filter.DateEnd;
          var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
          var dataReport = {
            'kategori': $scope.filter.kategori,
            'jenis': "export",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'sa': $scope.filter.SA,};

        if($scope.filter.SA == undefined  ){
            bsNotify.show({
            size: 'big',
            type: 'danger',
            title: "Terdapat Data Kosong Harus Diisi",
            });
        }else{    
          ReportSAProductivityGRFactory.getExportCabangByAmount(dataReport);
        }
    }


    });
