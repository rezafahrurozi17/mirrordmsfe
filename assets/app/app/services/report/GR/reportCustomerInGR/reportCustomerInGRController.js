angular.module('app')
    .controller('ReportCustomerInGRController', function($scope,$stateParams, $http, CurrentUser, ReportCustomerInGRFactory, $timeout ) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();

    console.log("$scope.user : ",$scope.user);
    $scope.roleId = $scope.user.RoleId;
    // if ($scope.roleId==1128 || $scope.roleId==1129) {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", DateStart:null, DateEnd:null, tamarea:43,dealerarea:43,kategori:'General Repair'  };
    // } else {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", DateStart:null, DateEnd:null, tamarea:43,dealerarea:43,kategori:'General Repair'  };
    // }

    $scope.dataDealer = [];
    $scope.dataCabang = [];
    $scope.dataAreaDealer = [];
    $scope.dataAreaCabang = [];
    $scope.role=$stateParams.role;
    console.log('role',$scope.role);
    $scope.filter = {kategori:'General Repair'  };
    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    var dateFormat2='MM/yyyy';
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.SdateOptions = {
        startingDay: 1,
        format: dateFormat,
    };
    $scope.EdateOptions = {
        startingDay: 1,
        format: dateFormat,
    };
    $scope.dateOptions2 = {
        startingDay: 1,
        format: dateFormat,
    };


//  =========================== Validasi Date Start ==========================================
    var today = new Date();
    $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(),today.getDate());
    $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(),today.getDate());
    $scope.minDateE = new Date(today.getFullYear(),today.getMonth(),today.getDate());


    $scope.changeStartDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.DateStart);
        var dayTwo = new Date($scope.filter.DateEnd);

        $scope.EdateOptions.minDate = $scope.filter.DateStart;

        if (tgl == null || tgl == undefined){
            $scope.filter.DateEnd = null;
        } else {
            if ($scope.filter.DateStart < $scope.filter.DateEnd){
                if((dayTwo.getMonth() - dayOne.getMonth()) > 2 || dayOne.getFullYear() != dayTwo.getFullYear()){
                $scope.filter.DateEnd = $scope.filter.DateStart;
                $scope.EdateOptions.maxDate = new Date(dayOne.getFullYear(),dayOne.getMonth()+3,dayOne.getDate());
                if ($scope.EdateOptions.maxDate > today) {
                    $scope.EdateOptions.maxDate = new Date(today.getFullYear(),today.getMonth(),today.getDate());
                }
                }

            } else {
                if (dayOne > today){
                    $scope.filter.DateStart = today;
                    $scope.filter.DateEnd = $scope.filter.DateStart;
                } else {
                $scope.filter.DateEnd = $scope.filter.DateStart;
                $scope.EdateOptions.maxDate = new Date(dayOne.getFullYear(),dayOne.getMonth()+3,dayOne.getDate());
                if ($scope.EdateOptions.maxDate > today) {
                    $scope.EdateOptions.maxDate = new Date(today.getFullYear(),today.getMonth(),today.getDate());
                }
            }
            }
        }
    }

    $scope.changeEndDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.DateStart);
        var dayTwo = new Date($scope.filter.DateEnd);

        if (dayTwo > today){
            $scope.EdateOptions.minDate = $scope.filter.DateStart;
            $scope.filter.DateEnd = today;
        }
    }
//  =========================== Validasi Date End ==========================================





    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
        $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };

    // //----------------------------------
    // // Get Data
    // //----------------------------------
    $scope.filter = {Dealer:null, DealerId:null, Cabang:null, DateStart:periode, DateEnd:periode, tamarea:0,dealerarea:0,kategori:"General Repair",isGR:1};
    ReportCustomerInGRFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
        $scope.filter.DealerId=response.data[0].dealerID;
        ReportCustomerInGRFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
          console.log('ayamtest',response.data)
          $scope.dataGroupDealerHO = response.data
          $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
          $scope.areaId($scope.dataGroupDealerHO[0])
          // console.log('sapitest',row,$scope.filter,$scope.dataGroupDealerHO[0])
        })
      })
    ReportCustomerInGRFactory.getDataCabang($scope.filter).then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
    ReportCustomerInGRFactory.getDataAreaTAM().then(function (response) {
        console.log('Response AreaTAM:', response.data);
        $scope.dataAreaTAM = response.data;
      })
    ReportCustomerInGRFactory.getDataAreaDealer().then(function (response) {
        console.log('Response AreaDealer:', response.data);
        $scope.dataAreaDealer = response.data;
      })
    ReportCustomerInGRFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
        console.log('Response AreaDealerHO:', response.data);
        $scope.dataAreaDealerHO = response.data;
        $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
        $scope.areaId($scope.dataAreaDealerHO[0])
      })
    // $scope.DisabledCabang = function() {
    //         if ($scope.dataCabang.length>1){
    //             $scope.Disabled =false;
    //             console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
    //         }
    //         else{
    //             $scope.Disabled =true;
    //             console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
    //         }
    //     };
    $scope.getData = function(){
            if ($scope.role == "Branch") $scope.getSummaryCabang();
            else $scope.getSummaryHO();
            console.log('get data');
    }

    $scope.areaId = function(row) {
        // $scope.DataDealer = row
      $scope.DataDealer = Object.assign(row,{'kategori':'General Repair'})
      if($scope.userlogin != 'TAM'){
        $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
      }
      console.log('row', row,$scope.DataDealer);
      ReportCustomerInGRFactory.getDataCabangHONew($scope.DataDealer).then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabangHO = response.data;
        $scope.DisabledCabang();
        $scope.filter.CabangHO = null
        console.log('kambing',$scope.filter)
        $timeout(function() {
          $scope.filter.CabangHO = response.data[0].cabangID;
          console.log('kambing123',$scope.filter.CabangHO)
        }, 100);
      })
    };

    $scope.dealerCode = function(row){
        row = Object.assign(row,{'isGR':$scope.filter.isGR})
        ReportCustomerInGRFactory.getDataAreaDealerHO(row).then(function (response) {
            console.log('ayam',response.data)
            console.log('rusa',$scope.dataGroupDealer)
            $scope.dataGroupDealer = response.data
            $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
            $scope.areaId($scope.dataGroupDealer[0])
            console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
        })
    }

    $scope.DisabledCabang = function() {
      if ($scope.dataCabang.length>1){
        $scope.Disabled =false;
        console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
      }else{
        $scope.Disabled =true;
        console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
      }
    };

    $scope.getSummaryHO = function(){
        var $tanggal=$scope.filter.DateStart;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
        var dataFilter = {
            'kategori': $scope.filter.kategori,
            'jenis': "summary",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'dealerarea': $scope.filter.dealerarea,
        };

        ReportCustomerInGRFactory.getSummaryHO(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;
            $scope.CustomerReleaseWO = response.data[0].allWo;
            $scope.CustomerReleaseWO = Math.round($scope.CustomerReleaseWO * 100) / 100
            $scope.CustomerIn = response.data[0].customerIn;
            $scope.CustomerIn = Math.round($scope.CustomerIn * 100) / 100
            $scope.CustomerCancel = response.data[0].customerCancel;
            $scope.CustomerCancel = Math.round($scope.CustomerCancel * 100) / 100
            $scope.OnTime = response.data[0].onTime;
            $scope.OnTime = Math.round($scope.OnTime * 100) / 100
            $scope.CustomerInFromBooking = response.data[0].customerInFromBooking;
            $scope.CustomerInFromBooking = Math.round($scope.CustomerInFromBooking * 100) / 100
            $scope.PotentialLoss = response.data[0].customerIn > 0 ? (response.data[0].customerCancel/response.data[0].customerIn)*100 : 0;
            $scope.PotentialLoss = Math.round($scope.PotentialLoss * 100) / 100
            $scope.BookingOnRate = response.data[0].customerInFromBooking > 0 ? (response.data[0].onTime/response.data[0].customerInFromBooking)*100 : 0;
            $scope.BookingOnRate = Math.round($scope.BookingOnRate * 100) / 100
        })
    }

    $scope.getReportHO = function(){
        var $tanggal=$scope.filter.DateStart;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
        var dataReport = {
            'kategori': $scope.filter.kategori,
            'jenis': "export",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'dealerarea': $scope.filter.dealerarea,
        };

        ReportCustomerInGRFactory.getExportHO(dataReport);
    }

    $scope.getSummaryCabang = function(){
        var $tanggal=$scope.filter.DateStart;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
        var dataFilter = {
            'kategori': $scope.filter.kategori,
            'jenis': "summary",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal
        };

        ReportCustomerInGRFactory.getSummaryCabang(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;
            $scope.CustomerReleaseWO = response.data[0].allWo;
            $scope.CustomerReleaseWO = Math.round($scope.CustomerReleaseWO * 100) / 100
            $scope.CustomerIn = response.data[0].customerIn;
            $scope.CustomerIn = Math.round($scope.CustomerIn * 100) / 100
            $scope.CustomerCancel = response.data[0].cancel;
            $scope.CustomerCancel = Math.round($scope.CustomerCancel * 100) / 100
            $scope.OnTime = response.data[0].onTime;
            $scope.OnTime = Math.round($scope.OnTime * 100) / 100
            $scope.CustomerInFromBooking = response.data[0].customerBooking;
            $scope.CustomerInFromBooking = Math.round($scope.CustomerInFromBooking * 100) / 100
            $scope.PotentialLoss = response.data[0].customerIn > 0 ? (response.data[0].cancel/response.data[0].allWo)*100 : 0;
            $scope.PotentialLoss = Math.round($scope.PotentialLoss * 100) / 100
            $scope.BookingOnRate = response.data[0].customerBooking > 0 ? (response.data[0].onTime/response.data[0].customerBooking)*100 : 0;
            $scope.BookingOnRate = Math.round($scope.BookingOnRate * 100) / 100
        })
    }

    $scope.getReportCabang = function(){
        var $tanggal=$scope.filter.DateStart;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
        var dataReport = {
            'kategori': $scope.filter.kategori,
            'jenis': "export",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'dealerarea': $scope.filter.dealerarea,
        };

        ReportCustomerInGRFactory.getExportCabang(dataReport);
    }


});
