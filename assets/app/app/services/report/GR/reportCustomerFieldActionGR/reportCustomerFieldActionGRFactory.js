angular.module('app')
  .factory('ReportCustomerFieldActionGRFactory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    var serverURL = ''; //http://localhost:8080';
    return {
      getDataDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},
      getDataCabang: function() {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            return res;},
      getSummaryFilter: function(data) {
              console.log('Data Filter:', data);
              // console.log('periode',moment(data.periode).format('YYYY-MM-DD'))
              var res = $http.get(serverURL + '/api/rpt/AS_CustomerFieldAction_GRCAB/'+ data.dealer +'/'+ data.cabang  +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/'+ data.kategori_workshop +'/'+ data.statusfa  + '/'+ data.jenisfa  );
              
              return res;},
      getSummaryExport: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_CustomerFieldAction_GRCAB?dealer='+ data.dealer +'&cabang='+ data.cabang  +'&kat_workshop='+ data.kategori_workshop +'&periode_awal='+ data.periode_awal +'&periode_akhir='+ data.periode_akhir +'&statusfa='+ data.statusfa +'&jenisfa='+ data.jenisfa);
              // var res = $http.get(serverURL + 'CCustomerCompleteness?area='+ data.area +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periode +'&jenis_pelanggan='+ data.jenispel);
              // return res;
            }

    }
  });
