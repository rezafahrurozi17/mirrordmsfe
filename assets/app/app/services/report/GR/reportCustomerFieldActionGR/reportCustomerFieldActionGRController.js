angular.module('app')
    .controller('ReportCustomerFieldActionGRController', function($scope, $http, CurrentUser, ReportCustomerFieldActionGRFactory ) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        // $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();

    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
        
        // mode:"'month'",
        // disableWeekend: 1
    };
    $scope.dataDealer = [];
    $scope.dataCabang = [];
    $scope.roleId = $scope.user.RoleId;

    // if ($scope.roleId==1128 || $scope.roleId==1129) {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", kategori:"General Repair", DateStart:null, DateEnd:null, JenisFA:'All',StatusFA:'All'};
    // } else {
        $scope.filter = {Dealer:null, Cabang:null, kategori:"General Repair", DateStart:periode, DateEnd:periode, JenisFA:'All',StatusFA:'All'};
    // }    
    ReportCustomerFieldActionGRFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.getDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
    })
    ReportCustomerFieldActionGRFactory.getDataCabang().then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.getCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
    })



    // $scope.roleId = 1; // 2 = HO | 1 = Branch
    $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.getCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.getCabang.length + ' ' + $scope.Disabled);
            }
        };
    $scope.getData = function() {
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'kategori_workshop': $scope.filter.kategori,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,                        
            'statusfa': $scope.filter.StatusFA,
            'jenisfa': $scope.filter.JenisFA
        };

        ReportCustomerFieldActionGRFactory.getSummaryFilter(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;
            var hasil=response.data[0];
            $scope.totalUnitRepair = hasil.totalUnitRepair;
        })
    }
    $scope.getReport = function(){
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'kategori_workshop': $scope.filter.kategori,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,                        
            'statusfa': $scope.filter.StatusFA,
            'jenisfa': $scope.filter.JenisFA};

        ReportCustomerFieldActionGRFactory.getSummaryExport(dataFilter);
    }

    $scope.getJenisFA = [
        {JenisFAId:"All",JenisFAName:"All"},
        // { JenisFAId: "SCC", JenisFAName: "SCC" },
        {JenisFAId:"SSC",JenisFAName:"SSC"},
        {JenisFAId:"ICC",JenisFAName:"ICC"}
    ];

    $scope.getStatusFA = [
        {StatusFAId:"All",StatusFAName:"All"},
        {StatusFAId:"Finish",StatusFAName:"Finish"},
        {StatusFAId:"Not Finish",StatusFAName:"Not Finish"}
    ];

    
});
