angular.module('app')
    .controller('ReportAppointmentGRController', function($scope,$stateParams, $http, CurrentUser, ReportAppointmentGRFactory, $timeout ) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        // $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();

    // $scope.periodeawal = '2017-01-01';
    // $scope.periodeakhir = '2017-11-20';
    var currentUser = CurrentUser.user();
    console.log("$scope.user : ",$scope.user);
    $scope.roleId = $scope.user.RoleId;
    // if ($scope.roleId==1128 || $scope.roleId==1129) {
    //     $scope.filter = {Dealer:null, Cabang:null, KategoriWorkshop:"Seminar", DateStart:null, DateThru:null, Satellite:null};
    // } else {
        // $scope.filter = {Dealer:null, Cabang:null, KategoriWorkshop:"General Repair", DateStart:periode, DateEnd:periode, Satellite:null};
    // }
    $scope.role=$stateParams.role;
    console.log('role',$scope.role);
    $scope.dataDealer = [];
    $scope.dataCabang = [];
    $scope.dataAreaDealer = [];
    $scope.dataAreaCabang = [];

    $scope.formatDate = function(date){
        var dateOut = new Date(date);
        return dateOut;
    };
    var dateFormat2='dd/MM/yyyy';
    var dateFormat='MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,

        // mode:"'month'",
    };
        $scope.dateOptions2 = {
        startingDay: 1,
        format: dateFormat2,

        // mode:"'month'",
    };
    $scope.filter = {Dealer:null, DealerId:null, Cabang:null, kategori:"General Repair", DateStart:periode, DateEnd:periode, Satellite:null,tamarea:0,dealerarea:0,isGR:1};
    // //----------------------------------
    // // Get Data
    // //----------------------------------
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
    ReportAppointmentGRFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
        $scope.filter.DealerId=response.data[0].dealerID;
        ReportAppointmentGRFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
          console.log('ayamtest',response.data)
          $scope.dataGroupDealerHO = response.data
          $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
          $scope.areaId($scope.dataGroupDealerHO[0])
          // console.log('sapitest',row,$scope.filter,$scope.dataGroupDealerHO[0])
        })
      })
    ReportAppointmentGRFactory.getDataCabang($scope.filter).then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
    ReportAppointmentGRFactory.getDataAreaDealer().then(function (response) {
        console.log('Response AreaDealer:', response.data);
        $scope.dataAreaDealer = response.data;
      })
    ReportAppointmentGRFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
        console.log('Response AreaDealerHO:', response.data);
        $scope.dataAreaDealerHO = response.data;
        $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
        $scope.areaId($scope.dataAreaDealerHO[0])
      })
    ReportAppointmentGRFactory.getDataAreaTAM().then(function (response) {
        console.log('Response AreaTAM:', response.data);
        $scope.dataAreaTAM = response.data;
      })
        // $scope.DisabledCabang = function() {
        //     if ($scope.dataCabang.length>1){
        //         $scope.Disabled =false;
        //         console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
        //     }
        //     else{
        //         $scope.Disabled =true;
        //         console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
        //     }
        // };
        $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };

    $scope.getData = function(){
            if ($scope.role == "Branch") $scope.getSummaryCabang();
            else $scope.getSummaryHO();
            console.log('get data');
    }

    // $scope.filter.dealerarea = null

    $scope.areaId = function(row) {
        // $scope.DataDealer = row
      $scope.DataDealer = Object.assign(row,{'kategori':'General Repair'})
      if($scope.userlogin != 'TAM'){
        $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
      }
      console.log('row', row,$scope.DataDealer,$scope.filter);
      ReportAppointmentGRFactory.getDataCabangHONew($scope.DataDealer).then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabangHO = response.data;
        $scope.DisabledCabang();
        $scope.filter.CabangHO = null
        console.log('kambing',$scope.filter)
        $timeout(function() {
          $scope.filter.CabangHO = response.data[0].cabangID;
          console.log('kambing123',$scope.filter.CabangHO)
        }, 100);
      })
    };

    $scope.dealerCode = function(row){
      row = Object.assign(row,{'isGR':$scope.filter.isGR})
      ReportAppointmentGRFactory.getDataAreaDealerHO(row).then(function (response) {
        console.log('ayam',response.data)
        console.log('rusa',$scope.dataGroupDealer)
        $scope.dataGroupDealer = response.data
        $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
        $scope.areaId($scope.dataGroupDealer[0])
        console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
      })
    }

    $scope.DisabledCabang = function() {
      if ($scope.dataCabang.length>1){
        $scope.Disabled =false;
        console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
      }else{
        $scope.Disabled =true;
        console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
      }
    };

    $scope.getSummaryHO = function(){
        var $tanggal=$scope.filter.DateStart;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
        var dataFilter = {
            'kategori': $scope.filter.kategori,
            'jenis': "summary",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'dealerarea': $scope.filter.dealerarea,};
          ReportAppointmentGRFactory.getSummaryHO(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;
            $scope.CustomerReschedule = response.data[0].customerReschedule;
            $scope.CustomerReschedule = Math.round($scope.CustomerReschedule * 100) / 100
            $scope.CustomerAppointmentShow = response.data[0].customerAppointmentShow;
            $scope.CustomerAppointmentShow = Math.round($scope.CustomerAppointmentShow * 100) / 100
            $scope.TotalUnitEntry = response.data[0].totalUnitEntry;
            $scope.TotalUnitEntry = Math.round($scope.TotalUnitEntry * 100) / 100
            $scope.CustomerAppointment = response.data[0].customerAppointment;
            $scope.CustomerAppointment = Math.round($scope.CustomerAppointment * 100) / 100
            $scope.CustomerAppointmentResch = response.data[0].custAppReschedule;
            // $scope.CustomerAppointmentResch =  Math.round($scope.CustomerReschedule/$scope.CustomerAppointmentResch);
            $scope.CustomerCancel = response.data[0].customerCancel;
            $scope.CustomerCancel = Math.round($scope.CustomerCancel * 100) / 100
            // $scope.AppointmentRate = response.data[0].totalUnitEntry>0 ? (response.data[0].customerAppointmentShow/response.data[0].totalUnitEntry)*100 : 0;
            $scope.AppointmentRate = Math.round(response.data[0].appointmentRate * 100) / 100
            // $scope.ShowRate = response.data[0].customerAppointment>0 ? (response.data[0].customerAppointmentShow/response.data[0].customerAppointment)*100 : 0;
            $scope.ShowRate = Math.round(response.data[0].showRate * 100) / 100
            // $scope.NoShowRate = response.data[0].customerAppointment>0 ? (response.data[0].customerCancel/response.data[0].customerAppointment)*100 : 0;
            $scope.NoShowRate = Math.round(response.data[0].noShowRate * 100) / 100
            // $scope.RescheduleRate = response.data[0].customerAppointment>0 ? (response.data[0].customerReschedule/response.data[0].customerAppointment)*100 : 0;
            // $scope.RescheduleRate = response.data[0].custAppReschedule>0 ? (response.data[0].customerReschedule/response.data[0].custAppReschedule)*100 : 0;
            $scope.RescheduleRate = Math.round(response.data[0].rescheduleRate * 100) / 100
          })
      }

      $scope.getReportHO = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
          var $tanggalakhir=$scope.filter.DateEnd;
          var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
          var dataReport = {
            'kategori': $scope.filter.kategori,
            'jenis': "export",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'dealerarea': $scope.filter.dealerarea,};

        ReportAppointmentGRFactory.getExportHO(dataReport);
    }
    $scope.getSummaryCabang = function(){
        var $tanggal=$scope.filter.DateStart;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
        var dataFilter = {
            'kategori': $scope.filter.kategori,
            'jenis': "summary",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'PIC':"1",
            'dealerarea': $scope.filter.dealerarea,};
          ReportAppointmentGRFactory.getSummaryCabang(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;
            $scope.CustomerReschedule = response.data[0].customerReschedule;
            $scope.CustomerReschedule = Math.round($scope.CustomerReschedule * 100) / 100
            $scope.CustomerAppointmentShow = response.data[0].customerAppointmentShow;
            $scope.CustomerAppointmentShow = Math.round($scope.CustomerAppointmentShow * 100) / 100
            $scope.TotalUnitEntry = response.data[0].totalUnitEntry;
            $scope.TotalUnitEntry = Math.round($scope.TotalUnitEntry * 100) / 100
            $scope.CustomerAppointment = response.data[0].customerAppointment;
            $scope.CustomerAppointment = Math.round($scope.CustomerAppointment * 100) / 100
            $scope.CustomerAppointmentResch = response.data[0].custAppReschedule;
            $scope.CustomerAppointmentResch =  Math.round($scope.CustomerAppointmentResch * 100) / 100
            $scope.CustomerCancel = response.data[0].customerCancel;
            $scope.CustomerCancel = Math.round($scope.CustomerCancel * 100) / 100
            $scope.AppointmentRate = response.data[0].totalUnitEntry>0 ? (response.data[0].customerAppointmentShow/response.data[0].totalUnitEntry)*100 : 0;
            $scope.AppointmentRate = Math.round($scope.AppointmentRate * 100) / 100
            $scope.ShowRate = response.data[0].customerAppointment>0 ? (response.data[0].customerAppointmentShow/response.data[0].customerAppointment)*100 : 0;
            $scope.ShowRate = Math.round($scope.ShowRate * 100) / 100
            $scope.NoShowRate = response.data[0].customerAppointment>0 ? (response.data[0].customerCancel/response.data[0].customerAppointment)*100 : 0;
            $scope.NoShowRate = Math.round($scope.NoShowRate * 100) / 100
            $scope.RescheduleRate = response.data[0].custAppReschedule>0 ? (response.data[0].customerReschedule/response.data[0].custAppReschedule)*100 : 0;
            $scope.RescheduleRate = Math.round($scope.RescheduleRate * 100) / 100
          })
      }

      $scope.getReportCabang = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
          var $tanggalakhir=$scope.filter.DateEnd;
          var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
          var dataReport = {
            'kategori': $scope.filter.kategori,
            'jenis': "summary",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'PIC':"1",
            'dealerarea': $scope.filter.dealerarea,};

        ReportAppointmentGRFactory.getExportCabang(dataReport);
    }

    //  $scope.getPic = [
    // {PicId:1,PicName:"Naufal"},
    // {PicId:2,PicName:"Daniel"}
    // ];


});
