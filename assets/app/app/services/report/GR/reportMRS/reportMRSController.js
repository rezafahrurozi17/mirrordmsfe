angular.module('app')
    .controller('ReportMRSGRController', function($scope, $stateParams,$http, CurrentUser, ReportMRSFactory, $timeout ) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        // $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();

    $scope.periodeawal = '2017-01-01';
    $scope.periodeakhir = '2017-11-20';
    var periode=new Date();
    console.log("$scope.user : ",$scope.user);
    $scope.roleId = $scope.user.RoleId;
    $scope.filter = {Dealer:null,DealerId:null, Cabang:null, periode:periode,DateStart:periode, DateEnd:periode, tamarea:0,dealerarea:0,kategori:"General Repair",isGR:1};
    // if ($scope.roleId==1128 || $scope.roleId==1129) {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", kategori:"General Repair", DateStart:null, DateEnd:null, tamarea:43, dealerarea:43 };
    // } else {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", kategori:"General Repair", DateStart:null, DateEnd:null, tamarea:43, dealerarea:43 };
    // }
    $scope.role=$stateParams.role;
    console.log('role',$scope.role);
    $scope.dataDealer = [];
    $scope.dataCabang = [];
    $scope.dataAreaDealer = [];
    $scope.dataAreaCabang = [];

    $scope.formatDate = function(date){
        var dateOut = new Date(date);
        return dateOut;
    };
    var dateFormat='MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var dateFormat2='dd/MM/yyyy';
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,

        // mode:"'month'",
    };
        $scope.dateOptions2 = {
        startingDay: 1,
        format: dateFormat2,

        // mode:"'month'",
    };

      ReportMRSFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
        $scope.filter.DealerId=response.data[0].dealerID;
        ReportMRSFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
          console.log('ayamtest',response.data)
          $scope.dataGroupDealerHO = response.data
          $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
          $scope.areaId($scope.dataGroupDealerHO[0])
        })
      })
      ReportMRSFactory.getDataCabang($scope.filter).then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
      ReportMRSFactory.getDataAreaDealer().then(function (response) {
        console.log('Response AreaDealer:', response.data);
        $scope.dataAreaDealer = response.data;
      })
      ReportMRSFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
        console.log('Response AreaDealerHO:', response.data);
        $scope.dataAreaDealerHO = response.data;
        $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
        $scope.areaId($scope.dataAreaDealerHO[0])
        console.log('kuda',$scope.filter,$scope.dataAreaDealerHO[0])
      })
      
      ReportMRSFactory.getDataAreaTAM().then(function (response) {
        console.log('Response AreaTAM:', response.data);
        $scope.dataAreaTAM = response.data;
      })
    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
        $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };
    $scope.getData = function(){
            if ($scope.role == "Branch") $scope.getSummaryCabang();
            else $scope.getSummaryHO();
            console.log('get data');
    }
    $scope.getReport = function(){
            if ($scope.role == "Branch") $scope.getExportCabang();
            else $scope.getExportHO();
            console.log('get data');
    }

    // $scope.filter.dealerarea = null
    // console.log("asu123",$scope.filter.dealerarea,$scope.dataAreaDealerHO)

    $scope.areaId = function(row) {
      // $scope.DataDealer = row
      console.log('row ', row,$scope.DataDealer);
      $scope.DataDealer = Object.assign(row,{'kategori':'General Repair'})
      if($scope.userlogin != 'TAM'){
        $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
      }
      console.log('row1', row,$scope.DataDealer,$scope.filter);
      ReportMRSFactory.getDataCabangHONew($scope.DataDealer).then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabangHO = response.data;
        $scope.DisabledCabang();
        $scope.filter.CabangHO = null
        console.log('kambing',$scope.filter)
        $timeout(function() {
          $scope.filter.CabangHO = response.data[0].cabangID;
          console.log('kambing123',$scope.filter.CabangHO)
        }, 100);
        
      })
    };

    $scope.dealerCode = function(row){
      // var row = {}
      row = Object.assign(row,{'isGR':$scope.filter.isGR})
      ReportMRSFactory.getDataAreaDealerHO(row).then(function (response) {
        console.log('ayam',response.data)
        console.log('rusa',$scope.dataGroupDealer)
        $scope.dataGroupDealer = response.data
        $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
        $scope.areaId($scope.dataGroupDealer[0])
        console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
      })
    }
  
    $scope.DisabledCabang = function() {
      if ($scope.dataCabang.length>1){
          $scope.Disabled =false;
          console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
      }else{
          $scope.Disabled =true;
          console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
      }
    };



    // $scope.cabangID = function(row){

    // }

    $scope.getSummaryHO = function(){
        var $tanggal=$scope.filter.DateStart;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
        var dataFilter = {
            'kategori': $scope.filter.kategori,
            'jenis': "summary",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'dealerarea': $scope.filter.dealerarea,};

          ReportMRSFactory.getSummaryHO(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;
            if (response.data[0] != undefined) {
                $scope.shouldInvited = response.data[0].shouldInvited;
                $scope.shouldInvited = Math.round($scope.shouldInvited * 100) / 100
                $scope.called = response.data[0].called;
                $scope.called = Math.round($scope.called * 100) / 100
                $scope.terhubung = response.data[0].terhubung;
                $scope.terhubung = Math.round($scope.terhubung * 100) / 100
                $scope.yangDiSMS = response.data[0].yangDiSMS;
                $scope.yangDiSMS = Math.round($scope.yangDiSMS * 100) / 100
                $scope.comeAfterInvited = response.data[0].comeAfterInvited;
                $scope.comeAfterInvited = Math.round($scope.comeAfterInvited * 100) / 100
                $scope.kontribusiMRSComeAfterInvited = response.data[0].kontribusiMRSComeAfterInvited;
                $scope.kontribusiMRSComeAfterInvited = Math.round($scope.kontribusiMRSComeAfterInvited * 100) / 100
                $scope.kontribusiMRSThdSBE = response.data[0].kontribusiMRSThdSBE;
                $scope.kontribusiMRSThdSBE = Math.round($scope.kontribusiMRSThdSBE * 100) / 100
                $scope.databaseValid = response.data[0].databaseValid;
                $scope.databaseValid = Math.round($scope.databaseValid * 100) / 100
                $scope.bookingOnCall = response.data[0].bookingOnCall;
                $scope.bookingOnCall = Math.round($scope.bookingOnCall * 100) / 100
                $scope.bookingOnCallShow = response.data[0].bookingOnCallShow;
                $scope.bookingOnCallShow = Math.round($scope.bookingOnCallShow * 100) / 100
            }
            else
            {
                $scope.shouldInvited = '';
                $scope.called = '';
                $scope.terhubung = '';
                $scope.yangDiSMS = '';
                $scope.comeAfterInvited = '';
                $scope.kontribusiMRSComeAfterInvited = '';
                $scope.kontribusiMRSThdSBE ='';
                $scope.databaseValid = '';
                $scope.bookingOnCall = '';
                $scope.bookingOnCallShow = '';
            }
          })
      }

      $scope.getExportHO = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
          var $tanggalakhir=$scope.filter.DateEnd;
          var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
          var dataReport = {
            'kategori': $scope.filter.kategori,
            'jenis': "export",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'dealerarea': $scope.filter.dealerarea,};

        ReportMRSFactory.getExportHO(dataReport);
    }

// ================================================================== CABANG ============================================

    $scope.getSummaryCabang = function(){
        var $tanggal=$scope.filter.periode;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var dataFilter = {
            'kategori': $scope.filter.kategori,
            'jenis': "summary",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periode': $filtertanggal,};

          ReportMRSFactory.getSummaryCabang(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;
            if (response.data[0] != undefined) {
                $scope.shouldInvited = response.data[0].shouldInvited;
                $scope.shouldInvited = Math.round($scope.shouldInvited * 100) / 100
                $scope.called = response.data[0].called;
                $scope.called = Math.round($scope.called * 100) / 100
                $scope.terhubung = response.data[0].terhubung;
                $scope.terhubung = Math.round($scope.terhubung * 100) / 100
                $scope.yangDiSMS = response.data[0].yangDiSMS;
                $scope.yangDiSMS = Math.round($scope.yangDiSMS * 100) / 100
                $scope.comeAfterInvited = response.data[0].comeAfterInvited;
                $scope.comeAfterInvited = Math.round($scope.comeAfterInvited * 100) / 100
                $scope.kontribusiMRSComeAfterInvited = response.data[0].kontribusiMRSComeAfterInvited;
                $scope.kontribusiMRSComeAfterInvited = Math.round($scope.kontribusiMRSComeAfterInvited * 100) / 100
                $scope.kontribusiMRSThdSBE = response.data[0].kontribusiMRSThdSBE;
                $scope.kontribusiMRSThdSBE = Math.round($scope.kontribusiMRSThdSBE * 100) / 100
                $scope.databaseValid = response.data[0].databaseValid;
                $scope.databaseValid = Math.round($scope.databaseValid * 100) / 100
                $scope.bookingOnCall = response.data[0].bookingOnCall;
                $scope.bookingOnCall = Math.round($scope.bookingOnCall * 100) / 100
                $scope.bookingOnCallShow = response.data[0].bookingOnCallShow;
                $scope.bookingOnCallShow = Math.round($scope.bookingOnCallShow * 100) / 100
            }
            else
            {
                $scope.shouldInvited = '';
                $scope.called = '';
                $scope.terhubung = '';
                $scope.yangDiSMS = '';
                $scope.comeAfterInvited = '';
                $scope.kontribusiMRSComeAfterInvited = '';
                $scope.kontribusiMRSThdSBE = '';
                $scope.databaseValid = '';
                $scope.bookingOnCall = ''
                $scope.bookingOnCallShow = '';
            }
          })
      }

      $scope.getExportCabang = function(){
          var $tanggal=$scope.filter.periode;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
          var dataReport = {
            'kategori': $scope.filter.kategori,
            'jenis': "export",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periode': $filtertanggal,};

        ReportMRSFactory.getExportCabang(dataReport);
    }

    // $scope.getPICMRA = [
    //     {PICMRAId:1,PICMRAName:"PICMRA001"},
    //     {PICMRAId:2,PICMRAName:"PICMRA002"}
    // ];

    // $scope.getData = function() {
    //     console.log("getData=>",$scope.filter);
    // }
});
