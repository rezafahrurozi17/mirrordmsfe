angular.module('app')
    .controller('PsfuGRController', function($scope,$stateParams, $http, CurrentUser, PsfuGRFactory,$timeout  ) {
        console.log('PSFUGR COMP');
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
        });
        $scope.user = CurrentUser.user();
        $scope.roleId = $scope.user.RoleId;
        $scope.role=$stateParams.role;
        console.log('role',$scope.role);
        var dateFormat = 'MM/yyyy';
        var dateFormat2 = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var periode=new Date();
        $scope.dateOptionsS = {
            startingDay: 1,
            format: dateFormat2,
            // mode:"'month'",
        };
        $scope.dateOptionsE = {
            startingDay: 1,
            format: dateFormat2,
            // mode:"'month'",
        };
        $scope.dateOptions2 = {
            startingDay: 1,
            format: dateFormat2,
            // mode:"'month'",
        };

        $scope.dataDealer = [];
        $scope.dataCabang = [];
        $scope.dataAreaTAM = [];
        $scope.dataAreaDealer = [];
        $scope.dataSA = [];
        $scope.filter = {Dealer:null,DealerId:null, Cabang:null, kategori:"General Repair", DateStart:periode, DateEnd:periode, Satellite:null, dealerarea:0,tamarea:0,isGR:1 };
        PsfuGRFactory.getDataDealer().then(function(response) {
            console.log('Response BEDealer:', response.data);
            $scope.dataDealer = response.data;
            $scope.filter.Dealer=response.data[0].dealerCode;
            $scope.filter.DealerId=response.data[0].dealerID;
                PsfuGRFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
                  console.log('ayamtest',response.data)
                  $scope.dataGroupDealerHO = response.data
                  $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
                  $scope.areaId($scope.dataGroupDealerHO[0])
                })
        })
        PsfuGRFactory.getDataCabang($scope.filter).then(function(response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabang = response.data;
            $scope.DisabledCabang();
            $scope.filter.Cabang=response.data[0].cabangID;
        })
        PsfuGRFactory.getDataAreaTAM().then(function(response) {
            console.log('Response AreaTAM:', response.data);
            $scope.dataAreaTAM = response.data;
        })
        PsfuGRFactory.getDataAreaDealerHO($scope.filter).then(function(response) {
            console.log('Response AreaDealerHO:', response.data);
            $scope.dataAreaDealerHO = response.data;
            $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
            $scope.areaId($scope.dataAreaDealerHO[0])
        })
        PsfuGRFactory.getDataAreaDealer().then(function(response) {
            console.log('Response AreaDealer:', response.data);
            $scope.dataAreaDealer = response.data;
        })
        PsfuGRFactory.getDataSA().then(function(response) {
            console.log('Response SA:', response.data);
            $scope.dataSA = response.data;
        })
    // ======================
    $scope.startMinOption = null;
    $scope.startMaxOption = new Date();
    $scope.endMinOption = new Date();
    $scope.endMaxOption = null;
    var dateFormat3= 'dd/MM/yyyy';
    $scope.DateFilterOptions1 = {
        startingDay: 1,
        format: dateFormat3,
        //disableWeekend: 1
    };
    $scope.DateFilterOptions2 = {
        startingDay: 1,
        format: dateFormat3,
        //disableWeekend: 1
    };


    //===============validasi date start and date end===========
    var today = new Date();
    $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(), today.getDate());
    $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());
    $scope.minDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());


    $scope.changeStartDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.DateStart);
        var dayTwo = new Date($scope.filter.DateEnd);

        $scope.dateOptionsE.minDate = new Date(dayOne.getFullYear(),dayOne.getMonth(), dayOne.getDate());
        if (tgl == null || tgl == undefined){
            $scope.filter.DateEnd = null;
        } else {
            if ($scope.filter.DateStart < $scope.filter.DateEnd){

            } else {
                $scope.filter.DateEnd = $scope.filter.DateStart;
            }
        }
    }

    //=============== End of validasi date start and date end===========







    $scope.startDateChange = function(selected){
        console.log(selected.dt)
        // if (changeId == 4 || changeId == 5) {
            console.log("changeId == 4 | 5");
            $scope.DateFilterOptions2.minDate = selected.dt;
        // } else if (changeId == 6) {
        //     console.log("changeId == 6");
        //     $scope.DateFilterOptions2.minDate = selected.dt;
        //     $scope.DateFilterOptions2.maxDate = null;
        // }
    }
    // ======================
    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
            $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };
        // $scope.DisabledCabang = function() {
        //     if ($scope.dataCabang.length>1){
        //         $scope.Disabled =false;
        //         console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
        //     }
        //     else{
        //         $scope.Disabled =true;
        //         console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
        //     }
        // };
        // =================================================================== GR HO ====================
        $scope.getData = function() {
            if ($scope.role == "Branch") $scope.getSummaryGRCabang();
            else $scope.getSummaryGRHO();
            console.log('get data');
        }

        $scope.getReport = function() {
            if ($scope.role == "Branch") $scope.getReportGRCabang();
            else $scope.getReportGRHO();
            console.log('get Report');
        }

        

        $scope.areaId = function(row) {
        // $scope.DataDealer = row
          $scope.DataDealer = Object.assign(row,{'kategori': $scope.filter.kategori})
          if($scope.userlogin != 'TAM'){
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
          }else{
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
          }
          console.log('row dealerid', row,$scope.DataDealer);
          PsfuGRFactory.getDataCabangHONew($scope.DataDealer).then(function (response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabangHO = response.data;
            $scope.DisabledCabang();
            $scope.filter.CabangHO = null
            console.log('kambing',$scope.filter)
            $timeout(function() {
              $scope.filter.CabangHO = response.data[0].cabangID;
              console.log('kambing123',$scope.filter.CabangHO)
            }, 100);
          })
        };

        $scope.dealerCode = function(row){
            row = Object.assign(row,{'isGR':$scope.filter.isGR})
            $scope.filter.DealerId = row.dealerID
            PsfuGRFactory.getDataAreaDealerHO(row).then(function (response) {
                console.log('ayam',response.data)
                console.log('rusa',$scope.dataGroupDealer)
                $scope.dataGroupDealer = response.data
                $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
                $scope.areaId($scope.dataGroupDealer[0])
                console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
            })
          }
    
        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };

        $scope.getSummaryGRHO = function() {
            var $tanggal1 = $scope.filter.DateStart;
            var $tanggal2 = $scope.filter.DateEnd;
            var $filtertanggal1 = $tanggal1.getFullYear() + '-' + ($tanggal1.getMonth() + 1) + '-' + $tanggal1.getDate();
            var $filtertanggal2 = $tanggal2.getFullYear() + '-' + ($tanggal2.getMonth() + 1) + '-' + $tanggal2.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'tamarea': $scope.filter.tamarea,
                'dealerarea': $scope.filter.dealerarea,
                'kategori': $scope.filter.kategori,
                'periode_awal': $filtertanggal1,
                'periode_akhir': $filtertanggal2,
                'Jenis': 'summary'
            };

            PsfuGRFactory.getSummaryGRHO(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);
                // $scope.dataSummary = response.data;
                var hasil = response.data[0];
                $scope.unitYangHarusFU = hasil.yangHarusFU;
                $scope.unitYangHarusFU = Math.round($scope.unitYangHarusFU * 100) / 100
                $scope.unitBerhasilFU = hasil.yangBerhasilFU;
                $scope.unitBerhasilFU = Math.round($scope.unitBerhasilFU * 100) / 100
                $scope.Q1 = hasil.q1;
                $scope.Q1 = Math.round($scope.Q1 * 100) / 100
                $scope.Q2 = hasil.q2;
                $scope.Q2 = Math.round($scope.Q2 * 100) / 100
                $scope.Q3 = hasil.q3;
                $scope.Q3 = Math.round($scope.Q3 * 100) / 100

                var TmpCabangName = []
                for(var i in $scope.dataCabang){
                  if($scope.dataCabang[i].cabangID == dataFilter.cabang){
                    TmpCabangName = $scope.dataCabang[i].cabangName
                  }
                }

                if(TmpCabangName == 'ALL'){
                    $scope.Q1Rate = hasil.avgQ1Ratio
                    $scope.Q2Rate = hasil.avgQ2Ratio
                    $scope.Q3Rate = hasil.avgQ3Ratio
                    $scope.firRate = hasil.avgFIRRatio
                }else{
                    $scope.Q1Rate = hasil.q1Ratio;
                    $scope.Q1Rate = Math.round($scope.Q1Rate * 100) / 100
                    $scope.Q2Rate = hasil.q2Ratio;
                    $scope.Q2Rate = Math.round($scope.Q2Rate * 100) / 100
                    $scope.Q3Rate = hasil.q3Ratio;
                    $scope.Q3Rate = Math.round($scope.Q3Rate * 100) / 100
                    // $scope.firRate = hasil.yangBerhasilFU > 0 ? (hasil.yangBerhasilFU - hasil.q1 - hasil.q2 - hasil.q3) / hasil.yangBerhasilFU : 0;
                    // $scope.firRate = Math.round($scope.firRate * 100) / 100
                    $scope.firRate = hasil.firRatio;
                    $scope.firRate = Math.round($scope.firRate * 100) / 100
                }
                

                
            })
        }
        $scope.getReportGRHO = function() {
            var $tanggal1 = $scope.filter.DateStart;
            var $tanggal2 = $scope.filter.DateEnd;
            var $filtertanggal1 = $tanggal1.getFullYear() + '/' + ($tanggal1.getMonth() + 1) + '/' + $tanggal1.getDate();
            var $filtertanggal2 = $tanggal2.getFullYear() + '/' + ($tanggal2.getMonth() + 1) + '/' + $tanggal2.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'tamarea': $scope.filter.tamarea,
                'dealerarea': $scope.filter.dealerarea,
                'kategori': $scope.filter.kategori,
                'periode_awal': $filtertanggal1,
                'periode_akhir': $filtertanggal2,
                'Jenis': 'summary'
            };

            PsfuGRFactory.getExportGRHO(dataFilter);
        }

        // ============================================================================== GR CABANG ====================
        $scope.getSummaryGRCabang = function() {
            var $tanggal1 = $scope.filter.DateStart;
            var $tanggal2 = $scope.filter.DateEnd;
            var $filtertanggal1 = $tanggal1.getFullYear() + '-' + ($tanggal1.getMonth() + 1) + '-' + $tanggal1.getDate();
            var $filtertanggal2 = $tanggal2.getFullYear() + '-' + ($tanggal2.getMonth() + 1) + '-' + $tanggal2.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'kategori': $scope.filter.kategori,
                'periode_awal': $filtertanggal1,
                'periode_akhir': $filtertanggal2,
                'sa': $scope.filter.SA
            };

            PsfuGRFactory.getSummaryGRCabang(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);
                // $scope.dataSummary = response.data;
                var hasil = response.data[0];
                $scope.unitYangHarusFU = hasil.jumlahYangHarusFollowUp;
                $scope.unitYangHarusFU = Math.round($scope.unitYangHarusFU * 100) / 100
                $scope.unitBerhasilFU = hasil.jumlahTerFollowUp;
                $scope.unitBerhasilFU = Math.round($scope.unitBerhasilFU * 100) / 100
                $scope.Q1 = hasil.jumlahQ1;
                $scope.Q1 = Math.round($scope.Q1 * 100) / 100
                $scope.Q2 = hasil.jumlahQ2;
                $scope.Q2 = Math.round($scope.Q2 * 100) / 100
                $scope.Q3 = hasil.jumlahQ3;
                $scope.Q3 = Math.round($scope.Q3 * 100) / 100
                $scope.Q1Rate = hasil.q1Rate;
                $scope.Q1Rate = Math.round($scope.Q1Rate * 100) / 100
                $scope.Q2Rate = hasil.q2Rate;
                $scope.Q2Rate = Math.round($scope.Q2Rate * 100) / 100
                $scope.Q3Rate = hasil.q3Rate;
                $scope.Q3Rate = Math.round($scope.Q3Rate * 100) / 100
                $scope.firRate = hasil.firRatio;
                $scope.firRate = Math.round($scope.firRate * 100) / 100
            })
        }
        $scope.getReportGRCabang = function() {
            var $tanggal1 = $scope.filter.DateStart;
            var $tanggal2 = $scope.filter.DateEnd;
            var $filtertanggal1 = $tanggal1.getFullYear() + '/' + ($tanggal1.getMonth() + 1) + '/' + $tanggal1.getDate();
            var $filtertanggal2 = $tanggal2.getFullYear() + '/' + ($tanggal2.getMonth() + 1) + '/' + $tanggal2.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'kategori': $scope.filter.kategori,
                'periode_awal': $filtertanggal1,
                'periode_akhir': $filtertanggal2,
                'sa': $scope.filter.SA
            };

            PsfuGRFactory.getExporGRCabang(dataFilter);
        }



        //----------------------------------
        //  // Start-Up
        //  //----------------------------------

        //  //----------------------------------
        //  // Initialization
        //  //----------------------------------
        //  $scope.user = CurrentUser.user();
        //  $scope.loading=false;
        //  $scope.formApi = {};
        //  $scope.backToMain = true;
        //  $scope.roleId = 0;// 0 = HO | 1 = Branch


        //  $scope.onSelectUser = function(item, model, label){
        //      console.log("onSelectUser=>",item,model,label);
        //  }

});
