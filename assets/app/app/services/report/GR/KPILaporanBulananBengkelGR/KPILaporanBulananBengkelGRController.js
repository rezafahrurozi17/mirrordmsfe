angular.module('app')
	.controller('ReportKPIBulananBengkelGRController', function ($scope, $http, CurrentUser, KPILaporanBulananBengkelGRFactory) {
		//----------------------------------
		// Start-Up
		//----------------------------------
		$scope.$on('$viewContentLoaded', function () {
			$scope.loading = false;
		});

		$scope.user = CurrentUser.user();
		$scope.roleId = $scope.user.RoleId;

		$scope.formatDate = function (date) {
			var dateOut = new Date(date);
			return dateOut;
		};
		var dateFormat = 'MM/yyyy';
		var dateFilter = 'date:"dd/MM/yyyy"';
		// var periode = new Date();
		var today = new Date();
		today.setDate(1);
		today.setHours(-1);
		var monthperiode = (today.getMonth() + 1).toString();
		var dayPeriode = today.getDate().toString();
		var yearPeriode = today.getFullYear().toString();
		$scope.periode = yearPeriode + '/' + (monthperiode[1] ? monthperiode : "0" + monthperiode[0]) + '/' + dayPeriode;
		$scope.dateOptions = {
			startingDay: 1,
			format: dateFormat,
			maxDate : $scope.periode
		};
		$scope.dataDealer = [];
		$scope.dataCabang = [];

		function format1(n, currency) {
			return currency + " " + n.toFixed(0).replace(/./g, function (c, i, a) {
				return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
			});
		}

		$scope.getData = function () {

		};
		$scope.filter = {
			Dealer: null,
			Cabang: null,
			KategoriWorkshop: "General Repair",
			periode: $scope.periode,
			Satellite: null,
			dealerarea: 0,
			tamarea: 0,
			listrikkwh: 0,
			absorption_rate: 0,
			ServiceCost: 0,
			ServiceRevenue: 0,
			partinternal: 0,
			partexternal: 0,
			oilsales: 0,
			outsidework: 0,
			accessories: 0,
			SLS: 0,
			PCGS: 0,
			OCGS: 0,
			ACGS: 0,
			SLC: 0,
			SDE: 0,
			amount51: 0,
			amount52: 0,
			amount53: 0,
			amount54: 0,
			amount55: 0,
			amount56: 0,
			amount57: 0,
			amount58: 0,
			amount59: 0,
			amount60: 0,
			amount61: 0,
			amount62: 0,
			amount63: 0,
			amount64: 0,
			amount65: 0,
			amount66: 0,
			amount67: 0,
			amount68: 0,
			amount69: 0,
			amount70: 0,
			amount71: 0,
			amount72: 0,
			amount73: 0,
			amount74: 0,
			amount75: 0,
			amount114: 0,
			amount34: 0,
			amount35: 0,
			amount36: 0,
			amount37: 0
		};
		KPILaporanBulananBengkelGRFactory.getDataDealer().then(function (response) {
			console.log('Response BEDealer:', response.data);
			$scope.dataDealer = response.data;
			$scope.filter.Dealer = response.data[0].dealerCode;
		})
		KPILaporanBulananBengkelGRFactory.getDataCabang().then(function (response) {
			console.log('Response BECabang:', response.data);
			$scope.dataCabang = response.data;
			$scope.DisabledCabang();
			$scope.filter.Cabang = response.data[0].cabangID;
		})
		$scope.DisabledCabang = function () {
			if ($scope.dataCabang.length > 1) {
				$scope.Disabled = false;
				console.log('Cabang Enabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
			} else {
				$scope.Disabled = true;
				console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
			}
		};
		$scope.getData = function () {
			console.log('get data');
		}
		$scope.allowPattern = function(item, event,param) {
            console.log("item", item);
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
                // var tmp = item;
                // if (tmp.includes(",")) {
                //     tmp = tmp.split(',');
                //     if (tmp.length > 1) {
                //         tmp = tmp.join('');
                //     } else {
                //         tmp = tmp[0];
                //     }
                // }
				// tmp = parseInt(tmp);
				if(param == 0){
					$scope.filter.SLC = $scope.filter.SLC.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
				}else if(param == 1){
					$scope.filter.SDE = $scope.filter.SDE.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
				}
                
                return;
            }
        };

		$scope.simpan = function () {
			console.log('simpan', $scope);
			var $tanggal1 = $scope.filter.periode;
			var $periode = $tanggal1.getFullYear() + '-' + ($tanggal1.getMonth() + 1) + '-' + $tanggal1.getDate();
			$scope.filter.SLC = $scope.filter.SLC.replace(/,/g, '');
			$scope.filter.SDE = $scope.filter.SDE.replace(/,/g, '');
			var dataFilter = {
				'cabang': $scope.filter.Cabang,
				'periode': $periode,
				'listrik': $scope.filter.listrikkwh,
				'SLC': $scope.filter.SLC,
				'SDE': $scope.filter.SDE,
				'amount51': $scope.filter.amount51,
				'amount52': $scope.filter.amount52,
				'amount53': $scope.filter.amount53,
				'amount54': $scope.filter.amount54,
				'amount55': $scope.filter.amount55,
				'amount56': $scope.filter.amount56,
				'amount59': $scope.filter.amount59,
				'amount60': $scope.filter.amount60,
				'amount61': $scope.filter.amount61,
				'amount62': $scope.filter.amount62,
				'amount64': $scope.filter.amount64,
				'amount65': $scope.filter.amount65,
				'amount67': $scope.filter.amount67,
				'amount68': $scope.filter.amount68,
				'amount69': $scope.filter.amount69,
				'amount70': $scope.filter.amount70,
				'amount71': $scope.filter.amount71,
				'amount72': $scope.filter.amount72,
				'amount74': $scope.filter.amount74,

			};

			KPILaporanBulananBengkelGRFactory.save(dataFilter).then(function (response) {
				console.log('Response BE Filter:', response.data);
				// $scope.dataSummary = response.data;
				//// save tidak get data, jadi tidak perlu set apapun
				// var hasil = response.data[0];
				// $scope.filter.listrikkwh = hasil.listrik;


				// $scope.filter.ServiceRevenue = hasil.partinternal + hasil.partexternal + hasil.oilsales + hasil.outsidework + hasil.accessories + hasil.sls;
				// $scope.filter.partinternal = hasil.partinternal;
				// $scope.filter.partexternal = hasil.partexternal;
				// $scope.filter.oilsales = hasil.oilsales;
				// $scope.filter.outsidework = hasil.outsidework;
				// $scope.filter.accessories = hasil.accessories;
				// $scope.filter.SLS = hasil.sls;

				// $scope.filter.ServiceCost = hasil.pcgs + hasil.ocgs + hasil.acgs + hasil.slc;
				// $scope.filter.absorption_rate = ($scope.filter.ServiceRevenue - $scope.filter.ServiceCost) * 100;
				// $scope.filter.PCGS = hasil.pcgs;
				// $scope.filter.OCGS = hasil.ocgs;
				// $scope.filter.ACGS = hasil.acgs;
				// $scope.filter.SLC = hasil.slc;

				// $scope.filter.SDE = hasil.sde;
				// $scope.filter.amount51 = hasil.amount51;
				// $scope.filter.amount52 = hasil.amount52;
				// $scope.filter.amount53 = hasil.amount53;
				// $scope.filter.amount54 = hasil.amount54;
				// $scope.filter.amount55 = hasil.amount55;
				// $scope.filter.amount56 = hasil.amount56;
				// $scope.filter.amount57 = hasil.amount51 + hasil.amount52 + hasil.amount53 + hasil.amount54 + hasil.amount55 + hasil.amount56;
				// $scope.filter.amount58 = hasil.amount52 + hasil.amount53 + hasil.amount54 + hasil.amount55 + hasil.amount56;

				// $scope.filter.amount59 = hasil.amount59;
				// $scope.filter.amount60 = hasil.amount60;
				// $scope.filter.amount61 = hasil.amount61;
				// $scope.filter.amount62 = hasil.amount62;
				// $scope.filter.amount63 = hasil.amount59 + hasil.amount60 + hasil.amount61 + hasil.amount62;

				// $scope.filter.amount64 = hasil.amount64;
				// $scope.filter.amount65 = hasil.amount65;
				// $scope.filter.amount66 = (hasil.amount64 + hasil.amount65) > 0 ? hasil.amount64 + hasil.amount65 : hasil.amount67 + hasil.amount68 + hasil.amount69;

				// $scope.filter.amount67 = hasil.amount67;
				// $scope.filter.amount68 = hasil.amount68;
				// $scope.filter.amount69 = hasil.amount69;
				// $scope.filter.amount70 = hasil.amount70;
				// $scope.filter.amount71 = hasil.amount71;
				// $scope.filter.amount72 = hasil.amount72;
				// $scope.filter.amount73 = hasil.amount70 + hasil.amount71 + hasil.amount72;

				// $scope.filter.amount74 = hasil.amount74;
				// $scope.filter.amount75 = $scope.filter.amount57 + $scope.filter.amount63 + $scope.filter.amount66 + $scope.filter.amount73 + $scope.filter.amount74;
			})
		}
		var convertDecimal = function(item){
			var num = item
			var with2Decimals = num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]
			var totalValue = parseFloat(with2Decimals);
			return totalValue;
		}
		$scope.tampilkan = function () {
			console.log('tampilkan', $scope);
			var $tanggal1 = $scope.filter.periode;
			var $periode = $tanggal1.getFullYear() + '-' + ($tanggal1.getMonth() + 1) + '-' + $tanggal1.getDate();
			var dataFilter = {
				'cabang': $scope.filter.Cabang,
				'periode': $periode,
				'listrik': 0,
				'SLC': 0,
				'SDE': 0,
				'amount51': 0,
				'amount52': 0,
				'amount53': 0,
				'amount54': 0,
				'amount55': 0,
				'amount56': 0,
				'amount59': 0,
				'amount60': 0,
				'amount61': 0,
				'amount62': 0,
				'amount64': 0,
				'amount65': 0,
				'amount67': 0,
				'amount68': 0,
				'amount69': 0,
				'amount70': 0,
				'amount71': 0,
				'amount72': 0,
				'amount74': 0,

			};

			KPILaporanBulananBengkelGRFactory.getTampilkan(dataFilter).then(function (response) {
				console.log('Response BE Filter:', response.data);
				// $scope.dataSummary = response.data;
				if (response.data.length > 0) {
					var hasil = response.data[0];
					$scope.filter.listrikkwh = hasil.listrik;

					// $scope.filter.ServiceRevenue = hasil.partinternal + hasil.partexternal + hasil.oilsales + hasil.outsidework + hasil.accessories + hasil.sls;
					//var ServiceRevenue = hasil.partinternal + hasil.partexternal + hasil.oilsales + hasil.outsidework + hasil.accessories + hasil.sls;
					var ServiceRevenue = hasil.serviceRevenue;					
					$scope.filter.ServiceRevenue = format1(ServiceRevenue, '');
					$scope.filter.partinternal = hasil.partinternal;
					$scope.filter.partinternal = format1($scope.filter.partinternal, '');
					$scope.filter.partexternal = hasil.partexternal;
					$scope.filter.partexternal = format1($scope.filter.partexternal, '');
					$scope.filter.oilsales = hasil.oilsales;
					$scope.filter.oilsales = format1($scope.filter.oilsales, '');
					$scope.filter.outsidework = hasil.outsidework;
					$scope.filter.outsidework = format1($scope.filter.outsidework, '');
					$scope.filter.accessories = hasil.accessories;
					$scope.filter.accessories = format1($scope.filter.accessories, '');
					$scope.filter.SLS = hasil.sls;
					$scope.filter.SLS = format1($scope.filter.SLS, '');

					// $scope.filter.ServiceCost = hasil.pcgs + hasil.ocgs + hasil.acgs + hasil.slc;
					//var ServiceCost = hasil.pcgs + hasil.ocgs + hasil.acgs + hasil.slc;
					var ServiceCost = hasil.serviceCost;
					$scope.filter.ServiceCost = format1(ServiceCost, '');
					$scope.filter.PCGS = hasil.pcgs;
					$scope.filter.PCGS = format1($scope.filter.PCGS, '');
					$scope.filter.OCGS = hasil.ocgs;
					$scope.filter.OCGS = format1($scope.filter.OCGS, '');
					$scope.filter.ACGS = hasil.acgs;
					$scope.filter.ACGS = format1($scope.filter.ACGS, '');
					$scope.filter.SLC = hasil.slc;
					$scope.filter.SLC = format1($scope.filter.SLC, '');
					$scope.filter.SDE = hasil.sde;
					$scope.filter.SDE = format1($scope.filter.SDE, '');

					$scope.filter.absorption_rate = convertDecimal((ServiceRevenue - ServiceCost) / hasil.sde * 100);

					$scope.filter.amount51 = hasil.amount51;
					$scope.filter.amount52 = hasil.amount52;
					$scope.filter.amount53 = hasil.amount53;
					$scope.filter.amount54 = hasil.amount54;
					$scope.filter.amount55 = hasil.amount55;
					$scope.filter.amount56 = hasil.amount56;
					$scope.filter.amount57 = hasil.amount51 + hasil.amount52 + hasil.amount53 + hasil.amount54 + hasil.amount55 + hasil.amount56;
					$scope.filter.amount58 = hasil.amount52 + hasil.amount53 + hasil.amount54 + hasil.amount55 + hasil.amount56;

					$scope.filter.amount59 = hasil.amount59;
					$scope.filter.amount60 = hasil.amount60;
					$scope.filter.amount61 = hasil.amount61;
					$scope.filter.amount62 = hasil.amount62;
					$scope.filter.amount63 = hasil.amount59 + hasil.amount60 + hasil.amount61 + hasil.amount62;

					$scope.filter.amount64 = hasil.amount64;
					$scope.filter.amount65 = hasil.amount65;
					$scope.filter.amount66 = (hasil.amount64 + hasil.amount65) > 0 ? hasil.amount64 + hasil.amount65 : hasil.amount67 + hasil.amount68 + hasil.amount69;

					$scope.filter.amount67 = hasil.amount67;
					$scope.filter.amount68 = hasil.amount68;
					$scope.filter.amount69 = hasil.amount69;
					$scope.filter.amount70 = hasil.amount70;
					$scope.filter.amount71 = hasil.amount71;
					$scope.filter.amount72 = hasil.amount72;
					$scope.filter.amount73 = hasil.amount70 + hasil.amount71 + hasil.amount72;

					$scope.filter.amount74 = hasil.amount74;
					$scope.filter.amount75 = $scope.filter.amount57 + $scope.filter.amount63 + $scope.filter.amount66 + $scope.filter.amount73 + $scope.filter.amount74;
				} else {
					$scope.filter.listrikkwh = 0;

					$scope.filter.ServiceRevenue = 0;
					$scope.filter.partinternal = 0;
					$scope.filter.partexternal = 0;
					$scope.filter.oilsales = 0;
					$scope.filter.outsidework = 0;
					$scope.filter.accessories = 0;
					$scope.filter.SLS = 0;

					$scope.filter.ServiceCost = 0;
					$scope.filter.PCGS = 0;
					$scope.filter.OCGS = 0;
					$scope.filter.ACGS = 0;
					$scope.filter.SLC = 0;
					$scope.filter.SDE = 0;

					$scope.filter.absorption_rate = 0;

					$scope.filter.amount51 = 0;
					$scope.filter.amount52 = 0;
					$scope.filter.amount53 = 0;
					$scope.filter.amount54 = 0;
					$scope.filter.amount55 = 0;
					$scope.filter.amount56 = 0;
					$scope.filter.amount57 = 0;
					$scope.filter.amount58 = 0;

					$scope.filter.amount59 = 0;
					$scope.filter.amount60 = 0;
					$scope.filter.amount61 = 0;
					$scope.filter.amount62 = 0;
					$scope.filter.amount63 = 0;

					$scope.filter.amount64 = 0;
					$scope.filter.amount65 = 0;
					$scope.filter.amount66 = 0;

					$scope.filter.amount67 = 0;
					$scope.filter.amount68 = 0;
					$scope.filter.amount69 = 0;
					$scope.filter.amount70 = 0;
					$scope.filter.amount71 = 0;
					$scope.filter.amount72 = 0;
					$scope.filter.amount73 = 0;

					$scope.filter.amount74 = 0;
					$scope.filter.amount75 = 0;
				}

				console.log('$scope.filter ==>', $scope.filter);
			})
		}

		$scope.getReport = function () {
			var $tanggal = $scope.filter.periode;
			var $filtertanggal = $tanggal.getFullYear() + '/' + ($tanggal.getMonth() + 1) + '/' + $tanggal.getDate();
			var dataReport = {
				'dealer': $scope.filter.Dealer,
				'cabang': $scope.filter.Cabang,
				'jenis': "export",
				'periode': $filtertanggal,
			};

			KPILaporanBulananBengkelGRFactory.getExport(dataReport);
		}
	});
