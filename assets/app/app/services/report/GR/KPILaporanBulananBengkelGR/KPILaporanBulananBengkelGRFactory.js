angular.module('app')
  .factory('KPILaporanBulananBengkelGRFactory', function ($http, CurrentUser, $q) {
    console.log('Service GR Bulanan KPILaporanBulananBengkelGRFactory Loaded...');
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = '';//http://localhost:8080';
    return {
      getDataDealer: function () {
        var res = $http.get(serverURL + '/api/rpt/Dealer/' + currentUser.OrgId);
        return res;
      },
      getDataCabang: function () {
        var res = $http.get(serverURL + '/api/rpt/Cabang/' + currentUser.OrgId);
        return res;
      },
      getData: function () {
        var res = $http.get('/api/fw/Role');
        console.log('data=>', res);
        //---------
        return $q.resolve(data);
      },
      save: function (data) {
        console.log('Data Filter:', data);
        var res = $http.get(serverURL + '/api/rpt/AS_KPILaporanBulananBengkel_GRCAB/' + data.cabang + '/' + data.periode + '/' + data.listrik + '/' + data.SLC + '/' + data.SDE + '/' + data.amount51 + '/' + data.amount52 + '/' + data.amount53 + '/' + data.amount54 + '/' + data.amount55 + '/' + data.amount56 + '/' + data.amount59 + '/' + data.amount60 + '/' + data.amount61 + '/' + data.amount62 + '/' + data.amount64 + '/' + data.amount65 + '/' + data.amount67 + '/' + data.amount68 + '/' + data.amount69 + '/' + data.amount70 + '/' + data.amount71 + '/' + data.amount72 + '/' + data.amount74 + '/' + 1);
        return res;
      },

      getTampilkan: function (data) {
        console.log('Data Filter:', data);
        var res = $http.get(serverURL + '/api/rpt/AS_KPILaporanBulananBengkel_GRCAB/' + data.cabang + '/' + data.periode + '/' + data.listrik + '/' + data.SLC + '/' + data.SDE + '/' + data.amount51 + '/' + data.amount52 + '/' + data.amount53 + '/' + data.amount54 + '/' + data.amount55 + '/' + data.amount56 + '/' + data.amount59 + '/' + data.amount60 + '/' + data.amount61 + '/' + data.amount62 + '/' + data.amount64 + '/' + data.amount65 + '/' + data.amount67 + '/' + data.amount68 + '/' + data.amount69 + '/' + data.amount70 + '/' + data.amount71 + '/' + data.amount72 + '/' + data.amount74 + '/' + 0);
        return res;
      },

      getExport: function (data) {
        console.log('Data Report:', data);
        window.open(serverURL + '/api/rpt/AS_KPILaporanBulananBengkel_GRCAB?dealer=' + data.dealer + '&cabang=' + data.cabang + '&periode=' + data.periode);
      }
    }
  });
