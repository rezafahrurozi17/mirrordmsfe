angular.module('app')
  .factory('ReportLeadTimeGR', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL ='';// 'http://localhost:8080';
    return {
      getDataDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},
      // getDataCabang: function() {
      //       var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
      //       return res;},
      getDataCabang: function(data) {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId + '/' + data.KategoriWorkshop);
            return res;},
      getDataCabangHONew: function(data) {
        console.log('Data Get Cabang:', data);
        var res=$http.get(serverURL + '/api/rpt/GetCabangHONew/'+ data.areaId + '/' +  data.DealerId + '/' + data.kategori);
        return res;},
      getDataAreaTAM: function() {
      var res=$http.get(serverURL + '/api/rpt/AreaTAM/');
      return res;},
      getDataAreaDealer: function() {
      var res=$http.get(serverURL + '/api/rpt/AreaDealer/');
      return res;},
      getDataAreaDealerHO: function(data) {
      var res=$http.get(serverURL + '/api/rpt/AreaDealerHONew/'+ data.isGR );
      return res;},
      getSummaryFilterCabang: function(data) {
              console.log('Data Filter:', data);
              // console.log('periode',moment(data.periode).format('YYYY-MM-DD'))
              var res = $http.get(serverURL + '/api/rpt/AS_LeadTime_GRCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode_awal +'/'+ data.periode_akhir);
              return res;},
      getSummaryExportCabang: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_LeadTime_GRCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode_awal='+ data.periode_awal +'&periode_akhir='+ data.periode_akhir +'&export='+ data.export);
              // var res = $http.get(serverURL + 'CCustomerCompleteness?area='+ data.area +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periode +'&jenis_pelanggan='+ data.jenispel);
              // return res;
            },
      getSummaryFilterHO: function(data) {
              console.log('Data Filter:', data);
              // console.log('periode',moment(data.periode).format('YYYY-MM-DD'))
              var res = $http.get(serverURL + '/api/rpt/AS_LeadTime_GRHO/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/'+ data.tamarea +'/'+ data.dealerarea);
              return res;},
      getSummaryExportHO: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_LeadTime_GRHO?TamArea='+ data.tamarea +'&DealerArea='+ data.dealerarea +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periode_awal +'&dateend='+ data.periode_akhir);
              // var res = $http.get(serverURL + 'CCustomerCompleteness?area='+ data.area +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periode +'&jenis_pelanggan='+ data.jenispel);
              // return res;
            }

    }

  });
