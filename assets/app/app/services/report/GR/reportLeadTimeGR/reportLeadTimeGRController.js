angular.module('app')
    .controller('ReportLeadTimeGRController', function($scope, $stateParams,$http, CurrentUser, ReportLeadTimeGR,$timeout ) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();

    console.log("$scope.user : ",$scope.user);
    $scope.roleId = $scope.user.RoleId;
    $scope.role=$stateParams.role;
    console.log('role',$scope.role);
    var dateFormat='MM/yyyy';
    var dateFormat2='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat2,
    };
    $scope.dateOptions2S = {
        startingDay: 1,
        format: dateFormat2,
    };
    $scope.dateOptions2E = {
        startingDay: 1,
        format: dateFormat2,
    };
    $scope.filter = {Dealer:null,DealerId:null, Cabang:null, DateStart:periode, DateEnd:periode, tamarea:0,dealerarea:0,KategoriWorkshop:"General Repair",isGR:1};
    // if ($scope.roleId==1128 || $scope.roleId==1129) {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", KategoriWorkshop:"General Repair", DateStart:null, DateEnd:null, tamarea:43,dealerarea:43};
    // } else {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", KategoriWorkshop:"General Repair", DateStart:null, DateEnd:null, tamarea:43,dealerarea:43};
    // }


    //===============validasi date start and date end===========
    var today = new Date();
    $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(), today.getDate());
    $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());
    $scope.minDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());


    $scope.changeStartDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.DateStart);
        var dayTwo = new Date($scope.filter.DateEnd);

        $scope.dateOptions2E.minDate = new Date(dayOne.getFullYear(),dayOne.getMonth(), dayOne.getDate());
        if (tgl == null || tgl == undefined){
            $scope.filter.DateEnd = null;
        } else {
            if ($scope.filter.DateStart < $scope.filter.DateEnd){

            } else {
                $scope.filter.DateEnd = $scope.filter.DateStart;
            }
        }
    }

    //=============== End of validasi date start and date end===========



    ReportLeadTimeGR.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
        $scope.filter.DealerId=response.data[0].dealerID;
        ReportLeadTimeGR.getDataAreaDealerHO($scope.filter).then(function (response) {
          console.log('ayamtest',response.data)
          $scope.dataGroupDealerHO = response.data
          $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
          $scope.areaId($scope.dataGroupDealerHO[0])
        })
      })
    ReportLeadTimeGR.getDataCabang($scope.filter).then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
    ReportLeadTimeGR.getDataAreaTAM().then(function (response) {
        console.log('Response AreaTAM:', response.data);
        $scope.dataAreaTAM = response.data;
      })
    ReportLeadTimeGR.getDataAreaDealer().then(function (response) {
        console.log('Response AreaDealer:', response.data);
        $scope.dataAreaDealer = response.data;
      })
    ReportLeadTimeGR.getDataAreaDealerHO($scope.filter).then(function (response) {
        console.log('Response AreaDealerHO:', response.data);
        $scope.dataAreaDealerHO = response.data;
        $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
        $scope.areaId($scope.dataAreaDealerHO[0])
      })
    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
        $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };
        // $scope.DisabledCabang = function() {
        //     if ($scope.dataCabang.length>1){
        //         $scope.Disabled =false;
        //         console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
        //     }
        //     else{
        //         $scope.Disabled =true;
        //         console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
        //     }
        // };


        $scope.areaId = function(row) {
            // $scope.DataDealer = row
            $scope.DataDealer = Object.assign(row,{'kategori': $scope.filter.KategoriWorkshop})
            if($scope.userlogin != 'TAM'){
              $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
            }else{
              $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
            }
            console.log('row dealerid', row,$scope.DataDealer);
            ReportLeadTimeGR.getDataCabangHONew($scope.DataDealer).then(function (response) {
              console.log('Response BECabang:', response.data);
              $scope.dataCabangHO = response.data;
              $scope.DisabledCabang();
              $scope.filter.CabangHO = null
              console.log('kambing',$scope.filter)
              $timeout(function() {
                $scope.filter.CabangHO = response.data[0].cabangID;
                console.log('kambing123',$scope.filter.CabangHO)
              }, 100);
            })
        };

        $scope.dealerCode = function(row){
            row = Object.assign(row,{'isGR':$scope.filter.isGR})
            $scope.filter.DealerId = row.dealerID
            ReportLeadTimeGR.getDataAreaDealerHO(row).then(function (response) {
                console.log('ayam',response.data)
                console.log('rusa',$scope.dataGroupDealer)
                $scope.dataGroupDealer = response.data
                $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
                $scope.areaId($scope.dataGroupDealer[0])
                console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
            })
          }
      
        $scope.DisabledCabang = function() {
          if ($scope.dataCabang.length>1){
              $scope.Disabled =false;
              console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
          }else{
              $scope.Disabled =true;
              console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
          }
        };

function FormatTime(number) {
    if (number < 10 && number >= 0){
        number = '0'+ number;
    }
    else if (number > -10 && number < 0){
        number = number * -1;
        number = '-0'+ number;
    }
    else {
        number = number;
    }
    return number;
}
    var changeFormatTime = function (data) {
        console.log('data ===',data);
        var mins_num = parseFloat(data, 10); // float anjay
        var hours   = Math.floor(mins_num / 60);
        var minutes = Math.floor((mins_num - ((hours * 3600)) / 60));
        var seconds = Math.floor((mins_num * 60) - (hours * 3600) - (minutes * 60));

        // nambah 0 anjay
        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        console.log('mins_num ===',mins_num);

        return hours+':'+minutes+':'+seconds;
    }
    // $scope.roleId = 1; // 2 = HO | 1 = Branch
        $scope.getData = function(){
            if ($scope.role == "Branch") $scope.getDataCabang();
            else $scope.getDataHO();
            console.log('get data');
        }
    $scope.getDataCabang = function() {
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'kategori_workshop': $scope.filter.KategoriWorkshop,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,
            'Jenis': 'summary'};

        ReportLeadTimeGR.getSummaryFilterCabang(dataFilter).then(function (response) {
            console.log('Response BE Summary:', response.data);
            // $scope.dataSummary = response.data;
            var hasil=response.data[0];
            if (response.data[0] != undefined) {
                $scope.totalData = hasil.totalData;
                $scope.totalData = Math.round($scope.totalData * 100) / 100
                $scope.OTDFirst1 = hasil.otD1;
                $scope.OTDFirst1 = Math.round($scope.OTDFirst1 * 100) / 100
                $scope.OTDFirst2 = hasil.otD1Persen;
                $scope.OTDFirst2 = Math.round($scope.OTDFirst2 * 100) / 100
                $scope.OTDRemisi1 = hasil.otD2;
                $scope.OTDRemisi1 = Math.round($scope.OTDRemisi1 * 100) / 100
                $scope.OTDRemisi2 = hasil.otD2Persen;
                $scope.OTDRemisi2 = Math.round($scope.OTDRemisi2 * 100) / 100
                $scope.OTDRate=hasil.otdRate;
                $scope.OTDRate = Math.round($scope.OTDRate * 100) / 100
                $scope.dataValid=hasil.dataValid;
                $scope.dataValid = Math.round($scope.dataValid * 100) / 100

                // $scope.LTProses= FormatTime(~~(hasil.ltpAvg/3600))  +':'+ FormatTime(~~((Math.abs(hasil.ltpAvg)%3600)/60)) +':'+FormatTime((Math.abs(hasil.ltpAvg)%60));
                // $scope.LTStagnasi= FormatTime(~~(hasil.ltsAvg/3600))  +':'+ FormatTime(~~((Math.abs(hasil.ltsAvg)%3600)/60)) +':'+FormatTime((Math.abs(hasil.ltsAvg)%60));
                // $scope.LTTotal= FormatTime(~~(hasil.totalAvg/3600))  +':'+ FormatTime(~~((Math.abs(hasil.totalAvg)%3600)/60)) +':'+FormatTime((Math.abs(hasil.totalAvg)%60));

                // $scope.em1= FormatTime(~~(hasil.eM_LTP/3600))  +':'+ FormatTime(~~((Math.abs(hasil.eM_LTP)%3600)/60)) +':'+FormatTime((Math.abs(hasil.eM_LTP)%60));
                // $scope.em2= FormatTime(~~(hasil.eM_LTS/3600))  +':'+ FormatTime(~~((Math.abs(hasil.eM_LTS)%3600)/60)) +':'+FormatTime((Math.abs(hasil.eM_LTS)%60));
                // $scope.sb1= FormatTime(~~(hasil.sB_LTP/3600))  +':'+ FormatTime(~~((Math.abs(hasil.sB_LTP)%3600)/60)) +':'+FormatTime((Math.abs(hasil.sB_LTP)%60));
                // $scope.sb2= FormatTime(~~(hasil.sB_LTS/3600))  +':'+ FormatTime(~~((Math.abs(hasil.sB_LTS)%3600)/60)) +':'+FormatTime((Math.abs(hasil.sB_LTS)%60));
                // $scope.gr1= FormatTime(~~(hasil.gR_LTP/3600))  +':'+ FormatTime(~~((Math.abs(hasil.gR_LTP)%3600)/60)) +':'+FormatTime((Math.abs(hasil.gR_LTP)%60));
                // $scope.gr2= FormatTime(~~(hasil.gR_LTS/3600))  +':'+ FormatTime(~~((Math.abs(hasil.gR_LTS)%3600)/60)) +':'+FormatTime((Math.abs(hasil.gR_LTS)%60));
                // $scope.pre1= FormatTime(~~(hasil.pD_LTP/3600))  +':'+ FormatTime(~~((Math.abs(hasil.pD_LTP)%3600)/60)) +':'+FormatTime((Math.abs(hasil.pD_LTP)%60));
                // $scope.pre2= FormatTime(~~(hasil.pD_LTS/3600))  +':'+ FormatTime(~~((Math.abs(hasil.pD_LTS)%3600)/60)) +':'+FormatTime((Math.abs(hasil.pD_LTS)%60));

                $scope.LTProses= changeFormatTime(hasil.ltpAvg);
                $scope.LTStagnasi= changeFormatTime(hasil.ltsAvg);
                $scope.LTTotal= changeFormatTime(hasil.totalAvg);

                $scope.em1= changeFormatTime(hasil.eM_LTP);
                $scope.em2= changeFormatTime(hasil.eM_LTS);
                $scope.sb1= changeFormatTime(hasil.sB_LTP);
                $scope.sb2= changeFormatTime(hasil.sB_LTS);
                $scope.gr1= changeFormatTime(hasil.gR_LTP);
                $scope.gr2= changeFormatTime(hasil.gR_LTS);
                $scope.pre1= changeFormatTime(hasil.pD_LTP);
                $scope.pre2= changeFormatTime(hasil.pD_LTS);

            }
            else
            {
                $scope.totalData = 0;
                $scope.OTDFirst1 = 0;
                $scope.OTDFirst2 =  0;
                $scope.OTDRemisi1 =  0 ;
                $scope.OTDRemisi2 = 0;
                $scope.OTDRate= 0 ;
                $scope.dataValid= 0 ;
                $scope.LTProses= 0;
                $scope.LTStagnasi= 0;
                $scope.LTTotal= 0;

                $scope.em1= 0;
                $scope.em2= 0;
                $scope.sb1= 0;
                $scope.sb2= 0;
                $scope.gr1= 0;
                $scope.gr2= 0;
                $scope.pre1= 0;
                $scope.pre2= 0;
            }

        })
    }
    $scope.getDataHO = function() {
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'tamarea': $scope.filter.tamarea,
            'dealerarea': $scope.filter.dealerarea,
            'kategori_workshop': $scope.filter.KategoriWorkshop,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,
            'Jenis': 'summary'};

        ReportLeadTimeGR.getSummaryFilterHO(dataFilter).then(function (response) {
            console.log('Response BE Summary:', response.data);
            // $scope.dataSummary = response.data;
            var hasil=response.data[0];
            if (response.data[0] !== undefined) {
                $scope.totalData = hasil.totalData;
                $scope.totalData = Math.round($scope.totalData * 100) / 100
                $scope.OTDFirst1 = hasil.otD1;
                $scope.OTDFirst1 = Math.round($scope.OTDFirst1 * 100) / 100
                $scope.OTDFirst2 = hasil.otD1Persen;
                $scope.OTDFirst2 = Math.round($scope.OTDFirst2 * 100) / 100
                $scope.OTDRemisi1 = hasil.otD2;
                $scope.OTDRemisi1 = Math.round($scope.OTDRemisi1 * 100) / 100
                $scope.OTDRemisi2 = hasil.otD2Persen;
                $scope.OTDRemisi2 = Math.round($scope.OTDRemisi2 * 100) / 100
                $scope.OTDRate=hasil.otdRate;
                $scope.OTDRate = Math.round($scope.OTDRate * 100) / 100
                $scope.dataValid=hasil.dataValid;
                $scope.dataValid = Math.round($scope.dataValid * 100) / 100

                // $scope.pre2 = Math.round($scope.pre2 * 100) / 100
                // $scope.LTProses= FormatTime(~~(hasil.ltpAvg/3600))  +':'+ FormatTime(~~((Math.abs(hasil.ltpAvg)%3600)/60)) +':'+FormatTime((Math.abs(hasil.ltpAvg)%60));
                // $scope.LTStagnasi= FormatTime(~~(hasil.ltsAvg/3600))  +':'+ FormatTime(~~((Math.abs(hasil.ltsAvg)%3600)/60)) +':'+FormatTime((Math.abs(hasil.ltsAvg)%60));
                // $scope.LTTotal= FormatTime(~~(hasil.totalAvg/3600))  +':'+ FormatTime(~~((Math.abs(hasil.totalAvg)%3600)/60)) +':'+FormatTime((Math.abs(hasil.totalAvg)%60));

                // $scope.em1= FormatTime(~~(hasil.eM_LTP/3600))  +':'+ FormatTime(~~((Math.abs(hasil.eM_LTP)%3600)/60)) +':'+FormatTime((Math.abs(hasil.eM_LTP)%60));
                // $scope.em2= FormatTime(~~(hasil.eM_LTS/3600))  +':'+ FormatTime(~~((Math.abs(hasil.eM_LTS)%3600)/60)) +':'+FormatTime((Math.abs(hasil.eM_LTS)%60));
                // $scope.sb1= FormatTime(~~(hasil.sB_LTP/3600))  +':'+ FormatTime(~~((Math.abs(hasil.sB_LTP)%3600)/60)) +':'+FormatTime((Math.abs(hasil.sB_LTP)%60));
                // $scope.sb2= FormatTime(~~(hasil.sB_LTS/3600))  +':'+ FormatTime(~~((Math.abs(hasil.sB_LTS)%3600)/60)) +':'+FormatTime((Math.abs(hasil.sB_LTS)%60));
                // $scope.gr1= FormatTime(~~(hasil.gR_LTP/3600))  +':'+ FormatTime(~~((Math.abs(hasil.gR_LTP)%3600)/60)) +':'+FormatTime((Math.abs(hasil.gR_LTP)%60));
                // $scope.gr2= FormatTime(~~(hasil.gR_LTS/3600))  +':'+ FormatTime(~~((Math.abs(hasil.gR_LTS)%3600)/60)) +':'+FormatTime((Math.abs(hasil.gR_LTS)%60));
                // $scope.pre1= FormatTime(~~(hasil.pD_LTP/3600))  +':'+ FormatTime(~~((Math.abs(hasil.pD_LTP)%3600)/60)) +':'+FormatTime((Math.abs(hasil.pD_LTP)%60));
                // $scope.pre2= FormatTime(~~(hasil.pD_LTS/3600))  +':'+ FormatTime(~~((Math.abs(hasil.pD_LTS)%3600)/60)) +':'+FormatTime((Math.abs(hasil.pD_LTS)%60));

                $scope.LTProses = changeFormatTime(hasil.ltpAvg);
                $scope.LTStagnasi = changeFormatTime(hasil.ltsAvg);
                $scope.LTTotal = changeFormatTime(hasil.totalAvg);
                $scope.em1 = changeFormatTime(hasil.eM_LTP);
                $scope.em2 = changeFormatTime(hasil.eM_LTS);
                $scope.sb1 = changeFormatTime(hasil.sB_LTP);
                $scope.sb2 = changeFormatTime(hasil.sB_LTS);
                $scope.gr1 = changeFormatTime(hasil.gR_LTP);
                $scope.gr2 = changeFormatTime(hasil.gR_LTS);
                $scope.pre1 = changeFormatTime(hasil.pD_LTP);
                $scope.pre2 = changeFormatTime(hasil.pD_LTS);
            }
            else
            {
                $scope.totalData = 0;
                $scope.OTDFirst1 = 0;
                $scope.OTDFirst2 =  0;
                $scope.OTDRemisi1 =  0 ;
                $scope.OTDRemisi2 = 0;
                $scope.OTDRate= 0 ;
                $scope.dataValid= 0 ;
                $scope.LTProses= 0;
                $scope.LTStagnasi= 0;
                $scope.LTTotal= 0;

                $scope.em1= 0;
                $scope.em2= 0;
                $scope.sb1= 0;
                $scope.sb2= 0;
                $scope.gr1= 0;
                $scope.gr2= 0;
                $scope.pre1= 0;
                $scope.pre2= 0;
            }

        })
    }

    $scope.getReportJOBCabang = function(){
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'/'+($tanggal1.getMonth()+1)+'/'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'/'+($tanggal2.getMonth()+1)+'/'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'kategori_workshop': $scope.filter.KategoriWorkshop,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,
            'export': 'perjob'};

        ReportLeadTimeGR.getSummaryExportCabang(dataFilter);
    }
    $scope.getReportWOCabang = function(){
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'/'+($tanggal1.getMonth()+1)+'/'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'/'+($tanggal2.getMonth()+1)+'/'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'kategori_workshop': $scope.filter.KategoriWorkshop,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,
            'export': 'perwo'};

        ReportLeadTimeGR.getSummaryExportCabang(dataFilter);
    }
    $scope.getReportHO = function(){
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'/'+($tanggal1.getMonth()+1)+'/'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'/'+($tanggal2.getMonth()+1)+'/'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'tamarea': $scope.filter.tamarea,
            'dealerarea': $scope.filter.dealerarea,
            'kategori_workshop': $scope.filter.KategoriWorkshop,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,
            'Jenis': 'export'};

        ReportLeadTimeGR.getSummaryExportHO(dataFilter);
    }



});



