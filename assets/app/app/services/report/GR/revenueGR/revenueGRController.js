angular.module('app')
    .controller('RevenueGRController', function($scope,$stateParams, $http, CurrentUser , RevenueGRFactory, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
      $scope.loading=false;
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;
    $scope.role=$stateParams.role;
    console.log('role',$scope.role);
    // $scope.filter = {kategori:$stateParams.kategori};
    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    var dateFormat='MM/yyyy';
    var dateFormat2='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat2,

        // mode:"'month'",
    };
        $scope.dateOptions2 = {
        startingDay: 1,
        format: dateFormat2,

        // mode:"'month'",
    };
    $scope.filter = {Dealer:null, DealerId:null, Cabang:null, DateStart:periode, DateEnd:periode, tamarea:0,dealerarea:0,kategori:$stateParams.kategori,isGR:1};
    var filterCabang = {
      'kategori': 'General Repair'
    }

    RevenueGRFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
        $scope.filter.DealerId=response.data[0].dealerID;
        RevenueGRFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
          console.log('ayamtest',response.data)
          $scope.dataGroupDealerHO = response.data
          $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
          $scope.areaId($scope.dataGroupDealerHO[0])
        })
      })
    RevenueGRFactory.getDataCabang(filterCabang).then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
    RevenueGRFactory.getDataAreaTAM().then(function (response) {
        console.log('Response AreaTAM:', response.data);
        $scope.dataAreaTAM = response.data;
      })
    RevenueGRFactory.getDataAreaDealer().then(function (response) {
        console.log('Response AreaDealer:', response.data);
        $scope.dataAreaDealer = response.data;
      })
    RevenueGRFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
        console.log('Response AreaDealerHO:', response.data);
        $scope.dataAreaDealerHO = response.data;
        $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
        $scope.areaId($scope.dataAreaDealerHO[0])
      })
    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
            $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };
        // $scope.DisabledCabang = function() {
        //     if ($scope.dataCabang.length>1){
        //         $scope.Disabled =false;
        //         console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
        //     }
        //     else{
        //         $scope.Disabled =true;
        //         console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
        //     }
        // };
    // if ($scope.roleId==1128 || $scope.roleId==1129) {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", DateStart:null, DateEnd:null, tamarea:43,dealerarea:43,kategori:$stateParams.kategori};
    // } else {
        
    // }
        $scope.getData = function(){
            if ($scope.role == "Branch") $scope.getSummaryBranch();
            else $scope.getSummaryHO();
            console.log('get data');

        };

        $scope.getReport = function(){
            if ($scope.role == "Branch") $scope.getReportBranch();
            else $scope.getReportHO();
            console.log('get data');

        };

  function format1(n, currency) {
      return currency + " " + n.toFixed(0).replace(/./g, function(c, i, a) {
          return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
      });
    }

  function format2(n, currency) {
    return currency + " " + n.toFixed(2).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
  }
  // $scope.filter.dealerarea = null

  $scope.areaId = function(row) {
    // $scope.DataDealer = row
		$scope.DataDealer = Object.assign(row,filterCabang)
    if($scope.userlogin != 'TAM'){
      $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
    }else{
      $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
    }
    console.log('row dealerid', row,$scope.DataDealer);
    RevenueGRFactory.getDataCabangHONew($scope.DataDealer).then(function (response) {
      console.log('Response BECabang:', response.data);
      $scope.dataCabangHO = response.data;
      $scope.DisabledCabang();
      $scope.filter.CabangHO = null
      console.log('kambing',$scope.filter)
      $timeout(function() {
        $scope.filter.CabangHO = response.data[0].cabangID;
        console.log('kambing123',$scope.filter.CabangHO)
      }, 100);
    })
	};

  $scope.dealerCode = function(row){
    row = Object.assign(row,{'isGR':$scope.filter.isGR})
    $scope.filter.DealerId = row.dealerID
    RevenueGRFactory.getDataAreaDealerHO(row).then(function (response) {
        console.log('ayam',response.data)
        console.log('rusa',$scope.dataGroupDealer)
        $scope.dataGroupDealer = response.data
        $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
        $scope.areaId($scope.dataGroupDealer[0])
        console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
    })
}

  $scope.DisabledCabang = function() {
    if ($scope.dataCabang.length>1){
        $scope.Disabled =false;
        console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
    }else{
        $scope.Disabled =true;
        console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
    }
  };
  
    $scope.getSummaryHO = function(){
        var $tanggal=$scope.filter.DateStart;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
        var dataFilter = {
            'kategori': $scope.filter.kategori,
            'jenis': "summary",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'dealerarea': $scope.filter.dealerarea,};

          RevenueGRFactory.getSummaryHO(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            var hasil= response.data[0];
            if (response.data[0] !== undefined) {
                $scope.CPUSTotalUnit=hasil.cpusUnit;
                $scope.CPUSTotalUnit = Math.round($scope.CPUSTotalUnit * 100) / 100
                $scope.CPUSJasa=hasil.cpusJasa;
                $scope.CPUSJasa = Math.round($scope.CPUSJasa * 100) / 100
                $scope.CPUSJasa = format1($scope.CPUSJasa,'');
                $scope.CPUSPart=hasil.cpusParts;
                $scope.CPUSPart = Math.round($scope.CPUSPart * 100) / 100
                $scope.CPUSPart = format1($scope.CPUSPart,'');
                $scope.CPUSBahan=hasil.cpusBahan;
                $scope.CPUSBahan = Math.round($scope.CPUSBahan * 100) / 100
                $scope.CPUSBahan = format1($scope.CPUSBahan,'');
                $scope.CPUSOPL=hasil.cpusopl;
                $scope.CPUSOPL = Math.round($scope.CPUSOPL * 100) / 100
                $scope.CPUSOPL = format1($scope.CPUSOPL,'');
                $scope.CPUSOPB=hasil.cpusopb;
                $scope.CPUSOPB = Math.round($scope.CPUSOPB * 100) / 100
                $scope.CPUSOPB = format1($scope.CPUSOPB,'');
                $scope.CPUSRevenue=hasil.cpusRevenue;
                $scope.CPUSRevenue = Math.round($scope.CPUSRevenue * 100) / 100
                $scope.CPUSRevenue = format1($scope.CPUSRevenue,'');
                $scope.CPUSRevenueAvg=hasil.cpusUnit > 0 ? hasil.cpusRevenue/hasil.cpusUnit : 0;
                $scope.CPUSRevenueAvg = Math.round($scope.CPUSRevenueAvg * 100) / 100
                $scope.CPUSRevenueAvg = format2($scope.CPUSRevenueAvg,'');
                $scope.CPUSEMUnit=hasil.eM_Unit;
                // $scope.CPUSEMUnit = Math.round($scope.CPUSEMUnit * 100) / 100
                $scope.CPUSEMRevenue=hasil.eM_Revenue;
                $scope.CPUSEMRevenue = Math.round($scope.CPUSEMRevenue * 100) / 100
                $scope.CPUSEMRevenue = format1($scope.CPUSEMRevenue,'');
                $scope.CPUSEMRevenueAvg=hasil.eM_Unit > 0 ? hasil.eM_Revenue/hasil.eM_Unit : 0;
                $scope.CPUSEMRevenueAvg = parseFloat($scope.CPUSEMRevenueAvg ).toFixed(2);
                // $scope.CPUSEMRevenueAvg = Math.round($scope.CPUSEMRevenueAvg * 100) / 100
                // $scope.CPUSEMRevenueAvg = format1($scope.CPUSEMRevenueAvg,'');
                $scope.CPUSSBEUnit=hasil.sbE_Unit;
                // $scope.CPUSSBEUnit = Math.round($scope.CPUSSBEUnit * 100) / 100
                $scope.CPUSSBERevenue=hasil.sbE_Revenue;
                $scope.CPUSSBERevenue = Math.round($scope.CPUSSBERevenue * 100) / 100
                $scope.CPUSSBERevenue = format1($scope.CPUSSBERevenue,'');
                $scope.CPUSSBERevenueAvg= hasil.sbE_Unit > 0 ? hasil.sbE_Revenue/hasil.sbE_Unit : 0;
                $scope.CPUSSBERevenueAvg = parseFloat($scope.CPUSSBERevenueAvg).toFixed(2);
                // $scope.CPUSSBERevenueAvg = Math.round($scope.CPUSSBERevenueAvg * 100) / 100
                // $scope.CPUSSBERevenueAvg = format1($scope.CPUSSBERevenueAvg,'');
                $scope.CPUSGRUnit=hasil.gR_Unit;
                // $scope.CPUSGRUnit = Math.round($scope.CPUSGRUnit * 100) / 100
                $scope.CPUSGRRevenue=hasil.gR_Revenue;
                $scope.CPUSGRRevenue = Math.round($scope.CPUSGRRevenue * 100) / 100
                $scope.CPUSGRRevenue = format1($scope.CPUSGRRevenue,'');
                $scope.CPUSGRRevenueAvg=hasil.gR_Unit > 0 ? hasil.gR_Revenue/hasil.gR_Unit : 0;
                $scope.CPUSGRRevenueAvg = parseFloat($scope.CPUSGRRevenueAvg ).toFixed(2);
                // $scope.CPUSGRRevenueAvg = Math.round($scope.CPUSGRRevenueAvg * 100) / 100
                // $scope.CPUSGRRevenueAvg = format1($scope.CPUSGRRevenueAvg,'');

                $scope.NoCPUSUnit=hasil.nonCPUSUnit;
                $scope.NoCPUSUnit = Math.round($scope.NoCPUSUnit * 100) / 100
                $scope.NoCPUSPart=hasil.nonCPUSParts;
                $scope.NoCPUSPart = Math.round($scope.NoCPUSPart * 100) / 100
                $scope.NoCPUSPart = format1($scope.NoCPUSPart,'');
                $scope.NoCPUSOPB=hasil.nonCPUSOPB;
                $scope.NoCPUSOPB = Math.round($scope.NoCPUSOPB * 100) / 100
                $scope.NoCPUSOPB = format1($scope.NoCPUSOPB,'');
                $scope.NoCPUSRevenue=hasil.nonCPUSRevenue;
                $scope.NoCPUSRevenue = Math.round($scope.NoCPUSRevenue * 100) / 100
                $scope.NoCPUSRevenue = format1($scope.NoCPUSRevenue,'');
                $scope.NoCPUSJasa=hasil.nonCPUSJasa;
                $scope.NoCPUSJasa = Math.round($scope.NoCPUSJasa * 100) / 100
                $scope.NoCPUSJasa = format1($scope.NoCPUSJasa,'');
                $scope.NoCPUSBahan=hasil.nonCPUSBahan;
                $scope.NoCPUSBahan = Math.round($scope.NoCPUSBahan * 100) / 100
                $scope.NoCPUSBahan = format1($scope.NoCPUSBahan,'');
                $scope.NoCPUSOPL=hasil.nonCPUSOPL;
                $scope.NoCPUSOPL = Math.round($scope.NoCPUSOPL * 100) / 100
                $scope.NoCPUSOPL = format1($scope.NoCPUSOPL,'');
                // $scope.NoCPUSRevenueAvg=hasil.nonCPUSRevenue/5;
                $scope.NoCPUSRevenueAvg = hasil.nonCPUSUnit > 0 ? hasil.nonCPUSRevenue/hasil.nonCPUSUnit : 0;
                $scope.NoCPUSRevenueAvg = Math.round($scope.NoCPUSRevenueAvg * 100) / 100
                $scope.NoCPUSRevenueAvg = format2($scope.NoCPUSRevenueAvg,'');
            }
            else
            {
                 $scope.CPUSTotalUnit=hasil.cpusUnit;
                $scope.CPUSJasa=0;
                $scope.CPUSPart=0;
                $scope.CPUSBahan=0;
                $scope.CPUSOPL=0;
                $scope.CPUSOPB=0;
                $scope.CPUSRevenue=0;
                $scope.CPUSRevenueAvg=0;
                $scope.CPUSEMUnit=0;
                $scope.CPUSEMRevenue=0;
                $scope.CPUSEMRevenueAvg=0;
                $scope.CPUSSBEUnit=0;
                $scope.CPUSSBERevenue=0;
                $scope.CPUSSBERevenueAvg=0;
                $scope.CPUSGRUnit=0;
                $scope.CPUSGRRevenue=0;
                $scope.CPUSGRRevenueAvg=0;

                // $scope.NoCPUSUnit=hasil.;
                $scope.NoCPUSPart=0;
                $scope.NoCPUSOPB=0;
                $scope.NoCPUSRevenue=0;
                $scope.NoCPUSJasa=0;
                $scope.NoCPUSBahan=0;
                $scope.NoCPUSOPL=0;
            }

          })
      }

      $scope.getSummaryBranch = function(){
        var $tanggal=$scope.filter.DateStart;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        var $tanggalakhir=$scope.filter.DateEnd;
        var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
        var dataFilter = {
            'kategori': $scope.filter.kategori,
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal};

          RevenueGRFactory.getSummaryCabang(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            var hasil= response.data[0];
            $scope.CPUSRevenueAvg=0;
            $scope.CPUSEMRevenueAvg=0;
            $scope.CPUSSBERevenueAvg=0;
            $scope.CPUSGRRevenueAvg=0;
            $scope.NoCPUSRevenueAvg=0;
            if (response.data[0] !== undefined) {
                $scope.CPUSTotalUnit=hasil.cpusUnit;
                $scope.CPUSTotalUnit = Math.round($scope.CPUSTotalUnit * 100) / 100
                $scope.CPUSJasa=hasil.cpusJasa;
                $scope.CPUSJasa = Math.round($scope.CPUSJasa * 100) / 100
                $scope.CPUSJasa = format1($scope.CPUSJasa,'');
                $scope.CPUSPart=hasil.cpusParts;
                $scope.CPUSPart = Math.round($scope.CPUSPart * 100) / 100
                $scope.CPUSPart = format1($scope.CPUSPart,'');
                $scope.CPUSBahan=hasil.cpusBahan;
                $scope.CPUSBahan = Math.round($scope.CPUSBahan * 100) / 100
                $scope.CPUSBahan = format1($scope.CPUSBahan,'');
                $scope.CPUSOPL=hasil.cpusopl;
                $scope.CPUSOPL = Math.round($scope.CPUSOPL * 100) / 100
                $scope.CPUSOPL = format1($scope.CPUSOPL,'');
                $scope.CPUSOPB=hasil.cpusopb;
                $scope.CPUSOPB = Math.round($scope.CPUSOPB * 100) / 100
                $scope.CPUSOPB = format1($scope.CPUSOPB,'');
                $scope.CPUSRevenue=hasil.cpusRevenue;
                $scope.CPUSRevenue = Math.round($scope.CPUSRevenue * 100) / 100
                $scope.CPUSRevenue = format1($scope.CPUSRevenue,'');
                // if (hasil.cpusUnit>0) $scope.CPUSRevenueAvg=hasil.cpusRevenue/hasil.cpusUnit;
                $scope.CPUSRevenueAvg=hasil.cpusUnit > 0 ? hasil.cpusRevenue/hasil.cpusUnit : 0;
                $scope.CPUSRevenueAvg = Math.round($scope.CPUSRevenueAvg * 100) / 100
                $scope.CPUSRevenueAvg = format2($scope.CPUSRevenueAvg,'');
                $scope.CPUSEMUnit=hasil.eM_Unit;
                // $scope.CPUSEMUnit = Math.round($scope.CPUSEMUnit * 100) / 100
                $scope.CPUSEMRevenue=hasil.eM_Revenue;
                $scope.CPUSEMRevenue = Math.round($scope.CPUSEMRevenue * 100) / 100
                $scope.CPUSEMRevenue = format1($scope.CPUSEMRevenue,'');
                // if (hasil.eM_Unit>0) 
                // $scope.CPUSEMRevenueAvg=hasil.eM_Revenue/hasil.eM_Unit;
                $scope.CPUSEMRevenueAvg=hasil.eM_Unit > 0 ? hasil.eM_Revenue/hasil.eM_Unit : 0;
                // $scope.CPUSEMRevenueAvg = Math.round($scope.CPUSEMRevenueAvg * 100) / 100
                // $scope.CPUSEMRevenueAvg = format1($scope.CPUSEMRevenueAvg,'');
                $scope.CPUSEMRevenueAvg = parseFloat($scope.CPUSEMRevenueAvg).toFixed(2);
                // $scope.CPUSEMRevenueAvg = format2($scope.CPUSEMRevenueAvg,'');
                $scope.CPUSSBEUnit=hasil.sbE_Unit;
                //$scope.CPUSSBEUnit = Math.round($scope.CPUSSBEUnit * 100) / 100
                $scope.CPUSSBERevenue=hasil.sbE_Revenue;
                $scope.CPUSSBERevenue = Math.round($scope.CPUSSBERevenue * 100) / 100
                $scope.CPUSSBERevenue = format1($scope.CPUSSBERevenue,'');
                // if (hasil.sbE_Unit>0) 
                // $scope.CPUSSBERevenueAvg=hasil.sbE_Revenue/hasil.sbE_Unit;
                $scope.CPUSSBERevenueAvg= hasil.sbE_Unit > 0 ? hasil.sbE_Revenue/hasil.sbE_Unit : 0;
                // $scope.CPUSSBERevenueAvg = Math.round($scope.CPUSSBERevenueAvg * 100) / 100
                // $scope.CPUSSBERevenueAvg = format1($scope.CPUSSBERevenueAvg,'');
                $scope.CPUSEMRevenueAvg = parseFloat($scope.CPUSEMRevenueAvg).toFixed(2);
                // $scope.CPUSSBERevenueAvg = format2($scope.CPUSSBERevenueAvg,'');
                $scope.CPUSGRUnit=hasil.gR_Unit;
                $scope.CPUSGRUnit = Math.round($scope.CPUSGRUnit * 100) / 100
                $scope.CPUSGRRevenue=hasil.gR_Revenue;
                //$scope.CPUSGRRevenue = Math.round($scope.CPUSGRRevenue * 100) / 100
                $scope.CPUSGRRevenue = format1($scope.CPUSGRRevenue,'');
                // if (hasil.gR_Unit>0) 
                // $scope.CPUSGRRevenueAvg=hasil.gR_Revenue/hasil.gR_Unit;
                $scope.CPUSGRRevenueAvg=hasil.gR_Unit > 0 ? hasil.gR_Revenue/hasil.gR_Unit : 0;
                // $scope.CPUSGRRevenueAvg = Math.round($scope.CPUSGRRevenueAvg * 100) / 100
                // $scope.CPUSGRRevenueAvg = format1($scope.CPUSGRRevenueAvg,'');
                // $scope.CPUSGRRevenueAvg = Math.round($scope.CPUSGRRevenueAvg * 100 ) / 100
                $scope.CPUSGRRevenueAvg = parseFloat($scope.CPUSGRRevenueAvg ).toFixed(2);
                $scope.NoCPUSUnit=hasil.nonCPUSUnit;
                $scope.NoCPUSUnit = Math.round($scope.NoCPUSUnit * 100) / 100
                $scope.NoCPUSPart=hasil.nonCPUSParts;
                $scope.NoCPUSPart = Math.round($scope.NoCPUSPart * 100) / 100
                $scope.NoCPUSPart = format1($scope.NoCPUSPart,'');
                $scope.NoCPUSOPB=hasil.nonCPUSOPB;
                $scope.NoCPUSOPB = Math.round($scope.NoCPUSOPB * 100) / 100
                $scope.NoCPUSOPB = format1($scope.NoCPUSOPB,'');
                $scope.NoCPUSRevenue=hasil.nonCPUSRevenue;
                $scope.NoCPUSRevenue = Math.round($scope.NoCPUSRevenue * 100) / 100
                $scope.NoCPUSRevenue = format1($scope.NoCPUSRevenue,'');
                $scope.NoCPUSJasa=hasil.nonCPUSJasa;
                $scope.NoCPUSJasa = Math.round($scope.NoCPUSJasa * 100) / 100
                $scope.NoCPUSJasa = format1($scope.NoCPUSJasa,'');
                $scope.NoCPUSBahan=hasil.nonCPUSBahan;
                $scope.NoCPUSBahan = Math.round($scope.NoCPUSBahan * 100) / 100
                $scope.NoCPUSBahan = format1($scope.NoCPUSBahan,'');
                $scope.NoCPUSOPL=hasil.nonCPUSOPL;
                $scope.NoCPUSOPL = Math.round($scope.NoCPUSOPL * 100) / 100
                $scope.NoCPUSOPL = format1($scope.NoCPUSOPL,'');
                // if (hasil.nonCPUSUnit>0) $scope.NoCPUSRevenueAvg=hasil.nonCPUSRevenue/5;
                $scope.NoCPUSRevenueAvg = hasil.nonCPUSUnit > 0 ? hasil.nonCPUSRevenue/hasil.nonCPUSUnit : 0;
                $scope.NoCPUSRevenueAvg = Math.round($scope.NoCPUSRevenueAvg * 100) / 100
                $scope.NoCPUSRevenueAvg = format2($scope.NoCPUSRevenueAvg,'');
            }
            else
            {
                $scope.CPUSTotalUnit=0;
                $scope.CPUSJasa=0;
                $scope.CPUSPart=0;
                $scope.CPUSBahan=0;
                $scope.CPUSOPL=0;
                $scope.CPUSOPB=0;
                $scope.CPUSRevenue=0;
                $scope.CPUSRevenueAvg=0;
                $scope.CPUSEMUnit=0;
                $scope.CPUSEMRevenue=0;
                $scope.CPUSEMRevenueAvg=0;
                $scope.CPUSSBEUnit=0;
                $scope.CPUSSBERevenue=0;
                $scope.CPUSSBERevenueAvg=0;
                $scope.CPUSGRUnit=0;
                $scope.CPUSGRRevenue=0;
                $scope.CPUSGRRevenueAvg=0;

                $scope.NoCPUSUnit=0;
                $scope.NoCPUSPart=0;
                $scope.NoCPUSOPB=0;
                $scope.NoCPUSRevenue=0;
                $scope.NoCPUSJasa=0;
                $scope.NoCPUSBahan=0;
                $scope.NoCPUSOPL=0;
            }

          })
      }

      $scope.getReportHO = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
          var $tanggalakhir=$scope.filter.DateEnd;
          var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
          var dataReport = {
            'kategori': $scope.filter.kategori,
            'jenis': "export",
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'tamarea': $scope.filter.tamarea,
            'dealerarea': $scope.filter.dealerarea,};

        RevenueGRFactory.getExportHO(dataReport);
    }
    $scope.getReportBranch = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
          var $tanggalakhir=$scope.filter.DateEnd;
          var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
          var dataReport = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal};

        RevenueGRFactory.getExportCabang(dataReport);
    }

});
