angular.module('app')
  .factory('RevenueGRFactory', function($http, CurrentUser,$q) {
    console.log('Service Revenue GR Loaded...');
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL ='';// 'http://localhost:8080';
    return {
            getDataDealer: function() {
                  var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
                  return res;},
            // getDataCabang: function() {
            //       var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            //       return res;},
            getDataCabang: function(data) {
              console.log('Data Get Cabang:', data);
              var res=$http.get(serverURL + '/api/rpt/Cabang/'+  currentUser.OrgId + '/' + data.kategori);
              return res;},
            getDataCabangHONew: function(data) {
              console.log('Data Get Cabang:', data);
              var res=$http.get(serverURL + '/api/rpt/GetCabangHONew/'+ data.areaId + '/' +  data.DealerId + '/' + data.kategori);
              return res;},
            getDataAreaTAM: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaTAM/');
            return res;},
            getDataAreaDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaDealer/');
            return res;},
            getDataAreaDealerHO: function(data) {
            var res=$http.get(serverURL + '/api/rpt/AreaDealerHONew/'+ data.isGR );
            return res;},
            getSummaryHO: function(data) {
              console.log('Data Report:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_Revenue_GRHO/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.tamarea +'/'+ data.dealerarea +'/'+ data.kategori +'/'+ data.jenis);
                return res;
            },

            getExportHO: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_Revenue_GRHO?TamArea='+ data.tamarea +'&DealerArea='+ data.dealerarea +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir +'&kategori='+ data.kategori +'&Jenis='+ data.jenis);
            },
            getSummaryCabang: function(data) {
              console.log('Data Report:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_Revenue_GRCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periodeawal +'/'+ data.periodeakhir +'/'+ data.kategori);
                return res;
            },

            getExportCabang: function(data) {
                console.log('Data Report:', data);
                window.open(serverURL + '/api/rpt/AS_Revenue_GRCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir );
            }


    }
  });
