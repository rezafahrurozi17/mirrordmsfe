angular.module('app')
    .controller('WorkshopGRController', function($scope, $http, CurrentUser, WorkshopGRFactory) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        // $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;

    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    var dateFormat='MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.filter = {Dealer:null, Cabang:null, DateStart:periode, DateEnd:periode, kategori:"General Repair", labour:0,overhead:0, depreciation:0};
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
        
        // mode:"'month'",
    };

      WorkshopGRFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
      })
      WorkshopGRFactory.getDataCabang().then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
      $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
    $scope.getData = function(){
    }

    $scope.simpan = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
            var dataFilter = {                
                'cabang': $scope.filter.Cabang,
                'periode': $filtertanggal,
                'labour':$scope.filter.labour,
                'overhead':$scope.filter.overhead,
                'depreciation':$scope.filter.depreciation
                
            };
            alert('Data berhasil di simpan.');
                
            WorkshopGRFactory.save(dataFilter).then(function(response) {
                console.log('Simpan:', response.data);
                // $scope.dataSummary = response.data;
                var hasil = response.data[0];
                if (response.data[0] !== undefined) {
                    $scope.filter.labour=hasil.labour;
                    $scope.filter.overhead=hasil.overhead;
                    $scope.filter.depreciation=hasil.depreciation;
                }
                else
                {
                    $scope.filter.labour=0;
                    $scope.filter.overhead=0;
                    $scope.filter.depreciation=0;
                }
            })
        }
        $scope.tampilkan = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
            var dataFilter = {                
                'cabang': $scope.filter.Cabang,
                'periode': $filtertanggal,
                // 'labour':0,
                // 'overhead':0,
                // 'depreciation':0
            };

            WorkshopGRFactory.tampilData(dataFilter).then(function(response) {
                console.log('Tampilkan:', response.data);
                // $scope.dataSummary = response.data;
                var hasil = response.data[0];
                if (response.data[0] !== undefined) {
                    $scope.filter.labour=hasil.labour;
                    $scope.filter.overhead=hasil.overhead;
                    $scope.filter.depreciation=hasil.depreciation;
                }
                else
                {
                    $scope.filter.labour=0;
                    $scope.filter.overhead=0;
                    $scope.filter.depreciation=0;
                }
            })

        }
    $scope.getReport = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
          var $tanggalakhir=$scope.filter.DateStart;
          var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
          var dataReport = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'kategori': $scope.filter.kategori};

        WorkshopGRFactory.getSummaryExport(dataReport);
    }
    
});
