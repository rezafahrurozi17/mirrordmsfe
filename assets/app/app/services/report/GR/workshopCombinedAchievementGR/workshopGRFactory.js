angular.module('app')
  .factory('WorkshopGRFactory', function($http, CurrentUser,$q) {
   console.log('Service WorkshopGR Loaded...');
    var serverURL = ''; //http://localhost:8080/api/'
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    return {
      getDataDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},
      getDataCabang: function() {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            return res;},
      getData: function() {
        var res=$http.get('/api/fw/Role');        
        console.log('data=>',res);
        //---------
        return $q.resolve(data);
      },
            save: function(data) {
              console.log('Data Filter:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_WorkshopCombinedAchievement_GRCAB/'+ data.cabang +'/'+ data.periode +'/'+ data.labour  +'/'+ data.overhead +'/'+ data.depreciation);
              return res;},
            
            tampilData: function(data) {
              console.log('Data Filter:', data);
              var res = $http.get(serverURL + '/api/rpt/GetDataWorkshopCombained/'+ data.cabang +'/'+ data.periode);
              return res;},  

            getSummaryExport: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_WorkshopCombinedAchievement_GRCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&kategori='+ data.kategori +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir);
            }
    
 
    }
  });