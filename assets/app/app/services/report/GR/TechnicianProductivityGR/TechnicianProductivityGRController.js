angular.module('app')
    .controller('TechnicianProductivityGRController', function($scope,$stateParams, $http, CurrentUser, TechnicianProductivityGRFactory,$timeout ) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
      $scope.loading=false;
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;
    $scope.role=$stateParams.role;
    // console.log('role',$scope.role);
    $scope.filter = {kategori:'General Repair'  };
    var dateFormat='dd/MM/yyyy';
    if ($scope.role== 'HO') dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
        // mode:"'month'",
    };
    $scope.filter = {Dealer:null,DealerId:null, Cabang:null, DateStart:periode, DateEnd:periode, tamarea:0,dealerarea:0,kategori:'General Repair',isGR:1};
    // if ($scope.role='HO') $scope.dateOptions.format='MM/yyyy';
    TechnicianProductivityGRFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
        $scope.filter.DealerId=response.data[0].dealerID;
        TechnicianProductivityGRFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
          console.log('ayamtest',response.data)
          $scope.dataGroupDealerHO = response.data
          $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
          $scope.areaId($scope.dataGroupDealerHO[0])
        })
      })
    TechnicianProductivityGRFactory.getDataCabang($scope.filter).then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
    TechnicianProductivityGRFactory.getDataAreaTAM().then(function (response) {
        console.log('Response AreaTAM:', response.data);
        $scope.dataAreaTAM = response.data;
      })
    TechnicianProductivityGRFactory.getDataAreaDealer().then(function (response) {
        console.log('Response AreaDealer:', response.data);
        $scope.dataAreaDealer = response.data;
      })
    TechnicianProductivityGRFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
        console.log('Response AreaDealerHO:', response.data);
        $scope.dataAreaDealerHO = response.data;
        $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
        $scope.areaId($scope.dataAreaDealerHO[0])
      })
    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
            $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };


        $scope.areaId = function(row) {
          // $scope.DataDealer = row
          $scope.DataDealer = Object.assign(row,{'kategori': $scope.filter.kategori})
          if($scope.userlogin != 'TAM'){
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
          }else{
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
          }
          console.log('row dealerid', row,$scope.DataDealer);
          TechnicianProductivityGRFactory.getDataCabangHONew($scope.DataDealer).then(function (response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabangHO = response.data;
            $scope.DisabledCabang();
            $scope.filter.CabangHO = null
            console.log('kambing',$scope.filter)
            $timeout(function() {
              $scope.filter.CabangHO = response.data[0].cabangID;
              console.log('kambing123',$scope.filter.CabangHO)
            }, 100);
          })
        };

        $scope.dealerCode = function(row){
          row = Object.assign(row,{'isGR':$scope.filter.isGR})
          $scope.filter.DealerId = row.dealerID
          TechnicianProductivityGRFactory.getDataAreaDealerHO(row).then(function (response) {
              console.log('ayam',response.data)
              console.log('rusa',$scope.dataGroupDealer)
              $scope.dataGroupDealer = response.data
              $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
              $scope.areaId($scope.dataGroupDealer[0])
              console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
          })
        }

        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
    // if ($scope.roleId==1128 || $scope.roleId==1129) {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", DateStart:null, DateEnd:null, tamarea:43,dealerarea:43,kategori:'General Repair'  };
    // } else {

    // }

        $scope.getData = function(){
            if ($scope.role == "Branch") $scope.getDataCabang(); else $scope.getDataHO();
            console.log('get data');

        };
    $scope.getDataCabang = function() {
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'kategori': $scope.filter.kategori,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,
            'Jenis': 'summary'};

          TechnicianProductivityGRFactory.getSummaryCabang(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;
            var hasil=response.data[0];
            $scope.sumHariKerja = hasil.hariKerja;
            $scope.sumJT = hasil.jamTersedia;
            // $scope.sumJT1 = Math.round(hasil.teknisi/hasil.hariKerja);
            $scope.sumJT1 = parseFloat(hasil.rata2kehadiran).toFixed(2);
            // $scope.sumJT1 = Math.round( $scope.sumJT1 * 100) / 100;
            $scope.sumUE = hasil.unitEntry;
            $scope.sumAvgTeknisi = hasil.rata2kehadiran;
            $scope.sumJF = hasil.jamTerjual;
            $scope.sumRTJ = hasil.rtj;
            $scope.sumtotalHariKerja = hasil.totalHariKerja;
            $scope.sumJA = hasil.jamTerpakai;
            //   $scope.proRUM = hasil.teknisi > 0 ? hasil.unitEntry / hasil.teknisi : 0;
            $scope.proRUM = $scope.sumJT1 > 0 ? hasil.unitEntry / (hasil.jmltknisi * $scope.sumHariKerja) : 0;
            console.log("hasil.teknisi",hasil.teknisi);
            //   $scope.proRUM = Math.round($scope.proRUM * 100) / 100
            $scope.proRUM = parseFloat($scope.proRUM).toFixed(2);
            $scope.proRTJ = hasil.unitEntry > 0 ? hasil.rtj/hasil.unitEntry : 0;
            //   $scope.proRTJ = Math.round($scope.proRTJ * 100) / 100
            $scope.proRTJ = parseFloat($scope.proRTJ * 100).toFixed(2);
             $scope.proTE = hasil.jamTerpakai > 0 ? hasil.jamTerjual/hasil.jamTerpakai : 0;
            //  $scope.proTE = (hasil.jf/hasil.ja);
             $scope.proTE = parseFloat($scope.proTE * 100).toFixed(2);
             //$scope.proTE = Math.round($scope.proTE * 100) / 100;
             $scope.proLU = hasil.jamTersedia > 0 ? hasil.jamTerpakai/hasil.jamTersedia : 0;
             //$scope.proLU = Math.round($scope.proLU * 100) / 100;
            //  $scope.proLU = (hasil.ja/hasil.jt);
             $scope.proLU = parseFloat($scope.proLU * 100).toFixed(2);
             $scope.proOP = hasil.jamTersedia > 0 ? hasil.jamTerjual/hasil.jamTersedia : 0;
            //  $scope.proOP = (hasil.jf/hasil.jt);
             $scope.proOP = parseFloat($scope.proOP * 100).toFixed(2);
             //$scope.proOP = Math.round($scope.proOP * 100)
             // TotalHariKerja,JamTersedia,JamTerjual,JamTerpakai,UnitEntry,RTJ
          })
      }
      $scope.getReportCabang = function(){
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'kategori': $scope.filter.kategori,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,
            'Jenis': 'export'};

        TechnicianProductivityGRFactory.getExportCabang(dataFilter);
    }

    $scope.getDataHO = function() {
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'tamarea': $scope.filter.tamarea,
            'dealerarea': $scope.filter.dealerarea,
            'kategori': $scope.filter.kategori,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,
            'Jenis': 'summary'};
            $scope.proTE = 0;
            $scope.proLU = 0;
            $scope.proOP = 0;
            $scope.proRTJ= 0;
            $scope.proRUM = 0;
          TechnicianProductivityGRFactory.getSummaryHO(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;
            var hasil=response.data[0];
            $scope.sumHariKerja = hasil.hariKerja;
            $scope.sumHariKerja = Math.round($scope.sumHariKerja * 100) / 100
            $scope.sumJT1 = Math.round(hasil.teknisi/hasil.hariKerja);
            $scope.sumUE = hasil.unitEntry;
            $scope.sumUE = Math.round($scope.sumUE * 100) / 100
            $scope.sumJT = hasil.jamTersedia;
            $scope.sumJT = Math.round($scope.sumJT * 100) / 100
            $scope.sumJF = hasil.jamTerjual;
            $scope.sumJF = Math.round($scope.sumJF * 100) / 100
            $scope.sumJA = hasil.jamTerpakai;
            $scope.sumJA = Math.round($scope.sumJA * 100) / 100
            $scope.sumRTJ = hasil.returnJob;  
            $scope.sumRTJ = Math.round($scope.sumRTJ * 100) / 100

            // if ((hasil.teknisi*hasil.hariKerja)>0) $scope.proRUM = hasil.unitEntry/(hasil.teknisi*hasil.hariKerja);
            // $scope.proRUM = Math.round($scope.proRUM * 100) / 100   
            var TmpCabangName = []
            for(var i in $scope.dataCabang){
              if($scope.dataCabang[i].cabangID == dataFilter.cabang){
                TmpCabangName = $scope.dataCabang[i].cabangName
              }
            }         
            if(TmpCabangName == 'ALL'){
              console.log('kambing',hasil)
              $scope.proRTJ = hasil.rtj
              $scope.proTE = hasil.te
              $scope.proLU = hasil.lu
              $scope.proOP = hasil.op
              $scope.proRUM = hasil.rum
              
              $scope.proTE = parseFloat($scope.proTE * 100 / 100).toFixed(2);
              $scope.proLU = parseFloat($scope.proLU * 100 / 100).toFixed(2);
              $scope.proOP = parseFloat($scope.proOP * 100 / 100).toFixed(2);
            }else{
              $scope.proRUM = $scope.sumJT1 > 0 ? hasil.unitEntry / (hasil.jmlTeknisi * $scope.sumHariKerja) : 0;
              $scope.proRUM = parseFloat($scope.proRUM).toFixed(2);
              if (hasil.unitEntry>0) $scope.proRTJ = (hasil.returnJob/hasil.unitEntry)*100;
              $scope.proRTJ = Math.round($scope.proRTJ * 100) / 100
              if (hasil.jamTerpakai>0)
              {
                  // $scope.proTE = (hasil.jamTerjual/hasil.jamTerpakai)*100;
                  // $scope.proLU = (hasil.jamTerpakai/hasil.jamTersedia)*100;
                  // $scope.proOP = (hasil.jamTerjual/hasil.jamTersedia)*100;
                  $scope.proTE = (hasil.jamTerjual/hasil.jamTerpakai);
                  $scope.proLU = (hasil.jamTerpakai/hasil.jamTersedia);
                  $scope.proOP = (hasil.jamTerjual/hasil.jamTersedia);

                  
                  $scope.proTE = parseFloat($scope.proTE * 100).toFixed(2);
                  $scope.proLU = parseFloat($scope.proLU * 100).toFixed(2);
                  $scope.proOP = parseFloat($scope.proOP * 100).toFixed(2);
              }
            }
            
            // $scope.proTE = Math.round($scope.proTE * 100) / 100
            // $scope.proTE = parseFloat($scope.proTE).toFixed(2);
            // $scope.proLU = Math.round($scope.proLU * 100) / 100
            // $scope.proLU = parseFloat($scope.proLU).toFixed(2);
            // $scope.proOP = Math.round($scope.proOP * 100) / 100
            // $scope.proOP = parseFloat($scope.proOP).toFixed(2);
            // $scope.proTE = parseFloat($scope.proTE * 100).toFixed(2);
            // $scope.proLU = parseFloat($scope.proLU * 100).toFixed(2);
            // $scope.proOP = parseFloat($scope.proOP * 100).toFixed(2);


            
          })
      }
      $scope.getReportHO = function(){
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'tamarea': $scope.filter.tamarea,
            'dealerarea': $scope.filter.dealerarea,
            'kategori': $scope.filter.kategori,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,
            'Jenis': 'export'};

        TechnicianProductivityGRFactory.getExportHO(dataFilter);
    }

});
