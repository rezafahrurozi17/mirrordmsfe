angular.module('app')
  .factory('TechnicianProductivityGRFactory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL ='';// 'http://localhost:8080';
    return {
      getDataDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},
      // getDataCabang: function() {
      //       var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
      //       return res;},
      getDataCabang: function(data) {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId + '/' + data.kategori);
            return res;},
      getDataCabangHONew: function(data) {
        console.log('Data Get Cabang:', data);
        var res=$http.get(serverURL + '/api/rpt/GetCabangHONew/'+ data.areaId + '/' +  data.DealerId + '/' + data.kategori);
        return res;},
      getDataAreaTAM: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaTAM/');
            return res;},
      getDataAreaDealer: function() {
      var res=$http.get(serverURL + '/api/rpt/AreaDealer/');
      return res;},
      getDataAreaDealerHO: function(data) {
      var res=$http.get(serverURL + '/api/rpt/AreaDealerHONew/'+ data.isGR );
      return res;},
      getData: function() {
        var res=$http.get('/api/fw/Role');
        console.log('data=>',res);
        //---------
        return $q.resolve(data);
      },
      getSummaryCabang: function(data) {
              console.log('Data Filter:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_TechnicianProductivity_GRCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.kategori +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/'+ data.Jenis );
              return res;},
      getExportCabang: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_TechnicianProductivity_GRCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periode_awal +'&dateend='+ data.periode_akhir +'&kategori='+ data.kategori +'&jenis='+ data.Jenis);
            },
      getSummaryHO: function(data) {
              console.log('Data Filter:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_TechnicianProductivity_GRHO/'+ data.tamarea +'/'+ data.dealerarea +'/'+ data.dealer +'/'+ data.cabang +'/'+ data.kategori +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/'+ data.Jenis);
              return res;},
      getExportHO: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_TechnicianProductivity_GRHO?TamArea='+ data.tamarea +'&DealerArea='+ data.dealerarea +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periode_awal +'&dateend='+ data.periode_akhir +'&kategori='+ data.kategori +'&Jenis='+ data.Jenis);
            }
    }
  });
