angular.module('app')
    .controller('reportOPLController', function($scope, $http, CurrentUser, reportOPLFactory, bsNotify) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        // $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    console.log('user ====>',$scope.user)
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.maxDate = new Date(periode.getFullYear(), periode.getMonth(), periode.getDate());
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
        
        // mode:"'month'",
        // disableWeekend: 1
    };
    $scope.dataDealer = [];
    $scope.dataCabang = [];
    $scope.roleId = $scope.user.RoleId;

    reportOPLFactory.getDataPIC('service advisor ' + $scope.role).then(function(response) {
        console.log('Response BEPIC:', response.data);
        $scope.dataSA = response.data;
    })  

    $scope.onSelectSA = function(selected){
        console.log('onSelectSA ===>',selected);
    }


    $scope.generateKategori = function(outletId){
        if(outletId < 2000000){
            return "General Repair"
        }else if(outletId >= 2000000){
            return "Body and Paint"
        }
    }


    

    $scope.summary = {};
    $scope.filter = {
        Dealer:null, 
        Cabang:null, 
        kategori: $scope.generateKategori($scope.user.OutletId),
        DateStart:periode, 
        DateEnd:periode, 
        employeeName: "All",
        employeeid:0,
        IsWO: 0,
    };
    $scope.filterCategoryDate =  { data: [
        { text: "Tanggal WO", value: 1 }, 
        { text: "Tanggal OPL", value: 2 }, 
    ] };    
    
    $scope.selectedFilterCategory = "Tanggal WO";


    $scope.changeCategoryFilterDate = function(selected){
        selected = parseInt(selected);
        $scope.filter.categoryDate = selected;
        if(selected == 1){
            console.log('changeKategoryFilter by WO ===>',selected);
            $scope.selectedFilterCategory = 'Tanggal WO';
            $scope.filter.categoryDateName = 'Tanggal WO';
            $scope.filter.IsWO = 0;
        }else{
            console.log('changeKategoryFilter by OPL ===>',selected);
            $scope.selectedFilterCategory = "Tanggal OPL";
            $scope.filter.categoryDateName = "Tanggal OPL";
            $scope.filter.IsWO = 1;

        }
    }

    $scope.changeCategoryFilterDate($scope.filterCategoryDate.data[1].value);

    // if ($scope.roleId==1128 || $scope.roleId==1129) {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", kategori:"General Repair", DateStart:null, DateEnd:null, JenisFA:'All',StatusFA:'All'};
    // } else {
    // }    
    reportOPLFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.getDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
    })
    reportOPLFactory.getDataCabang().then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.getCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
    })



    // $scope.roleId = 1; // 2 = HO | 1 = Branch
    $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.getCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.getCabang.length + ' ' + $scope.Disabled);
            }
        };
    $scope.getData = function() {
        console.log('filterr ===>',$scope.filter)

        if( $scope.filter.employeeid == undefined){
            bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "Nama SA belum dipilih",
            });
        }else if( $scope.filter.categoryDate == undefined){
            bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "Kategori Tangggal belum dipilih",
            });
        }else if($scope.filter.DateEnd == undefined){
            bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "Tanggal Akhir Filter harus diisi",
            });
        }else if($scope.filter.DateStart == undefined){
            bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "Tanggal Awal Filter harus diisi",
            });
        }else{


            var $tanggal1=$scope.filter.DateStart;
            var $tanggal2=$scope.filter.DateEnd;
            var $filtertanggal1= $tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
            var $filtertanggal2= $tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
            var dataFilter = {
                'IsWO': $scope.filter.IsWO,
                'Dealer': $scope.filter.Dealer,
                'Cabang': $scope.filter.Cabang,
                'OutletId': $scope.user.OutletId,
                'EmployeeId': $scope.filter.employeeid,//nama SA    
                'StartDate': $filtertanggal1,
                'EndDate': $filtertanggal2,                        
            };
    
            reportOPLFactory.getSummaryFilter(dataFilter).then(function(response) {
                console.log('Response BE Filter:', response.data);
                // $scope.dataSummary = response.data;
                var hasil=response.data[0];
                $scope.totalUnitRepair = hasil;
                // $scope.totalUnitRepair = JSON.parse(JSON.stringify(hasil).replace(/\s(?=\w+":)/g,""));
                console.log("T1",$scope.totalUnitRepair);
                console.log("T23",$scope.totalUnitRepair['total OPL External']);
            });

            // WoHistoryBP.checkApproval($scope.totalDp).then(function(res) {
            //     console.log("ressss checkApproval", res.data);
            //     ApproverRoleID = res.data;
            //     if (res.data !== -1) {
            //         withDpApproval = 1;
            //     }
            // });
        }
    }
    $scope.getReport = function(){
        console.log('summary ===>',$scope.summary);


        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        // var dataFilter = {
        //     'dealer': $scope.filter.Dealer,
        //     'cabang': $scope.filter.Cabang,
        //     'kategori_workshop': $scope.filter.kategori,
        //     'periode_awal': $filtertanggal1,
        //     'periode_akhir': $filtertanggal2,                        
        //     'statusfa': $scope.filter.StatusFA,
        //     'jenisfa': $scope.filter.JenisFA};
        var dataFilter = {
            'IsWO': $scope.filter.IsWO,
            'Dealer': $scope.filter.Dealer,
            'Cabang': $scope.filter.Cabang,
            'OutletId': $scope.user.OutletId,
            'EmployeeId': $scope.filter.employeeid,//nama SA    
            'StartDate': $filtertanggal1,
            'EndDate': $filtertanggal2                        
        };

        reportOPLFactory.getSummaryExport(dataFilter).then(
            function(res){
                window.open(res.config.url);
            }
        );
    }



    
});
