angular.module('app')
  .factory('reportOPLFactory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    var serverURL = ''; //http://localhost:8080';
    return {
        getDataDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;
        },
        getDataCabang: function() {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            return res;
        },
        getSummaryFilter: function(data) {
            console.log('Data Filter:', data);
            // console.log('periode',moment(data.periode).format('YYYY-MM-DD'))
            // https://dms-sit.toyota.astra.co.id/api/rpt/AS_OPLINTERNALEXTERNAL_GRCAB/1/TUT/280/1/2019-08-05/2020-08-05
            var res = $http.get(serverURL + '/api/rpt/AS_OPLINTERNALEXTERNAL_GRCAB/'+ data.IsWO +'/'+ data.Dealer  +'/'+ data.OutletId +'/'+ data.EmployeeId +'/'+ data.StartDate +'/'+ data.EndDate );
            return res;
        },
        getDataPIC: function(data) {
            console.log('data pic',data);
            var res=$http.get(serverURL + '/api/rpt/pic/'+ currentUser.OrgId +'/'+ data);
            return res;
        },
        getSummaryExport: function(data) {
              console.log('Data Report:', data);
              // window.open(serverURL + '/api/rpt/AS_GetOPLInternalExternal/'+ data.IsWO +'/'+ data.Dealer  +'/'+ data.OutletId +'/'+ data.EmployeeId +'/'+ data.StartDate +'/'+ data.EndDate + '/export');
              // https://dms-sit.toyota.astra.co.id/api/rpt/AS_OPLINTERNALEXTERNAL_GRCAB?IsWO=1&GroupDealerCode=TUT&OutletId=280&EmployeeId=1&StartDate=2019/8/5&EndDate=2020/8/5
              var res = $http.get(serverURL + '/api/rpt/AS_OPLINTERNALEXTERNAL_GRCAB?IsWO='+ data.IsWO +'&GroupDealerCode='+ data.Dealer  +'&OutletId='+ data.OutletId +'&EmployeeId='+ data.EmployeeId +'&StartDate='+ data.StartDate +'&EndDate='+ data.EndDate);
              return res;
            }

    }
  });
