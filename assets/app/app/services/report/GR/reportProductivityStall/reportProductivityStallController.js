angular.module('app')
    .controller('ReportGRProductivityStallController', function($scope, $stateParams,$http, CurrentUser, ReportProductivityStallFactory,ReportProductivityStallFactory,CAccessMatrix,$timeout) {
    console.log('report productivity');

	$scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
         $scope.getOrgData();
    });

    $scope.orgData = [];
	$scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;
    $scope.role=$stateParams.role;
    console.log('role',$scope.role);
    //var $item=angular.fromJson( $stateParams.tab.item);
    //$scope.roleId = $item.RoleId;
    //console.log('report Productivity Stall GR',$item.RoleId);
    var dateFormat2='dd/MM/yyyy';
    var dateFormat='MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
        // mode:"'month'",
    };
        $scope.dateOptions2S = {
        startingDay: 1,
        format: dateFormat,
        // mode:"'month'",
    };
    $scope.dateOptions2E = {
        startingDay: 1,
        format: dateFormat2,
        // mode:"'month'",
    };


    //===============validasi date start and date end===========
    var today = new Date();
    $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(), today.getDate());
    $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());
    $scope.minDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());


    $scope.changeStartDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.DateStart);
        var dayTwo = new Date($scope.filter.DateEnd);

        $scope.dateOptions2E.minDate = new Date(dayOne.getFullYear(),dayOne.getMonth(), dayOne.getDate());
        if (tgl == null || tgl == undefined){
            $scope.filter.DateEnd = null;
        } else {
            if ($scope.filter.DateStart < $scope.filter.DateEnd){

            } else {
                if (dayOne > today){
                    $scope.filter.DateStart = today;
                    $scope.filter.DateEnd = $scope.filter.DateStart;
                } else {
                    $scope.filter.DateEnd = $scope.filter.DateStart;
                    $scope.dateOptions2E.maxDate = new Date(dayOne.getFullYear(),dayOne.getMonth(),dayOne.getDate());
                    if ($scope.dateOptions2E.maxDate > today) {
                        $scope.dateOptions2E.maxDate = new Date(today.getFullYear(),today.getMonth(),today.getDate());
                    }
                }
            }
        }

    }

    $scope.changeEndDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.DateStart);
        var dayTwo = new Date($scope.filter.DateEnd);

        if (dayTwo > today){
            $scope.dateOptions2E.minDate = $scope.filter.DateStart;
            $scope.filter.DateEnd = today;
        }
    }

    //=============== End of validasi date start and date end===========




	$scope.dataDealer = [];
	$scope.dataCabang = [];
	$scope.dataAreaTAM = [];
	$scope.dataAreaDealer = [];
    $scope.filter = {Dealer:null,DealerId:null, Cabang:null, DateStart:periode, DateEnd:periode, tamArea:0,dealerarea:0,kategori:"General Repair",isGR:1};
    ReportProductivityStallFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
        $scope.filter.DealerId=response.data[0].dealerID;
        ReportProductivityStallFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
          console.log('ayamtest',response.data)
          $scope.dataGroupDealerHO = response.data
          $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
          $scope.areaId($scope.dataGroupDealerHO[0])
        })
      })
    ReportProductivityStallFactory.getDataCabang($scope.filter).then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
    ReportProductivityStallFactory.getDataAreaTAM().then(function (response) {
        console.log('Response AreaTAM:', response.data);
        $scope.dataAreaTAM = response.data;
      })
      ReportProductivityStallFactory.getDataAreaDealerHO($scope.filter).then(function(response) {
        console.log('Response AreaDealerHO:', response.data);
        $scope.dataAreaDealerHO = response.data;
        $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
        $scope.areaId($scope.dataAreaDealerHO[0])
    })
    ReportProductivityStallFactory.getDataAreaDealer().then(function (response) {
        console.log('Response AreaDealer:', response.data);
        $scope.dataAreaDealer = response.data;
      })
    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
            $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };
        // $scope.DisabledCabang = function() {
        //     if ($scope.dataCabang.length>1){
        //         $scope.Disabled =false;
        //         console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
        //     }
        //     else{
        //         $scope.Disabled =true;
        //         console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
        //     }
        // };
        $scope.getData = function(){
            if ($scope.role == "Branch") $scope.getSummaryCabang();
            else $scope.getSummaryHO();
            console.log('get data' + $scope.role);
        }
    

        $scope.areaId = function(row) {
          // $scope.DataDealer = row
          $scope.DataDealer = Object.assign(row,{'kategori': $scope.filter.kategori})
          if($scope.userlogin != 'TAM'){
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
          }else{
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
          }
          console.log('row dealerid', row,$scope.DataDealer);
          ReportProductivityStallFactory.getDataCabangHONew($scope.DataDealer).then(function (response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabangHO = response.data;
            $scope.DisabledCabang();
            $scope.filter.CabangHO = null
            console.log('kambing',$scope.filter)
            $timeout(function() {
              $scope.filter.CabangHO = response.data[0].cabangID;
              console.log('kambing123',$scope.filter.CabangHO)
            }, 100);
          })
        };

        $scope.dealerCode = function(row){
          row = Object.assign(row,{'isGR':$scope.filter.isGR})
          $scope.filter.DealerId = row.dealerID
          ReportProductivityStallFactory.getDataAreaDealerHO(row).then(function (response) {
              console.log('ayam',response.data)
              console.log('rusa',$scope.dataGroupDealer)
              $scope.dataGroupDealer = response.data
              $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
              $scope.areaId($scope.dataGroupDealer[0])
              console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
          })
        }
      
        $scope.DisabledCabang = function() {
          if ($scope.dataCabang.length>1){
              $scope.Disabled =false;
              console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
          }else{
              $scope.Disabled =true;
              console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
          }
        };

    //Get DealerArea
    $scope.getOrgData = function(){
      console.log("user==>",$scope.user);
        CAccessMatrix.getDealOut($scope.user).then(function(result) {
          $scope.orgData = result.data.Result;
          console.log("orgData=>",result.data);
        });
    }


         //Get outlet
    $scope.selectDealer = function(xD){
      console.log("dealeeer==>",xD);
      //$scope.outletData = xD.Child;
      CAccessMatrix.getOutlet(xD).then(function(result) {
        $scope.outletData = result.data.Result;
        console.log("Dn==>",result.data.Result);
      })
    }

    $scope.getSummaryHO = function(){
        var $tanggal=$scope.filter.DateStart;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        // var $tanggalakhir=$scope.filter.DateEnd;
        // var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
        var TglAkhir = new Date($tanggal.getFullYear(), $tanggal.getMonth()+1, 0)
        var $filtertanggalakhir=TglAkhir.getFullYear()+'-'+(TglAkhir.getMonth()+1)+'-'+TglAkhir.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'tamarea': $scope.filter.tamArea,
            'dealerarea': $scope.filter.dealerarea,
            'kategori': $scope.filter.kategori,
            'periodeawal': $filtertanggal,
            'periodeakhir': $filtertanggalakhir,
            'Jenis': 'summary'};

          ReportProductivityStallFactory.getSummaryHO(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;
            var hasil=response.data[0];
            $scope.stallEfektif = hasil.stallEfektif;
            $scope.stallEfektif = Math.round($scope.stallEfektif * 100) / 100
            $scope.targetUnitSeluruhStall = hasil.targetUnit;
            $scope.targetUnitSeluruhStall = Math.round($scope.targetUnitSeluruhStall * 100) / 100
            $scope.unitEntrySeluruhStall = hasil.unitEntrySeluruhStall
            $scope.unitEntrySeluruhStall = Math.round($scope.unitEntrySeluruhStall * 100) / 100
            $scope.pencapaianTargetStallWorkshop = response.data[0].targetUnit>0 ? (hasil.unitEntrySeluruhStall / hasil.targetUnit) * 100 : 0;
            // $scope.pencapaianTargetStallWorkshop = Math.round($scope.pencapaianTargetStallWorkshop * 100) / 100
            $scope.pencapaianTargetStallWorkshop = parseFloat($scope.pencapaianTargetStallWorkshop ).toFixed(2);
            

            $scope.stallEMEfektif = hasil.stallEMEfektif;
            $scope.stallEMEfektif = Math.round($scope.stallEMEfektif * 100) / 100
            $scope.targetUnitperStallEM = hasil.targetUnitPerStallEM;
            $scope.targetUnitperStallEM = Math.round($scope.targetUnitperStallEM * 100) / 100
            $scope.targetUnitSeluruhStallEM = hasil.targetUnitStallEM;
            $scope.targetUnitSeluruhStallEM = Math.round($scope.targetUnitSeluruhStallEM * 100) / 100
            $scope.unitEntryEM = hasil.unitEntryEM;
            $scope.unitEntryEM = Math.round($scope.unitEntryEM * 100) / 100
            $scope.repairUnitperStallEM = hasil.rpsem;
            $scope.repairUnitperStallEM = Math.round($scope.repairUnitperStallEM * 100) / 100
            $scope.pencapaianTargetStallEM = response.data[0].targetUnitStallEM>0 ? (hasil.unitEntryEM / hasil.targetUnitStallEM) * 100 : 0;
            // $scope.pencapaianTargetStallEM = Math.round($scope.pencapaianTargetStallEM * 100) / 100
            $scope.pencapaianTargetStallEM = parseFloat($scope.pencapaianTargetStallEM ).toFixed(2);

            $scope.stallSBGREfektif = hasil.stallSBEfektif;
            $scope.stallSBGREfektif = Math.round($scope.stallSBGREfektif * 100) / 100
            $scope.targetUnitperStallSBGR = hasil.targetUnitPerStallSB;
            $scope.targetUnitperStallSBGR = Math.round($scope.targetUnitperStallSBGR * 100) / 100
            $scope.targetUnitSeluruhStallSBGR = hasil.targetUnitStallSB;
            $scope.targetUnitSeluruhStallSBGR = Math.round($scope.targetUnitSeluruhStallSBGR * 100) / 100
            $scope.unitEntryStallSBGR = hasil.unitEntrySB;
            $scope.unitEntryStallSBGR = Math.round($scope.unitEntryStallSBGR * 100) / 100
            $scope.repairUnitperStallSBGR = hasil.rpssb;
            $scope.repairUnitperStallSBGR = Math.round($scope.repairUnitperStallSBGR * 100) / 100
            $scope.pencapaianTargetStallSBGR = response.data[0].targetUnitStallSB>0 ? (hasil.unitEntrySB / hasil.targetUnitStallSB) * 100 : 0;
            // $scope.pencapaianTargetStallSBGR = Math.round($scope.pencapaianTargetStallSBGR * 100) / 100
            $scope.pencapaianTargetStallSBGR = parseFloat($scope.pencapaianTargetStallSBGR).toFixed(2);

            $scope.HariKerja = hasil.hariKerja;
            $scope.HariKerja = Math.round($scope.HariKerja * 100) / 100
            $scope.JamTersediaSeluruhStall = hasil.jamTersediaSeluruhStall;
            $scope.JamTersediaSeluruhStall = Math.round($scope.JamTersediaSeluruhStall * 100) / 100
            $scope.TargetJamSeluruhStall = hasil.targetJamSeluruhStall;
            $scope.TargetJamSeluruhStall = Math.round($scope.TargetJamSeluruhStall * 100) / 100
            $scope.JamAktualSeluruhStall = hasil.jamAktualSeluruhStall;
            $scope.JamAktualSeluruhStall = Math.round($scope.JamAktualSeluruhStall * 100) / 100
            $scope.PencapaianTargetStallWorkshop = response.data[0].targetJamSeluruhStall>0 ? (hasil.jamAktualSeluruhStall / hasil.targetJamSeluruhStall) * 100 : 0;
            // $scope.PencapaianTargetStallWorkshop = Math.round($scope.PencapaianTargetStallWorkshop * 100) / 100
            $scope.PencapaianTargetStallWorkshop = parseFloat($scope.PencapaianTargetStallWorkshop).toFixed(2);

            $scope.JamTersediaStallEM = hasil.jamTersediaStallEM;
            $scope.JamTersediaStallEM = Math.round($scope.JamTersediaStallEM * 100) / 100
            $scope.TargetJamperStallEM = hasil.targetJamPerStallEM;
            $scope.TargetJamperStallEM = Math.round($scope.TargetJamperStallEM * 100) / 100
            $scope.TargetJamSeluruhStallEM = hasil.targetJamSeluruhStallEM;
            $scope.TargetJamSeluruhStallEM = Math.round($scope.TargetJamSeluruhStallEM * 100) / 100
            $scope.JamAktualStallEM = hasil.jamAktualSeluruhStallEM;
            $scope.JamAktualStallEM = Math.round($scope.JamAktualStallEM * 100) / 100
            $scope.PencapaianTargetEM = response.data[0].targetJamSeluruhStallEM>0 ? (hasil.jamAktualSeluruhStallEM / hasil.targetJamSeluruhStallEM) * 100 : 0;
            // $scope.PencapaianTargetEM = Math.round($scope.PencapaianTargetEM * 100) / 100
            $scope.PencapaianTargetEM = parseFloat($scope.PencapaianTargetEM).toFixed(2);

            $scope.JamTersediaStallSBGR = hasil.jamTersediaStallSB;
            $scope.JamTersediaStallSBGR = Math.round($scope.JamTersediaStallSBGR * 100) / 100
            $scope.TargetJamperStallSBGR = hasil.targetJamPerStallSB;
            $scope.TargetJamperStallSBGR = Math.round($scope.TargetJamperStallSBGR * 100) / 100
            $scope.TargetJamSeluruhStallSBGR = hasil.targetJamSeluruhStallSB;
            $scope.TargetJamSeluruhStallSBGR = Math.round($scope.TargetJamSeluruhStallSBGR * 100) / 100
            $scope.JamAktualStallSBGR = hasil.jamAktualSeluruhStallSB;
            $scope.JamAktualStallSBGR = Math.round($scope.JamAktualStallSBGR * 100) / 100
            $scope.PencapaianTargetStallSBGR = response.data[0].targetJamSeluruhStallSB>0 ? (hasil.jamAktualSeluruhStallSB / hasil.targetJamSeluruhStallSB) * 100 : 0;
            // $scope.PencapaianTargetStallSBGR = Math.round($scope.PencapaianTargetStallSBGR * 100) / 100
            $scope.PencapaianTargetStallSBGR = parseFloat($scope.PencapaianTargetStallSBGR).toFixed(2);

          })
      }
    $scope.getSummaryCabang = function() {
        var $tanggal1=$scope.filter.DateStart;
        // var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        // var $filtertanggalakhir=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeawal': $filtertanggal
            // 'periodeakhir': $filtertanggalakhir
        };

        ReportProductivityStallFactory.getSummaryCabang(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;
            var hasil=response.data[0];
            $scope.stallEfektif = hasil.stallEfektif;
            $scope.stallEfektif = Math.round($scope.stallEfektif * 100) / 100
            $scope.targetUnitSeluruhStall = hasil.targetUnit;
            $scope.targetUnitSeluruhStall = Math.round($scope.targetUnitSeluruhStall * 100) / 100
            $scope.unitEntrySeluruhStall = hasil.unitEntrySeluruhStall
            $scope.unitEntrySeluruhStall = Math.round($scope.unitEntrySeluruhStall * 100) / 100
            $scope.pencapaianTargetStallWorkshop = response.data[0].targetUnit>0 ? (hasil.unitEntrySeluruhStall / hasil.targetUnit) * 100 : 0;
            // $scope.pencapaianTargetStallWorkshop = Math.round($scope.pencapaianTargetStallWorkshop * 100) / 100
            $scope.pencapaianTargetStallWorkshop = parseFloat($scope.pencapaianTargetStallWorkshop).toFixed(2);

            $scope.stallEMEfektif = hasil.stallEMEfektif;
            $scope.stallEMEfektif = Math.round($scope.stallEMEfektif * 100) / 100
            $scope.targetUnitperStallEM = hasil.targetUnitPerStallEM;
            $scope.targetUnitperStallEM = Math.round($scope.targetUnitperStallEM * 100) / 100
            $scope.targetUnitSeluruhStallEM = hasil.targetUnitStallEM;
            $scope.targetUnitSeluruhStallEM = Math.round($scope.targetUnitSeluruhStallEM * 100) / 100
            $scope.unitEntryEM = hasil.unitEntryEM;
            $scope.unitEntryEM = Math.round($scope.unitEntryEM * 100) / 100
            $scope.repairUnitperStallEM = hasil.rpsem;
            $scope.repairUnitperStallEM = Math.round($scope.repairUnitperStallEM * 100) / 100
            $scope.pencapaianTargetStallEM = response.data[0].targetUnitStallEM>0 ? (hasil.unitEntryEM / hasil.targetUnitStallEM) * 100 : 0;
            // $scope.pencapaianTargetStallEM = Math.round($scope.pencapaianTargetStallEM * 100) / 100
            $scope.pencapaianTargetStallEM = parseFloat($scope.pencapaianTargetStallEM).toFixed(2);

            $scope.stallSBGREfektif = hasil.stallSBEfektif;
            $scope.stallSBGREfektif = Math.round($scope.stallSBGREfektif * 100) / 100
            $scope.targetUnitperStallSBGR = hasil.targetUnitPerStallSB;
            $scope.targetUnitperStallSBGR = Math.round($scope.targetUnitperStallSBGR * 100) / 100
            $scope.targetUnitSeluruhStallSBGR = hasil.targetUnitStallSB;
            $scope.targetUnitSeluruhStallSBGR = Math.round($scope.targetUnitSeluruhStallSBGR * 100) / 100
            $scope.unitEntryStallSBGR = hasil.unitEntrySB;
            $scope.unitEntryStallSBGR = Math.round($scope.unitEntryStallSBGR * 100) / 100
            $scope.repairUnitperStallSBGR = hasil.rpssb;
            $scope.repairUnitperStallSBGR = Math.round($scope.repairUnitperStallSBGR * 100) / 100
            $scope.pencapaianTargetStallSBGR = response.data[0].targetUnitStallSB>0 ? (hasil.unitEntrySB / hasil.targetUnitStallSB) * 100 : 0;
            // $scope.pencapaianTargetStallSBGR = Math.round($scope.pencapaianTargetStallSBGR * 100) / 100
            $scope.pencapaianTargetStallSBGR = parseFloat($scope.pencapaianTargetStallSBGR).toFixed(2);

            $scope.HariKerja = hasil.hariKerja;
            $scope.HariKerja = Math.round($scope.HariKerja * 100) / 100
            $scope.JamTersediaSeluruhStall = hasil.jamTersediaSeluruhStall;
            $scope.JamTersediaSeluruhStall = Math.round($scope.JamTersediaSeluruhStall * 100) / 100
            $scope.TargetJamSeluruhStall = hasil.targetJamSeluruhStall;
            $scope.TargetJamSeluruhStall = Math.round($scope.TargetJamSeluruhStall * 100) / 100
            $scope.JamAktualSeluruhStall = hasil.jamAktualSeluruhStall;
            $scope.JamAktualSeluruhStall = Math.round($scope.JamAktualSeluruhStall * 100) / 100
            $scope.PencapaianTargetStallWorkshop = response.data[0].targetJamSeluruhStall>0 ? (hasil.jamAktualSeluruhStall / hasil.targetJamSeluruhStall) * 100 : 0;
            // $scope.PencapaianTargetStallWorkshop = Math.round($scope.PencapaianTargetStallWorkshop * 100) / 100
            $scope.PencapaianTargetStallWorkshop = parseFloat($scope.PencapaianTargetStallWorkshop).toFixed(2);

            $scope.JamTersediaStallEM = hasil.jamTersediaStallEM;
            $scope.JamTersediaStallEM = Math.round($scope.JamTersediaStallEM * 100) / 100
            $scope.TargetJamperStallEM = hasil.targetJamPerStallEM;
            $scope.TargetJamperStallEM = Math.round($scope.TargetJamperStallEM * 100) / 100
            $scope.TargetJamSeluruhStallEM = hasil.targetJamSeluruhStallEM;
            $scope.TargetJamSeluruhStallEM = Math.round($scope.TargetJamSeluruhStallEM * 100) / 100
            $scope.JamAktualStallEM = hasil.jamAktualSeluruhStallEM;
            $scope.JamAktualStallEM = Math.round($scope.JamAktualStallEM * 100) / 100
            $scope.PencapaianTargetEM = response.data[0].targetJamSeluruhStallEM>0 ? (hasil.jamAktualSeluruhStallEM / hasil.targetJamSeluruhStallEM) * 100 : 0;
            // $scope.PencapaianTargetEM = Math.round($scope.PencapaianTargetEM * 100) / 100
            $scope.PencapaianTargetEM = parseFloat($scope.PencapaianTargetEM).toFixed(2);

            $scope.JamTersediaStallSBGR = hasil.jamTersediaStallSB;
            $scope.JamTersediaStallSBGR = Math.round($scope.JamTersediaStallSBGR * 100) / 100
            $scope.TargetJamperStallSBGR = hasil.targetJamPerStallSB;
            $scope.TargetJamperStallSBGR = Math.round($scope.TargetJamperStallSBGR * 100) / 100
            $scope.TargetJamSeluruhStallSBGR = hasil.targetJamSeluruhStallSB;
            $scope.TargetJamSeluruhStallSBGR = Math.round($scope.TargetJamSeluruhStallSBGR * 100) / 100
            $scope.JamAktualStallSBGR = hasil.jamAktualSeluruhStallSB;
            $scope.JamAktualStallSBGR = Math.round($scope.JamAktualStallSBGR * 100) / 100
            $scope.PencapaianTargetStallSBGR = response.data[0].targetJamSeluruhStallSB>0 ? (hasil.jamAktualSeluruhStallSB / hasil.targetJamSeluruhStallSB) * 100 : 0;
            // $scope.PencapaianTargetStallSBGR = Math.round($scope.PencapaianTargetStallSBGR * 100) / 100
            $scope.PencapaianTargetStallSBGR = parseFloat($scope.PencapaianTargetStallSBGR).toFixed(2);
        })
    }
        $scope.getReportCabang = function(){
            var $tanggal1=$scope.filter.DateStart;
            // var $tanggal2=$scope.filter.DateEnd;
            var $filtertanggal=$tanggal1.getFullYear()+'/'+($tanggal1.getMonth()+1)+'/'+$tanggal1.getDate();
            // var $filtertanggalakhir=$tanggal2.getFullYear()+'/'+($tanggal2.getMonth()+1)+'/'+$tanggal2.getDate();
            var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeawal': $filtertanggal,
                // 'periodeakhir': $filtertanggalakhir
            };
                ReportProductivityStallFactory.getExportCabang(dataFilter);
        }
        $scope.getReportHO = function(){
              var $tanggal=$scope.filter.DateStart;
              var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
            //   var $tanggalakhir=$scope.filter.DateEnd;
            //   var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
              var TglAkhir = new Date($tanggal.getFullYear(), $tanggal.getMonth()+1, 0)
              var $filtertanggalakhir=TglAkhir.getFullYear()+'-'+(TglAkhir.getMonth()+1)+'-'+TglAkhir.getDate();
              var dataReport = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'tamarea': $scope.filter.tamArea,
                'dealerarea': $scope.filter.dealerarea,
                'kategori': $scope.filter.kategori,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal};

            ReportProductivityStallFactory.getExportHO(dataReport);
        }

});
