angular.module('app')
    .controller('ReportInventoryPositionController', function($scope,$http, $stateParams, CurrentUser, ReportInventoryPositionFactory,ReportInventoryPositionFactory ) {
      $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };

	$scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
    });

	$scope.user = CurrentUser.user();
  $scope.roleId = $scope.user.RoleId;
	$scope.kategori=$stateParams.kategori;
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
        // mode:"'month'",
    };
    $scope.dateOptions2S = {
        startingDay: 1,
        format: dateFormat,
    };

    var today = new Date();
    $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(), today.getDate());

	$scope.filter = {Dealer:null, Cabang:null, periode:periode, zeroStock:false};
	$scope.dataDealer = [];
    $scope.dataCabang = [];
    $scope.filter.DateStart = new Date();

	ReportInventoryPositionFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
      })
    ReportInventoryPositionFactory.getDataCabang().then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
	$scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
  $scope.getData = function(){

    }
    $scope.getReport = function(){
          var $tanggal=$scope.filter.periode;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();

          var dataReport = {

            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'zerostock':$scope.filter.zeroStock,
            'periode': $filtertanggal,
            'kategori':$scope.kategori
            };

        ReportInventoryPositionFactory.getExport(dataReport);
    }
});
