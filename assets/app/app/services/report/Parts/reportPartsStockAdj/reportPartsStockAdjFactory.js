angular.module('app')
  .factory('ReportPartsStockAdjFactory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = ''; //'http://localhost:8080';
    return {
      getDataDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},
      getDataCabang: function() {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            return res;},
      getTipeMaterial: function() {
            var res=$http.get(serverURL + '/api/rpt/TipeMaterial/');
            return res;}, 
      getSummaryFilter: function(data) {
              console.log('Data Filter:', data);
              // console.log('periode',moment(data.periode).format('YYYY-MM-DD'))
              var res = $http.get(serverURL + '/api/rpt/AS_PartsStockAdjustment_PartsCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/'+ data.referensi +'/'+ data.reason +'/'+ data.tipe_material  +'/'+ data.kategori );
              return res;},
      getSummaryExport: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_PartsStockAdjustment_PartsCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode_awal='+ data.periode_awal +'&periode_akhir='+ data.periode_akhir +'&tipe_material='+ data.tipe_material +'&reason='+ data.reason +'&referensi='+ data.referensi+'&kategori='+data.kategori);
              // var res = $http.get(serverURL + 'CCustomerCompleteness?area='+ data.area +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periode +'&jenis_pelanggan='+ data.jenispel);
              // return res;
            }
    }
  });
