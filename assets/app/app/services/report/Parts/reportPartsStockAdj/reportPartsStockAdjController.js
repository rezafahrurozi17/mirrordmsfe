angular.module('app')
    .controller('ReportPartsStockAdjController', function($scope, $http,$stateParams, CurrentUser, ReportPartsStockAdjFactory ) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
    });
    //----------------------------------
    // Initialization
    //----------------------------------

    $scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;
    $scope.kategori=$stateParams.kategori;
    console.log($stateParams,'report Lead Time');

    console.log('report Lead Time');
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();   
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };
    
    // if ($scope.roleId==1128 || $scope.roleId==1129) {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", KategoriWorkshop:"General Repair", 
    //                 DateStart:null, DateEnd:null, referensi:'All', reason:0, material:1};
    // } else {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", KategoriWorkshop:"General Repair", 
    //                 DateStart:null, DateEnd:null, referensi:'All', reason:0, material:1};
    // }
    // $scope.filter = {Dealer:null, Cabang:null, DateStart:periode,DateEnd:periode, material:1, referensi :null,reason:null};
    $scope.filter = {Dealer:null, Cabang:null, DateStart:periode,DateEnd:periode, material:1, referensi :0,reason:0};
    ReportPartsStockAdjFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.getDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
    })
    ReportPartsStockAdjFactory.getDataCabang().then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.getCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
    })
    ReportPartsStockAdjFactory.getTipeMaterial().then(function (response) {
        console.log('Response BEMaterial:', response.data);
        $scope.getMaterial = response.data;
    })

    $scope.getReferensi = [
        {ReferensiId:0, Referensi:"All"},
        {ReferensiId:1, Referensi:"BAG"},
        {ReferensiId:2, Referensi:"Stock Opname"}
    ];

    $scope.getReason = [
        {ReasonId:0, ReasonName:'ALL'},
        {ReasonId:1, ReasonName:'Barang Hilang'},
        {ReasonId:2, ReasonName:'Kelebihan'}
        // {ReasonId:3, ReasonName:'Lainnya'}
    ];
    
    // $scope.roleId = 1; // 2 = HO | 1 = Branch
    $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };
    $scope.DisabledCabang = function() {
            if ($scope.getCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.getCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.getCabang.length + ' ' + $scope.Disabled);
            }
        };
    $scope.getData = function() {
        var referensi = null;
        if($scope.filter.referensi == 0){
            referensi = "All";
        }else if($scope.filter.referensi == 1){
            referensi = "BAG";
        }else if($scope.filter.referensi == 2){
            referensi = "Stock Opname";
        }
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,            
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,            
            'referensi': referensi,
            'reason': $scope.filter.reason,
            'tipe_material': $scope.filter.material,
            'kategori':$scope.kategori
        };

        ReportPartsStockAdjFactory.getSummaryFilter(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            $scope.dataSummary = response.data;
            var hasil=response.data[0];
            $scope.selisihQty = hasil.qtySelisih; 
            $scope.selisihLC = hasil.totalLCSelisih;
            $scope.selisihQtylebih = hasil.qtyhilang; 
            $scope.TotalLChilang = hasil.totalLChilang;
            if (hasil.tercatat !== undefined) { $scope.tercatat = hasil.tercatat} else {$scope.tercatat = 0}
            if (hasil.real !== undefined) { $scope.real = hasil.real} else {$scope.real = 0}
            // $scope.tercatat = hasil.tercatat;
            // $scope.real = hasil.real;
        });
    }

    $scope.getReport = function(){
        var referensi = null;
        if($scope.filter.referensi == 0){
            referensi = "All";
        }else if($scope.filter.referensi == 1){
            referensi = "BAG";
        }else if($scope.filter.referensi == 2){
            referensi = "Stock Opname";
        }
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,            
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,            
            'referensi': referensi,
            'reason': $scope.filter.reason,
            'tipe_material': $scope.filter.material,
            'kategori':$scope.kategori};
        ReportPartsStockAdjFactory.getSummaryExport(dataFilter);
    }
});
