angular.module('app')
  .factory('StockFactory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL ='';// 'http://localhost:8080';
    return {
      getDataDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},
      // getDataCabang: function() {
      //       var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
      //       return res;},
      getDataCabang: function(kategori) {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId + '/' + kategori);
            return res;},
      getDataCabangHONew: function(data) {
            console.log('Data Get Cabang:', data);
            var res=$http.get(serverURL + '/api/rpt/GetCabangHONew/'+ data.areaId + '/' +  data.DealerId + '/' + data.kategori);
            return res;},
      getDataCabangALL: function(data) {
            console.log('Data Get ALL:', data);
            var res=$http.get(serverURL + '/api/rpt/GetCabangALL/'+ data.areaId + '/' +  data.DealerId );
            return res;},      
      getDataAreaTAM: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaTAM/');
            return res;},
      getDataAreaDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaDealer/');
            return res;},
      getDataAreaDealerHO: function(data) {
            var res=$http.get(serverURL + '/api/rpt/AreaDealerHONew/'+ data.isGR );
            return res;},
      getDataAreaDealerHOParts: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaDealerHOParts/' );
            return res;},            
      getTipeMaterial: function() {
            var res=$http.get(serverURL + '/api/rpt/TipeMaterial/');
            return res;},
      getSummaryExport: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_StockReport_PartsCAB?Dealer='+ data.dealer +'&Cabang='+ data.cabang +'&Tanggal='+ data.periode +'&TipeMaterial='+ data.tipe_material+'&kategori='+data.kategori );
              // var res = $http.get(serverURL + 'CCustomerCompleteness?area='+ data.area +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periode +'&jenis_pelanggan='+ data.jenispel);
              // return res;
            },
      getSummaryExportHO: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_StockReport_PartsHO?Dealer='+ data.dealer +'&Cabang='+ data.cabang +'&Tanggal='+ data.periode +'&TAMArea='+ data.tamarea +'&GroupDealerArea='+ data.dealerarea +'&kategori='+data.kategori);
              // var res = $http.get(serverURL + 'CCustomerCompleteness?area='+ data.area +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periode +'&jenis_pelanggan='+ data.jenispel);
              // return res;
            },
    }
  });
