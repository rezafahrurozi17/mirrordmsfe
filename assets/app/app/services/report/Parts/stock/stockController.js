angular.module('app')
    .controller('StockController', function($scope,$stateParams, $http, CurrentUser, StockFactory, bsNotify,$timeout ) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        // $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;
    $scope.kategori=$stateParams.kategori;
    console.log('kategori', $scope.kategori);

    var dateFormat='dd/MM/yyyy';
    var dateFormat2='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat2,

        // mode:"'month'",
        //disableWeekend: 1
    };

    // if ($scope.roleId==1128 || $scope.roleId==1129) {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000",
    //                 tanggal:periode, material:1};
    // } else {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000",
    //                 tanggal:periode, material:1};
    // }
    var today = new Date();
    $scope.maxDate = new Date(today.getFullYear(),today.getMonth(), today.getDate());

    $scope.dataDealer = [];
    $scope.hiddenRefreshButton = true;
    $scope.dataCabang = [];
    $scope.dataAreaTAM = [];
    $scope.dataAreaDealer = [];
    $scope.getMaterial = [];
    $scope.role=$stateParams.role;
    console.log('role',$scope.role);
     $scope.filter = {Dealer:null,DealerId:null, Cabang:null, tanggal:periode, material:1, tamArea :0,DealerArea:0,isGR:null};

    if($stateParams.kategori == 'GR'){
      $scope.filter.isGR = 1
    }else{
      $scope.filter.isGR = 0
    }

    StockFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
        $scope.filter.DealerId=response.data[0].dealerID;
        StockFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
          console.log('ayamtest',response.data)
          $scope.dataGroupDealerHO = response.data
          $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
          $scope.areaId($scope.dataGroupDealerHO[0])
        })
    })
    StockFactory.getDataCabang($scope.kategori).then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
    })
    StockFactory.getDataAreaTAM().then(function (response) {
        console.log('Response AreaTAM:', response.data);
        $scope.dataAreaTAM = response.data;
      })
    StockFactory.getDataAreaDealer().then(function (response) {
        console.log('Response AreaDealer:', response.data);
        $scope.dataAreaDealer = response.data;
      })
    // StockFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
    //     console.log('Response AreaDealerHO:', response.data);
    //     $scope.dataAreaDealerHO = response.data;
    //     $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
    //     $scope.areaId($scope.dataAreaDealerHO[0])
    //   })
    StockFactory.getDataAreaDealerHOParts().then(function (response) {
        console.log('Response AreaDealerHOParts:', response.data);
        $scope.dataAreaDealerHO = response.data;
        $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
        $scope.areaId($scope.dataAreaDealerHO[0])
      })
    StockFactory.getTipeMaterial().then(function (response) {
        console.log('Response BEMaterial:', response.data);
        $scope.getMaterial = response.data;
    })
    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
            $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };


        $scope.areaId = function(row) {
          // $scope.DataDealer = row
          console.log('row ', row,$scope.DataDealer);
          if($stateParams.kategori == 'GR'){
            $scope.DataDealer = Object.assign(row,{'kategori':'General Repair'})
          }else{
            $scope.DataDealer = Object.assign(row,{'kategori':'Body and Paint'})
          }
          if($scope.userlogin != 'TAM'){
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
          }else{
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
          }
          console.log('row dealerid', row,$scope.DataDealer);
          
          // StockFactory.getDataCabangHONew($scope.DataDealer).then(function (response) {
          //   console.log('Response BECabang:', response.data);
          //   $scope.dataCabangHO = response.data;
          //   $scope.DisabledCabang();
          //   $scope.filter.CabangHO = null
          //   console.log('kambing',$scope.filter)
          //   $timeout(function() {
          //     $scope.filter.CabangHO = response.data[0].cabangID;
          //     console.log('kambing123',$scope.filter.CabangHO)
          //   }, 100);
          // })

          StockFactory.getDataCabangALL($scope.DataDealer).then(function (response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabangHO = response.data;
            $scope.DisabledCabang();
            $scope.filter.CabangHO = null
            console.log('kambing',$scope.filter)
            $timeout(function() {
              $scope.filter.CabangHO = response.data[0].cabangID;
              console.log('kambing123',$scope.filter.CabangHO)
            }, 100);
          })
        };

        $scope.dealerCode = function(row){
          row = Object.assign(row,{'isGR':$scope.filter.isGR})
          $scope.filter.DealerId = row.dealerID
          StockFactory.getDataAreaDealerHO(row).then(function (response) {
              console.log('ayam',response.data)
              console.log('rusa',$scope.dataGroupDealer)
              $scope.dataGroupDealer = response.data
              $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
              $scope.areaId($scope.dataGroupDealer[0])
              console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
          })
        }

        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };

    $scope.getData = function(){}
    $scope.getReport = function(){
        // if (($scope.filter.DealerArea == null || $scope.filter.Cabang == null) ||
        //     ($scope.filter.DealerArea == undefined || $scope.filter.Cabang == undefined) ||
        //     ($scope.filter.DealerArea == '' || $scope.filter.Cabang == '')){
        //         bsNotify.show({
        //             size: 'big',
        //             type: 'danger',
        //             title: "Isi data bertanda *",
        //             // content: error.join('<br>'),
        //             // number: error.length
        //         });
        // } else {
            var $tanggal1=$scope.filter.tanggal;

            var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();

            var dataFilterCab = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'tamarea': $scope.filter.tamArea,
                'dealerarea': $scope.filter.DealerArea,
                'periode': $filtertanggal1,
                'tipe_material': $scope.filter.material,
                'kategori':$scope.kategori};
            
            var dataFilterHO = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.CabangHO,
                'tamarea': $scope.filter.tamArea,
                'dealerarea': $scope.filter.DealerArea,
                'periode': $filtertanggal1,
                'tipe_material': $scope.filter.material,
                'kategori':$scope.kategori};

            if ($scope.role=="Branch")
                StockFactory.getSummaryExport(dataFilterCab);
            else
                StockFactory.getSummaryExportHO(dataFilterHO);
        // }

    }
});
