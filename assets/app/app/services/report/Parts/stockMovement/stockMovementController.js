angular.module('app')
    .controller('StockMovementController', function($scope, $http,$stateParams, CurrentUser, StockMovementFactory ) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        // $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;
    $scope.kategori=$stateParams.kategori;
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode =  new Date();
    $scope.dateOptionsS = {
        startingDay: 1,
        format: dateFormat,
        
        // mode:"'month'",
        // disableWeekend: 1
    };
    $scope.dateOptionsE = {
        startingDay: 1,
        format: dateFormat,
        
        // mode:"'month'",
        // disableWeekend: 1
    };

    //===============validasi date start and date end===========
    var today = new Date();
    // $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(), today.getDate());  
    $scope.maxDateS = new Date();
    $scope.maxDateS = $scope.maxDateS.setDate($scope.maxDateS.getDate() - 1);
    // $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());
    $scope.maxDateE = new Date();
    $scope.maxDateE = $scope.maxDateE.setDate($scope.maxDateE.getDate() - 1);
    $scope.minDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());  
    console.log('$scope.maxDateS', $scope.maxDateS);
    console.log('$scope.maxDateE', $scope.maxDateE);



    $scope.changeStartDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.DateStart);
        var dayTwo = new Date($scope.filter.DateEnd);
        
        $scope.dateOptionsE.minDate = new Date(dayOne.getFullYear(),dayOne.getMonth(), dayOne.getDate());
        if (tgl == null || tgl == undefined){
            $scope.filter.DateEnd = null;
        } else {
            if ($scope.filter.DateStart < $scope.filter.DateEnd){

            } else {
                $scope.filter.DateEnd = $scope.filter.DateStart;
            }
        }
    }

    //=============== End of validasi date start and date end===========



    // $scope.filter = {Dealer:null, Cabang:null,DateStart:periode, DateEnd:periode, partcode:null};
    $scope.filter = {Dealer:null, Cabang:null,DateStart:$scope.maxDateS, DateEnd:$scope.maxDateS, partcode:null};
    $scope.getDealer = [];
    $scope.getCabang = [];
    $scope.getMaterial = [];
    StockMovementFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.getDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
    })
    StockMovementFactory.getDataCabang().then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.getCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
    })
    StockMovementFactory.getTipeMaterial().then(function (response) {
        console.log('Response BEMaterial:', response.data);
        $scope.getMaterial = response.data;
    })
        $scope.DisabledCabang = function() {
            if ($scope.getCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.getCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.getCabang.length + ' ' + $scope.Disabled);
            }
        };

    $scope.getData = function() {

    }
    
    $scope.getReport = function(){
        var $tanggal1=new Date($scope.filter.DateStart);
        var $tanggal2=new Date($scope.filter.DateEnd);
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,            
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,            
            'kode_part': $scope.filter.partcode,
            
            'tipe_material': $scope.filter.material,
            'kategori':$scope.kategori};

        StockMovementFactory.getSummaryExport(dataFilter);
    }
});
