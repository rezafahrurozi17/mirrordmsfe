angular.module('app')
    .controller('ReportPartsReturnController', function($scope, $http,$stateParams, CurrentUser, ReportPartsReturnFactory,ReportPartsReturnFactory) {

	$scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
    });

	$scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;
    $scope.kategori=$stateParams.kategori;
    var periode=new Date();
    //var $item=angular.fromJson( $stateParams.tab.item);
    //$scope.roleId = $item.RoleId;

    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    var dateFormat='dd/MM/yyyy';
    var dateFormat2='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';

    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,

        // mode:"'month'",
    };

    $scope.dateOptions2S = {
        startingDay: 1,
        format: dateFormat2,
    };
    $scope.dateOptions2E = {
        startingDay: 1,
        format: dateFormat2,
    };

    $scope.getData = function(){

    }

    //===============validasi date start and date end===========
    var today = new Date();
    $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(), today.getDate());
    $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());
    $scope.minDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());


    $scope.changeStartDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.DateStart);
        var dayTwo = new Date($scope.filter.DateEnd);

        $scope.dateOptions2E.minDate = new Date(dayOne.getFullYear(),dayOne.getMonth(), dayOne.getDate());
        if (tgl == null || tgl == undefined){
            $scope.filter.DateEnd = null;
        } else {
            if ($scope.filter.DateStart < $scope.filter.DateEnd){

            }else {
              if (dayOne > today){
                  $scope.filter.DateStart = today;
                  console.log('masuk if today', today);
                  $scope.filter.DateEnd = $scope.filter.DateStart;
              } else {
                  $scope.filter.DateEnd = $scope.filter.DateStart;
              }
            }
        }

    }

    $scope.changeEndDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.DateStart);
        var dayTwo = new Date($scope.filter.DateEnd);

        if (dayTwo > today){
            $scope.dateOptions2E.minDate = $scope.filter.DateStart;
            $scope.filter.DateEnd = today;
        }
    }

    //=============== End of validasi date start and date end===========


	$scope.dataDealer = [];
	$scope.dataCabang = [];
	$scope.dataVendor = [];

    
    $scope.dataReferensi = [
        {referensi:"All"},
        {referensi:"Part Claim"},
        {referensi:"RPP"}
    ];
    
    $scope.filter = {Dealer:null, Cabang:null, referensi:"RPP", DateStart:periode, DateEnd:periode, Vendor:null };

      ReportPartsReturnFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
      })
      ReportPartsReturnFactory.getDataCabang().then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })

      ReportPartsReturnFactory.getVendor().then(function (response) {
        console.log('Response BEVendor:', response.data);
        $scope.dataVendor = response.data;
        $scope.filter.Vendor = response.data[1].vendorId;
      })
	    $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };



      $scope.getReport = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
          var $tanggalakhir=$scope.filter.DateEnd;
          var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
          var dataReport = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'referensi': $scope.filter.referensi,
            'vendor': $scope.filter.Vendor,
            'kategori':$scope.kategori};

        ReportPartsReturnFactory.getSummaryExport(dataReport);
    }

});
