angular.module('app')
  .factory('ReportPartsReturnFactory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = '';//http://localhost:8080';
    return {
// int vendor,string dealer,string cabang, string periode_awal, string periode_akhir, string TAMJudgement, string reason
      getDataDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},
      getDataCabang: function() {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            return res;},
      getVendor: function(data) {
            var res=$http.get(serverURL + '/api/rpt/getVendor/'+ currentUser.OrgId);
            return res;},  
      getSummaryExport: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_PartsStockReturn_PartsCAB?kode_vendor='+ data.vendor +'&dealer='+ data.dealer +'&cabang='+ data.cabang 
                    +'&periode_awal='+ data.periodeawal +'&periode_akhir='+ data.periodeakhir +'&referensi='+ data.referensi+'&kategori='+data.kategori);

            }
      }
  });
