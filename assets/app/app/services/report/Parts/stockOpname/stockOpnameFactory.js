angular.module('app')
  .factory('StockOpnameFactory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = ''; //'http://localhost:8080'
    return {
          getDataDealer: function() {
                var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
                return res;
          },
          getDataCabang: function() {
                var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
                return res;
          },
          getSummary: function(data) {
            console.log('Data Report:', data);
            var res = $http.get(serverURL + '/api/rpt/AS_StockOpname_PartsCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode +'/'+ data.kategori+'/'+data.materialtype);
              return res;
          },
          getExport: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_StockOpname_PartsCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&date='+ data.periode+'&kategori='+data.kategori+'&materialtype='+data.materialtype);
          }
    }
  });
