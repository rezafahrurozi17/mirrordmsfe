angular.module('app')
    .controller('StockOpnameController', function($scope, $http, CurrentUser,$stateParams, StockOpnameFactory ) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        // $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;
    $scope.kategori=$stateParams.kategori;
    // $scope.filter.periode = new Date();
    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,
        
        // mode:"'month'",
        //disableWeekend: 1
    };
    $scope.dataDealer = [];
    $scope.dataCabang = [];
    // $scope.MaterialTypeId = 1;
    StockOpnameFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
      })
    StockOpnameFactory.getDataCabang().then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
    $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
    

        function format1(n, currency) {
            return currency + " " + n.toFixed(0).replace(/./g, function (c, i, a) {
                return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
            });
        }
    $scope.filter = {Dealer:null, Cabang:null,periode:new Date() };
    
    $scope.getData = function(){
        var $tanggal=$scope.filter.periode;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        
        var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periode': $filtertanggal,
                'kategori':$scope.kategori,
                'materialtype': $scope.filter.MaterialTypeId
            };

        StockOpnameFactory.getSummary(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            // $scope.dataSummary = response.data;
            $scope.qPartBefore = response.data[0].qtyPartB;
            $scope.qPartAfter = response.data[0].qtyPartA;
            $scope.qPartDiff = response.data[0].qtyPartA-response.data[0].qtyPartB;
            $scope.qOilBefore = response.data[0].qtyOilB;
            $scope.qOilAfter = response.data[0].qtyOilA;
            $scope.qOilDiff = response.data[0].qtyOilA-response.data[0].qtyOilB;
            $scope.qTotalBefore = $scope.qPartBefore+$scope.qOilBefore;
            $scope.qTotalAfter = $scope.qPartAfter+$scope.qOilAfter;
            $scope.qTotalDiff = $scope.qPartDiff+$scope.qOilDiff;
            $scope.aPartBefore = format1(response.data[0].amtPartB, '');
            $scope.aPartAfter = format1(response.data[0].amtPartA,'');
            $scope.aPartDiff = format1(response.data[0].amtPartA-response.data[0].amtPartB,'');
            $scope.aOilBefore = format1(response.data[0].amtOilB,'');
            $scope.aOilAfter = format1(response.data[0].amtOilA,'');
            $scope.aOilDiff = format1(response.data[0].amtOilA - response.data[0].amtOilB,'');
            $scope.aTotalBefore = format1(response.data[0].amtPartB + response.data[0].amtOilB,'');
            $scope.aTotalAfter = format1(response.data[0].amtPartA + response.data[0].amtOilA,'');
            $scope.aTotalDiff = format1((response.data[0].amtPartA - response.data[0].amtPartB) + (response.data[0].amtOilA - response.data[0].amtOilB),'')
        })
    }

    $scope.getReport = function(){
        var $tanggal=$scope.filter.periode;
        var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
        
        var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periode': $filtertanggal,
                'kategori':$scope.kategori,
                'materialtype': $scope.filter.MaterialTypeId
            };

        StockOpnameFactory.getExport(dataFilter);
    }

    
});
