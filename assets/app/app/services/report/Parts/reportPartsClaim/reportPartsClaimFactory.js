angular.module('app')
  .factory('ReportPartsClaimFactory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = ''; //http://localhost:8080';
    return {
// int vendor,string dealer,string cabang, string periode_awal, string periode_akhir, string TAMJudgement, string reason
      getDataDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},
      getDataCabang: function() {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            return res;},
      getClaimReason: function(data) {
            var res=$http.get(serverURL + '/api/rpt/getClaimReason');
            return res;},   
      getTamJudgement: function(data) {
            var res=$http.get(serverURL + '/api/as/Parts/GetSRCJudgement/');
            return res;}, 
      getVendor: function(data) {
            var res=$http.get(serverURL + '/api/rpt/getVendor/'+ currentUser.OrgId);
            return res;}, 
      getSummaryExport: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_PartsClaim_PartsCAB?vendor='+ data.vendor +'&dealer='+ data.dealer +'&cabang='+ data.cabang 
                    +'&periode_awal='+ data.periodeawal +'&periode_akhir='+ data.periodeakhir +'&TAMJudgement='+ data.judgement +'&reason='+ data.reason+'&kategori='+data.kategori);

            }

    }
  });
