angular.module('app')
    .controller('ReportPartsClaimController', function($scope,$http,$stateParams,bsNotify, CurrentUser, ReportPartsClaimFactory ) {
    console.log('report part claim');

	$scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
    });

	$scope.user = CurrentUser.user();
  $scope.roleId = $scope.user.RoleId;
	$scope.kategori=$stateParams.kategori;

    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode=new Date();
    $scope.dateOptions = {
        startingDay: 1,
        format: dateFormat,

        // mode:"'month'",
    };
	$scope.getData = function(){

    }

	$scope.dataDealer = [];
	$scope.dataCabang = [];
	$scope.dataReason = [];
	$scope.dataVendor = [];


      ReportPartsClaimFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
      })
      ReportPartsClaimFactory.getDataCabang().then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
      ReportPartsClaimFactory.getClaimReason().then(function (response) {
        console.log('Response BEClaimReason:', response.data);
        $scope.dataReason = response.data;
      })
      ReportPartsClaimFactory.getTamJudgement().then(function (response) {
        console.log('Response BE Tam Judgement:', response.data);
        // $scope.dataJudgement = response.data;
        $scope.dataJudgement = response.data.Result;
        $scope.filter.judgement = response.data.Result[0].SRCJudgementId;
        // $scope.filter.judgement = $scope.dataJudgement;
        for (var i=0; i<$scope.dataJudgement.length; i++){
          $scope.dataJudgement[i].JudgementId = $scope.dataJudgement[i].SRCJudgementId
        }
      })
      ReportPartsClaimFactory.getVendor().then(function (response) {
        console.log('Response BEVendor:', response.data);
        $scope.dataVendor = response.data;
        $scope.Disabled = true
        $scope.filter.Vendor=response.data[1].vendorId;
      })


    // $scope.dataJudgement = [
    //     {JudgementId:1,JudgementName:"A"},
    //     {JudgementId:2,JudgementName:"B"},
    //     {JudgementId:3,JudgementName:"R1"},
    //     {JudgementId:4,JudgementName:"R2"},
    //     {JudgementId:5,JudgementName:"R3"}
    //     ];
      $scope.filter = {Dealer:null, Cabang:null, DateStart:periode, DateEnd:periode, reason:null,Vendor:null,judgement:null};

$scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
      $scope.getReport = function(){
          var $tanggal=$scope.filter.DateStart;
          var $filtertanggal=$tanggal.getFullYear()+'/'+($tanggal.getMonth()+1)+'/'+$tanggal.getDate();
          var $tanggalakhir=$scope.filter.DateEnd;
          var $filtertanggalakhir=$tanggalakhir.getFullYear()+'/'+($tanggalakhir.getMonth()+1)+'/'+$tanggalakhir.getDate();
          var dataReport = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'reason': $scope.filter.reason,
            'vendor': $scope.filter.Vendor,
            'judgement': $scope.filter.judgement,
            'kategori':$scope.kategori};

            console.log("$scope.filter ====",$scope.filter);
            if(($scope.filter.Vendor !== undefined && $scope.filter.Vendor !== null)&&($scope.filter.judgement !== undefined && $scope.filter.judgement !== null) && ($scope.filter.reason !== undefined && $scope.filter.reason !== undefined )){
              ReportPartsClaimFactory.getSummaryExport(dataReport);
            }else{
              bsNotify.show({
                size: 'big',
                type: 'danger',
                title: "error",
                content: "Ada field yg kosong "
              });
            }
            // if ((($scope.filter.Vendor == undefined) || ($scope.filter.Vendor == null) || ($scope.filter.Vendor ==  ""))
            // || (($scope.filter.judgement == undefined) || ($scope.filter.judgement == null)|| ($scope.filter.judgement == ""))
            // ||  (($scope.filter.reason == undefined ) || ($scope.filter.reason == null) || ($scope.filter.reason ==""))){
            //   bsNotify.show({
            //     size: 'big',
            //     type: 'danger',
            //     title: "error",
            //     content: "Ada field yg kosong "
            //   });
            // }else{
            //   ReportPartsClaimFactory.getSummaryExport(dataReport);
            // }


    }


});
