angular.module('app')
    .controller('ReportPartsPerformanceController', function($scope, $stateParams,$stateParams,$http, CurrentUser,ReportPartsPerformanceFactory, bsNotify,$timeout) {
    //----------------------------------
    // Start-Up
	$scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
    });

	$scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;
	$scope.role=$stateParams.role;
    $scope.kategori=$stateParams.kategori;
    // $scope.kategori=$stateParams.kategori;
    console.log("$stateParams.kategori anita", $stateParams.kategori);
    console.log('role',$scope.role);
    console.log('ReportPartsPerformance');
    //var $item=angular.fromJson( $stateParams.tab.item);
    //console.log('report productivity',$item,$item.RoleId);
    //$scope.roleId = $item.RoleId;
    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    var dateFormat='dd/MM/yyyy';
    var dateFormat2='yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    var periode =new Date();
    //===============validasi date start and date end===========
    var today = new Date();
    $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(), today.getDate() - 1);
    $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate() - 1);
    $scope.minDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());

    $scope.filter = {TAMArea:0, Dealer:null,DealerId:null,Cabang:null, GroupDealerArea:0, periode:$scope.maxDateS,  Satellite:null, DateStart:$scope.maxDateS, DateEnd:$scope.maxDateS, TipeReport:1,isGR:null};
    $scope.dateOptionsS = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };
    $scope.dateOptionsE = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };
    $scope.dateOptions1 = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };
    $scope.dateOptions2 = {
        startingDay: 1,
        format: dateFormat2,
        //disableWeekend: 1
    };
//


$scope.changeStartDate = function(tgl){
    var today = new Date();
    var dayOne = new Date($scope.filter.DateStart);
    var dayTwo = new Date($scope.filter.DateEnd);

    $scope.dateOptionsE.minDate = new Date(dayOne.getFullYear(),dayOne.getMonth(), dayOne.getDate());
    if (tgl == null || tgl == undefined){
        $scope.filter.DateEnd = null;
    } else {
        if ($scope.filter.DateStart < $scope.filter.DateEnd){

        } else {
            $scope.filter.DateEnd = $scope.filter.DateStart;
        }
    }
}

//=============== End of validasi date start and date end===========

  if($stateParams.kategori == 'GR'){
    $scope.filter.isGR = 1
  }else{
    $scope.filter.isGR = 0
  }

	$scope.dataDealer = [];
	$scope.dataCabang = [];
	$scope.dataAreaTAM = [];
	$scope.dataAreaDealer = [];
    $scope.getData = function(){
            if ($scope.roleId == "Branch") getSummaryBranch(); else getSummaryHO();
            console.log('get data');
    }

    ReportPartsPerformanceFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
        $scope.filter.DealerId=response.data[0].dealerID;
        ReportPartsPerformanceFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
          console.log('ayamtest',response.data)
          $scope.dataGroupDealerHO = response.data
          $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
          $scope.areaId($scope.dataGroupDealerHO[0])
        })
      })
    ReportPartsPerformanceFactory.getDataCabang($scope.kategori).then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
      })
    ReportPartsPerformanceFactory.getDataAreaTAM().then(function (response) {
        console.log('Response AreaTAM:', response.data);
        $scope.dataAreaTAM = response.data;
      })
    ReportPartsPerformanceFactory.getDataAreaDealer().then(function (response) {
        console.log('Response AreaDealer:', response.data);
        $scope.dataAreaDealer = response.data;
      })
    // ReportPartsPerformanceFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
    //     console.log('Response AreaDealerHO:', response.data);
    //     $scope.dataAreaDealerHO = response.data;
    //     $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
    //     $scope.areaId($scope.dataAreaDealerHO[0])
    //   })
    ReportPartsPerformanceFactory.getDataAreaDealerHOParts().then(function (response) {
      console.log('Response AreaDealerHOParts:', response.data);
      $scope.dataAreaDealerHO = response.data;
      $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
      $scope.areaId($scope.dataAreaDealerHO[0])
    })
    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
            $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };

        // $scope.filter.GroupDealerArea = null

        $scope.areaId = function(row) {
          // $scope.DataDealer = row
          console.log('row ', row,$scope.DataDealer);
          if($stateParams.kategori == 'GR'){
            $scope.DataDealer = Object.assign(row,{'kategori':'General Repair'})
          }else{
            $scope.DataDealer = Object.assign(row,{'kategori':'Body and Paint'})
          }

          if($scope.userlogin != 'TAM'){
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
          }else{
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
          }
          console.log('row dealerid', row,$scope.DataDealer);
          // ReportPartsPerformanceFactory.getDataCabangHONew($scope.DataDealer).then(function (response) {
          //   console.log('Response BECabang:', response.data);
          //   $scope.dataCabangHO = response.data;
          //   $scope.DisabledCabang();
          //   $scope.filter.CabangHO = null
          //   console.log('kambing',$scope.filter)
          //   $timeout(function() {
          //     $scope.filter.CabangHO = response.data[0].cabangID;
          //     console.log('kambing123',$scope.filter.CabangHO)
          //   }, 100);
          // })
          ReportPartsPerformanceFactory.getDataCabangALL($scope.DataDealer).then(function (response) {
            console.log('Response BECabang:', response.data);
            $scope.dataCabangHO = response.data;
            $scope.DisabledCabang();
            $scope.filter.CabangHO = null
            console.log('kambing',$scope.filter)
            $timeout(function() {
              $scope.filter.CabangHO = response.data[0].cabangID;
              console.log('kambing123',$scope.filter.CabangHO)
            }, 100);
          })
        };

        $scope.dealerCode = function(row){
          row = Object.assign(row,{'isGR':$scope.filter.isGR})
          $scope.filter.DealerId = row.dealerID
          ReportPartsPerformanceFactory.getDataAreaDealerHO(row).then(function (response) {
              console.log('ayam',response.data)
              console.log('rusa',$scope.dataGroupDealer)
              $scope.dataGroupDealer = response.data
              $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
              $scope.areaId($scope.dataGroupDealer[0])
              console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
          })
        }

	    $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };
     $scope.TipeReport = {data:[{text:"WO",value:1},{text:"Inventory",value:2}]};

     $scope.getSummaryBranch = function(){
        console.log('tipe report Branch',$scope.filter.TipeReport);
        if ($scope.filter.TipeReport==1)
        {
            var $tanggal=$scope.filter.DateStart;
            var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
            var $tanggalakhir=$scope.filter.DateEnd;
            var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
            var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periode': $filtertanggalakhir,
            'periode_awal': $filtertanggal,
            'kategori':$scope.kategori};
             ReportPartsPerformanceFactory.getSummaryCabangWO(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            var hasil= response.data[0];
            $scope.ImmediateFillRate=hasil.immediateFillRate //Math.round(hasil.immediateFillRate);
            $scope.FillRate=hasil.fillRate //Math.round(hasil.fillRate);
            $scope.ServiceRate=hasil.serviceRate //Math.round(hasil.serviceRate);

            // $scope.NoCPUSRevenueAvg=hasil.nonCPUSRevenue/hasil.nonCPUSUnit;

          })

        }else
        {
            var $tanggal=$scope.filter.periode;
            var $filtertanggal=$tanggal.getFullYear()+'-12-31';
            var $filtertanggalawal=$tanggal.getFullYear()+'-1-1';
            var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'periode': $filtertanggal,
            'periode_awal': $filtertanggalawal,
            'kategori':$scope.kategori};
            ReportPartsPerformanceFactory.getSummaryCabangInventory(dataFilter).then(function (response) {
                console.log('Response BE Filter:', response.data);
                var hasil= response.data[0];
                $scope.StockDay=hasil.stockday //Math.round(hasil.stockday);
                $scope.StockEfisiensi=hasil.stockEfficiency //Math.round(hasil.stockEfficiency);
                $scope.BackOrderPerformance=hasil.boPerformance //Math.round(hasil.boPerformance);

                // $scope.NoCPUSRevenueAvg=hasil.nonCPUSRevenue/hasil.nonCPUSUnit;

              })
          }


      }
      $scope.getSummaryHO = function(){
        console.log('tipe report HO',$scope.filter);
        if ($scope.filter.TipeReport==1)
        {
            console.log('tipe report 1');
            var $tanggal=$scope.filter.DateStart;
            var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
            var $tanggalakhir=$scope.filter.DateEnd;
            var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
            var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'TAMArea': $scope.filter.TAMArea,
            'DealerArea': $scope.filter.GroupDealerArea,
            'periode': $filtertanggalakhir,
            'periode_awal': $filtertanggal,
            'kategori':$scope.kategori};
            console.log('filter',dataFilter);
             ReportPartsPerformanceFactory.getSummaryHOWO(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            var hasil= response.data[0];
            $scope.ImmediateFillRate=hasil.immediateFillRate //Math.round(hasil.immediateFillRate);
            $scope.FillRate=hasil.fillRate //Math.round(hasil.fillRate);
            $scope.ServiceRate=hasil.serviceRate //Math.round(hasil.serviceRate);

            // $scope.NoCPUSRevenueAvg=hasil.nonCPUSRevenue/hasil.nonCPUSUnit;

          })

        }else
        {
            console.log('tipe report 2');
            var $tanggal=$scope.filter.periode;
            var $filtertanggal=$tanggal.getFullYear()+'-12-31';
            var $filtertanggalawal=$tanggal.getFullYear()+'-1-1';
            var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'TAMArea': $scope.filter.TAMArea,
            'DealerArea': $scope.filter.GroupDealerArea,
            'periodeakhir': $filtertanggal,
            'periodeawal': $filtertanggalawal,
            'kategori':$scope.kategori};
            ReportPartsPerformanceFactory.getSummaryHOInventory(dataFilter).then(function (response) {
                console.log('Response BE Filter:', response.data);
                var hasil= response.data[0];
                $scope.StockDay=hasil.stockday//Math.round(hasil.stockday);
                $scope.StockEfisiensi=hasil.stockEfficiency //Math.round(hasil.stockEfficiency);
                $scope.RatioPerformanceSupply=hasil.ratioPerformanceSupply;
                $scope.RatioPerformanceSupplyBO=hasil.ratioPerformanceSupplyBO;
                $scope.RatioAccuracySupplyBO=hasil.ratioAccuracySupplyBO;

                // $scope.NoCPUSRevenueAvg=hasil.nonCPUSRevenue/hasil.nonCPUSUnit;

              })



          }

      }

      function getReportBranch(){
          console.log('tipe report Branch',$scope.filter.TipeReport);
          if ($scope.filter.TipeReport==1)
        {
            if(($scope.ImmediateFillRate == undefined) || ($scope.FillRate == undefined) || ($scope.ServiceRate == undefined) ||
               ($scope.ImmediateFillRate == null) || ($scope.FillRate == null) || ($scope.ServiceRate == null)){
                bsNotify.show({
                  size: 'big',
                  type: 'danger',
                  title: "error",
                  content: "Search Data Dahulu!"
                });
            } else
            {
                var $tanggal=$scope.filter.DateStart;
                var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
                var $tanggalakhir=$scope.filter.DateEnd;
                var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
                var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeakhir': $filtertanggalakhir,
                'periodeawal': $filtertanggal,
                'kategori':$scope.kategori};



                ReportPartsPerformanceFactory.getExportCabangWO(dataFilter).then(function (response) {
                    console.log('Response BE Filter:', response.data);
                    var hasil= response.data[0];
                    $scope.CPUSTotalUnit=hasil.cpusUnit;

                    // $scope.NoCPUSRevenueAvg=hasil.nonCPUSRevenue/hasil.nonCPUSUnit;

                })
            }
        } else
        {
            if(($scope.StockDay == undefined) || ($scope.StockEfisiensi == undefined) || ($scope.BackOrderPerformance == undefined) ||
               ($scope.StockDay == null) || ($scope.StockEfisiensi == null) || ($scope.BackOrderPerformance == null))
            {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "error",
                    content: "Search Data Dahulu!"
                  });
            } else
            {
                var $tanggal=$scope.filter.periode;
                console.log("periode",$tanggal);
                var $filtertanggal=$tanggal.getFullYear()+'-12-31';
                var $filtertanggalawal=$tanggal.getFullYear()+'-1-1';
                console.log("tanggal",$filtertanggal);
                var dataFilter = {
                'dealer': $scope.filter.Dealer,
                'cabang': $scope.filter.Cabang,
                'periodeakhir': $filtertanggal,
                'periodeawal': $filtertanggalawal,
                'kategori':$scope.kategori};

                ReportPartsPerformanceFactory.getExportCabangInventory(dataFilter).then(function (response) {
                    console.log('Response BE Filter:', response.data);
                    var hasil= response.data[0];
                    $scope.StockDay=hasil.stockDay//Math.round(hasil.stockDay);
                    $scope.StockEfisiensi=hasil.stockEfficiency //Math.round(hasil.stockEfficiency);
                    $scope.BackOrderPerformance=hasil.bOPerformance //Math.round(hasil.bOPerformance);

                    // $scope.NoCPUSRevenueAvg=hasil.nonCPUSRevenue/hasil.nonCPUSUnit;

                })
            }

        }
    }
    function getReportHO(){

          if ($scope.filter.TipeReport==1)
        {
            var $tanggal=$scope.filter.DateStart;
            var $filtertanggal=$tanggal.getFullYear()+'-'+($tanggal.getMonth()+1)+'-'+$tanggal.getDate();
            var $tanggalakhir=$scope.filter.DateEnd;
            var $filtertanggalakhir=$tanggalakhir.getFullYear()+'-'+($tanggalakhir.getMonth()+1)+'-'+$tanggalakhir.getDate();
            var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'TAMArea': $scope.filter.TAMArea,
            'DealerArea': $scope.filter.GroupDealerArea,
            'periodeakhir': $filtertanggalakhir,
            'periodeawal': $filtertanggal,
            'kategori':$scope.kategori};

          ReportPartsPerformanceFactory.getExportHOWO(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            var hasil= response.data[0];
            $scope.CPUSTotalUnit=hasil.cpusUnit;

            // $scope.NoCPUSRevenueAvg=hasil.nonCPUSRevenue/hasil.nonCPUSUnit;

          })
        }else
        {
            var $tanggal=$scope.filter.periode;
            console.log("periode",$tanggal);
            var $filtertanggal=$tanggal.getFullYear()+'-12-31';
            var $filtertanggalawal=$tanggal.getFullYear()+'-1-1';
            console.log("tanggal",$filtertanggal);
            var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'TAMArea': $scope.filter.TAMArea,
            'DealerArea': $scope.filter.GroupDealerArea,
            'periodeakhir': $filtertanggal,
            'periodeawal': $filtertanggalawal,
            'kategori':$scope.kategori};

          ReportPartsPerformanceFactory.getExportHOInventory(dataFilter).then(function (response) {
            console.log('Response BE Filter:', response.data);
            var hasil= response.data[0];
            $scope.StockDay=hasil.stockDay //Math.round(hasil.stockDay);
            $scope.StockEfisiensi=hasil.stockEfficiency //Math.round(hasil.stockEfficiency);
            $scope.BackOrderPerformance=hasil.bOPerformance //Math.round(hasil.bOPerformance);

            // $scope.NoCPUSRevenueAvg=hasil.nonCPUSRevenue/hasil.nonCPUSUnit;

          })
          }
        }
        $scope.getReport=function(){
            if ($scope.role=='Branch') getReportBranch(); else if ($scope.role=='HO') getReportHO();
        }
        $scope.getData=function(){
            if ($scope.role=='Branch') $scope.getSummaryBranch(); else $scope.getSummaryHO();
        }


});
