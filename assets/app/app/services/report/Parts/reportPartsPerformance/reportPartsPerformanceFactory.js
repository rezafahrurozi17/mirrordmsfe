angular.module('app')
  .factory('ReportPartsPerformanceFactory', function($http, CurrentUser,$q) {
    console.log('Service PartsPerformance Loaded...');
    var currentUser = CurrentUser.user();
    var serverURL =''; //http://localhost:8080';
    return {
            getDataDealer: function() {
                  var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
                  return res;},
            // getDataCabang: function() {
            //       var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            //       return res;},
            getDataCabang: function(kategori) {
                  var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId + '/' + kategori);
                  return res;},
            getDataCabangHONew: function(data) {
              console.log('Data Get Cabang:', data);
              var res=$http.get(serverURL + '/api/rpt/GetCabangHONew/'+ data.areaId + '/' +  data.DealerId + '/' + data.kategori);
              return res;},
            getDataCabangALL: function(data) {
              console.log('Data Get ALL:', data);
              var res=$http.get(serverURL + '/api/rpt/GetCabangALL/'+ data.areaId + '/' +  data.DealerId );
              return res;},
            getDataAreaTAM: function() {
                  var res=$http.get(serverURL + '/api/rpt/AreaTAM/');
                  return res;},
            getDataAreaDealer: function() {
                  var res=$http.get(serverURL + '/api/rpt/AreaDealer/');
                  return res;},
            getDataAreaDealerHO: function(data) {
                  var res=$http.get(serverURL + '/api/rpt/AreaDealerHONew/'+ data.isGR );
                  return res;},
            getDataAreaDealerHOParts: function() {
                  var res=$http.get(serverURL + '/api/rpt/AreaDealerHOParts/' );
                  return res;},            
            getSummaryCabang: function(data) {
              console.log('Data Filter:', data);
              var res = $http.get(serverURL + '/api/rpt/ACabPartsPerformanceInventory/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode+'/'+ data.periode_awal +'/'+ data.kategori);
              return res;},
            getSummaryCabangInventory: function(data) {
              console.log('Data Filter cabang inventory:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_PartsPerformanceInventory_PartsCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode_awal+'/'+ data.periode +'/'+ data.kategori);

              return res;},
            getSummaryCabangWO: function(data) {
              console.log('Data Filter:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_PartsPerformanceWO_PartsCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode_awal+'/'+ data.periode +'/'+ data.kategori);
              return res;},
            getSummaryHOWO: function(data) {
              console.log('Data Filter1:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_PartsPerformanceWO_PartsHO/'+ data.TAMArea + '/' +data.dealer +'/'+data.DealerArea +'/'+ data.cabang +'/'+ data.periode_awal+'/'+ data.periode +'/'+ data.kategori);
              return res;},
            getSummaryHOInventory: function(data) {
              console.log('Data Filter HO Inv:', data);
              var res = $http.get(serverURL + '/api/rpt/AS_PartsPerformanceInventory_PartsHO/'+  data.TAMArea + '/' +data.dealer +'/'+data.DealerArea +'/'+ data.cabang +'/'+ data.periodeawal+'/'+ data.periodeakhir +'/'+ data.kategori);
              return res;},


            getExportCabangInventory: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_PartsPerformanceInventory_PartsCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periodeawal+'&kategori='+data.kategori);
            },

            getExportCabangWO: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_PartsPerformanceWO_PartsCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir+'&kategori='+data.kategori);
            },
            getExportHOInventory: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_PartsPerformanceInventory_PartsHO?dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periodeawal +'&TAMArea='+ data.TAMArea +'&DealerArea='+ data.DealerArea +'&jenis=export&enddate='+data.periodeakhir+'&kategori='+data.kategori);
            },

            getExportHOWO: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_PartsPerformanceWO_PartsHO?dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periodeawal +'&dateend='+ data.periodeakhir +'&TAMArea='+ data.TAMArea +'&DealerArea='+ data.DealerArea+'&kategori='+data.kategori);
            }
    }
  });
