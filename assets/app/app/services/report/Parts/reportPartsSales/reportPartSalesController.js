angular.module('app')
    .controller('ReportPartsSalesController', function($scope,$stateParams, $http, CurrentUser, ReportPartsSalesFactory, CAccessMatrix, bsNotify,$timeout ) {
    // ----------------------------------
    // Start-Up

    // ----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        $scope.getOrgData();
    });

    //----------------------------------
    // Initialization
    $scope.user = CurrentUser.user();
    $scope.orgData = []; //OrgChart Collection
    $scope.outletData= [];
    //----------------------------------
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    $scope.user = CurrentUser.user();
    $scope.roleId = $scope.user.RoleId;
    $scope.kategori=$stateParams.kategori;
    var periode=new Date();
        $scope.dateOptionsS = {
        startingDay: 1,
        format: dateFormat,

        // mode:"'month'",
    };
    $scope.dateOptionsE = {
        startingDay: 1,
        format: dateFormat,

        // mode:"'month'",
    };


    $scope.getJenis=[{"jenis":"ALL"},{"jenis":"Direct Sales"},{"jenis":"WO"}];
    $scope.role=$stateParams.role;
    console.log('role',$scope.role);
    $scope.filter         = {Dealer: null,DealerId: null, Cabang: null,jenis: "ALL", DateStart: periode, DateEnd: periode, DealerArea: 0,tamArea: 0,isGR:null};
    $scope.dataDealer     = [];
    $scope.dataCabang     = [];
    $scope.dataAreaDealer = [];
    $scope.dataAreaTAM    = [];






    $scope.filterMaterialType =  [
        { text: "ALL", value: 1 }, 
        { text: "Part", value: 2 }, 
        { text: "Bahan", value: 3 }, 
    ];

    $scope.changeMaterialType = function(selected){
        console.log('changeMaterialType ===>',selected);
        $scope.filter.materialType = selected.value;

        if(selected.text == "Part"){
            $scope.summaryParts = 1;
            // $scope.summaryBahan = 0;
            $scope.summaryOLI   = 0;
        }
        // else if(selected.text == "Bahan"){
        //     $scope.summaryParts = 0;
        //     $scope.summaryBahan = 1;
        //     $scope.summaryOLI   = 0;
        // }
        else{
            $scope.summaryParts = 1;
            // $scope.summaryBahan = 1;
            $scope.summaryOLI   = 1;
        }
     
    }

    $scope.changeMaterialType($scope.filterMaterialType[0]);




    //===============validasi date start and date end===========
    var today = new Date();
    $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(), today.getDate());
    $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());
    $scope.minDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());


    $scope.changeStartDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.DateStart);
        var dayTwo = new Date($scope.filter.DateEnd);

        $scope.dateOptionsE.minDate = new Date(dayOne.getFullYear(),dayOne.getMonth(), dayOne.getDate());
        if (tgl == null || tgl == undefined){
            $scope.filter.DateEnd = null;
        } else {
            if ($scope.filter.DateStart < $scope.filter.DateEnd){

            } else {
                $scope.filter.DateEnd = $scope.filter.DateStart;
            }
        }
    }

    //=============== End of validasi date start and date end===========






    
    // if ($scope.roleId==1128 || $scope.roleId==1129) {
        //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", KategoriWorkshop:"General Repair",jenis:"ALL", DateStart:null, DateEnd:null};
        // } else {
            //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", KategoriWorkshop:"General Repair",jenis:"ALL", DateStart:null, DateEnd:null};
            // }
   

    //Get DealerArea
    $scope.getOrgData = function(){
      console.log("user==>",$scope.user);
        CAccessMatrix.getDealOut($scope.user).then(function(result) {
          $scope.orgData = result.data.Result;
          console.log("orgData=>",result.data);
        });
    }

    //Get outlet
    $scope.selectDealer = function(xD){
      console.log("dealeeer==>",xD);
      // $scope.outletData = xD.Child;
      CAccessMatrix.getOutlet(xD).then(function(result) {
        $scope.outletData = result.data.Result;
        console.log("Dn==>",result.data.Result);
      })



    }

    if($stateParams.kategori == 'GR'){
        $scope.filter.isGR = 1
    }else{
        $scope.filter.isGR = 0
    }

    ReportPartsSalesFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.dataDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
        $scope.filter.DealerId=response.data[0].dealerID;
        ReportPartsSalesFactory.getDataAreaDealerHO($scope.filter).then(function (response) {
          console.log('ayamtest',response.data)
          $scope.dataGroupDealerHO = response.data
          $scope.dataGroupDealerHO[0] = Object.assign($scope.dataGroupDealerHO[0],{'DealerId':$scope.filter.DealerId})
          $scope.areaId($scope.dataGroupDealerHO[0])
        })
    })
    ReportPartsSalesFactory.getDataCabang($scope.kategori).then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.dataCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
    })
    ReportPartsSalesFactory.getDataAreaTAM().then(function (response) {
        console.log('Response AreaTAM:', response.data);
        $scope.dataAreaTAM = response.data;
      })
    ReportPartsSalesFactory.getDataAreaDealer().then(function (response) {
        console.log('Response AreaDealer:', response.data);
        $scope.dataAreaDealer = response.data;
      })
    ReportPartsSalesFactory.getDataAreaDealerHO($scope.filter).then(function(response) {
        console.log('Response AreaDealerHO:', response.data);
        $scope.dataAreaDealerHO = response.data;
        $scope.dataAreaDealerHO[0] = Object.assign($scope.dataAreaDealerHO[0],{'DealerId':$scope.filter.DealerId})
        $scope.areaId($scope.dataAreaDealerHO[0])
    })
    var currentUser = CurrentUser.user();
    console.log('LOGIN :', currentUser.OrgId)
    $scope.userlogin = currentUser.OrgCode.substring(0, 3);
    console.log('Dealer :', currentUser.OrgCode.substring(0, 3))
            $scope.DisabledDealer = function() {
         if ($scope.userlogin == 'TAM') {
           return true;
           console.log("DisabledDealer TRUE"); }
          else {
           return false;
           console.log("DisabledDealer FALSE"); }
        };
        $scope.DisabledTAM = function() {
         if ($scope.userlogin != 'TAM') {
           return true;
           console.log("DisabledTAM TRUE"); }
          else {
           return false;
           console.log("DisabledTAM FALSE"); }
        };
        

        $scope.areaId = function(row) {
          // $scope.DataDealer = row
          console.log('row ', row,$scope.DataDealer);
          if($stateParams.kategori == 'GR'){
            $scope.DataDealer = Object.assign(row,{'kategori':'General Repair'})
          }else{
            $scope.DataDealer = Object.assign(row,{'kategori':'Body and Paint'})
          }
          if($scope.userlogin != 'TAM'){
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
          }else{
            $scope.DataDealer = Object.assign($scope.DataDealer,{'DealerId':$scope.filter.DealerId})
          }
          console.log('row dealerid', row,$scope.DataDealer);
        //   ReportPartsSalesFactory.getDataCabangHONew($scope.DataDealer).then(function (response) {
        //     console.log('Response BECabang:', response.data);
        //     $scope.dataCabangHO = response.data;
        //     $scope.DisabledCabang();
        //     $scope.filter.CabangHO = null
        //     console.log('kambing',$scope.filter)
        //     $timeout(function() {
        //       $scope.filter.CabangHO = response.data[0].cabangID;
        //       console.log('kambing123',$scope.filter.CabangHO)
        //     }, 100);
        //   })

        ReportPartsSalesFactory.getDataCabangALL($scope.DataDealer).then(function (response) {
            console.log('Response ALL:', response.data);
                $scope.dataCabangHO = response.data;
                $scope.DisabledCabang();
                $scope.filter.CabangHO = null
                console.log('kambing',$scope.filter)
                $timeout(function() {
                  $scope.filter.CabangHO = response.data[0].cabangID;
                  console.log('kambing123',$scope.filter.CabangHO)
                }, 100);
            })
        };

        $scope.dealerCode = function(row){
            row = Object.assign(row,{'isGR':$scope.filter.isGR})
            $scope.filter.DealerId = row.dealerID
            ReportPartsSalesFactory.getDataAreaDealerHO(row).then(function (response) {
                console.log('ayam',response.data)
                console.log('rusa',$scope.dataGroupDealer)
                $scope.dataGroupDealer = response.data
                $scope.dataGroupDealer[0] = Object.assign($scope.dataGroupDealer[0],{'DealerId':row.dealerID})
                $scope.areaId($scope.dataGroupDealer[0])
                console.log('sapi',row,$scope.filter,$scope.dataGroupDealer[0])
            })
        }

        $scope.DisabledCabang = function() {
            if ($scope.dataCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.dataCabang.length + ' ' + $scope.Disabled);
            }
        };

function format3(n, currency) {
    return currency + " " + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
    });
}

function format1(n, currency){
    var with2Decimals = n.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]
    return with2Decimals.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function format2(n, currency){
    var with1Decimals = n.toString().match(/^-?\d+(?:\.\d{0,1})?/)[0]
    return with1Decimals.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function round(value, precision) {
    var multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
}

    $scope.getData = function(){
            if ($scope.role == "Branch") $scope.getDataCabang();
            else $scope.getDataHO();
            console.log('filter ====>',$scope.filter);
    }
    $scope.getDataCabang = function() {
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var $tipeMaterial = 0;
        if($scope.filter.materialType == 3){
            $tipeMaterial = 2;
        }else if($scope.filter.materialType == 2){
            $tipeMaterial = 1;
        }else{
            $tipeMaterial = 0;
        }
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'sales_type': $scope.filter.jenis,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,
            'Jenis': 'summary',
            'kategori':$scope.kategori,
            'tipeMaterial':$tipeMaterial};

          ReportPartsSalesFactory.getSummaryFilter(dataFilter).then(function (response) {
            console.log('getDataCabang : Response BE Filter ===>', response.data);
            var hasil=response.data;
            var beli = 0;
            var jual = 0;
            var profit = 0;
            var jual_oli = 0;
            var beli_oli = 0;
            var profit_oli = 0;
            var tmpProfitOli =0;
            var tmpProfitParts = 0;
            $scope.partBahanBeli = 0;
            $scope.partBahanJual = 0;

            $scope.olibeli         = format1(0,'');
            $scope.olijual         = format1(0,'');
            $scope.oliprofit       = format1(0,'');
            $scope.partBahanBeli   = format1(0,'');
            $scope.partBahanJual   = format1(0,'');
            $scope.partBahanProfit = format1(0,'');


            for(i=0;i<hasil.length;i++)
            {
                var row=hasil[i];
                if (row.franchise=="Oil"){
                    // $scope.olibeli=format1(row.beli,'');
                    // $scope.olijual=format1(row.jual,'');
                    jual_oli = row.jual;
                    beli_oli = row.beli;
                    profit_oli = row.profit;
                    // $scope.oliprofit =format2(row.profit,'');
                    $scope.olibeli=format3(row.beli,'');
                    $scope.olijual=format3(row.jual,'');
                    $scope.oliprofit=format3(row.profit,'');
                    
                    // $scope.olibeli=parseFloat(row.beli).toFixed(2);
                    // $scope.olijual=parseFloat(row.jual).toFixed(2);
                    // $scope.oliprofit=parseFloat(row.profit).toFixed(2);
                    console.log("$scope.oliprofit",$scope.oliprofit);
                    // $scope.oliprofit = parseFloat(tmpProfitOli.toFixed(1));
                }
                if (row.franchise=="Parts" && $scope.filter.materialType != 3){
                    beli = beli + row.beli;
                    jual = jual + row.jual;
                    profit = profit + row.profit;
                    console.log('Part')
                    // $scope.partBahanBeli=format1(beli,'');
                    // $scope.partBahanJual=format1(jual,'');
                }
                if (row.franchise=="Bahan" && $scope.filter.materialType != 2){
                    beli = beli + row.beli;
                    jual = jual + row.jual;
                    profit = profit + row.profit;
                    console.log('Bahan')
                    // $scope.partBahanBeli=format1(beli,'');
                    // $scope.partBahanJual=format1(jual,'');
                }
            }
            profit = profit;
            console.log('profit 1 =>', jual, '-', beli,profit);

            //request by mbak merry, harga OLI di jumlahkan dengan harga Parts/Bahan 13/11/2020

            /*$scope.partBahanBeli=format1(beli+beli_oli,'');
            $scope.partBahanJual=format1(jual+jual_oli,'');
            $scope.partBahanProfit=format2(profit+profit_oli,'');*/
			$scope.partBahanBeli=format3(beli,'');
            $scope.partBahanJual=format3(jual,'');
			$scope.partBahanProfit=format3(profit,'');
            // $scope.partBahanBeli=parseFloat(beli).toFixed(2);
            // $scope.partBahanJual=parseFloat(jual).toFixed(2);
            // $scope.partBahanProfit=parseFloat(profit).toFixed(2);
            console.log("$scope.partBahanProfit",$scope.partBahanProfit);
            // $scope.partBahanProfit = parseFloat(tmpProfitParts.toFixed(1));
          })
      }
      $scope.getReport = function(){
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'/'+($tanggal1.getMonth()+1)+'/'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'/'+($tanggal2.getMonth()+1)+'/'+$tanggal2.getDate();
        var $tipeMaterial = 0;
        if($scope.filter.materialType == 3){
            $tipeMaterial = 2;
        }else if($scope.filter.materialType == 2){
            $tipeMaterial = 1;
        }else{
            $tipeMaterial = 0;
        }
        if ($scope.role=="Branch")
        {
            var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,
            'sales_type': $scope.filter.jenis,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,
            'Jenis': 'summary',
            'kategori':$scope.kategori,
            'tipeMaterial':$tipeMaterial};

            ReportPartsSalesFactory.getSummaryExport(dataFilter);
        }else if($scope.role=="HO")
        {
            if (($scope.filter.Dealer == null || $scope.filter.Dealer == undefined) ||
                ($scope.filter.Cabang == null || $scope.filter.Cabang == undefined) ||
                ($scope.filter.jenis == null || $scope.filter.jenis == undefined) ||
                ($filtertanggal1 == null || $filtertanggal1 == undefined) ||
                ($filtertanggal2 == null || $filtertanggal2 == undefined) ||
                ($scope.filter.DealerArea == null || $scope.filter.DealerArea == undefined) ||
                ($scope.filter.tamArea == null || $scope.filter.tamArea == undefined)
            ){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Isi seluruh data yang kosong",
                });


            } else {
                var dataFilter = {
                    'dealer': $scope.filter.Dealer,
                    'cabang': $scope.filter.CabangHO,
                    'sales_type': $scope.filter.jenis,
                    'periode_awal': $filtertanggal1,
                    'periode_akhir': $filtertanggal2,
                    'groupdealerarea':$scope.filter.DealerArea,
                    'tamarea':$scope.filter.tamArea,
                    'Jenis': 'export',
                    'kategori':$scope.kategori,
                    'typematerial':$tipeMaterial};

                    ReportPartsSalesFactory.getSummaryExportHO(dataFilter);
            }

        }

    }
    $scope.getDataHO = function() {
        var tmpOliProfit = 0;
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var $tipeMaterial = 0;
        if($scope.filter.materialType == 3){
            $tipeMaterial = 2;
        }else if($scope.filter.materialType == 2){
            $tipeMaterial = 1;
        }else{
            $tipeMaterial = 0;
        }
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.CabangHO,
            'sales_type': $scope.filter.jenis,
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,
            'groupdealerarea':$scope.filter.DealerArea,
            'tamarea':$scope.filter.tamArea,
            'Jenis': 'summary',
            'kategori':$scope.kategori,
            'typematerial':$tipeMaterial};

          ReportPartsSalesFactory.getSummaryFilterHO(dataFilter).then(function (response) {
            console.log('Response BE Filter HO summary:', response.data);
            var hasil=response.data;
            var beli = 0;
            var jual = 0;
            var profit = 0;
            var jual_oli = 0;
            var beli_oli = 0;
            var profit_oli = 0;

            for(i=0;i<hasil.length;i++)
            {
                var row=hasil[i];
                if (row.franchise=="Oil")
                {
                    beli_oli = beli_oli + row.beli;
                    jual_oli = jual_oli + row.jual;
                    profit_oli = profit_oli + row.profit;            
                    // $scope.olibeli=parseFloat(row.beli).toFixed(2);
                    // $scope.olijual=parseFloat(row.jual).toFixed(2);
                    // $scope.oliprofit=parseFloat(row.jual-row.beli).toFixed(2);
                }
                if (row.franchise=="Parts" && $scope.filter.materialType != 3){
                    beli = beli + row.beli;
                    jual = jual + row.jual;
                    profit = profit + row.profit;
                    console.log('Part')
                    // $scope.partBahanBeli=format1(beli,'');
                    // $scope.partBahanJual=format1(jual,'');
                }
                if (row.franchise=="Bahan" && $scope.filter.materialType != 2){
                    console.log('Bahan',row.profit)
                    beli = beli + row.beli;
                    jual = jual + row.jual;
                    profit = profit + row.profit;
                    // $scope.partBahanBeli=format1(beli,'');
                    // $scope.partBahanJual=format1(jual,'');
                }
            }
            console.log(profit)
            profit = profit

            $scope.partBahanBeli=format3(beli,'');
            $scope.partBahanJual=format3(jual,'');
            $scope.partBahanProfit=format3(profit,'');
            $scope.olibeli=format3(beli_oli,'');
            $scope.olijual=format3(jual_oli,'');
            $scope.oliprofit=format3(profit_oli,'');
            // $scope.partBahanBeli=parseFloat(row.beli).toFixed(2);
            // $scope.partBahanJual=parseFloat(row.jual).toFixed(2);
            // $scope.partBahanProfit=parseFloat(row.jual-row.beli).toFixed(2);
          })
      }


});
