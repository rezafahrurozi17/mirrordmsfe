angular.module('app')
  .factory('ReportPartsSalesFactory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = '';//http://localhost:8080'
    return {
      getDataDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},
      // getDataCabang: function() {
      //       var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
      //       return res;},
      getDataCabang: function(kategori) {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId + '/' + kategori);
            return res;},
      getDataCabangHONew: function(data) {
            console.log('Data Get Cabang:', data);
            var res=$http.get(serverURL + '/api/rpt/GetCabangHONew/'+ data.areaId + '/' +  data.DealerId + '/' + data.kategori);
            return res;},
       getDataCabangALL: function(data) {
            console.log('Data Get ALL:', data);
            var res=$http.get(serverURL + '/api/rpt/GetCabangALL/'+ data.areaId + '/' +  data.DealerId );
            return res;},
      getDataAreaTAM: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaTAM/');
            return res;},
      getDataAreaDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/AreaDealer/');
            return res;},
      getDataAreaDealerHO: function(data) {
            var res=$http.get(serverURL + '/api/rpt/AreaDealerHONew/'+ data.isGR );
            return res;},
      getSummaryFilter: function(data) {
              console.log('Data Filter:', data);
              // console.log('periode',moment(data.periode).format('YYYY-MM-DD'))
              var res = $http.get(serverURL + '/api/rpt/AS_PartsSales_PartsCAB/'+ data.dealer +'/'+ data.cabang +'/'+ data.sales_type +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/'+ data.kategori +'/'+ data.tipeMaterial);
              return res;},
      getSummaryExport: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL +'/api/rpt/AS_PartsSales_PartsCAB?dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periode_awal +'&dateend='+ data.periode_akhir +'&salestype='+ data.sales_type+'&kategori='+data.kategori +'&TypeMaterial='+ data.tipeMaterial);
              // var res = $http.get(serverURL + 'CCustomerCompleteness?area='+ data.area +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periode +'&jenis_pelanggan='+ data.jenispel);
              // return res;
            },
      getSummaryFilterHO: function(data) {
              console.log('Data Filter:', data);
              // {dealer}/{outletcode}/{datestart}/{dateend}/{TAMArea}/{GroupDealerArea}/{grup}/{Jenis}
              var res = $http.get(serverURL + '/api/rpt/AS_PartsSales_PartsHO/'+ data.dealer +'/'+ data.cabang +'/'+ data.periode_awal +'/'+ data.periode_akhir +'/'+ data.tamarea +'/'+ data.groupdealerarea +'/'+ data.sales_type +'/'+ data.kategori +'/'+data.typematerial);
              return res;},

      getSummaryExportHO: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_PartsSales_PartsHO?dealer='+ data.dealer +'&cabang='+ data.cabang +'&datestart='+ data.periode_awal +'&dateend='+ data.periode_akhir +'&TAMArea='+ data.tamarea +'&GroupDealerArea='+ data.groupdealerarea +'&Grup='+ data.sales_type+'&kategori='+data.kategori+'&TypeMaterial='+ data.typematerial);
              // var res = $http.get(serverURL + 'CCustomerCompleteness?area='+ data.area +'&dealer='+ data.dealer +'&cabang='+ data.cabang +'&periode='+ data.periode +'&jenis_pelanggan='+ data.jenispel);
              // return res;
            }

    }
  });
