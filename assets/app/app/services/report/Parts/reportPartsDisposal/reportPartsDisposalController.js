angular.module('app')
    .controller('ReportPartsDisposalController', function($scope, $http,$stateParams, CurrentUser, ReportPartsDisposalFactory ) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=false;
        // $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    var periode=new Date();
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    $scope.kategori=$stateParams.kategori;
    $scope.dateOptionsS = {
        startingDay: 1,
        format: dateFormat,
        
        // mode:"'month'",
        //disableWeekend: 1
    };    
    $scope.dateOptionsE = {
        startingDay: 1,
        format: dateFormat,
        
        // mode:"'month'",
        //disableWeekend: 1
    };   


    //===============validasi date start and date end===========
    var today = new Date();
    $scope.maxDateS = new Date(today.getFullYear(),today.getMonth(), today.getDate());  
    $scope.maxDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());      
    $scope.minDateE = new Date(today.getFullYear(),today.getMonth(), today.getDate());          


    $scope.changeStartDate = function(tgl){
        var today = new Date();
        var dayOne = new Date($scope.filter.DateStart);
        var dayTwo = new Date($scope.filter.DateEnd);
        
        $scope.dateOptionsE.minDate = new Date(dayOne.getFullYear(),dayOne.getMonth(), dayOne.getDate());
        if (tgl == null || tgl == undefined){
            $scope.filter.DateEnd = null;
        } else {
            if ($scope.filter.DateStart < $scope.filter.DateEnd){

            } else {
                $scope.filter.DateEnd = $scope.filter.DateStart;
            }
        }
    }

    //=============== End of validasi date start and date end===========




    
    $scope.roleId = $scope.user.RoleId;
    // if ($scope.roleId==1128 || $scope.roleId==1129) {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", KategoriWorkshop:"General Repair", 
    //                 DateStart:null, DateEnd:null, referensi:2, material:1};
    // } else {
    //     $scope.filter = {Dealer:"TAM", Cabang:"TAM000000", KategoriWorkshop:"General Repair", 
    //                 DateStart:null, DateEnd:null, referensi:2, material:1};
    // }
    $scope.dataDealer = [];
    $scope.getCabang = [];
    $scope.getMaterial = [];
    $scope.filter = {Dealer:null, Cabang:null, DateStart:periode, DateEnd:periode, referensi:'All', material:1};
    ReportPartsDisposalFactory.getDataDealer().then(function (response) {
        console.log('Response BEDealer:', response.data);
        $scope.getDealer = response.data;
        $scope.filter.Dealer=response.data[0].dealerCode;
    })
    ReportPartsDisposalFactory.getDataCabang().then(function (response) {
        console.log('Response BECabang:', response.data);
        $scope.getCabang = response.data;
        $scope.DisabledCabang();
        $scope.filter.Cabang=response.data[0].cabangID;
    })
    ReportPartsDisposalFactory.getTipeMaterial().then(function (response) {
        console.log('Response BEMaterial:', response.data);
        $scope.getMaterial = response.data;
    })
    $scope.DisabledCabang = function() {
            if ($scope.getCabang.length>1){
                $scope.Disabled =false;
                console.log('Cabang Enabled' , $scope.getCabang.length + ' ' + $scope.Disabled);
            }
            else{
                $scope.Disabled =true;
                console.log('Cabang Disabled', $scope.getCabang.length + ' ' + $scope.Disabled);
            }
        };


    $scope.getReferensi = [
        {ReferensiId:0,ReferensiName:"All"},
        {ReferensiId:2,ReferensiName:"Langsung"},
        {ReferensiId:1,ReferensiName:"Parts Claim"}
    ];
    
    // $scope.roleId = 1; // 2 = HO | 1 = Branch

    $scope.getData = function(){
    }

    $scope.getReport = function(){
        var $tanggal1=$scope.filter.DateStart;
        var $tanggal2=$scope.filter.DateEnd;
        var $filtertanggal1=$tanggal1.getFullYear()+'-'+($tanggal1.getMonth()+1)+'-'+$tanggal1.getDate();
        var $filtertanggal2=$tanggal2.getFullYear()+'-'+($tanggal2.getMonth()+1)+'-'+$tanggal2.getDate();
        var dataFilter = {
            'dealer': $scope.filter.Dealer,
            'cabang': $scope.filter.Cabang,            
            'periode_awal': $filtertanggal1,
            'periode_akhir': $filtertanggal2,            
            'referensi': $scope.filter.referensi,            
            'tipe_material': $scope.filter.material,
            'kategori':$scope.kategori};

        ReportPartsDisposalFactory.getSummaryExport(dataFilter);
    }
    

    
});
