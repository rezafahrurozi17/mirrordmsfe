angular.module('app')
  .factory('ReportPartsDisposalFactory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user();
    console.log(currentUser);
    var serverURL = ''; //'http://localhost:8080';
    return {
      getDataDealer: function() {
            var res=$http.get(serverURL + '/api/rpt/Dealer/'+ currentUser.OrgId);
            return res;},
      getDataCabang: function() {
            var res=$http.get(serverURL + '/api/rpt/Cabang/'+ currentUser.OrgId);
            return res;},
      getTipeMaterial: function() {
            var res=$http.get(serverURL + '/api/rpt/TipeMaterial/');
            return res;},  
      getSummaryExport: function(data) {
              console.log('Data Report:', data);
              window.open(serverURL + '/api/rpt/AS_PartsStockDisposal_PartsCAB?Dealer='+ data.dealer +'&Cabang='+ data.cabang +'&TipeMaterial='+ data.tipe_material +'&Periode_Awal='+ data.periode_awal +'&Periode_Akhir='+ data.periode_akhir +'&Referensi='+ data.referensi+'&kategori='+data.kategori);
            }
    }
  });
