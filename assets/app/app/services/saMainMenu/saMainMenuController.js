angular.module('app')
  .controller('saMainMenuController', function($scope, $http, CurrentUser, $timeout) {
    var showBoard = function() {

      $scope.click = function() {
        alert("gundam");
      };

      $scope.data = [{
          'policenumber': "B 1234 H",
          'modelname': "Avanza"
        },
        {
          'policenumber': "B 1234 Z",
          'modelname': "Innova"
        }
      ];

      var boardConfig = {
        type: 'saMainMenu',
        board: {
          container: 'customer-status-gr',
          chipGroup: 'saMainMenu', //<---- group dari chip
          headerHeight: 50,
        },
        stage: {
          height: 480,
          width: 720,
        }
      }

      var statusBoard = new JsBoard.Container(boardConfig);

      var theChips = [{
          chip: 'header',
          title: 'INFORMASI STATUS SERVIS',
          tanggal: 'SA Assignment',
          jam: '10:00',
          lokasi: 'header'
        },
        /*, //<--- definisikan chip dengan nama header
                {
                  chip: 'test',
                  title: 'JUDUL'
                }*/
      ]

      statusBoard.createChips(theChips); //<--- untuk menampilkan chip
      statusBoard.redraw(); //<--- harus di-redraw

    }

    $scope.$on('$viewContentLoaded', function() {

      $scope.loading = true;
      $scope.gridData = [];

      $timeout(showBoard, 10);

    });

    $scope.dummy = {
      NoPolisi: 'AB 10 KZ',
      Date: '20-12-2017',
      Time: '12:05',
      ModelName: 'CRV',
      Place: 'Pramuka',

    }
  });
