angular.module('app')
    .controller('followupServiceController', function($scope, $http, CurrentUser, FollowupService, bsAlert,uiGridConstants, AppointmentGrService, $timeout, bsNotify, ngDialog, WOBP) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.dataId = [];
        $scope.user = CurrentUser.user();
        // console.log('$scope.user', $scope.user)
        $scope.mFService = []; //Model
        $scope.xFService = {};
        $scope.xFService.selected = [];
        $scope.showFollowService = false;
        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var selectedRow = [];
        // $scope.smsText = null;
        $scope.Negative = [];
        $scope.disableQuestion = [];
        $scope.modalMode = 'new';
        $scope.dataGrid = [];
        $scope.resizeLayout = function(){
            $timeout(function() {
                var staticHeight =
                    $("#page-footer").outerHeight() +
                    $(".nav.nav-tabs").outerHeight() +
                    $(".ui.breadcrumb").outerHeight() + 85;
                $("#layoutContainer_FollowupServiceForm").height($(window).height() - staticHeight - 5);
            }, 500);
        }
        $scope.show_modal = {
            sms: false
        };
        $scope.customBulk = [{
            title: 'SMS',
            func: function() {
                // selectedRow
                var trigger = 0;
                // console.log('data sms', selectedRow);
                _.map(selectedRow, function(val) {
                    console.log('val ', val);
                    if (val.SMSSent != true) {
                        trigger = 1;
                    };
                });
                // console.log('trigger', trigger);
                if (trigger == 0) {
                    $scope.gridSms.data = selectedRow;
                    $scope.show_modal.sms = true;
                } else {
                    bsAlert.alert({
                        title: "Data yang anda pilih tidak Valid untuk SMS",
                        text: "Silahkan Cek kembali !",
                        type: "warning"
                    })
                };
            }
        }, {
            title: 'Delete',
            func: function() {
                _.map(selectedRow, function(val) {
                    console.log('val ', val.Id);
                    $scope.dataId.push(val.Id);
                    console.log('cek data', $scope.dataGrid.Grid);
                    ngDialog.openConfirm({
                        template: '\
                                  <div align="center" class="ngdialog-buttons">\
                                  <p><b>Konfirmasi</b></p>\
                                  <p>Anda yakin delete data ini?</p>\
                                  <div class="ngdialog-buttons" align="center">\
                                    <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">Tidak</button>\
                                    <button type="button" class="ngdialog-button ngdialog-button-primary"  ng-click="deleteAja(dataId)">Ya</button>\
                                  </div>\
                                  </div>',
                        plain: true,
                        scope: $scope
                    });




                });
            }
        }];

        $scope.deleteAja = function(id) {
            console.log('id', $scope.dataId);
            $scope.ngDialog.close();
            FollowupService.delete(id).then(function(res) {
                $scope.getData();
                console.log('masuk dong', res);
            });
        }

        $scope.gridSms = {
            columnDefs: [{
                name: 'No Polisi',
                field: 'Job.PoliceNumber'
            }, {
                name: 'Nama Pelanggan',
                field: 'Job.ContactPerson'
            }, {
                name: 'Handphone',
                field: 'PhoneNumber'
            }, {
                name: 'Tanggal Service',
                //field: 'Job.JobDate',
                field: 'Job.WoCreatedDate',
                cellFilter: dateFilter
            }]
        };
        $scope.statusData = [
            { Id: 1, Name: "Terhubung" },
            { Id: 2, Name: "Tidak Terhubung" },
            { Id: 3, Name: "Follow Up Ulang" }
        ];
        $scope.resultData = [
            { Id: 1, Name: "Contacted" },
            { Id: 2, Name: "Not Contacted" },
            { Id: 3, Name: "Connected" },
            { Id: 4, Name: "Not Connected" },
            { Id: 5, Name: "Booked" },
            { Id: 6, Name: "Canceled" }
        ];
        // $scope.reasonData = [
        //     {Name:"Sibuk"},
        //     {Name:"Salah Nomor Handphone"},
        //     {Name:"Salah Sambung"}
        // ];
        $scope.buttonSms = {
            save: {
                text: 'Kirim',
                icon: "fa fa-fw fa fa-commenting",
                func: function() {
                    $scope.show_modal.sms = false;
                    // console.log('$scope.dataGrid sms', $scope.dataGrid);
                    // console.log('smsText buttonSms', $scope.dataGrid.SMSMessage);
                    // $scope.grid.data = $scope.dataGrid;
                    _.map($scope.dataGrid.Grid, function(val) {
                        val.StatusPSFU = true;
                        // val.SMSSent = true;
                    });
                    FollowupService.updateSMS($scope.dataGrid.Grid).then(function(resu) {
                        $scope.getData();
                        // console.log('resu updateSMS', resu);
                    });
                }
            }
        };
        $scope.changeFormatDate = function(item) {
            var tmpAppointmentDate = item;
            console.log("changeFormatDate item", item);
            tmpAppointmentDate = new Date(tmpAppointmentDate);
            var finalDate
            var yyyy = tmpAppointmentDate.getFullYear().toString();
            var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = tmpAppointmentDate.getDate().toString();
            // finalDate = (dd[1] ? dd : "0" + dd[0]) + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + yyyy;
            finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
            console.log("changeFormatDate finalDate", finalDate);
            return finalDate;
        };
        $scope.cancelSMS = function(row, mode) {
            // console.log('row mode cancel', row, mode);
            // selectedRow = [];
        };
        $scope.sendSMS = function(row) {
            console.log('row mode sendSMS', row);
            // console.log('smsText', $scope.dataGrid.SMSMessage);
            var arr = [];
            var nextDate = new Date();
            nextDate.setHours(24, 0, 0, 0);
            console.log('nextDate', nextDate);
            _.map(row.Grid, function(val) {
                console.log('val row.grid >>', val);
                var obj = {};
                obj.ParamBody = { 'to': val.PhoneNumber, 'message': row.SMSMessage };
                obj.ExecuteTime = nextDate;
                arr.push(obj);
            });
            FollowupService.sendSMS(arr).then(function(resu) {
                $scope.dataGrid.SMSMessage = "";
                console.log('smsText', resu.data);
            });
        };
        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        // $scope.ContactPerson = [];
        $scope.getData = function() {
            // gridData = []
            FollowupService.getDataPSFU().then(function(resu) {
                var newData = resu.data;
                console.log("getDataPSFU", newData);
                $scope.loading = false;
                _.map(newData, function(val) {
                    if (val.StatusFU == null) {
                        val.StatusFUDesc = '-';
                    } else if (val.StatusFU == 1) {
                        val.StatusFUDesc = 'Terhubung';
                    } else if (val.StatusFU == 2) {
                        val.StatusFUDesc = 'Tidak Terhubung';
                    } else if (val.StatusFU == 3) {
                        var a = new Date(val.StatusFUDate);
                        var yearFirst = a.getFullYear();
                        var monthFirst = a.getMonth() + 1;
                        var dayFirst = a.getDate();
                        var newDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                        if (val.StatusFUTime == 1) {
                            val.StatusFUTimeDesc = 'Pagi';
                        } else if (val.StatusFUTime == 2) {
                            val.StatusFUTimeDesc = 'Siang';
                        } else {
                            val.StatusFUTimeDesc = 'Sore';
                        };

                        val.StatusFUDesc = 'Follow Up Kembali' + '/ ' + newDate + '/ ' + val.StatusFUTimeDesc;
                    };
                    if (val.FollowUpCount == 3) {
                        val.isSMS = 'Aktif';
                    } else {
                        val.isSMS = '-';
                    };
                    if (val.FollowUpTime == null) {
                        val.FollowUpTime = "-";
                    }
                    if (val.FollowUpDate == null) {
                        val.FollowUpDate = "-";
                    }
                    // val.nameSA = '-';
                    // FollowupService.GetEmployeeId(val.Job.UserIdWORelease).then(function(resu) {
                    // console.log("==============?", val.Job.UserSa);
                    if (val.Job.UserSa !== null) {
                        val.nameSA = val.Job.UserSa.EmployeeName;
                    } else {
                        val.nameSA = '-';
                    }
                    // val.nameSA = val.Job.UserSa.EmployeeName;
                    // });
                });
                $scope.grid.data = newData;

                // console.log('resu.data', newData);
                FollowupService.getQuestion().then(function(resu2) {
                    var question = resu2.data.Result;
                    var data = [];
                    _.map(question, function(val, k) {
                        var obj = {};
                        obj.Status = true;
                        data.push(obj);
                    });
                    $scope.Negative = angular.copy(data);
                    $scope.disableQuestion = angular.copy(data);
                    $scope.Question = question;
                    $scope.QuestionLength = question.length;
                    console.log('$scope.Negative', $scope.Negative);

                });

                FollowupService.getUncontactableReason().then(function(res) {
                    $scope.reasonData = res.data.Result;
                    console.log('$scope.reasonData', $scope.reasonData);
                });
            });

        };
        $scope.testingQA = function() {
            // console.log('mFService', $scope.mFService);
        };
        $scope.selectTypeFU = function(item) {
            console.log('selectType', item);

        };
        $scope.selectTypeWaktu = function(item) {
            // console.log('selectType', item);

        };
        $scope.selectTypeWaktuFU = function(item) {
            // console.log('selectType', item);

        };
        $scope.selectTypeReason = function(item) {
            // console.log('selectType', item);

        };
        $scope.selectedAnswer = function(irem) {
            // console.log('selectedAnswer', item);
        };
        $scope.mFService.arrQuestion = {
            Reason: {}
        };
        // $scope.checkNegative = function(item, id, index) {
        //     console.log('checkNegative', item, id, index);
        //     console.log(' $scope.disableQuestion', $scope.disableQuestion);
        //     if (index == 0) {
        //         _.map(item, function(s) {
        //             if (s.AnswerId == id) {
        //                 $scope.mFService.arrQuestion.Reason = {};
        //                 // console.log("$scope.mFService.arrQuestion.Reason[a] <=>", $scope.mFService.arrQuestion.Reason);
        //                 for (var a = 0; a <= $scope.QuestionLength - 1; a++) {
        //                     $scope.mFService.arrQuestion.Reason[a] = null;
        //                     // console.log("$scope.mFService.arrQuestion.Reason <=>", $scope.mFService);
        //                 };
        //                 for (var count = 1; count <= $scope.QuestionLength - 1; count++) {
        //                     // console.log("$scope.mFService.arrQuestion.AnswerId[count]=>", $scope.mFService.arrQuestion.AnswerId);
        //                     // console.log("$scope.mFService.arrQuestion.Reason[count]=>", $scope.mFService);
        //                     if (s.IsNegative == true) {
        //                         console.log('count >>', count);
        //                         $scope.Negative[index].Status = true;
        //                         if (count == 1 || count == 2) {
        //                             $scope.Negative[count].Status = true;
        //                             $scope.disableQuestion[count].Status = false;
        //                         }
        //                         // else {
        //                         //     $scope.Negative[count].Status = true;
        //                         //     $scope.disableQuestion[count].Status = true;
        //                         // }
        //                         // original
        //                         // $scope.Negative[count].Status = true;
        //                         // $scope.disableQuestion[count].Status = false;
        //                         $scope.mFService.arrQuestion.AnswerId[count] = null;
        //                         $scope.mFService.arrQuestion.Reason[count] = null;
        //                     } else {
        //                         console.log('count 2 >>', count);
        //                         if (count == 1 || count == 2) {
        //                             $scope.Negative[count].Status = false;
        //                             $scope.disableQuestion[count].Status = true;
        //                         }
        //                         // else {
        //                         //     $scope.disableQuestion[count].Status = true;
        //                         //     $scope.Negative[count].Status = true;
        //                         // };
        //                         // original
        //                         // $scope.disableQuestion[count].Status = true;
        //                         // $scope.Negative[count].Status = true;
        //                     };
        //                 };
        //             };
        //         });
        //     };

        //     _.map(item, function(val) {
        //         if (val.AnswerId == id) {
        //             if (val.IsNegative == true) {
        //                 $scope.Negative[index].Status = false;
        //                 // console.log('nyampe sini -', $scope.Negative[index]);
        //             } else {
        //                 $scope.Negative[index].Status = true;
        //                 // console.log('nyampe sini +', $scope.Negative[index]);
        //             };
        //         };
        //     });
        // };

        $scope.checkNegative = function(item, id, index) {
            console.log('checkNegative', item, id, index);
            console.log(' $scope.disableQuestion', $scope.disableQuestion);
            if (index == 0) {
                _.map(item, function(s) {
                    if (s.AnswerId == id) {
                        $scope.mFService.arrQuestion.Reason = {};
                        // console.log("$scope.mFService.arrQuestion.Reason[a] <=>", $scope.mFService.arrQuestion.Reason);
                        for (var a = 0; a <= $scope.QuestionLength - 1; a++) {
                            $scope.mFService.arrQuestion.Reason[a] = null;
                            // console.log("$scope.mFService.arrQuestion.Reason <=>", $scope.mFService);
                        };
                        for (var count = 1; count <= $scope.QuestionLength - 1; count++) {
                            console.log("$scope.mFService.arrQuestion.AnswerId[count]=>", $scope.mFService.arrQuestion.AnswerId);
                            console.log("$scope.mFService.arrQuestion.Reason[count]=>", $scope.mFService);
                            console.log('count', count);
                            if (s.IsNegative == true) {
                                $scope.Negative[index].Status = false;
                                $scope.Negative[count].Status = true;
                                $scope.disableQuestion[count].Status = false;
                                $scope.mFService.arrQuestion.AnswerId[count] = null;
                                $scope.mFService.arrQuestion.Reason[count] = null;
                            } else {
                                console.log('count 2', count);
                                $scope.disableQuestion[count].Status = true;
                                $scope.Negative[count].Status = true;
                            };
                        };
                    };
                });
            };

            _.map(item, function(val) {
                if (val.AnswerId == id) {
                    if (val.IsNegative == true) {
                        $scope.Negative[index].Status = false;
                        // console.log('nyampe sini -', $scope.Negative[index]);
                    } else {
                        $scope.Negative[index].Status = true;
                        // console.log('nyampe sini +', $scope.Negative[index]);
                    };
                };
            });
        };

        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };

        $scope.onSelectRows = function(rows) {
            var a = rows;
            console.log("onSelectRows=>", rows);
            $scope.available = a.length;
            selectedRow = rows;
            $scope.dataGrid.Grid = angular.copy(selectedRow);
        };

        var isGood = 0;
        $scope.onValidateSave = function(data) {
            console.log('data', data);
            if (data.StatusFU == null) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Status Follow Up Terlebih Dahulu",
                });
            } else {
                // console.log('masuk kemari');
                var arrKosong = [];
                isGood = 0;

                if (parseInt(data.StatusFU) == 2) {
                    data.FollowUpCount++;
                    if (data.FollowUpCount == 3) {
                        data.SMSSent = true;
                    } else {
                        data.SMSSent = false;
                    };
                    isGood = 0;
                } else if (parseInt(data.StatusFU) == 1) {
                    // data.FollowUpCount = 3;
                    data.StatusPSFU = true;
                    console.log("data.arrQuestion", data.arrQuestion);

                    if (data.arrQuestion != null || data.arrQuestion === undefined) {

                        if (data.arrQuestion == undefined || data.arrQuestion.AnswerId == undefined) {
                            isGood = 1;
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Mohon Input Jawaban pada kolom pertanyaan terlebih dahulu",
                            });
                        } else {
                            data.FollowUpCount++;
                            isGood = 0;
                            for (var i in data.arrQuestion.AnswerId) {
                                //Validasi ada kosong gak...
                                if (data.arrQuestion.Reason == undefined) {
                                    // console.log('masuk sini ArrQuestion.Reason');
                                    arrKosong.push({
                                        QuestionId: $scope.Question[i].QuestionId,
                                        AnswerId: data.arrQuestion.AnswerId[i],
                                        // AnswerReason: data.arrQuestion.Reason[i] ? data.arrQuestion.Reason[i] : null,
                                        PSFUId: data.Id
                                    });
                                } else {
                                    arrKosong.push({
                                        QuestionId: $scope.Question[i].QuestionId,
                                        AnswerId: data.arrQuestion.AnswerId[i],
                                        AnswerReason: data.arrQuestion.Reason[i],
                                        PSFUId: data.Id
                                    });
                                }

                            };

                        };
                    };
                } else if (parseInt(data.StatusFU) == 3) {
                    isGood = 0;
                    data.StatusFUDate = new Date();
                    data.FUDate = $scope.changeFormatDate(data.FUDate)
                };

                data.arrQuestionAnswer = arrKosong;
                var count = 0
                var obj = {};
                _.map(arrKosong, function(val, key) {
                    if (key == 0) {
                        obj.QuestionId1 = val.QuestionId;
                        obj.AnswerId1 = parseInt(val.AnswerId);
                        obj.Reason1 = val.AnswerReason ? val.AnswerReason : null;
                    } else if (key == 1) {
                        obj.QuestionId2 = val.QuestionId;
                        obj.AnswerId2 = parseInt(val.AnswerId);
                        obj.Reason2 = val.AnswerReason ? val.AnswerReason : null;
                    } else if (key == 2) {
                        obj.QuestionId3 = val.QuestionId;
                        obj.AnswerId3 = parseInt(val.AnswerId);
                        obj.Reason3 = val.AnswerReason ? val.AnswerReason : null;
                    };
                    obj.VehicleId = data.Job.VehicleId;
                    console.log('obj', obj);
                });
                data.VehicleVIN = $scope.VIN == null ? 0 : $scope.VIN;
                data.PSFUCRM = obj;
                data.IsNonTAM = $scope.IsNonTAM == null ? 0 : $scope.IsNonTAM;
                console.log('data >>>', data);
                // if (obj.Reason1 == undefined || obj.Reason2 == undefined || obj.Reason3 == undefined ){
                //     console.log("alasan belum diisi")
                //     isGood = 1;
                //     bsNotify.show({
                //         size: 'big',
                //         type: 'danger',
                //         title: "Mohon lengkapi reason/alasan terlebih dahulu",
                //     });
                // } else 
                if (isGood == 0) {
                    bsAlert.alert({
                        title: "Data Sudah Tersimpan",
                        text: "",
                        type: "success",
                        showCancelButton: false
                    })
                    return data;
                };
            };
        };
        $scope.dataPSFU = [];
        $scope.mDataVehicleHistory = [];
        $scope.onBeforeEditMode = function(row) {
            // FollowupService.GetVehicVIN(row.Job.VehicleId).then(function(resu) {
            //     console.log('resu.data.Result[0].VIN', resu.data.Result[0].VIN);
            //     $scope.mDataVehicleHistory = [];
            //     $scope.VIN = resu.data.Result[0].VIN;
            //     if(resu.data.Result.length > 0){
            //         FollowupService.GetDataLatest(resu.data.Result[0].VIN).then(function(res){
            //             if(res.data !== null){                            
            //                 $scope.mDataVehicleHistory.push(res.data);
            //                 console.log("WAnjaayyyyy",$scope.mDataVehicleHistory);
            //             }

            //         });
            //     }

            // });
            // $scope.mFService.Reason = null;
            // $scope.mFService.StatusFU = null;
            $scope.JobRequest = row.Job.JobRequest;
            $scope.JobComplaint = row.Job.JobComplaint;
            $scope.mFService.ContactPerson = row.Job.ContactPerson + ' / ' + row.Job.PhoneContactPerson1 + ' / ' + row.Job.PhoneContactPerson2;
            // console.log(' $scope.ContactPerson', $scope.ContactPerson);
            // console.log('onBeforeEdit row, ', row);
            // console.log('onBeforeEdit $scope.mFService, ', $scope.mFService);
            $scope.dataPSFU = row;
            // return row;
        };
        $scope.beforeCancel = function(data) {
            // console.log('beforeCancel', data);
            // $scope.ContactPerson = [];
        }
        $scope.onShowDetail = function(row, mode) {
            // $scope.ContactPerson = [];
            console.log("row1", row);
            $scope.mFService = angular.copy(row);
            $scope.mFService.FollowUpDate = $scope.changeFormatDate(row.FollowUpDate);
            //$scope.mFService.Job.JobDate = $scope.changeFormatDate(row.Job.JobDate);
            $scope.mFService.Job.WoCreatedDate = $scope.changeFormatDate(row.Job.WoCreatedDate);
            $scope.mFService.FollowUpTime = row.StatusFUTime != null ? row.StatusFUTime.toString() : null;
            console.log("mFService.Job.JobDate", $scope.mFService.Job.JobDate);
            console.log("$scope.mFService.FollowUpDate", $scope.mFService.FollowUpDate);

            FollowupService.GetVehicVIN(row.Job.VehicleId).then(function(resu) {
                console.log('resu.data.Result[0].VIN', resu.data.Result[0].VIN);
                $scope.mDataVehicleHistory = [];
                $scope.VIN = resu.data.Result[0].VIN;
                $scope.IsNonTAM = resu.data.Result[0].IsNonTAM;
                // =====
                var objSearch = {};
                objSearch.flag = 1;
                objSearch.filterValue = row.Job.PoliceNumber;
                objSearch.TID = null;
                WOBP.getCustomerVehicleList(objSearch).then(function(rest) {
                    var data = {}
                    if (rest.data.Result.length > 0) {
                        data = rest.data.Result[0];
                        console.log("ini loh punya",data.CustomerList );
                        console.log("ini loh punya",data.CustomerList.CustomerListPersonal.length );
                        if (data.CustomerList.CustomerListPersonal.length == 0 ){
                            $scope.mFService.CustName = data.CustomerList.CustomerListInstitution[0].Name;
                            console.log("ape lu", data.CustomerList.CustomerListInstitution[0].Name );
                        } else if (data.CustomerList.CustomerListPersonal.length != 0){
                            $scope.mFService.CustName =  data.CustomerList.CustomerListPersonal[0].CustomerName;
                            $scope.mFService.Title = data.CustomerList.CustomerListPersonal[0].FrontTitle;
                            $scope.mFService.Degree = data.CustomerList.CustomerListPersonal[0].EndTitle;
                        }
                           
                        // $scope.mFService.Title = data.CustomerList.CustomerListPersonal[0].FrontTitle;
                        $scope.mFService.ToyotaId = data.CustomerList.ToyotaId;
                        // $scope.mFService.Degree = data.CustomerList.CustomerListPersonal[0].EndTitle;
                        $scope.mFService.ContactPerson = row.Job.ContactPerson + ' / ' + row.Job.PhoneContactPerson1 + ' / ' + row.Job.PhoneContactPerson2;
                        $scope.JobRequest = row.Job.JobRequest;
                        $scope.JobComplaint = row.Job.JobComplaint;
                        // $scope.StatusFUTime = row.StatusFUTime;

                    }
                    console.log("dataaaa", data);
                    if (resu.data.Result.length > 0) {
                        FollowupService.GetDataLatest(resu.data.Result[0].VIN).then(function(res) {
                            if (res.data !== null) {
                                // angular.forEach(res.data, function(val) {
                        // di komen 30 agustus 2019, katanya pengen tampilan nya 2 angka belakang koma semua. jadi di kasi pipe aja di html

                                    // if (res.data.VehicleJobService != null && res.data.VehicleJobService != undefined){
                                    //     angular.forEach(res.data.VehicleJobService, function(val2) {
                                    //         if (val2.VehicleServiceHistoryDetail != null && val2.VehicleServiceHistoryDetail != undefined){
                                    //             angular.forEach(val2.VehicleServiceHistoryDetail, function(val3) {
                                    //                 if (val3.Quantity.toString().includes('.')){
                                    //                     val3.Quantity = parseFloat(val3.Quantity).toFixed(2);
                                    //                     val3.Quantity = val3.Quantity.toString();
                                    //                 }
                                    //             })
                                    //         }
                                    //     })
                                    // }
                                    
                                // })
                                $scope.mDataVehicleHistory.push(res.data);
                                console.log("WAnjaayyyyy", $scope.mDataVehicleHistory);
                                $scope.NamaSA = row.nameSA;
                            }

                        });
                    }
                });
            })
            if (mode == 'edit') {

            }
            $scope.resizeLayout();
        };
        $scope.actDetail = function() {
            $scope.showFollowService = true;
        };
        $scope.back = function() {
            $scope.showFollowService = false;
        };
        // $scope.hIn = function(){
        //     var d = new Date($scope.mS.TimeThru);
        //     d.getHours();
        //     d.getMinutes();
        //     $scope.maxFrom = d;
        //     if($scope.mShift.TimeFrom > $scope.mShift.TimeThru){
        //         $scope.mShift.TimeFrom =  $scope.mShift.TimeThru;
        //     }
        // }
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'id', field: 'Id', width: '7%', visible: false },
                { name: 'Tanggal Plan', displayName: "Tgl. Plan", field: 'FollowUpDate', cellFilter: dateFilter,
                // sort: {
                //     direction: uiGridConstants.DESC,
                //     // ignoreSort: true,
                //     priority: 0
                //    }
                },
                { name: 'Waktu', field: 'FollowUpTime' },
                { name: 'Nama SA', displayName: "SA", field: 'nameSA' },
                { name: 'Nama Customer', displayName: "Nama Pelanggan", field: 'Job.ContactPerson' },
                { name: 'Phone Number', displayName: "No. Telepon", field: 'PhoneNumber' },
                { name: 'Nomor Polisi', displayName: "No. Polisi", field: 'Job.PoliceNumber', allowCellFocus: false },
                { name: 'Model Kendaraan', displayName: "Model", field: 'Job.ModelType'},
                { name: 'Nomor WO', displayName: "No. WO", field: 'Job.WoNo', allowCellFocus: false },
                { name: 'Status FollowUp Terakhir', displayName: "Status", field: 'StatusFUDesc', },
                { name: 'SMS Aktif', displayName: "SMS Aktif", field: 'isSMS' },
                //{ name: 'Tanggal Service', displayName: "Tgl. Service", field: 'Job.JobDate', cellFilter: dateFilter },
                { name: 'Tanggal Service', displayName: "Tgl. Service", field: 'Job.WoCreatedDate', cellFilter: dateFilter },
            ]
        };

        // $scope.gridUpdate = {
        //     enableSorting: true,
        //     enableRowSelection: true,
        //     multiSelect: true,
        //     enableSelectAll: true,
        //     //showTreeExpandNoChildren: true,
        //     // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        //     // paginationPageSize: 15,
        //     columnDefs: [
        //         { name:'id',    field:'Id', width:'7%', visible:false },
        //         { name:'Pertanyaan', field:'Waktu' },
        //         { name:'Jawaban Customer', field:'InitialName' },
        //         { name:'Alasan Customer', field:'CustomerName' }
        //     ]
        // };
        AppointmentGrService.getPayment().then(function(res) {
            $scope.paymentData = res.data.Result;
        });
        AppointmentGrService.getUnitMeasurement().then(function(res) {
            $scope.unitData = res.data.Result;
        });
        AppointmentGrService.getTaskCategory().then(function(res) {
            $scope.taskCategory = res.data.Result;
        });
        AppointmentGrService.getWoCategory().then(function(res) {
            $scope.woCategory = res.data.Result;
        });
        var gridTemp = [];
        $scope.listApi = {};
        $scope.lmComplaint = {};
        $scope.lmRequest = {};
        $scope.lmModel = {};
        $scope.ldModel = {};
        $scope.listSelectedRows = [];
        $scope.ComplaintCategory = [
            { ComplaintCatg: 'Body' },
            { ComplaintCatg: 'Body Electrical' },
            { ComplaintCatg: 'Brake' },
            { ComplaintCatg: 'Drive Train' },
            { ComplaintCatg: 'Engine' },
            { ComplaintCatg: 'Heater System & AC' },
            { ComplaintCatg: 'Restraint' },
            { ComplaintCatg: 'Steering' },
            { ComplaintCatg: 'Suspension and Axle' },
            { ComplaintCatg: 'Transmission' },
        ];
        $scope.JobRequest = [];
        $scope.JobComplaint = [];
        $scope.listButtonSettings = { new: { enable: true, icon: "fa fa-fw fa-car" } };
        $scope.listView = {
            new: { enable: false },
            view: { enable: false },
            delete: { enable: false },
            select: { enable: false },
            selectall: { enable: false },
            edit: { enable: false },
        }
        $scope.listCustomButtonSettings = {
            enable: true,
            icon: "fa fa-fw fa-car",
            func: function(row) {
                // console.log("customButton", row);
            }
        };
        $scope.listButtonFalse1 = {
            new: {
                enable: false
            }
        };
        $scope.listButtonFalse2 = {
            new: {
                enable: false
            }
        };
        $scope.gridActionTemplateNew = '<div class="ui-grid=cell=contents">\
        <a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.actView(row.entity)" tabindex="0"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>\
        <a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Ubah" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.actEdit(row.entity)" tabindex="0" ng-hide="row.entity.FollowUpCount == 3"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>\
        </div>';

        // $scope.newButtonSettings = [{
        //     actionType: 'edit', //Use 'Edit Action' of bsForm
        //     title: 'Ubah',
        //     icon: 'fa fa-fw fa-edit',
        //     color: 'rgb(213,51,55)'
        // }, ];
        $scope.mFService.DateAppointment = null;
        $scope.requestAppointment = function(date) {
            var data = $scope.dataPSFU;
            console.log('datedatedate', date);
            console.log('datadatadata', data);
            if (date == null) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Tanggal Request Appointment Terlebih Dahulu",
                });
            } else {
                var a = new Date(date);
                var yearFirst = a.getFullYear();
                var monthFirst = a.getMonth() + 1;
                var dayFirst = a.getDate();
                date = yearFirst + '-' + monthFirst + '-' + dayFirst;
                FollowupService.requestAppointmentBro(data, date).then(function(resu) {
                    console.log('resu', resu.data);
                    if (resu.data.ResponseCode == 13) {

                    }
                    bsAlert.alert({
                        title: "Request Appointment Sudah Tersimpan",
                        text: "",
                        type: "success",
                        showCancelButton: false
                    })
                })
            };
        };

    });