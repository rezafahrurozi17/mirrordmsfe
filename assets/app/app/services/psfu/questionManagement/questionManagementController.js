angular.module('app')
    .controller('questionManagementController', function ($scope, $http, CurrentUser, QuestionManagementService, bsNotify, $timeout) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mQManagement = null; //Model
        $scope.xQManagement = {};
        $scope.xQManagement.selected = [];
        $scope.showFollowService = false;
        var dateFormat = 'dd/MM/yyyy';
        // var dateFilter = 'date:"dd/MM/yyyy"';
        $scope.answerCount = [{}];
        //----------------------------------
        // Get Data
        //----------------------------------
        $scope.getData = function () {
            QuestionManagementService.getData().then(function (resu) {
                $scope.loading = false;
                console.log("resu.data.Result=>", resu.data.Result);
                $scope.grid.data = resu.data.Result;
            });
        };
        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.onSelectRows = function (rows) {
            var a = rows;
            console.log("onSelectRows=>", rows);
            // $scope.available = a.length;
        };
        $scope.dataOnEdit = null;
        $scope.onValidateSave = function (data) {
            console.log("onValidateSave=>", data);
            console.log("onValidateSave AnswerList=>", $scope.AnswerList);
            if ($scope.AnswerList.length == 0) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Jawaban",
                });
            } else {
                if (data.QuestionId == null || data.QuestionId === undefined) {
                    data.QuestionId = 0;
                    // data.QuestionParentId = 0;
                    // data.Status = 0;
                    // data.Type = 0;
                } else {
                    data.QuestionParentId = $scope.dataOnEdit.QuestionParentId;
                    data.Status = $scope.dataOnEdit.Status;
                    data.Type = $scope.dataOnEdit.Type;
                };
                _.map($scope.AnswerList, function (val) {
                    if (val.AnswerId == null || val.AnswerId === undefined) {
                        val.AnswerId = 0;
                    }
                    val.IsNegative = val.IsNegative ? val.IsNegative : false;
                });
                data.Answer = $scope.AnswerList;
                return data;
            };
        };
        $scope.onBeforeNew = function () {
            $scope.mQManagement = [];
            $scope.AnswerList = [];
        };
        // $scope.onBeforeEdit = function(row, mode) {
        //     console.log('onBeforeEdit', row, mode);
        // };
        $scope.onShowDetail = function (row, mode) {
            console.log('onShowDetail', row, mode);
            $scope.mQManagement = [];
            $scope.AnswerList = [];

            $scope.dataOnEdit = row;
            var data = {};
            data.QDescription = row.Description;
            data.QuestionId = row.QuestionId;
            data.Answer = [];
            // if (mode == 'edit') {
            // if (row.Answer.length != 0) {
            //     _.map(row.Answer, function(val) {
            //         val.ADescription = val.Description;
            //         val.AIsNegative = val.IsNegative;
            //     });
            //     data.Answer = row.Answer;
            // } else {
            //     data.Answer = row.Answer;
            // };
            data.Answer = row.Answer;
            // } else if (mode == 'view') {
            //     data.Answer = row.Answer;
            // }
            $scope.mQManagement = data;
            $scope.AnswerList = data.Answer;
            // console.log('onShowDetail data', data);

            data = {};
        };
        $scope.actDetail = function () {
            $scope.showFollowService = true;
        };
        $scope.back = function () {
            $scope.showFollowService = false;
        };
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            columnDefs: [
                { name: 'Question Id', field: 'QuestionId', width: '10%' },
                { name: 'Description', field: 'Description' },
            ]
        };
        //----------------------------------
        // BSList Setup
        //----------------------------------
        $scope.AnswerList = [];
        $scope.lmAnswer = {};
        $scope.listApi = {};
    });