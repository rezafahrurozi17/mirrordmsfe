angular.module('app')
    .factory('QuestionManagementService', function ($http, $q, CurrentUser) {
        var currentUser = CurrentUser.user;
        console.log(currentUser);
        return {
            getData: function () {
                var res = $http.get('/api/as/QuestionsManagement');
                return res;
            },
            create: function (data) {
                console.log('create data >>', data);
                // var promise = [];
                // promise.push(
                //     $http.post('/api/as/newQuestion', [{
                //         QuestionId: data.QuestionId,
                //         QuestionParentId: data.QuestionParentId,
                //         Description: data.QDescription,
                //         Status: data.Status,
                //         Type: data.Type
                //     }]).then(function(resu) {
                //         var QuestionIdBaru = resu.data.ResponseMessage.split('#').pop();
                //         _.map(data.Answer, function(val) {
                //             if (val.QuestionId == null) {
                //                 val.QuestionId = QuestionIdBaru;
                //             }
                //             val.Description = val.ADescription;
                //         });
                //         $http.post('/api/as/newAnswer', data.Answer);
                //     })
                // );
                // return $q.all(promise);

                return $http.post('/api/as/newQuestionAnswer', [{
                    QuestionId: data.QuestionId,
                    QuestionParentId: 0,
                    Description: data.QDescription,
                    Status: 1,
                    Type: 1,
                    Answer: data.Answer
                }]);

            },
            update: function (data) {
                // return $http.put('/api/as/updateQuestion', [{
                //     QuestionId: data.QuestionId,
                //     QuestionParentId: data.QuestionParentId,
                //     Description: data.QDescription,
                //     Status: data.Status,
                //     Type: data.Type
                // }]).then(function(resu) {
                //     _.map(data.Answer, function(val) {
                //         if (val.QuestionId == null) {
                //             val.QuestionId = data.QuestionId;
                //         }
                //         val.Description = val.ADescription;
                //     });
                //     $http.put('/api/as/updateAnswer', data.Answer);

                // });
                _.map(data.Answer, function (val) {
                    if (val.QuestionId == null) {
                        val.QuestionId = data.QuestionId;
                    }
                });
                console.log('update data >>', data);

                return $http.put('/api/as/updateQuestionAnswer/', [{
                    QuestionId: data.QuestionId,
                    QuestionParentId: data.QuestionParentId,
                    Description: data.QDescription,
                    Status: data.Status,
                    Type: data.Type,
                    Answer: data.Answer
                }]);
            },
            delete: function (id) {
                // return $http.delete('/api/as/WorkingShift', { data: id, headers: { 'Content-Type': 'application/json' } });
                return $http.put('/api/as/QuestionManagement/Delete/' + id);
            },
        }
    });