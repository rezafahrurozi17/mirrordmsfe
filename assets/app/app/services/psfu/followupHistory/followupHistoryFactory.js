angular.module('app')
    .factory('FollowupHistoryService', function($http, $q, CurrentUser) {
        var currentUser = CurrentUser.user();
        // console.log(currentUser);
        return {
            getData: function() {
                var res = $http.get('/api/as/WorkingShift');
                return res;
            },
            getDataService: function(firstDate, lastDate) {
                if (firstDate != null || lastDate != null) {
                    var res = $http.get('/api/as/PSFUsList/FO/--/--/Service/' + firstDate + '/' + lastDate);
                    return res
                }
            },
            getDataFollowUp: function(firstDate, lastDate) {
                if (firstDate != null || lastDate != null) {
                    var res = $http.get('/api/as/PSFUsList/FO/' + firstDate + '/' + lastDate + '/Service/--/--');
                    return res
                }
            },
            getDataAll: function(f1, f2, s1, s2) {
                var res = $http.get('/api/as/PSFUsList/FO/' + f1 + '/' + f2 + '/Service/' + s1 + '/' + s2);
                return res
            },
            create: function(data) {
                return $http.post('/api/as/WorkingShift', [{
                    ShiftByte: data.ShiftByte,
                    Name: data.Name,
                    TimeFrom: data.TimeFrom,
                    TimeThru: data.TimeThru
                }]);
            },
            update: function(data) {
                console.log('data update psfu', data);
                if (data.StatusFU == 1) {
                    var promise = $q.resolve(
                        $http.put('/api/as/PutPSFU/', [{
                            Id: data.Id,
                            JobId: data.JobId,
                            StatusFU: data.StatusFU,
                            Description: data.Description,
                            Result: data.Result,
                            Reason: data.Reason,
                            SMSSent: data.SMSSent,
                            StatusPSFU: data.StatusPSFU,
                            StatusFUDate: data.StatusFUDate,
                            StatusFUTime: data.StatusFUTime,
                            FollowUpCount: data.FollowUpCount,
                            FUDate: data.FUDate,

                            QuestionAnswer: data.arrQuestionAnswer
                        }]),
                        /*$http.put('/api/ct/PutVehicleListPSFU', [{
                            VIN: data.VehicleVIN,
                            VehicleId: data.PSFUCRM.VehicleId,
                            QuestionId1: data.PSFUCRM.QuestionId1,
                            AnswerId1: data.PSFUCRM.AnswerId1,
                            QuestionId2: data.PSFUCRM.QuestionId2,
                            AnswerId2: data.PSFUCRM.AnswerId2,
                            QuestionId3: data.PSFUCRM.QuestionId3,
                            AnswerId3: data.PSFUCRM.AnswerId3,
                            Reason1: data.PSFUCRM.Reason1,
                            Reason2: data.PSFUCRM.Reason2,
                            Reason3: data.PSFUCRM.Reason3
                        }]),*/
                        $http.put('/api/crm/PutVehicleListPSFU', [{
                            VIN: data.VehicleVIN,
                            VehicleId: data.PSFUCRM.VehicleId,
                            QuestionId1: data.PSFUCRM.QuestionId1,
                            AnswerId1: data.PSFUCRM.AnswerId1,
                            QuestionId2: data.PSFUCRM.QuestionId2,
                            AnswerId2: data.PSFUCRM.AnswerId2,
                            QuestionId3: data.PSFUCRM.QuestionId3,
                            AnswerId3: data.PSFUCRM.AnswerId3,
                            Reason1: data.PSFUCRM.Reason1,
                            Reason2: data.PSFUCRM.Reason2,
                            Reason3: data.PSFUCRM.Reason3
                        }])
                    );
                    return promise;
                } else {
                    var promise = $q.resolve(
                        $http.put('/api/as/PutPSFU/', [{
                            Id: data.Id,
                            JobId: data.JobId,
                            StatusFU: data.StatusFU,
                            Description: data.Description,
                            Result: data.Result,
                            Reason: data.Reason,
                            SMSSent: data.SMSSent,
                            StatusPSFU: data.StatusPSFU,
                            StatusFUDate: data.StatusFUDate,
                            StatusFUTime: data.StatusFUTime,
                            FollowUpCount: data.FollowUpCount,
                            QuestionAnswer: data.arrQuestionAnswer
                        }])

                    );
                    return promise;
                }

                // $http.put('/api/as/PutPSFU/', [{
                //     Id: data.Id,
                //     JobId: data.JobId,
                //     StatusFU: data.StatusFU,
                //     Reason: data.Reason,
                //     SMSSent: data.SMSSent,
                //     StatusPSFU: data.StatusPSFU,
                //     StatusFUDate: data.StatusFUDate,
                //     StatusFUTime: data.StatusFUTime,
                //     FollowUpCount: data.FollowUpCount,
                //     QuestionAnswer: data.arrQuestionAnswer
                // }]);
            },
            updateSMS: function(data) {
                return $http.put('/api/as/updateSMS/', data);
            },
            delete: function(id) {
                return $http.delete('/api/as/WorkingShift', { data: id, headers: { 'Content-Type': 'application/json' } });
            },
            getDataPSFU: function() {
                return $http.get('/api/as/GetPSFU/');
            },
            getQuestion: function() {
                return $http.get('/api/as/QuestionsManagement');
            },
            savePSFUtoCRM: function(item) {
                return $http.put('/api/ct/PutVehicleListPSFU', [{
                    VehicleId: item.VehicleId,
                    QuestionId1: item.QuestionId1,
                    AnswerId1: item.AnswerId1,
                    QuestionId2: item.QuestionId2,
                    AnswerId2: item.AnswerId2,
                    QuestionId3: item.QuestionId3,
                    AnswerId3: item.AnswerId3
                }]);
            },
            requestAppointmentBro: function(data, appDate) {
                console.log("coba", data);
                return $http.post('/api/as/AppointmentRequests', [{
                    Id: 0,
                    VehicleId: data.Job.VehicleId,
                    DadangKemod: appDate,
                    isGr: data.Job.isGr,
                    // AppointmentSourceId: 4
                    AppointmentSourceId: 3 //ini 3 psfu
                }]);
            },
            getRequestAppointmentDate: function(jobId) {
                return $http.get('/api/as/GetAppointRequestHistory/' + jobId);
            },
            getDataAnswer: function(data) {
                return $http.get('/api/as/GetAnswerPSFU', {
                    params: {
                        Id: data
                    }
                });
            },
            sendSMS: function(data) {
                var promise = [];
                var phoneNumber = {};
                console.log('masuk sendSMS Service', JSON.stringify(data.ParamBody));
                console.log('masuk sendSMS Service currentUser', currentUser);

                function send(data) {
                    $http.post('/api/as/PostUWSQueue/', [{
                        // WSQueueId: 0,
                        OutletId: currentUser.OrgId,
                        WorkerId: currentUser.UserId,
                        Url: '-', //Insert Service SMS
                        Method: '-', //Insert Method
                        AuthUser: null,
                        AuthPassword: null,
                        AuthApiKey: null,
                        ParamBody: JSON.stringify(data.ParamBody),
                        RetryCount: null,
                        Response: null,
                        Status: 1,
                        InsertTime: new Date(),
                        ExecuteTime: data.ExecuteTime,
                        FetchTime: null,
                        FinisihTime: null
                    }]);
                };
                // console.log('masuk sendSMS Service send', send(data));

                _.map(data, function(v) {
                    console.log('masuk sendSMS Service v', v);
                    var a = send(v);
                    promise.push(a);
                });
                console.log('masuk sendSMS Service Promise', promise);

                return $q.all(promise);

            }
        }
    });