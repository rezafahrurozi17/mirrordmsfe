angular.module('app')
    .controller('followupHistoryController', function($scope, $http, CurrentUser, VehicleHistory, FollowupService, FollowupHistoryService, bsAlert, AppointmentGrService, $timeout, bsNotify) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.isPSFU = true;
        // console.log('$scope.user', $scope.user)
        $scope.mFHistory = {}; //Model
        $scope.xFHistory = [];
        $scope.mData = {};
        $scope.mData.RequestAppointmentDate = null;
        $scope.xFHistory.selected = [];
        $scope.showFollowService = false;
        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var selectedRow = [];
        // $scope.smsText = null;
        $scope.Negative = [];
        $scope.filter = {};
        $scope.modalMode = 'new';
        $scope.dataGrid = [];
        $scope.DisablePermintaan = { new: { enable: false } };
        $scope.DisableKeluhan = { new: { enable: false } };
        $scope.show_modal = {
            sms: false
        };
        $scope.typeFilter = [{
                Id: 1,
                Name: "Tanggal Follow Up"
            },
            {
                Id: 2,
                Name: "Tanggal Service"
            }
        ];
        $scope.filter.typeCategoryFollowUp = 0;
        $scope.filter.typeCategoryService = 0;
        $scope.dService = true;
        $scope.dFollowup = true;
        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        // $scope.ContactPerson = [];
        $scope.resizeLayout = function(){
            $timeout(function() {
                var staticHeight =
                    $("#page-footer").outerHeight() +
                    $(".nav.nav-tabs").outerHeight() +
                    $(".ui.breadcrumb").outerHeight() + 85;
                $("#layoutContainer_FollowupHistoryServiceForm").height($(window).height() - 5);
            },500);
        }
        $scope.getData = function() {
            if ($scope.filter.typeCategoryFollowUp == 1 && $scope.filter.typeCategoryService == 0 && $scope.filter.firstDateFollowUp != null && $scope.filter.lastDateFollowUp != null) {
                var a = new Date($scope.filter.firstDateFollowUp);
                var yearFirst = a.getFullYear();
                var monthFirst = a.getMonth() + 1;
                var dayFirst = a.getDate();
                var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                var b = new Date($scope.filter.lastDateFollowUp);
                var yearLast = b.getFullYear();
                var monthLast = b.getMonth() + 1;
                var dayLast = b.getDate();
                var lastDate = yearLast + '-' + monthLast + '-' + dayLast;
                $scope.loading = true;
                FollowupHistoryService.getDataFollowUp(firstDate, lastDate).then(
                    function(res) {
                        var gridData = [];
                        gridData = res.data.Result;
                        $scope.grid.data = gridData;
                        $scope.loading = false
                        console.log("getDataFollowUp ==========>", res.data.Result);
                    },
                    function(err) {

                    }
                );

            } else if ($scope.filter.typeCategoryService == 1 && $scope.filter.typeCategoryFollowUp == 0 && $scope.filter.firstDateService != null && $scope.filter.lastDateService != null) {
                var a = new Date($scope.filter.firstDateService);
                var yearFirst = a.getFullYear();
                var monthFirst = a.getMonth() + 1;
                var dayFirst = a.getDate();
                var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;

                var b = new Date($scope.filter.lastDateService);
                var yearLast = b.getFullYear();
                var monthLast = b.getMonth() + 1;
                var dayLast = b.getDate();
                var lastDate = yearLast + '-' + monthLast + '-' + dayLast;
                $scope.loading = true;
                FollowupHistoryService.getDataService(firstDate, lastDate).then(
                    function(res) {
                        $scope.loading = false;
                        var gridData = [];
                        gridData = res.data.Result;
                        $scope.grid.data = gridData;
                        console.log("getDataService ==========>", res.data.Result);
                    },
                    function(err) {

                    }
                );

            } else if ($scope.filter.typeCategoryService == 1 && $scope.filter.typeCategoryFollowUp == 1 && $scope.filter.firstDateService != null && $scope.filter.lastDateService != null && $scope.filter.firstDateFollowUp != null && $scope.filter.lastDateFollowUp != null) {
                var a = new Date($scope.filter.firstDateFollowUp);
                var yearFirstFollow = a.getFullYear();
                var monthFirstFollow = a.getMonth() + 1;
                var dayFirstFollow = a.getDate();
                var firstDateFollow = yearFirstFollow + '-' + monthFirstFollow + '-' + dayFirstFollow;
                // ===================================
                var b = new Date($scope.filter.lastDateFollowUp);
                var yearLastFollow = b.getFullYear();
                var monthLastFollow = b.getMonth() + 1;
                var dayLastFollow = b.getDate();
                var lastDateFollow = yearLastFollow + '-' + monthLastFollow + '-' + dayLastFollow;
                // ======================================
                var c = new Date($scope.filter.firstDateService);
                var yearFirstService = c.getFullYear();
                var monthFirstService = c.getMonth() + 1;
                var dayFirstService = c.getDate();
                var firstDateService = yearFirstService + '-' + monthFirstService + '-' + dayFirstService;
                //========================================
                var d = new Date($scope.filter.lastDateService);
                var yearLastService = d.getFullYear();
                var monthLastService = d.getMonth() + 1;
                var dayLastService = d.getDate();
                var lastDateService = yearLastService + '-' + monthLastService + '-' + dayLastService;

                $scope.loading = true;
                FollowupHistoryService.getDataAll(firstDateFollow, lastDateFollow, firstDateService, lastDateService).then(
                    function(res) {
                        $scope.loading = false;
                        var gridData = [];
                        gridData = res.data.Result;
                        $scope.grid.data = gridData;
                        console.log("getDataService ==========>", res.data.Result);
                    },
                    function(err) {

                    }
                );
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Filter",
                    // content: error.join('<br>'),
                    // number: error.length
                });
                $scope.grid.data = [];
            }
            $scope.loading = false;
        };
        // $scope.getData = function() {
        //     if($scope.filter.typeCategory == 1 && $scope.filter.firstDate != null && $scope.filter.lastDate != null){
        //         var a = new Date($scope.filter.firstDate);
        //         var yearFirst = a.getFullYear();
        //         var monthFirst = a.getMonth() + 1;
        //         var dayFirst = a.getDate();
        //         var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
        //         var b = new Date($scope.filter.lastDate);
        //         var yearLast = b.getFullYear();
        //         var monthLast = b.getMonth() + 1;
        //         var dayLast = b.getDate();
        //         var lastDate = yearLast + '-' + monthLast + '-' + dayLast;
        //         $scope.loading =true;
        //         FollowupHistoryService.getDataFollowUp(firstDate,lastDate).then(
        //             function(res){
        //                 var gridData = [];
        //                 gridData = res.data.Result;
        //                 $scope.grid.data = gridData;
        //                 $scope.loading = false
        //                 console.log("getDataFollowUp ==========>",res.data.Result);
        //             },function(err){

        //             }
        //         );

        //     }else if($scope.filter.typeCategory == 2 && $scope.filter.firstDate != null && $scope.filter.lastDate != null){
        //         var a = new Date($scope.filter.firstDate);
        //         var yearFirst = a.getFullYear();
        //         var monthFirst = a.getMonth() + 1;
        //         var dayFirst = a.getDate();
        //         var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;

        //         var b = new Date($scope.filter.lastDate);
        //         var yearLast = b.getFullYear();
        //         var monthLast = b.getMonth() + 1;
        //         var dayLast = b.getDate();
        //         var lastDate = yearLast + '-' + monthLast + '-' + dayLast;
        //         $scope.loading = true;
        //         FollowupHistoryService.getDataService(firstDate,lastDate).then(
        //             function(res){
        //                 $scope.loading = false;
        //                 var gridData = [];
        //                 gridData = res.data.Result;
        //                 $scope.grid.data = gridData;
        //                 console.log("getDataService ==========>",res.data.Result);
        //             },function(err){

        //             }
        //         );

        //     }else{
        //         bsNotify.show({
        //             size: 'big',
        //             type: 'danger',
        //             title: "Mohon Input Filter",
        //             // content: error.join('<br>'),
        //             // number: error.length
        //         });
        //         $scope.grid.data = [];
        //     }
        //     $scope.loading = false;
        // };
        $scope.getDataQuestion = function() {
            FollowupService.getQuestion().then(function(resu2) {
                var question = resu2.data.Result;
                var data = [];
                _.map(question, function(val, k) {
                    var obj = {};
                    obj.Status = true;
                    data.push(obj);
                    // if (val.Answer.length == 0) {
                    //     delete val;
                    // } else {

                    // }
                });
                $scope.Negative = angular.copy(data);
                // $scope.mFService.arrQuestion.IsNegative = $scope.Negative;

                $scope.Question = question;
                console.log('resu2', $scope.Question);

            })
        }
        $scope.getDataAnswer = function(tmpRow) {
            console.log('$scope.Negative', tmpRow.Id);
            //$scope.SebenernyaId = $scope.mFHistory.id.ToString();
            FollowupHistoryService.getDataAnswer(tmpRow.Id).then(function(res) {
                $scope.Jawaban = res.data.Result;
                $scope.mFHistory.Jawaban = $scope.Jawaban;

                console.log("scope.mFHistory.Jawaban", $scope.mFHistory.Jawaban);

            });
            // item.Jawaban = $scope.IsiNya.Jawaban;
            // item.Jawaban = $scope.IsiNya.AlasanJawaban;
            // console.log("MASUK2",item);
        }
        $scope.selectCategory = function(selected) {
            console.log("selected nyaaaaa", selected);
            // if (selected == 1) {
            //     $scope.filter.firstDate = null;
            //     $scope.filter.lastDate = null;
            //     $scope.grid.data = [];
            // }else{
            //     $scope.filter.firstDate = null;
            //     $scope.filter.lastDate = null;
            //     $scope.grid.data = [];
            // }
        };
        $scope.selectCategoryFollow = function(selected) {
            if (selected.typeCategoryFollowUp == 1) {
                $scope.dFollowup = false;
            } else {
                $scope.dFollowup = true;
                $scope.filter.firstDateFollowUp = null;
                $scope.filter.lastDateFollowUp = null;
            }
        }
        $scope.selectCategoryService = function(selected) {
            if (selected.typeCategoryService == 1) {
                $scope.dService = false;
            } else {
                $scope.dService = true;
                $scope.filter.firstDateService = null;
                $scope.filter.lastDateService = null;
            }
        }
        $scope.testingQA = function() {
            // console.log('mFHistory', $scope.mFHistory);
        };
        $scope.selectTypeFU = function(item) {
            // console.log('selectType', item);

        };
        $scope.selectTypeWaktu = function(item) {
            // console.log('selectType', item);

        };
        $scope.selectTypeWaktuFU = function(item) {
            // console.log('selectType', item);

        };
        $scope.selectTypeReason = function(item) {
            // console.log('selectType', item);

        };
        $scope.selectedAnswer = function(item) {
            // console.log('selectedAnswer', item);
        };
        $scope.checkNegative = function(item, id, index) {
            console.log('checkNegative', item, id, index);
            _.map(item, function(val) {
                if (val.AnswerId == id) {
                    if (val.IsNegative == true) {
                        $scope.Negative[index].Status = false;
                        // console.log('nyampe sini -', $scope.Negative[index]);
                    } else {
                        $scope.Negative[index].Status = true;
                        // console.log('nyampe sini +', $scope.Negative[index]);
                    }
                }
            });
            // console.log('cek $scope.Negative[index]', $scope.Negative);

            // if (item == false) {
            //     $scope.Negative = false;
            // } else {
            //     $scope.Negative = true;
            // }
        };
        // $scope.Negative = function(index) {
        //     console.log('Negative', index);

        //     return false;
        // };
        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };
        $scope.onSelectRows = function(rows) {
            var a = rows;
            console.log("onSelectRows=>", rows);
            $scope.available = a.length;
            selectedRow = rows;
            $scope.dataGrid.Grid = angular.copy(selectedRow);
        };

        var isGood = 0;
        $scope.onValidateSave = function(data) {
            console.log('data', data);
            if (data.StatusFU == null) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Status Follow Up Terlebih Dahulu",
                });
            } else {
                // console.log('masuk kemari');
                var arrKosong = [];
                isGood = 0;

                if (parseInt(data.StatusFU) == 2) {
                    data.FollowUpCount++;
                    if (data.FollowUpCount == 3) {
                        data.SMSSent = true;
                    } else {
                        data.SMSSent = false;
                    };
                    isGood = 0;

                } else if (parseInt(data.StatusFU) == 1) {
                    // data.FollowUpCount = 3;
                    data.StatusPSFU = true;

                    if (data.arrQuestion != null || data.arrQuestion === undefined) {

                        if (data.arrQuestion.AnswerId == undefined) {
                            isGood = 1;
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Mohon Input Jawaban pada kolom pertanyaan terlebih dahulu",
                            });
                        } else {
                            data.FollowUpCount++;
                            isGood = 0;
                            for (var i in data.arrQuestion.AnswerId) {
                                //Validasi ada kosong gak...
                                if (data.arrQuestion.Reason == undefined) {
                                    console.log('masuk sini ArrQuestion.Reason');
                                    arrKosong.push({
                                        QuestionId: $scope.Question[i].QuestionId,
                                        AnswerId: data.arrQuestion.AnswerId[i],
                                        // AnswerReason: data.arrQuestion.Reason[i] ? data.arrQuestion.Reason[i] : null,
                                        PSFUId: data.Id
                                    });
                                } else {
                                    arrKosong.push({
                                        QuestionId: $scope.Question[i].QuestionId,
                                        AnswerId: data.arrQuestion.AnswerId[i],
                                        AnswerReason: data.arrQuestion.Reason[i],
                                        PSFUId: data.Id
                                    });
                                }

                            };

                        };
                    };
                } else if (parseInt(data.StatusFU) == 3) {
                    isGood = 0;
                    console.log('masuk date', $scope.mData.RequestAppointmentDate);
                    data.StatusFUDate = $scope.mFHistory.StatusFUDate;

                };

                data.arrQuestionAnswer = arrKosong;
                var count = 0
                var obj = {};
                _.map(arrKosong, function(val, key) {
                    if (key == 0) {
                        obj.QuestionId1 = val.QuestionId;
                        obj.AnswerId1 = parseInt(val.AnswerId);
                        obj.Reason1 = val.AnswerReason ? val.AnswerReason : null;
                    } else if (key == 1) {
                        obj.QuestionId2 = val.QuestionId;
                        obj.AnswerId2 = parseInt(val.AnswerId);
                        obj.Reason2 = val.AnswerReason ? val.AnswerReason : null;
                    } else if (key == 2) {
                        obj.QuestionId3 = val.QuestionId;
                        obj.AnswerId3 = parseInt(val.AnswerId);
                        obj.Reason3 = val.AnswerReason ? val.AnswerReason : null;
                    };
                    obj.VehicleId = data.Job.VehicleId;
                    console.log('obj', obj);
                });
                data.VehicleVIN = $scope.VIN == null ? 0 : $scope.VIN;
                data.PSFUCRM = obj;
                console.log('data >>>', data);
                // console.log('isGood ??', isGood);
                if (isGood == 0) {
                    return data;
                };
            };
        };
        $scope.dataPSFU = [];
        $scope.onBeforeEditMode = function(row) {
            $scope.dataPSFU = row;
            console.log('masuk onBeforeEditMode');
            // $scope.mFHistory.Reason = null;
            // $scope.mFHistory.StatusFU = null;
            // $scope.JobRequest = row.Job.JobRequest;
            // $scope.JobComplaint = row.Job.JobComplaint;
            // $scope.mFHistory.ContactPerson = row.Job.ContactPerson + ' / ' + row.Job.PhoneContactPerson1 + ' / ' + row.Job.PhoneContactPerson2;
            // // console.log(' $scope.ContactPerson', $scope.ContactPerson);
            // // console.log('onBeforeEdit row, ', row);
            // // console.log('onBeforeEdit $scope.mFHistory, ', $scope.mFHistory);
            // // return row;
        };
        $scope.beforeCancel = function(data) {
            // console.log('beforeCancel', data);
            // $scope.ContactPerson = [];
        }
        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
            //disableWeekend: 1
        };

        $scope.changeFormatDate = function(item) {
            var tmpAppointmentDate = item;
            console.log("changeFormatDate item", item);
            tmpAppointmentDate = new Date(tmpAppointmentDate);
            var finalDate
            var yyyy = tmpAppointmentDate.getFullYear().toString();
            var mm = (tmpAppointmentDate.getMonth() + 1).toString(); // getMonth() is zero-based
            var dd = tmpAppointmentDate.getDate().toString();
            finalDate = (dd[1] ? dd : "0" + dd[0]) + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + yyyy;
            console.log("changeFormatDate finalDate", finalDate);
            return finalDate;
        };

        $scope.onShowDetail = function(row, mode) {
            // $scope.ContactPerson = [];
            $scope.mDataVehicleHistory = [];
            $scope.MRS = {};
            console.log("row", row);
            var tmpRow = angular.copy(row);
            tmpRow.FollowUpTime = tmpRow.StatusFUTime;
            // tmpRow.FollowUpTime = '3';
            console.log("tmpRow", tmpRow);


            $scope.mData = {};
            if (mode === 'edit') {
                $scope.getDataQuestion();
            } else {
                $scope.getDataAnswer(tmpRow);
            }

            console.log('$scope.Question', $scope.Question);
            console.log("tmpRow======", tmpRow.Job.AppointmentDate);
            if (tmpRow.Job.VehicleId !== null) {
                AppointmentGrService.getDataVehicle(tmpRow.Job.VehicleId).then(function(res) {
                    var tmpDetail = res.data.Result;
                    $scope.mDataDetail = tmpDetail[0];
                    $scope.mDataCrm = {
                        Vehicle:{
                            VIN:tmpDetail[0].VIN
                        }
                    }
                });
            }
            FollowupHistoryService.getRequestAppointmentDate(tmpRow.Job.JobId).then(function(res) {
                if (res.data !== null) {
                    $scope.mData.RequestAppointmentDate = res.data.RequestAppointmentDate;
                }
            });

            $scope.mFHistory = tmpRow;
            // $scope.mFHistory.FollowUpDate = $scope.mFHistory.FollowUpDate != null ? $scope.changeFormatDate($scope.mFHistory.FollowUpDate) : $scope.mFHistory.FollowUpDate;
            // $scope.mFHistory.Job.JobDate = $scope.mFHistory.Job.JobDate != null ? $scope.changeFormatDate($scope.mFHistory.Job.JobDate) : $scope.mFHistory.Job.JobDate;
            // $scope.mFHistory.FollowUpTime = $scope.mFHistory.StatusFUTime != null ? $scope.mFHistory.StatusFUTime.toString() : null;
            $scope.mFHistory.mode = mode;
            // console.log("scope.mFHistory.Jawaban", $scope.mFHistory.Jawaban);
            AppointmentGrService.getWobyJobId(row.JobId).then(function(res) {
                var tmpRes = res.data.Result[0];
                $scope.JobRequest = tmpRes.JobRequest;
                $scope.JobComplaint = tmpRes.JobComplaint;
                $scope.mFHistory.ContactPerson = tmpRes.ContactPerson + ' / ' + tmpRes.PhoneContactPerson1 + ' / ' + tmpRes.PhoneContactPerson2;
                console.log("$scope.mFHistory.ContactPerson ", $scope.mFHistory.ContactPerson);
                console.log("$scope.mFHistory", $scope.mFHistory);
                console.log('sarapan', $scope.mData.RequestAppointmentDate);
                $scope.resizeLayout();
                $timeout(function(){
                    // $('#antiDisable')
                    $('#antiDisable').find('*').removeAttr('disabled');
                },1000)
            });
        };
        $scope.actDetail = function() {
            $scope.showFollowService = true;
        };
        $scope.back = function() {
            $scope.showFollowService = false;
        };
        $scope.change = function(startDate, endDate, category, vin) {

            console.log("start Date 1", startDate);
            console.log("start Date 2", endDate);
            category = category.toString();
            var copyAngular = angular.copy(category);
            console.log("category", category);
            console.log("Copycategory", copyAngular);
            console.log("vin", vin);
            var param = 2; // tombol cari
            if (category == "" || category == null || category == undefined) {
                bsNotify.show({
                    size: 'big',
                    title: 'Mohon Isi Kategori Pekerjaan',
                    text: 'I will close in 2 seconds.',
                    timeout: 2000,
                    type: 'danger'
                });
            } else {

                if (startDate == undefined || startDate == "" || startDate == null || startDate == "Invalid Date") {
                    startDate = "-"
                } else {

                    var tmpDate = startDate;
                    var tmpMonth = startDate;
                    console.log("tmpMonth", tmpMonth);
                    var tmpTime = $scope.startDate;
                    // if (tmpDate.getMonth() < 10) {
                    // tmpMonth = "0" + (tmpDate.getMonth() + 1);
                    // } else {
                    tmpMonth = (tmpDate.getMonth() + 1).toString();
                    // }
                    var ini = tmpDate.getFullYear() + "-" + (tmpMonth[1] ? tmpMonth : "0" + tmpMonth[0]) + "-" + tmpDate.getDate();
                    var d = new Date(ini);
                    console.log('data yang diinginkan', ini);
                    startDate = ini;
                };


                if (endDate == undefined || endDate == "" || endDate == null || endDate == "Invalid Date") {
                    endDate = "-"
                } else {

                    var tmpDate2 = endDate;
                    var tmpMonth2 = endDate;
                    console.log("tmpMonth2", tmpMonth2);
                    var tmpTime2 = endDate;

                    // if (tmpDate2.getMonth() < 10) {
                    //     tmpMonth2 = "0" + (tmpDate2.getMonth() + 1);
                    // } else {
                    tmpMonth2 = (tmpDate2.getMonth() + 1).toString();
                    // }
                    var ini2 = tmpDate2.getFullYear() + "-" + (tmpMonth2[1] ? tmpMonth2 : "0" + tmpMonth2[0]) + "-" + tmpDate2.getDate();
                    var d2 = new Date(ini2);
                    console.log('data yang diinginkan 2', ini2);
                    endDate = ini2;
                };

                if (typeof vin !== "undefined") {
                    $scope.ServiceHistory(category, vin, startDate, endDate);
                } else {
                    bsNotify.show({
                        size: 'big',
                        title: 'VIN Tidak Ditemukan',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                };
            };
        };

        $scope.ServiceHistory = function(category, vin, startDate, endDate, param) {
            VehicleHistory.getData(category, vin, startDate, endDate).then(
                function(res) {
                    if (res.data.length == 0) {
                        $scope.mDataVehicleHistory = [];
                        bsNotify.show({
                            size: 'big',
                            title: 'Data Riwayat Service Tidak Ditemukan',
                            text: 'I will close in 2 seconds.',
                            timeout: 2000,
                            type: 'danger'
                        });
                    } else {
                        res.data.sort(function(a, b) {
                            // Turn your strings into dates, and then subtract them
                            // to get a value that is either negative, positive, or zero.
                            return new Date(b.ServiceDate) - new Date(a.ServiceDate);
                        });
                        for (var i in res.data) {
                            for (var j in res.data[i].VehicleJobService) {
                                for (var z in res.data[i].VehicleJobService[j].VehicleServiceHistoryDetail) {
                                    res.data[i].VehicleJobService[j].VehicleServiceHistoryDetail[z].Quantity = parseFloat(res.data[i].VehicleJobService[j].VehicleServiceHistoryDetail[z].Quantity).toFixed(1);
                                }

                            }
                        }
                        $scope.mDataVehicleHistory = res.data;
                        console.log("kesini ga?");
                        $scope.value = angular.copy($scope.mDataVehicleHistory);

                        $scope.MRS.category = category;

                        if (param == 1) { // tab riwayat servis

                            var dateEnd = $scope.mDataVehicleHistory[0].ServiceDate;
                            $scope.MRS.endDate = dateEnd;

                            var terakhir = $scope.mDataVehicleHistory.length;
                            $scope.MRS.startDate = $scope.mDataVehicleHistory[terakhir - 1].ServiceDate;

                        }
                        console.log('category', $scope.MRS.category);
                        console.log('startDate', $scope.MRS.startDate);
                        console.log('EndDate', $scope.MRS.endDate);
                    };

                    console.log("mDataVehicleHistory=> Followup ", $scope.mDataVehicleHistory);
                },
                function(err) {}
            );
        }
        // $scope.hIn = function(){
        //     var d = new Date($scope.mS.TimeThru);
        //     d.getHours();
        //     d.getMinutes();
        //     $scope.maxFrom = d;
        //     if($scope.mShift.TimeFrom > $scope.mShift.TimeThru){
        //         $scope.mShift.TimeFrom =  $scope.mShift.TimeThru;
        //     }
        // }
        //----------------------------------
        // Grid Setup
        //----------------------------------


        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'id', field: 'Id', width: '7%', visible: false },
                { name: 'Tanggal Follow Up', field: 'FollowUpDate', cellFilter: dateFilter },
                { name: 'SA', field: 'Job.UserSa.EmployeeName' },
                { name: 'Nomor Polisi', field: 'Job.PoliceNumber' },
                { name: 'Nama Customer', field: 'Job.ContactPerson' },
                { name: 'Nomor Handphone', field: 'PhoneNumber' },
                { name: 'Tanggal Service', field: 'Job.WoCreatedDate', cellFilter: dateFilter },
                { name: 'Nomor WO', field: 'Job.WoNo', },
                { name: 'Status FU', width: '7%', field: 'StatusFU', cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>' },
            ]
        };

        AppointmentGrService.getPayment().then(function(res) {
            $scope.paymentData = res.data.Result;
        });
        AppointmentGrService.getUnitMeasurement().then(function(res) {
            $scope.unitData = res.data.Result;
        });
        AppointmentGrService.getTaskCategory().then(function(res) {
            $scope.taskCategory = res.data.Result;
        });
        AppointmentGrService.getWoCategory().then(function(res) {
            $scope.woCategory = res.data.Result;
        });
        var gridTemp = [];
        $scope.listApi = {};
        $scope.lmComplaint = {};
        $scope.lmRequest = {};
        $scope.lmModel = {};
        $scope.ldModel = {};
        $scope.listSelectedRows = [];
        $scope.ComplaintCategory = [
            { ComplaintCatg: 'Body' },
            { ComplaintCatg: 'Body Electrical' },
            { ComplaintCatg: 'Brake' },
            { ComplaintCatg: 'Drive Train' },
            { ComplaintCatg: 'Engine' },
            { ComplaintCatg: 'Heater System & AC' },
            { ComplaintCatg: 'Restraint' },
            { ComplaintCatg: 'Steering' },
            { ComplaintCatg: 'Suspension and Axle' },
            { ComplaintCatg: 'Transmission' },
        ];
        $scope.StastusFollow = [
            { StatusFoll: 1, nameDesc: 'Tehubung' },
            { StatusFoll: 2, nameDesc: 'Tidak terhubung' },
            { StatusFoll: 3, nameDesc: 'Follow up ulang' },
        ]
        $scope.StastusFollowTime = [
            { Id: 1, nameDesc: 'Pagi' },
            { Id: 2, nameDesc: 'Siang' },
            { Id: 3, nameDesc: 'Sore' },
        ]
        $scope.JobRequest = [];
        $scope.JobComplaint = [];
        $scope.listButtonSettings = { new: { enable: true, icon: "fa fa-fw fa-car" } };
        $scope.listCustomButtonSettings = {
            enable: true,
            icon: "fa fa-fw fa-car",
            func: function(row) {
                // console.log("customButton", row);
            }
        };
        $scope.listButtonFalse1 = {
            new: {
                enable: false
            }
        };
        $scope.listButtonFalse2 = {
            new: {
                enable: false
            }
        };
        $scope.gridActionTemplateNew = '<div class="ui-grid=cell=contents">\
        <a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Lihat" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.actView(row.entity)" tabindex="0"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>\
        </div>';

        // <a href="" style="color:#777;" class="trlink ng-scope" uib-tooltip="Ubah" tooltip-placement="bottom" onclick="this.blur()" ng-click="grid.appScope.actEdit(row.entity)" tabindex="0" ng-hide="row.entity.StatusFU == 1"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>\
        // ini button edit di hilangkan karena kata tiket nya ga bs edit di menu ini

        // $scope.newButtonSettings = [{
        //     actionType: 'edit', //Use 'Edit Action' of bsForm
        //     title: 'Ubah',
        //     icon: 'fa fa-fw fa-edit',
        //     color: 'rgb(213,51,55)'
        // }, ];
        $scope.mFHistory.DateAppointment = null;
        $scope.requestAppointment = function(date) {
            var data = $scope.dataPSFU;
            console.log('datedatedate', date);
            console.log('datadatadata', data);
            if (date == null) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Tanggal Request Appointment Terlebih Dahulu",
                });
            } else {
                var a = new Date(date);
                var yearFirst = a.getFullYear();
                var monthFirst = a.getMonth() + 1;
                var dayFirst = a.getDate();
                date = yearFirst + '-' + monthFirst + '-' + dayFirst;
                console.log('====>', data, date);
                FollowupService.requestAppointmentBro(data, date).then(function(resu) {
                    console.log('resu', resu.data);
                    if (resu.data.ResponseCode == 13) {
                        console.log('masuk ya', data, date);
                    }
                    bsAlert.alert({
                        title: "Request Appointment Sudah Tersimpan",
                        text: "",
                        type: "success",
                        showCancelButton: false
                    })
                })
            };
        };


    });