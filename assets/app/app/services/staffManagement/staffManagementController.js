angular.module('app')
    .controller('staffManagementController', function($scope, $http, CurrentUser, WorkingShift, StaffManagement,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
        $scope.getDataShift();
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mStaff = null; //Mode;
    $scope.xStaff = {};
    $scope.xStaff.selected=[];
    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    var det = new Date("October 13, 2014 11:13:00");
    // var res=[{
    //       StaffId:1,
    //       FullName:"John Doe",
    //       CodeName:"John",
    //       DoB:det,
    //       Nip:"10000",
    //       ShiftByte: 1
    //     },
    //     {
    //       StaffId:2,
    //       FullName:"Robert Doe",
    //       CodeName:"Robert",
    //       DoB:det,
    //       Nip:"10000",
    //       ShiftByte: 4
    //     }];
    // var shiftB=[
    //     {id:1,shiftByte:1,name:"Pagi"},
    //     {id:2,shiftByte:2,name:"Siang"},
    //     {id:3,shiftByte:4,name:"Sore"},
    //     {id:4,shiftByte:8,name:"Malam"},
    //     {id:5,shiftByte:16,name:"Subuh"}
    //     ];

    //$scope.shiftB = shiftB;
    var kumpulan = []
    $scope.check = function(cek){
        console.log("test",cek);
        var a = kumpulan.indexOf(cek);
        if (a > -1){
            kumpulan.splice(a, 1);
            console.log("nih di if 1", kumpulan);
        }else {
            kumpulan.push(cek);
            console.log("nih else", kumpulan);
        }
        var sum = kumpulan.reduce(add, 0);

        $scope.mStaff.ShiftByte = sum;
        console.log("$scope.mStaff.ShiftByte==>",$scope.mStaff.ShiftByte);
        console.log("mStaff.sift",$scope.mStaff.sift);
    }
    function add(a, b) {
        return a + b;
    }
    $scope.onValidateSave = function(data){
        console.log("data",data);
        return true
        // if(data.ShiftByte == null || data.ShiftByte == undefined){
            // $scope.mStaff.ShiftByte = sum;
            // console.log("$scope.mStaff.ShiftByte",$scope.mStaff.ShiftByte);
            // return true
        // }

    }
    $scope.getData = function() {
        gridData = []
        // $scope.grid.data = res;
        StaffManagement.getData()
        .then(
            function(res){
                gridData = [];
                //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
                $scope.grid.data = res.data.Result;
                //console.log("role=>",res);
                //console.log("grid data=>",$scope.grid.data);
                //$scope.roleData = res.data;
                $scope.loading=false;
            },
            function(err){
                // console.log("err=>",err);
            }
        );
    }
    $scope.getDataShift = function(){
        WorkingShift.getData()
        .then(
            function(res){
                //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
                // $scope.grid.data = res.data.Result;
                console.log("Shift",res);
                //$scope.roleData = res.data;
                $scope.shiftB = res.data.Result;
                $scope.loading=false;
                $scope.shiftSum($scope.shiftB);
                $scope.hitung($scope.shiftB);
            },
            function(err){
                // console.log("err=>",err);
            }
        );
    }
    $scope.mStaf = [];
    $scope.hitung = function(data){
        var obj = {};
        for(var i=0; i<data.length; i++){
            obj[data[i].Name] = false;  
        }
        $scope.mStaf=obj;
        console.log("Obj nya",obj);
        console.log("data Hitung",data);
        console.log("$scope.mStaf.sift",$scope.mStaf);
    }
    var sum
    var bitSum = [];
    $scope.shiftSum = function(data){
       var bit = data;
       if(bit.length !== 0){
           for(var i=0; i<bit.length; i++){
                bitSum.push(bit[i].ShiftByte);
           }
            sum = bitSum.reduce(add, 0);
            console.log("bitSum",sum);
            console.log("bitsum[i]",bitSum);
        }
    }
    $scope.onBeforeEdit = function(data,mode){
        // console.log("data",data);
        // console.log("onBeforeEdit",sum);
        // console.log("$scope.mStaff.sift harusnya ini",$scope.mStaf);
        // console.log("$scope.mStaff.sift harusnya ini",$scope.mStaff);
        // var a = [];
        var t = $scope.shiftB;
        // // for(var i=0;i<t.length;i++){
        // //     a.push
        // // }
        // console.log("$scope.mStaff.sift",$scope.mStaff);
        for(var i=0;i<t.length;i++){
            var bit
            console.log("data.ShiftByte",data.ShiftByte);
            console.log("bitSum[i]",bitSum[i]);
            console.log("data.ShiftByte & bitSum[i]",data.ShiftByte & bitSum[i]);
            if(data.ShiftByte & bitSum[i]){
                bit = true;
            }else{
                bit=false;
            }
            // console.log("bit",data.ShiftByte & bitSum[i] );
            // console.log("t[i].Name",t[i].Name);
            $scope.mStaf[t[i].Name] = bit;
        }
        // console.log("$scope.mStaff.sift harusnya ini",$scope.mStaf);
        // console.log("$scope.mStaff.sift harusnya ini",bitSum);
        
        // if(data.ShiftByte !== null){
        //     var ini
        //     for(var k in bitSum){
        //        $scope.mStaff.sift['pagi'] = bitSum[k] & data.ShiftByte;
        //        // console.log("ininniin",ini 
        //     }
        //     // var ini = data.ShiftByte & sum;

        // }
    }
    $scope.DateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };
    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    $scope.DateChange = function(dt){
        console.log("DateChange=>",dt);
    }
    $scope.onBeforeNewMode = function(){
        $scope.getDataShift();

        console.log("berhasil");
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'People Id', field:'People.PeopleId', width:'7%', visible:false},
            { name:'Staff Id',    field:'EmployeeId'},
            { name:'Full Name', field:'People.Name' },
            { name:'Code Name', field:'Initial' },
            { name:'Date of Birth', field:'People.DateofBirth', cellFilter: dateFilter },
            { name:'Shift', field:'ShiftByte' },
        ]
    };
});
