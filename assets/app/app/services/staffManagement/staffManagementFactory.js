angular.module('app')
  .factory('StaffManagement', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    console.log(currentUser);
    return {
      getData: function() {
        var res=$http.get('/api/as/Employees');
        // var res=[{
        //   StaffId:1,
        //   FullName:"John Doe",
        //   CodeName:"John",
        //   DoB:"17/06/1998",
        //   NIP:"10000",
        //   ShiftByte: 7
        // },
        // {
        //   StaffId:2,
        //   FullName:"Robert Doe",
        //   CodeName:"Robert",
        //   DoB:"17/06/1998",
        //   NIP:"10000",
        //   ShiftByte: 7
        // }];
        // console.log('res=>',res);
        //res.data.Result = null;
        return res;
      },
      create: function(data) {
        return $http.post('/api/as/Employees', [{
                                            Employee:data.EmployeeId,
                                            Initial:data.Initial,
                                            ShiftByte:data.ShiftByte,
                                            People : {
                                              Name: data.Name,
                                              DateofBirth: data.DateofBirth
                                            }
                                            }]);
      },
      update: function(role){
        return $http.put('/api/fw/Role', [{
                                            Id: role.Id,
                                            //pid: role.pid,
                                            Name: role.Name,
                                            Description: role.Description}]);
      },
      delete: function(id) {
        return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
      },
    }
  });
