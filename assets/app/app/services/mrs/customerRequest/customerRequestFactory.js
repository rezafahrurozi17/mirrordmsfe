angular.module('app')
  .factory('CustomerRequest', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    // console.log(currentUser);
    return {
      getData: function() {
        var res = $http.get('api/as/MRS/CustomerRequest');
        
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      create: function(data) {
        console.log(' tambah data=>', data);

        return $http.post('api/as/MRS/CustomerRequest', [data]);
      },
      update: function(data) {
        console.log('rubah data=>', data);
        return $http.put('api/as/MRS/CustomerRequest', [data]);
      },
      delete: function(id) {
        console.log("delete id==>", id);
        return $http.delete('/api/as/Stall', {
          data: id,
          headers: {
            'Content-Type': 'application/json'
          }
        });
      }
    }
  });
