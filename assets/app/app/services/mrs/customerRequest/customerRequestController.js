angular.module('app')
  .controller('customerRequestController', function($scope, $http, CurrentUser, bsTransTree, CustomerRequest, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
      //$scope.loading = true;
    });

    $scope.onBeforeEdit = function() {
      $scope.formMode = 'edit';
      $scope.reminderType = $scope.mData.MonthDuration > 0 ? "month" : "km";
    }

    $scope.onBeforeNewMode = function() {
        $scope.formMode = 'new';
        $scope.reminderType = '';


      }
      //----------------------------------
      // Initialization
      //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mData = {}; //Model
    $scope.xRole = {
      selected: []
    };

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.gridDataTree = [];
    $scope.getData = function() {
      $scope.grid.data = [{
        'Owner': 'Harris',
        'Model': 'Jazz',
        'CustRequestId': 1,
        'MonthDuration': 5,
        'Kilometers': 10567
      }, {
        'Owner': 'Bukan Harris',
        'Model': 'Innova',
        'CustRequestId': 2,
        'MonthDuration': 0,
        'Kilometers': 12643
      }];
      $scope.loading = false;
      // CustomerRequest.getData().then(function(res) {
      //     console.log("gridata=>", res);
      //     $scope.grid.data = res.data.Result;
      //     $scope.loading = false;
      //   },
      //   function(err) {
      //     console.log("err=>", err);
      //   }
      // );
    }

    $scope.onSelectRows = function(rows) {
      console.log("onSelectRows=>", rows);
    }

    //----------------------------------
    // Grid Setup
    //----------------------------------

    $scope.grid = {
      enableSorting: true,
      enableRowSelection: true,
      multiSelect: true,
      enableSelectAll: true,
      //showTreeExpandNoChildren: true,
      // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
      // paginationPageSize: 15,
      columnDefs: [{
          name: 'Customer Request Id',
          field: 'CustRequestId',
          width: '7%',
          visible: false
        },
        {
          name: 'Pemilik',
          field: 'Owner'
        },
        {
          name: 'Model',
          field: 'Model'
        },
        {
          name: 'Month Duration',
          field: 'MonthDuration'
        },
        {
          name: 'Kilometers',
          field: 'Kilometers'
        }
      ]
    };

  });
