angular.module('app')
    .factory('MrsList', function($http, $q, CurrentUser) {
        var currentUser = CurrentUser.user();
        // console.log(currentUser);
        return {
            getSMSGateway: function() {
                return $http.get('/api/as/GlobalMaster?CategoryId=2032');
            },
            getUncontactableReason: function() {
                // return $http.get('/api/as/GlobalMaster?CategoryId=1003');
                return $http.get('/api/as/MRS/Invitation/2231');
                
            },
            getContactableReason: function() {
                // return $http.get('/api/as/GlobalMaster?CategoryId=1004');
                return $http.get('/api/as/MRS/Invitation/2230');
            },
            getReminderMedia: function() {
                return $http.get('/api/as/GlobalMaster?CategoryId=1013');
            },
            getMRSInvitationStatus: function() {
                return $http.get('/api/as/GlobalMaster?CategoryId=2031');
            },
            sendSMS: function(data) {
                var promise = [];
                var phoneNumber = {};
                console.log('masuk sendSMS Service', JSON.stringify(data.ParamBody));
                console.log('masuk sendSMS Service currentUser', currentUser);

                function send(data) {
                    $http.post('/api/as/PostUWSQueue/', [{
                        // WSQueueId: 0,
                        OutletId: currentUser.OrgId,
                        WorkerId: currentUser.UserId,
                        Url: data.Url, //Insert Service SMS
                        Method: '-', //Insert Method
                        AuthUser: null,
                        AuthPassword: null,
                        AuthApiKey: null,
                        ParamBody: JSON.stringify(data.ParamBody),
                        RetryCount: null,
                        Response: null,
                        Status: 1,
                        InsertTime: new Date(),
                        ExecuteTime: data.ExecuteTime,
                        FetchTime: null,
                        FinisihTime: null
                    }]);
                };
                // console.log('masuk sendSMS Service send', send(data));

                _.map(data, function(v) {
                    console.log('masuk sendSMS Service v', v);
                    var a = send(v);
                    promise.push(a);
                });
                console.log('masuk sendSMS Service Promise', promise);

                return $q.all(promise);
            },
            sendEmail: function(data) {
                return $http.post('/api/as/PostUmailQueue/', [{
                    OutletId: currentUser.OrgId,
                    WorkerId: null,
                    Sender: data.Sender,
                    SendTo: data.SendTo,
                    SendCc: data.SendCc,
                    SendBcc: data.SendBcc,
                    Subject: data.Subject,
                    AsHtml: 1,
                    Message: data.Message,
                    Attachment: data.AttachmentURL,
                    RetryCount: 0,
                    Status: 1,
                    InsertTime: new Date(),
                    ExecuteTime: null,
                    FetchTime: null,
                    FinisihTime: null


                    // SendTo: data.SendTo,
                    // SendCc: data.SendCc,
                    // SendBcc: data.SendBcc,
                    // Subject: data.Subject,
                    // AsHtml: data.AsHtml,
                    // Message: data.Message,
                    // Attachment: data.Attachment                    
                }])
            },
            getData: function(arr) {
                var res = $http.get('/api/as/MRS/' + arr);
                // api/as/MRS/8/-/-/-/-/-/-/-

                //console.log('res=>',res);
                //res.data.Result = null;
                return res;
            },
            getDataVehicle: function(key) {
                var res = $http.get('/api/crm/GetDataInfo/' + key);
                return res;
            },
            getDetail: function(param) {
                console.log("param factory", param);
                var res = $http.get('/api/as/MRS/' + param.MrsId + '/' + param.VehicleId);
                // api/as/MRS/{MrsId}/
                return res;
            },
            getReminderTypes: function() {
                var res = $http.get('/api/as/MRS/ReminderTypes');
                return res;
            },
            getMrsInvitation: function() {
                var res = $http.get('/api/as/MRS/Invitation/0');
                return res;
            },
            getInvitationMedia: function() {
                var res = $http.get('/api/as/MRS/InvitationMedia/');
                return res;
            },
            getMrsStatus: function() {
                var res = $http.get('/api/as/MRS/InvitationStatus/');
                return res;
            },
            getReminderStatus: function() {
                var res = $http.get('/api/as/MRS/Invitation/2230');
                return res;
            },
            getReasonStatus: function() {
                var res = $http.get('/api/as/MRS/Invitation/2231');
                return res;
            },
            getDataModel: function() {
                // var res=$http.get('/api/param/VehicleModels?start=1&limit=1000');
                var res = $http.get('/api/as/VehicleModel');
                return res;
            },
            getDataTypeByModel: function(arr) {
                console.log(arr);
                // var res=$http.get('/api/param/VehicleModels?start=1&limit=1000');
                var res = $http.get('/api/as/VehicleTypeByModel/' + arr);
                return res;
            },
            getVehicleTypeById: function(id) {
                var url = '/api/crm/GetCVehicleTypeById/' + id
                var res = $http.get(url);
                return res;
            },
            getDataProvince: function() {
                var res = $http.get('/api/crm/GetProvince/');
                return res;
            },
            getCCustomerCategory: function() {
                var res = $http.get('/api/crm/GetCCustomerCategory/?withParent=' + 0); //0 kalau mau ga pake parent, 1 kalau mau pake parent
                return res;
            },
            getDataProvinceById: function(o) {
                var res = $http.get('/api/crm/ProvinceById/' + o);
                return res;
            },
            getDataCity: function() {
                var res = $http.get('/api/crm/GetRegency/');
                return res;
            },
            getDataCityById: function(o) {
                var res = $http.get('/api/crm/GetRegencyById/' + o);
                return res;
            },
            getDataDistrict: function() {
                var res = $http.get('/api/crm/GetDistrict/');
                return res;
            },
            getDataDistrictById: function(o) {
                var res = $http.get('/api/crm/GetDistrictById/' + o);
                return res;
            },
            getDataVillage: function() {
                var res = $http.get('/api/crm/GetVillage/');
                return res;
            },
            getDataVillageById: function(o) {
                var res = $http.get('/api/crm/GetVillageById/' + o);
                return res;
            },
            getDataGender: function() {
                var res = $http.get('/api/as/Gender/');
                return res;
            },
            getDataCusRole: function() {
                var res = $http.get('/api/crm/GetCCustomerCategory/');
                return res;
            },
            getDataCusCategory: function() {
                var res = $http.get('/api/as/CustomerCategory/');
                return res;
            },
            getDataCusType: function() {
                var res = $http.get('/api/as/CustomerType/');
                return res;
            },
            getDataPostalCode: function() {
                var res = $http.get('/api/as/PostalCode/');
                return res;
            },

            // create: function(role) {
            //   return $http.post('/api/fw/Role', [{
            //                                       AppId: 1,
            //                                       ParentId: 0,
            //                                       Name: role.Name,
            //                                       Description: role.Description}]);
            // },
            // update: function(role){
            //   return $http.put('/api/fw/Role', [{
            //                                       Id: role.Id,
            //                                       //pid: role.pid,
            //                                       Name: role.Name,
            //                                       Description: role.Description}]);
            // },
            // delete: function(id) {
            //   return $http.delete('/api/fw/Role',{data:id,headers: {'Content-Type': 'application/json'}});
            // },
            update: function(param) {
                // /api/as/MRS/{mrsId}/{mediaId}/{reminderStatusId} note : tambah date & alasan
                var xparam = '';
                if (param.MrsId != '' && param.MrsId != undefined) {
                    xparam = xparam + 'mrsId=' + param.MrsId;
                };
                if (param.InvMediaId != '' && param.InvMediaId != undefined) {
                    xparam = xparam + '&mediaId=' + param.InvMediaId;
                };
                if (param.ReminderStatusId != '' && param.ReminderStatusId != undefined) {
                    xparam = xparam + '&reminderStatusId=' + param.ReminderStatusId;
                };
                return $http.post('/api/as/MRS?' + xparam);
            },
            getLastServiceHistory: function(vin) {
                return $http.get('/api/as/MRS/getLastServiceHistory/' + vin);
            },
            updateByCall: function(param) {
                var data = param;
                if (data.MrsId == null) {
                    data.MrsId = 0;
                }
                return $http.put('/api/as/MRS/updateByCall', [data]);
            },
            getVehicleMList: function() {
                return $http.get('/api/crm/GetCVehicleModel/');
            },
            getVehicleTypeColor: function() {
                return $http.get('/api/sales/VehicleTypeColorJoin');
            },
            getVehicleTList: function() {
                return $http.get('/api/crm/GetCVehicleType/');
            },
            getCVehicleTypeById: function(param) {
                return $http.get('/api/crm/GetCVehicleTypeById/' + param);
            },
            getMRSListReminder: function(param) {
                return $http.put('/api/as/GetMRSListReminder', [param]);
            },
            recalculate: function(id, vin) {
                return $http.get('/api/as/MRS/recalculate/' + id + '/' + vin);
            },
            getVehicleRecalculate: function(vin, lastKMDate, lastServiceDate) {
                return $http.put('/api/ct/GetVehicleServiceRecalculate/' + vin, [{
                    LastKM: lastKMDate,
                    LastServiceDate: lastServiceDate
                }]);
            },
            sendNotif: function(data, recepient, param) {
                // console.log("model", IdSA);
                // Model.Message = 'Ploting Ulang Chip, ' + Model.Message;
                return $http.post('/api/as/SendNotificationForRole', [{
                    Message: data.Message,
                    RecepientId: recepient,
                    Param: param
                }]);
            }

        }


    });