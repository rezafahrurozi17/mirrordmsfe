angular.module('app')
    .controller('mrsListController', function($scope, $http, CurrentUser, Role, MrsList, bsNotify, bsAlert, $window, $timeout, WO, MasterTemplateMessageFactory, AppointmentGrService, FollowupService, WOBP, VehicleHistory) {
        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            $scope.getSMSGateway();
            $scope.tab = {};
            $scope.tab.activeJustified = 2;
        });
        $scope.dateOptions = {
            startingDay: 1,
            format: 'dd/MM/yyyy',
            // disableWeekend: 1
        };
        var currentDate = new Date();
        // currentDate.setDate(currentDate.getDate() + 1);
        currentDate.setDate(currentDate.getDate()); // edit bisa hari ini Pak Dodi
        $scope.minDate = new Date(currentDate);
        $scope.DateOptionsHistory = {
            startingDay: 1,
            format: 'dd/MM/yyyy',
        };
        $scope.minDateNow = new Date();

        var resizeLayout = function() {
            console.log("Make sure this function called");
            $timeout(function() {
                var staticHeight =
                    $("#page-footer").outerHeight() +
                    $(".nav.nav-tabs").outerHeight() +
                    $(".ui.breadcrumb").outerHeight() + 85;
                var advsearch = $(".advsearch");
                var advsearchHeight = $(".advsearch").outerHeight();
                if (!advsearchHeight) advsearchHeight = 0;
                $("#layoutContainer_MrsListForm").height($(window).height() - staticHeight - advsearchHeight - 5);
            }, 0);
        };
        angular.element($window).bind('resize', resizeLayout);
        $scope.$on('$destroy', function() {
            angular.element($window).unbind('resize', resizeLayout);
        });
        var staticHeight = $('#layoutContainer_MrsListForm .well').height();
        $scope.onBeforeCancel = function(hiya) {
                console.log('hiyaaaaaa', staticHeight)
                for (var i in $scope.filter.reminderType) {
                    if ($scope.filter.reminderType[i].Name == 'Other' && $scope.filter.reminderType[i].checked == true) {
                        resizeLayout()
                        $timeout(function() {
                            $("#layoutContainer_MrsListForm").height(staticHeight);
                        }, 1000);
                    }
                }
            }
            //----------------------------------
            // Initialization
            //----------------------------------
        $scope.formApi = {};
        $scope.dissReCalculate = true;
        $scope.dataDetail = {};
        $scope.advanced = false;
        $scope.detailData = {};
        $scope.show_modal = {
            sms: false,
            email: false
        };
        $scope.getSMSGateway = function() {
            MrsList.getSMSGateway().then(function(resu) {
                console.log('resu smsgateway', resu.data.Result);
                var smsGateway = resu.data.Result[0].Name.split("|");
                console.log('smsGateway smsgateway', smsGateway);
                $scope.arrSmsGateway = smsGateway;
            });
        };
        $scope.modalMode = 'new';
        $scope.customBulk = [{
            title: 'SMS',
            func: function() {
                // selectedRow
                $scope.show_modal.sms = !$scope.show_modal.sms;
                // console.log('data sms', selectedRow);
                $scope.gridSms.data = selectedRow;
                console.log('data sms', $scope.gridSms.data);
                //$scope.dataGrid.SMSMessage = "Kami dari WorkshopTAM_ mengundang Bapak/ibu NamaCustomer_ untuk melakukan Service Berkala JenisSB_ atas kendaraan Kendaraan_ warna Warna_ nomor Polisi Nopol_ pada tanggal TanggalService_ ";
                MasterTemplateMessageFactory.getData(1).then(function(res) {
                    //console.log('data sms 123'.res.data.Result[0].MessageType)
                    $scope.TemplateMessageMain_SMS = res.data.Result[0].MessageDesc;
                    $scope.TemplateMessageMain_SMSMessageType = res.data.Result[0].MessageType;
                    $scope.TemplateMessageMain_SMSMessageId = res.data.Result[0].MessageId;

                    $scope.dataGrid.SMSMessage = $scope.TemplateMessageMain_SMS;
                });
            }
        }, {
            title: 'Email',
            func: function() {
                $scope.show_modal.email = !$scope.show_modal.email;
                console.log('data email', selectedRow);
                $scope.gridEmail.data = selectedRow;
                //$scope.dataGrid.EmailMessage = "Kami dari WorkshopTAM_ mengundang Bapak/ibu NamaCustomer_ untuk melakukan Service Berkala JenisSB_ atas kendaraan Kendaraan_ warna Warna_ nomor Polisi Nopol_ pada tanggal TanggalService_ ";
                MasterTemplateMessageFactory.getData(0).then(function(res) {
                    //console.log('data sms 123'.res.data.Result[0].MessageType)
                    $scope.TemplateMessageMain_Email = res.data.Result[0].MessageDesc;
                    $scope.TemplateMessageMain_EmailMessageType = res.data.Result[0].MessageType;
                    $scope.TemplateMessageMain_EmailMessageId = res.data.Result[0].MessageId;

                    $scope.dataGrid.EmailMessage = $scope.TemplateMessageMain_Email;

                });
            }
        }];
        $scope.gridSms = {
            columnDefs: [{
                name: 'No Polisi',
                field: 'Nopol'
            }, {
                name: 'Nama Pelanggan',
                field: 'Name'
            }, {
                name: 'Handphone',
                field: 'Phone'
            }, {
                name: 'Estimasi Jatuh Tempo',
                // field: 'estDate'
                field: 'DueDate',
                cellFilter: 'date:\'dd-MM-yyyy\''
            }]
        };
        $scope.gridEmail = {
            columnDefs: [{
                name: 'No Polisi',
                field: 'Nopol'
            }, {
                name: 'Nama Pelanggan',
                field: 'Name'
            }, {
                name: 'Email',
                field: 'Email'
            }, {
                name: 'Estimasi Jatuh Tempo',
                field: 'DueDate',
                cellFilter: 'date:\'dd-MM-yyyy\''
            }]
        };

        $scope.buttonSms = {
            save: {
                text: 'Kirim',
                icon: "fa fa-fw fa fa-commenting",
                func: function() {
                    _.forEach($scope.dataGrid.Grid, function(e) {
                        e.smsCount--;
                    });
                    console.log('$scope.dataGrid sms', $scope.dataGrid);
                }
            }
        };

        $scope.buttonEmail = {
            save: {
                text: 'Kirim',
                icon: 'fa fa-fw fa fa-envelope',
                func: function() {
                    _.forEach($scope.dataGrid.Grid, function(e) {
                        e.emailCount--;
                    });
                    console.log('$scope.dataGrid email', $scope.dataGrid);
                    // $scope.grid.data = $scope.dataGrid;
                }
            }
        };

        $scope.cancelSMS = function(row, mode) {
            // console.log('row mode cancel', row, mode);
            selectedRow = [];
            $scope.gridData.SMSMessage = '';
        };

        $scope.sendSMS = function(row) {
            console.log('row mode sendSMS', row);
            // console.log('smsText', $scope.dataGrid.SMSMessage);
            var nextDate = new Date();
            nextDate.setHours(24, 0, 0, 0);
            console.log('nextDate', nextDate);

            function LoopSMS(data, x){
                var arr = [];

                if (x < row.Grid.length){
                    var val = row.Grid[x];

                    var param = {};
                    var user = CurrentUser.user();

                    param.MrsId = val.MrsId != null ? val.MrsId : "-";
                    param.VehicleId = val.MrsId != null ? "-" : val.VehicleId;

                    MrsList.getDetail(param).then(function(res) {
                        var detil = res.data[0];
                        console.log('row.detil >>', detil);
                        if (val.DueDate != null) {
                            var year = val.DueDate.getFullYear();
                            var month = val.DueDate.getMonth() + 1;
                            var day = val.DueDate.getDate();
                            var date = day + '-' + month + '-' + year;
                        } else {
                            date = '';
                        };

                        var newChange = {
                            '#Outlet#': user.OrgName,
                            '#Nm_Customer#': detil.CustomerName,
                            '#NoHPCustomer#': val.Phone,
                            '#JenisSB#': val.ReminderType,
                            '#ModelKendaraan#': detil.VehicleModelName,
                            '#WarnaKendaraan#': detil.ColorName,
                            '#NomorPolisi#': detil.LicensePlate,
                            '#TanggalJatuhTempo#': date
                        };

                        var CopySMSTemplate = angular.copy($scope.TemplateMessageMain_SMS)
                        var SMSMessage = CopySMSTemplate.replace(/#Outlet#|#Nm_Customer#|#NoHPCustomer#|#JenisSB#|#ModelKendaraan#|#WarnaKendaraan#|#NomorPolisi#|#TanggalJatuhTempo#/gi, function(matched) {
                            return newChange[matched];
                        });

                        console.log('row.SMSMessage >>', SMSMessage);
                        var arrUrl = [];
                        _.map($scope.arrSmsGateway, function(o, k) {
                            if (k === 0) {
                                arrUrl[k] = o + val.Phone;
                            } else if (k === 1) {
                                arrUrl[k] = o + encodeURIComponent(SMSMessage);
                            }
                        });
                        console.log('$scope.arrSmsGateway >>', arrUrl);
                        var obj = {};
                        obj.ParamBody = { 'to': val.Phone, 'message': SMSMessage };
                        obj.ExecuteTime = nextDate;
                        obj.Phone = val.Phone;
                        obj.Message = SMSMessage;
                        obj.Url = arrUrl[0] + arrUrl[1];
                        arr.push(obj);

                        console.log('arr', arr);
                        MrsList.sendSMS(arr).then(function(resu) {
                            selectedRow = [];
                            $scope.dataGrid.SMSMessage = "";
                            console.log('smsText', resu.data);
                            $scope.show_modal.sms = false;
                            // bsNotify.show({
                            //     title: 'Kirim SMS Berhasil',
                            //     text: 'I will close in 2 seconds.',
                            //     size: 'big',
                            //     timeout: 2000,
                            //     type: 'success'
                            // });
                            x++
                            LoopSMS(row.Grid,x)

                        });
                    });

                } else {
                    bsNotify.show({
                        title: 'Kirim SMS Berhasil',
                        text: 'I will close in 2 seconds.',
                        size: 'big',
                        timeout: 2000,
                        type: 'success'
                    });
                }
            }

            LoopSMS(row.Grid,0)

            
            // _.map(row.Grid, function(val, key) {
            //     // console.log('val row.grid >>', val);
            //     var param = {};
            //     var user = CurrentUser.user();
            //     // console.log("user" ,user);
            //     param.MrsId = val.MrsId != null ? val.MrsId : "-";
            //     param.VehicleId = val.MrsId != null ? "-" : val.VehicleId;
            //     MrsList.getDetail(param).then(function(res) {
            //         var detil = res.data[0];
            //         console.log('row.detil >>', detil);
            //         // if (e.DueDate != null) {
            //         //     var year = e.DueDate.getFullYear();
            //         //     var month = e.DueDate.getMonth() + 1;
            //         //     var day = e.DueDate.getDate();
            //         //     var date = day + '-' + month + '-' + year;
            //         // } else {
            //         //     date = '';
            //         // };
            //         if (val.DueDate != null) {
            //             var year = val.DueDate.getFullYear();
            //             var month = val.DueDate.getMonth() + 1;
            //             var day = val.DueDate.getDate();
            //             var date = day + '-' + month + '-' + year;
            //         } else {
            //             date = '';
            //         };
            //         // var change = {
            //         //     WorkshopTAM_: user.OrgName,
            //         //     NamaCustomer_: detil.CustomerName,
            //         //     JenisSB_: val.ReminderType,
            //         //     Kendaraan_: detil.VehicleModelName,
            //         //     Warna_: detil.ColorName,
            //         //     Nopol_: detil.LicensePlate,
            //         //     TanggalService_: date
            //         // };

            //         // var SMSMessage = row.SMSMessage.replace(/WorkshopTAM_|NamaCustomer_|JenisSB_|Kendaraan_|Warna_|Nopol_|TanggalService_/gi, function(matched) {
            //         //     return change[matched];
            //         // });

            //         var newChange = {
            //             '#Outlet#': user.OrgName,
            //             '#Nm_Customer#': detil.CustomerName,
            //             // '#NoHPCustomer#': e.Phone,
            //             // '#JenisSB#': e.ReminderType,
            //             '#NoHPCustomer#': val.Phone,
            //             '#JenisSB#': val.ReminderType,
            //             '#ModelKendaraan#': detil.VehicleModelName,
            //             '#WarnaKendaraan#': detil.ColorName,
            //             '#NomorPolisi#': detil.LicensePlate,
            //             '#TanggalJatuhTempo#': date
            //         };

            //         var SMSMessage = row.SMSMessage.replace(/#Outlet#|#Nm_Customer#|#NoHPCustomer#|#JenisSB#|#ModelKendaraan#|#WarnaKendaraan#|#NomorPolisi#|#TanggalJatuhTempo#/gi, function(matched) {
            //             return newChange[matched];
            //         });

            //         console.log('row.SMSMessage >>', SMSMessage);
            //         var arrUrl = [];
            //         _.map($scope.arrSmsGateway, function(o, k) {
            //             if (k === 0) {
            //                 arrUrl[k] = o + val.Phone;
            //             } else if (k === 1) {
            //                 arrUrl[k] = o + encodeURIComponent(SMSMessage);
            //             }
            //         });
            //         console.log('$scope.arrSmsGateway >>', arrUrl);
            //         var obj = {};
            //         obj.ParamBody = { 'to': val.Phone, 'message': SMSMessage };
            //         obj.ExecuteTime = nextDate;
            //         obj.Phone = val.Phone;
            //         obj.Message = SMSMessage;
            //         obj.Url = arrUrl[0] + arrUrl[1];
            //         // arrhp.push(val.Phone);
            //         arr.push(obj);

            //         console.log('arr', arr);
            //         MrsList.sendSMS(arr).then(function(resu) {
            //             selectedRow = [];
            //             $scope.dataGrid.SMSMessage = "";
            //             console.log('smsText', resu.data);
            //             // $scope.show_modal.sms = !$scope.show_modal.sms;
            //             $scope.show_modal.sms = false;
            //             bsNotify.show({
            //                 title: 'Kirim SMS Berhasil',
            //                 text: 'I will close in 2 seconds.',
            //                 size: 'big',
            //                 timeout: 2000,
            //                 type: 'success'
            //             });
            //         });
            //     });
            // });
            $scope.show_modal.sms = false;

        };

        $scope.cancelEmail = function(row, mode) {
            // console.log('row mode cancel', row, mode);
            selectedRow = [];
            $scope.gridData.EmailMessage = '';

        };

        $scope.sendEmail = function(row) {
            console.log("sendEmail row>> ", row);
            var arr = [];
            _.map(row.Grid, function(e) {
                console.log("sendEmail >> ", e);

                var param = {};
                var user = CurrentUser.user();
                // console.log("user" ,user);
                param.MrsId = e.MrsId != null ? e.MrsId : "-";
                param.VehicleId = e.MrsId != null ? "-" : e.VehicleId;
                MrsList.getDetail(param).then(function(res) {
                    if (res.data[0].Email == null || res.data[0].Email == "") {
                        console.log("kosong");
                    } else {
                        var detil = res.data[0];
                        console.log("detil = ", detil);
                        if (e.DueDate != null) {
                            var year = e.DueDate.getFullYear();
                            var month = e.DueDate.getMonth() + 1;
                            var day = e.DueDate.getDate();
                            var date = day + '-' + month + '-' + year;
                        } else {
                            date = '';
                        };
                        // var change = {
                        //     WorkshopTAM_: user.OrgName,
                        //     NamaCustomer_: detil.CustomerName,
                        //     JenisSB_: e.ReminderType,
                        //     Kendaraan_: detil.VehicleModelName,
                        //     Warna_: detil.ColorName,
                        //     Nopol_: detil.LicensePlate,
                        //     TanggalService_: date
                        // };

                        var newChange = {
                            '#Outlet#': user.OrgName,
                            '#Nm_Customer#': detil.CustomerName,
                            '#NoHPCustomer#': e.Phone,
                            '#JenisSB#': e.ReminderType,
                            '#ModelKendaraan#': detil.VehicleModelName,
                            '#WarnaKendaraan#': detil.ColorName,
                            '#NomorPolisi#': detil.LicensePlate,
                            '#TanggalJatuhTempo#': date
                        };

                        console.log('cek change', newChange);

                        var EmailMessage = row.EmailMessage.replace(/#Outlet#|#Nm_Customer#|#NoHPCustomer#|#JenisSB#|#ModelKendaraan#|#WarnaKendaraan#|#NomorPolisi#|#TanggalJatuhTempo#/gi, function(matched) {
                            return newChange[matched];
                        });

                        var obj = {};
                        obj.SendTo = detil.Email;
                        obj.SendCc = null;
                        obj.SendBcc = null;
                        obj.Subject = 'MRS Reminder';
                        obj.AsHtml = 1;
                        obj.Message = EmailMessage;
                        obj.Attachment = null;
                        arr.push(obj);

                        MrsList.sendEmail(obj).then(function(res) {
                            selectedRow = [];
                            $scope.dataGrid.EmailMessage = "";
                            // $scope.show_modal.email = !$scope.show_modal.email;
                            $scope.show_modal.email = false;
                            bsNotify.show({
                                title: 'Kirim Email Berhasil',
                                text: 'I will close in 2 seconds.',
                                size: 'big',
                                timeout: 2000,
                                type: 'success'
                            });
                        });
                    }
                });
            });
            // if (arr.length > 0) {
            //     _.map(arr, function(e) {
            //         MrsList.sendEmail(e).then(function(res) {
            //             $scope.dataGrid.EmailMessage = "";
            //         });
            //     });
            // }
            // console.log("arr = ", arr);
        };

        $scope.checkedAllforListTypeDetailModel = false;
        $scope.$watch('checkedAllforListTypeDetailModel', function(newValue, oldValue) {});
        $scope.dataFilter = {
            'custometType': [{
                    'CustomerTypeId': 3,
                    'CustomerTypeDesc': 'Individu'
                },
                {
                    'CustomerTypeId': 4,
                    'CustomerTypeDesc': 'Pemerintah'
                },
                {
                    'CustomerTypeId': 5,
                    'CustomerTypeDesc': 'Yayasan'
                },
                {
                    'CustomerTypeId': 6,
                    'CustomerTypeDesc': 'Perusahaan'
                },
            ]
        }
        $scope.filter = {
            'reminderType': [],
            'km': {
                'checked': false,
                'km1': null,
                'km2': null
            },
            'period': {
                'checked': false,
                'period1': null,
                'period2': null
            },
            'assemblyYear': {
                'checked': false,
                'assemblyYear1': null,
                'assemblyYear2': null
            },
            'customerType': {
                'checked': false,
                'customerType': null
            },
            'rangeKm': {
                'checked': false,
                'km1': null,
                'km2': null
            },
            'rangeServiceDate': {
                'checked': false,
                'period1': null,
                'period2': null
            },
            'rangeAsemblyYear': {
                'checked': false,
                'asemblyYear1': null,
                'asemblyYear2': null
            },
            'vehicleModel': {
                'checked': false,
                'vehicleModel': null
            }
        };

        $scope.VehicleGasType = [
            { VehicleGasTypeId: 1, VehicleGasTypeName: "Bensin" },
            { VehicleGasTypeId: 2, VehicleGasTypeName: "Diesel" },
            { VehicleGasTypeId: 3, VehicleGasTypeName: "Hybrid" }
        ];

        $scope.calYear = function(data) {
            var tmpYear = [];
            if (data != null) {
                for (var i = 1; i <= 5; i++) {
                    tmpYear.push({
                        yearName: data + i
                    });
                }
            }
            console.log("tmpYearnya===>", tmpYear);
            $scope.yearData = tmpYear;
        }
        $scope.selectYear = function(selected) {
            console.log("selectModel selected : ", selected);
        }
        $scope.selectModel = function(selected) {
            console.log("selectModel selected : ", selected);
            // MrsList.getDataTypeByModel(selected).then(function (res) {
            WO.getVehicleTypeById(selected).then(function(res) {
                console.log("result : ", res);
                //$scope.typeData = res.data;
                $scope.typeData = _.uniq(res.data.Result, 'KatashikiCode');
            });
        };
        MrsList.getReminderTypes().then(function(res) {
            console.log("result reminder type : ", res.data.Result);
            var temp = res.data.Result;

            for (var i = 0; i < temp.length; i++) {
                var obj = temp[i];
                obj.checked = false;
            }

            $scope.filter.reminderType = temp;
            console.log("result reminder type : ", $scope.filter.reminderType);
        });
        MrsList.getDataModel().then(function(res) {
            console.log("result : ", res.data.Result);
            $scope.modelData = res.data.Result;
        });
        MrsList.getVehicleMList().then(function(res) {
            console.log('getVehicleMList', res.data.Result);
            $scope.VehicM = res.data.Result;
        });
        $scope.onSelectedModel = function(item) {
            $scope.filter.vehicleModel.vehicleType = null;
            $scope.VehicT = [];
            MrsList.getCVehicleTypeById(item.VehicleModelId).then(function(res) {
                var VehicType = [];
                for (var i = 0; i < res.data.Result.length; i++) {
                    var obj = {};
                    obj.VehicleTypeId = res.data.Result[i].VehicleTypeId;
                    obj.NewDescription = res.data.Result[i].Description + ' - ' + res.data.Result[i].KatashikiCode;
                    obj.KatashikiCode = res.data.Result[i].KatashikiCode;
                    VehicType.push(obj);
                }
                $scope.VehicT = VehicType;
                console.log("VehicT", $scope.VehicT);
            });
        };

        $scope.selectedType = function(item) {
            console.log("selectedType", item);
        };

        $scope.getAllParameter = function(item) {
            console.log("getAllParameter", item);
            MrsList.getDataGender().then(function(res) {
                console.log("result gender : ", res.data.Result);
                var temp = res.data.Result;
                for (var i = 0; i < temp.length; i++) {
                    temp[i].value = temp[i].CustomerGenderId;
                    temp[i].text = (temp[i].CustomerGenderName == "Laki-Laki" ? "L" : "P");
                }
                console.log("tmp", temp);
                $scope.genderData = temp;
                resizeLayout();
            });
            MrsList.getDataCusRole().then(function(res) {
                console.log("result role : ", res.data.Result);
                $scope.RoleData = res.data.Result;
                resizeLayout();
            });
            MrsList.getCCustomerCategory().then(function(res) {
                $scope.CustomerTypeData = res.data.Result;
                resizeLayout();
            });
            if (item !== undefined) {
                if (item.ProvinceId !== undefined) {
                    // MrsList.getDataProvinceById(item.ProvinceId).then(function(res) {
                    //     console.log("result province : ", res.data.Result);
                    //     $scope.ProvinceData = res.data.Result;
                    //     resizeLayout();
                    // });
                    WOBP.getMLocationProvince().then(function(res) {
                        console.log("List Province====>", res.data.Result);
                        $scope.ProvinceData = res.data.Result;
                        resizeLayout();
                    })
                }
                if (item.CityRegencyId !== undefined) {
                    // MrsList.getDataCityById(item.CityRegencyId).then(function(res) {
                    //     console.log("result city : ", res.data.Result);
                    //     $scope.CityRegencyData = res.data.Result;
                    //     resizeLayout();
                    // });
                    WOBP.getMLocationCityRegency(item.ProvinceId).then(function(resu) {
                        $scope.CityRegencyData = resu.data.Result;
                        resizeLayout();
                    });
                }
                if (item.DistrictId !== undefined) {
                    // MrsList.getDataDistrictById(item.DistrictId).then(function(res) {
                    //     console.log("result district : ", res.data.Result);
                    //     $scope.DistrictData = res.data.Result;
                    // });
                    WOBP.getMLocationKecamatan(item.CityRegencyId).then(function(resu) {
                        $scope.DistrictData = resu.data.Result;
                    });
                }
                if (item.VillageId !== undefined) {
                    // MrsList.getDataVillageById(item.VillageId).then(function(res) {
                    //     console.log("result village : ", res.data.Result);
                    //     $scope.VillageData = res.data.Result;
                    //     resizeLayout();
                    // });
                    WOBP.getMLocationKelurahan(item.DistrictId).then(function(resu) {
                        $scope.VillageData = resu.data.Result;
                        resizeLayout();
                    });
                }
            }
            MrsList.getReminderMedia().then(function(res) {
                console.log("result media : ", res.data.Result);
                $scope.InvMediaData = res.data.Result;
                resizeLayout();
            });
            MrsList.getMRSInvitationStatus().then(function(res) {
                console.log("result mrs getMrsInvitation : ", res.data.Result);
                $scope.MrsStatus = res.data.Result;
                resizeLayout();
            });
            MrsList.getContactableReason().then(function(res) {
                console.log("result mrs invit : ", res.data.Result);
                $scope.MrsReminderStatusData = res.data.Result;
                resizeLayout();
            });
            MrsList.getUncontactableReason().then(function(res) {
                console.log("result mrs invit : ", res.data.Result);
                $scope.MrsReasonStatusData = res.data.Result;
                resizeLayout();
            });
            MrsList.getLastServiceHistory(item.VIN).then(function(res) {
                console.log("result getLastServiceHistory : ", res.data[0]);
                $scope.MrsLastServiceHistoryData = typeof res.data[0] == 'undefined' ? {} : res.data[0];
                resizeLayout();
            });
            if (item.VIN != null) {
                AppointmentGrService.getTowass(item.VIN).then(function(res) {
                    console.log("data Towasssssss", res.data);
                    $scope.dataTowass = res.data.Result;

                    // var isLengthOpeNo = res.data.Result.length;
                    // if (isLengthOpeNo > 0) {

                    //     var category = 1;   // KATEGORI GR
                    //     var vin = item.VIN;
                    //     var startDate = '2001-01-1';
                    //     var endDate = new Date();

                    //     var d = endDate.getDate();
                    //     var m = endDate.getMonth() + 1;
                    //     var y = endDate.getFullYear();

                    //     endDate = y + '-' + m + '-' + d;
                    //     console.log('endDate ==>', endDate);

                    //     VehicleHistory.getData(category, vin, startDate, endDate).then(
                    //         function (resu) {

                    //             console.log('DATA HISTORY TOWASS ====>', resu.data);
                    //             $scope.mDataVehicleHistory = resu.data;
                    //             var isLengthHistory = $scope.mDataVehicleHistory.length;

                    //             if (isLengthHistory != 0) {
                    //                 console.log("panjang isLengthHistory", isLengthHistory);

                    //                 for (var k = 0; k < isLengthOpeNo; k++) {
                    //                     console.log("k ==>", k);
                    //                     $scope.OpeNo = res.data.Result[k].OpeNo;
                    //                     console.log("$scope.OpeNo", $scope.OpeNo);

                    //                     for (var i = 0; i < isLengthHistory; i++) {

                    //                         console.log("i ==>", i);
                    //                         var isLengthVehicleJob = $scope.mDataVehicleHistory[i].VehicleJobService.length;
                    //                         console.log("panjang isLengthVehicleJob", isLengthVehicleJob);

                    //                         if (isLengthVehicleJob > 0) {

                    //                             for (var j = 0; j < isLengthVehicleJob; j++) {

                    //                                 console.log("j ==>", j);
                    //                                 var jobdesc = $scope.mDataVehicleHistory[i].VehicleJobService[j].JobDescription

                    //                                 if (jobdesc.includes($scope.OpeNo)) {
                    //                                     $scope.dataTowass = [];
                    //                                     console.log('masuk includes ===>');
                    //                                     console.log("$scope.OpeNo", $scope.OpeNo);
                    //                                     console.log("jobdesc", jobdesc);

                    //                                 } else {
                    //                                     console.log('tidak masuk includes ===>');
                    //                                 }
                    //                             }
                    //                         }

                    //                     }
                    //                 }

                    //             } else {

                    //                 $scope.dataTowass = res.data.Result;
                    //                 console.log('$scope.dataTowass ELSE =>', $scope.dataTowass);
                    //             }
                    //         },
                    //         function (err) { }
                    //     );
                    //     console.log('isLengthOpeNo ==>', isLengthOpeNo);
                    // }
                });
            };
            AppointmentGrService.getDataVehiclePSFU(item.VehicleId).then(function(resu) {
                console.log("getDataVehiclePSFU", resu.data.Result);
                var dataQuestionAnswerPSFU = resu.data.Result[0];

                //------- validasi tgl riwayat servis -----------------------
                var minTglDEC = new Date(dataQuestionAnswerPSFU.DECDate);
                console.log("minTglDEC", minTglDEC);
                $scope.DateOptionsHistory.minDate = minTglDEC.setDate(minTglDEC.getDate() + 1);

                var maxTglDEC = new Date();
                console.log("maxTglDEC", maxTglDEC);
                $scope.DateOptionsHistory.maxDate = maxTglDEC;
                //------------------------------------------------------------

                $scope.mDataDetail.JobSuggest = dataQuestionAnswerPSFU.JobSuggest;
                FollowupService.getQuestion().then(function(resu2) {
                    console.log("getQuestion", resu2.data.Result);
                    var dataQuestionAnswer = resu2.data.Result;
                    var arr = [];
                    _.map(dataQuestionAnswer, function(val, key) {
                        var obj = {};
                        if (dataQuestionAnswerPSFU.QuestionId1 == val.QuestionId) {
                            obj.QuestionId = val.QuestionId;
                            obj.Description = val.Description;
                            obj.Answer = val.Answer;
                            obj.Reason = dataQuestionAnswerPSFU.Reason1 ? dataQuestionAnswerPSFU.Reason1 : null;
                            obj.ReasonStatus = dataQuestionAnswerPSFU.Reason1 ? false : true;
                            arr.push(obj);
                        } else if (dataQuestionAnswerPSFU.QuestionId2 == val.QuestionId) {
                            obj.QuestionId = val.QuestionId;
                            obj.Description = val.Description;
                            obj.Answer = val.Answer;
                            obj.Reason = dataQuestionAnswerPSFU.Reason2 ? dataQuestionAnswerPSFU.Reason1 : null;
                            obj.ReasonStatus = dataQuestionAnswerPSFU.Reason2 ? false : true;
                            arr.push(obj);
                        } else if (dataQuestionAnswerPSFU.QuestionId3 == val.QuestionId) {
                            obj.QuestionId = val.QuestionId;
                            obj.Description = val.Description;
                            obj.Answer = val.Answer;
                            obj.Reason = dataQuestionAnswerPSFU.Reason3 ? dataQuestionAnswerPSFU.Reason3 : null;
                            obj.ReasonStatus = dataQuestionAnswerPSFU.Reason3 ? false : true;
                            arr.push(obj);
                        };
                        // arr.push(obj);
                    });
                    console.log('arr >>>', arr);
                    $scope.Question = arr;
                })
            });
            WO.getDataPKS(item.LicensePlate).then(function(res) {
                $scope.dataPKS = res.data.Result;
                console.log("PKS", $scope.dataPKS);
            });
        }
        $scope.reCalculate = function() {
            console.log('$scope.dataDetail ====', $scope.dataDetail);
            console.log('$scope.mDataDetail ===', $scope.mDataDetail);
            console.log('$scope.MrsLastServiceHistoryData ===', $scope.MrsLastServiceHistoryData);
            MrsList.getVehicleRecalculate($scope.mDataDetail.VIN, $scope.MrsLastServiceHistoryData.LastKM, $scope.MrsLastServiceHistoryData.LastServiceDate).then(function(res) {
                // if (res.data == -1) {
                //     bsNotify.show({
                //         size: 'big',
                //         type: 'danger',
                //         title: "Data Riwayat Service Tidak Ditemukan"
                //     });
                // } else {
                $scope.update.NextServiceDate = res.data.NextServiceDate;
                //$scope.update.nextKm = res.data.NextKm;                
                // console.log($("#LastKM").val())
                // var KMSekarang = Math.round($("#LastKM").val() / 1000) * 1000
                // console.log(KMSekarang);
                // var NextKM = parseFloat(KMSekarang) + 10000;
                // console.log(NextKM);
                // $scope.update.NextKm = parseFloat(KMSekarang) + 10000;
                $scope.update.nextKm = res.data.NextKm;
                console.log("NEXT KM", $scope.update.nextKm);
                resizeLayout();
                // }
            });

        }

        $scope.checkedAllforListTypeDetailFunc = function() {
            console.log("hello");
            console.log($scope.checkedAllforListTypeDetailModel);

            $scope.checkedAllforListTypeDetailModel != $scope.checkedAllforListTypeDetailModel;

            for (var i = 0; i < $scope.filter.vehicleType.listTypeDetail.length; i++) {
                $scope.filter.vehicleType.listTypeDetail[i].checked = $scope.checkedAllforListTypeDetailModel;
            }
        };

        $scope.user = CurrentUser.user();
        $scope.mData = null; //Model
        $scope.xData = {
            selected: []
        };
        $scope.update = {};
        var det = new Date("October 13, 2014 11:13:00");

        var dateFormat = 'dd/MM/yyyy';
        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
        };

        var dateFormat1 = 'dd/MM/yyyy';
        $scope.DateOptions1 = {
            startingDay: 1,
            format: dateFormat1,
        };

        var dt = [{ name: 'January' },
            { name: 'February' },
            { name: 'March' },
            { name: 'April' },
            { name: 'May' },
            { name: 'June' },
            { name: 'July' },
            { name: 'August' },
            { name: 'September' },
            { name: 'October' },
            { name: 'November' },
            { name: 'Desember' }
        ]
        $scope.selectType = function() {

            }
            // var dt = {'January','February','March','April','May','June','July','August','September','October','November','Desember'}
        $scope.dt = dt;
        var gridData = [];


        //----------------------------------
        // Get Data
        //----------------------------------

        $scope.getData = function() {
            console.log("filter-->", $scope.filter);

            if (_.findIndex($scope.filter.reminderType, { 'checked': true }) == -1) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Filter"
                });
                console.log("Other checked", $scope.filter.reminderType);
            } else {
                $scope.grid.data = [];
                //if ($scope.filter.reminderType[5].checked) {

                console.log("Other checked", $scope.filter.reminderType);
                if ($scope.filter.reminderType[5].checked && !$scope.filter.customerType.checked && !$scope.filter.rangeKm.checked && !$scope.filter.rangeServiceDate.checked && !$scope.filter.rangeAsemblyYear.checked && !$scope.filter.vehicleModel.checked) {

                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Mohon Input Filter"
                    });
                } else {

                    var reminderTypeArray = [];
                    var filter = {};
                    var isError = false;

                    for (var key in $scope.filter.reminderType) {
                        if ($scope.filter.reminderType[key].checked) {
                            reminderTypeArray.push($scope.filter.reminderType[key].MasterId);
                        }
                    }

                    filter.reminderType = reminderTypeArray.join();
                    filter.customerTypeId = "-";
                    filter.kmStart = filter.kmEnd = "-";
                    filter.dateStart = filter.dateEnd = "-";
                    filter.assemblyYearStart = filter.assemblyYearEnd = "-";
                    filter.vehicleModel = "-";
                    filter.vehicleType = "-";

                    console.log('customerType =>', $scope.filter.customerType.customerType);
                    if ($scope.filter.customerType.checked) {

                        if ($scope.filter.customerType.customerType != null) {
                            filter.customerTypeId = $scope.filter.customerType.customerType;
                        } else {
                            isError = true;
                        }
                    }

                    if ($scope.filter.rangeKm.checked) {
                        console.log("filter--> rangeKm ", $scope.filter.rangeKm);
                        if ($scope.filter.rangeKm.km1 != null && $scope.filter.rangeKm.km2 != null) {
                            filter.kmStart = $scope.filter.rangeKm.km1;
                            filter.kmEnd = $scope.filter.rangeKm.km2;
                        } else {
                            isError = true;
                        }
                    } else {

                    }
                    if ($scope.filter.rangeServiceDate.checked) {
                        if ($scope.filter.rangeServiceDate.period1 != null && $scope.filter.rangeServiceDate.period2 != null) {
                            // filter.dateStart = $scope.filter.rangeServiceDate.period1;
                            // filter.dateEnd = $scope.filter.rangeServiceDate.period2;
                            var tmpDate = $scope.filter.rangeServiceDate.period1;
                            var tmpDate2 = $scope.filter.rangeServiceDate.period2;
                            var tmpMonth = $scope.filter.rangeServiceDate.period1;
                            var tmpMonth2 = $scope.filter.rangeServiceDate.period2;
                            console.log("tmpMonth", tmpMonth);
                            var tmpTime1 = $scope.filter.rangeServiceDate.period1;
                            var tmpTime2 = $scope.filter.rangeServiceDate.period2;
                            // if (tmpDate.getMonth() < 10) {
                            // tmpMonth = "0" + (tmpDate.getMonth() + 1);
                            // } else {
                            tmpMonth = (tmpDate.getMonth() + 1).toString();
                            tmpMonth2 = (tmpDate2.getMonth() + 1).toString();
                            // }
                            var ini = tmpDate.getFullYear() + "-" + (tmpMonth[1] ? tmpMonth : "0" + tmpMonth[0]) + "-" + tmpDate.getDate();
                            var ini2 = tmpDate2.getFullYear() + "-" + (tmpMonth2[1] ? tmpMonth2 : "0" + tmpMonth2[0]) + "-" + tmpDate2.getDate();

                            var d = new Date(ini);
                            var d2 = new Date(ini2);
                            console.log('data yang diinginkan', ini);
                            filter.dateStart = ini;
                            filter.dateEnd = ini2;
                        } else {
                            isError = true;
                        }
                    }

                    if ($scope.filter.rangeAsemblyYear.checked) {
                        if ($scope.filter.rangeAsemblyYear.asemblyYear1 != null && $scope.filter.rangeAsemblyYear.asemblyYear2 != null) {
                            filter.assemblyYearStart = $scope.filter.rangeAsemblyYear.asemblyYear1;
                            filter.assemblyYearEnd = $scope.filter.rangeAsemblyYear.asemblyYear2;
                        } else {
                            isError = true;
                        }
                    }

                    if ($scope.filter.vehicleModel.checked) {
                        if ($scope.filter.vehicleModel.vehicleModel != null) {
                            filter.vehicleModel = $scope.filter.vehicleModel.vehicleModel;
                            if ($scope.filter.vehicleModel.vehicleType != null) {
                                // if ($scope.filter.vehicleModel.vehicleType.length > 0) {
                                filter.vehicleType = $scope.filter.vehicleModel.vehicleType.toString();
                                // }
                            }
                        } else {
                            isError = true;
                        }
                    }

                    if (isError) {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Mohon Input Filter"
                        });
                        return;
                    }

                    console.log("filter--> Final ", filter);
                    MrsList.getMRSListReminder(filter).then(
                        function(res) {
                            console.log("data hello=>", res);
                            // $scope.grid.data = $scope.dataDummy;
                            $scope.loading = false;
                            $scope.grid.data = res.data.Result;
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
                }
                // } else {
                //     var temp = [];
                //     var filParam = '';
                //     for (var key in $scope.filter.reminderType) {
                //         if ($scope.filter.reminderType[key].checked) {
                //             temp.push($scope.filter.reminderType[key].MasterId);
                //             console.log('skuy living');
                //         }
                //     }

                //     console.log(temp);
                //     var thisDay = [];
                //     var x = new Date();
                //     thisDay.push(x.getFullYear());
                //     var tempMonth = x.getMonth() + 1;
                //     var tempDate = x.getDate();
                //     thisDay.push((tempMonth < 10 ? '0' + tempMonth : tempMonth));
                //     thisDay.push((tempDate < 10 ? '0' + tempDate : tempDate));
                //     console.log(thisDay);

                //     filParam += temp.join();
                //     if ($scope.filter.km.checked == true) {
                //         filParam += ($scope.filter.km.km1 == null ? '/-' : '/' + $scope.filter.km.km1);
                //         filParam += ($scope.filter.km.km2 == null ? '/-' : '/' + $scope.filter.km.km2);
                //     } else {
                //         filParam += ('/-');
                //         filParam += ('/-');
                //     }
                //     if ($scope.filter.period.checked == true) {
                //         if ($scope.filter.period.period1 !== null && $scope.filter.period.period2 !== null) {
                //             var a = new Date($scope.filter.period.period1);
                //             var yearFirst1 = a.getFullYear();
                //             var monthFirst1 = a.getMonth() + 1;
                //             monthFirst1 = (monthFirst1 < 10 ? '0' + monthFirst1 : monthFirst1);
                //             var dayFirst1 = a.getDate();
                //             dayFirst1 = (dayFirst1 < 10 ? '0' + dayFirst1 : dayFirst1);
                //             $scope.filter.period.period1 = yearFirst1 + '-' + monthFirst1 + '-' + dayFirst1;
                //             var b = new Date($scope.filter.period.period2);
                //             var yearFirst2 = b.getFullYear();
                //             var monthFirst2 = b.getMonth() + 1;
                //             monthFirst2 = (monthFirst2 < 10 ? '0' + monthFirst2 : monthFirst2);
                //             var dayFirst2 = b.getDate();
                //             dayFirst2 = (dayFirst2 < 10 ? '0' + dayFirst2 : dayFirst2);
                //             $scope.filter.period.period2 = yearFirst2 + '-' + monthFirst2 + '-' + dayFirst2;
                //         }
                //         // ==================
                //         filParam += ($scope.filter.period.period1 == null ? '/' + thisDay.join('-') : '/' + $scope.filter.period.period1);
                //         filParam += ($scope.filter.period.period2 == null ? '/' + thisDay.join('-') : '/' + $scope.filter.period.period2);
                //     } else {
                //         filParam += ('/' + thisDay.join('-'));
                //         filParam += ('/' + thisDay.join('-'));
                //     }
                //     // filParam+=($scope.filter.period.period1==null?'/-':'/'+$scope.filter.period.period1);
                //     // filParam+=($scope.filter.period.period2==null?'/-':'/'+$scope.filter.period.period2);
                //     if ($scope.filter.assemblyYear.checked == true) {
                //         filParam += ($scope.filter.assemblyYear.assemblyYear1 == null ? '/-' : '/' + $scope.filter.assemblyYear.assemblyYear1);
                //         filParam += ($scope.filter.assemblyYear.assemblyYear2 == null ? '/-' : '/' + $scope.filter.assemblyYear.assemblyYear2);
                //     } else {
                //         filParam += ('/-');
                //         filParam += ('/-');
                //     }
                //     filParam += ($scope.filter.selType == null || $scope.filter.selType == undefined ? '/-' : '/' + $scope.filter.selType.join());

                //     console.log(filParam);
                //     MrsList.getData(filParam).then(
                //         function (res) {
                //             console.log("data hello=>", res);
                //             // $scope.grid.data = $scope.dataDummy;
                //             $scope.grid.data = res.data;
                //         },
                //         function (err) {
                //             console.log("err=>", err);
                //         }
                //     );
                // }

            }
        }

        $scope.clickChecked = function(data) {
            console.log("$scope.filter====>", $scope.filter);
            $scope.filter.km.checked = false;
            $scope.filter.period.checked = false;
            $scope.filter.assemblyYear.checked = false;
            // $scope.filter.vehicleType.checked = false;
        }

        $scope.onClickChange = function(data) {
            // resizeLayout();
            if (data.Name == "Other" && !data.checked) {
                $timeout(function() {
                    staticHeight = $('#layoutContainer_MrsListForm .well').height();
                    $("#layoutContainer_MrsListForm").height(staticHeight)
                    console.log("staticHeight", staticHeight);
                }, 100);
                $scope.filter.customerType.checked = false;
                $scope.filter.rangeKm.checked = false;
                $scope.filter.rangeServiceDate.checked = false;
                $scope.filter.rangeAsemblyYear.checked = false;
                $scope.filter.vehicleModel.checked = false;

                $scope.filter.customerType.customerType = null;
                $scope.filter.rangeKm.km1 = null;
                $scope.filter.rangeKm.km2 = null;
                $scope.filter.rangeServiceDate.period1 = null;
                $scope.filter.rangeServiceDate.period2 = null;
                $scope.filter.rangeAsemblyYear.asemblyYear1 = null;
                $scope.filter.rangeAsemblyYear.asemblyYear2 = null;
                $scope.filter.vehicleModel.vehicleModel = null;
                $scope.filter.vehicleModel.vehicleType = null;
            }
        };

        $scope.getDetail = function(cb, val) {
            $scope.tab.activeJustified = 0;
            $scope.mDataVehicleHistory = [];
            $scope.MRS = {};
            $scope.update = null;
            if (val.ReminderType == "SBE") {
                $scope.dissReCalculate = false;
            } else {
                $scope.dissReCalculate = true;
            }
            console.log("inside getDetail..");
            console.log(val);
            $scope.dataDetail = angular.copy(val);
            console.log("inside dataDetail..", $scope.dataDetail);
            $scope.disab = true;
            var param = {};
            param.MrsId = val.MrsId != null ? val.MrsId : "-";
            param.VehicleId = val.MrsId != null ? "-" : val.VehicleId;
            console.log("inside Param..", param);
            MrsList.getDetail(param).then(
                function(res) {

                    var detail = res.data[0];
                    console.log("detail=>", res);
                    $scope.mDataDetail = res.data[0];
                    if (res.data[0].CustomerTypeId == 2) {
                        $scope.mDataDetail.PICName = res.data[0].CustomerName;
                    }
                    $scope.getAllParameter(detail);
                    if (cb) {
                        cb(val);
                        console.log("before resize..");
                        resizeLayout();
                    }
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        };

        $scope.test = function(data) {
            console.log("ini data", data);
            if (data == 3) {
                console.log("test id 3");
            } else if (data == 2) {
                console.log("test id 2");
            } else if (data == 1) {
                console.log("test id 1");
            } else if (data == 7) {
                $scope.update.nextKm = null;
            } else {
                console.log("data ", data);
            }
        };

        $scope.onShowDetail = function(mdl, act) {
            console.log("mdl", mdl);
            console.log("act", act);
        }
        $scope.doCustomSave = function(mdl, mode) {
            console.log("custom save..");
            console.log("mdl = ", mdl);
            console.log("mode = ", mode);

            if (mdl.RequestAppointmentDate) {
                var a = new Date(mdl.RequestAppointmentDate);
                var yearFirst1 = a.getFullYear();
                var monthFirst1 = a.getMonth() + 1;
                monthFirst1 = (monthFirst1 < 10 ? '0' + monthFirst1 : monthFirst1);
                var dayFirst1 = a.getDate();
                dayFirst1 = (dayFirst1 < 10 ? '0' + dayFirst1 : dayFirst1);
                mdl.RequestAppointmentDate = yearFirst1 + '-' + monthFirst1 + '-' + dayFirst1;
            }
            if (mdl.reminderPlanDate) {
                var a = new Date(mdl.reminderPlanDate);
                var yearFirst1 = a.getFullYear();
                var monthFirst1 = a.getMonth() + 1;
                monthFirst1 = (monthFirst1 < 10 ? '0' + monthFirst1 : monthFirst1);
                var dayFirst1 = a.getDate();
                dayFirst1 = (dayFirst1 < 10 ? '0' + dayFirst1 : dayFirst1);
                mdl.reminderPlanDate = yearFirst1 + '-' + monthFirst1 + '-' + dayFirst1;
            }
            if (mdl.NextServiceDate) {
                var a = new Date(mdl.NextServiceDate);
                var yearFirst1 = a.getFullYear();
                var monthFirst1 = a.getMonth() + 1;
                monthFirst1 = (monthFirst1 < 10 ? '0' + monthFirst1 : monthFirst1);
                var dayFirst1 = a.getDate();
                dayFirst1 = (dayFirst1 < 10 ? '0' + dayFirst1 : dayFirst1);
                mdl.NextServiceDate = yearFirst1 + '-' + monthFirst1 + '-' + dayFirst1;
            }
            if (mdl.DueDate) {
                var a = new Date(mdl.DueDate);
                var yearFirst1 = a.getFullYear();
                var monthFirst1 = a.getMonth() + 1;
                monthFirst1 = (monthFirst1 < 10 ? '0' + monthFirst1 : monthFirst1);
                var dayFirst1 = a.getDate();
                dayFirst1 = (dayFirst1 < 10 ? '0' + dayFirst1 : dayFirst1);
                mdl.DueDate = yearFirst1 + '-' + monthFirst1 + '-' + dayFirst1;
            }


            if (mdl.RequestAppointmentDate != undefined || mdl.RequestAppointmentDate != null) {
                var Data = {};
                var a = new Date(mdl.RequestAppointmentDate);
                var yearFirst1 = a.getFullYear();
                var monthFirst1 = a.getMonth() + 1;
                var nameMonth = {
                    0: "Januari",
                    1: "Februari",
                    2: "Maret",
                    3: "April",
                    4: "Mei",
                    5: "Juni",
                    6: "Juli",
                    7: "Agustus",
                    8: "September",
                    9: "Oktober",
                    10: "November",
                    11: "Desember"
                };
                var monthFirst12 = a.getMonth();
                var monthFirst1 = nameMonth[monthFirst12];
                //monthFirst1 = (monthFirst1 < 10 ? '0' + monthFirst1 : monthFirst1);
                var dayFirst1 = a.getDate();
                dayFirst1 = (dayFirst1 < 10 ? '0' + dayFirst1 : dayFirst1);
                var DateAppointment = dayFirst1 + ' ' + monthFirst1 + ' ' + yearFirst1;

                Data.Message = "No. Polisi : " + mdl.Nopol + " <br> Tgl. Permintaan : " + DateAppointment +
                    "<br> Type Permintaan : " + mdl.ReminderType;
                MrsList.sendNotif(Data, 1056, 13).then(
                    function(res) {

                    },
                    function(err) {
                        //console.log("err=>", err);
                    });
                //console.log('DateAppointmentaaaaa', DateAppointment)
                MrsList.sendNotif(Data, 1025, 13).then(
                    function(res) {

                    },
                    function(err) {
                        //console.log("err=>", err);
                    });

            }

            MrsList.updateByCall(mdl).then(function(res) {
                bsAlert.alert({
                            title: "Simpan Berhasil",
                            text: "",
                            type: "warning",
                            showCancelButton: false
                        },
                        function() {
                            $scope.getData();
                        },
                        function() {

                        }
                    )
                    // $scope.getData();
            });
            $scope.formApi.setMode("grid");

        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() {
                $scope.$broadcast('show-errors-check-validity');
            });
        }
        $scope.dis = 0;
        var selectedRow = [];
        $scope.dataGrid = [];
        $scope.onSelectRows = function(rows) {
            $scope.dis = rows.length;
            console.log("test", $scope.dis);
            console.log("onSelectRows=>", rows);
            selectedRow = rows;
            $scope.dataGrid.Grid = angular.copy(selectedRow);
            console.log('datagrid', $scope.dataGrid);
        };

        $scope.onCustomerTypeChange = function() {
            if ($scope.filter.customerType.checked) {
                $scope.filter.customerType.customerType = null;
            }
        };
        $scope.onKMChange = function() {
            if ($scope.filter.rangeKm.checked) {
                $scope.filter.rangeKm.km1 = null;
                $scope.filter.rangeKm.km2 = null;
            }
        }
        $scope.onRangeServiceDateChange = function() {
            if ($scope.filter.rangeServiceDate.checked) {
                $scope.filter.rangeServiceDate.period1 = null;
                $scope.filter.rangeServiceDate.period2 = null;
            }
        }
        $scope.onRangeAsemblyYearChange = function() {
            if ($scope.filter.rangeAsemblyYear.checked) {
                $scope.filter.rangeAsemblyYear.asemblyYear1 = null;
                $scope.filter.rangeAsemblyYear.asemblyYear2 = null;
            }
        }
        $scope.onVehicleModelChange = function() {
                if ($scope.filter.vehicleModel.checked) {
                    $scope.filter.vehicleModel.vehicleModel = null;
                    $scope.filter.vehicleModel.vehicleType = null;
                }
            }
            //----------------------------------
            // Grid Setup
            //----------------------------------

        // $scope.gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
        //     '<a href="#" uib-tooltip="View" tooltip-placement="bottom" ng-click="grid.appScope.actView(row.entity)" style=""><i class="fa fa-fw fa-lg fa-list" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>'+
        //     '<a href="#" uib-tooltip="Edit" tooltip-placement="bottom" ng-click="grid.appScope.actEdit(row.entity)" style=""><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>'+
        //     '<a href="#" uib-tooltip="Buat" tooltip-placement="bottom" ng-click="grid.appScope.actView(row.entity)" style=""><i class="fa fa-fw fa-lg fa-phone" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>'+
        //     // '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="Buat" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-plus-circle" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>'+
        //   '</div>'

        // CallCountAvailable harus lebih besar dari "0"
        $scope.gridActionButtonTemplate =
            '<div class="ui-grid-cell-contents">' +
            //   '<a href="#" uib-tooltip="View" tooltip-placement="bottom" ng-click="grid.appScope.actView(row.entity)" style=""><i class="fa fa-fw fa-lg fa-list" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            // '<a href="#" uib-tooltip="Edit" tooltip-placement="bottom" ng-click="grid.appScope.actEdit(row.entity)" style=""><i class="fa fa-fw fa-lg fa-phone" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>'+
            '<a href="#" ng-show="row.entity.CallCountAvailable > 0" uib-tooltip="Edit" tooltip-placement="bottom" ng-click="grid.appScope.$parent.getDetail(grid.appScope.actEdit,row.entity)" style=""><i class="fa fa-fw fa-lg fa-phone" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';

        $scope.callCount = 3;
        $scope.gridActionTemplate = '<div class="ui-grid-cell-contents">' +
            //<i ng-hide="row.entity.CallCountAvailable == 0 || row.entity.CallCountAvailable > 2" class="fa fa-fw fa-lg fa-phone"></i><span ng-hide="row.entity.CallCountAvailable == 0 || row.entity.CallCountAvailable > 2" class="badge badge-menu" style="font-size:8px;" >{{row.entity.CallCountAvailable}}</span>&nbsp;&nbsp;\
            '<i ng-show="row.entity.CallCountAvailable > 0 " class="fa fa-fw fa-lg fa-phone"></i><span ng-show="row.entity.CallCountAvailable > 0" class="badge badge-menu" style="font-size:8px;" >{{row.entity.CallCountAvailable}}</span>&nbsp;&nbsp;\
          <i ng-hide="row.entity.SmsCountAvailable == 0" class="fa fa-fw fa-lg fa-commenting" ></i><span ng-hide="row.entity.SmsCountAvailable == 0" class="badge badge-menu" style="margin-bottom: 18px; font-size:10px;" >{{row.entity.SmsCountAvailable}}</span>&nbsp;&nbsp;\
          <i ng-hide="row.entity.EmailCountAvailable == 0" class="fa fa-fw fa-lg fa-envelope"></i><span ng-hide="row.entity.EmailCountAvailable == 0" class="badge badge-menu" style="margin-bottom: 18px; font-size:10px;" >{{row.entity.EmailCountAvailable}}</span></div>'

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                { name: 'id', field: 'Id', width: '7%', visible: false },
                { name: 'Jenis Reminder', field: 'ReminderType', displayName: 'Tipe Reminder', width: '10%' },
                { name: 'No. Polisi', field: 'Nopol', displayName: 'No. Polisi', width: '12%' },
                { name: 'Nama Pelanggan', field: 'Name', width: '20%' },
                { name: 'Alamat', field: 'Address', width: '25%' },
                { name: 'Telpon', field: 'Phone', displayName: 'No.Telepon', width: '15%' },
                { name: 'KM', displayName: 'KM', field: 'KM', width: '8%' },
                { name: 'Next KM', displayName: 'Next KM', field: 'NextKm', width: '8%' },
                { name: 'Est. Tgl. Service', field: 'DueDate', cellFilter: 'date:\'dd-MM-yyyy\'', width: '12%' },
                { name: 'Available  Action', field: 'avaiableAction', cellTemplate: $scope.gridActionTemplate, width: '12%' },
                { name: 'Status', field: 'Stat', width: '15%' }
            ]
        };
        // $scope.dummyData = [{
        //     Id : 1,
        //     ReminderType : 1,
        //     Name : "ASD",
        //     Address : "Disitu Noh",
        //     Phone : 085765554137,
        //     Km : 1000,
        //     NextKm : 1500,
        //     status : 1
        // }]
        $scope.hidePhone = false;
        $scope.hideSMS = false;
        $scope.hideEmail = false;
        // $scope.callCount = 3;
        $scope.dataDummy = [{
            id: 1,
            ReminderType: 'GR',
            Name: 'Adi Daya Sudaya',
            Address: 'Jl. Sini XV No. 212',
            Phone: '085765554137',
            KM: '5200',
            NextKm: '12000',
            avaiableAction: '',
            status: 'Open',
            estDate: '20/05/2017',
            noPolisi: 'ABC 123 KZL',
            email: 'Adi@Adi.Adi',
            callCount: 3,
            smsCount: 1,
            emailCount: 1
        }, {
            id: 2,
            ReminderType: 'FA',
            Name: 'Edi Daya Sudaya',
            Address: 'Jl. Sini X No. 212',
            Phone: '',
            KM: '70000',
            NextKm: '150000',
            avaiableAction: '',
            status: 'Open',
            estDate: '18/05/2017',
            noPolisi: 'ASD 123 KZL',
            email: 'Edi@Edi.Edi',
            callCount: 3,
            smsCount: 1,
            emailCount: 1

        }, {
            id: 3,
            ReminderType: 'GR',
            Name: 'Armadi Daya Sudaya',
            Address: 'Jl. Sini V No. 212',
            Phone: '085132147777',
            KM: '8000',
            NextKm: '20000',
            avaiableAction: '',
            status: 'Open',
            estDate: '22/05/2017',
            noPolisi: 'QWE 123 KZL',
            email: 'Armadi@Armadi.Armadi',
            callCount: 3,
            smsCount: 1,
            emailCount: 1

        }]

        $scope.actHistory = function(vin) {
            $scope.MRS.category = 2;
            var category = 2;
            var VIN = vin;
            var startDate = '-';
            var endDate = '-';
            $scope.ServiceHistory(category, VIN, startDate, endDate);
        }

        $scope.change = function(startDate, endDate, category, vin) {

            console.log("start Date 1", startDate);
            console.log("start Date 2", endDate);
            category = category.toString();
            var copyAngular = angular.copy(category);
            console.log("category", category);
            console.log("Copycategory", copyAngular);
            console.log("vin", vin);
            if (category == "" || category == null || category == undefined) {
                bsNotify.show({
                    size: 'big',
                    title: 'Mohon Isi Kategori Pekerjaan',
                    text: 'I will close in 2 seconds.',
                    timeout: 2000,
                    type: 'danger'
                });
            } else {

                if (startDate == undefined || startDate == "" || startDate == null || startDate == "Invalid Date") {
                    bsNotify.show({
                        size: 'big',
                        title: 'Mohon Isi tanggal mulai servis',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                } else {

                    var tmpDate = startDate;
                    var tmpMonth = startDate;
                    console.log("tmpMonth", tmpMonth);
                    var tmpTime = $scope.startDate;
                    // if (tmpDate.getMonth() < 10) {
                    // tmpMonth = "0" + (tmpDate.getMonth() + 1);
                    // } else {
                    tmpMonth = (tmpDate.getMonth() + 1).toString();
                    // }
                    var ini = tmpDate.getFullYear() + "-" + (tmpMonth[1] ? tmpMonth : "0" + tmpMonth[0]) + "-" + tmpDate.getDate();
                    var d = new Date(ini);
                    console.log('data yang diinginkan', ini);
                    startDate = ini;
                };


                if (endDate == undefined || endDate == "" || endDate == null || endDate == "Invalid Date") {
                    bsNotify.show({
                        size: 'big',
                        title: 'Mohon Isi tanggal mulai servis',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                } else {

                    var tmpDate2 = endDate;
                    var tmpMonth2 = endDate;
                    console.log("tmpMonth2", tmpMonth2);
                    var tmpTime2 = endDate;

                    // if (tmpDate2.getMonth() < 10) {
                    //     tmpMonth2 = "0" + (tmpDate2.getMonth() + 1);
                    // } else {
                    tmpMonth2 = (tmpDate2.getMonth() + 1).toString();
                    // }
                    var ini2 = tmpDate2.getFullYear() + "-" + (tmpMonth2[1] ? tmpMonth2 : "0" + tmpMonth2[0]) + "-" + tmpDate2.getDate();
                    var d2 = new Date(ini2);
                    console.log('data yang diinginkan 2', ini2);
                    endDate = ini2;
                };

                if (typeof vin !== "undefined") {
                    category = parseInt(category);
                    $scope.ServiceHistory(category, vin, startDate, endDate);
                } else {
                    bsNotify.show({
                        size: 'big',
                        title: 'VIN Tidak Ditemukan',
                        text: 'I will close in 2 seconds.',
                        timeout: 2000,
                        type: 'danger'
                    });
                };
            };
        };

        $scope.ServiceHistory = function(category, vin, startDate, endDate) {
            VehicleHistory.getData(category, vin, startDate, endDate).then(
                function(res) {
                    if (res.data.length == 0) {
                        $scope.mDataVehicleHistory = [];
                        bsNotify.show({
                            size: 'big',
                            title: 'Data Riwayat Service Tidak Ditemukan',
                            text: 'I will close in 2 seconds.',
                            timeout: 2000,
                            type: 'danger'
                        });
                    } else {
                        res.data.sort(function(a, b) {
                            // Turn your strings into dates, and then subtract them
                            // to get a value that is either negative, positive, or zero.
                            return new Date(b.ServiceDate) - new Date(a.ServiceDate);
                        });
                        // di komen 30 agustus 2019, katanya pengen tampilan nya 2 angka belakang koma semua. jadi di kasi pipe aja di html
                        // if (res.data !== null) {
                        //     for(var i in res.data){
                        //         if (res.data[i].VehicleJobService != null && res.data[i].VehicleJobService != undefined){
                        //             angular.forEach(res.data[i].VehicleJobService, function(val2) {
                        //                 if (val2.VehicleServiceHistoryDetail != null && val2.VehicleServiceHistoryDetail != undefined){
                        //                     angular.forEach(val2.VehicleServiceHistoryDetail, function(val3) {
                        //                         // if (val3.Quantity.toString().includes('.')){ 
                        //                             // val3.Quantity = parseFloat(val3.Quantity).toFixed(2);
                        //                             // val3.Quantity = val3.Quantity.toString();
                        //                         // }
                        //                     })
                        //                 }
                        //             })
                        //         }
                        //     }
                        // }
                        $scope.mDataVehicleHistory = res.data;
                        console.log("kesini ga?");
                        $scope.value = angular.copy($scope.mDataVehicleHistory);

                        $scope.MRS.category = category;

                        var dateEnd = $scope.mDataVehicleHistory[0].ServiceDate;
                        $scope.MRS.endDate = dateEnd;

                        var terakhir = $scope.mDataVehicleHistory.length;
                        $scope.MRS.startDate = $scope.mDataVehicleHistory[terakhir - 1].ServiceDate;

                        console.log('category', $scope.MRS.category);
                        console.log('startDate', $scope.MRS.startDate);
                        console.log('EndDate', $scope.MRS.endDate);
                    };

                    console.log("mDataVehicleHistory=> Followup ", $scope.mDataVehicleHistory);
                },
                function(err) {}
            );
        }
    });