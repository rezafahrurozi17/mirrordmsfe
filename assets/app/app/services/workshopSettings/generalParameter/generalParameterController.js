angular.module('app')
    .controller('GeneralParameterController', function ($scope, $http, bsAlert, CurrentUser, GeneralParameter, translator2, translator, $timeout) {
        // agGrid.initialiseAgGridWithAngular1(angular);
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function () {
            $scope.loading = true;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mParam = {}; //Model
        $scope.xParam = {};
        $scope.xParam.selected = [];
        console.log("mparam", $scope.mParam);
        $scope.ismeridian = false;
        // $scope.mytime = new Date();
        $scope.hstep = 1;
        $scope.mstep = 1;

        //----------------------------------
        // Get Data
        //----------------------------------
        var gridData = [];
        $scope.getData = function () {
            GeneralParameter.getData().then(function (res) {
                console.log(res);
                var getData = translator2.arraySortforBSFormTree(res.data.Result, 'ParamId', "ParamParentId");
                console.log('getData', getData);

                $scope.grid.data = getData;

                $scope.loading = false;
            },
                function (err) {
                    console.log("err=>", err);
                }
            );
        };

        $scope.doSave = function (data, mode) {
            console.log('doSave', data, mode);
            if (data.TimeVal1 != null) {
                var d = new Date($scope.mParam.TimeVal1);
                data.TimeValFlag1 = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                // console.log("d",d.getHours());
            }
            if (data.TimeVal2 != null) {
                var da = new Date($scope.mParam.TimeVal2);
                // da.getHours();
                // da.getMinutes();
                data.TimeValFlag2 = da.getHours() + ':' + da.getMinutes() + ':' + da.getSeconds();
                // console.log("da",da);
            }
            console.log("data befor diup", data);

            GeneralParameter.update(data).then(function (resu) {
                console.log('update', resu);
                if (resu.data.ResponseCode == 23)
                    bsAlert.alert({
                        title: "Data berhasil diperbaharui",
                        text: "",
                        type: "success",
                        showCancelButton: false
                    });
                $scope.getData();
            });
        };

        $scope.onBeforeEdit = function (data) {
            console.log("data", data);
            var tThru = new Date("Wed Jan 18 2017 " + data.TimeVal1 + " GMT+0700 (SE Asia Standard Time)");
            tThru = tThru.toISOString();
            $scope.mParam.TimeVal1 = tThru;

            var tThru2 = new Date("Wed Jan 18 2017 " + data.TimeVal2 + " GMT+0700 (SE Asia Standard Time)");
            tThru2 = tThru2.toISOString();
            $scope.mParam.TimeVal2 = tThru2;

        }

        $scope.disableTimeVal1 = false;
        $scope.disableTimeVal2 = false;
        $scope.disableStrVal = false;
        $scope.disableNumVal1 = false;
        $scope.disableNumVal2 = false;

        $scope.onShowDetail = function (row) {

            // list yg bisa di edit base on type (row.Type)
            // type=0 : strval
            // type=1 : numval1
            // type=2 : numval1 and numval2
            // type=3 : timeval1
            // type=4 : timeval1 and timeval2
            // type=5 : strval and numval1 and numval2

            if (row.Type == 0) {
                $scope.disableTimeVal1 = true;
                $scope.disableTimeVal2 = true;
                $scope.disableStrVal = false;
                $scope.disableNumVal1 = true;
                $scope.disableNumVal2 = true;
            } else if (row.Type == 1) {
                $scope.disableTimeVal1 = true;
                $scope.disableTimeVal2 = true;
                $scope.disableStrVal = true;
                $scope.disableNumVal1 = false;
                $scope.disableNumVal2 = true;
            } else if (row.Type == 2) {
                $scope.disableTimeVal1 = true;
                $scope.disableTimeVal2 = true;
                $scope.disableStrVal = true;
                $scope.disableNumVal1 = false;
                $scope.disableNumVal2 = false;
            } else if (row.Type == 3) {
                $scope.disableTimeVal1 = false;
                $scope.disableTimeVal2 = true;
                $scope.disableStrVal = true;
                $scope.disableNumVal1 = true;
                $scope.disableNumVal2 = true;
            } else if (row.Type == 4) {
                $scope.disableTimeVal1 = false;
                $scope.disableTimeVal2 = false;
                $scope.disableStrVal = true;
                $scope.disableNumVal1 = true;
                $scope.disableNumVal2 = true;
            } else if (row.Type == 5) {
                $scope.disableTimeVal1 = true;
                $scope.disableTimeVal2 = true;
                $scope.disableStrVal = false;
                $scope.disableNumVal1 = false;
                $scope.disableNumVal2 = false;
            }

            console.log("detail detail detail", row);
            $scope.mParam.StrVal = row.StrVal;
            $scope.mParam.NumVal1 = row.NumVal1;
            $scope.mParam.NumVal2 = row.NumVal2;

            $timeout(function () {

                var tThru = new Date("Wed Jan 18 2017 " + row.TimeVal1 + " GMT+0700 (SE Asia Standard Time)");
                tThru = tThru.toISOString();
                $scope.mParam.TimeVal1 = tThru;

                var tThru2 = new Date("Wed Jan 18 2017 " + row.TimeVal2 + " GMT+0700 (SE Asia Standard Time)");
                tThru2 = tThru2.toISOString();
                $scope.mParam.TimeVal2 = tThru2;
            }, 200);


        }
        // $scope.onShowDetail = function(row){
        //     console.log("row detail",row);
        //     var tThru = new Date("Wed Jan 18 2017 "+row.TimeVal1+" GMT+0700 (SE Asia Standard Time)");
        //     // tThru = tThru.toISOString();
        //     $scope.mParam.TimeVal1 = tThru;

        //     var tThru2 = new Date("Wed Jan 18 2017 "+row.TimeVal2+" GMT+0700 (SE Asia Standard Time)");
        //     // tThru2 = tThru2.toISOString();
        //     $scope.mParam.TimeVal2 = tThru2;
        // }

        // function roleFlattenAndSetLevel(node, lvl) {
        //   for (var i = 0; i < node.length; i++) {
        //     node[i].$$treeLevel = lvl;
        //     gridData.push(node[i]);
        //     if (node[i].child.length > 0) {
        //       roleFlattenAndSetLevel(node[i].child, lvl + 1)
        //     } else {
        //
        //     }
        //   }
        //   return gridData;
        // }
        // $scope.selectRole = function(rows) {
        //   console.log("onSelectRows=>", rows);
        //   $timeout(function() {
        //     $scope.$broadcast('show-errors-check-validity');
        //   });
        // }
        // $scope.onSelectRows = function(rows) {
        //     console.log("onSelectRows=>", rows);
        //   }
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            columnDefs: [{
                headerName: 'Param Id',
                field: 'ParamId',
                visible: false
            },
            {
                headerName: 'Param Parent Id',
                field: 'ParamParentId',
                width: '7%',
                visible: false
            },
            {
                headerName: 'Param Key',
                field: 'ParamKey',
                visible: false
            },
            {
                headerName: 'Description',
                field: 'Description'
            },
            {
                headerName: 'Type',
                field: 'Type',
                visible: false
            },
            {
                headerName: 'Options',
                field: 'Options',
                visible: false
            },
            {
                headerName: 'StrVal',
                field: 'StrVal'
            },
            {
                headerName: 'NumVal1',
                field: 'NumVal1'
            },
            {
                headerName: 'NumVal2',
                field: 'NumVal2'
            },
            {
                headerName: 'TimeVal1',
                field: 'TimeVal1'
            },
            {
                headerName: 'TimeVal2',
                field: 'TimeVal2'
            }
            ],
            rowSelection: 'multiple',
            suppressRowClickSelection: true,
            suppressMovableColumns: true,
            enableColResize: true,
            enableSorting: true,
            animateRows: true,
            // rowHeight: 30,
            angularCompileRows: true,
            groupSelectsChildren: true,
            getNodeChildDetails: function (row) {
                if (row.child) {
                    return {
                        group: row.child && row.child.length > 0,
                        field: "Description",
                        children: row.child,
                        key: row.Description,
                        expanded: row.open
                    };
                } else {
                    return null;
                }
            },

        };
    });