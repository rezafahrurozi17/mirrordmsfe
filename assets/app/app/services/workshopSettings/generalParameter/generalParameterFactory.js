angular.module('app')
  .factory('GeneralParameter', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    // console.log(currentUser);
    return {
      getData: function() {
        var res = $http.get('/api/as/GeneralParameters');
        //console.log('res=>',res);
        //res.data.Result = null;
        return res;
      },
      getDataDetail: function(id) {
        var res = $http.get('/api/as/GeneralParameters/' + id);
        //console.log('res=>',res);
        //res.data.Result = null;
        return res;
      },
      getDataCompanyGroupDealer: function() {
        var res = $http.get('/api/as/GeneralParameter/CompanyGroupDealer');
        //console.log('res=>',res);
        //res.data.Result = null;
        return res;
      },
      update: function(data) {
        console.log("updaaate==>", data);
        return $http.put('/api/as/GeneralParameters', [{
          ParamId: data.ParamId,
          StrVal: data.StrVal,
          NumVal1: data.NumVal1,
          NumVal2: data.NumVal2,
          TimeVal1: data.TimeValFlag1,
          TimeVal2: data.TimeValFlag2
        }]);
      }
    }
  });
