angular.module('app')
    .controller('workingShiftController', function($scope, $http, CurrentUser, WorkingShift,$timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mShift = {}; //Model
    $scope.xShift = {};
    $scope.xShift.selected=[];

    var dateFormat='dd/MM/yyyy';
    var dateFilter='date:"dd/MM/yyyy"';
    //----------------------------------
    // Get Data
    //----------------------------------
    var gridData = [];
    // var det = new Date("October 13, 2014 11:13:00");
    // var res=[{
    //       StaffId:1,
    //       FullName:"John Doe",
    //       CodeName:"John",
    //       DoB:det,
    //       Nip:"10000",
    //       ShiftByte: 1
    //     },
    //     {
    //       StaffId:2,
    //       FullName:"Robert Doe",
    //       CodeName:"Robert",
    //       DoB:det,
    //       Nip:"10000",
    //       ShiftByte: 4
    //     }];
    // var shiftB=[
    //     {id:1,shiftByte:1,name:"Pagi"},
    //     {id:2,shiftByte:2,name:"Siang"},
    //     {id:3,shiftByte:4,name:"Sore"},
    //     {id:4,shiftByte:8,name:"Malam"},
    //     {id:5,shiftByte:16,name:"Subuh"}
    //     ];
    $scope.ismeridian = false;
    // $scope.mytime = new Date();
    $scope.hstep = 1;
    $scope.mstep = 1;
    $scope.toggleMode = function() {
        $scope.ismeridian = ! $scope.ismeridian;
    };
    // $scope.timeFrom = function (dt) {
    //    $scope.TimeThruOptions.min = dt; 
    // };
    // $scope.timeThru = function(dt){
    //     $scope.TimeFromOptions.max = dt;
    // }

    $scope.getData = function() {
        gridData = []
        // $scope.grid.data = res;
        WorkingShift.getData()
        .then(
            function(res){
                gridData = [];
                gridData=res.data.Result;
                //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
                $scope.grid.data = gridData ;
                console.log("role=>",res);
                $scope.loading=false;
            },
            function(err){
                // console.log("err=>",err);
            }
        );
    }
    $scope.DateOptions = {
        startingDay: 1,
        format: dateFormat,
        //disableWeekend: 1
    };
    $scope.fFrom = function(){
    var d = new Date($scope.mShift.TimeThru);
    d.getHours();
    d.getMinutes();
    $scope.maxFrom = d;
    if($scope.mShift.TimeFrom > $scope.mShift.TimeThru){
            $scope.mShift.TimeFrom =  $scope.mShift.TimeThru;
        }
    }
    $scope.fThrough =function(){
        var z = new Date($scope.mShift.TimeFrom);
        z.getHours();
        z.getMinutes();
        $scope.minThru = z;
        if($scope.mShift.TimeThru < $scope.mShift.TimeFrom){
            $scope.mShift.TimeThru = $scope.mShift.TimeFrom;
        }
    }
   
    $scope.onValidateSave = function(){
        var dt = new Date('"'+$scope.mShift.TimeFrom+'"');
        var dtt = new Date('"'+$scope.mShift.TimeThru+'"');
        dt.setSeconds(0);
        dtt.setSeconds(0);
        var dtFrom = dt.toTimeString();
        var dtThru = dtt.toTimeString();
        dtFrom = dtFrom.split(' ');
        dtThru = dtThru.split(' ');
        $scope.date = dtFrom[0];
        $scope.mShift.TimeFrom = dtFrom[0];
        $scope.mShift.TimeThru = dtThru[0];
        
        var a = $scope.mShift.ShiftByte;
        var b = $scope.grid.data.length;
        var awal = 1;
        var dataGrid = $scope.grid.data;
        for(var i=0;i<b; i++){
            if($scope.grid.data[i].ShiftByte == null){
                awal = 1; 
            }else{
            dataGrid.sort(function (a, b) {
              return a.ShiftByte - b.ShiftByte;
            });
             awal = dataGrid[i].ShiftByte;
                for(var j=0; j<1; j++){
                    var hasil = awal + awal;
                    awal = hasil;
                }
            }
            console.log("Panjang ==",dataGrid);
        }
        $scope.mShift.ShiftByte = awal;
        console.log("mShift ShiftByte==",$scope.mShift);
        console.log("length grid==",b);
        console.log("harusnya isi shiftByte==",awal);
        return true;

    }
    //  $scope.onBeforeEdit = function(row,mode){
    //     console.log("===>",$scope.mShift.TimeFrom);
    //     // var d = new Date("Wed Jan 18 2017 "+row.TimeFrom+" GMT+0700 (SE Asia Standard Time)");
    //     var tFrom = new Date("Wed Jan 18 2017 "+$scope.mShift.TimeFrom+" GMT+0700 (SE Asia Standard Time)");
    //     tFrom = tFrom.toISOString();
    //     var tThru = new Date("Wed Jan 18 2017 "+$scope.mShift.TimeThru+" GMT+0700 (SE Asia Standard Time)");
    //     tThru = tThru.toISOString();
    //     // var ConvDate = new Dae("2017-01-18T23:24:52.966Z");
    //     $scope.mShift.TimeFrom = tFrom;
    //     $scope.mShift.TimeThru = tThru;
        
    // }
    $scope.onShowDetail = function(row,mode){
        var workingShift = angular.copy(row);
        $scope.mShift=workingShift;
        if(mode == 'view'){
            $scope.dThru = true;
            $scope.dFrom = true;
        }else{
            $scope.dThru = false;
            $scope.dFrom = false;
        }
        console.log("===>",$scope.mShift.TimeFrom);
        // var d = new Date("Wed Jan 18 2017 "+row.TimeFrom+" GMT+0700 (SE Asia Standard Time)");
        // var tFrom = new Date("Wed Jan 18 2017 "+$scope.mShift.TimeFrom+" GMT+0700 (SE Asia Standard Time)");
        // var tThru = new Date("Wed Jan 18 2017 "+$scope.mShift.TimeThru+" GMT+0700 (SE Asia Standard Time)");
        var tFrom = new Date("Wed Jan 18 2017 "+row.TimeFrom+" GMT+0700 (SE Asia Standard Time)");
        var tThru = new Date("Wed Jan 18 2017 "+row.TimeThru+" GMT+0700 (SE Asia Standard Time)");
        // var ConvDate = new Date("2017-01-18T23:24:52.966Z");
        $scope.mShift.TimeFrom = tFrom;
        $scope.mShift.TimeThru = tThru;
        // $scope.dThru = true;
        // $scope.dFrom = true;
        console.log("Ini bukan");
    }
    $scope.onBeforeNewMode = function(row,mode){
        $scope.dThru = false;
        $scope.dFrom = false;
    }

    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        //showTreeExpandNoChildren: true,
        // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
        // paginationPageSize: 15,
        columnDefs: [
            { name:'id',    field:'Id', width:'7%', visible:false },
            { name:'Shift Byte',    field:'ShiftByte', width:'7%', visible:false },
            { name:'Name', field:'Name' },
            { name:'Time From', field:'TimeFrom' },
            { name:'Time Through', field:'TimeThru' },
        ]
    };
});
