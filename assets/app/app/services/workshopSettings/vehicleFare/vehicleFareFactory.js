angular.module('app')
  .factory('VehicleFare', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    console.log(currentUser);
    return {
      getData: function() {
        // var res=$http.get('/api/as/Vehiclefare');
        var res=$http.get('/api/as/GetVehicleFares');
        //console.log('res=>',res);
        //res.data.Result = null;
        return res;
      },
      create: function(mData) {
        return $http.post('/api/as/PostVehicleFare', [{
          VehicleFareId: 0,
          VehicleModelId: mData.VehicleModelId,
          ServiceRate: mData.ServiceRate,
          WarrantyRate: mData.WarrantyRate,
          ValidFrom: mData.ValidFrom,
          ValidTo: mData.ValidTo,
          StatusCode: 1
        }]);
      },
      update: function(mData){
        // if(mData.Fleg == 1){ 
        //   return $http.put('/api/as/PutVehicleFare', [{
        //                                       VehicleModelId: mData.VehicleModelId,
        //                                       FRFare: mData.FRFare}]);
        // }else if(mData.Fleg == 0){
        //   return $http.post('/api/as/PostVehicleFare', [{
        //                                     VehicleModelId: mData.VehicleModelId,
        //                                     FRFare: mData.FRFare}]);
        // }
        return $http.put('/api/as/PutVehicleFare', [{
          VehicleFareId: mData.VehicleFareId,
          VehicleModelId: mData.VehicleModelId,
          ServiceRate: mData.ServiceRate,
          WarrantyRate: mData.WarrantyRate,
          ValidFrom: mData.ValidFrom,
          ValidTo: mData.ValidTo,
          StatusCode: 1
        }]);
      },
      delete: function(VehicleFareId) {
        return $http.put('/api/as/DeleteVehiclefares',VehicleFareId);
      },
      getVehicleMList: function() {
        return $http.get('/api/crm/GetCVehicleModel/');
    },
    upload: function(data) {
      return $http.put('/api/as/VehicleFares/UploadData',data);
    },
    }
  });
