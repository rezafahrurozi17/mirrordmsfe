angular.module('app')
  .controller('vehicleFareController', function ($scope, $http, $filter, bsNotify, CurrentUser, Role, VehicleFare, MrsList, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function () {
      $scope.loading = true;
      $scope.gridData = [];
      $scope.formApi = {}
      $scope.getModelV();
      // $scope.getAllDrop();
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mData = null; //Model
    $scope.xRole = {
      selected: []
    };

    $scope.dateOptions = {
      startingDay: 1,
      format: 'dd/MM/yyyy',
    };
    $scope.formApi = {}


    //----------------------------------
    // Get Data
    //----------------------------------
    var VehicleModel = [];

    //Get Data Model VehaicleModel
    // $scope.getDrop = function(){
    //   $scope.VM = [];
    //     MrsList.getData()
    //       .then(
    //         function(res) {
    //           $scope.loading = false;
    //           // var temp = [];
    //           // for(var i = 0; i<res.data.length; i++){
    //           //   if(res.data[i].FRFare == null || res.data[i].FRFare <= 0){
    //           //     temp.push(res.data[i]);
    //           //   }
    //           // }

    //           // console.log('tempppp',temp);
    //           console.log("mrslist",res);
    //           $scope.VM = res.data.Result; 
    //         },
    //         function(err) {
    //           console.log("err=>", err);
    //         }
    //       );
    // }

    // $scope.getAllDrop = function(){
    //   MrsList.getData()
    //       .then(

    //         function(res) {
    //           VehicleModel = [];
    //           //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
    //           $scope.VM = res.data.Result;
    //           console.log("VehicleModel=>", res.data);
    //           //console.log("grid data=>",$scope.grid.data);
    //           //$scope.roleData = res.data;
    //           $scope.loading = false;
    //           console.log("VehicleModelRes",res);
    //           console.log("nih", $scope.VM );
    //         },
    //         function(err) {
    //           console.log("err=>", err);
    //         }
    //       );
    // }

    // }

    

    // $scope.stringToInt = function(){
    //   var DataVehicle = $scope.mData.FRFare;
    //   $scope.mData.FRFare =  parseInt(DataVehicle);
    //   // $scope.mData.Mode = $scope.mData.FRFare;
    // }

    function roleFlattenAndSetLevel(node, lvl) {
      for (var i = 0; i < node.length; i++) {
        node[i].$$treeLevel = lvl;
        gridData.push(node[i]);
        if (node[i].child.length > 0) {
          roleFlattenAndSetLevel(node[i].child, lvl + 1)
        } else {

        }
      }
      return gridData;
    }

    $scope.selectRole = function (rows) {
      console.log("selectRole", rows);
      $timeout(function () {
        $scope.$broadcast('show-errors-check-validity');
      });
    }

    //==================================================================================================
    $scope.getModelV = function() {
      VehicleFare.getVehicleMList().then(function(resuM) {
          console.log('getVehicleMList', resuM.data.Result);
          $scope.VehicM = resuM.data.Result;
      })
    }

    $scope.selectVehicleModel = function(){
      // console.log("nih", $scope.mData.VehicleModel );
    }

    $scope.onShowDetail = function (data,mode) {
      console.log('show detail', mode)
      $scope.disModel = false
      if (mode == 'edit'){
        $scope.disModel = true
      } else {
        $scope.disModel = false
      }
    }

    $scope.onBeforeNewMode = function () {
      // $scope.getDrop();
      console.log('onBeforeNewMode')
    }
    $scope.onAfterNewMode = function () {
      // $scope.getAllDrop();
      console.log('onAfterNewMode')
    }

    $scope.onValidateSave = function(data, mode) {
      if (data.ValidFrom.getTime() > data.ValidTo.getTime()){
        bsNotify.show({
            size: 'big',
            type: 'danger',
            title: "Tanggal Berlaku Awal Harus Lebih Kecil Dari Tanggal Akhir",
        });
        return false
      }

      if (mode == 'create'){
        var duplicate = 0;
        for (var i=0; i<$scope.grid.data.length; i++){
          if($scope.grid.data[i].VehicleModelId == data.VehicleModelId){
            duplicate++;
          }
        }
        if (duplicate > 0){
          bsNotify.show({
              size: 'big',
              type: 'danger',
              title: "Model Kendaraan Sudah Ada",
          });
          return false
        } else {
          return true;
        }
      } else {
        return true
      }

    }
    $scope.doCustomSave = function(data, mode){
      console.log('data mdata',data)
      console.log('mode',mode)
      if (mode == 'create'){
        
        data.ValidFrom = $filter('date')(data.ValidFrom, 'yyyy-MM-dd');
        data.ValidTo = $filter('date')(data.ValidTo, 'yyyy-MM-dd');
        VehicleFare.create(data).then(function(res){
          bsNotify.show({
              size: 'small',
              type: 'success',
              title: "Simpan Data Berhasil",
          });
          $scope.getData();

        })
        
      } else {
        data.ValidFrom = $filter('date')(data.ValidFrom, 'yyyy-MM-dd');
        data.ValidTo = $filter('date')(data.ValidTo, 'yyyy-MM-dd');
        VehicleFare.update(data).then(function(res){
          bsNotify.show({
              size: 'small',
              type: 'success',
              title: "Ubah Data Berhasil",
          });
          $scope.getData();

        })
      }
    }

    var gridData = [];
    $scope.getData = function () {

      // console.log("nih", $scope.VM );
      VehicleFare.getData().then(function (res) {
        console.log(res);
        gridData = [];
        //$scope.grid.data = roleFlattenAndSetLevel(angular.copy(res.data.result),0);
        // console.log("role=>", res.data.Result);
        //console.log("grid data=>",$scope.grid.data);
        //$scope.roleData = res.data;
        var dataX = res.data.Result;
        for (var i=0; i<dataX.length; i++){
          dataX[i].xValidFrom = $scope.ubahDateToString(dataX[i].ValidFrom)
          dataX[i].xValidTo = $scope.ubahDateToString(dataX[i].ValidTo)
        }
        $scope.grid.data = dataX;

        $scope.loading = false;
        console.log("test", res);
      },
        function (err) {
          console.log("err=>", err);
        }
      );
    }

    $scope.onSelectRows = function (rows) {
      console.log("onSelectRows=>", rows);
    }

    $scope.actionButtonSettingsDetail = [
      {
        func: function (row, formScope) {
          $scope.VehicleFare_Upload_Clicked()
        },
        title: 'Upload Dari Excel',
        icon: '',
      },
      {
        func: function (row, formScope) {
          $scope.VehicleFare_Download_Clicked();
        },
        title: 'Download Template',
        icon: '',
      }
    ];

    $scope.VehicleFare_Download_Clicked = function () {
      var excelData = [];
      var fileName = "Vehicle_Fare_Template";

      var excelSheetFormat = {
        "VehicleModel": "CHAR", 
        "ServiceRate": "Numerik",
        "WarrantyRate": "Numerik", 
        "StartPeriod": "Date('YYYY/MM/DD)", 
        "EndPeriod": "Date('YYYY/MM/DD)"
      };
      var excelSheetExample = {
        "VehicleModel": "AVANZA", 
        "ServiceRate": "123456",
        "WarrantyRate": "123456", 
        "StartPeriod": "'2020/01/31", 
        "EndPeriod": "'2020/01/31"
      };
      
      var excelSheetSeparator = {
        "VehicleModel": "------------------------------------------------", 
        "ServiceRate": "ISI",
        "WarrantyRate": "DI BAWAH", 
        "StartPeriod": "BARIS INI",
        "EndPeriod": "------------------------------------------------",
      };
      var excelData = [];
      excelData.push(excelSheetFormat);
      excelData.push(excelSheetExample);
      excelData.push(excelSheetSeparator);
      //excelData.push(excelSheet3);
      console.log("excel data", excelData);


      XLSXInterface.writeToXLSX(excelData, fileName);

    }

    $scope.VehicleFare_Upload_Clicked = function(){
      angular.element( document.querySelector( '#UploadVehicleFare' ) ).val(null);
      angular.element('#ModalUploadVehicleFare').modal('show');
    }

  $scope.loadXLSVehicleFare = function(ExcelFile){
    var myEl = angular.element( document.querySelector( '#UploadVehicleFare' ) ); //ambil elemen dari dokumen yang di-upload


    XLSXInterface.loadToJson(myEl[0].files[0], function(json){
        $scope.ExcelData = json;
        console.log("ExcelData : ", $scope.ExcelData);
        console.log("json", json);
            });
  }

    $scope.UploadVehicleFare = function(){
      var eachData = {};
      var inputData = [];
      var counter = 0;
      console.log('excel Data', $scope.ExcelData);

  
      angular.forEach($scope.ExcelData, function(value, key){
        if (counter>2) {
          console.log('value', value);
          console.log('key', key);

          var id = 0;
          angular.forEach($scope.VehicM, function(val, k){
            if (val.VehicleModelName.toUpperCase() == value.VehicleModel.toUpperCase()){
              id = val.VehicleModelId
            }
          })

          eachData = {
            "VehicleFareId" : 0,
            "VehicleModelId" : id,
            "VehicleModelName" : value.VehicleModel.toUpperCase(),
            "ServiceRate" : value.ServiceRate,
            "WarrantyRate" : value.WarrantyRate,
            "ValidFrom" : value.StartPeriod,
            "ValidTo" : value.EndPeriod,
            "StatusCode": 1
          }
          inputData.push(eachData);
        }
        counter = counter +1;
      });
      console.log('cek data upload',inputData);

      //cek kl ada typo
      var daftarTypo = '';
      angular.forEach(inputData, function(v, k){
        if (v.VehicleModelId == 0){
          daftarTypo = daftarTypo + v.VehicleModelName + ', ';
        }
      })

      if (daftarTypo != ''){
        bsNotify.show({
            size: 'big',
            type: 'danger',
            title: "Model Kendaraan " + daftarTypo + " Tidak Ada Di List / Penulisan Tidak Sesuai",
        });
        angular.element('#ModalUploadVehicleFare').modal('hide');
        angular.element( document.querySelector( '#UploadVehicleFare' ) ).val(null);
        return false
      }
  
      //SAVE UPLOAD DATA FROM EXCEL TO BACKEND
      VehicleFare.upload(inputData).then(function (res) {
  
        //AFTER DATA SAVED, THEN HIDE MODAL UPLOAD
        angular.element('#ModalUploadVehicleFare').modal('hide');
        angular.element( document.querySelector( '#UploadVehicleFare' ) ).val(null);
  
        //POP UP SUCCES
        bsNotify.show({
          size: 'small',
          type: 'success',
          title: "Success",
          content: "Data Berhasil Di Upload"
        });
        $scope.getData()
  
        
      })
    }


    $scope.ubahDateToString = function(val) {
      var pecahData = val.toString().split(' ');
      // console.log('pecah data', pecahData);
      // Jan:0, Feb:0, Mar:0, Apr:0, May:0, Jun:0, Jul:0, Aug:0, Sep:0, Oct:0, Nov:0, Dec:0
      if (pecahData[1] == 'Jan'){
          pecahData[1] = '01'
      } else if (pecahData[1] == 'Feb'){
          pecahData[1] = '02'
      } else if (pecahData[1] == 'Mar'){
          pecahData[1] = '03'
      } else if (pecahData[1] == 'Apr'){
          pecahData[1] = '04'
      } else if (pecahData[1] == 'May'){
          pecahData[1] = '05'
      } else if (pecahData[1] == 'Jun'){
          pecahData[1] = '06'
      } else if (pecahData[1] == 'Jul'){
          pecahData[1] = '07'
      } else if (pecahData[1] == 'Aug'){
          pecahData[1] = '08'
      } else if (pecahData[1] == 'Sep'){
          pecahData[1] = '09'
      } else if (pecahData[1] == 'Oct'){
          pecahData[1] = '10'
      } else if (pecahData[1] == 'Nov'){
          pecahData[1] = '11'
      } else if (pecahData[1] == 'Dec'){
          pecahData[1] = '12'
      } else {
          pecahData[1] = '01'
      }

      var hasilFormat = pecahData[2] + '/' + pecahData[1] + '/' + pecahData[3]
      return hasilFormat;
  }

    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
      enableSorting: true,
      enableRowSelection: true,
      multiSelect: true,
      enableSelectAll: true,
      //showTreeExpandNoChildren: true,
      // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
      // paginationPageSize: 15,
      columnDefs: [
        {
          name: 'VehicleModelId',
          field: 'VehicleModelId',
          width: '7%',
          visible: false
        },
        {
          name: 'Model Kendaraan',
          field: 'VehicleModel.VehicleModelName'
        },
        {
          name: 'Service Rate',
          field: 'ServiceRate'
        },
        {
          name: 'Warranty Rate',
          field: 'WarrantyRate'
        },
        {
          name: 'Berlaku Dari',
          field: 'xValidFrom'
        },
        {
          name: 'Berlaku Sampai',
          field: 'xValidTo'
        },
      ]
    };
  });
