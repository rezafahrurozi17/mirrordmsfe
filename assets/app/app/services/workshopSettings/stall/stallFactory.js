angular.module('app')
  .factory('Stall', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    // console.log(currentUser);
    return {
      getData: function() {
        var res=$http.get('/api/as/Stall');
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      create: function(data) {
        console.log(' tambah data=>', data);

        return $http.post('/api/as/Stall', [{
                                            StallParentId: (data.StallParentId==null?0:data.StallParentId),
                                            Name: data.Name,
                                            Type: 0}]);
      },
      update: function(data){
        console.log('rubah data=>', data);
        return $http.put('/api/as/Stall', [{
                                            StallId: data.StallId,
                                            StallParentId: (data.StallParentId==null?0:data.StallParentId),
                                            Name: data.Name,
                                            Type: 0}]);
      },
      delete: function(id) {
        console.log("delete id==>",id);
        return $http.delete('/api/as/Stall',{data:id,headers: {'Content-Type': 'application/json'}});
      }
    }
  });
