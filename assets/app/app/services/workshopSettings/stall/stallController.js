angular.module('app')
  .controller('StallController', function($scope, $http, CurrentUser, bsTransTree, Stall, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
      $scope.loading = true;
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mData = {}; //Model
    $scope.xRole = {
      selected: []
    };

    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.gridDataTree=[];
    $scope.getData = function() {
      Stall.getData().then(function(res) {
          var gridData = res.data.Result;
          gridData = bsTransTree.createTree(gridData, 'StallId', 'StallParentId');
          $scope.gridDataTree = angular.copy(gridData);
          $scope.grid.data = bsTransTree.createUIGridTreeData(gridData);
          console.log("gridata=>",gridData);
          $scope.loading = false;
        },
        function(err) {
          console.log("err=>", err);
        }
      );
    }

    // $scope.selectRole = function(rows) {
    //   console.log("onSelectRows=>", rows);
    //   $timeout(function() {
    //     $scope.$broadcast('show-errors-check-validity');
    //   });
    // }

    $scope.onSelectRows = function(rows) {
        console.log("onSelectRows=>", rows);
      }

    //----------------------------------
    // Grid Setup
    //----------------------------------

    $scope.grid = {
      enableSorting: true,
      enableRowSelection: true,
      multiSelect: true,
      enableSelectAll: true,
      //showTreeExpandNoChildren: true,
      // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
      // paginationPageSize: 15,
      columnDefs: [{
          name: 'Stall Id',
          field: 'StallId',
          width: '7%'
        },
        {
          name: 'Stall Parent Id',
          field: 'StallParentId',
          width: '7%'
        },
        {
          name: 'Name',
          field: 'Name'
        }
      ]
    };

    // Stall.getData().then(
    //   function(res) {
    //     // $scope.parentData = res.data.Result;
    //     var temp = res.data.Result;
    //     $scope.parentData = [];
    //     var obj = {};
    //     for (var i in temp) {
    //       if (!obj[temp[i].StallId]) {
    //         obj[temp[i].StallId] = {};
    //         obj[temp[i].StallId]["Name"] = temp[i].Name;
    //         obj[temp[i].StallId]["StallId"] = temp[i].StallId;
    //         $scope.parentData.push(obj[temp[i].StallId]);
    //       }
    //     }

    //     $scope.loading = false;
    //     return res.data;
    //   }
    // );
    // $scope.onBeforeEdit = function() {
    //   var json = $scope.grid.data;
    //   var xjson = []
    //   for (var i = 0; i < json.length; i++) {
    //     if (json[i]["StallId"] !== $scope.mData.StallId) {
    //       xjson.push({
    //         'StallId': json[i].StallId,
    //         'Name': json[i].Name
    //       });
    //     }
    //   }
    //   $scope.parentData = xjson;
    // }
  });
