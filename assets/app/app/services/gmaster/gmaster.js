angular.module('app')
    .controller('GeneralMasterController', function($scope, $http, CurrentUser, GeneralMaster, $timeout,bsAlert) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = true;
            $scope.gridData = [];

            setInterval(function() {
                $scope.bolehTambahUbah();
            }, 200);
        });

        $scope.bolehTambahUbah = function() {
            // var validateNew = [1009,2027,2023,2024,2025,2026,2021,2022,2013,2016,2017,2018,2038,2039]; // untuk yang ga boleh add
            // var validateNew = [
            //     //1008,
            //     //1009,
            //     // 2027,
            //     1014
            //     ,2013
            //     ,2016
            //     ,2017
            //     ,2018
            //     ,2021
            //     ,2022
            //     ,2023
            //     ,2024
            //     ,2025
            //     ,2026
            //     ,2035
            //     ,2038
            //     ,2039
            //     ,1005
            // ];
            var validateNew = [   // =========== new from pa cien ===============
                1,	    //Unit of Measurement
                2,	    //Workshop Flow Process
                1003,	//Uncontactable Reason
                1004,	//Contactable Reason
                1005,	//Reminder Type
                1006,	//Payor
                1007,	//Wo Category
                1008,	//Paid By
                1009,	//Pause Clock Reason
                1010,	//Task Category
                1012,	//Wo Catg for BP
                1013,	//Reminder Media
                1014,	//Transaction Type
                2013,	//ResProb Market Impact
                2016,	//ResProb Technical Rank
                2017,	//ResProb Transmission Type
                2018,	//ResProb Engine Type
                2019,	//ResProb Status Fail Part
                2020,	//Gate button
                2021,	//Kategori Komponen
                2022,	//Komponen
                2023,	//T1
                2024,	//T2
                2025,	//Kategori Problem
                2026,	//Sub Kategori Problem
                2028,	//Kode Transaksi Faktur Pajak
                2030,	//WAC Car Type
                2031,	//MRS Invitation Status
                2035,	//status satelite center
                2036,	//Metode Perbaikan
                2037,	//Status Kerusakan
                2038,	//Status Towing
                2039,	//Kategori WO BP
                2040,	//Tingkat Kerusakan
            ]
            // if(!$scope.filter.MasterCategoryId || $scope.filter.MasterCategoryId == 1 || $scope.filter.MasterCategoryId == 2 || $scope.filter.MasterCategoryId == 3){
            if (!$scope.filter.MasterCategoryId || validateNew.indexOf($scope.filter.MasterCategoryId) != -1) {
                //ga bisa
                $scope.bolehTambah = true;
            } else {
                $scope.bolehTambah = false;
            }

        }

        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mGeneralMaster = {}; //Model
        $scope.xGeneralMaster = {};
        $scope.xGeneralMaster.selected = [];
        $scope.masterCategories = [];
        $scope.filter = { MasterCategoryId: 0 };
        var tempCat = 0;
        //----------------------------------
        // Get Data
        //----------------------------------
        GeneralMaster.getCategories().then(function(res) {
            $scope.masterCategories = res.data.Result;
            $scope.loading = false;
        });
        $scope.getData = function() {
            console.log("model", $scope.filter.MasterCategoryId);
            tempCat = angular.copy($scope.filter.MasterCategoryId);
            if ($scope.filter.MasterCategoryId > 0) {
                GeneralMaster.getData($scope.filter.MasterCategoryId).then(function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        return res.data.Result;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            }

        }

        $scope.selectCategory = function(){
            $scope.bolehTambahUbah();
        }

        $scope.doCustomSave = function(model, mode) {
            var newModel = angular.copy(model);
            console.log("model, mode=>", model, mode);
            newModel.MasterCategoryId = $scope.filter.MasterCategoryId;
            console.log("newModel", newModel);
            if (mode == 'create') {
                model.MasterId = 0
                GeneralMaster.CekExistingGeneralMaster(model.MasterId, model.Name)
                    .then(
                        function (res) {
                            if (res.data[0] == 'true') {
                                GeneralMaster.create(newModel).then(function(a) {
                                    bsAlert.alert({
                                        title: "Sukses",
                                        text: "Data berhasil di Disimpan",
                                        type: "success",
                                        showCancelButton: false
                                    });
                                    $scope.getData();
                                })
                            } else {
                                bsAlert.warning("Nama Sudah Terdaftar")
                            }
                        });
            } else {
                GeneralMaster.CekExistingGeneralMaster(model.MasterId, model.Name)
                    .then(
                        function (res) {
                            if (res.data[0] == 'true') {
                                GeneralMaster.update(newModel).then(function(a) {
                                    bsAlert.alert({
                                        title: "Sukses",
                                        text: "Data berhasil di Disimpan",
                                        type: "success",
                                        showCancelButton: false
                                    });
                                    $scope.getData();
                                })
                            } else {
                                bsAlert.warning("Nama Sudah Terdaftar")
                            }
                        });
                
            }

        };
        //----------------------------------
        // Grid Setup
        //----------------------------------
        var actionTemp = '<div>' +
            '<button class="ui icon inverted grey button"' +
            'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
            'onclick="this.blur()"' +
            'ng-click="grid.appScope.gridClickViewDetailHandler(row.entity)">' +
            '<i class="fa fa-fw fa-lg fa-list-alt"></i>' +
            '</button>' +
            '<button class="ui icon inverted grey button"' +
            'style="font-size:1em;padding:0.5em;font-weight:400;box-shadow:none!important;color:#777;margin:1px 1px 0px 2px"' +
            'onclick="this.blur()"' +
            'ng-click="grid.appScope.gridClickEditHandler(row.entity);" ng-show="row.entity.FlagEdit == 1">' +
            '<i class="fa fa-fw fa-lg fa-pencil"></i>' +
            '</button>' +
            '</div>';

        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            columnDefs: [
                { name: 'MasterId', field: 'MasterId', visible: false },
                { name: 'MasterCategoryId', field: 'MasterCategoryId', visible: false },
                { name: 'Nama', field: 'Name' },
                {
                    name: 'action',
                    allowCellFocus: false,
                    width: 250,
                    pinnedRight: true,
                    enableColumnMenu: false,
                    enableSorting: false,
                    enableColumnResizing: false,
                    cellTemplate: actionTemp
                }
            ]
        };

    });