angular.module('app')
  .factory('GeneralMaster', function($http) {
    return {
      getCategories: function() {
        var res=$http.get('/api/as/GlobalMaster/Categories');
        return res;
      },
      getData: function(catId) {
        var res=$http.get('/api/as/GlobalMaster?CategoryId='+catId);
        return res;
      },
      create: function(obj) {
        console.log(obj);
        return $http.post('/api/as/GlobalMaster', [obj]);
      },
      update: function(obj){
        console.log(obj);
        return $http.put('/api/as/GlobalMaster', [obj]);
      },
      delete: function(arrId) {
        return $http.delete('/api/as/GlobalMaster/DeleteSubCategory', {data:arrId,headers: {'Content-Type': 'application/json'}});
      },

      CekExistingGeneralMaster: function(MasterId, Name) {
        var url = '/api/as/GlobalMaster/CekGeneralMaster/?MasterId=' + MasterId + '&Name=' + Name ;
        var res = $http.get(url);
        return res;
    }
    }
  });