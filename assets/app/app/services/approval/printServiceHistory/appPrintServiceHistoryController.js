angular.module('app')
  .controller('AppPrintServiceHistoryController', function($scope, $state, $http, CurrentUser,  bsNotify, ngDialog, CAppPrintServiceHistory, Role, OrgChart, $timeout, Auth) {
    // $scope, $http, $sce,$filter,$timeout,$window,CurrentUser, Service, Role, ServiceRights, OrgChart, Auth

    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {

            $scope.loading = false;
                    // $scope.getOrgData();
                    // $scope.getRoleData();
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mAction={show:false};

    $scope.modalMode='new';

//     //                   };
    $scope.approvalData = []; //OrgChart Collection
//     // Backend Operation
    // var bin = [1,2,4,8,16,32,64];
    $scope.getData = function(){
      console.log("get Data");
      CAppPrintServiceHistory.getPrintHistoryRequest(1).then(function(result) {
          console.log("getPrintHistoryRequest=>",result.data.Result);
          $scope.grid.data = result.data.Result;

          // console.log("getPrintHistoryRequest=>",result.data);
        });

    }
    $scope.updateData = function(){
      
      CAppPrintServiceHistory.updatePrintHistoryRequest(mAction).then(function(result) {
          // $scope.grid.data = result.data.Result;
          CAppPrintServiceHistory.getPrintHistoryRequest().then(function(result) {
            $scope.grid.data = result.data.Result;

            console.log("getPrintHistoryRequest=>",result.data);
          });
        });

    }
    $scope.buttonSettingModal = {save:{text:'Reject'}};
    $scope.onReject = function() {
      console.log("===>",$scope.mPrintServiceHistory);
      $scope.mAction.show=false;
    }
    $scope.customBulkSettings = [
        {
          func: function() {
            // console.log("func_openall=>",$scope);
            // merubah di bsFromGrid, dibagia custom bulk menambah kiriman parameter selected rows
            console.log("select ==>",xdata);
            // console.log("api=>",$scope.formApi);
            CAppPrintServiceHistory.updateData(xdata).then(
                function(res){
                    console.log("sukses start transfer==>",res);
                    $scope.getData();
                }
            );
          },
          title: 'Approve'
        }
    ];

    // $scope.getOrgData = function(){
    //   console.log("user==>",$scope.user);
    //     CAccessMatrix.getDealOut($scope.user).then(function(result) {
    //       $scope.orgData = result.data.Result;
    //       console.log("orgData=>",result.data);
    //       // $scope.ctlOrg.expand_all();
    //     });
    // }

    $scope.onSelectRows = function(rows){
        console.log("onSelectRows=>",rows);
    }

    
      // var btnActionAccesTemplate = '<bsradiobox ng-model="row.entity.FlagState"'+
      //                                         'options="[{text:\'Basic\',value:1},{text:\'Advanced\',value:2}]"'+
      //                                         'layout="H"'+
      //                                         '>'+
      //                             '</bsradiobox>';
      // var btnActionApproveTemplate = 
      var btnActionTemplate = '<button type="button" class="btn btn-success"'+
                                    // ' onclick="this.blur()"'+
                                    ' ng-click="grid.appScope.$parent.gridClickApprove(row.entity,grid.renderContainers.body.visibleRowCache.indexOf(row))">'+
                                    
                                    'Approve'+
                                '</button>'+
                                '<button class="btn btn-danger"'+
                                    // ' onclick="this.blur()"'+
                                    ' ng-click="grid.appScope.$parent.gridClickReject(row.entity,grid.renderContainers.body.visibleRowCache.indexOf(row))">'+
                                    
                                    'Reject'+
                                '</button>';
                                // var btnActionRejectTemplate = 

      $scope.gridClickApprove = function(row,idx){
      console.log(idx,"click==>",row);
      bsNotify.show(
            {
                title: "Approval",
                content: "Sales Order has been successfully approved.",
                type: 'success'
            }
          );
        // $scope.grid.
        // $scope.mAction.show=true;
        
      
      }
      $scope.gridClickReject = function(row,idx){
      console.log(idx,"click==>",row);
      $scope.mPrintServiceHistory=angular.copy(row);
      $scope.mAction.show=true;
      bsNotify.show(
            {
                title: "Approval",
                content: "Sales Order has been successfully approved.",
                type: 'success'
            }
          );
      // $scope.grid.
        
      
      }
    $scope.cd = 89;
    if ($scope.cd == 89) {
        $scope.grid = {
        enableGridMenu: true,
        enableSorting: true,
        // enableRowSelection: true,
        // multiSelect : false,
        // enableFullRowSelection: true,
        // enableRowHeaderSelection: false,
        // enableFiltering:true,
        // enableSelectAll : false,
    // grid.appScope.
        columnDefs: [
            {name: 'Tanggal',field: 'LastModifiedDate', width: '10%'}, // , cellTemplate:'<button ng-click="toyotaIdLink(row.entity)">{{row.entity.toyotaId}}</button>'}
            {name: 'Permintaan',field: 'RequestorRoleId', width: '20%'},
            {name: 'No Polisi',field: 'LicensePlate', width: '10%'},
            {name: 'VIN',field: 'VIN', width: '10%'},
            {name: 'Type',field: 'VIN', width: '20%'},
            // {name: 'Kategori',field: 'key'},
            { name:'Action', allowCellFocus: false, width:'30%', pinnedRight:true,
                                       enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
                                       cellTemplate: btnActionTemplate
              },
            // { name:'Advanced', allowCellFocus: false, width:100, pinnedRight:true,
            //                            enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
            //                            cellTemplate: btnActionEditTemplate
            //   },
             //, cellTemplate:'<a ng-click="grid.appScope.rangkaLink(row.entity)">{{row.entity.noRank}}</a>' 
        // };{{row.entity}}
        ],
        onRegisterApi : function(gridApi) {
            // set gridApi on $scope
            $scope.gridApi = gridApi;

            $scope.gridApi.selection.on.rowSelectionChanged($scope,function(row) {
                  $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                  // console.log("selected=>",$scope.selectedRows);
                  if($scope.onSelectRows){
                      $scope.onSelectRows($scope.selectedRows);
                  }
            });
            $scope.gridApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
                  $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                  if($scope.onSelectRows){
                      $scope.onSelectRows($scope.selectedRows);
                  }
            });
        }
      }
    } else {
      $scope.grid = {
        enableGridMenu: true,
        enableSorting: true,
        // enableRowSelection: true,
        // multiSelect : false,
        // enableFullRowSelection: true,
        // enableRowHeaderSelection: false,
        // enableFiltering:true,
        // enableSelectAll : false,
    // grid.appScope.
        columnDefs: [
            {name: 'No Polisi',field: 'LicensePlate', width: '10%'},
            {name: 'Permintaan',field: 'RequestorRoleId', width: '20%'},
            {name: 'Tanggal',field: 'LastModifiedDate', width: '10%'}, // , cellTemplate:'<button ng-click="toyotaIdLink(row.entity)">{{row.entity.toyotaId}}</button>'}
            {name: 'VIN',field: 'VIN', width: '10%'},
            {name: 'Type',field: 'VIN', width: '20%'},
            // {name: 'Kategori',field: 'key'},
            { name:'Action', allowCellFocus: false, width:'30%', pinnedRight:true,
                                       enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
                                       cellTemplate: btnActionTemplate
              },
            // { name:'Advanced', allowCellFocus: false, width:100, pinnedRight:true,
            //                            enableColumnMenu:false,enableSorting: false,enableColumnResizing: false,
            //                            cellTemplate: btnActionEditTemplate
            //   },
             //, cellTemplate:'<a ng-click="grid.appScope.rangkaLink(row.entity)">{{row.entity.noRank}}</a>' 
        // };{{row.entity}}
        ],
        onRegisterApi : function(gridApi) {
            // set gridApi on $scope
            $scope.gridApi = gridApi;

            $scope.gridApi.selection.on.rowSelectionChanged($scope,function(row) {
                  $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                  // console.log("selected=>",$scope.selectedRows);
                  if($scope.onSelectRows){
                      $scope.onSelectRows($scope.selectedRows);
                  }
            });
            $scope.gridApi.selection.on.rowSelectionChangedBatch($scope,function(row) {
                  $scope.selectedRows = $scope.gridApi.selection.getSelectedRows();
                  if($scope.onSelectRows){
                      $scope.onSelectRows($scope.selectedRows);
                  }
            });
        }
      }
    }
    




});