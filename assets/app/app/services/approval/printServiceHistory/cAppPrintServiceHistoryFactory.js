angular.module('app')
  .factory('CAppPrintServiceHistory', function($http, CurrentUser,$q) {
    var currentUser = CurrentUser.user;
    // console.log(currentUser);
    return {
      getPrintHistoryRequest: function(isGr) {
        var res=$http.get('/api/as/GetRequestedApprovalPrintService/'+isGr);
      
        // console.log('hasil=>',res);
        //res.data.Result = null;
        return res;
      },
      
      checkRequestPrintHistory: function(data) {

        console.log("request",data);
        // var res=$http.get('/api/ct/PostCCustomerAccessTab/',data);
        
        return $http.put('/api/as/CheckingApprovalPrintServiceHistory', data);
      },
      insertRequestPrintHistory: function(data) {

        console.log("request",data);

        // var res=$http.get('/api/ct/PostCCustomerAccessTab/',data);
        
        return $http.post('/api/as/PostApprovalRequestPrintServiceHistory', data);
        
      },

      updatePrintHistoryRequest: function(data) {

        console.log("data update",data);
        // var res=$http.get('/api/ct/PostCCustomerAccessTab/',data);
        
        // return $http.put('/api/as/ApprovalPrintServiceHistory_TR', data);
        return $http.put('/api/as/approv/UpdateStatusApprovalServiceHistory/'+data.TransactionId+'/'+data.StatusApprovalId, []);
      },
    
      
    }
  });


