angular.module('app')
    .factory('EstimasiFactory', function($http, CurrentUser, $q) {
        var currentUser = CurrentUser.user();
        console.log(currentUser);
        return {
            getData: function(item, isGr) {
                // item = item.uppercase();
                var res = $http.get('/api/as/jobs/getListEstimationbyPoliceNumberRev/PoliceNumber/' + item + '?isGr=' + isGr);
                return res;
            },
            getDataByDate: function(first, last, isGr) {
                var res = $http.get('/api/as/jobs/getListEstimationbyRangeDate/firstDay/' + first + '/lastDay/' + last + '/IsGr/' + isGr);
                return res;
            },
            // ================================
            getPayment: function() {
                var catId = 1008;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getUnitMeasurement: function() {
                var catId = 1;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getWoCategory: function() {
                var catId = 1007;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getVehicleTypeById: function(id) {
                var url = '/api/crm/GetCVehicleTypeById/' + id
                var res = $http.get(url);
                return res;
            },
            getTaskCategory: function() {
                var catId = 1010;
                var res = $http.get('/api/as/GlobalMaster?CategoryId=' + catId);
                console.log('resnya pause=>', res);
                return res;
            },
            getWobyJobId: function(filter) {
                var res = $http.get('/api/as/Jobs/' + filter);
                return res;
            },
            getLocation: function() {
                var res = $http.get('/api/crm/GetLocation/');
                return res
            },
            create: function(data) {
                return $q.resolve();
            },
            createEstimation: function(data, jamserver) {
                console.log("data di factory", data, 'jam', jamserver);
                return $http.put('/api/as/Jobs/updateDetil/3?createNo=1', [{
                    JobId: 0,
                    JobNo: 0,
                    OutletId: 655,
                    CalId: 1,
                    TechnicianAction: 1,
                    JobSuggest: 1,
                    JobType: 0,
                    Status: 0,
                    JobTask: data.JobTask,
                    JobComplaint: data.JobComplaint,
                    JobRequest: data.JobRequest,
                    AppointmentNo: null,
                    NewAppointmentRel: null,
                    isAppointment: 0,
                    isGr: 1,
                    isCash: 0,
                    isSpk: 0,
                    SpkNo: "-",
                    Km: data.KmFinal,
                    InsuranceName: "-",
                    isWOBase: 1,
                    AppointmentVia: 0,
                    Process: null,
                    WoNo: 0,
                    ModelCode: data.ModelTypeCode,
                    ContactPerson: data.ContactPerson,
                    AddressForEstimation: data.AddressForEstimation,
                    PhoneContactPerson1: data.PhoneContactPerson1,
                    Email: data.Email,
                    AssemblyYearForEstimation: data.AssemblyYearForEstimation,
                    WoCategoryId: data.WoCategoryId,
                    IsEstimation: 1,
                    PoliceNumber: data.LicensePlate,
                    VIN: data.VIN,
                    ModelType: data.VehicleModelName,
                    KatashikiCode: data.KatashikiCode,
                    // EstimationDate: data.EstimationDate,
                    EstimationDate: jamserver,

                    VehicleTypeId: data.VehicleTypeId,
                    PlanDateStart: data.PlanDateStart,
                    PlanDateFinish: data.PlanDateFinish,
                    StatusPreDiagnose: data.StatusPreDiagnose,
                    EstimationNo: "",
                }]);
            },
            updateEstimation: function(data) {
                console.log("data di factory", data);
                return $http.put('/api/as/Jobs/updateDetil/3', [{
                    JobId: data.JobId,
                    JobNo: 0,
                    OutletId: data.OutletId,
                    CalId: 1,
                    TechnicianAction: 1,
                    JobSuggest: 1,
                    JobType: 0,
                    Status: 0,
                    JobTask: data.JobTask,
                    JobComplaint: data.JobComplaint,
                    JobRequest: data.JobRequest,
                    AppointmentNo: null,
                    NewAppointmentRel: null,
                    EstimationNo: data.EstimationNo,
                    isAppointment: 0,
                    isGr: 1,
                    isCash: 0,

                    isSpk: 0,
                    SpkNo: "-",
                    Km: data.KmFinal,
                    InsuranceName: "-",
                    isWOBase: 1,
                    AppointmentVia: 0,
                    Process: null,
                    WoNo: 0,
                    ModelCode: data.ModelTypeCode,
                    ContactPerson: data.ContactPerson,
                    AddressForEstimation: data.AddressForEstimation,
                    PhoneContactPerson1: data.PhoneContactPerson1,
                    Email: data.Email,
                    AssemblyYearForEstimation: data.AssemblyYearForEstimation,
                    WoCategoryId: data.WoCategoryId,
                    IsEstimation: 1,
                    PoliceNumber: data.LicensePlate,
                    VIN: data.VIN,
                    ModelType: data.VehicleModelName,
                    KatashikiCode: data.KatashikiCode,
                    EstimationDate: data.EstimationDate,
                    PlanDateStart: data.PlanDateStart,
                    PlanDateFinish: data.PlanDateFinish,
                    StatusPreDiagnose: data.StatusPreDiagnose,
                    VehicleTypeId: data.VehicleTypeId,
                }]);
            },
            // create: function(data) {
            //   console.log("data di factory",data);
            //   return $http.post('/api/as/Jobs/postEstimation', [{
            //                                       JobId: 0,
            //                                       JobNo: 0,
            //                                       OutletId:655,
            //                                       CalId: 1,
            //                                       TechnicianAction: 1,
            //                                       JobSuggest: 1,
            //                                       JobType: 0,
            //                                       Status: 0,
            //                                       JobTask: data.JobTask,
            //                                       JobComplaint: data.JobComplaint,
            //                                       JobRequest: data.JobRequest,
            //                                       AppointmentNo: null,
            //                                       NewAppointmentRel:null,
            //                                       isAppointment: 0,
            //                                       isGr: 1,
            //                                       isCash: 0,
            //                                       isSpk: 0,
            //                                       SpkNo:"-",
            //                                       Km: data.Km,
            //                                       InsuranceName: "-",
            //                                       isWOBase: 1,
            //                                       AppointmentVia:0,
            //                                       Process: null,
            //                                       WoNo:0,
            //                                       AssemblyYearForEstimation:data.AssemblyYearForEstimation,
            //                                       WoCategoryId:data.WoCategoryId,
            //                                       IsEstimation: 1,
            //                                       PoliceNumber:data.LicensePlate,
            //                                       ModelType:data.VehicleModelName,
            //                                       KatashikiCode:data.KatashikiCode,
            //                                       EstimationDate:data.EstimationDate,
            //                                       VehicleTypeId:data.VehicleTypeId,
            //                                       EstimationNo:"",
            //                                       }]);
            // },
            update: function(role) {
                return $http.put('/api/fw/Role', [{
                    Id: role.Id,
                    //pid: role.pid,
                    Name: role.Name,
                    Description: role.Description
                }]);
            },
            delete: function(id) {
                    return $http.delete('/api/fw/Role', { data: id, headers: { 'Content-Type': 'application/json' } });
                },
            getJamServer: function() {
                var res = $http.get('/api/as/GetDateTimeServer/');
                return res;
            },
            getDataVIN: function(vin){
                var res = $http.get('/api/as/MasterVehicle_FPCFLCPLC/' + vin)
                return res;
            },
            listHistoryClaim: function(vin){
                var res = $http.get('/api/as/ToWass/ListHistoryClaim?VIN=' + vin)
                return res;
            }         
                // getClaim: function(jobId) {
                //   return $http.get('/api/as/SPKClaim/'+jobId);
                // },
                // createClaim: function(data) {
                //   return $http.post('/api/as/SPKClaim', data);
                // },
                // updateClaim: function(data) {
                //   return $http.put('/api/as/SPKClaim', data);
                // }
        }
    });