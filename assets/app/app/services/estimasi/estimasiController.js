angular.module('app')
    .controller('estimasiController', function($scope, $q, $filter, PreDiagnose, AsbGrFactory, FollowUpGrService, PrintRpt, $http, MrsList, bsAlert, CMaster, CurrentUser, EstimasiFactory, $timeout, bsNotify, ASPricingEngine, AppointmentGrService, RepairSupportActivityGR, AppointmentBpService, WO, PrintRpt) {
        //----------------------------------
        // Start-Up
        //----------------------------------

        var PPNPerc = 0;
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
            $scope.getPPN()
        });
        //----------------------------------
        // Initialization
        //----------------------------------

        $scope.getPPN = function (noNpwp) {
            if (noNpwp !== undefined && noNpwp !== null && noNpwp !== ''){
                noNpwp = noNpwp.toString();
                if (noNpwp.length === 20){
                    if (noNpwp !== '00.000.000.0-000.000'){
                        ASPricingEngine.getPPN(1, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    } else {
                        ASPricingEngine.getPPN(0, 3).then(function (res) {
                            PPNPerc = res.data
                        });
                    }
                } else {
                    ASPricingEngine.getPPN(0, 3).then(function (res) {
                        PPNPerc = res.data
                    });
                }

            } else {
                // param 1 adalah isnpwp, param 2 tipe ppn
                ASPricingEngine.getPPN(0, 3).then(function (res) {
                    PPNPerc = res.data
                });
            }
            
        };

        $scope.boardName = 'asbGrEstimasi';
        $scope.mData = {}; //Model
        $scope.mData.JobComplaint = [];
        $scope.mData.JobTask = [];
        $scope.mData.JobRequest = [];
        $scope.user = CurrentUser.user();
        $scope.stallAsb = 0;
        $scope.disT1 = true;
        var currentDate = new Date();
        $scope.minDateASB = new Date(currentDate);
        $scope.maxDateASB = new Date(currentDate.setDate(currentDate.getDate() + 1));
        console.log("$scope.user=========================>", $scope.user);
        $scope.xEstimate = {};
        $scope.filter = {};
        $scope.xEstimate.selected = [];
        $scope.selectedModel = {};
        $scope.selectedType = {};
        var dateFormat = 'dd/MM/yyyy';
        var dateFilter = 'date:"dd/MM/yyyy"';
        var lastIdx = 2;
        var Parts = [];
        // =======================
        var gridTemp = [];
        $scope.listApi = {};
        $scope.formApi = {};
        var tmpPrediagnose = {};
        $scope.listSelectedRows = [];
        $scope.lmModel = {};
        $scope.ldModel = {};
        $scope.lmRequest = {};
        $scope.paramAsb = 0;
        $scope.ComplaintCategory = [
            { ComplaintCatg: 'Body' },
            { ComplaintCatg: 'Body Electrical' },
            { ComplaintCatg: 'Brake' },
            { ComplaintCatg: 'Chassis' },
            { ComplaintCatg: 'Drive Train' },
            { ComplaintCatg: 'Engine' },
            { ComplaintCatg: 'Heater System & AC' },
            { ComplaintCatg: 'Restraint' },
            { ComplaintCatg: 'Steering' },
            { ComplaintCatg: 'Suspension and Axle' },
            { ComplaintCatg: 'Transmission' },
        ]
        $scope.JobRequest = [];
        $scope.JobComplaint = [];
        $scope.jobAvailable = false;
        $scope.partsAvailable = false;
        $scope.FlatRateAvailable = false;
        $scope.PriceAvailable = false;
        // $scope.asb = { showMode: 'main', nopol: "D 1234 ABC", tipe: "Avanza" }
        // $scope.asb.endDate = new Date();
        // $scope.asb.endDate.setDate($scope.asb.endDate.getDate() + 7);
        $scope.listButtonSettings = { new: { enable: true, icon: "fa fa-fw fa-car" } };
        $scope.listCustomButtonSettings = {
            enable: true,
            icon: "fa fa-fw fa-car",
            func: function(row) {
                console.log("customButton", row);
            }
        };
        $scope.typeMData = [{ Id: 1, Name: 1 },
            { Id: 2, Name: 2 },
            { Id: 3, Name: 3 }
        ];
        $scope.typeFilter = [{ Id: 1, Name: "No. Polisi" },
            { Id: 2, Name: "Periode" }
        ];
        $scope.materialCategory = [{ MaterialTypeId: 1, Name: "Spare Parts" }, { MaterialTypeId: 2, Name: "Bahan" }];
        $scope.totalDp = 0;
        $scope.totalWork = 0;
        // $scope.tmpServiceRate = {};
        $scope.tmpServiceRate = null;
        $scope.totalMaterial = 0;
        $scope.totalEstimasi = 0;
        $scope.totalPPN = 0;
        $scope.openPlottingStall = false;

        // ======================Board======================
        var getDateEstimasi = function(data) {
            console.log('onplot data...', data);
            if ((data.startTime === null) || (data.endTime === null)) {
                // jika startTime/EndTime null berarti belum ada plot, Data tanggal awal/akhir serta Tanggal Appointment harus dikosongkan maka harus dikosongkan
                if ((data.PrediagScheduledTime !== null && data.PrediagStallId !== 0)) {
                    var dt = new Date(data.PrediagScheduledTime);
                    var dtt = new Date(data.PrediagScheduledTime);
                    dt.setSeconds(0);
                    dtt.setSeconds(0);
                    var timeStart = dt.toTimeString();
                    var timeFinish = dtt.toTimeString();
                    timeStart = timeStart.split(' ');
                    var finaltimeFinish = timeFinish.split(' ');
                    finaltimeFinish = finaltimeFinish[0].split(':');
                    finaltimeFinish = (parseInt(finaltimeFinish[0]) + 1).toString() + ":" + finaltimeFinish[1] + ":" + finaltimeFinish[2]
                        // =============================
                    var hourFinish = dtt.getHours();
                    var minuteFinish = dtt.getMinutes();
                    var minuteFinishplus = minuteFinish + 30;
                    var hourFinishplus = hourFinish + 1;
                    if (minuteFinishplus > 59) {
                        hourFinishplus = hourFinish + 1;
                        minuteFinishplus = minuteFinishplus - 60;
                    }
                    if (dtt.getHours() == 12) {
                        hourFinishplus = hourFinish + 1;
                    }
                    var finaltimeFinishDelivery = (hourFinishplus > 9 ? hourFinishplus : "0" + hourFinishplus) + ":" + minuteFinishplus + ":00";
                    console.log("timeStart", timeStart);
                    console.log("finaltimeFinish", finaltimeFinish);
                    console.log("finaltimeFinishDelivery", finaltimeFinishDelivery);
                    // ===============================
                    var yearFirst = dt.getFullYear();
                    var monthFirst = dt.getMonth() + 1;
                    var dayFirst = dt.getDate();
                    var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                    // ===============================
                    var yearFinish = dtt.getFullYear();
                    var monthFinish = dtt.getMonth() + 1;
                    var dayFinish = dtt.getDate();
                    var FinishDate = yearFinish + '-' + monthFinish + '-' + dayFinish;
                    // ===============================
                    // if(timeStart[0] !== null && timeFinish[0] !== null){
                    $scope.mData.StallId = data.PrediagStallId;
                    $scope.mData.FullDate = dt;
                    $scope.mData.PlanStart = timeStart[0];
                    $scope.mData.PlanFinish = finaltimeFinish;
                    $scope.mData.PlanDateStart = firstDate;
                    $scope.mData.AppointmentDate = firstDate;
                    $scope.mData.AppointmentTime = timeStart[0];
                    $scope.mData.TargetDateAppointment = FinishDate;
                    $scope.mData.FixedDeliveryTime1 = finaltimeFinishDelivery;
                    $scope.mData.PlanDateFinish = FinishDate;
                    $scope.mData.StatusPreDiagnose = data.StatusPreDiagnose;
                    $scope.mData.PrediagScheduledTime = data.PrediagScheduledTime;
                    $scope.mData.PrediagStallId = data.PrediagStallId;
                } else {
                    $scope.mData.StallId = 0;
                    $scope.mData.FullDate = null;
                    $scope.mData.PlanStart = null;
                    $scope.mData.PlanFinish = null;
                    $scope.mData.PlanDateStart = null;
                    //$scope.mData.AppointmentDate = null; 
                    delete $scope.mData.AppointmentDate;
                    //$scope.mData.AppointmentTime = null;
                    delete $scope.mData.AppointmentTime;
                    $scope.mData.TargetDateAppointment = null;
                    $scope.mData.FixedDeliveryTime1 = null;
                    $scope.mData.PlanDateFinish = null;
                    $scope.mData.StatusPreDiagnose = 0;
                    delete $scope.mData.PrediagScheduledTime;
                    delete $scope.mData.PrediagStallId;
                }
                // delete tmpPrediagnose.ScheduledTime;
                // delete tmpPrediagnose.StallId;
            } else {
                if ((data.PrediagStallId !== undefined && data.PrediagScheduledTime !== null) && $scope.gridWork.length == 0) {
                    var dt = new Date(data.PrediagScheduledTime);
                    var dtt = new Date(data.PrediagScheduledTime);
                    dt.setSeconds(0);
                    dtt.setSeconds(0);
                    var timeStart = dt.toTimeString();
                    var timeFinish = dtt.toTimeString();
                    timeStart = timeStart.split(' ');
                    var finaltimeFinish = timeFinish.split(' ');
                    finaltimeFinish = finaltimeFinish[0].split(':')
                    finaltimeFinish = (parseInt(finaltimeFinish[0]) + 1).toString() + ":" + finaltimeFinish[1] + ":" + finaltimeFinish[2];
                    console.log("timeStart", timeStart);
                    console.log("timeFinish", finaltimeFinish);
                    // =====
                    var hourFinish = dtt.getHours();
                    var minuteFinish = dtt.getMinutes();
                    var minuteFinishplus = minuteFinish + 30;
                    var hourFinishplus = hourFinish + 1;
                    if (minuteFinishplus > 59) {
                        hourFinishplus = hourFinish + 1;
                        minuteFinishplus = minuteFinishplus - 60;
                    }
                    if (dtt.getHours() == 12) {
                        hourFinishplus = hourFinish + 1;
                    }
                    var finaltimeFinishDelivery = (hourFinishplus > 9 ? hourFinishplus : "0" + hourFinishplus) + ":" + minuteFinishplus + ":00";
                    // ===============================
                    var yearFirst = dt.getFullYear();
                    var monthFirst = dt.getMonth() + 1;
                    var dayFirst = dt.getDate();
                    var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                    firstDate = $scope.changeFormatDate(dt);
                    // ===============================
                    var yearFinish = dtt.getFullYear();
                    var monthFinish = dtt.getMonth() + 1;
                    var dayFinish = dtt.getDate();
                    var FinishDate = yearFinish + '-' + monthFinish + '-' + dayFinish;
                    FinishDate = $scope.changeFormatDate(dtt);
                    // ===============================
                    // if(timeStart[0] !== null && timeFinish[0] !== null){
                    $scope.mData.StallId = data.PrediagStallId;
                    $scope.mData.FullDate = dt;
                    $scope.mData.PlanStart = timeStart[0];
                    $scope.mData.PlanFinish = finaltimeFinish;
                    $scope.mData.PlanDateStart = firstDate;
                    $scope.mData.AppointmentDate = firstDate;
                    $scope.mData.AppointmentTime = timeStart[0];
                    $scope.mData.TargetDateAppointment = FinishDate;
                    $scope.mData.FixedDeliveryTime1 = finaltimeFinishDelivery;
                    $scope.mData.PlanDateFinish = FinishDate;
                    $scope.mData.StatusPreDiagnose = data.StatusPreDiagnose;
                    $scope.mData.PrediagScheduledTime = data.PrediagScheduledTime;
                    tmpPrediagnose.ScheduledTime = data.PrediagScheduledTime;
                    $scope.mData.PrediagStallId = data.PrediagStallId;
                } else {
                    console.log("plotting stall");
                    var dt = new Date(data.startTime);
                    var dtt = new Date(data.endTime);
                    console.log("date", dt);
                    dt.setSeconds(0);
                    dtt.setSeconds(0);
                    var timeStart = dt.toTimeString();
                    var timeFinish = dtt.toTimeString();
                    timeStart = timeStart.split(' ');
                    timeFinish = timeFinish.split(' ');

                    var hourFinish = dtt.getHours();
                    var minuteFinish = dtt.getMinutes();
                    var minuteFinishplus = minuteFinish + 30;
                    var hourFinishplus = hourFinish;
                    if (minuteFinishplus > 59) {
                        hourFinishplus = hourFinish + 1;
                        minuteFinishplus = minuteFinishplus - 60;
                    }
                    if (dtt.getHours() == 12) {
                        hourFinishplus = hourFinish + 1;
                    }
                    var finaltimeFinishDelivery = (hourFinishplus > 9 ? hourFinishplus : "0" + hourFinishplus) + ":" + minuteFinishplus + ":00";
                    // ===============================
                    var yearFirst = dt.getFullYear();
                    var monthFirst = dt.getMonth() + 1;
                    var dayFirst = dt.getDate();
                    var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                    firstDate = $scope.changeFormatDate(dt);
                    // ===============================
                    var yearFinish = dtt.getFullYear();
                    var monthFinish = dtt.getMonth() + 1;
                    var dayFinish = dtt.getDate();
                    var FinishDate = yearFinish + '-' + monthFinish + '-' + dayFinish;
                    FinishDate = $scope.changeFormatDate(dtt);
                    // ===============================
                    // if(timeStart[0] !== null && timeFinish[0] !== null){
                    $scope.mData.StallId = data.stallId;
                    $scope.mData.FullDate = dt;
                    $scope.mData.PlanStart = timeStart[0];
                    $scope.mData.PlanFinish = timeFinish[0];
                    $scope.mData.PlanDateStart = firstDate;
                    $scope.mData.AppointmentDate = firstDate;
                    $scope.mData.AppointmentTime = timeStart[0];
                    $scope.mData.TargetDateAppointment = FinishDate;
                    $scope.mData.FixedDeliveryTime1 = finaltimeFinishDelivery;
                    $scope.mData.PlanDateFinish = FinishDate;
                    $scope.mData.StatusPreDiagnose = data.StatusPreDiagnose;
                    $scope.mData.PrediagScheduledTime = data.PrediagScheduledTime;
                    $scope.mData.PrediagStallId = data.PrediagStallId;
                    tmpPrediagnose.ScheduledTime = data.PrediagScheduledTime;
                    // tmpPrediagnose.ScheduledTime = data.PrediagScheduledTime;
                    // tmpPrediagnose.StallId = data.PrediagStallId;
                }
            }

            // }

            // }
        };
        var showBoardAsbGr = function(item) {
            console.log("item item", item);
            var vtime
            var tmphour
            var tmpMinute
            if (item !== undefined && item !== null) {
                vtime = item;
                tmphour = item.getHours();
                tmpMinute = item.getMinutes();
            } else {
                vtime = new Date();
                tmphour = vtime.getHours();
                tmpMinute = vtime.getMinutes();
            }

            vtime.setHours(tmphour);
            vtime.setMinutes(tmpMinute);
            vtime.setSeconds(0);
            if ($scope.mData.JobId !== 0) {
                $scope.currentItem = {
                        mode: 'main',
                        currentDate: $scope.asb.startDate,
                        nopol: $scope.mData.LicensePlate,
                        tipe: $scope.mData.ModelType,
                        durasi: $scope.tmpActRate,
                        stallId: $scope.stallAsb,
                        startTime: vtime,
                        visible: 0,
                        jobId: $scope.mData.JobId,
                        preDiagnose: tmpPrediagnose,
                        asbStatus: $scope.asb,
                        prediagnoseOnly: 1
                            /*stallId :1,
                            startDate :,
                            endDate:,
                            durasi:,*/
                    }
                    // $scope.currentItem = {
                    //     mode: 'main',
                    //     currentDate: $scope.currentDate,
                    //     nopol: $scope.mDataDetail.LicensePlate,
                    //     tipe: $scope.mDataDetail.VehicleModelName,
                    //     durasi: $scope.tmpActRate,
                    //     stallId: $scope.stallAsb,
                    //     startTime: vtime,
                    //     visible: $scope.visibleAsb,
                    //     jobId: $scope.mData.JobId,
                    //     preDiagnose: tmpPrediagnose,
                    //     asbStatus: $scope.asb,
                    //     /*stallId :1,
                    //     startDate :,
                    //     endDate:,
                    //     durasi:,*/
                    // }
            } else {
                $scope.currentItem = {
                    mode: 'main',
                    currentDate: $scope.asb.startDate,
                    nopol: $scope.mData.LicensePlate,
                    tipe: $scope.mData.ModelType,
                    durasi: $scope.tmpActRate,
                    stallId: 0,
                    startTime: vtime,
                    visible: 0,
                    preDiagnose: tmpPrediagnose,
                    prediagnoseOnly: 1
                        // JobId: $scope.mData.JobId,
                        /*stallId :1,
                        startDate :,
                        endDate:,
                        durasi:,*/
                }
            }
            console.log('=============Showboard==============')
            console.log("actRe", $scope.tmpActRate);
            console.log("harusnya tanggal", $scope.asb.startDate);
            console.log("ini vtime", vtime);
            console.log("$scope.stallAsb", $scope.stallAsb);
            console.log("$scope.currentDate", $scope.currentDate);
            console.log("$scope.mData.JobId", $scope.mData.JobId);
            console.log('=============/Showboard/==============')
            $timeout(function() {
                AsbGrFactory.showBoard({ boardName: $scope.boardName, container: 'plottingStallEstimasi', currentChip: $scope.currentItem, onPlot: getDateEstimasi });

            }, 100);

        }
        $scope.reloadBoard = function(item) {
            var tmpMinDateASB = new Date();
            tmpMinDateASB.setHours(0, 0, 0);
            console.log("itemmm", item);
            if (item < tmpMinDateASB.getTime()) {
                console.log("ini nih bossss", tmpMinDateASB);
                console.log("item ini nih", item);
            } else {
                $scope.mData.PlanStart = null;
                $scope.mData.PlanFinish = null;
                $scope.mData.PlanDateStart = null;
                $scope.mData.AppointmentDate = null;
                $scope.mData.AppointmentTime = null;
                $scope.mData.TargetDateAppointment = null;
                $scope.mData.TimeTarget = null;
                $scope.mData.PlanDateFinish = null;
                console.log("dari ng-change", item);
                console.log("$scope.mData.FullDate", $scope.mData.FullDate);
                var vtime = item;
                var tmphour
                var tmpMinute
                if ($scope.mData.FullDate !== undefined) {
                    console.log("$scope.mData.FullDate", $scope.mData.FullDate);
                    var tmpTime = $scope.mData.FullDate;
                    tmphour = tmpTime.getHours();
                    tmpMinute = tmpTime.getMinutes();
                } else {
                    tmphour = 9;
                    tmpMinute = 0;
                };
                vtime.setHours(tmphour);
                vtime.setMinutes(tmpMinute);
                vtime.setSeconds(0);
                $scope.mData.LicensePlate = $scope.mData.LicensePlate ? $scope.mData.LicensePlate : "";
                $scope.mData.VehicleModelName = $scope.mData.VehicleModelName ? $scope.mData.VehicleModelName : "";
                console.log("$scope.mData.LicensePlate", $scope.mData.LicensePlate);
                console.log("$scope.mData.VehicleModelName", $scope.mData.VehicleModelName);
                $scope.currentItem = {
                    mode: 'main',
                    currentDate: $scope.asb.startDate,
                    nopol: $scope.mData.LicensePlate,
                    tipe: $scope.mData.VehicleModelName,
                    durasi: $scope.tmpActRate,
                    stallId: 0,
                    startTime: vtime,
                    visible: 0,
                    preDiagnose: tmpPrediagnose,
                    prediagnoseOnly: 1
                        // JobId: $scope.mData.JobId,
                        /*stallId :1,
                        startDate :,
                        endDate:,
                        durasi:,*/
                };
                console.log("actRe", $scope.tmpActRate);
                console.log("harusnya tanggal", $scope.asb.startDate);
                console.log("ini vtime", vtime);
                console.log("stallId", $scope.currentItem.stallId);
                $timeout(function() {
                    AsbGrFactory.showBoard({ boardName: $scope.boardName, container: 'plottingStall', currentChip: $scope.currentItem, onPlot: getDateEstimasi });

                }, 100);
            };
        };
        $scope.actionButtonSettingsDetail = [{
            title: 'Print',
            icon: 'fa fa-fw fa-print',
            func: function() {
                console.log("detail action edit=>", $scope);
                $scope.mDataCopy = angular.copy($scope.mData);
                setEditMode(true);
                // $scope.actionButtonSettingsDetail[1].enable=true;
                // $scope.actionButtonSettingsDetail[2].enable=true;
                // $scope.actionButtonSettingsDetail[3].enable=false;
                $scope.backToMain = false;
            }
        }, ];
        var setEditMode = function(editing) {
                // console.log("editing==>",editing);
                $scope.actionButtonSettingsDetail[0].visible = !editing; //back
                $scope.actionButtonSettingsDetail[1].visible = editing; //save
                $scope.actionButtonSettingsDetail[2].visible = editing; //cancel
                $scope.actionButtonSettingsDetail[3].visible = !editing;
                // $scope.actionButtonSettingsDetail[4].visible=!editing;
                // $scope.actionButtonSettingsDetail[5].visible=!editing;
                $scope.show.disableOtherTab = editing;
                $scope.formApi.setFormReadOnly(!editing);
            }
            // ========================
        $scope.estimateAsbParam = function(param) {
            if ($scope.paramAsb == 0) {
                $scope.estimateAsb();
                $scope.paramAsb = 1;
            }
        };
        //----------------------------------
        // Get Data
        //----------------------------------
        var det = new Date("October 13, 2014 11:13:00");
        $scope.selectModel = function(selected) {
            var tmpSelected = angular.copy(selected);

            $scope.mData.KatashikiCodex = ''
            $scope.mData.VehicleTypeId = ''
            $scope.KatashikiData = []
            $scope.typeData = []


            

            console.log("selectModel selected : ", selected);
            $scope.mData.VehicleModelName = selected.VehicleModelName;
            // MrsList.getDataTypeByModel(selected.VehicleModelId).then(function(res) {
            WO.getVehicleTypeById(selected.VehicleModelId).then(function(res) {
                console.log("result : ", res);
                //$scope.typeData = res.data;
                // $scope.typeData = _.uniq(res.data.Result, 'KatashikiCode');

                $scope.KatashikiData = _.uniq(res.data.Result, 'KatashikiCode');

                for (var i=0; i<$scope.KatashikiData.length; i++){
                    $scope.KatashikiData[i].NewDescription = $scope.mData.VehicleModelName + ' - ' + $scope.KatashikiData[i].KatashikiCode
                }

            });
            if (tmpSelected.VehicleModelName == $scope.mData.ModelType) {

            } else {
                $scope.mData.VehicleTypeId = null
                $scope.mData.KatashikiCode = null
            }

        };

        $scope.selectKatashiki = function(selected) {
            $scope.mData.VehicleTypeId = ''
            $scope.typeData = []
            $scope.mData.KatashikiCode = selected.KatashikiCode;

            WO.GetCVehicleTypeListByKatashiki(selected.KatashikiCode).then(function(res) {

                // $scope.typeData = res.data.Result[0]
                $scope.typeData = res.data.Result


                for (var i=0; i<$scope.typeData.length; i++){
                    $scope.typeData[i].NewDescription = $scope.typeData[i].Description + ' - ' + $scope.typeData[i].KatashikiCode + ' - ' + $scope.typeData[i].SuffixCode
                }

            })

        }

        


        $scope.selectType = function(selected) {
            console.log("selected", selected);
            $scope.mData.KatashikiCode = selected.KatashikiCode;
            AppointmentGrService.getDataTaskListByKatashiki(selected.KatashikiCode).then(function(restask) {
                taskList = restask.data.Result;
                var tmpTask = {};
                // if (taskList.length > 0) {
                //     for (var i = 0; i < taskList.length; i++) {
                //         if (taskList[i].ServiceRate !== undefined) {
                //             var summary = taskList[i].ServiceRate;
                //             var summaryy = parseInt(summary);
                //             tmpTask.Summary = Math.round(summaryy);
                //             // row.Fare = taskList[i].AdjustmentServiceRate;
                //             tmpTask.Fare = taskList[i].ServiceRate;
                //             $scope.PriceAvailable = true;
                //         } else {
                //             tmpTask.Fare = taskList[i].ServiceRate;
                //             tmpTask.Summary = tmpTask.Fare;
                //         }
                //         $scope.tmpServiceRate = tmpTask;
                //     }
                // }
                if (taskList.length > 0) {
                    for (var i = 0; i < taskList.length; i++) {
                        if (taskList[i].ServiceRate !== undefined && taskList[i].WarrantyRate !== undefined) {
                            tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                            tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                            $scope.PriceAvailable = true;
                        } else {
                            tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                            tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                        }
                        $scope.tmpServiceRate = tmpTask;
                    }
                } else {
                    $scope.tmpServiceRate = null;
                }
            });
            $scope.mData.ModelTypeCode = selected.Description
            console.log("$scope.mData", $scope.mData);
        }
        $scope.onBeforeEdit = function(data, mode) {
                console.log("before edit", mode, data);
                // added by sss on 2017-09-25
                // $scope.modalMode = mode;
                $scope.mData = {};
                var Parts = [];
                Parts.splice();
                gridTemp = [];
                $scope.openPlottingStall = false;
                tmpPrediagnose = {};
                // =================
                $scope.asb = {};
                $scope.asb = { showMode: 'main' }
                $scope.asb.endDate = new Date();
                $scope.asb.endDate.setDate($scope.asb.endDate.getDate() + 7);
                // =================
                // $scope.estimateAsb();
                $scope.paramAsb = 0;
                $scope.JobComplaint = [];
                $scope.JobRequest = [];
                $scope.gridWork = gridTemp;
                $scope.totalWork = 0;
                $scope.totalMaterial = 0;
                $scope.totalEstimasi = 0;
                $scope.totalPPN = 0;
                $scope.totalDp = 0;
                $scope.mData.JobRequest = [];
                $scope.mData.JobComplaint = [];
                if (mode == 'edit' || mode == 'view') {
                    // ============================
                    // $scope.givePatternEdit(data.Km);

                    MrsList.getDataModel().then(function(res) {
                        console.log("result : ", res.data.Result);
                        $scope.modelData = res.data.Result;
                    });
                    if (data.JobId !== null) {
                        EstimasiFactory.getWobyJobId(data.JobId).then(function(res) {
                            console.log("resss", res);
                            var dataWo = res.data.Result[0]
                            console.log(dataWo);
                            $scope.mData = dataWo;

                            if($scope.mData.VIN != null && $scope.mData.VIN != undefined && $scope.mData.VIN != ""){
                                AppointmentGrService.getDataVIN($scope.mData.VIN).then(function(res){
                                    console.log('cek input rangka', $scope.mData.VIN)
                                    $scope.tempVIN = res.data.Result[0];
                                    var tempProgramName = $scope.tempVIN.ProgramName
                                    if ($scope.tempVIN.DEC_Date != null && $scope.tempVIN.DEC_Date != undefined) {
                                        $scope.tempVIN.DEC_Date = $filter('date')($scope.tempVIN.DEC_Date, 'dd/MM/yyyy')
                                    }else{
                                        $scope.tempVIN.DEC_Date = '-'
                                    }
                    
                                    if ($scope.tempVIN.ProgramName != null && $scope.tempVIN.ProgramName != undefined) {
                                        $scope.tempVIN.ProgramName = tempProgramName
                                    }else{
                                        $scope.tempVIN.ProgramName = '-'
                                    }
                                    console.log('cek res input', $scope.tempVIN)
                                })
                                AppointmentGrService.listHistoryClaim($scope.mData.VIN).then(function(res){
                                    $scope.listClaim = res.data.data;
                                    $scope.tempListClaim = [];
                                    console.log('cek list claim', res)
                                    console.log('list claim', res.data.data)
                                    console.log('list history claim', $scope.tempListClaim)
                                    for (var i = 0; i < $scope.listClaim.historyClaimDealerList.length; i++) {
                                        $scope.tempListClaim.push($scope.listClaim.historyClaimDealerList[i]);
                                    }
                                })
                            }else{
                                $scope.tempListClaim = [];
                                $scope.tempVIN = [];
                            }

                            console.log("$scope.mData onShowDetail", $scope.mData);

                            console.log("$scope.mData.Km", $scope.mData.Km);
                            $scope.mData.Km = String($scope.mData.Km);
                            $scope.mData.Km = $scope.mData.Km.replace(/\B(?=(\d{3})+(?!\d))/g, ',');

                            $scope.JobRequest = dataWo.JobRequest;
                            $scope.JobComplaint = dataWo.JobComplaint;
                            $scope.mData.LicensePlate = $scope.mData.PoliceNumber;
                            $scope.mData.VehicleModelId = dataWo.VehicleType.VehicleModelId;
                            $scope.mData.AssemblyYearForEstimation = parseInt(dataWo.AssemblyYearForEstimation);
                            var taskList = {};
                            AppointmentGrService.getDataTaskListByKatashiki($scope.mData.KatashikiCode).then(function(restask) {
                                taskList = restask.data.Result;
                                console.log("resTask", taskList);
                                var tmpTask = {};
                                if (taskList.length > 0) {
                                    for (var i = 0; i < taskList.length; i++) {
                                        if (taskList[i].ServiceRate !== undefined && taskList[i].WarrantyRate !== undefined) {
                                            // tmpTask.ServiceRate = Math.ceil(taskList[i].ServiceRate);
                                            // tmpTask.WarrantyRate = Math.ceil(taskList[i].WarrantyRate);
                                            tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                                            tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                                            $scope.PriceAvailable = true;
                                        } else {
                                            // tmpTask.ServiceRate = Math.ceil(taskList[i].ServiceRate);
                                            // tmpTask.WarrantyRate = Math.ceil(taskList[i].WarrantyRate);
                                            tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
                                            tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
                                        }
                                        $scope.tmpServiceRate = tmpTask;
                                    }
                                } else {
                                    $scope.tmpServiceRate = null;
                                }
                            });
                            // $scope.mData.VehicleTypeId = dataWo.VehicleType.VehicleTypeId;
                            // MrsList.getDataTypeByModel($scope.mData.VehicleModelId).then(function(res) {
                            WO.getVehicleTypeById($scope.mData.VehicleModelId).then(function(res) {
                                console.log("result : ", res);
                                //$scope.typeData = res.data;
                                // $scope.typeData = _.uniq(res.data.Result, 'KatashikiCode');

                                $scope.KatashikiData = _.uniq(res.data.Result, 'KatashikiCode');

                                for (var i=0; i<$scope.KatashikiData.length; i++){
                                    $scope.KatashikiData[i].NewDescription = $scope.mData.VehicleType.VehicleModel.VehicleModelName + ' - ' + $scope.KatashikiData[i].KatashikiCode
                                }

                                for (var i=0; i < $scope.KatashikiData.length; i++) {
                                    if ($scope.KatashikiData[i].KatashikiCode == dataWo.VehicleType.KatashikiCode) {
                                        $scope.mData.KatashikiCodex   = dataWo.VehicleType.KatashikiCode;
                                    }
                                }

                                if ($scope.mData.KatashikiCodex != null && $scope.mData.KatashikiCodex != undefined && $scope.mData.KatashikiCodex != '') {
                                    WO.GetCVehicleTypeListByKatashiki($scope.mData.KatashikiCode).then(function(res) {
                                        // $scope.typeData = res.data.Result[0]
                                        $scope.typeData = res.data.Result


                                        for (var i=0; i<$scope.typeData.length; i++){
                                            $scope.typeData[i].NewDescription = $scope.typeData[i].Description + ' - ' + $scope.typeData[i].KatashikiCode + ' - ' + $scope.typeData[i].SuffixCode
                                        }

                                        for (var z = 0; z < $scope.typeData.length; z++) {
                                            if($scope.typeData[z].VehicleTypeId == dataWo.VehicleType.VehicleTypeId){
                                                $scope.selectType($scope.typeData[z])
                                            }
                                        }
                                    })
                                }



                                // added by sss on 2017-09-25
                                // EstimasiFactory.getClaim(data.JobId).then(function(res){
                                //   console.log("get Claim..",res.data.Result);
                                //   if (res.data.Result.length>0) {
                                //     $scope.mClaim = angular.copy(res.data.Result[0]);
                                //   }
                                // });
                                // 
                            });
                            $scope.mData.VehicleTypeId = dataWo.VehicleType.VehicleTypeId;
                            console.log("$scope.mData.VehicleModelId", $scope.mData.VehicleModelId);
                            console.log("$scope.mData.VehicleType", $scope.mData.VehicleType);
                            $scope.dataForBslist(dataWo);
                            $scope.dataForPrediagnose(dataWo);
                            // $timeout($scope.sumAllPrice(),5000);
                        });
                    }
                    // $scope.mData = model;
                }
            }
            // $scope.onShowDetail = function(data, mode) {
            //   $scope.hideGridParts = true;
            //   $scope.JobComplaint = [];
            //   $scope.JobRequest = [];
            //   $scope.gridWork = [];
            //   if (data.JobId != null) {
            //     FollowUpGrService.getWobyJobId(data.JobId).then(function(res) {
            //       console.log("resss", res);
            //       var dataWo = res.data.Result[0]
            //       console.log(dataWo);
            //       $scope.mData = dataWo;
            //       $scope.JobRequest = dataWo.JobRequest;
            //       $scope.JobComplaint = dataWo.JobComplaint;
            //       $scope.mData.LicensePlate = $scope.mData.PoliceNumber;
            //       $scope.dataForBslist(dataWo);
            //       console.log("$scope.mData onShowDetail", $scope.mData);
            //     });
            //   }
            // };
            // $scope.dataForBslist = function(data) {
            //     gridTemp = []
            //     Parts.splice();
            //     Parts = [];
            //     if (data.JobTask.length > 0) {
            //         var tmpJob = data.JobTask;
            //         for (var i = 0; i < tmpJob.length; i++) {
            //             console.log("tmpJob", tmpJob);
            //             // 
            //             if (tmpJob[i].Fare !== undefined) {
            //                 var sum = tmpJob[i].Fare;
            //                 var summ = tmpJob[i].FlatRate;
            //                 var summaryy = sum * summ;
            //                 summaryy = parseInt(summaryy);
            //             }
            //             for (var j = 0; j < tmpJob[i].JobParts.length; j++) {
            //                 console.log("tmpJob[i].JobParts[j]",tmpJob[i].JobParts[j]);
            //                 if(tmpJob[i].JobParts[j].PaidBy !== null){
            //                     tmpJob[i].JobParts[j].paidName = tmpJob[i].JobParts[j].PaidBy.Name;
            //                     delete tmpJob[i].JobParts[j].PaidBy;
            //                 }
            //                 if(tmpJob[i].JobParts[j].Satuan !== null){
            //                     tmpJob[i].JobParts[j].satuanName = tmpJob[i].JobParts[j].Satuan.Name;
            //                     delete tmpJob[i].JobParts[j].Satuan;
            //                 }
            //                 tmpJob[i].subTotal = tmpJob[i].Qty * tmpJob[i].RetailPrice;
            //                 $scope.getAvailablePartsServiceManual(tmpJob[i].JobParts[j]);

        //                 // Parts.push(tmpJob[i].JobParts[j]);
        //             }
        //             gridTemp.push({
        //                 ActualRate: tmpJob[i].ActualRate,
        //                 JobTaskId: tmpJob[i].JobTaskId,
        //                 JobTypeId: tmpJob[i].JobTypeId,
        //                 catName: tmpJob[i].JobType.Name,
        //                 Fare: tmpJob[i].Fare,
        //                 Summary: summaryy,
        //                 PaidById: tmpJob[i].PaidById,
        //                 JobId: tmpJob[i].JobId,
        //                 TaskId: tmpJob[i].JobTaskId,
        //                 TaskName: tmpJob[i].TaskName,
        //                 FlatRate: tmpJob[i].FlatRate,
        //                 ProcessId: tmpJob[i].ProcessId,
        //                 TFirst1: tmpJob[i].TFirst1,
        //                 PaidBy: tmpJob[i].PaidBy.Name,
        //                 index:"$$"+i,
        //                 child: Parts
        //             });
        //         }
        //         $scope.gridWork = gridTemp;
        //         // $scope.onAfterSave();
        //         console.log("totalw", $scope.totalWork);
        //     }
        //     $scope.sumAllPrice();
        //     console.log("$scope.gridWork", $scope.gridWork);
        //     console.log("$scope.JobRequest", $scope.JobRequest);
        //     console.log("$scope.JobComplaint", $scope.JobComplaint);
        // };
        var Parts = [];
        $scope.dataForBslist = function(data) {
            gridTemp = []
            Parts.splice();
            Parts = [];
            if (data.JobTask.length > 0) {
                var tmpJob = data.JobTask;
                for (var i = 0; i < tmpJob.length; i++) {
                    console.log("tmpJob", tmpJob);
                    // 
                    for (var j = 0; j < tmpJob[i].JobParts.length; j++) {
                        console.log("tmpJob[i].JobParts[j]", tmpJob[i].JobParts[j]);
                        // if (tmpJob[i].JobParts[j].PaidBy !== null) {
                        //     tmpJob[i].JobParts[j].PaidBy = tmpJob[i].JobParts[j].PaidBy.Name;
                        //     delete tmpJob[i].JobParts[j].PaidBy;
                        // }
                        if (tmpJob[i].JobParts[j].Satuan !== null) {
                            tmpJob[i].JobParts[j].satuanName = tmpJob[i].JobParts[j].Satuan.Name;
                            delete tmpJob[i].JobParts[j].Satuan;
                        }
                        tmpJob[i].subTotal = tmpJob[i].Qty * tmpJob[i].RetailPrice;
                        // $scope.getAvailablePartsServiceManual(tmpJob[i].JobParts[j]);

                        // Parts.push(tmpJob[i].JobParts[j]);
                    }
                }
                // added by sss on 2017-11-03
                $scope.getAvailablePartsServiceManual(tmpJob, 0, 0);
                // 
                $scope.gridWork = gridTemp;
                $scope.gridOriginal = gridTemp;
                // $scope.onAfterSave();
                // console.log("totalw", $scope.totalWork);
            }
            // $scope.sumAllPrice();
            console.log("$scope.gridWork", $scope.gridWork);
            console.log("$scope.JobRequest", $scope.JobRequest);
            console.log("$scope.JobComplaint", $scope.JobComplaint);
        };
        $scope.countPrice = function() {
            var totalW = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].Price !== undefined) {
                    totalW += gridTemp[i].Price;
                }
            }
            $scope.totalWork = totalW;
            console.log(totalW);
            // ============
            var totalM = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].RetailPrice !== undefined) {
                            totalM += gridTemp[i].child[j].RetailPrice;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].RetailPrice);
                        }
                    }
                }
            }
            $scope.totalMaterial = totalM;
        };
        // $scope.selectType = function(selected) {
        //     console.log("selected", selected);
        //     $scope.mData.KatashikiCode = selected.KatashikiCode;
        //     AppointmentGrService.getDataTaskListByKatashiki(selected.KatashikiCode).then(function(restask) {
        //         taskList = restask.data.Result;
        //         var tmpTask = {};
        //         // if (taskList.length > 0) {
        //         //     for (var i = 0; i < taskList.length; i++) {
        //         //         if (taskList[i].ServiceRate !== undefined) {
        //         //             var summary = taskList[i].ServiceRate;
        //         //             var summaryy = parseInt(summary);
        //         //             tmpTask.Summary = Math.round(summaryy);
        //         //             // row.Fare = taskList[i].AdjustmentServiceRate;
        //         //             tmpTask.Fare = taskList[i].ServiceRate;
        //         //             $scope.PriceAvailable = true;
        //         //         } else {
        //         //             tmpTask.Fare = taskList[i].ServiceRate;
        //         //             tmpTask.Summary = tmpTask.Fare;
        //         //         }
        //         //         $scope.tmpServiceRate = tmpTask;
        //         //     }
        //         // }
        //         if (taskList.length > 0) {
        //             for (var i = 0; i < taskList.length; i++) {
        //                 if (taskList[i].ServiceRate !== undefined && taskList[i].WarrantyRate !== undefined) {
        //                     tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
        //                     tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
        //                     $scope.PriceAvailable = true;
        //                 } else {
        //                     tmpTask.ServiceRate = Math.round(taskList[i].ServiceRate);
        //                     tmpTask.WarrantyRate = Math.round(taskList[i].WarrantyRate);
        //                 }
        //                 $scope.tmpServiceRate = tmpTask;
        //             }
        //         } else {
        //             $scope.tmpServiceRate = null;
        //         }
        //     });
        //     $scope.mData.ModelTypeCode = selected.Description
        //     console.log("$scope.mData", $scope.mData);
        // }
        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
            // maxDate: $scope.tesDate,
            // disableWeekend: 1
        };
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }
        $scope.onBeforeNewMode = function() {
            console.log("before new mode");
            // added by sss on 2017-09-25
            // $scope.modalMode = 'new';
            // 
            $scope.openPlottingStall = false;
            $scope.mData.JobRequest = [];
            $scope.mData.JobComplaint = [];
            $scope.disabledBtn = false;
            tmpPrediagnose = {};
            $scope.paramAsb = 0;
            $scope.asb = {};
            $scope.asb = { showMode: 'main', NewChips: 1 }
            $scope.asb.endDate = new Date();
            $scope.asb.endDate.setDate($scope.asb.endDate.getDate() + 7);
            $scope.mData.EstimationDate = new Date();
            MrsList.getDataModel().then(function(res) {
                console.log("result : ", res.data.Result);
                $scope.modelData = res.data.Result;
            });
            $scope.estimateAsb();
            // EstimasiFactory.getLocation().then(function(res){
            //     // console.log("List Province====>",res.data.Result);
            //     $scope.ProvinceData = res.data.Result;
            //     console.log("ProvinceData", $scope.ProvinceData);
            //   });
            console.log("$scope.mData", $scope.mData);
        }
        $scope.selectProvince = function(row) {
            $scope.CityRegencyData = row.MCityRegency;
        }

        $scope.selectRegency = function(row) {
            $scope.DistrictData = row.MDistrict;
        }

        $scope.selectDistrict = function(row) {
            $scope.VillageData = row.MVillage;
        };


        $scope.getData = function() {
                var RoleId = $scope.user.RoleId;
                var IsGR = 1;

                if (RoleId == 1028) {
                    IsGR = 1;
                } else if (RoleId == 1029) {
                    IsGR = 0;
                } else if (RoleId == 1025) {
                    IsGR = 1;
                }


                console.log("====>", $scope.filter);
                if ($scope.filter.typeCategory == 1 && $scope.filter.LicensePlate !== "" && $scope.filter.LicensePlate !== undefined) {
                    console.log("$scope.filter.LicensePlate", $scope.filter.LicensePlate);
                    $scope.loading = true;
                    $scope.filter.LicensePlate = $scope.filter.LicensePlate.toUpperCase();
                    EstimasiFactory.getData($scope.filter.LicensePlate, IsGR).then(function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                    }); // $scope.gridData = res;

                } else if ($scope.filter.typeCategory == 2 && $scope.filter.firstDate != null && $scope.filter.lastDate != null) {
                    var a = new Date($scope.filter.firstDate);
                    var yearFirst = a.getFullYear();
                    var monthFirst = a.getMonth() + 1;
                    var dayFirst = a.getDate();
                    var firstDate = yearFirst + '-' + monthFirst + '-' + dayFirst;

                    var b = new Date($scope.filter.lastDate);
                    var yearLast = b.getFullYear();
                    var monthLast = b.getMonth() + 1;
                    var dayLast = b.getDate();
                    var lastDate = yearLast + '-' + monthLast + '-' + dayLast;
                    $scope.loading = true;
                    EstimasiFactory.getDataByDate(firstDate, lastDate, IsGR).then(function(res) {
                        $scope.grid.data = res.data.Result;
                        $scope.loading = false;
                        console.log('sampe sini gak sih', $scope.grid.data);
                    });
                } else {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Mohon Input Filter",
                        // content: error.join('<br>'),
                        // number: error.length
                    });
                    $scope.grid.data = [];
                    $scope.loading = false;
                }
            }
            // ==================================
        EstimasiFactory.getPayment().then(function(res) {
            // $scope.paymentData = res.data.Result;
            var tmpPayment = [];
                var tmpPaymentPart = [];
                var tmpPaymentIsNotWarranty = [];
                var tmpPaymentThereIstWarranty = [];
                _.map(res.data.Result,function(val){
                    if(val.Name !== 'Insurance'){
                        tmpPayment.push(val);
                        if (val.Name !== 'Warranty') {
                          tmpPaymentPart.push(val);
                        }else {
                          tmpPaymentThereIstWarranty.push(val);
                        }
                        if(val.Name !== 'Insurance' && val.Name !== 'Warranty'){
                            tmpPaymentIsNotWarranty.push(val);
                        }
                    }
                })
                $scope.paymentData = tmpPayment;
                $scope.paymentDataPart = tmpPaymentPart;
                $scope.paymentDataIsNotWarranty = tmpPaymentIsNotWarranty;
                $scope.paymentDataThereIstWarranty = tmpPaymentThereIstWarranty;
                console.log('gaada warratny', $scope.paymentDataIsNotWarranty);
                console.log('payment part', $scope.paymentDataPart);
                console.log('payment ada warranty', $scope.paymentDataThereIstWarranty);
                console.log('payment gaada warranty', $scope.paymentDataIsNotWarranty);
                //validasi variable paymentData tanpa warranty
                var tmpIdx = _.findIndex($scope.paymentData, { 'Name': 'Warranty' });
                $scope.somePaymentData = angular.copy($scope.paymentData);
                $scope.somePaymentData.splice(tmpIdx, 1);		
        });
        EstimasiFactory.getUnitMeasurement().then(function(res) {
            $scope.unitData = res.data.Result;
        });
        EstimasiFactory.getWoCategory().then(function(res) {
            var temparrCategory = []
            for (var i=0; i<res.data.Result.length; i++) {
                if (res.data.Result[i].Name != 'BP') {
                    temparrCategory.push(res.data.Result[i])
                }
            }
            $scope.woCategory = temparrCategory;

            // $scope.woCategory = res.data.Result;
            console.log("getWoCategory", $scope.woCategory);
        });
        EstimasiFactory.getTaskCategory().then(function(res) {
            $scope.fullCategory = res.data.Result;
            $scope.notFlcFpc = [];
            $scope.notFpcPlc = [];
            $scope.notFlcPlc = [];
            $scope.notFlcPlcFpc = [];
            _.map(res.data.Result, function(val){
                if (val.Name !== "FLC - Free Labor Claim" && val.Name !== "FPC - Free Parts Claim") {
                    $scope.notFlcFpc.push(val); 
                }
                if (val.Name !== "FPC - Free Parts Claim" && val.Name !== "PLC - Parts Labor Claim") {
                    $scope.notFpcPlc.push(val)  
                }
                if (val.Name !== "FLC - Free Labor Claim" && val.Name !== "PLC - Parts Labor Claim") {
                    $scope.notFlcPlc.push(val)  
                }
                if (val.Name !== "FLC - Free Labor Claim" && val.Name !== "PLC - Parts Labor Claim" && val.Name !== "FPC - Free Parts Claim") {
                    $scope.notFlcPlcFpc.push(val)  
                }
            })
        });
        RepairSupportActivityGR.getT1().then(function(res) {
                $scope.TFirst = res.data.Result;
                for (var i=0; i<res.data.Result.length; i++){
                    $scope.TFirst[i].xName = $scope.TFirst[i].T1Code + ' - ' + $scope.TFirst[i].Name;
                }
                console.log("T1 checked ini cuy", $scope.TFirst);
            },
            function(err) {
                console.log("err=>", err);
            }
        );
        // ==================================
        // Grid Action Custom
        // ==================================
        $scope.gridActionButtonTemplate = '<div class="ui-grid-cell-contents">' +
            // '<a href="#" uib-tooltip="Buat" tooltip-placement="bottom" ng-click="grid.appScope.actView(row.entity)" style="">Appointment</a>'+
            '<a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="View" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-list-alt" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Edit" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-pencil" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '<a href="#" ng-click="grid.appScope.$parent.actPrint(row.entity)" uib-tooltip="Print" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-print" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [{
                    name: 'Id Estimate',
                    field: 'IdEstimate',
                    width: '7%',
                    visible: false
                },
                {
                    name: 'No. Estimasi',
                    field: 'EstimationNo'
                },
                {
                    name: 'No. Polisi',
                    field: 'PoliceNumber'
                },
                {
                    name: 'Model',
                    field: 'ModelType'
                },
                {
                    name: 'Thn. Rakit',
                    field: 'AssemblyYearForEstimation'
                },
                {
                    name: 'Tipe',
                    field: 'ModelCode'
                },
                {
                    name: 'Tgl. Estimasi',
                    field: 'EstimationDate',
                    cellFilter: dateFilter
                }
            ]
        };
        $scope.dataForPrediagnose = function(data) {
            FollowUpGrService.getPreDiagnoseByJobId(data.JobId).then(function(res) {
                tmpPrediagnose = res.data;
                // if(tmpPrediagnose.StallId !== undefined){
                //     if(tmpPrediagnose.StallId == null ){
                //         tmpPrediagnose.StallId = 0;
                //     }
                // }
                tmpPrediagnose.PrediagStallId = tmpPrediagnose.StallId;
                $scope.mData.PrediagStallId = tmpPrediagnose.StallId;
                $scope.mData.StallId = tmpPrediagnose.StallId;
                $scope.estimateAsb();
                if ($scope.modeDetail == 'view') {
                    $scope.disableButton();
                }
                console.log("resss Prediagnosenya bosque", res.data);
            });
        }
        $scope.addDetail = function(data, item, itemPrice, row) {
            console.log('clearDetail', $scope.listApi.clearDetail());
            $scope.listApi.clearDetail();
            row.FlatRate = data.FlatRate;
            row.catName = $scope.tmpCatg.Name;
            // row.ActualRate = data.FlatRate;
            if(data.standardactualrate !== 0 ){
                row.ActualRate = data.standardactualrate
            }else{
                row.ActualRate = data.FlatRate;
            }
            row.tmpTaskId = null;
            // row.cusType=$scope.tmpCus.Name
            console.log('$scope.tmpPaidById 3', $scope.tmpPaidById)
            if ($scope.tmpPaidById !== null) {
                row.PaidById = $scope.tmpPaidById;
                row.PaidBy = $scope.tmpPaidName;
            }
            console.log("itemPrice", itemPrice);
            if (itemPrice.length > 0) {
                for (var i = 0; i < itemPrice.length; i++) {
                    console.log("scope.tmpCatg.MasterId", $scope.tmpCatg.MasterId, $scope.tmpServiceRate);
                    // if($scope.tmpCatg.MasterId !== undefined && $scope.tmpCatg.MasterId !== null){
                    //     $scope.checkServiceRate(data.FlatRate,$scope.tmpCatg.MasterId,1);
                    // }

                    // if (itemPrice[i].ServiceRate !== undefined) {
                    //     var harganya = itemPrice[i].ServiceRate;
                    //     var harganyaa = data.FlatRate;
                    //     var summary = harganya * harganyaa;
                    //     var summaryy = parseInt(summary);
                    //     row.Summary = Math.round(summaryy);
                    //     // row.Fare = itemPrice[i].AdjustmentServiceRate;
                    //     row.Fare = itemPrice[i].ServiceRate;
                    // $scope.PriceAvailable = true;
                    // } else {
                    //     row.Fare = itemPrice[i].ServiceRate;
                    //     row.Summary = row.Fare;
                    // }
                    var sum = 0;
                    var tmpItem;
                    if ($scope.tmpCatg.MasterId == 59 || $scope.tmpCatg.MasterId == 60) {
                        if ($scope.tmpServiceRate !== null) {
                            if (data.FlatRate !== null) {
                                tmpItem = data.FlatRate.toString();
                                if (tmpItem.includes(".")) {
                                    tmpItem = tmpItem.split('.');
                                    if (tmpItem[tmpItem.length - 1] !== ".") {
                                        // item = parseInt(tmpItem);
                                        sum = $scope.tmpServiceRate.WarrantyRate * data.FlatRate;
                                        sum = Math.round(sum);
                                    }
                                } else {
                                    sum = $scope.tmpServiceRate.WarrantyRate * data.FlatRate;
                                    sum = Math.round(sum);
                                }
                            } else {
                                sum = $scope.tmpServiceRate.WarrantyRate;
                                sum = Math.round(sum);
                            }
                            $scope.PriceAvailable = true;
                            sum = Math.round(sum);
                            row.Summary = sum;
                            row.Fare = $scope.tmpServiceRate.WarrantyRate;
                            console.log("row WarrantyRate lewat $scope.addDetail", row);
                        }
                    } else {
                        if ($scope.tmpServiceRate !== null) {
                            if (data.FlatRate !== null) {
                                tmpItem = data.FlatRate.toString();
                                if (tmpItem.includes(".")) {
                                    tmpItem = tmpItem.split('.');
                                    if (tmpItem[tmpItem.length - 1] !== ".") {
                                        // data.FlatRate = parseInt(tmpItem);
                                        sum = $scope.tmpServiceRate.ServiceRate * data.FlatRate;
                                        sum = Math.round(sum);
                                    }
                                } else {
                                    sum = $scope.tmpServiceRate.ServiceRate * data.FlatRate;
                                    sum = Math.round(sum);
                                }
                            } else {
                                sum = $scope.tmpServiceRate.ServiceRate;
                                sum = Math.round(sum);
                            }
                            $scope.PriceAvailable = true;
                            sum = Math.round(sum);
                            row.Summary = sum;
                            row.Fare = $scope.tmpServiceRate.ServiceRate;
                            console.log("row ServiceRate lewat $scope.addDetail", row);
                        }
                    }
                    // row.ActualRate = data.FlatRate;
                    if(data.standardactualrate !== 0 ){
                        row.ActualRate = data.standardactualrate
                    }else{
                        row.ActualRate = data.FlatRate;
                    }
                    console.log("row terakhir lewat $scope.addDetail", row);
                }
            } else {
                $scope.PriceAvailable = false;
                row.Fare = "";
                row.Summary = "";
            }
            row.TaskId = data.TaskId;
            row.isOPB = data.isOPB ? data.isOPB : null;
            var tmp = {}
                // $scope.listApi.addDetail(tmp,row)
            if (item.length > 0) {
                for (var i = 0; i < item.length; i++) {
                    // var tmpItem = item[i];
                    // $scope.getAvailablePartsService(item[i]);
                    // if ($scope.tmpPaidById !== null) {
                    //     item[i].PaidById = $scope.tmpPaidById;
                    //     item[i].paidName = $scope.tmpPaidName;
                    // }
                    // item[i].subTotal = item[i].Qty * item[i].RetailPrice;
                    // // console.log("item[i].subTotal",item[i].subTotal);
                    // tmp = item[i]
                    // $scope.listApi.addDetail(tmp, row);
                    $scope.getAvailablePartsService(item[i], row);
                }
            } else {
                $scope.listApi.addDetail(false, row);
            }
        };
        $scope.actPrint = function(row) {
            console.log("row", row);
            $scope.printPreview(row.JobId);
        }
        $scope.onListSelectRows = function(rows) {
                console.log("form controller=>", rows);
            }
            // $scope.gridPartsDetail = {

        //     columnDefs: [
        //         { name: "No. Material", field: "PartsCode" },
        //         { name: "Nama Material", field: "PartsName" },
        //         { name: "Qty", field: "Qty" },
        //         { name: "Satuan", field: "satuanName" },
        //         // { name: "Satuan", field: "SatuanId", cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.unitDataGrid[row.entity.SatuanId]}}</div>' },
        //         { name: "Ketersedian", field: "Availbility" },
        //         { name: "Tipe", field: "Type" },
        //         { name: "ETA", displayName: "ETA", field: "ETA", cellFilter: dateFilter },
        //         { name: "Harga", field: "RetailPrice" },
        //         { name: "Discount", field: "Discount" },
        //         { name: "SubTotal", field: "subTotal" },
        //         { name: "Nilai Dp", displayName: "Nilai DP", displayName: "Nilai DP", field: "DPRequest" },
        //         { name: "Status DP", displayName: "Status DP", field: "StatusDp", cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>' },
        //         { name: "Pembayaran", field: "PaidBy" },
        //         { name: "OPB", displayName: "OPB", field: "isOPB", cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>' }
        //     ]
        // };
        $scope.gridPartsDetail = {

            columnDefs: [
                { name: "No. Material", field: "PartsCode", width: '15%' },
                { name: "Nama Material", field: "PartsName", width: '20%' },
                { name: "Qty", field: "Qty", width: '6%' },
                { name: "Satuan", field: "satuanName", width: '8%' },
                // { name: "Satuan", field: "SatuanId", cellTemplate: '<div class="ui-grid-cell-contents">{{grid.appScope.$parent.unitDataGrid[row.entity.SatuanId]}}</div>' },
                { name: "Ketersediaan", field: "Availbility", width: '12%' },
                { name: "Tipe", field: "Type", width: '6%' },
                { name: "ETA", displayName: "ETA", field: "ETA", cellFilter: dateFilter, width: '11%' },
                { name: "Harga", field: "RetailPrice", width: '10%',cellFilter: 'currency:"":0' },
                { name: "Discount", field: "Discount", width: '10%' },
                { name: "SubTotal", field: "subTotal", width: '10%',cellFilter: 'currency:"":0' },
                { name: "Nilai Dp", displayName: "Nilai DP", field: "DownPayment", width: '10%',cellFilter: 'currency:"":0' },
                { name: "Status DP", displayName: "Status DP", field: "StatusDp", cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="0" disabled="disabled"></div>', width: '7%' },
                { name: "Pembayaran", field: "PaidBy", width: '11%' },
                { name: "OPB", displayName: "OPB", field: "isOPB", width: '7%', cellTemplate: '<div class="ui-grid-cell-contents" style="text-align:center;"><input type="checkbox" ng-model="MODEL_COL_FIELD" ng-disabled="true" ng-true-value="1" ng-false-value="null" disabled="disabled"></div>' }
            ]
        };
        // '<div class="ui-grid-cell-contents" style="text-align:center;padding-top:-10px;"><bscheckbox ng-model="MODEL_COL_FIELD" true-value="1" ng-disabled="true" false-value="0"></bscheckbox></div>'
        $scope.gridWork = gridTemp;
        $scope.sendWork = function(key, data) {
            var taskandParts = [];
            var taskList = {};
            AppointmentGrService.getDataPartsByTaskId(data.TaskId).then(function(res) {
                console.log("resabis ambil parts", res.data.Result);
                taskandParts = res.data.Result;
                AppointmentGrService.getDataTaskListByKatashiki($scope.mData.KatashikiCode).then(function(restask) {
                    taskList = restask.data.Result;
                    $scope.addDetail(data, taskandParts, taskList, $scope.lmModel)
                });
            });
        }
        $scope.onBeforeNewList = function() {
            console.log("before new list");
            AppointmentGrService.getDataVIN($scope.mData.VIN).then(function(res){
                console.log('cek input rangka 1', $scope.mData.VIN)
                var dataVIN = res.data.Result;
                // console.log('cek res input 1', $scope.tempVIN);
                if (dataVIN.length > 0) {
                    if( dataVIN == "" ||  dataVIN == null ||  dataVIN == undefined){
                        var filtered = $scope.taskCategory.filter(function(item) { 
                            return item.Name !== "FLC - Free Labor Claim" && item.Name !== "FPC - Free Parts Claim" && item.Name !== "PLC - Parts Labor Claim" ;  
                         });
                         console.log("filtered",filtered);
                         $scope.taskCategory = filtered;
                         console.log("$scope.taskCategory final",$scope.taskCategory);
                    }else{
                        for (var i = 0; i < dataVIN.length; i++) {
                            if (dataVIN[i].ClaimType == "FPC - Free Parts Claim") {
                                $scope.taskCategory = $scope.notFlcPlc
                            }else if(dataVIN[i].ClaimType == "FLC - Free Labor Claim"){
                                $scope.taskCategory = $scope.notFpcPlc
                            }else if(dataVIN[i].ClaimType == "PLC - Parts Labor Claim"){
                                $scope.taskCategory = $scope.notFlcFpc
                            }else{
                                $scope.taskCategory = filtered;
                            }
                        }
                    }
                }else{
                    $scope.taskCategory = $scope.notFlcPlcFpc
                }
            })
            $scope.isRelease = false;
            $scope.FlatRateAvailable = false;
            $scope.PriceAvailable = false;
            $scope.partsAvailable = false;
            var tmpTask = {}
            tmpTask.isOpe = 0;
            tmpTask.ProcessId = 2;
            $scope.listApi.addDetail(false, tmpTask);
        };
        $scope.sendParts = function(key, data) {
                console.log('checked data part', data)
                var tmpMaterialId = angular.copy($scope.ldModel.MaterialTypeId);
                console.log("a $scope.ldModel.Parts", $scope.ldModel);
                $scope.ldModel = data;
                var tmp = {};
                tmp = data;
                $scope.ldModel.PartsId = data.PartsId;
                $scope.ldModel.PartsName = data.PartsName;
                $scope.ldModel.MaterialTypeId = tmpMaterialId;
                $scope.ldModel.SatuanId = data.UomId;
                $scope.ldModel.satuanName = data.Name;
                // $scope.ldModel.PaidById = data.PaidById; 
                // $scope.listApi.addDetail(tmp);
                console.log("key Parts", key);
                console.log("data Parts", data);
                console.log("b $scope.ldModel.Parts", $scope.ldModel);

                if ($scope.JobIsWarranty == 1){
                    console.log('pekerjaan nya warranty nih parts nya jg harus', $scope.ldModel.PaidById);
                    _.map($scope.paymentData, function(val) {
                        console.log("valll tipe", val);
                        if (val.Name === "Warranty") {
                            $scope.ldModel.PaidById = val.MasterId;
                            $scope.ldModel.PaidBy = val.Name;
                        }
                    })
    
                }else if($scope.JobIsWarranty == 3){
                    console.log('fpc PART', $scope.ldModel.PaidById);
                    _.map($scope.paymentDataThereIstWarranty, function(val) {
                      if (val.Name === "Warranty") {
                          $scope.ldModel.PaidById = val.MasterId;
                          $scope.ldModel.PaidBy = val.Name;
                      }
                   })
                  }else if($scope.JobIsWarranty == 4){
                    console.log('PLC PART', $scope.ldModel.PaidById);
                      _.map($scope.paymentDataThereIstWarranty, function(val) {
                        if (val.Name === "Warranty") {
                            $scope.ldModel.PaidById = val.MasterId;
                            $scope.ldModel.PaidBy = val.Name;
                        }
                     })
                  } else {
                    console.log('pekerjaan nya bukan warranty');
                }

            }
            //----------------------------------
            // Type Ahead
            //----------------------------------
        $scope.getWork = function(key) {
            var Katashiki = $scope.mData.KatashikiCode;
            var catg = $scope.tmpCatg.MasterId;
            var vehModelId = $scope.mData.VehicleModelId;
            console.log("vehiclemodelllll",$scope.mData.VehicleModelId);
            console.log("$scope.mDataDetail.KatashikiCode", Katashiki);
            console.log("$scope.lmModel.JobType", catg);
            if (Katashiki !== undefined) {
                if (Katashiki != null && catg != null) {
                    var ress = AppointmentGrService.getDataTask(key, Katashiki, catg, vehModelId).then(function(resTask) {
                        return resTask.data.Result;
                    });
                    console.log("ress", ress);
                    return ress
                }
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input Data Mobil Terlebih Dahulu",
                });
            }
        };
        $scope.getParts = function(key) {
            var kategori = $scope.ldModel.MaterialTypeId;
            var isGr = 1;
            if (kategori !== undefined) {
                // var ress = AppointmentGrService.getDataParts(key, kategori).then(function(resparts) {
                var ress = AppointmentGrService.getDataParts(key, isGr, kategori).then(function(resparts) {
                    console.log("respart WO GR", resparts);
                    var tempResult = [];
                    // tempResult.push(resparts.data.Result[0]);
                    // console.log('isGr COYYY 0', tempResult);
                    // return tempResult;
                    if (resparts.data.Result[0].isGr == 1) {
                        // tempResult.push(resparts.data.Result[0]);
                        for (var i=0; i<resparts.data.Result.length; i++){
                            if(resparts.data.Result[i].PartsClassId1 == kategori){
                                tempResult.push(resparts.data.Result[i]);
                            }
                        }
                        console.log('isGr COYYY 0', tempResult);
                        return tempResult;
                    } else {
                        // tempResult.push(resparts.data.Result[1]);
                        for (var i=0; i<resparts.data.Result.length; i++){
                            if(resparts.data.Result[i].PartsClassId1 == kategori){
                                tempResult.push(resparts.data.Result[i]);
                            }
                        }
                        console.log('isGr COYYY 1', tempResult);
                        return tempResult;
                    }
                });
                console.log("ress", ress);
                return ress
            }
            // }
        };
        $scope.modelOptions = {
            debounce: {
                default: 500,
                blur: 250
            },
            getterSetter: true
        };
        $scope.noResults = true;
        // var idx = gridTemp.length;
        $scope.onSelectWork = function($item, $model, $label) {
            // console.log("onSelectWork=>", idx);
            console.log("onSelectWork=>", $item);
            console.log("onSelectWork=>", $model);
            console.log("onSelectWork=>", $label);
            // $scope.mData.Work = $label;
            // console.log('materialArray', materialArray);
            // materialArray = [];
            // materialArray.push($item);
            if ($item.FlatRate != null) {
                console.log("FlatRate", $item.FlatRate);
                $scope.FlatRateAvailable = true;
            } else {
                $scope.FlatRateAvailable = false;
            }
            $scope.sendWork($label, $item);
            console.log("modelnya", $scope.lmModel);
            // console.log('materialArray',materialArray);
        }
        $scope.onSelectParts = function($item, $model, $label) {
            // console.log("onSelectWork=>", idx);
            console.log("onSelectParts=>", $item);
            console.log("onSelectParts=>", $model);
            console.log("onSelectParts=>", $label);
            // $scope.mData.Work = $label;
            // console.log('materialArray', materialArray);
            // materialArray = [];
            // materialArray.push($item);
            if ($item.PartsName !== null) {
                $scope.partsAvailable = true;
            } else {
                $scope.partsAvailable = false;
            }
            $scope.sendParts($label, $item);
            console.log("modelnya", $item);
            // console.log('materialArray',materialArray);
        };

        //Sementara pake ini dulu, nanti klu uda orang FE yg fixing biar di convert dengan benar.
        $scope.changeFormatDateStrip = function(item) {
            console.log("item item", item);
            var finalDate = "";
            var res = item.substring(0, 19).replace("T"," ");
            console.log("item res", res);
            finalDate += res;
            return finalDate;
        };

        $scope.convertingJamServer = function(dataJamServer) {
            // 2020-02-29T17:13:04 ga blh pake new date karena akan jadi jam local FE
            dataJamServer = dataJamServer.substr(0,19)
            dataJamServer = dataJamServer.replace('T', ' ');
            // var yyyy = dataJamServer.getFullYear().toString();
            // var MM = (dataJamServer.getMonth() + 1).toString(); // getMonth() is zero-based
            // var dd = dataJamServer.getDate().toString();
            // var Hour = dataJamServer.getHours().toString();
            // var minute = dataJamServer.getMinutes().toString();
            // var second = dataJamServer.getSeconds().toString();

            // finalDateTime = yyyy + '-' + (MM[1] ? MM : "0" + MM[0]) + '-' + (dd[1] ? dd : "0" + dd[0]) + ' ' + (Hour[1] ? Hour : "0" + Hour[0]) + ':' + (minute[1] ? minute : "0" + minute[0]) + ':' + (second[1] ? second : "0" + second[0]);
            return dataJamServer;
        }

        $scope.onValidateSave = function(data) {
            console.log("sebelum di urus", data);
            console.log("gridTemp,$scope.gridWork", gridTemp, $scope.gridWork);
            var tmpGrid = [];
            var tmpJobComplaint = [];
            var tmpJobRequest = [];
            tmpGrid = angular.copy($scope.gridWork);
            tmpJobComplaint = angular.copy($scope.JobComplaint);
            tmpJobRequest = angular.copy($scope.JobRequest);
            console.log('ini prediagnose', tmpPrediagnose)
            // if (tmpPrediagnose == null || tmpPrediagnose == undefined){
            //     tmpPrediagnose.FirstIndication = new Date();
            // } else {
            //     if (tmpPrediagnose.FirstIndication == null || tmpPrediagnose.FirstIndication == undefined){
            //         tmpPrediagnose.FirstIndication = new Date();
            //     }
            // }

            if ($scope.mData.VehicleModelName == null) {
                $scope.mData.VehicleModelName = $scope.mData.ModelType;
            }
            if ($scope.mData.ModelTypeCode == null) {
                $scope.mData.ModelTypeCode = $scope.mData.ModelCode;
            }
            var tmpKm = angular.copy($scope.mData.Km);
            var backupKm = angular.copy($scope.mData.Km);
            if (tmpKm.includes(",")) {
                tmpKm = tmpKm.split(',');
                if (tmpKm.length > 1) {
                    tmpKm = tmpKm.join('');
                } else {
                    tmpKm = tmpKm[0];
                }
            }
            tmpKm = parseInt(tmpKm);
            $scope.mData.KmFinal = tmpKm;
            // if (gridTemp.length > 0) {
            if (data.JobId == undefined) {

                EstimasiFactory.getJamServer().then(function(resuJam) {
                    // $scope.JamServer = $filter('date')(resuJam.data, 'yyyy-MM-dd HH:mm:ss');
                    // $scope.JamServer =  $scope.changeFormatDateStrip(resuJam.data);
                    $scope.JamServer = $scope.convertingJamServer(resuJam.data);

                    console.log("sebelum di JamServer", $scope.JamServer);

                    if ($scope.gridWork.length > 0) {
                        for (var i = 0; i < tmpGrid.length; i++) {
                            console.log('tmpGrid data', tmpGrid[i])
                            tmpGrid[i].JobParts = [];
                            angular.forEach(tmpGrid[i].child, function(v, k) {
                                tmpGrid[i].JobParts[k] = v;
                                tmpGrid[i].JobParts[k].Price = tmpGrid[i].JobParts[k].RetailPrice;
                                if (tmpGrid[i].JobParts[k].JobPartsId == undefined || tmpGrid[i].JobParts[k].JobPartsId == null) {
                                    tmpGrid[i].JobParts[k].JobPartsId = 0;
                                }
                                delete tmpGrid[i].JobParts[k].ETA;
                                // delete tmpGrid[i].JobParts[k].ValueDP;
                                delete tmpGrid[i].JobParts[k].PaidBy;
                                delete tmpGrid[i].child;
    
                            })
                            delete tmpGrid[i].PaidBy;
                            delete tmpGrid[i].tmpTaskId;
    
                        }
    
                    } else {
                        tmpGrid = [];
                    }
                    for (var i in tmpJobComplaint) {
                        if (tmpJobComplaint[i].JobComplaintId == null || tmpJobComplaint[i].JobComplaintId == undefined) {
                            tmpJobComplaint[i].JobComplaintId = 0;
                        }
                        if (data.JobId !== undefined || data.JobId !== null) {
                            tmpJobComplaint[i].JobId = data.JobId;
                        } else {
                            tmpJobComplaint[i].JobId = 0;
                        }
                    }
                    for (var i in tmpJobRequest) {
                        if (tmpJobRequest[i].JobRequestId == null || tmpJobRequest[i].JobRequestId == undefined) {
                            tmpJobRequest[i].JobRequestId = 0;
                        }
                        if (data.JobId !== undefined || data.JobId !== null) {
                            tmpJobRequest[i].JobId = data.JobId;
                        } else {
                            tmpJobRequest[i].JobId = 0;
                        }
                    }
                    data.JobTask = tmpGrid;
                    data.JobComplaint = tmpJobComplaint;
                    data.JobRequest = tmpJobRequest;
                    var tmpLicensePlate = angular.copy(data.LicensePlate);
                    data.LicensePlate = tmpLicensePlate.toUpperCase();
                    // =======================
                    var tmpDate = angular.copy(data.EstimationDate);
                    var finalDate
                    var yyyy = tmpDate.getFullYear().toString();
                    var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                    var dd = tmpDate.getDate().toString();
                    finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                    data.EstimationDate = finalDate;
                    // =======================
                    tmpPrediagnose.ScheduledTime = $filter('date')($scope.mData.PrediagScheduledTime, 'yyyy-MM-dd HH:mm:ss');
                    tmpPrediagnose.StallId = $scope.mData.PrediagStallId;
                    tmpPrediagnose.Status = $scope.mData.StatusPreDiagnose;
    
                    if ($scope.mData.PrediagStallId == null && tmpPrediagnose.fromAppointment == undefined && tmpPrediagnose.StallId == null) {
                        $scope.mData.StatusPreDiagnose = null;
                    }
                    // =======================
                    console.log("sesudah Di urus", data);
                    EstimasiFactory.createEstimation(data, $scope.JamServer).then(function(res) {
                        var tmpJobId = res.data.ResponseMessage;
                        tmpJobId = tmpJobId.split('#');
                        tmpPrediagnose.JobId = tmpJobId[1];
                        AppointmentGrService.getNoAppointment(tmpJobId[1]).then(function(resu) {
                            console.log('getNoAppointment', resu.data);
                            if (tmpPrediagnose.fromAppointment !== undefined && tmpPrediagnose.StallId !== null) {
                                if (tmpPrediagnose.PrediagnoseId !== null && tmpPrediagnose.PrediagnoseId !== undefined) {
                                    AppointmentGrService.updatePrediagnose(tmpPrediagnose).then(function(res) {
    
                                    });
                                } else {
                                    if (tmpPrediagnose.FirstIndication != null && tmpPrediagnose.FirstIndication != undefined){
                                        AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {
    
                                        });
                                    }
                                };
                            }
                            $scope.alertAfterSave(resu.data, tmpJobId[1]);
                            // added by sss on 2017-09-25
                            // EstimasiFactory.createClaim([$scope.mClaim]).then(function(cres){
                            //   $scope.alertAfterSave(resu.data);
                            // });
                            // 
                        });
                    });
    
                    return
                })

                
            } else {
                for (var i = 0; i < tmpGrid.length; i++) {
                    tmpGrid[i].JobParts = [];
                    angular.forEach(tmpGrid[i].child, function(v, k) {
                        tmpGrid[i].JobParts[k] = v;
                        tmpGrid[i].JobParts[k].Price = tmpGrid[i].JobParts[k].RetailPrice;
                        if (tmpGrid[i].JobParts[k].JobPartsId == undefined || tmpGrid[i].JobParts[k].JobPartsId == null) {
                            tmpGrid[i].JobParts[k].JobPartsId = 0;
                        }
                        delete tmpGrid[i].JobParts[k].ETA;
                        delete tmpGrid[i].JobParts[k].PaidBy;
                        // delete tmpGrid[i].JobParts[k].ValueDP;
                        delete tmpGrid[i].child;
                    })
                    delete tmpGrid[i].PaidBy;

                }
                data.JobTask = tmpGrid;
                data.JobComplaint = $scope.JobComplaint;
                data.JobRequest = $scope.JobRequest;
                // tmpPrediagnose.ScheduledTime = $filter('date')(data.PrediagScheduledTime, 'yyyy-MM-dd HH:mm:ss');
                // tmpPrediagnose.StallId = data.PrediagStallId;
                var tmpLicensePlate = angular.copy(data.LicensePlate);
                data.LicensePlate = tmpLicensePlate.toUpperCase();
                console.log("sesudah Di urus", data);
                // =======================
                var tmpDate = angular.copy(data.EstimationDate);
                var finalDate
                var yyyy = tmpDate.getFullYear().toString();
                var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                var dd = tmpDate.getDate().toString();
                finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                data.EstimationDate = finalDate;
                // =======================
                tmpPrediagnose.ScheduledTime = $filter('date')($scope.mData.PrediagScheduledTime, 'yyyy-MM-dd HH:mm:ss');
                tmpPrediagnose.StallId = $scope.mData.PrediagStallId;
                tmpPrediagnose.Status = $scope.mData.StatusPreDiagnose;

                if ($scope.mData.PrediagStallId == null && tmpPrediagnose.fromAppointment == undefined && tmpPrediagnose.StallId == null) {
                    $scope.mData.StatusPreDiagnose = null;
                }
                // =======================
                EstimasiFactory.updateEstimation(data).then(function(res) {
                    var tmpJobId = res.data.ResponseMessage;
                    tmpJobId = tmpJobId.split('#');
                    tmpPrediagnose.JobId = tmpJobId[1];
                    AppointmentGrService.getNoAppointment(tmpJobId[1]).then(function(resu) {
                        console.log('getNoAppointment', resu.data);
                        $scope.alertAfterSave(resu.data, tmpJobId[1]);
                        if (tmpPrediagnose.fromAppointment !== undefined && tmpPrediagnose.StallId !== null) {
                            if (tmpPrediagnose.PrediagnoseId !== null && tmpPrediagnose.PrediagnoseId !== undefined) {
                                AppointmentGrService.updatePrediagnose(tmpPrediagnose).then(function(res) {

                                });
                            } else {
                                if (tmpPrediagnose.FirstIndication != null && tmpPrediagnose.FirstIndication != undefined){
                                    AppointmentGrService.createPreadiagnose(tmpPrediagnose).then(function(res) {

                                    });
                                }
                            };
                        }
                        // added by sss on 2017-09-25
                        // EstimasiFactory.updateClaim([$scope.mClaim]).then(function(cres){
                        //   $scope.alertAfterSave(resu.data);
                        // });
                        // 
                    });
                });

                return
            }
            // } else {
            //     bsAlert.warning("Form masih ada yang belum terisi", "silahkan cek kembali");
            // }
        }
        var linkSaveCb = function(mode, model) {
            tmpPrediagnose = model;
            console.log("tmpPrediagnose linkSaveCb", tmpPrediagnose);
            $scope.estimateAsb();
        }
        var linkBackCb = function(mode, model) {
            console.log("mode", mode);
            console.log("mode", model);
        }
        $scope.preDiagnose = function() {
            tmpPrediagnose.fromAppointment = 1;
            console.log('tmpPrediagnose', tmpPrediagnose);
            console.log("Edit");
            $scope.formApi.showLinkView(linkSaveCb, linkBackCb, 'edit', tmpPrediagnose, 'true', 'true');
        }
        $scope.estimateAsb = function() {
                if ($scope.gridWork.length > 0) {
                    var totalW = 0;
                    for (var i = 0; i < $scope.gridWork.length; i++) {
                        if ($scope.gridWork[i].ActualRate !== undefined) {
                            totalW += $scope.gridWork[i].ActualRate;
                        }
                    }
                    $scope.visibleAsb = 1;
                    $scope.tmpActRate = totalW;
                } else {
                    $scope.tmpActRate = 1;
                    $scope.visibleAsb = 0;
                }
                $scope.checkAsb = true;
                $scope.asb.startDate = "";
                var currentDate;
                currentDate = new Date();
                currentDate.setDate(currentDate.getDate());
                $scope.minDateASB = new Date(currentDate);
                // $scope.currentDate = new Date(currentDate);
                // $scope.asb.startDate = new Date(currentDate);
                if ($scope.mData.AppointmentDate !== undefined && $scope.mData.AppointmentDate !== null) {
                    var tmpDate = $scope.mData.EstimationDate;
                    var tmpTime = tmpPrediagnose.ScheduledTime;
                    var tmphour
                    var tmpMinute
                    var tmpFinalTime = "00:00:00"
                    if (tmpTime !== undefined && tmpTime !== null) {
                        tmphour = tmpTime.getHours();
                        tmpMinute = tmpTime.getMinutes();
                        tmpFinalTime = tmphour + ":" + tmpMinute + ":" + "00";
                    }
                    // console.log("tmpTime",new Date(tmpTime));
                    var finalDate
                    var yyyy = tmpDate.getFullYear().toString();
                    var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                    var dd = tmpDate.getDate().toString();
                    finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + " " + tmpFinalTime;
                    var d = new Date(finalDate);

                    $scope.asb.startDate = d;
                    $scope.currentDate = tmpDate;
                    console.log("dateBro", tmpDate);
                } else {
                    $scope.asb.startDate = new Date(currentDate);
                    $scope.currentDate = new Date(currentDate);
                }
                // ===================================
                var timeToDateStart = $scope.asb.startDate;
                var timeToDateFinish = angular.copy($scope.asb.startDate);
                var timePlanStart = $scope.mData.PlanStart;
                var timePlanFinish = $scope.mData.PlanFinish;
                if (($scope.mData.PlanStart !== null && $scope.mData.PlanStart !== undefined) && $scope.mData.PlanFinish !== null && $scope.mData.PlanFinish !== undefined) {
                    timePlanStart = timePlanStart.split(":");
                    timePlanFinish = timePlanFinish.split(":");
                    timeToDateStart = new Date(timeToDateStart.setHours(timePlanStart[0], timePlanStart[1], timePlanStart[2]));
                    timeToDateFinish = new Date(timeToDateFinish.setHours(timePlanFinish[0], timePlanFinish[1], timePlanFinish[2]));
                } else {
                    var a = timeToDateStart.getHours();
                    var b = timeToDateStart.getMinutes();
                    var d = (a + 1) + ":" + b + ":" + '00';
                    timeToDateFinish = new Date(timeToDateFinish.setHours((a + 1), b, "00"));
                    timeToDateStart = $scope.asb.startDate;
                }
                // ====================================
                $scope.asb.JobStallId = $scope.mData.StallId;
                $scope.asb.JobStartTime = tmpPrediagnose.ScheduledTime;
                $scope.asb.JobEndTime = timeToDateFinish;
                // $scope.asb.currentDate = currentDate;
                // $scope.asb.JobStartTime = $scope.mData.PlanStart;
                // $scope.asb.JobEndTime = $scope.mData.PlanFinish;
                if (tmpPrediagnose.PrediagStallId !== undefined) {
                    $scope.asb.PrediagStallId = tmpPrediagnose.PrediagStallId;
                } else {
                    $scope.asb.PrediagStallId = $scope.mData.PrediagStallId
                }
                $scope.asb.preDiagnose = tmpPrediagnose;
                if (tmpPrediagnose.ScheduledTime !== undefined && tmpPrediagnose.ScheduledTime !== null) {
                    $scope.asb.PrediagStartTime = tmpPrediagnose.ScheduledTime;
                } else {
                    $scope.asb.PrediagStartTime = $scope.mData.PrediagScheduledTime;
                }

                // $scope.asb.PrediagEndTime = tmpPrediagnose.PrediagEndTime;
                // $scope.asb.startDate = $scope.mData.AppointmentDate;
                $scope.mData.FullDate = $scope.asb.startDate;
                $scope.stallAsb = $scope.mData.StallId;
                console.log("curDate", currentDate);
                console.log("$scope.asb.startDate", $scope.asb.startDate, tmpPrediagnose);
                showBoardAsbGr($scope.mData.FullDate);
            }
            // $scope.estimateAsb = function() {
            //     if (gridTemp.length > 0) {
            //         var totalW = 0;
            //         for (var i = 0; i < gridTemp.length; i++) {
            //             if (gridTemp[i].ActualRate !== undefined) {
            //                 totalW += gridTemp[i].ActualRate;
            //             };
            //         };
            //         $scope.visibleAsb = 1;
            //         $scope.tmpActRate = totalW;
            //         $scope.mData.EstimateHour = totalW;
            //         console.log("gridTemp.length", gridTemp.length, $scope.visibleAsb);

        //     } else {
        //         $scope.tmpActRate = 1;
        //         $scope.visibleAsb = 0;
        //         $scope.mData.EstimateHour = 1;
        //     };
        //     $scope.checkAsb = true;
        //     $scope.asb.startDate = "";
        //     currentDate = new Date();
        //     // currentDate.setDate(currentDate.getDate() + 1);
        //     // $scope.minDateASB = new Date(currentDate);
        //     $scope.currentDate = new Date(currentDate);
        //     // currentDate.setDate(currentDate.getDate() + 1);
        //     // $scope.currentDate = new Date(currentDate);
        //     // $scope.asb.startDate = new Date(currentDate);
        //     console.log('appointment date', $scope.mData.EstimationDate);
        //     if ($scope.mData.JobId !== undefined && $scope.mData.JobId !== null) {
        //         var tmpDate = $scope.mData.EstimationDate;
        //         var tmpTime = tmpPrediagnose.ScheduledTime;
        //         var tmphour
        //         var tmpMinute
        //         var tmpFinalTime = "00:00:00"
        //         if (tmpTime !== undefined && tmpTime !== null ) {
        //             tmphour = tmpTime.getHours();
        //             tmpMinute = tmpTime.getMinutes();
        //             tmpFinalTime = tmphour+":"+tmpMinute+":"+"00";
        //         }
        //         // console.log("tmpTime",new Date(tmpTime));
        //         var finalDate
        //         var yyyy = tmpDate.getFullYear().toString();
        //         var mm = (tmpDate.getMonth() + 1).toString(); // getMonth() is zero-based         
        //         var dd = tmpDate.getDate().toString();
        //         finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0])  + " " + tmpFinalTime;
        //         var d = new Date(finalDate);

        //         $scope.asb.startDate = d;
        //         $scope.currentDate = tmpDate;
        //         console.log("dateBro", tmpDate);
        //     } else {
        //         // $scope.asb.startDate = new Date(currentDate);
        //         $scope.asb.startDate = $scope.mData.EstimationDate;
        //         $scope.currentDate = new Date(currentDate);
        //         $scope.mData.EstimationDate = new Date(currentDate);
        //     };
        //     // $scope.asb.startDate = $scope.mData.EstimationDate;
        //     // ===============================
        //     var timeToDateStart = $scope.asb.startDate;
        //     var timeToDateFinish = $scope.asb.startDate;
        //     var timePlanStart = $scope.mData.PlanStart;
        //     var timePlanFinish = $scope.mData.PlanFinish;
        //     if (($scope.mData.PlanStart !== null && $scope.mData.PlanStart !== undefined) && $scope.mData.PlanFinish !== null && $scope.mData.PlanFinish !== undefined) {
        //         timePlanStart = timePlanStart.split(":");
        //         timePlanFinish = timePlanFinish.split(":");
        //         timeToDateStart = new Date(timeToDateStart.setHours(timePlanStart[0], timePlanStart[1], timePlanStart[2]));
        //         timeToDateFinish = new Date(timeToDateFinish.setHours(timePlanFinish[0], timePlanFinish[1], timePlanFinish[2]));
        //     } else {
        //         timeToDateStart = $scope.mData.PlanStart;
        //         timeToDateFinish = $scope.mData.PlanFinish;
        //     }
        //     // ===============================
        //     $scope.asb.JobStallId = $scope.mData.StallId;
        //     if(timeToDateStart == null || timeToDateStart == undefined){
        //         $scope.asb.JobStartTime = $scope.asb.startDate;
        //     }else{
        //         $scope.asb.JobStartTime = timeToDateStart;
        //     }
        //     $scope.asb.JobEndTime = timeToDateFinish;

        //     if (tmpPrediagnose.PrediagStallId !== undefined) {
        //         $scope.asb.PrediagStallId = tmpPrediagnose.PrediagStallId;
        //     } else {
        //         $scope.asb.PrediagStallId = $scope.mData.PrediagStallId
        //     }

        //     if (tmpPrediagnose.ScheduledTime !== undefined && tmpPrediagnose.ScheduledTime !== null) {
        //         $scope.asb.PrediagStartTime = tmpPrediagnose.ScheduledTime;
        //     } else {
        //         $scope.asb.PrediagStartTime = $scope.mData.PrediagScheduledTime;
        //     }
        //     if($scope.mData.LicensePlate !== undefined && $scope.mData.LicensePlate !== null){
        //         $scope.mData.LicensePlate = $scope.mData.LicensePlate.toUpperCase();
        //     }
        //     $scope.asb.preDiagnose = tmpPrediagnose;
        //     // $scope.stallAsb = $scope.mData.StallId;
        //     $scope.mData.FullDate = $scope.asb.startDate;
        //     $scope.stallAsb = $scope.mData.StallId;
        //     console.log("Estimate ASB curDate", currentDate);
        //     console.log("Estimate ASB $scope.asb.startDate", $scope.asb.startDate);
        //     console.log("Estimate ASB $scope.stallAsb", $scope.stallAsb);
        //     console.log("Estimate ASB tmpPrediagnose",tmpPrediagnose);
        //     console.log("Estimate ASB $scope.asb",$scope.asb);
        //     showBoardAsbGr($scope.mData.FullDate);

        // };
        $scope.alertAfterSave = function(item, JobId) {
            bsAlert.alert({
                    title: "Data tersimpan dengan Nomor Estimasi",
                    text: item.EstimationNo,
                    type: "warning",
                    showCancelButton: false
                },
                function() {
                    $scope.printPreview(JobId);
                    $scope.formApi.setMode('grid');
                    //reload after create new is here
                    if ($scope.filter.typeCategory == 1 && $scope.filter.LicensePlate !== "" && $scope.filter.LicensePlate !== undefined) {
                        $scope.getData();
                    } else if ($scope.filter.typeCategory == 2 && $scope.filter.firstDate != null && $scope.filter.lastDate != null) {
                        $scope.getData();
                    }
                },
                function() {

                }
            )
        }

        $scope.onAfterSaveTes = function() {
            console.log("onAfterSaveTesonAfterSaveTesonAfterSaveTesonAfterSaveTes");
        }
        $scope.onNoResult = function() {
            console.log("gak", $scope.lmModel);
            $scope.listApi.clearDetail();
            var row = angular.copy($scope.lmModel);
            row.FlatRate = "";
            row.Fare = "";
            row.Summary = "";
            row.TaskId = null;
            row.tmpTaskId = null;
            row.ActualRate = null;
            row.catName = $scope.tmpCatg.Name;
            // row.cusType =$scope.tmpCus.Name;
            $scope.listApi.addDetail(false, row);
            console.log("uunnnnchhh", $scope.lmModel)
            $scope.jobAvailable = false;
            $scope.PriceAvailable = false;
            $scope.FlatRateAvailable = false;
        };
        $scope.onNoPartResult = function() {
            console.log("onGotResult=>");
            $scope.partsAvailable = false;
            $scope.ldModel.PartsName = null;
            $scope.ldModel.RetailPrice = null;
            $scope.ldModel.PercentDP = null;
            $scope.ldModel.ETA = null;
            $scope.ldModel.SatuanId = 4;
            $scope.ldModel.Availbility = null;
            $scope.ldModel.satuanName = null;
            // $scope.ldModel.paidName = null;
        }
        $scope.selectTypeParts = function(data) {
            $scope.ldModel.SatuanId = 4;
            _.map($scope.unitData,function(val){
                if(val.MasterId == 4){
                    $scope.ldModel.satuanName = val.Name;
                }
            });
        }
        $scope.onGotResult = function() {
            console.log("onGotResult=>");
        }
        $scope.selected = {};
        $scope.selectTypeWork = function(data) {
            $scope.lmModel.JobTypeId = data.MasterId;
            console.log("selectTypeWork", data);
            if (data.MasterId == 59 || data.MasterId == 60) {

                var cek_jml_job_War = 0
                if ($scope.gridWork.length > 0) {
                    for (var i=0; i<$scope.gridWork.length; i++) {
                        if ($scope.gridWork[i].PaidById == 30) {
                            cek_jml_job_War++
                        }
                    }
                }

                if (cek_jml_job_War > 0) {
                    bsAlert.warning('Claim Warranty hanya untuk 1 Job Openo, jika ada penambahan silahkan gunakan fitur Add Openo')
                    var xdat = {}
                    xdat.MasterId = null
                    xdat.Name = null
                    $scope.selectTypeWork(xdat,null)
                    return;
                }
                $scope.disT1 = false;
            } else {
                $scope.disT1 = true;
            }
            if ($scope.lmModel.JobTypeId != 60 && $scope.lmModel.JobTypeId != 59 && $scope.lmModel.JobTypeId != 2675 && $scope.lmModel.JobTypeId != 2678) {
                $scope.paymentData = $scope.somePaymentData;
            }else{
                $scope.paymentData = $scope.paymentDataThereIstWarranty;
            }
            $scope.tmpCatg = data;
            var tmpModel = {};
            var tmpCatgName = data.Name
            tmpModel.catName = data.Name;
            if (tmpCatgName == "TWC" || tmpCatgName == "PWC") {
                $scope.JobIsWarranty = 1;
                _.map($scope.paymentData, function(val) {
                    console.log("valll tipe", val);
                    if (val.Name === "Warranty") {
                        $scope.tmpPaidById = val.MasterId;
                        $scope.tmpPaidName = val.Name;
                        tmpModel.JobTypeId = data.MasterId;
                        tmpModel.PaidById = val.MasterId;
                        $scope.lmModel.PaidById = 30;
                        $scope.lmModel.PaidBy = 'Warranty';
                        tmpModel.TaskName = null;
                        tmpModel.FlatRate = null;
                        tmpModel.Fare = null;
                        tmpModel.ActualRate = null;
                        tmpModel.Summary = null;
                        $scope.listApi.addDetail(false, tmpModel);
                    }
                })
            }else if (tmpCatgName == "FLC - Free Labor Claim") {
                $scope.JobIsWarranty = 2;
                if ($scope.lmModel.PaidById == 30) {
                  $scope.ldModel.PaidById = null;
                }
                // _.map($scope.paymentDataPart, function(val){
                //   if (val.Name !== "Warranty") {
                //     console.log('cek val flc', val);
                //     $scope.tmpPaidById = val.MasterId;
                //     $scope.tmpPaidName = val.Name;
                //     tmpModel.JobTypeId = data.MasterId;
                //     tmpModel.PaidById = null;
                //     tmpModel.TaskName = null;
                //     tmpModel.FlatRate = null;
                //     tmpModel.Fare = null;
                //     tmpModel.ActualRate = null;
                //     tmpModel.Summary = null;
                //     $scope.listApi.addDetail(false, tmpModel);
                //   }
                // })
                _.map($scope.paymentDataThereIstWarranty, function(val){
                  if (val.Name === "Warranty") {
                    console.log('cek val flc', val);
                    $scope.tmpPaidById = val.MasterId;
                    $scope.tmpPaidName = val.Name;
                    tmpModel.JobTypeId = data.MasterId;
                    tmpModel.PaidById = val.MasterId;
                    $scope.lmModel.PaidById = 30;
                    $scope.lmModel.PaidBy = 'Warranty';
                    tmpModel.TaskName = null;
                    tmpModel.FlatRate = null;
                    tmpModel.Fare = null;
                    tmpModel.ActualRate = null;
                    tmpModel.Summary = null;
                    $scope.listApi.addDetail(false, tmpModel);
                  }
                })
            }else if(tmpCatgName == "FPC - Free Parts Claim"){
                $scope.JobIsWarranty = 3;
                if ($scope.ldModel.PaidById == 30) {
                  $scope.lmModel.PaidById = '';
                }
                $scope.lmModel.PaidById = '';
                // _.map($scope.paymentDataThereIstWarranty, function(val){
                //   if (val.Name === "Warranty") {
                //     console.log('cek val part', val);
                //     $scope.tmpPaidById = val.MasterId;
                //     $scope.tmpPaidName = val.Name;
                //     $scope.lmModel.PaidById = '';
                //     tmpModel.JobTypeId = data.MasterId;
                //     tmpModel.PaidById = val.MasterId;
                //     tmpModel.TaskName = null;
                //     tmpModel.FlatRate = null;
                //     tmpModel.Fare = null;
                //     tmpModel.ActualRate = null;
                //     tmpModel.Summary = null;
                //     $scope.listApi.addDetail(false, tmpModel);
                //   }
                // })
                // _.map($scope.paymentDataIsNotWarranty, function(val){
                //   if (val.Name !== "Warranty") {
                //     console.log('cek val job', val);
                //     console.log('masuk sini fpc');
                //     $scope.tmpPaidById = null;
                //     tmpModel.JobTypeId = data.MasterId;
                //     tmpModel.PaidById = null;
                //     tmpModel.TaskName = null;
                //     tmpModel.FlatRate = null;
                //     tmpModel.Fare = null;
                //     tmpModel.ActualRate = null;
                //     tmpModel.Summary = null;
                //     $scope.listApi.addDetail(false, tmpModel);
                //   }
                // })
                tmpModel.JobTypeId = data.MasterId;
                $scope.tmpPaidById = null;
                tmpModel.PaidById = null;
                tmpModel.TaskName = null;
                tmpModel.FlatRate = null;
                tmpModel.Fare = null;
                tmpModel.ActualRate = null;
                tmpModel.Summary = null;
                tmpModel.DiscountTypeId = -1;
                tmpModel.DiscountTypeListId = null;
                tmpModel.Discount = 0;
                $scope.listApi.addDetail(false, tmpModel);
            }else if(tmpCatgName == "PLC - Parts Labor Claim"){
                $scope.JobIsWarranty = 4;
                _.map($scope.paymentDataThereIstWarranty, function(val){
                    if (val.Name === "Warranty") {
                      console.log('cek val flc', val);
                      $scope.tmpPaidById = val.MasterId;
                      $scope.tmpPaidName = val.Name;
                      tmpModel.JobTypeId = data.MasterId;
                      tmpModel.PaidById = val.MasterId;
                      $scope.lmModel.PaidById = val.MasterId;
                      $scope.lmModel.PaidBy = "Warranty";
                      tmpModel.TaskName = null;
                      tmpModel.FlatRate = null;
                      tmpModel.Fare = null;
                      tmpModel.ActualRate = null;
                      tmpModel.Summary = null;
                      $scope.listApi.addDetail(false, tmpModel);
                    }
                
                  })
            } else {
                // $scope.paymentData = $scope.somePaymentData;
                tmpModel.JobTypeId = data.MasterId;
                $scope.tmpPaidById = null;
                $scope.lmModel.PaidById = null;
                tmpModel.PaidById = null;
                tmpModel.TaskName = null;
                tmpModel.FlatRate = null;
                tmpModel.Fare = null;
                tmpModel.ActualRate = null;
                tmpModel.Summary = null;
                $scope.listApi.addDetail(false, tmpModel);
                // $scope.listApi.addDetail(false, $scope.lmModel);
            }
            console.log("gridTemp", gridTemp);
            console.log("gridWork", $scope.lmModel);
        };

        $scope.tableListClear = function(){
            $scope.tempVIN = null;
            $scope.tempListClaim = null;
        }

        $scope.CheckedVIN = function() {
            console.log('mklm')
            if ($scope.mData.VIN != null && $scope.mData.VIN != "" && $scope.mData.VIN != undefined) {
                var count = $scope.mData.VIN;
                console.log('mdata vin', count.length)
                    if (count.length <= 17) {
                        EstimasiFactory.getDataVIN($scope.mData.VIN).then(function(res){
                            console.log('cek input rangka', $scope.mData.VIN)
                            var vinMasterId = res.data.Result
                            $scope.tempVIN = res.data.Result[0]
                            if ($scope.tempVIN.DEC_Date != null && $scope.tempVIN.DEC_Date != undefined) {
                                $scope.tempVIN.DEC_Date = $filter('date')($scope.tempVIN.DEC_Date, 'dd/MM/yyyy')
                            }else{
                                $scope.tempVIN.DEC_Date = '-'
                            }
                            console.log('cek res rangka', $scope.tempVIN)
                            if ($scope.tempVIN.DEC_Date != null && $scope.tempVIN.DEC_Date != undefined) {
                                $scope.tempVIN.DEC_Date = $filter('date')($scope.tempVIN.DEC_Date, 'dd/MM/yyyy')
                            }else{
                                $scope.tempVIN.DEC_Date = '-'
                            }
                            for(var i = 0; i < vinMasterId.length; i++){
                                if ($scope.mData.VIN != null && $scope.mData.VIN != "") {
                                    if (vinMasterId[i].MasterId == 2678) {
                                        $scope.taskCategory = $scope.notFlcFpc;
                                    }else if(vinMasterId[i].MasterId == 2677){
                                        $scope.taskCategory = $scope.notFpcPlc
                                    }else if(vinMasterId[i].MasterId == 2675){
                                        $scope.taskCategory = $scope.notFlcPlc
                                    }else{
                                        $scope.taskCategory = $scope.notFlcPlcFpc
                                    }
                                }else{
                                    $scope.taskCategory = $scope.notFlcPlcFpc
                                }
                            }
                                console.log('cek res input', $scope.tempVIN)
                                console.log('$scope.taskCategory ', $scope.taskCategory )
                        })

                        EstimasiFactory.listHistoryClaim($scope.mData.VIN).then(function(res){
                            // res.data.data = $scope.ubahFormatTanggalIndo(res.data.data.historyClaimDealerList)
                            $scope.listClaim = res.data.data;
                            $scope.tempListClaim = [];
                            // $scope.historyDate = new Date($scope.tempListClaim.claimDate)
                            console.log('cek list claim', res)
                            console.log('list claim', res.data.data)
                            console.log('list history claim', $scope.tempListClaim)
                            for (var i = 0; i < $scope.listClaim.historyClaimDealerList.length; i++) {
                                $scope.tempListClaim.push($scope.listClaim.historyClaimDealerList[i]);
                            }
                        })

                    }else{
                        console.log('tanpa service')
                        $scope.taskCategory = $scope.notFlcPlcFpc
                        $scope.tableListClear();
                    }
            }else {
                console.log('tanpa service')
                $scope.taskCategory = $scope.notFlcPlcFpc
                $scope.tableListClear();
            }   
        }

        $scope.selectTypeCust = function(data) {
            console.log("selectTypeCust", data);
            $scope.tmpCus = data;
            $scope.lmModel.PaidBy = data.Name;
            $scope.lmModel.PaidById = data.MasterId;
        }
        $scope.selectTypeUnitParts = function(data) {
            console.log("selectTypeUnitParts", data);
            $scope.tmpUnit = data;
            $scope.ldModel.satuanName = data.Name;
            console.log("selectTypeUnitParts", $scope.ldModel);
        }
        $scope.selectTypePaidParts = function(data) {
            console.log('checked selectTypePaidParts', data)
            if($scope.gridPartsDetail.data.length > 0){
                for (var i = 0; i < $scope.gridPartsDetail.data.length; i++) {
                    if (($scope.ldModel.PartsCode !== null && $scope.ldModel.PartsCode !== undefined) && $scope.ldModel.PartsCode == $scope.gridPartsDetail.data[i].PartsCode && $scope.ldModel.PaidById == $scope.gridPartsDetail.data[i].PaidById ) {
                        console.log('parts sudah ada');
                        // bsNotify.show({
                        //     size: 'big',
                        //     title: 'Parts Sudah Ada!',
                        //     text: 'I will close in 2 seconds.',
                        //     timeout: 2000,
                        //     type: 'danger'
                        // });
                        bsAlert.warning('Parts Sudah Ada!','Silahkan ganti tipe pembayaran');
                        $scope.ldModel.PaidById = null;
                    }else{
                        console.log("selectTypePaidParts", data);
                        $scope.tmpPaidName = data.Name;
                        $scope.ldModel.PaidBy = data.Name;
                        $scope.ldModel.PaidById = data.MasterId;
                        console.log("selectTypePaidParts", $scope.ldModel);
                    }
                }
            }else{
                console.log("selectTypePaidParts ELSE ===>",data);
                $scope.tmpPaidName = data.Name;
                $scope.ldModel.PaidBy = data.Name;
                $scope.ldModel.PaidById = data.MasterId;
                console.log("selectTypePaidParts", $scope.ldModel);
            }
            
        }
        $scope.onListSave = function(item) {
            console.log("save_modal=>", item);
            // if(item[$scope.id]){ //Update
            //   //find the master Id
            //   var key = {};
            //   key[$scope.mId] = item[$scope.mId];
            //   var match = _.find($scope.contacts, key);
            //   var xitem = angular.copy(item);
            //   if(match){
            //       var index = _.indexOf($scope.contacts, _.find($scope.contacts, key));
            //       $scope.contacts.splice(index, 1, xitem);
            //   }
            // }else{
            //   $scope.contacts.push(item);
            // }
        }
        $scope.onListCancel = function(item) {
            console.log("cancel_modal=>", item);
        }
        $scope.onAfterCancel = function() {
            $scope.jobAvailable = false;
        }
        $scope.onShowModal = function(mode, data) {
            console.log("====", mode);
            console.log("=====", data);
        }
        $scope.onBeforeSave = function(data, mode) {
            console.log("onBeforeSave data", data);
            console.log("onBeforeSave mode", mode);
            // for (var i = 0; i < data.child.length; i++) {
            //     data.child[i].PaidBy = $scope.ldModel.PaidBy;
            // }
            var tmpData = angular.copy(data);
            var tmpGridWork = $scope.gridWork;
            var lengthGrid = tmpGridWork.length - 1;
            if ($scope.tmpCus !== undefined) {
                tmpData.PaidBy = $scope.tmpCus.Name;
                tmpData.PaidById = $scope.tmpCus.MasterId;
            }
            // tmpData.Summary = tmpData.Fare * tmpData.FlatRate;
            tmpData.Summary = Math.round(tmpData.Fare * tmpData.FlatRate);
            // tmpData.PaidBy = $scope.tmpCus.Name;
            if (mode == 'new') {
                if (tmpData.tmpTaskId == null) {
                    if (tmpGridWork.length > 0) {
                        tmpData.tmpTaskId = tmpGridWork[lengthGrid].tmpTaskId + 1;
                    } else {
                        tmpData.tmpTaskId = 1;
                    }
                }
                if (data.child.length > 0) {
                    var tmpDataChild = angular.copy(data.child);
                    for (var i = 0; i < tmpDataChild.length; i++) {
                        delete data.child[i];
                        if (isNaN(tmpDataChild[i].subTotal)) {
                            tmpDataChild[i].subTotal = 0;
                            tmpDataChild[i].RetailPrice = null;
                        }
                    }
                    $scope.listApi.addDetail(tmpDataChild, tmpData);
                } else {
                    $scope.listApi.addDetail(false, tmpData);
                };


            } else {
                $scope.listApi.addDetail(false, tmpData);
            }
        }
        $scope.onAfterSave = function(data, mode) {
            console.log("onAfterSave data", data);
            console.log("onAfterSave mode", mode);
            console.log("ini sih wkwwk", $scope.taskCategory);
            
            if (data.catName == "PLC - Parts Labor Claim" || data.catName == "TWC" || data.catName == "PWC" || data.catName == "FLC - Free Labor Claim") {
                data.PaidById = $scope.lmModel.PaidById
                data.PaidBy = $scope.lmModel.PaidBy
            }

            if (data.JobTypeId == 59 || data.JobTypeId == 60 || data.JobTypeId == 2686 || data.JobTypeId == 2675 || data.JobTypeId == 2677 || data.JobTypeId == 2678) {
                if (data.TaskId == null && data.TaskId == undefined) {
                    var tempJobList = data.JobTypeId;
                    var index = $scope.gridWork.indexOf(tempJobList);
                    $scope.gridWork.splice(index, 1);
                    bsAlert.warning("Input pekerjaan Warranty & Free Claim harus menggunakan Openo / Tasklist, silakan ubah pekerjaan terlebih dahulu") 
                }
            }

            if (data.TaskName != null && data.TaskName != undefined) {
                if (data.TaskName.includes('|')) {
                    var split_TaskName = data.TaskName.split('|')
                    var hasilSplitTaskName = split_TaskName[0].trim()
                }
            }

            console.log('cek hasil split taskName', hasilSplitTaskName);
            if (hasilSplitTaskName != '1000km') {
                if (data.catName == "PLC - Parts Labor Claim" || data.catName == "FPC - Free Parts Claim") {
                    for (var i = 0; i < $scope.gridWork.length; i++) {
                        $scope.tempPart = $scope.gridWork[i];
                    }
                    console.log('data part child', $scope.tempPart.child)
                    console.log('data part', $scope.tempPart)
                    if($scope.tempPart.child == undefined || $scope.tempPart.child == null || $scope.tempPart.child == "") {   
                        bsAlert.warning('Harus ada Part, dengan Pembayaran Warranty, Karena tipe Pekerjaan adalah ' + $scope.tempPart.catName)
                        $scope.gridWork = [];   
                    }else{
                        console.log('gaada parts gausah kesini')
                    }
                }
            }

            var totalW = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].Summary !== undefined && gridTemp[i].PaidById !== 30) {
                    if (gridTemp[i].Summary == "") {
                        gridTemp[i].Summary = gridTemp[i].Fare;
                    }
                    // totalW += gridTemp[i].Summary;
                    totalW += ASPricingEngine.calculate({
                                InputPrice: gridTemp[i].Summary,
                                // Discount: 5,
                                // Qty: 2,
                                Tipe: 2, 
                                PPNPercentage: PPNPerc,
                            });
                }
            };
            // $scope.totalWork = totalW / (1.1);
            $scope.totalWork = totalW 
            

            // var totalM = 0;
            // for (var i = 0; i < gridTemp.length; i++) {
            //     if (gridTemp[i].child !== undefined) {
            //         for (var j = 0; j < gridTemp[i].child.length; j++) {
            //             if (gridTemp[i].child[j].subTotal !== undefined && !isNaN(gridTemp[i].child[j].subTotal)) {
            //                 totalM += gridTemp[i].child[j].subTotal;
            //                 console.log("gridTemp[i].child.Price", gridTemp[i].child[j].subTotal);
            //             }
            //         }
            //     }
            // }
            // $scope.totalMaterial = totalM / (1.1);
            var totalDp = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].DownPayment !== undefined && !isNaN(gridTemp[i].child[j].DownPayment !== NaN)) {
                            totalDp += gridTemp[i].child[j].DownPayment;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].DownPayment);
                        }
                    }
                }
            }
            $scope.totalDp = totalDp;


            var totalMaterial_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30){
                            // totalMaterial_byKevin += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                            totalMaterial_byKevin += 0
                        }else{
                            // if($scope.gridWork[i].child[j].MaterialTypeId == 1){ //parts
                            //     totalMaterial_byKevin += Math.round(($scope.gridWork[i].child[j].RetailPrice - ($scope.gridWork[i].child[j].Discount/100))/1.1) * $scope.gridWork[i].child[j].Qty;
                            // }else{ //bahan ga ada diskonnya
                            //     totalMaterial_byKevin += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                            // }
                            totalMaterial_byKevin += ASPricingEngine.calculate({
                                                        InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                        // Discount: 5,
                                                        Qty: $scope.gridWork[i].child[j].Qty,
                                                        Tipe: 102, 
                                                        PPNPercentage: PPNPerc,
                                                    }); 
                        }
                    }
                }
            };

            $scope.totalMaterial    = totalMaterial_byKevin;
            // $scope.totalPPN         = Math.floor(($scope.totalMaterial +  $scope.totalWork) * (10/100));   
            $scope.totalPPN         = ASPricingEngine.calculate({
                                            InputPrice: (($scope.totalMaterial +  $scope.totalWork) * (1+PPNPerc/100)),
                                            // Discount: 0,
                                            // Qty: 0,
                                            Tipe: 33, 
                                            PPNPercentage: PPNPerc,
                                        });    
            $scope.totalEstimasi    = $scope.totalWork + $scope.totalMaterial + $scope.totalPPN  
            console.log("gridTemp", gridTemp);
            console.log("gridWork", $scope.gridWork);
            $scope.estimateAsb();
        };

        $scope.onAfterDelete = function() {
            $scope.sumAllPrice();
        }
        $scope.sumAllPrice = function() {
            var totalW = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].Summary !== undefined) {
                    if (gridTemp[i].Summary == "") {
                        gridTemp[i].Summary = gridTemp[i].Fare;
                    }
                    // totalW += gridTemp[i].Summary;
                    totalW += ASPricingEngine.calculate({
                                InputPrice: gridTemp[i].Summary,
                                // Discount: 5,
                                // Qty: 2,
                                Tipe: 2, 
                                PPNPercentage: PPNPerc,
                            });
                }
            }
            // $scope.totalWork = totalW / (1.1);
            $scope.totalWork = totalW 
           

            // var totalM = 0;
            // for (var i = 0; i < Parts.length; i++) {
            //     if (Parts[i].subTotal !== undefined) {
            //         totalM += Parts[i].subTotal;
            //         console.log("gridTemp[i].child.Price", Parts[i].subTotal);
            //     }
            // }
            // $scope.totalMaterial = totalM /(1.1);

            var totalDp = 0;
            for (var i = 0; i < gridTemp.length; i++) {
                if (gridTemp[i].child !== undefined) {
                    for (var j = 0; j < gridTemp[i].child.length; j++) {
                        if (gridTemp[i].child[j].DownPayment !== undefined && !isNaN(gridTemp[i].child[j].DownPayment !== NaN)) {
                            totalDp += gridTemp[i].child[j].DownPayment;
                            console.log("gridTemp[i].child.Price", gridTemp[i].child[j].DownPayment);
                        }
                    }
                }
            }
            $scope.totalDp = totalDp;


            var totalMaterial_byKevin = 0;
            for (var i = 0; i < $scope.gridWork.length; i++) {
                if ($scope.gridWork[i].child !== undefined) {
                    for (var j = 0; j < $scope.gridWork[i].child.length; j++) {
                        if($scope.gridWork[i].child[j].PaidById == 2277 || $scope.gridWork[i].child[j].PaidById == 30 ){
                            // totalMaterial_byKevin += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                            totalMaterial_byKevin += 0
                        }else{
                            // if($scope.gridWork[i].child[j].MaterialTypeId == 1){ //parts
                            //     totalMaterial_byKevin += Math.round(($scope.gridWork[i].child[j].RetailPrice - ($scope.gridWork[i].child[j].Discount/100))/1.1) * $scope.gridWork[i].child[j].Qty;
                            // }else{ //bahan ga ada diskonnya
                            //     totalMaterial_byKevin += Math.round($scope.gridWork[i].child[j].RetailPrice/1.1) * $scope.gridWork[i].child[j].Qty;
                            // }
                            totalMaterial_byKevin += ASPricingEngine.calculate({
                                                        InputPrice: $scope.gridWork[i].child[j].RetailPrice,
                                                        // Discount: 5,
                                                        Qty: $scope.gridWork[i].child[j].Qty,
                                                        Tipe: 102, 
                                                        PPNPercentage: PPNPerc,
                                                    }); 
                        }
                    }
                }
            };

            $scope.totalMaterial    = totalMaterial_byKevin;
            // $scope.totalPPN         = Math.floor(($scope.totalMaterial +  $scope.totalWork) * (10/100));
            $scope.totalPPN         = ASPricingEngine.calculate({
                                            InputPrice: (($scope.totalMaterial +  $scope.totalWork) * (1+PPNPerc/100)),
                                            // Discount: 0,
                                            // Qty: 0,
                                            Tipe: 33, 
                                            PPNPercentage: PPNPerc,
                                        }); 
            $scope.totalEstimasi    = $scope.totalWork + $scope.totalMaterial + $scope.totalPPN


            console.log("gridTemp", gridTemp);
            console.log("gridWork", $scope.gridWork);
        };
        $scope.checkPrice = function(data) {
            console.log("RetailPrice nya ===>", data);
            console.log("$scope.ldModel.Qty nya ==>", $scope.ldModel.Qty);
            var sum = 0;
            var sumDp = 0;
            if (data !== undefined && data !== null) {
                if ($scope.ldModel.Qty !== null) {
                    var qty = $scope.ldModel.Qty;
                    var tmpDp = $scope.ldModel.nightmareIjal ? $scope.ldModel.nightmareIjal : 0;
                    if (qty !== null) {
                        sum = Math.round(qty * data);
                        sumDp = Math.round(qty * tmpDp);
                        $scope.ldModel.subTotal = sum;
                        $scope.ldModel.DownPayment = sumDp;
                        $scope.ldModel.DPRequest = sumDp;
                        $scope.ldModel.minimalDp = sumDp;
                        $scope.ldModel.Discount = 0;

                    } else {
                        $scope.ldModel.subTotal = 0;
                        $scope.ldModel.Discount = 0;
                        $scope.ldModel.DownPayment = Math.round(tmpDp);
                        $scope.ldModel.DPRequest = Math.round(tmpDp);
                        $scope.ldModel.minimalDp = Math.round(tmpDp);
                    }
                }
            } else {
                $scope.ldModel.subTotal = 0;
                $scope.ldModel.Discount = 0;
                $scope.ldModel.DownPayment = 0;
                $scope.ldModel.DPRequest = 0;
                $scope.ldModel.minimalDp = 0;
            }
        };
        $scope.checkServiceRate = function(item, data) {
            console.log("item", item);
            console.log("item", data);
            var sum = 0;
            var row = {};
            var tmpItem;
            if (data == 59 || data == 60) {
                if ($scope.tmpServiceRate !== null) {
                    if (item !== null) {
                        tmpItem = item.toString();
                        if (tmpItem.includes(".")) {
                            tmpItem = tmpItem.split('.');
                            if (tmpItem[tmpItem.length - 1] !== ".") {
                                // item = parseInt(tmpItem);
                                sum = $scope.tmpServiceRate.WarrantyRate * item;
                                sum = Math.round(sum);
                            }
                        } else {
                            sum = $scope.tmpServiceRate.WarrantyRate * item;
                            sum = Math.round(sum);
                        }
                    } else {
                        sum = $scope.tmpServiceRate.WarrantyRate;
                        sum = Math.round(sum);
                    }
                    $scope.PriceAvailable = true;
                    sum = Math.round(sum);
                    row.Summary = sum;
                    row.Fare = $scope.tmpServiceRate.WarrantyRate;
                }
            } else {
                if ($scope.tmpServiceRate !== null) {
                    if (item !== null) {
                        tmpItem = item.toString();
                        if (tmpItem.includes(".")) {
                            tmpItem = tmpItem.split('.');
                            if (tmpItem[tmpItem.length - 1] !== ".") {
                                // item = parseInt(tmpItem);
                                sum = $scope.tmpServiceRate.ServiceRate * item;
                                sum = Math.round(sum);
                            }
                        } else {
                            sum = $scope.tmpServiceRate.ServiceRate * item;
                            sum = Math.round(sum);
                        }
                    } else {
                        sum = $scope.tmpServiceRate.ServiceRate;
                        sum = Math.round(sum);
                    }
                    $scope.PriceAvailable = true;
                    sum = Math.round(sum);
                    row.Summary = sum;
                    row.Fare = $scope.tmpServiceRate.ServiceRate;
                }
            }
            row.ActualRate = item;
            $scope.listApi.addDetail(false, row);
        }

        $scope.DisTipeOrder = false;
        $scope.checkIsOPB = function(data, obj){
            $timeout(function(){
                if(obj.IsOPB == 1){
                    console.log("=====", data, obj)
                    $scope.ldModel.PartsCode = null;
                    $scope.ldModel.PartId = null;
                    $scope.ldModel.PartsId = null;
                    $scope.ldModel.PartsName = null
                    $scope.partsAvailable = false;
                }else{
                    $scope.partsAvailable = true;
                }
            },100)
        }
        $scope.checkAvailabilityParts = function(data) {
            console.log("data data data Parts", data);
            if (data.PartsId !== undefined) {
                AppointmentGrService.getAvailableParts(data.PartsId, 0, $scope.ldModel.Qty).then(function(res) {

                    // ====== validasi cek kl retailprice nya null ato 0 ga blh di pake ==================================================== start
                    if (res.data.Result[0].RetailPrice === 0 || res.data.Result[0].RetailPrice === null || res.data.Result[0].RetailPrice === undefined){
                        bsAlert.warning('Harga Retail Belum Tersedia. Harap hubungi Partsman/Petugas Gudang Bahan.');
                        return
                    }
                    // ====== validasi cek kl retailprice nya null ato 0 ga blh di pake ==================================================== end
                    console.log("Ressss abis availability", res.data);
                    var tmpAvailability = res.data.Result[0];
                    $scope.ldModel.RetailPrice = tmpAvailability.RetailPrice;
                    $scope.ldModel.minimalDp = tmpAvailability.PriceDP;
                    $scope.ldModel.DPRequest = tmpAvailability.PriceDP;
                    $scope.ldModel.nightmareIjal = tmpAvailability.PriceDP;
                    if ($scope.ldModel.Qty !== undefined && $scope.ldModel.Qty !== null) {
                        $scope.checkPrice(tmpAvailability.RetailPrice);
                    }
                    if (tmpAvailability.isAvailable == 1) {
                        $scope.DisTipeOrder = true;
                    } else {
                        $scope.DisTipeOrder = false;
                    }

                    _.map($scope.paymentData,function(val){
                        console.log('warranty 1', val)
                        if(data.PaidById == 30){
                            if(val.MasterId == data.PaidById ){
                                $scope.ldModel.PaidBy = val.Name;
                                $scope.selectTypePaidParts(val);
                            }
                        }else{
                            if(val.MasterId == $scope.tmpPaidById && $scope.JobIsWarranty != 2 ){
                                $scope.ldModel.PaidBy = val.Name;
                                $scope.selectTypePaidParts(val);
                            }
                        }
                    })

                    // $scope.ldModel.priceDp = tmpAvailability.ValueDP;
                    $scope.ldModel.priceDp = tmpAvailability.PriceDP;
                    // if (tmpAvailability.isAvailable == 0) {
                    //     $scope.ldModel.Availbility = "Tidak Tersedia";
                    //     $scope.ldModel.ETA = tmpAvailability.DefaultETA;
                    //     $scope.ldModel.OrderType = 3;
                    // } else {
                    //     $scope.ldModel.Availbility = "Tersedia";
                    // }
                    switch (tmpAvailability.isAvailable) {
                        case 0:
                            $scope.DisTipeOrder = false;
                            $scope.ldModel.Availbility = "Tidak Tersedia";
                            if (tmpAvailability.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                $scope.ldModel.ETA = tmpAvailability.DefaultETA;
                            } else {
                                $scope.ldModel.ETA = tmpAvailability.DefaultETA;
                            }
                            $scope.ldModel.OrderType = 3;
                            $scope.ldModel.Type = 3;
                            data.OrderType = 3;
                            data.Type = 3;

                            break;
                        case 1:
                            $scope.DisTipeOrder = true;
                            $scope.ldModel.Availbility = "Tersedia";
                            break;
                        case 2:
                            $scope.DisTipeOrder = true;
                            $scope.ldModel.Availbility = "Tersedia Sebagian";
                            break;
                    }

                });
            }
        }
        $scope.getAvailablePartsService = function(item, row) {
                if (item.PartsId !== null) {
                    AppointmentGrService.getAvailableParts(item.PartsId).then(function(res) {
                        var tmpRes = res.data.Result[0];
                        console.log("tmpRes", tmpRes);
                        tmpParts = tmpRes;
                        if (tmpParts !== null) {
                            console.log("have data RetailPrice");
                            item.RetailPrice = tmpParts.RetailPrice;
                            item.subTotal = item.Qty * tmpParts.RetailPrice;
                            item.DownPayment = tmpParts.PriceDP * item.Qty;
                            item.MaterialTypeId = item.PartsClassId1;
                            item.DPRequest = tmpParts.PriceDP * item.Qty;
                            item.Discount = 0;
                            item.SatuanId = item.UomId;

                            item.nightmareIjal = item.minimalDp;
                            for (var i in $scope.unitData) {
                                if ($scope.unitData[i].MasterId == item.SatuanId) {
                                    item.satuanName = $scope.unitData[i].Name;
                                }
                            }
                            if (tmpParts.isAvailable == 0) {
                                item.Availbility = "Tidak Tersedia";
                                if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                    item.ETA = "";
                                } else {
                                    item.ETA = tmpParts.DefaultETA;
                                }

                                item.OrderType = 3;
                            } else {
                                item.Availbility = "Tersedia";
                            }
                        } else {
                            console.log("haven't data RetailPrice");
                        }
                        console.log('$scope.tmpPaidById', $scope.tmpPaidById)
                        if ($scope.tmpPaidById !== null) {
                            item.PaidById = $scope.tmpPaidById;
                            item.PaidBy = $scope.tmpPaidName;
                        }

                        if (row.catName == 'FPC - Free Parts Claim' || row.catName == 'PLC - Parts Labor Claim') {
                            console.log('imam sini')
                            for (var i in $scope.paymentDataThereIstWarranty) {
                                console.log('paymentDataThereIstWarranty', $scope.paymentDataThereIstWarranty[i])
                                if ($scope.paymentDataThereIstWarranty[i].Name == "Warranty") {
                                    item.PaidById = $scope.paymentDataThereIstWarranty[i].MasterId;
                                    item.PaidBy = $scope.paymentDataThereIstWarranty[i].Name;
                                }
                            }
                        }else if(row.catName == 'FLC - Free Labor Claim'){
                            for (var i in $scope.paymentDataIsNotWarranty) {
                                console.log('paymentDataIsNotWarranty', $scope.paymentDataIsNotWarranty[i])
                                if ($scope.paymentDataIsNotWarranty[i].Name == "Customer") {
                                    item.PaidById = $scope.paymentDataIsNotWarranty[i].MasterId;
                                    item.PaidBy = $scope.paymentDataIsNotWarranty[i].Name;
                                }
                            }
                        }else{
                            for (var i in $scope.paymentData) {
                                if ($scope.paymentData[i].Name == "Customer") {
                                    item.PaidById = $scope.paymentData[i].MasterId;
                                    item.PaidBy = $scope.paymentData[i].Name;
                                }
                            }
                        }

                        tmp = item;
                        $scope.listApi.addDetail(tmp, row);
                        return item;
                    });
                } else {
                    console.log('$scope.tmpPaidById 2', $scope.tmpPaidById)
                    if ($scope.tmpPaidById !== null) {
                        item.PaidById = $scope.tmpPaidById;
                        item.PaidBy = $scope.tmpPaidName;
                    }
                    if (row.catName == 'FPC - Free Parts Claim' || row.catName == 'PLC - Parts Labor Claim') {
                        console.log('imam sini')
                        for (var i in $scope.paymentDataThereIstWarranty) {
                            console.log('paymentDataThereIstWarranty', $scope.paymentDataThereIstWarranty[i])
                            if ($scope.paymentDataThereIstWarranty[i].Name == "Warranty") {
                                item.PaidById = $scope.paymentDataThereIstWarranty[i].MasterId;
                                item.PaidBy = $scope.paymentDataThereIstWarranty[i].Name;
                            }
                        }
                    }else if(row.catName == 'FLC - Free Labor Claim'){
                        for (var i in $scope.paymentDataIsNotWarranty) {
                            console.log('paymentDataIsNotWarranty', $scope.paymentDataIsNotWarranty[i])
                            if ($scope.paymentDataIsNotWarranty[i].Name == "Customer") {
                                item.PaidById = $scope.paymentDataIsNotWarranty[i].MasterId;
                                item.PaidBy = $scope.paymentDataIsNotWarranty[i].Name;
                            }
                        }
                    }else{
                        for (var i in $scope.paymentData) {
                            if ($scope.paymentData[i].Name == "Customer") {
                                item.PaidById = $scope.paymentData[i].MasterId;
                                item.PaidBy = $scope.paymentData[i].Name;
                            }
                        }
                    }
                    item.subTotal = item.Qty * item.Price;
                    item.MaterialTypeId = item.PartsClassId1;
                    item.RetailPrice = item.Price;
                    item.subTotal = item.Qty * item.Price;
                    item.DPRequest = item.DPRequest * item.Qty;
                    item.Discount = 0;
                    item.SatuanId = item.UomId;

                    item.nightmareIjal = item.minimalDp;
                    for (var i in $scope.unitData) {
                        if ($scope.unitData[i].MasterId == item.SatuanId) {
                            item.satuanName = $scope.unitData[i].Name;
                        }
                    }
                    tmp = item;
                    $scope.listApi.addDetail(tmp, row);
                    return item;
                }
            }
            // $scope.getAvailablePartsServiceManual = function(item) {
            //  if(item.PartsId !== null){
            //     AppointmentGrService.getAvailableParts(item.PartsId).then(function(res) {
            //        if(res.data.Result.length > 0){
            //             var tmpRes = res.data.Result[0];
            //             tmpParts = tmpRes;
            //             if(tmpParts !== null){
            //                 console.log("have data RetailPrice");
            //                 item.RetailPrice = tmpParts.RetailPrice;
            //                 item.priceDp = tmpParts.PriceDP;
            //                 item.subTotal = item.Qty * item.RetailPrice;
            //                 if(tmpParts.isAvailable == 0){
            //                     item.Availbility = "Tidak Tersedia";
            //                     if(tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)"){
            //                         item.ETA = "";
            //                     }else{
            //                         item.ETA = tmpParts.DefaultETA;
            //                     }
            //                     item.Type = 3;
            //                 }else{
            //                     item.Availbility = "Tersedia";
            //                 }
            //             }else{
            //                 console.log("haven't data RetailPrice");
            //             }
            //             Parts.push(item);
            //             $scope.sumAllPrice();
            //             return item;
            //        }else{
            //            item.RetailPrice = item.Price;
            //            item.subTotal = item.Qty * item.Price;
            //            item.DownPayment = item.DownPayment * item.Qty; 
            //            tmp = item;
            //            Parts.push(item);
            //            $scope.sumAllPrice();
            //        }
            //     });
            //    }else{
            //     item.RetailPrice = item.Price;
            //     item.subTotal = item.Qty * item.Price;
            //     item.DownPayment = item.DownPayment * item.Qty; 
            //     tmp = item;
            //     Parts.push(item);
            //     $scope.sumAllPrice();
            //     return item;
            //    }
            // };
            $scope.dissallowSemicolon = function(event){
                var patternRegex = /[\x3B]/i;
                var keyCode = event.which || event.keyCode;
                var keyCodeChar = String.fromCharCode(keyCode);
                if (keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                    event.preventDefault();
                    return false;
                }
            }
            $scope.getAvailablePartsServiceManual = function(item, x, y) {
                console.log("getAvailablePartsServiceManual", item);
                console.log("x : ", x);
                console.log("y : ", y);
            
                if (x > item.length - 1) {
                    return item;
                }
                if (item[x].isDeleted !== 1) {
                    if (item[x].JobParts[y] !== undefined && item[x].JobParts[y].PartsId !== null) {
                        var itemTemp = item[x].JobParts[y];
                        AppointmentGrService.getAvailableParts(itemTemp.PartsId).then(function(res) {
                            var tmpParts = res.data.Result[0];
                            console.log('tmpParts', tmpParts)
                            if (tmpParts !== null && tmpParts !== undefined) {
                                // console.log("have data RetailPrice",item,tmpRes);
                                item[x].JobParts[y].RetailPrice = tmpParts.RetailPrice;
                                // item[x].JobParts[y].minimalDp = tmpParts.PriceDP;
                                item[x].JobParts[y].nightmareIjal = item[x].JobParts[y].minimalDp;
                                item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                // item.DiscountedPrice =  (normalPrice * item.Discount)/100;
                                if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                    item[x].JobParts[y].typeDiskon = -1;
                                    item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice;
                                } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                    item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].RetailPrice) * item[x].JobParts[y].Discount) / 100;
                                    item[x].JobParts[y].typeDiskon = 0;
                                }
                                if (tmpParts.isAvailable == 0) {
                                    item[x].JobParts[y].Availbility = "Tidak Tersedia";
                                    if (tmpParts.DefaultETA == "Mon Jan 01    1 00:00:00 GMT+0700 (SE Asia Standard Time)") {
                                        item[x].JobParts[y].ETA = "";
                                    } else {
                                        item[x].JobParts[y].ETA = tmpParts.DefaultETA;
                                    }
                                    item[x].JobParts[y].OrderType = 3;
                                } else {
                                    item[x].JobParts[y].Availbility = "Tersedia";
                                }
                            } else {
                                console.log("haven't data RetailPrice");
                                item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                                item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                                item[x].JobParts[y].NormalPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                                if (item[x].JobParts[y].IsCustomDiscount == 0 && item[x].JobParts[y].Discount == 0) {
                                    item[x].JobParts[y].typeDiskon = -1;
                                    item[x].JobParts[y].DiscountedPrice = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                                } else if (item[x].JobParts[y].IsCustomDiscount == 1 && item[x].JobParts[y].Discount !== 0) {
                                    item[x].JobParts[y].DiscountedPrice = ((item[x].JobParts[y].Qty * item[x].JobParts[y].Price) * item[x].JobParts[y].Discount) / 100;
                                    item[x].JobParts[y].typeDiskon = 0;
                                }
                            }
                            if (item[x].JobParts[y].PartsId !== null) {
                                item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                                delete item[x].JobParts[y].Part;
                            }
                            if (item[x].JobParts[y].PaidBy != undefined && item[x].JobParts[y].PaidBy != null) {
                                item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy.Name;
                            }
                            if (item[x].JobParts[y].IsOPB !== null) {
                                item[x].JobParts[y].isOPB = item[x].JobParts[y].IsOPB;
                            }
                            Parts.push(item[x].JobParts[y]);
                            $scope.sumAllPrice();
                            // return item;                    
            
                            if (x <= (item.length - 1)) {
                                if (y >= (item[x].JobParts.length - 1)) {
                                    if (item[x].Fare !== undefined) {
                                        var sum = item[x].Fare;
                                        var summ = item[x].FlatRate;
                                        // var summaryy = Math.ceil(sum * summ);
                                        var summaryy = Math.round(sum * summ);
                                        var discountedPrice = (summaryy * item[x].Discount) / 100;
                                        var normalPrice = summaryy
                                        summaryy = parseInt(summaryy);
                                    }
                                    if (item[x].JobType == null) {
                                        item[x].JobType = { Name: "" };
                                    }
                                    if (item[x].PaidBy == null) {
                                        item[x].PaidBy = { Name: "" }
                                    }
                                    if (item[x].AdditionalTaskId == null) {
                                        item[x].isOpe = 0;
                                    } else {
                                        item[x].isOpe = 1;
                                    }
                                    if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                        item[x].typeDiskon = -1;
                                        item[x].DiscountedPrice = normalPrice;
                                    } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                        item[x].typeDiskon = 0;
                                        item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                    }
                                    gridTemp.push({
                                        ActualRate: item[x].ActualRate,
                                        AdditionalTaskId: item[x].AdditionalTaskId,
                                        isOpe: item[x].isOpe,
                                        Discount: item[x].Discount,
                                        JobTaskId: item[x].JobTaskId,
                                        JobTypeId: item[x].JobTypeId,
                                        DiscountedPrice: discountedPrice,
                                        typeDiskon: item[x].typeDiskon,
                                        NormalPrice: normalPrice,
                                        catName: item[x].JobType.Name,
                                        Fare: item[x].Fare,
                                        IsCustomDiscount: item[x].IsCustomDiscount,
                                        Summary: summaryy,
                                        PaidById: item[x].PaidById,
                                        JobId: item[x].JobId,
                                        TaskId: item[x].TaskId,
                                        TaskName: item[x].TaskName,
                                        FlatRate: item[x].FlatRate,
                                        ProcessId: item[x].ProcessId,
                                        TFirst1: item[x].TFirst1,
                                        PaidBy: item[x].PaidBy.Name,
                                        index: "$$" + x,
                                        child: Parts
                                    });
                                    $scope.sumAllPrice();
                                    Parts.splice();
                                    Parts = [];
                                    console.log("if 1 nilai x", x);
                                    $scope.getAvailablePartsServiceManual(item, x + 1, 0);
            
                                } else {
                                    $scope.getAvailablePartsServiceManual(item, x, y + 1);
                                }
                            }
                        });
                    } else if (item[x].JobParts[y] !== undefined && (item[x].JobParts[y].PartsId == undefined || item[x].JobParts[y].PartsId == null)) {
                        item[x].JobParts[y].RetailPrice = item[x].JobParts[y].Price;
                        item[x].JobParts[y].subTotal = item[x].JobParts[y].Qty * item[x].JobParts[y].Price;
                        item[x].JobParts[y].DPRequest = item[x].JobParts[y].DPRequest * item[x].JobParts[y].Qty;
                        tmp = item[x].JobParts[y];
                        if (item[x].JobParts[y].PartsId !== null) {
                            item[x].JobParts[y].PartsCode = item[x].JobParts[y].Part.PartsCode;
                            delete item[x].JobParts[y].Part;
                        }
                        if (item[x].JobParts[y].PaidBy != undefined && item[x].JobParts[y].PaidBy != null) {
                            item[x].JobParts[y].PaidBy = item[x].JobParts[y].PaidBy.Name;
                        }
                        if (item[x].JobParts[y].IsOPB !== null) {
                            item[x].JobParts[y].isOPB = item[x].JobParts[y].IsOPB;
                        }
                        Parts.push(item[x].JobParts[y]);
                        $scope.sumAllPrice();
            
                        if (x <= (item.length - 1)) {
                            if (y >= (item[x].JobParts.length - 1)) {
                                if (item[x].Fare !== undefined) {
                                    var sum = item[x].Fare;
                                    var summ = item[x].FlatRate;
                                    // var summaryy = Math.ceil(sum * summ);
                                    var summaryy = Math.round(sum * summ);
                                    var discountedPrice = (summaryy * item[x].Discount) / 100;
                                    var normalPrice = summaryy
                                    summaryy = parseInt(summaryy);
                                }
                                if (item[x].JobType == null) {
                                    item[x].JobType = { Name: "" };
                                }
                                if (item[x].PaidBy == null) {
                                    item[x].PaidBy = { Name: "" }
                                }
                                if (item[x].AdditionalTaskId == null) {
                                    item[x].isOpe = 0;
                                } else {
                                    item[x].isOpe = 1;
                                }
                                if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                    item[x].typeDiskon = -1;
                                    item[x].DiscountedPrice = normalPrice;
                                } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                    item[x].typeDiskon = 0;
                                    item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
                                }
            
                                gridTemp.push({
                                    ActualRate: item[x].ActualRate,
                                    AdditionalTaskId: item[x].AdditionalTaskId,
                                    isOpe: item[x].isOpe,
                                    Discount: item[x].Discount,
                                    JobTaskId: item[x].JobTaskId,
                                    JobTypeId: item[x].JobTypeId,
                                    DiscountedPrice: discountedPrice,
                                    typeDiskon: item[x].typeDiskon,
                                    NormalPrice: normalPrice,
                                    catName: item[x].JobType.Name,
                                    Fare: item[x].Fare,
                                    IsCustomDiscount: item[x].IsCustomDiscount,
                                    Summary: summaryy,
                                    PaidById: item[x].PaidById,
                                    JobId: item[x].JobId,
                                    TaskId: item[x].TaskId,
                                    TaskName: item[x].TaskName,
                                    FlatRate: item[x].FlatRate,
                                    ProcessId: item[x].ProcessId,
                                    TFirst1: item[x].TFirst1,
                                    PaidBy: item[x].PaidBy.Name,
                                    index: "$$" + x,
                                    child: Parts
                                });
                                $scope.sumAllPrice();
                                Parts.splice();
                                Parts = [];
                                $scope.getAvailablePartsServiceManual(item, x + 1, 0);
                                console.log("if 2 nilai x", x);
                            } else {
                                $scope.getAvailablePartsServiceManual(item, x, y + 1);
                            }
                        }
                    } else if (item[x].JobParts.length == 0) {
                        if (x <= (item.length - 1)) {
                            if (y >= (item[x].JobParts.length - 1)) {
                                if (item[x].Fare !== undefined) {
                                    var sum = item[x].Fare;
                                    var summ = item[x].FlatRate;
                                    // var summaryy = Math.ceil(sum * summ);
                                    var summaryy = Math.round(sum * summ);
                                    var discountedPrice = (summaryy * item[x].Discount) / 100;
                                    var normalPrice = summaryy
                                    summaryy = parseInt(summaryy);
                                }
                                if (item[x].JobType == null) {
                                    item[x].JobType = { Name: "" };
                                }
                                if (item[x].PaidBy == null) {
                                    item[x].PaidBy = { Name: "" }
                                }
                                if (item[x].AdditionalTaskId == null) {
                                    item[x].isOpe = 0;
                                } else {
                                    item[x].isOpe = 1;
                                }
                                if (item[x].IsCustomDiscount == 0 && item[x].Discount == 0) {
                                    item[x].typeDiskon = -1;
                                    item[x].DiscountedPrice = normalPrice;
                                } else if (item[x].IsCustomDiscount == 1 && item[x].Discount !== 0) {
                                    item[x].typeDiskon = 0;
                                    item[x].DiscountedPrice = (normalPrice * item[x].Discount) / 100;
            
                                }
                                gridTemp.push({
                                    ActualRate: item[x].ActualRate,
                                    AdditionalTaskId: item[x].AdditionalTaskId,
                                    isOpe: item[x].isOpe,
                                    Discount: item[x].Discount,
                                    JobTaskId: item[x].JobTaskId,
                                    JobTypeId: item[x].JobTypeId,
                                    DiscountedPrice: discountedPrice,
                                    typeDiskon: item[x].typeDiskon,
                                    NormalPrice: normalPrice,
                                    catName: item[x].JobType.Name,
                                    Fare: item[x].Fare,
                                    IsCustomDiscount: item[x].IsCustomDiscount,
                                    Summary: summaryy,
                                    PaidById: item[x].PaidById,
                                    JobId: item[x].JobId,
                                    TaskId: item[x].TaskId,
                                    TaskName: item[x].TaskName,
                                    FlatRate: item[x].FlatRate,
                                    ProcessId: item[x].ProcessId,
                                    TFirst1: item[x].TFirst1,
                                    PaidBy: item[x].PaidBy.Name,
                                    index: "$$" + x,
                                    child: Parts
                                });
                                $scope.sumAllPrice();
                                Parts.splice();
                                Parts = [];
                                $scope.getAvailablePartsServiceManual(item, x + 1, 0);
                            } else {
                                $scope.getAvailablePartsServiceManual(item, x, y + 1);
                            }
                        }
                        // return item;                
                    }
            
                    if (x > item.length - 1) {
                        return item;
                    }
            
                    // gridTemp.push({
                    //     ActualRate: item[x].ActualRate,
                    //     AdditionalTaskId:item[x].AdditionalTaskId,
                    //     isOpe:item[x].isOpe,
                    //     Discount: item[x].Discount,
                    //     JobTaskId: item[x].JobTaskId,
                    //     JobTypeId: item[x].JobTypeId,
                    //     DiscountedPrice:$scope.discountedPrice,
                    //     typeDiskon: item[x].typeDiskon,
                    //     NormalPrice:$scope.normalPrice,
                    //     catName: item[x].JobType.Name,
                    //     Fare: item[x].Fare,
                    //     IsCustomDiscount: item[x].IsCustomDiscount,
                    //     Summary: $scope.summaryy,
                    //     PaidById: item[x].PaidById,
                    //     JobId: item[x].JobId,
                    //     TaskId: item[x].TaskId,
                    //     TaskName: item[x].TaskName,
                    //     FlatRate: item[x].FlatRate,
                    //     ProcessId: item[x].ProcessId,
                    //     TFirst1: item[x].TFirst1,
                    //     PaidBy: item[x].PaidBy.Name,
                    //     child: Parts
                    // });
                    // Parts.splice();
                    // console.log("tmpJob : ",item);
                } else {
                    $scope.getAvailablePartsServiceManual(item, x + 1, 0);
                }
            };



            $scope.ubahPartsCode = function(data){
                console.log('ubur ubur',data)
                // kata pa ari kl part yang ga pake partscode (manual), ga blh di kasi diskon pas release wo nya
                // hrs isi dr listodo dl partscode nya, baru kasi diskon di wo list
                if (data.PartsCode != null && data.PartsCode != undefined){
                    if (data.PartsCode.length < 4){
                        $scope.ldModel.DiscountTypeId = -1;
                        $scope.ldModel.DiscountTypeListId = null;
                        $scope.ldModel.Discount = 0;
                        $scope.ldModel.PartsId = null; // kl mau harga nya enable waktu dia ubah partscode
    
                        // ========================= hilangkan tipe diskon material jika ganti part, hrs click "Cek" dl ====================== start
                        // for (var p=0; p<$scope.diskonData.length; p++){
                        //     if ($scope.diskonData[p].MasterId == 3){
                        //         $scope.diskonData.splice(p,1)
                        //     }
                        // }
                        // ========================= hilangkan tipe diskon material jika ganti part, hrs click "Cek" dl ====================== end
                    }
                }
            }
    
            $scope.checkNameParts = function(data, model) {
                if ($scope.ldModel.IsOPB == null && data != null) {
                    $scope.ldModel.RetailPrice = 0;
                } else {
                    $scope.ldModel.RetailPrice = "";
                }
    
                if (model.MaterialTypeId == 1 && (model.PartsCode == null || model.PartsCode == undefined || model.PartsCode == '')) {
                    model.OrderType = 3
                    model.Type = 3
    
                }
    
            }


        // =====================================
        // Print Preview
        // =====================================
        $scope.printPreview = function(data) {
            console.log('mdata cetakOPL', data);
            // var data = $scope.mData;
            $scope.CetakanEstimasi = 'as/PrintAppointmentEstimasi/' + data + '/' + $scope.user.OrgId + '/1';
            // return $q.resolve($scope.cetakanOPL);
            $scope.cetakan($scope.CetakanEstimasi);

        };

        $scope.cetakan = function(data) {
            var pdfFile = null;

            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame 
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    }
                    else {
                        printJS(pdfFile);
                    }
                }
                else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };


        $scope.givePattern = function(item, event) {
            console.log("item", item);
            if (event.which > 37 && event.which < 40) {
                event.preventDefault();
                return false;
            } else {
                $scope.mData.Km = $scope.mData.Km.replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                return;
            }
        };

        $scope.allowPatternFilterNumericOnly = function(event) {
            console.log("event", event);
            patternRegex = /[0-9]/i;
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };

        $scope.allowPatternFilterAlphaNumericOnly = function(event) {
            console.log("event", event);
            patternRegex = /\d|[a-z]|\s/i; //ALPHANUMERIC ONLY
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };

        $scope.allowPatternFilterNoPol = function(event) {
            console.log("event", event);
            patternRegex = /\d|[a-z]/i; //ALPHANUMERIC Nopol
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };

        $scope.allowPatternFilterAlphabet = function(event) {
            console.log("event", event);
            patternRegex = /[a-z]|\s/i; //ALPHAbeth Only
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };



        // added by sss on 2017-09-25
        // $scope.fClaim = {show:false};
        // $scope.modalMode = 'new';
        // $scope.mClaim = {};
        // AppointmentBpService.getInsurence().then(function(res){
        //     console.log("res getInsurence",res.data);
        //     $scope.Asuransi = res.data;
        // });
        // WO.getVehicleTypeColor().then(function(resu) {
        //     console.log('getVehicleTypeColor', resu.data.Result);
        //     $scope.Color = resu.data.Result;
        // })
        // $scope.claimShow = function() {
        //   if ($scope.modalMode == 'new') {
        //     $scope.mClaim = {};
        //     if (typeof $scope.mData.LicensePlate !== 'undefined' && $scope.mData.LicensePlate!==null) {
        //       $scope.mClaim.LicensePlate = angular.copy($scope.mData.LicensePlate);
        //     }
        //     if (typeof $scope.mData.VehicleModelId !== 'undefined' && $scope.mData.VehicleModelId!==null) {
        //       $scope.mClaim.VehicleModelId = angular.copy($scope.mData.VehicleModelId);
        //     }
        //     if (typeof $scope.mData.VehicleTypeId !== 'undefined' && $scope.mData.VehicleTypeId!==null) {
        //       $scope.mClaim.VehicleTypeId = angular.copy($scope.mData.VehicleTypeId);
        //       var temp = _.find($scope.Color, {VehicleTypeId:$scope.mData.VehicleTypeId});
        //       if (typeof temp !== 'undefined') {
        //         selectTypeClaim(temp);
        //       }
        //     }
        //   }

        //   $scope.fClaim.show = true;
        // }
        // // $scope.selectModelClaim = function(item) {
        // //     console.log('selectedModel >>>', item);
        // //     $scope.VehicT = [];
        // //     $scope.mDataCrm.Vehicle.Type = null;
        // //     var dVehic = item.MVehicleType;
        // //     _.map(dVehic, function(a) {
        // //         a.NewDescription = a.Description + ' - ' + a.KatashikiCode + ' - ' + a.SuffixCode;
        // //     });
        // //     console.log('dVehic >>>', dVehic);
        // //     $scope.VehicT = dVehic;
        // //     // $scope.enableType = false;
        // // };      
        // $scope.selectTypeClaim = function(item) {
        //     console.log('selectedType >>>', item);
        //     $scope.colorData = [];
        //     var arrCol = [];
        //     _.map($scope.Color, function(val) {
        //         if (val.VehicleTypeId == item.VehicleTypeId) {
        //             arrCol = val.ListColor;
        //         };
        //     });
        //     // if (param == 1) {
        //     //     _.map(arrCol, function(a) {
        //     //         // if(a.ColorId == item.)
        //     //     })
        //     // };
        //     // $scope.enableColor = false;
        //     $scope.colorData = arrCol;
        // };
        // $scope.selectColorClaim = function(item) {
        //     console.log('selectedColor >>>', item);
        //     $scope.mDataCrm.Vehicle.ColorCode = item.ColorCode;
        //     console.log('$scope.mDataCrm >>>', $scope.mDataCrm);
        // };
        // 
    });