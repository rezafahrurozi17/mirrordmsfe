angular.module('app')
    .controller('GateController', function($scope, $http, CurrentUser, Gate, $sce, bsNotify, $timeout, ngDialog, bsAlert, $q, $state) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.myBarcodePicker ;
        console.log("gate=>", $state, $state.current.data.title);

        // var ScanditSDK = require("scandit-sdk");
       
        var initScandit = function()
        {
            var lic1 = "AZIMmzYEKzWICPhn0AR9HI4cjgXiFkKn7mNCBIlEqGj9cuRyo0LK+8Ijk6miYqy9vHZRsWkhUS8TTxRkSXPcDItDu33Zf9zrvFaTVpFFHIj/fvLq3UxFxcxXCQDzHIxWHUM8R98nW2CnDBcLdxqn2yuK0Uu13u1hz40g/W6ysYltXaymAgq/k4R8kz5e4KbJ7uNvECFSZmKjI0ac+4sW/VhT/4KR9QDojuJZVIhQ11ic71bQIixqLc3BSlBRjBoqfj4GlsvoYg4Vf71lLEcygegQPfyVkEY+dfaUhqYP0XXS8oNGvd+xEhUE+UeZaRKFnYo8iHHaBDgz2LCceOrXboG8F9p633Vqr0/QSUHYXln/Ut1Q1WoZV2KxgetQe6QwMVxRT7a8qQa2x5VOpgIyDd4tlZYgaPZ9KCLDlV1PchA1NQ5J39xCrN5+zCc78Srj1tK5NKqXEmZglDxu8ONBMm0yqK+N9tO7WtBVXv0Ci8OpGwgr/L/J1C3eoRPmSe5LGPDloahDtcz2piX8CjrfO7fvs54eesT74KrlVfNVO6QG7jDAuDOcVIxC9PGzGoUB+uNgmyxWiAVH8GPGqCw8Sg3QRUiopcG9ezrmaKpQUx74/nSCreJ1JsovwhJ1HCi+RSbfVQ1d+d9/d4rNvKGIaPycOyeJ60wYyXvvNbfdCoAws0BUv4pvL4+FCyU1JP7raCrwb+Qb8ylwd53tpIEo4oDi8R3BSUpfu6iO+gEB3pSsX3cd0LwvCGLFniY4t2EIgEJiKhM5N0TGepas7XFaLUVuIRiRHGXGkmRGdx+n4CwiuQNoOWkUSwLSGAOqY4KX";
            var lic2 = "Aav8eywHMVl0RcSaRxKqR4Iqsr5XAvdFUm324XE4b0TeaIEtRl73q1dlHOyUeYPOo08zbVgeUGnrWwVDhi/bekh1gQCTYFSfai7Vt+YsA3McZraKanuTZVRDUaCuXUQzJ1wwNRZqB+5eS7O5I0sqCRUT8eWmd7N97EwUCUV4ttm4XpvXnFBfMhVax+39ZT9t7UAat89uECg8Ru2KJXjJAXFz/H3OXFqJA2q0zFMmYVjKS9ZNtGNwCGRX9IX3TXLyolB8nGdJMm5fLHPDD3stCxBGh6ztdaJgZ3C4sfBkd/A0Wu78zEpBZj5GcBA/ZR+0Cnc4GpwnzmWORML3HWAAbzN6ZXdKbWH7RWstiMR1E9raJHoFuF/G2QpNyxR2eq1yUF1a091t4fDNf8DV7Wk5dOhcb1r5ZEDSInDEjD1hLhT5Uly9H0BVcGRQIHSVZejlulZsC/1NJhB9M3RcmGYOaYVkmWHfTVKhB0mznJhR8jWRXnxES3GvT5BScOCIfC3lviY+rRs4e0+VL1bEv39nwqnBHzUPwJvHj5E1/yfBpmwFWIBFUNmEe6qSstIq8BPSWV50Ln4g/f/Y7hVQrjovu0Wp0N5An8aHVopzcisVULnknkwSeAAiNAIRKvM66vIbxs0pSMAIz7vsw0ORJ5qXVSIX7vTZhn21sFFUZRIMmJThKotXrO/wLc711qNkYUXBfo5uuU2Bk2XKdlhKkri0uBR2P7mkaRpJe+4kbZd4RLZJPTZw87RcgJqItH0tww1y8NjUA5iCiT1WnC27hhyepazcSHwk3k+vlqbGqhZ1G1mgoxosi3v7qbC5lEbU5+f6dnOb05NClF0JvqeqrQ7zfoBghqOxhinxuUbc2OwqBO19KY+XAQv3+eUstzsJedPX8OvUR5Bqqc1fZZVLIf34L3yb9j3toIJge33/r81xlJ2XqDDAevjoPluVH6tlbAvYXcCCQ5LqYLzJSvphnnFDC5VuKjq4OC1uE2YEuU3XzH2bDc4tSQ/SVBkMxZ4SNIwDrPdWMkqHsnSfxrd0LsuHDNulwJnS3MuPt52bCC1GsLY+6IdJ8V1MX4xsR4TbLUjNmasuprR0JRIrEr/mxSEW8wyakRx1IyxspkECgxs5mzyCKk6cUQ/agFL9br+qZP0UodOAZsMMf05P8ctLCEnfuGFi38iBSqye2WU0Q6S8F6aMf+LEfbIKQfkC";
            var lic3 = "Adws9SIHP+9cHwwd7DmB2/oHuoXVKPXOiH0stZETr2+LenToSkpL5lRV3zrnaclWHFHsU3Zf3ZAWVwCSNzMoKhVVfPfdOq8yzzceUjRqysvAai1H2khUxXhhx57kJIGm7kn+OZJlbXw9YUztVSYKoNgIPcneODdbmy/Lug0Zfa53gm3RVridpo9RtqKqPKx2t/beShadR7vbZqFZCNaDSL+ZDMKNyPA7e/eBZvZbupJpfDbq7s6z9hqz+MQmUGx4wHYaLj9f/eG+1s02qq19fPe40I5eBXWRJluqegULMfx0SBK3JLs0HfHAyceIhYwK609WKI0wV6zsmDZFUYoj/BllGj4VKpYvzGQD8IpQI/XQb8y3G5aDHoM6/Rgvlc7W1sZOKXEcMIaBQ5ZUZRoQJ5qQl/bxcye+Hqo+TkdSjnfDm6i1JdkRe/gUfMsWyOt5BrTzTtdjdFgIcbrUOwp756BpmchYLMrwZSbU7xRqzizxO5T0ajEBcjkDZ+xKVCmd1lATFKu7MqNffllXqGZcagVy2mFHhZx1iTNnzkXy2wH3lEHEn6K+bdQM1hLYfOk61s7Z8Qo8b93pGxzW3Kojkgmu13xXchRZhom6BE+jxuPwu2bRIs9DvQVYowlfFxXRmbGoeMBRCeLMgxjNLBtj58Sr1svybIU+kGNH3UB6IlLW1yWqdBdH0L8ba/zyZBNM9hg0y41rqbva7wvO2JEiuni2BSPb2teG8Z21P+4JxRGMurKggKtuR/wQhvlNqrYM9X3xrLyXufCZNmGm2p4GhcJqsJqpTfnPNEixQ8MipTP6RCOedEfGScaxvgX7jk9rC/DZ0YG9j8Z4tgqVq4rr";
             lic3 ='ASN/wBPhDN6YLFjYwERrvms7S8VEQrRrXV8JRM10xbRTf4m3qFjS7poq0opGXBglzTY/d1hnslKuMz45/F2IeXJcosdqLdiLx0LmpyBPfmAxXLz08XFvxQNMDLvwJOW9bVC9ij9r0CWOMS8orzxgAXo6fyMeHhdZhhO2NHgYWFR6DvrJWhpAf6+eR1smIO8nwkjXh3a+3mGwZ0YUtKlKcX3TibrBIwH/cwhLCazPKkg3aS4gcmgg7lPxcfiy8+e764fhwxHmjjNoPOsb9A91VGgJ/fUh0s1G+do3pMlmhNINXDfaUW1aL+HF0nowipc5ofHFG38Q0pw9KjxqqWWUkj8FtewQVllPFmo0n8jEOYGJnpitTfzRIXXNbKwtjZG0J6S/pqiDAc5/vuwRpE6vvg+dykahMiImZDjzlRWnU+yPg6YxvfGVlVlh7TFQzyGx9ADFwTmXdPslEyvH926mnazoD45KGj/QkH6/FQ0r7XU17SHUSFpFe5N4b8M+aIV1W2QNVSov2AMO035u7Na0qa7dk31yprwm6axu+IKeRJCuZcTCvHSTgaYdz7u/cTK2+4sqLlCibn+CtBhgskgi2Bqj0UnN5qIs1F2sHaFnNwN13u8k36wLSY6qNz30DcBZyoNgutogK+ifCuVDf7FeFC27QOkT8I2X6gryiTH06b3eKHg4uNI76vs0o10BK/bkiGS+KmpUxBsix549/uS6QKeNVRc+4rsbiRcq/wsjSdfWndQrBiMV2bxhl1mfAu7o9UuFwxjfq5+n+CihMaWgz8pOVCl56M7el+o1OluOJCXeGShHfbydHDd9hfQQg+qUf+d760/dyuiJPTrQ3HYhgs4Bb30='
            $scope.debuginfo = "scan mode";
            if ($scope.myBarcodePicker )
            { 
                $scope.myBarcodePicker.pauseScanning(false);
            }
            else 
            {
                ScanditSDK.configure(lic3, {
                    // engineLocation: "module/barcode/build/"
                    engineLocation: "module/barcode/"
                }).then(function(){ 

                    // return ScanditSDK.BarcodePicker.create(document.getElementById("scandit-barcode-picker"), {
                    //     playSoundOnScan: true,
                    //     vibrateOnScan: true,
                    //   }).then(function (barcodePicker) {
                    //     // barcodePicker is ready here to be used (rest of the tutorial code should go here)
                    //     var scanSettings = new ScanditSDK.ScanSettings({
                    //         enabledSymbologies: ["ean8", "ean13", "upca", "upce", "code128", "code39", "code93", "itf"],
                    //         codeDuplicateFilter: 1000,
                    //       });
                    //       barcodePicker.applyScanSettings(scanSettings);

                    //       barcodePicker.on("scan", function (scanResult) {
                    //         alert(
                    //           scanResult.barcodes.reduce(function (string, barcode) {
                    //             return string + ScanditSDK.Barcode.Symbology.toHumanizedName(barcode.symbology) + ": " + barcode.data + "\n";
                    //           }, "")
                    //         );
                    //       });
                    //   });
                    return ScanditSDK.BarcodePicker
                    .create(document.getElementById("scandit-barcode-picker"), 
                        { playSoundOnScan: true, vibrateOnScan: true }
                    ).then(function(barcodePicker) {

                        var scanSettings = new ScanditSDK.ScanSettings({ 
                            enabledSymbologies: [
                                "aztec", "codabar", "code11", "code128", "dotcode", "ean13", "microqr", "qr",
                                "ean8", "upca", "upce",  "code39", "code93", "itf"           
                                ], 
                            codeDuplicateFilter: 1000,
                            searchArea: { x: 0, y: 0.45, width: 1.0, height: 0.1}
                         }); 
                        //barcodePicker.setGuiStyle(ScanditSDK.BarcodePicker.GuiStyle.VIEWFINDER);
                        barcodePicker.applyScanSettings(scanSettings);
                        
                        // barcodePicker.onScan(function(scanResult) {
                        barcodePicker.on("scan", function (scanResult) {
                            if (typeof scanResult.barcodes[0].data == "string") {
                                $scope.barcode = scanResult.barcodes[0].data;
                                $scope.mData.CodeInput = scanResult.barcodes[0].data;
                                var dateNow = new Date();
                                var yearFirst = dateNow.getFullYear();
                                var monthFirst = dateNow.getMonth() + 1;
                                var dayFirst = dateNow.getDate();
                                var fullDate = dayFirst + '-' + monthFirst + '-' + yearFirst;
                                $scope.scanDate = fullDate;
                                $scope.mode = $scope.mode == 'scanIn' ? 'scanInResult' : 'scanOutResult';
                                window.scrollTo(0, 0);
                                this.pauseScanning(true);
                            }
                        });

                        barcodePicker.pauseScanning = function(shouldPause)
                        {
                            this.scanningPaused = shouldPause;
                            this.cameraAccess = !shouldPause;
                            this.setVisible(!shouldPause);
                            if (shouldPause){
                                $scope.debuginfo = "paused"; 
                            }
                            else{
                                $scope.debuginfo = "ready";
                                 
                            }
                        }

                        // barcodePicker.onScanError(function(error) {
                        //     console.error(error.message);
                        //     $scope.debuginfo = error.message;
                        // });

                        // barcodePicker.onReady(function(){                            
                        //     $scope.debuginfo = "ready"; 
                            
                        // });
                        $scope.myBarcodePicker = barcodePicker;

                    });
                });

 
            }
            
        
        }
        //----------------------------------
        // Start-Up
        //----------------------------------

        $scope.$on('$viewContentLoaded', function() {
            $scope.mode = "in";
            $scope.loading = false;
            $scope.gridData = [];
            // $scope.getData();

            // $scope.mDataGateIn.forEach(function(d) {
            //   $sce.trustAsHtml(d.PSFU);
            //   $sce.trustAsHtml(d.sentVehicleFrom);
            // });
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        console.log("$scope.user", $scope.user);
        $scope.mData = {};
        var CodeScan = {};
        var dateFilter = 'date:"dd/MM/yyyy"';
        $scope.xRole = {
            selected: []
        };
        $scope.gridDataTree = [];
        $scope.show_modal = {
            show: false
        };
        $scope.modalMode = 'new';
        $scope.tmp = {};
        $scope.modal_model = {};

        $scope.disabledBP = false;
        $scope.disabledParts = false;
        $scope.disabledSales = false;
        $scope.disabledGR = false;
        // $scope.disabledSatelite = false;
        $scope.disabledSatelite = true;

        $scope.isSatelite = null;
        $scope.gridBook1 = [];

        $scope.ProsesSimpan = false;
        //----------------------------------
        // BsForm-Base
        //----------------------------------
        $scope.formApi = {};
        //----------------------------------
        // Action Button
        //----------------------------------
        $scope.actionButtonSettings = [

        ];
        //----------------------------------
        // Detail Action Button
        //----------------------------------
        $scope.actionButtonSettingsDetail = [

        ];


        $scope.labelBengkelGR = 'Bengkel GR'
        $scope.labelBengkelBP = 'Bengkel BP'



        //----------------------------------
        // Get Data
        //----------------------------------

        Gate.getOutletFunction().then(function(res) {
            // var gridData = res.data;
            console.log("getOutletFunction", res.data);
            $scope.OutletFunction = res.data
            // if (res.data.BP == 0) {
                // $scope.disabledBP = true;
                // if (res.data.SateliteCenter !== 'satelite') {
                //     $scope.disabledSatelite = true;
                //     $scope.disabledSateliteCek = true;
                // }
            // }
            if (res.data.SateliteCenter == 'center') {
                $scope.isSatelite = false;
            } else {
                $scope.isSatelite = true;
            };

            // if(res.data.P == 0){
            //     $scope.disabledGR = true;
            // }

            if (res.data.SP == 0) {
                $scope.disabledParts = true;
                $scope.disabledGR = true;
            }

            if (res.data.V == 0) {
                $scope.disabledSales = true;
            }

            if ($scope.user.OutletId < 2000000) { // ini untuk GR
            // GR hanya aktif, disabledSales, disabledGR, disabledComplaint, disabledOthers, disabledParts, disabledLainnya
                $scope.disabledSales = false;
                $scope.disabledGR = false;
                $scope.disabledComplaint = false;
                $scope.disabledOthers = false;
                $scope.disabledParts = false;
                $scope.disabledLainnya = false;

                $scope.disabledBP = true;
                $scope.disabledPengambilanBp = true;
                // if ($scope.disabledSateliteCek){
                //     $scope.disabledSatelite = true;
                // } else {
                //     $scope.disabledSatelite = false;
                // }
                // $scope.disabledSatelite = false;
                $scope.iniGR = 1;


            } else if ($scope.user.OutletId >= 2000000){ //ini untuk yang bp
            // BP hanya aktif, disabledBP, disabledPengambilanBp, disabledSatelite
                $scope.disabledSales = true;
                $scope.disabledGR = true;
                $scope.disabledComplaint = true;
                $scope.disabledOthers = true;
                
                $scope.disabledParts = false;
                $scope.disabledLainnya = false;

                $scope.disabledBP = false;
                $scope.disabledPengambilanBp = false;
                // $scope.disabledSatelite = false;
                // $scope.disabledSatelite = true;
                $scope.iniGR = 0;
            }

            //update kondisi disable tombol penerimaan satelite 23 mei 2022 ------------------ start
            if (res.data.SateliteCenter == "satelite") {
                $scope.disabledSatelite = false;
            } else {
                $scope.disabledSatelite = true;
            }
            //update kondisi disable tombol penerimaan satelite 23 mei 2022 ------------------ end 
            
            if(res.data.BP == 1){
                $scope.disabledBP = false;
                $scope.btnDisableBP = false;
                $scope.disabledPengambilanBp = false;
                $scope.disabledComplaint = false;
                $scope.disabledOthers = false;
            }else{
                $scope.disabledBP = true;
                $scope.btnDisableBP = true;
                $scope.disabledPengambilanBp = true;
            }

        });


        $scope.loadDataInputFile = function(data){
            var preview = document.querySelector('#UploadInputFile');
            var file = document.querySelector('#UploadInputFile').files[0];
            var reader = new FileReader();
            var imgReader = new FileReader();

            imgReader.onloadend = function() {
                console.log('Base64 Format', imgReader);
                $scope.LagiScanning = true;
                $scope.valueImg = imgReader.result;
                $scope.scanFile()
            }
            imgReader.readAsDataURL(file);


            // reader.addEventListener("onload", function () {
            //     // convert image file to base64 string
            //     preview.src = reader.result;
            //   }, false);

            //   if (file) {
            //     reader.readAsDataURL(file);
            //   }

        }

        // $scope.openModalScan = function(){
        //     $scope.scanFoto = {}
        //     $scope.scanFoto.show = true;
        //     $timeout(function() {
        //         initQuagga()
        //     }, 500);
        // }

        // $scope.saveScanFoto = function() {
        //     Quagga.stop()
        // }

        // $scope.cancelScanFoto = function() {
        //     Quagga.stop()
        // }

        $scope.scanFile = function(){
            console.log('wadooo')
            if ($scope.valueImg === undefined || $scope.valueImg === null || $scope.valueImg === ''){
                bsAlert.info('Silahkan pilih file gambar yang akan di-scan')
            } else {
                $scope.LagiScanning = true;
                Quagga.decodeSingle({
                    src: $scope.valueImg,
                    numOfWorkers: 0,  // Needs to be 0 when used within node
                    inputStream: {
                        size: 800  // restrict input-size to be 800px in width (long-side)
                    },
                    decoder: {
                        readers: ["code_128_reader"] // List of active readers
                    },
                }, function(result) {
                    if (result){
                        if(result.codeResult) {
                            console.log("result", result.codeResult.code);
                        } else {
                            console.log("not detected");
                            bsAlert.info('Barcode tidak terdeteksi / gambar kurang kurang jelas / barcode terlalu besar. Silahkan coba dengan gambar lain')
                            $scope.LagiScanning = false
                        }
                    } else {
                        bsAlert.info('Barcode tidak terdeteksi / gambar kurang kurang jelas / barcode terlalu besar. Silahkan coba dengan gambar lain')
                        $scope.LagiScanning = false
                    }
                    
                });
            }
        }

        var initQuagga = function() {
            $scope.LagiScanning = false
            document.getElementById("UploadInputFile").value = null;
            $scope.valueImg = null;
            var elements = document.getElementsByClassName('imgBuffer');
            while(elements.length > 0){
                elements[0].parentNode.removeChild(elements[0]);
            }
            var elements2 = document.getElementsByClassName('drawingBuffer');
            while(elements2.length > 0){
                elements2[0].parentNode.removeChild(elements2[0]);
            }

            Quagga.init({
                inputStream : {
                  type : "ImageStream",
                  target: document.querySelector('#interactive'),    // Or '#yourElement' (optional)
                //   constraints: {
                //                     width: dim.w,
                //                     height: dim.h,
                //                     // facingMode: "environment",
                //                 }
                 
                },
                frequency : 1,
                decoder : {
                    multiple: false,
                    readers : ['code_128_reader']
                //   readers : ['ean_reader']
                //   readers : ["ean_reader",'code_128_reader', 'ean_8_reader', 'code_39_reader', 'code_39_vin_reader', 'codabar_reader', 'upc_reader', 'upc_e_reader', 'i2of5_reader', '2of5_reader', 'code_93_reader'],
                },
                locate : true,
                locator : {
                    halfSample: true,
                    patchSize: "x-large",
                },
                src : null
              }, function(err) {
                  if (err) {

                      console.log(err);
                      return
                  }
                  console.log("Initialization finished. Ready to start");
                  Quagga.start();
              });

             

              Quagga.onProcessed(function(result) {
                var drawingCtx = Quagga.canvas.ctx.overlay
                var drawingCanvas = Quagga.canvas.dom.overlay
        
                if (result) {
                    if (result.boxes) {
                        drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
                        result.boxes.filter(function (box) {
                            return box !== result.box;
                        }).forEach(function (box) {
                            Quagga.ImageDebug.drawPath(box, {x: 0, y: 1}, drawingCtx, {color: "green", lineWidth: 2});
                        });
                    }
        
                    if (result.box) {
                        Quagga.ImageDebug.drawPath(result.box, {x: 0, y: 1}, drawingCtx, {color: "#00F", lineWidth: 2});
                    }
        
                    if (result.codeResult && result.codeResult.code) {
                        Quagga.ImageDebug.drawPath(result.line, {x: 'x', y: 'y'}, drawingCtx, {color: 'red', lineWidth: 3});
                    }
        
                   
                }
            });


            Quagga.onDetected(function (result) {
                console.log("Barcode detected and processed : [" + result.codeResult.code + "]", result);
                $scope.barcode = result.codeResult.code;
                $scope.mData.CodeInput = result.codeResult.code;
                var dateNow = new Date();
                var yearFirst = dateNow.getFullYear();
                var monthFirst = dateNow.getMonth() + 1;
                var dayFirst = dateNow.getDate();
                var fullDate = dayFirst + '-' + monthFirst + '-' + yearFirst;
                $scope.scanDate = fullDate;
                if ($scope.mode == 'scanIn' || $scope.mode == 'scanOut'){
                    $scope.mode = $scope.mode == 'scanIn' ? 'scanInResult' : 'scanOutResult';
                }
                window.scrollTo(0, 0);
                Quagga.stop();
            });

        }
        
        // var initQuagga = function() {
        //     if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
        //         alert("enumerateDevices() not supported.");
        //         return;
        //     }

        //     var backCamID;

        //     navigator.mediaDevices.enumerateDevices()
        //         .then(function(devices) {
        //             devices.forEach(function(device) {
        //                 //alert( JSON.stringify(device) );
        //                 if (device.kind == "videoinput" && device.label.match(/back/) != null) {
        //                     //alert("Back found!");
        //                     backCamID = device.deviceId;
        //                 }
        //             });
        //         })
        //         .catch(function(err) {
        //             //alert(err.name + ": " + err.message);
        //         });

        //     if (typeof(backCamID) == "undefined") {
        //         console.log("back camera not found.");
        //     }
            
        //     Quagga.init({
        //         inputStream: {
        //             name: "Live",
        //             type: "LiveStream",
        //             facing: "environment", // or user kalau mau pakai kamera depan
        //             target: document.querySelector('#interactive'),
        //             /*Or '#yourElement' (optional)*/
        //             constraints: {
        //                 width: 600,
        //                 height: 481,
        //                 facingMode: "environment",
        //                     // width: 320,
        //                     // height: 480
        //                     //deviceId: backCamID
        //             }
        //         },
        //         decoder: {
        //             // readers: ["code_39_reader"],
        //             readers: ["code_39_reader", "code_128_reader"],
        //             // readers: ["ean_reader"] // long black bar code
        //         },
        //         numOfWorkers: 4,
        //         locate: true,
        //         multiple: true,
        //         frequency: 1, 
        //         debug: {
        //             drawBoundingBox: true,
        //             showFrequency: false,
        //             drawScanline: true,
        //             showPattern: true,
        //             drawContour:true
        //         }
        //     }, function(err) {
        //         if (err) {
        //             console.log(err);
        //             return
        //         }
        //         console.log("Initialization finished. Ready to start");
        //         Quagga.start();
        //     });
        // }
        // Quagga.onDetected(function(data) {
        //     console.log("data", data);
        //     if (typeof data.codeResult.code == "string") {
        //         $scope.barcode = data.codeResult.code;
        //         var dateNow = new Date();
        //         var yearFirst = dateNow.getFullYear();
        //         var monthFirst = dateNow.getMonth() + 1;
        //         var dayFirst = dateNow.getDate();
        //         var fullDate = dayFirst + '-' + monthFirst + '-' + yearFirst;
        //         $scope.scanDate = fullDate;
        //         $scope.mode = $scope.mode == 'scanIn' ? 'scanInResult' : 'scanOutResult';
        //         window.scrollTo(0, 0);
        //         //console.log("ini data coy", data);
        //         CodeScan = data.codeResult.code;
        //         //console.log("ini hasil codenya", CodeScan);
        //         Quagga.stop();
        //     }
        // });
        // Quagga.onProcessed(function(result) {
        //     var drawingCtx = Quagga.canvas.ctx.overlay,
        //         drawingCanvas = Quagga.canvas.dom.overlay;

        //     if (result) {
        //         if (result.boxes) {
        //             drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
        //             result.boxes.filter(function(box) {
        //                 return box !== result.box;
        //             }).forEach(function(box) {
        //                 Quagga.ImageDebug.drawPath(box, {
        //                     x: 0,
        //                     y: 1
        //                 }, drawingCtx, {
        //                     color: "green",
        //                     lineWidth: 2
        //                 });
        //             });
        //         }

        //         if (result.box) {
        //             Quagga.ImageDebug.drawPath(result.box, {
        //                 x: 0,
        //                 y: 1
        //             }, drawingCtx, {
        //                 color: "#00F",
        //                 lineWidth: 2
        //             });
        //         }

        //         if (result.codeResult && result.codeResult.code) {
        //             Quagga.ImageDebug.drawPath(result.line, {
        //                 x: 'x',
        //                 y: 'y'
        //             }, drawingCtx, {
        //                 color: 'red',
        //                 lineWidth: 3
        //             });
        //         }
        //     }
        // });
        //button

        $scope.manualScan = function() {
            // var CodeScan = {};
            if ($scope.mData.CodeInput == "" || $scope.mData.CodeInput == undefined) {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Isi Data Barcode",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            } else {
                if ($scope.mData.CodeInput.length < 16 || $scope.mData.CodeInput.length > 17){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Panjang Karakter VIN Tidak Valid",
                        // content: error.join('<br>'),
                        // number: error.length
                    });
                    return;
                }
                
                // Quagga.onDetected(function(data) {
                // if (typeof data.codeResult.code == "string") {
                $scope.mData.CodeInput = $scope.mData.CodeInput.toUpperCase();
                $scope.barcode = $scope.mData.CodeInput;
                var dateNow = new Date();
                var yearFirst = dateNow.getFullYear();
                var monthFirst = dateNow.getMonth() + 1;
                var dayFirst = dateNow.getDate();
                var fullDate = yearFirst + '-' + monthFirst + '-' + dayFirst;
                $scope.scanDate = fullDate;
                $scope.mode = $scope.mode == 'scanIn' ? 'scanInResult' : 'scanOutResult';
                window.scrollTo(0, 0);
                // console.log("ini data coy", data);
                CodeScan = $scope.mData.CodeInput;
                console.log("ini hasil codenya", CodeScan);
                // Quagga.stop();
                // }
                // });
            }
        }

        $scope.gateIn = function() {
            $scope.mode = "in";
        }

        $scope.gateOut = function() {
            $scope.mode = "out";
        }

        $scope.gateScanIn = function() {
            $scope.mode = "scanIn";
            initQuagga();
            // initScandit();
            window.scrollTo(0, 0);
        }

        $scope.gateScanOut = function() {
            $scope.mode = "scanOut";
            initQuagga();
            // initScandit();
            window.scrollTo(0, 0);
        }

        $scope.gateScanBack = function() {
            $scope.mData.CodeInput = "";
            $scope.barcode = $scope.scanDate = "";
            // $scope.mode = $scope.mode == 'scanInResult' ? 'in' : 'out';
            if ($scope.mode == 'scanInResult' || $scope.mode == 'scanIn' ){
                $scope.mode = 'in'
            } else {
                $scope.mode = 'out'
            }
            Quagga.stop();
            // $scope.myBarcodePicker.pauseScanning(true);
            window.scrollTo(0, 0);
            $scope.getData();
        }

        
        var getAllData = function() {
            var xres = [];
            var xxres = [];
            var rawatJalan = [];
            $scope.loading = false;
            // =======Get Appointment=========
            var gridData = [];
            var gridInListData = [];
            $scope.isBPSatelite = false;

            Gate.getDataAppointment().then(function(res) {
                    // gridData = res.data.Result;
                    console.log("gridata", res.data.Result);
                    for (var i = 0; i < res.data.Result.length; i++) {
                        if(res.data.Result[i].isGr == "0"){
                            res.data.Result[i].Bengkel = "BP";
                        }else{
                            res.data.Result[i].Bengkel = "GR";
                        }
                        gridData.push(res.data.Result[i]);
                    }
                    // =====================
                    // Gate.getDataforGatein().then(function(resu) {
                    //     // var gridData = res.data;
                    //     for (var i = 0; i < resu.data.length; i++) {
                    //         resu.data[i].VehicleModelName = resu.data[i].TypeName == null ? resu.data[i].ModelType : resu.data[i].ModelType + ' - ' + resu.data[i].TypeName;
                    //         gridData.push(resu.data[i]);
                    //     }
                        console.log("gridata From Gate In=>", gridData);

                        Gate.gateFromSatelite_GR().then(function(resul_GR) {
                            Gate.gateFromSatelite_BP().then(function(resul_BP) {
                                console.log('nah ini sate GR', resul_GR)
                                console.log('nah ini sate BP', resul_BP)

                                $scope.isBPSatelite = true;
                                // console.log("gateFromSatelite", resul.data.Result)
                                for (var i = 0; i < resul_GR.data.Result.length; i++) {
                                    resul_GR.data.Result[i].VehicleModelName = resul_GR.data.Result[i].TypeName == null ? resul_GR.data.Result[i].VehicleModelName : resul_GR.data.Result[i].VehicleModelName + ' - ' + resul_GR.data.Result[i].TypeName;
                                    resul_GR.data.Result[i].Bengkel = 'GR'
                                    gridData.push(resul_GR.data.Result[i]);
                                }
                                for (var i = 0; i < resul_BP.data.Result.length; i++) {
                                    resul_BP.data.Result[i].VehicleModelName = resul_BP.data.Result[i].TypeName == null ? resul_BP.data.Result[i].VehicleModelName : resul_BP.data.Result[i].VehicleModelName + ' - ' + resul_BP.data.Result[i].TypeName;
                                    resul_BP.data.Result[i].Bengkel = 'BP'
                                    gridData.push(resul_BP.data.Result[i]);
                                }

                                gridData.sort(function(a, b) {
                                    var nameA = a.AppointmentTime // ignore upper and lowercase
                                    var nameB = b.AppointmentTime // ignore upper and lowercase
                                    console.log("a,b", a, b);
                                    if (nameA < nameB) {
                                        return -1;
                                    }
                                    if (nameA > nameB) {
                                        return 1;
                                    }

                                    // names must be equal
                                    return 0;
                                });
                                
                                $scope.gridGateIn.data = gridData;

                                // try order data by nopol, asc =============================
                                var SortedData2 = _.sortBy($scope.gridGateIn.data, ['LicensePlate']);
                                $scope.gridGateIn.data = SortedData2
                                // try order data by nopol, asc =============================

                                // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ start
                                // for (var e = 0; e<$scope.gridGateIn.data.length; e++) {
                                //     $scope.gridGateIn.data[e].Bengkel = $scope.gridGateIn.data[e].Bengkel ? $scope.gridGateIn.data[e].Bengkel : '-';
                                //     $scope.gridGateIn.data[e].VehicleModelName = $scope.gridGateIn.data[e].VehicleModelName ? $scope.gridGateIn.data[e].VehicleModelName : '-';
                                //     $scope.gridGateIn.data[e].ColorName = $scope.gridGateIn.data[e].ColorName ? $scope.gridGateIn.data[e].ColorName : '-';
                                //     $scope.gridGateIn.data[e].LicensePlate = $scope.gridGateIn.data[e].LicensePlate ? $scope.gridGateIn.data[e].LicensePlate : '-';
                                //     $scope.gridGateIn.data[e].Name = $scope.gridGateIn.data[e].Name ? $scope.gridGateIn.data[e].Name : '-';
                                //     $scope.gridGateIn.data[e].AppointmentTime = $scope.gridGateIn.data[e].AppointmentTime ? $scope.gridGateIn.data[e].AppointmentTime : '-';
                                // }
                                // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ end


                            })
                        })

                        // ----------------------- komen dl karena mau di satuin gate GR dan bp nya, jadi pake yg di atas ---------------------------------------- start
                        // Gate.gateFromSatelite().then(function(resul) {
                        //     // var gridData = res.data;
                        //     $scope.isBPSatelite = true;
                        //     console.log("gateFromSatelite", resul.data.Result)
                        //     for (var i = 0; i < resul.data.Result.length; i++) {
                        //         resul.data.Result[i].VehicleModelName = resul.data.Result[i].TypeName == null ? resul.data.Result[i].VehicleModelName : resul.data.Result[i].VehicleModelName + ' - ' + resul.data.Result[i].TypeName;
                        //         gridData.push(resul.data.Result[i]);
                        //     }

                        //     gridData.sort(function(a, b) {
                        //         var nameA = a.AppointmentTime // ignore upper and lowercase
                        //         var nameB = b.AppointmentTime // ignore upper and lowercase
                        //         console.log("a,b", a, b);
                        //         if (nameA < nameB) {
                        //             return -1;
                        //         }
                        //         if (nameA > nameB) {
                        //             return 1;
                        //         }

                        //         // names must be equal
                        //         return 0;
                        //     });
                        //     // console.log("gridata From Gate In=>", gridData);

                        // });
                        // ----------------------- komen dl karena mau di satuin gate GR dan bp nya, jadi pake yg di atas ---------------------------------------- end


                    // });
                    // if(gridData.length > 0){
                    //    for (var i = 0;i<gridData.length; i++) {
                    //      if (gridData[i].sentVehicleFrom == 0){
                    //          xres.push(res.data.Result[i]);
                    //      }
                    //   }
                    // }
                    console.log("gridata From Appointment=>", gridData);
                    $scope.gridBook1 = angular.copy(gridData);
                    // $scope.gridGateIn.data = xres;
                    // $scope.gridGateIn.data = gridData;
                    $scope.loading = false;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            $scope.gridGateIn.data = gridData;
            console.log("$scope.gridGateIn.data", $scope.gridGateIn.data);

            // try order data by nopol, asc =============================
            var SortedData2 = _.sortBy($scope.gridGateIn.data, ['LicensePlate']);
            $scope.gridGateIn.data = SortedData2
            // try order data by nopol, asc =============================


            // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ start

            // for (var e = 0; e<$scope.gridGateIn.data.length; e++) {
            //     $scope.gridGateIn.data[e].Bengkel = $scope.gridGateIn.data[e].Bengkel ? $scope.gridGateIn.data[e].Bengkel : '-';
            //     $scope.gridGateIn.data[e].VehicleModelName = $scope.gridGateIn.data[e].VehicleModelName ? $scope.gridGateIn.data[e].VehicleModelName : '-';
            //     $scope.gridGateIn.data[e].ColorName = $scope.gridGateIn.data[e].ColorName ? $scope.gridGateIn.data[e].ColorName : '-';
            //     $scope.gridGateIn.data[e].LicensePlate = $scope.gridGateIn.data[e].LicensePlate ? $scope.gridGateIn.data[e].LicensePlate : '-';
            //     $scope.gridGateIn.data[e].Name = $scope.gridGateIn.data[e].Name ? $scope.gridGateIn.data[e].Name : '-';
            //     $scope.gridGateIn.data[e].AppointmentTime = $scope.gridGateIn.data[e].AppointmentTime ? $scope.gridGateIn.data[e].AppointmentTime : '-';
            // }
            // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ end


            // $scope.gridGateIn.data=$scope.mDataGateIn;

            // =======Get List Production=====
            var dt = new Date;
            var yearFirst = dt.getFullYear();
            var monthFirst = dt.getMonth() + 1;
            var dayFirst = dt.getDate();
            var fullDate = yearFirst + '-' + monthFirst + '-' + dayFirst;

            Gate.getDataInListProduction(fullDate).then(function(res) {
                    console.log(res, "data list prod coi====>", $scope.gridGateInProduction.data);
                    $scope.gridGateInProduction.data = [];
                    var dataGateInProduction = [];

                    if (res.data != undefined || res.data != null) {
                        if (res.data.Result.length > 0) {
                            _.map(res.data.Result, function(prodin) {
                                    var tmpCheck = ''
                                    if(prodin.StatusJob !== null && prodin.StatusJob !== undefined){
                                        if(prodin.StatusJob == 25){
                                            tmpCheck = 'fa fa-plus colorRed';
                                        }
                                    }else{
                                        tmpCheck = 'glyphicon glyphicon-wrench'
                                    }

                                    var rwjln = ''
                                    if (prodin.sentVehicleFrom == 6){
                                        rwjln = 'fa fa-plus colorRed'
                                    }
                                    if (prodin.OutletId > 2000000){
                                        prodin.Bengkel = "BP";
                                    }else{
                                        prodin.Bengkel = "GR";
                                    }
                                    dataGateInProduction.push({
                                        id: prodin.id,
                                        LicensePlate: prodin.LicensePlate,
                                        VehicleModelName: prodin.VehicleModelName + ' - ' + prodin.TypeName,
                                        Name: prodin.Name,
                                        ColorName: prodin.ColorName,
                                        Check: tmpCheck,
                                        StatusTestDrive: 0,
                                        sentVehicleFrom: prodin.sentVehicleFrom,
                                        rwjln: rwjln,
                                        Bengkel:prodin.Bengkel
                                    });
                                    // $scope.gridBook1.push(dataGateInProduction);
                            });
                        }
                    }
                    for (var i=0; i<dataGateInProduction.length; i++){
                        $scope.gridBook1.push(dataGateInProduction[i]);
                    }
                    $scope.gridGateInProduction.data = dataGateInProduction;
                    console.log(res, "data list prod coi 2====>", dataGateInProduction);

                    // try order data by nopol, asc =============================
                    var SortedData2 = _.sortBy($scope.gridGateInProduction.data, ['LicensePlate']);
                    $scope.gridGateInProduction.data = SortedData2
                    // try order data by nopol, asc =============================

                    // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ start
                    // for (var e = 0; e<$scope.gridGateInProduction.data.length; e++) {
                    //     $scope.gridGateInProduction.data[e].Bengkel = $scope.gridGateInProduction.data[e].Bengkel ? $scope.gridGateInProduction.data[e].Bengkel : '-';
                    //     $scope.gridGateInProduction.data[e].VehicleModelName = $scope.gridGateInProduction.data[e].VehicleModelName ? $scope.gridGateInProduction.data[e].VehicleModelName : '-';
                    //     // $scope.gridGateInProduction.data[e].ColorName = $scope.gridGateInProduction.data[e].ColorName ? $scope.gridGateInProduction.data[e].ColorName : '-';
                    //     $scope.gridGateInProduction.data[e].LicensePlate = $scope.gridGateInProduction.data[e].LicensePlate ? $scope.gridGateInProduction.data[e].LicensePlate : '-';
                    //     $scope.gridGateInProduction.data[e].Name = $scope.gridGateInProduction.data[e].Name ? $scope.gridGateInProduction.data[e].Name : '-';
                    //     // $scope.gridGateInProduction.data[e].AppointmentTime = $scope.gridGateInProduction.data[e].AppointmentTime ? $scope.gridGateInProduction.data[e].AppointmentTime : '-';
                    // }
                    // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ end

                    var DataOplGateIn = [];
                    Gate.getInOpl().then(function(res) {
                            console.log("get in opl", res.data);
                            if (res.data.Result.length > 0) {
                                for (var i = 0; i < res.data.Result.length; i++) {
                                    if (res.data.Result.length > 0) {
                                        // DataOplGateIn.push({
                                        //     id: res.data.Result[i].Id,
                                        //     LicensePlate: res.data.Result[i].PoliceNo,
                                        //     VehicleModelName: res.data.Result[i].Model,
                                        //     Name: res.data.Result[i].ContactPerson,
                                        //     ColorName: res.data.Result[i].VehicleColor,
                                        //     Check: "glyphicon glyphicon-wrench",
                                        //     StatusTestDrive: 0
                                        //         // Name : 'Kicang Faisal'
                                        // });

                                        var rwjln = ''
                                        if (res.data.Result[i].sentVehicleFrom == 6){
                                            rwjln = 'fa fa-plus colorRed'
                                        }

                                        if (res.data.Result[i].OutletId > 2000000){
                                            res.data.Result[i].Bengkel = "BP";
                                        }else{
                                            res.data.Result[i].Bengkel = "GR";
                                        }

                                        dataGateInProduction.push({
                                            id: res.data.Result[i].Id,
                                            LicensePlate: res.data.Result[i].PoliceNo,
                                            VehicleModelName: res.data.Result[i].Model + ' - ' + res.data.Result[i].TypeName,
                                            Name: res.data.Result[i].ContactPerson,
                                            ColorName: res.data.Result[i].VehicleColor,
                                            Check: "glyphicon glyphicon-wrench",
                                            StatusTestDrive: 0,
                                            sentVehicleFrom: res.data.Result[i].sentVehicleFrom,
                                            rwjln: rwjln,
                                            Bengkel:res.data.Result[i].Bengkel
                                                // Name : 'Kicang Faisal'
                                        });
                                        // $scope.gridBook1.push(dataGateInProduction);
                                    }
                                };
                            }
                            for (var i=0; i<dataGateInProduction.length; i++){
                                $scope.gridBook1.push(dataGateInProduction[i]);
                            }
                            $scope.gridGateInProduction.data = dataGateInProduction;

                            // try order data by nopol, asc =============================
                            var SortedData2 = _.sortBy($scope.gridGateInProduction.data, ['LicensePlate']);
                            $scope.gridGateInProduction.data = SortedData2
                            // try order data by nopol, asc =============================

                            // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ start
                            // for (var e = 0; e<$scope.gridGateInProduction.data.length; e++) {
                            //     $scope.gridGateInProduction.data[e].Bengkel = $scope.gridGateInProduction.data[e].Bengkel ? $scope.gridGateInProduction.data[e].Bengkel : '-';
                            //     $scope.gridGateInProduction.data[e].VehicleModelName = $scope.gridGateInProduction.data[e].VehicleModelName ? $scope.gridGateInProduction.data[e].VehicleModelName : '-';
                            //     // $scope.gridGateInProduction.data[e].ColorName = $scope.gridGateInProduction.data[e].ColorName ? $scope.gridGateInProduction.data[e].ColorName : '-';
                            //     $scope.gridGateInProduction.data[e].LicensePlate = $scope.gridGateInProduction.data[e].LicensePlate ? $scope.gridGateInProduction.data[e].LicensePlate : '-';
                            //     $scope.gridGateInProduction.data[e].Name = $scope.gridGateInProduction.data[e].Name ? $scope.gridGateInProduction.data[e].Name : '-';
                            //     // $scope.gridGateInProduction.data[e].AppointmentTime = $scope.gridGateInProduction.data[e].AppointmentTime ? $scope.gridGateInProduction.data[e].AppointmentTime : '-';
                            // }
                            // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ end

                            $scope.loading = false;
                            Gate.getFinalInspectionTestDriveIn().then(function(resu) {
                                console.log('getFinalInspectionTestDriveIn', resu.data);
                                if (resu.data != undefined || resu.data != null) {
                                    if (resu.data.length > 0) {
                                        _.map(resu.data, function(a) {
                                            // DataOplGateIn.push({
                                            //     id: a.Id,
                                            //     LicensePlate: a.PoliceNo,
                                            //     // VehicleModelName: a.Job.VehicleType.VehicleModel.VehicleModelName,
                                            //     VehicleModelName: a.Job.VehicleType == null ? '' : a.Job.VehicleType.VehicleModel.VehicleModelName,
                                            //     Name: a.Job.ContactPerson,
                                            //     ColorName: '-',
                                            //     sentVehicleFrom: a.sentVehicleFrom,
                                            //     Check: "fa fa-car-side",
                                            //     StatusTestDrive: 1
                                            // });
                                            if (a.OutletId > 2000000){
                                                a.Bengkel = "BP";
                                            }else{
                                                a.Bengkel = "GR";
                                            }
                                            dataGateInProduction.push({
                                                id: a.Id,
                                                LicensePlate: a.PoliceNo,
                                                // VehicleModelName: a.Job.VehicleType.VehicleModel.VehicleModelName,
                                                VehicleModelName: (a.Job != undefined || a.Job != null) && (a.Job.VehicleType != undefined || a.Job.VehicleType != null) && (a.Job.VehicleType.VehicleModel != undefined || a.Job.VehicleType.VehicleModel != null) ? a.Job.VehicleType.VehicleModel.VehicleModelName : "",
                                                Name: (a.Job != undefined || a.Job != null) ? a.Job.ContactPerson : "",
                                                ColorName: '-',
                                                sentVehicleFrom: a.sentVehicleFrom,
                                                Check: "fa fa-car-side",
                                                StatusTestDrive: 1,
                                                Bengkel: a.Bengkel
                                            });
                                            // $scope.gridBook1.push(dataGateInProduction);
                                        });
                                    };
                                }
                                for (var i=0; i<dataGateInProduction.length; i++){
                                    $scope.gridBook1.push(dataGateInProduction[i]);
                                }
                                $scope.gridGateInProduction.data = dataGateInProduction;

                                // try order data by nopol, asc =============================
                                var SortedData2 = _.sortBy($scope.gridGateInProduction.data, ['LicensePlate']);
                                $scope.gridGateInProduction.data = SortedData2
                                // try order data by nopol, asc =============================

                                // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ start
                                // for (var e = 0; e<$scope.gridGateInProduction.data.length; e++) {
                                //     $scope.gridGateInProduction.data[e].Bengkel = $scope.gridGateInProduction.data[e].Bengkel ? $scope.gridGateInProduction.data[e].Bengkel : '-';
                                //     $scope.gridGateInProduction.data[e].VehicleModelName = $scope.gridGateInProduction.data[e].VehicleModelName ? $scope.gridGateInProduction.data[e].VehicleModelName : '-';
                                //     // $scope.gridGateInProduction.data[e].ColorName = $scope.gridGateInProduction.data[e].ColorName ? $scope.gridGateInProduction.data[e].ColorName : '-';
                                //     $scope.gridGateInProduction.data[e].LicensePlate = $scope.gridGateInProduction.data[e].LicensePlate ? $scope.gridGateInProduction.data[e].LicensePlate : '-';
                                //     $scope.gridGateInProduction.data[e].Name = $scope.gridGateInProduction.data[e].Name ? $scope.gridGateInProduction.data[e].Name : '-';
                                //     // $scope.gridGateInProduction.data[e].AppointmentTime = $scope.gridGateInProduction.data[e].AppointmentTime ? $scope.gridGateInProduction.data[e].AppointmentTime : '-';
                                // }
                                // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ end

                                $scope.loading = false;
                            })
                            console.log('gridInListData GETIN OPL1', dataGateInProduction);
                        },
                        function(err) {
                            console.log("err=>", err);
                        });
                    //console.log("gridata=>", gridInListData);
                    // $scope.gridGateInProduction.data = xxres;
                    gridInListData = [];
                    $scope.loading = false;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );


            Gate.getOutOpl().then(function(res) {
                    console.log("get out opl", res.data);
                    var dataGateOutProduction = [];
                    if (res.data.Result.length > 0) {
                        for (var i = 0; i < res.data.Result.length; i++) {
                            if (res.data.Result.length > 0) {
                                if(res.data.Result[i].OutletId > 2000000){
                                    res.data.Result[i].Bengkel = "BP";
                                }else{
                                    res.data.Result[i].Bengkel = "GR";
                                }

                                dataGateOutProduction.push({
                                    id: res.data.Result[i].Id,
                                    LicensePlate: res.data.Result[i].PoliceNo,
                                    VehicleModelName: res.data.Result[i] != null ? res.data.Result[i].TypeName == null ? res.data.Result[i].Model : res.data.Result[i].Model + ' - ' + res.data.Result[i].TypeName : '',
                                    Name: res.data.Result[i].ContactPerson,
                                    ColorName: res.data.Result[i].VehicleColor,
                                    Check: "glyphicon glyphicon-wrench",
                                    StatusTestDrive: 0,
                                    SortNo: 1,
                                    Bengkel: res.data.Result[i].Bengkel
                                });
                            }
                        }
                    }
                    $scope.gridGateOutProduction.data = dataGateOutProduction;

                    // try order data by nopol, asc =============================
                    var SortedData2 = _.sortBy($scope.gridGateOutProduction.data, ['LicensePlate']);
                    $scope.gridGateOutProduction.data = SortedData2
                    // try order data by nopol, asc =============================

                    // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ start
                    // for (var e = 0; e<$scope.gridGateOutProduction.data.length; e++) {
                    //     $scope.gridGateOutProduction.data[e].Bengkel = $scope.gridGateOutProduction.data[e].Bengkel ? $scope.gridGateOutProduction.data[e].Bengkel : '-';
                    //     $scope.gridGateOutProduction.data[e].VehicleModelName = $scope.gridGateOutProduction.data[e].VehicleModelName ? $scope.gridGateOutProduction.data[e].VehicleModelName : '-';
                    //     $scope.gridGateOutProduction.data[e].ColorName = $scope.gridGateOutProduction.data[e].ColorName ? $scope.gridGateOutProduction.data[e].ColorName : '-';
                    //     $scope.gridGateOutProduction.data[e].LicensePlate = $scope.gridGateOutProduction.data[e].LicensePlate ? $scope.gridGateOutProduction.data[e].LicensePlate : '-';
                    //     $scope.gridGateOutProduction.data[e].Name = $scope.gridGateOutProduction.data[e].Name ? $scope.gridGateOutProduction.data[e].Name : '-';
                    //     // $scope.gridGateOutProduction.data[e].AppointmentTime = $scope.gridGateOutProduction.data[e].AppointmentTime ? $scope.gridGateOutProduction.data[e].AppointmentTime : '-';
                    // }
                    // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ end

                    console.log('data getOutTestDrive', dataGateOutProduction);
                    $scope.loading = false;
                    console.log('dataGateOutProduction', dataGateOutProduction);
                    Gate.getOutTestDrive().then(function(ress) {
                        if (ress.data != undefined || ress.data != null) {
                            for (var i = 0; i < ress.data.length; i++) {
                                if (ress.data.length > 0) {

                                    if (ress.data[i].OutletId >= 2000000) {
                                        ress.data[i].Bengkel = 'BP'
                                    } else {
                                        ress.data[i].Bengkel = 'GR'
                                    }

                                    dataGateOutProduction.push({
                                        id: ress.data[i].Id,
                                        LicensePlate: ress.data[i].PoliceNo,
                                        VehicleModelName: ress.data[i] != null ? ress.data[i].TypeName == null ? ress.data[i].Model : ress.data[i].Model + ' - ' + ress.data[i].TypeName : '',
                                        Name: ress.data[i].ContactPerson,
                                        ColorName: ress.data[i].VehicleColor,
                                        // Check: "fa fa-automobile",
                                        Check: "",
                                        SortNo: 2,
                                        Bengkel: ress.data[i].Bengkel,
                                    });
                                }
                            }
                        }

                        Gate.getFinalInspectionTestDriveOut().then(function(resu) {
                            console.log('getFinalInspectionTestDriveOut', resu.data);
                            // console.log('getFinalInspectionTestDriveOut', resu.data[0].Job != undefined || resu.data[0].Job != null ? resu.data[0].Job.VehicleType.VehicleModel != undefined || resu.data[0].Job.VehicleType.VehicleModel != null ? resu.data[0].Job.VehicleType.VehicleModel.VehicleModelName : "" : "");

                            if (resu.data.length > 0) {
                                _.map(resu.data, function(a) {
                                    if (a.OutletId >= 2000000) {
                                        a.Bengkel = 'BP'
                                    } else {
                                        a.Bengkel = 'GR'
                                    }
                                    dataGateOutProduction.push({
                                        id: a.Id,
                                        LicensePlate: a.PoliceNo,
                                        // VehicleModelName: a.Job.VehicleType.VehicleModel.VehicleModelName,
                                        VehicleModelName: (a.Job != undefined || a.Job != null) && (a.Job.VehicleType != undefined || a.Job.VehicleType != null) ? (a.Job.VehicleType.VehicleModel != null ? a.Job.VehicleType.VehicleModel.VehicleModelName : "") + ' - ' + a.Job.VehicleType.Description : "",
                                        Name: a.Job != undefined || a.Job != null ? a.Job.ContactPerson : "",
                                        ColorName: (a.Job != undefined || a.Job != null) && (a.Job.VehicleType != undefined || a.Job.VehicleType != null) ? a.Job.VehicleColor : "",
                                        sentVehicleFrom: a.sentVehicleFrom,
                                        Check: (a.sentVehicleFrom != 5 ? "fas fa-car-side" : "fa fa-rss"),
                                        //Check: (a.MasterId != undefined || a.MasterId != null ? (a.MasterId == 98 ? "fa fa-rss" : "fas fa-car-side") : "fas fa-car-side"),
                                        StatusTestDrive: (a.Job.isWoFromBpSatelit == 1 ? 0 : 1),
                                        SortNo: 2,
                                        Bengkel: a.Bengkel
                                    });
                                })
                            };

                            var sortDataGateOutProduction = _.sortBy(dataGateOutProduction, ['SortNo'], ['asc']);

                            $scope.gridGateOutProduction.data = sortDataGateOutProduction;
                            console.log('data getOutTestDrive', dataGateOutProduction);

                            // try order data by nopol, asc =============================
                            var SortedData2 = _.sortBy($scope.gridGateOutProduction.data, ['LicensePlate']);
                            $scope.gridGateOutProduction.data = SortedData2
                            // try order data by nopol, asc =============================

                            // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ start
                            // for (var e = 0; e<$scope.gridGateOutProduction.data.length; e++) {
                            //     $scope.gridGateOutProduction.data[e].Bengkel = $scope.gridGateOutProduction.data[e].Bengkel ? $scope.gridGateOutProduction.data[e].Bengkel : '-';
                            //     $scope.gridGateOutProduction.data[e].VehicleModelName = $scope.gridGateOutProduction.data[e].VehicleModelName ? $scope.gridGateOutProduction.data[e].VehicleModelName : '-';
                            //     $scope.gridGateOutProduction.data[e].ColorName = $scope.gridGateOutProduction.data[e].ColorName ? $scope.gridGateOutProduction.data[e].ColorName : '-';
                            //     $scope.gridGateOutProduction.data[e].LicensePlate = $scope.gridGateOutProduction.data[e].LicensePlate ? $scope.gridGateOutProduction.data[e].LicensePlate : '-';
                            //     $scope.gridGateOutProduction.data[e].Name = $scope.gridGateOutProduction.data[e].Name ? $scope.gridGateOutProduction.data[e].Name : '-';
                            //     // $scope.gridGateOutProduction.data[e].AppointmentTime = $scope.gridGateOutProduction.data[e].AppointmentTime ? $scope.gridGateOutProduction.data[e].AppointmentTime : '-';
                            // }
                            // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ end

                            $scope.loading = false;
                        })
                    });

                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            // Gate.getDataOutListProduction(fullDate).then(function(res) {
            //         var gridListData = res.data.Result;
            //         var tmpData = []
            //         if (gridListData.length > 0) {
            //             //$scope.gridGateOutProduction.data = gridListData;
            //             //added by AMK
            //             //tambah validasi untuk get productionlist dengan sentVehicleFrom == 6
            //             for (var i = 0; i < gridListData.length; i++) {
            //                 if (gridListData[i].sentVehicleFrom < 7 && gridListData[i].sentVehicleFrom != 6) {
            //                     tmpData.push(res.data.Result[i]);
            //                 } 
            //                 // else if (gridListData[i].sentVehicleFrom == 6) {
            //                 //     rawatJalan.push(res.data.Result[i]);
            //                 // };
            //             };
            //         };

            //         $scope.gridGateOutProduction.data = tmpData;
            //         // $scope.gridOutPatient.data = rawatJalan;
            //         console.log("gridata Get Out dari Production=>", gridListData);
            //         // $scope.gridGateInProduction.data = xxres;
            //         $scope.loading = false;
            //     },
            //     function(err) {
            //         console.log("err=>", err);
            //     }
            // );
            //============rawat jalan
            Gate.getRawatJalan().then(function(res) {
                        for (var i = 0; i < res.data.Result.length; i++) {
                            res.data.Result[i].VehicleModelName = res.data.Result[i].TypeName == null ? res.data.Result[i].VehicleModelName : res.data.Result[i].VehicleModelName + ' - ' + res.data.Result[i].TypeName;
                            // if (res.data.Result[i].Billing.StatusBilling == 1) {
                            if (res.data.Result[i].Status == 25) {
                                res.data.Result[i].xStatus = "Rawat Jalan";
                            } else {
                                res.data.Result[i].xStatus = " - ";
                            }
                            
                            if(res.data.Result[i].OutletId > 2000000){
                                res.data.Result[i].Bengkel = "BP";
                            }else{
                                res.data.Result[i].Bengkel = "GR";
                            }
                        }
                        $scope.gridOutPatient.data = res.data.Result;

                        // try order data by nopol, asc =============================
                        var SortedData2 = _.sortBy($scope.gridOutPatient.data, ['PoliceNumber']);
                        $scope.gridOutPatient.data = SortedData2
                        // try order data by nopol, asc =============================

                        // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ start
                        // for (var e = 0; e<$scope.gridOutPatient.data.length; e++) {
                        //     $scope.gridOutPatient.data[e].Bengkel = $scope.gridOutPatient.data[e].Bengkel ? $scope.gridOutPatient.data[e].Bengkel : '-';
                        //     $scope.gridOutPatient.data[e].ModelType = $scope.gridOutPatient.data[e].ModelType ? $scope.gridOutPatient.data[e].ModelType : '-';
                        //     $scope.gridOutPatient.data[e].ColorName = $scope.gridOutPatient.data[e].ColorName ? $scope.gridOutPatient.data[e].ColorName : '-';
                        //     $scope.gridOutPatient.data[e].PoliceNumber = $scope.gridOutPatient.data[e].PoliceNumber ? $scope.gridOutPatient.data[e].PoliceNumber : '-';
                        //     $scope.gridOutPatient.data[e].ContactPerson = $scope.gridOutPatient.data[e].ContactPerson ? $scope.gridOutPatient.data[e].ContactPerson : '-';
                        //     // $scope.gridOutPatient.data[e].AppointmentTime = $scope.gridOutPatient.data[e].AppointmentTime ? $scope.gridOutPatient.data[e].AppointmentTime : '-';
                        // }
                        // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ end

                        console.log("rawat jalan dari roshikul", res.data.Result);
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                )
                // ===========================
            Gate.getDataOutFinal(fullDate).then(function(res) {
                    var gridListData = res.data.Result;
                    var dataGridFinal = [];
                    //var gateSateliteCenterData = [];
                    var FinalSatelite = [];
                    var StatusCtgId = 1;
                    var isSateliteCheck = false;

                    console.log("gridListData Selesai Service", gridListData);

                    if (gridListData != null || gridListData != null) {
                        for (var i = 0; i < gridListData.length; i++) {
                            //get data satelite 5. Delivery to Satelite, Ready For Pickup
                            if (gridListData[i].MasterId == 98 && gridListData[i].sentVehicleFrom == 5) {
                                StatusCtgId = 6;
                                isSateliteCheck = true;
                            }
                            if(gridListData[i].OutletId > 2000000){
                                gridListData[i].Bengkel = "BP";
                            }else{
                                gridListData[i].Bengkel = "GR";
                            }
                            //gateSateliteCenterData.push(gridListData[i]);
                        }
                    }

                    if (isSateliteCheck == false) {

                        Gate.gateSateliteCenter(1).then(function(res) {
                                var gridListSatelite = res.data.Result;
                                for (var i = 0; i < res.data.length; i++) {
                                    if (gridListSatelite[i].StatusId == 2398) {
                                        FinalSatelite.push({
                                            VehicleModelName: res.data[i].TypeName == null ? res.data[i].ModelType : res.data[i].ModelType + ' - ' + res.data[i].TypeName,
                                            LicensePlate: res.data[i].PoliceNumber,
                                            Status: res.data[i].Status,
                                            Name: res.data[i].ContactPerson,
                                            id: res.data[i].GateSateliteCenterId,
                                            sentVehicleFrom: res.data[i].sentVehicleFrom,
                                            ColorName: res.data[i].ColorName
                                        });
                                        console.log("FinalSatelite", FinalSatelite, res.data);
                                        for (var a = 0; a < FinalSatelite.length; a++) {
                                            dataGridFinal.push(FinalSatelite[a]);
                                        }
                                        console.log("dataGridFinal rrr", dataGridFinal);
                                        // $scope.gridGateOutFinishService.data = dataGridFinal;
                                    }
                                }
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );

                        //get data satelite 5. Delivery to Satelite, Ready For Pickup
                        Gate.gateSateliteDeliveryToSatelite(6).then(function(res) {
                                var gridListSatelite = res.data.Result;
                                for (var i = 0; i < res.data.length; i++) {
                                    if (gridListSatelite[i].StatusId == 2398) {
                                        FinalSatelite.push({
                                            VehicleModelName: res.data[i].TypeName == null ? res.data[i].ModelType : res.data[i].ModelType + ' - ' + res.data[i].TypeName,
                                            LicensePlate: res.data[i].PoliceNumber,
                                            Status: res.data[i].Status,
                                            Name: res.data[i].ContactPerson,
                                            id: res.data[i].GateSateliteCenterId,
                                            sentVehicleFrom: res.data[i].sentVehicleFrom,
                                            ColorName: res.data[i].ColorName
                                        });
                                        console.log("Final DeliveryToSatelite", FinalSatelite, res.data);

                                        for (var a = 0; a < FinalSatelite.length; a++) {
                                            dataGridFinal.push(FinalSatelite[a]);
                                        }
                                        console.log("dataGridFinal rrr", dataGridFinal);
                                        $scope.gridGateOutFinishService.data = dataGridFinal;

                                        // try order data by nopol, asc =============================
                                        var SortedData2 = _.sortBy($scope.gridGateOutFinishService.data, ['LicensePlate']);
                                        $scope.gridGateOutFinishService.data = SortedData2
                                        // try order data by nopol, asc =============================

                                        // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ start
                                        // for (var e = 0; e<$scope.gridGateOutFinishService.data.length; e++) {
                                        //     $scope.gridGateOutFinishService.data[e].Bengkel = $scope.gridGateOutFinishService.data[e].Bengkel ? $scope.gridGateOutFinishService.data[e].Bengkel : '-';
                                        //     $scope.gridGateOutFinishService.data[e].VehicleModelName = $scope.gridGateOutFinishService.data[e].VehicleModelName ? $scope.gridGateOutFinishService.data[e].VehicleModelName : '-';
                                        //     $scope.gridGateOutFinishService.data[e].ColorName = $scope.gridGateOutFinishService.data[e].ColorName ? $scope.gridGateOutFinishService.data[e].ColorName : '-';
                                        //     $scope.gridGateOutFinishService.data[e].LicensePlate = $scope.gridGateOutFinishService.data[e].LicensePlate ? $scope.gridGateOutFinishService.data[e].LicensePlate : '-';
                                        //     $scope.gridGateOutFinishService.data[e].Name = $scope.gridGateOutFinishService.data[e].Name ? $scope.gridGateOutFinishService.data[e].Name : '-';
                                        //     // $scope.gridGateOutFinishService.data[e].AppointmentTime = $scope.gridGateOutFinishService.data[e].AppointmentTime ? $scope.gridGateOutFinishService.data[e].AppointmentTime : '-';
                                        // }
                                        // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ end
                                    }
                                }
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    }

                    if (gridListData != null || gridListData != null) {
                        if (gridListData.length > 0) {
                            // for (var i = 0; i < gridListData.length; i++) {
                            //     dataGridFinal.push(gridListData[i]);
                            // }
                            _.map(gridListData, function(val) {
                                val.VehicleModelName = val.TypeName == null ? val.VehicleModelName : val.VehicleModelName + ' - ' + val.TypeName;
                                dataGridFinal.push(val);
                            });
                            console.log("gridListData", gridListData);


                            $scope.gridGateOutFinishService.data = dataGridFinal;

                            // try order data by nopol, asc =============================
                            var SortedData2 = _.sortBy($scope.gridGateOutFinishService.data, ['LicensePlate']);
                            $scope.gridGateOutFinishService.data = SortedData2
                            // try order data by nopol, asc =============================

                            // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ start
                            // for (var e = 0; e<$scope.gridGateOutFinishService.data.length; e++) {
                            //     $scope.gridGateOutFinishService.data[e].Bengkel = $scope.gridGateOutFinishService.data[e].Bengkel ? $scope.gridGateOutFinishService.data[e].Bengkel : '-';
                            //     $scope.gridGateOutFinishService.data[e].VehicleModelName = $scope.gridGateOutFinishService.data[e].VehicleModelName ? $scope.gridGateOutFinishService.data[e].VehicleModelName : '-';
                            //     $scope.gridGateOutFinishService.data[e].ColorName = $scope.gridGateOutFinishService.data[e].ColorName ? $scope.gridGateOutFinishService.data[e].ColorName : '-';
                            //     $scope.gridGateOutFinishService.data[e].LicensePlate = $scope.gridGateOutFinishService.data[e].LicensePlate ? $scope.gridGateOutFinishService.data[e].LicensePlate : '-';
                            //     $scope.gridGateOutFinishService.data[e].Name = $scope.gridGateOutFinishService.data[e].Name ? $scope.gridGateOutFinishService.data[e].Name : '-';
                            //     // $scope.gridGateOutFinishService.data[e].AppointmentTime = $scope.gridGateOutFinishService.data[e].AppointmentTime ? $scope.gridGateOutFinishService.data[e].AppointmentTime : '-';
                            // }
                            // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ end

                            //  for (var i = 0;i<gridListData.length; i++) {
                            //    if (gridListData[i].sentVehicleFrom  > 6 && gridListData[i].sentVehicleFrom < 14){
                            //        xxres.push(res.data.Result[i]);
                            //    }
                            // }
                        } else {
                            $scope.gridGateOutFinishService.data = [];
                        }
                    }

                    // $scope.gridGateOutFinishService.data = test;

                    console.log("gridata Get Out Selesai Service=>", gridListData);
                    // $scope.gridGateOutFinishService.data = xxres;
                    $scope.loading = false;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );


            // ======== Lalu Lalang======
            // Gate.getDataLaluLalang(fullDate).then(function(res) {
            //         var gridLaluData = res.data.Result;
            //         if (gridLaluData.length > 0) {
            //             // $scope.gridGateOutPassingBy.data = gridLaluData;
            //             for (var i = 0; i < gridLaluData.length; i++) {
            //                 if (gridLaluData[i].sentVehicleFrom > 6 && gridLaluData[i].sentVehicleFrom < 15) {
            //                     xxres.push(res.data.Result[i]);
            //                 }
            //             }
            //         }
            //         Gate.getOutVisitor().then(function(ress) {
            //             var gridgetOutVisitor = ress.data;
            //             console.log("gridgetOutVisitor", gridgetOutVisitor);
            //             if (gridgetOutVisitor.length > 0) {
            //                 // $scope.gridGateOutPassingBy.data = gridLaluData;
            //                 for (var i = 0; i < gridgetOutVisitor.length; i++) {
            //                     //if (gridgetOutVisitor[i].sentVehicleFrom > 6 && gridgetOutVisitor[i].sentVehicleFrom < 15) {
            //                     xxres.push(ress.data[i]);
            //                     //  xxres.push({
            //                     //      id: ress.Id,
            //                     //      Name: ress.Job.ContactPerson
            //                     //  });
            //                     // }
            //                 }
            //             }
            //         });
            //         // Gate.getOutVisitorNew().then(function(ress) {
            //         //     var getOutVisitorNew = ress.data;
            //         //     console.log("getOutVisitorNew", getOutVisitorNew);
            //         //     if (getOutVisitorNew.length > 0) {
            //         //         // $scope.gridGateOutPassingBy.data = gridLaluData;
            //         //         for (var i = 0; i < getOutVisitorNew.length; i++) {
            //         //             // if (getOutVisitorNew[i].sentVehicleFrom > 6 && getOutVisitorNew[i].sentVehicleFrom < 15) {
            //         //             xxres.push(ress.data[i]);
            //         //             // xxres.push({
            //         //             //      id: ress.Id,
            //         //             //      Name: ress.Job.ContactPerson
            //         //             // });
            //         //             // //Name: res.data[i].ContactPerson;
            //         //             // }
            //         //         }
            //         //     }
            //         // });
            //         console.log("gridata Get Out Visitor=>", gridLaluData);
            //         $scope.gridGateOutPassingBy.data = xxres;
            //         $scope.loading = false;
            //     },
            //     function(err) {
            //         console.log("err=>", err);
            //     }
            // );

            Gate.getOutVisitor().then(function(ress) {
                    var gridgetOutVisitor = ress.data.Result;
                    console.log("gridgetOutVisitor", gridgetOutVisitor);
                    if (gridgetOutVisitor.length > 0) {
                        // $scope.gridGateOutPassingBy.data = gridLaluData;
                        // for (var i = 0; i < gridgetOutVisitor.length; i++) {

                        //     //if (gridgetOutVisitor[i].sentVehicleFrom > 6 && gridgetOutVisitor[i].sentVehicleFrom < 15) {
                        //     xxres.push(ress.data[i]);
                        //     //  xxres.push({
                        //     //      id: ress.Id,
                        //     //      Name: ress.Job.ContactPerson
                        //     //  });
                        //     // }
                        // }
                        _.map(gridgetOutVisitor, function(val) {
                            // val.VehicleModelName = val.Job.VehicleType.VehicleModel.VehicleModelName;
                            xxres.push(val);
                            console.log("cek Val", val);
                            if(val.OutletId > 2000000){
                                val.bengkel = "BP";
                            }else{
                                val.bengkel = "GR";
                            }
                        });
                    };


                    console.log("gridata Get Out Visitor=>", gridgetOutVisitor);
                    $scope.gridGateOutPassingBy.data = xxres;

                    // try order data by nopol, asc =============================
                    var SortedData2 = _.sortBy($scope.gridGateOutPassingBy.data, ['PoliceNo']);
                    $scope.gridGateOutPassingBy.data = SortedData2
                    // try order data by nopol, asc =============================

                    for (var e = 0; e<$scope.gridGateOutPassingBy.data.length; e++) {
                        $scope.gridGateOutPassingBy.data[e].Bengkel = $scope.gridGateOutPassingBy.data[e].Bengkel ? $scope.gridGateOutPassingBy.data[e].Bengkel : '-';
                        $scope.gridGateOutPassingBy.data[e].TypeName = $scope.gridGateOutPassingBy.data[e].TypeName ? $scope.gridGateOutPassingBy.data[e].TypeName : '-';
                        $scope.gridGateOutPassingBy.data[e].VehicleColor = $scope.gridGateOutPassingBy.data[e].VehicleColor ? $scope.gridGateOutPassingBy.data[e].VehicleColor : '-';
                        $scope.gridGateOutPassingBy.data[e].PoliceNo = $scope.gridGateOutPassingBy.data[e].PoliceNo ? $scope.gridGateOutPassingBy.data[e].PoliceNo : '-';
                        $scope.gridGateOutPassingBy.data[e].ContactPerson = $scope.gridGateOutPassingBy.data[e].ContactPerson ? $scope.gridGateOutPassingBy.data[e].ContactPerson : '-';
                        // $scope.gridGateOutPassingBy.data[e].AppointmentTime = $scope.gridGateOutPassingBy.data[e].AppointmentTime ? $scope.gridGateOutPassingBy.data[e].AppointmentTime : '-';
                    }

                    $scope.loading = false;
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
        }
        $scope.filterBtnChange = function(col) {
            $scope.filterBtnLabel = col.name;
            $scope.filterColIdx = (col.idx) + 1;
            console.log("col=>", col, $scope.filterColIdx);
            console.log("$scope.filterBtnLabel", $scope.filterBtnLabel);
        }
        $scope.getData = function() {
            getAllData();
            return $q.resolve(
                null
            )
        }

        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        }
        $scope.actCheckInCar = function(type, row) {
            console.log('row appointment', row)
            bsAlert.alert({
                    title: "Apakah anda yakin No. Polisi " + row.LicensePlate + " akan check In",
                    text: "",
                    type: "info",
                    showCancelButton: true
                },
                function() {
                    $scope.checkInCar(type, row, 2);
                },
                function() {

                }
            )
        }
        $scope.actCheckInCarProduction = function(type, row) {
            bsAlert.alert({
                    title: "Apakah anda yakin No. Polisi " + row.LicensePlate + " akan check In",
                    text: "",
                    type: "info",
                    showCancelButton: true
                },
                function() {
                    $scope.checkInCarProduction(type, row);
                },
                function() {

                }
            )
        }
        $scope.actGetOutCar = function(row) {
            bsAlert.alert({
                    title: "Apakah anda yakin mobil No. Polisi " + row.LicensePlate + " akan keluar?",
                    text: "",
                    type: "info",
                    showCancelButton: true
                },
                function() {
                    console.log('row actGetOutCar production', row);
                    // bsAlert.alert({
                    //     title: "Mobil dengan No. Polisi " + row.LicensePlate + " telah keluar",
                    //     text: "",
                    //     type: "info",
                    //     showCancelButton: false
                    // });
                    $scope.getOutCar(row);
                },
                function() {

                }
            )
        }

        $scope.actionGetOutCarRawatJalan = function(row) {
            bsAlert.alert({
                    title: "Apakah anda yakin mobil No. Polisi " + row.PoliceNumber + " akan keluar?",
                    text: "",
                    type: "info",
                    showCancelButton: true
                },
                function() {
                    // bsAlert.alert({
                    //     title: "Mobil dengan No. Polisi " + row.LicensePlate + " telah keluar",
                    //     text: "",
                    //     type: "info",
                    //     showCancelButton: false
                    // });
                    Gate.CheckGateOutRawatJalan(row.JobId, row.PoliceNumber).then(function(result) {
                        if (result.data == 666) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Kendaraan dengan No. Polisi tersebut sudah melakukan gate out rawat jalan, silahkan refresh.",
                                
                            });
                        } else if (result.data == 0) {
                            $scope.gateOutCarRawatJalan(row);
                        }
                    })
                },
                function() {

                }
            )
        }
        $scope.actVisitorCar = function(row) {
            Gate.GetStatusGateQueue(row).then(function(result) {
                if (result.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Kendaraan dengan No. Polisi tersebut dalam proses pembuatan WO",
                        // content: error.join('<br>'),
                        // number: error.length
                    });
                    $scope.getData();
                } else {
                    Gate.GetStatusJobByPoliceNo(row).then(function(res) {
                        if (res.data == 1){
                            // ga bs gate out
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Kendaraan dengan No. Polisi tersebut sedang proses service",
                                // content: error.join('<br>'),
                                // number: error.length
                            });
                            $scope.getData();
                        } else if (res.data == 2) {
                            // ga bs gate out
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Proses Billing Belum Lunas",
                                // content: error.join('<br>'),
                                // number: error.length
                            });
                            $scope.getData();
                        } else if (res.data == 3){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Ada DP / OR Belum Lunas",
                            });
                            $scope.getData();

                        } else if (res.data == 4){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Status kendaraan telah berubah, harap lakukan refresh terlebih dahulu",
                            });
                            $scope.getData();
                        }else {
                            // validasi ini di komen karena kata nya dp wo / dp booking bs di balikin pake menu finance, jd menu AS ga perlu pake stoper validasi -- di pasang lg, bolak balik aja trs
                            
                            if (row.JobId !== undefined && row.JobId !== '' && row.JobId !== null && row.JobId !== 0) {
                                Gate.CheckAppointmentDP(row.JobId).then(function(resu){
                                    var cek = resu.data;
                                    var hasil = cek.split('#');
                                    var valid = hasil[0];
                                    var totalDP = hasil[1];
                                    var totalDPFix = totalDP.split(',');
                                    if (valid == 666){
                                        // bsAlert.warning("Tidak bisa gate out karena sudah ada DP appointment sebesar " + totalDPFix[0] + " yang telah dibayar")
                                        bsAlert.alert({
                                            title: "Kendaraan ini memiliki DP yang belum di kembalikan,  silakan hubungi petugas Appointment atau SA di area penerimaan",
                                            text: "",
                                            type: "info",
                                            showCancelButton: false
                                        },
                                        function() {
                                            bsAlert.alert({
                                                title: "Apakah anda yakin mobil No. Polisi " + row.PoliceNo + " akan keluar?",
                                                text: "",
                                                type: "info",
                                                showCancelButton: true
                                            },
                                            function() {
                                                $scope.visitorOut(row);
                                            },
                                            function() {
                            
                                            })
                                        },
                                        function() {
                        
                                        })
                                    } else {
                                        bsAlert.alert({
                                            title: "Apakah anda yakin mobil No. Polisi " + row.PoliceNo + " akan keluar?",
                                            text: "",
                                            type: "info",
                                            showCancelButton: true
                                        },
                                        function() {
                                            $scope.visitorOut(row);
                                        },
                                        function() {
                        
                                        })
                                    }
                                })
                            } else {
                                bsAlert.alert({
                                    title: "Apakah anda yakin mobil No. Polisi " + row.PoliceNo + " akan keluar?",
                                    text: "",
                                    type: "info",
                                    showCancelButton: true
                                },
                                function() {
                                    $scope.visitorOut(row);
                                },
                                function() {
                
                                })
                            }
                            
                        }
                        
                    });
                }
            })
            
            // bsAlert.alert({
            //         title: "Apakah anda yakin mobil No. Polisi " + row.PoliceNo + " akan keluar?",
            //         text: "",
            //         type: "info",
            //         showCancelButton: true
            //     },
            //     function() {
            //         $scope.visitorOut(row);
            //     },
            //     function() {

            //     }
            // )
        }
        $scope.checkInCar = function(type, row, flag, queue_kah) {
                console.log("row CheckIn", row);
                console.log("jam controller", new Date());
                if (flag == undefined || flag == null){
                    flag = 1;
                }
                var jobid = 0;
                if (flag == 2){
                    jobid = row.JobId;
                }

                if (flag == 'Others/VIP GR') {
                    flag = 'Others GR'
                } else if (flag == 'Others/VIP BP') {
                    flag = 'Others BP'
                }

                $scope.backdrop = true
                // console.log("jam controller", new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
                row.time = new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds();
                row.DateNow = new Date();
                console.log("row CheckIn", row);
                // Gate.checkAvailableByPoliceNo(type, row,1).then(function(rs) {
                
                Gate.checkAvailableByPoliceNoExclusive(type, row,flag, jobid).then(function(rs) {
                    if (rs.data.IsAvailable == false) {
                        if (jobid > 0){
                            Gate.getStatWO(row.JobId,row.Status).then(function(resul){
                                if (resul.data == 0){
                                    var tmpLicenseplate = row.LicensePlate.toUpperCase();
                                    Gate.checkIn(type, row).then(function(reres) {
                                        if (reres.data.ResponseCode == 13) {
                                            $scope.backdrop = false

                                            bsAlert.alert({
                                                    title: "No. Polisi " + tmpLicenseplate + " berhasil disimpan",
                                                    text: "",
                                                    type: "success",
                                                    showCancelButton: false
                                                },
                                                function() {
                                                    $scope.mData.LicensePlate = ""
                                                    $scope.getData();
                                                },
                                                function() {
                
                                                }
                                            )
                                        } else if (reres.data.ResponseCode == undefined) {
                                            if (reres.data.RootCause == 0){
                                                //boleh gatein kok 
                                                console.log('boleh gatein kok')
                                                $scope.backdrop = false
                                            } else if (reres.data.RootCause == 1){
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "Kendaraan dengan No. Polisi tersebut sedang proses service oleh SA " + rs.data.SAName + '.',
                                                    
                                                });
                                                $scope.backdrop = false
                                            } else if (reres.data.RootCause == 2){
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "Kendaraan dengan No Polisi " + row.LicensePlate.toUpperCase() + " masih terdapat Billing yang belum lunas, harap lunasi dahulu kemudian lakukan Gate Out.",
                                                    
                                                });
                                                $scope.backdrop = false
                                            } else if (reres.data.RootCause == 3){
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "Ada DP / OR Belum Lunas",
                                                    
                                                });
                                                $scope.backdrop = false
                                            } else if (reres.data.RootCause == 4) {
                                                if (queue_kah != 'tidak_queue') {
                                                    bsNotify.show({
                                                        size: 'big',
                                                        type: 'danger',
                                                        // title: "Kendaraan Dengan No. Polisi " + row.LicensePlate.toUpperCase() + " terdapat di Gate Out-Visitor. Lakukan Gate Out terlebih dahulu",
                                                        title: "Kendaraan Dengan No. Polisi " + row.LicensePlate.toUpperCase() + " sudah Gate In dan ada di antrian.",
                                                        
                                                    });
                                                } else {
                                                    bsNotify.show({
                                                        size: 'big',
                                                        type: 'danger',
                                                        // title: "Kendaraan Dengan No. Polisi " + row.LicensePlate.toUpperCase() + " terdapat di Gate Out-Visitor. Lakukan Gate Out terlebih dahulu",
                                                        title: "Kendaraan Dengan No. Polisi " + row.LicensePlate.toUpperCase() + " sudah Gate In.",
                                                        
                                                    });
                                                }
                                                
                                                $scope.backdrop = false
                                            } else if (reres.data.RootCause == 5) {
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "WO Kendaraan dengan No Polisi " + row.LicensePlate.toUpperCase() + " sudah dibatalkan, harap lakukan gate out dari Visitor dahulu.",
                                                });
                                                $scope.backdrop = false
                                            } else if (reres.data.RootCause == 6) {
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "Kendaraan dengan No Polisi " + row.LicensePlate.toUpperCase() + " sudah Selesai Service, harap lakukan Gate Out dahulu.",
                                                });
                                                $scope.backdrop = false
                                            } 

                                        }
                                        
                                    },function(err) {
                                        console.log("err=>", err);
                                        $scope.backdrop = false
                                    });
                                } else  {
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "Data Ini Ada Perubahan, Refresh Terlebih Dahulu",
                                        // content: error.join('<br>'),
                                        // number: error.length
                                    });
                                    $scope.backdrop = false
                                }
                            },function(err) {
                                console.log("err=>", err);
                                $scope.backdrop = false
                            })
                        } else {
                            var tmpLicenseplate = row.LicensePlate.toUpperCase();
                                    Gate.checkIn(type, row).then(function(reres) {

                                        if (reres.data.ResponseCode == 13) {
                                            $scope.backdrop = false

                                            bsAlert.alert({
                                                title: "No. Polisi " + tmpLicenseplate + " berhasil disimpan",
                                                text: "",
                                                type: "success",
                                                showCancelButton: false
                                            },
                                            function() {
                                                $scope.mData.LicensePlate = ""
                                                $scope.getData();
                                            },
                                            function() {
            
                                            }
                                            )
                                        } else if (reres.data.ResponseCode == undefined) {
                                            if (reres.data.RootCause == 0){
                                                //boleh gatein kok 
                                                console.log('boleh gatein kok')
                                                $scope.backdrop = false
                                            } else if (reres.data.RootCause == 1){
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "Kendaraan dengan No. Polisi tersebut sedang proses service oleh SA " + rs.data.SAName + '.',
                                                    
                                                });
                                                $scope.backdrop = false
                                            } else if (reres.data.RootCause == 2){
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "Kendaraan dengan No Polisi " + row.LicensePlate.toUpperCase() + " masih terdapat Billing yang belum lunas, harap lunasi dahulu kemudian lakukan Gate Out.",
                                                    
                                                });
                                                $scope.backdrop = false
                                            } else if (reres.data.RootCause == 3){
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "Ada DP / OR Belum Lunas",
                                                    
                                                });
                                                $scope.backdrop = false
                                            } else if (reres.data.RootCause == 4) {
                                                if (queue_kah != 'tidak_queue') {
                                                    bsNotify.show({
                                                        size: 'big',
                                                        type: 'danger',
                                                        // title: "Kendaraan Dengan No. Polisi " + row.LicensePlate.toUpperCase() + " terdapat di Gate Out-Visitor. Lakukan Gate Out terlebih dahulu",
                                                        title: "Kendaraan Dengan No. Polisi " + row.LicensePlate.toUpperCase() + " sudah Gate In dan ada di antrian.",
                                                        
                                                    });
                                                } else {
                                                    bsNotify.show({
                                                        size: 'big',
                                                        type: 'danger',
                                                        // title: "Kendaraan Dengan No. Polisi " + row.LicensePlate.toUpperCase() + " terdapat di Gate Out-Visitor. Lakukan Gate Out terlebih dahulu",
                                                        title: "Kendaraan Dengan No. Polisi " + row.LicensePlate.toUpperCase() + " sudah Gate In.",
                                                        
                                                    });
                                                }
                                                
                                                $scope.backdrop = false
                                            } else if (reres.data.RootCause == 5) {
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "WO Kendaraan dengan No Polisi " + row.LicensePlate.toUpperCase() + " sudah dibatalkan, harap lakukan gate out dari Visitor dahulu.",
                                                });
                                                $scope.backdrop = false
                                            } else if (reres.data.RootCause == 6) {
                                                bsNotify.show({
                                                    size: 'big',
                                                    type: 'danger',
                                                    title: "Kendaraan dengan No Polisi " + row.LicensePlate.toUpperCase() + " sudah Selesai Service, harap lakukan Gate Out dahulu.",
                                                });
                                                $scope.backdrop = false
                                            } 
                                            
                                        }


                                        // if (queue_kah != 'tidak_queue') {
                                            
                                        // } else {
                                        //     bsAlert.alert({
                                        //         title: "Kendaraan Dengan No. Polisi " + tmpLicenseplate + " sudah Gate In",
                                        //         text: "",
                                        //         type: "success",
                                        //         showCancelButton: false
                                        //     },
                                        //     function() {
                                        //         $scope.mData.LicensePlate = ""
                                        //         $scope.getData();
                                        //     },
                                        //     function() {
            
                                        //     }
                                        //     )
                                        // }
                                        
                                    },function(err) {
                                        console.log("err=>", err);
                                        $scope.backdrop = false
                                    });
                        }
                        

                        
                    } else {
                        if (rs.data.RootCause == 0){
                            //boleh gatein kok 
                            console.log('boleh gatein kok')
                            $scope.backdrop = false
                        } else if (rs.data.RootCause == 1){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Kendaraan dengan No. Polisi tersebut sedang proses service oleh SA " + rs.data.SAName + '.',
                                
                            });
                            $scope.backdrop = false
                        } else if (rs.data.RootCause == 2){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Kendaraan dengan No Polisi " + row.LicensePlate.toUpperCase() + " masih terdapat Billing yang belum lunas, harap lunasi dahulu kemudian lakukan Gate Out.",
                                
                            });
                            $scope.backdrop = false
                        } else if (rs.data.RootCause == 3){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Ada DP / OR Belum Lunas",
                                
                            });
                            $scope.backdrop = false
                        } else if (rs.data.RootCause == 4) {
                            if (queue_kah != 'tidak_queue') {
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    // title: "Kendaraan Dengan No. Polisi " + row.LicensePlate.toUpperCase() + " terdapat di Gate Out-Visitor. Lakukan Gate Out terlebih dahulu",
                                    title: "Kendaraan Dengan No. Polisi " + row.LicensePlate.toUpperCase() + " sudah Gate In dan ada di antrian.",
                                    
                                });
                            } else {
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    // title: "Kendaraan Dengan No. Polisi " + row.LicensePlate.toUpperCase() + " terdapat di Gate Out-Visitor. Lakukan Gate Out terlebih dahulu",
                                    title: "Kendaraan Dengan No. Polisi " + row.LicensePlate.toUpperCase() + " sudah Gate In.",
                                    
                                });
                            }
                            
                            $scope.backdrop = false
                        } else if (rs.data.RootCause == 5) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "WO Kendaraan dengan No Polisi " + row.LicensePlate.toUpperCase() + " sudah dibatalkan, harap lakukan gate out dari Visitor dahulu.",
                            });
                            $scope.backdrop = false
                        } else if (rs.data.RootCause == 6) {
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Kendaraan dengan No Polisi " + row.LicensePlate.toUpperCase() + " sudah Selesai Service, harap lakukan Gate Out dahulu.",
                            });
                            $scope.backdrop = false
                        } 
                        
                    }
                },function(err) {
                    console.log("err=>", err);
                    $scope.backdrop = false
                });




            }
            //checkInProduction
        $scope.checkInCarProduction = function(type, row) {
            console.log("row CheckInProduction", row);
            // if (row.sentVehicleFrom == 2) {
            // } else {
            Gate.checkInProduction(row).then(function() {
                $scope.mData.LicensePlate = ""
                $scope.getData();
            });
            // };
        };
        $scope.getOutCar = function(row) {
            console.log("row", row);
            if (row.sentVehicleFrom == 2) { // for testdrive dari FI production
                Gate.gateOutCarTestDrive(row).then(function() {
                    bsAlert.alert({
                        title: "Mobil dengan No. Polisi " + row.LicensePlate + " telah keluar",
                        text: "",
                        type: "info",
                        showCancelButton: false
                    });
                    $scope.getData();
                    // getAllData();
                    // return $q.resolve(
                    //     null
                    // )
                });
            } else {
                Gate.gateOutCar(row).then(function() {
                    bsAlert.alert({
                        title: "Mobil dengan No. Polisi " + row.LicensePlate + " telah keluar",
                        text: "",
                        type: "info",
                        showCancelButton: false
                    });
                    $scope.getData();
                    // getAllData();
                    // return $q.resolve(
                    //     null
                    // )
                });
            }

        }

        $scope.gateOutCarRawatJalan = function(row) {
            console.log("row", row);
            Gate.gateOutCarRawatJalan(row).then(function() {
                bsAlert.alert({
                    title: "Mobil dengan No. Polisi " + row.PoliceNumber + " telah keluar",
                    text: "",
                    type: "info",
                    showCancelButton: false
                });
                $scope.getData();
            });
        }

        $scope.visitorOut = function(row) {
            console.log("row", row);
            Gate.GetStatusGateQueue(row).then(function(result) {
                if (result.data == 666){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Kendaraan dengan No. Polisi tersebut dalam proses pembuatan WO",
                    });
                    $scope.getData();
                } else {
                    Gate.GetStatusJobByPoliceNo(row).then(function(res) {
                        if (res.data == 1){
                            // ga bs gate out
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Kendaraan dengan No. Polisi tersebut sedang proses service",
                            });
                            $scope.getData();
                        } else if (res.data == 2) {
                            // ga bs gate out
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Proses Billing Belum Lunas",
                            });
                            $scope.getData();
                        } else if (res.data == 3){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Ada DP / OR Belum Lunas",
                            });
                            $scope.getData();

                        } else if (res.data == 4){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Status kendaraan telah berubah, harap lakukan refresh terlebih dahulu",
                            });
                            $scope.getData();
                        } else {
                            Gate.visitorOut(row).then(function(res) {
                                var resp = res;
                                // Checking if the data is creating WO
                                // if (resp == 2) {
                                //     bsNotify.show({
                                //         size: 'big',
                                //         type: 'danger',
                                //         title: "Kendaraan dengan No. Polisi tersebut sedang proses pembuatan Work Order",
                                //     });
                                //     return -1;
                                // }
                
                                bsAlert.alert({
                                    title: "Mobil dengan No. Polisi " + row.PoliceNo + " telah keluar",
                                    text: "",
                                    type: "info",
                                    showCancelButton: false
                                });
                                $scope.getData();
                            });
                        }
                    })

                }
            })
            
        }

        //----------------------------------
        // Grid Setup
        //----------------------------------

        //col_field itu bisa di taruh tennary cuman gw gak bikin gitu soalnya blom tau bentuk backendnya
        //nanti harusnya kejer biar dari sini aja nentuin gylp nya pakai apa
        //kalo gak isa pakai array.map(function(){});
        var cellTemplatePSFU = '<div class="ui-grid-cell-contents" ng-bind-html="COL_FIELD">' +
            '</div>';

        var cellTemplateStatus = '<div class="ui-grid-cell-contents" ng-bind-html="COL_FIELD">' +
            '</div>';

        // 98: WI Bp Satellite, 87: GI Appoint
        // 2394: Gate Out Satelite, On The Way, 2398: Gate Out Center, On The Way
        var actionCheckIn = '<div class="ui-grid-cell-contents" style="text-align:center;">' +
            '<a href="#" ng-click="grid.appScope.actCheckInCar((row.entity.Status == 2394 || row.entity.Status == 2398 || row.entity.StatusGateSatellite == 2394 || row.entity.StatusGateSatellite == 2398 ? 98 : 87),row.entity)" uib-tooltip="Check-In" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-check" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';

        var actionCheckInProduction = '<div class="ui-grid-cell-contents" style="text-align:center;">' +
            '<a href="#" ng-click="grid.appScope.actCheckInCarProduction(87,row.entity)" uib-tooltip="Check-In" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-check" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';
        var actionGetOutCar = '<div class="ui-grid-cell-contents" style="text-align:center;">' +
            '<a href="#" ng-click="grid.appScope.actGetOutCar(row.entity)" uib-tooltip="Check-Out" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-check" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';

        var actionGetOutCarRawatJalan = '<div class="ui-grid-cell-contents" style="text-align:center;">' +
            '<a href="#" ng-click="grid.appScope.actionGetOutCarRawatJalan(row.entity)" uib-tooltip="Check-Out" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-check" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';

        var actionVisitor = '<div class="ui-grid-cell-contents" style="text-align:center;">' +
            '<a href="#" ng-click="grid.appScope.actVisitorCar(row.entity)" uib-tooltip="Check-Out" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-check" style="padding:3px 8px 8px 0px;margin-left:8px;"></i></a>' +
            '</div>';
        $scope.alertConfirm = function(type, item, desc, queue_kah) {
            bsAlert.alert({
                    title: "Apakah anda yakin Memilih " + desc + " ?",
                    text: "",
                    type: "question",
                    showCancelButton: true
                },
                function() {
                    $scope.checkInCar(type, item, desc, queue_kah);
                },
                function() {

                }
            )
        };
        $scope.buyCar = function(item) {
            var cekalphanum = $scope.alphanumValid()
            if (cekalphanum == false){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "No. Polisi harus huruf dan angka saja",
                });
                return false
            }
            var type = 88;
            console.log(item);
            if (item.LicensePlate != null && item.LicensePlate != "") {
                item.LicensePlate = item.LicensePlate.replace(/\s/g, '') //hilangkan spasi
                // $scope.checkInCar(type, item);
                var desc = "Beli Mobil"
                $scope.alertConfirm(type, item, desc, 'tidak_queue');
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input No. Polisi",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            }
        };

        $scope.serviceGr = function(item) {

            var cekalphanum = $scope.alphanumValid()
            if (cekalphanum == false){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "No. Polisi harus huruf dan angka saja",
                });
                return false
            }

            $scope.loading = false;
            // =======Get Appointment=========
            var gridData = [];
            $scope.isBPSatelite = false;

            Gate.getDataAppointment().then(function(res) {
                    // gridData = res.data.Result;
                    console.log("gridata", res.data.Result);
                    for (var i = 0; i < res.data.Result.length; i++) {
                        gridData.push(res.data.Result[i]);
                    }
                    // =====================
                    // Gate.getDataforGatein().then(function(resu) {
                    //     // var gridData = res.data;
                    //     for (var i = 0; i < resu.data.length; i++) {
                    //         resu.data[i].VehicleModelName = resu.data[i].TypeName == null ? resu.data[i].ModelType : resu.data[i].ModelType + ' - ' + resu.data[i].TypeName;
                    //         gridData.push(resu.data[i]);
                    //     }
                        console.log("gridata From Gate In=>", gridData);


                        Gate.gateFromSatelite_GR().then(function(resul_GR) {
                            Gate.gateFromSatelite_BP().then(function(resul_BP) {
                                console.log('nah ini sate GR', resul_GR)
                                console.log('nah ini sate BP', resul_BP)

                                $scope.isBPSatelite = true;
                                // console.log("gateFromSatelite", resul.data.Result)
                                for (var i = 0; i < resul_GR.data.Result.length; i++) {
                                    resul_GR.data.Result[i].VehicleModelName = resul_GR.data.Result[i].TypeName == null ? resul_GR.data.Result[i].VehicleModelName : resul_GR.data.Result[i].VehicleModelName + ' - ' + resul_GR.data.Result[i].TypeName;
                                    resul_GR.data.Result[i].Bengkel = 'GR'
                                    gridData.push(resul_GR.data.Result[i]);
                                }
                                for (var i = 0; i < resul_BP.data.Result.length; i++) {
                                    resul_BP.data.Result[i].VehicleModelName = resul_BP.data.Result[i].TypeName == null ? resul_BP.data.Result[i].VehicleModelName : resul_BP.data.Result[i].VehicleModelName + ' - ' + resul_BP.data.Result[i].TypeName;
                                    resul_BP.data.Result[i].Bengkel = 'BP'
                                    gridData.push(resul_BP.data.Result[i]);
                                }

                                

                                gridData.sort(function(a, b) {
                                    var nameA = a.AppointmentTime // ignore upper and lowercase
                                    var nameB = b.AppointmentTime // ignore upper and lowercase
                                    console.log("a,b", a, b);
                                    if (nameA < nameB) {
                                        return -1;
                                    }
                                    if (nameA > nameB) {
                                        return 1;
                                    }

                                    // names must be equal
                                    return 0;
                                });

                                console.log("gridata From Appointment=>", gridData);
                                $scope.gridBook1 = angular.copy(gridData);
                            
                                $scope.loading = false;

                                // ================================== jeroan isi function ServiceGR ===================================================================
                                if (item.LicensePlate != null && item.LicensePlate != "" && item.LicensePlate != undefined) {
                                    item.LicensePlate = item.LicensePlate.replace(/\s/g, '') //hilangkan spasi
                                } else {
                                    item.LicensePlate = '';
                                }
                                var alreadyExist = 0;
                                for (var i = 0; i < $scope.gridBook1.length; i++) {
                                    console.log('item', item.LicensePlate);
                                    console.log('gridBook1', $scope.gridBook1[i].LicensePlate);
                                    // if (item.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate && $scope.gridBook1[i].isGr == 1) { // hrs nya uda ga pake isGr lagi karena sudah beda outletid
                                    if (item.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate ) {
                                        alreadyExist = 1;
                                    }
                                }
                                if (alreadyExist == 0) { //validation buat plat nomor yang sudah booking GR tidak boleh GR tapi boleh BP, yg BP ga boleh BP tapi boleh GR
                                    var type = 89;
                                    if (item.LicensePlate != null && item.LicensePlate != "") {
                                        // $scope.checkInCar(type, item);
                                        var desc = "Service GR"
                                        $scope.alertConfirm(type, item, desc);
                                    } else {
                                        bsNotify.show({
                                            size: 'big',
                                            type: 'danger',
                                            title: "Mohon Input No. Polisi",
                                            // content: error.join('<br>'),
                                            // number: error.length
                                        });
                                    }
                                } else {
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "No. Polisi Sudah Booking / Sedang Service",
                                        // content: error.join('<br>'),
                                        // number: error.length
                                    });
                                }
                                // ================================== jeroan isi function ServiceGR ===================================================================
                                $scope.gridGateIn.data = gridData;
                                console.log("$scope.gridGateIn.data", $scope.gridGateIn.data);

                                // try order data by nopol, asc =============================
                                var SortedData2 = _.sortBy($scope.gridGateIn.data, ['LicensePlate']);
                                $scope.gridGateIn.data = SortedData2
                                // try order data by nopol, asc =============================


                                // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ start
                                // for (var e = 0; e<$scope.gridGateIn.data.length; e++) {
                                //     $scope.gridGateIn.data[e].Bengkel = $scope.gridGateIn.data[e].Bengkel ? $scope.gridGateIn.data[e].Bengkel : '-';
                                //     $scope.gridGateIn.data[e].VehicleModelName = $scope.gridGateIn.data[e].VehicleModelName ? $scope.gridGateIn.data[e].VehicleModelName : '-';
                                //     $scope.gridGateIn.data[e].ColorName = $scope.gridGateIn.data[e].ColorName ? $scope.gridGateIn.data[e].ColorName : '-';
                                //     $scope.gridGateIn.data[e].LicensePlate = $scope.gridGateIn.data[e].LicensePlate ? $scope.gridGateIn.data[e].LicensePlate : '-';
                                //     $scope.gridGateIn.data[e].Name = $scope.gridGateIn.data[e].Name ? $scope.gridGateIn.data[e].Name : '-';
                                //     $scope.gridGateIn.data[e].AppointmentTime = $scope.gridGateIn.data[e].AppointmentTime ? $scope.gridGateIn.data[e].AppointmentTime : '-';
                                // }
                                // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ end


                            })
                        })


                        // ----------------------- komen dl karena mau di satuin gate GR dan bp nya, jadi pake yg di atas ---------------------------------------- start
                        // Gate.gateFromSatelite().then(function(resul) {
                        //     // var gridData = res.data;
                        //     $scope.isBPSatelite = true;
                        //     console.log("gateFromSatelite", resul.data.Result)
                        //     for (var i = 0; i < resul.data.Result.length; i++) {
                        //         resul.data.Result[i].VehicleModelName = resul.data.Result[i].TypeName == null ? resul.data.Result[i].VehicleModelName : resul.data.Result[i].VehicleModelName + ' - ' + resul.data.Result[i].TypeName;
                        //         gridData.push(resul.data.Result[i]);
                        //     }

                        //     gridData.sort(function(a, b) {
                        //         var nameA = a.AppointmentTime // ignore upper and lowercase
                        //         var nameB = b.AppointmentTime // ignore upper and lowercase
                        //         console.log("a,b", a, b);
                        //         if (nameA < nameB) {
                        //             return -1;
                        //         }
                        //         if (nameA > nameB) {
                        //             return 1;
                        //         }

                        //         // names must be equal
                        //         return 0;
                        //     });

                        //     console.log("gridata From Appointment=>", gridData);
                        //     $scope.gridBook1 = angular.copy(gridData);
                           
                        //     $scope.loading = false;

                        //     // ================================== jeroan isi function ServiceGR ===================================================================
                        //     if (item.LicensePlate != null && item.LicensePlate != "" && item.LicensePlate != undefined) {
                        //         item.LicensePlate = item.LicensePlate.replace(/\s/g, '') //hilangkan spasi
                        //     } else {
                        //         item.LicensePlate = '';
                        //     }
                        //     var alreadyExist = 0;
                        //     for (var i = 0; i < $scope.gridBook1.length; i++) {
                        //         console.log('item', item.LicensePlate);
                        //         console.log('gridBook1', $scope.gridBook1[i].LicensePlate);
                        //         // if (item.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate && $scope.gridBook1[i].isGr == 1) { // hrs nya uda ga pake isGr lagi karena sudah beda outletid
                        //         if (item.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate ) {
                        //             alreadyExist = 1;
                        //         }
                        //     }
                        //     if (alreadyExist == 0) { //validation buat plat nomor yang sudah booking GR tidak boleh GR tapi boleh BP, yg BP ga boleh BP tapi boleh GR
                        //         var type = 89;
                        //         if (item.LicensePlate != null && item.LicensePlate != "") {
                        //             // $scope.checkInCar(type, item);
                        //             var desc = "Service GR"
                        //             $scope.alertConfirm(type, item, desc);
                        //         } else {
                        //             bsNotify.show({
                        //                 size: 'big',
                        //                 type: 'danger',
                        //                 title: "Mohon Input No. Polisi",
                        //                 // content: error.join('<br>'),
                        //                 // number: error.length
                        //             });
                        //         }
                        //     } else {
                        //         bsNotify.show({
                        //             size: 'big',
                        //             type: 'danger',
                        //             title: "No. Polisi Sudah Booking / Sedang Service",
                        //             // content: error.join('<br>'),
                        //             // number: error.length
                        //         });
                        //     }
                        //     // ================================== jeroan isi function ServiceGR ===================================================================
                        //     $scope.gridGateIn.data = gridData;
                        //     console.log("$scope.gridGateIn.data", $scope.gridGateIn.data);
                        // });
                        // ----------------------- komen dl karena mau di satuin gate GR dan bp nya, jadi pake yg di atas ---------------------------------------- end

                    // });

                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            

            

        };
        $scope.serviceBp = function(item) {
            var cekalphanum = $scope.alphanumValid()
            if (cekalphanum == false){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "No. Polisi harus huruf dan angka saja",
                });
                return false
            }
            $scope.loading = false;
            // =======Get Appointment=========
            var gridData = [];
            $scope.isBPSatelite = false;

            Gate.getDataAppointment().then(function(res) {
                    // gridData = res.data.Result;
                    console.log("gridata", res.data.Result);
                    for (var i = 0; i < res.data.Result.length; i++) {
                        gridData.push(res.data.Result[i]);
                    }
                    // =====================
                    // Gate.getDataforGatein().then(function(resu) {
                    //     // var gridData = res.data;
                    //     for (var i = 0; i < resu.data.length; i++) {
                    //         resu.data[i].VehicleModelName = resu.data[i].TypeName == null ? resu.data[i].ModelType : resu.data[i].ModelType + ' - ' + resu.data[i].TypeName;
                    //         gridData.push(resu.data[i]);
                    //     }
                        console.log("gridata From Gate In=>", gridData);


                        Gate.gateFromSatelite_GR().then(function(resul_GR) {
                            Gate.gateFromSatelite_BP().then(function(resul_BP) {
                                console.log('nah ini sate GR', resul_GR)
                                console.log('nah ini sate BP', resul_BP)

                                $scope.isBPSatelite = true;
                                // console.log("gateFromSatelite", resul.data.Result)
                                for (var i = 0; i < resul_GR.data.Result.length; i++) {
                                    resul_GR.data.Result[i].VehicleModelName = resul_GR.data.Result[i].TypeName == null ? resul_GR.data.Result[i].VehicleModelName : resul_GR.data.Result[i].VehicleModelName + ' - ' + resul_GR.data.Result[i].TypeName;
                                    resul_GR.data.Result[i].Bengkel = 'GR'
                                    gridData.push(resul_GR.data.Result[i]);
                                }
                                for (var i = 0; i < resul_BP.data.Result.length; i++) {
                                    resul_BP.data.Result[i].VehicleModelName = resul_BP.data.Result[i].TypeName == null ? resul_BP.data.Result[i].VehicleModelName : resul_BP.data.Result[i].VehicleModelName + ' - ' + resul_BP.data.Result[i].TypeName;
                                    resul_BP.data.Result[i].Bengkel = 'BP'
                                    gridData.push(resul_BP.data.Result[i]);
                                }

                                gridData.sort(function(a, b) {
                                    var nameA = a.AppointmentTime // ignore upper and lowercase
                                    var nameB = b.AppointmentTime // ignore upper and lowercase
                                    console.log("a,b", a, b);
                                    if (nameA < nameB) {
                                        return -1;
                                    }
                                    if (nameA > nameB) {
                                        return 1;
                                    }

                                    // names must be equal
                                    return 0;
                                });

                                console.log("gridata From Appointment=>", gridData);
                                $scope.gridBook1 = angular.copy(gridData);
                            
                                $scope.loading = false;

                                // ================================== jeroan isi function ServiceBP ===================================================================
                                if (item.LicensePlate != null && item.LicensePlate != "" && item.LicensePlate != undefined) {
                                    item.LicensePlate = item.LicensePlate.replace(/\s/g, '') //hilangkan spasi
                                } else {
                                    item.LicensePlate = '';
                                }
                    
                                //here
                                var alreadyExist = 0;
                                for (var i = 0; i < $scope.gridBook1.length; i++) {
                                    console.log('item', item.LicensePlate);
                                    console.log('gridBook1', $scope.gridBook1[i].LicensePlate);
                                    // if (item.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate && $scope.gridBook1[i].isGr == 0) { // hrs nya uda ga pake isGr lagi karena sudah beda outletid
                                    if (item.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate) {
                                        alreadyExist = 1;
                                    }
                                }
                    
                                if (alreadyExist == 0) { //validation buat plat nomor yang sudah booking GR tidak boleh GR tapi boleh BP, yg BP ga boleh BP tapi boleh GR
                                    var type = 90;
                                    if (item.LicensePlate != null && item.LicensePlate != "") {
                                        // $scope.checkInCar(type, item);
                                        var desc = "Service BP"
                                        $scope.alertConfirm(type, item, desc);
                                    } else {
                                        bsNotify.show({
                                            size: 'big',
                                            type: 'danger',
                                            title: "Mohon Input No. Polisi",
                                            // content: error.join('<br>'),
                                            // number: error.length
                                        });
                                    }
                                } else {
                                    bsNotify.show({
                                        size: 'big',
                                        type: 'danger',
                                        title: "No. Polisi Sudah Booking / Sedang Service",
                                        // content: error.join('<br>'),
                                        // number: error.length
                                    });
                                }
                                // ================================== jeroan isi function ServiceBP ===================================================================

                                $scope.gridGateIn.data = gridData;
                                console.log("$scope.gridGateIn.data", $scope.gridGateIn.data);

                                // try order data by nopol, asc =============================
                                var SortedData2 = _.sortBy($scope.gridGateIn.data, ['LicensePlate']);
                                $scope.gridGateIn.data = SortedData2
                                // try order data by nopol, asc =============================


                                // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ start
                                // for (var e = 0; e<$scope.gridGateIn.data.length; e++) {
                                //     $scope.gridGateIn.data[e].Bengkel = $scope.gridGateIn.data[e].Bengkel ? $scope.gridGateIn.data[e].Bengkel : '-';
                                //     $scope.gridGateIn.data[e].VehicleModelName = $scope.gridGateIn.data[e].VehicleModelName ? $scope.gridGateIn.data[e].VehicleModelName : '-';
                                //     $scope.gridGateIn.data[e].ColorName = $scope.gridGateIn.data[e].ColorName ? $scope.gridGateIn.data[e].ColorName : '-';
                                //     $scope.gridGateIn.data[e].LicensePlate = $scope.gridGateIn.data[e].LicensePlate ? $scope.gridGateIn.data[e].LicensePlate : '-';
                                //     $scope.gridGateIn.data[e].Name = $scope.gridGateIn.data[e].Name ? $scope.gridGateIn.data[e].Name : '-';
                                //     $scope.gridGateIn.data[e].AppointmentTime = $scope.gridGateIn.data[e].AppointmentTime ? $scope.gridGateIn.data[e].AppointmentTime : '-';
                                // }
                                // pakai kalau butuh default - di tabel nya ------------------------------------------------------------------------ end


                            })
                        })


                        // ----------------------- komen dl karena mau di satuin gate GR dan bp nya, jadi pake yg di atas ---------------------------------------- start

                        // Gate.gateFromSatelite().then(function(resul) {
                        //     // var gridData = res.data;
                        //     $scope.isBPSatelite = true;
                        //     console.log("gateFromSatelite", resul.data.Result)
                        //     for (var i = 0; i < resul.data.Result.length; i++) {
                        //         resul.data.Result[i].VehicleModelName = resul.data.Result[i].TypeName == null ? resul.data.Result[i].VehicleModelName : resul.data.Result[i].VehicleModelName + ' - ' + resul.data.Result[i].TypeName;
                        //         gridData.push(resul.data.Result[i]);
                        //     }

                        //     gridData.sort(function(a, b) {
                        //         var nameA = a.AppointmentTime // ignore upper and lowercase
                        //         var nameB = b.AppointmentTime // ignore upper and lowercase
                        //         console.log("a,b", a, b);
                        //         if (nameA < nameB) {
                        //             return -1;
                        //         }
                        //         if (nameA > nameB) {
                        //             return 1;
                        //         }

                        //         // names must be equal
                        //         return 0;
                        //     });

                        //     console.log("gridata From Appointment=>", gridData);
                        //     $scope.gridBook1 = angular.copy(gridData);
                           
                        //     $scope.loading = false;

                        //      // ================================== jeroan isi function ServiceBP ===================================================================
                        //     if (item.LicensePlate != null && item.LicensePlate != "" && item.LicensePlate != undefined) {
                        //         item.LicensePlate = item.LicensePlate.replace(/\s/g, '') //hilangkan spasi
                        //     } else {
                        //         item.LicensePlate = '';
                        //     }
                
                        //     //here
                        //     var alreadyExist = 0;
                        //     for (var i = 0; i < $scope.gridBook1.length; i++) {
                        //         console.log('item', item.LicensePlate);
                        //         console.log('gridBook1', $scope.gridBook1[i].LicensePlate);
                        //         // if (item.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate && $scope.gridBook1[i].isGr == 0) { // hrs nya uda ga pake isGr lagi karena sudah beda outletid
                        //         if (item.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate) {
                        //             alreadyExist = 1;
                        //         }
                        //     }
                
                        //     if (alreadyExist == 0) { //validation buat plat nomor yang sudah booking GR tidak boleh GR tapi boleh BP, yg BP ga boleh BP tapi boleh GR
                        //         var type = 90;
                        //         if (item.LicensePlate != null && item.LicensePlate != "") {
                        //             // $scope.checkInCar(type, item);
                        //             var desc = "Service BP"
                        //             $scope.alertConfirm(type, item, desc);
                        //         } else {
                        //             bsNotify.show({
                        //                 size: 'big',
                        //                 type: 'danger',
                        //                 title: "Mohon Input No. Polisi",
                        //                 // content: error.join('<br>'),
                        //                 // number: error.length
                        //             });
                        //         }
                        //     } else {
                        //         bsNotify.show({
                        //             size: 'big',
                        //             type: 'danger',
                        //             title: "No. Polisi Sudah Booking / Sedang Service",
                        //             // content: error.join('<br>'),
                        //             // number: error.length
                        //         });
                        //     }
                        //     // ================================== jeroan isi function ServiceBP ===================================================================

                        //     $scope.gridGateIn.data = gridData;
                        //     console.log("$scope.gridGateIn.data", $scope.gridGateIn.data);


                        // });
                        // ----------------------- komen dl karena mau di satuin gate GR dan bp nya, jadi pake yg di atas ---------------------------------------- end


                    // });
                },
                function(err) {
                    console.log("err=>", err);
                }
            );
            


            
        }
        $scope.buyParts = function(item) {
            var cekalphanum = $scope.alphanumValid()
            $scope.dataModal = item;
            $scope.buttonClick = 'Beli Parts'
            $scope.labelBengkelGR = 'Bengkel GR'
            $scope.labelBengkelBP = 'Bengkel BP'
            if (cekalphanum == false){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "No. Polisi harus huruf dan angka saja",
                });
                return false
            }
            if (item.LicensePlate != null && item.LicensePlate != "" && item.LicensePlate != undefined) {
                item.LicensePlate = item.LicensePlate.replace(/\s/g, '') //hilangkan spasi
            }

            var alreadyExist = 0;
            for (var i = 0; i < $scope.gridBook1.length; i++) {
                console.log('item', item.LicensePlate);
                console.log('gridBook1', $scope.gridBook1[i].LicensePlate);
                // if (item.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate && $scope.gridBook1[i].isGr == 0) { // hrs nya uda ga pake isGr lagi karena sudah beda outletid
                if (item.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate) {
                    alreadyExist = 1;
                }
            }

            if (alreadyExist == 0){
                var type = 91;
                if (item.LicensePlate != null && item.LicensePlate != "") {
                    // $scope.checkInCar(type, item);
                    var desc = "Beli Parts"
                    // $scope.alertConfirm(type, item, desc);
                    // $('#ModalPilihBengkel').modal('show');
                    var numLength = angular.element('#ModalPilihBengkel').length;
                    setTimeout(function () {
                        angular.element('#ModalPilihBengkel').modal('refresh');
                    }, 0);

                    if (numLength > 1) {
                        angular.element('#ModalPilihBengkel').not(':first').remove();
                    }

                    angular.element("#ModalPilihBengkel").modal("show");
                } else {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Mohon Input No. Polisi",
                        // content: error.join('<br>'),
                        // number: error.length
                    });
                }
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "No. Polisi Sudah Booking / Sedang Service",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            }
            
        }

        $scope.buttonClick = null;
        $scope.dataModal = null;
        $scope.complaint = function(item) {
            $scope.dataModal = item;
            // $scope.buttonClick = "Complaint";
            $scope.buttonClick = "Return Job";

            $scope.labelBengkelGR = 'Bengkel GR'
            $scope.labelBengkelBP = 'Bengkel BP'
            var cekalphanum = $scope.alphanumValid()
            if (cekalphanum == false){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "No. Polisi harus huruf dan angka saja",
                });
                return false
            }
            var type = 92;
            if (item.LicensePlate != null && item.LicensePlate != "") {
                item.LicensePlate = item.LicensePlate.replace(/\s/g, '') //hilangkan spasi
                // $scope.checkInCar(type, item);
                var desc = "Complaint"
                // $('#ModalPilihBengkel').modal('show');
                var numLength = angular.element('#ModalPilihBengkel').length;
                    setTimeout(function () {
                        angular.element('#ModalPilihBengkel').modal('refresh');
                    }, 0);

                    if (numLength > 1) {
                        angular.element('#ModalPilihBengkel').not(':first').remove();
                    }

                    angular.element("#ModalPilihBengkel").modal("show");
                // $scope.alertConfirm(type, item, desc);
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input No. Polisi",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            }
        }
        $scope.othersGr = function(item) {
            $scope.dataModal = item;
            $scope.buttonClick = "Others/VIP";
            $scope.labelBengkelGR = 'Bengkel GR'
            $scope.labelBengkelBP = 'Bengkel BP'
            var cekalphanum = $scope.alphanumValid()
            if (cekalphanum == false){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "No. Polisi harus huruf dan angka saja",
                });
                return false
            }
            if (item.LicensePlate != null && item.LicensePlate != "" && item.LicensePlate != undefined) {
                item.LicensePlate = item.LicensePlate.replace(/\s/g, '') //hilangkan spasi
            }

            var alreadyExist = 0;
            for (var i = 0; i < $scope.gridBook1.length; i++) {
                console.log('item', item.LicensePlate);
                console.log('gridBook1', $scope.gridBook1[i].LicensePlate);
                // if (item.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate && $scope.gridBook1[i].isGr == 1) {
                if (item.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate) {
                    alreadyExist = 1;
                }
            }

            if (alreadyExist == 0) { //validation buat plat nomor yang sudah booking GR tidak boleh GR tapi boleh BP, yg BP ga boleh BP tapi boleh GR
                var type = 93;
                if (item.LicensePlate != null && item.LicensePlate != "") {
                    // $scope.checkInCar(type, item);
                    var desc = "Others GR"
                    // $scope.alertConfirm(type, item, desc);
                    // $('#ModalPilihBengkel').modal('show');
                    var numLength = angular.element('#ModalPilihBengkel').length;
                    setTimeout(function () {
                        angular.element('#ModalPilihBengkel').modal('refresh');
                    }, 0);

                    if (numLength > 1) {
                        angular.element('#ModalPilihBengkel').not(':first').remove();
                    }

                    angular.element("#ModalPilihBengkel").modal("show");
                } else {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Mohon Input No. Polisi",
                        // content: error.join('<br>'),
                        // number: error.length
                    });
                }
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "No. Polisi Sudah Booking / Sedang Service",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            }
        }
        $scope.takeBp = function(item) {
            var cekalphanum = $scope.alphanumValid()
            if (cekalphanum == false){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "No. Polisi harus huruf dan angka saja",
                });
                return false
            }
            if (item.LicensePlate != null && item.LicensePlate != "" && item.LicensePlate != undefined) {
                item.LicensePlate = item.LicensePlate.replace(/\s/g, '') //hilangkan spasi
            }

            var alreadyExist = 0;
            for (var i = 0; i < $scope.gridBook1.length; i++) {
                console.log('item', item.LicensePlate);
                console.log('gridBook1', $scope.gridBook1[i].LicensePlate);
                if (item.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate) {
                    alreadyExist = 1;
                }
            }

            if (alreadyExist == 0) { //validation buat plat nomor yang sudah booking GR tidak boleh GR tapi boleh BP, yg BP ga boleh BP tapi boleh GR

                var type = 94;
                if (item.LicensePlate != null && item.LicensePlate != "") {
                    // $scope.checkInCar(type, item);
                    var desc = "Pengambilan BP"
                    $scope.alertConfirm(type, item, desc);
                } else {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Mohon Input No. Polisi",
                        // content: error.join('<br>'),
                        // number: error.length
                    });
                }
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "No. Polisi Sudah Booking / Sedang Service",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            }
        }
        $scope.oplBengkel = function(item) {
            var type = 96;
            if (item.LicensePlate != null && item.LicensePlate != "") {
                // $scope.checkInCar(type, item);
                $scope.alertConfirm(type, item);
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input No. Polisi",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            }
        }
        $scope.finalInspection = function(item) {
            var type = 97;
            if (item.LicensePlate != null && item.LicensePlate != "") {
                // $scope.checkInCar(type, item);
                $scope.alertConfirm(type, item);
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input No. Polisi",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            }
        }


        // ==================== New PopUp Bengkel Start ==============================
        $scope.ModalPilihBengkel_GR = function (){
            // if($scope.buttonClick == "Complaint"){
            if($scope.buttonClick == "Return Job"){
                var type = 92;
                var desc = "Return Job";
                $('#ModalPilihBengkel').modal('hide')
                $scope.alertConfirm(type, $scope.dataModal, desc);
            }else if($scope.buttonClick == "Others/VIP"){
                var type = 93;
                var desc = "Others/VIP GR";
                $('#ModalPilihBengkel').modal('hide')
                $scope.alertConfirm(type, $scope.dataModal, desc);
            } else if ($scope.buttonClick == 'Visitor') {
                $scope.show_modal.show = !$scope.show_modal.show;
                $scope.visitor_from = 'GR'

            } else if ($scope.buttonClick == 'Beli Parts') {
                var type = 91;
                var desc = "Beli Parts";
                $('#ModalPilihBengkel').modal('hide')
                $scope.alertConfirm(type, $scope.dataModal, desc, 'tidak_queue');

            }
        }
        $scope.ModalPilihBengkel_BP = function(){
            // if($scope.buttonClick == "Complaint"){
            if($scope.buttonClick == "Return Job"){

                var type = 112;
                var desc = "Return Job";
                $('#ModalPilihBengkel').modal('hide')
                $scope.alertConfirm(type, $scope.dataModal, desc);
            }else if($scope.buttonClick == "Others/VIP"){
                var type = 113;
                var desc = "Others/VIP BP";
                $('#ModalPilihBengkel').modal('hide')
                $scope.alertConfirm(type, $scope.dataModal, desc);
            } else if ($scope.buttonClick == 'Visitor') {
                $scope.show_modal.show = !$scope.show_modal.show;
                $scope.visitor_from = 'BP'

            } else if ($scope.buttonClick == 'Beli Parts') {
                var type = 114;
                var desc = "Beli Parts";
                $('#ModalPilihBengkel').modal('hide')
                $scope.alertConfirm(type, $scope.dataModal, desc, 'tidak_queue');

            }
        }
        $scope.Kembali = function(){
            $('#ModalPilihBengkel').modal('hide')
        }
        // ==================== New PopUp Bengkel End ================================

        $scope.clickButtonSate = 0;
        $scope.sateliteBp = function(item) {
            if ($scope.clickButtonSate == 0){
                var cekalphanum = $scope.alphanumValid()
                if (cekalphanum == false){
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "No. Polisi harus huruf dan angka saja",
                    });
                    $scope.clickButtonSate = 0;
                    return false
                }
                if (item.LicensePlate != null && item.LicensePlate != "" && item.LicensePlate != undefined) {
                    item.LicensePlate = item.LicensePlate.replace(/\s/g, '') //hilangkan spasi
                }
    
                //here b
                var alreadyExist = 0;
                for (var i = 0; i < $scope.gridBook1.length; i++) {
                    console.log('item', item.LicensePlate);
                    console.log('gridBook1', $scope.gridBook1[i].LicensePlate);
                    if (item.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate) {
                        alreadyExist = 1;
                    }
                }
    
                if (alreadyExist == 0) { //validation buat plat nomor yang sudah booking GR tidak boleh GR tapi boleh BP, yg BP ga boleh BP tapi boleh GR
    
    
                    // var type = 98;
                    //Diganti Seperti Masuk BP Biasa
                    if ($scope.isSatelite) {
                        var type = 98;
                    } else {
                        var type = 90;
                    }
    
                    if (item.LicensePlate != null && item.LicensePlate != "") {
                        // $scope.checkInCar(type, item);
                        Gate.checkAvailableByPoliceNoSatellite(type,item.LicensePlate).then(function(resu){
                           
                            if (resu.data == -1){
                                //Gak bisa gate-in lagi karena nopol masih di bengkel satellite GR
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Kendaraan sudah ada di bengkel Satelite BP",
                                });
                                $scope.clickButtonSate = 0;
                            } else if (resu.data == -2) {
                                //Gak bisa gate-in lagi karena nopol masih di otw dari Center mau balik ke Satellite
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Kendaraan sudah di proses di BP Center dan sudah ada di list Gate Satelite BP",
                                });
                                $scope.clickButtonSate = 0;
                            } else if (resu.data == -11) {
                                //Gak bisa gate-in lagi karena nopol masih di bengkel Center BP
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Kendaraan sedang di proses di BP Center",
                                });
                                $scope.clickButtonSate = 0;
                            } else if (resu.data == -22) {
                                //Gak bisa gate-in lagi karena nopol masih di otw dari Satellite mau ke Center
                                bsNotify.show({
                                    size: 'big',
                                    type: 'danger',
                                    title: "Kendaraan sedang menuju BP Center dan sudah ada di list Gate BP Center",
                                });
                                $scope.clickButtonSate = 0;
                            } else if (resu.data == 0) {
                                var desc = "Penerimaan Satelite BP"
    
                                $scope.alertConfirm(type, item, desc);
                                setTimeout(function(){
                                    $scope.clickButtonSate = 0;
                                },200);
                                
                            }
    
                        }, function(err){
                            $scope.clickButtonSate = 0;
                        })
                        
                    } else {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: "Mohon Input No. Polisi",
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                        $scope.clickButtonSate = 0;
                    }
                } else {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "No. Polisi Sudah Booking / Sedang Service",
                        // content: error.join('<br>'),
                        // number: error.length
                    });
                    $scope.clickButtonSate = 0;
                }
            }
            $scope.clickButtonSate++;
            
        }
        $scope.walkInOthers = function(item) {
            var cekalphanum = $scope.alphanumValid()
            $scope.dataModal = item;
            $scope.buttonClick = 'Visitor'
            $scope.labelBengkelGR = 'Bengkel GR & Sales'
            $scope.labelBengkelBP = 'Bengkel BP'
            if (cekalphanum == false){
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "No. Polisi harus huruf dan angka saja",
                });
                return false
            }
            if (item.LicensePlate != null && item.LicensePlate != "" && item.LicensePlate != undefined) {
                item.LicensePlate = item.LicensePlate.replace(/\s/g, '') //hilangkan spasi
            }

            $scope.tmp.LicensePlate = item.LicensePlate;
            if (item.LicensePlate != null && item.LicensePlate != "") {
                // $scope.show_modal.show = !$scope.show_modal.show;
                // $('#ModalPilihBengkel').modal('show');
                var numLength = angular.element('#ModalPilihBengkel').length;
                    setTimeout(function () {
                        angular.element('#ModalPilihBengkel').modal('refresh');
                    }, 0);

                    if (numLength > 1) {
                        angular.element('#ModalPilihBengkel').not(':first').remove();
                    }

                    angular.element("#ModalPilihBengkel").modal("show");
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "Mohon Input No. Polisi",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            }
        }
        $scope.reasonSave = function(item) {

            var alreadyExist = 0;
            for (var i = 0; i < $scope.gridBook1.length; i++) {
                console.log('item', item.LicensePlate);
                console.log('gridBook1', $scope.gridBook1[i].LicensePlate);
                // if (item.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate && $scope.gridBook1[i].isGr == 0) { // hrs nya uda ga pake isGr lagi karena sudah beda outletid
                if ($scope.mData.LicensePlate.toUpperCase() == $scope.gridBook1[i].LicensePlate) {
                    alreadyExist = 1;
                }
            }

            if (alreadyExist == 0){
                $scope.modal_model = {};
                var type = 95;

                if ($scope.visitor_from == 'GR') {
                    type = 95
                } else if ($scope.visitor_from == 'BP') {
                    type = 115
                }

                $scope.tmp.Reason = item.Reason;
                console.log("$scope.tmp", $scope.tmp);
                // console.log("item",$scope.modal_model);
                if ($scope.tmp.LicensePlate != null) {
                    $scope.checkInCar(type, $scope.tmp, undefined, 'tidak_queue');
                    $scope.show_modal.show = !$scope.show_modal.show;
                } else {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: "Mohon Input No. Polisi",
                        // content: error.join('<br>'),
                        // number: error.length
                    });
                }
            } else {
                bsNotify.show({
                    size: 'big',
                    type: 'danger',
                    title: "No. Polisi Sudah Booking / Sedang Service",
                    // content: error.join('<br>'),
                    // number: error.length
                });
            }
        }
        $scope.reasonCancel = function(item) {
            console.log("item");
        }
        $scope.filterGrid = function(item) {
            if ($scope.mode == 'in') {
                $scope.gridApiGateIn.grid.columns[3].filters[0].term = item
                $scope.gridApiGateInProduction.grid.columns[2].filters[0].term = item;
                console.log("mode innn");
            } else {
                console.log("mode outttt");
                $scope.gridApiGateOutProduction.grid.columns[3].filters[0].term = item;
                $scope.gridApiGateOutPassingBy.grid.columns[3].filters[0].term = item;
                $scope.gridApiOutPatient.grid.columns[3].filters[0].term = item;
                $scope.gridApiGateOutFinishService.grid.columns[3].filters[0].term = item;
                console.log("$scope.gridApiGateOutFinishService.grid.columns[2].filters[0]", $scope.gridApiGateOutFinishService.grid.columns);
                console.log("$scope.gridApiGateOutPassingBy.grid.columns[2].filters[0]", $scope.gridApiGateOutPassingBy.grid.columns);
                console.log("$scope.gridApiOutPatient.grid.columns[2].filters[0]", $scope.gridApiOutPatient.grid.columns);
            }

        }

        // $scope.DataWarranty =
        // [
        //   {
        //     LicensePlate : 'F12345AS',
        //     PSFU : 'test',
        //     Name : 'Faisal',
        //     AppointmentTime : '20:00:00',
        //     VehicleModelName : 'Avanza',
        //     ColorName : 'green',
        //     Id : 1
        //   },
        //   {
        //     LicensePlate : 'F12345AS',
        //     PSFU : 'test',
        //     Name : 'Amieklo',
        //     AppointmentTime : '20:00:00',
        //     VehicleModelName : 'Agya',
        //     ColorName : 'red',
        //     Id : 2
        //   }
        // ];
        $scope.gridGateIn = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            enableFiltering: true,
            paginationPageSizes: [250, 500, 1000],
            // pageSize: 10,
            // widthForHorizontalScrolling: Math.floor(Math.random() * (85 - 30 + 1)) + 50,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 250,
            onRegisterApi: function(gridApi) {
                $scope.gridApiGateIn = gridApi;
            },
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            //row.ent
            columnDefs: [
                {
                    name: 'Bengkel',
                    displayName: 'Bengkel',
                    field: 'Bengkel',
                    width: '7.5%',
                    cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                        if (grid.getCellValue(row,col) === 'GR') {
                          return 'blue2';
                        } else {
                            return 'red2';
                        }
                    }
                },
                {
                    name: 'Model',
                    // ng-if="row.entity.Id == 2" (this if i find the condition)
                    // <div class="col-md-1"><i class="fa fa-compass" aria-hidden="true"></i></div><div class="col-md-1"><i class="fa fa-rss" aria-hidden="true"></i></div><div class="col-md-1"><i class="fa fa-plus" aria-hidden="true"></i></div><div class="col-md-1"><i class="fa fa-frown-o" aria-hidden="true"></i></div>
                    // cellTemplate: '<div class="ui-grid-cell-contents col-md-11" style="padding-left:0px;">{{row.entity.VehicleModelName}} - {{row.entity.ColorName}}</div>',
                    cellTemplate: '<div class="ui-grid-cell-contents col-md-11" style="padding-left:0px;">{{row.entity.VehicleModelName}}</div>',
                    width: '25%'
                },
                {
                    name: 'Warna',
                    field: 'ColorName',
                    width: '15%'
                },
                // <div class="col-md-1" style="color:red;"><i class="fa fa-plus" aria-hidden="true"></i></div>
                {
                    name: 'No.Polisi',
                    displayName: 'No. Polisi',
                    field: 'LicensePlate',
                    cellTemplate: '<div class="ui-grid-cell-contents col-md-11" style="padding-left:0px;"> <div class="col-md-7">{{row.entity.LicensePlate}}</div><div class="col-md-1"  ng-show="row.entity.AppointmentVia == 3"><i class="fa fa-frown-o" aria-hidden="true"></i></div><div class="col-md-1"  ng-show="row.entity.iconForbidden == 1" style="color:green"><i class="fa fa-minus-circle" aria-hidden="true"></i></div><div class="col-md-1"  ng-show="row.entity.StatusId == 25" style="color:red"><i class="fa fa-plus" aria-hidden="true"></i></div><div class="col-md-1"  ng-show="row.entity.Status == 2396 || row.entity.Status == 2397"><i class="fa fa-tencent-weibo" aria-hidden="true"></i></div><div class="col-md-1"  ng-show="row.entity.Status == 2394 || row.entity.Status == 2398 || row.entity.StatusGateSatellite == 2394 || row.entity.StatusGateSatellite == 2398"><i class="fa fa-rss" aria-hidden="true"></i></div></div>',
                    width: '15%'
                }, 
               // {
                //     name: 'PSFU',
                //     displayName: 'PSFU',
                //     field: 'PSFU',
                //     cellTemplate: cellTemplatePSFU,
                //     width: '10%'
                // },
                // {
                //     name: 'Status',
                //     field: 'Status',
                //     cellTemplate: cellTemplateStatus,
                //     width: '10%'
                // },
                {
                    name: 'Nama',
                    field: 'Name',
                    width: '25%'
                },
                {
                    name: 'Janji',
                    field: 'AppointmentTime',
                    width: '15%'
                },
                {
                    name: 'Check In',
                    field: 'Action',
                    cellTemplate: actionCheckIn,
                    width: 85,
                    pinnedRight: true,
                    // enableSorting: false
                }
            ]
        };
        $scope.gridGateOutPassingBy = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            enableFiltering: true,
            paginationPageSizes: [250, 500, 1000],
            // pageSize: 10,
            // widthForHorizontalScrolling: Math.floor(Math.random() * (85 - 30 + 1)) + 50,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 250,
            onRegisterApi: function(gridApi) {
                $scope.gridApiGateOutPassingBy = gridApi;
            },
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                // {
                //     name: 'Model',
                //     //cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.VehicleModelName}} - {{row.entity.ColorName}}</div>',
                //     //cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.VehicleModelName}}</div>',
                //     //cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.ModelType}}</div>',
                //     field: 'Job.ModelType',
                //     width: '25%'
                // },
                {
                    name: 'Bengkel',
                    //cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.VehicleModelName}} - {{row.entity.ColorName}}</div>',
                    //cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.VehicleModelName}}</div>',
                    //cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.ModelType}}</div>',
                    field: 'bengkel',
                    width: '7.5%',
                    cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                        if (grid.getCellValue(row,col) === 'GR') {
                          return 'blue2';
                        } else {
                            return 'red2';
                        }
                    }
                },{
                    name: 'Model',
                    //cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.VehicleModelName}} - {{row.entity.ColorName}}</div>',
                    //cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.VehicleModelName}}</div>',
                    //cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.ModelType}}</div>',
                    field: 'TypeName',
                    width: '25%'
                },
                {
                    name: 'Warna',
                    field: 'VehicleColor',
                    width: '15%'
                },
                {
                    name: 'No. Polisi',
                    field: 'PoliceNo',
                    width: '15%'
                },
                // {name: 'PSFU',field: 'PSFU',cellTemplate: cellTemplatePSFU,width: '5%'},
                // {name: 'Status',field: 'Status',cellTemplate: cellTemplateStatus,width:'5%'},
                {
                    name: 'Nama',
                    field: 'ContactPerson',
                    width: '30%'
                },
                // {
                //     name: 'Time',
                //     field: 'PushTimeIn',
                //     cellFilter: dateFilter
                // },
                {
                    name: 'Check Out',
                    field: 'Action',
                    cellTemplate: actionVisitor,
                    // width: 85,
                    // pinnedRight: true,
                    // width: '7.5%',
                    // enableSorting: false
                }
            ]
        };
        $scope.gridGateInProduction = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            enableFiltering: true,
            paginationPageSizes: [250, 500, 1000],
            // pageSize: 10,
            // widthForHorizontalScrolling: Math.floor(Math.random() * (85 - 30 + 1)) + 50,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 250,
            onRegisterApi: function(gridApi) {
                $scope.gridApiGateInProduction = gridApi;
            },
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                {
                    name: 'Bengkel',
                    field: 'Bengkel',
                    width: '7.5%',
                    cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                        if (grid.getCellValue(row,col) === 'GR') {
                          return 'blue2';
                        } else {
                            return 'red2';
                        }
                    }
                },
                {
                    name: 'Model',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.VehicleModelName}} - {{row.entity.ColorName}}</div>',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.VehicleModelName}}</div>',
                    width: '25%'
                },
                {
                    name: 'No. Polisi',
                    field: 'LicensePlate',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.LicensePlate}} <span class="{{row.entity.Check}} {{row.entity.rwjln}}"></span> <span ng-show="({{row.entity.StatusTestDrive}} == 1)"><img src="images/iconSteer/steer.png" style="width:12px;height:12px;"></span>  </div>',
                    width: '25%'
                },
                // {
                //     name: 'PSFU',
                //     field: 'PSFU',
                //     displayName: 'PSFU',
                //     cellTemplate: cellTemplatePSFU,
                //     width: '10%'
                // },
                // {
                //     name: 'Status',
                //     field: 'Status',
                //     cellTemplate: cellTemplateStatus,
                //     width: '10%'
                // },
                {
                    name: 'Nama',
                    field: 'Name',
                    width: '35%'
                },
                {
                    name: 'Check In',
                    field: 'Action',
                    cellTemplate: actionCheckInProduction,
                    width: 85,
                    pinnedRight: true,
                    // enableSorting: false
                }
            ]
        };

        $scope.gridGateOutProduction = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            enableFiltering: true,
            paginationPageSizes: [250, 500, 1000],
            // pageSize: 10,
            // widthForHorizontalScrolling: Math.floor(Math.random() * (85 - 30 + 1)) + 50,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 250,
            onRegisterApi: function(gridApi) {
                $scope.gridApiGateOutProduction = gridApi;
            },
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                {
                    name: 'Bengkel',
                    field: 'Bengkel',
                    width: '7.5%',
                    cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                        if (grid.getCellValue(row,col) === 'GR') {
                          return 'blue2';
                        } else {
                            return 'red2';
                        }
                    }
                },
                {
                    name: 'Model',
                    // <div class="col-md-1"><i class="fa fa-compass" aria-hidden="true"></i></div><div class="col-md-1"><i class="fa fa-rss" aria-hidden="true"></i></div><div class="col-md-1"><i class="fa fa-plus" aria-hidden="true"></i></div><div class="col-md-1"><i class="fa fa-frown-o" aria-hidden="true"></i></div>
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.VehicleModelName}} - {{row.entity.ColorName}}</div>',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.VehicleModelName}}</div>',
                    width: '35%'
                },
                {
                    name: 'Warna',
                    field: 'ColorName',
                    width: '15%'
                },
                {
                    name: 'No. Polisi',
                    field: 'LicensePlate',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.LicensePlate}} <span class={{row.entity.Check}}></span><span></span><span ng-show="({{row.entity.StatusTestDrive}} == 1)"><img src="images/iconSteer/steer.png" style="width:12px;height:12px;"></span> </div>',
                    width: '15%'
                },
                // {
                //     name: 'PSFU',
                //     field: 'PSFU',
                //     cellTemplate: cellTemplatePSFU,
                //     width: '5%'
                // },
                // {
                //     name: 'Status',
                //     field: 'Status',
                //     cellTemplate: cellTemplateStatus,
                //     width: '5%'
                // },
                {
                    name: 'Nama',
                    field: 'Name',
                    width: '35%'
                },
                {
                    name: 'Check Out',
                    field: 'Action',
                    cellTemplate: actionGetOutCar,
                    width: 85,
                    pinnedRight: true,
                    // enableSorting: false
                }
            ]
        };

        $scope.gridGateOutFinishService = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            enableFiltering: true,
            paginationPageSizes: [250, 500, 1000],
            // pageSize: 10,
            // widthForHorizontalScrolling: Math.floor(Math.random() * (85 - 30 + 1)) + 50,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 250,
            onRegisterApi: function(gridApi) {
                $scope.gridApiGateOutFinishService = gridApi;
            },
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                {
                    name: 'Bengkel',
                    field: 'Bengkel',
                    width: '7.5%',
                    cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                        if (grid.getCellValue(row,col) === 'GR') {
                          return 'blue2';
                        } else {
                            return 'red2';
                        }
                    }
                },
                {
                    name: 'Model',
                    // <div class="col-md-1"><i class="fa fa-compass" aria-hidden="true"></i></div><div class="col-md-1"><i class="fa fa-rss" aria-hidden="true"></i></div><div class="col-md-1"><i class="fa fa-plus" aria-hidden="true"></i></div><div class="col-md-1"><i class="fa fa-frown-o" aria-hidden="true"></i></div>
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.VehicleModelName}} - {{row.entity.ColorName}}</div>',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.VehicleModelName}}</div>',
                    width: '35%'
                },
                {
                    name: 'Warna',
                    field: 'ColorName',
                    width: '15%'
                },
                {
                    name: 'No. Polisi',
                    field: 'LicensePlate',
                    cellTemplate: '<div class="ui-grid-cell-contents " > <div class="col-md-9">{{row.entity.LicensePlate}}</div><div class="col-md-1" ng-show="row.entity.sentVehicleFrom == 5 && row.entity.StatusJob != 17"><i class="fa fa-rss" aria-hidden="true"></i></div></div>',
                    width: '15%'
                },
                // {
                //     name: 'PSFU',
                //     field: 'PSFU',
                //     cellTemplate: cellTemplatePSFU,
                //     width: '5%'
                // },
                // {
                //     name: 'Status',
                //     field: 'Status',
                //     cellTemplate: cellTemplateStatus,
                //     width: '5%'
                // },
                {
                    name: 'Nama',
                    field: 'Name',
                    width: '35%'
                },
                {
                    name: 'Check Out',
                    field: 'Action',
                    cellTemplate: actionGetOutCar,
                    width: 85,
                    pinnedRight: true,
                    // enableSorting: false
                }
            ]
        };
        $scope.gridOutPatient = {
            enableSorting: true,
            enableRowSelection: false,
            multiSelect: false,
            enableSelectAll: false,
            enableFiltering: true,
            paginationPageSizes: [250, 500, 1000],
            // pageSize: 10,
            // widthForHorizontalScrolling: Math.floor(Math.random() * (85 - 30 + 1)) + 50,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            paginationPageSize: 250,
            onRegisterApi: function(gridApi) {
                $scope.gridApiOutPatient = gridApi;
            },
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            columnDefs: [
                {
                    name: 'Bengkel',
                    field: 'Bengkel',
                    width: '7.5%',
                    cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                        if (grid.getCellValue(row,col) === 'GR') {
                          return 'blue2';
                        } else {
                            return 'red2';
                        }
                    }
                },
                {
                    name: 'Model',
                    // cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.ModelType}} - {{row.entity.ColorName}}</div>',
                    cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.ModelType}}</div>',
                    width: '35%'
                },
                {
                    name: 'Warna',
                    field: 'ColorName',
                    width: '15%'
                },
                {
                    name: 'No. Polisi',
                    field: 'PoliceNumber',
                    cellTemplate: '<div class="ui-grid-cell-contents"><div class = "col-md-9">{{row.entity.PoliceNumber}}</div><div class="col-md-1" style="color:red;"><i class="fa fa-plus" aria-hidden="true"></i></div></div>',
                    width: '15%'
                },
                // {
                //     name: 'PSFU',
                //     field: 'PSFU',
                //     cellTemplate: cellTemplatePSFU,
                //     width: '5%'
                // },
                // {
                //     name: 'Status',
                //     field: 'xStatus',
                //     cellTemplate: cellTemplateStatus,
                //     width: '5%'
                // },
                {
                    name: 'Nama',
                    field: 'ContactPerson',
                    width: '35%'
                },
                {
                    name: 'Check Out',
                    field: 'Action',
                    cellTemplate: actionGetOutCarRawatJalan,
                    width: 85,
                    pinnedRight: true,
                    // enableSorting: false
                }
            ]
        };

        $scope.columnDefs = [{
                name: 'model',
                // cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.VehicleModelName}} - {{row.entity.ColorName}}</div>',
                cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.VehicleModelName}}</div>',
                width: '35%'
            },
            {
                name: 'policeNumber',
                field: 'LicensePlate',
                width: '10%'
            },
            {
                name: 'PSFU',
                cellTemplate: cellTemplatePSFU,
                width: '5%'
            },
            {
                name: 'status',
                cellTemplate: cellTemplateStatus,
                width: '5%'
            },
            {
                name: 'Nama',
                field: 'Name',
                width: '25%'
            },
            {
                name: 'time',
                field: 'AppointmentTime',
                width: '10%'
            },
            {
                name: 'Check In',
                width: '10%'
            }
        ];

        $scope.columnDefs2 = [{
                name: 'model',
                // cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.model}} - {{row.entity.color}}</div>',
                cellTemplate: '<div class="ui-grid-cell-contents">{{row.entity.model}}</div>',
                width: '45%'
            },
            {
                name: 'policeNumber',
                width: '10%'
            },
            {
                name: 'status',
                cellTemplate: cellTemplateStatus,
                width: '5%'
            },
            {
                name: 'Nama',
                field: 'Name',
                width: '30%'
            },
            {
                name: 'Check In',
                width: '10%'
            }
        ];

        $scope.GoodRecived = function() {
            console.log("ini codenya setelah diclick", CodeScan);

            $scope.ProsesSimpan = true;
            // Gate.goodReceived(CodeScan, 2)
            Gate.goodReceived(CodeScan, 1)
                .then(
                    function(res) {
                        bsNotify.show({
                            size: 'big',
                            type: 'success',
                            title: "Sukses Good Received",
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                        console.log("suksess GoodRecived");
                        $scope.ProsesSimpan = false;
                        $scope.gateScanBack();
                    },
                    function(err) {
                        console.log("err=>", err);
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: err.data.Message,
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                        // console.log("vin tidak terdaftar");
                        $scope.ProsesSimpan = false;
                    }
                );
            console.log("ini good recived");
        }


        $scope.RepairIn = function() {
            console.log("ini codenya setelah diclick", CodeScan);
            $scope.ProsesSimpan = true;
            Gate.goodReceived(CodeScan, 5)
                .then(

                    function(res) {
                        console.log("suksess RepairIn");
                        bsNotify.show({
                            size: 'big',
                            type: 'success',
                            title: "Sukses Repair In",
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                        $scope.ProsesSimpan = false;
                        $scope.gateScanBack();
                    },
                    function(err) {
                        console.log("err=>", err);
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: err.data.Message,
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                        $scope.ProsesSimpan = false;
                    }
                );
            console.log("ini RepairIn");
        }


        $scope.DeliveryOut = function() {
            console.log("ini codenya setelah diclick", CodeScan);
            Gate.goodReceived(CodeScan, 11)
                .then(

                    function(res) {
                        console.log("suksess DeliveryOut");
                        bsNotify.show({
                            size: 'big',
                            type: 'success',
                            title: "Sukses Delivery Out",
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                    },
                    function(err) {
                        console.log("err=>", err);
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: err.data.Message,
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                    }
                );
            console.log("ini DeliveryOut");
        }

        $scope.RepairOut = function() {
            console.log("ini codenya setelah diclick", CodeScan);
            Gate.goodReceived(CodeScan, 4)
                .then(

                    function(res) {
                        console.log("suksess RepairOut");
                        bsNotify.show({
                            size: 'big',
                            type: 'success',
                            title: "Sukses Repair Out",
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                    },
                    function(err) {
                        console.log("err=>", err);
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: err.data.Message,
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                    }
                );
            console.log("ini Repair Out");
        }


        $scope.ServiceGrBtn = function() {
            console.log("ini codenya setelah diclick", CodeScan);
            console.log("suksess RepairIn");
            $scope.ProsesSimpan = true;
            // Gate.postVinAfterScan(CodeScan, 1).then(function(res) {
            Gate.postVinAfterScan($scope.barcode, 1).then(function(res) {

                if (res.data == 666){
                    bsAlert.alert(  {
                        title: "Kendaraan dengan VIN " + $scope.barcode + " sedang proses service.",
                        text: "",
                        type: "info",
                        showCancelButton: false
                    },
                    function() {
                        $scope.gateScanBack();
                        $scope.ProsesSimpan = false;
                    },
                    function() {

                    });
                } else {
                    bsAlert.alert({
                        title: "VIN Berhasil disimpan.",
                        text: "",
                        type: "info",
                        showCancelButton: false
                    },
                    function() {
                        // $scope.checkInCar(type, row);
                        $scope.gateScanBack();
                        $scope.ProsesSimpan = false;
                    },
                    function() {

                    });
                    console.log("suksess post after scan");
                }
                    
                },
                function(err) {
                    bsNotify.show({
                        size: 'big',
                        type: 'danger',
                        title: err.data.Message,
                        // content: error.join('<br>'),
                        // number: error.length
                    });
                    $scope.ProsesSimpan = false;
                }
            );

            console.log("ini Service Gr");
        }

        $scope.ServiceBpBtn = function() {
                console.log("ini codenya setelah diclick", CodeScan);
                console.log("suksess RepairIn");
                $scope.ProsesSimpan = true;
                // Gate.postVinAfterScan(CodeScan, 0).then(function(res) {
                Gate.postVinAfterScan($scope.barcode, 0).then(function(res) {
                    if (res.data == 666){
                        bsAlert.alert({
                            title: "Kendaraan dengan VIN " + $scope.barcode + " sedang proses service.",
                            text: "",
                            type: "info",
                            showCancelButton: false
                        },
                        function() {
                            $scope.gateScanBack();
                            $scope.ProsesSimpan = false;
                        },
                        function() {
    
                        });
                    } else {
                        bsAlert.alert({
                            title: "VIN Berhasil disimpan.",
                            text: "",
                            type: "info",
                            showCancelButton: false
                        },
                        function() {
                            $scope.gateScanBack();
                            $scope.ProsesSimpan = false;
                            // $scope.checkInCar(type, row);
                        },
                        function() {

                        });
                        console.log("suksess post after scan");
                    }
                        
                    },
                    function(err) {
                        bsNotify.show({
                            size: 'big',
                            type: 'danger',
                            title: err.data.Message,
                            // content: error.join('<br>'),
                            // number: error.length
                        });
                        $scope.ProsesSimpan = false;
                    }
                );

                console.log("ini Service Bp");
            }
            // -----GRID----------------
            // $scope.getTableHeight = function() {
            //     // var rowHeight = 30; // your row height
            //     // var headerHeight = 50; // your header height
            //     // var filterHeight = 40; // your filter height
            //     // var pageSizegridGateIn = $scope.gridGateIn.paginationPageSize;
            //     // var pageSizegridGateInProduction = $scope.gridGateInProduction.paginationPageSize;
            //     // var pageSizegridGateOutFinishService = $scope.gridGateOutFinishService.paginationPageSize;
            //     // var pageSizegridGateOutProduction = $scope.gridGateOutProduction.paginationPageSize;
            //     // var pageSizegridOutPatient = $scope.gridOutPatient.paginationPageSize;
            //     // var pageSizegridGateOutPassingBy = $scope.gridGateOutPassingBy.paginationPageSize;

        //     // if ($scope.gridGateIn.paginationPageSize > $scope.gridGateIn.data.length) {
        //     //     pageSizegridGateIn = $scope.gridGateIn.data.length;
        //     // }
        //     // if ($scope.gridOutPatient.paginationPageSize > $scope.gridOutPatient.data.length) {
        //     //     pageSizegridOutPatient = $scope.gridOutPatient.data.length;
        //     // }
        //     // if ($scope.gridGateInProduction.paginationPageSize > $scope.gridGateInProduction.data.length) {
        //     //     pageSizegridGateInProduction = $scope.gridGateInProduction.data.length;
        //     // }
        //     // if ($scope.gridGateOutFinishService.paginationPageSize > $scope.gridGateOutFinishService.data.length) {
        //     //     pageSizegridGateOutFinishService = $scope.gridGateOutFinishService.data.length;
        //     // }
        //     // if ($scope.gridGateOutProduction.paginationPageSize > $scope.gridGateOutProduction.data.length) {
        //     //     pageSizegridGateOutProduction = $scope.gridGateOutProduction.data.length;
        //     // }
        //     // if ($scope.gridGateOutPassingBy.paginationPageSize > $scope.gridGateOutPassingBy.data.length) {
        //     //     pageSizegridGateOutPassingBy = $scope.gridGateOutPassingBy.data.length;
        //     // }
        //     // if (pageSizegridGateIn < 4) {
        //     //     pageSizegridGateIn = 3;
        //     // }

        //     // return {
        //     //     height: (pageSize * rowHeight + headerHeight) + 50 + "px"
        //     // };
        // };
        $scope.getTableHeightgridGateIn = function() {
            var rowHeight = 30; // your row height
            var headerHeight = 50; // your header height
            var filterHeight = 40; // your filter height
            var pageSizegridGateIn = $scope.gridGateIn.paginationPageSize;

            if ($scope.gridGateIn.paginationPageSize > $scope.gridGateIn.data.length) {
                pageSizegridGateIn = $scope.gridGateIn.data.length;
            }
            if (pageSizegridGateIn < 4) {
                pageSizegridGateIn = 3;
            }

            return {
                height: (pageSizegridGateIn * rowHeight + headerHeight) + 50 + "px"
            };

        };
        $scope.getTableHeightgridGateInProduction = function() {
            var rowHeight = 30; // your row height
            var headerHeight = 50; // your header height
            var filterHeight = 40; // your filter height
            var pageSizegridGateInProduction = $scope.gridGateInProduction.paginationPageSize;

            if ($scope.gridGateInProduction.paginationPageSize > $scope.gridGateInProduction.data.length) {
                pageSizegridGateInProduction = $scope.gridGateInProduction.data.length;
            }
            if (pageSizegridGateInProduction < 4) {
                pageSizegridGateInProduction = 3;
            }

            return {
                height: (pageSizegridGateInProduction * rowHeight + headerHeight) + 50 + "px"
            };

        };
        $scope.getTableHeightgridGateOutFinishService = function() {
            var rowHeight = 30; // your row height
            var headerHeight = 50; // your header height
            var filterHeight = 40; // your filter height
            var pageSizegridGateOutFinishService = $scope.gridGateOutFinishService.paginationPageSize;

            if ($scope.gridGateOutFinishService.paginationPageSize > $scope.gridGateOutFinishService.data.length) {
                pageSizegridGateOutFinishService = $scope.gridGateOutFinishService.data.length;
            }
            if (pageSizegridGateOutFinishService < 4) {
                pageSizegridGateOutFinishService = 3;
            }

            return {
                height: (pageSizegridGateOutFinishService * rowHeight + headerHeight) + 50 + "px"
            };

        };
        $scope.getTableHeightgridGateOutProduction = function() {
            var rowHeight = 30; // your row height
            var headerHeight = 50; // your header height
            var filterHeight = 40; // your filter height
            var pageSizegridGateOutProduction = $scope.gridGateOutProduction.paginationPageSize;

            if ($scope.gridGateOutProduction.paginationPageSize > $scope.gridGateOutProduction.data.length) {
                pageSizegridGateOutProduction = $scope.gridGateOutProduction.data.length;
            }
            if (pageSizegridGateOutProduction < 4) {
                pageSizegridGateOutProduction = 3;
            }

            return {
                height: (pageSizegridGateOutProduction * rowHeight + headerHeight) + 50 + "px"
            };

        };
        $scope.getTableHeightgridGateOutPassingBy = function() {
            var rowHeight = 30; // your row height
            var headerHeight = 50; // your header height
            var filterHeight = 40; // your filter height
            var pageSizegridGateOutPassingBy = $scope.gridGateOutPassingBy.paginationPageSize;

            if ($scope.gridGateOutPassingBy.paginationPageSize > $scope.gridGateOutPassingBy.data.length) {
                pageSizegridGateOutPassingBy = $scope.gridGateOutPassingBy.data.length;
            }
            if (pageSizegridGateOutPassingBy < 4) {
                pageSizegridGateOutPassingBy = 3;
            }

            return {
                height: (pageSizegridGateOutPassingBy * rowHeight + headerHeight) + 50 + "px"
            };

        };

        
        // ===============================
        // REGEX
        // ===============================
        $scope.allowPattern = function(event, type, item) {
            console.log("event", event);
            var patternRegex
            if (type == 1) {
                patternRegex = /\d/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /\d|[a-z]|[*]/i; //ALPHANUMERIC ONLY
                if (item.includes("*")) {
                    event.preventDefault();
                    return false;
                }
            } else if (type == 3) {
                patternRegex = /\d|[a-z]/i;
            }
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };

        $scope.alphanumValid = function() {
            var item = $scope.mData.LicensePlate
            if (item == undefined || item == null || item =='') {
                item = ''
            }
            var cekalnum = /^[0-9a-zA-Z]+$/;
            if (item.match(cekalnum)) {
                return true
            } else {
                return false
            }
        }
    });