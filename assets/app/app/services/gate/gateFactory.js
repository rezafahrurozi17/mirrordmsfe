angular.module('app')
    .factory('Gate', function($http, CurrentUser) {
        var currentUser = CurrentUser.user;
        // console.log(currentUser);
        return {
            getDataAppointment: function() {
                var res = $http.get('/api/as/jobs/3/AppointForGate');
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            getDataforGatein: function() {
                var res = $http.get('/api/as/Gate/AppointForGate');
                
                return res;
            },
            getOutOpl: function() {
                // var res = $http.get('/api/as/Gate/getGateOutOPL');
                var res = $http.get('/api/as/Gate/getGateOutOPLSP');
                return res;
            },
            getInOpl: function() {
                // var res = $http.get('/api/as/Gate/getGateInOPL');
                var res = $http.get('/api/as/Gate/getGateInOPLSP');
                return res;
            },
            gateOutCar: function(data) {
                console.log('rubah data=>', data);
                return $http.put('/api/as/Gates/ProductionVehicleInOut/' + data.id + '/1');
            },
            gateOutCarTestDrive: function(data) {
                console.log('rubah data=>', data);
                return $http.put('/api/as/Gates/ProductionVehicleInOut/' + data.id + '/3');
            },
            gateOutCarRawatJalan: function(data) {
                console.log('rubah data=>', data);
                return $http.put('/api/as/Gates/ProductionVehicleInOut/' + data.GateId + '/1');
            },
            gateInCar: function(data) {
                console.log('rubah data=>', data);
                return $http.put('/api/as/Gates/ProductionVehicleInOut/' + data.id + '/0');
            },
            getDataInListProduction: function(currentDate) {
                var res = $http.get('/api/as/Gates/PostFrom/1/0/' + currentDate + '/List');
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            getDataOutListProduction: function(currentDate) {
                var res = $http.get('/api/as/Gates/PostFrom/0/0/' + currentDate + '/List');
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            getDataLaluLalang: function(currentDate) {
                var res = $http.get('/api/as/Gates/OutLaluLalang/' + currentDate);
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            getDataOutFinal: function(currentDate) {
                var res = $http.get('/api/as/Gates/PostFrom/0/4/' + currentDate + '/List');
                // console.log('hasil=>',res);
                //res.data.Result = null;
                return res;
            },
            checkIn: function(type, data) {
                console.log("type", type);
                console.log("data", data);

                var yyyy = data.DateNow.getFullYear().toString();
                var mm = (data.DateNow.getMonth() + 1).toString(); // getMonth() is zero-based
                var dd = data.DateNow.getDate().toString();
                var finalDate = yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);

                var tmpJobId = null;
                var LicensePlate = data.LicensePlate.toUpperCase();
                // data.time = data.time.replace(":", "%20");
                console.log("data", data);

                var statusx = 0;
                if (data.StatusGateSatellite != null && data.StatusGateSatellite != undefined){
                    statusx = data.StatusGateSatellite
                } else {
                    statusx = data.Status
                }

                if (data.JobId != undefined) {
                    console.log("data", data);
                    tmpJobId = data.JobId;
                    return $http.post('/api/as/Gates/1/StatusGate/' + type + '/PoliceNo/' + LicensePlate, [{
                        JobIds: [tmpJobId],
                        OtherReason: "",
                        ArrivalTime: finalDate,
                        // ArrivalTime: data.DateNow,
                        time: data.time,
                        // Status: data.Status,
                        Status: statusx
                    }]);
                } else if (data.Reason != undefined) {
                    console.log("data", data);
                    return $http.post('/api/as/Gates/1/StatusGate/' + type + '/PoliceNo/' + LicensePlate, [{
                        JobIds: [],
                        OtherReason: data.Reason,
                        ArrivalTime: finalDate,
                        // ArrivalTime: data.DateNow,
                        time: data.time,
                        Status: data.Status
                    }]);
                } else {
                    console.log("data", data);
                    return $http.post('/api/as/Gates/1/StatusGate/' + type + '/PoliceNo/' + LicensePlate, [{
                        JobIds: [],
                        OtherReason: "",
                        ArrivalTime: finalDate,
                        // ArrivalTime: data.DateNow,
                        time: data.time,
                        Status: data.Status
                    }]);
                }
            },
            checkInProduction: function(data) {
                //api/as/Gates/CheckInProduction/4239
                console.log("data", data);
                var res = $http.get('/api/as/Gates/CheckInProduction/' + data.id);
                return res;
            },
            GetStatusJobByPoliceNo: function(data) {
                //api/as/Gates/CheckInProduction/4239
                console.log("cek data", data);
                var res = $http.get('/api/as/Jobs/GetStatusJobByPoliceNo/' + data.PoliceNo + '/' + data.Id + '/' + 1);
                return res;
            },
            GetStatusGateQueue: function(data) {
                //api/as/Gates/CheckInProduction/4239
                console.log("cek data", data);
                var res = $http.get('/api/as/Jobs/GetStatusGateQueue/' + data.PoliceNo + '/' + data.Id + '/');
                return res;
            },
            visitorOut: function(row) {
                return $http.put('/api/as/Gates/OutLaluLalang/' + row.Id);
            },
            create: function(data) {
                console.log(' tambah data=>', data);

                return $http.post('/api/as/Stall', [{
                    StallParentId: (data.StallParentId == null ? 0 : data.StallParentId),
                    Name: data.Name,
                    Type: 0
                }]);
            },
            update: function(data) {
                console.log('rubah data=>', data);
                return $http.put('/api/as/Stall', [{
                    StallId: data.StallId,
                    StallParentId: (data.StallParentId == null ? 0 : data.StallParentId),
                    Name: data.Name,
                    Type: 0
                }]);
            },
            delete: function(id) {
                console.log("delete id==>", id);
                return $http.delete('/api/as/Stall', { data: id, headers: { 'Content-Type': 'application/json' } });
            },

            goodReceived: function(FrameNo, StatusDRId) {
                console.log("data in factory", FrameNo, StatusDRId);
                return $http.put('/api/sales/SDRStatusDR', [{
                    FrameNo: FrameNo,
                    StatusDRId: StatusDRId
                }]);
            },
            postVinAfterScan: function(data, isGr) {
                console.log('tambah data sesudah scan=>', data);

                return $http.post('/api/as/QueuePDS', [{
                    vin: data,
                    isGR: isGr
                }]);
            },
            getRawatJalan: function() {
                var res = $http.get('/api/as/GateForRawatJalan');
                return res;
            },
            gateSateliteCenter: function(StatusCtgId) {
                var res = $http.get('/api/as/GateSateliteCenter/getByStatus/' + StatusCtgId);
                return res;
                // Lookup untuk status :
                // 1. Release BSTK,Waiting For Pickup
                // 2. Gate Out Satelite, On The Way
                // 3. Gate In Center, Waiting For Reception
                // 4. Delivery to Center
                // 5. Delivery to Satelite, Ready For Pickup
                // 6. Gate Out Center, On The Way
                // 7. Gate In satelite , Ready For Pickup

            },
            gateSateliteDeliveryToSatelite: function() {
                var res = $http.get('/api/as/GateSateliteCenter/getByStatus/6');
                return res;
            },

            gateFromSatelite: function() {
                //ini buata rosikhul
                var res = $http.get('/api/as/GateInFromSatellite');
                return res;
            },
            gateFromSatelite_GR: function() {
                //ini buata rosikhul
                // api/as/GateInFromSatellite/{Flag}
                var res = $http.get('/api/as/GateInFromSatellite/1');
                return res;
            },
            gateFromSatelite_BP: function() {
                //ini buata rosikhul
                // api/as/GateInFromSatellite/{Flag}
                var res = $http.get('/api/as/GateInFromSatellite/0');
                return res;
            },
            getOutletFunction: function() {
                //ini buata rosikhul
                var res = $http.get('/api/as/Gate/OutletFunction');
                return res;
            },
            getOutVisitor: function() {
                // var res = $http.get('/api/as/Gate/getGateOutVisitor');
                var res = $http.get('/api/as/Gate/getGateOutVisitor_SP');
                return res;
            },
            getOutVisitorNew: function() {
                // getGateOutVisitor_SP
                var res = $http.get('/api/as/Gate/getGateOutVisitorNew');
                return res;
            },
            checkAvailableByPoliceNo: function(type, data,flag, jobid) {
                var res = $http.get('/api/as/Gate/checkAvailableByPoliceNo/' + type + '/' + data.LicensePlate +'/'+flag+'/'+jobid);
                return res;
            },
            checkAvailableByPoliceNoExclusive: function(type, data,flag, jobid) {
                var res = $http.get('/api/as/Gate/checkAvailableByPoliceNoExclusive/' + type + '/' + data.LicensePlate +'/'+flag+'/'+jobid);
                return res;
            },
            checkAvailableByPoliceNoSatellite: function(type, LicensePlate,flag, jobid) {
                // api/as/Gate/checkAvailableByPoliceNoSatellite/{MasterId}/{PoliceNo}
                // 0 = ok
                // -1 = //Gak bisa gate-in lagi karena nopol masih di bengkel satellite GR
                // -2 = //Gak bisa gate-in lagi karena nopol masih di otw dari Center mau balik ke Satellite
                // -11 = /Gak bisa gate-in lagi karena nopol masih di bengkel Center BP
                // -22 =  //Gak bisa gate-in lagi karena nopol masih di otw dari Satellite mau ke Center
                var res = $http.get('/api/as/Gate/checkAvailableByPoliceNoSatellite/' + type + '/' + LicensePlate);
                return res;
            },
            getOutTestDrive: function() {
                var res = $http.get('/api/as/Gate/getGateOutTestDrive');
                return res;
            },
            getFinalInspectionTestDriveIn: function() {
                var res = $http.get('/api/as/Gate/GetFinalInspectionTestDrive/1');
                return res;
            },
            getFinalInspectionTestDriveOut: function() {
                var res = $http.get('/api/as/Gate/GetFinalInspectionTestDrive/0');
                return res;
            },
            getStatWO: function(JobId, status) {
                return $http.get('/api/as/Jobs/GetStatusJob/' + JobId + '/' + status);
            },

            CheckAppointmentDP: function(JobId) {
                return $http.get('/api/as/CheckAppointmentDP/' + JobId);
            },
            CheckGateOutRawatJalan: function(JobId, PoliceNo) {
                // api/as/CheckGateOutRawatJalan/{JobId}/{PoliceNo}
                return $http.get('/api/as/CheckGateOutRawatJalan/' + JobId + '/' + PoliceNo);
            },



        }
    });