angular.module('app')
  .factory('Calendar', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function() {
        return res=$http.get('/api/as/Calendars');
      },
      update: function(obj){
        return $http.put('/api/as/Calendars/', obj);
      },
      delete: function(arrId) {
        return $http.delete('/api/as/Calendars/', {data:arrId,headers: {'Content-Type': 'application/json'}});
      },
    }
  });