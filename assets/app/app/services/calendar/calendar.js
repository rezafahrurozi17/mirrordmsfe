angular.module('app')
    .controller('WorkingCalendarController', function($scope, $http, CurrentUser, Calendar, WorkingShift, ngDialog, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.eventSources = [];
    $scope.datastate = 'view';
    $scope.allowNew = true;
    $scope.mCalendar = {};
    $scope.shifts = [];
    $scope.activeShift = [];
    $scope.iterationData = [
    							{Id: 0, Name: "Tidak berulang"},
    							{Id: 1, Name: "Mingguan"},
    							{Id: 2, Name: "Bulanan"},
    							{Id: 3, Name: "Tahunan"},
    						];
    $scope.dayOption = [{text:"Hari Kerja",value:1},{text:"Hari Libur",value:2}];

    $scope.checkShiftActive = function(){
        $scope.activeShift = [];
        for(shift in $scope.shifts){
            $scope.activeShift.push($scope.mCalendar.ShiftByte & $scope.shifts[shift].ShiftByte);
        }
        console.log($scope.activeShift);
    }
    $scope.actRefresh = function(){
        Calendar.getData().then(
            function(res){
                var holidays = [];
                var lastHol = '';
                for(var i=0; i<res.data.Result.length; i++){
                    holidays.push({title: res.data.Result[i].Description, start: res.data.Result[i].CalDate, ShiftByte: res.data.Result[i].ShiftByte});
                }
                $scope.eventSources.push(holidays);
            },
            function(err){
                console.log("err=>",err);
            }
        )
    }
    WorkingShift.getData().then(
        function(res){
            $scope.shifts = res.data.Result;
        },
        function(err){
            console.log("err=>",err);
        }
    );
    $scope.actSave = function(){
        $scope.mCalendar.ShiftByte = 0;
        for(shift in $scope.shifts){
            if($scope.activeShift[shift]==1)
                $scope.mCalendar.ShiftByte+=$scope.shifts[shift].ShiftByte;
        }
        Calendar.update($scope.mCalendar);
    }
    $scope.doAction = function(byte){
    	if(byte&2){
    		$scope.datastate = 'new';
    	}else if(byte&4){
            $scope.checkShiftActive();
    		$scope.datastate = 'edit';
    	}else if(byte&8){    		
    		$scope.datastate = 'delete';
    	}else{
    		$scope.datastate = 'view';
    	}
    }

    $scope.startDateOptions = {
        startingDay: 1,
        format: 'dd/MM/yyyy',
    };
    $scope.endDateOptions = {
        startingDay: 1,
        format: 'dd/MM/yyyy',
    };

    $scope.startDateChange = function(dt){
        $scope.endDateOptions.minDate = dt;
    }
    $scope.endDateChange = function(dt){

    }

    function createEvent(date, jsEvent, view) {
    	$scope.mCalendar = {CalDate: date};
        $scope.doAction(2);
    }

    function editEvent(date, jsEvent, view) {
    	$scope.mCalendar.ShiftByte = date.ShiftByte;
        $scope.mCalendar.CalDate = date.start;
        $scope.mCalendar.Description = date.title;
        $scope.doAction(4);
    }
    
    $scope.uiConfig = {
    	calendar:{
	        height: 800,
	        editable: true,
	        header:{
				left: 'title',
				center: '',
				right: 'today prev,next'
	        },
	    dayClick: createEvent,
        eventClick: editEvent,
        eventDrop: $scope.alertOnDrop,
        eventResize: $scope.alertOnResize,
        eventRender: $scope.eventRender
      }
    };

    $scope.actRefresh();
});
