angular.module('app')
    .factory('PreDiagnose', function($http, CurrentUser, $q) {
        var currentUser = CurrentUser.user;
        function fixDate(date) {
            if (date != null || date != undefined) {
                var fix = date.getFullYear() + '-' +
                    ('0' + (date.getMonth() + 1)).slice(-2) + '-' +
                    ('0' + date.getDate()).slice(-2) + 'T' +
                    ((date.getHours() < '10' ? '0' : '') + date.getHours()) + ':' +
                    ((date.getMinutes() < '10' ? '0' : '') + date.getMinutes()) + ':' +
                    ((date.getSeconds() < '10' ? '0' : '') + date.getSeconds());

                return fix;
            } else {
                return null;
            }
        };


        return {
            getData: function() {
                var res = $http.get('/api/as/GetPrediagnoses');
                // console.log('res=>',res);
                return res;
            },
            getDataDetail: function(predId) {
                console.log("predid factory", predId);
                var res = $http.get('/api/as/GetPrediagnoses/'+predId);
                console.log('resdetail=>', res);
                return res;
            },

            getDataByDate: function(startDate,finishDate) {
                // console.log("predid factory", predId);
                var res = $http.get('/api/as/GetPrediagnoses/'+startDate+'/'+finishDate);
                console.log('resdetail=>', res);
                return res;
            },
            create: function() {
                var defer = $q.defer();
                defer.resolve();
                return defer.promise;
            },
            update: function(mData) {
                console.log('mdata',mData);
                var xStatus = 0;
                console.log("mData.CustomerConfirmation",mData.CustomerConfirmation);
                if(mData.CustomerConfirmation == 1){
                    xStatus = 2;
                }else{
                    xStatus = 1;
                }
                console.log("xStatus",xStatus);
                var tmpItemDate = angular.copy(mData.FirstIndication);
                console.log("changeFormatDate item",tmpItemDate);
                tmpItemDate = new Date(tmpItemDate);
                console.log("tmpItemDate",tmpItemDate);
                var finalDate = '';
                var yyyy = tmpItemDate.getFullYear().toString();
                var mm = (tmpItemDate.getMonth() + 1).toString(); // getMonth() is zero-based         
                var dd = (tmpItemDate.getDate()).toString();
                
                // finalDate = yyyy + '/' + (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]);
                finalDate = yyyy + '-' + mm + '-' + dd;
                console.log("finalDate",finalDate)
                console.log("tahu >>", mData);

                mData.ScheduledTime = fixDate(mData.ScheduledTime);
                return $http.put('/api/as/PutPrediagnoses', [{
                    PrediagnoseId: mData.PrediagnoseId,
                    JobId: mData.JobId,
                    FirstIndication: finalDate,
                    FrequencyIncident: mData.FrequencyIncident,
                    Mil: mData.Mil,
                    ConditionIndication: mData.ConditionIndication,
                    MachineCondition: mData.MachineCondition,
                    MachineConditionTxt: mData.MachineConditionTxt,
                    PositionShiftLever: mData.PositionShiftLever,
                    PositionShiftLeverTxt: mData.PositionShiftLeverTxt,
                    VehicleSpeed: mData.VehicleSpeed,
                    RPM: mData.RPM,
                    ScheduledTime: mData.ScheduledTime,
                    VehicleLoads: mData.VehicleLoads,
                    PassengerNumber: mData.PassengerNumber,
                    ClassificationProblemR: mData.ClassificationProblemR,
                    ClassificationProblemRTxt: mData.ClassificationProblemRTxt,
                    Traffic: mData.Traffic,
                    TrafficTxt: mData.TrafficTxt,
                    WeatherConditions: mData.WeatherConditions,
                    WeatherConditionsTxt: mData.WeatherConditionsTxt,
                    CustomerFinalConfirmation: mData.CustomerFinalConfirmation,
                    ClassificationProblemM: mData.ClassificationProblemM,
                    ClassificationProblemMTxt: mData.ClassificationProblemMTxt,
                    BlowerSpeed: mData.BlowerSpeed,
                    TempSetting: mData.TempSetting,
                    SittingPosition: mData.SittingPosition,
                    TechnicalSupport: mData.TechnicalSupport,
                    PreDiagnoseTxt: mData.PreDiagnoseTxt,
                    JobDetail: mData.JobDetail,
                    notes : mData.notes,
                    FinalConfirmation: mData.FinalConfirmation,
                    CustomerConfirmation: mData.CustomerConfirmation,
                    LastModifiedUserId: mData.LastModifiedUserId,
                    LastModifiedDate: mData.LastModifiedDate,
                    // ComplaintCatId : mData.ComplaintCatId,
                    // DetailJSON : mData.DetailJSON,
                    OutletId: mData.OutletId,
                    Status: xStatus,
                    IsNeedTest : mData.IsNeedTest,
                    DataUploadUrl: {
                        strBase64: mData.Signature
                    },
                    DataUploadFoto: {
                        strBase64: mData.UploadPhoto
                    },
                    DataUploadFoto2: {
                        strBase64: mData.UploadPhoto2
                    },
                    DataUploadFoto3: {
                        strBase64: mData.UploadPhoto3
                    },
                    DataUploadFoto4: {
                        strBase64: mData.UploadPhoto4
                    }
                }]);
                // }
            },
            TestDrive: function(JobId) {
                return $http.post('/api/as/Gates/PostFrom/1/' + JobId + '');
            },
            BatalPrediagnose: function(Data,Status){
                return $http.put('/api/as/Prediagnose/updateStatus', [{
                        "PrediagnoseId":Data.PrediagnoseId,
                        "Status":Status
                }]);
            },
            CheckPrediagnoseGate: function (JobId, PoliceNo) {
                return $http.get('/api/as/CheckPrediagnoseGate/' + JobId + '/' + PoliceNo);
            },
            CheckKendaraanGateInOut: function(JobId,PoliceNo,flag) {
                //Flag 1 : Cek Dari OPL
                //Flag 2 : Cek Dari Final Inspection Test Drive
                //Flag 3 : Cek Dari Production Rawat Jalan
                //Flag 4 : Cek Dari Prediagnose Test Drive
                // belum gatein 666#TestDrive 666#OPL 666#RawatJalan 666#Prediagnose
                // belum gateout 999#TestDrive999#OPL 999#RawatJalan 999#Prediagnose
                var res = $http.get('/api/as/CheckKendaraanGateInOut/' + JobId + '/' + PoliceNo + '/' + flag);
                return res;
            },
        }
    }); 