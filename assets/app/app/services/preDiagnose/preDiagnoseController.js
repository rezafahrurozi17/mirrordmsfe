angular.module('app')
    .controller('PreDiagnoseController', function($scope, $http, CurrentUser, PreDiagnose, PreDiagnoseFirstCheck, PreDiagnoseDTC, $timeout, bsNotify, bsAlert, RepairProcessGR, PrintRpt) {
        //----------------------------------
        // Start-Up
        //----------------------------------
        $scope.boundingBox = {
            width: 600,
            height: 300
        };
        $scope.$on('$viewContentLoaded', function() {
            $scope.loading = false;
            $scope.gridData = [];
        });
        //----------------------------------
        // Initialization
        //----------------------------------
        $scope.user = CurrentUser.user();
        $scope.mData = {}; //Model
        $scope.mData.Signature = null;
        // $scope.mData.IsNeedTest = 1;

        $scope.formApi = {};
        $scope.lmModel = {};
        $scope.lmModelDTC = {};
        $scope.xRole = { selected: [] };
        $scope.Filter = {};
        $scope.dataFilterTanggal = [{
                FilterId: 1,
                FilterName: '90 hari terakhir'
            },
            {
                FilterId: 2,
                FilterName: 'Filter Tanggal'
            }
        ];

        var dateFormat = 'dd/MM/yyyy';
        var dateFormatX = 'date:\'dd-MM-yyyy\'';
        $scope.DateOptions = {
            startingDay: 1,
            format: dateFormat,
        };

        var todayS = new Date();
        $scope.maxDateS = new Date(todayS.getFullYear(), todayS.getMonth(), todayS.getDate());


        $scope.uploadFiles = [];
        $scope.mData.UploadPhoto = [];

        $scope.uploadFiles2 = [];
        $scope.mData.UploadPhoto2 = [];

        $scope.uploadFiles3 = [];
        $scope.mData.UploadPhoto3 = [];

        $scope.uploadFiles4 = [];
        $scope.mData.UploadPhoto4 = [];

        $scope.PrediagnoseI = {};
        $scope.listApi = {};
        $scope.listApiDTC = {};
        $scope.PrediagnoseId = {};
        $scope.disableReset = false;
        $scope.signature = { dataUrl: null };
        var EMPTY_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjgAAADcCAQAAADXNhPAAAACIklEQVR42u3UIQEAAAzDsM+/6UsYG0okFDQHMBIJAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcCQADAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDkQAwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAegeayZAN3dLgwnAAAAAElFTkSuQmCC';
        $scope.open = function(data) {
            bsAlert.alert({
                    title: "Apakah anda yakin signature telah benar ?",
                    text: "Action tidak dapat di ubah setelah Sign",
                    type: "warning",
                    showCancelButton: true
                },
                function() {
                    // $scope.cancelAppointment(item);
                    $scope.mData.Signature = data.dataUrl;
                    console.log('AAAA', $scope.mData);
                    console.log('$scope.disableReset A', $scope.disableReset);
                    $scope.disableReset = true;
                },
                function() {
                    $scope.signature.dataUrl = EMPTY_IMAGE;
                    $scope.disableReset = false;
                    console.log('BBBB');
                    console.log('$scope.disableReset B', $scope.disableReset);
                    $scope.clear();
                }
            );
            console.log("sign", $scope.signature.dataUrl);
            console.log("data", data);
        };

        $scope.CustomKembali = false;
        $scope.showFoto1 = false;
        $scope.showFoto2 = false;
        $scope.showFoto3 = false;
        $scope.showFoto4 = false;
        $scope.countFoto = 0;

        $scope.KembaliView = function() {
            $scope.getData();
            $scope.CustomKembali = false;
            $scope.formApi.setMode('grid');
        };

        $scope.addFoto = function() {
                $scope.countFoto++;
                console.log("coount foto", $scope.countFoto);
                // if($scope.countFoto >= 1 && $scope.countFoto < 4){
                //     $scope.showFoto1 = true;
                // }else if($scope.countFoto == 2 && $scope.countFoto < 4){
                //     $scope.showFoto2 = true;
                // }else if($scope.countFoto == 3 && $scope.countFoto < 4){
                //     $scope.showFoto3 = true;
                // }else if($scope.countFoto == 4 && $scope.countFoto < 5){
                //     $scope.showFoto4 = true;
                // }else{
                $scope.showFoto1 = true;
                $scope.showFoto2 = true;
                $scope.showFoto3 = true;
                $scope.showFoto4 = true;
                // }
                // console.log("data foto upload", $scope.uploadFiles);
                // for(var i = 0; i < $scope.uploadFiles.length; i++){
                //     $scope.mData.UploadPhoto = $scope.uploadFiles[i].strBase64;
                // }
                //     console.log("ini string basenya bro",$scope.mData.UploadPhoto);
            }
            //----------------------------------
            // Get Data
            //----------------------------------
            //--------------data model-------------------------
        $scope.forceGetData = function() {
            console.log("Success ForceGetData");
        };
        // $scope.onBeforeNewPredCheck = function(){
        //     console.log("test beforenya");
        // }

        // $scope.test = function(){
        //     console.log("test on before save");
        // }
        $scope.showXkeluhan = false;
        $scope.disTestDrive = false;
        var DataCetakan;
        $scope.onShowDetail = function(data, mode) {
            console.log("mode", mode);
            $scope.signature.dataUrl = EMPTY_IMAGE;
            console.log("data PreDiagnose", data);
            console.log("ini", $scope.mData.fromAppointment);

            $scope.jobidTerpilih = null
            $scope.PoliceNumberTerpilih = null
            $scope.statusTerpilih = null
            $scope.jobidTerpilih = data.JobId
            $scope.PoliceNumberTerpilih = data.PoliceNumber
            $scope.statusTerpilih = data.status

            $timeout(function() {
                angular.element('#signature-clear').triggerHandler('click');
            });
            RepairProcessGR.getDataDetail(data.JobId)
                .then(
                    function(res) {
                        console.log("data Job By Jobid", res.data.Result);

                        DataCetakan = res.data.Result;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            // if(data.){

            // }
            // $scope.mData.fromAppointment= 0;
            // if(typeof $scope.mData.fromAppointment == 0 || typeof $scope.mData.fromAppointment == undefined){
            if (data.fromAppointment != 1) {
                PreDiagnose.getDataDetail(data.PrediagnoseId)
                    .then(
                        function(res) {
                            // console.log("lm model",res);
                            // $scope.lmModel.PrediagnoseId.addDetail(res.data.PrediagnoseId;
                            $scope.showXkeluhan = true;

                            $scope.disTestDrive = false;
                            // $scope.noShowKeluhan = true;

                            $scope.PrediagnoseId = res.data.PrediagnoseId;
                            // console.log("$scope.PrediagnoseId",$scope.PrediagnoseId);
                            // $scope.tes($scope.PrediagnoseId,$scope.lmModel);

                            console.log("pred id", $scope.PrediagnoseId);
                            $scope.getDataDTC();
                            $scope.getDataFirstCheck();
                            $scope.mData = res.data;
                            console.log("resDetail", res);

                            //untuk bagian Posisi Shift Lever
                            var co = ["PosisiShiftP", "PosisiShiftN", "PosisiShiftD", "PosisiShiftS", "PosisiShiftR", "PosisiShiftOne", "PosisiShiftTwo", "PosisiShiftThree", "PosisiShiftFour", "PosisiShiftLainnya"];

                            var total = $scope.mData.PositionShiftLever;
                            var arrBin = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512];
                            for (var i in arrBin) {
                                if (total & arrBin[i]) {
                                    var xc = co[i];
                                    $scope.mData[xc] = arrBin[i];


                                    // console.log(co[i]+' : nyala');
                                }
                            }
                            //akhir bagian posisi shift lever

                            //untuk bagian posisi Problem R
                            var CheckBxProblemR = ["KlasifikasiMasalahDKota", "KlasifikasiMasalahLKota", "KlasifikasiMasalahTol", "KlasifikasiMasalahBelok", "KlasifikasiMasalahLurus", "KlasifikasiMasalahDatar", "KlasifikasiMasalahTanjakan", "KlasifikasiMasalahTurunan", "KlasifikasiMasalahAkselerasi", "KlasifikasiMasalahPengereman", "KlasifikasiMasalahLainnya"];

                            var TotalProblemR = $scope.mData.ClassificationProblemR;
                            var arrBinProblemR = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024];
                            for (var a in arrBinProblemR) {
                                if (TotalProblemR & arrBinProblemR[a]) {
                                    var resProblemR = CheckBxProblemR[a];
                                    $scope.mData[resProblemR] = arrBinProblemR[a];
                                }
                            }
                            //akhir untuk bagian posisi Problem R

                            //untuk bagian Lalu lintas
                            var CheckBxTraffic = ["LaluLintasMct", "LaluLintasLancar", "LaluLintasLainnya"];

                            var TotalTraffic = $scope.mData.Traffic;
                            var arrBinTraffic = [1, 2, 4];

                            for (var Tr in arrBinTraffic) {
                                if (TotalTraffic & arrBinTraffic[Tr]) {
                                    var resTraffic = CheckBxTraffic[Tr];
                                    $scope.mData[resTraffic] = arrBinTraffic[Tr];
                                }
                            }
                            //akhir bagian Lalu lintas

                            //untuk bagian Kondisi Cuaca
                            var CheckBxWeather = ["KondisiCuacaCerah", "KondisiCuacaBerawan", "KondisiCuacaHujan", "KondisiCuacaPanas", "KondisiCuacaLembab"];

                            var TotalWeather = $scope.mData.WeatherConditions;
                            var arrBinWeather = [1, 2, 4, 8, 16];

                            for (var j in arrBinWeather) {
                                if (TotalWeather & arrBinWeather[j]) {
                                    var resWeather = CheckBxWeather[j];
                                    $scope.mData[resWeather] = arrBinWeather[j];

                                    // console.log(CheckBxWeather[j]+' : Nyala');
                                }
                            }
                            //akhir bagian kondisi cuaca

                            //klasifikasi masalah pada kendaraan
                            var CheckProblemM = ["Engine", "Suspension", "Rem", "Lainnya"];

                            var TotalProblemM = $scope.mData.ClassificationProblemM;
                            var arrProblemM = [1, 2, 4, 8];

                            for (var c in arrProblemM) {
                                if (TotalProblemM & arrProblemM[c]) {
                                    var resProblemM = CheckProblemM[c];
                                    $scope.mData[resProblemM] = arrProblemM[c];
                                }
                            }
                            //akhir klasifikasi masalah pada kendaraan

                            //untuk bagian Posisi DUduk
                            var CheckBxPosition = ["Auto", "Posisi1", "Posisi2", "Posisi3", "Posisi4", "Posisi5"];

                            var TotalPosition = $scope.mData.SittingPosition;
                            var arrBinPosition = [1, 2, 4, 8, 16, 32];

                            for (var j in arrBinPosition) {
                                if (TotalPosition & arrBinPosition[j]) {
                                    var resPosition = CheckBxPosition[j];
                                    $scope.mData[resPosition] = arrBinPosition[j];

                                    // console.log(CheckBxWeather[j]+' : Nyala');
                                }
                            }

                            if (mode == 'edit') {
                                if (data.status == 0 || data.status == null) {

                                    $scope.mData.CustomerConfirmation = 0;

                                    PreDiagnose.BatalPrediagnose(data, 1)
                                        .then(
                                            function(res) {
                                                console.log("Save");

                                                $scope.getData();
                                            },
                                            function(err) {
                                                console.log("err=>", err);
                                            }
                                        );
                                }
                            } else {
                                $scope.CustomKembali = true;
                            }
                            //akhir bagian Posisi DUduk
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
            } else {
                $scope.showXkeluhan = false;

                $scope.disTestDrive = true;
                console.log("$scope.disTestDrive", $scope.disTestDrive);
                // $scope.noShowKeluhan = true;
                console.log("sukses juga", $scope.mData);
            }
        };
        $scope.listApi = {}
        $scope.CountShift = function() {
            $scope.mData.PositionShiftLever = ($scope.mData.PosisiShiftP == undefined ? 0 : $scope.mData.PosisiShiftP) + ($scope.mData.PosisiShiftN == undefined ? 0 : $scope.mData.PosisiShiftN) + ($scope.mData.PosisiShiftD == undefined ? 0 : $scope.mData.PosisiShiftD) + ($scope.mData.PosisiShiftS == undefined ? 0 : $scope.mData.PosisiShiftS) + ($scope.mData.PosisiShiftR == undefined ? 0 : $scope.mData.PosisiShiftR) + ($scope.mData.PosisiShiftOne == undefined ? 0 : $scope.mData.PosisiShiftOne) + ($scope.mData.PosisiShiftTwo == undefined ? 0 : $scope.mData.PosisiShiftTwo) + ($scope.mData.PosisiShiftThree == undefined ? 0 : $scope.mData.PosisiShiftThree) + ($scope.mData.PosisiShiftFour == undefined ? 0 : $scope.mData.PosisiShiftFour) + ($scope.mData.PosisiShiftLainnya == undefined ? 0 : $scope.mData.PosisiShiftLainnya);
        }

        $scope.countKlasifikasiProbR = function() {
            $scope.mData.ClassificationProblemR = ($scope.mData.KlasifikasiMasalahDKota == undefined ? 0 : $scope.mData.KlasifikasiMasalahDKota) + ($scope.mData.KlasifikasiMasalahLKota == undefined ? 0 : $scope.mData.KlasifikasiMasalahLKota) + ($scope.mData.KlasifikasiMasalahTol == undefined ? 0 : $scope.mData.KlasifikasiMasalahTol) + ($scope.mData.KlasifikasiMasalahBelok == undefined ? 0 : $scope.mData.KlasifikasiMasalahBelok) + ($scope.mData.KlasifikasiMasalahLurus == undefined ? 0 : $scope.mData.KlasifikasiMasalahLurus) + ($scope.mData.KlasifikasiMasalahDatar == undefined ? 0 : $scope.mData.KlasifikasiMasalahDatar) + ($scope.mData.KlasifikasiMasalahTanjakan == undefined ? 0 : $scope.mData.KlasifikasiMasalahTanjakan) + ($scope.mData.KlasifikasiMasalahTurunan == undefined ? 0 : $scope.mData.KlasifikasiMasalahTurunan) + ($scope.mData.KlasifikasiMasalahAkselerasi == undefined ? 0 : $scope.mData.KlasifikasiMasalahAkselerasi) + ($scope.mData.KlasifikasiMasalahPengereman == undefined ? 0 : $scope.mData.KlasifikasiMasalahPengereman) + ($scope.mData.KlasifikasiMasalahLainnya == undefined ? 0 : $scope.mData.KlasifikasiMasalahLainnya);
        }

        $scope.choseTraffic = function() {
            $scope.mData.Traffic = ($scope.mData.LaluLintasMct == undefined ? 0 : $scope.mData.LaluLintasMct) + ($scope.mData.LaluLintasLancar == undefined ? 0 : $scope.mData.LaluLintasLancar) + ($scope.mData.LaluLintasLainnya == undefined ? 0 : $scope.mData.LaluLintasLainnya);
        }

        $scope.countWeather = function() {
            $scope.mData.WeatherConditions = ($scope.mData.KondisiCuacaCerah == undefined ? 0 : $scope.mData.KondisiCuacaCerah) + ($scope.mData.KondisiCuacaBerawan == undefined ? 0 : $scope.mData.KondisiCuacaBerawan) + ($scope.mData.KondisiCuacaHujan == undefined ? 0 : $scope.mData.KondisiCuacaHujan) + ($scope.mData.KondisiCuacaPanas == undefined ? 0 : $scope.mData.KondisiCuacaPanas) + ($scope.mData.KondisiCuacaLembab == undefined ? 0 : $scope.mData.KondisiCuacaLembab);
        }

        $scope.countPosisiDuduk = function() {
            $scope.mData.SittingPosition = ($scope.mData.Auto == undefined ? 0 : $scope.mData.Auto) + ($scope.mData.Posisi1 == undefined ? 0 : $scope.mData.Posisi1) + ($scope.mData.Posisi2 == undefined ? 0 : $scope.mData.Posisi2) + ($scope.mData.Posisi3 == undefined ? 0 : $scope.mData.Posisi3) + ($scope.mData.Posisi4 == undefined ? 0 : $scope.mData.Posisi4) + ($scope.mData.Posisi5 == undefined ? 0 : $scope.mData.Posisi5);
        }

        $scope.countKlasifikasiProbM = function() {
            $scope.mData.ClassificationProblemM = ($scope.mData.Engine == undefined ? 0 : $scope.mData.Engine) + ($scope.mData.Suspension == undefined ? 0 : $scope.mData.Suspension) + ($scope.mData.Rem == undefined ? 0 : $scope.mData.Rem) + ($scope.mData.Lainnya == undefined ? 0 : $scope.mData.Lainnya);
        }

        $scope.PrintPrediagnose = function(data) {
            console.log('JobId cetakWO', data);
            $scope.DataPrediagnose = 'as/PrintReceptionGrPrediagnose/' + data.JobId + '/' + $scope.user.OrgId + '/1';
            $scope.cetakan($scope.DataPrediagnose);
        }



        $scope.cetakan = function(data) {
            var pdfFile = null;

            PrintRpt.print(data).success(function(res) {
                var file = new Blob([res], { type: 'application/pdf' });
                var fileURL = URL.createObjectURL(file);

                console.log("pdf", fileURL);
                //$scope.content = $sce.trustAsResourceUrl(fileURL);
                pdfFile = fileURL;

                if (pdfFile != null) {
                    //CHROME ANDROID 68 DOWNLOAD SUPPORT Cross-Origin frame 
                    var ua = navigator.userAgent;
                    if ((/Android/i.test(ua)) && (/Chrome/i.test(ua))) {
                        var link = document.createElement('a');
                        link.href = fileURL;
                        //link.download="erlangga_file.pdf";
                        link.click();
                    }
                    else {
                        printJS(pdfFile);
                    }
                }
                else
                    console.log("error cetakan", pdfFile);
            }).error(function(res) {
                console.log("error cetakan", pdfFile);
            });
        };

        $scope.Batal = function(Data) {
            console.log("Data", Data);
            bsAlert.alert({
                    title: "Apakah anda yakin membatalkan prediagnose " + Data.PoliceNumber + " ?",
                    text: "",
                    type: "question",
                    showCancelButton: true
                },
                function() {

                    PreDiagnose.BatalPrediagnose(Data, 3)
                        .then(
                            function(res) {
                                console.log("Save");

                                $scope.getData();
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                },
                function() {

                }
            )
        };

        $scope.gridActionTemplate = '<div class="ui-grid-cell-contents"> \
        <a href="#" ng-click="grid.appScope.actView(row.entity)" uib-tooltip="View" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-list-alt fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
        <a href="#" ng-hide="row.entity.status == 3"  ng-click="grid.appScope.actEdit(row.entity)" uib-tooltip="Edit" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-pencil fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
        <a href="#" ng-show="row.entity.status == 3"  uib-tooltip="Edit" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-pencil fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
        <a href="#" ng-hide="row.entity.status == 3"  ng-click="grid.appScope.$parent.Batal(row.entity)" uib-tooltip="Batal" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-close fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
        <a href="#" ng-show="row.entity.status == 3"  uib-tooltip="Batal" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-close fa-lg" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a> \
        <a href="#" ng-hide="row.entity.status == 3"  ng-click="grid.appScope.$parent.PrintPrediagnose(row.entity)" uib-tooltip="Print Pre Diagnose" tooltip-placement="bottom" style="color:#777;"><i class="fa fa-fw fa-lg fa-print" style="padding:8px 8px 8px 0px;margin-left:8px;"></i></a>\
        </div>';

        $scope.customButtonActionSettingsDetail = [{
                actionType: 'edit',
                title: 'Edit',
                icon: 'fa fa-fw fa-edit',
                color: 'rgb(213,51,55)'
            },
            {
                func: function(row, formScope) {
                    console.log("print");
                },
                title: 'Print',
                icon: 'fa fa-fw fa-print',
                //rightsBit: 1
            },
            {
                func: function(row, formScope) {
                    console.log("Test Drive JobId", $scope.mData.JobId);
                    PreDiagnose.TestDrive($scope.mData.JobId)
                        .then(
                            function(res) {
                                console.log("Save");
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                },
                title: 'Test Drive',
                icon: 'fa fa-fw fa-car',
                //rightsBit: 1
            }
        ];

        // var res = [{
        //     PrediagnoseId : 1
        // }];
        // $scope.testDrive = function(data){
        //     console.log("data test drive", data);
        //     if(data == 1){
        //         PreDiagnose.TestDrive($scope.mData.JobId)
        //                     .then(
        //                         function(res) {
        //                             console.log("Masuk TestDrive");
        //                         },
        //                         function(err) {
        //                             console.log("err=>", err);
        //                         }
        //                     );
        //     }else{
        //         console.log("ini tidak masuk testdrive");
        //     }
        // }

        $scope.testDrive = function(selected) {
            var IsDataNeed = (selected.IsNeedTest == undefined ? 0 : selected.IsNeedTest)
            console.log("selected", IsDataNeed);
            if (IsDataNeed == 0) {

                PreDiagnose.CheckPrediagnoseGate($scope.jobidTerpilih, $scope.PoliceNumberTerpilih).then(function(res) {
                    if (res.data == 666){
                       console.log('belum gate in')
                    } else if (res.data == 999){
                        console.log('belum gate out')
                    } else {
                        PreDiagnose.TestDrive($scope.mData.JobId)
                        .then(
                            function(res) {
                                console.log("Masuk TestDrive");
                            },
                            function(err) {
                                console.log("err=>", err);
                            }
                        );
                    }
                })

                
            } else {
                console.log("ini tidak masuk testdrive");
            }
        }

        $scope.getData = function() {
            if ($scope.Filter.FilterTanggal == 1) {

                PreDiagnose.getData().then(function(res) {

                        for (var i = 0; i < res.data.Result.length; i++) {
                            if (res.data.Result[i].status == 1) {
                                res.data.Result[i].Xstatus = "On progress";
                            } else if (res.data.Result[i].status == 0 || res.data.Result[i].status == null) {
                                res.data.Result[i].Xstatus = "Open";
                            } else if (res.data.Result[i].status == 2) {
                                res.data.Result[i].Xstatus = "Completed";
                            } else if (res.data.Result[i].status == 3) {
                                res.data.Result[i].Xstatus = "Cancel";
                            } else {
                                res.data.Result[i].Xstatus = " - ";
                            }
                        }
                        $scope.grid.data = res.data.Result;
                        console.log("$scope.grid.data=>", $scope.grid.data);
                        $scope.loading = false;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
            } else if ($scope.Filter.FilterTanggal == 2) {
                console.log("ini filter ke dua");
                if ($scope.Filter.firstDate == '' || $scope.Filter.firstDate == null || $scope.Filter.firstDate == undefined || $scope.Filter.lastDate == '' || $scope.Filter.lastDate == null || $scope.Filter.lastDate == undefined) {
                    bsNotify.show({
                        title: 'Mohon Isi Filter Tanggal',
                        text: 'I will close in 2 seconds.',
                        size: 'big',
                        timeout: 2000,
                        type: 'danger'
                    });
                } else {
                    console.log("ini filter dari mas bud");

                    var d = new Date($scope.Filter.firstDate);
                    var yearLastd = d.getFullYear();
                    var monthLastd = d.getMonth() + 1;
                    var dayLastd = d.getDate();
                    var FirstAfterBill = yearLastd + '-' + monthLastd + '-' + dayLastd;

                    var g = new Date($scope.Filter.lastDate);
                    var yearLast = g.getFullYear();
                    var monthLast = g.getMonth() + 1;
                    var dayLast = g.getDate();
                    var lastDateAfterBill = yearLast + '-' + monthLast + '-' + dayLast;

                    PreDiagnose.getDataByDate(FirstAfterBill, lastDateAfterBill).then(function(res) {

                            for (var i = 0; i < res.data.Result.length; i++) {
                                if (res.data.Result[i].status == 1) {
                                    res.data.Result[i].Xstatus = "On progress";
                                } else if (res.data.Result[i].status == 0 || res.data.Result[i].status == null) {
                                    res.data.Result[i].Xstatus = "Open";
                                } else if (res.data.Result[i].status == 2) {
                                    res.data.Result[i].Xstatus = "Completed";
                                } else if (res.data.Result[i].status == 3) {
                                    res.data.Result[i].Xstatus = "Cancel";
                                } else {
                                    res.data.Result[i].Xstatus = " - ";
                                }
                            }
                            $scope.grid.data = res.data.Result;
                            console.log("$scope.grid.data=>", $scope.grid.data);
                            $scope.loading = false;
                        },
                        function(err) {
                            console.log("err=>", err);
                        }
                    );
                }
            } else {
                bsNotify.show({
                    title: 'Mohon Pilih Filter',
                    text: 'I will close in 2 seconds.',
                    size: 'big',
                    timeout: 2000,
                    type: 'danger'
                });
            }
        }

        function roleFlattenAndSetLevel(node, lvl) {
            for (var i = 0; i < node.length; i++) {
                node[i].$$treeLevel = lvl;
                gridData.push(node[i]);
                if (node[i].child.length > 0) {
                    roleFlattenAndSetLevel(node[i].child, lvl + 1)
                } else {

                }
            }
            return gridData;
        }
        $scope.selectRole = function(rows) {
            console.log("onSelectRows=>", rows);
            $timeout(function() { $scope.$broadcast('show-errors-check-validity'); });
        }
        $scope.onSelectRows = function(rows) {
            console.log("onSelectRows=>", rows);
        };
        //----------------------------------
        // Grid Setup
        //----------------------------------
        $scope.grid = {
            enableSorting: true,
            enableRowSelection: true,
            multiSelect: true,
            enableSelectAll: true,
            //showTreeExpandNoChildren: true,
            // paginationPageSizes: [15, 30, 40, 50, 60, 70, 80, 90, 100,200],
            // paginationPageSize: 15,
            // data : 'gridData',
            columnDefs: [
                { name: 'PrediagnoseId', field: 'PrediagnoseId', width: '7%', visible: false },
                { name: 'Prediagnose No.', field: 'PrediagnoseNo', visible: true },
                { name: 'No. Polisi', field: 'PoliceNumber' },
                { name: 'Model', field: 'VehicleModelName' },
                { name: 'Tanggal', field: 'ScheduledTime', cellFilter: dateFormatX },
                { name: 'Nama', field: 'Name' },
                { name: 'Status', field: 'Xstatus' }
            ]
        };


        $scope.onListSelectRows = function(rows) {
            console.log("form controller=>", rows);
        }

        // $scope.test = function(){

        //     console.log("data foto upload", $scope.uploadFiles);
        //     for(var i = 0; i < $scope.uploadFiles.length; i++){
        //         $scope.mData.UploadPhoto = $scope.uploadFiles[i].strBase64;
        //         // console.log("ini string basenya bro",$scope.mData.UploadPhoto);
        //     }

        //     console.log("data foto upload2", $scope.uploadFiles2);
        //     for(var i = 0; i < $scope.uploadFiles2.length; i++){
        //         $scope.mData.UploadPhoto2 = $scope.uploadFiles2[i].strBase64;
        //         // console.log("ini string basenya bro",$scope.mData.UploadPhoto2);
        //     }

        //     console.log("data foto upload3", $scope.uploadFiles3);
        //     for(var i = 0; i < $scope.uploadFiles3.length; i++){
        //         $scope.mData.UploadPhoto3 = $scope.uploadFiles3[i].strBase64;
        //         // console.log("ini string basenya bro",$scope.mData.UploadPhoto3);
        //     }

        //     console.log("data foto upload4", $scope.uploadFiles4);
        //     for(var i = 0; i < $scope.uploadFiles4.length; i++){
        //         $scope.mData.UploadPhoto4 = $scope.uploadFiles4[i].strBase64;
        //         // console.log("ini string basenya bro",$scope.mData.UploadPhoto4);
        //     }
        // }


        $scope.getDataFirstCheck = function() {
            // if($scope.Filter.dt !== undefined){
            // $scope.grid.data = res;
            PreDiagnoseFirstCheck.GetDataFirstCheck($scope.PrediagnoseId)
                .then(
                    function(res) {
                        $scope.DataPemeriksaanAwal = res.data.Result;
                        console.log("First Check", $scope.DataPemeriksaanAwal);
                        // $scope.loading=false;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        }

        $scope.listButtonSettingsFamily = { view: { enable: false }, new: { enable: true, icon: "fa fa-group" } };

        $scope.afterDelete = function() {
            $scope.getDataFirstCheck();
        }

        $scope.onAfterSaveGrid = function() {
            $scope.getData();
        }

        $scope.afterSave = function() {
            $scope.getDataFirstCheck();
        }

        $scope.beforeSave = function(row) {
            console.log("before save nih", $scope.PrediagnoseId);
            row.PrediagnoseId = $scope.PrediagnoseId;
            console.log("nih pred id", $scope.lmModel.PrediagnoseId);

            // $scope.mData.PrediagnoseIdd = $scope.PrediagnoseId;

            // $scope.lmModel.PrediagnoseId = $scope.mData.PrediagnoseIdd;
            // $scope.lmModel.PrediagnoseId = $scope.PrediagnoseId;
        }
        $scope.beforeNew = function() {
            console.log("beforeNew");
        };
        $scope.beforeEdit = function() {
            console.log("before Edit");
        };

        $scope.getDataDTC = function() {
            PreDiagnoseDTC.GetDataDTC($scope.PrediagnoseId)
                .then(
                    function(res) {
                        $scope.DataDTC = res.data.Result;
                        console.log("DTC", $scope.DataDTC);
                        // $scope.loading=false;
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        };

        $scope.afterDeleteDTC = function() {
            $scope.getDataDTC();
        };

        $scope.afterSaveDTC = function() {
            $scope.getDataDTC();
        };

        $scope.beforeSaveDTC = function(row) {
            console.log("before save nih", $scope.PrediagnoseId);
            row.PrediagnoseId = $scope.PrediagnoseId;
            console.log("nih pred id", $scope.lmModel.PrediagnoseId);

            // $scope.mData.PrediagnoseIdd = $scope.PrediagnoseId;

            // $scope.lmModel.PrediagnoseId = $scope.mData.PrediagnoseIdd;
            // $scope.lmModel.PrediagnoseId = $scope.PrediagnoseId;
        };
        $scope.beforeNewDTC = function() {
            console.log("beforeNew");
        };
        $scope.beforeEditDTC = function() {
            console.log("before Edit");
        };
        $scope.allowPattern = function(event, type) {
            var patternRegex
            if (type == 1) {
                patternRegex = /\d/i; //NUMERIC ONLY
            } else if (type == 2) {
                patternRegex = /\d|[a-z]/i; //ALPHANUMERIC ONLY
            }
            var keyCode = event.which || event.keyCode;
            var keyCodeChar = String.fromCharCode(keyCode);
            if (!keyCodeChar.match(new RegExp(patternRegex, "i"))) {
                event.preventDefault();
                return false;
            }
        };
        $scope.onValidateSave = function(row, mode) {
            console.log("onValidateSave >>>", row, mode);
            if (mode == 'create') {
                return row;
            } else if (mode == "update") {
                if ($scope.statusTerpilih === 1 && row.IsNeedTest === 1){
                    PreDiagnose.CheckPrediagnoseGate($scope.jobidTerpilih, $scope.PoliceNumberTerpilih).then(function(res) {
                        if (res.data == 666){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Kendaraan belum Gate In untuk proses Prediagnose",
                            });
                            return false;
                        } else if (res.data == 999){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Kendaraan belum Gate Out untuk proses Prediagnose",
                            });
                            return false;
                        } else {
                            $scope.onValidateSave2(row, mode)
                        }
                           
                    },
                    function(err) {
                        console.log("err=>", err);
                    });
                } else if (($scope.statusTerpilih === 0 || $scope.statusTerpilih === null) && row.IsNeedTest === 1) {
                    PreDiagnose.CheckPrediagnoseGate($scope.jobidTerpilih, $scope.PoliceNumberTerpilih).then(function(res) {
                        if (res.data == 666 && $scope.mData.CustomerConfirmation == 1){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Tidak bisa memilih konfirmasi pelanggan 'OK', karena belum melakukan test drive",
                            });
                            return false;
                        } else if (res.data == 999 && $scope.mData.CustomerConfirmation == 1){
                            bsNotify.show({
                                size: 'big',
                                type: 'danger',
                                title: "Tidak bisa memilih konfirmasi pelanggan 'OK', karena belum melakukan test drive",
                            });
                            return false;
                        } else {
                            PreDiagnose.CheckKendaraanGateInOut($scope.jobidTerpilih, $scope.PoliceNumberTerpilih,4).then(function (resultnya) {
                                var splitRes = resultnya.data.split('#')
                                    
                                if (splitRes[0] == 999){
                                    bsAlert.warning("Kendaraan sudah berada di list Gate Out security untuk aktivitas " + splitRes[1]);
                                    return false;
                                } else if (splitRes[0] == 666){
                                    bsAlert.warning("Kendaraan sedang berada di luar Cabang/Bengkel untuk aktivitas " + splitRes[1]);
                                    return false;
                                } else {
                                    $scope.onValidateSave2(row, mode)
                                }
                            })
                        }
                           
                    },
                    function(err) {
                        console.log("err=>", err);
                    });
                    

                } else {
                    $scope.onValidateSave2(row, mode)
                }
               

            };
        }

        $scope.onValidateSave2 = function (row, mode) {
            console.log('signature', $scope.mData.Signature);
            //ini buat tandatangan dimatiin karena kalo ttdnya diisi jadi error kata pak rio dimatiin
            // if ($scope.mData.Signature == undefined) {
            //     bsNotify.show({
            //         size: 'big',
            //         type: 'danger',
            //         title: "Mohon Sign signature terlebih dahulu",
            //     });
            // } else {
            // return row;
            // }
            //ini buat tandatangan dimatiin karena kalo ttdnya diisi jadi error kata pak rio dimatiin

            console.log("data foto upload", $scope.uploadFiles);
            for (var i = 0; i < $scope.uploadFiles.length; i++) {
                $scope.mData.UploadPhoto = $scope.uploadFiles[i].strBase64;
                // console.log("ini string basenya bro",$scope.mData.UploadPhoto);
            }

            console.log("data foto upload2", $scope.uploadFiles2);
            for (var i = 0; i < $scope.uploadFiles2.length; i++) {
                $scope.mData.UploadPhoto2 = $scope.uploadFiles2[i].strBase64;
                // console.log("ini string basenya bro",$scope.mData.UploadPhoto2);
            }

            console.log("data foto upload3", $scope.uploadFiles3);
            for (var i = 0; i < $scope.uploadFiles3.length; i++) {
                $scope.mData.UploadPhoto3 = $scope.uploadFiles3[i].strBase64;
                // console.log("ini string basenya bro",$scope.mData.UploadPhoto3);
            }

            console.log("data foto upload4", $scope.uploadFiles4);
            for (var i = 0; i < $scope.uploadFiles4.length; i++) {
                $scope.mData.UploadPhoto4 = $scope.uploadFiles4[i].strBase64;
                // console.log("ini string basenya bro",$scope.mData.UploadPhoto4);
            }

            PreDiagnose.update($scope.mData)
                .then(
                    function(res) {
                        $scope.formApi.setMode('grid');
                        $scope.getData();
                    },
                    function(err) {
                        console.log("err=>", err);
                    }
                );
        }

    });
// angular.module('signature', []);

// angular.module('signature').directive('signaturePad', ['$interval', '$timeout', '$window',
//     function($interval, $timeout, $window) {
//         'use strict';

//         var signaturePad;
//         var isEmpty;
//         var dataUrl;
//         var element;
//         var EMPTY_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAjgAAADcCAQAAADXNhPAAAACIklEQVR42u3UIQEAAAzDsM+/6UsYG0okFDQHMBIJAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcCQADAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMB8BwAMMBDAfAcADDATAcwHAAwwEwHMBwAAwHMBzAcAAMBzAcAMMBDAcwHADDAQwHwHAAwwEMB8BwAMMBMBzAcADDkQAwHMBwAAwHMBwAwwEMBzAcAMMBDAfAcADDAQwHwHAAwwEwHMBwAMMBMBzAcAAMBzAcwHAADAcwHADDAQwHMBwAwwEMB8BwAMMBMBzAcADDATAcwHAADAcwHMBwAAwHMBwAwwEMBzAcAMMBDAegeayZAN3dLgwnAAAAAElFTkSuQmCC';

//         return {
//             restrict: 'EA',
//             replace: true,
//             template: '<div class="signature" style="width: 100%; max-width:{{width}}px; height: 274px; max-height:{{height}}px;"><canvas style="display: block; margin: 0 auto;" ng-mouseup="onMouseup()" ng-mousedown="notifyDrawing({ drawing: true })"></canvas></div>',
//             scope: {
//                 accept: '=?',
//                 clear: '=?',
//                 dataurl: '=?',
//                 height: '@',
//                 width: '@',
//                 notifyDrawing: '&onDrawing',
//             },
//             controller: [
//                 '$scope',
//                 function($scope) {
//                     $scope.accept = function() {
//                         console.log("CEK SIGNATURE");
//                         alert("signed");
//                         return {
//                             isEmpty: $scope.dataurl === EMPTY_IMAGE,
//                             dataUrl: $scope.dataurl
//                         };
//                     };

//                     $scope.onMouseup = function() {
//                         $scope.updateModel();

//                         // notify that drawing has ended
//                         $scope.notifyDrawing({ drawing: false });
//                     };

//                     $scope.updateModel = function() {
//                         console.log('updateModel', $scope.signaturePad);
//                         /*
//                          defer handling mouseup event until $scope.signaturePad handles
//                          first the same event
//                          */
//                         $timeout().then(function() {
//                             $scope.dataurl = $scope.signaturePad.isEmpty() ? EMPTY_IMAGE : $scope.signaturePad.toDataURL();
//                         });
//                     };

//                     $scope.clear = function() {
//                         $scope.signaturePad.clear();
//                         $scope.dataurl = EMPTY_IMAGE;
//                     };

//                     $scope.$watch("dataurl", function(dataUrl) {
//                         if (!dataUrl || $scope.signaturePad.toDataURL() === dataUrl) {
//                             return;
//                         }

//                         $scope.setDataUrl(dataUrl);
//                     });
//                 }
//             ],
//             link: function(scope, element, attrs) {
//                 var canvas = element.find('canvas')[0];
//                 var parent = canvas.parentElement;
//                 var scale = 0;
//                 var ctx = canvas.getContext('2d');

//                 var width = parseInt(scope.width, 10);
//                 var height = parseInt(scope.height, 10);

//                 canvas.width = width;
//                 canvas.height = height;

//                 scope.signaturePad = new SignaturePad(canvas);
//                 console.log('scope.signaturePad', scope.signaturePad);

//                 scope.setDataUrl = function(dataUrl) {
//                     ctx.setTransform(1, 0, 0, 1, 0, 0);
//                     ctx.scale(1, 1);

//                     scope.signaturePad.clear();
//                     scope.signaturePad.fromDataURL(dataUrl);

//                     $timeout().then(function() {
//                         ctx.setTransform(1, 0, 0, 1, 0, 0);
//                         ctx.scale(1 / scale, 1 / scale);
//                     });
//                 };

//                 var calculateScale = function() {
//                     var scaleWidth = Math.min(parent.clientWidth / width, 1);
//                     var scaleHeight = Math.min(parent.clientHeight / height, 1);

//                     var newScale = Math.min(scaleWidth, scaleHeight);

//                     if (newScale === scale) {
//                         return;
//                     }

//                     var newWidth = width * newScale;
//                     var newHeight = height * newScale;
//                     canvas.style.height = Math.round(newHeight) + "px";
//                     canvas.style.width = Math.round(newWidth) + "px";

//                     scale = newScale;
//                     ctx.setTransform(1, 0, 0, 1, 0, 0);
//                     ctx.scale(1 / scale, 1 / scale);
//                 };

//                 var resizeIH = $interval(calculateScale, 200);
//                 scope.$on('$destroy', function() {
//                     $interval.cancel(resizeIH);
//                     resizeIH = null;
//                 });

//                 angular.element($window).bind('resize', calculateScale);
//                 scope.$on('$destroy', function() {
//                     angular.element($window).unbind('resize', calculateScale);
//                 });

//                 calculateScale();

//                 element.on('touchstart', onTouchstart);
//                 element.on('touchend', onTouchend);

//                 function onTouchstart() {
//                     console.log('touchStart');
//                     scope.$apply(function() {
//                         // notify that drawing has started
//                         scope.notifyDrawing({ drawing: true });
//                     });
//                 }

//                 function onTouchend() {
//                     scope.$apply(function() {
//                         // updateModel
//                         scope.updateModel();

//                         // notify that drawing has ended
//                         scope.notifyDrawing({ drawing: false });
//                     });
//                 }
//             }
//         };
//     }
// ]);

// Backward compatibility
// angular.module('ngSignaturePad', ['signature']);