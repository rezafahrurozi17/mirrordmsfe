angular.module('app')
  .factory('PreDiagnoseFirstCheck', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
    	create:function(lmModel){
    		return $http.post('/api/as/PostPrediagnoseFirstCheck', [{
    												PrediagnoseId :lmModel.PrediagnoseId,
    												SystemCheck :lmModel.SystemCheck,
    												Dtc :lmModel.Dtc,
    												Desc :lmModel.Desc,
    												Stats :lmModel.Stats,
    												Freeze :lmModel.Freeze,
    												isFirstCheck : 1
    		                                  }]);
    	},
	    GetDataFirstCheck : function(PredId){
	        var res=$http.get('/api/as/GetPrediagnosesFirstCheck/'+PredId+'');
	        // console.log('res=>',res);
	        return res;        
	    },
	    update:function(lmModel){
	    	return $http.put('/api/as/PutPrediagnoseFirstCheck',[{
	    								  idPrediagnoseFirstCheck: lmModel.idPrediagnoseFirstCheck,
	    								  PrediagnoseId: lmModel.PrediagnoseId,
	    								  SystemCheck: lmModel.SystemCheck,
	    								  Dtc: lmModel.Dtc,
	    								  Desc: lmModel.Desc,
	    								  Stats: lmModel.Stats,
	    								  Freeze: lmModel.Freeze,
	    								  isFirstCheck: 1
	    						}]);
	    },
	    delete: function(idPrediagnoseFirstCheck) {
	      return $http.delete('/api/as/DeletePrediagnoseFirstCheck',{data:idPrediagnoseFirstCheck,headers: {'Content-Type': 'application/json'}});
	    }
    }
});