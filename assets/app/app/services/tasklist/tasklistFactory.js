angular.module('app')
  .factory('TaskList', function($http, CurrentUser) {
    var currentUser = CurrentUser.user;
    return {
      getData: function(filter) {        
        return $http.get('/api/as/TaskLists?'+$httpParamSerializer(filter));
      },
      create: function(obj) {
        return $http.post('/api/as/TaskLists',[obj]);
      },
      update: function(obj){
        return $http.put('/api/as/TaskLists',[obj]);
      },
      delete: function(arrId) {
        return $http.delete('/api/as/TaskLists', {data:arrId,headers: {'Content-Type': 'application/json'}});
      },
    }
  });