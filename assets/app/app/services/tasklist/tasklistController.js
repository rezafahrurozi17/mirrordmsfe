angular.module('app')
    .controller('TaskListController', function($scope, $http, CurrentUser, TaskList, $timeout) {
    //----------------------------------
    // Start-Up
    //----------------------------------
    $scope.$on('$viewContentLoaded', function() {
        $scope.loading=true;
        $scope.gridData=[];
    });
    //----------------------------------
    // Initialization
    //----------------------------------
    $scope.user = CurrentUser.user();
    $scope.mTaskList = null; //Model
    $scope.xTaskList = {};
    $scope.xTaskList.selected=[];
    $scope.filter = {};
    //----------------------------------
    // Get Data
    //----------------------------------
    $scope.getData = function() {
        TaskList.getData($scope.filter).then(
            function(res){
                $scope.grid.data = res.data.Result;
                $scope.loading=false;
                return res.data.Result;
            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    $scope.onShowDetail = function(row){
        TaskList.getData({TaskId: row.TaskId}).then(
            function(res){
                $scope.partList.data = res.data.Result;
                $scope.loading=false;

            },
            function(err){
                console.log("err=>",err);
            }
        );
    }
    //----------------------------------
    // Grid Setup
    //----------------------------------
    $scope.grid = {
        enableSorting: true,
        enableRowSelection: true,
        multiSelect: true,
        enableSelectAll: true,
        columnDefs: [
            { name:'TaskId',    field:'TaskId', visible:false },
            { name:'Kode Task', width:'30%', field: 'Code' },
            { name:'Nama Task', field: 'Name' },
            { name:'Flat Rate', field: 'FR' },
               
        ]
    };
    $scope.partList = {
        // enableCellEditOnFocus: true,
        columnDefs: [
            { name:'PartsId',    field:'PartsId', visible:false },
            { name:'Kode Parts', width:'20%', field: 'PartsCode' },
            { name:'Nama Parts', field: 'PartsName' },
            { name:'Qty', field: 'Qty'},
            { name:'Satuan', field: 'UomId'}
        ]
    };
    $scope.loading=true;

});
