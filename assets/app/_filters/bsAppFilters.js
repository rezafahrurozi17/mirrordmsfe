angular.module('bsAppFilters',[])
.filter('mapPeriodType', function() {
  var periodTypeList = {
    1: 'OAP',
    2: 'RAP'
  };

  return function(input) {
    console.log("input=>",input);
    if (!input){
      return '';
    } else {
      return periodTypeList[input];
    }
  };
});