angular.module('bsFilters',[])
.filter('trustHtml', function($sce){
	return function(value) {
	  return $sce.trustAs('html', value);
  };
})
.filter('dateInMillis', function() {
    return function(dateString) {
      return Date.parse(dateString);
  };
})
.filter('truncate', function () {
        return function (text, length, end) {
            if (isNaN(length))
                length = 10;

            if (end === undefined)
                end = "...";

            if (text.length <= length || text.length - end.length <= length) {
                return text;
            }
            else {
                return String(text).substring(0, length-end.length) + end;
            }
        };
})
.filter('htmlToPlaintext', function() {
    return function(text) {
      return angular.element(text).text();
    }
})
.filter('mapGender', function() {
  var genderList = {
    1: 'male',
    2: 'female'
  };

  return function(input) {
    if (!input){
      return '';
    } else {
      return genderList[input];
    }
  };
})
.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);

      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if(item[prop]){
            if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
              itemMatches = true;
              break;
            }
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
})
// .filter('percentage', function($filter) {
//   return function(input) {
//     if (undefined === input || null === input) {
//       return "";
//     }
//     var output = (input * 100).toFixed(0).replace(".", ",")+'%';
//     return output;
//   };
// });
.filter('percentage', function($filter) {
  return function(input) {   
    console.log("inside percentage : "+input); 
    return input+'%';
  };
});