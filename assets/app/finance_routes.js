//-- routes untuk part di sini
angular.module('app')
    .config(['$locationProvider','$stateProvider','$urlRouterProvider', function($locationProvider,$stateProvider, $urlRouterProvider) {
 
    $stateProvider
		  
         .state('app.jenispajak', {
            //url: '/kb',
            data: {
                title: 'Jenis Pajak',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "jenispajak@app": {
                    templateUrl: 'app/finance/jenisPajak/jenisPajak.html',
                    controller: 'JenisPajakController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		.state('app.materialcost', {
            //url: '/kb',
            data: {
                title: 'Material Cost',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "materialcost@app": {
                    templateUrl: 'app/finance/materialCost/materialCost.html',
                    controller: 'MaterialCostController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		
		.state('app.masterparamglaccount', {
            //url: '/kb',
            data: {
                title: 'Konfigurasi GL Account',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "masterparamglaccount@app": {
                    templateUrl: 'app/finance/parameterGLAccount/parameterGLAccount.html',
                    controller: 'ParameterGLAccountController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		
		.state('app.pajakmasukan', {
            //url: '/kb',
            data: {
                title: 'Pajak Masukan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "pajakmasukan@app": {
                    templateUrl: 'app/finance/pajakMasukan/pajakMasukan.html',
                    controller: 'PajakMasukanController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		
		.state('app.financePotonganPajak', {
            data: {
                title: 'Nomor Bukti Potongan Pajak',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "financePotonganPajak@app": {
                    templateUrl: 'app/finance/parameterSettings/financePotonganPajak/financePotonganPajak.html',
                    controller: 'FinancePotonganPajakController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		
		.state('app.pajakkeluaran', {
            //url: '/kb',
            data: {
                title: 'Pajak Keluaran',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "pajakkeluaran@app": {
                    templateUrl: 'app/finance/pajakKeluaran/pajakKeluaran.html',
                    controller: 'PajakKeluaranController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
        .state('app.pajakpenghasilan', {
            //url: '/kb',
            data: {
                title: 'Pajak Penghasilan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "pajakpenghasilan@app": {
                    templateUrl: 'app/finance/pajakPenghasilan/pajakPenghasilan.html',
                    controller: 'PajakPenghasilanController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		.state('app.pphprogresif', {
            //url: '/kb', 
            data: { 
                title: 'PPh21 Progresif',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "pphprogresif@app": {
                    templateUrl: 'app/finance/parameterSettings/pphProgresif/pphProgresif.html',
                    controller: 'PPhProgresifController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		.state('app.pembuatanfakturpajak', {
            //url: '/kb',
            data: {
                title: 'Pembuatan Faktur Pajak',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "pembuatanfakturpajak@app": {
                    templateUrl: 'app/finance/pembuatanFakturPajak/pembuatanFakturPajak.html',
                    controller: 'PembuatanFakturPajakController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		.state('app.paramanggaranbelanja', {
            //url: '/kb',
            data: {
                title: 'Parameter Anggaran Belanja',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "paramanggaranbelanja@app": {
                    templateUrl: 'app/finance/parameterSettings/paramAnggaranBelanja/paramAnggaranBelanja.html',
                    controller: 'ParamAnggaranBelanjaController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		.state('app.anggaranbelanja', {
            //url: '/kb',
            data: {
                title: 'Anggaran Belanja',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "anggaranbelanja@app": {
                    templateUrl: 'app/finance/anggaranBelanja/anggaranBelanja.html',
                    controller: 'AnggaranBelanjaController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		
		.state('app.reportaraging', {
            //url: '/kb',
            data: {
                title: 'Report AR Aging',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "reportaraging@app": {
                    templateUrl: 'app/finance/report/arAging/reportArAging.html',
                    controller: 'ReportArAgingController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		
		.state('app.daftarnomorkendaraan', {
            //url: '/kb',
            data: {
                title: 'Daftar Rincian Nomor Kendaraan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "daftarnomorkendaraan@app": {
                    templateUrl: 'app/finance/report/daftarRincianKendaraan/daftarRincianKendaraan.html',
                    controller: 'DaftarRincianKendaraanController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		
		.state('app.batalbilling', {
            //url: '/kb',
            data: {
                title: 'Report Batal Billing',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "batalbilling@app": {
                    templateUrl: 'app/finance/report/batalBilling/batalBilling.html',
                    controller: 'BatalBillingController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		
		.state('app.reportbungadf', {
            //url: '/kb',
            data: {
                title: 'Report Bunga DF',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "reportbungadf@app": {
                    templateUrl: 'app/finance/report/BungaDFUnit/reportBungaDFUnit.html',
                    controller: 'ReportBungaDFUnitController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })	
		
		.state('app.penggunaanpreprintedkuitansi', {
            //url: '/kb',
            data: {
                title: 'Penggunaan Preprinted Kuitansi',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "penggunaanpreprintedkuitansi@app": {
                    templateUrl: 'app/finance/report/penggunaanKuitansi/penggunaanKuitansi.html',
                    controller: 'PenggunaanKuitansiController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		
		.state('app.reportgrossprofit', {
            //url: '/kb',
            data: {
                title: 'Report Gross Profit',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "reportgrossprofit@app": {
                    templateUrl: 'app/finance/report/grossProfit/reportGrossProfit.html',
                    controller: 'ReportGrossProfitController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		
		.state('app.reportgrossprofitparts', {
            //url: '/kb',
            data: {
                title: 'Report Gross Profit Parts',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "reportgrossprofitparts@app": {
                    templateUrl: 'app/finance/report/grossProfitParts/reportGrossProfitParts.html',
                    controller: 'ReportGrossProfitPartsController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		
		.state('app.reportdailycashbank', {
            //url: '/kb',
            data: {
                title: 'Report Daily Cash Bank',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "reportdailycashbank@app": {
                    templateUrl: 'app/finance/report/dailyCashBank/reportDailyCashBank.html',
                    controller: 'ReportDailyCashBankController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		
		.state('app.parambeamateraielektronik', {
            //url: '/kb',
            data: {
                title: 'Parameter Bea Materai Elektronik',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parambeamateraielektronik@app": {
                    templateUrl: 'app/finance/parameterSettings/paramBeaMateraiElektronik/paramBeaMateraiElektronik.html',
                    controller: 'ParamBeaMateraiElektronikController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		.state('app.parambjdbph', {
            //url: '/kb',
            data: {
                title: 'Parameter BJD dan BPH',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parambjdbph@app": {
                    templateUrl: 'app/finance/parameterSettings/paramBJDBPH/paramBJDBPH.html',
                    controller: 'ParamBJDBPHController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
		.state('app.paramkuitansittus', {
            //url: '/kb',
            data: {
                title: 'Parameter Kuitansi dan TTUS',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "paramkuitansittus@app": {
                    templateUrl: 'app/finance/parameterSettings/paramKuitansiTTUS/paramKuitansiTTUS.html',
                    controller: 'ParamKuitansiTTUSController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
        .state('app.financePajak', {
            //url: '/kb',
            data: {
                title: 'Finance Pajak',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "financePajak@app": {
                    templateUrl: 'app/finance/parameterSettings/financePajak/financePajak.html',
                    controller: 'FinancePajakController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
        .state('app.biayamaterai', {
            //url: '/kb',
            data: {
                title: 'Biaya Materai',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "biayamaterai@app": {
                    templateUrl: 'app/finance/biayaMaterai/biayaMaterai.html',
                    controller: 'BiayaMateraiController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
        .state('app.bank', { //format harus "app.nama_menu" utk daftarin di sref db Menu
        //url: '/kb',
            data: {
                title: 'Bank',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "bank@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/bank/bank.html', //lokasi dmn html nya
                    controller: 'BankController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
        .state('app.coa', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Chart Of Account',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "coa@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/chartOfAccount/chartOfAccount.html', //lokasi dmn html nya
                    controller: 'ChartOfAccountController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
        .state('app.costcenter', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Cost Center',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "costcenter@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/costCenter/costCenter.html', //lokasi dmn html nya
                    controller: 'CostCenterController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
        .state('app.rekeningbank', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Rekening Bank',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "rekeningbank@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/rekeningBank/rekeningBank.html', //lokasi dmn html nya
                    controller: 'RekeningBankController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })

        .state('app.masteredc', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Mesin EDC',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "masteredc@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/master/masterEDC/masterEDC.html', //lokasi dmn html nya
                    controller: 'MasterEDCController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })


        .state('app.rekapincomingpaymentcreditdebit', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Transaksi Kas dan Bank',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "rekapincomingpaymentcreditdebit@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/rekapIncomingPaymentCreditDebit/rekapIncomingPaymentCreditDebit.html', //lokasi dmn html nya
                    controller: 'RekapIncomingPaymentCreditDebitController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })


        .state('app.reconciliation', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Reconciliation',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "reconciliation@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/reconciliation/reconciliation.html', //lokasi dmn html nya
                    controller: 'ReconciliationController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
        //-------------------------------------//
        //END Application Module - FINANCE     //
        //-------------------------------------//
        
        //-------------------------------------//
        //BEGIN Application Module - FINANCE   //
        //-------------------------------------//
        .state('app.incomingpayment', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'incomingpayment',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "incomingpayment@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/incomingPayment/incomingPayment.html', //lokasi dmn html nya
                    controller: 'IncomingPaymentController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })

        .state('app.transaksicashbank', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'transaksicashbank',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "transaksicashbank@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/transaksiCashBank/transaksiCashBank.html', //lokasi dmn html nya
                    controller: 'TransaksiCashBankController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })


        .state('app.alokasiunknown', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'alokasiunknown',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "alokasiunknown@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/alokasiUnknown/alokasiUnknown.html', //lokasi dmn html nya
                    controller: 'AlokasiUnknownController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })

        .state('app.invoicemasuk', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Invoice Masuk',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "invoicemasuk@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/invoiceMasuk/invoiceMasuk.html', //lokasi dmn html nya
                    controller: 'InvoiceMasukController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })

 
 

        .state('app.distribusibookingfee', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'distribusibookingfee',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "distribusibookingfee@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/distribusiBookingFee/distribusiBookingFee.html', //lokasi dmn html nya
                    controller: 'DistribusiBookingFeeController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })

        .state('app.instruksipenerimaan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Instruksi Penerimaan',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "instruksipenerimaan@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/instruksiPenerimaan/instruksiPenerimaan.html', //lokasi dmn html nya
                    controller: 'InstruksiPenerimaanController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })

        .state('app.outgoingpayment', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Outgoing Payment',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "outgoingpayment@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/outgoingPayment/outgoingPayment.html', //lokasi dmn html nya
                    controller: 'OutgoingPaymentController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
		
		.state('app.nomorFakturPajak', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Nomor Faktur Pajak',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "nomorFakturPajak@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/nomorFakturPajak/nomorFakturPajak.html', //lokasi dmn html nya
                    controller: 'NomorFakturPajakController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })	
		
		.state('app.notifikasiIncomingPayment', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Notifikasi Incoming Payment',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "notifikasiIncomingPayment@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/notifikasiIncomingPayment/notifikasiIncomingPayment.html', //lokasi dmn html nya
                    controller: 'NotifikasiIncomingPaymentController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })		
		
		.state('app.penerimaanCashSPK', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Penerimaan Cash SPK',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "penerimaanCashSPK@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/penerimaanCashSPK/penerimaanCashSPK.html', //lokasi dmn html nya
                    controller: 'PenerimaanCashSPKController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
		
		.state('app.penerimaanCashCITCIB', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Penerimaan Cash CIT CIB',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "penerimaanCashCITCIB@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/penerimaanCashCITCIB/penerimaanCashCITCIB.html', //lokasi dmn html nya
                    controller: 'PenerimaanCashCITCIBController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })

		.state('app.reportPembayaran', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Report Pembayaran',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "reportPembayaran@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/report/pembayaran/reportPembayaran.html', //lokasi dmn html nya
                    controller: 'ReportPembayaranController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
		
		.state('app.reportPenjualanServicePart', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Report Penjualan Service Part',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "reportPenjualanServicePart@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/report/penjualanServicePart/reportPenjualanServicePart.html', //lokasi dmn html nya
                    controller: 'ReportPenjualanServicePartController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
	
		.state('app.reportPenjualanUnit', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Report Penjualan Unit',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "reportPenjualanUnit@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/report/penjualanUnit/reportPenjualanUnit.html', //lokasi dmn html nya
                    controller: 'ReportPenjualanUnitController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })			
		
		.state('app.reportCashMargin', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Report Cash Margin',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "reportCashMargin@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/report/cashmargin/reportCashMargin.html', //lokasi dmn html nya
                    controller: 'ReportCashMarginController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
		
		.state('app.reportAPAging', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Report AP Aging',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "reportAPAging@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/report/apAging/reportAPAging.html', //lokasi dmn html nya
                    controller: 'ReportAPAgingController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })	 

		.state('app.reportPenerimaan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Report Penerimaan',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "reportPenerimaan@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/report/Penerimaan/reportPenerimaan.html', //lokasi dmn html nya
                    controller: 'ReportPenerimaanController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })	

		.state('app.reportIncomingEscrowAccount', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Report Incoming Escrow Account',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "reportIncomingEscrowAccount@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/report/incomingEscrowAccount/reportIncomingEscrowAccount.html', //lokasi dmn html nya
                    controller: 'ReportIncomingEscrowAccountController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })			
		
		.state('app.reportadvancepayment', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Report Advance Payment',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "reportadvancepayment@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/report/advancePayment/reportAdvancePayment.html', //lokasi dmn html nya
                    controller: 'ReportAdvancePaymentController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
		
		.state('app.reportprofitloss', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Report Profit Loss',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "reportprofitloss@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/report/profitLoss/reportProfitLoss.html', //lokasi dmn html nya
                    controller: 'ReportProfitLossController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
		
		.state('app.reportincomingstatement', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Report Profit Loss',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "reportincomingstatement@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/report/incomingStatement/reportIncomingStatement.html', //lokasi dmn html nya
                    controller: 'ReportIncomingStatementController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
		
		.state('app.reportrekon', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Reconciliation Report',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "reportrekon@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/report/rekon/reportRekon.html', //lokasi dmn html nya
                    controller: 'reportRekonController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })		
		
		.state('app.reportestampduty', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Report EStamp Duty',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "reportestampduty@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/report/eStampDuty/reportEStampDuty.html', //lokasi dmn html nya
                    controller: 'ReportEStampDutyController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
		
		.state('app.reportpenggunaannomorfakturpajak', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Report EStamp Duty',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "reportpenggunaannomorfakturpajak@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/report/penggunaanNomorFakturPajak/reportPenggunaanNomorFakturPajak.html', //lokasi dmn html nya
                    controller: 'ReportPenggunaanNomorFakturPajakController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
		
		.state('app.reporttitipan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Report Titipan',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "reporttitipan@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/report/titipan/reportTitipan.html', //lokasi dmn html nya
                    controller: 'ReportTitipanController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
		
        .state('app.instruksipembayaran', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'instruksipembayaran',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "instruksipembayaran@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/instruksiPembayaran/instruksiPembayaran.html', //lokasi dmn html nya
                    controller: 'InstruksiPembayaranController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
		
		.state('app.cetakkuitansivirtualaccount', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'cetakkuitansivirtualaccount',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "cetakkuitansivirtualaccount@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/cetakKuitansiVirtualAccount/cetakKuitansiVirtualAccount.html', //lokasi dmn html nya
                    controller: 'CetakKuitansiVirtualAccountController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
		
		.state('app.cetakulang', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'cetakulang',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "cetakulang@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/cetakUlang/cetakUlang.html', //lokasi dmn html nya
                    controller: 'CetakUlangController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
		
		.state('app.financepersetujuan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'financepersetujuan',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "financepersetujuan@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/financePersetujuan/financePersetujuan.html', //lokasi dmn html nya
                    controller: 'FinancePersetujuanController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
		.state('app.tipegrupgl', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Tipe Grup GL',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "tipegrupgl@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/master/tipeGrupGL/tipeGrupGL.html', //lokasi dmn html nya
                    controller: 'TipeGrupGLController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
        .state('app.setpajakgabungan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Set Pajak Gabungan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "setpajakgabungan@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/setPajakGabungan/setPajakGabungan.html', //lokasi dmn html nya
                    controller: 'SetPajakGabunganController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
        .state('app.registrasidokumenpajak', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Registrasi dokumen Pajak',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "registrasidokumenpajak@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/registrasiDokumenPajak/registrasiDokumenPajak.html', //lokasi dmn html nya
                    controller: 'RegistrasiDokumenPajakController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
        .state('app.selisihkelebihan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Selisih Kelebihan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "selisihkelebihan@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/parameterSettings/selisihKelebihan/selisihKelebihan.html', //lokasi dmn html nya
                    controller: 'SelisihKelebihanController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
        .state('app.refundcg', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Refund CG',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "refundcg@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/parameterSettings/refundCG/refundCG.html', //lokasi dmn html nya
                    controller: 'RefundCGController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
        .state('app.preprintedkuitansi', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Kuitansi Preprinted',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "preprintedkuitansi@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/parameterSettings/preprintedKuitansi/preprintedKuitansi.html', //lokasi dmn html nya
                    controller: 'PreprintedKuitansiController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
		
		.state('app.preprintedreceiptnum', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Nomor Preprinted Kuitansi',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "preprintedreceiptnum@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/parameterSettings/preprintedReceiptNum/preprintedReceiptNum.html', //lokasi dmn html nya
                    controller: 'PreprintedReceiptNumController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
		
        .state('app.periodereversal', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Periode Reversal',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "periodereversal@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/parameterSettings/periodeReversal/periodeReversal.html', //lokasi dmn html nya
                    controller: 'PeriodeReversalController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
        .state('app.beamateraielektronik', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Bea Materai Elektronik',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "beamateraielektronik@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/beaMateraiElektronik/beaMateraiElektronik.html', //lokasi dmn html nya
                    controller: 'BeaMateraiElektronikController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
        .state('app.tipetransaksigl', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Tipe Transaksi GL',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "tipetransaksigl@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/master/tipeTransaksiGL/tipeTransaksiGL.html', //lokasi dmn html nya
                    controller: 'TipeTransaksiGLController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })

        .state('app.alokasibfdpunit', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Alokasi BF/DP Unit',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "alokasibfdpunit@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/alokasiBFDPUnit/alokasiBFDPUnit.html', //lokasi dmn html nya
                    controller: 'alokasiBFDPUnitController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
        .state('app.clearing', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Clearing',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "clearing@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/clearing/clearing.html', //lokasi dmn html nya
                    controller: 'clearingController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
        .state('app.proposal', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Proposal Payment',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "proposal@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/proposal/proposal.html', //lokasi dmn html nya
                    controller: 'proposalController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
        .state('app.parampajakkeluaran', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Param Pajak Keluaran',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parampajakkeluaran@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/parameterSettings/paramPajakKeluaran/paramPajakKeluaran.html', //lokasi dmn html nya
                    controller: 'ParamPajakKeluaranController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
        .state('app.advancedpayment', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Advanced Payment',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "advancedpayment@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/advancedPayment/advancedPayment.html', //lokasi dmn html nya
                    controller: 'AdvancedPaymentController'
                }
            },
            resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

            },
            params: { tab : null },
        })
 
		
		.state('app.parambungadf', { //format harus "app.nama_menu" utk daftarin di sref db Menu
		//url: '/kb',
		data: {
			title: 'Param Bunga DF',
			access: 1
		},
		deepStateRedirect: true,
		sticky: true,
		views: {
			"parambungadf@app": { //nama_menu + "@app"
				templateUrl: 'app/finance/parameterSettings/paramBungaDF/paramBungaDF.html', //lokasi dmn html nya
				controller: 'ParamBungaDFController'
			}
		},
		resolve: {
				   sysapp: ['$ocLazyLoad',function($ocLazyLoad){
					 return $ocLazyLoad.load('sysapp');
				   }],
				   AppModule: ['$ocLazyLoad',function($ocLazyLoad){
					 return $ocLazyLoad.load('app_finance');
				   }]

		},
		params: { tab : null },
        })
        .state('app.financeVersion', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Finance Version',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"financeVersion@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/financeVersion/financeVersion.html', //lokasi dmn html nya
					controller: 'FinanceVersionController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })
        .state('app.registerCheque', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Register Cheque',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"registerCheque@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/registerCheque/registerCheque.html', //lokasi dmn html nya
					controller: 'RegisterChequeController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })
        .state('app.registerchequegiro', { //format harus "app.nama_menu" utk daftarin di sref db Menu
        //url: '/kb',
        data: {
            title: 'Register Cheque/Giro Pembayaran',
            access: 1
        },
        deepStateRedirect: true, 
        sticky: true,
        views: {
            "registerchequegiro@app": { //nama_menu + "@app"
                templateUrl: 'app/finance/registerChequeGiro/registerChequeGiro.html', //lokasi dmn html nya
                controller: 'registerChequeGiroController'
            }
        },
        resolve: {
                    sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                        return $ocLazyLoad.load('app_finance');
                    }]

        },
        params: { tab : null },
    })
        .state('app.informasipembayaranleasing', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Informasi Pembayaran: Leasing',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"informasipembayaranleasing@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/informasiPembayaranLeasing/informasiPembayaranLeasing.html', //lokasi dmn html nya
					controller: 'informasiPembayaranLeasingController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })		
		
        .state('app.masterpricingdealertobranchfinishunit', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Master PE Finish Unit',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"masterpricingdealertobranchfinishunit@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/masterPricingDealerToBranchFinishedUnit/masterPricingDealerToBranchFinishedUnit.html', //lokasi dmn html nya
					controller: 'MasterPricingDealerToBranchFinishedUnitController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })			
		
        .state('app.masterpricingdealertobranchfinishunitspcprg', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Master PE Finish Unit Special Program',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"masterpricingdealertobranchfinishunitspcprg@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/masterPricingDealerToBranchFinishedSpcPrg/masterPricingDealerToBranchFinishedSpcPrg.html', //lokasi dmn html nya
					controller: 'MasterPricingDealerToBranchFinishedSpcPrgController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })		

		.state('app.masterpricingdealertobranchfinishunitbranch', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Master PE Finish Unit By Branch',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"masterpricingdealertobranchfinishunitbranch@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/masterPricingDealerToBranchFinishedBranch/masterPricingDealerToBranchFinishedBranch.html', //lokasi dmn html nya
					controller: 'MasterPricingDealerToBranchFinishedBranchController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })			

		.state('app.masterpricingdealertobranchfinishunitspccust', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Master PE Finish Unit By Special Customer',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"masterpricingdealertobranchfinishunitspccust@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/masterPricingDealerToBranchFinishedSpcCust/masterPricingDealerToBranchFinishedSpcCust.html', //lokasi dmn html nya
					controller: 'MasterPricingDealerToBranchFinishedSpcCustController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })			
		
		.state('app.masterpricingdealertobranchkaroseri', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Master PE Karoseri',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"masterpricingdealertobranchkaroseri@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/masterPricingDealerToBranchKaroseri/masterPricingDealerToBranchKaroseri.html', //lokasi dmn html nya
					controller: 'MasterPricingDealerToBranchKaroseriController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })		
		
		.state('app.masterpricingdealertobranchaccpkg', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Master PE Price Paket Accessories',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"masterpricingdealertobranchaccpkg@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/masterPricingDealerToBranchAccPackage/masterPricingDealerToBranchAccPackage.html', //lokasi dmn html nya
					controller: 'MasterPricingDealerToBranchAccPackageController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })	
		
	    .state('app.masterpricingdealertobranchlogisticprice', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Master PE Price Link Paket DIO',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"masterpricingdealertobranchlogisticprice@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/masterPricingDealerToBranchLogisticPrice/masterPricingDealerToBranchLogisticPrice.html', //lokasi dmn html nya
					controller: 'MasterPricingDealerToBranchLogisticPriceController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })	
		
	    .state('app.masterpricingdealertobranchsellingcost', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Master PE Selling Cost',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"masterpricingdealertobranchsellingcost@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/masterPricingDealerToBranchSellingCost/masterPricingDealerToBranchSellingCost.html', //lokasi dmn html nya
					controller: 'MasterPricingDealerToBranchSellingCostController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })		

	    .state('app.masterpricingtamtodealersellingcost', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Master PE TAM Selling Cost',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"masterpricingtamtodealersellingcost@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/masterPricingTAMToDealerSellingCost/masterPricingTAMToDealerSellingCost.html', //lokasi dmn html nya
					controller: 'MasterPricingTAMToDealerSellingCostController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]
			},
			params: { tab : null },
        })			

		.state('app.masterpricingdealertobranchbbn', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Master PE BBN',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"masterpricingdealertobranchbbn@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/masterPricingDealerToBranchBBN/masterPricingDealerToBranchBBN.html', //lokasi dmn html nya
					controller: 'MasterPricingDealerToBranchBBNController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })

        .state('app.masterpriceopd', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Master Price OPD',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "masterpriceopd@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/masterPriceOPD/masterPriceOPD.html', //lokasi dmn html nya
                    controller: 'MasterPriceOPDController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })
		
		.state('app.masterpricingdealertobranchpriceacc', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Master PE BBN',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"masterpricingdealertobranchpriceacc@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/masterPricingDealerToBranchPriceAccessories/masterPricingDealerToBranchPriceAccessories.html', //lokasi dmn html nya
					controller: 'MasterPricingDealerToBranchPriceAccessoriesController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })	
		
	    .state('app.masterpricingdealertobranchpricelinkdio', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Master PE Price Link Paket DIO',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"masterpricingdealertobranchpricelinkdio@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/masterPricingDealerToBranchPriceLinkDIO/masterPricingDealerToBranchPriceLinkDIO.html', //lokasi dmn html nya
					controller: 'MasterPricingDealerToBranchPriceLinkDIOController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })	
		
	    .state('app.masterpricingdealertobranchareapricing', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Master PE Price Link Paket DIO',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"masterpricingdealertobranchareapricing@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/masterPricingDealerToBranchAreaPricing/masterPricingDealerToBranchAreaPricing.html', //lokasi dmn html nya
					controller: 'MasterPricingDealerToBranchAreaPricingController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })	
		
	    .state('app.masterpricingdealertobranchpricedio', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Master PE Price DIO',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"masterpricingdealertobranchpricedio@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/masterPricingDealerToBranchPriceDIO/masterPricingDealerToBranchPriceDIO.html', //lokasi dmn html nya
					controller: 'MasterPricingDealerToBranchPriceDIOController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })		
		
        .state('app.informasipembayaranasuransi', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Informasi Pembayaran: Asuransi',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"informasipembayaranasuransi@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/informasiPembayaranAsuransi/informasiPembayaranAsuransi.html', //lokasi dmn html nya
					controller: 'informasiPembayaranAsuransiController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })			
        .state('app.informasibuktipotong', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Informasi Bukti Potong PPH23: Asuransi',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"informasibuktipotong@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/informasiBuktiPotong/informasiBuktiPotong.html', //lokasi dmn html nya
					controller: 'informasiBuktiPotongController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })		
        .state('app.pedealertobranch', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Pricing Engine',
                access: 1
            },
            deepStateRedirect: true, 
            sticky: true,
            views: {
                "pedealertobranch@app": { //nama_menu + "@app"
                    templateUrl: 'app/finance/PricingEngineDealerToBranch/PEDealerToBranch.html', //lokasi dmn html nya
                    controller: 'PEDealerToBranchController'
                }
            },
            resolve: {
                        sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('sysapp');
                        }],
                        AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                            return $ocLazyLoad.load('app_finance');
                        }]

            },
            params: { tab : null },
        })

        .state('app.reopenperiode', {
            //url: '/kb',
            data: {
                title: 'Reopen Periode',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "reopenperiode@app": {
                    templateUrl: 'app/finance/reopenPeriode/reopenPeriode.html',
                    controller: 'ReopenPeriodeController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_finance');
                       }]

            },
            params: { tab : null },
        })
                
        //-------------------------------------//
        //END Application Module - FINANCE     //
        //-------------------------------------//
         
		.state('app.templatepricingengine', { //format harus "app.nama_menu" utk daftarin di sref db Menu
			//url: '/kb',
			data: {
				title: 'Pricinng Engine',
				access: 1
			},
			deepStateRedirect: true, 
			sticky: true,
			views: {
				"templatepricingengine@app": { //nama_menu + "@app"
					templateUrl: 'app/finance/templatePricingEngine/templatePricingEngine.html', //lokasi dmn html nya
					controller: 'TemplatePricingEngineController'
				}
			},
			resolve: {
						sysapp: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('sysapp');
						}],
						AppModule: ['$ocLazyLoad',function($ocLazyLoad){
							return $ocLazyLoad.load('app_finance');
						}]

			},
			params: { tab : null },
        })
        
        
  }])
