//-- routes untuk part di sini
angular.module('app')
    .config(['$locationProvider', '$stateProvider', '$urlRouterProvider', function($locationProvider, $stateProvider, $urlRouterProvider) {

        $stateProvider
        

            .state('app.boardsalesglobalparameter', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Board Sales Global Parameter',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "boardsalesglobalparameter@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales8/boardSalesGlobalParameter/boardSalesGlobalParameter.html', //lokasi dmn html nya
                    controller: 'BoardSalesGlobalParameterController'
                }
            },

            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales8');
                }]

            },
            params: { tab: null },
        })

        .state('app.boardsalesmonitoringdriver', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Board Sales Monitoring Driver',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "boardsalesmonitoringdriver@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales8/boardSalesMonitoringDriver/boardSalesMonitoringDriver.html', //lokasi dmn html nya
                    controller: 'BoardSalesMonitoringDriverController'
                }
            },

            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales8');
                }]

            },
            params: { tab: null },
        })

        .state('app.mastervirtualaccount', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Master Virtual Account',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "mastervirtualaccount@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/virtualAccount/virtualAccount.html', //lokasi dmn html nya
                    controller: 'VirtualAccountController'
                }
            },

            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.masterstokbasket', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Konfigurasi Stok Basket',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "masterstokbasket@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/konfigurasiStokBasket/konfigurasiStokBasket.html', //lokasi dmn html nya
                    controller: 'KonfigurasiStokBasketController'
                }
            },

            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.mastercao', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Master CAO',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "mastercao@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/masterCAO/masterCAO.html', //lokasi dmn html nya
                    controller: 'MasterCAOController'
                }
            },

            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

		 .state('app.mastersalesratio', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Sales Ratio',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "mastersalesratio@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/ratioSales/ratioSales.html', //lokasi dmn html nya
                    controller: 'RatioSalesController'
                }
            },

            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })
		
        .state('app.boardsalesproductivitysales', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Board Sales Productivity Sales',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "boardsalesproductivitysales@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales8/boardSalesProductivitySales/boardSalesProductivitySales.html', //lokasi dmn html nya
                    controller: 'BoardSalesProductivitySalesController'
                }
            },

            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales8');
                }]

            },
            params: { tab: null },
        })

        .state('app.boardsales', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Board Sales',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "boardsales@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales8/boardSales/boardSales.html', //lokasi dmn html nya
                    controller: 'BoardSalesController'
                }
            },

            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales8');
                }]

            },
            params: { tab: null },
        })

        .state('app.sampleuitemplate', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Sample UI Template',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "sampleuitemplate@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sampleUITemplate/sampleUITemplate.html', //lokasi dmn html nya
                    controller: 'SampleUITemplateController'
                }
            },

            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales');
                }]

            },
            params: { tab: null },
        })

        .state('app.sampleuitemplate2', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Sample UI Template 2',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "sampleuitemplate2@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sampleUITemplate2/sampleUITemplate2.html', //lokasi dmn html nya
                    controller: 'SampleUITemplate2Controller'
                }
            },

            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales');
                }]

            },
            params: { tab: null },
        })

        .state('app.samplecetakan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Sample Cetakan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "samplecetakan@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sampleCetakan/sampleCetakan.html', //lokasi dmn html nya
                    controller: 'SampleCetakanController'
                }
            },

            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales');
                }]

            },
            params: { tab: null },
        })

        .state('app.templatedigitalsignature', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Template Digital Signature',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "templatedigitalsignature@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/templateDigitalSignature/templateDigitalSignature.html', //lokasi dmn html nya
                    controller: 'TemplateDigitalSignatureController'
                }
            },

            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales');
                }]

            },
            params: { tab: null },
        })

        /*---------END Sample Template--------------------*/


        /*---------BEGIN Master Data SALES----------*/
        .state('app.activitychecklistitemadministrasipembayaran', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Activity Checklist Item Administrasi Pembayaran',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "activitychecklistitemadministrasipembayaran@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/activityChecklistItemAdministrasiPembayaran/activityChecklistItemAdministrasiPembayaran.html', //lokasi dmn html nya
                    controller: 'ActivityChecklistItemAdministrasiPembayaranController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.activitychecklistitemdec', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Activity Checklist Item DEC',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "activitychecklistitemdec@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/activityChecklistItemDEC/activityChecklistItemDec.html', //lokasi dmn html nya
                    controller: 'ActivityChecklistItemDecController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.activitychecklistiteminspeksi', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Activity Checklist Item Inspeksi',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "activitychecklistiteminspeksi@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/activityChecklistItemInspeksi/activityChecklistItemInspeksi.html', //lokasi dmn html nya
                    controller: 'ActivityChecklistItemInspeksiController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.activitygroupchecklistdec', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Activity Group Checklist DEC',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "activitygroupchecklistdec@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/activityGroupChecklistDec/activityGroupChecklistDec.html', //lokasi dmn html nya
                    controller: 'ActivityGroupChecklistDECController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })


        .state('app.activitygroupchecklistinspeksi', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Activity Group Checklist Inspeksi',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "activitygroupchecklistinspeksi@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/activityGroupChecklistInspeksi/activityGroupChecklistInspeksi.html', //lokasi dmn html nya
                    controller: 'ActivityGroupChecklistInspeksiController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })


        .state('app.alasanpembeliankendaraan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Alasan Pembelian Kendaraan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "alasanpembeliankendaraan@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/alasanPembelianKendaraan/alasanPembelianKendaraan.html', //lokasi dmn html nya
                    controller: 'AlasanPembelianKendaraanController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })


        .state('app.assetbrosur', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Asset Brosur',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "assetbrosur@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/assetBrosur/assetBrosur.html', //lokasi dmn html nya
                    controller: 'AssetBrosurController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.assettestdriveunit', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Asset Test Drive Unit',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "assettestdriveunit@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/assetTestDriveUnit/assetTestDriveUnit.html', //lokasi dmn html nya
                    controller: 'AssetTestDriveUnitController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.profileasuransiperluasanjaminan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Profile Asuransi Perluasan Jaminan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "profileasuransiperluasanjaminan@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/profileAsuransiPerluasanJaminan/profileAsuransiPerluasanJaminan.html', //lokasi dmn html nya
                    controller: 'ProfileAsuransiPerluasanJaminanController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.profilebank', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Profile Bank',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "profilebank@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/profileBank/profileBank.html', //lokasi dmn html nya
                    controller: 'ProfileBankController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.profiledriverpds', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Profile Driver PDS',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "profiledriverpds@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/profileDriverPDS/profileDriverPDS.html', //lokasi dmn html nya
                    controller: 'ProfileDriverPDSController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.profileleasing', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Profile Leasing',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "profileleasing@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/profileLeasing/profileLeasing.html', //lokasi dmn html nya
                    controller: 'ProfileLeasingController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.financedaftarrekeningbank', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Finance Daftar Rekening Bank',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "financedaftarrekeningbank@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/financeDaftarRekeningBank/financeDaftarRekeningBank.html', //lokasi dmn html nya
                    controller: 'FinanceDaftarRekeningBankController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.siteareapelanggaranwilayah', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Area Pelanggaran Wilayah',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "siteareapelanggaranwilayah@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/siteAreaPelanggaranWilayah/siteAreaPelanggaranWilayah.html', //lokasi dmn html nya
                    controller: 'SiteAreaPelanggaranWilayahController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })


        .state('app.sitepds', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Site PDS',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "sitepds@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/sitePDS/sitePds.html', //lokasi dmn html nya
                    controller: 'SitePdsController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.unitperlengkapanstandar', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Unit Perlengkapan Standar',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "unitperlengkapanstandar@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/unitPerlengkapanStandar/unitPerlengkapanStandar.html', //lokasi dmn html nya
                    controller: 'UnitPerlengkapanStandarController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.unitchecklistperlengkapanstandar', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Unit Perlengkapan Standar',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "unitchecklistperlengkapanstandar@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/unitChecklistPerlengkapanStandar/unitChecklistPerlengkapanStandar.html', //lokasi dmn html nya
                    controller: 'UnitChecklistPerlengkapanStandarController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.unituom', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Unit UOM',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "unituom@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/unitUOM/unitUOM.html', //lokasi dmn html nya
                    controller: 'UnitUOMController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.activitymappingkendaraantestdrive', {
            //url: '/kb',
            data: {
                title: 'Activity Mapping Kendaraan Test Drive',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "activitymappingkendaraantestdrive@app": {
                    templateUrl: 'app/sales/sales9/master/activityMappingKendaraanTestDrive/activityMappingKendaraanTestDrive.html',
                    controller: 'ActivityMappingKendaraanTestDriveController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.activitypredefinerespond', {
            //url: '/kb',
            data: {
                title: 'Activity Pre Define Respond',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "activitypredefinerespond@app": {
                    templateUrl: 'app/sales/sales9/master/activityPreDefineRespond/activityPreDefineRespond.html',
                    controller: 'ActivityPreDefineRespondController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.activityworkingcalender', {
            //url: '/kb',
            data: {
                title: 'Activity Working Calendar',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "activityworkingcalender@app": {
                    templateUrl: 'app/sales/sales9/master/activityWorkingCalender/activityWorkingCalender.html',
                    controller: 'ActivityWorkingCalenderController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.assettestdriveunithistory', {
            //url: '/kb',
            data: {
                title: 'Asset Test Drive Unit History',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "assettestdriveunithistory@app": {
                    templateUrl: 'app/sales/sales9/master/assetTestDriveUnitHistory/assetTestDriveUnitHistory.html',
                    controller: 'AssetTestDriveUnitHistoryController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.profilecustomerfleet', {
            //url: '/kb',
            data: {
                title: 'Profile Customer Fleet',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "profilecustomerfleet@app": {
                    templateUrl: 'app/sales/sales9/master/profileCustomerFleet/profileCustomerFleet.html',
                    controller: 'ProfileCustomerFleetController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.profilesalesprogram', {
            //url: '/kb',
            data: {
                title: 'Profile Sales Program',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "profilesalesprogram@app": {
                    templateUrl: 'app/sales/sales9/master/profileSalesProgram/profileSalesProgram.html',
                    controller: 'ProfileSalesProgramController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })


        .state('app.activityalasanbatalpembeliankendaraan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'activityalasanbatalpembeliankendaraan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "activityalasanbatalpembeliankendaraan@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/activityAlasanBatalPembelianKendaraan/activityAlasanBatalPembelianKendaraan.html', //lokasi dmn html nya
                    controller: 'ActivityAlasanBatalPembelianKendaraanController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.activityalasanpengecualianpengiriman', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Activity Alasan Pengecualian Pengiriman',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "activityalasanpengecualianpengiriman@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/activityAlasanPengecualianPengiriman/activityAlasanPengecualianPengiriman.html', //lokasi dmn html nya
                    controller: 'ActivityAlasanPengecualianPengirimanController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.activityalasandropprospek', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'activityalasandropprospek',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "activityalasandropprospek@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/activityAlasanDropProspek/activityAlasanDropProspek.html', //lokasi dmn html nya
                        controller: 'ActivityAlasanDropProspekController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.unitaksesoris', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Unit Aksesoris',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "unitaksesoris@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/unitAksesoris/unitAksesoris.html', //lokasi dmn html nya
                        controller: 'UnitAksesorisController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.financedaftarcash', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Finance Daftar Cash',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "financedaftarcash@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/financeDaftarCash/financeDaftarCash.html', //lokasi dmn html nya
                        controller: 'FinanceDaftarCashController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.siteplantaddress', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Site Plant Address',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "siteplantaddress@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/sitePlantAddress/sitePlantAddress.html', //lokasi dmn html nya
                        controller: 'SitePlantAddressController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.siteareastock', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Site Area Stock',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "siteareastock@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/siteAreaStock/siteAreaStock.html', //lokasi dmn html nya
                        controller: 'SiteAreaStockController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })

        .state('app.accountingcoa', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Accounting COA',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "accountingcoa@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/accountingCOA/accountingCOA.html', //lokasi dmn html nya
                        controller: 'AccountingCOAController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.accountinginventorygroup', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Accounting Inventory Group',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "accountinginventorygroup@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/accountingInventoryGroup/accountingInventoryGroup.html', //lokasi dmn html nya
                        controller: 'AccountingInventoryGroupController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.accountinginventorytype', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Accounting Inventory Type',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "accountinginventorytype@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/accountingInventoryType/accountingInventoryType.html', //lokasi dmn html nya
                        controller: 'AccountingInventoryTypeController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.accountingtransactioncode', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Accounting Transaction Code',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "accountingtransactioncode@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/accountingTransactionCode/accountingTransactionCode.html', //lokasi dmn html nya
                        controller: 'AccountingTransactionCodeController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.globalparameter', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Global Parameter',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "globalparameter@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/globalParameter/globalParameter.html', //lokasi dmn html nya
                        controller: 'GlobalParameterController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.financefundsource', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Finance Fund Source',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "financefundsource@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/financeFundSource/financeFundSource.html', //lokasi dmn html nya
                        controller: 'FinanceFundSourceController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })


        .state('app.assetbrochuretype', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Asset Brochure Type',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "assetbrochuretype@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/assetBrochureType/assetBrochureType.html', //lokasi dmn html nya
                    controller: 'AssetBrochureTypeController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.reason', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Reason',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "reason@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/reason/reason.html', //lokasi dmn html nya
                    controller: 'ReasonController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.profilebirojasa', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Profile Biro Jasa',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "profilebirojasa@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/profileBiroJasa/profileBiroJasa.html', //lokasi dmn html nya
                    controller: 'ProfileBiroJasaController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.profileinsurance', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Profile Insurance',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "profileinsurance@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/profileInsurance/profileInsurance.html', //lokasi dmn html nya
                    controller: 'ProfileInsuranceController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.profileinsurancetype', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Profile Insurance Type',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "profileinsurancetype@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/profileInsuranceType/profileInsuranceType.html', //lokasi dmn html nya
                    controller: 'ProfileInsuranceTypeController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.profileinsuranceusertype', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Profile Insurance User Type',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "profileinsuranceusertype@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/profileInsuranceUserType/profileInsuranceUserType.html', //lokasi dmn html nya
                    controller: 'ProfileInsuranceUserTypeController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.profileleasingsimulation', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Profile Leasing Simulation',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "profileleasingsimulation@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/profileLeasingSimulation/profileLeasingSimulation.html', //lokasi dmn html nya
                    controller: 'ProfileLeasingSimulationController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.profileleasingtenor', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Profile Leasing Tenor',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "profileleasingtenor@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/profileLeasingTenor/profileLeasingTenor.html', //lokasi dmn html nya
                    controller: 'ProfileLeasingTenorController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.unitbrand', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Unit Brand',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "unitbrand@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/unitBrand/unitBrand.html', //lokasi dmn html nya
                        controller: 'UnitBrandController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.unitprice', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Unit Price',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "unitprice@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/unitPrice/unitPrice.html', //lokasi dmn html nya
                        controller: 'UnitPriceController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.unityear', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Unit Year',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "unityear@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/unitYear/unitYear.html', //lokasi dmn html nya
                        controller: 'UnitYearController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })


        .state('app.typeincomingpayment', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Type Incoming Payment',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "typeincomingpayment@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/typeIncomingPayment/typeIncomingPayment.html', //lokasi dmn html nya
                    controller: 'TypeIncomingPaymentController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.typeoutgoingpayment', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Type Outgoing Payment',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "typeoutgoingpayment@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/typeOutgoingPayment/typeOutgoingPayment.html', //lokasi dmn html nya
                    controller: 'TypeOutgoingPaymentController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.typerefund', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Type Refund',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "typerefund@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/typeRefund/typeRefund.html', //lokasi dmn html nya
                    controller: 'TypeRefundController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.typetransactionpoint', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Type Transaction Point',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "typetransactionpoint@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/typeTransactionPoint/typeTransactionPoint.html', //lokasi dmn html nya
                    controller: 'TypeTransactionPointController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.potype', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'PO Type',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "potype@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/poType/poType.html', //lokasi dmn html nya
                    controller: 'PoTypeController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.sotype', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'SO Type',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "sotype@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/soType/soType.html', //lokasi dmn html nya
                    controller: 'SoTypeController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.socancelreason', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'SO Cancel Reason',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "socancelreason@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/soCancelReason/soCancelReason.html', //lokasi dmn html nya
                    controller: 'SoCancelReasonController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.spkcancel', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'SPK Cancel',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "spkcancel@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/spkCancel/spkCancel.html', //lokasi dmn html nya
                    controller: 'SpkCancelController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.spklost', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'SPK Cancel',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "spklost@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/spkLost/spkLost.html', //lokasi dmn html nya
                    controller: 'SpkLostController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })


        .state('app.testdriveunit', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Test Drive Unit',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "testdriveunit@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/testDriveUnit/testDriveUnit.html', //lokasi dmn html nya
                    controller: 'TestDriveUnitController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.categorypromo', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Category Promo',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "categorypromo@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/categoryPromo/categoryPromo.html', //lokasi dmn html nya
                        controller: 'CategoryPromoController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.categoryreasonbookingunitrevision', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Category Reason Booking Unit Revision',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "categoryreasonbookingunitrevision@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/categoryReasonBookingUnitRevision/categoryReasonBookingUnitRevision.html', //lokasi dmn html nya
                        controller: 'CategoryReasonBookingUnitRevisionController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })

        .state('app.reasoncancelbilling', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Reason Cancel Billing',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "reasoncancelbilling@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/reasonCancelBilling/reasonCancelBilling.html', //lokasi dmn html nya
                    controller: 'ReasonCancelBillingController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.stampcost', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Stamp Cost',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "stampcost@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/stampCost/stampCost.html', //lokasi dmn html nya
                    controller: 'StampCostController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.mastertasklist', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Task List',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "mastertasklist@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/masterTaskList/masterTaskList.html', //lokasi dmn html nya
                        controller: 'MasterTaskListController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.vendor', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Vendor',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "vendor@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/vendor/vendor.html', //lokasi dmn html nya
                        controller: 'VendorController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.customerlist', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Customer List',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "customerlist@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/customerList/customerList.html', //lokasi dmn html nya
                        controller: 'CustomerListController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.customerlistpoint', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Customer List Point',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "customerlistpoint@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/customerListPoint/customerListPoint.html', //lokasi dmn html nya
                        controller: 'CustomerListPointController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.customerlistcontact', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Customer List Contact',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "customerlistcontact@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/customerListContact/customerListContact.html', //lokasi dmn html nya
                        controller: 'CustomerListContactController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.customerlistpreference', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Customer List Preference',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "customerlistpreference@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/customerListPreference/customerListPreference.html', //lokasi dmn html nya
                        controller: 'CustomerListPreferenceController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.customerlistpromo', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Customer List Promo',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "customerlistpromo@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/customerListPromo/customerListPromo.html', //lokasi dmn html nya
                        controller: 'CustomerListPromoController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.customerlistsocialmedia', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Customer List Social Media',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "customerlistsocialmedia@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/customerListSosialMedia/customerListSosialMedia.html', //lokasi dmn html nya
                        controller: 'CustomerListSocialMediaController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.customerlistunsatified', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Customer List Unsatified',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "customerlistunsatified@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/customerListUnsatified/customerListUnsatified.html', //lokasi dmn html nya
                        controller: 'CustomerListUnsatifiedController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.customerlistvehicle', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Customer List Vehicle',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "customerlistvehicle@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/customerListVehicle/customerListVehicle.html', //lokasi dmn html nya
                        controller: 'CustomerListVehicleController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.customerpreferencetp', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Customer Preference',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "customerpreferencetp@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/customerPreferenceTP/customerPreferenceTP.html', //lokasi dmn html nya
                        controller: 'CustomerPreferenceTPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.customersosialmediatp', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Customer Preference TP',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "customersosialmediatp@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/customerSosialMediaTP/customerSosialMediaTP.html', //lokasi dmn html nya
                        controller: 'CustomerSosialMediaTPController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.accountaccessiblegl', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Account Accessible GL',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "accountaccessiblegl@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/accountAccessibleGL/accountAccessibleGL.html', //lokasi dmn html nya
                        controller: 'AccountAccessibleGLController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.accountbanklist', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Account Accessible GL',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "accountbanklist@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/accountBankList/accountBankList.html', //lokasi dmn html nya
                        controller: 'AccountBankListController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.accountgl', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Account GL',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "accountgl@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/accountGL/accountGL.html', //lokasi dmn html nya
                        controller: 'AccountGLController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.accountgroupgl', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Account Group GL',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "accountgroupgl@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/accountGroupGL/accountGroupGL.html', //lokasi dmn html nya
                        controller: 'AccountGLGroupController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.accountgroup', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Account Group ',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "accountgroup@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/accountGroup/accountGroup.html', //lokasi dmn html nya
                        controller: 'AccountGroupController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.accountgroupcoarelations', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Account Group COA Relations',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "accountgroupcoarelations@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/accountGroupCOARelations/accountGroupCOARelations.html', //lokasi dmn html nya
                        controller: 'AccountGroupCOARelationsController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.accountlist', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Account List',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "accountlist@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/accountList/accountList.html', //lokasi dmn html nya
                        controller: 'AccountListController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })
            .state('app.accountlistsequence', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Account List',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "accountlistsequence@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/accountListSequence/accountListSequence.html', //lokasi dmn html nya
                        controller: 'AccountListSequenceController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]

                },
                params: { tab: null },
            })

        .state('app.categorycontact', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Category Contact',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "categorycontact@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/categoryContact/pCategoryContact.html', //lokasi dmn html nya
                        controller: 'ContactCategoryController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.deliverycategory', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Delivery Category',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "deliverycategory@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/categoryDeliveryCategory/deliveryCategory.html', //lokasi dmn html nya
                        controller: 'DeliveryCategoryController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.categorydocument', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Document Category',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "categorydocument@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/categoryDocument/categoryDocument.html', //lokasi dmn html nya
                        controller: 'CategoryDocumentController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.categoryfeedback', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Category Feedback',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "categoryfeedback@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/categoryFeedback/categoryFeedback.html', //lokasi dmn html nya
                        controller: 'CategoryFeedbackController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.categorymodelkendaraandiminati', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Category Model Kendaraan Diminati',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "categorymodelkendaraandiminati@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/categoryModelKendaraanDiminati/categoryModelKendaraanDiminati.html', //lokasi dmn html nya
                        controller: 'CategoryModelKendaraanDiminatiController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.categorypointsource', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Category Pointsource',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "categorypointsource@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/categoryPointsource/categoryPointsource.html', //lokasi dmn html nya
                        controller: 'CategoryPointsourceController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })
            .state('app.categorypreprintedttuservices', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Category PrePrinted TTUServices',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "categorypreprintedttuservices@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales9/master/categoryPrePrintedTTUServices/categoryPrePrinterdTTUServices.html', //lokasi dmn html nya
                        controller: 'CategoryPrePrinterdTTUServicesController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales9');
                    }]
                },
                params: { tab: null },
            })

        .state('app.accountinggltype', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Accounting GL Type',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "accountinggltype@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/accountingGlType/accountingGlType.html', //lokasi dmn html nya
                    controller: 'AccountingGlTypeController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.pddbuffer', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'PDD Buffer',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "pddbuffer@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/baru/pddBuffer/pddBuffer.html', //lokasi dmn html nya
                    controller: 'PddBufferController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.leadtimebookingunitkendaraan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'lead time booking unit kendaraan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "leadtimebookingunitkendaraan@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/baru/leadTimeBookingUnitKendaraan/leadTimeBookingUnitKendaraan.html', //lokasi dmn html nya
                    controller: 'LeadTimeBookingUnitKendaraanController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.leadtimeprospectfu', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'lead time booking unit kendaraan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "leadtimeprospectfu@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/baru/leadTimeProspectFu/leadTimeProspectFu.html', //lokasi dmn html nya
                    controller: 'LeadTimeProspectFuController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.accessorypackage', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'accessory package',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "accessorypackage@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/baru/accessoryPackage/accessoryPackage.html', //lokasi dmn html nya
                    controller: 'AccessoryPackageController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.categoryprospectvariable', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'category Prospect Variable',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "categoryprospectvariable@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/baru/categoryProspectVariable/categoryProspectVariable.html', //lokasi dmn html nya
                    controller: 'CategoryProspectVariableController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.arearestriction', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'area restriction',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "arearestriction@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/baru/areaRestriction/areaRestriction.html', //lokasi dmn html nya
                    controller: 'AreaRestrictionController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.agama', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'agama',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "agama@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/agama/agama.html', //lokasi dmn html nya
                    controller: 'AgamaController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })
		
		.state('app.mastertop', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'mastertop',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "mastertop@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/masterTop/masterTop.html', //lokasi dmn html nya
                    controller: 'MasterTopController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.masterkaroseri', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'masterkaroseri',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "masterkaroseri@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/karoseri/karoseri.html', //lokasi dmn html nya
                    controller: 'karoseriController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })
		
		.state('app.konfigurasiregistration', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'konfigurasiregistration',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "konfigurasiregistration@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/konfigurasiRegistration/konfigurasiRegistration.html', //lokasi dmn html nya
                    controller: 'KonfigurasiRegistrationController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })
		
		.state('app.vendorjasa', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'vendorjasa',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "vendorjasa@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/vendorJasa/vendorJasa.html', //lokasi dmn html nya
                    controller: 'VendorJasaController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })
		
		.state('app.jasa', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'jasa',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "jasa@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/jasa/jasa.html', //lokasi dmn html nya
                    controller: 'JasaController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })
		
		.state('app.masteroutlet', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'masteroutlet',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "masteroutlet@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/outlet/outlet.html', //lokasi dmn html nya
                    controller: 'OutletController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })
		
		.state('app.masterfollowupdec', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'masterfollowupdec',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "masterfollowupdec@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/masterFollowUpDec/masterFollowUpDec.html', //lokasi dmn html nya
                    controller: 'MasterFollowUpDecController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.suku', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'suku',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "suku@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/suku/suku.html', //lokasi dmn html nya
                    controller: 'SukuController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.jabatan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'jabatan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "jabatan@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/jabatan/jabatan.html', //lokasi dmn html nya
                    controller: 'JabatanController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.leadtimebookingfeedanpenyerahanbookingfeekekasir', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'leadtimebookingfeedanpenyerahanbookingfeekekasir',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "leadtimebookingfeedanpenyerahanbookingfeekekasir@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/leadTimeBookingFeeDanPenyerahanBookingFeeKeKasir/leadTimeBookingFeeDanPenyerahanBookingFeeKeKasir.html', //lokasi dmn html nya
                    controller: 'LeadTimeBookingFeeDanPenyerahanBookingFeeKeKasirController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.kategoripelanggan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'kategoripelanggan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "kategoripelanggan@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/kategoriPelanggan/kategoriPelanggan.html', //lokasi dmn html nya
                    controller: 'KategoriPelangganController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.sumberprospect', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'sumberprospect',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "sumberprospect@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/sumberProspect/sumberProspect.html', //lokasi dmn html nya
                    controller: 'SumberProspectController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.penggunaankendaraan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'penggunaankendaraan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "penggunaankendaraan@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/penggunaanKendaraan/penggunaanKendaraan.html', //lokasi dmn html nya
                    controller: 'PenggunaanKendaraanController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.sektorbisnis', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'sektorbisnis',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "sektorbisnis@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/sektorBisnis/sektorBisnis.html', //lokasi dmn html nya
                    controller: 'SektorBisnisController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.metodepembayaran', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'metodepembayaran',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "metodepembayaran@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/metodePembayaran/metodePembayaran.html', //lokasi dmn html nya
                    controller: 'MetodePembayaranController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.tipetugas', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'tipetugas',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "tipetugas@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/tipeTugas/tipeTugas.html', //lokasi dmn html nya
                    controller: 'TipeTugasController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.employee', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'employee',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "employee@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/employee/employee.html', //lokasi dmn html nya
                    controller: 'EmployeeController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.provinsikabkeckelkodepos', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'provinsikabkeckelkodepos',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "provinsikabkeckelkodepos@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/provinsiKabKecKelKodePos/provinsiKabKecKelKodePos.html', //lokasi dmn html nya
                    controller: 'ProvinsiKabKecKelKodePosController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.groupchecklistfinalinspection', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'groupchecklistfinalinspection',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "groupchecklistfinalinspection@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/groupChecklistFinalInspection/groupChecklistFinalInspection.html', //lokasi dmn html nya
                    controller: 'GroupChecklistFinalInspectionController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.checklistinspeksi', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'checklistinspeksi',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "checklistinspeksi@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/checklistInspeksi/checklistInspeksi.html', //lokasi dmn html nya
                    controller: 'ChecklistInspeksiController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })
		
		.state('app.pelanggaranwilayah', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'pelanggaranwilayah',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "pelanggaranwilayah@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/pelanggaranWilayah/pelanggaranWilayah.html', //lokasi dmn html nya
                    controller: 'PelanggaranWilayahController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.tujuanpengiriman', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'tujuanpengiriman',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "tujuanpengiriman@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/tujuanPengiriman/tujuanPengiriman.html', //lokasi dmn html nya
                    controller: 'TujuanPengirimanController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })

        .state('app.alasanrevisipdd', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'alasanrevisipdd',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "alasanrevisipdd@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/alasanRevisiPdd/alasanRevisiPdd.html', //lokasi dmn html nya
                    controller: 'AlasanRevisiPddController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.dpdanbookingfee', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'dpdanbookingfee',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "dpdanbookingfee@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/dpDanBookingFee/dpDanBookingFee.html', //lokasi dmn html nya
                    controller: 'DpDanBookingFeeController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.checklistitemadministrasi', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'checklistitemadministrasi',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "checklistitemadministrasi@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/checklistItemAdministrasi/checklistItemAdministrasi.html', //lokasi dmn html nya
                    controller: 'ChecklistItemAdministrasiController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.tiperole', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'tiperole',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "tiperole@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/tipeRole/tipeRole.html', //lokasi dmn html nya
                    controller: 'TipeRoleController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.areatam', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'areatam',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "areatam@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/areaDealer/areaDealer.html', //lokasi dmn html nya
                    controller: 'AreaDealerController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })
		
		.state('app.masterareadealersales', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'masterareadealersales',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "masterareadealersales@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/masterAreaDealer/masterAreaDealer.html', //lokasi dmn html nya
                    controller: 'MasterAreaDealerController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.diskon', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'diskon',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "diskon@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/diskon/diskon.html', //lokasi dmn html nya
                    controller: 'DiskonController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.alasan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'alasan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "alasan@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/alasan/alasan.html', //lokasi dmn html nya
                    controller: 'AlasanController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.leadtimeadministrasipdd', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'leadtimeadministrasipdd',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "leadtimeadministrasipdd@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/leadtimeAdministrasiPdd/leadtimeAdministrasiPdd.html', //lokasi dmn html nya
                    controller: 'LeadtimeAdministrasiPddController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.settingancetakan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'settingancetakan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "settingancetakan@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/settinganCetakan/settinganCetakan.html', //lokasi dmn html nya
                    controller: 'SettinganCetakanController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.setstatusfleet', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Status Fleet',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "setstatusfleet@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/setStatusFleet/setStatusFleet.html', //lokasi dmn html nya
                    controller: 'SetStatusFleetController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.vehicleModelDms', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Vehicle Model',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "vehicleModelDms@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/vehicleModel/vehicleModel.html', //lokasi dmn html nya
                    controller: 'VehicleModelController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })
		
		.state('app.vehicleMappingDms', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Vehicle Mapping',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "vehicleMappingDms@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/vehicleTypeColor/vehicleTypeColor.html', //lokasi dmn html nya
                    controller: 'VehicleTypeColorController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })
		
		.state('app.powerBISales', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Analityc Report',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "powerBISales@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/powerBI/powerBI.html', //lokasi dmn html nya
                    controller: 'PowerBIController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        /*---------END Master Data SALES----------*/

        /*---------BEGIN Daily Sales Activity SALES----------*/

        .state('app.addtasklist', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Add Task List',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "addtasklist@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/dailySalesActivity/addTaskList/addTaskList.html', //lokasi dmn html nya
                    controller: 'AddTaskListController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.assigntask', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Assign Task',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "assigntask@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/dailySalesActivity/assignTask/assignTask.html', //lokasi dmn html nya
                    controller: 'AssignTaskController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.daftarsuspect', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Daftar Suspect',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "daftarsuspect@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/dailySalesActivity/daftarSuspect/daftarSuspect.html', //lokasi dmn html nya
                    controller: 'DaftarSuspectController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.listprospect', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'List Prospect',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "listprospect@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/dailySalesActivity/listProspect/listProspect.html', //lokasi dmn html nya
                    controller: 'ListProspectController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]
            },
            params: { tab: null },
        })

        .state('app.lookupprospect', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Lookup Prospect',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "lookupprospect@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/dailySalesActivity/lookupProspect/lookupProspect.html', //lokasi dmn html nya
                    controller: 'LookupProspectController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.lookupsales', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Lookup Sales',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "lookupsales@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/dailySalesActivity/lookupSales/lookupSales.html', //lokasi dmn html nya
                    controller: 'LookupSalesController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.maintainsuspect', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Maintain Suspect',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "maintainsuspect@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/dailySalesActivity/maintainSuspect/maintainSuspect.html', //lokasi dmn html nya
                    controller: 'MaintainSuspectController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.movingtask', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Moving Task',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "movingtask@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/dailySalesActivity/movingTask/movingTask.html', //lokasi dmn html nya
                    controller: 'MovingTaskController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.rescheduletask', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Reschedule Task',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "rescheduletask@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/dailySalesActivity/rescheduleTask/rescheduleTask.html', //lokasi dmn html nya
                    controller: 'RescheduleTaskController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.viewapproval', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'View Approval',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "viewapproval@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/dailySalesActivity/viewApproval/viewApproval.html', //lokasi dmn html nya
                    controller: 'ViewApprovalController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.viewsalesman', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'View Salesman',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "viewsalesman@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/dailySalesActivity/viewSalesman/viewSalesman.html', //lokasi dmn html nya
                    controller: 'ViewSalesmanController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.viewtasklist', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'View Task List',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "viewtasklist@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/dailySalesActivity/viewTaskList/viewTaskList.html', //lokasi dmn html nya
                    controller: 'ViewTaskListController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        /*---------END Daily Sales Activity SALES----------*/


        /*---------BEGIN PDD SALES----------*/
        
        .state('app.maintainpdd', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Maintain PDD',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "maintainpdd@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales3/pdd/maintainPDD/maintainPDD.html', //lokasi dmn html nya
                    controller: 'MaintainPDDController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales3');
                }]

            },
            params: { tab: null },
        })

        /*---------END PDD SALES----------*/


        /*---------BEGIN REGISTRATION SALES----------*/
        
        .state('app.monitoringstnkbpkb', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Monitoring STNK dan BPKB',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "monitoringstnkbpkb@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales7/registration/monitoringSTNKdanBPKB/monitoringSTNKdanBPKB.html', //lokasi dmn html nya
                    controller: 'MonitoringSTNKBPKBController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales7');
                }]

            },
            params: { tab: null },
        })

        .state('app.approvalrevisibatalafi', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Approval Revisi Batal Afi',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "approvalrevisibatalafi@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales7/registration/approvalRevisiBatalAfi/approvalRevisiBatalAfi.html', //lokasi dmn html nya
                    controller: 'ApprovalRevisiBatalAfiController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales7');
                }]

            },
            params: { tab: null },
        })

        .state('app.maintainafi', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Maintain AFI',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "maintainafi@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales7/registration/maintainAFI/maintainAfi.html', //lokasi dmn html nya
                        controller: 'MaintainAfiController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales7');
                    }]

                },
                params: { tab: null },
            })
            .state('app.ajuafitam', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Aju AFI TAM',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "ajuafitam@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales7/registration/ajuAfiTAM/ajuAfiTAM.html', //lokasi dmn html nya
                        controller: 'AjuAfiTAMController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales7');
                    }]

                },
                params: { tab: null },
            })
            .state('app.terimadokumenfakturtam', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Aju AFI TAM',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "terimadokumenfakturtam@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales7/registration/terimaDokumenFakturTAM/terimaDokumenFakturTAM.html', //lokasi dmn html nya
                        controller: 'TerimaDokumenFakturTAMController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales7');
                    }]

                },
                params: { tab: null },
            })
            .state('app.maintainsuratdistribusi', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Maintain Surat Distribusi',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "maintainsuratdistribusi@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales7/registration/maintainSuratDistribusi/maintainSuratDistribusi.html', //lokasi dmn html nya
                        controller: 'MaintainSuratDistribusiController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales7');
                    }]

                },
                params: { tab: null },
            })
            .state('app.maintainsuratdistribusistnk', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Maintain Surat Distribusi',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "maintainsuratdistribusistnk@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales7/registration/maintainSuratDistribusiSTNK/maintainSuratDistribusiSTNK.html', //lokasi dmn html nya
                        controller: 'MaintainSuratDistribusiSTNKController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales7');
                    }]

                },
                params: { tab: null },
            })
            .state('app.maintainpobirojasa', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Maintain Surat Distribusi',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "maintainpobirojasa@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales7/registration/maintainPOBirojasa/maintainPOBirojasa.html', //lokasi dmn html nya
                        controller: 'MaintainPOBirojasaController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales7');
                    }]

                },
                params: { tab: null },
            })
            .state('app.terimastnkbirojasa', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Maintain Surat Distribusi',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "terimastnkbirojasa@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales7/registration/terimaSTNKdariBiroJasa/terimaSTNKdariBirojasa.html', //lokasi dmn html nya
                        controller: 'TerimaSTNKBirojasaController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales7');
                    }]

                },
                params: { tab: null },
            })
            .state('app.terimabpkb', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Terima BPKB',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "terimabpkb@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales7/registration/terimaBpkb/terimaBpkb.html', //lokasi dmn html nya
                        controller: 'TerimaBpkbController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales7');
                    }]

                },
                params: { tab: null },
            })
            .state('app.maintainsuratdistribusibpkb', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'maintain surat distribusi bpkb',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "maintainsuratdistribusibpkb@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales7/registration/maintainSuratDistribusiBpkb/maintainSuratDistribusiBpkb.html', //lokasi dmn html nya
                        controller: 'MaintainSuratDistribusiBpkbController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales7');
                    }]

                },
                params: { tab: null },
            })
            .state('app.terimadokumenpelanggan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Terima Dokumen Pelanggan',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "terimadokumenpelanggan@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales7/registration/terimaDokumenPelanggan/terimaDokumenPelanggan.html', //lokasi dmn html nya
                        controller: 'TerimaDokumenPelangganController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales7');
                    }]

                },
                params: { tab: null },
            })
            .state('app.serahterimafakturbpkbpelanggan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Terima Dokumen Pelanggan',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "serahterimafakturbpkbpelanggan@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales7/registration/serahTerimaFakturDanBpkbKePelanggan/serahTerimaFakturDanBpkbKePelanggan.html', //lokasi dmn html nya
                        controller: 'SerahTerimaFakturDanBpkbKePelangganController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales7');
                    }]

                },
                params: { tab: null },
            })
            .state('app.serahterimastnkpelanggan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Terima Dokumen Pelanggan',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "serahterimastnkpelanggan@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales7/registration/serahTerimaStnkKePelanggan/serahTerimaStnkKePelanggan.html', //lokasi dmn html nya
                        controller: 'SerahTerimaStnkKePelangganController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales7');
                    }]

                },
                params: { tab: null },
            })
            .state('app.terimadokumenbpkb', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Terima Dokumen Pelanggan',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "terimadokumenbpkb@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales7/registration/terimaDokumenBpkb/terimaDokumenBpkb.html', //lokasi dmn html nya
                        controller: 'TerimaDokumenBpkbController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales7');
                    }]

                },
                params: { tab: null },
            })
            .state('app.terimadokumenstnk', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Terima Dokumen Pelanggan',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "terimadokumenstnk@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales7/registration/terimaDokumenStnk/terimaDokumenStnk.html', //lokasi dmn html nya
                        controller: 'TerimaDokumenStnkController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales7');
                    }]

                },
                params: { tab: null },
            })


            .state('app.goodreceiptstnk', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Good Receipt STNK',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "goodreceiptstnk@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales7/registration/goodReceiptSTNK/goodReceiptSTNK.html', //lokasi dmn html nya
                        controller: 'goodReceiptSTNKController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales7');
                    }]

                },
                params: { tab: null },
            })
            

        /*---------END REGISTRATION SALES----------*/



        /*---------BEGIN PROSPECTING SALES----------*/

        .state('app.prospect', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'prospect',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "prospect@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/prospecting/prospect/prospect.html', //lokasi dmn html nya
                    controller: 'ProspectController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.approvaldropsalessupervisor', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'approvaldropsalessupervisor',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "approvaldropsalessupervisor@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/prospecting/approvalDropSalesSupervisor/approvalDropSalesSupervisor.html', //lokasi dmn html nya
                    controller: 'ApprovalDropSalesSupervisorController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.klasifikasipembelian', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'klasifikasipembelian',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "klasifikasipembelian@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/klasifikasiPembelian/klasifikasiPembelian.html', //lokasi dmn html nya
                    controller: 'KlasifikasiPembelianController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]

            },
            params: { tab: null },
        })



        /*---------END PROSPECTING SALES----------*/

        /*---------BEGIN ADMIN HANDLING SALES----------*/
        .state('app.maintainso', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'MaintainSO',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "maintainso@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales2/adminHandling/maintainSO/maintainSO.html', //lokasi dmn html nya
                    controller: 'MaintainSOController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales2');
                }]
            },
            params: { tab: null },
        })
		
		.state('app.maintainppatk', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'MaintainPpatk',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "maintainppatk@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales2/adminHandling/maintainPpatk/maintainPpatk.html', //lokasi dmn html nya
                    controller: 'MaintainPpatkController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales2');
                }]
            },
            params: { tab: null },
        })

        .state('app.maintaintop', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Maintain TOP',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "maintaintop@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales2/adminHandling/tOP/maintainTOP/tOP.html', //lokasi dmn html nya
                    controller: 'TOPController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales2');
                }]
            },
            params: { tab: null },
        })

        .state('app.persetujuan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Persetujuan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "persetujuan@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales2/adminHandling/persetujuan/persetujuan/persetujuan.html', //lokasi dmn html nya
                    controller: 'PersetujuanController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales2');
                }]
            },
            params: { tab: null },
        })

        .state('app.approval', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Approval',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "approval@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales2/adminHandling/persetujuan/approval/approval.html', //lokasi dmn html nya
                    controller: 'ApprovalController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales2');
                }]
            },
            params: { tab: null },
        })

        .state('app.maintainbilling', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'History Pengajuan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "maintainbilling@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales2/adminHandling/maintainBilling/maintainBilling.html', //lokasi dmn html nya
                    controller: 'MaintainBillingController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales2');
                }]
            },
            params: { tab: null },
        })

        .state('app.proformainvoice', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Proforma Invoice',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "proformainvoice@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales2/adminHandling/maintainPI/maintainPI.html', //lokasi dmn html nya
                    controller: 'MaintainPIController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales2');
                }]
            },
            params: { tab: null },
        })
		
		.state('app.approvalpi', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Approval Proforma Invoice',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "approvalpi@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales2/adminHandling/approvalPI/approvalPI.html', //lokasi dmn html nya
                    controller: 'ApprovalPIController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales2');
                }]
            },
            params: { tab: null },
        })

        .state('app.purchaseorder', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'History Pengajuan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "purchaseorder@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales2/adminHandling/maintainPO/maintainPO.html', //lokasi dmn html nya
                    controller: 'MaintainPOController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales2');
                }]
            },
            params: { tab: null },
        })

        .state('app.aksesorisstandard', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Maintain PO Aksesoris Standar',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "aksesorisstandard@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales2/adminHandling/maintainPOAksesorisStandar/maintainPOAksesorisStandar.html', //lokasi dmn html nya
                    controller: 'MaintainPOAksesorisStandarController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales2');
                }]
            },
            params: { tab: null },
        })

        .state('app.graksesorisstandard', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Good Receipt Aksesoris Standar',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "graksesorisstandard@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales2/adminHandling/goodReceiptAccStandar/goodReceiptAccStandar.html', //lokasi dmn html nya
                    controller: 'GoodReceiptAccStandarController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales2');
                }]
            },
            params: { tab: null },
        })

        .state('app.maintainacc', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Mapping Aksesoris',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "maintainacc@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales9/master/mappingAcc/mappingAksesoris.html', //lokasi dmn html nya
                    controller: 'MappingAksesorisController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales9');
                }]
            },
            params: { tab: null },
        })

        .state('app.goodreceipt', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'History Pengajuan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "goodreceipt@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales2/adminHandling/goodReceipt/goodReceipt.html', //lokasi dmn html nya
                    controller: 'GoodReceiptController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales2');
                }]
            },
            params: { tab: null },
        })

        /*---------END ADMIN HANDLING SALES----------*/


        /*---------BEGIN DELIVERY REQUEST SALES----------*/
        .state('app.monitoringjadwaldriver', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Monitoring Jadwal Driver',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "monitoringjadwaldriver@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales4/deliveryRequest/monitoringJadwalDriver/monitoringJadwalDriver.html', //lokasi dmn html nya
                    controller: 'MonitoringJadwalDriverController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales4');
                }]

            },
            params: { tab: null },
        })

        .state('app.approvalurgentmemo', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Approval Urgent Memo',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "approvalurgentmemo@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales4/deliveryRequest/approvalUrgentMemo/approvalUrgentMemo.html', //lokasi dmn html nya
                    controller: 'ApprovalUrgentMemoController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales4');
                }]

            },
            params: { tab: null },
        })

        .state('app.daftarunitmasuk', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Daftar Unit Masuk',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "daftarunitmasuk@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales4/deliveryRequest/daftarUnitMasuk/daftarUnitMasuk.html', //lokasi dmn html nya
                    controller: 'DaftarUnitMasukNewController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales4');
                }]

            },
            params: { tab: null },
        })

        .state('app.daftarunitrepair', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Daftar Unit Repair',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "daftarunitrepair@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales4/deliveryRequest/daftarUnitRepair/daftarUnitRepair.html', //lokasi dmn html nya
                    controller: 'DaftarUnitRepairController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales4');
                }]

            },
            params: { tab: null },
        })

        .state('app.followupklaimasuransi', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'FollowUp Klaim Asuransi',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "followupklaimasuransi@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales4/deliveryRequest/followUpKlaimAsuransi/followUpKlaimAsuransi.html', //lokasi dmn html nya
                    controller: 'FollowUpKlaimAsuransiController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales4');
                }]

            },
            params: { tab: null },
        })

        .state('app.daftarurgentmemo', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Daftar Urgent Memo',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "daftarurgentmemo@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales4/deliveryRequest/daftarUrgentMemo/daftarUrgentMemo.html', //lokasi dmn html nya
                    controller: 'DaftarUrgentMemoController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales4');
                }]

            },
            params: { tab: null },
        })

        .state('app.lookupdataspk', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'LookUp Data SPK',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "lookupdataspk@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales4/deliveryRequest/lookUpDataSPK/lookUpDataSPK.html', //lokasi dmn html nya
                    controller: 'LookUpDataSPKController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales4');
                }]

            },
            params: { tab: null },
        })

        .state('app.checklistpds', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Checklist PDS',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "checklistpds@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales4/deliveryRequest/checklistPDS/checklistPDS.html', //lokasi dmn html nya
                        controller: 'ChecklistPDSController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales4');
                    }]

                },
                params: { tab: null },
            })
            .state('app.getingetout', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Get In Get Out',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "getingetout@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales4/deliveryRequest/getInGetOut/getInGetOut.html', //lokasi dmn html nya
                        controller: 'GetInGetOutController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales4');
                    }]

                },
                params: { tab: null },
            })
            .state('app.deliverynote', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Delivery Note',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "deliverynote@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales4/deliveryRequest/deliveryNote/deliveryNote.html', //lokasi dmn html nya
                        controller: 'DeliveryNoteController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales4');
                    }]

                },
                params: { tab: null },
            })
            .state('app.listtaskdriver', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'List Task Driver',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "listtaskdriver@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales4/deliveryRequest/listTaskDriver/listTaskDriver.html', //lokasi dmn html nya
                        controller: 'ListTaskDriverController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales4');
                    }]

                },
                params: { tab: null },
            })


        /*---------END DELIVERY REQUEST SALES----------*/


        /*---------BEGIN DEMAND SUPPLY SALES----------*/

        .state('app.approvalswappingho', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Approval Swapping Ho',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "approvalswappingho@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales5/demandSupply/approvalSwappingHo/approvalSwappingHo.html', //lokasi dmn html nya
                    controller: 'ApprovalSwappingHoController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales5');
                }]

            },
            params: { tab: null },
        })

        .state('app.approvalreceivequotation', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Approval Receive Quotation',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "approvalreceivequotation@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales5/demandSupply/approvalReceiveQuotation/approvalReceiveQuotation.html', //lokasi dmn html nya
                    controller: 'ApprovalReceiveQuotationController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales5');
                }]

            },
            params: { tab: null },
        })

        .state('app.approvalsendingquotation', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Approval Sending Quotation',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "approvalsendingquotation@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales5/demandSupply/approvalSendingQuotation/approvalSendingQuotation.html', //lokasi dmn html nya
                    controller: 'ApprovalSendingQuotationController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales5');
                }]

            },
            params: { tab: null },
        })

        .state('app.maintainswappingnondms', { //format harus "app.nama_menu" untuk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Maintain Swapping Non DMS',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "maintainswappingnondms@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales5/demandSupply/maintainSwappingIn/maintainSwappingIn.html', //lokasi dmn html nya
                    controller: 'MaintainSwappingInController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales5');
                }]

            },
            params: { tab: null },
        })

        .state('app.stockview', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Stock View',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "stockview@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales5/demandSupply/stockView/stockView.html', //lokasi dmn html nya
                    controller: 'StockViewController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales5');
                }]

            },
            params: { tab: null },
        })

        .state('app.stockviewmobile', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Stock View Mobile',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "stockviewmobile@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales5/demandSupply/stockViewMobile/stockViewMobile.html', //lokasi dmn html nya
                    controller: 'StockViewMobileController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales5');
                }]

            },
            params: { tab: null },
        })

        .state('app.swappingsugestion', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Swapping Sugestion',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "swappingsugestion@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales5/demandSupply/swappingSugestion/swappingSugestion.html', //lokasi dmn html nya
                    controller: 'SwappingSugestionController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales5');
                }]

            },
            params: { tab: null },
        })

        .state('app.findstock', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Find Stock',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "findstock@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales5/demandSupply/findStock/findStock.html', //lokasi dmn html nya
                    controller: 'FindStockController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales5');
                }]

            },
            params: { tab: null },
        })

        .state('app.manualmatchingprosses', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Matching Process',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "manualmatchingprosses@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales5/demandSupply/manualMatchingProsses/manualMatchingProsses.html', //lokasi dmn html nya
                        controller: 'ManualMatchingProssesController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales5');
                    }]

                },
                params: { tab: null },
            })
            .state('app.maintainrequestswapping', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Matching Process',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "maintainrequestswapping@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales5/demandSupply/maintainRequestSwapping/maintainRequestSwapping.html', //lokasi dmn html nya
                        controller: 'MaintainRequestSwappingController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales5');
                    }]

                },
                params: { tab: null },
            })
            .state('app.summarystock', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Matching Process',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "summarystock@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales5/demandSupply/summaryStock/summaryStock.html', //lokasi dmn html nya
                        controller: 'SummaryStockController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales5');
                    }]

                },
                params: { tab: null },
            })
            .state('app.findlocation', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Matching Process',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "findlocation@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales5/demandSupply/findLocation/findLocation.html', //lokasi dmn html nya
                        controller: 'FindLocationController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales5');
                    }]

                },
                params: { tab: null },
            })
            .state('app.modelassignment', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Model Assignment',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "modelassignment@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales5/demandSupply/modelAssignment/modelAssignment.html', //lokasi dmn html nya
                        controller: 'ModelAssignmentController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales5');
                    }]

                },
                params: { tab: null },
            })
            .state('app.targetassignment', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Target Assignment',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "targetassignment@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales5/demandSupply/targetAssignment/targetAssignment.html', //lokasi dmn html nya
                        controller: 'TargetAssignmentController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales5');
                    }]

                },
                params: { tab: null },
            })
            .state('app.salescompositionhistory', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Composition History',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "salescompositionhistory@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales5/demandSupply/salesCompositionHistory/salesCompositionHistory.html', //lokasi dmn html nya
                        controller: 'SalesCompositionHistoryController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales5');
                    }]

                },
                params: { tab: null },
            })
            .state('app.matchingstockdealer', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Matching Process',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "matchingstockdealer@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales5/demandSupply/matchingStockDealer/matchingStockDealer.html', //lokasi dmn html nya
                        controller: 'MatchingStockDealerController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales5');
                    }]

                },
                params: { tab: null },
            })
            .state('app.maintainquotation', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Matching Process',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "maintainquotation@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales5/demandSupply/maintainQuotation/maintainQuotation.html', //lokasi dmn html nya
                        controller: 'MaintainQuotationController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales5');
                    }]

                },
                params: { tab: null },
            })
            .state('app.dsmaintainpo', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Matching Process',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "dsmaintainpo@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales5/demandSupply/dsmaintainPO/dsmaintainPO.html', //lokasi dmn html nya
                        controller: 'DsMaintainPOController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales5');
                    }]

                },
                params: { tab: null },
            })
            .state('app.maintaints', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Matching Process',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "maintaints@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales5/demandSupply/maintainTS/maintainTS.html', //lokasi dmn html nya
                        controller: 'MaintainTSController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales5');
                    }]

                },
                params: { tab: null },
            })
            /*---------END DEMAND SUPPLY SALES----------*/


        /*---------BEGIN DEC SALES----------*/
        .state('app.approvalpengirimandenganpengecualian', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Approval Pengiriman Dengan Pengecualian',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "approvalpengirimandenganpengecualian@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales6/dec/approvalPengirimanDenganPengecualian/approvalPengirimanDenganPengecualian.html', //lokasi dmn html nya
                    controller: 'ApprovalPengirimanDenganPengecualianController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales6');
                }]

            },
            params: { tab: null },
        })

        .state('app.listdec', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'List DEC',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "listdec@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales6/dec/listDec/listDec.html', //lokasi dmn html nya
                    controller: 'ListDecController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales6');
                }]

            },
            params: { tab: null },
        })

        .state('app.followupdec', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'List DEC',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "followupdec@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales6/dec/followUpDec/followUpDec.html', //lokasi dmn html nya
                    controller: 'FollowUpDecController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales6');
                }]

            },
            params: { tab: null },
        })

        .state('app.konfirmasiwaktudanalasanpengiriman', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'konfirmasi waktu dan alasan pengiriman',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "konfirmasiwaktudanalasanpengiriman@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales6/dec/konfirmasiWaktuDanAlasanPengiriman/konfirmasiWaktuDanAlasanPengiriman.html', //lokasi dmn html nya
                    controller: 'KonfirmasiWaktuDanAlasanPengirimanController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales6');
                }]

            },
            params: { tab: null },
        })

        .state('app.cetakbstkb', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'cetakbstkb',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "cetakbstkb@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales6/dec/cetakBstkb/cetakBstkb.html', //lokasi dmn html nya
                    controller: 'CetakBstkbController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales6');
                }]

            },
            params: { tab: null },
        })

        .state('app.deliveryunit', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Delivery Unit',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "deliveryunit@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales6/dec/deliveryUnit/deliveryUnit.html', //lokasi dmn html nya
                    controller: 'DeliveryUnitController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales6');
                }]

            },
            params: { tab: null },
        })

        .state('app.listdeliveryunit', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'List Delivery Unit',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "listdeliveryunit@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales6/dec/listDeliveryUnit/listDeliveryUnit.html', //lokasi dmn html nya
                    controller: 'ListDeliveryUnitController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales6');
                }]

            },
            params: { tab: null },
        })

        .state('app.getinout', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Get In Out',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "getinout@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales6/dec/getInOut/getInOut.html', //lokasi dmn html nya
                    controller: 'getInOutController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales6');
                }]
            },
            params: { tab: null },
        })

        /*---------END DEC SALES----------*/

        /*---------BEGIN NEGOTIATION SALES----------*/

        .state('app.bookingunit', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'bookingUnit',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "bookingunit@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/negotiation/bookingUnit/bookingUnit.html', //lokasi dmn html nya
                    controller: 'BookingUnitController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.testdrivepersetujuan', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'testdrivepersetujuan',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "testdrivepersetujuan@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/negotiation/testDrivePersetujuan/testDrivePersetujuan.html', //lokasi dmn html nya
                    controller: 'TestDrivePersetujuanController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.approvaldiskonnegotiation', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'approvaldiskonnegotiation',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "approvaldiskonnegotiation@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/negotiation/approvalDiskonNegotiation/approvalDiskonNegotiation.html', //lokasi dmn html nya
                    controller: 'ApprovalDiskonNegotiationController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.simulasikredit', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'simulasikredit',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "simulasikredit@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/negotiation/simulasiKredit/simulasiKredit.html', //lokasi dmn html nya
                    controller: 'SimulasiKreditController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })



        .state('app.negotiation', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Test Drive Request',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "negotiation@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/negotiation/negotiation/negotiation.html', //lokasi dmn html nya
                    controller: 'NegotiationController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.listbookingunit', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'listbookingunit',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "listbookingunit@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/negotiation/listBookingUnit/listBookingUnit.html', //lokasi dmn html nya
                    controller: 'ListBookingUnitController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })


        .state('app.listbrosur', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'List Brosur',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "listbrosur@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/negotiation/listBrosur/listBrosur.html', //lokasi dmn html nya
                    controller: 'ListBrosurController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.brosursalesmanview', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Brosur Salesman View',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "brosursalesmanview@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/negotiation/brosurSalesmanView/brosurSalesmanView.html', //lokasi dmn html nya
                    controller: 'BrosurSalesmanViewController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })


        /*---------END NEGOTIATION SALES----------*/

        /*---------BEGIN SPK SALES----------*/
        .state('app.approvaldiskonspk', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'approval diskon spk',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "approvaldiskonspk@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/spk/approvalDiskonSpk/approvalDiskonSpk.html', //lokasi dmn html nya
                    controller: 'ApprovalDiskonSpkController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.approvalspkbatalhilang', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Approval Spk Batal Hilang',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "approvalspkbatalhilang@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/spk/approvalSpkBatalHilang/approvalSpkBatalHilang.html', //lokasi dmn html nya
                    controller: 'ApprovalSpkBatalHilangController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.spktaking', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'SPK Taking',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "spktaking@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/spk/createSpk/createSpk.html', //lokasi dmn html nya
                    controller: 'CreateSpkController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.distributionspk', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Distribution SPK',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "distributionspk@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/spk/distributionSpk/distributionSpk.html', //lokasi dmn html nya
                    controller: 'DistributionSpkController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.reassignspk', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Reassign SPK',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "reassignspk@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales1/spk/reassignSpk/spkTakingMobile.html', //lokasi dmn html nya
                        controller: 'SpkTakingController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales1');
                    }]

                },
                params: { tab: null },
            })
            .state('app.spkreassignweb', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Reassign SPK Web',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "spkreassignweb@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales1/spk/reassignSpkWeb/reassignWeb.html', //lokasi dmn html nya
                        controller: 'ReassignWebController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales1');
                    }]

                },
                params: { tab: null },
            })
            .state('app.maintenancespk', { //format harus "app.nama_menu" utk daftarin di sref db Menu
                //url: '/kb',
                data: {
                    title: 'Maintenance SPK',
                    access: 1
                },
                deepStateRedirect: true,
                sticky: true,
                views: {
                    "maintenancespk@app": { //nama_menu + "@app"
                        templateUrl: 'app/sales/sales1/spk/maintenanceSpk/maintenanceSpk.html', //lokasi dmn html nya
                        controller: 'MaintenanceSpkController'
                    }
                },
                resolve: {
                    sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('sysapp');
                    }],
                    AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('app_sales1');
                    }]

                },
                params: { tab: null },
            })

        .state('app.cekspkvalid', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Cek SPK Valid',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "cekspkvalid@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/spk/cekSpkValid/cekSpkValid.html', //lokasi dmn html nya
                    controller: 'CekSpkValidController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.cekspkvaliddiskon', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Cek SPK Valid',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "cekspkvaliddiskon@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/spk/cekSpkValidDiskon/cekSpkValidDiskon.html', //lokasi dmn html nya
                    controller: 'CekSpkValidDiskonController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })


        .state('app.purchaserequisition', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Purchase Requisition',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "purchaserequisition@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales2/spk/purchaseRequisition/purchaseRequisition.html', //lokasi dmn html nya
                    controller: 'PurchaseRequisitionController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales2');
                }]

            },
            params: { tab: null },
        })

        .state('app.distribusispk', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Distribusi SPK',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "distribusispk@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/spk/distribusiSpk/distribusiSpk.html', //lokasi dmn html nya
                    controller: 'DistribusiSpkController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.spk', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Distribusi SPK',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "spk@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/spk/spk/spk.html', //lokasi dmn html nya
                    controller: 'SpkController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })

        .state('app.spksign', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'SPK Signature',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "spksign@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales1/spk/spkSignature/spkSignature.html', //lokasi dmn html nya
                    controller: 'TemplateDigitalSignatureController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales1');
                }]

            },
            params: { tab: null },
        })
        .state('app.maintainswappingho', { //format harus "app.nama_menu" utk daftarin di sref db Menu
            //url: '/kb',
            data: {
                title: 'Maintain Swapping HO',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "maintainswappingho@app": { //nama_menu + "@app"
                    templateUrl: 'app/sales/sales5/demandSupply/maintainSwappingHO/maintainSwappingHO.html', //lokasi dmn html nya
                    controller: 'MaintainSwappingHOController'
                }
            },
            resolve: {
                sysapp: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('sysapp');
                }],
                AppModule: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load('app_sales5');
                }]

            },
            params: { tab: null },
        })
        /*---------END SPK SALES----------*/

        /*---------BEGIN REPORT SALES----------*/
        

        /*---------END REPORT SALES----------*/

        // -------------------------------- //
        // END Application Module - SALES 	//
        // -------------------------------- //

    }])