//"use strict";

//angular.module('app', ['ui.router','ngResource']);

angular.module('app')
  .config(['$locationProvider','$stateProvider','$urlRouterProvider', function($locationProvider,$stateProvider, $urlRouterProvider) {

$stateProvider
        // -------------------------------- //
        // Application Module - BranchOrder //
        // -------------------------------- //
        .state('app.fleet', {
            //url: '/kb',
            data: {
                title: 'Customer Fleet',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "fleet@app": {
                    templateUrl: 'app/master/customerFleet/customer_fleet.html',
                    controller: 'CustomerFleetController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })
        .state('app.productCat', {
            // url: '/org',
            data: {
                title: 'Vehicle Category',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "productCat@app": {
                    templateUrl: 'app/master/product/productCat.html',
                    controller: 'ProductCatController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]
            },
            params: { tab : null },
        })
        .state('app.region', {
            // url: '/org',
            data: {
                title: 'Region',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "region@app": {
                    templateUrl: 'app/master/region/region.html',
                    controller: 'RegionController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]
            },
            params: { tab : null },
        })
        .state('app.oap', {
            data: {
                title: 'OAP',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "oap@app": {
                    templateUrl: 'app/branchOrder/oap/oap.html',
                    controller: 'OAPController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })
        .state('app.suggestion', {
            data: {
                title: 'RS Suggestion',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "suggestion@app": {
                    templateUrl: 'app/branchOrder/rs/rs.html',
                    controller: 'RSSuggestionController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })
        .state('app.marketadj', {
            data: {
                title: 'Market Adjustment',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "marketadj@app": {
                    templateUrl: 'app/branchOrder/marketadj/marketadj.html',
                    controller: 'MarketAdjController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })
        .state('app.oapperiod', {
            data: {
                title: 'OAP Period',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "oapperiod@app": {
                    templateUrl: 'app/branchOrder/oapPeriod/oapPeriod.html',
                    controller: 'OAPPeriodController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })
        .state('app.parameter', {
            // url: '/#/parameter',
            data: {
                title: 'Parameter',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "parameter@app": {
                    templateUrl: 'app/parameter/parameter.html',
                    controller: 'ParameterController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })
        .state('app.getsudoperiod', {
            //url: '/kb',
            data: {
                title: 'Getsudo Period',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "getsudoperiod@app": {
                    templateUrl: 'app/branchOrder/getsudoPeriod/getsudoPeriod.html',
                    controller: 'GetsudoPeriodController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })

        .state('app.konstanta', {
            //url: '/kb',
            data: {
                title: 'Constants Settings',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "konstanta@app": {
                    templateUrl: 'app/branchOrder/rssp/konstanta/konstanta.html',
                    controller: 'KonstantaController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })

        // .state('app.borreport', {
        .state('app.reportfilter', {
            //url: '/kb',
            data: {
                title: 'Report',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,

            views: {
                // "borreport@app": {
                //     templateUrl: 'app/branchOrder/report/report.html',
                //     controller: 'BORReportController'
                // }
                "reportfilter@app": {
                    templateUrl: 'app/branchOrder/report/reportFilter.html',
                    controller: 'ReportFilterController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })
        .state('app.rsspvar', {
            //url: '/kb',
            data: {
                title: 'RSSP Variable',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "rsspvar@app": {
                    templateUrl: 'app/branchOrder/rssp/rsspvar/rsspvar.html',
                    controller: 'RSSPVarController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })
        .state('app.rssptemp', {
            //url: '/kb',
            data: {
                title: 'RSSP Template',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "rssptemp@app": {
                    templateUrl: 'app/branchOrder/rssp/rssptemp/rssptemp.html',
                    controller: 'RSSPTemplateController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })
        .state('app.modeltemp', {
            //url: '/kb',
            data: {
                title: 'Model Template',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "modeltemp@app": {
                    templateUrl: 'app/branchOrder/rssp/modeltemp/modeltemp.html',
                    controller: 'ModelTemplateController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })

        // .state('app.oapperiod', {
        //     //url: '/kb',
        //     data: {
        //         title: 'OAP/RAP Period',
        //         access: 1
        //     },
        //     deepStateRedirect: true,
        //     sticky: true,
        //     views: {
        //         "oapperiod@app": {
        //             templateUrl: 'app/branchOrder/oapPeriod/oapPeriod.html',
        //             controller: 'OAPPeriodController'
        //         }
        //     },
        //     resolve: {
        //                sysapp: ['$ocLazyLoad',function($ocLazyLoad){
        //                  return $ocLazyLoad.load('sysapp');
        //                }],
        //                AppModule: ['$ocLazyLoad',function($ocLazyLoad){
        //                  return $ocLazyLoad.load('app_branchorder');
        //                }]

        //     },
        //     params: { tab : null },
        // })
        .state('app.vehicleModel', {
            //url: '/kb',
            data: {
                title: 'Vehicle Model',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "vehicleModel@app": {
                    templateUrl: 'app/master/vehicleModel/vehicleModel.html',
                    controller: 'VehicleModelController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })
        // .state('app.oap', {
        //     //url: '/kb',
        //     data: {
        //         title: 'OAP/RAP',
        //         access: 1
        //     },
        //     deepStateRedirect: true,
        //     sticky: true,
        //     views: {
        //         "oap@app": {
        //             templateUrl: 'app/branchOrder/oap/oap.html',
        //             controller: 'OAPController'
        //         }
        //     },
        //     resolve: {
        //                sysapp: ['$ocLazyLoad',function($ocLazyLoad){
        //                  return $ocLazyLoad.load('sysapp');
        //                }],
        //                AppModule: ['$ocLazyLoad',function($ocLazyLoad){
        //                  return $ocLazyLoad.load('app_branchorder');
        //                }]

        //     },
        //     params: { tab : null },
        // })
        .state('app.rssp', {
            //url: '/kb',
            data: {
                title: 'RSSP',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "rssp@app": {
                    templateUrl: 'app/branchOrder/rssp/rssp/rssp.html',
                    controller: 'RSSPController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })
        .state('app.rsspfleet', {
            //url: '/kb',
            data: {
                title: 'RSSP Fleet',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "rsspfleet@app": {
                    templateUrl: 'app/branchOrder/rssp/fleet/rsspfleet.html',
                    controller: 'RSSPFleetController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })
        .state('app.internaloutlet', {
            //url: '/kb',
            data: {
                title: 'Internal Outlet',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "internaloutlet@app": {
                    templateUrl: 'app/branchOrder/outlet/internaloutlet.html',
                    controller: 'InternalOutletController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })
        .state('app.rsspinternal', {
            //url: '/kb',
            data: {
                title: 'RSSP Internal',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "rsspinternal@app": {
                    templateUrl: 'app/branchOrder/rssp/internal/rsspinternal.html',
                    controller: 'RSSPInternalController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })
        .state('app.rsspmodel', {
            //url: '/kb',
            data: {
                title: 'RSSP',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "rsspmodel@app": {
                    templateUrl: 'app/branchOrder/rssp/rssp/rssp_model.html',
                    controller: 'RSSPModelController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })
        .state('app.maintenance', {
            //url: '/kb',
            data: {
                title: 'Maintenance Status',
                access: 1
            },
            deepStateRedirect: true,
            sticky: true,
            views: {
                "maintenance@app": {
                    templateUrl: 'app/branchOrder/maintenanceStatus/maintenanceStatus.html',
                    controller: 'MaintenanceStatusController'
                }
            },
            resolve: {
                       sysapp: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('sysapp');
                       }],
                       AppModule: ['$ocLazyLoad',function($ocLazyLoad){
                         return $ocLazyLoad.load('app_branchorder');
                       }]

            },
            params: { tab : null },
        })

        if(window.history && window.history.pushState){
          $locationProvider.html5Mode({enabled: true,requireBase:false});
        };
        $urlRouterProvider.deferIntercept();
  }]);